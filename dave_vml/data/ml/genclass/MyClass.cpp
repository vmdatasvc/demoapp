##ifndef TEMPLATE
##ifndef HEADER_ONLY
##ifndef FORWARD_ONLY

#include "MyClass.hpp"

namespace ait 
{

##ifdef VISION_NAMESPACE
namespace vision
{

##endif
MyClass::MyClass()
##ifdef INIT_MEMBER
: mIsInitialized(false)
##endif
{
}

##ifdef COPYABLE
MyClass::MyClass(const MyClass& from)
: mIsInitialized(from.mIsInitialized)
{
}

##endif
MyClass::~MyClass()
{
}

##ifdef INIT_MEMBER
void 
MyClass::init()
{
  // Initialize this object.

  mIsInitialized = true;
}

##endif
##ifdef COPYABLE
//
// OPERATORS
//

MyClass& 
MyClass::operator=(const MyClass& from)
{
  if (this != &from)
  {
    // Assign data members
  }
  return *this;
}

bool
MyClass::operator==(const MyClass& from) const
{
  // Compare members
  PvUtil::exitError("Comparison not implemented.");
  return false;
}

bool
MyClass::operator!=(const MyClass& from) const
{
  return !(*this == from);
}

##endif
//
// OPERATIONS
//

##ifdef EXAMPLES
bool
MyClass::repeatChar(std::string& output, const int length, const char character)
{
  // implementation...
  return true;
}

##endif
//
// ACCESS
//

//
// INQUIRY
//

##ifdef VISION_NAMESPACE
}; // namespace vision

##endif
}; // namespace ait

##endif
##endif
##endif

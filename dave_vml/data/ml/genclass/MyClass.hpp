##ifndef FORWARD_ONLY

#pragma once

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
##ifdef TEMPLATE
#include <boost/shared_ptr.hpp>
##endif
##ifdef HEADER_ONLY
#include <boost/shared_ptr.hpp>
##endif

// AIT INCLUDES
//

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

##ifndef TEMPLATE
##ifndef HEADER_ONLY
#include "MyClass_fwd.hpp"
##endif
##endif

namespace ait 
{

##ifdef VISION_NAMESPACE
namespace vision
{

##endif

/**
 * This class is not yet documented.
 * Here is a more detailed description of the class. Next come the 
 * Responsibilities of the class. Try to minimize the responsibilities, this
 * will help in creating a good design. Next, optionally write any design patterns
 * that were used for this class. Refer to the "Design Patterns" book by Gamma et al.
 * @par Responsibilities:
 * - The First Responsibility
 * - The Second Responsibility:
 *   - A sub-responsibility
 *   - Another sub-responsibility
 * - The Third Responsibility
 * @par Patterns:
 * - The first design pattern
 * - The second design pattern
 * @par Design Considerations:
 * - Here you can itemize some justifications for your decisions. It will help
 * others to understand why your class is the way it is.
 * @remarks
 * Some special comments or warnings about this class.
 * @see TheirClass, OurClass (other related classes).
 */
##ifdef TEMPLATE
template<class T> class MyClass
##else
##ifndef COPYABLE
class MyClass : private boost::noncopyable
##else
class MyClass
##endif
##endif
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
##ifndef HEADER_ONLY
  MyClass();
##else
  MyClass()
  {
  }
##endif

##ifdef COPYABLE
  /**
   * Copy Constructor
   */
  MyClass(const MyClass& from);

##endif
  /**
   * Destructor
   */
##ifndef HEADER_ONLY
  virtual ~MyClass();
##else
  virtual ~MyClass()
  {
  }
##endif

##ifdef INIT_MEMBER
  /**
   * Initialization.
   */
  void init();

##endif
##ifdef COPYABLE
  //
  // OPERATORS
  //

public:

  /**
   * Assignment operator.
   * @param from [in] The value to assign to this object.
   * @return A reference to this object.
   */
##ifndef HEADER_ONLY
  MyClass& operator=(const MyClass& from);
##else
  MyClass& operator=(const MyClass& from)
  {
    if (this != &from)
    {
      // Assign data members
    }
    return *this;
  }
##endif

  /**
   * Comparison operator.
   * @param from [in] The value to compare this class with.
   * @return true if classes are equal, false otherwise.
   */
##ifndef HEADER_ONLY
  bool operator==(const MyClass& from) const;
##else
  bool operator==(const MyClass& from) const
  {
    // Compare members
    PvUtil::exitError("Comparison not implemented.");
    return false;
  }
##endif

  /**
   * Difference operator. This is the negation of the comparison.
   */
##ifndef HEADER_ONLY
  bool operator!=(const MyClass& from) const;
##else
  bool operator!=(const MyClass& from) const
  {
    return !(*this == from);
  }
##endif
##endif

  //
  // OPERATIONS
  //

public:

##ifdef EXAMPLES
  /**
   * An example member function.
   * This is an example of all the possible type of comments that can be
   * included in a member function. It is supposed to fill the string with
   * the given character and length
   * @pre (length >= 0) && (character != '\0')
   * @param output [out] This is an output parameter, it will be changed inside the function.
   * @param length [in] The desired length of the string.
   * @param character [in] Character to repeat.
   * @return true on success, false if there was a problem.
   * @remark The to buffer must be long enough to hold 
   * the entire from buffer.
   * @par Example:
   * @code 
   *   MyClass c;
   *   std::string s;
   *   c.repeatChar(s,10,'-');
   * @endcode
   * @see otherFunction()
   */
  bool repeatChar(std::string& output, const int length, const char character);
##endif

  //
  // ACCESS
  //

public:

##ifdef EXAMPLES
  /**
   * Return member1
   */
  int getMember1() const { return mMember1; }

  /**
   * Return member1
   * Set the member1. It is not efficient to use a reference for integers, 
   * this is just to illustrate.
   */
  void setMember1(const int val) { mMember1 = val; }
##endif

  //
  // INQUIRY
  //

public:

##ifdef INIT_MEMBER
  /**
   * Ask if this object has been initialized (the init() function has been called).
   */
  bool isInitialized() const { return mIsInitialized; }

##endif

  //
  // ATTRIBUTES
  //

protected:

##ifdef INIT_MEMBER
  /**
   * Tells if this object has been initialized.
   */
  bool mIsInitialized;

##endif
##ifdef EXAMPLES
  /**
   * Member description.
   */
  int mMember1;

  /**
   * Member description.
   */
  int mMember2;

##endif
};

##ifdef TEMPLATE
/// Smart pointer to MyClass
typedef boost::shared_ptr<MyClass> MyClassPtr;
##else
##ifdef HEADER_ONLY
/// Smart pointer to MyClass
typedef boost::shared_ptr<MyClass> MyClassPtr;
##endif
##endif

##ifdef VISION_NAMESPACE
}; // namespace vision

##endif
}; // namespace ait

##endif

/*
  TO ADD THIS NEW MODULE TO THE VISION FRAMEWORK YOU HAVE TO REGISTER BEFORE INITIALIZING
  VISION BY CALLING:

  1.- Add this line in the beginning of your main file

#include "MyModuleMod.hpp"

  2.- Add the following in the beginning of the main function.

  AIT_REGISTER_VISION_MODULE(vision::MyModuleMod);

  3.- DELETE THIS COMMENT.
*/

#pragma once

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

#include <String.hpp>
#include <ait/Image.hpp>

// LOCAL INCLUDES
//

##ifdef ASYNC_MOD
#include "AsyncMod.hpp"
##else
#include "RealTimeMod.hpp"
##endif
##ifdef HAS_MESSAGE
#include "MyModuleMsg.hpp"
##endif

// FORWARD REFERENCES
//

#include "MyModuleMod_fwd.hpp"

namespace ait 
{

namespace vision
{


/**
 * This vision module is not yet documented.
 * @par Responsibilities:
 * - Write the responsibilities here.
 */
##ifdef ASYNC_MOD
class MyModuleMod : public AsyncMod
##else
class MyModuleMod : public RealTimeMod
##endif
{
public:

  //
  // LIFETIME
  //

  /**
   * Constructor. The constructor creates a quick instance. Expensive operations
   * are executed in the init() function.
   */
  MyModuleMod();

  /**
   * Destructor
   */
  virtual ~MyModuleMod();

  /**
   * Provide dependency information for the ExecutionGraph.
   * @see VisionMod::initExecutionGraph for more info.
   */
  virtual void initExecutionGraph(std::list<VisionModPtr>& srcModules);

  /**
   * Initialization function.
   */
  virtual void init();

  /**
   * Finalization function
   */
  virtual void finish();

  //
  // OPERATIONS
  //

public:

  /**
   * Process this module.
   */
  virtual void process();

  /**
   * Visualize this module.
   */
  virtual void visualize(Image32& img);

##ifdef HAS_MESSAGE
  /**
   * Process the incoming vision messages.
   */
  virtual void processMessages();

##endif
protected:

##ifdef HAS_MESSAGE
  /**
   * Process incoming message.
   */
  virtual void processMsg(MyModuleMsg* pMsg);

##endif
  //
  // ACCESS
  //

public:

  /**
   * Get the static name of this module. This name is used to name the type of this module.
   * The constructor sets the member variable mName to this value.
   * @remark This static function is only implemented for concrete classes.
   */
  static const char *getModuleStaticName() { return "MyModuleMod"; }

  //
  // INQUIRY
  //

public:

  //
  // ATTRIBUTES
  //

protected:

};


}; // namespace vision

}; // namespace ait




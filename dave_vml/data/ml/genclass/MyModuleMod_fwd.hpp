
#pragma once

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

##ifdef HAS_MESSAGE
class MyModuleMsg;
##endif
class MyModuleMod;
typedef boost::shared_ptr<MyModuleMod> MyModuleModPtr;

}; // namespace vision

}; // namespace ait



##ifndef TEMPLATE
##ifndef HEADER_ONLY

#pragma once

#include <boost/shared_ptr.hpp>

namespace ait
{

##ifdef VISION_NAMESPACE
namespace vision
{

##endif
class MyClass;
typedef boost::shared_ptr<MyClass> MyClassPtr;

##ifdef VISION_NAMESPACE
}; // namespace vision

##endif
}; // namespace ait

##endif
##endif

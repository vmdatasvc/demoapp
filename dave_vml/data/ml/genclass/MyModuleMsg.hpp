##ifdef HAS_MESSAGE

#pragma once

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>

// AIT INCLUDES
//

#include <Object.hpp>
#include <String.hpp>

// LOCAL INCLUDES
//

#include "VisionMsg.hpp"
##ifdef HAS_PERSON
#include "Person.hpp"
##endif

// FORWARD REFERENCES
//

namespace ait 
{

namespace vision
{


/**
 * MyModuleMod vision messages.
 * @par Responsibilities:
 * - Type of information to be carried in these messages.
 */
class MyModuleMsg : public VisionMsg
{
public:

  //
  // LIFETIME
  //

  /**
   * Constructor.
   */
  MyModuleMsg(VisualSensorId vid = VisualSensorId(), double timeStamp = 0) 
    : VisionMsg(vid,timeStamp)
  {
  }

  /**
   * Destructor
   */
  virtual ~MyModuleMsg()
  {
  }

  //
  // OPERATIONS
  //

public:

  //
  // ACCESS
  //

public:

##ifdef HAS_PERSON
  /**
   * Get the reference to the source person.
   */
  PersonPtr getSourcePerson()
  {
    return mpSrcPerson;
  }

  /**
   * Set the source person reference.
   */
  void setSourcePerson(PersonPtr& pSource)
  {
    mpSrcPerson = pSource;
  }

##endif
  //
  // INQUIRY
  //

public:

  //
  // ATTRIBUTES
  //

public:

  enum
  {
    /// This is an example flag. It must start in 0x10 (the other bits are reserved for VisionMsg).
    EXAMPLE             = 0x00000010,
  };

protected:

##ifdef HAS_PERSON
  /**
   * Optional pointer to the source person.
   */
  PersonPtr mpSrcPerson;

##endif
};

/// Smart pointer to MyModuleMsg
typedef boost::shared_ptr<MyModuleMsg> MyModuleMsgPtr;

}; // namespace vision

}; // namespace ait

##endif // HAS_MESSAGE
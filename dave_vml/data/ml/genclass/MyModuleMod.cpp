
#include "MyModuleMod.hpp"

namespace ait 
{

namespace vision
{

MyModuleMod::MyModuleMod()
##ifdef ASYNC_MOD
: AsyncMod(getModuleStaticName())
##else
: RealTimeMod(getModuleStaticName())
##endif
{
}

MyModuleMod::~MyModuleMod()
{
}

void 
MyModuleMod::initExecutionGraph(std::list<VisionModPtr>& srcModules)
{
##ifdef ASYNC_MOD
  AsyncMod::initExecutionGraph(srcModules);
##else
  RealTimeMod::initExecutionGraph(srcModules);
##endif
  
  // Add any source module to the given list here.
  // VisionModPtr pMod;  
  // getModule("SampleMod",pMod); 
  // srcModules.push_back(pMod);
}

void 
MyModuleMod::init()
{
##ifdef ASYNC_MOD
  AsyncMod::init();
##else
  RealTimeMod::init();
##endif

  // Initialize this module.
}

void
MyModuleMod::finish()
{
##ifdef ASYNC_MOD
  AsyncMod::finish();
##else
  RealTimeMod::finish();
##endif
}

//
// OPERATIONS
//

void 
MyModuleMod::process()
{
##ifndef ASYNC_MOD
  Image32& frame = getCurrentFrame();
##endif

}

##ifdef HAS_MESSAGE
void
MyModuleMod::processMessages()
{
  VisionMsgPtr pMsg;

  while (mpInputMsgQueue->get(pMsg))
  {
    MyModuleMsg* pMsg0 = dynamic_cast<MyModuleMsg*>(pMsg.get());
    if (pMsg0 != NULL)
    {
      processMsg(pMsg0);
      continue;
    }
    PvUtil::exitError("Message type not supported by this module [%s]",getFullName().c_str());
  }
}

void 
MyModuleMod::processMsg(MyModuleMsg *pMsg)
{
}

##endif
void 
MyModuleMod::visualize(Image32& img)
{
}

//
// ACCESS
//

//
// INQUIRY
//

}; // namespace vision

}; // namespace ait


#!/bin/bash
# Author: Paul J. Shin

# -- Need to build dlib if we just get vml source code from the repository
#cd cpp/external/dlib-18.17/dlib
cd cpp/external/dlib-19.1/dlib
mkdir build; cd build
cmake ..
make -j $(nproc)
cd ../../../../../

# -- Need to build figtree if we just get vml source code from the repository
cd cpp/external/figtree-0.9.3/
make -j $(nproc) FIGTREE_LIB_TYPE=static
cd ../../../

# -- Build MySQL Client Library
cd ~/Repository/vml/cpp/external/mysql-connector-cpp/trunk
cmake . -DBOOST_ROOT=/usr/local/lib -DCMAKE_ENABLE_C++11=ON -DMYSQLCLIENT_STATIC_LINKING=1
make -j
sudo make install

# -- Run cmake and make and copy the run-time data to the build folder
rm -rf build
mkdir build
cd build
cmake -G "Eclipse CDT4 - Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -D_ECLIPSE_VERSION=4.5 -DCMAKE_ECLIPSE_MAKE_ARGUMENTS=-j$(nproc) ../cpp 
make -j $(nproc) vmlcore
make -j $(nproc) vmlutility
make -j $(nproc) vmlvision
make -j $(nproc) vmliot
make vmlml
make DMG_Recognizer
make OmniSensr_Vision_Tracker
make -j $(nproc)


cp -rf ../cpp/ThingsToBeDeployed/certs bin/
cp -rf ../cpp/ThingsToBeDeployed/config bin/
cd ..

# -- Copy binaries and files to /home/omniadmin/omnisensr-apps/
cp -rf build/bin/* /home/omniadmin/omnisensr-apps/.
cmake -G "Eclipse CDT4 - Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -D_ECLIPSE_VERSION=4.5 -DCMAKE_ECLIPSE_MAKE_ARGUMENTS=-j$(nproc) ../cppcmake -G "Eclipse CDT4 - Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -D_ECLIPSE_VERSION=4.5 -DCMAKE_ECLIPSE_MAKE_ARGUMENTS=-j$(nproc) ../cpp cmake -G "Eclipse CDT4 - Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -D_ECLIPSE_VERSION=4.5 -DCMAKE_ECLIPSE_MAKE_ARGUMENTS=-j$(nproc) ../cpp cmake -G "Eclipse CDT4 - Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -D_ECLIPSE_VERSION=4.5 -DCMAKE_ECLIPSE_MAKE_ARGUMENTS=-j$(nproc) ../cpp cmake -G "Eclipse CDT4 - Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -D_ECLIPSE_VERSION=4.5 -DCMAKE_ECLIPSE_MAKE_ARGUMENTS=-j$(nproc) ../cpp cmake -G "Eclipse CDT4 - Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -D_ECLIPSE_VERSION=4.5 -DCMAKE_ECLIPSE_MAKE_ARGUMENTS=-j$(nproc) ../cpp 
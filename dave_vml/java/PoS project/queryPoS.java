/* Usage: This class is to establish connection and query PoS data
 * Authors: Created by Anup, Updated by Yongjing
 * Update: Nov 17, 2015 More usage instruction and querying range of parameters
 * Update: Nov 13, 2015 Add one function to verify the input and show hints if the input parameters are invalid
 * Update: Nov 12, 2015 Anup's code is wrong... I corrected and now we can query by minute
 * Update: Nov 11, 2015 replace hard-coded parameters, parameterForQueryPoS.java is essential
 */

import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.regex.Pattern;
import java.io.File;
import java.io.FileWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class queryPoS {
	
	public static void main(String[] args) throws SQLException, ParseException {

		// To manually check the range of each parameter, you can follow the follow steps
		// 1st step, query the range of stores in dataset
	   //storeSelectionRange();
	
	   // 2nd step, select one store and query the range of dates of that one
	   //dateSelectionRange(parameterForQueryPoS.storeID);
	
	   // 3rd step, select one store and one day, query the range of time on that day
	   //timeSelectionRange(parameterForQueryPoS.storeID,parameterForQueryPoS.startDate);
	
	   // Final step, specify 5 arguments which are store id, start date, end date, start time and end time
	   /*
	   String storeID = parameterForQueryPoS.storeID;
	   String startDate = parameterForQueryPoS.startDate;
	   String endDate = parameterForQueryPoS.endDate;
	   String startTime = parameterForQueryPoS.startTime;
	   String endTime = parameterForQueryPoS.endTime;
	   exportData(storeID,startDate,startTime,endDate,endTime);
	   */
	}
	
	public static boolean inputVerify(String[] args) throws ParseException{
		if (args.length <= 4) return false;
		String startDateStr = args[1];
		String endDateStr = args[3];
		String startTimeStr = args[2];
		String endTimeStr = args[4];
		DateFormat df = new SimpleDateFormat("yyyy-mm-dd");
		Date startDate = df.parse(startDateStr);
		Date endDate = df.parse(endDateStr);
		DateFormat tdf = new SimpleDateFormat("hh:mm:ss");
		Date startTime = tdf.parse(startTimeStr);
		Date endTime = tdf.parse(endTimeStr);
		// The valid input should be like this: "6097" "2015-06-05" "09:59:11" "2015-06-05" "15:02:31" "c:\\new1.txt" "c:\\new2.txt"
		if (args[0]==null || args[1]==null || args[2]==null || args[3]==null || args[4]==null ||args[5]==null ||args[6]==null){
			return false;
		}else if(args[1].length() != 10 || args[3].length() != 10 || args[2].length() != 8 || args[4].length() != 8){
			return false;
		}else if(startDate.compareTo(endDate) > 0){
			System.out.println("\n************* Error *************");
			System.out.println("*Start_Date comes after End_Date*");
			System.out.println("*********************************");
			return false;
		}else if(startDate.compareTo(endDate) == 0 && startTime.compareTo(endTime) > 0){
			System.out.println("\n************* Error *************");
			System.out.println("*Start_Time comes after End_Time*");
			System.out.println("*********************************");
			return false;
		}else{
			return true;
		}
	}
	
	/*
	public static void showHints(){
		System.out.println("======================= Helper =======================");
		System.out.println("|  Please use the following command to fetch the data:");
		System.out.print("|  $ java -cp .;mysql-connector-java-5.0.8-bin.jar demoForQueryPoS ");
		System.out.println("\"STORE_ID\" "+"\"START_DATE\" "+"\"START_TIME\" "+"\"END_DATE\" "+"\"END_TIME\" "+"\"lookupTable_path\" "+"\"selectedData_path\"");
		System.out.println("|  e.g.:");
		System.out.println("|  $ java -cp .;mysql-connector-java-5.0.8-bin.jar demoForQueryPoS"+" \"6097\" "+"\"2015-06-05\" "+"\"09:59:11\" "+"\"2015-06-05\" "+"\"15:02:31\" "+"\"c:\\new1.txt\" "+"\"c:\\new2.txt\"");
		System.out.println("======================================================");
	}
	*/
	
	public static void timeSelectionRange(String storeID, String startDate) throws SQLException{
		Connection conn = null;
		Statement stmt = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(parameterForQueryPoS.PoSserverLoc, parameterForQueryPoS.PoSusername, parameterForQueryPoS.PoSpassword);
			stmt = conn.createStatement();
			String sql;
			//----------MIN------------------------------------
			sql = 	"SELECT MIN(txn_tm) "
					+ "FROM " +parameterForQueryPoS.PoStable1+ " "
					+ "WHERE store_id = "+storeID+" "
					+ "AND txn_dt = '" +startDate+"' "
					+";";
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()){
			   		String txn_tm = rs.getString("MIN(txn_tm)");
			   		System.out.println(txn_tm);
			}
			
			//------------MAX-----------------------------------
			sql = 	"SELECT MAX(txn_tm) "
					+ "FROM " +parameterForQueryPoS.PoStable1+ " "
					+ "WHERE store_id = "+storeID+" "
					+ "AND txn_dt = '" +startDate+"' "
					+";";
			rs = stmt.executeQuery(sql);
			while(rs.next()){
			   		String txn_tm = rs.getString("MAX(txn_tm)");
			   		System.out.println(txn_tm);
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	
	//----------------------------------------------------------------------------------
	public static void dateSelectionRange(String storeID) throws SQLException{
		Connection conn = null;
		Statement stmt = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(parameterForQueryPoS.PoSserverLoc, parameterForQueryPoS.PoSusername, parameterForQueryPoS.PoSpassword);
			stmt = conn.createStatement();
			String sql;
			/*
			 * "SELECT DISTINCT txn_dt "
					+ "FROM pos_ahold.3_tran_master_tyson_2015 "
					+ "WHERE store_id = "+storeID+" "
					+ "UNION "
					+ "SELECT DISTINCT txn_dt "
					+ "FROM pos_ahold.3_tran_master "
					+ "WHERE store_id = "+storeID+" "
					+ "UNION "
					+ + "UNION "
					+ "SELECT DISTINCT txn_dt "
					+ "FROM pos_ahold.3_tran_master_new "
					+ "WHERE store_id = "+storeID+" "
			 */
			
			sql = 	"SELECT DISTINCT txn_dt "
					+ "FROM " +parameterForQueryPoS.PoStable1+ " "
					+ "WHERE store_id = "+storeID+" "
					+ "ORDER BY txn_dt;";
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()){
			   		String txn_dt = rs.getString("txn_dt");
			   		System.out.println(txn_dt);
			   }
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	
	
	//----------------------------------------------------------------------------------------------------------------
	public static void exportData(String storeID, String startDate, String startTime, String endDate, String endTime,String lookupTable, String selectedDataTable){
		parameterForQueryPoS.lookupTableFile = lookupTable;
		parameterForQueryPoS.selectedDataTableFile = selectedDataTable;
		exportData(storeID, startDate, startTime, endDate, endTime);
	}
	
	
	//----------------------------------------------------------------------------------------------------------------
	public static void exportData(String storeID, String startDate, String startTime, String endDate, String endTime){
		Connection conn = null;
		Statement stmt = null;
		HashMap<String, String> map = new HashMap<String, String>();   
	   try{
		   //-------------------------------------------------------------------------------------------------
		   // Parse XML format and set up the first table
		   Path forMap = Paths.get(parameterForQueryPoS.RegionIDserverLoc+parameterForQueryPoS.storeVendor+"-"+storeID+".vmproj");
		   File inputFile = new File(forMap.toString());
		   DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		   DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		   Document doc = dBuilder.parse(inputFile);
		   doc.getDocumentElement().normalize();
		   
		   //System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
		   NodeList nList = doc.getElementsByTagName("camera_view");
		   for (int temp = 0; temp < nList.getLength(); temp++){
				Node nNode = nList.item(temp);
	            //System.out.println("\nCurrent Element :"  + nNode.getNodeName());
	            if (nNode.getNodeType() == Node.ELEMENT_NODE){
	            	Element eElement = (Element) nNode;
	                //System.out.println("\ncamera_view id: " + eElement.getAttribute("id"));
	                //System.out.println("points: "  + eElement.getElementsByTagName("points").item(0).getTextContent());
	                String pointsString = eElement.getElementsByTagName("points").item(0).getTextContent();
	                //System.out.println("label: "  + eElement.getElementsByTagName("label").item(0).getTextContent());
	                //System.out.println(eElement.getElementsByTagName("label").getLength());
	                for (int j=0; j<eElement.getElementsByTagName("label").getLength();j++){
	                	String label = eElement.getElementsByTagName("label").item(j).getTextContent();
	                	//System.out.print(label+"|");
	                	if ( label.indexOf('#') == 4 ){
		                	map.put(label.split("#")[0], pointsString);
		                }
	                }
	            }
		   }
		   System.out.println("***First output file completed***");
		   // HashMap is set up now
		   
		   String pxperft = parameterForQueryPoS.getPXPERFT(parameterForQueryPoS.storeID);
		   
		   // Set path and name of the first table
		   FileWriter writerFirst = new FileWriter(parameterForQueryPoS.lookupTableFile);
		   // Write the names of attributes in the first line
		   writerFirst.write("store_id: "+parameterForQueryPoS.storeVendor+storeID+"\n");
		   writerFirst.write("pxperft: "+pxperft+"\n");
		   writerFirst.write("region_id|pixel_unit"+"\n");
		   for (String each : map.keySet()){
				writerFirst.write(each+"|");
				writerFirst.write(map.get(each)+"\n");
		   }
		   writerFirst.flush();
		   writerFirst.close();
		   // First table is set up
		   
		   
		   //-----------------------------Second Table--------------------------------------------------------
		   Class.forName("com.mysql.jdbc.Driver");
		   //System.out.println("Connecting to database");
		   conn = DriverManager.getConnection(parameterForQueryPoS.PoSserverLoc, parameterForQueryPoS.PoSusername, parameterForQueryPoS.PoSpassword);
		   //System.out.println("Creating statement");
		   stmt = conn.createStatement();
		   String sql;

		   sql = "SELECT store_id,txn_Id,reg_id,txn_dt,txn_tm,"
		   		+ "GROUP_CONCAT(DISTINCT b.node_name ORDER BY b.node_name) AS categories,"
		   		+ "GROUP_CONCAT( DISTINCT c.region_Id ORDER BY c.region_Id) AS regions "
		   		+ "FROM pos_ahold.`3_txn` a "
		   		+ "JOIN pos_ahold.`2_node_upc_mapping` b "
		   		+ "ON(a.`prod_id` = b.`upc_id`) "
		   		+ "JOIN region_info_fed.`map_names` c "
		   		+ "ON(b.`node_name` = c.`name`)"
		   		+ "WHERE store_id = '"+storeID+"' "
		   		+ "AND txn_dt IN ('"+startDate+"','"+endDate +"')"+" ";
		   if (!startDate.equals(endDate)){
		   		sql += "AND IF (txn_dt = '"+startDate+"', txn_tm >= '"+ startTime+"', txn_tm <='"+endTime+"')"+" "+ "GROUP BY txn_id;";
		   }else{
			   sql += "AND txn_tm BETWEEN '"+startTime+"' AND '"+endTime+"' "+ "GROUP BY txn_id;";
		   }
		   ResultSet rs = stmt.executeQuery(sql);
		   
		   // Set path and name of the output file
		   //FileWriter writer = new FileWriter("c:\\Users\\yshi\\workspace\\PoSDB\\outputResult.txt");
		   FileWriter writer = new FileWriter(parameterForQueryPoS.selectedDataTableFile);
		   	// Write the names of attributes in the first line
		   writer.write("|tran_date|tran_time|register_id|region_id|\n");
		   while(rs.next()){
		   		String reg_id = rs.getString("reg_id");
		   		String txn_dt = rs.getString("txn_dt");
		   		String txn_tm = rs.getString("txn_tm");
		   		String regions = rs.getString("regions");
	   			//System.out.println("\n"+"original 4-digit region id: "+regions);
	   			String[] regionsArray = regions.split(Pattern.quote(","));
	   			//System.out.println(regionsArray.length+" pixel units:");
	   			//System.out.print("{");
	   			//pixelUnit += "{";
	   			for(int i=0; i<regionsArray.length; i++){
	   				if(map.containsKey(regionsArray[i])){
	   				}else{
	   					//System.out.print("{N/A}");
	   				}
	   			}
	   			//pixelUnit += "}";
	   			writer.append("|");
	   			writer.append(txn_dt);
		   		writer.append("|");
		   		writer.append(txn_tm);
		   		writer.append("|");
		   		writer.append(reg_id);
		   		writer.append("|");
		   		//writer.append(pixelUnit);
		   		writer.append(regions);
		   		writer.append("|");
		   		writer.append("\n");
		   	}
		   	writer.flush();
		    writer.close();
	      rs.close();
	      stmt.close();
	      conn.close();
	   }catch(SQLException se){
	      se.printStackTrace();
	   }catch(Exception e){
	      e.printStackTrace();
	   }
	   finally{
	      try{
	         if(stmt!=null)
	            stmt.close();
	      } catch (SQLException se2){
	      }
	      try{
	         if(conn!=null)
	            conn.close();
	      }catch(SQLException se){
	         se.printStackTrace();
	      }
	   }
	   System.out.println("***Second output file completed**");
	}
	
	
	//----------------------------------------------------------------------------------------
	public static void storeSelectionRange() throws SQLException{
		Connection conn = null;
		Statement stmt = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(parameterForQueryPoS.PoSserverLoc, parameterForQueryPoS.PoSusername, parameterForQueryPoS.PoSpassword);
			stmt = conn.createStatement();
			String sql;
			sql = "SELECT DISTINCT store_id "
					+ "FROM pos_ahold.3_tran_master_tyson_2015 "
					+ "UNION "
					+ "SELECT DISTINCT store_id "
					+ "FROM pos_ahold.3_tran_master "
					+ "UNION "
					+ "SELECT DISTINCT store_id "
					+ "FROM pos_ahold.3_txn "
					+ "UNION "
					+ "SELECT DISTINCT store_id "
					+ "FROM pos_ahold.3_tran_master_new;";
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()){
			   		String store_id = rs.getString("store_id");
			   		System.out.println(store_id);
			   }
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
	}
   
   
}
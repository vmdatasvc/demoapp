/*
 * Usage: This class will set parameters for queryPoS.java
 * Update: Nov 11, 2015 create this class
 */
public class parameterForQueryPoS {
	
	// Querying parameters
	static String storeVendor = "Ahold";			// The vendor of store to be selected
	static String storeID = "6097";					// The number of store to be selected
	static String startDate = "2015-06-05";			// Starting Date of the above store
	static String endDate = "2015-06-05";			// Ending Date of the above store
	static String startTime = "1";					// Starting Time of the above store, now the accuracy is hour
	static String endTime = "23";					// Ending Time of the above store, now the accuracy is hour
	
	// MySQL server locations
	static String PoSserverLoc = "jdbc:mysql://vmdbpos001/";
	static String PoSusername = "pshin";
	static String PoSpassword = "pshin";
	static String RegionIDserverLoc = "//VMVASE/Data/Store Regions/GSI/baseVmproj1/";
	
	// PoS table names
	static String PoStable1 = "pos_ahold.3_txn";
	static String PoStable2 = "pos_ahold.3_tran_master_tyson_2015";
	static String PoStable3 = "pos_ahold.3_tran_master";
	static String PoStable4 = "pos_ahold.3_tran_master_new";
	
	// Output table path and name
	static String lookupTableFile = "c:\\Users\\yshi\\workspace\\PoSDB\\lookupTable.txt";
	static String selectedDataTableFile = "c:\\Users\\yshi\\workspace\\PoSDB\\outputData.txt";
	
	// Other parameter
	static String pxperft = "";
	
	public static String getPXPERFT(String storeID){
		switch (storeID){
		   case "316":
				System.out.println("Ahold"+storeID);
				return "22.6896551724138";
		   case "6097":
				System.out.println("Ahold"+storeID);
				return "10.625";
		   case "606":
				System.out.println("Ahold"+storeID);
				return "6.16049382716049";
		   default:
				System.out.println("Unknown");
				return null;
		   }
	}
	
}

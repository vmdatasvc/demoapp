import java.sql.SQLException;
import java.text.ParseException;
/*
 * Usage: Just to test the hard-coded parameters in queryPoS.java
 * Update: Nov 17, 2015 Add flag to query the range of parameters
 * Update: Nov 16, 2015 Update usage instruction
 * Update: Nov 13, 2015 Input verify and show hints
 * Update: Nov 11, 2015 Created this class
 */

public class demoForQueryPoS {
	public static void main(String[] args) throws ParseException, SQLException{
		System.out.println("=================================== Helper ===================================");
		System.out.println("|-----Check range of \"STORE_ID:\"");
		System.out.println("|  	  $ java -cp .;mysql-connector-java-5.0.8-bin.jar demoForQueryPoS"+" \"-s\"");
		System.out.println("|-----Check range of \"Date:\"");
		System.out.println("|  	  $ java -cp .;mysql-connector-java-5.0.8-bin.jar demoForQueryPoS"+" \"-d\""+" \"STORE_ID\" ");
		System.out.println("|-----Check range of \"Time:\"");
		System.out.println("|  	  $ java -cp .;mysql-connector-java-5.0.8-bin.jar demoForQueryPoS"+" \"-t\""+" \"STORE_ID\" "+" \"START_DATE\" ");
		System.out.println("|-----FETCH THE DATA:");
		System.out.print("|  	  $ java -cp .;mysql-connector-java-5.0.8-bin.jar demoForQueryPoS ");
		System.out.println("\"STORE_ID\" "+"\"START_DATE\" "+"\"START_TIME\" "+"\"END_DATE\" "+"\"END_TIME\" "+"\"lookupTable_path\" "+"\"selectedData_path\"");
		System.out.println("|  	  e.g.:");
		System.out.println("|  	  $ java -cp .;mysql-connector-java-5.0.8-bin.jar demoForQueryPoS"+" \"6097\" "+"\"2015-06-05\" "+"\"09:59:11\" "+"\"2015-06-05\" "+"\"15:02:31\" "+"\"c:\\new1.txt\" "+"\"c:\\new2.txt\"");
		System.out.println("==============================================================================");
		boolean inputvalid = queryPoS.inputVerify(args);
		if (inputvalid){
			System.out.println("\n*********************************");
			System.out.println("      Plese wait for results     ");
			queryPoS.exportData(args[0],args[1],args[2],args[3],args[4],args[5],args[6]);
			System.out.println("*********************************");
		}else{
			switch (args[0]){
			case "-s":	queryPoS.storeSelectionRange();
						break;
			case "-d":	queryPoS.dateSelectionRange(args[1]);
						break;
			case "-t": queryPoS.timeSelectionRange(args[1],args[2]);
						break;
			default:	System.out.println("\nInput parameters are invalid, please input again");
						//queryPoS.showHints();
						break;		
			}
		}
	}
}

# PoS Data Association
A description of PoS could be found from [How to tag location to PoS Data](http://wiki.videomining.com/index.php?title=Wi-Fi_based_Shopper_Tracking).
# How to run the command
##(1)Put the .jar in the same folder with the .java files.
##(2)Compile the .java files
$ javac queryPoS.java parameterForQueryPoS.java demoForQueryPoS.java
##(3)Check range of STORE_ID
$ java -cp .;mysql-connector-java-5.0.8-bin.jar demoForQueryPoS "-s"
##(4)Check range of DATE for the above STORE_ID
$ java -cp .;mysql-connector-java-5.0.8-bin.jar demoForQueryPoS "-d" "STORE_ID"
##(5)Check range of TIME for the above STORE_ID on the DATE
$ java -cp .;mysql-connector-java-5.0.8-bin.jar demoForQueryPoS "-t" "STORE_ID" "START_DATE"
##(6)Change the parameters and run the following command
$  java -cp .;mysql-connector-java-5.0.8-bin.jar demoForQueryPoS "store_id" "start_date" "start_hour" "end_date" "end_hour" "lookupTable_path_name" "selectedData_path_name"

e.g.

$  java -cp .;mysql-connector-java-5.0.8-bin.jar demoForQueryPoS "6097" "2015-06-05" "10:02:00" "2015-06-06" "23:50:00" "c:\\new1.txt" "c:\\new2.txt"

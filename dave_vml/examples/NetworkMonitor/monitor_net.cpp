#include <modules/iot/NetworkMonitor.hpp>

#include <string>
#include <boost/bind.hpp>
#include <iostream>

using std::string;
using std::cout;
using std::endl;

void run_command(const string& cmd){
	system(cmd.c_str());
}

int main (int argc, char** argv)
{
	unsigned int PANIC_SEC=30;
	string panic_action = "echo 'PANIC: Wired network is down'";
	string change_action = "echo 'Wired network changed state'";
	
	if (argc >= 2){
		change_action = argv[1];
	}
	
	if (argc >= 3){
		PANIC_SEC = std::max(1, atoi(argv[2]));
	}
	
	if (argc >= 4){
		panic_action = argv[3];
	}
	
	// Note the use of boost::bind which transforms a (void (*)(const string&)) to the needed (void (*)(void)) type
	NetworkMonitor::callback_t panic_fn = boost::bind(run_command, panic_action);
	NetworkMonitor::callback_t change_fn = boost::bind(run_command, change_action);
	NetworkMonitor nm(change_fn, PANIC_SEC, panic_fn);
	
	cout << "My IP address is: "  << nm.get_ipaddr_str() << endl;
	cout << "My MAC address is: " << nm.get_macaddr_str() << endl;
	
	nm.run();
	  
    return 0;

}


if (UNIX AND NOT APPLE)
	get_filename_component(INTEL_ROOT "${TVS_3RDPARY_ROOT}/intel/linux" ABSOLUTE)
endif()

if (APPLE)
	get_filename_component(INTEL_ROOT "${TVS_3RDPARY_ROOT}/intel/osx" ABSOLUTE)
endif ()

if (WIN32)
	message ("FATAL ERROR: ")
	get_filename_component (INTEL_ROOT "${TVS_3RDPARY_ROOT}/intel/windows" ABSOLUTE)
endif ()

set (TBB_INCLUDE_DIRS "${INTEL_ROOT}/tbb/include/")
find_library (LIB_TBB tbb PATHS ${INTEL_ROOT}/tbb/lib)
set (TBB_LINKER_LIBS ${LIB_TBB})

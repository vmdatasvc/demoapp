# - Find FFTW library
# Find the native FFTW includes and library
# This module defines
#  FFTW_INCLUDE_DIR, where to find tiff.h, etc.
#  FFTW_LIBRARIES, libraries to link against to use FFTW.
#  FFTW_FOUND, If false, do not try to use FFTW.
# also defined, but not for general use are
#  FFTW_LIBRARY, where to find the FFTW library.

# here is the target environment located
IF(APPLE)
get_filename_component(THIS_ROOT_CONFIG_PATH "${CMAKE_CURRENT_LIST_FILE}" PATH)
get_filename_component(THIS_ROOT_CONFIG_PATH "${THIS_ROOT_CONFIG_PATH}/../external/osx" ABSOLUTE)
SET(CMAKE_FIND_ROOT_PATH ${THIS_ROOT_CONFIG_PATH})
ELSE(APPLE)
get_filename_component(THIS_ROOT_CONFIG_PATH "${CMAKE_CURRENT_LIST_FILE}" PATH)
get_filename_component(THIS_ROOT_CONFIG_PATH "${THIS_ROOT_CONFIG_PATH}/../3rdparty/intel/linux/mkl/10.2.3.029/" ABSOLUTE)
SET(CMAKE_FIND_ROOT_PATH "${THIS_ROOT_CONFIG_PATH}")

ENDIF(APPLE)

# adjust the default behavior of the FIND_XXX() commands:
# search headers and libraries in the target environment, search 
# programs in the host environment
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

IF(APPLE)
FIND_PATH(FFTW_INCLUDE_DIR fftw3.h )
SET(FFTW_NAMES ${FFTW_NAMES} fftw3 libfftw3 libfftw3_threads)
FIND_LIBRARY(FFTW_LIBRARY NAMES ${FFTW_NAMES} )
ELSE(APPLE)
FIND_PATH(FFTW_INCLUDE_DIR fftw/fftw3.h )
SET(FFTW_INCLUDE_DIR "${FFTW_INCLUDE_DIR}/fftw")
SET(FFTW_LIBRARY "-L${THIS_ROOT_CONFIG_PATH}/lib/em64t ${THIS_ROOT_CONFIG_PATH}/lib/em64t/libmkl_solver_ilp64_sequential.a -Wl,--start-group -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -Wl,--end-group -lpthread" CACHE STRING "Intel MKL")
MESSAGE("\n######################################################################################")
MESSAGE ("Do not forget to set your LD_LIBRARY_PATH eg:\n export LD_LIBRARY_PATH=${THIS_ROOT_CONFIG_PATH}/lib/em64t:$LD_LIBRARY_PATH")
MESSAGE("######################################################################################\n")
ENDIF(APPLE)

# handle the QUIETLY and REQUIRED arguments and set FFTW_FOUND to TRUE if 
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(FFTW  DEFAULT_MSG  FFTW_LIBRARY  FFTW_INCLUDE_DIR)

IF(FFTW_FOUND)
  SET( FFTW_LIBRARIES ${FFTW_LIBRARY} )
MESSAGE("FTTW includes found at: ${FFTW_INCLUDE_DIR}")
MESSAGE("FTTW libs found at: ${FFTW_LIBRARY}")
ENDIF(FFTW_FOUND)

MARK_AS_ADVANCED(FFTW_INCLUDE_DIR FFTW_LIBRARY)

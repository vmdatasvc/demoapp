# - Try to find fuse
# Once done, this will define
#
#  Fuse_FOUND - system has Fuse
#  Fuse_INCLUDE_DIRS - the Fuse include directories
#  Fuse_LIBRARIES - link these to use Fuse

#include(LibFindMacros)

# Dependencies
find_package(fuse)

# Use pkg-config to get hints about paths
find_pkg_check_modules(Fuse_PKGCONF fuse)

# Include dir
find_path(Fuse_INCLUDE_DIR
  NAMES fuse.h
  PATHS ${Fuse_PKGCONF_INCLUDE_DIRS}
)

# Finally the library itself
find_library(Fuse_LIBRARY
  NAMES fuse
  PATHS ${Fuse_PKGCONF_LIBRARY_DIRS}
)

# Set the include dir variables and the libraries and let libfind_process do the rest.
# NOTE: Singular variables for this library, plural for libraries this this lib depends on.
#set(Fuse_PROCESS_INCLUDES Fuse_INCLUDE_DIR Fuse_INCLUDE_DIRS)
#set(Fuse_PROCESS_LIBS Fuse_LIBRARY Fuse_LIBRARIES)
#find_process(Fuse)

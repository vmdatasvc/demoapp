# - Find BLAS library
# This module finds an installed fortran library that implements the BLAS
# linear-algebra interface (see http://www.netlib.org/blas/).
# The list of libraries searched for is taken
# from the autoconf macro file, acx_blas.m4 (distributed at
# http://ac-archive.sourceforge.net/ac-archive/acx_blas.html).
#
# This module sets the following variables:
#  BLAS_FOUND - set to true if a library implementing the BLAS interface
#    is found
#  BLAS_LINKER_FLAGS - uncached list of required linker flags (excluding -l
#    and -L).
#  BLAS_LIBRARIES - uncached list of libraries (using full path name) to
#    link against to use BLAS
#  BLAS95_LIBRARIES - uncached list of libraries (using full path name)
#    to link against to use BLAS95 interface
#  BLAS95_FOUND - set to true if a library implementing the BLAS f95 interface
#    is found
#  BLA_STATIC  if set on this determines what kind of linkage we do (static)
#  BLA_VENDOR  if set checks only the specified vendor, if not set checks
#     all the possibilities
#  BLA_F95     if set on tries to find the f95 interfaces for BLAS/LAPACK
##########
### List of vendors (BLA_VENDOR) valid in this module
##  Goto,ATLAS PhiPACK,CXML,DXML,SunPerf,SCSL,SGIMATH,IBMESSL,Intel10_32 (intel mkl v10 32 bit),Intel10_64lp (intel mkl v10 64 bit,lp thread model, lp64 model),
##  Intel10_64lp_seq (intel mkl v10 64 bit,sequential code, lp64 model),
##  Intel( older versions of mkl 32 and 64 bit), ACML,ACML_MP,ACML_GPU,Apple, NAS, Generic
# C/CXX should be enabled to use Intel mkl

#=============================================================================
# Copyright 2007-2009 Kitware, Inc.
#
# Distributed under the OSI-approved BSD License (the "License");
# see accompanying file Copyright.txt for details.
#
# This software is distributed WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the License for more information.
#=============================================================================
# (To distribute this file outside of CMake, substitute the full
#  License text for the above reference.)


get_filename_component(INTEL_ROOT "${TVS_EXTERNAL_ROOT}" PATH)

if(UNIX AND NOT APPLE)

	get_filename_component(INTEL_ROOT "${INTEL_ROOT}/3rdparty/intel/linux" ABSOLUTE)
	set(BLAS_LIBS ${INTEL_ROOT}/mkl/lib/em64t/)
	set(BLAS_INCLUDES ${INTEL_ROOT}/mkl/include/)

	# Temporary hack to get things working on linux
	# Just hardcode the libs in place and skip everything below
	# This will be remedied when we switch to the new build system
	find_library(LIB_MKL_INTEL_LP64 mkl_intel_lp64
		PATHS ${BLAS_LIBS}
		)
	find_library(LIB_MKL_INTEL_THREAD mkl_intel_thread
		PATHS ${BLAS_LIBS}
		)

	find_library(LIB_MKL_CORE mkl_core
		PATHS ${BLAS_LIBS}
		)

	find_library(LIB_IOMP5 iomp5
		PATHS ${BLAS_LIBS}
		)

	find_library(LIB_PTHREAD pthread)

	set(BLAS_LIBRARIES -Wl,--start-group 
		${LIB_MKL_INTEL_LP64} 
		${LIB_MKL_INTEL_THREAD} 
		${LIB_MKL_CORE} 
		${LIB_IOMP5} 
		${LIB_PTHREAD} -Wl,--end-group)

else ()
	if(APPLE)
		get_filename_component(INTEL_ROOT "${INTEL_ROOT}/3rdparty/intel/osx" ABSOLUTE)

	elseif(WIN32)
		get_filename_component(INTEL_ROOT "${INTEL_ROOT}/3rdparty/intel/windows" ABSOLUTE)
	endif()
	set(BLAS_INCLUDES "${INTEL_ROOT}/mkl/include")
	set(BLAS_LIBS "${INTEL_ROOT}/mkl/lib")

	include(CheckFunctionExists)
	include(CheckFortranFunctionExists)

	set(_blas_ORIG_CMAKE_FIND_LIBRARY_SUFFIXES ${CMAKE_FIND_LIBRARY_SUFFIXES})

	# Check the language being used
	get_property( _LANGUAGES_ GLOBAL PROPERTY ENABLED_LANGUAGES )
	if( _LANGUAGES_ MATCHES Fortran )
		set( _CHECK_FORTRAN TRUE )
	elseif( (_LANGUAGES_ MATCHES C) OR (_LANGUAGES_ MATCHES CXX) )
		set( _CHECK_FORTRAN FALSE )
	else()
		if(BLAS_FIND_REQUIRED)
			message(FATAL_ERROR "FindBLAS requires Fortran, C, or C++ to be enabled.")
		else()
			message(STATUS "Looking for BLAS... - NOT found (Unsupported languages)")
			return()
		endif()
	endif()

	macro(Check_Fortran_Libraries LIBRARIES _prefix _name _flags _list _thread)
		# This macro checks for the existence of the combination of fortran libraries
		# given by _list.  If the combination is found, this macro checks (using the
		# Check_Fortran_Function_Exists macro) whether can link against that library
		# combination using the name of a routine given by _name using the linker
		# flags given by _flags.  If the combination of libraries is found and passes
		# the link test, LIBRARIES is set to the list of complete library paths that
		# have been found.  Otherwise, LIBRARIES is set to FALSE.

		# N.B. _prefix is the prefix applied to the names of all cached variables that
		# are generated internally and marked advanced by this macro.

		#set(_libdir ${ARGN})
		set(_libdir "${INTEL_ROOT}/mkl/lib")
		set( BLA_STATIC TRUE)

		set(_libraries_work TRUE)
		set(${LIBRARIES})
		set(_combined_name)
		if (NOT _libdir)
			if (WIN32)
				set(_libdir ENV LIB)
			elseif (APPLE)
				set(_libdir ENV DYLD_LIBRARY_PATH)
			else ()
				set(_libdir ENV LD_LIBRARY_PATH)
			endif ()
		endif ()

		foreach(_library ${_list})
			set(_combined_name ${_combined_name}_${_library})

			if(_libraries_work)
				if (BLA_STATIC)
					if (WIN32)
						set(CMAKE_FIND_LIBRARY_SUFFIXES .lib ${CMAKE_FIND_LIBRARY_SUFFIXES})
					endif ()
					if (APPLE)
						set(CMAKE_FIND_LIBRARY_SUFFIXES .a ${CMAKE_FIND_LIBRARY_SUFFIXES})
					else ()
						set(CMAKE_FIND_LIBRARY_SUFFIXES .a ${CMAKE_FIND_LIBRARY_SUFFIXES})
					endif ()
				else ()
					if (CMAKE_SYSTEM_NAME STREQUAL "Linux")
						# for ubuntu's libblas3gf and liblapack3gf packages
						set(CMAKE_FIND_LIBRARY_SUFFIXES ${CMAKE_FIND_LIBRARY_SUFFIXES} .so.3gf)
					endif ()
				endif ()
				find_library(${_prefix}_${_library}_LIBRARY
					NAMES ${_library}
					PATHS ${_libdir}
					)

				mark_as_advanced(${_prefix}_${_library}_LIBRARY)
				set(${LIBRARIES} ${${LIBRARIES}} ${${_prefix}_${_library}_LIBRARY})
				set(_libraries_work ${${_prefix}_${_library}_LIBRARY})
			endif()
		endforeach()
		if(_libraries_work)
			# Test this combination of libraries.
			set(CMAKE_REQUIRED_LIBRARIES ${_flags} ${${LIBRARIES}} ${_thread})
			#  message("DEBUG: CMAKE_REQUIRED_LIBRARIES = ${CMAKE_REQUIRED_LIBRARIES}")
			if (_CHECK_FORTRAN)
				check_fortran_function_exists("${_name}" ${_prefix}${_combined_name}_WORKS)
			else()
				check_function_exists("${_name}_" ${_prefix}${_combined_name}_WORKS)
			endif()
			set(CMAKE_REQUIRED_LIBRARIES)
			mark_as_advanced(${_prefix}${_combined_name}_WORKS)
			set(_libraries_work ${${_prefix}${_combined_name}_WORKS})
		endif()
		if(NOT _libraries_work)
			set(${LIBRARIES} FALSE)
		endif()
		#message("DEBUG: ${LIBRARIES} = ${${LIBRARIES}}")
	endmacro()

	set(BLAS_LINKER_FLAGS)
	set(BLAS_LIBRARIES)
	set(BLAS95_LIBRARIES)

	if(NOT BLA_VENDOR)
		set(BLA_VENDOR "Intel10_64lp")
	endif()



	#BLAS in intel mkl 10 library? (em64t 64bit)
	if (BLA_VENDOR MATCHES "Intel*" OR BLA_VENDOR STREQUAL "All")
		if (NOT WIN32)
			set(LM "-lm")
		endif ()
		if (_LANGUAGES_ MATCHES C OR _LANGUAGES_ MATCHES CXX)
			if(BLAS_FIND_QUIETLY OR NOT BLAS_FIND_REQUIRED)
				find_package(Threads)
			else()
				find_package(Threads REQUIRED)
			endif()

			set(BLAS_SEARCH_LIBS "")

			if(BLA_F95)
				set(BLAS_mkl_SEARCH_SYMBOL SGEMM)
				set(_LIBRARIES BLAS95_LIBRARIES)
				if (WIN32)
					list(APPEND BLAS_SEARCH_LIBS
						"mkl_blas95 mkl_intel_c mkl_intel_thread mkl_core libiomp5")
				else ()
					if (BLA_VENDOR STREQUAL "Intel10_32" OR BLA_VENDOR STREQUAL "All")
						list(APPEND BLAS_SEARCH_LIBS
							"mkl_blas95 mkl_intel mkl_intel_thread mkl_core iomp5")
					endif ()
					if (BLA_VENDOR STREQUAL "Intel10_64lp" OR BLA_VENDOR STREQUAL "All")
						# old version
						list(APPEND BLAS_SEARCH_LIBS
							"mkl_blas95 mkl_intel_lp64 mkl_intel_thread mkl_core iomp5")

						# mkl >= 10.3
						if (CMAKE_C_COMPILER MATCHES ".+gcc.*")
							list(APPEND BLAS_SEARCH_LIBS
								"mkl_blas95_lp64 mkl_intel_lp64 mkl_gnu_thread mkl_core gomp")
						else ()
							list(APPEND BLAS_SEARCH_LIBS
								"mkl_blas95_lp64 mkl_intel_lp64 mkl_intel_thread mkl_core iomp5")
						endif ()
					endif ()
				endif ()
				if (BLA_VENDOR STREQUAL "Intel10_64lp_seq" OR BLA_VENDOR STREQUAL "All")
					list(APPEND BLAS_SEARCH_LIBS
						"mkl_blas95_lp64 mkl_intel_lp64 mkl_sequential mkl_core")
				endif ()
			else ()
				set(BLAS_mkl_SEARCH_SYMBOL sgemm)
				set(_LIBRARIES BLAS_LIBRARIES)
				if (WIN32)
					list(APPEND BLAS_SEARCH_LIBS
						"mkl_c_dll mkl_intel_thread_dll mkl_core_dll libiomp5")
				else ()
					if (BLA_VENDOR STREQUAL "Intel10_32" OR BLA_VENDOR STREQUAL "All")
						list(APPEND BLAS_SEARCH_LIBS
							"mkl_intel mkl_intel_thread mkl_core iomp5")
					endif ()
					if (BLA_VENDOR STREQUAL "Intel10_64lp" OR BLA_VENDOR STREQUAL "All")

						# old version
						list(APPEND BLAS_SEARCH_LIBS
							"mkl_intel_lp64 mkl_intel_thread mkl_core iomp5")

						# mkl >= 10.3
						if (CMAKE_C_COMPILER MATCHES ".+gcc.*")
							list(APPEND BLAS_SEARCH_LIBS
								"mkl_intel_lp64 mkl_gnu_thread mkl_core gomp")
						else ()
							list(APPEND BLAS_SEARCH_LIBS
								"mkl_intel_lp64 mkl_intel_thread mkl_core iomp5")
						endif ()
					endif ()

					#older vesions of intel mkl libs
					if (BLA_VENDOR STREQUAL "Intel" OR BLA_VENDOR STREQUAL "All")
						list(APPEND BLAS_SEARCH_LIBS
							"mkl")
						list(APPEND BLAS_SEARCH_LIBS
							"mkl_ia32")
						list(APPEND BLAS_SEARCH_LIBS
							"mkl_em64t")
					endif ()
				endif ()
				if (BLA_VENDOR STREQUAL "Intel10_64lp_seq" OR BLA_VENDOR STREQUAL "All")
					list(APPEND BLAS_SEARCH_LIBS
						"mkl_intel_lp64 mkl_sequential mkl_core")
				endif ()
			endif ()

			foreach (IT ${BLAS_SEARCH_LIBS})
				string(REPLACE " " ";" SEARCH_LIBS ${IT})
				if (${_LIBRARIES})
				else ()
					check_fortran_libraries(
						${_LIBRARIES}
						BLAS
						${BLAS_mkl_SEARCH_SYMBOL}
						""
						"${SEARCH_LIBS}"
						"${CMAKE_THREAD_LIBS_INIT};${LM}"
						)
				endif ()
			endforeach ()

		endif ()
	endif ()


	if(BLA_F95)
		if(BLAS95_LIBRARIES)
			set(BLAS95_FOUND TRUE)
		else()
			set(BLAS95_FOUND FALSE)
		endif()

		if(NOT BLAS_FIND_QUIETLY)
			if(BLAS95_FOUND)
				message(STATUS "A library with BLAS95 API found.")
			else()
				if(BLAS_FIND_REQUIRED)
					message(FATAL_ERROR
						"A required library with BLAS95 API not found. Please specify library location.")
				else()
					message(STATUS
						"A library with BLAS95 API not found. Please specify library location.")
				endif()
			endif()
		endif()
		set(BLAS_FOUND TRUE)
		set(BLAS_LIBRARIES "${BLAS95_LIBRARIES}")
	else()
		if(BLAS_LIBRARIES)
			set(BLAS_FOUND TRUE)
		else()
			set(BLAS_FOUND FALSE)
			set(BLAS_LIBRARIES "")
		endif()

		if(NOT BLAS_FIND_QUIETLY)
			if(BLAS_FOUND)
				message(STATUS "A library with BLAS API found.")
			else()
				if(BLAS_FIND_REQUIRED)
					message(FATAL_ERROR
						"A required library with BLAS API not found. Please specify library location."
						)
				else()
					message(STATUS
						"A library with BLAS API not found. Please specify library location."
						)
				endif()
			endif()
		endif()
	endif()

	set(CMAKE_FIND_LIBRARY_SUFFIXES ${_blas_ORIG_CMAKE_FIND_LIBRARY_SUFFIXES})

	set (FULLPATH_LIBS)
	foreach (BLIB ${BLAS_LIBRARIES})
		find_library (BLIB ${BLIB})
		list (APPEND FULLPATH_LIBS ${BLIB})
	endforeach ()

	set (BLAS_LIBRARIES ${FULLPATH_LIBS})
endif ()

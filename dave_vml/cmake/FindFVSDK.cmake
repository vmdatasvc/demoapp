# ======================================================
# Include directories to add to the user project:
# ======================================================
get_filename_component(THIS_ROOT_CONFIG_PATH "${CMAKE_CURRENT_LIST_FILE}" PATH)


SET(FVSDK_INCLUDE_DIRS "${THIS_ROOT_CONFIG_PATH}/FVSDK_8_4_0/include")
INCLUDE_DIRECTORIES(${FVSDK_INCLUDE_DIRS})

# ======================================================
# Link directories to add to the user project:
# ======================================================
SET(FVSDK_LIB_DIRS "${THIS_ROOT_CONFIG_PATH}/FVSDK_8_4_0/lib/x86_64/gcc-4.3-ipp" "${THIS_ROOT_CONFIG_PATH}/FVSDK_8_4_0/lib/x86_64/share")
LINK_DIRECTORIES(${FVSDK_LIB_DIRS})
# Provide the libs directory anyway, it may be needed in some cases.
SET(FVSDK_LIB_DIR ${FVSDK_LIB_DIRS})

# ====================================================================
# Link libraries: e.g.   -lfrsdk-8.4.0 -lstdc++ -lpthread, etc...
# ====================================================================
set(FVSDK_LIB_COMPONENTS -lfrsdk-8.4.0 -lstdc++ -lpthread)
SET(FVSDK_LIBS "")
foreach(__FVLIB ${FVSDK_LIB_COMPONENTS})
                SET(FVSDK_LIBS ${FVSDK_LIBS} ${__FVLIB})
endforeach(__FVLIB)


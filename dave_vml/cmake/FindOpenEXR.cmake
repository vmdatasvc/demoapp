# ===================================================================================
#  The OpenEXR CMake configuration file
#
#             ** File generated automatically, do not modify **
#
#  Usage from an external project: 
#    In your CMakeLists.txt, add these lines:
#
#    FIND_PACKAGE(OpenEXR REQUIRED )
#    TARGET_LINK_LIBRARIES(MY_TARGET_NAME ${OpenEXR_LIBS})   
#
#    This file will define the following variables:
#      - OpenEXR_LIBS          : The list of libraries to links against.
#      - OpenEXR_LIB_DIR       : The directory where lib files are. Calling LINK_DIRECTORIES
#                                with this path is NOT needed.

# ===================================================================================

set (OpenEXR_LIB_COMPONENTS Half IlmImf Iex Imath IlmThread)
set (OpenEXR_LIBS "")
foreach (__CVLIB ${OpenEXR_LIB_COMPONENTS})
	find_library (${__CVLIB}_PATH ${__CVLIB})
	list (APPEND OpenEXR_LIBS ${${__CVLIB}_PATH})
endforeach ()

set (OPENEXR_INCLUDE_DIR ${TVS_EXTERNAL_ROOT}/${TARGET}/include/OpenEXR/)

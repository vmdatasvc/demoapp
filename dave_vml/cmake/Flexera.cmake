if (WIN32)
	if (WIN64TARGET)
		set (FLEXERA_ROOT 
			"${TVS_ROOT}/deploy/win_emt64/flexera/x64_n6-11")

	else ()
		set (FLEXERA_ROOT "${TVS_ROOT}/deploy/win_i86/flexera/")
	endif ()

elseif (APPLE)
	set (FLEXERA_ROOT "${TVS_ROOT}/deploy/osx_emt64/flexera/mac10-11.11.0.2/universal_mac10")
	find_library (IOKIT_LIBRARY IOKit)
	set (FLEXERA_LIBS
		"${FLEXERA_ROOT}/libsb.a"
		"${FLEXERA_ROOT}/libcrvs.a"
		"${FLEXERA_ROOT}/liblmgr.a"
		"${FLEXERA_ROOT}/liblmgr_dongle_stub.a"
		"${FLEXERA_ROOT}/activation/lib/libnoact.a"
		"${IOKIT_LIBRARY}"
		)

else ()
	set (FLEXERA_ROOT "${TVS_3RDPARY_ROOT}/Flexera/x64_lsb--11.12.1.4/x64_lsb/")
	find_library (LIB_PTHREAD pthread)

	# Order matters here, or linking errors will result
	# lm_new.c must also be linked prior to any of these
	set (FLEXERA_LIBS
		"${FLEXERA_ROOT}/liblmgr_pic.a"
		"${FLEXERA_ROOT}/libcrvs_pic.a"
		"${FLEXERA_ROOT}/libsb_pic.a"
		"${FLEXERA_ROOT}/activation/lib/libnoact_pic.a"
		"${FLEXERA_ROOT}/liblmgr_dongle_stub_pic.a"
		"${LIB_PTHREAD}"
		)
endif ()

set (FLEXERA_INCLUDE "${FLEXERA_ROOT}/../machind")

if (TVS_DEPLOY)
	# Generate a separate lm_new.c for each build, as recommended by Flexera
	execute_process (COMMAND ./lmnewgen tandent -o ${PROJECT_BINARY_DIR}/lm_new.c WORKING_DIRECTORY ${FLEXERA_ROOT})
	set (FLEXERA_LM_NEW "${PROJECT_BINARY_DIR}/lm_new.c")
endif ()

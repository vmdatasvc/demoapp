macro (vml_add_library LIBNAME TYPE)
       vml_get_includes()

       # Define filecodes for all cpp files
#       tvs_define_filecode (${ARGN})  # file codes are for hiding file names in deployment. Use md5 to encrypt file names

       # Also include header files in library so they show up in IDE
       add_library (${LIBNAME} ${TYPE} ${ARGN} ${PROJECT_HEADER_FILES})

       # Run astyle on all source files prior to building above library
#	   vml_run_astyle (${LIBNAME} ${ARGN} ${PROJECT_HEADER_FILES})	# optionally apply astyle
endmacro ()

macro (vml_add_executable EXENAME)
       # Define filecodes for all cpp files
#       tvs_define_filecode (${ARGN})

	   add_executable (${EXENAME} ${TYPE} ${ARGN})

       # Run astyle on all source files prior to building above executable
#	   vml_run_astyle (${EXENAME} ${ARGN} ${PROJECT_HEADER_FILES})
endmacro ()

# Optional macro for running astyle
if (VML_RUN_ASTYLE)
	find_program (ASTYLE astyle)

	if (NOT ASTYLE)
		message (FATAL_ERROR "Error: Astyle executable not found")
	endif ()

	macro (vml_run_astyle LIBNAME)
		add_custom_command (
			TARGET ${LIBNAME}
			PRE_LINK
			COMMAND ${ASTYLE}
			--quiet
			--indent=tab
			--min-conditional-indent=0
			--suffix=none
			--indent-namespaces
			--indent-switches
			--max-instatement-indent=79
			--convert-tabs ${ARGN}
			WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
			)
	endmacro ()

else ()
	# Stub function which does nothing
	macro (vml_run_astyle)
	endmacro ()
endif ()

##### PLATFORM #####

# Macro for generating public (deploy) headers
macro (vml_generate_header HEADERFILE)
	vml_get_include_dir (PROJECT_INCLUDE_DIR)

	add_custom_command (
		OUTPUT ${VML_ROOT}/include/public/${HEADERFILE}

		DEPENDS
		${VML_ROOT}/utils/scripts/generateAPI_H.py
		${PROJECT_INCLUDE_DIR}/${HEADERFILE}

		COMMAND ${TVS_ROOT}/utils/scripts/generateAPI_H.py
		${PROJECT_INCLUDE_DIR}/${HEADERFILE}
		${TARGET}
		${TVS_ROOT}/include/public/${HEADERFILE}
		)

	add_custom_target (${HEADERFILE} DEPENDS ${TVS_ROOT}/include/public/${HEADERFILE})
	add_dependencies (${LIB_NAME} ${HEADERFILE})
endmacro ()

# TODO: Generate files from TrillienCommon only once
# macro for generating about headers and logo
macro (vml_generate_about LIBNAME LR_SHORTNAME)
	add_custom_command (
		OUTPUT
		${PROJECT_BINARY_DIR}/LightRayLogo${LR_SHORTNAME}.cpp
		${PROJECT_BINARY_DIR}/tvsAbout.h

		DEPENDS
		${TVS_ROOT}/doc/LightRayCommon/figures/LightRayLogo.ppm
		${TVS_ROOT}/doc/LightRayCommon/ACKNOWLEDGEMENTS.txt
		${TVS_ROOT}/doc/LightRayCommon/COPYRIGHT.txt
		${TVS_ROOT}/doc/LightRayCommon/LICENSE.txt
		${TVS_ROOT}/doc/LightRayCommon/NOTICE.txt
		${TVS_ROOT}/include/core/tvsAbout.h.in
		${TVS_ROOT}/meta.cfg

		COMMAND ${TVS_ROOT}/utils/scripts/genAbout.py
		${TVS_ROOT}/doc/LightRayCommon/figures/LightRayLogo.ppm ${PROJECT_BINARY_DIR}/LightRayLogo${LR_SHORTNAME}.cpp
		${TVS_ROOT}/doc/LightRayCommon/ACKNOWLEDGEMENTS.txt
		${TVS_ROOT}/doc/LightRayCommon/COPYRIGHT.txt
		${TVS_ROOT}/doc/LightRayCommon/LICENSE.txt
		${TVS_ROOT}/doc/LightRayCommon/NOTICE.txt
		${TVS_ROOT}/include/core/tvsAbout.h.in ${PROJECT_BINARY_DIR}/tvsAbout.h ${LR_SHORTNAME}
		)

	add_custom_target (tvsAbout${LR_SHORTNAME} DEPENDS ${PROJECT_BINARY_DIR}/tvsAbout.h)

	add_dependencies (${LIBNAME} tvsAbout${LR_SHORTNAME})

	include_directories (${PROJECT_BINARY_DIR})
endmacro ()

# Find a library in tvsext
# Does NOT search normal system paths for the library: only searches tvsext
# Library path is assigned to the variable LIB_${libname}
macro (vml_find_library libname)
	find_library (
		LIB_${libname}
		${libname}
		PATHS
		"${CMAKE_LIBRARY_PATH}"
		"${VML_EXTERNAL_ROOT}/${TARGET}/share/OpenCV/3rdparty/lib"
		"${IPP_LIBRARY_DIRS}"
		"${TBB_LIB_DIR}"
		"${BLAS_LIBS}"
		NO_DEFAULT_PATH
		)
endmacro ()

##### ALLPARAMS #####

# Generate the "AllParams" CPP and H files
#
# Must be called from the CMakeLists.txt of the library for which we're
# generating the files, because it uses the variable ${PROJECT_SOURCE_DIR}
#
# Parameters:
#	LIBNAME: Name of the library for which we're generating the files. See
#		genAllParams.py for a list of valid options.
#	ALLPARAMS_FILENAME: Name of the H and CPP files generated, without extension
#macro (vml_generate_allparams LIBNAME ALLPARAMS_FILENAME)

#	vml_get_include_dir (PROJECT_INCLUDE_DIR)

#	set (AllParams_CPP ${PROJECT_SOURCE_DIR}/${ALLPARAMS_FILENAME}.cpp)
#	set (AllParams_H ${PROJECT_INCLUDE_DIR}/${ALLPARAMS_FILENAME}.h)

#	add_custom_command (
#		OUTPUT ${AllParams_CPP}
#		OUTPUT ${AllParams_H}
#		COMMAND ${VML_ROOT}/utils/scripts/genAllParams.py ${LIBNAME}
#		DEPENDS ${VML_ROOT}/utils/scripts/genAllParams.py
#		DEPENDS ${VML_ROOT}/utils/scripts/genAllParamsInputs.py
#		DEPENDS ${VML_ROOT}/utils/scripts/genAllParamsOutputs.py
#		MAIN_DEPENDENCY ${VML_ROOT}/include/core/AllParams.par
#		)
#endmacro ()

##### INCLUDE DIRS #####

# Each subdirectory under src has a corresponding subdirectory under include
# This function converts from one to the other, by manipulating PROJECT_SOURCE_DIR
macro (vml_get_include_dir OUTVARNAME)
	string (REPLACE "${VML_ROOT}/src" "${VML_ROOT}/include" ${OUTVARNAME} "${PROJECT_SOURCE_DIR}")
endmacro ()

macro (vml_get_includes)
	vml_get_include_dir (PROJECT_INCLUDE_DIR)
	file (GLOB PROJECT_HEADER_FILES "${PROJECT_INCLUDE_DIR}/*.hpp")
endmacro ()

# - Find PNG
# Find the native PNG includes and library
# This module defines
#  PNG_INCLUDE_DIR, where to find jpeglib.h, etc.
#  PNG_LIBRARIES, the libraries needed to use PNG.
#  PNG_FOUND, If false, do not try to use PNG.
# also defined, but not for general use are
#  PNG_LIBRARY, where to find the PNG library.

# here is the target environment located
get_filename_component(THIS_ROOT_CONFIG_PATH "${TVS_EXTERNAL_ROOT}" ABSOLUTE)
SET(CMAKE_FIND_ROOT_PATH ${THIS_ROOT_CONFIG_PATH})

# adjust the default behavior of the FIND_XXX() commands:
# search headers and libraries in the target environment, search 
# programs in the host environment
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

FIND_PATH(PNG_INCLUDE_DIR png.h )

SET(PNG_NAMES ${PNG_NAMES} png)
FIND_LIBRARY(PNG_LIBRARY NAMES ${PNG_NAMES} )

# handle the QUIETLY and REQUIRED arguments and set PNG_FOUND to TRUE if 
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(PNG DEFAULT_MSG PNG_LIBRARY PNG_INCLUDE_DIR)

IF(PNG_FOUND)
  SET(PNG_LIBRARIES ${PNG_LIBRARY})
ENDIF(PNG_FOUND)

# Deprecated declarations.
SET (NATIVE_PNG_INCLUDE_PATH ${PNG_INCLUDE_DIR} )
IF(PNG_LIBRARY)
  GET_FILENAME_COMPONENT (NATIVE_PNG_LIB_PATH ${PNG_LIBRARY} PATH)
ENDIF(PNG_LIBRARY)

MARK_AS_ADVANCED(PNG_LIBRARY PNG_INCLUDE_DIR )

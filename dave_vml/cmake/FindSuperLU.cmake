# ===================================================================================
#  The SuperLU CMake configuration file
#
#             ** File generated automatically, do not modify **
#
#  Usage from an external project: 
#    In your CMakeLists.txt, add these lines:
#
#    FIND_PACKAGE(SuperLU REQUIRED )
#    TARGET_LINK_LIBRARIES(MY_TARGET_NAME ${SuperLU_LIBS})   
#
#    This file will define the following variables:
#      - SuperLU_LIBS          : The list of libraries to links against.
#      - SuperLU_LIB_DIR       : The directory where lib files are. Calling LINK_DIRECTORIES
#                                with this path is NOT needed.

# ===================================================================================

# Extract the directory where *this* file has been installed (determined at cmake run-time)
#  This variable may or may not be used below, depending on the parsing of SuperLUConfig.cmake
get_filename_component(THIS_SuperLU_CONFIG_PATH "${TVS_EXTERNAL_ROOT}" ABSOLUTE)

# ======================================================
# Include directories to add to the user project:
# ======================================================
get_filename_component(SuperLU_INCLUDE_DIRS "${THIS_SuperLU_CONFIG_PATH}/include" ABSOLUTE)
INCLUDE_DIRECTORIES(${SuperLU_INCLUDE_DIRS})

# ======================================================
# Link directories to add to the user project:
# ======================================================
get_filename_component(SuperLU_LIB_DIRS "${THIS_SuperLU_CONFIG_PATH}/lib" ABSOLUTE)
LINK_DIRECTORIES(${SuperLU_LIB_DIRS})
# Provide the libs directory anyway, it may be needed in some cases.
SET(SuperLU_LIB_DIR ${SuperLU_LIB_DIRS})

# ====================================================================
# Link libraries:
# ====================================================================
set(SuperLU_LIB_COMPONENTS superlu_4.1)
SET(SuperLU_LIBS "")
foreach(__UMFLIB ${SuperLU_LIB_COMPONENTS})
	# CMake>=2.6 supports the notation "debug XXd optimized XX"
	if (CMAKE_MAJOR_VERSION GREATER 2  OR  CMAKE_MINOR_VERSION GREATER 4)
		# Modern CMake:
		SET(SuperLU_LIBS ${SuperLU_LIBS} debug ${__UMFLIB} optimized ${__UMFLIB})
	else(CMAKE_MAJOR_VERSION GREATER 2  OR  CMAKE_MINOR_VERSION GREATER 4)
		# Old CMake:
		SET(SuperLU_LIBS ${SuperLU_LIBS} ${__UMFLIB})
	endif(CMAKE_MAJOR_VERSION GREATER 2  OR  CMAKE_MINOR_VERSION GREATER 4)
endforeach(__UMFLIB)

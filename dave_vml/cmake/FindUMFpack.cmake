# ===================================================================================
#  The UMFpack CMake configuration file
#
#             ** File generated automatically, do not modify **
#
#  Usage from an external project: 
#    In your CMakeLists.txt, add these lines:
#
#    FIND_PACKAGE(UMFpack REQUIRED )
#    TARGET_LINK_LIBRARIES(MY_TARGET_NAME ${UMFpack_LIBS})   
#
#    This file will define the following variables:
#      - UMFpack_LIBS          : The list of libraries to links against.
#      - UMFpack_LIB_DIR       : The directory where lib files are. Calling LINK_DIRECTORIES
#                                with this path is NOT needed.

# ===================================================================================

# Extract the directory where *this* file has been installed (determined at cmake run-time)
#  This variable may or may not be used below, depending on the parsing of UMFpackConfig.cmake
get_filename_component(THIS_UMFpack_CONFIG_PATH "${TVS_EXTERNAL_ROOT}" ABSOLUTE)

# ======================================================
# Include directories to add to the user project:
# ======================================================
get_filename_component(UMFpack_INCLUDE_DIRS "${THIS_UMFpack_CONFIG_PATH}/include" ABSOLUTE)
INCLUDE_DIRECTORIES(${UMFpack_INCLUDE_DIRS})

# ======================================================
# Link directories to add to the user project:
# ======================================================
get_filename_component(UMFpack_LIB_DIRS "${THIS_UMFpack_CONFIG_PATH}/lib" ABSOLUTE)
LINK_DIRECTORIES(${UMFpack_LIB_DIRS})
# Provide the libs directory anyway, it may be needed in some cases.
SET(UMFpack_LIB_DIR ${UMFpack_LIB_DIRS})

# ====================================================================
# Link libraries:
# ====================================================================
set(UMFpack_LIB_COMPONENTS umfpack amd cxsparse)
SET(UMFpack_LIBS "")
foreach(__UMFLIB ${UMFpack_LIB_COMPONENTS})
	# CMake>=2.6 supports the notation "debug XXd optimized XX"
	if (CMAKE_MAJOR_VERSION GREATER 2  OR  CMAKE_MINOR_VERSION GREATER 4)
		# Modern CMake:
		SET(UMFpack_LIBS ${UMFpack_LIBS} debug ${__UMFLIB} optimized ${__UMFLIB})
	else(CMAKE_MAJOR_VERSION GREATER 2  OR  CMAKE_MINOR_VERSION GREATER 4)
		# Old CMake:
		SET(UMFpack_LIBS ${UMFpack_LIBS} ${__UMFLIB})
	endif(CMAKE_MAJOR_VERSION GREATER 2  OR  CMAKE_MINOR_VERSION GREATER 4)
endforeach(__UMFLIB)

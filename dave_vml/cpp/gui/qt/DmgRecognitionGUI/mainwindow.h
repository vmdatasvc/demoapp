#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QMessageBox>
#include <QTimer>
#include <QtDebug>

#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "modules/ml/AITPort/Image.hpp"
#include "boost/shared_ptr.hpp"
#include <fstream>
#include <sstream>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void cvtMatToImage32(cv::Mat iMat, aitvml::Image32 *img);
    void cvtMatToImage8(cv::Mat imat, aitvml::Image8 *img);
    void cvtImage32ToMat(aitvml::Image32 img, cv::Mat *iMat);
    void cvtImage8ToMat(aitvml::Image8 img, cv::Mat *imat);

    cv::Mat gImgMat;


private slots:

    void updateMethod();

    void on_OpenFilePushButton_clicked();

    void on_StartPushButton_toggled(bool checked);

    void on_PausePushButton_toggled(bool checked);

    void on_pushButton_clicked();

    void on_WebCamInputRadioButton_clicked();

private:
    Ui::MainWindow *ui;

    // ui widget maps
    QPushButton *muiStartButton;

    // opencv

    cv::VideoCapture    mCapture;

    bool mStarted;


    QTimer *timer;

protected:

    void init();
    void timerEvent(QTimerEvent *event);

    // Test
    void testImageConversion();
    void testDemographicsRecognition();
    void testReadXML();
    void testFaceDetection();
    void faceDetectionByDirectOpenCV();
    void faceDetectionByVML();

};

#endif // MAINWINDOW_H

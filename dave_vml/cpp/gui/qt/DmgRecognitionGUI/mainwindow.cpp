#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "core/Settings.hpp"
#include "modules/ml/Recognition.hpp"
#include "modules/ml/AITPort/Settings.hpp"
#include "modules/ml/AITPort/FaceDetectorAdaBoost.hpp"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    init();
    timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()), this, SLOT(updateMethod()));

}

MainWindow::~MainWindow()
{
    delete ui;

    delete timer;
}

void MainWindow::init()
{

    // clean viewer
    ui->openCVViewer1->clearImage(); // this function is changed by Dave, but still have a problem.
    ui->openCVViewer2->clearImage(); // this function is changed by Dave, but still have a problem.

    // initial selection of a radio button
    ui->ImageInputRadioButton->setChecked(true);
    ui->VideoInputRadioButton->setChecked(false);
    ui->WebCamInputRadioButton->setChecked(false);

}


void MainWindow::timerEvent(QTimerEvent *event)
{
    //cv::Mat image, dispImg;
    //mCapture >> image;

    // Show the image
    //cv::cvtColor(image,dispImg, CV_BGR2RGB);
    //ui->openCVViewer1->showImage(dispImg);
}


void MainWindow::updateMethod()
{

    cv::Mat image, dispImg;
    mCapture >> image;

    // Show the image
    cv::cvtColor(image,dispImg, CV_BGR2RGB);
    ui->openCVViewer1->showImage(dispImg);


}


void MainWindow::on_OpenFilePushButton_clicked()
{
    QString tmpString = QFileDialog::getOpenFileName(this, "Open File");

    if( !tmpString.trimmed().isEmpty())
    {
        ui->OpenFileLineEdit->setText(tmpString);
    }
}

void MainWindow::on_StartPushButton_toggled(bool checked)
{
    if(checked)
    {
        // change StartButton text to Stop
        ui->StartPushButton->setText("Stop");
        // disable ui
        ui->OpenFileLineEdit->setEnabled(false);
        ui->VidIntervalSpinBox->setEnabled(false);
        ui->OpenFilePushButton->setEnabled(false);
        // enable ui
        ui->PausePushButton->setEnabled(true);

        if (ui->ImageInputRadioButton->isChecked())
        {
            cv::Mat image, dispImg;

            image = cv::imread(ui->OpenFileLineEdit->text().toStdString());

            // Show the image
            cv::cvtColor(image,dispImg, CV_BGR2RGB);
            ui->openCVViewer1->showImage(dispImg);

            image.copyTo(gImgMat);

        }
        else
        {
            if (ui->VideoInputRadioButton->isChecked())
            {
                mCapture.open(ui->OpenFileLineEdit->text().toStdString());
            }
            else if (ui->WebCamInputRadioButton->isChecked())
            {
                mCapture.open(0);

            }

            if( !mCapture.isOpened() )
            {
                QMessageBox msg;
                msg.setText("Error : Can't open video source!");
                return;
            }

            timer->start(ui->VidIntervalSpinBox->value());
            ui->Viewer1Label->setText("Input Image");

        }
    }
    else  // uncheced
    {
        ui->StartPushButton->setText("Start");
        // enable ui
        ui->OpenFileLineEdit->setEnabled(true);
        ui->VidIntervalSpinBox->setEnabled(true);
        ui->OpenFilePushButton->setEnabled(true);
        // disable ui
        ui->PausePushButton->setEnabled(false);

        ui->Viewer1Label->setText("");

        // stop all timer
        timer->stop();

        // clear viewers#include <face_detector/FaceDetectorAdaBoost.hpp>
        ui->openCVViewer1->clearImage();

        // stop video capture
        if (mCapture.isOpened())
            mCapture.release();

    }
}

void MainWindow::on_PausePushButton_toggled(bool checked)
{
    if(checked)
    {
        // chage capture to Resume
        ui->PausePushButton->setText("Resume");
        ui->VidIntervalSpinBox->setEnabled(true);

        // stop the timers
        timer->stop();


    }
    else
    {
        // chage caption to pausimge
        ui->PausePushButton->setText("Pause");
        ui->VidIntervalSpinBox->setEnabled(false);

        // restart the timers
        timer->start(ui->VidIntervalSpinBox->value());
    }

}


void MainWindow::cvtMatToImage32(cv::Mat imat, aitvml::Image32 *img)
{
       int w = imat.cols;
       int h = imat.rows;

       (*img) = aitvml::Image32(w,h);
/*
       cv::Mat tmpMat =  cv::Mat(h,w,CV_8UC4);


       if(img->getInternalFormat() == 0)
       {
           cv::cvtColor(imat,tmpMat, CV_RGB2BGRA);
       }
       else
       {
           cv::cvtColor(imat,tmpMat, CV_RGB2RGBA);
       }


       for(int y=0; y<h; y++)
       {
           unsigned char *pSrcData = ((unsigned char *)tmpMat.data) + y*w*4;
           unsigned char *pDstData = ((unsigned char *)img->pointer()) + y*w*4;

           for(int x=0; x<w; x++)
           {
                memcpy(pSrcData, pDstData, sizeof(unsigned char)*4);
           }
       }


//       memcpy((unsigned char *)tmpMat.data, (unsigned char *)img->pointer(), sizeof(unsigned char)*w*h*4);


*/
       for(int y=0; y<h; y++)
       {
           unsigned char *pSrcData = ((unsigned char *)imat.data) + y*w*3;
           unsigned char *pDstData = ((unsigned char *)img->pointer()) + y*w*4;

           for(int x=0; x<w; x++)
           {
               if(img->getInternalFormat() == 0)
               {
                    pDstData[0] = pSrcData[2];
                    pDstData[1] = pSrcData[1];
                    pDstData[2] = pSrcData[0];
               }
               else
               {
                   pDstData[0] = pSrcData[0];
                   pDstData[1] = pSrcData[1];
                   pDstData[2] = pSrcData[2];
               }
               pSrcData += 3;
               pDstData += 4;
            }
       }


}

void MainWindow::cvtImage32ToMat(aitvml::Image32 img, cv::Mat *imat)
{

    int w = img.width();
    int h = img.height();


    cv::Mat tmpMat =  cv::Mat(h,w,CV_8UC4, (void*)img.pointer());

    if(img.getInternalFormat() == 0)
    {
        cv::cvtColor(tmpMat, (*imat), CV_BGRA2RGB);
    }
    else
    {
        cv::cvtColor(tmpMat, (*imat), CV_BGRA2BGR);
    }


/*
    (*imat) = cv::Mat(h,w,CV_8UC3);

    for(int y=0; y<h; y++)
    {
        unsigned char *pSrcData = (unsigned char *)img.pointer() + y*w*4;
        unsigned char *pDstData = (unsigned char *)imat->data + y*w*3;

        for(int x=0; x<w; x++)
        {
            if(img.getInternalFormat() == 0)
            {
                 pDstData[0] = pSrcData[2];
                 pDstData[1] = pSrcData[1];
                 pDstData[2] = pSrcData[0];
            }
            else
            {
                pDstData[0] = pSrcData[0];
                pDstData[1] = pSrcData[1];
                pDstData[2] = pSrcData[2];
            }
            pSrcData += 4;
            pDstData += 3;
         }
    }
*/

}

void MainWindow::cvtMatToImage8(cv::Mat imat, aitvml::Image8 *img)
{
    int w = imat.cols;
    int h = imat.rows;

    (*img) = aitvml::Image8(w,h);


    for(int y=0; y<h; y++)
    {
        unsigned char *pSrcData = ((unsigned char *)imat.data) + y*w;
        unsigned char *pDstData = ((unsigned char *)img->pointer()) + y*w;

        for(int x=0; x<w; x++)
        {
            pDstData[0] = pSrcData[0];
            pSrcData ++;
            pDstData ++;
         }
    }


}

void MainWindow::cvtImage8ToMat(aitvml::Image8 img, cv::Mat *imat)
{

    int w = img.width();
    int h = img.height();

    (*imat) = cv::Mat(h,w,CV_8UC1);

    for(int y=0; y<h; y++)
    {
        unsigned char *pSrcData = (unsigned char *)img.pointer() + y*w;
        unsigned char *pDstData = (unsigned char *)imat->data + y*w;

        for(int x=0; x<w; x++)
        {
            pDstData[0] = pSrcData[0];
            pSrcData ++;
            pDstData ++;
         }
    }

}


void MainWindow::on_pushButton_clicked()
{
    //testImageConversion();

   //testReadXML();

   //testFaceDetection();

   testDemographicsRecognition();

}


//----------------- Test functions-------------------------------------------//

void MainWindow::testImageConversion()
{
    // Color image conversion
    aitvml::Image32 inImg;    
    cv::Mat tmpMat, disptmpMat;

    cvtMatToImage32(gImgMat, &inImg);
    cvtImage32ToMat(inImg, &tmpMat);

    cv::cvtColor(tmpMat, disptmpMat, CV_BGR2RGB);
    ui->openCVViewer1->showImage(disptmpMat);

    // Gray-scale image conversion
    aitvml::Image8  ginImg;
    cv::Mat grayMat(gImgMat.rows, gImgMat.cols, CV_8UC1), grayMatOut;

    cv::cvtColor(gImgMat, grayMat, CV_BGR2GRAY);
    cvtMatToImage8(grayMat, &ginImg );
    cvtImage8ToMat(ginImg, &grayMatOut );

    ui->openCVViewer2->showImage(grayMatOut);


}

void MainWindow::testReadXML()
{

    aitvml::Settings mSet;
    mSet.parseXml("/home/dkim/VM/vml/cpp/settings/vml_modules/FaceTracker.xml");
//    mSet.parseXml("/home/dkim/VM/vml/cpp/settings/vml_modules/CameraModelOpenCV.xml");
    //aitvml::Settings mSet("/home/dkim/VM/vml/cpp/settings/vml_modules/DemographicsRecognition.xml");

    int val = mSet.getInt("FaceDetection/minFaceSize", 1, false);
//    float val = mSet.getFloat("CameraModelOpenCV/cameraHeight",0.0,false);

    qDebug() << "output = " << val;

}

void MainWindow::testFaceDetection()
{
    faceDetectionByVML();
    //faceDetectionByDirectOpenCV();

}

void MainWindow::faceDetectionByVML()
{

    int NumDetectedFace;
    aitvml::Image32 frame;
    aitvml::Image8  grayFrame;

    // Color to gray
    cv::Mat grayImgMat(gImgMat.rows, gImgMat.cols, CV_8UC1);
    cv::cvtColor(gImgMat, grayImgMat, CV_BGR2GRAY);
    //equalizeHist(grayImgMat, grayImgMat);
    //cvtMatToImage8(grayImgMat, &grayFrame);

    cvtMatToImage32(gImgMat, &frame);
    aitvml::PvImageConverter::convert(frame,grayFrame);


    // Load the cascaed xml for face detection
    aitvml::Settings mSet;
    mSet.parseXml("/home/dkim/VM/vml/cpp/settings/vml_modules/FaceTracker.xml");
    std::string fdfile = mSet.getString("FaceDetection/cascadeFile"," ",false);
    qDebug() << "Cascade file for Face Detection: " << fdfile.c_str();


    // Use of vmlml (With the class of aitvml::vision::FaceDetectorAdaBoost)
    aitvml::vision::FaceDetectorAdaBoost fd;
    fd.initialize();
    fd.init(fdfile);
    fd.detect(grayFrame);
    NumDetectedFace = fd.nrOfFaces();

     // Display rectangles of detected faces
    cv::Mat outMat,dispOutMat;
    gImgMat.copyTo(outMat);

    aitvml::Face lface[10];
    for (int i = 0; i < NumDetectedFace; i++)
    {
        lface[i] = fd.face(i);
        cv::Rect frect(lface[i].x,lface[i].y, lface[i].w, lface[i].h);
        cv::rectangle(outMat,frect, CV_RGB(0,255,0),1, 8, 0);

    }

    ui->openCVViewer2->clearImage();
    cv::cvtColor(outMat, dispOutMat, CV_BGR2RGB );
    ui->openCVViewer2->showImage(dispOutMat);

    qDebug() << "vmlml Face# = " << NumDetectedFace;


}

void MainWindow::faceDetectionByDirectOpenCV()
{
    int NumDetectedFace ;
    cv::Mat grayImgMat(gImgMat.rows, gImgMat.cols, CV_8UC1);
    cv::cvtColor(gImgMat, grayImgMat, CV_BGR2GRAY);
    equalizeHist(grayImgMat, grayImgMat);

    // Direct use of OpenCV
    cv::CascadeClassifier face_cascade;
    std::vector<cv::Rect> faces;

    face_cascade.load("/home/dkim/VM/vml/data/ml/opencv/haarcascades/haarcascade_frontalface_alt2.xml");
    face_cascade.detectMultiScale(grayImgMat, faces, 1.2, 2, CV_HAAR_FIND_BIGGEST_OBJECT | CV_HAAR_SCALE_IMAGE, cv::Size(30, 30));
    NumDetectedFace = faces.size();

    qDebug() << "directOpenCV Face# =" << NumDetectedFace;


    // Display rectangles of detected faces
    cv::Mat outMat, dispOutMat;
    gImgMat.copyTo(outMat);
    for (int i = 0; i < NumDetectedFace; i++)
    {
        cv::Rect face_i = faces[i];
        cv::rectangle(outMat, face_i, CV_RGB(255, 0,0), 1);
        int pos_x = std::max(face_i.tl().x - 10, 0);
        int pos_y = std::max(face_i.tl().y - 10, 0);
        string box_text = cv::format("(G,A,R)");
        cv::putText(outMat,box_text, cv::Point(pos_x, pos_y), 1, 1.0, CV_RGB(0,255,0), 1.0);
    }
    ui->openCVViewer2->clearImage();
    cv::cvtColor(outMat, dispOutMat, CV_BGR2RGB );
    ui->openCVViewer2->showImage(dispOutMat);

}


void MainWindow::testDemographicsRecognition()
{

    int NumDetectedFace ;
    cv::Mat grayImgMat(gImgMat.rows, gImgMat.cols, CV_8UC1);
    cv::cvtColor(gImgMat, grayImgMat, CV_BGR2GRAY);
    equalizeHist(grayImgMat, grayImgMat);

    // Direct use of OpenCV
    cv::CascadeClassifier face_cascade;
    std::vector<cv::Rect> faces;

    face_cascade.load("/home/dkim/VM/vml/data/ml/opencv/haarcascades/haarcascade_frontalface_alt2.xml");
    face_cascade.detectMultiScale(grayImgMat, faces, 1.2, 2, CV_HAAR_FIND_BIGGEST_OBJECT | CV_HAAR_SCALE_IMAGE, cv::Size(30, 30));
    NumDetectedFace = faces.size();

    qDebug() << "directOpenCV Face# =" << NumDetectedFace;

    // Initialization

    boost::shared_ptr<vml::Settings> mpSettings;
    mpSettings.reset(new vml::Settings);
    mpSettings->parseXml("/home/dkim/VM/vml/cpp/settings/vml_modules/DMGRecognition.xml");

    boost::shared_ptr<aitvml::Recognition>  	oRecog;
    //aitvml::Recognition oRecog;
    oRecog.reset(new aitvml::Recognition(mpSettings));

    oRecog->initFromFace();
    qDebug() << "Initialized!";

    // Display rectangles of detected faces
    cv::Mat outMat, dispOutMat;
    gImgMat.copyTo(outMat);
    for (int i = 0; i < NumDetectedFace; i++)
    {
        cv::Rect cropBox = faces[i];
        cv::rectangle(outMat, cropBox, CV_RGB(255, 0,0), 1);
        //int pos_x = std::max(cropBox.tl().x - 10, 0);
        //int pos_y = std::max(cropBox.tl().y - 10, 0);
        //string box_text = cv::format("(G,A,R)");
        //cv::putText(outMat,box_text, cv::Point(pos_x, pos_y), 1, 1.0, CV_RGB(0,255,0), 1.0);


        // Demographics Recognition
        cv::Mat cropped = grayImgMat(cropBox);
        aitvml::Image32 inImg;

        cvtMatToImage32(cropped, &inImg);
        oRecog->faceImage = inImg;

         oRecog->processFromFace();


         std::ostringstream gender_str, age_str, eth_str;
         cv::Point gender_pos(cropBox.x + cropBox.width + 5, cropBox.y -5);
         gender_str << oRecog->mEstGenderString;
         cv::putText(outMat, gender_str.str(), gender_pos, 1, 0.8f, cv::Scalar(255,0,0));

         cv::Point age_pos(cropBox.x + cropBox.width + 5, cropBox.y + std::floor(cropBox.height/2) );
         age_str << oRecog->mEstAgeString;
         cv::putText(outMat, age_str.str(), age_pos, 1, 0.8f, cv::Scalar(0,255,255));

         cv::Point eth_pos(cropBox.x + cropBox.width + 5 , cropBox.y + cropBox.height + 5);
         eth_str << oRecog->mEstEthnicityString;
         cv::putText(outMat, eth_str.str(), eth_pos, 1, 0.8f, cv::Scalar(255,200,255));

    }

    ui->openCVViewer2->clearImage();
    cv::cvtColor(outMat, dispOutMat, CV_BGR2RGB );
    ui->openCVViewer2->showImage(dispOutMat);

    cv::Mat dispImg;
    cv::cvtColor(gImgMat, dispImg, CV_BGR2RGB );
    ui->openCVViewer1->showImage(dispImg);

}


/*
void MainWindow::testDemographicsRecognition()
{

    aitvml::Recognition oRecog;

    // Need to set up Image or video information before using Recognition class
    oRecog.gVideoWidth = gImgMat.cols;
    oRecog.gVideoHeight = gImgMat.rows;
    oRecog.gVideoFPS = 0;

    // Initialization
    oRecog.initFromFace();

    qDebug() << "Initialized!";


    oRecog.faceImage = aitvml::Image32(gImgMat.cols, gImgMat.rows);


    if(1)
    {
        cvtMatToImage32(gImgMat, &(oRecog.faceImage));
    }
    else
    {
        aitvml::Image32 inImg;

        cvtMatToImage32(gImgMat, &inImg);
        cv::Mat tmpMat;

        cvtImage32ToMat(inImg, &tmpMat);
        ui->openCVViewer2->showImage(tmpMat);

        oRecog.faceImage = inImg;
    }
    // Process with the selected frame as an input


    oRecog.processFromFace();


    // Visualize the gray-scale image
    cv::Mat tmpMat, dispMat;

    //cvtImage8ToMat(oRecog.grayFrame, &tmpMat);
    cvtImage32ToMat(oRecog.faceDetectedImage, &tmpMat);


    qDebug() << "# of detected faces" << oRecog.gNumDetectedFace;


    // Visualize Face detection result

    gImgMat.copyTo(tmpMat);
    for (int i = 0; i < oRecog.gNumDetectedFace; i++)
    {
        cv::Rect frect(oRecog.mpFront->face(i).x, oRecog.mpFront->face(i).y,oRecog.mpFront->face(i).w, oRecog.mpFront->face(i).h);

        cv::rectangle(tmpMat,frect, CV_RGB(255,i*10,0),1, 8, 0);
    }
    ui->openCVViewer2->clearImage();
    cv::cvtColor(tmpMat,dispMat, CV_BGR2RGB);
    ui->openCVViewer2->showImage(dispMat);


    // Visualize Normalization face


    // Output
    int igender, iage, irace;

    igender = oRecog.mEstGenderLabel;
    iage =  oRecog.mEstAgeLabel;
    irace = oRecog.mEstRaceLabel;

    qDebug("Result:Gender = %d, Age = %d, Race = %d",igender, iage, irace);
}
*/

void MainWindow::on_WebCamInputRadioButton_clicked()
{
    QString lineText = "0";
    ui->OpenFileLineEdit->setText(lineText);
}

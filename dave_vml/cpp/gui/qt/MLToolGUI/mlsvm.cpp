#include "mlsvm.h"
#include "errno.h"

#ifdef USE_LIBSVM
MLSVM::MLSVM(boost::shared_ptr<vml::Settings> settings)
{

    setParams(settings);
}

MLSVM::~MLSVM()
{
    if(model)
        svm_free_and_destroy_model(&model);
    if(x)
        free(x);
    if(line)
        free(line);

}


void MLSVM::setParams(boost::shared_ptr<vml::Settings> settings)
{
/*
    param.svm_type = settings->getInt("Classifier/SVM/type", 0, false);    //0(C_SVC);
    param.kernel_type = settings->getInt("Classifier/SVM/kernel", 2, false);    //2(RBF);
    param.C = settings->getFloat("Classifier/SVM/C",1, false);
    param.gamma = settings->getFloat("Classifier/SVM/gamma",0.5, false);

    param.eps = settings->getFloat("Classifier/SVM/eps",1e-3, false);
    param.probability = settings->getInt("Classifier/SVM/probability",0, false);
    param.cache_size = settings->getInt("Classifier/SVM/cachesize",100, false);
    cross_validation = settings->getInt("Classifier/SVM/crossvalidation",0, false);
    nr_fold = settings->getInt("Classifier/SVM/nfold", 5, false);

    param.degree = settings->getInt("Classifier/SVM/degree",3, false);
    param.coef0 = settings->getFloat("Classifier/SVM/coef0",0, false);
    param.nu = settings->getFloat("Classifier/SVM/nu",0.5, false);
    param.p = settings->getFloat("Classifier/SVM/p",0.1, false);
    param.shrinking = 1;
    param.nr_weight = 0;
    param.weight_label = NULL;
    param.weight = NULL;

    // for svm-scale
    scaleL = settings->getFloat("Classifier/SVM/scaleLowerLimit",-1.0, false);
    scaleU = settings->getFloat("Classifier/SVM/scaleUpperLimit",1.0, false);
*/

    param.probability = settings->getInt("Classifier/SVM/probability",0, false);
    predict_probability = param.probability;
}


char* MLSVM::readline(std::FILE *input)
{
    int len;

    if(fgets(line,max_line_len,input) == NULL)
        return NULL;

    while(strrchr(line,'\n') == NULL)
    {
        max_line_len *= 2;
        line = (char *) realloc(line,max_line_len);
        len = (int) strlen(line);
        if(fgets(line+len,max_line_len-len,input) == NULL)
            break;
    }
    return line;
}

void MLSVM::exit_input_error(int line_num)
{
    fprintf(stderr,"Wrong input format at line %d\n", line_num);
    exit(1);
}

static int (*info)(const char *fmt,...) = &printf;
void MLSVM::predictFromFile(std::FILE *input, std::FILE *output)
{
    int correct = 0;
    int total = 0;
    double error = 0;
    double sump = 0, sumt = 0, sumpp = 0, sumtt = 0, sumpt = 0;


    int svm_type=svm_get_svm_type(model);
    int nr_class=svm_get_nr_class(model);
    double *prob_estimates=NULL;
    int j;  

    if(predict_probability)
    {
        if (svm_type==NU_SVR || svm_type==EPSILON_SVR)
            info("Prob. model for test data: target value = predicted value + z,\nz: Laplace distribution e^(-|z|/sigma)/(2sigma),sigma=%g\n",svm_get_svr_probability(model));
        else
        {
            int *labels=(int *) malloc(nr_class*sizeof(int));
            svm_get_labels(model,labels);
            prob_estimates = (double *) malloc(nr_class*sizeof(double));
            fprintf(output,"labels");
            for(j=0;j<nr_class;j++)
                fprintf(output," %d",labels[j]);
            fprintf(output,"\n");
            free(labels);
        }
    }

    max_line_len = 1024;
    line = (char *)malloc(max_line_len*sizeof(char));
    while(readline(input) != NULL)
    {
        int i = 0;
        double target_label, predict_label;
        char *idx, *val, *label, *endptr;
        int inst_max_index = -1; // strtol gives 0 if wrong format, and precomputed kernel has <index> start from 0

        label = strtok(line," \t\n");
        if(label == NULL) // empty line
            exit_input_error(total+1);

        target_label = strtod(label,&endptr);
        if(endptr == label || *endptr != '\0')
            exit_input_error(total+1);

        while(1)
        {
            if(i>=max_nr_attr-1)	// need one more for index = -1
            {
                max_nr_attr *= 2;
                x = (struct svm_node *) realloc(x,max_nr_attr*sizeof(struct svm_node));
            }

            idx = strtok(NULL,":");
            val = strtok(NULL," \t");

            if(val == NULL)
                break;
            errno = 0;
            x[i].index = (int) strtol(idx,&endptr,10);
            if(endptr == idx || errno != 0 || *endptr != '\0' || x[i].index <= inst_max_index)
                exit_input_error(total+1);
            else
                inst_max_index = x[i].index;

            errno = 0;
            x[i].value = strtod(val,&endptr);
            if(endptr == val || errno != 0 || (*endptr != '\0' && !isspace(*endptr)))
                exit_input_error(total+1);

            ++i;
        }
        x[i].index = -1;

        if (predict_probability && (svm_type==C_SVC || svm_type==NU_SVC))
        {
            predict_label = svm_predict_probability(model,x,prob_estimates);
            fprintf(output,"%g",predict_label);
            for(j=0;j<nr_class;j++)
                fprintf(output," %g",prob_estimates[j]);
            fprintf(output,"\n");
        }
        else
        {
            predict_label = svm_predict(model,x);
            fprintf(output,"%g\n",predict_label);
        }

        if(predict_label == target_label)
            ++correct;
        error += (predict_label-target_label)*(predict_label-target_label);
        sump += predict_label;
        sumt += target_label;
        sumpp += predict_label*predict_label;
        sumtt += target_label*target_label;
        sumpt += predict_label*target_label;
        ++total;
    }
    if (svm_type==NU_SVR || svm_type==EPSILON_SVR)
    {
        info("Mean squared error = %g (regression)\n",error/total);
        info("Squared correlation coefficient = %g (regression)\n",
            ((total*sumpt-sump*sumt)*(total*sumpt-sump*sumt))/
            ((total*sumpp-sump*sump)*(total*sumtt-sumt*sumt))
            );
    }
    else
    {
        testAcc = (double)correct/total*100 ;
        info("Accuracy = %g%% (%d/%d) (classification)\n",
            (double)correct/total*100,correct,total);
    }
    if(predict_probability)
        free(prob_estimates);

}

double MLSVM::predict(cv::Mat testData)
{

    // feature vector to svm_node for libsvm
    struct svm_node *vin;
    vin = (struct svm_node *) malloc((testData.cols+1)*sizeof(struct svm_node));

    int i;
    for(i=0; i<testData.cols;i++)
    {
        vin[i].index = i+1;
        vin[i].value = testData.at<float>(0,i);  // Row-based data
    }
    vin[i].index = -1;
    vin[i].value = '?';

    int svm_type=svm_get_svm_type(model);
    int nr_class=svm_get_nr_class(model);

    double predict_label;
    double *prob_estimates=NULL;
    prob_estimates = (double *) malloc(nr_class*sizeof(double));

    if (predict_probability && (svm_type==C_SVC || svm_type==NU_SVC))
    {
        predict_label = svm_predict_probability(model,vin,prob_estimates);
        std::cout << predict_label;

        for(int j=0;j<nr_class;j++)
            std::cout << prob_estimates[j] << " ";
        std::cout << std::endl;
    }
    else
    {
        predict_label = svm_predict(model,vin);
        std::cout << predict_label << std::endl;
    }


    if(vin)
        free(vin);

    if(predict_probability)
        free(prob_estimates);

    return predict_label;
}

double MLSVM::predict(std::vector<float> featVec)
{
    // feature vector to svm_node for libsvm
    struct svm_node *vin;
    vin = (struct svm_node *) malloc((featVec.size()+1)*sizeof(struct svm_node));

    int i;
    for(i=0; i<featVec.size();i++)
    {
        vin[i].index = i+1;
        vin[i].value = featVec.at(i);
    }
    vin[i].index = -1;
    vin[i].value = '?';


    int svm_type=svm_get_svm_type(model);
    int nr_class=svm_get_nr_class(model);

    double predict_label;
    double *prob_estimates=NULL;
    prob_estimates = (double *) malloc(nr_class*sizeof(double));

    if (predict_probability && (svm_type==C_SVC || svm_type==NU_SVC))
    {
        predict_label = svm_predict_probability(model,vin,prob_estimates);
        std::cout << predict_label;

        for(int j=0;j<nr_class;j++)
            std::cout << prob_estimates[j] << " ";
        std::cout << std::endl;
    }
    else
    {
        predict_label = svm_predict(model,vin);
        std::cout << predict_label << std::endl;
    }


    if(vin)
        free(vin);

    if(predict_probability)
        free(prob_estimates);

    return predict_label;
}


// import the model to a model file
int MLSVM::loadModelFromFile(std::string filename)
{
    if((model=svm_load_model(filename.c_str()))==0)
    {
        std::cout << "can't open model file " << filename.c_str();
        return 0;
    }

    x = (struct svm_node *) malloc(max_nr_attr*sizeof(struct svm_node));
    if(predict_probability)
    {
        if(svm_check_probability_model(model)==0)
        {
            std::cout << "Model does not support probabiliy estimates";
            return 0;
        }
    }
    else
    {
        if(svm_check_probability_model(model)!=0)
            std::cout << "Model supports probability estimates, but disabled in prediction.";
    }

    return 1;
}

#endif


#ifdef USE_OPENCV_SVM

MLSVM::MLSVM(boost::shared_ptr<vml::Settings> settings)
{

    pSVM = cv::ml::SVM::create();
    setParams(settings);
}

void MLSVM::setParams(boost::shared_ptr<vml::Settings> settings)
{
    pSVM->setC(settings->getDouble("Classifier/SVM/C", 8.0, false));
    pSVM->setGamma(settings->getDouble("Classifier/SVM/gamma", 0.03125, false));

    pSVM->setTermCriteria(cv::TermCriteria(cv::TermCriteria::MAX_ITER, 1000, 1e-6));

    // Need to load the setup parameters from settings
    std::string typeStr = settings->getString("Classifier/SVM/type");

    if(typeStr.c_str() == "C_SVC")
    {
        pSVM->setType(cv::ml::SVM::C_SVC);
    }

    std::string kernelStr = settings->getString("Classifier/SVM/kernel");
    if(kernelStr.c_str() == "LINEAR")
    {
        pSVM->setKernel(cv::ml::SVM::LINEAR);
    }
    else if(kernelStr.c_str() == "RBF")
    {
        pSVM->setKernel(cv::ml::SVM::RBF);        
    }


}

// Set the file to write the resulting opencv classifier as YAML file
// dataMat should have CV_32F type and labelMat should have CV_32F or CV_32S
void MLSVM::train(cv::Mat trainDataMat, cv::Mat labelDataMat)
{

   // Train the SVM
    std::cout << "Start training...";

   pSVM->train(trainDataMat, cv::ml::ROW_SAMPLE, labelDataMat);

   // Auto tunning
   //cv::Mat varIdx = cv::Mat::zeros(trainDataMat.cols,1, CV_32FC1);
   //cv::Mat sampleIdx = cv::Mat::zeros(trainDataMat.rows,1, CV_32FC1);

   //cv::Ptr<cv::ml::TrainData> mlData = cv::ml::TrainData::create(trainDataMat, cv::ml::ROW_SAMPLE, labelDataMat);

  // pSVM->trainAuto(mlData, 5);   // k-fold parameter is sensitive to make a memory issue in the do_train function
   //pSVM->trainAuto(mlData, 5, cv::ml::SVM::getDefaultGrid(cv::ml::SVM::C), cv::ml::SVM::getDefaultGrid(cv::ml::SVM::GAMMA), cv::ml::SVM::getDefaultGrid(cv::ml::SVM::P),
   //            cv::ml::SVM::getDefaultGrid(cv::ml::SVM::NU), cv::ml::SVM::getDefaultGrid(cv::ml::SVM::COEF), cv::ml::SVM::getDefaultGrid(cv::ml::SVM::DEGREE), false);

   std::cout << "...[done]" << std::endl;

}

float MLSVM::predict(cv::Mat testData)
{
        // Train the SVM    
      return pSVM->predict(testData);
}

// export the model to a model file
void MLSVM::saveModelToFile(std::string filename)
{
    pSVM->save(filename);
}


// import the model to a model file
void MLSVM::loadModelFromFile(std::string filename)
{
    pSVM = cv::ml::StatModel::load<cv::ml::SVM>(filename);

}
#endif

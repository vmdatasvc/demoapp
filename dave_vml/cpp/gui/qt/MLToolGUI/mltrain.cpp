#include "mltrain.h"
#include <dirent.h>

//using namespace std;

MLTrain::MLTrain( boost::shared_ptr<vml::Settings> settings)
{
    FeatMode   selected_feature;
    ColMode    selected_color;

    std::string fmodeStr=  settings->getString("Feature/fmode");
    std::string cmodeStr=   settings->getString("Feature/cmode");

    if(fmodeStr == "FM_HOG")
    {
        selected_feature = FM_HOG;
    }
    else if(fmodeStr == "FM_COLOR")
    {
        selected_feature = FM_COLOR;
    }

    if(cmodeStr == "CM_YCrCb")
    {
        selected_color = CM_YCrCb;
    }
    if(cmodeStr == "CM_HSV")
    {
        selected_color = CM_HSV;
    }
    if(cmodeStr == "CM_RGB")
    {
        selected_color = CM_RGB;
    }

    normImgSize.width = settings->getInt("Feature/normImgWidth");
    normImgSize.height = settings->getInt("Feature/normImgHeight");

    pMLFeature.reset(new vml::MLFeature(settings));
    pMLSVM.reset(new vml::MLSVM(settings));



}

// Loading the images files with their own classe and create mat files for using OpenCV SVM 
// For the cases of DATA_FORMAT_OPENCV
int MLTrain::extFeaturesMatFromPath(std::string &samplesDir, cv::Mat &dataMat, cv::Mat &labelMat)
{   

   // Get the files to train from somewhere
    std::vector<std::string> classDirs;
    std::vector<std::string> trainingImages;
    std::vector<std::string> validExtensions;
    validExtensions.push_back("jpg");
    validExtensions.push_back("png");
    validExtensions.push_back("ppm");

    getSubDirsInDirectory(samplesDir,classDirs);
    unsigned int numClasses  = classDirs.size();

    std::string slash("/");

    float percent;
    unsigned long num_samplesInClass[numClasses], dim_features;
    unsigned long num_samples = 0;

    for(unsigned int i=0; i< numClasses; i++)
    {
        std::string dirName  = samplesDir + slash + classDirs[i] + slash;

        trainingImages.clear();
        getFilesInDirectory(dirName, trainingImages, validExtensions);

        num_samplesInClass[i] = trainingImages.size();

        num_samples += num_samplesInClass[i];

        if(i==0)
        {
            std::vector<float> featureVector;
            const std::string currentImageFile = trainingImages.at(0);
            // Calculate feature vector from current image file
            cv::Mat orgData = cv::imread(currentImageFile);
            cv::Mat inData;

            // size normalization to determine the dimensionality of data space
            cv::resize(orgData, inData, normImgSize);

            if(inData.empty())
            {
                std::cout << "Error: the input image is empty!\n";
                return EXIT_FAILURE;
            }

            pMLFeature->featData.clear();
            pMLFeature->extFeature(inData);
            dim_features = pMLFeature->featData.size();
        }

    }


	// setup mat file 
    dataMat = cv::Mat(num_samples, dim_features, CV_32FC1);     // data type:  float
    labelMat = cv::Mat(num_samples, 1, CV_32SC1);               // label type: singed interger


    // setup labelMat
    labelMat.rowRange(0, num_samplesInClass[0]).setTo(std::atoi(classDirs[0].c_str()));
    for(unsigned int i=0; i< numClasses-1; i++)
    {
        labelMat.rowRange(num_samplesInClass[i], num_samplesInClass[i+1]).setTo(std::atoi(classDirs[i+1].c_str()));
    }


    // setup dataMat

    unsigned long cntSample = 0;
    // a variable of classes, i

    for(unsigned int i=0; i< numClasses; i++)
    {
        std::string dirName  = samplesDir + slash + classDirs[i] + slash;

        // class number is followed by the name of each class directory (eg. (-1, 1 ) for binary classifier, (1,2,3) for multi-classes classifier)

        trainingImages.clear();
        getFilesInDirectory(dirName, trainingImages, validExtensions);

        /// Retrieve the descriptor vectors from the samples
        unsigned long numSamples = trainingImages.size();

        if (numSamples == 0) {
            printf("No training sample files found, nothing to do!\n");
            return EXIT_SUCCESS;
        }

        // a variable of image files, m
        for (unsigned long m = 0;  m < num_samplesInClass[i]; ++m)
        {
            storeCursor();

            const std::string currentImageFile = trainingImages.at(m);


            // Calculate feature vector from current image file
            cv::Mat orgData = cv::imread(currentImageFile);
            cv::Mat inData;

            // size normalization to determine the dimensionality of data space
            cv::resize(orgData, inData, normImgSize);

            if(inData.empty())
            {
                std::cout << "Error: the input image is empty!\n";
            }

            pMLFeature->featData.clear();
            pMLFeature->extFeature(inData);


            if (!pMLFeature->featData.empty())
            {

                // Save feature vector components:  a variable of features for the m-th image, k
                dataMat.row(cntSample) = cv::Mat(pMLFeature->featData);
            }


            // Output progress
            percent = ((cntSample+1) * 100 / num_samples);
            printf("%5lu (%3.0f%%):\tFile '%s'\n", (cntSample+1), percent, currentImageFile.c_str());
            std::fflush(stdout);
            //resetCursor();

            cntSample++;
        }

    }

    //pSVM->setKernel(cv::ml::SVM::RBF);
    printf("Feature dimensionality = %5lu \n", dim_features);
    std::fflush(stdout);
    resetCursor();

    return EXIT_SUCCESS;

}



// return EXIT_SUCCESS (0) or EXIT_FAILURE (1)
// For the cases of DATA_FORMAT_LIBSVM or DATA_FORMAT_TORCH3
int MLTrain::extFeaturesFromPath(std::string &samplesDir, std::string &featuresFile, int mode)
{   

    // Get the files to train from somewhere


    std::vector<std::string> classDirs;
    std::vector<std::string> trainingImages;
    std::vector<std::string> validExtensions;
    validExtensions.push_back("jpg");
    validExtensions.push_back("png");
    validExtensions.push_back("ppm");

    getSubDirsInDirectory(samplesDir,classDirs);
    unsigned int numClasses  = classDirs.size();

    std::string slash("/");

    std::fstream File;
    float percent;

    File.open(featuresFile.c_str(), std::ios::out);

    if (File.good() && File.is_open())
    {
		// a variable of classes, i
        for(unsigned int i=0; i< numClasses; i++)
        {

            std::string dirName  = samplesDir + slash + classDirs[i] + slash;

            // class number is followed by the name of each class directory (eg. (-1, 1 ) for binary classifier, (1,2,3) for multi-classes classifier)
            trainingImages.clear();
            getFilesInDirectory(dirName, trainingImages, validExtensions);

            /// Retrieve the descriptor vectors from the samples
            unsigned long numSamples = trainingImages.size();

            if (numSamples == 0) {
                printf("No training sample files found, nothing to do!\n");
                return EXIT_SUCCESS;
            }

			// a variable of image files, m
            for (unsigned long m = 0; m < numSamples; ++m)
            {
                storeCursor();

                const std::string currentImageFile = trainingImages.at(m);

                // Output progress
                if ( (m+1) % 10 == 0 || (m+1) == numSamples )
                {
                    percent = ((m+1) * 100 / numSamples);
                    printf("%5lu (%3.0f%%):\tFile '%s'", (m+1), percent, currentImageFile.c_str());
                    std::fflush(stdout);
                    resetCursor();
                }
                // Calculate feature vector from current image file
                cv::Mat orgData = cv::imread(currentImageFile);
                cv::Mat inData;
                cv::resize(orgData, inData, normImgSize);

                if(inData.empty())
                {                   
                    std::cout << "Error: the input image is empty!\n";
                }
                pMLFeature->featData.clear();
                pMLFeature->extFeature(inData);


                if (!pMLFeature->featData.empty())
                {
                    if(mode == DATA_FORMAT_LIBSVM)
                    {
                        // File format for libSVM
                        File << classDirs[i];

                        // Save feature vector components: a variable of features for the m-th image, k
                        for (unsigned int k = 0; k < pMLFeature->featData.size(); ++k)
                        {
                            File << " " << (k + 1) << ":" << pMLFeature->featData.at(k);
                        }
                        File << std::endl;
                    }
                    else if(mode == DATA_FORMAT_TORCH3)
                    {
                        // File format for TORCH3
                        // Save feature vector components: a variable of features for the m-th image, k
                       for (unsigned int k = 0; k < pMLFeature->featData.size(); ++k)
                        {
                            File << pMLFeature->featData.at(k) << " ";
                        }
                        File << classDirs[i] << std::endl;
                    }
                    else
                    {
                         std::cout << "Error: please select correct feature data format as the output!\n";

                    }


                }
            }
        }

    }
    File.close();

    return EXIT_SUCCESS;

}

// Load image files for multi classes
// Build a svm model using training vocabulary (multi-classes)
// Create a train csv file using feature vectors obtained from train images (For openCV)
// Create a train dat file using using feature vectors obtained from train images (For LibSVM)


std::string MLTrain::toLowerCase(const std::string& in)
{
    std::string t;
    for (std::string::const_iterator i = in.begin(); i != in.end(); ++i)
    {
        t += tolower(*i);
    }
    return t;
}


void MLTrain::storeCursor(void) {
    printf("\033[s");
}

void MLTrain::resetCursor(void) {
    printf("\033[u");
}

void MLTrain::getSubDirsInDirectory(const std::string& dirName, std::vector<std::string>& subDirNames)
{
    printf("Opening directory %s\n", dirName.c_str());

    struct dirent* ep;
    DIR* dp = opendir(dirName.c_str());
    char cfilter[10] = ".";
    if (dp != NULL)
    {
        while((ep=readdir(dp)))
        {
            if (ep->d_type & DT_DIR)
            {
                if( ep->d_name[0] != '.')
                {

                    subDirNames.push_back(ep->d_name);
                }
            }
        }
        (void) closedir(dp);
    }
    else
    {
        printf("Error opening directory '%s'!\n", dirName.c_str());
    }
    return;

}


void MLTrain::getFilesInDirectory(const std::string& dirName, std::vector<std::string>& fileNames, const std::vector<std::string>& validExtensions)
{
    printf("Opening directory %s\n", dirName.c_str());

#ifdef __MINGW32__
    struct stat s;
#endif
    struct dirent* ep;
    std::size_t extensionLocation;
    DIR* dp = opendir(dirName.c_str());
    if (dp != NULL) {
        while ((ep = readdir(dp))) {
            // Ignore (sub-)directories like . , .. , .svn, etc.
#ifdef __MINGW32__
            stat(ep->d_name, &s);
            if (s.st_mode & S_IFDIR) {
                continue;
            }
#else
            if (ep->d_type & DT_DIR) {
                continue;
            }
#endif
            extensionLocation = std::string(ep->d_name).find_last_of("."); // Assume the last point marks beginning of extension like file.ext
            // Check if extension is matching the wanted ones
            std::string tempExt = toLowerCase(std::string(ep->d_name).substr(extensionLocation + 1));
            if (find(validExtensions.begin(), validExtensions.end(), tempExt) != validExtensions.end()) {
                printf("Found matching data file '%s'\n", ep->d_name);
                fileNames.push_back((std::string) dirName + ep->d_name);
            } else {
                printf("Found file does not match required file type, skipping: '%s'\n", ep->d_name);
            }
        }
        (void) closedir(dp);
    } else {
        printf("Error opening directory '%s'!\n", dirName.c_str());
    }
    return;
}

void MLTrain::saveDescriptorVectorToFile(std::vector<float>& descriptorVector, std::vector<unsigned int>& _vectorIndices, std::string fileName) {
    printf("Saving descriptor vector to file '%s'\n", fileName.c_str());
    std::string separator = " "; // Use blank as default separator between single features
    std::fstream File;
    float percent;
    File.open(fileName.c_str(), std::ios::out);
    if (File.good() && File.is_open()) {
        printf("Saving %lu descriptor vector features:\t", descriptorVector.size());
        storeCursor();
        for (int feature = 0; feature < descriptorVector.size(); ++feature) {
            if ((feature % 10 == 0) || (feature == (descriptorVector.size()-1)) ) {
                percent = ((1 + feature) * 100 / descriptorVector.size());
                printf("%4u (%3.0f%%)", feature, percent);
                fflush(stdout);
                resetCursor();
            }
            File << descriptorVector.at(feature) << separator;
        }
        printf("\n");
        File << std::endl;
        File.flush();
        File.close();
    }
}

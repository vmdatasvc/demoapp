#ifndef MLTRAIN_H
#define MLTRAIN_H

#include <core/Settings.hpp>
#include "boost/shared_ptr.hpp"
#include <opencv2/core/core.hpp>
#include <modules/ml/mlfeature.h>
#include <modules/ml/mlsvm.h>
#include <fstream>


#define DATA_FORMAT_OPENCV 0
#define DATA_FORMAT_LIBSVM 1
#define DATA_FORMAT_TORCH3 2

class MLTrain
{
public:

    MLTrain( boost::shared_ptr<vml::Settings> settings);

    std::string toLowerCase(const std::string& in);
    void storeCursor(void);
    void resetCursor(void);

    void getSubDirsInDirectory(const std::string& dirName, std::vector<std::string>& subDirNames);
    void getFilesInDirectory(const std::string& dirName, std::vector<std::string>& fileNames, const std::vector<std::string>& validExtensions);
    void saveDescriptorVectorToFile(std::vector<float>& descriptorVector, std::vector<unsigned int>& _vectorIndices, std::string fileName);

    int extFeaturesFromPath(std::string &samplesDir, std::string &featuresFile, int mode);
    int extFeaturesMatFromPath(std::string &samplesDir, cv::Mat &dataMat, cv::Mat &labelMat);

    boost::shared_ptr<vml::MLFeature>   pMLFeature;
    boost::shared_ptr<vml::MLSVM>       pMLSVM;


protected:
    cv::Size normImgSize;


};

#endif // MLUTILS_H

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "core/Settings.hpp"
#include "modules/ml/Recognition.hpp"
#include "modules/ml/AITPort/Settings.hpp"
#include "modules/ml/AITPort/FaceDetectorAdaBoost.hpp"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    init();
    timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()), this, SLOT(updateMethod()));

}

MainWindow::~MainWindow()
{
    delete ui;

    delete timer;
}

void MainWindow::init()
{

    // clean viewer
    ui->openCVViewer1->clearImage(); // this function is changed by Dave, but still have a problem.
    ui->openCVViewer2->clearImage(); // this function is changed by Dave, but still have a problem.

    // initial selection of a radio button
    ui->ImageInputRadioButton->setChecked(true);
    ui->VideoInputRadioButton->setChecked(false);
    ui->WebCamInputRadioButton->setChecked(false);

}


void MainWindow::timerEvent(QTimerEvent *event)
{
    //cv::Mat image, dispImg;
    //mCapture >> image;

    // Show the image
    //cv::cvtColor(image,dispImg, CV_BGR2RGB);
    //ui->openCVViewer1->showImage(dispImg);
}


void MainWindow::updateMethod()
{

    cv::Mat image, dispImg;
    mCapture >> image;

    // Show the image
    cv::cvtColor(image,dispImg, CV_BGR2RGB);
    ui->openCVViewer1->showImage(dispImg);


}


void MainWindow::on_OpenFilePushButton_clicked()
{
    QString tmpString = QFileDialog::getOpenFileName(this, "Open File");

    if( !tmpString.trimmed().isEmpty())
    {
        ui->OpenFileLineEdit->setText(tmpString);
    }
}

void MainWindow::on_StartPushButton_toggled(bool checked)
{
    if(checked)
    {
        // change StartButton text to Stop
        ui->StartPushButton->setText("Stop");
        // disable ui
        ui->OpenFileLineEdit->setEnabled(false);
        ui->VidIntervalSpinBox->setEnabled(false);
        ui->OpenFilePushButton->setEnabled(false);
        // enable ui
        ui->PausePushButton->setEnabled(true);

        if (ui->ImageInputRadioButton->isChecked())
        {
            cv::Mat image, dispImg;

            image = cv::imread(ui->OpenFileLineEdit->text().toStdString());

            // Show the image
            cv::cvtColor(image,dispImg, CV_BGR2RGB);
            ui->openCVViewer1->showImage(dispImg);

            image.copyTo(gImgMat);

        }
        else
        {
            if (ui->VideoInputRadioButton->isChecked())
            {
                mCapture.open(ui->OpenFileLineEdit->text().toStdString());
            }
            else if (ui->WebCamInputRadioButton->isChecked())
            {
                mCapture.open(0);

            }

            if( !mCapture.isOpened() )
            {
                QMessageBox msg;
                msg.setText("Error : Can't open video source!");
                return;
            }

            timer->start(ui->VidIntervalSpinBox->value());
            ui->Viewer1Label->setText("Input Image");

        }
    }
    else  // uncheced
    {
        ui->StartPushButton->setText("Start");
        // enable ui
        ui->OpenFileLineEdit->setEnabled(true);
        ui->VidIntervalSpinBox->setEnabled(true);
        ui->OpenFilePushButton->setEnabled(true);
        // disable ui
        ui->PausePushButton->setEnabled(false);

        ui->Viewer1Label->setText("");

        // stop all timer
        timer->stop();

        // clear viewers#include <face_detector/FaceDetectorAdaBoost.hpp>
        ui->openCVViewer1->clearImage();

        // stop video capture
        if (mCapture.isOpened())
            mCapture.release();

    }
}

void MainWindow::on_PausePushButton_toggled(bool checked)
{
    if(checked)
    {
        // chage capture to Resume
        ui->PausePushButton->setText("Resume");
        ui->VidIntervalSpinBox->setEnabled(true);

        // stop the timers
        timer->stop();


    }
    else
    {
        // chage caption to pausimge
        ui->PausePushButton->setText("Pause");
        ui->VidIntervalSpinBox->setEnabled(false);

        // restart the timers
        timer->start(ui->VidIntervalSpinBox->value());
    }

}


void MainWindow::cvtMatToImage32(cv::Mat imat, aitvml::Image32 *img)
{
       int w = imat.cols;
       int h = imat.rows;

       (*img) = aitvml::Image32(w,h);
/*
       cv::Mat tmpMat =  cv::Mat(h,w,CV_8UC4);


       if(img->getInternalFormat() == 0)
       {
           cv::cvtColor(imat,tmpMat, CV_RGB2BGRA);
       }
       else
       {
           cv::cvtColor(imat,tmpMat, CV_RGB2RGBA);
       }


       for(int y=0; y<h; y++)
       {
           unsigned char *pSrcData = ((unsigned char *)tmpMat.data) + y*w*4;
           unsigned char *pDstData = ((unsigned char *)img->pointer()) + y*w*4;

           for(int x=0; x<w; x++)
           {
                memcpy(pSrcData, pDstData, sizeof(unsigned char)*4);
           }
       }


//       memcpy((unsigned char *)tmpMat.data, (unsigned char *)img->pointer(), sizeof(unsigned char)*w*h*4);


*/
       for(int y=0; y<h; y++)
       {
           unsigned char *pSrcData = ((unsigned char *)imat.data) + y*w*3;
           unsigned char *pDstData = ((unsigned char *)img->pointer()) + y*w*4;

           for(int x=0; x<w; x++)
           {
               if(img->getInternalFormat() == 0)
               {
                    pDstData[0] = pSrcData[2];
                    pDstData[1] = pSrcData[1];
                    pDstData[2] = pSrcData[0];
               }
               else
               {
                   pDstData[0] = pSrcData[0];
                   pDstData[1] = pSrcData[1];
                   pDstData[2] = pSrcData[2];
               }
               pSrcData += 3;
               pDstData += 4;
            }
       }


}

void MainWindow::cvtImage32ToMat(aitvml::Image32 img, cv::Mat *imat)
{

    int w = img.width();
    int h = img.height();


    cv::Mat tmpMat =  cv::Mat(h,w,CV_8UC4, (void*)img.pointer());

    if(img.getInternalFormat() == 0)
    {
        cv::cvtColor(tmpMat, (*imat), CV_BGRA2RGB);
    }
    else
    {
        cv::cvtColor(tmpMat, (*imat), CV_BGRA2BGR);
    }


/*
    (*imat) = cv::Mat(h,w,CV_8UC3);

    for(int y=0; y<h; y++)
    {
        unsigned char *pSrcData = (unsigned char *)img.pointer() + y*w*4;
        unsigned char *pDstData = (unsigned char *)imat->data + y*w*3;

        for(int x=0; x<w; x++)
        {
            if(img.getInternalFormat() == 0)
            {
                 pDstData[0] = pSrcData[2];
                 pDstData[1] = pSrcData[1];
                 pDstData[2] = pSrcData[0];
            }
            else
            {
                pDstData[0] = pSrcData[0];
                pDstData[1] = pSrcData[1];
                pDstData[2] = pSrcData[2];
            }
            pSrcData += 4;
            pDstData += 3;
         }
    }
*/

}

void MainWindow::cvtMatToImage8(cv::Mat imat, aitvml::Image8 *img)
{
    int w = imat.cols;
    int h = imat.rows;

    (*img) = aitvml::Image8(w,h);


    for(int y=0; y<h; y++)
    {
        unsigned char *pSrcData = ((unsigned char *)imat.data) + y*w;
        unsigned char *pDstData = ((unsigned char *)img->pointer()) + y*w;

        for(int x=0; x<w; x++)
        {
            pDstData[0] = pSrcData[0];
            pSrcData ++;
            pDstData ++;
         }
    }


}

void MainWindow::cvtImage8ToMat(aitvml::Image8 img, cv::Mat *imat)
{

    int w = img.width();
    int h = img.height();

    (*imat) = cv::Mat(h,w,CV_8UC1);

    for(int y=0; y<h; y++)
    {
        unsigned char *pSrcData = (unsigned char *)img.pointer() + y*w;
        unsigned char *pDstData = (unsigned char *)imat->data + y*w;

        for(int x=0; x<w; x++)
        {
            pDstData[0] = pSrcData[0];
            pSrcData ++;
            pDstData ++;
         }
    }

}


void MainWindow::on_pushButton_clicked()
{
    //testImageConversion();

   //testReadXML();

   //testFaceDetection();

   //testDemographicsRecognition();

    testHOGRepresentation();

}


//----------------- Test functions-------------------------------------------//
void MainWindow::testHOGRepresentation()
{
    cv::Mat image, disp;

    image = cv::imread(ui->OpenFileLineEdit->text().toStdString());

    if(ui->FeatureHOGRadioButton->isEnabled())
    {
        if(mpSettings)
        {
            pfeature.reset(new vml::MLFeature(mpSettings));
        }
        else
        {
            QMessageBox::critical(this,tr("Default Settings Not Set"),tr("Default Settings hasn't been set yet. Please click Load button to load default settings"));
            return;
        }

    }
    else
    {
        QMessageBox::critical(this,tr("HOG stting Not Set"),tr("Please click HOG feature settings correctly"));
        return;
    }

    std::vector<float> featureVec;
    cv::Size wSize(pfeature->winsize,pfeature->winsize), cSize(pfeature->cellsize,pfeature->cellsize);
    int scaleFactor=1;
    double viz_factor=1.0;

    cv::Mat nimage, nimageRGB,dispRGB;

    cv::resize(image,nimage,wSize);

    pfeature->extFeature(nimage);
    disp = pfeature->get_hogdescriptor_visual_image(nimage,pfeature->featData,wSize, cSize,pfeature->nbins, scaleFactor, viz_factor);

    cv::cvtColor(nimage,nimageRGB, CV_BGR2RGB);
    cv::cvtColor(disp,dispRGB, CV_BGR2RGB);
    ui->openCVViewer1->showImage(nimageRGB);
    ui->openCVViewer2->showImage(dispRGB);

}

void MainWindow::testImageConversion()
{
    // Color image conversion
    aitvml::Image32 inImg;
    cv::Mat tmpMat, disptmpMat;

    cvtMatToImage32(gImgMat, &inImg);
    cvtImage32ToMat(inImg, &tmpMat);

    cv::cvtColor(tmpMat, disptmpMat, CV_BGR2RGB);
    ui->openCVViewer1->showImage(disptmpMat);

    // Gray-scale image conversion
    aitvml::Image8  ginImg;
    cv::Mat grayMat(gImgMat.rows, gImgMat.cols, CV_8UC1), grayMatOut;

    cv::cvtColor(gImgMat, grayMat, CV_BGR2GRAY);
    cvtMatToImage8(grayMat, &ginImg );
    cvtImage8ToMat(ginImg, &grayMatOut );

    ui->openCVViewer2->showImage(grayMatOut);


}

void MainWindow::testReadXML()
{

    aitvml::Settings mSet;
    mSet.parseXml("/home/dkim/VM/vml/cpp/settings/vml_modules/FaceTracker.xml");
//    mSet.parseXml("/home/dkim/VM/vml/cpp/settings/vml_modules/CameraModelOpenCV.xml");
    //aitvml::Settings mSet("/home/dkim/VM/vml/cpp/settings/vml_modules/DemographicsRecognition.xml");

    int val = mSet.getInt("FaceDetection/minFaceSize", 1, false);
//    float val = mSet.getFloat("CameraModelOpenCV/cameraHeight",0.0,false);

    qDebug() << "output = " << val;

}

void MainWindow::testFaceDetection()
{
    faceDetectionByVML();
    //faceDetectionByDirectOpenCV();

}

void MainWindow::faceDetectionByVML()
{

    int NumDetectedFace;
    aitvml::Image32 frame;
    aitvml::Image8  grayFrame;

    // Color to gray
    cv::Mat grayImgMat(gImgMat.rows, gImgMat.cols, CV_8UC1);
    cv::cvtColor(gImgMat, grayImgMat, CV_BGR2GRAY);
    //equalizeHist(grayImgMat, grayImgMat);
    //cvtMatToImage8(grayImgMat, &grayFrame);

    cvtMatToImage32(gImgMat, &frame);
    aitvml::PvImageConverter::convert(frame,grayFrame);


    // Load the cascaed xml for face detection
    aitvml::Settings mSet;
    mSet.parseXml("/home/dkim/VM/vml/cpp/settings/vml_modules/FaceTracker.xml");
    std::string fdfile = mSet.getString("FaceDetection/cascadeFile"," ",false);
    qDebug() << "Cascade file for Face Detection: " << fdfile.c_str();


    // Use of vmlml (With the class of aitvml::vision::FaceDetectorAdaBoost)
    aitvml::vision::FaceDetectorAdaBoost fd;
    fd.initialize();
    fd.init(fdfile);
    fd.detect(grayFrame);
    NumDetectedFace = fd.nrOfFaces();

     // Display rectangles of detected faces
    cv::Mat outMat,dispOutMat;
    gImgMat.copyTo(outMat);

    aitvml::Face lface[10];
    for (int i = 0; i < NumDetectedFace; i++)
    {
        lface[i] = fd.face(i);
        cv::Rect frect(lface[i].x,lface[i].y, lface[i].w, lface[i].h);
        cv::rectangle(outMat,frect, CV_RGB(0,255,0),1, 8, 0);

    }

    ui->openCVViewer2->clearImage();
    cv::cvtColor(outMat, dispOutMat, CV_BGR2RGB );
    ui->openCVViewer2->showImage(dispOutMat);

    qDebug() << "vmlml Face# = " << NumDetectedFace;


}

void MainWindow::faceDetectionByDirectOpenCV()
{
    int NumDetectedFace ;
    cv::Mat grayImgMat(gImgMat.rows, gImgMat.cols, CV_8UC1);
    cv::cvtColor(gImgMat, grayImgMat, CV_BGR2GRAY);
    equalizeHist(grayImgMat, grayImgMat);

    // Direct use of OpenCV
    cv::CascadeClassifier face_cascade;
    std::vector<cv::Rect> faces;

    face_cascade.load("/home/dkim/VM/vml/data/ml/opencv/haarcascades/haarcascade_frontalface_alt2.xml");
    face_cascade.detectMultiScale(grayImgMat, faces, 1.2, 2, CV_HAAR_FIND_BIGGEST_OBJECT | CV_HAAR_SCALE_IMAGE, cv::Size(30, 30));
    NumDetectedFace = faces.size();

    qDebug() << "directOpenCV Face# =" << NumDetectedFace;


    // Display rectangles of detected faces
    cv::Mat outMat, dispOutMat;
    gImgMat.copyTo(outMat);
    for (int i = 0; i < NumDetectedFace; i++)
    {
        cv::Rect face_i = faces[i];
        cv::rectangle(outMat, face_i, CV_RGB(255, 0,0), 1);
        int pos_x = std::max(face_i.tl().x - 10, 0);
        int pos_y = std::max(face_i.tl().y - 10, 0);
        string box_text = cv::format("(G,A,R)");
        cv::putText(outMat,box_text, cv::Point(pos_x, pos_y), 1, 1.0, CV_RGB(0,255,0), 1.0);
    }
    ui->openCVViewer2->clearImage();
    cv::cvtColor(outMat, dispOutMat, CV_BGR2RGB );
    ui->openCVViewer2->showImage(dispOutMat);

}


void MainWindow::testDemographicsRecognition()
{

    int NumDetectedFace ;
    cv::Mat grayImgMat(gImgMat.rows, gImgMat.cols, CV_8UC1);
    cv::cvtColor(gImgMat, grayImgMat, CV_BGR2GRAY);
    equalizeHist(grayImgMat, grayImgMat);

    // Direct use of OpenCV
    cv::CascadeClassifier face_cascade;
    std::vector<cv::Rect> faces;

    face_cascade.load("/home/dkim/VM/vml/data/ml/opencv/haarcascades/haarcascade_frontalface_alt2.xml");
    face_cascade.detectMultiScale(grayImgMat, faces, 1.2, 2, CV_HAAR_FIND_BIGGEST_OBJECT | CV_HAAR_SCALE_IMAGE, cv::Size(30, 30));
    NumDetectedFace = faces.size();

    qDebug() << "directOpenCV Face# =" << NumDetectedFace;

    // Initialization

    boost::shared_ptr<vml::Settings> mpSettings;
    mpSettings.reset(new vml::Settings);
    mpSettings->parseXml("/home/dkim/VM/vml/cpp/settings/vml_modules/DMGRecognition.xml");

    boost::shared_ptr<aitvml::Recognition>  	oRecog;
    //aitvml::Recognition oRecog;
    oRecog.reset(new aitvml::Recognition(mpSettings));

    oRecog->initFromFace();
    qDebug() << "Initialized!";

    // Display rectangles of detected faces
    cv::Mat outMat, dispOutMat;
    gImgMat.copyTo(outMat);
    for (int i = 0; i < NumDetectedFace; i++)
    {
        cv::Rect cropBox = faces[i];
        cv::rectangle(outMat, cropBox, CV_RGB(255, 0,0), 1);
        //int pos_x = std::max(cropBox.tl().x - 10, 0);
        //int pos_y = std::max(cropBox.tl().y - 10, 0);
        //string box_text = cv::format("(G,A,R)");
        //cv::putText(outMat,box_text, cv::Point(pos_x, pos_y), 1, 1.0, CV_RGB(0,255,0), 1.0);


        // Demographics Recognition
        cv::Mat cropped = grayImgMat(cropBox);
        aitvml::Image32 inImg;

        cvtMatToImage32(cropped, &inImg);
        oRecog->faceImage = inImg;

         oRecog->processFromFace();


         std::ostringstream gender_str, age_str, eth_str;
         cv::Point gender_pos(cropBox.x + cropBox.width + 5, cropBox.y -5);
         gender_str << oRecog->mEstGenderString;
         cv::putText(outMat, gender_str.str(), gender_pos, 1, 0.8f, cv::Scalar(255,0,0));

         cv::Point age_pos(cropBox.x + cropBox.width + 5, cropBox.y + std::floor(cropBox.height/2) );
         age_str << oRecog->mEstAgeString;
         cv::putText(outMat, age_str.str(), age_pos, 1, 0.8f, cv::Scalar(0,255,255));

         cv::Point eth_pos(cropBox.x + cropBox.width + 5 , cropBox.y + cropBox.height + 5);
         eth_str << oRecog->mEstEthnicityString;
         cv::putText(outMat, eth_str.str(), eth_pos, 1, 0.8f, cv::Scalar(255,200,255));

    }

    ui->openCVViewer2->clearImage();
    cv::cvtColor(outMat, dispOutMat, CV_BGR2RGB );
    ui->openCVViewer2->showImage(dispOutMat);

    cv::Mat dispImg;
    cv::cvtColor(gImgMat, dispImg, CV_BGR2RGB );
    ui->openCVViewer1->showImage(dispImg);

}


/*
void MainWindow::testDemographicsRecognition()
{

    aitvml::Recognition oRecog;

    // Need to set up Image or video information before using Recognition class
    oRecog.gVideoWidth = gImgMat.cols;
    oRecog.gVideoHeight = gImgMat.rows;
    oRecog.gVideoFPS = 0;

    // Initialization
    oRecog.initFromFace();

    qDebug() << "Initialized!";


    oRecog.faceImage = aitvml::Image32(gImgMat.cols, gImgMat.rows);


    if(1)
    {
        cvtMatToImage32(gImgMat, &(oRecog.faceImage));
    }
    else
    {
        aitvml::Image32 inImg;

        cvtMatToImage32(gImgMat, &inImg);
        cv::Mat tmpMat;

        cvtImage32ToMat(inImg, &tmpMat);
        ui->openCVViewer2->showImage(tmpMat);

        oRecog.faceImage = inImg;
    }
    // Process with the selected frame as an input


    oRecog.processFromFace();


    // Visualize the gray-scale image
    cv::Mat tmpMat, dispMat;

    //cvtImage8ToMat(oRecog.grayFrame, &tmpMat);
    cvtImage32ToMat(oRecog.faceDetectedImage, &tmpMat);


    qDebug() << "# of detected faces" << oRecog.gNumDetectedFace;


    // Visualize Face detection result

    gImgMat.copyTo(tmpMat);
    for (int i = 0; i < oRecog.gNumDetectedFace; i++)
    {
        cv::Rect frect(oRecog.mpFront->face(i).x, oRecog.mpFront->face(i).y,oRecog.mpFront->face(i).w, oRecog.mpFront->face(i).h);

        cv::rectangle(tmpMat,frect, CV_RGB(255,i*10,0),1, 8, 0);
    }
    ui->openCVViewer2->clearImage();
    cv::cvtColor(tmpMat,dispMat, CV_BGR2RGB);
    ui->openCVViewer2->showImage(dispMat);


    // Visualize Normalization face


    // Output
    int igender, iage, irace;

    igender = oRecog.mEstGenderLabel;
    iage =  oRecog.mEstAgeLabel;
    irace = oRecog.mEstRaceLabel;

    qDebug("Result:Gender = %d, Age = %d, Race = %d",igender, iage, irace);
}
*/

void MainWindow::on_WebCamInputRadioButton_clicked()
{
    QString lineText = "0";
    ui->OpenFileLineEdit->setText(lineText);
}

void MainWindow::on_toolButton_clicked()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::Directory);
    QString newPath = dialog.getExistingDirectory(this,tr("Training Data Path"), ui->TrainDataPathLineEdit->text(),QFileDialog::ShowDirsOnly);

    // set url
    ui->TrainDataPathLineEdit->setText(newPath);
}

void MainWindow::on_toolButton_2_clicked()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::Directory);
    QString newPath = dialog.getExistingDirectory(this,tr("Training Data Path"), ui->ModelOutputPathLineEdit->text(),QFileDialog::ShowDirsOnly);

    // set url
    ui->ModelOutputPathLineEdit->setText(newPath);
}




void MainWindow::on_SettingsToolButton_clicked()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::Directory);
    QString newPath = dialog.getExistingDirectory(this,tr("Settings Path"), ui->SettingsPathLineEdit->text(),QFileDialog::ShowDirsOnly);

    // set url
    ui->SettingsPathLineEdit->setText(newPath);
}

void MainWindow::on_LoadSettingsPushButton_toggled(bool checked)
{
    if(checked)
    {
        ui->LoadSettingsPushButton->setEnabled(false);

        // create settings and load settings files
        this->mpSettings.reset(new vml::Settings);

        QString settings_path = ui->SettingsPathLineEdit->text();

        // load MLTool.xml
           QString settings_file = settings_path + "/MLTool.xml";
           QFileInfo checkFile(settings_file);
           if ((not checkFile.exists())||(not checkFile.isFile()))
           {
               QMessageBox::critical(this,tr("File not found"),tr("Settings file MLTool.xml not found"));
               this->mpSettings.reset();
               return;
           }
           mpSettings->parseXml(settings_file.toUtf8().constData());

           std::string fm_method = mpSettings->getString("Feature/fmode");

           if(fm_method == "FM_HOG")
           {
               ui->FeatureHOGRadioButton->setChecked(true);
               ui->ColorModeGroupBox->setEnabled(false);
           }
           else if(fm_method == "FM_COLOR")
           {
                ui->FeatureColorRadioButton->setChecked(true);
                ui->ColorModeGroupBox->setEnabled(true);
           }

           std::string cm_method = mpSettings->getString("Feature/cmode");
           if(cm_method == "CM_YCrCb")
           {
               ui->ColorYCrCbRadioButton->setEnabled(true);
           }
           else if(cm_method == "CM_HSV")
           {
               ui->ColorHSVRadioButton->setEnabled(true);
           }
           if(cm_method == "CM_RGB")
           {
               ui->ColorRGBRadioButton->setEnabled(true);
           }

           libType = mpSettings->getInt("Classifier/libType",0, false); // 0-opencv, 1-libsvm, 2-torch3

    }
}

void MainWindow::on_FeatureHOGRadioButton_clicked(bool checked)
{
    if (not mpSettings)
     {
         QMessageBox::critical(this,tr("Default Settings Not Set"),tr("Default Settings hasn't been set yet. Please click Load button to load default settings"));
         return;
     }

     if (checked)
     {
         ui->ColorModeGroupBox->setEnabled(false);
         mpSettings->setString("Feature/fmode","FM_HOG");
     }
}

void MainWindow::on_FeatureColorRadioButton_clicked(bool checked)
{
    if (not mpSettings)
     {
         QMessageBox::critical(this,tr("Default Settings Not Set"),tr("Default Settings hasn't been set yet. Please click Load button to load default settings"));
         return;
     }

     if (checked)
     {
         ui->ColorModeGroupBox->setEnabled(true);
         mpSettings->setString("Feature/fmode","FM_COLOR");
     }
}

void MainWindow::on_ColorYCrCbRadioButton_clicked(bool checked)
{
    if (not mpSettings)
     {
         QMessageBox::critical(this,tr("Default Settings Not Set"),tr("Default Settings hasn't been set yet. Please click Load button to load default settings"));
         return;
     }

     if (checked)
     {
         mpSettings->setString("Feature/cmode","CM_YCrCb");
     }
}

void MainWindow::on_ColorHSVRadioButton_clicked(bool checked)
{
    if (not mpSettings)
     {
         QMessageBox::critical(this,tr("Default Settings Not Set"),tr("Default Settings hasn't been set yet. Please click Load button to load default settings"));
         return;
     }

     if (checked)
     {
         mpSettings->setString("Feature/cmode","CM_HSV");
     }
}

void MainWindow::on_ColorRGBRadioButton_clicked(bool checked)
{
    if (not mpSettings)
     {
         QMessageBox::critical(this,tr("Default Settings Not Set"),tr("Default Settings hasn't been set yet. Please click Load button to load default settings"));
         return;
     }

     if (checked)
     {
         mpSettings->setString("Feature/cmode","CM_RGB");
     }
}

// ----------------- Build a model --------------------//
void MainWindow::on_BuildModelPushButton_clicked()
{
    // initialization
    if(mpSettings)
    {
        pMLTrain.reset(new MLTrain(mpSettings));
    }
    else
    {
        QMessageBox::critical(this,tr("Default Settings Not Set"),tr("Default Settings hasn't been set yet. Please click Load button to load default settings"));
        return;

    }

    ui->BuildModelPushButton->setEnabled(false);

    std::string featureFileName, modelFileName, testFileName, trainDirectory;
    std::string slash = "/";
    //std::string filename = "feature";
    std::string featureExtension = ".fvoc";
    std::string modelExtension = ".yml";

    trainDirectory = ui->TrainDataPathLineEdit->text().toStdString();

    featureFileName = ui->ModelOutputPathLineEdit->text().toStdString() + slash + ui->ModelNameLineEdit->text().toStdString() + featureExtension;

    modelFileName = ui->ModelOutputPathLineEdit->text().toStdString() + slash + ui->ModelNameLineEdit->text().toStdString() + modelExtension;


    // 1. prepare the vocabulary
    if(libType == DATA_FORMAT_LIBSVM | libType == DATA_FORMAT_TORCH3)
    {
        if( pMLTrain->extFeaturesFromPath(trainDirectory,featureFileName, libType))
        {
               qDebug() << "Error on building the vocabulary!";
        }
        else
        {
              qDebug() << "Built the vocabulary successfully!";
        }

        // Save feature data

    }
    else if(libType == DATA_FORMAT_OPENCV)
    {
        cv::Mat dataMat, labelMat;
        if( pMLTrain->extFeaturesMatFromPath(trainDirectory, dataMat, labelMat) )
        {
               qDebug() << "Error on building the vocabulary!";
        }
        else
        {
              qDebug() << "Built the vocabulary successfully!";
        }


        // 2. Training the model file as xxx.model
#ifdef USE_OPENCV_SVM
        pMLTrain->pMLSVM->train(dataMat, labelMat);
        pMLTrain->pMLSVM->saveModelToFile(modelFileName);

        qDebug() << "Built the svm model!";
#endif
    }

    pMLTrain.reset();
    ui->BuildModelPushButton->setEnabled(true);


}

void MainWindow::on_LoadModelPathToolButton_clicked()
{
    QString tmpString = QFileDialog::getOpenFileName(this, "Open File");

    if( !tmpString.trimmed().isEmpty())
    {
        ui->LoadModelLineEdit->setText(tmpString);
    }
}


void MainWindow::on_LoadTestImgPushButton_clicked()
{
    QString tmpString = QFileDialog::getOpenFileName(this, "Open File");

    if( !tmpString.trimmed().isEmpty())
    {
        ui->LoadTestImgLineEdit->setText(tmpString);
    }
}


void MainWindow::on_LoadModelPushButton_toggled(bool checked)
{
    if(checked)
    {
        ui->LoadModelPushButton->setText("Model is Loaded");

        // Loading model
        if(mpSettings)            
        {
            pMLTrain.reset(new MLTrain(mpSettings));
            //pfeature.reset(new MLFeature(mpSettings));
            //psvm.reset(new MLSVM(mpSettings));
        }
        else
        {
            ui->ClassificationPushButton->setChecked(false);
            QMessageBox::critical(this,tr("Default Settings Not Set"),tr("Default Settings hasn't been set yet. Please click Load button to load default settings"));
            return;
        }

        std::string modelFileName;

        modelFileName = ui->LoadModelLineEdit->text().toStdString();

        pMLTrain->pMLSVM->loadModelFromFile(modelFileName);
        //psvm->loadModelFromFile(modelFileName);
        qDebug() << "SVM is loaded!\n";

    }
    else
    {
        ui->LoadModelPushButton->setText("Load a model");

        pfeature.reset();
        psvm.reset();
    }
}

void MainWindow::on_ClassificationPushButton_clicked()
{
    // initialization
    if(not mpSettings)
    {
        QMessageBox::critical(this,tr("Default Settings Not Set"),tr("Default Settings hasn't been set yet. Please click Load button to load default settings"));
        return;

    }

    if(not pMLTrain)
    {
        pMLTrain.reset(new MLTrain(mpSettings));

    }

    std::string testImgFileName;
    testImgFileName = ui->LoadTestImgLineEdit->text().toStdString();

    cv::Mat cImgTest =cv::imread(testImgFileName);

    pMLTrain->pMLFeature->extFeature(cImgTest);
    int featDim = pMLTrain->pMLFeature->featData.size();

    cv::Mat testMat(1, featDim, CV_32FC1, &pMLTrain->pMLFeature->featData.front());  // Row-based data

#ifdef USE_LIBSVM
    double estimate = pMLTrain->pMLSVM->predict(pMLTrain->pMLFeature->featData);
    //double estimate = pMLTrain->pMLSVM->predict(testMat);

#endif

#ifdef USE_OPENCV_SVM

    int varCnt = pMLTrain->pMLSVM->pSVM->getVarCount();
    if(testMat.type() == CV_32F && varCnt == testMat.cols)
    {
        qDebug() << "correct type! testMat!\n";
    }
    else
    {
        qDebug() << "wrong type!!\n";
    }

    float estimate = pMLTrain->pMLSVM->predict(testMat);

#endif

    QString result = QString::number(estimate);
    ui->outputLabel->setText(result);
    qDebug() << "Estimated result = " << estimate <<"\n";
    //qDebug() << "Classification is ended! \n";

    ui->ClassificationPushButton->setText("Classification");
    ui->ClassificationPushButton->setEnabled(true);
}

void MainWindow::on_pushButton_2_clicked()
{
    // evaluate the test file from libsvm
    std::FILE *input, *output;
    std::string modelFileName = "/home/dkim/Documents/FPFiltering/model/DBFD_hog_LibSVM.fvoc.model";
    std::string testFileName = "/home/dkim/Documents/FPFiltering/model/DBTV_hog_LibSVM.fvoc";
    std::string outFileName = "/home/dkim/Documents/FPFiltering/model/DBTV_hog_LibSVM.fvoc.out";

    input = fopen(testFileName.c_str(),"r");

    // Load the setting parameter for SVM
    if(not mpSettings)
    {
        QMessageBox::critical(this,tr("Default Settings Not Set"),tr("Default Settings hasn't been set yet. Please click Load button to load default settings"));
        return;
    }
    // Load a model
    psvm.reset(new vml::MLSVM(mpSettings));

    if(psvm->loadModelFromFile(modelFileName) )
    {
        qDebug() << "SVM is loaded!\n";

        // Set the output
        output = fopen(outFileName.c_str(),"w");

        psvm->predictFromFile(input, output);

        fclose(input);
        fclose(output);

        QString result = QString::number(psvm->getTestAccuracy());
        ui->outputLabel->setText(result);
        qDebug() << "Test Accuracy is " << psvm->getTestAccuracy() << " %.";

    }
    else
    {
        qDebug() << "SVM model is not loaded!\n";
    }



}

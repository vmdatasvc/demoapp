#include "mlfeature.h"

// when a calass is created, setup the mode with feature mode and color mode (color mode only affects the color-based feature extraction)
MLFeature::MLFeature( boost::shared_ptr<vml::Settings> settings)
    :selected_feature(FM_HOG),selected_color(CM_YCrCb)
{

    // parameters
    std::string fmodeStr= settings->getString("Feature/fmode", "FM_COLOR", false);
    std::string cmodeStr= settings->getString("Feature/cmode", "CM_YCrCb", false);

    if(fmodeStr == "FM_HOG")
    {
        selected_feature = FM_HOG;
    }
    else if(fmodeStr == "FM_COLOR")
    {
        selected_feature = FM_COLOR;
    }

    if(cmodeStr == "CM_YCrCb")
    {
        selected_color = CM_YCrCb;
    }
    if(cmodeStr == "CM_HSV")
    {
        selected_color = CM_HSV;
    }
    if(cmodeStr == "CM_RGB")
    {
        selected_color = CM_RGB;
    }



    // Load feature parameters according to the fmode
    setParams(settings);
}

void MLFeature::setParams(boost::shared_ptr<vml::Settings> settings)
{
    // for image size normalization
    norm_size.width = settings->getInt("Feature/normImgWidth", 30, false);
    norm_size.height = settings->getInt("Feature/normImgHeight", 30, false);


    if(selected_feature == FM_HOG)
    {
        winsize = settings->getInt("Feature/HOG/winsize", 30, false);
        cellsize = settings->getInt("Feature/HOG/cellize", 6, false);
        blocksize = settings->getInt("Feature/HOG/blocksize", 12, false);
        blockstride = settings->getInt("Feature/HOG/blockstride", 12, false);
        nbins = settings->getInt("Feature/HOG/nbins", 9, false);
        winSigma = settings->getFloat("Feature/HOG/winSigma", -1, false);
    }

}


void MLFeature::extFeature(cv::Mat &inData)
{
    cv::Mat normData;

    cv::resize(inData, normData, cv::Size(norm_size));

    featData.clear();

    // color features
    if(selected_feature == FM_COLOR)
    {
        extMeanColor(normData, selected_color);
    }
    else if(selected_feature == FM_HOG)
    {
        extHOG(normData);
    }

}


void MLFeature::extHOG(cv::Mat &cimg)
{
    cv::Mat gimg;
    cv::cvtColor(cimg, gimg, CV_BGR2GRAY);

    cv::HOGDescriptor hog;

    // validateHOGParams()
    // parameters: winsize(ws)- region of interest, cellsize(cs), blocksize(bs), blockstride(bsd), numberofbins(nb)
    // conditions: ws % cs = 0,  bs = B*cs (in pixel unit for BxB blocks), bsd = (1-overlapRate)*bs, (ws -bs) % bd =0
    //

    // HOG parameters
    unsigned int ws, cs, bs, B, nb, bsd;
    //float ovRate, bsd;
    //ws = 30;
    //cs = 6;     // cs X cs pixel cell
    //B = 2;      // B x B blocks
    //ovRate = 1;   // overlapping rate of blocks for histogram normalization
    //bs = B*cs;
    //bsd = (1-ovRate)*bs;
    //nb = 6;

    ws = winsize;
    cs = cellsize;
    bs = blocksize;
    bsd = blockstride;
    nb = nbins;

    hog.winSize = cv::Size(ws,ws);      // Detection window size, Align to block size and block stride
    hog.blockSize = cv::Size(bs,bs);      // Block size in pixels. Align to cell size. Only(16,16) is supported in version opencv 2.4
    hog.blockStride = cv::Size(bsd,bsd);    // Block stride. It must be a multiple of cell size.
    hog.cellSize = cv::Size(cs,cs);       // Cell size. Only (8, 8) is supported for opencv 2.4.
    hog.nbins = nb;                      // Number of bins. Only 9 bins per cell are supported for opencv 2.4.
    hog.winSigma = winSigma;                  // Gaussian smoothing window parameter.


    cv::Size trainingPadding = cv::Size(0, 0);
    cv::Size winStride = cv::Size(0,0);


    // Check for mismatching dimensions
    if (gimg.cols != hog.winSize.width || gimg.rows != hog.winSize.height) {
        featData.clear();
        printf("Error: Image dimensions (%u x %u) do not match HOG window size (%u x %u)!\n", gimg.cols, gimg.rows, hog.winSize.width, hog.winSize.height);
        return;
    }

    if( ws % cs !=0 || (ws-bs) % (unsigned int)bsd !=0 )
    {
        std::cout << "Error: paramters are not corrected!\n";
        std::cout << "(ws,cs,bs,bsd): " << ws << ", " << cs << ", " << (ws-bs) << ", " << bsd;
    }

    std::vector<cv::Point> locations;
    //std::vector<float> features;

    hog.compute(gimg, featData, winStride, trainingPadding, locations);


    //size = hog.(size_t)nbins*
    //(blockSize.width/cellSize.width)*
    //(blockSize.height/cellSize.height)*
    //((winSize.width - blockSize.width)/blockStride.width + 1)*
    //((winSize.height - blockSize.height)/blockStride.height + 1);

    //printf("descriptor number =%d\n", featData.size() );
}

void MLFeature::extMeanColor(cv::Mat &cimg, ColMode cmode)
{
    cv::Mat dimg;

    if(cmode == CM_YCrCb)     // YCrCb
    {
        cv::cvtColor(cimg, dimg, CV_BGR2YCrCb);
    }
    else if(cmode == CM_HSV)
    {
        cv::cvtColor(cimg, dimg, CV_BGR2HSV);
    }

    // Extract channel individually
    cv::Mat planes[3];
    cv::split(dimg, planes);

    cv::Scalar mean_ch1 = cv::mean(planes[0]);
    cv::Scalar mean_ch2 = cv::mean(planes[1]);
    cv::Scalar mean_ch3 = cv::mean(planes[2]);

    featData.push_back(mean_ch1.val[0]);
    featData.push_back(mean_ch2.val[0]);
    featData.push_back(mean_ch3.val[0]);

    //cv::Mat ret = cv::Mat(mcolor_vec);

}


// HOGDescriptor visual_imagealizer
// adapted for arbitrary size of feature sets and training images
cv::Mat MLFeature::get_hogdescriptor_visual_image(cv::Mat& origImg,
                                   std::vector<float>& descriptorValues,
                                   cv::Size winSize,
                                   cv::Size cellSize,
                                   int nBins,
                                   int scaleFactor,
                                   double viz_factor)
{
    cv::Mat visual_image;
    cv::resize(origImg, visual_image, cv::Size(origImg.cols*scaleFactor, origImg.rows*scaleFactor));

    //int gradientBinSize = 9;
    int gradientBinSize = nBins;
    // dividing 180° into 9 bins, how large (in rad) is one bin?
    float radRangeForOneBin = 3.14/(float)gradientBinSize;

    // prepare data structure: 9 orientation / gradient strenghts for each cell
    int cells_in_x_dir = winSize.width / cellSize.width;
    int cells_in_y_dir = winSize.height / cellSize.height;
    int totalnrofcells = cells_in_x_dir * cells_in_y_dir;
    float*** gradientStrengths = new float**[cells_in_y_dir];
    int** cellUpdateCounter   = new int*[cells_in_y_dir];
    for (int y=0; y<cells_in_y_dir; y++)
    {
        gradientStrengths[y] = new float*[cells_in_x_dir];
        cellUpdateCounter[y] = new int[cells_in_x_dir];
        for (int x=0; x<cells_in_x_dir; x++)
        {
            gradientStrengths[y][x] = new float[gradientBinSize];
            cellUpdateCounter[y][x] = 0;

            for (int bin=0; bin<gradientBinSize; bin++)
                gradientStrengths[y][x][bin] = 0.0;
        }
    }

    // nr of blocks = nr of cells - 1
    // since there is a new block on each cell (overlapping blocks!) but the last one
    int blocks_in_x_dir = cells_in_x_dir - 1;
    int blocks_in_y_dir = cells_in_y_dir - 1;

    // compute gradient strengths per cell
    int descriptorDataIdx = 0;
    int cellx = 0;
    int celly = 0;

    for (int blockx=0; blockx<blocks_in_x_dir; blockx++)
    {
        for (int blocky=0; blocky<blocks_in_y_dir; blocky++)
        {
            // 4 cells per block ...
            for (int cellNr=0; cellNr<4; cellNr++)
            {
                // compute corresponding cell nr
                int cellx = blockx;
                int celly = blocky;
                if (cellNr==1) celly++;
                if (cellNr==2) cellx++;
                if (cellNr==3)
                {
                    cellx++;
                    celly++;
                }

                for (int bin=0; bin<gradientBinSize; bin++)
                {
                    float gradientStrength = descriptorValues[ descriptorDataIdx ];
                    descriptorDataIdx++;

                    gradientStrengths[celly][cellx][bin] += gradientStrength;

                } // for (all bins)


                // note: overlapping blocks lead to multiple updates of this sum!
                // we therefore keep track how often a cell was updated,
                // to compute average gradient strengths
                cellUpdateCounter[celly][cellx]++;

            } // for (all cells)


        } // for (all block x pos)
    } // for (all block y pos)


    // compute average gradient strengths
    for (int celly=0; celly<cells_in_y_dir; celly++)
    {
        for (int cellx=0; cellx<cells_in_x_dir; cellx++)
        {

            float NrUpdatesForThisCell = (float)cellUpdateCounter[celly][cellx];

            // compute average gradient strenghts for each gradient bin direction
            for (int bin=0; bin<gradientBinSize; bin++)
            {
                gradientStrengths[celly][cellx][bin] /= NrUpdatesForThisCell;
            }
        }
    }


    std::cout << "descriptorDataIdx = " << descriptorDataIdx << std::endl;

    // draw cells
    for (int celly=0; celly<cells_in_y_dir; celly++)
    {
        for (int cellx=0; cellx<cells_in_x_dir; cellx++)
        {
            int drawX = cellx * cellSize.width;
            int drawY = celly * cellSize.height;

            int mx = drawX + cellSize.width/2;
            int my = drawY + cellSize.height/2;

            cv::rectangle(visual_image,
                      cv::Point(drawX*scaleFactor,drawY*scaleFactor),
                      cv::Point((drawX+cellSize.width)*scaleFactor,
                      (drawY+cellSize.height)*scaleFactor),
                      CV_RGB(100,100,100),
                      1);

            // draw in each cell all 9 gradient strengths
            for (int bin=0; bin<gradientBinSize; bin++)
            {
                float currentGradStrength = gradientStrengths[celly][cellx][bin];

                // no line to draw?
                if (currentGradStrength==0)
                    continue;

                float currRad = bin * radRangeForOneBin + radRangeForOneBin/2;

                float dirVecX = cos( currRad );
                float dirVecY = sin( currRad );
                float maxVecLen = cellSize.width/2;
                float scale = viz_factor; // just a visual_imagealization scale,
                                          // to see the lines better

                // compute line coordinates
                float x1 = mx - dirVecX * currentGradStrength * maxVecLen * scale;
                float y1 = my - dirVecY * currentGradStrength * maxVecLen * scale;
                float x2 = mx + dirVecX * currentGradStrength * maxVecLen * scale;
                float y2 = my + dirVecY * currentGradStrength * maxVecLen * scale;

                // draw gradient visual_imagealization
                cv::line(visual_image,
                     cv::Point(x1*scaleFactor,y1*scaleFactor),
                     cv::Point(x2*scaleFactor,y2*scaleFactor),
                     CV_RGB(0,0,255),
                     1);

            } // for (all bins)

        } // for (cellx)
    } // for (celly)


    // don't forget to free memory allocated by helper data structures!
    for (int y=0; y<cells_in_y_dir; y++)
    {
      for (int x=0; x<cells_in_x_dir; x++)
      {
           delete[] gradientStrengths[y][x];
      }
      delete[] gradientStrengths[y];
      delete[] cellUpdateCounter[y];
    }
    delete[] gradientStrengths;
    delete[] cellUpdateCounter;

    return visual_image;

}

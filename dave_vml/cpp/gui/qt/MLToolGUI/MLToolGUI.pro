#-------------------------------------------------
#
# Project created by QtCreator 2015-12-28T13:44:04
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MLToolGUI
TEMPLATE = app

win32{
LIBS += -lopengl32
DEFINES += WINDOWS
#DEFINES += BOOST_ALL_NO_LIB
}

SOURCES += main.cpp\
        mainwindow.cpp \
    mltrain.cpp

HEADERS  += mainwindow.h \
    mltrain.h

FORMS    += mainwindow.ui

LIBS    += -L../../../../build/lib/Debug -lvmlcore -lvmlml -ltorch3

# OpenCV GL Viewer
INCLUDEPATH += ../opencv
#DEPENDPATH += ../opencv
#include(../opencv/OpenCV.pri)
SOURCES += ../opencv/cqtopencvviewergl.cpp
HEADERS += ../opencv/cqtopencvviewergl.h

#OpenCV and boost
INCLUDEPATH += /usr/local/include
LIBS += -L/usr/local/lib

# OpenCV
LIBS += -lopencv_core -lopencv_imgproc -lopencv_highgui -lopencv_video -lopencv_videoio -lopencv_calib3d -lopencv_features2d -lopencv_ml -lopencv_objdetect -lopencv_imgcodecs

# boost
LIBS += -lboost_system

# misc
LIBS += -ljpeg -lpng -lz -lexpat

# vml libs
INCLUDEPATH += ../../../include

# external libs
INCLUDEPATH += ../../../external/torch3/src

# dlib
INCLUDEPATH += ../../../external/dlib-18.17
LIBS += -L../../../external/dlib-18.17/dlib/build -ldlib

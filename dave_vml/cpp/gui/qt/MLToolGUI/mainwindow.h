#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QMessageBox>
#include <QTimer>
#include <QtDebug>

#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "modules/ml/AITPort/Image.hpp"
#include "boost/shared_ptr.hpp"
#include <fstream>
#include <sstream>

#include "core/Image.hpp"
#include "core/Video.hpp"
#include "core/Settings.hpp"

#include "mltrain.h"
//#include "mlsvm.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void cvtMatToImage32(cv::Mat iMat, aitvml::Image32 *img);
    void cvtMatToImage8(cv::Mat imat, aitvml::Image8 *img);
    void cvtImage32ToMat(aitvml::Image32 img, cv::Mat *iMat);
    void cvtImage8ToMat(aitvml::Image8 img, cv::Mat *imat);

    cv::Mat gImgMat;


    boost::shared_ptr<MLTrain> pMLTrain;
    boost::shared_ptr<vml::MLSVM> psvm;
    boost::shared_ptr<vml::MLFeature> pfeature;

    // settings
    boost::shared_ptr<vml::Settings>   mpSettings;

private slots:

    void updateMethod();

    void on_OpenFilePushButton_clicked();

    void on_StartPushButton_toggled(bool checked);

    void on_PausePushButton_toggled(bool checked);

    void on_pushButton_clicked();

    void on_WebCamInputRadioButton_clicked();

    void on_toolButton_clicked();

    void on_toolButton_2_clicked();

    void on_BuildModelPushButton_clicked();

    void on_LoadTestImgPushButton_clicked();

    void on_SettingsToolButton_clicked();

    void on_LoadSettingsPushButton_toggled(bool checked);

    void on_FeatureHOGRadioButton_clicked(bool checked);

    void on_FeatureColorRadioButton_clicked(bool checked);

    void on_ColorYCrCbRadioButton_clicked(bool checked);

    void on_ColorHSVRadioButton_clicked(bool checked);

    void on_ColorRGBRadioButton_clicked(bool checked);

    void on_LoadModelPathToolButton_clicked();

    void on_LoadModelPushButton_toggled(bool checked);

    void on_ClassificationPushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::MainWindow *ui;

    // ui widget maps
    QPushButton *muiStartButton;

    // opencv

    cv::VideoCapture    mCapture;

    bool mStarted;
    int  libType;

    QTimer *timer;


protected:

    void init();
    void timerEvent(QTimerEvent *event);

    // Test
    void testImageConversion();
    void testDemographicsRecognition();
    void testReadXML();
    void testFaceDetection();
    void faceDetectionByDirectOpenCV();
    void faceDetectionByVML();
    void testHOGRepresentation();


};

#endif // MAINWINDOW_H

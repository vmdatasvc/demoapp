#ifndef MLFEATURE_H
#define MLFEATURE_H

#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/ml/ml.hpp>

#include "boost/shared_ptr.hpp"
#include <core/Settings.hpp>

enum ColMode { CM_YCrCb, CM_HSV, CM_RGB };
enum FeatMode { FM_COLOR, FM_HOG };

class MLFeature
{
public:

    MLFeature(boost::shared_ptr<vml::Settings> settings);

    void setParams(boost::shared_ptr<vml::Settings> settings);

    void extFeature(cv::Mat &inData);

    std::vector<float> getFeature() {return featData;}


    // Color related
    void extMeanColor(cv::Mat &cimg, ColMode color_mode);


    // HOG related
    void extHOG(cv::Mat &cimg);
    cv::Mat get_hogdescriptor_visual_image(cv::Mat& origImg,
                                       std::vector<float>& descriptorValues,
                                       cv::Size winSize,
                                       cv::Size cellSize,
                                       int nBins,
                                       int scaleFactor,
                                       double viz_factor);
    // Variables
    std::vector<float> featData;

    // HOG parameters
    int winsize;
    int cellsize;
    int blocksize;
    int blockstride;
    int nbins;
    float winSigma;

protected:

    FeatMode selected_feature;
    ColMode  selected_color;
    cv::Size norm_size;




};

#endif // MLFEATURE_H


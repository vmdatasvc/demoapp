#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include "boost/filesystem.hpp"
#include "boost/regex.hpp"
//#include <boost/range/adaptors.hpp>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    init();
    timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()), this, SLOT(updateMethod()));

    timer2 = new QTimer(this);
    connect(timer2,SIGNAL(timeout()), this, SLOT(updateDetection()));

}

MainWindow::~MainWindow()
{
    delete ui;

    delete timer;
}

void MainWindow::init()
{
    // Default: image path: "../../../../cpp/gui/qt/FaceTrackerGUI/ai-face.jpg"
    // For ipcam: "http://10.250.1.90/axis-cgi/mjpg/video.cgi?resolution=1280x960"
    // For OmniSensr in ST office:
    //  (FaceCam1-from the door toward the office inside ) http://172.31.11.1:8080/stream/video.mjpeg
    //  (FaceCam2-from the office inside toward the door ) http://172.31.11.2:8080/stream/video.mjpeg
    //  (4:3)  2048x1536, 1296x972, 1280x960, 1024x768, 800x600, 640x480, 480x360, 320x240
    //  (16:9) 1920x1080, 1280x720, 800x450, 640x360, 480x270, 320x180

    // clean viewer
    ui->openCVViewer1->clearImage();
    ui->openCVViewer2->clearImage();
    ui->openCVViewer3->clearImage();
    ui->openCVViewer4->clearImage();

    // initial selection of a radio button
//    ui->VideoInputRadioButton->setChecked(true);
//    ui->ImageInputRadioButton->setChecked(false);
//    ui->WebCamInputRadioButton->setChecked(false);

}


void MainWindow::timerEvent(QTimerEvent *event)
{
    //cv::Mat image, dispImg;
    //mCapture >> image;

    // Show the image
    //cv::cvtColor(image,dispImg, CV_BGR2RGB);
    //ui->openCVViewer1->showImage(dispImg);
}


/*
void MainWindow::updateDetection()
{
    if (not dt1->detect())
    {
        this->on_FDPushButton_toggled(false);
        return;
    }

     cv::Mat image,disp;

     dt1->getGrabbedImage().copyTo(image);
     dt1->drawValidTargets(image);

     // show the images
     cv::cvtColor(image, disp, CV_BGR2RGB);
     ui->openCVViewer1->showImage(disp);

     mDetectedFaceCount += dt1->getNumDetectedFaces();

     mFrameCount++;

     if(curMinFaceSize > dt1->getCurMinFaceSize())
         curMinFaceSize = dt1->getCurMinFaceSize();
     if(curMaxFaceSize < dt1->getCurMaxFaceSize())
         curMaxFaceSize = dt1->getCurMaxFaceSize();

     // update labels
     //ui->ImageWidthLabel->setText(QString::number(image.cols));
     //ui->ImageHeightLabel->setText(QString::number(image.rows));
     //ui->FPSLabel->setText(QString::number(dt1->getFPS()));
     //ui->DetectedFaceCountLabel->setText(QString::number(mDetectedFaceCount));
     //ui->FrameCountLabel->setText(QString::number(mFrameCount));
     //ui->DetectedMinFaceSizeLabel->setText(QString::number(curMinFaceSize));
     //ui->DetectedMaxFaceSizeLabel->setText(QString::number(curMaxFaceSize));

     // open UI
     if(dt1->getNumDetectedFaces() >0)
     {
         //dt1->openFacialLandmarkUI(image);
         on_HoldPushButton_toggled(true);
     }


}
*/

void MainWindow::on_OpenFilePushButton_clicked()
{
    QString tmpString = QFileDialog::getOpenFileName(this, "Open File");

    if( !tmpString.trimmed().isEmpty())
    {
        ui->OpenFileLineEdit->setText(tmpString);
    }
}

/*
void MainWindow::on_toolButton_clicked()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::Directory);
    QString newPath = dialog.getExistingDirectory(this,tr("Settings Path"), ui->SettingsPathLineEdit->text(),QFileDialog::ShowDirsOnly);

    // set url
    ui->SettingsPathLineEdit->setText(newPath);
}
*/



void MainWindow::on_AnnotatePushButton_clicked()
{

}

void MainWindow::on_SaveAndNextPushButton_clicked()
{

}

void MainWindow::on_LoadFileDirPushButton_clicked()
{
    // Load the face image files
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::Directory);
    QString newPath = dialog.getExistingDirectory(this,tr("Face image Path"), ui->OpenFileLineEdit->text(),QFileDialog::ShowDirsOnly);

    // set url
    ui->OpenFileLineEdit->setText(newPath);

    //numFiles=0;
}

void MainWindow::on_StartPushButton_clicked(bool checked)
{
    if(checked)
    {
        // load the image in the file list of directory
        boost::filesystem::path dir_path = ui->OpenFileLineEdit->text().toStdString().c_str();

        boost::filesystem::directory_iterator itr;

        for( itr=boost::filesystem::directory_iterator(dir_path) ; itr != boost::filesystem::directory_iterator() ; itr++)
        {
            if (!boost::filesystem::is_directory(itr->path()))
            {
               files.push_back(itr->path().string());
            }
            else
                continue;
        }


        // Set vaiables
        nTotalFiles = files.size();
        numFiles=0;

        // load the first face image
        cv::Mat image = cv::imread(files[numFiles].c_str());
        cv::Mat disp;

        image.copyTo(disp);

        cv::cvtColor(image, disp, CV_BGR2RGB);
        ui->openCVViewer1->showImage(disp);

        numFiles++;


        // If the writing file is existed, add data to it. If not, create new csv file.
        QString filename = QFileDialog::getSaveFileName(this, "DialogTitle", "filename.csv", "CSV files (.csv);;Zip files (.zip, *.7z)", 0, 0); // getting the filename (full path)
        QFile data(filename);
        if(data.open(QFile::WriteOnly |QFile::Truncate))
        {
            QTextStream output(&data);
            output << "'your csv content';'more csv content'";
        }
        else  // create a file
        {

        }


    }
}

void MainWindow::on_PassPushButton_clicked()
{
    if(numFiles < nTotalFiles)
    {
        // load the first face image
        cv::Mat image = cv::imread(files[numFiles].c_str());
        cv::Mat disp;
        image.copyTo(disp);

        cv::cvtColor(image, disp, CV_BGR2RGB);
        ui->openCVViewer1->showImage(disp);
        numFiles++;
    }
}


std::vector<std::string> MainWindow::searchSameID(std::string filename)
{
    // extract the id only from filename
    boost::filesystem::path p(filename.c_str());
    boost::filesystem::path dir_path = p.parent_path();
    //boost::filesystem::path name = p.path.filename();
    std::string name = p.stem().string();

    size_t sep = name.find_first_of("_");
    std::string pid = name.substr(0, sep);

    std::string prefix = pid + "*";
    //

    boost::regex filter(prefix);

    std::vector<std::string> all_matching_files;
    boost::filesystem::directory_iterator itr;
/*
    for(boost::filesystem::directory_iterator i(dir_path); i!=itr; ++i)
    {
        if(! boost::filesystem::is_regular_file(i->status()) ) continue;
        boost::smatch what;

        if(!boost::regex_match(i->path.filename(), what, filter) ) continue;

        all_matching_files.push_back(i->leaf());
    }
*/
    return all_matching_files;

}


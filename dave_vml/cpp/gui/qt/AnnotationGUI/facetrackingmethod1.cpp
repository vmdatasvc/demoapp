#include "facetrackingmethod1.h"

using namespace vml;

FaceTrackingMethod1::FaceTrackingMethod1(std::string url,
                                         boost::shared_ptr<vml::Settings> settings)
{
    // create tracker
    mpFaceTracker = new FaceTracker(url,settings);
    //mpFaceTracker = new MultiTargetVisionTracker(url,settings);
}


FaceTrackingMethod1::~FaceTrackingMethod1()
{
    delete mpFaceTracker;
}


bool FaceTrackingMethod1::process()
{
    // create new targets after verification
    return mpFaceTracker->process();
}


const cv::Mat&  FaceTrackingMethod1::getGrabbedImage(void)
{

    return mpFaceTracker->getGrabbedImage();
}

const cv::Mat& FaceTrackingMethod1::getBackProjection(void)
{
    return mpFaceTracker->getBackProjection();
}

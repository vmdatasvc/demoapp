#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QMessageBox>
#include <QTimer>

#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
//#include <facetrackingmethod1.h>
#include <facedetectmethod1.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:


    void on_OpenFilePushButton_clicked();

    void on_AnnotatePushButton_clicked();

    void on_SaveAndNextPushButton_clicked();

    void on_LoadFileDirPushButton_clicked();

    void on_StartPushButton_clicked(bool checked);

    void on_PassPushButton_clicked();

private:
    Ui::MainWindow *ui;

    // ui widget maps
    QPushButton *muiStartButton;

    // opencv

    cv::VideoCapture    mCapture;

    bool mStarted;


    QTimer *timer;
    QTimer *timer2;

    // tracking method classes
    FaceDetectMethod1           *dt1;

    // variables
    unsigned int    mFrameCount;
    unsigned int    mDetectedFaceCount;
    unsigned int    curMinFaceSize;
    unsigned int    curMaxFaceSize;

    //
    int numFiles;
    int nTotalFiles;
    std::vector<std::string> files;


protected:

    void init();
    void timerEvent(QTimerEvent *event);

    void updateParams();

//    void cvtMatToImage32(cv::Mat iMat, ait::Image32 &img);
//    void cvtImage32ToMat(ait::Image32 &img, cv::Mat *iMat);


   std::vector<std::string> searchSameID(std::string filename);

};

#endif // MAINWINDOW_H

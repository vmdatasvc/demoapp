#ifndef FACETRACKINGMETHOD1_H
#define FACETRACKINGMETHOD1_H

#include "core/Image.hpp"
#include "core/Video.hpp"
#include "core/Settings.hpp"

#include "modules/ml/FaceTracker.hpp"

class FaceTrackingMethod1
{
public:
    FaceTrackingMethod1(std::string url,
                        boost::shared_ptr<vml::Settings> settings);
    ~FaceTrackingMethod1();

    bool process();
    void setTrackingMethod(vml::FaceTrackingMethods hm) {mpFaceTracker->setTrackingMethod(hm);}

    // for visualize
    const cv::Mat& getGrabbedImage(void);
    const cv::Mat& getBackProjection(void);

    // display methods
    void drawActiveTargets(cv::Mat &display_image) {mpFaceTracker->drawActiveTargets(display_image);}
    void drawActiveTargetsWithDMG(cv::Mat &display_image) {mpFaceTracker->drawActiveTargetsWithDMG(display_image);}
    void drawValidNewTargets(cv::Mat &display_image) {mpFaceTracker->drawValidNewTargets(display_image);}

    int getFrameCount() {return mpFaceTracker->getCurrentFrameNumber();}
    unsigned int getNumActiveTargets() {return mpFaceTracker->getNumActiveTargets();}
    unsigned int getNumTotalTargets() {return mpFaceTracker->getNumTotalTargets();}
    unsigned int getNumInactiveTargets() {return mpFaceTracker->getNumInactiveTargets();}
    unsigned int getNumCompletedTargets() {return mpFaceTracker->getNumCompletedTargets();}


private:

    vml::FaceTracker *mpFaceTracker;
    //vml::MultiTargetVisionTracker  *mpFaceTracker;
};

#endif // FACETRACKINGMETHOD1_H

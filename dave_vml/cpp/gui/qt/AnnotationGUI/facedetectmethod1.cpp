#include "facedetectmethod1.h"
#include <QDebug>

using namespace vml;

FaceDetectMethod1::FaceDetectMethod1(std::string url,float sF, int mNbr)
{

    bool openSuccess;
    if (isdigit(url[0]))
    {
        openSuccess = mCapture.open(atoi(url.c_str()));
    }
    else
    {
        openSuccess = mCapture.open(url);
    }

    if (!openSuccess)
    {
        qDebug() <<"Camera is not opened!";
    }

    scaleFactor = sF;
    minNeighbors = mNbr;
    minFaceSize = 20;
    maxFaceSize = 500;

    std::string cascadeFilePath = "../../../../data/ml/opencv/haarcascades/haarcascade_frontalface_alt2.xml";
    mpCascade.load(cascadeFilePath.c_str());

}

FaceDetectMethod1::~FaceDetectMethod1()
{
    if(mCapture.isOpened())
    {
        mCapture.release();
    }
}


bool FaceDetectMethod1::detect()
{
    //cv::Mat	inputimage;

    mCapture >> inputimage;

    if(inputimage.empty())
    {
        return false;
    }
    // face detection
    mpCascade.detectMultiScale(inputimage,mFaces,scaleFactor, minNeighbors,0,cv::Size(minFaceSize,minFaceSize), cv::Size(maxFaceSize,maxFaceSize));


    unsigned int minsizetmp=1000, maxsizetmp=0;

    for (int i = 0; i < mFaces.size(); i++) {

           unsigned int fsize = std::min(mFaces[i].width, mFaces[i].height);

           if(fsize < minsizetmp)
           {
               minsizetmp = fsize;

           }
           if(fsize > maxsizetmp)
           {
               maxsizetmp = fsize;
           }
     }

    mMinFaceSize = minsizetmp;
    mMaxFaceSize = maxsizetmp;

    return true;
}


void FaceDetectMethod1::drawValidTargets(cv::Mat &image) const
{

    for (int i = 0; i < mFaces.size(); i++) {

           cv::rectangle(image, mFaces[i].br(), mFaces[i].tl(), cv::Scalar(0, 255, 255), 1, 8, 0);
     }

}


const cv::Mat&  FaceDetectMethod1::getGrabbedImage(void)
{

    return inputimage;
}


unsigned int FaceDetectMethod1::getNumDetectedFaces(void)
{
    return mFaces.size();
}

unsigned int FaceDetectMethod1::getCurMinFaceSize(void)
{

    return mMinFaceSize;
}

unsigned int FaceDetectMethod1::getCurMaxFaceSize(void)
{

    return mMaxFaceSize;
}

double FaceDetectMethod1::getFPS(void)
{
     return mCapture.get(CV_CAP_PROP_FPS);
}

void FaceDetectMethod1::openFacialLandmarkUI(cv::Mat &image)
{

    cv::namedWindow("FacialLandmarkUI",cv::WINDOW_AUTOSIZE);
    cv::imshow("FacialLandmarkUI", image);

}

void FaceDetectMethod1::updateFacialLandmarkUI(const std::string &winname, cv::Mat &image)
{
    cv::imshow(winname.c_str(), image);
}

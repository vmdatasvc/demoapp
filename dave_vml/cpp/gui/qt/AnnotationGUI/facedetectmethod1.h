#ifndef FACEDETECTMETHOD1_H
#define FACEDETECTMETHOD1_H

#include "core/Image.hpp"
#include "core/Video.hpp"
//#include "core/Settings.hpp"

#include "opencv2/objdetect/objdetect.hpp"


class FaceDetectMethod1
{
public:
    FaceDetectMethod1(std::string url,float sF, int mNbr);
    ~FaceDetectMethod1();

    bool detect();
    void drawValidTargets(cv::Mat &image) const;
    void openFacialLandmarkUI(cv::Mat &image);
    void updateFacialLandmarkUI(const std::string &winname, cv::Mat &image);

    const cv::Mat&  getGrabbedImage(void);

    unsigned int getNumDetectedFaces(void);

    unsigned int getCurMinFaceSize(void);
    unsigned int getCurMaxFaceSize(void);

    double getFPS(void);

    // Draw landmark
    cv::Point2f point;
    bool addRemovePt;

private:

    cv::Mat	inputimage;

    cv::VideoCapture        mCapture;
    cv::CascadeClassifier	mpCascade;
    float scaleFactor;
    int minNeighbors;
    int minFaceSize;
    int maxFaceSize;

    unsigned int mMinFaceSize;
    unsigned int mMaxFaceSize;

    std::vector<cv::Rect>	mFaces;

};

#endif // FACEDETECTMETHOD1_H

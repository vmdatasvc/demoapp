#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <opencv2/highgui/highgui.hpp>
#include <QPushButton>
#include <QLineEdit>
#include <QTimer>

#include <fstream>

// opencv include
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/video/background_segm.hpp>

//#include "trackingmethod1.h"
#include <core/Image.hpp>
#include <core/Settings.hpp>

#include <modules/iot/IOTCommunicator.hpp>
#include <modules/vision/VisionVideoModule.hpp>
#include <modules/vision/VisionDetector.hpp>
#include <modules/vision/MultiTargetVisionTracker.hpp>

#include "trackingmethod2.h"
#include "trackingmethodlegacy.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_StartButton_toggled(bool checked);

    void on_quitbutton_clicked();

    void on_OpenFilePushButton_clicked();

    // process slots
    void updateMethod1();

    void saveTrajectory();

    void on_pausePushButton_toggled(bool checked);

    void on_stepPushButton_clicked();

    void on_Method1SettingsLoadPushButton_clicked();

    void on_Method1SettingsPathToolButton_clicked();

    void on_TrackingMethodHSVHistRadioButton_clicked(bool checked);

    void on_TrackingMethodrgChromHistRadioButton_clicked(bool checked);

    void on_TrackingMethodRGBHistRadioButton_clicked(bool checked);

    void on_TrackingMethodDlibCorrRadioButton_clicked(bool checked);

    void on_TrackingMethodHybridRadioButton_clicked(bool checked);

    void on_saveVideoImagePushButton_clicked();

    void on_TrackingMethodrbChromRadioButton_clicked(bool checked);

    void on_TrackingMethodCIELabKDERadioButton_clicked(bool checked);

private:
    Ui::MainWindow *ui;

    // ui widget maps
    QPushButton *muiStartButton;
    QPushButton *muiQuitButton;
    QLineEdit   *muiVideoURLLineEdit;

    QString     mLastOpenedFile;

    // opencv

    cv::VideoCapture    mCapture;

    bool    mStarted;   // toggle switch for start button

    int     mTimerID;   // timer id for current processor

    // timers for processing
    QTimer  *mpTimerMethod1;
    QTimer  *mpTimerTrajectorySave;

    // tracking method classes
    // for vml tracking method
    boost::shared_ptr<SETTINGS>                         mpSettings;
    boost::shared_ptr<vml::VisionVideoModule>           mpVideoModule;
    boost::shared_ptr<vml::MultiTargetVisionTracker>    mpTracker;

    PaulMethod::TrackingMethod2         *tr2;
    TrackingMethodLegacy    *trlegacy;

    // variables
    unsigned int    mFrameCount;

    vml::TrackingMethods mTrackingMethod;

    // for displaying movie output
    cv::Mat	mMovieImage;

    // for trajectoryoutput
    std::ofstream   mTrajectoryOutputFile;


protected:
    void    timerEvent(QTimerEvent *event);  // timer for triggering video capture

private:

};

#endif // MAINWINDOW_H

#include <QFileDialog>
#include <QMessageBox>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "modules/utility/Util.hpp"

#include <boost/shared_ptr.hpp>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    muiStartButton(NULL),
    muiQuitButton(NULL),
    muiVideoURLLineEdit(NULL),
    mLastOpenedFile(""),
    mFrameCount(0)
{
    ui->setupUi(this);

    // clean viewer
    ui->openCVViewer1->clearImage();
    ui->openCVViewer2->clearImage();
    ui->openCVViewer3->clearImage();

    // map ui components for quick access
    muiStartButton = ui->StartButton;
    muiQuitButton = ui->quitbutton;
    muiVideoURLLineEdit = ui->VideoURLLineEdit;

    mpTimerMethod1 = new QTimer(this);
    mpTimerTrajectorySave = new QTimer(this);

    // connect timers
    connect(mpTimerMethod1,SIGNAL(timeout()),this,SLOT(updateMethod1()));
    connect(mpTimerTrajectorySave,SIGNAL(timeout()),this,SLOT(saveTrajectory()));


}

MainWindow::~MainWindow()
{
    delete ui;

    delete mpTimerMethod1;
    delete mpTimerTrajectorySave;
}

void MainWindow::timerEvent(QTimerEvent *event)
{
    cv::Mat image;  // for capturing video image
    mCapture >> image;

    // if end of video sequence is reached, signal StartButton toggle

    // show the image
    ui->openCVViewer1->showImage(image);

    // run process

}

void MainWindow::on_StartButton_toggled(bool checked)
{

    if (checked)
    {
            // run my tracking code here
            QString media_url = muiVideoURLLineEdit->text();
            QFileInfo media_file_info(media_url);

            // check settings were already loaded
//            if (not mpSettings)
//            {
//                QMessageBox::critical(this,tr("Settings not loaded"),tr("Settings were not loaded. Please load settings before start tracking"));
//                return;
//            }
            if ((not media_url.startsWith("http",Qt::CaseInsensitive))&&(not media_url[0].isDigit()))
            {
                // this is file. check if file exists
                if ((not media_file_info.exists())||(not media_file_info.isFile()))
                {
                    QMessageBox::critical(this,"Video file not found","Video file "+media_url+" is not found");
                    return;
                }
            }

            // create settings instance
            mpSettings.reset(new SETTINGS);
            // set settings
            QString video_file = ui->VideoURLLineEdit->text();
            QString settings_file;
            // check if the video url is file, ip cam or webcam.
            if ((video_file.startsWith("http",Qt::CaseInsensitive))||(video_file[0].isDigit()))
            {
                // this is ip cam or web cam. Load settings from settings lineedit path
                settings_file = ui->Method1SettingsPathLineEdit->text();
                QFileInfo checkFile(settings_file);
                if ((not checkFile.exists())||(not checkFile.isFile()))
                {
                    QMessageBox::critical(this,"File not found","Settings file"+settings_file+"not found");
                    return;
                }
            }
            else
            {
                // this is video file. load settings in the file path
                QFileInfo video_file_info(video_file);
                QString video_path = video_file_info.absolutePath();
                settings_file = video_path + "/settings.xml";
                QFileInfo settings_file_info(settings_file);
                if ((not settings_file_info.exists())||(not settings_file_info.isFile()))
                {
                    QMessageBox::critical(this,"File not found","Settings file"+settings_file+"not found");
                    return;
                }

            }
            // parse settings file
            mpSettings->parseXml(settings_file.toUtf8().constData(),false);

            // create video buffer instance
            mpVideoModule.reset(new vml::VisionVideoModule());
            // initialize video
            mpVideoModule->initialize(mpSettings,std::string(media_file_info.absoluteFilePath().toUtf8().constData()));
            // create tracker instance
            mpTracker.reset(new vml::MultiTargetVisionTracker());

            // set settings and video to tracker
            mpTracker->setSettings(mpSettings);
            mpTracker->setVideoModule(mpVideoModule);
            // initialize tracker modules
            mpTracker->initializeModules();

            // start method1 timer
            mpTimerMethod1->start(ui->VidIntervalSpinBox->value());

            // trajectory save
            if (ui->TrajectoryOutputCheckBox->isChecked())
            {
                // open trajectory save file
                QString trajectory_file = ui->TrajectoryOutputPathLineEdit->text();
                std::string traj_file_name = trajectory_file.toUtf8().constData();
                mTrajectoryOutputFile.open(traj_file_name.c_str());
                // print header
                mTrajectoryOutputFile << "Start Time,Duration,Trajectory" << std::endl;
                // start trajectory save timer
                mpTimerTrajectorySave->start(1000);  // fire it every seconds.
            }

            // set display window captions
            ui->Viewer1Label->setText("Tracker View");
            ui->Viewer2Label->setText("Undistorted Image");
            ui->Viewer3Label->setText("Foreground & Tracker");
            ui->Viewer4Label->setText("Likelihood");

            // change StartButton text to Stop
            muiStartButton->setText("Stop");
            // disable ui
            ui->VideoURLLineEdit->setEnabled(false);
            ui->VidIntervalSpinBox->setEnabled(false);
            ui->OpenFilePushButton->setEnabled(false);

            ui->pausePushButton->setEnabled(true);
            ui->stepPushButton->setEnabled(true);
            ui->saveVideoImagePushButton->setEnabled(true);

        }
    else
    {
        // change StartButton text to Start
        muiStartButton->setText("Start");

        ui->VideoURLLineEdit->setEnabled(true);
        ui->VidIntervalSpinBox->setEnabled(true);
        ui->OpenFilePushButton->setEnabled(true);

        ui->pausePushButton->setChecked(false);
        ui->pausePushButton->setEnabled(false);
        ui->stepPushButton->setEnabled(false);
        ui->saveVideoImagePushButton->setEnabled(false);

        // reset display labels
        ui->Viewer1Label->setText("");
        ui->Viewer2Label->setText("");
//        ui->Viewer3Label->setText("");

        // stop all timers
        mpTimerMethod1->stop();
        mpTimerTrajectorySave->stop();

        mTrajectoryOutputFile.close();

        // block here to ensure all update methods are stopped.

        // stop video capture
        if (mCapture.isOpened())
            mCapture.release();

        if (mpTracker) {
            mpTracker.reset();
        }

        ui->openCVViewer1->clearImage();
        ui->openCVViewer2->clearImage();
        ui->openCVViewer3->clearImage();

    }
}

void MainWindow::on_quitbutton_clicked()
{
    // exit app

    // if video capture is open, close it
    if ( mCapture.isOpened())
        mCapture.release();

    exit(0);
}

void MainWindow::on_OpenFilePushButton_clicked()
{
    // open file dialog and get file name
    if (mLastOpenedFile.isEmpty())
    {
        mLastOpenedFile = QFileDialog::getOpenFileName(this,tr("Open Video"), "/home/yyoon/VM/data/Giant/Giant-6097", tr("Video Files (*.avi)"));
    }
    else
    {
        mLastOpenedFile = QFileDialog::getOpenFileName(this,tr("Open Video"), mLastOpenedFile, tr("Video Files (*.avi)"));
    }

    // set url to
    muiVideoURLLineEdit->setText(mLastOpenedFile);
}

void MainWindow::updateMethod1()
{
    // run process
    if (not mpTracker->process())
    {
        // signal stop button and return
        this->on_StartButton_toggled(false);
    }

    std::vector<vml::VisionTrajectory>  completed_traj_list;
    if (mFrameCount % 400 == 0)
    {
        // retrieve completed trajectories.
        mpTracker->getCompletedTrajectories(completed_traj_list);
    }

    // save completed trajectory

    // show the image
    cv::Mat image;
    cv::Mat bg;
    cv::Mat fg;
    cv::Mat fgc;
    cv::Mat bpi;
    cv::Mat bpic;
//    std::vector<std::vector<cv::Point> > detected_blobs;
//    detected_blobs = tr1->getBlobs();

    mpTracker->getGrabbedImage().copyTo(image);
//    ui->openCVViewer1->showImage(image);
    // display tracked target
    // display newly detected targets
    if (ui->mthd1DisplayTrackRegionsCheckBox->isChecked())
    {
        mpTracker->drawTrackingRegions(image);
    }
    if (ui->mthd1DisplayNoStartRegionsCheckBox->isChecked())
    {
        mpTracker->drawNoStartRegions(image);
    }
    // display trajectories
    if (ui->mthd1DisplayTrajectoryCheckBox->isChecked())
    {
        mpTracker->drawActiveTrajectories(image);
    }
    if (ui->mthd1DisplayDetectedTargetCheckBox->isChecked())
    {
        mpTracker->drawDetectedTargets(image);
    }
    if (ui->mthd1DisplayInactiveTargetCheckBox->isChecked())
    {
        mpTracker->drawInactiveTargets(image);
    }
    if (ui->mthd1DisplayActiveTargetCheckBox->isChecked())
    {
        mpTracker->drawActiveTargets(image);
    }
    ui->openCVViewer1->showImage(image);

    if (ui->mthd1DisplayMovieWindowCheckBox->isChecked())
    {
//        mpTracker->getGrabbedImage().copyTo(mMovieImage);
//        mpTracker->drawTrackingRegions(mMovieImage);
//        mpTracker->drawActiveTargets(mMovieImage);
//        mpTracker->drawDetectedTargets(mMovieImage);
//        mpTracker->drawActiveTrajectories(mMovieImage);
        cv::cvtColor(image,image,CV_BGR2RGB);
        cv::imshow("TrackerDemo",image);
        cv::waitKey(20);
    }

    // display detected blobs
    mpTracker->getBlobImage().copyTo(fg);
    cv::cvtColor(fg,fgc,CV_GRAY2RGB);
//    for (int i=0;i<detected_targets.size();i++)
//    {
//        cv::drawContours(fgc, detected_targets, i, CV_RGB(0,0,255),2);
//    }
    mpTracker->drawDetectedTargets(fgc);
    mpTracker->drawActiveTargets(fgc);
    ui->openCVViewer2->showImage(fgc);

//    mpTracker->getForeground().copyTo(fg);
//    cv::cvtColor(fg,fgc,CV_GRAY2RGB);
//    mpTracker->drawDetectedTargets(fgc);
//    mpTracker->drawActiveTargets(fgc);
//    ui->openCVViewer4->showImage(fgc);

//    tr1->getBackground().copyTo(bg);
//    cv::cvtColor(bg,bg,CV_BGR2RGB);
//    ui->openCVViewer3->showImage(bg);
    mpTracker->getBackProjection().copyTo(bpi);
    cv::cvtColor(bpi,bpic,CV_GRAY2RGB);
    ui->openCVViewer4->showImage(bpi);
    // for debugging purpose, draw hsv back projection on window 3

    mFrameCount = mpTracker->getCurrentFrameNumber();
    // display frame count
    ui->FrameCountLabel->setText(QString::number(mFrameCount));
    unsigned int nActiveTargets = mpTracker->getNumActiveTargets();
    ui->ActiveTargetCountLabel->setText(QString::number(nActiveTargets));
    unsigned int nTotalTargets = mpTracker->getNumTotalTargets();
    ui->TotalTargetCountLabel->setText(QString::number(nTotalTargets));
    unsigned int nInactiveTargets = mpTracker->getNumInactiveTargets();
    ui->InactiveTargetCountLabel->setText(QString::number(nInactiveTargets));
    unsigned int nCompletedTargets = mpTracker->getNumCompletedTargets();
    ui->completedTargetCountLabel->setText(QString::number(nCompletedTargets));

    image.release();
    mpTracker->copyUndistortImageTo(image);
//    mpTracker->drawUndistortedActiveTargets(image);
    ui->openCVViewer3->showImage(image);

}

void MainWindow::saveTrajectory()
{
    std::vector<vml::VisionTrajectory>	trajectories;	// buffer for completed trajectory
    mpTracker->getCompletedTrajectories(trajectories);

    if (trajectories.size() > 0)
    {
        for(std::vector<vml::VisionTrajectory>::iterator it = trajectories.begin();it!=trajectories.end();it++)
        {
            mTrajectoryOutputFile << it->toXmlString(3) << std::endl;
        }
    }
}



void MainWindow::on_pausePushButton_toggled(bool checked)
{
    if (checked)
    {
        // change caption to Resume
        ui->pausePushButton->setText("Resume");
        ui->VidIntervalSpinBox->setEnabled(true);

        // disable tracking method change here
#if 0
        if (ui->HistogramMethodHSVRadioButton->isChecked())
        {
            tr1->setTrackingMethod(vml::VisionTarget::HIST_HSV);
        }
        else if (ui->HistogramMethodrgChromRadioButton->isChecked())
        {
            tr1->setTrackingMethod(vml::VisionTarget::HIST_rgCHROM);

        }
        else if (ui->HistogramMethodRGBRadioButton)
        {
            tr1->setTrackingMethod(vml::VisionTarget::HIST_RGB);
        }
#endif
        // stop the timers
        mpTimerMethod1->stop();

    }
    else
    {
        // change caption to pause
        ui->pausePushButton->setText("Pause");
        ui->VidIntervalSpinBox->setEnabled(false);
        // start method1 timer
        mpTimerMethod1->start(ui->VidIntervalSpinBox->value());

    }
}

void MainWindow::on_stepPushButton_clicked()
{
    // manually call process
    this->updateMethod1();
}

void MainWindow::on_Method1SettingsLoadPushButton_clicked()
{
    QString video_file = ui->VideoURLLineEdit->text();
    QString settings_file;
    // check if the video url is file, ip cam or webcam.
    if ((video_file.startsWith("http",Qt::CaseInsensitive))||(video_file[0].isDigit()))
    {
        // this is ip cam or web cam. Load settings from settings lineedit path
        settings_file = ui->Method1SettingsPathLineEdit->text();
        QFileInfo checkFile(settings_file);
        if ((not checkFile.exists())||(not checkFile.isFile()))
        {
            QMessageBox::critical(this,"File not found","Settings file"+settings_file+"not found");
            return;
        }
    }
    else
    {
        // this is video file. load settings in the file path
        QFileInfo video_file_info(video_file);
        QString video_path = video_file_info.absolutePath();
        settings_file = video_path + "/settings.xml";
        QFileInfo settings_file_info(settings_file);
        if ((not settings_file_info.exists())||(not settings_file_info.isFile()))
        {
            QMessageBox::critical(this,"File not found","Settings file"+settings_file+"not found");
            return;
        }

    }

//    if(mpSettings)
//    {
//        // if settings instance already exists, delete it
//        mpSettings.reset();
//    }
//    mpSettings.reset(new vml::Settings);    // create a new settings instance
//    mpSettings->parseXml(settings_file.toUtf8().constData(),false);   // parse xml

//    // update settings fields in ui
//    ui->mthd1HistoryLengthSpinBox->setValue(mpSettings->getInt("VisionDetector/historyLength"));
//    ui->mthd1VarThresholdDoubleSpinBox->setValue(mpSettings->getFloat("VisionDetector/varThreshold"));
//    ui->mthd1ShadowDetectionCheckBox->setChecked(mpSettings->getBool("VisionDetector/shadowDetection [bool]"));
//    ui->mthd1BackgroundTrainingNumFrameSpinBox->setValue(mpSettings->getInt("VisionDetector/numTrainingFrame"));
//    std::vector<double> cam_params = mpSettings->getDoubleVector("Camera/distortionParameters","",false);
//    ui->fxLineEdit->setText(QString::number(cam_params[7]));
//    ui->fyLineEdit->setText(QString::number(cam_params[8]));
//    std::stringstream dist_str;
//    for (int i=0;i<6;i++)
//    {
//        dist_str << cam_params[i] << ",";
//    }
//    ui->camDistortionLineEdit->setText(QString(dist_str.str().c_str()));
//    ui->camHeightLineEdit->setText(QString::number(mpSettings->getFloat("Camera/height")));
//    ui->personRadiusLineEdit->setText(QString::number(mpSettings->getFloat("Camera/personRadius")));
//    ui->personHeightLineEdit->setText(QString::number(mpSettings->getFloat("Camera/personHeight")));

//    std::string tr_method = mpSettings->getString("BaseVisionTracker/trackingMethod");

//    if (tr_method == "TrackingMethod_HIST_HSV")
//    {
//        ui->TrackingMethodHSVHistRadioButton->setChecked(true);
//    }
//    else if (tr_method == "TrackingMethod_HIST_rgCHROM")
//    {
//        ui->TrackingMethodrgChromHistRadioButton->setChecked(true);
//    }
//    else if (tr_method == "TrackingMethod_RBChrom")
//    {
//        ui->TrackingMethodrbChromRadioButton->setChecked(true);
//    }
//    else if (tr_method == "TrackingMethod_CIELabKDE")
//    {
//        ui->TrackingMethodCIELabKDERadioButton->setChecked(true);
//    }
//    else if (tr_method == "TrackingMethod_HIST_RGB")
//    {
//        ui->TrackingMethodRGBHistRadioButton->setChecked(true);
//    }
////    else if (tr_method == "TrackingMethod_DLIB_Correlation_Tracking")
////    {
////        ui->TrackingMethodDlibCorrRadioButton->setChecked(true);
////    }
//    else if (tr_method == "TrackingMethod_Hybrid")
//    {
//        ui->TrackingMethodHybridRadioButton->setChecked(true);
//    }
//    else if (tr_method == "TrackingMethod_GeodesicDist")
//    {
//        // not yet supported
//    }
}

void MainWindow::on_Method1SettingsPathToolButton_clicked()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::Directory);
    QString newPath = dialog.getExistingDirectory(this,tr("Settings Path"), ui->Method1SettingsPathLineEdit->text(),QFileDialog::ShowDirsOnly);

    // set url to
    ui->Method1SettingsPathLineEdit->setText(newPath);

}


void MainWindow::on_TrackingMethodHSVHistRadioButton_clicked(bool checked)
{
//    if (not mpSettings)
//    {
//        QMessageBox::critical(this,tr("Default Settings Not Set"),tr("Default Settings hasn't been set yet. Please click Load button to load default settings"));
//        return;
//    }

//    if (checked)
//    {
//        mpSettings->setString("BaseVisionTracker/trackingMethod","TrackingMethod_HIST_HSV");
//    }

}

void MainWindow::on_TrackingMethodrgChromHistRadioButton_clicked(bool checked)
{
//    if (not mpSettings)
//    {
//        QMessageBox::critical(this,tr("Default Settings Not Set"),tr("Default Settings hasn't been set yet. Please click Load button to load default settings"));
//        return;
//    }

//    if (checked)
//    {
//        mpSettings->setString("BaseVisionTracker/trackingMethod","TrackingMethod_HIST_rgCHROM");
//    }
}

void MainWindow::on_TrackingMethodrbChromRadioButton_clicked(bool checked)
{
//    if (not mpSettings)
//    {
//        QMessageBox::critical(this,tr("Default Settings Not Set"),tr("Default Settings hasn't been set yet. Please click Load button to load default settings"));
//        return;
//    }

//    if (checked)
//    {
//        mpSettings->setString("BaseVisionTracker/trackingMethod","TrackingMethod_RBChrom");
//    }
}

void MainWindow::on_TrackingMethodRGBHistRadioButton_clicked(bool checked)
{
//    if (not mpSettings)
//    {
//        QMessageBox::critical(this,tr("Default Settings Not Set"),tr("Default Settings hasn't been set yet. Please click Load button to load default settings"));
//        return;
//    }

//    if (checked)
//    {
//        mpSettings->setString("BaseTracker/trackingMethod","TrackingMethod_HIST_RGB");
//    }
}

void MainWindow::on_TrackingMethodDlibCorrRadioButton_clicked(bool checked)
{
//    if (not mpSettings)
//    {
//        QMessageBox::critical(this,tr("Default Settings Not Set"),tr("Default Settings hasn't been set yet. Please click Load button to load default settings"));
//        return;
//    }

//    if (checked)
//    {
//        mpSettings->setString("BaseTracker/trackingMethod","TrackingMethod_DLIB_Correlation_Tracking");
//    }
}

void MainWindow::on_TrackingMethodHybridRadioButton_clicked(bool checked)
{
//    if (not mpSettings)
//    {
//        QMessageBox::critical(this,tr("Default Settings Not Set"),tr("Default Settings hasn't been set yet. Please click Load button to load default settings"));
//        return;
//    }

//    if (checked)
//    {
//        mpSettings->setString("BaseTracker/trackingMethod","TrackingMethod_Hybrid");
//    }
}

void MainWindow::on_saveVideoImagePushButton_clicked()
{
    // save current grabbe image from tracker
    cv::Mat image;
    if (mpTracker)
    {
        mpTracker->getGrabbedImage().copyTo(image);
        cv::cvtColor(image,image,CV_RGB2BGR);
        cv::imwrite("screenshot.png",image);
    }

}

void MainWindow::on_TrackingMethodCIELabKDERadioButton_clicked(bool checked)
{
//    if (not mpSettings)
//    {
//        QMessageBox::critical(this,tr("Default Settings Not Set"),tr("Default Settings hasn't been set yet. Please click Load button to load default settings"));
//        return;
//    }

//    if (checked)
//    {
//        mpSettings->setString("BaseTracker/trackingMethod","TrackingMethod_CIELabKDE");
//    }
}

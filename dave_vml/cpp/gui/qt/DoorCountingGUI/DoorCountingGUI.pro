#-------------------------------------------------
#
# Project created by QtCreator 2015-07-13T16:53:22
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DoorCountingGUI
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    trackingmethod2.cpp \       # Paul's tracking method
    trackingmethodlegacy.cpp


HEADERS  += mainwindow.h \
    trackingmethod2.h \
    trackingmethodlegacy.h

FORMS    += mainwindow.ui

# vml libs
INCLUDEPATH += ../../../include
LIBS    += -L../../../../build/lib/Debug -lvmlvision -lvmlcore -lvmliot -lvmlutility

# Legacy door counting project
#include(Legacy.pri)

# OpenCV GL viewer
INCLUDEPATH += ../opencv
#DEPENDPATH += ../opencv
#include(../opencv/OpenCV.pri)
SOURCES += ../opencv/cqtopencvviewergl.cpp
HEADERS += ../opencv/cqtopencvviewergl.h

# OpenCV and boost
INCLUDEPATH += /usr/local/include
LIBS += -L/usr/local/lib

# boost
LIBS += -lboost_system -lboost_regex -lboost_thread -lboost_date_time

# misc
LIBS += -lexpat -ludev

# OpenCV
#LIBS += -lopencv_videoio -lopencv_objdetect -lopencv_video -lopencv_ml -lopencv_calib3d -lopencv_features2d -lopencv_highgui -lopencv_imgproc -lopencv_core
LIBS += -lopencv_video -lopencv_videoio -lopencv_calib3d -lopencv_imgcodecs -lopencv_highgui -lopencv_imgproc -lopencv_core

# dlib - obsolete
#INCLUDEPATH += ../../../external/dlib-18.17
#LIBS += -L../../../external/dlib-18.17/dlib/build -ldlib
# figtree
INCLUDEPATH += ../../../external/figtree-0.9.3/include
LIBS += -L../../../external/figtree-0.9.3/lib -lfigtree -lann_figtree_version
# mqtt
INCLUDEPATH += ../../../external/aws-iot-sdk/aws_iot_src/shadow ../../../external/aws-iot-sdk/aws_iot_src/utils ../../../external/aws-iot-sdk/aws_iot_src/protocol/mqtt
LIBS += -lmosquitto -lmosquittopp

# Other projects to be added

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// TrackingMethodLegacy
//
// Implementation of VMS legacy tracking code
//
// Youngrock Yoon
// 2015-07-15

#ifndef TRACKINGMETHODLEGACY_H
#define TRACKINGMETHODLEGACY_H

class TrackingMethodLegacy
{
public:
    TrackingMethodLegacy();
};

#endif // TRACKINGMETHODLEGACY_H

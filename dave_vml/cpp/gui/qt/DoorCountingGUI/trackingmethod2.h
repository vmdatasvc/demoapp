/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// TrackingMethod2
//
// Implementation of Paul's tracking
//
// Youngrock Yoon
// 2015-07-15

#ifndef TRACKINGMETHOD2_H
#define TRACKINGMETHOD2_H

#include <opencv2/core/core.hpp>
#include <opencv2/video/video.hpp>

namespace PaulMethod
{

class TrackingMethod2
{
    enum ForegroundMask {
        FOREGROUND_MASK_CONVEXHULL,
        FOREGROUND_MASK_CIRCULAR
    };

public:
    TrackingMethod2(int height,
                    int width);
    TrackingMethod2(int height,
                    int width,
                    int histlength,
                    float   varthresh,
                    bool    shadowdetect);
    TrackingMethod2(int     height,
                    int     width,
                    int histlength,
                    float   varthresh,
                    bool    shadowdetect,
                    int     max_targets,
                    int     min_target_area,
                    int     connected_component_dist_thresh_max,
                    int     connected_component_dist_thresh_min,
                    float   connected_component_closeness_factor);
    ~TrackingMethod2();

private:
    // parameters
    // background subtractor parameters
    int     historyLength;
    float   varThreshold;
    bool    shadowDetection;

    int     imgWidth;
    int     imgHeight;

    int     maxTargets;
    int     minTargetArea;
    int     connectedComponentDistThresholdMax;
    int     connectedComponentDistThresholdMin;
    float   connectedComponentClosenessFactor;

    ForegroundMask  eFGMask;


    // members
    cv::BackgroundSubtractorMOG2    *bgSubtractor;
    std::vector<std::vector<cv::Point> > contours;
    cv::Mat     foreSegmented;
    cv::Mat     foreground;
    cv::Mat     rawFrame;
    cv::Mat     foregroundOverlayed;
    cv::Mat     background;

    // private member functions
    void JoinConnectedComponents(std::vector<std::vector<cv::Point> > &contours, std::vector<std::vector<cv::Point> > &contours_poly_joined);
    void FindBlobs(cv::Mat &rview, std::vector<std::vector<cv::Point> > &contours_poly_joined);

public:
    // parameter setters

    void setMaxTargets(int max_targets) {maxTargets=max_targets;}
    void setMinTargetArea(int min_target_area) {minTargetArea=min_target_area;}
    void setConnectedComponentDistThresholdMax(int thresh_max) {connectedComponentDistThresholdMax=thresh_max;}
    void setConnectedComponentDistThresholdMin(int thresh_min) {connectedComponentDistThresholdMin=thresh_min;}
    void setConnectedComponentClosenessFactor(float f) {connectedComponentClosenessFactor=f;}

    // getters
    int getHeight() {return imgHeight;}
    int getWidth() {return imgWidth;}

    // output getters
    void getForeground(cv::Mat &fg) {foreground.copyTo(fg);}
    void getForegroundSegmented(cv::Mat &fgseg) {foreSegmented.copyTo(fgseg);}
    void getBackground(cv::Mat &bg) {background.copyTo(bg);}

    // methods
    void process(const cv::Mat &input_frame);

};

} // of namespace PaulMethod

#endif // TRACKINGMETHOD2_H

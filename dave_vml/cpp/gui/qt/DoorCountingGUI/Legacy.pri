# for integrating legacy ait code
INCLUDEPATH = legacy_lib/math \
    legacy_lib/pv \
    legacy_lib/types \
    legacy_lib/vision \
    legacy_lib/vision_blob \
    legacy_lib/vision_blob2 \
    legacy_lib/vision_face \
    legacy_lib/vision_tools \
    legacy_lib/low_level

SOURCES += legacy_lib/vision_blob/DoorCounterMod.cpp \ # legacy ait door counting module
    legacy_lib/vision_blob/BlobTrackerMod.cpp \
    legacy_lib/vision_blob/BlobSizeEstimate.cpp \
    legacy_lib/vision_blob/ForegroundSegmentMod.cpp \
    legacy_lib/vision_blob/MotionDetectionMod.cpp \
    legacy_lib/vision_blob/MultiFrameTrack.cpp \
    legacy_lib/pv/Image.cpp \
    legacy_lib/pv/PvUtil.cpp \
    legacy_lib/low_level/XmlParser.cpp \
    legacy_lib/vision_tools/BackgroundLearner.cpp

HEADERS += legacy_lib/vision_blob/DoorCounterMod.hpp \
    legacy_lib/vision_blob/BlobTrackerMod.hpp \
    legacy_lib/vision_blob/BlobSizeEstimate.hpp \
    legacy_lib/vision_blob/ForegroundSegmentMod.hpp \
    legacy_lib/vision_blob/MotionDetectionMod.hpp \
    legacy_lib/vision_blob/MultiFrameTrack.hpp \
    legacy_lib/pv/ait/BasicMatrix.hpp \
    legacy_lib/pv/ait/Matrix.hpp \
    legacy_lib/pv/ait/Image.hpp \
    legacy_lib/pv/ait/Rectangle.hpp \
    legacy_lib/pv/ait/Vector3.hpp \
    legacy_lib/pv/PvUtil.hpp \
    legacy_lib/low_level/XmlParser.hpp \
    legacy_lib/vision_tools/BackgroundLearner.hpp \
    legacy_lib/math/Condensation.hpp




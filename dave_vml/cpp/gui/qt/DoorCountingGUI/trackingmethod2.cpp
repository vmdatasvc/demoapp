/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// TrackingMethod2
//
// Implementation of Paul's tracking
//
// Youngrock Yoon
// 2015-07-15

#include "trackingmethod2.h"
#include <algorithm>

using namespace std;
using namespace cv;

namespace PaulMethod
{

// define functor class for sort comparison
class ContourAreaCompare {
public:
    int operator() (const std::vector<cv::Point>& contour1,const std::vector<cv::Point>& contour2)
    {
        int i = abs( (int)contourArea(cv::Mat(contour1)) );
        int j = abs( (int)contourArea(cv::Mat(contour2)) );
        return ( i > j );

    }
};

#define SQR(a)  ((a)*(a))

// default constructor. shouldn't be used
TrackingMethod2::TrackingMethod2(int height, int width):
    historyLength(100), // not sure. must check with Paul
    varThreshold(32),
    shadowDetection(true),
    imgWidth(width),
    imgHeight(height),
    maxTargets(500),
    minTargetArea(100),
    connectedComponentDistThresholdMax(50),
    connectedComponentDistThresholdMin(10),
    connectedComponentClosenessFactor(1.2f),
    eFGMask(FOREGROUND_MASK_CONVEXHULL),
    bgSubtractor(NULL),
    foreSegmented(Mat(height,width,CV_8U)),
    foreground(Mat(height,width,CV_8U)),
    rawFrame(Mat(height,width,CV_8UC3)),
    foregroundOverlayed(Mat(height,width,CV_8UC3)),
    background(Mat(height,width,CV_8UC3))
{
    // construct the background subtractor
#if CV_MAJOR_VERSION == 2
    bgSubtractor = new cv::BackgroundSubtractorMOG2(historyLength,varThreshold,shadowDetection);
#elif CV_MAJOR_VERSION == 3
    bgSubtractor = cv::createBackgroundSubtractorMOG2(historyLength,varThreshold,shadowDetection);
#endif

}

TrackingMethod2::TrackingMethod2(int height,
                                 int width,
                                 int histlength,
                                 float varthresh,
                                 bool shadowdetect):
    historyLength(histlength),
    varThreshold(varthresh),
    shadowDetection(shadowdetect),
    imgWidth(width),
    imgHeight(height),
    maxTargets(500),
    minTargetArea(100),
    connectedComponentDistThresholdMax(50),
    connectedComponentDistThresholdMin(10),
    connectedComponentClosenessFactor(1.2f),
    eFGMask(FOREGROUND_MASK_CONVEXHULL),
    bgSubtractor(NULL),
    foreSegmented(Mat(height,width,CV_8U)),
    foreground(Mat(height,width,CV_8U)),
    rawFrame(Mat(height,width,CV_8UC3)),
    foregroundOverlayed(Mat(height,width,CV_8UC3)),
    background(Mat(height,width,CV_8UC3))
{
    // construct the background subtractor   
#if CV_MAJOR_VERSION == 2
     bgSubtractor = new cv::BackgroundSubtractorMOG2(historyLength,varThreshold,shadowDetection);
#elif CV_MAJOR_VERSION == 3
     bgSubtractor = cv::createBackgroundSubtractorMOG2(historyLength,varThreshold,shadowDetection);
#endif

} // of constructor

TrackingMethod2::TrackingMethod2(int height,
                                 int width,
                                 int histlength,
                                 float   varthresh,
                                 bool    shadowdetect,
                                 int     max_targets,
                                 int     min_target_area,
                                 int     connected_component_dist_thresh_max,
                                 int     connected_component_dist_thresh_min,
                                 float   connected_component_closeness_factor):
    historyLength(histlength),
    varThreshold(varthresh),
    shadowDetection(shadowdetect),
    imgWidth(width),
    imgHeight(height),
    maxTargets(max_targets),
    minTargetArea(min_target_area),
    connectedComponentDistThresholdMax(connected_component_dist_thresh_max),
    connectedComponentDistThresholdMin(connected_component_dist_thresh_min),
    connectedComponentClosenessFactor(connected_component_closeness_factor),
    eFGMask(FOREGROUND_MASK_CONVEXHULL),
    bgSubtractor(NULL),
    foreSegmented(Mat(height,width,CV_8U)),
    foreground(Mat(height,width,CV_8U)),
    rawFrame(Mat(height,width,CV_8UC3)),
    foregroundOverlayed(Mat(height,width,CV_8UC3)),
    background(Mat(height,width,CV_8UC3))
{
    // construct the background subtractor
#if CV_MAJOR_VERSION == 2
    bgSubtractor = new cv::BackgroundSubtractorMOG2(historyLength,varThreshold,shadowDetection);
#elif CV_MAJOR_VERSION == 3
    bgSubtractor = cv::createBackgroundSubtractorMOG2(historyLength,varThreshold,shadowDetection);
#endif
}

TrackingMethod2::~TrackingMethod2()
{
    if (bgSubtractor)
        delete bgSubtractor;
}

void TrackingMethod2::process(const Mat &input_frame)
{
    input_frame.copyTo(rawFrame);

    // rectification goes here? if needed

    // run background subtraction
#if CV_MAJOR_VERSION == 2
    bgSubtractor->operator ()(rawFrame,foreground);
#elif CV_MAJOR_VERSION == 3
    bgSubtractor->apply(rawFrame,foreground);
#endif

    // save background
    bgSubtractor->getBackgroundImage(background);

    // blur foreground
    blur(foreground,foreground,cvSize(13,13));

    threshold(foreground,foreground, 75, 255, THRESH_BINARY);

    Mat foreHull;
    {
        foreground.copyTo(foreHull);
        foreHull.setTo(Scalar(0,0,0));

        vector<vector<Point> > contours_poly_joined;
        FindBlobs(foreground, contours_poly_joined);

        // find foreground mask
        if (eFGMask == FOREGROUND_MASK_CONVEXHULL)
        {
            // extract convex hull of the detected object
            vector<vector<Point> > hull;
            for (unsigned int i=0;i < contours_poly_joined.size();i++)
            {
                vector<Point> hull_temp;
                convexHull((Mat(contours_poly_joined[i])),hull_temp);
                hull.push_back(hull_temp);
            }

            for (int i=0;i<contours_poly_joined.size() && i < maxTargets;i++)
            {
                drawContours(foreHull, hull, i, CV_RGB(255,255,255),-1);
            }

            // Get foreground blobs by using the extracted foreground binary image as a mask of the raw image
            rawFrame.copyTo(foreSegmented);    // clear foreSegmented
            rawFrame.copyTo(foreSegmented, foreHull);

            for (int i=0;i<contours_poly_joined.size()&&i<maxTargets;i++)
            {
                drawContours(foreSegmented, hull, i, CV_RGB(255,0,0),2);
            }
        }
        else if (eFGMask == FOREGROUND_MASK_CIRCULAR)
        {
            vector<Point2f> center_joined( contours_poly_joined.size() );
            vector<float> radius_joined( contours_poly_joined.size() );
            int r;

            for(unsigned int i = 0; i < contours_poly_joined.size(); i++ ) {
                minEnclosingCircle( (Mat)contours_poly_joined[i], center_joined[i], radius_joined[i] );
            }
            for(unsigned int i = 0; i < contours_poly_joined.size(); i++ ) {
                r = MAX(5, radius_joined[i]*1.1);
                circle(foreHull, center_joined[i], r, CV_RGB(255,255,255), -1);	// -- Center
            }

            // -- Get foreground blobs by using the extracted foreground binary image as a mask of the raw image
            //rawFrame.copyTo(foreSegmented, foreground);
            rawFrame.copyTo(foreSegmented, foreHull);

            for(unsigned int i = 0; i < contours_poly_joined.size(); i++ ) {
                r = MAX(5, radius_joined[i]*1.1);
                circle(foreSegmented, center_joined[i], r, CV_RGB(255,255,255), 2);	// -- Center
            }
        }
        // else do nothing


    }
}


//////////////////////////////////////////////////////////////////////////////////
/* Below are Paul's code */
// -- Join two close contours using minEnclosingCircle
void TrackingMethod2::JoinConnectedComponents(std::vector<std::vector<cv::Point> > &contours, std::vector<std::vector<cv::Point> > &contours_poly_joined)
{
    bool isConverged = false;

    vector<vector<Point> > contours_poly( contours.size() );
    //vector<Rect> boundRect( contours.size() );
    vector<Point2f>center( contours.size() );
    vector<float>radius( contours.size() );
    vector<float>area( contours.size() );

    bool isConnected[contours_poly.size()][contours_poly.size()];
    for(unsigned int i = 0; i < contours_poly.size(); i++ )
        for(unsigned int j = i; j < contours_poly.size(); j++ ) {
            isConnected[i][j] = false;
            isConnected[j][i] = false;
        }

    for(unsigned int i = 0; i < contours.size(); i++) {
        approxPolyDP( Mat(contours[i]), contours_poly[i], 5, true );
        //boundRect[i] = boundingRect( Mat(contours_poly[i]) );
        minEnclosingCircle( (Mat)contours_poly[i], center[i], radius[i] );
        area[i] = fabs(contourArea(Mat(contours[i])));
    }

    for(unsigned int i = 0; i < contours_poly.size(); i++ )
        isConnected[i][i] = true;

    while (isConverged == false) {
        isConverged = true;

        for(unsigned int i = 0; i < contours_poly.size()-1; i++ ) {

            if(area[i] < minTargetArea) isConnected[i][i] = false;
            else {

                for(unsigned int j = i+1; j < contours_poly.size(); j++ ) {
                    // -- Distance btw two contours based on minEnclosingCircles of two blobs
                    float dist = sqrt(SQR(center[i].x - center[j].x) + SQR(center[i].y - center[j].y)) - radius[i] - radius[j];
                    if(isConnected[j][j] && !(isConnected[i][j] || isConnected[j][i])
                            && (dist < radius[i]*connectedComponentClosenessFactor || dist < radius[j]*connectedComponentClosenessFactor || dist < connectedComponentDistThresholdMin)
                            && dist < connectedComponentDistThresholdMax) {
                        isConnected[i][j] = true;
                        isConnected[j][i] = true;
                        isConnected[j][j] = false;

                        isConverged = false;

                        center[i].x = (center[i].x + center[j].x)*0.5;	center[j].x = center[i].x;
                        center[i].y = (center[i].y + center[j].y)*0.5;	center[j].y = center[i].y;

                        radius[i] = radius[i] + radius[j] + dist*0.5; radius[j] = radius[i];

                    }
                }
            }
        }

    }


        // -- Join connected components
        for(unsigned int i = 0; i < contours_poly.size(); i++ ) {
            if(isConnected[i][i] == false || area[i] < minTargetArea) continue;

            std::vector<cv::Point> temp_contour;
            for(unsigned int j = i; j < contours_poly.size(); j++ ) {
                if(isConnected[i][j]) {
                    for(unsigned int k = 0; k < contours_poly[j].size(); k++ ) {
                        temp_contour.push_back(contours_poly[j][k]);
                    }
                    isConnected[i][j] = false;
                    isConnected[j][i] = false;
                    isConnected[j][j] = false;
                }
            }

            if(temp_contour.size() > 0)
                contours_poly_joined.push_back(temp_contour);
        }
}

void TrackingMethod2::FindBlobs(Mat &rview, vector<vector<Point> > &contours_poly_joined)
{
    std::vector<std::vector<cv::Point> > contours;
    //vector<Vec4i> hierarchy;

    Mat rview2;// = Mat::zeros(rview.size(), CV_8UC1);
    rview.copyTo(rview2);
    //cvtColor(rview,rview2,CV_RGB2GRAY);

    // -- Find contours of blobs in the segmented foreground
    findContours( rview2, contours, CV_RETR_LIST, CV_CHAIN_APPROX_TC89_KCOS);

    if(contours.size() == 0) return;

    // -- Remove too small objects
    for(unsigned int i=0; i<contours.size(); i++)
    {
        double area = fabs(contourArea(Mat(contours[i])));
        if(area < minTargetArea)
            contours.erase(contours.begin()+i);
    }

    if(contours.size() == 0) return;

    // -- Sort contours
    if(contours.size() > 1)
        std::sort(contours.begin(), contours.end(), ContourAreaCompare());

        //	std::vector<cv::Point> smallestContour = contours[contours.size()-1];
        //	std::vector<cv::Point> biggestContour = contours[0];


    // -- Find connected components
    JoinConnectedComponents(contours, contours_poly_joined);

}

/*
 // not used
void UndistortImage(Mat img_raw, Mat &rview)
{
    // -- Intrinsic parameters of wide-angle Movius 1080P ActionCam
    double mFx = 315.9597796948216;
    double mFy = 354.6229122080455;
    double mCx = 319.5;
    double mCy = 239.5;
    double mK1 = 0.326609061343013;
    double mK2 = -0.02704473733547915;
    double mK3 = 0.001643749938211476;
    double mK4 = 0.6915312518425791;

    Mat mCameraMatrix = Mat::eye(3, 3, CV_64F);
    Mat mDistCoeffs = Mat::zeros(8, 1, CV_64F);
    Mat mNewCameraMatrix;

    mCameraMatrix.at<double>(0,0) = mFx; mCameraMatrix.at<double>(1,1) = mFy;
    mCameraMatrix.at<double>(0,2) = mCx; mCameraMatrix.at<double>(1,2) = mCy;

    mDistCoeffs.at<double>(0) = mK1; mDistCoeffs.at<double>(1) = mK2;
    mDistCoeffs.at<double>(4) = mK3; mDistCoeffs.at<double>(5) = mK4;

    // -- Scale the undistorted image
    mCameraMatrix.copyTo(mNewCameraMatrix);
    mNewCameraMatrix.at<double>(0,0) *= UNDISTORT_SCALING_FACTOR;
    mNewCameraMatrix.at<double>(1,1) *= UNDISTORT_SCALING_FACTOR;

    undistort(img_raw, rview, mCameraMatrix, mDistCoeffs, mNewCameraMatrix);
}
*/

} // of namespace PaulMethod

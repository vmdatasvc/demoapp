#ifndef FACEDETECTMETHOD1_H
#define FACEDETECTMETHOD1_H

#include "core/Image.hpp"
#include "core/Video.hpp"
//#include "core/Settings.hpp"

#include "opencv2/objdetect/objdetect.hpp"

#include "modules/ml/mlfeature.hpp"
#include "modules/ml/mlsvm.hpp"

class FaceDetectMethod1
{
public:
    FaceDetectMethod1(std::string url,float sF, int mNbr, boost::shared_ptr<vml::Settings> settings);
    //FaceDetectMethod1(std::string url,float sF, int mNbr);
    ~FaceDetectMethod1();

    bool detect(cv::Rect2f roi);
    void drawValidTargets(cv::Mat &image) const;
    void drawValidTargetsFiltered(cv::Mat &image) const;
    void drawValidTargetsAF(cv::Mat &image) const;
    void drawROI(cv::Mat &image, cv::Rect2f roi) const;

    const cv::Mat&  getGrabbedImage(void);

    unsigned int getNumDetectedFaces(void);
    unsigned int getNumDetectedFacesFiltered(void);
    unsigned int getNumDetectedFacesAF(void);

    unsigned int getCurMinFaceSize(void);
    unsigned int getCurMaxFaceSize(void);

    double getFPS(void);
    int getFrameCount(void) {return mDetectFrameCount;}

    void FPFiltering(cv::Mat &faceCand, std::vector<cv::Rect> &face_vecs);
    bool FPFilteringByColor(cv::Mat &cimg);
    bool FPFilteringByHOG(cv::Mat &cimg);

    void saveFaces(cv::Mat &image, std::string path, int fno, int flag);
    void saveFacesWithRatio(cv::Mat &image, std::string path, int fno, int flag, double ratio);

    bool mFPFiltering;

private:

    cv::Mat	inputimage;
    cv::Mat maskedImage;

    cv::VideoCapture        mCapture;
    cv::CascadeClassifier	mpCascade;
    float scaleFactor;
    int minNeighbors;
    int minFaceSize;
    int maxFaceSize;
    int mDetectFrameCount;

    unsigned int mMinFaceSize;
    unsigned int mMaxFaceSize;
    int mFPFilterFeature;  // 0: Averaged color, 1: HOG feature

    std::vector<cv::Rect>	mFaces;
    std::vector<cv::Rect>	mFacesAfterFiltering;
    std::vector<cv::Rect>	mFalsePositiveFaces;

    // for a face filter
    boost::shared_ptr<vml::MLSVM> psvm;
    boost::shared_ptr<vml::MLFeature> pfeature;


};

#endif // FACEDETECTMETHOD1_H

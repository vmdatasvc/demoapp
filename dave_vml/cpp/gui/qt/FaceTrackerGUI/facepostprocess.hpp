#ifndef FACEPOSTPROCESS_HPP
#define FACEPOSTPROCESS_HPP

#include "mysql_connection.h"
#include "cppconn/driver.h"
#include "cppconn/exception.h"
#include "cppconn/resultset.h"
#include "cppconn/statement.h"
#include "cppconn/resultset_metadata.h"
#include "cppconn/prepared_statement.h"

#include <curl/curl.h>

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <ctime>

#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>

#include <../external/jsonByNlohmann/json.hpp>   // using json = nlohmann::json;

#include <aws/core/Aws.h>


namespace vml {

namespace fpp {

#define AWS_RDS_TESTBED_URL  "vmiotstore.cxbttlf9n1ld.us-east-1.rds.amazonaws.com"
#define AWS_RDS_TESTBED_ID   "omniadmin"
#define AWS_RDS_TESTBED_PWD  "videoMININGlabs2015"

#define SFTP_URL        "sftp://demoftp.videomining.com/images/"
#define SFTP_USERNAME   "demographics"
#define SFTP_PASSWD     "demoftp"

#define ACCESS_KEY_ID   "AKIAJ4X3HZL7LDNV4Y3A"
#define SECRET_KEY      "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"

#define MAX_NUM_FACE_ANALYSIS   15

#define RDS_UPDATE_ERROR        -1
#define RDS_UPDATE_UNCHANGED    0
#define RDS_UPDATE_A_ROW        1

#define GENDER_UNKNOWN          100


typedef struct
{
   int year;
   int month;
   int day;
   int hour;
   int min;
   int sec;

} DateTime_t;

typedef struct
{
    std::string faceId;
    std::string faceURL;
//    cv::Mat     data;

} LocalFaceData;

struct FtpFile {
  const char *filename;
  FILE *stream;
};

struct MemoryStruct {
  char *memory;
  size_t size;
};

class FacePostProcess
{

 public:
    FacePostProcess();
    ~FacePostProcess();

    std::string dateTostrUTC(int year, int month, int day, int hour, int min, int sec);
    std::string dateStructureTostrUTC(DateTime_t dt);

    int postProcessDMG(std::string storeName, std::string devName, DateTime_t sdt, DateTime_t edt);
    bool postProcessJBT();

    bool getConnectedStatusToRDS() {return mIsConnectedToRDS;}
    bool getConnectedStatusToSFTP() {return mIsConnectedToSFTP;}
    bool getConnectedStatusToAmzRknAPI() {return mIsConnectedToAmzRknAPI;}

     std::vector<LocalFaceData> getFaceDataSet() {return mLocalFaceData;}
    int getTotalFaceDataSize() {return mLocalFaceData.size();}

    // later, make the below protected.
     void setDBconfigRDS(std::string dbURL, std::string dbID, std::string dbPWD, std::string storeName, std::string devName);
     bool createLocalDB();
     bool connectToRDS();
     void disconnectToRDS();
     bool getFaceDataFromRDS(std::string startDateTime, std::string endDateTime);
     int updateDMGdataToRDS(double gender, double age, std::string faceId, bool mode);
     int  updateMultipleDMGdataToRDS(std::vector<double> vgender, std::vector<double> vage, std::vector<std::string> vfaceId);

     void setDBConfigSFTP(std::string dbURL, std::string dbID, std::string dbPWD);
     void connectToSFTP();
     void disconnectToSFTP();

     //bool getFaceImagesFromSFTP();

     static std::size_t local_fwrite(void *buffer, std::size_t size, std::size_t nmemb, void *stream);
     bool getImageFileFromSFTP(std::string filename, std::string targetfilename);

     static std::size_t WriteMemoryCallback(void *contents, std::size_t size, std::size_t nmemb, void *userp);
     bool getBufferedDataSFTP(std::string filename, cv::Mat &fdata);


     void connectToAmzRknAPI();
     void disconnectToAmzRknAPI();
     void setConfigAmzRknAPI(std::string access_id, std::string secret_key, std::string token);
     bool getFaceMetaDataByAmzRknAPI(cv::Mat imgMat, double &age, double &ageRange, double &gender, double &genderScore);


 private:

    // For AWS RDS
    std::string mDbID, mDbURL, mDbPWD, mStoreName, mDevName, mTableMetaData, mTableImages;
    std::string mTableMetaDataID, mTableImagesID, mTableImagesURL, mTableMetaDataStartTimeTrack, mTableMetaDataEndTimeTrack;

    // For SFTP server
    std::string mSFTPurl, mSFTPid, mSFTPpwd,mSFTPuserpwd;

    // For Amazon Rekognition API
     Aws::SDKOptions options;

     Aws::String keyId;
     Aws::String keyPw;
     const Aws::String token;

    sql::Driver         *mDbDriver;
    sql::Connection     *mDbConn;
    sql::ResultSet      *mDbResultSet;
    sql::ResultSetMetaData  *mDbResultMetaData;

    std::vector<LocalFaceData>  mLocalFaceData;

    bool mIsConnectedToRDS;
    bool mIsConnectedToSFTP;
    bool mIsConnectedToAmzRknAPI;

};

}  // using namespace fpp

} //  namespace vml

#endif // FACEPOSTPROCESS_HPP

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/****************************************************************************/
//
//      Post-processing (for Demographics recognition)
//   1. Access to AWS RDS DB using mysql and cppconn
//   2. Save data to local DB by a proper query
//   3. Using local DB, fetch Images from face image storage
//   4. Demographics recognition using Cloud API and/or legacy module
//   5. Update the RDS DB (holistically or individually?)
//
//   Dave Donghun Kim
//   2017.2.7
/****************************************************************************/

//#include <aws/core/Aws.h>
#include <aws/core/auth/AWSCredentialsProvider.h>
#include <aws/rekognition/RekognitionClient.h>
#include <aws/rekognition/model/DetectFacesRequest.h>
#include <aws/rekognition/model/DetectFacesResult.h>
#include <aws/core/utils/Outcome.h>


#include <modules/utility/base64.h>

#include "modules/utility/DateTime.hpp"
#include "facepostprocess.hpp"
#include <iostream>
#include <ostream>

#define DEBUG_PP    1

namespace vml {

namespace fpp {

FacePostProcess::FacePostProcess():
    mIsConnectedToRDS(false),
    mIsConnectedToSFTP(false),
    mIsConnectedToAmzRknAPI(false)
{

    // clear local data
    mLocalFaceData.clear();

}

FacePostProcess::~FacePostProcess()
{

}

// 1. Given a time period, fetch faceIds and faceURLs from RDS DB and images from the sftp server by faceURLs directly

//      -. get the list of ids from the fac010_demogphcs and get the lists of faceURL from the demogphcs_images

//          mysql --user=omniadmin --password=videoMININGlabs2015 -h vmiotstore.cxbttlf9n1ld.us-east-1.rds.amazonaws.com -e
//          "USE giant_6072; SELECT faceId,faceURL from demogphcs_images WHERE faceTimestamp BETWEEN start_time and end_time"

//          mysql --user=omniadmin --password=videoMININGlabs2015 -h vmiotstore.cxbttlf9n1ld.us-east-1.rds.amazonaws.com -e
//          "USE giant_6072; SELECT faceId from fac010_demogphcs WHERE startTimeTrack BETWEEN start_time and end_time"

//          mysql --user=omniadmin --password=videoMININGlabs2015 -h vmiotstore.cxbttlf9n1ld.us-east-1.rds.amazonaws.com -e
//          "USE giant_6072; SELECT faceURL from demogphcs_images,fac010_demogphcs WHERE fac010_demogphcs.faceId=demogphcs_images.faceId
//           AND fac010_demogphcs.startTimeTrack BETWEEN start_time and end_time"

//      -. connect to the SFTP server to access the url
//           RestAPI GET url to dataBuffer or file (processing one by one)


// 2. processes
//      a. DMGRecog process
//      b. JoiningTracks process

// 3. Reaction
//      a. (DMGRecog process) Update the table according to faceIds in fac010_demgphcs
//      b. (JoiningTracks process)Merge the table in fac010_demgphcs and change the name of faceURL in demogphcs_images
//
//      mysql UPDATE fac010_demogphcs SET gender='',age='',eth0='',eth1='',eth2='' where faceId='xxxx';
//      mysql DELETE form fac010_demogphcs where faceId='xxxx';


void FacePostProcess::setDBconfigRDS(std::string dbURL, std::string dbID, std::string dbPWD, std::string storeName, std::string devName)
{

    mDbID = dbID;
    mDbURL = dbURL;
    mDbPWD = dbPWD;

    mStoreName = storeName;
    mDevName = devName;
    mTableMetaData = devName + "_demogphcs";
    mTableImages = "demogphcs_images";

    mTableMetaDataID = mTableMetaData + ".faceId";
    mTableImagesID = mTableImages + ".faceId";

    mTableImagesURL = mTableImages + ".faceURL";

    mTableMetaDataStartTimeTrack = mTableMetaData + ".startTimeTrack";
    mTableMetaDataEndTimeTrack = mTableMetaData + ".endTimeTrack";


    //connectToRDS();
}

bool FacePostProcess::connectToRDS()
{

    try {

         // -- Create a connection
         mDbDriver = get_driver_instance();
         mDbConn = mDbDriver->connect(mDbURL, mDbID, mDbPWD);

         // -- Connect to the MySQL test database
         mDbConn->setSchema(mStoreName.c_str());

         if(DEBUG_PP)
         {
            std::cout << "SQL Driver Name: " << mDbDriver->getName() << std::endl;
            std::cout << "Connecting as " << mDbID << "@" << mDbURL << " using password " << mDbPWD << std::endl;
            std::cout << "Setting schema to " << mStoreName.c_str() << std::endl;
         }

         mIsConnectedToRDS = true;

     } catch (sql::SQLException &e) {
         /*
         The MySQL Connector/C++ throws three different exceptions:

         - sql::MethodNotImplementedException (derived from sql::SQLException)
         - sql::InvalidArgumentException (derived from sql::SQLException)
         - sql::SQLException (derived from std::runtime_error)
         */

         std::cout << "# ERR: SQLException in " << __FILE__;
         std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
         std::cout << "# ERR: " << e.what();
         std::cout << " (MySQL error code: " << e.getErrorCode();
         std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;

         mIsConnectedToRDS = false;
         return EXIT_FAILURE;
     } catch (std::runtime_error &e) {
         std::cout << "# ERR: runtime_error in " << __FILE__;
         std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
         std::cout << "# ERR: " << e.what() << std::endl;
         std::cout << "not ok 1 - examples/resultset.cpp" << std::endl;

         mIsConnectedToRDS = false;
         return EXIT_FAILURE;
     }

      return true;
}

void FacePostProcess::disconnectToRDS()
{
    mIsConnectedToRDS = false;
    delete mDbConn;

}

// IN: Query of time period
bool FacePostProcess::getFaceDataFromRDS(std::string startDateTime, std::string endDateTime)
{
    // Query time: local time to utc
    //std::string sQueryStartTime;
    //std::string sQueryEndTime;

    std::ostringstream sQuery;

//    sQuery << "mysql --user=" << mDbID << " --password=" << mDbPWD << " -h " << mDbURL << " -e " << "\"USE " << mStoreName << "; SELECT "
//               << mTableImagesID << "," << mTableImagesURL << " from " << mTableImages << "," << mTableMetaData << " WHERE " << mTableMetaDataID << "=" << mTableImagesID
//               << " AND " <<  mTableMetaDataStartTimeTrack << ">" << startDateTime << " AND " <<  mTableMetaDataEndTimeTrack << "<" << endDateTime << "\"";

    sQuery << "SELECT "
           << mTableImagesID << "," << mTableImagesURL << " from " << mTableImages << "," << mTableMetaData << " WHERE " << mTableMetaDataID << "=" << mTableImagesID
           << " AND " <<  mTableMetaDataStartTimeTrack << ">" << startDateTime << " AND " <<  mTableMetaDataEndTimeTrack << "<" << endDateTime;

    if(DEBUG_PP)
        printf("%s\n",sQuery.str().c_str());

    //system(sQuery.str().c_str());

    sql::Statement *stmt;

    try {

        stmt = mDbConn->createStatement();
        mDbResultSet = stmt->executeQuery(sQuery.str().c_str());
        // mDbResultMetaData = mDbResultSet->getMetaData();

        // Parse the data into a data structure

        mDbResultSet->first();

        mLocalFaceData.clear();

        LocalFaceData fdata;
        do {
            fdata.faceId=""; fdata.faceURL="";

            fdata.faceId = mDbResultSet->getString("faceId");
            fdata.faceURL = mDbResultSet->getString("faceURL");
            mLocalFaceData.push_back(fdata);

            if(DEBUG_PP)
            {
                printf("faceId: %s, faceURL: %s\n",fdata.faceId.c_str(), fdata.faceURL.c_str());
            }


        } while (mDbResultSet->next());


        delete stmt;

    } catch (sql::SQLException &e) {
        /*GetGen
         The MySQL Connector/C++ throws three different exceptions:

         - sql::MethodNotImplementedException (derived from sql::SQLException)
         - sql::InvalidArgumentException (derived from sql::SQLException)
         - sql::SQLException (derived from std::runtime_error)
         */

         std::cout << "# ERR: SQLException in " << __FILE__;
         std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
         std::cout << "# ERR: " << e.what();
         std::cout << " (MySQL error code: " << e.getErrorCode();
         std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
         return false;

         //return EXIT_FAILURE;
     } catch (std::runtime_error &e) {
         std::cout << "# ERR: runtime_error in " << __FILE__;
         std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
         std::cout << "# ERR: " << e.what() << std::endl;
         std::cout << "not ok 1 - examples/resultset.cpp" << std::endl;
         return false;
         //return EXIT_FAILURE;
     }


    return true;

}

// For the update of single faceId
// UPDATE fac10_demographics SET gender=1.0,age=32 WHERE faceId="xxxx"
// if a mode is true, update data. but, if a mode is false, delete the faceId because it is a wrong face.
int FacePostProcess::updateDMGdataToRDS(double gender, double age, std::string faceId, bool mode)
{
   // For a given table and faceId, update demographics data
    std::ostringstream sQuery;


    if(mode)
    {
        sQuery << "UPDATE "
           << mTableMetaData << " SET " << "gender=" << gender << "," << "age=" << age
           << " WHERE " << "faceId=\'" << faceId << "\';";
    }
    else
    {
        // TO-DO: delete the rows in facxxx_demogphcs and demogphcs_images with respect to faceId
        // Temporarily
        return RDS_UPDATE_ERROR;
    }

    if(DEBUG_PP)
        printf("%s\n",sQuery.str().c_str());

    sql::Statement *stmt;    

    int ret=1;
    try {
        stmt = mDbConn->createStatement();
        if(mode)
        {
            ret = stmt->executeUpdate(sQuery.str().c_str());

            if(DEBUG_PP)
            {
                std::cout <<"The number of rows changed: " << ret << std::endl;
            }
        }

        delete stmt;

    } catch (sql::SQLException &e) {
        /*
         The MySQL Connector/C++ throws three different exceptions:

         - sql::MethodNotImplementedException (derived from sql::SQLException)
         - sql::InvalidArgumentEcreateStatementxception (derived from sql::SQLException)
         - sql::SQLException (derived from std::runtime_error)
         */

         std::cout << "# ERR: SQLException in " << __FILE__;
         std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
         std::cout << "# ERR: " << e.what();
         std::cout << " (MySQL error code: " << e.getErrorCode();
         std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
         ret= 100;

         //return EXIT_FAILURE;
     } catch (std::runtime_error &e) {
         std::cout << "# ERR: runtime_error in " << __FILE__;
         std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
         std::cout << "# ERR: " << e.what() << std::endl;
         std::cout << "not ok 1 - examples/resultset.cpp" << std::endl;
         ret=100;
         //return EXIT_FAILURE;
     }

    if(ret == 0)
        return RDS_UPDATE_UNCHANGED;
    else if(ret == 1)
        return RDS_UPDATE_A_ROW;
    else
        return RDS_UPDATE_ERROR;

}

// Basic function for updating multiple dmg data to RDS
int FacePostProcess::updateMultipleDMGdataToRDS(std::vector<double> vgender, std::vector<double> vage, std::vector<std::string> vfaceId)
{
   // For a given table and faceId, update demographics data
    std::ostringstream sQuery;

    //sQuery << "UPDATE "
    //       << mTableMetaData << " SET " << "gender=" << gender << "," << "age=" << age
    //       << " WHERE " << "faceId=\'" << faceId << "\';";
    sQuery << "UPDATE "
           << mTableMetaData << " SET " << "gender=?" << "," << "age=?"
           << " WHERE " << "faceId=?;";


    if(DEBUG_PP)
        printf("%s\n",sQuery.str().c_str());

    sql::Statement *stmt;
    sql::PreparedStatement *pstmt;


    pstmt = mDbConn->prepareStatement(sQuery.str().c_str());

    for(int i=0; i< vgender.size(); i++)
    {
        pstmt->setDouble(1,vgender[i]);
        pstmt->setDouble(2,vage[i]);
        pstmt->setString(3,vfaceId[i]);
        pstmt->executeUpdate();
        pstmt->clearParameters();
    }

    return 0;
}


//---------------------------------------------------------------------//
// set the config of image Database (SFTP)
void FacePostProcess::setDBConfigSFTP(std::string dbURL, std::string dbID, std::string dbPWD)
{
    mSFTPurl = dbURL;
    mSFTPid  = dbID;
    mSFTPpwd = dbPWD;
    mSFTPuserpwd = dbID+":"+dbPWD;

}

void FacePostProcess::connectToSFTP()
{
    mIsConnectedToSFTP = true;
    // For SFTP server
    curl_global_init(CURL_GLOBAL_ALL);

}

void FacePostProcess::disconnectToSFTP()
{
    mIsConnectedToSFTP = false;

    // For SFTP server
    curl_global_cleanup();
}

/*
// Load image from the SFTP server
bool FacePostProcess::getFaceImagesFromSFTP()
{
    if( mLocalFaceData.size() < 1)
        return false;

    int i=0;

    // using REST API
    while( i < mLocalFaceData.size())
    {
        getBufferedDataSFTP(mLocalFaceData[i].faceURL,mLocalFaceData[i].data);

    }

    return true;
}
*/

std::size_t FacePostProcess::local_fwrite(void *buffer, std::size_t size, std::size_t nmemb, void *stream)
{
  struct FtpFile *out=(struct FtpFile *)stream;

  if(out && !out->stream)
  {
    /* open file for writing */
    out->stream=fopen(out->filename, "wb");

    if(!out->stream)
      return -1; /* failure, can't open file to write */
  }

  return fwrite(buffer, size, nmemb, out->stream);
}


std::size_t FacePostProcess::WriteMemoryCallback(void *contents, std::size_t size, std::size_t nmemb, void *userp)
{
    std::size_t realsize = size * nmemb;

    struct MemoryStruct *mem = (struct MemoryStruct *)userp;

    mem->memory = (char *)std::realloc(mem->memory, mem->size + realsize + 1);

    if(mem->memory == NULL)
    {
        /* out of memory! */
        printf("not enough memory (realloc returned NULL)\n");
        return 0;
    }
    //std::vector<uchar>::size_type sz = std::strlen((const char *)chunk.memory);
    //long imgBuffLength = static_cast<long>(chunk.size);
    //std::vector<char>::size_type size = strlen(chunk.memory);
    //std::vector<uchar> bufferedData((uchar *)chunk.memory, imgBuffLength);  //chunk.memory+ size); //

    //cv::Mat data_mat(bufferedData);
    //fdata = cv::imdecode(bufferedData, cv::IMREAD_UNCHANGED);//  cv::IMREAD_ANYCOLOR);

    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;

    return realsize;
}


// download to where and with what name
bool FacePostProcess::getImageFileFromSFTP(std::string filename, std::string targetfilename)
{
      CURL *curlhandle;
      CURLcode res;
      bool ret;

      struct FtpFile ftpfile={
              targetfilename.c_str(),  // name to store the file as if successful
              NULL
      };

      std::string downloadfile_url = mSFTPurl + filename;

      curl_global_init(CURL_GLOBAL_DEFAULT);

      curlhandle = curl_easy_init();

      if(curlhandle)
      {

         if(DEBUG_PP)
         {
            // Switch on full protocol/debug output
            curl_easy_setopt(curlhandle, CURLOPT_VERBOSE, 1L);

            // For getting a body with error message in the first place
            curl_easy_setopt(curlhandle, CURLOPT_FAILONERROR, 0);
          }

          curl_easy_setopt(curlhandle, CURLOPT_URL, downloadfile_url.c_str());

          // Define our callback to get called when there's data to be written
          curl_easy_setopt(curlhandle, CURLOPT_WRITEFUNCTION, local_fwrite);

          // Set a pointer to our struct to pass to the callback
          curl_easy_setopt(curlhandle, CURLOPT_WRITEDATA, &ftpfile);

          curl_easy_setopt(curlhandle, CURLOPT_USERNAME, mSFTPid.c_str());

          curl_easy_setopt(curlhandle, CURLOPT_PASSWORD, mSFTPpwd.c_str());

          // ssh
          curl_easy_setopt(curlhandle,CURLOPT_SSH_AUTH_TYPES, CURLSSH_AUTH_PASSWORD);

          res = curl_easy_perform(curlhandle);

          // always cleanup
          curl_easy_cleanup(curlhandle);

          if(CURLE_OK != res)
          {
              /* we failed */
              fprintf(stderr, "curl told us %d\n", res);
              ret = false;
          }
          else
              ret = true;
      }

      if(ftpfile.stream)
      {
          fclose(ftpfile.stream); /* close the local file */
      }

      curl_global_cleanup();

      return ret;
}




bool FacePostProcess::getBufferedDataSFTP(std::string filename, cv::Mat &fdata)
{
    CURL        *curlhandle;
    CURLcode    res;
    bool        ret;

    struct MemoryStruct buffer;

    std::string downloadfile_url = mSFTPurl + filename;

    buffer.memory = (char *)std::malloc(1);  /* will be grown as needed by the realloc above */
    buffer.size = 0;    /* no data at this point */

    curl_global_init(CURL_GLOBAL_ALL);

    /* init the curl session */
    curlhandle = curl_easy_init();


    if(curlhandle)
    {

       if(DEBUG_PP)
       {
          // Switch on full protocol/debug output
          curl_easy_setopt(curlhandle, CURLOPT_VERBOSE, 1L);

          // For getting a body with error message in the first place
          curl_easy_setopt(curlhandle, CURLOPT_FAILONERROR, 0);
        }

        /* specify URL to get */
        curl_easy_setopt(curlhandle, CURLOPT_URL, downloadfile_url.c_str());

        /* send all data to this function  */
        curl_easy_setopt(curlhandle, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);

        /* we pass our 'chunk' struct to the callback function */
        curl_easy_setopt(curlhandle, CURLOPT_WRITEDATA, (void *)&buffer);

        /* some servers don't like requests that are made without a user-agent
           field, so we provide one */
        curl_easy_setopt(curlhandle, CURLOPT_USERAGENT, "libcurl-agent/1.0");


        curl_easy_setopt(curlhandle, CURLOPT_USERNAME, mSFTPid.c_str());

        curl_easy_setopt(curlhandle, CURLOPT_PASSWORD, mSFTPpwd.c_str());

        // ssh
        curl_easy_setopt(curlhandle,CURLOPT_SSH_AUTH_TYPES, CURLSSH_AUTH_PASSWORD);


        /* get it! */
        res = curl_easy_perform(curlhandle);

        /* check for errors */
        if(res != CURLE_OK)
        {
            if(DEBUG_PP)
            {
                fprintf(stderr, "curl_easy_perform() failed: %s\n",
                curl_easy_strerror(res));
            }
            ret= false;
        }
        else {

            // Save char data to cv::Mat
            fdata = cv::imdecode(cv::Mat(1,buffer.size, CV_8UC1, buffer.memory), cv::IMREAD_UNCHANGED);

            //if(DEBUG_PP)
            //{
            //    printf("%lu bytes retrieved\n", (long)buffer.size);
            //}
            ret = true;
        }


        /* cleanup curl stuff */
        curl_easy_cleanup(curlhandle);
    }

    free(buffer.memory);

    /* we're done with libcurl, so clean it up */
    curl_global_cleanup();

    return ret;

}



// set the config of Face Meta Database
// Input: mon[1-12], day[1-31], hour[0-23, 0: midnight]
std::string FacePostProcess::dateTostrUTC(int year, int month, int day, int hour, int min, int sec)
{
    struct tm ctm;
    ctm.tm_year = year -1900;
    ctm.tm_mon  = month -1;     // [0-11]
    ctm.tm_mday = day;          // [1-31]

    ctm.tm_hour = hour;     // [0-23]   18 => pm 5 (maybe daylight saving)

    ctm.tm_min  =  min;  //[0-59]
    ctm.tm_sec  = sec;   //[0-61];

    time_t ctt;

    ctt = mktime(&ctm);

    //printf("The current Date and time: %s\n", asctime(&ctm));


    std::stringstream cstrm;
    cstrm << ctt;

    return cstrm.str();

}

// set the config of Face Meta Database
// Input: mon[1-12], day[1-31], hour[0-23, 0: midnight]
std::string FacePostProcess::dateStructureTostrUTC(DateTime_t dt)
{
    struct tm ctm;
    ctm.tm_year = dt.year -1900;
    ctm.tm_mon  = dt.month -1;     // [0-11]
    ctm.tm_mday = dt.day;          // [1-31]

    ctm.tm_hour = dt.hour;     // [0-23]   18 => pm 5 (maybe daylight saving)

    ctm.tm_min  = dt.min;  //[0-59]
    ctm.tm_sec  = dt.sec;   //[0-61];

    time_t ctt;

    ctt = mktime(&ctm);

    //printf("The current Date and time: %s\n", asctime(&ctm));


    std::stringstream cstrm;
    cstrm << ctt;

    return cstrm.str();

}


//------------------------------------------------------------------------------//
//#define ACCESS_KEY_ID   "AKIAJ4X3HZL7LDNV4Y3A"
//#define SECRET_KEY      "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"

void FacePostProcess::setConfigAmzRknAPI(std::string access_id, std::string secret_key, std::string token)
{
   keyId = access_id.c_str();
   keyPw = secret_key.c_str();
   token = "";
}


void FacePostProcess::connectToAmzRknAPI()
{
    mIsConnectedToAmzRknAPI = true;
    options.loggingOptions.logLevel = Aws::Utils::Logging::LogLevel::Info;  // (optional) for logging
    Aws::InitAPI(options);

}

void FacePostProcess::disconnectToAmzRknAPI()
{
    mIsConnectedToAmzRknAPI = false;
    Aws::ShutdownAPI(options);
}

bool FacePostProcess::getFaceMetaDataByAmzRknAPI(cv::Mat imgMat, double &ageMean, double &ageRange, double &gender, double &genderScore)
{
    bool ret;

    // 1. change cv::Mat to Base64 data
    std::vector<unsigned char> imgBuffer;
    cv::imencode(".png", imgMat, imgBuffer);

    std::string imgStrBuffer = std::string(imgBuffer.begin(), imgBuffer.end());
    unsigned char *imgBuffPtr = (unsigned char *)imgBuffer.data();
    unsigned int imgBuffLength = static_cast<unsigned int>(imgStrBuffer.size());

    // encoding
    //std::string imgBase64Str = base64_encode(imgBuffPtr, imgBuffLength);

    double age[MAX_NUM_FACE_ANALYSIS];
    double ageSigma[MAX_NUM_FACE_ANALYSIS];

    Aws::String gtstr[MAX_NUM_FACE_ANALYSIS];
    double genderConfidence[MAX_NUM_FACE_ANALYSIS];


    // 2. Call API
    //Aws::SDKOptions options;
    //options.loggingOptions.logLevel = Aws::Utils::Logging::LogLevel::Info;  // (optional) for logging
    //Aws::InitAPI(options);
    {

        Aws::Client::ClientConfiguration config;

        config.scheme = Aws::Http::Scheme::HTTPS;
        config.connectTimeoutMs = 30000;
        config.requestTimeoutMs = 30000;
        config.region = Aws::Region::US_EAST_1;

        Aws::Rekognition::RekognitionClient clientRK(Aws::Auth::AWSCredentials(keyId, keyPw, token), config);

        Aws::Rekognition::Model::DetectFacesRequest dtRequest;
        Aws::Rekognition::Model::Image img;
        img.SetBytes(Aws::Utils::ByteBuffer(imgBuffPtr, imgBuffLength));

        dtRequest.SetImage(img);

        Aws::Vector<Aws::Rekognition::Model::Attribute> attr;
        attr.push_back(Aws::Rekognition::Model::Attribute::ALL);
        dtRequest.SetAttributes(attr);

        auto dtResult = clientRK.DetectFaces(dtRequest);

        if(dtResult.IsSuccess())
        {
            // to get values of gender and age
            auto outcome = dtResult.GetResult();
            auto fd = outcome.GetFaceDetails();

            int i=0;
            for( auto itr = fd.begin(); itr != fd.end(); itr++)
            {
                // Filtering the 70 % confidence that a face in the bounding box and Large Pose variation
                Aws::Rekognition::Model::Pose fpose = itr->GetPose();

                if(DEBUG_PP)
                {
                    std::cout << "Detection confidence=" << itr->GetConfidence() << ","
                              <<"Pose: Y(" << fpose.GetYaw() <<"), P(" << fpose.GetPitch() << "), R(" << fpose.GetRoll() << ")" << std::endl;

                }
                // not proper face for demographics recognition
                if(itr->GetConfidence() < 70.0)
                {
                    //age[i] = -1;
                    //ageSigma[i] = 0;
                    //genderConfidence[i] = 1;
                    //gtstr[i] = Aws::Rekognition::Model::GenderTypeMapper::GetNameForGenderType(Aws::Rekognition::Model::GenderType::NOT_SET);

                    ret = false;

                    if(DEBUG_PP)
                        std::cout << "CLOUD API: Filtering unproper face (confidence=" << itr->GetConfidence() << ")" << std::endl;

                }
                else if(std::abs(fpose.GetPitch()) > 40.0 || std::abs(fpose.GetRoll()) > 40 || std::abs(fpose.GetYaw()) > 40)
                {

                    ret = false;

                    if(DEBUG_PP)
                        std::cout << "CLOUD API: Filtering unproper face (large pose variation: Y("
                                  << fpose.GetYaw() <<"), P(" << fpose.GetPitch() << "), R(" << fpose.GetRoll() << ")" << std::endl;

                }
                else
                {
                    Aws::Rekognition::Model::AgeRange ar = itr->GetAgeRange();
                    age[i] = (int)(ar.GetLow() + ar.GetHigh())/2;
                    ageSigma[i] = double(age[i] - ar.GetLow());   // sigma

                    Aws::Rekognition::Model::Gender gr = itr->GetGender();
                    genderConfidence[i] = gr.GetConfidence();

                    Aws::Rekognition::Model::GenderType gt = gr.GetValue();
                    gtstr[i] = Aws::Rekognition::Model::GenderTypeMapper::GetNameForGenderType(gt);
                    ret = true;
                }

                i++;
            }

        }
        else
            ret = false;

    }
    //Aws::ShutdownAPI(options);

    // 3. return deomographics score
    // For the case of Single face image
    // Representation:
    //      - Age (meanAge.AgeInterval) for example, 24.16 (Interval: lowAge=16 and highAge=32)
    //      - Gender ( femail(-1*confidence) ; male(1*confidence) ; unknown(100) )

    if(ret)
    {
        ageMean = age[0];
        ageRange = ageSigma[0]*2 * 0.01;
        ageMean += ageRange;

        if(gtstr[0] == "Female")
            gender = -1.0f * genderConfidence[0]*0.01;
        else if(gtstr[0] == "Male")
            gender = 1.0f * genderConfidence[0]*0.01;

        else // unknown
            gender = GENDER_UNKNOWN;

        genderScore = genderConfidence[0];
    }
    else
    {
        ageMean = -1;
        gender = 100.0f;
        genderScore =0;
    }

    return ret;
}


// Post-processing for demographics recognition
int FacePostProcess::postProcessDMG(std::string storeName, std::string devName, DateTime_t sdt, DateTime_t edt)
{
    bool ret;

    // FacePostProcess fpp;

    if(DEBUG_PP)
    {
        std::cout << "[Post-processing]: DMG is started..." << std::endl;
    }

    // connect DBs and a service
    setDBconfigRDS(AWS_RDS_TESTBED_URL, AWS_RDS_TESTBED_ID,AWS_RDS_TESTBED_PWD,storeName, devName);
    connectToRDS();

    cv::Mat fData;
    setDBConfigSFTP(SFTP_URL,SFTP_USERNAME,SFTP_PASSWD);
    connectToSFTP();

    setConfigAmzRknAPI(ACCESS_KEY_ID,SECRET_KEY,"");
    connectToAmzRknAPI();


    // make a query as an input
    std::string stime = dateStructureTostrUTC(sdt);
    std::string etime = dateStructureTostrUTC(edt);

    if(DEBUG_PP)
        gStartTime();

    // process
    if(getFaceDataFromRDS(stime,etime))
    {
        ret = true;
        if(DEBUG_PP)
            std::cout << "RDS: Fetching and parseing meta data are succeded." << std::endl;
    }
    else
    {
        ret = false;
        if(DEBUG_PP)
            std::cout << "RDS: Fetching and parseing meta data are failed." << std::endl;
    }

    std::vector<LocalFaceData> lfd;
    lfd = getFaceDataSet();

    if(DEBUG_PP)
    {
        double elapsed_time1 = gTimeSinceStart();
        std::cout <<  "RDS: Fetching meta data - Elapsed time = " <<elapsed_time1 << std::endl;
    }

    // Loop for each face ID
    int numFPP=0;
    double gender, genderConf, ageMean, ageRange;

    //double elapsed_time, elapsed_time2, elapsed_time3;

    for(int i=0; i< lfd.size(); i++)
    {

        if(DEBUG_PP)
            gStartTime();

        // 2. Fetch face image
        if(getBufferedDataSFTP(lfd[i].faceURL, fData))
        {
            if(DEBUG_PP)
            {
                std::cout << "SFTP: downloading is succeded." << std::endl;
                //elapsed_time = gTimeSinceStart();
                //std::cout <<  "SFTP: Fetching images - Elapsed time = " <<elapsed_time << std::endl;
            }

            // 3. Call Cloud API
            bool ret_api = getFaceMetaDataByAmzRknAPI(fData,ageMean,ageRange,gender,genderConf);

            if(ret_api)
            {
                if(DEBUG_PP)
                    std::cout << "Cloud API: processed as a correct face." << std::endl;
            }
            else
            {
                if(DEBUG_PP)
                    std::cout << "Cloud API: processed as a wrong face." << std::endl;
            }
            //if(DEBUG_PP)
            //{
                //elapsed_time2 = gTimeSinceStart() - elapsed_time;
                //std::cout <<  "Cloud API: Elapsed time = " << elapsed_time2 << std::endl;
            //}

            // 4. Update
            if(updateDMGdataToRDS(gender,ageMean,lfd[i].faceId, true) /*ret_api)*/ != RDS_UPDATE_ERROR)
            {
                numFPP++;

                if(DEBUG_PP)
                {
                    if(ret_api)
                        std::cout << "RDS: data might be updated where faceId=" << lfd[i].faceId << std::endl;
                    else
                        std::cout << "RDS: data might be deleted where faceId=" << lfd[i].faceId << std::endl;
                }
             }
             else
             {
                if(DEBUG_PP)
                   std::cout << "RDS: updating data is failed." << std::endl;
             }
            //if(DEBUG_PP)
            //{
                //elapsed_time3 = gTimeSinceStart() - elapsed_time - elapsed_time2;
                //std::cout <<  "RDS: Upadting data - Elapsed time = " << elapsed_time3 << std::endl;
            //}

        }
        else
        {
            if(DEBUG_PP)
                std::cout << "SFTP: downloading is failed." << std::endl;
        }


    }

    // disconnect DBs and a service
    disconnectToRDS();
    disconnectToSFTP();
    disconnectToAmzRknAPI();

    if(DEBUG_PP)
         std::cout << "[Post-processing]: DMG is ended." << std::endl << std::endl;

    return numFPP;
}

// Post-processing for joining broken tracks
bool FacePostProcess::postProcessJBT()
{

}



}  // using namespace fpp

}  // using namespace vml

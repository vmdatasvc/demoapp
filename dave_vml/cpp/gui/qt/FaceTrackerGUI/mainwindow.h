#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QMessageBox>
#include <QTimer>

#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <facetrackingmethod1.h>
#include <facedetectmethod1.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void updateMethod();

    void updateDetection();

    void on_OpenFilePushButton_clicked();

    void on_StartPushButton_toggled(bool checked);

    void on_PausePushButton_toggled(bool checked);

    void on_toolButton_clicked();

    void on_TrackingMethodHSVHistRadioButton_clicked(bool checked);

    void on_TrackingMethodrgChromHistRadioButton_clicked(bool checked);

    void on_TrackingMethodRGBHistRadioButton_clicked(bool checked);

    void on_TrackingMethodDlibCorrRadioButton_clicked(bool checked);

    void on_TrackingMethodHybridRadioButton_clicked(bool checked);

    void on_QuitPushButton_clicked();

    void on_LoadSettingsPushButton_toggled(bool checked);

    void on_WebCamInputRadioButton_clicked();


    void on_stepPushButton_clicked();

    void on_MeanShiftTrackerRadioButton_clicked(bool checked);

    void on_CamShiftTrackerRadioButton_clicked(bool checked);

    void on_FaceDetectTestPushButton_clicked();

    void on_FDPushButton_toggled(bool checked);

    void on_FDPausePushButton_toggled(bool checked);

    void on_FDStepPushButton_clicked();

    void on_TrackingMethodYCrCbHistRadioButton_clicked(bool checked);

    void on_PostImageToCloudinaryPushButton_clicked();


    void on_RecordPushButton_clicked();

    void on_UploadImageToSFTPserverPushButton_clicked();

    void on_PeopleCounterPushButton_clicked();

    void on_DetectedPeopleCountPushButton_clicked();

    void on_TempPushButton_clicked();

    void on_FetchFaceDataPushButton_clicked();

    void on_DownloadImageFromSFTPServerPushButton_clicked();

    void on_CloudAPITestPushButton_clicked();

    void on_DMGProcessPushButton_clicked();

    void on_UpdateToRDSPushButton_clicked();

private:
    Ui::MainWindow *ui;

    // ui widget maps
    QPushButton *muiStartButton;

    // opencv

    cv::VideoCapture    mCapture;

    bool mStarted;


    QTimer *timer;
    QTimer *timer2;

    // tracking method classes
    FaceDetectMethod1           *dt1;

    // tracking method classes
    FaceTrackingMethod1         *tr1;

    // method1 settings
    boost::shared_ptr<vml::Settings>   mpFTrSettings;

    // variables
    unsigned int    mPeopleCount;
    unsigned int    mDetectedPeopleCount;
    unsigned int    mFrameCount;
    unsigned int    mDetectedFaceCount;
    unsigned int    mDetectedFaceAFCount;

    unsigned int    curMinFaceSize;
    unsigned int    curMaxFaceSize;

    vml::FaceTrackingMethods mTrackingMethod;

    // ROI
    float rx,ry,rw,rh;

protected:

    void init();
    void timerEvent(QTimerEvent *event);

    void updateParams();
    void initFDParams();

//    void cvtMatToImage32(cv::Mat iMat, ait::Image32 &img);
//    void cvtImage32ToMat(ait::Image32 &img, cv::Mat *iMat);

};

#endif // MAINWINDOW_H

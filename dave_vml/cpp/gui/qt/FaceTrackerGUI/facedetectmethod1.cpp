#include "facedetectmethod1.h"
#include <QDebug>

using namespace vml;

FaceDetectMethod1::FaceDetectMethod1(std::string url,float sF, int mNbr, boost::shared_ptr<vml::Settings> settings)
{

    bool openSuccess;
    if (isdigit(url[0]))
    {
        openSuccess = mCapture.open(atoi(url.c_str()));
    }
    else
    {
        openSuccess = mCapture.open(url);
    }

    if (!openSuccess)
    {
        qDebug() <<"Camera is not opened!";
    }

    scaleFactor = sF;
    minNeighbors = mNbr;
    minFaceSize = 20;
    maxFaceSize = 500;
    //mFPFiltering = true;

    std::string cascadeFilePath = "../../../../data/ml/opencv/haarcascades/haarcascade_frontalface_alt2.xml";    
    mpCascade.load(cascadeFilePath.c_str());


    // load the model of a face filter
    psvm.reset(new vml::MLSVM(settings));
    pfeature.reset(new vml::MLFeature(settings));

    std::string modelFileName = settings->getString("FaceDetection/NFFModelFile"," ", false);
    psvm->loadModelFromFile(modelFileName);

    mDetectFrameCount = 0;
}

FaceDetectMethod1::~FaceDetectMethod1()
{
    if(mCapture.isOpened())
    {
        mCapture.release();
    }
}


bool FaceDetectMethod1::detect(cv::Rect2f roi)
{
    //cv::Mat	inputimage;

    mCapture >> inputimage;   

    if(inputimage.empty())
    {
        return false;
    }

    // Masked by roi
    cv::Mat mask = cv::Mat::zeros(inputimage.rows, inputimage.cols, CV_8UC1);
    cv::Mat roiMask = mask(cv::Rect( std::floor(roi.x*inputimage.cols),std::floor(roi.y*inputimage.rows), std::floor(roi.width*inputimage.cols), std::floor(roi.height*inputimage.rows)));
    roiMask.setTo(cv::Scalar(255));

    cv::bitwise_and(inputimage, cv::Scalar(255,255,255), maskedImage, mask);


    mDetectFrameCount++;

    // face detection
    mpCascade.detectMultiScale(maskedImage,mFaces,scaleFactor, minNeighbors,0,cv::Size(minFaceSize,minFaceSize), cv::Size(maxFaceSize,maxFaceSize));

    // do false face filtering, if checked the option of false face filtering
    if(mFPFiltering)
    {
        mFacesAfterFiltering.clear();
        mFalsePositiveFaces.clear();
        FPFiltering(maskedImage, mFaces);


        unsigned int minsizetmp=1000, maxsizetmp=0;

        for (int i = 0; i < mFacesAfterFiltering.size(); i++) {

               unsigned int fsize = std::min(mFacesAfterFiltering[i].width, mFacesAfterFiltering[i].height);

               if(fsize < minsizetmp)
               {
                   minsizetmp = fsize;

               }
               if(fsize > maxsizetmp)
               {
                   maxsizetmp = fsize;
               }
         }

        mMinFaceSize = minsizetmp;
        mMaxFaceSize = maxsizetmp;
    }
    else
    {
        unsigned int minsizetmp=1000, maxsizetmp=0;

        for (int i = 0; i < mFaces.size(); i++) {

               unsigned int fsize = std::min(mFaces[i].width, mFaces[i].height);

               if(fsize < minsizetmp)
               {
                   minsizetmp = fsize;

               }
               if(fsize > maxsizetmp)
               {
                   maxsizetmp = fsize;
               }
         }

        mMinFaceSize = minsizetmp;
        mMaxFaceSize = maxsizetmp;
    }




    return true;
}


void FaceDetectMethod1::drawROI(cv::Mat &image, cv::Rect2f roi) const
{
    cv::Point br = cv::Point(std::floor(roi.br().x * image.cols), std::floor(roi.br().y * image.rows));
    cv::Point tl = cv::Point(std::floor(roi.tl().x * image.cols), std::floor(roi.tl().y * image.rows));
    cv::rectangle(image,  br, tl, cv::Scalar(255, 255, 0), 2, 8, 0);
}

void FaceDetectMethod1::drawValidTargets(cv::Mat &image) const
{

    for (int i = 0; i < mFaces.size(); i++) {

           cv::rectangle(image, mFaces[i].br(), mFaces[i].tl(), cv::Scalar(0, 255, 255), 2, 8, 0);
     }
}

void FaceDetectMethod1::drawValidTargetsFiltered(cv::Mat &image) const
{

    for (int i = 0; i < mFalsePositiveFaces.size(); i++) {

           cv::rectangle(image, mFalsePositiveFaces[i].br(), mFalsePositiveFaces[i].tl(), cv::Scalar(0, 0, 255), 2, 8, 0);
     }

}

void FaceDetectMethod1::drawValidTargetsAF(cv::Mat &image) const
{

    for (int i = 0; i < mFacesAfterFiltering.size(); i++) {

           cv::rectangle(image, mFacesAfterFiltering[i].br(), mFacesAfterFiltering[i].tl(), cv::Scalar(0, 255, 255), 2, 8, 0);
     }

}

const cv::Mat&  FaceDetectMethod1::getGrabbedImage(void)
{

    return inputimage;
}


unsigned int FaceDetectMethod1::getNumDetectedFaces(void)
{
    return mFaces.size();
}

unsigned int FaceDetectMethod1::getNumDetectedFacesAF(void)
{
    return mFacesAfterFiltering.size();
}

unsigned int FaceDetectMethod1::getNumDetectedFacesFiltered(void)
{
    return mFalsePositiveFaces.size();
}


unsigned int FaceDetectMethod1::getCurMinFaceSize(void)
{

    return mMinFaceSize;
}

unsigned int FaceDetectMethod1::getCurMaxFaceSize(void)
{

    return mMaxFaceSize;
}

double FaceDetectMethod1::getFPS(void)
{
     return mCapture.get(CV_CAP_PROP_FPS);
}


void FaceDetectMethod1::FPFiltering(cv::Mat &image, std::vector<cv::Rect> &face_vecs)
{

    cv::Mat cropped, face_cand;

    // False positive face filtering
    for (int i=0; i<face_vecs.size();i++)
    {
        cropped = image(face_vecs[i]);
        cropped.copyTo(face_cand);

            // 1. color feature-based filtering
 /*           if(FPFilteringByColor(face_cand))
            {
                mFacesAfterFiltering.push_back(face_vecs[i]);
            }
            else
            {
                mFalsePositiveFaces.push_back(face_vecs[i]);
            }
*/
            // 2. shape feature-based filtering
            // 2. HoG feature-based filtering
            if(FPFilteringByHOG(face_cand))
             {
                 mFacesAfterFiltering.push_back(face_vecs[i]);
             }
             else
             {
                 mFalsePositiveFaces.push_back(face_vecs[i]);
             }
    }


}

bool FaceDetectMethod1::FPFilteringByColor(cv::Mat &cimg)
{
    cv::Mat dimg;
    cv::cvtColor(cimg, dimg, CV_BGR2YCrCb);

    // Extract channel individually
    cv::Mat planes[3];
    cv::split(dimg, planes);

    cv::Scalar mean_ch1 = cv::mean(planes[0]);
    cv::Scalar mean_ch2 = cv::mean(planes[1]);
    cv::Scalar mean_ch3 = cv::mean(planes[2]);

    float mch1 = mean_ch1.val[0];
    float mch2 = mean_ch2.val[0];
    float mch3 = mean_ch3.val[0];

    if (mch3 > 135 &&  (mch2-62) > 0 )
    {
        return false;
    }

    return true;

}

bool FaceDetectMethod1::FPFilteringByHOG(cv::Mat &cimg)
{
    // svm-based prediction
    pfeature->extFeature(cimg);
    double estimate = psvm->predict(pfeature->featData);
    if(estimate ==1) return true;
    else return false;
}


void FaceDetectMethod1::saveFaces(cv::Mat &image, std::string path, int fno, int flag)
{
    std::vector<cv::Rect> facevec;
    time_t timer;
    cv::Mat cropped;

    if(flag == 0)       // detected output before filtering faces
    {
        facevec = mFaces;
    }
    else if(flag ==1)   // detected output after filtering faces
    {
        facevec = mFacesAfterFiltering;
    }
    else                // false positive faces which are filtered out
    {
        facevec = mFalsePositiveFaces;
    }


    for(int i=0;i< (int)facevec.size();i++)
    {

        cropped = image(facevec[i]);

        time(&timer);
        tm *bar = gmtime(&timer);

        std::ostringstream ossr;
        //ossr << bar->tm_mon <<  bar->tm_mday << bar->tm_year <<"_" << bar->tm_hour << bar->tm_min << bar->tm_sec << '_' << i;
        ossr << fno << '_' << i;

        std::string facefile =  path + ossr.str() + ".jpg";


        // write a face image to a local disk(path)
        cv::imwrite(facefile.c_str(), cropped);
    }

}

void FaceDetectMethod1::saveFacesWithRatio(cv::Mat &image, std::string path, int fno, int flag, double ratio)
{
    std::vector<cv::Rect> facevec;
    time_t timer;
    cv::Mat cropped;

    if(flag == 0)       // detected output before filtering faces
    {
        facevec = mFaces;
    }
    else if(flag ==1)   // detected output after filtering faces
    {
        facevec = mFacesAfterFiltering;
    }
    else                // false positive faces which are filtered out
    {
        facevec = mFalsePositiveFaces;
    }


    for(int i=0;i< (int)facevec.size();i++)
    {
        // cropped with ratio
        cv::Rect box(facevec[i]);

        cv::Point center;
        center.x =(int)(box.x + (float)box.width /2.f);
        center.y = (int)(box.y + (float)box.height /2.f);

        int nx,ny,nwidth, nheight;

        nwidth = (int)(box.width*ratio);
        nheight = (int)(box.height*ratio);

        // For using Cloud API, the size of an image including a face must be over 80 pixels for its width and height.
        if(nwidth < 80)	{
            nwidth = 80;
            //mStoreImageRatio = 80.f /(float)box.width;   // here, a face image is rectangle so that we can calculate the ratio only with a width.
        }
        if(nheight < 80) nheight = 80;

        nx = (int)(center.x-(float)nwidth/2.f); //boost::lexical_cast<int>((float)center.x-(float)nwidth/2.f);
        ny = (int)(center.y-(float)nheight/2.f); //boost::lexical_cast<int>((float)center.y-(float)nheight/2.f);


        // boundary
        if(nx <0)
        {
            nwidth += nx;
            nx =0;
        }
        if(ny <0)
        {
            nheight += ny;
            ny=0;
        }
        if(nx+nwidth >= image.cols)
        {
            int del = image.cols - (nx+nwidth) ;
            //float hdel = (float)del/2.f;
            nwidth += del;
        }
        if(ny+nheight >= image.rows)
        {
            int del = image.rows - (ny+nheight) ;
            //float hdel = (float)del/2.f;
            nwidth += del;
        }

        cv::Rect nrect(nx,ny,nwidth,nheight);


        cropped = image(nrect);

        time(&timer);
        tm *bar = gmtime(&timer);

        std::ostringstream ossr;
        //ossr << bar->tm_mon <<  bar->tm_mday << bar->tm_year <<"_" << bar->tm_hour << bar->tm_min << bar->tm_sec << '_' << i;
        ossr << fno << '_' << i;

        std::string facefile =  path + ossr.str() + ".jpg";


        // write a face image to a local disk(path)
        cv::imwrite(facefile.c_str(), cropped);
    }

}


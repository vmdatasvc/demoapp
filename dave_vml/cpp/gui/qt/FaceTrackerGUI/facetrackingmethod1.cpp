#include "facetrackingmethod1.h"

using namespace vml;

FaceTrackingMethod1::FaceTrackingMethod1(std::string url,
                                         boost::shared_ptr<vml::Settings> settings)
{
    // create tracker
    //mpFaceTracker = new MultiTargetFaceTracker();

    mpFaceTracker.reset(new MultiTargetFaceTracker());
    mpFaceTracker->setSettings(settings);
    mpFaceTracker->initializeVideo(url);
    mpFaceTracker->initializeModules();

}


FaceTrackingMethod1::~FaceTrackingMethod1()
{
    //delete mpFaceTracker;
    if(mpFaceTracker)
        mpFaceTracker.reset();
}


bool FaceTrackingMethod1::process()
{
    // create new targets after verification
    return mpFaceTracker->process();
}


const cv::Mat&  FaceTrackingMethod1::getGrabbedImage(void)
{

    return mpFaceTracker->getGrabbedImage();
}

const cv::Mat& FaceTrackingMethod1::getBackProjection(void)
{
    return mpFaceTracker->getBackProjection();
}

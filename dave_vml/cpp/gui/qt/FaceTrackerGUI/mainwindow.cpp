#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include "modules/utility/TimeKeeper.hpp"
#include <modules/utility/DateTime.hpp>

#include <modules/utility/restAPI.hpp>
#include <modules/utility/Serialize.hpp>
//#include <modules/utility/base64.h>

#include <facepostprocess.hpp>
#include <ctime>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    init();
    timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()), this, SLOT(updateMethod()));

    timer2 = new QTimer(this);
    connect(timer2,SIGNAL(timeout()), this, SLOT(updateDetection()));

}

MainWindow::~MainWindow()
{
    delete ui;

    delete timer;
    delete timer2;
}

void MainWindow::init()
{
    // Default: image path: "../../../../cpp/gui/qt/FaceTrackerGUI/ai-face.jpg"
    // For ipcam: "http://10.250.1.90/axis-cgi/mjpg/video.cgi?resolution=1280x960"
    // For OmniSensr in ST office:
    //  (FaceCam1-from the door toward the office inside ) http://172.31.11.1:8080/stream/video.mjpeg
    //  (FaceCam2-from the office inside toward the door ) http://172.31.11.2:8080/stream/video.mjpeg
    //  (4:3)  2048x1536, 1296x972, 1280x960, 1024x768, 800x600, 640x480, 480x360, 320x240
    //  (16:9) 1920x1080, 1280x720, 800x450, 640x360, 480x270, 320x180

    // clean viewer
    ui->openCVViewer1->clearImage();
    ui->openCVViewer2->clearImage();

    // initial selection of a radio button
    ui->VideoInputRadioButton->setChecked(true);
    ui->ImageInputRadioButton->setChecked(false);
    ui->WebCamInputRadioButton->setChecked(false);

}


void MainWindow::timerEvent(QTimerEvent *event)
{
    //cv::Mat image, dispImg;
    //mCapture >> image;

    // Show the image
    //cv::cvtColor(image,dispImg, CV_BGR2RGB);
    //ui->openCVViewer1->showImage(dispImg);
}


void MainWindow::updateMethod()
{
    // run process
    if (not tr1->process())
    {
        // signal stop button and return
        this->on_StartPushButton_toggled(false);
        return;
    }
/*
    std::vector<vml::FaceTrajectory>  completed_traj_list;
    if (mFrameCount % 400 == 0)
    {
        // retrieve completed trajectories.
        tr1->getCompletedTrajectories(completed_traj_list);
    }
*/
   // save face data
    std::vector<vml::TargetFaceData> fdataVec;
    if (mFrameCount % 400 == 0)
    {
        tr1->getCompletedFaceData(fdataVec);
    }

    cv::Mat image, bpi, bpic;

    tr1->getGrabbedImage().copyTo(image);

    // display tracked region
    tr1->drawTrackingRegions(image);

    // display tracked target
    //tr1->drawActiveTargets(image);
    tr1->drawActiveTargetsWithDMG(image);

    if (ui->ShowActiveTrajectoriesCheckBox->isChecked())
    {
        tr1->drawActiveTrajectories(image);
    }

    // display newly detected targets
    tr1->drawValidNewTargets(image);

    // show the images
    ui->openCVViewer1->showImage(image);

    tr1->getBackProjection().copyTo(bpi);
    cv::cvtColor(bpi,bpic,CV_GRAY2RGB);
    ui->openCVViewer2->showImage(bpi);


    mFrameCount = tr1->getFrameCount();
    double fps = mFrameCount / gTimeSinceLastSaved();

     //std::cout << "frame rate: " << fps << " fps" << std::endl;

    // display frame count
    ui->FrameRateLabel->setText(QString::number(fps));
    ui->FrameCountLabel->setText(QString::number(mFrameCount));
    unsigned int nActiveTargets = tr1->getNumActiveTargets();
    ui->ActiveTargetCountLabel->setText(QString::number(nActiveTargets));
    unsigned int nTotalTargets = tr1->getNumTotalTargets();
    ui->TotalTargetCountLabel->setText(QString::number(nTotalTargets));
    unsigned int nInactiveTargets = tr1->getNumInactiveTargets();
    ui->InactiveTargetCountLabel->setText(QString::number(nInactiveTargets));
    unsigned int nCompletedTargets = tr1->getNumCompletedTargets();
    ui->completedTargetCountLabel->setText(QString::number(nCompletedTargets));

    //image.release();

    // update image size
    ui->ImageWidthLabel->setText(QString::number(image.cols));
    ui->ImageHeightLabel->setText(QString::number(image.rows));



}

void MainWindow::updateDetection()
{



    // update the ROI for detection from the given settings
    float rx,ry,rw,rh;
    rx = ui->xROILineEdit->text().toFloat();
    ry = ui->yROILineEdit->text().toFloat();
    rw = ui->wROILineEdit->text().toFloat();
    rh = ui->hROILineEdit->text().toFloat();

    if( (1 - rx) < rw )
    {
        rw= 1-rx;
        ui->wROILineEdit->setText(QString::number(rw));
    }
    if( (1-ry) < rh )
    {
        rh =1-ry;
        ui->hROILineEdit->setText(QString::number(rh));
    }

    cv::Rect2f roi = cv::Rect2f(rx,ry,rw,rh);

    // update checkbox
    if(ui->FPFilteringCheckBox->isChecked()) {
       dt1->mFPFiltering = true;
    }
    else {
      dt1->mFPFiltering = false;
    }

    if (not dt1->detect(roi))
    {
        this->on_FDPushButton_toggled(false);
        return;
    }    

     cv::Mat image,disp;

     dt1->getGrabbedImage().copyTo(image);



     if(ui->SaveDetectedFacesCheckBox->isChecked())
     {
         int saveflag;
         std::string  path(ui->SaveFacesPathLineEdit->text().toStdString());
         if(ui->SaveDetectedFacesRadioButton->isChecked())
         {
             saveflag = 0;
         }
         else if(ui->SaveFilteredFacesRadioButton->isChecked())
         {
             saveflag = 1;
         }
         else
         {
             saveflag = 2;
         }
         //dt1->saveFaces(image, path, mFrameCount, saveflag);
         dt1->saveFacesWithRatio(image, path, mFrameCount, saveflag, ui->faceSizeDoubleSpinBox->value());
     }


     dt1->drawValidTargets(image);
     //dt1->drawValidTargetsAF(image);

     if(dt1->mFPFiltering)
     {
         dt1->drawValidTargetsFiltered(image);
     }

/*     // Color test
     cv::Mat tmpimg;
     cv::cvtColor(image,tmpimg, CV_BGR2HSV);
     cv::Mat planes[3];
     cv::split(tmpimg, planes);

     cv::namedWindow("Ch1");
     cv::imshow("Ch1",planes[0]);
     cv::namedWindow("Ch2");
     cv::imshow("Ch2",planes[1]);
     cv::namedWindow("Ch3");
     cv::imshow("Ch3",planes[2]);

*/

     // show the images
     // show the ROI

     dt1->drawROI(image,roi);

     cv::cvtColor(image, disp, CV_BGR2RGB);
     ui->openCVViewer1->showImage(disp);

     mDetectedFaceCount += dt1->getNumDetectedFaces();
     mDetectedFaceAFCount += dt1->getNumDetectedFacesFiltered();

     mFrameCount++;

     if(curMinFaceSize > dt1->getCurMinFaceSize())
         curMinFaceSize = dt1->getCurMinFaceSize();
     if(curMaxFaceSize < dt1->getCurMaxFaceSize())
         curMaxFaceSize = dt1->getCurMaxFaceSize();

     // update labels
     ui->ImageWidthLabel->setText(QString::number(image.cols));
     ui->ImageHeightLabel->setText(QString::number(image.rows));

     double fps = mFrameCount / gTimeSinceLastSaved();

     // display frame count
     ui->FPSLabel->setText(QString::number(fps));

     //ui->FPSLabel->setText(QString::number(dt1->getFPS()));
     ui->DetectedFaceCountLabel->setText(QString::number(mDetectedFaceCount));
     ui->FilteredFaceCountLabel->setText(QString::number(mDetectedFaceAFCount));
     ui->FrameCountLabel->setText(QString::number(mFrameCount));
     ui->DetectedMinFaceSizeLabel->setText(QString::number(curMinFaceSize));
     ui->DetectedMaxFaceSizeLabel->setText(QString::number(curMaxFaceSize));


     // update the detection rate and average face number per a person
     if(mPeopleCount)
     {
         float drate = (float)mDetectedPeopleCount*100.0f/(float)mPeopleCount;
         ui->DetectionRateLabel->setText(QString::number(drate));
     }
     if(mDetectedPeopleCount)
     {
         float avgfn = (float)(mDetectedFaceCount-mDetectedFaceAFCount)/ (float)mDetectedPeopleCount;
         ui->AvgFaceNumLabel->setText(QString::number(avgfn));
     }

}


void MainWindow::on_OpenFilePushButton_clicked()
{
    QString tmpString = QFileDialog::getOpenFileName(this, "Open File");

    if( !tmpString.trimmed().isEmpty())
    {
        ui->OpenFileLineEdit->setText(tmpString);
    }
}

void MainWindow::on_StartPushButton_toggled(bool checked)
{

    if (not mpFTrSettings)
    {
        QMessageBox::critical(this,tr("Default Settings Not Set"),tr("Default Settings hasn't been set yet. Please click Load button to load default settings"));
        // unset start button
        ui->StartPushButton->setChecked(false);
        return;
    }

    if(checked)
    {
        // change StartButton text to Stop
        ui->StartPushButton->setText("Stop");

        // disable ui
        ui->OpenFileLineEdit->setEnabled(false);
        //ui->VidIntervalSpinBox->setEnabled(false);
        ui->TrackerParamsGroupBox->setEnabled(false);
        ui->DetectorParamsGroupBox->setEnabled(false);

        ui->OpenFilePushButton->setEnabled(false);
        ui->TrackingMethodsGroupBox->setEnabled(false);
        ui->TrackersGroupBox->setEnabled(false);
        ui->StorageOptionsGroupBox->setEnabled(false);
        ui->NonfaceFilterCheckBox->setEnabled(false);
        ui->KalmanFilterCheckBox->setEnabled(false);

        // enable ui
        ui->PausePushButton->setEnabled(true);

        if (ui->ImageInputRadioButton->isChecked())
        {            
            if (ui->OpenFileLineEdit->text().isEmpty())
            {
                QMessageBox::critical(this,tr("Wrong Default Image Path"),tr("Loading an image is not correct yet. Please set the right path of an image."));

                // unset start button
                ui->StartPushButton->setChecked(false);
                return;
            }

            cv::Mat image, dispImg;

            image = cv::imread(ui->OpenFileLineEdit->text().toStdString());

            // Show the image
            cv::cvtColor(image,dispImg, CV_BGR2RGB);
            ui->openCVViewer1->showImage(dispImg);

        }
        else
        {
            // currently only run tracking in video mode
            if ( (not ui->ImageInputRadioButton->isChecked()) && (not mpFTrSettings))
            {
                if (not checked)
                {
                    return;
                }
                QMessageBox::critical(this,tr("Default Settings Not Set"),tr("Default Settings hasn't been set yet. Please click Load button to load default settings"));

                // unset start button
                ui->StartPushButton->setChecked(false);
                return;
            }

            updateParams();

            // run my tracking code
            QString media_url = ui->OpenFileLineEdit->text();
            tr1 = new FaceTrackingMethod1(std::string(media_url.toUtf8().constData()),mpFTrSettings);
            gStartTime();

            if (ui->TrackingMethodHSVHistRadioButton->isChecked())
            {
                tr1->setTrackingMethod(vml::FaceTrackerFeature_HIST_HSV);    // Test for Face tracker source
            }
            else if (ui->TrackingMethodYCrCbHistRadioButton->isChecked())
            {
                tr1->setTrackingMethod(vml::FaceTrackerFeature_HIST_YCrCb);
            }
            else if (ui->TrackingMethodrgChromHistRadioButton->isChecked())
            {
                tr1->setTrackingMethod(vml::FaceTrackerFeature_HIST_rgCHROM);
            }
            else if (ui->TrackingMethodRGBHistRadioButton->isChecked())
            {
                tr1->setTrackingMethod(vml::FaceTrackerFeature_HIST_RGB);
            }
            else if (ui->TrackingMethodDlibCorrRadioButton->isChecked())
            {
                tr1->setTrackingMethod(vml::FaceTrackerFeature_DLIB);
            }
            else if (ui->TrackingMethodHybridRadioButton->isChecked())
            {
                tr1->setTrackingMethod(vml::FaceTrackerFeature_Hybrid);
            }


/*
            if (ui->TrackingMethodHSVHistRadioButton->isChecked())
            {
                tr1->setTrackingMethod(vml::FaceTrackerFeature_HIST_HSV);    // Test for Face tracker source
            }
            else if (ui->TrackingMethodYCrCbHistRadioButton->isChecked())
            {
                tr1->setTrackingMethod(vml::FaceTrackerFeature_HIST_YCrCb);
            }
            else if (ui->TrackingMethodrgChromHistRadioButton->isChecked())
            {
                tr1->setTrackingMethod(vml::FaceTrackerFeature_HIST_rgCHROM);
            }
            else if (ui->TrackingMethodRGBHistRadioButton->isChecked())
            {
                tr1->setTrackingMethod(vml::FaceTrackerFeature_HIST_RGB);
            }
            else if (ui->TrackingMethodDlibCorrRadioButton->isChecked())
            {
                tr1->setTrackingMethod(vml::FaceTrackerFeature_DLIB);
            }
            else if (ui->TrackingMethodHybridRadioButton->isChecked())
            {
                tr1->setTrackingMethod(vml::FaceTrackerFeature_Hybrid);
            }

            // update parameters for tracker
            mpFTrSettings->setFloat("FaceTracker/endTrackerParam",ui->EndTrackerParamDoubleSpinBox->value());
            mpFTrSettings->setFloat("FaceTracker/searchWindowRatio",ui->SearchWindowRatioDoubleSpinBox->value());
            mpFTrSettings->setFloat("FaceTracker/inactiveTargetInterval",ui->InactiveTargetIntervalDoubleSpinBox->value());

            // update parameters for detector
           mpFTrSettings->setFloat("FaceDetection/scaleFactor", ui->scaleFactorDoubleSpinBox->value());
           mpFTrSettings->setInt( "FaceDetection/minNeighbors", ui->minNeighborSpinBox->value());

           // update parameters for storing faceData
           mpFTrSettings->setBool("FaceTargetManager/storeTarget [bool]", ui->StoreTargetSpinBox->value());
           mpFTrSettings->setString("FaceTargetManager/storeTargetPath", ui->StoreTargetPathLineEdit->text());
           mpFTrSettings->setBool("FaceTargetManager/numberStoreFaces", ui->NumberStoreFacesSpinBox->value());

*/
            // set display window captions
            ui->Viewer1Label->setText("Input Image");

            // start timer
            timer->start(ui->VidIntervalSpinBox->value());


        }
    }
    else  // unchecked
    {
        ui->StartPushButton->setText("Start");

        // disable ui
        on_PausePushButton_toggled(false);
        ui->PausePushButton->setEnabled(false);

        // enable ui
        ui->OpenFileLineEdit->setEnabled(true);
        //ui->VidIntervalSpinBox->setEnabled(true);
        ui->TrackerParamsGroupBox->setEnabled(true);
        ui->DetectorParamsGroupBox->setEnabled(true);
        ui->StorageOptionsGroupBox->setEnabled(true);

        ui->OpenFilePushButton->setEnabled(true);
        ui->TrackingMethodsGroupBox->setEnabled(true);
        ui->TrackersGroupBox->setEnabled(true);       
        ui->NonfaceFilterCheckBox->setEnabled(true);
        ui->KalmanFilterCheckBox->setEnabled(true);

        ui->Viewer1Label->setText("");

        // stop all timer
        timer->stop();

        // clear viewers
        ui->openCVViewer1->clearImage();
        ui->openCVViewer2->clearImage();

        // stop on_CamShiftTrackerRadioButton_clickedvideo capture
        if (mCapture.isOpened())
            mCapture.release();

        if (tr1 &&  mpFTrSettings) {
            delete(tr1);
            tr1 = NULL;
        }

    }
}

void MainWindow::on_PausePushButton_toggled(bool checked)
{
    if(checked)
    {
        // chage capture to Resume
        ui->PausePushButton->setText("Resume");
        //ui->VidIntervalSpinBox->setEnabled(true);
        ui->TrackerParamsGroupBox->setEnabled(true);

        // stop the timers
        timer->stop();


    }
    else
    {
        // chage caption to pause
        ui->PausePushButton->setText("Pause");
        //ui->VidIntervalSpinBox->setEnabled(false);
        ui->TrackerParamsGroupBox->setEnabled(false);
/*
        // update parameters for tracker
        mpFTrSettings->setFloat("FaceTracker/endTrackerParam",ui->EndTrackerParamDoubleSpinBox->value());
        mpFTrSettings->setFloat("FaceTracker/searchWindowRatio",ui->SearchWindowRatioDoubleSpinBox->value());
        mpFTrSettings->setFloat("FaceTracker/inactiveTargetInterval",ui->InactiveTargetIntervalDoubleSpinBox->value());

        // update parameters for detector
       mpFTrSettings->setFloat("FaceDetection/scaleFactor", ui->scaleFactorDoubleSpinBox->value());
       mpFTrSettings->setInt( "FaceDetection/minNeighbors", ui->minNeighborSpinBox->value());

       // update parameters for storing faceData
       mpFTrSettings->setBool("FaceTargetManager/storeTarget [bool]", ui->StoreTargetSpinBox->value());
       mpFTrSettings->setString("FaceTargetManager/storeTargetPath", ui->StoreTargetPathLineEdit->text());
       mpFTrSettings->setBool("FaceTargetManager/numberStoreFaces", ui->NumberStoreFacesSpinBox->value());
*/

        updateParams();
        // restart the timers        
        timer->start(ui->VidIntervalSpinBox->value());

    }

}

void MainWindow::updateParams()
{

    // update parameters for tracker
    mpFTrSettings->setFloat("FaceTracker/endTrackerParam",ui->EndTrackerParamDoubleSpinBox->value());
    mpFTrSettings->setFloat("FaceTracker/searchWindowRatio",ui->SearchWindowRatioDoubleSpinBox->value());
    //mpFTrSettings->setFloat("FaceTracker/inactiveTargetInterval",ui->InactiveTargetIntervalDoubleSpinBox->value());
    mpFTrSettings->setFloat("FaceTracker/inactiveTimeout",ui->InactiveTargetIntervalDoubleSpinBox->value());

    // update parameters for detector
    mpFTrSettings->setFloat("FaceDetection/scaleFactor", ui->scaleFactorDoubleSpinBox->value());
    mpFTrSettings->setInt( "FaceDetection/minNeighbors", ui->minNeighborSpinBox->value());
    mpFTrSettings->setBool("FaceDetection/FPFiltering", ui->FPFilteringCheckBox->isChecked());

    // update parameters for storing faceData
    mpFTrSettings->setBool("FaceTargetManager/storeTarget [bool]", ui->StoreTargetSpinBox->value());
    mpFTrSettings->setString("FaceTargetManager/storeTargetPath", ui->StoreTargetPathLineEdit->text().toStdString());
    mpFTrSettings->setInt("FaceTargetManager/numberStoreFaces", ui->NumberStoreFacesSpinBox->value());

    if(ui->NonfaceFilterCheckBox->isChecked())
        mpFTrSettings->setBool("FaceDetection/NFFiltering", true);
    else
        mpFTrSettings->setBool("FaceDetection/NFFiltering", false);

    if(ui->KalmanFilterCheckBox->isChecked())
        mpFTrSettings->setBool("FaceTracker/doKalmanFilter", true);
    else
        mpFTrSettings->setBool("FaceTracker/doKalmanFilter", false);


}


void MainWindow::on_toolButton_clicked()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::Directory);
    QString newPath = dialog.getExistingDirectory(this,tr("Settings Path"), ui->SettingsPathLineEdit->text(),QFileDialog::ShowDirsOnly);

    // set url
    ui->SettingsPathLineEdit->setText(newPath);
}


void MainWindow::on_TrackingMethodHSVHistRadioButton_clicked(bool checked)
{

    if (not mpFTrSettings)
    {
        QMessageBox::critical(this,tr("Default Settings Not Set"),tr("Default Settings hasn't been set yet. Please click Load button to load default settings"));
        return;
    }

    if (checked)
    {
        mpFTrSettings->setString("FaceTracker/trackingMethod","FaceTrackerFeature_HIST_HSV");
    }

}

void MainWindow::on_TrackingMethodYCrCbHistRadioButton_clicked(bool checked)
 {

     if (not mpFTrSettings)
     {
         QMessageBox::critical(this,tr("Default Settings Not Set"),tr("Default Settings hasn't been set yet. Please click Load button to load default settings"));
         return;
     }

     if (checked)
     {
         mpFTrSettings->setString("FaceTracker/trackingMethod","FaceTrackerFeature_HIST_YCrCb");
     }
 }
void MainWindow::on_TrackingMethodrgChromHistRadioButton_clicked(bool checked)
{
    if (not mpFTrSettings)
    {
        QMessageBox::critical(this,tr("Default Settings Not Set"),tr("Default Settings hasn't been set yet. Please click Load button to load default settings"));
        return;
    }

    if (checked)
    {
        mpFTrSettings->setString("FaceTracker/trackingMethod","FaceTrackerFeature_HIST_rgCHROM");
    }
}

void MainWindow::on_TrackingMethodRGBHistRadioButton_clicked(bool checked)
{
    if (not mpFTrSettings)
    {
        QMessageBox::critical(this,tr("Default Settings Not Set"),tr("Default Settings hasn't been set yet. Please click Load button to load default settings"));
        return;
    }

    if (checked)
    {
        mpFTrSettings->setString("FaceTracker/trackingMethod","FaceTrackerFeature_HIST_RGB");
    }
}

void MainWindow::on_TrackingMethodDlibCorrRadioButton_clicked(bool checked)
{
    if (not mpFTrSettings)
    {
        QMessageBox::critical(this,tr("Default Settings Not Set"),tr("Default Settings hasn't been set yet. Please click Load button to load default settings"));
        return;
    }

    if (checked)
    {
        mpFTrSettings->setString("FaceTracker/trackingMethod","FaceTrackerFeature_DLIB");
    }
}

void MainWindow::on_TrackingMethodHybridRadioButton_clicked(bool checked)
{
    if (not mpFTrSettings)
    {
        QMessageBox::critical(this,tr("Default Settings Not Set"),tr("Default Settings hasn't been set yet. Please click Load button to load default settings"));
        return;
    }

    if (checked)
    {
        mpFTrSettings->setString("FaceTracker/trackingMethod","FaceTrackerFeature_Hybrid");
    }
}

void MainWindow::on_QuitPushButton_clicked()
{
    // exit app

    // if video capture is open, close it
    if ( mCapture.isOpened())
        mCapture.release();

    exit(0);
}

void MainWindow::on_LoadSettingsPushButton_toggled(bool checked)
{
    if(checked)
    {

        ui->LoadSettingsPushButton->setEnabled(false);

        // create settings and load settings files
        this->mpFTrSettings.reset(new vml::Settings);

        QString settings_path = ui->SettingsPathLineEdit->text();

        // load AutoDmgRecog.xml   as a single file
        QString settings_file = settings_path + "/AutoDmgRecog.xml";
        QFileInfo checkFile(settings_file);
        if ((not checkFile.exists())||(not checkFile.isFile()))
        {
            QMessageBox::critical(this,tr("File not found"),tr("Settings file AutoDmgRecog.xml not found"));
            this->mpFTrSettings.reset();
            return;
        }
        mpFTrSettings->parseXml(settings_file.toUtf8().constData());

/*
        // load DMGReoconition.xml
        QString settings_file = settings_path + "/DMGRecognition.xml";
        QFileInfo checkFile(settings_file);
        if ((not checkFile.exists())||(not checkFile.isFile()))
        {
            QMessageBox::critical(this,tr("File not found"),tr("Settings file DMGRecognition.xml not found"));
            this->mpFTrSettings.reset();
            return;
        }
        mpFTrSettings->parseXml(settings_file.toUtf8().constData());


        // load MLTool.xml
        settings_file = settings_path + "/MLTool.xml";
        checkFile.setFile(settings_file);
        if ((not checkFile.exists())||(not checkFile.isFile()))
        {
            QMessageBox::critical(this,tr("File not found"),tr("Settings file MLTool.xml not found"));
            this->mpFTrSettings.reset();
            return;
        }
        mpFTrSettings->parseXml(settings_file.toUtf8().constData());


        // load FaceTrackers.xml
        settings_file = settings_path + "/FaceTracker.xml";
        checkFile.setFile(settings_file);
        if ((not checkFile.exists())||(not checkFile.isFile()))
        {
            QMessageBox::critical(this,tr("File not found"),tr("Settings file FaceTracker.xml not found"));
            this->mpFTrSettings.reset();
            return;
        }
        mpFTrSettings->parseXml(settings_file.toUtf8().constData());

        // load FaceTarget.xml
        settings_file = settings_path + "/FaceTarget.xml";
        checkFile.setFile(settings_file);
        if ((not checkFile.exists())||(not checkFile.isFile()))
        {
            QMessageBox::critical(this,tr("File not found"),tr("Settings file FaceTracker.xml not found"));
            this->mpFTrSettings.reset();
            return;
        }
        mpFTrSettings->parseXml(settings_file.toUtf8().constData());
*/

        // set a tracking method
        std::string tr_method = mpFTrSettings->getString("FaceTracker/trackingMethod");
//        mpFTrSettings->setString("BaseTracker/trackingMethod",tr_method);
//        mpFTrSettings->setFloat("BaseTracker/searchWindowRatio",mpFTrSettings->getFloat("FaceTracker/searchWindowRatio"));


        if (tr_method == "FaceTrackerFeature_HIST_HSV")
        {
            ui->TrackingMethodHSVHistRadioButton->setChecked(true);
        }
        else if (tr_method == "FaceTrackerFeature_HIST_YCrCb")
        {
            ui->TrackingMethodYCrCbHistRadioButton->setChecked(true);
        }
        else if (tr_method == "FaceTrackerFeature_HIST_rgCHROM")
        {
            ui->TrackingMethodrgChromHistRadioButton->setChecked(true);
        }
        else if (tr_method == "FaceTrackerFeature_HIST_RGB")
        {
            ui->TrackingMethodRGBHistRadioButton->setChecked(true);
        }
        else if (tr_method == "FaceTrackerFeature_DLIB")
        {
            ui->TrackingMethodDlibCorrRadioButton->setChecked(true);
        }
        else if (tr_method == "FaceTrackerFeature_Hybrid")
        {
            ui->TrackingMethodHybridRadioButton->setChecked(true);
        }

        std::string cascadeFileName = mpFTrSettings->getString("FaceDetection/cascadeFile"," ", false);
        //qDebug() << cascadeFileName.c_str();


    }

    // update the changed parameters on the GUI
    updateParams();
}

void MainWindow::on_WebCamInputRadioButton_clicked()
{
    QString lineText = "0";
    ui->OpenFileLineEdit->setText(lineText);
}

void MainWindow::on_stepPushButton_clicked()
{
    this->updateMethod();
}



void MainWindow::on_MeanShiftTrackerRadioButton_clicked(bool checked)
{
    if (not mpFTrSettings)
    {
        QMessageBox::critical(this,tr("Default Settings Not Set"),tr("Default Settings hasn't been set yet. Please click Load button to load default settings"));
        return;
    }

    if (checked)
    {
        mpFTrSettings->setBool("FaceTracker/useCamShift [bool]",false);
    }
}

void MainWindow::on_CamShiftTrackerRadioButton_clicked(bool checked)
{
    if (not mpFTrSettings)
    {
        QMessageBox::critical(this,tr("Default Settings Not Set"),tr("Default Settings hasn't been set yet. Please click Load button to load default settings"));
        return;
    }

    if (checked)
    {
        mpFTrSettings->setBool("FaceTracker/useCamShift [bool]",true);
    }
}



void MainWindow::on_FaceDetectTestPushButton_clicked()
{

    // temporary codes

}

void MainWindow::initFDParams()
{
    mPeopleCount = 0;
    mDetectedPeopleCount = 0;

    mFrameCount = 0;
    mDetectedFaceCount =0;
    mDetectedFaceAFCount =0;
    curMinFaceSize = 10000;
    curMaxFaceSize = 0;

    // labels
    ui->PeopleCountLabel->setText(QString::number(0));
    ui->DetectedPeopleCountLabel->setText(QString::number(0));
    ui->DetectionRateLabel->setText(QString::number(0));
    ui->AvgFaceNumLabel->setText(QString::number(0));
    ui->xROILineEdit->setText(QString::number(0));
    ui->yROILineEdit->setText(QString::number(0));
    ui->wROILineEdit->setText(QString::number(1.0));
    ui->hROILineEdit->setText(QString::number(1.0));

    ui->DetectedFaceCountLabel->setText(QString::number(0));
    ui->DetectedMinFaceSizeLabel->setText(QString::number(0));
    ui->DetectedMaxFaceSizeLabel->setText(QString::number(0));
    ui->FilteredFaceCountLabel->setText(QString::number(0));

}

void MainWindow::on_FDPushButton_toggled(bool checked)
{
    ui->openCVViewer1->clearImage();
    ui->openCVViewer2->clearImage();

    if (not mpFTrSettings)
    {
        QMessageBox::critical(this,tr("Default Settings Not Set"),tr("Default Settings hasn't been set yet. Please click Load button to load default settings"));
        // unset start button
        ui->FDPushButton->setChecked(false);
        return;
    }

    if(checked)
    {
        // change StartButton text to Stop
        ui->FDPushButton->setText("Stop a test");

        // enable ui
        ui->FDPausePushButton->setEnabled(true);

        QString media_url = ui->OpenFileLineEdit->text();
        dt1 = new FaceDetectMethod1(std::string(media_url.toUtf8().constData()),ui->scaleFactorDoubleSpinBox->value(), ui->minNeighborSpinBox->value(), mpFTrSettings);

        initFDParams();
/*
        mPeopleCount = 0;
        mDetectedPeopleCount = 0;

        mFrameCount = 0;
        mDetectedFaceCount =0;
        mDetectedFaceAFCount =0;
        curMinFaceSize = 10000;
        curMaxFaceSize = 0;
*/
        // start timer
        timer2->start(ui->VidIntervalForDetectorSpinBox->value());
        gStartTime();
    }
    else
    {
        // change StartButton text to Stop
        ui->FDPushButton->setText("FD Test");

        // disable ui
        on_FDPausePushButton_toggled(false);
        ui->FDPausePushButton->setEnabled(false);

        timer2->stop();

        if (dt1) {
            delete(dt1);
            dt1 = NULL;
        }
    }
}

void MainWindow::on_FDPausePushButton_toggled(bool checked)
{
    if(checked)
    {
        // chage capture to Resume
        ui->FDPausePushButton->setText("Resume");

        // stop the timers
        timer2->stop();


    }
    else
    {
        // chage caption to pause
        ui->FDPausePushButton->setText("Pause");
        //ui->VidIntervalSpinBox->setEnabled(false);

        // restart the timers
        timer2->start(ui->VidIntervalForDetectorSpinBox->value());

    }
}

void MainWindow::on_FDStepPushButton_clicked()
{
    this->updateDetection();
}


// Simple test for posing an image to the cloudinary using curl library
void MainWindow::on_PostImageToCloudinaryPushButton_clicked()
{
    // Load an image
        cv::Mat imgMat = cv::imread("ai.jpg");
        cv::imshow("TEST", imgMat);


     // Image to buffered image
        std::vector<unsigned char> imgBuffer;
        cv::imencode(".jpg", imgMat, imgBuffer);

        std::string imgStrBuff = std::string(imgBuffer.begin(), imgBuffer.end());
        char *imgBuffPtr = (char *)imgBuffer.data();
        long imgBuffLength = static_cast<long>(imgStrBuff.size());


        // Setup cloudinary info
            // Timestamp: Unix time in (s)
            std::time_t t = std::time(0);
            std::stringstream ss;
            ss << t;
            std::string unixTime = ss.str();
            //std::string unixTime = std::ctime(&t);

            // unsinged preset
            std::string upload_preset = "rwz9vlwb";

            // API_key
            std::string APIKey = "611288379721968";
            // API_Secret:
            std::string APISecret = "QBQ3C3T1oQgs1OswI2knOMmcf9Q";

            // Create SHA1 signature required by cloudinary
            std::string pid = "DKIM/cam008/0c16f18c-41b6-4746-8ea2-7a21cf72d355_n2";
            std::string hashString = "public_id="+pid+"&timestamp=" + unixTime + APISecret;
            std::string authSig = sha1(hashString);

            // POST URL
            std::string cloud_name = "vmldave";
            std::string url = "https://api.cloudinary.com/v1_1/"+ cloud_name +"/image/upload";


            // Use the vml::RestAPI by dave

            long res_code;
            vml::RestAPI  cp;

            // setup config data
            cp.setConfig(APIKey,APISecret,cloud_name,upload_preset);

            // a case of using buffered image
            res_code = cp.uploadBufferedData(pid,true, imgBuffPtr, imgBuffLength);

            // a case of using filename
//                std::string filename ="ai.jpg";
//                res_code = cp.uploadFile(pid,true, filename);

            // 200 is the
            if(res_code != 200)
            {
                printf("Cloudinary: failed: %d\n",res_code);

                //ret= false;
            }
            else
            {
                printf("Cloudinary: succeeded: %d\n",  res_code);
                //ret = true;
            }

            // Authentication message
            //std::string authString = APIKey +":" + APISecret;
            // change authInfo to auth64String
            //std::string auth64String = base64_encode(reinterpret_cast<const unsigned char *>(authString.c_str()), authString.length());
            // replace '\n' to '' in auth64String

            // Then add auth64String as the form of 'Authorization: Basic xxxxx' with auth64String at xxxx
/*
        // HTTP POST request
        curl_global_init(CURL_GLOBAL_ALL);

        // Signed POST
        //-----CURL SETUP----
            //Set headers as null
            struct curl_slist *headers = NULL;
            struct curl_httppost *formpost = NULL;
            struct curl_httppost *lastptr = NULL;

            // header
            curl_slist_append(headers,"Content-Type: multipart/form-data");

            curl_formadd(&formpost,
                   &lastptr,
                   CURLFORM_COPYNAME, "api_key",
                   CURLFORM_COPYCONTENTS, APIKey.c_str(),
                   CURLFORM_END);

            curl_formadd(&formpost,
                  &lastptr,
                  CURLFORM_COPYNAME, "public_id",
                  CURLFORM_COPYCONTENTS,  pid.c_str(),
                  CURLFORM_END);


             curl_formadd(&formpost,
                   &lastptr,
                   CURLFORM_COPYNAME, "signature",
                   CURLFORM_COPYCONTENTS, authSig.c_str(),
                   CURLFORM_END);

             curl_formadd(&formpost,
                     &lastptr,
                     CURLFORM_COPYNAME, "timestamp",
                     CURLFORM_COPYCONTENTS, unixTime.c_str(),
                     CURLFORM_END);

          // buffered image (ok)
          curl_formadd(&formpost,
                   &lastptr,
                   CURLFORM_COPYNAME, "file",
                   CURLFORM_BUFFER, "singed_uploaded img.jpg",
                   CURLFORM_BUFFERPTR, imgBuffPtr,
                   CURLFORM_BUFFERLENGTH, imgBuffLength,
                   CURLFORM_END);


//          curl_formadd(&formpost,
//                   &lastptr,
//                   CURLFORM_COPYNAME, "upload_preset",
//                   CURLFORM_COPYCONTENTS, upload_preset.c_str(),
//                   CURLFORM_END);
          // image file
//          curl_formadd(&formpost,
//                   &lastptr,
//                   CURLFORM_COPYNAME, "file",
//                   CURLFORM_FILE, "ai.jpg",
//                   CURLFORM_END);


        CURL *curlhandle = curl_easy_init();
        long res_code;
        if(curlhandle)
        {

            // ask libcurl to show us the verbose output (DEBUG ONLY)
            curl_easy_setopt(curlhandle, CURLOPT_VERBOSE, 1L);
            curl_easy_setopt(curlhandle, CURLOPT_URL, url.c_str());
            curl_easy_setopt(curlhandle, CURLOPT_POST, 1L);
            //curl_easy_setopt(curlhandle, CURLOPT_COPYPOSTFIELDS, imgBuffPtr);
            //curl_easy_setopt(curlhandle, CURLOPT_POSTFIELDSIZE, imgBuffLength);

            // For getting a body with error message in the first place
            curl_easy_setopt(curlhandle, CURLOPT_FAILONERROR, 0);

            // Second, specify the POST data
            curl_easy_setopt(curlhandle, CURLOPT_HTTPHEADER, headers);
            curl_easy_setopt(curlhandle, CURLOPT_HTTPPOST, formpost);

             CURLcode res = curl_easy_perform(curlhandle);
             curl_easy_getinfo(curlhandle, CURLINFO_RESPONSE_CODE, &res_code);

             if(res != CURLE_OK)
             {
                 printf("Cloudinary: failed: %s\n", curl_easy_strerror(res));

                 //ret= false;
             }
             else
             {
                 printf("Cloudinary: successed: %s\n",  curl_easy_strerror(res));
                 //ret = true;
             }

             // always cleanup
             curl_easy_cleanup(curlhandle);
       }
       // finish
       curl_global_cleanup();

*/

}




void MainWindow::on_RecordPushButton_clicked()
{

    // Setup
    int ex = cv::VideoWriter::fourcc('D','I','V','X');  //1734701162;  XVID, DIVX, MJPG
    double x_res = 1280;
    double y_res = 960;
    //float fps_base = 15;

    float fps = 10;
    int rectime = 60;  // (sec)

    //int rectimevalue =  rectime * fps / fps_base;

    std::string recdir = "../VideoRecord/";
    std::string filename = "test_10fps_60s_divx.avi";
    std::string vid_url = "http://192.168.0.211:8080/?action=stream?video.mjpeg";

    // Goal is to record a video during a rectime as the x_res by y_res image resolution and fps at recpath+filename
    std::string recpath = recdir + filename;


    cv::VideoCapture cap(vid_url.c_str());

    cv::VideoWriter outputVideo(recpath, ex, fps, cv::Size(x_res,y_res), true);

    if(!cap.isOpened()) {
        cout << "Not opened" << endl;
        //return -1;
    }

    //std::time_t start= std::time(0);
    int frame_count = 1;

    while (1) {
        cv::Mat frame;
        cap >> frame;
        outputVideo << frame;
        if (frame_count > fps * rectime)  //(std::difftime(std::time(0), start) >= rectimevalue)
        {
             cout<< "Recording is ended!" << endl;
             break;
        }

        frame_count++;
    }

    outputVideo.release();
    cap.release();

    //return 0;
}

void MainWindow::on_UploadImageToSFTPserverPushButton_clicked()
{
    // Load an image
        cv::Mat imgMat = cv::imread("ai.jpg");
        cv::imshow("TEST", imgMat);


     // Image to buffered image
        std::vector<unsigned char> imgBuffer;
        cv::imencode(".jpg", imgMat, imgBuffer);

        std::string imgStrBuff = std::string(imgBuffer.begin(), imgBuffer.end());
        char *imgBuffPtr = (char *)imgBuffer.data();
        long imgBuffLength = static_cast<long>(imgStrBuff.size());


        // Setup sftp info
            // Timestamp: Unix time in (s)
            std::time_t t = std::time(0);
            std::stringstream ss;
            ss << t;
            std::string unixTime = ss.str();
            //std::string unixTime = std::ctime(&t);


            // POST URL
            /*
            std::string username = "zava";
            std::string password = "zava";  //
            std::string url = "sftp://192.168.0.142:22/test_readwrite/";
            std::string pid = "DKIM-16801-cam008-gae-0c16f18c-41b6-4746-8ea2-7a21cf72d355_n2";
            */
            std::string username = "demographics";
            std::string password = "demographics";  //
            std::string url = "sftp://demoftp.videomining.com:22/demographics/";
            std::string pid = "DKIM/16801/cam008/0c16f18c-41b6-4746-8ea2-7a21cf72d355_1";
            // Use the vml::RestAPI by dave

            long res_code;
            vml::RestAPI  cp;

            // setup config data            
            cp.setConfigSFTP(url,username,password);

            // a case of using buffered image
                //res_code = cp.uploadBufferedData(pid,true, imgBuffPtr, imgBuffLength);

            // a case of using filename
            std::string filename ="ai.jpg";
            bool ret = cp.uploadFileSFTP(pid,filename);

            // 200 is the
            if(ret)
            {
                printf("SFTP: uploading is failed: %d\n",res_code);
                //ret= false;
            }
            else
            {
                printf("SFTP: uploading is succeeded: %d\n",  res_code);
                //ret = true;
            }

}

void MainWindow::on_PeopleCounterPushButton_clicked()
{
    mPeopleCount++;
    ui->PeopleCountLabel->setText(QString::number(mPeopleCount));
}

void MainWindow::on_DetectedPeopleCountPushButton_clicked()
{
    mDetectedPeopleCount++;
    ui->DetectedPeopleCountLabel->setText(QString::number(mDetectedPeopleCount));

    // update the detection rate and average face number per a person
    if(mPeopleCount)
    {
        float drate = (float)mDetectedPeopleCount*100.0f/(float)mPeopleCount;
        ui->DetectionRateLabel->setText(QString::number(drate));
    }
    if(mDetectedPeopleCount)
    {
        float avgfn = (float)(mDetectedFaceCount-mDetectedFaceAFCount)/ (float)mDetectedPeopleCount;
        ui->AvgFaceNumLabel->setText(QString::number(avgfn));
    }
}

void MainWindow::on_TempPushButton_clicked()
{
/*
    // Current time test
    time_t t = time(0);
    struct tm *now = localtime(&t);

    std::stringstream timeData;
    timeData << "time_t Local: " << (now->tm_year + 1900)  << "/" << (now->tm_mon + 1)  << "/" << now->tm_mday ;
    std::cout << timeData.str() << std::endl;

    double time = gCurrentTime();

    vml::DateTime::Struct ds;
    vml::DateTime::secsToLocal(time, ds);

    std::stringstream timeData2;
    timeData2 << "time: " << time << ", " << "Local: " << ds.year << "/" << ds.month << "/" << ds.day ;
    std::cout << timeData2.str() << std::endl;
*/
    std::string data("First");
    std::string data2("Second");
    std::string data3("Third");

    std::string str;
    str = data + data2 + "/" + data3;
    printf("%s\n",str.c_str());

}


void MainWindow::on_FetchFaceDataPushButton_clicked()
{
    // Fetch Face data from RDS table

    vml::fpp::FacePostProcess fpp;
    //vml::fpp::LocalFaceData   fDb;

    std::string storeName = "giant_6072";
    std::string devName = "fac010";

    //  std::string stime = "1485874552.0";
    //  std::string etime = "1485876000.0";

    // In terms of the hour, I guess that there is an issue about daylight saving in local.
    // If we do post-processing in the basis of days, it would be fine.
    string stime = fpp.dateTostrUTC(2017, 2, 9, 7, 0,0);
    string etime = fpp.dateTostrUTC(2017, 2, 9, 8, 0,0);

    // 1. Fetch face data
    // required for fetching the face meta data from AWS RDS DB
    fpp.setDBconfigRDS(AWS_RDS_TESTBED_URL,AWS_RDS_TESTBED_ID,AWS_RDS_TESTBED_PWD,storeName, devName);

    // required for fetching the face images from a SFTP server
    fpp.setDBConfigSFTP(SFTP_URL,SFTP_USERNAME,SFTP_PASSWD);

    if(fpp.getFaceDataFromRDS(stime,etime))
        printf("Fetch and parse the face data Correctly!!\n");
    else
        printf("Failed to fetch and parse!!\n");

    fpp.disconnectToSFTP();


}

void MainWindow::on_DownloadImageFromSFTPServerPushButton_clicked()
{

    std::string username = SFTP_USERNAME;
    std::string password = SFTP_PASSWD;
    std::string url = SFTP_URL;
    std::string faceURL = "giant/6072/fac010/2017/2/9/d1b9b1c0-e845-4b3c-9570-6f6635ce2e48_0_121.png";


    // a case of using filename
    //std::string filename ="/rd/Test_81810c96-2d32-49b5-8187-253bb703354e_0_121.png";
    //std::string filename ="/rd/d1b9b1c0-e845-4b3c-9570-6f6635ce2e48_0_121.png";

    // Use the vml::RestAPI by dave
    //vml::RestAPI  cp;
    //cp.setConfigSFTP(url,username,password);
    //bool ret = cp.downloadFileSFTP(faceURL, filename);

    vml::fpp::FacePostProcess pp;
    pp.setDBConfigSFTP(url,username,password);
    //bool ret = pp.getImageFileFromSFTP(faceURL, filename);
    cv::Mat fData;
    bool ret = pp.getBufferedDataSFTP(faceURL, fData);
    pp.disconnectToSFTP();

    // 200 is the
    if(!ret)
    {
        printf("SFTP: downloading is failed.\n");
        //ret= false;
    }
    else
    {
        printf("SFTP: downloading is succeeded.\n");
        //ret = true;

    }

    cv::namedWindow( "TEST", cv::WINDOW_AUTOSIZE );
    cv::imshow("TEST", fData);
    cv::waitKey(0);

}

void MainWindow::on_CloudAPITestPushButton_clicked()
{
    cv::Mat imgMat= cv::imread("faceImg.png");


    double gender, genderConf, ageMean, ageRange;

    vml::fpp::FacePostProcess pp;

   pp.setConfigAmzRknAPI(ACCESS_KEY_ID,SECRET_KEY,"");
   pp.connectToAmzRknAPI();
   if( pp.getFaceMetaDataByAmzRknAPI(imgMat,ageMean,ageRange,gender,genderConf))
       printf("API: process is succeded.");
   else
       printf("API: process is failed.");

    pp.disconnectToAmzRknAPI();



    //cv::namedWindow( "TEST", cv::WINDOW_AUTOSIZE );
    //cv::imshow("TEST", imgMat);
    //cv::waitKey(0);


}

void MainWindow::on_DMGProcessPushButton_clicked()
{
     // Initialization
    // 1. Get metaData for a given period
    std::string storeName = "giant_6072";
    std::string devName = "fac010";


    vml::fpp::FacePostProcess pp;

    vml::fpp::DateTime_t sdt = {2017,2,9,19,30,0};
    vml::fpp::DateTime_t edt = {2017,2,9,20,0,0};

    gStartTime();

    int ret = pp.postProcessDMG(storeName,devName, sdt, edt);

    double elapsed_time = gTimeSinceStart();

    int total_number_data = pp.getTotalFaceDataSize();

    if (ret == total_number_data)
    {
        std::cout << "All data is newly updated." << std::endl;
    }
    else
    {
        std::cout << "The number of Object that are not updated: " <<  (total_number_data - ret) << std::endl;
    }
    std::cout << "Elapsed Time: " << elapsed_time << " (s), Total # of objects: " << total_number_data  << std::endl;

    return;
}

void MainWindow::on_UpdateToRDSPushButton_clicked()
{
    double gender = 0.82;
    double age = 34.12;
    std::string faceId = "9df5c6df-b9d5-474e-a76a-99b163d21a11"; //"ff777c97-de39-4322-a759-cca0ccc7db84";

    std::string storeName = "giant_6072";
    std::string devName = "fac010";

    vml::fpp::FacePostProcess pp;

    // required for fetching the face meta data from AWS RDS DB
    pp.setDBconfigRDS(AWS_RDS_TESTBED_URL,AWS_RDS_TESTBED_ID,AWS_RDS_TESTBED_PWD,storeName, devName);
    pp.connectToRDS();

    int ret = pp.updateDMGdataToRDS(gender, age, faceId, true);

    pp.disconnectToRDS();

    return;
}

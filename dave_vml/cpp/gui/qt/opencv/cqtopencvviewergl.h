#ifndef CQTOPENCVVIEWERGL_H
#define CQTOPENCVVIEWERGL_H

#include <QtOpenGL/QGLWidget>
#include <opencv2/core/core.hpp>

class CQtOpenCVViewerGl : public QGLWidget
{
    Q_OBJECT
public:
    explicit CQtOpenCVViewerGl(QWidget *parent = 0);

signals:
    void    imageSizeChanged( int outW, int outH);  // used to resize the image outside the widget

public slots:
    bool    showImage(cv::Mat image);

    void    clearImage();

protected:
    void    initializeGL(); // OpenGL initialization
    void    paintGL();   // OpenGL rendering
    void    resizeGL( int width, int height);   // Widget resize event

    void    updateScene();
    void    renderImage();

private:
    bool    mSceneChanged;      // indicates when OpenGL view is to be redrawn

    QImage  mRenderQtImg;       // Qt image to be rendered
    cv::Mat mOrigImage;         // original opencv image to be shown

    QColor  mBgColor;

    int     mOutH;
    int     mOutW;

    float   mImgRatio;          // height/width ratio

    int     mPosX;
    int     mPosY;

};

#endif // CQTOPENCVVIEWERGL_H


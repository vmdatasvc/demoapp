#include "cqtopencvviewergl.h"

#include <QOpenGLFunctions>
#include <opencv2/imgproc/imgproc.hpp>

CQtOpenCVViewerGl::CQtOpenCVViewerGl(QWidget *parent) :
    QGLWidget(parent)
{
    mSceneChanged = false;
    mBgColor = QColor::fromRgb(150,150,150);

    mOutH = 0;
    mOutW = 0;
    mImgRatio = 4.0f/3.0f;

    mPosX = 0;
    mPosY = 0;
}

void CQtOpenCVViewerGl::initializeGL()
{
    makeCurrent();

//    QOpenGLFunctions    *f = QOpenGLContext::currentContext()->functions();
//    float   r = ((float)mBgColor.darker().red())/255.f;
//    float   g = ((float)mBgColor.darker().green())/255.f;
//    float   b = ((float)mBgColor.darker().blue())/255.f;
    //f->glClearColor(r,g,b,1.f);
    qglClearColor(mBgColor.darker());


}

void CQtOpenCVViewerGl::resizeGL(int width, int height)
{
    makeCurrent();
    glViewport(0,0,(GLint)width,(GLint)height);
    
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    
    glOrtho(0, width, 0, height, 0,1);
    
    glMatrixMode(GL_MODELVIEW);
    
    // scaled image sizes
    mOutH = width/mImgRatio;
    mOutW = width;
    
    if(mOutH>height)
    {
        mOutW = height*mImgRatio;
        mOutH = height;
    }
    
    emit imageSizeChanged(mOutW,mOutH);
    // scaled image sizes
    
    mPosX = (width-mOutW)/2;
    mPosY = (height-mOutH)/2;
    
    mSceneChanged = true;
}

void CQtOpenCVViewerGl::updateScene()
{
    if ( mSceneChanged && this->isVisible() )
    {
//        updateGL();
        update();
    }
}

void CQtOpenCVViewerGl::paintGL()
{
    makeCurrent();
    
    if ( !mSceneChanged )
        return;
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    renderImage();

    mSceneChanged = false;
}

void CQtOpenCVViewerGl::renderImage()
{
    makeCurrent();

    glClear(GL_COLOR_BUFFER_BIT);

    if ( !mRenderQtImg.isNull())
    {
        glLoadIdentity();

        QImage  image;

        glPushMatrix();
        {
            int imW = mRenderQtImg.width();
            int imH = mRenderQtImg.height();

            // The image is to be resized to fit the widget?
            if ( imW != this->size().width() &&
                 imH != this->size().height())
            {
                image = mRenderQtImg.scaled(QSize(mOutW,mOutH),
                                            Qt::IgnoreAspectRatio,
                                            Qt::SmoothTransformation);

            }
            else
                image = mRenderQtImg;

            // <------ Cenering image in draw area

            imW = image.width();
            imH = image.height();

            glDrawPixels( imW, imH, GL_RGBA, GL_UNSIGNED_BYTE, image.bits());
        }

        glPopMatrix();

        // end
        glFlush();
    }
}

bool CQtOpenCVViewerGl::showImage(cv::Mat image)
{
    image.copyTo(mOrigImage);

    mImgRatio = (float)image.cols/(float)image.rows;

    if ( image.channels() == 3 )
    {
//        cv::cvtColor(image,mOrigImage,CV_BGR2RGB);
        mRenderQtImg = QImage((const unsigned char*)(mOrigImage.data),
                              mOrigImage.cols, mOrigImage.rows,
                              mOrigImage.step, QImage::Format_RGB888);  //
    }
    else if ( image.channels() == 1 )
    {
        mRenderQtImg = QImage((const unsigned char*)(mOrigImage.data),
                              mOrigImage.cols, mOrigImage.rows,
                              mOrigImage.step, QImage::Format_Indexed8);
    }
    else
    {
        return false;
    }

    mRenderQtImg = QGLWidget::convertToGLFormat(mRenderQtImg);

    mSceneChanged = true;

    updateScene();

    return true;
}

void CQtOpenCVViewerGl::clearImage()
{
//    // initialize mRenderQtImg
//    cv::Mat timg = cv::Mat(this->size().height(),this->size().width(),CV_8U);
//    timg = 125;

//    showImage(timg);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    // Updated by Dave
    cv::Mat timg= cv::Mat(this->size().height(), this->size().width(),CV_8UC3, cv::Scalar(0,0,0));
    QImage QtImg = QImage((const unsigned char*)(timg.data),
                          timg.cols, timg.rows,
                          timg.step, QImage::Format_RGB888);

    mRenderQtImg = QGLWidget::convertToGLFormat(QtImg);   //

    mSceneChanged = true;
    updateScene();
}

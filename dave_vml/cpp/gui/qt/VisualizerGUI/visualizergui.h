#ifndef VISUALIZERGUI_H
#define VISUALIZERGUI_H

#include <QMainWindow>
#include <QTimer>
#include <QFileDialog>
#include <QFile>
#include <QMessageBox>
#include <QTextStream>
#include <QtWidgets>
#include <QComboBox>

// opencv include
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>

//#include <projects/WiFi_Shopper_Visualizer/WiFi_Shopper_Visualizer.hpp>
#include <WiFi_Shopper_Visualizer.hpp>

typedef std::shared_ptr<WiFi_Shopper_Visualizer> WiFi_Shopper_VisualizerPtr;

namespace Ui {
class VisualizerGUI;
}

class VisualizerGUI : public QMainWindow
{
    Q_OBJECT

public:
    explicit VisualizerGUI(QWidget *parent = 0);
    ~VisualizerGUI();

private slots:
    void updateImage();
    void updateThingShadowMsg();

    void on_StartButton_clicked();
    void on_QuitButton_clicked();

    void on_actionSave_triggered();
    void on_ShowSensors_toggled(bool checked);
    void on_FontScale_valueChanged(double arg1);
    void on_ThingShadowDesiredSend_clicked();
    void on_ShadowDelete_clicked();

    void on_StartCalib_clicked();    
    void on_QuitCalib_clicked();
    void on_VerifyCalib_clicked();
    void on_ShowCalib_clicked();
    void on_StopButton_clicked();
    void on_DebugIoT_clicked(bool checked);

    void on_ControlMsgSendButton_clicked();

    void on_ShadowGet_clicked();

    void on_TrajVizTime_valueChanged(double arg1);

private:
    Ui::VisualizerGUI *ui;

    // timers for processing
    //QTimer  *mpTimerMethod1;

    QThread     *mWorkerThread;

    int mCurrentTrackerIndex;

    //WiFi_Shopper_VisualizerPtr	mpWiFi_Visualizer;
    WiFi_Shopper_Visualizer	*mpWiFi_Visualizer;

    bool mIsRunning;
    CvSize mMapSize;
    vector<string> mTrackerList;
    ostringstream mThingShadowMsgStr_extracted, mThingShadowMsgStr_time_wifiTrack, mThingShadowMsgStr_time_connectorApp;
    unsigned int mThingShadowMsg_wifiTrack_cnt, mThingShadowMsg_connectorApp_cnt;

    double mPrevTime_ThingShadowReported_wifiTrack, mPrevTime_ThingShadowReported_connectorApp;

    //QComboBox *mStoreNameComboBox;

    void initializeGUI();
    void DebugOutputChangeFontSetting();
    void quitTracker(bool sendRealtimeMsg = true);


};

#endif // VISUALIZERGUI_H

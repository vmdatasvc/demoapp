/*
 * WiFi_Shopper_Visualizer.cpp
 *
 *  Created on: Oct 4, 2016
 *      Author: paulshin
 */

#include <unistd.h>

#include <QApplication>

#include "modules/utility/TimeKeeper.hpp"
#include "modules/utility/ParseConfigTxt.hpp"
#include "../VisualizerGUI/WiFi_Shopper_Visualizer.hpp"

		#define WiFiVisualizer_APP_NAME		"WiFi_Shopper_Visualizer"

        #define VISUALIZER_TESTBED_LIST_FILE    "/home/omniadmin/omnisensr-apps/config/OmniSensr_AWS_IoT_Connector/testbed_list.txt"

        // -- TODO: Read these from AWS_IoT_Connector_config.txt
        #define AWS_IOT_CERTS_ROOT_DIRECTORY_TESTBED        "/home/omniadmin/omnisensr-apps/certs_testbed"
        #define AWS_IOT_CERTS_ROOT_DIRECTORY_PRODUCTION      "/home/omniadmin/omnisensr-apps/certs_production"

        #define AWS_IOT_MQTT_HOST_TESTBED				"A9BFTZDLDJ0ZJ.iot.us-east-1.amazonaws.com"	 // -- < Customer specific MQTT HOST. The same will be used for Thing Shadow
        #define AWS_IOT_MQTT_HOST_PRODUCTION			"a1ctj8raly2bgp.iot.us-east-1.amazonaws.com"	 // -- < Customer specific MQTT HOST. The same will be used for Thing Shadow
        #define AWS_IOT_ROOT_CA_FILENAME		"rootCA.pem"	 		// -- < Root CA file name
        #define AWS_IOT_CERTIFICATE_FILENAME	"certificate.pem.crt"	 // -- < device signed certificate file name
        #define AWS_IOT_PRIVATE_KEY_FILENAME	"private.pem.key"		 // -- < Device private key filename
        #define AWS_IOT_MQTT_PORT            8883 // default port for MQTT/S

        #define MAX_DATA_BUFFER     (1024*200)    // -- AWS IoT's limit for one message is 8KB

// -- Refer to https://github.com/nlohmann/json
#include "json.hpp"
using json = nlohmann::json;
using namespace std;

extern pthread_mutex_t gMutex;

/*
    TODO: Currently a lot of shell-based mosquitto_sub or mosquitto_pub clients are being used. But, this causes a lot of zombie processes when the QT app quits.
    Also, it creates a lot of connection/disconnections to AWS IoT, adding up cost. So needs to replace them with the C++ SDK of mosquitto lib instead.
  */

/**
 * @brief WiFi_Shopper_Visualizer::WiFi_Shopper_Visualizer
 * @param trackerId_suffix
 */

WiFi_Shopper_Visualizer::WiFi_Shopper_Visualizer(string trackerId_suffix) : WiFi_Shopper_Tracker (trackerId_suffix) {

    mMqttPubMsgCnt = 1;
    mDebugIoT = false;
    mWiFiOperationMode_prev = WIFI_TRACKING;
}

/**
 * @brief WiFi_Shopper_Visualizer::~WiFi_Shopper_Visualizer
 */

WiFi_Shopper_Visualizer:: ~WiFi_Shopper_Visualizer() {
    if(mMQTT_Tracker != NULL)
        delete mMQTT_Tracker;

    mRunningWiFiSensor.clear();
    mFoundDevice.clear();
    mKnownServiceAgents.clear();
    mKnownAPs.clear();
    mDevicesOfInterest.clear();
    mOUI_Lookup.clear();
}

/**
 * @brief WiFi_Shopper_Visualizer::InitiateKillProcess
 */
void WiFi_Shopper_Visualizer::InitiateKillProcess() {

    pthread_mutex_lock(&gMutex);
    mKillRequestPending = true;
    pthread_mutex_unlock(&gMutex);

//    for(deque<FILE *>::iterator it = mOpenedPipe.begin(); it != mOpenedPipe.end(); ++it) {
//        if(*it != NULL)
//            pclose(*it);
//    }

    for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it) {
        WiFiSensor_short_Ptr os = *it;
        if(os == NULL) continue;

        pthread_mutex_lock(&gMutex);
        os->mKillRequestPending = true;
        pthread_mutex_unlock(&gMutex);

//        if(os->mStreamProcessing_thread != 0)
//            //pthread_join(os->mStreamProcessing_thread, NULL);
//            pthread_cancel(os->mStreamProcessing_thread);
    }
//    if(mPacketStreamParsing_thread != 0)
//    	pthread_join(mPacketStreamParsing_thread, NULL);
//
//    if(mWiFiCalibProcessing_thread != 0)
//    	pthread_join(mWiFiCalibProcessing_thread, NULL);
//
//    if(mPeriodicIoTProcessing_thread != 0)
//    	pthread_join(mPeriodicIoTProcessing_thread, NULL);
//
//    if(mPeriodicBufferProcessing_thread != 0)
//    	pthread_join(mPeriodicBufferProcessing_thread, NULL);

//    pthread_exit(NULL);

    emit finished();
}

/**
 * @brief WiFi_Shopper_Visualizer::ThingShadow_Delete
 * @return
 */
string WiFi_Shopper_Visualizer::ThingShadow_Delete() {
    char mqtt_sub_cmd[599] = {'\0'};

    sprintf(mqtt_sub_cmd, "mosquitto_pub --cafile %s/%s --cert %s/%s --key %s/%s --insecure -q %d -d -h %s -p %d -I %s -t \'$aws/things/%s/shadow/delete\' -m ",
            mAwsIotCertDirectory.c_str(),AWS_IOT_ROOT_CA_FILENAME, mAwsIotCertDirectory.c_str(), AWS_IOT_CERTIFICATE_FILENAME, mAwsIotCertDirectory.c_str(), AWS_IOT_PRIVATE_KEY_FILENAME,
            MQTT_BROKER_QOS, mAwsIotMqttHost.c_str(),AWS_IOT_MQTT_PORT, mTrackerID.str().c_str(), mTrackerThingName.c_str());

    ostringstream cmd_pub_desired;

    // -- Put a timeout to the system command
    //cmd_pub_desired << "timeout 7 ";

    cmd_pub_desired << mqtt_sub_cmd << "\'{\"temp\":1}\'";

    if(mDebugVerboseLevel > 0)
        std::cout << cmd_pub_desired.str() << '\n';

    //system(cmd_pub_desired.str().c_str());

    // -- Subscribe to a MQTT broker
    FILE *streamStdout = popen(cmd_pub_desired.str().c_str(), "r");

//	double currTime = gTimeSinceStart();
//	double timeElapsed = 0;
//	WiFiSensor_short_Ptr ap = NULL;

    // -- Read from remote broker and publish to local broker
    char buffer[MAX_BUFFER];
    string stdOutMsg;

    while(fgets(buffer, MAX_BUFFER, streamStdout)) {	// -- Reads a line from the pipe and save it to buffer
        stdOutMsg.append(buffer);

        if(mKillRequestPending == true)
            break;
    }

    return stdOutMsg;
}

/**
 * @brief WiFi_Shopper_Visualizer::ThingShadow_Get
 * @return
 */
string WiFi_Shopper_Visualizer::ThingShadow_Get() {
    char mqtt_sub_cmd[599] = {'\0'};

    sprintf(mqtt_sub_cmd, "mosquitto_pub --cafile %s/%s --cert %s/%s --key %s/%s --insecure -q %d -d -h %s -p %d -I %s -t \'$aws/things/%s/shadow/get\' -m ",
            mAwsIotCertDirectory.c_str(),AWS_IOT_ROOT_CA_FILENAME, mAwsIotCertDirectory.c_str(), AWS_IOT_CERTIFICATE_FILENAME, mAwsIotCertDirectory.c_str(), AWS_IOT_PRIVATE_KEY_FILENAME,
            MQTT_BROKER_QOS, mAwsIotMqttHost.c_str(),AWS_IOT_MQTT_PORT, mTrackerID.str().c_str(), mTrackerThingName.c_str());

    ostringstream cmd_pub_get;

    cmd_pub_get << mqtt_sub_cmd << "\'{\"temp\":1}\'";

    if(mDebugVerboseLevel > 0)
        std::cout << cmd_pub_get.str() << '\n';

    //system(cmd_pub_get.str().c_str());

    // -- Subscribe to a MQTT broker
    FILE *streamStdout = popen(cmd_pub_get.str().c_str(), "r");

//	double currTime = gTimeSinceStart();
//	double timeElapsed = 0;
//	WiFiSensor_short_Ptr ap = NULL;

    // -- Read from remote broker and publish to local broker
    char buffer[MAX_BUFFER];
    string stdOutMsg;

    while(fgets(buffer, MAX_BUFFER, streamStdout)) {	// -- Reads a line from the pipe and save it to buffer
        stdOutMsg.append(buffer);

        if(mKillRequestPending == true)
            break;
    }

    return stdOutMsg;
}



string WiFi_Shopper_Visualizer::ControlMsg_Send_toDev(string controlStr, unsigned int appType) {

        ostringstream controlTopic;

        controlTopic << AWS_IOT_CONTROL_CHANNEL_PREAMBLE;

        // -- the control messages should be published to 'control/<app_id>/<store_name>/<device_name>' (e.g., control/wifiRSS/vmhq/16801/cam001)
        switch(appType) {

            case connectorApp:
                controlTopic << "/" << APP_ID_CONNECTOR;
                break;
            default:
            case wifiTrack:
                controlTopic << "/" << APP_ID_WIFI_TRACKER;
                break;

        }

        // -- TODO: Set the topic for the target device, not for this device
        //controlTopic <<"/" << mIoTGateway.GetMyStoreName() << "/" << mIoTGateway.GetMyDevName();
        controlTopic <<"/" << mTargetStoreName_slash << "/" << mTargetDevName;

        // -- Connect to the AP and get the results through a MQTT broker
        char mqtt_pub_cmd[599];

        sprintf(mqtt_pub_cmd, "mosquitto_pub --cafile %s/%s --cert %s/%s --key %s/%s --insecure -q %d -d -h %s -p %d -I %s-control -t \'%s\' -m ",
                mAwsIotCertDirectory.c_str(),AWS_IOT_ROOT_CA_FILENAME, mAwsIotCertDirectory.c_str(), AWS_IOT_CERTIFICATE_FILENAME, mAwsIotCertDirectory.c_str(), AWS_IOT_PRIVATE_KEY_FILENAME,
                MQTT_BROKER_QOS, mAwsIotMqttHost.c_str(),AWS_IOT_MQTT_PORT, mTrackerID.str().c_str(), controlTopic.str().c_str());

       // printf("[VisualizerGUI] mqtt_pub_cmd = %s\n", mqtt_sub_cmd);

        ostringstream cmd_pub_control;

        cmd_pub_control << mqtt_pub_cmd
                        << "\'" << controlStr << "\'";

        //if(mDebugVerboseLevel > 0)
            std::cout << "[VisualizerGUI] mqtt_pub_cmd = " << cmd_pub_control.str() << '\n';

        //system(cmd_pub_control.str().c_str());

        // -- Subscribe to a MQTT broker
        FILE *streamStdout = popen(cmd_pub_control.str().c_str(), "r");

    //	double currTime = gTimeSinceStart();
    //	double timeElapsed = 0;
    //	WiFiSensor_short_Ptr ap = NULL;

        // -- Read from remote broker and publish to local broker
        char buffer[MAX_BUFFER];
        string stdOutMsg = cmd_pub_control.str();
        stdOutMsg.append("\n");

        while(fgets(buffer, MAX_BUFFER, streamStdout)) {	// -- Reads a line from the pipe and save it to buffer
            stdOutMsg.append(buffer);

            if(mKillRequestPending == true)
                break;
        }

        return stdOutMsg;

}


/**
 * @brief WiFi_Shopper_Visualizer::ThingShadow_Desired_Send
 * @param desiredStr
 * @return
 */
string WiFi_Shopper_Visualizer::ThingShadow_Desired_Send(string desiredStr, unsigned int appType) {

        char mqtt_sub_cmd[599] = {'\0'};

        sprintf(mqtt_sub_cmd, "mosquitto_pub --cafile %s/%s --cert %s/%s --key %s/%s --insecure -q %d -d -h %s -p %d -I %s -t \'$aws/things/%s/shadow/update\' -m ",
                mAwsIotCertDirectory.c_str(),AWS_IOT_ROOT_CA_FILENAME, mAwsIotCertDirectory.c_str(), AWS_IOT_CERTIFICATE_FILENAME, mAwsIotCertDirectory.c_str(), AWS_IOT_PRIVATE_KEY_FILENAME,
                MQTT_BROKER_QOS, mAwsIotMqttHost.c_str(),AWS_IOT_MQTT_PORT, mTrackerID.str().c_str(), mTrackerThingName.c_str());

        ostringstream cmd_pub_desired;

        //cmd_pub_desired << mqtt_sub_cmd << "'{\"state\":{\"desired\":" << desiredStr << "}, \"clientToken\":\"GUI-" << mTrackerID.str() << "-" << mMqttPubMsgCnt++ <<"\"}'";
        cmd_pub_desired << mqtt_sub_cmd << "'{\"state\":{\"desired\":{" << "\"" << appType << "\":"
                        << desiredStr
                        //<< "}}, \"clientToken\":\"GUI-" << mTrackerID.str() << "-" << mMqttPubMsgCnt++ <<"\"}'";
                        << "}}}'";

        if(mDebugIoT)
            std::cout << cmd_pub_desired.str() << endl;

        //system(cmd_pub_desired.str().c_str());

        // -- Subscribe to a MQTT broker
        FILE *streamStdout = popen(cmd_pub_desired.str().c_str(), "r");

    //	double currTime = gTimeSinceStart();
    //	double timeElapsed = 0;
    //	WiFiSensor_short_Ptr ap = NULL;

        // -- Read from remote broker and publish to local broker
        char buffer[MAX_BUFFER];
        string stdOutMsg = cmd_pub_desired.str();
        stdOutMsg.append("\n");

        while(fgets(buffer, MAX_BUFFER, streamStdout)) {	// -- Reads a line from the pipe and save it to buffer
            stdOutMsg.append(buffer);

            if(mKillRequestPending == true)
                break;
        }

        return stdOutMsg;

}

 /**
 * @brief WiFi_Shopper_Visualizer::ThingShadow_PacketStreamParsing
 * Main function for receiving and parsing Thing Shadow packet stream
 */
void WiFi_Shopper_Visualizer::ThingShadow_PacketStreamParsing() {

    FILE *streamFromTracker;

    // -- Connect to the AP and get the results through a MQTT broker
    char mqtt_sub_cmd[599] = {'\0'};
    /*
     *  mosquitto_sub --cafile /etc/mosquitto/certs/rootCA.pem --cert /etc/mosquitto/certs/039f098178-certificate.pem.crt --key /etc/mosquitto/certs/039f098178-private.pem.key
     *      --insecure -q 1 -d -h A9BFTZDLDJ0ZJ.iot.us-east-1.amazonaws.com -p 8883 -I test_sub_002 -t '$aws/things/+/shadow/update'
     */

    sprintf(mqtt_sub_cmd, "mosquitto_sub --cafile %s/%s --cert %s/%s --key %s/%s --insecure -q %d -d -h %s -p %d -I %s-shadow -t \'$aws/things/%s/shadow/update\' -v",
            mAwsIotCertDirectory.c_str(),AWS_IOT_ROOT_CA_FILENAME, mAwsIotCertDirectory.c_str(), AWS_IOT_CERTIFICATE_FILENAME, mAwsIotCertDirectory.c_str(), AWS_IOT_PRIVATE_KEY_FILENAME,
            MQTT_BROKER_QOS, mAwsIotMqttHost.c_str(),AWS_IOT_MQTT_PORT, mTrackerID.str().c_str(), mTrackerThingName.c_str());

//    sprintf(mqtt_sub_cmd, "mosquitto_sub -h %s -p %d -q %d -k %d -t \"%s\" -v",
//                            mAwsIotMqttHost.c_str(), MQTT_BROKER_PORT, MQTT_BROKER_QOS, MQTT_BROKER_KEEP_ALIVE, mMQTT_subTopic.c_str());

    printf("\n[VisualizerGUI] mqtt_sub_cmd = %s\n\n", mqtt_sub_cmd);
    fflush(stdout);

    sleep(1);


    // -- Subscribe to a MQTT broker
    streamFromTracker = popen(mqtt_sub_cmd, "r");

    mOpenedPipe.push_back(streamFromTracker);

//	double currTime = gTimeSinceStart();
//	double timeElapsed = 0;
//	WiFiSensor_short_Ptr ap = NULL;

    // -- Read from remote broker and publish to local broker
    char buffer[MAX_BUFFER];
    string lineInput;

    while(fgets(buffer, MAX_BUFFER, streamFromTracker)) {	// -- Reads a line from the pipe and save it to buffer
        lineInput.clear();
        lineInput.append(buffer);

        if(mKillRequestPending == true)
            break;

//		currTime = gTimeSinceStart();

        // -- Break the line input into words and save it into a word vector
        istringstream iss (lineInput);
        vector<string> v;
        string s;
        while(iss >> s)
            v.push_back(s);

//        fflush(stdout);
//        cout << "1 STATE MSG: \n" << lineInput << endl;
//        cout << "Is it State Msg? " << (int)(v[0].compare(1,11,"aws/things/") == 0) << endl << endl;

        // -- If the size of word vector is greater than one, then it should be the first line of message containing {topic, #DevName, IP}
        if(v.size() > 1)
            if(v[0].compare(1,11,"aws/things/") == 0) {		// -- See if the line starts with "$aws/things/"

                // -- Published topic
                mCurrentThingShadowTopic = v[0];

                ostringstream measBuff;
                for(int i=1; i<v.size(); i++)
                    measBuff << v[i];

                fflush(stdout);
                sleep(1);

                try {

                    // -- Read the JSON message and parse
                    json shadowJson = json::parse(measBuff.str());

                    // -- Extract only the body JSON part
                    json shadowJson_body = shadowJson["state"]["reported"][mAppType_Tracker];

                    mCurrentThingShadowMsg.clear();
                    mCurrentThingShadowMsg.str("");                    
                    mCurrentThingShadowMsg << std::setw(2) << shadowJson << endl;

                    // -- Signals QT that Thing Shadow msg is updated
                    emit updateThingShadowMsg();

                    if(shadowJson_body.find("noShoppersInStore") != shadowJson_body.end()) {
                        mTrackerState.noTrackedShoppersWithinArea = shadowJson_body.at("noShoppersInStore");
                    }
                    if(shadowJson_body.find("noShoppers") != shadowJson_body.end()) {
                        mTrackerState.noTrackedShoppers = shadowJson_body.at("noShoppers");
                    }
                    if(shadowJson_body.find("PeriodicProcRate") != shadowJson_body.end()) {
                        mTrackerState.avgPeriodicProcessingRate = shadowJson_body.at("PeriodicProcRate");
                    }
                    if(shadowJson_body.find("WiFiAvail") != shadowJson_body.end()) {
                        mTrackerState.WiFiSensorAvailability = shadowJson_body.at("WiFiAvail");
                    }

                    if(shadowJson_body.find("RunningTime") != shadowJson_body.end()) {
                        int runningTime_in_min = shadowJson_body.at("RunningTime");
                        double remoteStartTime = gCurrentTime() - runningTime_in_min*60.f;

                        // -- Sync the start time with the tracker's local time
                        //gSetStartTime(remoteStartTime);
                    }



                } catch (std::invalid_argument& e) {		// -- Handle exception when the received msg does not comply with JSON standard
                    // -- TODO: Implement how to handle this exception. Should we let the sender know about this?
                    std::cout << "WiFi_Shopper_Visualizer::ThingShadow_PacketStreamParsing() invalid_argument: " << e.what() << '\n';
                    std::cout << "payload = " << measBuff.str() << "\n\n";
                } catch (std::out_of_range& e) {	// -- Handle exception when a non-existing filed is accessed.
                    // -- TODO: Implement how to handle this exception. Should we let the sender know about this?
                    std::cout << "WiFi_Shopper_Visualizer::ThingShadow_PacketStreamParsing() out of range: " << e.what() << '\n';
                    std::cout << "payload = " << measBuff.str() << "\n\n";
                } catch (std::domain_error& e) {	// -- Handle exception when an incompatible conversion is attempted (e.g., float --> string).
                    // -- TODO: Implement how to handle this exception. Should we let the sender know about this?
                    std::cout << "WiFi_Shopper_Visualizer::ThingShadow_PacketStreamParsing() domain error: " << e.what() << '\n';
                    std::cout << "payload = " << measBuff.str() << "\n\n";
                }

            }
        }

    if(streamFromTracker != 0)
        pclose(streamFromTracker);

        fprintf(stderr, "[VisualizerGUI]: $$$$ Connection was closed, so packetStreamParsing() ended $$$$\n");

}

/**
 * @brief WiFi_Shopper_Visualizer::Data_PacketStreamParsing
 * Main function for receiving and parsing Data packet stream
 */
void WiFi_Shopper_Visualizer::Data_PacketStreamParsing() {
//    if(mDebugIoT) {
//    try {
//            json tempJson = R"({"CalibParams":{"cam001":{"ch1":"-0.0796832,1.86444","ch11":"-0.0796832,1.86444","ch6":"-0.0796832,1.86444"},"cam002":{"ch1":"-0.0772466,1.86463","ch11":"-0.0772466,1.86463","ch6":"-0.0772466,1.86463"},"cam003":{"ch1":"-0.080046,1.86441","ch11":"-0.080046,1.86441","ch6":"-0.080046,1.86441"},"cam005":{"ch1":"-0.0757916,1.92304","ch11":"-0.0757916,1.92304","ch6":"-0.0757916,1.92304"},"cam007":{"ch1":"-0.0711712,1.86512","ch11":"-0.0711712,1.86512","ch6":"-0.0711712,1.86512"},"cam008":{"ch1":"-0.0755359,1.86476","ch11":"-0.0755359,1.86476","ch6":"-0.0755359,1.86476"},"cam009":{"ch1":"-0.0761506,1.86472","ch11":"-0.0761506,1.86472","ch6":"-0.0761506,1.86472"},"cam010":{"ch1":"-0.0732783,1.92626","ch11":"-0.0732783,1.92626","ch6":"-0.0732783,1.92626"},"cam011":{"ch1":"-0.0667355,1.86544","ch11":"-0.0667355,1.86544","ch6":"-0.0667355,1.86544"},"cam012":{"ch1":"-0.0684239,1.86531","ch11":"-0.0684239,1.86531","ch6":"-0.0684239,1.86531"},"cam013":{"ch1":"-0.0835783,1.86416","ch11":"-0.0835783,1.86416","ch6":"-0.0835783,1.86416"},"cam014":{"ch1":"-0.0850358,1.86403","ch11":"-0.0850358,1.86403","ch6":"-0.0850358,1.86403"},"cam015":{"ch1":"-0.0782193,1.86455","ch11":"-0.0782193,1.86455","ch6":"-0.0782193,1.86455"},"cam017":{"ch1":"-0.079437,1.86446","ch11":"-0.079437,1.86446","ch6":"-0.079437,1.86446"},"cam018":{"ch1":"-0.0706749,1.86515","ch11":"-0.0706749,1.86515","ch6":"-0.0706749,1.86515"},"cam019":{"ch1":"-0.079437,1.86446","ch11":"-0.079437,1.86446","ch6":"-0.079437,1.86446"},"cam034":{"ch1":"-0.079437,1.86446","ch11":"-0.079437,1.86446","ch6":"-0.079437,1.86446"}},"date":"2016-10-14","store_name":"vmhq-16801","time":"10:24:50.823"})"_json;

//            printf("+++++++ [tempJson] ++++++++++++++++\n");
//            std::cout << std::setw(2) << tempJson << '\n';
//            printf("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");

//            // -- Split multiple fields into a vector
//            string topic = "data/wifiTrack/vmhq/16801/trac001/001/calibResult";
//            vector<string> topic_vec = vml::split(topic, "/");

//            cout << "-- The bottom topic = " << topic_vec[topic_vec.size()-1] << endl;

//    } catch (std::invalid_argument& e) {		// -- Handle exception when the received msg does not comply with JSON standard
//        // -- TODO: Implement how to handle this exception. Should we let the sender know about this?
//        std::cout << "invalid_argument: " << e.what() << '\n';
//    } catch (std::out_of_range& e) {	// -- Handle exception when a non-existing filed is accessed.
//        // -- TODO: Implement how to handle this exception. Should we let the sender know about this?
//        std::cout << "out of range: " << e.what() << '\n';
//    } catch (std::domain_error& e) {	// -- Handle exception when an incompatible conversion is attempted (e.g., float --> string).
//        // -- TODO: Implement how to handle this exception. Should we let the sender know about this?
//        std::cout << "domain error: " << e.what() << '\n';
//    }
//      }


    FILE *streamFromTracker;
    ostringstream dataTopic;

    // -- the control messages should be published to 'data/<app_id>/<store_name_prefix>/<store_name_suffix>/<device_name>/<trackerId_suffix>' (e.g., data/wifiRss/vmhq/16801/cam001/001)    
    //dataTopic << AWS_IOT_APP_DATA_CHANNEL_PREAMBLE << "/" << APP_ID_WIFI_TRACKER <<"/" << mIoTGateway.GetMyStoreName() << "/" << mIoTGateway.GetMyDevName() << "/#";
    dataTopic << AWS_IOT_APP_DATA_CHANNEL_PREAMBLE << "/" << APP_ID_WIFI_TRACKER <<"/" << mTargetStoreName_slash << "/" << mTargetDevName << "/#";

	// -- Connect to the AP and get the results through a MQTT broker
	char mqtt_sub_cmd[299];

    sprintf(mqtt_sub_cmd, "mosquitto_sub --cafile %s/%s --cert %s/%s --key %s/%s --insecure -q %d -d -h %s -p %d -I %s-data -t \'%s\' -v",
            mAwsIotCertDirectory.c_str(),AWS_IOT_ROOT_CA_FILENAME, mAwsIotCertDirectory.c_str(), AWS_IOT_CERTIFICATE_FILENAME, mAwsIotCertDirectory.c_str(), AWS_IOT_PRIVATE_KEY_FILENAME,
            MQTT_BROKER_QOS, mAwsIotMqttHost.c_str(),AWS_IOT_MQTT_PORT, mTrackerID.str().c_str(), dataTopic.str().c_str());

    printf("[VisualizerGUI] mqtt_sub_cmd = %s\n", mqtt_sub_cmd);

	// -- Subscribe to a MQTT broker
	streamFromTracker = popen(mqtt_sub_cmd, "r");

    mOpenedPipe.push_back(streamFromTracker);

	// -- Read from remote broker and publish to local broker
    char buffer[MAX_DATA_BUFFER];
    string lineInput;

    while(fgets(buffer, MAX_DATA_BUFFER, streamFromTracker)) {	// -- Reads a line from the pipe and save it to buffer
		lineInput.clear();
		lineInput.append(buffer);

        if(mKillRequestPending == true)
            break;

//		currTime = gTimeSinceStart();

		// -- Break the line input into words and save it into a word vector
		istringstream iss (lineInput);
		vector<string> v;
		string s;
		while(iss >> s)
			v.push_back(s);

        if(v.size() == 0)
            continue;


        //cout <<"ValidData? [" << (bool)(v[0].compare(0,14,"data/wifiTrack") == 0) << "]:" << lineInput << endl;

		// -- If the size of word vector is greater than one, then it should be the first line of message containing {topic, #DevName, IP}

            if(v[0].compare(0,14,"data/wifiTrack") == 0) {		// -- See if the line starts with "data/wifiTrack"

                // -- Published topic
                string topic = v[0];
                string payload = v[1];

					// -- Wait until the other thread finishes its turn.
					do {
						pthread_mutex_lock(&gMutex);
						bool isOtherThreadProcessing = mIsThereSomeProcessing;
						pthread_mutex_unlock(&gMutex);

						if(isOtherThreadProcessing > 0) usleep(5000);
						else break;
					} while(true);

					pthread_mutex_lock(&gMutex);
					mIsThereSomeProcessing = (1<<ADDING_NEW_DEVICE);
					pthread_mutex_unlock(&gMutex);

                    // -- Split multiple fields into a vector
                    vector<string> topic_vec = vml::split(topic, "/");

                    // -- Read the JSON message and parse
                    json measJson;

                    try {

                            measJson = json::parse(payload);

                    } catch (std::invalid_argument& e) {		// -- Handle exception when the received msg does not comply with JSON standard
                        // -- TODO: Implement how to handle this exception. Should we let the sender know about this?
                        std::cout << "WiFi_Shopper_Visualizer::Data_PacketStreamParsing(): json::parse(payload): invalid_argument: " << e.what() << '\n';
                        std::cout << "payload = " << payload << "\n\n";
                    } catch (std::out_of_range& e) {	// -- Handle exception when a non-existing filed is accessed.
                        // -- TODO: Implement how to handle this exception. Should we let the sender know about this?
                        std::cout << "WiFi_Shopper_Visualizer::Data_PacketStreamParsing(): json::parse(payload): out of range: " << e.what() << '\n';
                        std::cout << "payload = " << payload << "\n\n";
                    } catch (std::domain_error& e) {	// -- Handle exception when an incompatible conversion is attempted (e.g., float --> string).
                        // -- TODO: Implement how to handle this exception. Should we let the sender know about this?
                        std::cout << "WiFi_Shopper_Visualizer::Data_PacketStreamParsing(): json::parse(payload): domain error: " << e.what() << '\n';
                        std::cout << "payload = " << payload << "\n\n";
                    }

//                    //if(mDebugIoT)
//                    {

//                        if(topic_vec[topic_vec.size()-1].compare("calibResult") == 0) {
//                            cout << "-- The bottom topic = [" << topic_vec[topic_vec.size()-1] << "]" << endl;
//                            cout << "-- payload = [" << payload << "]" << endl;
//                        }
//                    }




//                        if(mDebugIoT)
//                        {
//                            printf("+++++++ [VisualizerGUI] Data Msg Received ++++++++++++++++\n");
//                            std::cout << std::setw(2) << measJson << '\n';
//                            printf("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
//                        }

                        // -- Calibration progress data
                        if(topic_vec[topic_vec.size()-1].compare("calibProgress") == 0) {

                            try {

                                if(mDebugIoT)
                                {
                                    printf("+++++++ [VisualizerGUI] calibProgress Msg Received ++++++++++++++++\n");
                                    std::cout << std::setw(2) << measJson << '\n';
                                    printf("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
                                }

                                //mWiFiOperationMode = WIFI_CALIBRATION;

                                string store_name = measJson.at("store_name").dump();
                                //string calibParamsStr = measJson.at("CalibParams").dump();
                                string date = measJson.at("date").dump();
                                string time = measJson.at("time").dump();
                                int calibElapsedTime = atoi(measJson.at("calibElapsedTime").dump().c_str());
                                int calibDuration = atoi(measJson.at("calibDuration").dump().c_str());

                                mCalibElapsedTime = calibElapsedTime;

                                cout << "[Calib Progress] calibElapsedTime = " << mCalibElapsedTime << " [sec], Total = " << calibDuration << "[sec]"  << endl;

                                json CalibProgress = measJson.at("CalibProgress");

                                // special iterator member functions for objects
                                for (json::iterator it = CalibProgress.begin(); it != CalibProgress.end(); ++it) {
                                    string dev_name = it.key();
                                    ostringstream valueStr0;
                                    valueStr0 << it.value();

                                    WiFiSensor_short_Ptr os = findRunningWiFiSensor(dev_name);

                                    //cout << dev_name << " : " << valueStr0.str() << "\n";

                                    string calibProgressStr = valueStr0.str();

                                    // -- erase '"' from the address field if there is
                                    if(calibProgressStr.size() > 0)
                                        calibProgressStr.erase(std::remove(calibProgressStr.begin(), calibProgressStr.end(), '"'), calibProgressStr.end());

                                    // -- Split multiple fields into a vector
                                    vector<string> calibProgress_vec = vml::split(calibProgressStr, ",");

                                    unsigned int ch = 0;
                                    int ind = 0;
                                    for(vector<string>::iterator jt = calibProgress_vec.begin(); jt != calibProgress_vec.end(); ++jt, ++ind) {
                                        string measNumStr = *jt;
                                        int measNum = atoi(measNumStr.c_str());

                                        // -- Insert garbage measurement to match the size
                                        os->mCalibMeas[ch][ind].clear();
                                        for(int k=0; k<measNum; k++) {
                                            calibMeas_t temp;
                                            os->mCalibMeas[ch][ind].push_back(temp);
                                        }
                                    }

                                    pthread_mutex_lock(&gMutex);
                                    os->mWiFiCalibStarted[ch] = true;
                                    pthread_mutex_unlock(&gMutex);

                                }

                                if(mDebugIoT)
                                {
                                    printf("store_name: %s\n", store_name.c_str());
                                    //printf("calibParams: %s\n", calibParamsStr.c_str());
                                    printf("date: %s\n", date.c_str());
                                    printf("time: %s\n", time.c_str());
                                }

                            } catch (std::invalid_argument& e) {		// -- Handle exception when the received msg does not comply with JSON standard
                                // -- TODO: Implement how to handle this exception. Should we let the sender know about this?
                                std::cout << "WiFi_Shopper_Visualizer::Data_PacketStreamParsing(): calibProgress invalid_argument: " << e.what() << '\n';
                                std::cout << "payload = " << payload << "\n\n";
                            } catch (std::out_of_range& e) {	// -- Handle exception when a non-existing filed is accessed.
                                // -- TODO: Implement how to handle this exception. Should we let the sender know about this?
                                std::cout << "WiFi_Shopper_Visualizer::Data_PacketStreamParsing(): calibProgress out of range: " << e.what() << '\n';
                                std::cout << "payload = " << payload << "\n\n";
                            } catch (std::domain_error& e) {	// -- Handle exception when an incompatible conversion is attempted (e.g., float --> string).
                                // -- TODO: Implement how to handle this exception. Should we let the sender know about this?
                                std::cout << "WiFi_Shopper_Visualizer::Data_PacketStreamParsing(): calibProgress domain error: " << e.what() << '\n';
                                std::cout << "payload = " << payload << "\n\n";
                            }

                        }



                        // -- Calibration result data
                        else if(topic_vec[topic_vec.size()-1].compare("calibResult") == 0) {

                            try {
                            mWiFiOperationMode = WIFI_TRACKING;

                            /*
                            data/wifiTrack/vmhq/16801/trac001/001/calib {
                                "CalibParams":{
                                        "cam001":{"ch1":[-0.0811406,1.86432],"ch11":[-0.0811406,1.86432],"ch6":[-0.0811406,1.86432]},
                                        "cam034":{"ch1":[-0.0796804,1.86444],"ch11":[-0.0796804,1.86444],"ch6":[-0.0796804,1.86444]}
                                    },
                                "date":"2016-10-13",
                                "store_name":"vmhq-16801",
                                "time":"16:35:41.989"
                            }
                            */

                            //if(mDebugIoT)
                            {
                                printf("+++++++ [VisualizerGUI] calibResult Msg Received ++++++++++++++++\n");
                                std::cout << std::setw(2) << measJson << '\n';
                                printf("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
                            }

                            string store_name = measJson.at("store_name").dump();
                            //string calibParamsStr = measJson.at("CalibParams").dump();
                            string date = measJson.at("date").dump();
                            string time = measJson.at("time").dump();
                            //string calibRssOffset = measJson.at("rssOffset").dump();
                            //mWifiCalibRssOffset = atoi(calibRssOffset.c_str());
                            mWifiCalibRssOffset = measJson.at("rssOffset").get<int>();

                            json calibParams = measJson.at("CalibParams");

                            // special iterator member functions for objects
                            for (json::iterator it = calibParams.begin(); it != calibParams.end(); ++it) {
                                string dev_name = it.key();
                                ostringstream valueStr0;
                                valueStr0 << it.value();

                                WiFiSensor_short_Ptr os = findRunningWiFiSensor(dev_name);

                                cout << dev_name << " : " << valueStr0.str() << "\n";

                                json calibParams_perCam = calibParams.at(it.key());

                                for (json::iterator jt = calibParams_perCam.begin(); jt != calibParams_perCam.end(); ++jt) {
                                    string channel = jt.key();
                                    ostringstream valueStr1;
                                    valueStr1 << jt.value();

                                    string calibParamsStr = valueStr1.str();

                                    //cout << channel << " : " << valueStr1.str() << "\n";

                                    // -- erase '"' from the address field if there is
                                    if(calibParamsStr.size() > 0)
                                        calibParamsStr.erase(std::remove(calibParamsStr.begin(), calibParamsStr.end(), '"'), calibParamsStr.end());

                                    // -- Split multiple fields into a vector
                                    vector<string> calibParams_vec = vml::split(calibParamsStr, ",");

                                    double calib_a = atof(calibParams_vec[0].c_str());
                                    double calib_b = atof(calibParams_vec[1].c_str());

                                    cout << channel << " : (" << calib_a << "," << calib_b << "," << mWifiCalibRssOffset << ")\n";

                                    int ch = 0;
                                    if(channel.compare("ch1") == 0) {
                                        ch = 0;
                                    } else if(channel.compare("ch6") == 0) {
                                        ch = 1;
                                    } else if(channel.compare("ch11") == 0) {
                                        ch = 2;
                                    }
                                    os->mCalib_a_best[ch] = os->mCalib_a[ch] = calib_a;
                                    os->mCalib_b_best[ch] = os->mCalib_b[ch] = calib_b;


                                    // -- Save the calibration results
                                    SaveWiFiCalibResults(os, ch);

                                    // -- Show and/or save the calibration result visualization.
                                    SaveWiFiCalibResultVisualization(os, ch);

                                    if(mDebugCalib)
                                        printf("[WiFi_Shopper_Tracker] OS[%s] @Ind_Ch[%d]: WiFi Calibration is DONE!\n", os->mDevName.c_str(), ch);

                                    pthread_mutex_lock(&gMutex);
                                    os->mWiFiCalibStarted[ch] = false;
                                    pthread_mutex_unlock(&gMutex);
                                }

                            }

                            if(mDebugIoT)
                            {
                                printf("store_name: %s\n", store_name.c_str());
                                //printf("calibParams: %s\n", calibParamsStr.c_str());
                                printf("date: %s\n", date.c_str());
                                printf("time: %s\n", time.c_str());
                            }

                            } catch (std::invalid_argument& e) {		// -- Handle exception when the received msg does not comply with JSON standard
                                // -- TODO: Implement how to handle this exception. Should we let the sender know about this?
                                std::cout << "WiFi_Shopper_Visualizer::Data_PacketStreamParsing(): calibResult: invalid_argument: " << e.what() << '\n';
                                std::cout << "payload = " << payload << "\n\n";
                            } catch (std::out_of_range& e) {	// -- Handle exception when a non-existing filed is accessed.
                                // -- TODO: Implement how to handle this exception. Should we let the sender know about this?
                                std::cout << "WiFi_Shopper_Visualizer::Data_PacketStreamParsing(): calibResult: out of range: " << e.what() << '\n';
                                std::cout << "payload = " << payload << "\n\n";
                            } catch (std::domain_error& e) {	// -- Handle exception when an incompatible conversion is attempted (e.g., float --> string).
                                // -- TODO: Implement how to handle this exception. Should we let the sender know about this?
                                std::cout << "WiFi_Shopper_Visualizer::Data_PacketStreamParsing(): calibResult: domain error: " << e.what() << '\n';
                                std::cout << "payload = " << payload << "\n\n";
                            }
                        }



                        // -- Completed trajectory data
                        else if(topic_vec[topic_vec.size()-1].compare("completedTracks") == 0) {

                            try {

                            deque<TrackedPos_t_> trackedPos;

                            //mWiFiOperationMode = WIFI_TRACKING;

                            /*
                            data/wifiTrack/Giant/6072/trac001/003/completedTracks
                            {
                                "A4":"0.0,5958,4964;39.9,5962,3481;41.5,6096,3735;84.5,6946,5080;130.2,6899,6256;131.8,6234,6394;224.8,5282,7846;226.3,5365,7717;",
                                "dev_id":"6226e86daae7",
                                "duration":226,
                                "cnt":8,
                                "start_time":"1477112954.9
                            }
                            */

                            if(mDebugIoT)
                            {
                                printf("+++++++ [VisualizerGUI] completedTracks Msg Received ++++++++++++++++\n");
                                std::cout << std::setw(2) << measJson << '\n';
                                printf("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
                            }

                            string tracks = measJson.at("A4").dump();
                            double start_time = measJson.at("start_time").get<double>();
                            string dev_id = measJson.at("dev_id").dump();
                            int duration = measJson.at("duration").get<int>();
                            int locCnt = measJson.at("cnt").get<int>();


                            // -- erase '"' from the address field if there is
                            if(tracks.size() > 0)
                                tracks.erase(std::remove(tracks.begin(), tracks.end(), '"'), tracks.end());

                            // -- Split multiple locations into a vector
                            vector<string> tracks_vec = vml::split(tracks, ";");

                            if(tracks_vec.size() > 0) {
                                for(vector<string>::iterator it = tracks_vec.begin(); it != tracks_vec.end(); ++it) {
                                    string locStr = *it;

                                    vector<string> each_loc = vml::split(locStr, ",");

                                    if(each_loc.size() != 3) {
                                        continue;
                                    }
                                    double dt = atof(each_loc[0].c_str());

                                    TrackedPos_t_ pos;
                                    pos.localtime = start_time + dt;
                                    pos.pos.x = atoi(each_loc[1].c_str());
                                    pos.pos.y = atoi(each_loc[2].c_str());

                                    trackedPos.push_front(pos);

                                }
                            }



                            // -- Debug
                            if(mDebugIoT) {
                                if(tracks_vec.size() > 0) {

                                    double start_time = (trackedPos.end()-1)->localtime;

                                    mClock.updateTime(start_time);

                                    cout << "## Dev[" << dev_id << "]'s Tracks, started @" << mClock.mDateTime_str << " : ";

                                    for(deque<TrackedPos_t_>::iterator it = trackedPos.end()-1; it != trackedPos.begin(); --it) {
                                        TrackedPos_t_ *pos = &*it;
                                        printf("(%.1f, %d, %d), ", pos->localtime - start_time, pos->pos.x, pos->pos.y);

                                    }
                                    cout << endl;
                                }
                            }

                            } catch (std::invalid_argument& e) {		// -- Handle exception when the received msg does not comply with JSON standard
                                // -- TODO: Implement how to handle this exception. Should we let the sender know about this?
                                std::cout << "WiFi_Shopper_Visualizer::Data_PacketStreamParsing(): completedTracks: invalid_argument: " << e.what() << '\n';
                                std::cout << "payload = " << payload << "\n\n";
                            } catch (std::out_of_range& e) {	// -- Handle exception when a non-existing filed is accessed.
                                // -- TODO: Implement how to handle this exception. Should we let the sender know about this?
                                std::cout << "WiFi_Shopper_Visualizer::Data_PacketStreamParsing(): completedTracks: out of range: " << e.what() << '\n';
                                std::cout << "payload = " << payload << "\n\n";
                            } catch (std::domain_error& e) {	// -- Handle exception when an incompatible conversion is attempted (e.g., float --> string).
                                // -- TODO: Implement how to handle this exception. Should we let the sender know about this?
                                std::cout << "WiFi_Shopper_Visualizer::Data_PacketStreamParsing(): completedTracks: domain error: " << e.what() << '\n';
                                std::cout << "payload = " << payload << "\n\n";
                            }


                        }




                        // -- Real-time location data
                        else if(topic_vec[topic_vec.size()-1].compare("realTimeLocs") == 0) {
                            try {

                                //mWiFiOperationMode = WIFI_TRACKING;

                                /*
                                data/wifiTrack/vmhq/16801/trac001/001/realTimeLocs {
                                    "Locs":"001f29fbaf3a-2070-645-532,0015c5ef43de-1066-466-420,08bd43701569-1058-443-487,",
                                    "date":"2016-10-13",
                                    "time":"16:50:53.831"
                                }
                                */

                                if(mDebugIoT)
                                {
                                    printf("+++++++ [VisualizerGUI] realTimeLocs Msg Received ++++++++++++++++\n");
                                    std::cout << std::setw(2) << measJson << '\n';
                                    printf("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
                                }

                                string locs = measJson.at("Locs").dump();
                                double localtime = measJson.at("time").get<double>();
    //                            string date = measJson.at("date").dump();
    //                            string time = measJson.at("time").dump();

                                if(mDebugIoT)
                                {
                                    printf("Locs: %s\n", locs.c_str());
    //                                printf("date: %s\n", date.c_str());
                                    printf("localtime: %f\n", localtime);
                                }

                                // -- erase '"' from the address field if there is
                                if(locs.size() > 0)
                                    locs.erase(std::remove(locs.begin(), locs.end(), '"'), locs.end());

                                // -- Split multiple fields into a vector
                                vector<string> locs_vec = vml::split(locs, ",");

                                if(locs_vec.size() > 0) {

                                    string src;
                                    Point2D_t loc;
                                    int track_age;

                                    for(vector<string>::iterator it = locs_vec.begin(); it != locs_vec.end(); ++it) {
                                        string locStr = *it;

                                        vector<string> each_loc = vml::split(locStr, "-");

                                        if(each_loc.size() != 4) {
                                            continue;
                                        }

                                        src = each_loc[0];
                                        loc.x = atoi(each_loc[1].c_str());
                                        loc.y = atoi(each_loc[2].c_str());
                                        track_age = atoi(each_loc[3].c_str());

                                        if(mDebugIoT)
                                            printf("     --> unScrambled Data: [%s] (%d, %d) @age(%d) \n", src.c_str(), loc.x, loc.y, track_age);

                                        // -- Add the track data into each device
                                        RemoteDevicePtr dev = findDevice(src);

                                        // -- See if the device is new
                                        if(dev == NULL) {

                                                // -- Create a temp measurement
                                                tcpdump_t meas;
                                                meas.src = src;
                                                meas.rss = -30;
                                                meas.deviceType = 0;

                                                // -- If device is still NULL, then we must create a brand-new entry for this src. Otherwise, we reuse the entry.
                                                dev = make_shared<RemoteDevice>();

                                                // -- Initialize and store the measurement
                                                if(initializeRemoteDevice_distributed(dev, &meas) == true) {
                                                    mFoundDevice.push_back(dev);
                                                } else {
                                                    dev = NULL;
                                                }

                                        }

                                        if(dev != NULL) {
                                            // -- Add the track data to the device buffer
                                            TrackedPos_t_ trackedDev;

                                            trackedDev.pos.x = loc.x;
                                            trackedDev.pos.y = loc.y;
                                            trackedDev.localtime = localtime;
                                            trackedDev.isReported = false;

                                            pthread_mutex_lock(&gMutex);
                                            dev->PutToTrackedPosBuf(trackedDev, mTrackedPoints_max);
                                            *dev->AtTrackedPosAge() = track_age;
                                            pthread_mutex_unlock(&gMutex);

                                            // -- Update mKalmanPos_m
                                            {
                                                if(dev->mKalmanPos_m.GetKalmanAge() == 0)	{
                                                    dev->mKalmanPos_m.Initialize(loc.x, loc.y, kalman_position_covariance_11, kalman_position_covariance_33);
                                                    dev->mKalmanPos_m.mNeedToInitialize = false;
                                                }

                                                dev->mKalmanPos_m.Update(loc.x, loc.y, 1.0);
                                            }

                                            // -- Update mKalmanSignal
                                            for(list<KalmanFilter_4S>::iterator it = dev->mKalmanAP_Signals.begin(); it != dev->mKalmanAP_Signals.end(); ++it) {
                                                KalmanFilter_4S *kal = &*it;
                                                double distToAP = sqrt(pow(kal->mCenterOfSensingField_X - loc.x,2) + pow(kal->mCenterOfSensingField_Y - loc.y,2));

                                                 if(kal->GetKalmanAge() == 0) {
                                                     kal->Initialize(distToAP, distToAP, kalman_signal_covariance_11, kalman_signal_covariance_33);
                                                     kal->mNeedToInitialize = false;
                                                 }

                                                kal->Update(distToAP, distToAP);
                                            }
                                        }
                                    }

                                    if(mDebugIoT)
                                        printf("\n");
                                }


                            } catch (std::invalid_argument& e) {		// -- Handle exception when the received msg does not comply with JSON standard
                                // -- TODO: Implement how to handle this exception. Should we let the sender know about this?
                                std::cout << "WiFi_Shopper_Visualizer::Data_PacketStreamParsing(): realTimeLocs: invalid_argument: " << e.what() << '\n';
                                std::cout << "payload = " << payload << "\n\n";
                            } catch (std::out_of_range& e) {	// -- Handle exception when a non-existing filed is accessed.
                                // -- TODO: Implement how to handle this exception. Should we let the sender know about this?
                                std::cout << "WiFi_Shopper_Visualizer::Data_PacketStreamParsing(): realTimeLocs: out of range: " << e.what() << '\n';
                                std::cout << "payload = " << payload << "\n\n";
                            } catch (std::domain_error& e) {	// -- Handle exception when an incompatible conversion is attempted (e.g., float --> string).
                                // -- TODO: Implement how to handle this exception. Should we let the sender know about this?
                                std::cout << "WiFi_Shopper_Visualizer::Data_PacketStreamParsing(): realTimeLocs: domain error: " << e.what() << '\n';
                                std::cout << "payload = " << payload << "\n\n";
                            }

                        }


					pthread_mutex_lock(&gMutex);
					//mIsThereSomeProcessing &= ~(1<<ADDING_NEW_DEVICE);
					mIsThereSomeProcessing = 0;
                    pthread_mutex_unlock(&gMutex);
            }
		}

    if(streamFromTracker != 0)
        pclose(streamFromTracker);

        fprintf(stderr, "[VisualizerGUI]: $$$$ Connection was closed, so packetStreamParsing() ended $$$$\n");
}

/**
 * @brief WiFi_Shopper_Visualizer::Data_PacketStreamParsingThread
 * @param apdata
 * @return
 */
void* WiFi_Shopper_Visualizer::Data_PacketStreamParsingThread(void *apdata) {
	WiFi_Shopper_Visualizer* pb = (WiFi_Shopper_Visualizer*)apdata;

	int cnt = 0;

	double time_diff_avg, prev_time = gCurrentTime();

	while(1) {

		pb->Data_PacketStreamParsing();

		cnt++;

		double time_diff = gCurrentTime() - prev_time;
		time_diff_avg = (time_diff_avg*cnt + time_diff)/(cnt+1);

		if( (cnt > 3 && time_diff_avg < 20)
		) {
			break;
		}

		sleep(10);
	}

	return NULL;
}

/**
 * @brief WiFi_Shopper_Visualizer::ThingShadow_PacketStreamParsingThread
 * @param apdata
 * @return
 */
void* WiFi_Shopper_Visualizer::ThingShadow_PacketStreamParsingThread(void *apdata) {
	WiFi_Shopper_Visualizer* pb = (WiFi_Shopper_Visualizer*)apdata;

	int cnt = 0;

	double time_diff_avg, prev_time = gCurrentTime();

	while(1) {

		pb->ThingShadow_PacketStreamParsing();

		cnt++;

		double time_diff = gCurrentTime() - prev_time;
		time_diff_avg = (time_diff_avg*cnt + time_diff)/(cnt+1);

		if( (cnt > 3 && time_diff_avg < 20)
		) {
			break;
		}

		sleep(10);
	}

	return NULL;
}

/**
 * @brief WiFi_Shopper_Visualizer::SendWiFiCalibMsg
 * @param mode
 */
void WiFi_Shopper_Visualizer::SendWiFiCalibMsg(unsigned int mode) {

    mWiFiOperationMode = mode;

    ostringstream cmd_pub_control, controlMsg, controlTopic;

    // -- the control messages should be published to 'control/<app_id>/<store_name>/<device_name>' (e.g., control/wifiRSS/vmhq/16801/cam001)
    //controlTopic << AWS_IOT_CONTROL_CHANNEL_PREAMBLE << "/" << APP_ID_WIFI_TRACKER <<"/" << mIoTGateway.GetMyStoreName() << "/" << mIoTGateway.GetMyDevName();
    controlTopic << AWS_IOT_CONTROL_CHANNEL_PREAMBLE << "/" << APP_ID_WIFI_TRACKER <<"/" << mTargetStoreName_slash << "/" << mTargetDevName;

    char mqtt_sub_cmd[599] = {'\0'};

    sprintf(mqtt_sub_cmd, "mosquitto_pub --cafile %s/%s --cert %s/%s --key %s/%s --insecure -q %d -d -h %s -p %d -I %s -t \'%s\' -m ",
            mAwsIotCertDirectory.c_str(),AWS_IOT_ROOT_CA_FILENAME, mAwsIotCertDirectory.c_str(), AWS_IOT_CERTIFICATE_FILENAME, mAwsIotCertDirectory.c_str(), AWS_IOT_PRIVATE_KEY_FILENAME,
            MQTT_BROKER_QOS, mAwsIotMqttHost.c_str(),AWS_IOT_MQTT_PORT, mTrackerID.str().c_str(), controlTopic.str().c_str());


    controlMsg << "\'{\"wifiCalib\":{\"Mode\":" << mWiFiOperationMode << "}}\'";

    // -- Put a timeout to the system command
    cmd_pub_control << "timeout 7 ";

    cmd_pub_control << mqtt_sub_cmd << controlMsg.str();

    printf("[WiFi_Shopper_Visualizer] Control Msg Cmd = %s\n", cmd_pub_control.str().c_str());

    system(cmd_pub_control.str().c_str());
}

// -- Extract Store name (like "vmhq-16801") from TrackerThingName (like "vmhq-16801-trac001")
void WiFi_Shopper_Visualizer::InitTargetNames(string targetName) {

        vector<string> storenametokens = vml::split(targetName, "-");
        mTargetStoreName = storenametokens[0];
        mTargetStoreName.append("-");
        mTargetStoreName.append(storenametokens[1]);

        mTargetStoreName_slash = storenametokens[0];
        mTargetStoreName_slash.append("/");
        mTargetStoreName_slash.append(storenametokens[1]);

        mTargetDevName = storenametokens[2];

        mTrackerThingName = targetName;

        printf("Store Name = %s\n",mTargetStoreName.c_str());

}

/**
 * @brief WiFi_Shopper_Visualizer::run
 * @param showGUI
 */
void WiFi_Shopper_Visualizer::run(bool showGUI) {

    loadTestbedList(VISUALIZER_TESTBED_LIST_FILE);

    if(IsThisTestbed(mTargetStoreName_slash) == true) {
        mAwsIotCertDirectory = AWS_IOT_CERTS_ROOT_DIRECTORY_TESTBED;
        mAwsIotMqttHost = AWS_IOT_MQTT_HOST_TESTBED;
    } else {
        mAwsIotCertDirectory = AWS_IOT_CERTS_ROOT_DIRECTORY_PRODUCTION;
        mAwsIotMqttHost = AWS_IOT_MQTT_HOST_PRODUCTION;
    }

    gSaveTime();

	mShowMapVisualization = showGUI;

	// -- Convert the store name from vmhq/16801 to vmhq-16801
	// -- Set Store Name: where to track shoppers. This must be consistent with the folder name under config/WiFi_Shopper_Tracker/Stores
    mAppStoreName = mIoTGateway.GetMyStoreName_dash();

	// -- Tracker ID should look like vmhq-16801-{tracker name in dev_name.txt}-{pid}.
    mTrackerID << mTargetStoreName << "-" << mTargetDevName << "-" << getpid();

	// -- Read config file
	{
		ostringstream configFilename;
        configFilename << APP_CONFIG_ROOT_PATH << "/" << WiFiTracker_APP_NAME << "/" << APP_STORE_ROOT_PATH << "/" << mTargetStoreName << "/" << APP_CONFIG_FILE;
		if(ParseSetting(configFilename.str()) == false) {
			fprintf(stderr, "[WiFi_Shopper_Visualizer] ParseSetting() failed\n");
			exit(0);
        }
	}

	gStartTime();		// -- Mark the initial time

	mAppID = APP_ID_WIFI_VISUALIZER;

	// -- Set the AppId in string. (e.g., mAppType = "8" for WiFi Tracker)
	{
		ostringstream appIdStr;

        appIdStr.clear();
        appIdStr.str("");
        appIdStr << wifiTrack;

        mAppType_Tracker = appIdStr.str();

        appIdStr.clear();
        appIdStr.str("");
        appIdStr << wifiViz;

        mAppType_Viz = appIdStr.str();

        appIdStr.clear();
        appIdStr.str("");
        appIdStr << connectorApp;

        mAppType_Connector = appIdStr.str();
    }

			// -- Define the topic for data
//			{
//				// -- Compose the publish topic
//				// -- AWS_IoT_Gateway will publish data to a topic 'data/<app_id>/<store_name_prefix>/<store_name_suffix>/<device_name>' (e.g., data/wifiRss/vmhq/16801/cam001)
//				mqttTopicData.append(AWS_IOT_APP_DATA_CHANNEL_PREAMBLE);
//				mqttTopicData.append("/");
//				mqttTopicData.append(mAppID);
//				mqttTopicData.append("/");
//				mqttTopicData.append(mIoTGateway.GetMyStoreName());
//				mqttTopicData.append("/");
//				mqttTopicData.append(mIoTGateway.GetMyDevName());
//				mqttTopicData.append("/");
//				mqttTopicData.append(mTrackerID_suffix);		// -- To differentiate different trackers running on the same device.
//
//				printf("[WiFi_Shopper_Visualizer] mqttTopicData = %s\n", mqttTopicData.c_str());
//			}

	// -- Load OUI Lookup table
	loadOUIlookuptable();

    // -- Load known APs and Stations
	loadKnownAPs_n_ServiceAgents();

	// -- Load devices of interest to be tracked explicitly
	loadDevicesOfInterest();

	// -- Load radio fingerprints for device localization
#if defined(USE_RADIO_FINGERPRINT_BASED_LOCALIZATION) || defined(USE_WIFI_PLAYER)
	loadRadioFingerprints();
#endif

    // -- Remove all the old log files
    if(mCreateMeasLog) {
        for(char i='0'; i<='9'; i++)
            for(char j='0'; j<='9'; j++) {
                char cmd[255];
                sprintf(cmd, "rm -f measurements/%c%c*", i, j);
                //printf("Performing: %s\n", cmd);
                system(cmd);
            }
        for(char i='a'; i<='z'; i++)
            for(char j='a'; j<='z'; j++) {
                char cmd[255];
                sprintf(cmd, "rm -f measurements/%c%c*", i, j);
                //printf("Performing: %s\n", cmd);
                system(cmd);
            }
    }

	{
		// -- Specify subscription topic. In this case, it will be the WiFi_Shopper_Tracker in a specific retail store
		mMQTT_subTopic.clear();
		mMQTT_subTopic.append(AWS_IOT_APP_DATA_CHANNEL_PREAMBLE);
		mMQTT_subTopic.append("/");
		mMQTT_subTopic.append(APP_ID_WIFI_TRACKER);
		mMQTT_subTopic.append("/");
		mMQTT_subTopic.append(mIoTGateway.GetMyStoreName());
		mMQTT_subTopic.append("/");
		mMQTT_subTopic.append("#");
	}

    // -- Read IP addresses of APs
    if(mMonitoringAP_IPsFilePath.str().find(".txt") != std::string::npos)
    	ReadMonitoringAPs_text(mMonitoringAP_IPsFilePath.str());
    else if(mMonitoringAP_IPsFilePath.str().find(".csv") != std::string::npos)
    	ReadMonitoringAPs_csv(mMonitoringAP_IPsFilePath.str());
    else {
    	fprintf(stderr, "[WiFi_Shopper_Visualizer] Error MONITORING_AP_IP_ADDR_LIST_FILENAME [%s] is not a valid file name/path", mMonitoringAP_IPsFilePath.str().c_str());
    	exit(0);
    }

        // -- Load per-ap calibration parameters
    //loadCalibrationParams();

	usleep(1e5);

	// -- Generate threads
	{
			// -- Create packet analyzer thread for Location Data messages
			if(pthread_create(&mDataPacketStreamParsing_thread, &mThreadAttr, Data_PacketStreamParsingThread, (void *)this))
			{
				fprintf(stderr, "Error in pthread_create(Data_PacketStreamParsingThread)\n");
                return;
			} else {
				printf( " -- pthread_create(Data_PacketStreamParsingThread) -- \n");
			}

			// -- Create packet analyzer thread for Thing Shadow messages
			if(pthread_create(&mStatePacketStreamParsing_thread, &mThreadAttr, ThingShadow_PacketStreamParsingThread, (void *)this))
			{
				fprintf(stderr, "Error in pthread_create(ThingShadow_PacketStreamParsingThread)\n");
                return;
			} else {
				printf( " -- pthread_create(ThingShadow_PacketStreamParsingThread) -- \n");
			}
	}

	usleep(1e5);

    // -- Change and send the desired state to the remote tracker so that it start sending real-time locs data
    {
        string realTimeSendMsg = "{\"RealTimeReport\":true}";

        ThingShadow_Desired_Send(realTimeSendMsg, wifiTrack);
        ControlMsg_Send_toDev(realTimeSendMsg, wifiTrack);

        sleep(1);

        ThingShadow_Desired_Send(realTimeSendMsg, connectorApp);
        ControlMsg_Send_toDev(realTimeSendMsg, connectorApp);


    }

	// -- GUI begin ---------------------------------------------------

	if(mRunningWiFiSensor.size() >= 3) {

		// -- Displays the initial image of the room
		ostringstream storeFilename;
        storeFilename << APP_CONFIG_ROOT_PATH << "/" << WiFiTracker_APP_NAME << "/" << APP_STORE_ROOT_PATH << "/" << mTargetStoreName << "/" << mStoreLayoutFilename;

        // -- Generate initial map and phy-to-img conversion function
        InitializeMapGeneration(storeFilename.str());


        // -- Calculate the coordinates of the sensors on the store layout/map
        for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it) {
            WiFiSensor_short_Ptr os = *it;

            CvPoint mapPos = ConvertPhyCoordToMapCoord(os->mSensorPos_x, os->mSensorPos_y);

            os->mSensorPos_x_map = mapPos.x;
            os->mSensorPos_y_map = mapPos.y;
        }

        if(mShowMapVisualization == true) {
            namedWindow(mImgMapName, CV_WINDOW_AUTOSIZE);
            moveWindow(mImgMapName, 0, 0);
        }

        double currTime, prevTime_map, prevTime_clear;
        prevTime_clear = prevTime_map = mCalibStartTime = gTimeSinceStart();

        currTime = prevTime_map + mPeriodicInterval_GuiMapRefresh;

		while(1) {

            if(mKillRequestPending == true)
                break;

            // -- Process QT events while still in the infinite loop
            qApp->processEvents();

            if(mWiFiOperationMode_prev != mWiFiOperationMode) {
                mWiFiOperationMode_prev = mWiFiOperationMode;

                if(mWiFiOperationMode == WIFI_CALIBRATION) {

                    // -- Prior to the new calibration, empty the existing buffer.
                    pthread_mutex_lock(&gMutex);
                    mCalibStartTime = currTime;
                    mStartWiFiCalib = true;
                    mDebugCalib = true;
                    mDebugIoT = true;

                    for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it) {
                        WiFiSensor_short_Ptr os = *it;
                        if(os == NULL) continue;

                        for(int ch=0; ch<NUM_CHANNELS_TO_CAPTURE; ch++) {	// -- For each of three channels
                            os->mTotalMeasCntThreshold[ch] = mCalibEnoughMeasBinsToKickOffCalib_min*mCalibMeasPerBin_min*3;
                            os->mCalibFailCnt[ch] = 0;
                            os->mWiFiCalibStarted[ch] = true;
                            os->mCalibMeasCollectionProgress[ch] = 0;
                            for(int i=0; i<CALIB_NUM_DISTANCE_BINS; i++) {
                                os->mCalibMeas[ch][i].clear();
                                os->mCalibMeas_sampled[ch][i].clear();
                            }

                            os->mCalib_a[ch] = 0;
                            os->mCalib_b[ch] = 0;

                            os->mCalib_a_best[ch] = 0;
                            os->mCalib_b_best[ch] = 0;

                            os->mCalibMeas_per_peer[ch].clear();

                        }
                    }
                    pthread_mutex_unlock(&gMutex);

                } else if(mWiFiOperationMode == WIFI_TRACKING) {

                    pthread_mutex_lock(&gMutex);
                    mStartWiFiCalib = false;
                    mDebugCalib = false;
                    mDebugIoT = false;

                    for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it) {
                        WiFiSensor_short_Ptr os = *it;
                        if(os == NULL) continue;

                        for(int m=0; m<NUM_CHANNELS_TO_CAPTURE; m++) {	// -- For each of three channels
                            os->mWiFiCalibStarted[m] = false;
                        }
                    }

                    pthread_mutex_unlock(&gMutex);

                } else if(mWiFiOperationMode == WIFI_CALIB_VERIFICATION) {

                    pthread_mutex_lock(&gMutex);
                    mStartWiFiCalib = false;
                    pthread_mutex_unlock(&gMutex);
                }



            }

            // -- Clear-up devices if they don't exist any more
            if(currTime - prevTime_clear >= mPeriodicInterval_DevClearUp && mRunningWiFiSensor.size() > 0) {
                // -- Discard devices that are not active
                clearUpDeviceList();

                prevTime_clear = currTime;
            }

			// -- Refresh the display
			if(currTime - prevTime_map >= mPeriodicInterval_GuiMapRefresh) {

				// -- Wait until the other thread finishes its turn.
				do {
					pthread_mutex_lock(&gMutex);
					bool isOtherThreadProcessing = mIsThereSomeProcessing; // mIsAddingNewDevice || mIsPeriodicProcessing;
					pthread_mutex_unlock(&gMutex);

					if(isOtherThreadProcessing > 0) usleep(5000);
					else break;
				} while(true);

				pthread_mutex_lock(&gMutex);
				//mIsThereSomeProcessing |= (1<<DRAWING_MAP);
				mIsThereSomeProcessing = (1<<DRAWING_MAP);
				pthread_mutex_unlock(&gMutex);

				// -- Calculate tracker stats
                //calculateTrackStats();

				// -- Draw and show the map                
                drawMap();

				pthread_mutex_lock(&gMutex);
				//mIsThereSomeProcessing &= ~(1<<DRAWING_MAP);
				mIsThereSomeProcessing = 0;
				pthread_mutex_unlock(&gMutex);

                // -- Send signal to QT
                emit updateImage();

				prevTime_map = currTime;
			}

			currTime = gTimeSinceStart();

            if(mShowMapVisualization == true) {

                // -- Note: Don't replace this with sleep() or usleep() function.
                char key = waitKey(20);

                if(key > 0)
                switch(key) {
                    default:
                    case 'h':		// -- Show help message
                        cout << "--------- Help Message --------" << endl;
                        cout << " 'a': Show Accuracy Statistics of OmniSensrs" << endl;
                        cout << " 's': Show Device Statistics" << endl;
                        cout << " 'x': Stop printing anything on the screen" << endl;
                        cout << " 'd': Change Debug Mode Params" << endl;

                        cout << " 'h': Show Help Messages" << endl << endl;
                        break;

                    case 'x': {
                        cout << "Disable all Debug Mode\n";
                        mDebugVerboseLevel = mDebugDeviceStatusVerboseMode = 0;
                        mDebugTrackerStatus = mDebugCalib = mDebugLocalization = mDebugLocalizationErrorCode = mDebugIoT = false;
                        break;
                    }
                    case 'd': {
                        char debugModeType;
                        int debugValue;

                        stringstream ss;
                        cout << "------- Choose what Debug Param. do you want to change:" << endl;
                        cout << "1: mDebugVerboseLevel (int)" << endl;
                        cout << "2: mDebugDeviceStatusVerboseMode (int)" << endl;
                        cout << "3: mDebugTrackerStatus (bool)" << endl;
                        cout << "4: mDebugCalib (bool)" << endl;
                        cout << "5: mDebugLocalization (bool)" << endl;
                        cout << "6: mDebugLocalizationErrorCode (bool)" << endl;
                        cout << "7: mDebugIoT (bool)" << endl;

                        ss.str(""); ss << vml::getLineFromStdIn();
                        ss >> debugModeType;

                        cout << "---- Change to what value? (int) ---\n";
                        ss.str(""); ss << vml::getLineFromStdIn();
                        ss >> debugValue;

                        switch(debugModeType) {
                        case '1':
                            mDebugVerboseLevel = debugValue;
                            break;
                        case '2':
                            mDebugDeviceStatusVerboseMode = debugValue;
                            break;
                        case '3':
                            mDebugTrackerStatus = debugValue;
                            break;
                        case '4':
                            mDebugCalib = debugValue;
                            break;
                        case '5':
                            mDebugLocalization = debugValue;
                            break;
                        case '6':
                            mDebugLocalizationErrorCode = debugValue;
                            break;
                        case '7':
                            mDebugIoT = debugValue;
                            break;
                        }

                        break;
                    }
                    case 'a': {		// -- Show updated accuracy statistics when tracking OmniSensrs

                        pthread_mutex_lock(&gMutex);
                        if(mFoundDevice.size() > 0) {
                            double distErrorAvg = 0, distErrorStdAvg = 0, cnt = 0;
                            for(deque<RemoteDevicePtr>::iterator it = mFoundDevice.begin(); it != mFoundDevice.end();++it)
                            {
                                RemoteDevicePtr dev = *it;
                                if(dev == NULL)
                                    continue;

                                bool isOmniSensr = dev->GetDeviceType() == OmniSensr;
                                if(isOmniSensr == true && dev->mAccuracyStat.distError.size() > 0) {
                                    printf("++ Accuracy Stat ++ OS[%s]: distError [size = %d] (avg, std) = (%.2f, %.2f) [m] w/ mPerDevRssCalibOffset = %.2f\n", dev->GetDevAddr().c_str(), (int)dev->mAccuracyStat.distError.size(), dev->mAccuracyStat.distErrorAvg/100.0, dev->mAccuracyStat.distErrorStd/100.0, dev->mPerDevRssCalibOffset);

                                    distErrorAvg += dev->mAccuracyStat.distErrorAvg;
                                    distErrorStdAvg += dev->mAccuracyStat.distErrorStd;
                                    cnt += 1;
                                }
                            }
                            if(cnt > 0) {
                                distErrorAvg /= cnt;
                                distErrorStdAvg /= cnt;

                                printf("SUMMARY ++ Accuracy Stat ++ Total distError (avg, std) = (%.2f, %.2f) [m]\n", distErrorAvg/100.0, distErrorStdAvg/100.0);
                            } else {
                                printf("No OmniSensrs are qualified for Accuracy Stat.\n");
                            }
                        }
                        pthread_mutex_unlock(&gMutex);

                        break;
                    }

                    case 's': {
                        bool onlyDevOfInterest;
                        char statType;

                        stringstream ss;
                        cout << "------- Only Device of Interest? (1 or 0)" << endl;
                        ss.str(""); ss << vml::getLineFromStdIn();
                        ss >> onlyDevOfInterest;

                        cout << "------- Choose what stat do you want:" << endl;
                        cout << "1: Total RSS Historgram" << endl;
                        cout << "2: Per-channel RSS Historgram" << endl;
                        cout << "3: Revised Per-channel RSS Historgram" << endl;
                        cout << "4: Time-Diff Historgram of Packet arrivals" << endl;

                        ss.str(""); ss << vml::getLineFromStdIn();
                        ss >> statType;

                        if(mFoundDevice.size() > 0) {
                            for(deque<RemoteDevicePtr>::iterator it = mFoundDevice.begin(); it != mFoundDevice.end();++it)
                            {
                                RemoteDevicePtr dev = *it;
                                if(dev == NULL)
                                    continue;

                                if(onlyDevOfInterest == true && isDevicesOfInterest(dev->GetDevAddr()) == false)
                                    continue;

                                switch(statType) {
                                    case '1':
                                        dev->ShowStats_RssHist_Total();
                                        break;
                                    case '2':
                                        dev->ShowStats_RssHist_PerChannel();
                                        break;
                                    case '3':
                                        dev->ShowStats_RssHist_PerChannel_Revised();
                                        break;
                                    case '4':
                                        dev->ShowStats_TimeDiffHistCDF();
                                        break;
                                }
                            }
                        }

                        break;
                    }
                }
            } else {
                usleep(20*1000);
            }
		}
	} else {
		printf("mRunningAP.size() = %d TOO LOW!\n", (int)mRunningWiFiSensor.size());
	}

    emit finished();

    return;
}


void WiFi_Shopper_Visualizer::loadTestbedList(string testbedList) {

    FILE *fp_list;

    fp_list = fopen(testbedList.c_str(), "r");
    if(fp_list == NULL) {
        fprintf(stderr, "Error opening AWS_IOT_CONNECTOR_TESTBED_LIST_FILE file\n");
        //exit(0);
    } else {
        char buffer[255];

        // -- Read from remote broker and publish to local broker
        string lineInput;
        vector<string> measBuffer;
        while(fgets(buffer, 255, fp_list)) {	// -- Reads a line from the pipe and save it to buffer
            lineInput.clear();
            lineInput.append(buffer);

            // -- If there's any comment, then discard the rest of the line
            std::size_t found = lineInput.find_first_of("#");
            // -- If '#' is found, then discard the rest.
            if(found != string::npos)
                lineInput.resize(found);

            istringstream iss (lineInput);
            string testbed;
            iss >> testbed;

            cout << "Testbed = [" << testbed << "]\n";

            mTestbedList.push_back(testbed);
        }

    }
}

bool WiFi_Shopper_Visualizer::IsThisTestbed(string storeAddr) {

    bool isTestbed = false;
    for(deque<string>::iterator it = mTestbedList.begin(); it != mTestbedList.end(); ++it) {
        string testbed = *it;

        if(testbed.compare(storeAddr) == 0) {
            isTestbed = true;
            break;
        }
    }

    return isTestbed;
}

/**
 * @brief WiFi_Shopper_Visualizer::ParseSetting
 * @param configFile
 * @return
 *
 * Read config file
 */
bool WiFi_Shopper_Visualizer::ParseSetting(string configFile) {
	ParseConfigTxt config;
	config.parse(configFile);

	int paramReadCnt = 0;

	if(config.getValue_string("VERSION", mSoftwareVersion)) { paramReadCnt++; printf("VERSION = %s\n", mSoftwareVersion.c_str()); }

	// -- Debug Mode Selection
	if(config.getValue_bool("CREATE_MEASUREMENT_LOGFILE", mCreateMeasLog)) { paramReadCnt++;	printf("CREATE_MEASUREMENT_LOGFILE = %d\n", (int)mCreateMeasLog); }

	// -- Select what to track
	if(config.getValue_bool("ONLY_FOR_DEVICES_OF_INTEREST", mOnlyDevOfInterest)) { paramReadCnt++;	printf("ONLY_FOR_DEVICES_OF_INTEREST = %d\n", (int)mOnlyDevOfInterest); }
	if(config.getValue_bool("TRACK_AP", mTrackAP)) { paramReadCnt++;	printf("TRACK_AP = %d\n", (int)mTrackAP); }
	if(config.getValue_bool("TRACK_SERVICE_AGENT", mTrackServiceAgent)) { paramReadCnt++;	printf("TRACK_SERVICE_AGENT = %d\n", (int)mTrackServiceAgent); }
	if(config.getValue_bool("TRACK_NULL_DEVICE", mTrackNullDev)) { paramReadCnt++;	printf("TRACK_NULL_DEVICE = %d\n", (int)mTrackNullDev); }

	// -- Select what to show
	if(config.getValue_bool("SHOW_NO_APS", mShowNoAPs)) { paramReadCnt++;	printf("SHOW_NO_APS = %d\n", (int)mShowNoAPs); }
	if(config.getValue_bool("SHOW_NO_SERVICE_AGENT", mShowNoServiceAgent)) { paramReadCnt++;	printf("SHOW_NO_SERVICE_AGENT = %d\n", (int)mShowNoServiceAgent); }
	if(config.getValue_bool("SHOW_RSS_MEASUREMENTS", mShowRSSMeas)) { paramReadCnt++;	printf("SHOW_RSS_MEASUREMENTS = %d\n", (int)mShowRSSMeas); }
	if(config.getValue_bool("SHOW_LOCALIZATION_POLYGON", mShowLocalizationPolygon)) { paramReadCnt++;	printf("SHOW_LOCALIZATION_POLYGON = %d\n", (int)mShowLocalizationPolygon); }
	if(config.getValue_bool("SHOW_TRAJECTORY", mShowTrajectory)) { paramReadCnt++;	printf("SHOW_TRAJECTORY = %d\n", (int)mShowTrajectory); }
		if(config.getValue_bool("SHOW_TRAJECTORY_ONLY_DevOfInt", mShowTrajectory_DevOfInt)) { paramReadCnt++;	printf("SHOW_TRAJECTORY_ONLY_DevOfInt = %d\n", (int)mShowTrajectory_DevOfInt); }
		if(config.getValue_bool("SHOW_SELECTED_SENSOR_CENTROID", mShowSelectedSensorCentroid)) { paramReadCnt++;	printf("SHOW_SELECTED_SENSOR_CENTROID = %d\n", (int)mShowSelectedSensorCentroid); }

	if(config.getValue_bool("SHOW_RAW_MESSAGE", mShowRawMsg)) { paramReadCnt++;	printf("SHOW_RAW_MESSAGE = %d\n", (int)mShowRawMsg); }
	if(config.getValue_bool("SHOW_PARSED_MESSAGE", mShowParsedMsg)) { paramReadCnt++;	printf("SHOW_PARSED_MESSAGE = %d\n", (int)mShowParsedMsg); }

	// -- Shopper-related parameters
	if(config.getValue_int("MAX_FOUND_DEVICES", mFoundDevices_max)) { paramReadCnt++;	printf("MAX_FOUND_DEVICES = %d\n", mFoundDevices_max); }
	if(config.getValue_int("MAX_MEASUREMENT_BUFFER_PER_DEV", mMeasBufferPerDev_max)) { paramReadCnt++;	printf("MAX_MEASUREMENT_BUFFER_PER_DEV = %d\n", mMeasBufferPerDev_max); }
	if(config.getValue_int("TIME_THRESHOLD_DEVICE_DISAPPEAR", mTimeThresholdForDevAbsency)) { paramReadCnt++;	printf("TIME_THRESHOLD_DEVICE_DISAPPEAR = %d\n", mTimeThresholdForDevAbsency); }
	if(config.getValue_int("MIN_SHOPPING_TIME", mShoppingTime_min)) { paramReadCnt++;	printf("MIN_SHOPPING_TIME = %d\n", mShoppingTime_min); }
	if(config.getValue_int("MAX_SHOPPING_TIME", mShoppingTime_max)) { paramReadCnt++;	printf("MAX_SHOPPING_TIME = %d\n", mShoppingTime_max); }
	if(config.getValue_int("MIN_SHOPPING_DISTANCE", mShoppingDistance_min)) { paramReadCnt++;	printf("MIN_SHOPPING_DISTANCE = %d\n", mShoppingDistance_min); }
	if(config.getValue_int("MIN_SHOPPER_DETECTION", mShopperDetection_min)) { paramReadCnt++;	printf("MIN_SHOPPER_DETECTION = %d\n", mShopperDetection_min); }
	if(config.getValue_int("MAX_TRACKED_POINTS", mTrackedPoints_max)) { paramReadCnt++;	printf("MAX_TRACKED_POINTS = %d\n", mTrackedPoints_max); }
	if(config.getValue_int("MIN_TRAJECTORY_POINTS_TO_REPORT", mTrackedPointsSizeToReport_min)) { paramReadCnt++;	printf("MIN_TRAJECTORY_POINTS_TO_REPORT = %d\n", mTrackedPointsSizeToReport_min); }
	if(config.getValue_float("TRAJECTORY_DIST_PRECISION_WHEN_REPORTING", mTrajectoryPrecision_dist)) { paramReadCnt++;	printf("TRAJECTORY_DIST_PRECISION_WHEN_REPORTING = %.2f\n", mTrajectoryPrecision_dist); }
	if(config.getValue_float("TRAJECTORY_TIME_PRECISION_WHEN_REPORTING", mTrajectoryPrecision_time)) { paramReadCnt++;	printf("TRAJECTORY_TIME_PRECISION_WHEN_REPORTING = %.2f\n", mTrajectoryPrecision_time); }


	if(config.getValue_bool("CHANNEL_SEPARATION", mChannelSeperation)) { paramReadCnt++;	printf("CHANNEL_SEPARATION = %d\n", mChannelSeperation); }
	if(config.getValue_int("MAX_CALIB_MEAS_WAIT_PERIOD", mCalibMeasWaitPeriod_max)) { paramReadCnt++;	printf("MAX_CALIB_MEAS_WAIT_PERIOD = %d\n", mCalibMeasWaitPeriod_max); }
    if(config.getValue_int("MAX_CALIB_PERIOD", mCalibPeriod_max)) { paramReadCnt++;	printf("MAX_CALIB_PERIOD = %d\n", mCalibPeriod_max); }
	if(config.getValue_int("MIN_CALIB_START_WAIT_PERIOD", mCalibStartWaitPeriod_min)) { paramReadCnt++;	printf("MIN_CALIB_START_WAIT_PERIOD = %d\n", mCalibStartWaitPeriod_min); }
	if(config.getValue_int("TIME_THRESHOLD_SERVICE_AGENT_DEVICE", mTimeThreshold_ServiceAgent)) { paramReadCnt++;	printf("TIME_THRESHOLD_SERVICE_AGENT_DEVICE = %d\n", mTimeThreshold_ServiceAgent); }
	if(config.getValue_int("MAX_SERVICE_AGENT_LIST_SIZE", mServiceAgentListSize_max)) { paramReadCnt++;	printf("MAX_SERVICE_AGENT_LIST_SIZE = %d\n", mServiceAgentListSize_max); }
	if(config.getValue_int("MAX_AP_LIST_SIZE", mApListSize_max)) { paramReadCnt++;	printf("MAX_AP_LIST_SIZE = %d\n", mApListSize_max); }
	if(config.getValue_int("MAX_ACCURACY_STAT_HISTORY", mStatAccuracySize_max)) { paramReadCnt++;	printf("MAX_ACCURACY_STAT_HISTORY = %d\n", mStatAccuracySize_max); }
	if(config.getValue_int("AP_MAX_RELIABLE_DISTANCE", mReliableDistanceInMeas_max)) { paramReadCnt++;	printf("AP_MAX_RELIABLE_DISTANCE = %d\n", mReliableDistanceInMeas_max); }


	// -- Localization parameters
	if(config.getValue_double("PERIODIC_BUFFER_PROCESSING_INTERVAL", mPeriodicBufferProcessingInterval)) {
		paramReadCnt++;
		mKalmanMeasurementLifetimeForTracking = mPeriodicBufferProcessingInterval*1.99;
		printf("PERIODIC_BUFFER_PROCESSING_INTERVAL = %.2f\n", mPeriodicBufferProcessingInterval);
	}

	if(config.getValue_bool("REPORT_REAL_TIME_TRACK_RESULTS", mReportRealTimeTrackResult)) { paramReadCnt++;	printf("REPORT_REAL_TIME_TRACK_RESULTS = %d\n", (int)mReportRealTimeTrackResult); }
	if(config.getValue_int("MEAN_SHIFT_MAX_RADIUS_EXPANSION_COUNT", mMeanShiftRadiusExpansionCnt_max)) { paramReadCnt++;	printf("MEAN_SHIFT_MAX_RADIUS_EXPANSION_COUNT = %d\n", mMeanShiftRadiusExpansionCnt_max); }
	if(config.getValue_int("MIN_NUM_MEASUREMENTS_TO_DO_LOCALIZATION", mMinNumMeasToDoLocalization)) { paramReadCnt++;	printf("MIN_NUM_MEASUREMENTS_TO_DO_LOCALIZATION = %d\n", mMinNumMeasToDoLocalization); }
	if(config.getValue_int("MAX_NUM_SENSORS_TO_CHOOSE_FOR_LOCALIZATION", mNumSensorForLocalization_max)) { paramReadCnt++;	printf("MAX_NUM_SENSORS_TO_CHOOSE_FOR_LOCALIZATION = %d\n", mNumSensorForLocalization_max); }
	if(config.getValue_int("MIN_NUM_SENSORS_TO_CHOOSE_FOR_LOCALIZATION", mNumSensorForLocalization_min)) { paramReadCnt++;	printf("MIN_NUM_SENSORS_TO_CHOOSE_FOR_LOCALIZATION = %d\n", mNumSensorForLocalization_min); }
	if(config.getValue_int("MAX_NUM_SENSORS_TO_USE_FOR_MEANSHIFT_SENSOR_SELECTION", mNumSensorForSelection_max)) { paramReadCnt++;	printf("MAX_NUM_SENSORS_TO_USE_FOR_MEANSHIFT_SENSOR_SELECTION = %d\n", mNumSensorForSelection_max); }
	if(config.getValue_int("MIN_NUM_SENSORS_TO_USE_FOR_MEANSHIFT_SENSOR_SELECTION", mNumSensorForSelection_min)) { paramReadCnt++;	printf("MIN_NUM_SENSORS_TO_USE_FOR_MEANSHIFT_SENSOR_SELECTION = %d\n", mNumSensorForSelection_min); }
	if(config.getValue_double("MAX_AVG_SQUARED_ERROR_DISTANCE_IN_LOCALIZATION", mLocalizationMaxAvgSqrErrorDistance)) { paramReadCnt++;	printf("MAX_AVG_SQUARED_ERROR_DISTANCE_IN_LOCALIZATION = %f\n", mLocalizationMaxAvgSqrErrorDistance); }
	if(config.getValue_int("LOCALIZATION_ALGORITHM_TYPE", mLocalizationAlgorithmType)) { paramReadCnt++;	printf("LOCALIZATION_ALGORITHM_TYPE = %d\n", mLocalizationAlgorithmType); }
	if(config.getValue_int("LOCALIZATION_BOUNDINGBOX_MARGIN", mLocalizationBoundingBoxMargin)) { paramReadCnt++;	printf("LOCALIZATION_BOUNDINGBOX_MARGIN = %d\n", mLocalizationBoundingBoxMargin); }
	if(config.getValue_int("LOCALIZATION_MAX_DISTANCE_FROM_CONVEXHULL", mLocalizationMaxDistFromConvexHull)) { paramReadCnt++;	printf("LOCALIZATION_MAX_DISTANCE_FROM_CONVEXHULL = %d\n", mLocalizationMaxDistFromConvexHull); }
	if(config.getValue_int("MAX_DIST_FOR_NEARBY_NEIGHBORS", mMaxDistForNearbyNeighbors)) { paramReadCnt++;	printf("MAX_DIST_FOR_NEARBY_NEIGHBORS = %d\n", mMaxDistForNearbyNeighbors); }

	// -- Measurement pre-processing
	if(config.getValue_bool("MEASUREMENT_ADJUSTMENT_ENABLE", mMeasurementAdjustmentEnable)) { paramReadCnt++;	printf("MEASUREMENT_ADJUSTMENT_ENABLE = %d\n", (int)mMeasurementAdjustmentEnable); }

	// -- Kalman filter parameters for RSS estimation
	if(config.getValue_bool("KALMAN_RSS_ENABLE", mKalmanRssEnable)) { paramReadCnt++;	printf("KALMAN_RSS_ENABLE = %d\n", (int)mKalmanRssEnable); }
	if(config.getValue_double("KALMAN_RSS_PROCESS_NOISE_VARIANCE", mKalmanRssProcessNoise_var)) { paramReadCnt++;	printf("KALMAN_RSS_PROCESS_NOISE_VARIANCE = %f\n", mKalmanRssProcessNoise_var); }
	if(config.getValue_double("KALMAN_RSS_MEASUREMENT_NOISE_VARIANCE", mKalmanRssMeasurementNoise_var)) { paramReadCnt++;	printf("KALMAN_RSS_MEASUREMENT_NOISE_VARIANCE = %f\n", mKalmanRssMeasurementNoise_var); }
	if(config.getValue_double("KALMAN_RSS_MIN_COV_COEFFICENT_POS", mKalmanRssCovCoeff_pos_min)) { paramReadCnt++;	printf("KALMAN_RSS_MIN_COV_COEFFICENT_POS = %f\n", mKalmanRssCovCoeff_pos_min); }
	if(config.getValue_double("KALMAN_RSS_MIN_COV_COEFFICENT_ACCEL", mKalmanRssCovCoeff_accel_min)) { paramReadCnt++;	printf("KALMAN_RSS_MIN_COV_COEFFICENT_ACCEL = %f\n", mKalmanRssCovCoeff_accel_min); }
	if(config.getValue_double("KALMAN_RSS_MAX_STD_ALLOWED", mKalmanRssMaxStdAllowed)) { paramReadCnt++;	printf("KALMAN_RSS_MAX_STD_ALLOWED = %f\n", mKalmanRssMaxStdAllowed); }
	if(config.getValue_double("KALMAN_RSS_TIME_SCALE", mKalmanTimeScale_rss)) { paramReadCnt++;	printf("KALMAN_RSS_TIME_SCALE = %f\n", mKalmanTimeScale_rss); }

	// -- Kalman filter parameters for Location estimation
	if(config.getValue_bool("KALMAN_POS_ENABLE", mKalmanPosEnable)) { paramReadCnt++;	printf("KALMAN_POS_ENABLE = %d\n", (int)mKalmanPosEnable); }
	if(config.getValue_double("KALMAN_POS_PROCESS_NOISE_VARIANCE", mKalmanPosProcessNoise_var)) { paramReadCnt++;	printf("KALMAN_POS_PROCESS_NOISE_VARIANCE = %f\n", mKalmanPosProcessNoise_var); }
	if(config.getValue_double("KALMAN_POS_MEASUREMENT_NOISE_VARIANCE", mKalmanPosMeasurementNoise_var)) { paramReadCnt++;	printf("KALMAN_POS_MEASUREMENT_NOISE_VARIANCE = %f\n", mKalmanPosMeasurementNoise_var); }
	if(config.getValue_double("KALMAN_POS_MIN_COV_COEFFICENT_POS", mKalmanPosCovCoeff_pos_min)) { paramReadCnt++;	printf("KALMAN_POS_MIN_COV_COEFFICENT_POS = %f\n", mKalmanPosCovCoeff_pos_min); }
	if(config.getValue_double("KALMAN_POS_MIN_COV_COEFFICENT_ACCEL", mKalmanPosCovCoeff_accel_min)) { paramReadCnt++;	printf("KALMAN_POS_MIN_COV_COEFFICENT_ACCEL = %f\n", mKalmanPosCovCoeff_accel_min); }
	if(config.getValue_double("KALMAN_POS_TIME_SCALE", mKalmanTimeScale_pos)) { paramReadCnt++;	printf("KALMAN_POS_TIME_SCALE = %f\n", mKalmanTimeScale_pos); }
	if(config.getValue_bool("KALMAN_POS_FEEDBACK_TO_RSS_ENABLE", mKalmanPosFeedbackToRssEnable)) { paramReadCnt++;	printf("KALMAN_POS_FEEDBACK_TO_RSS_ENABLE = %d\n", (int)mKalmanPosFeedbackToRssEnable); }
	if(config.getValue_double("KALMAN_POS_FEEDBACK_TO_RSS_NOSIE_SCALE", mKalmanPosFeedbackToRssNoiseScale)) { paramReadCnt++;	printf("KALMAN_POS_FEEDBACK_TO_RSS_NOSIE_SCALE = %f\n", mKalmanPosFeedbackToRssNoiseScale); }

	// -- Kalman filter general parameters
	if(config.getValue_int("KALMAN_ESTIMATE_VISUALIZATION_TIME_THRESHOLD", mKalmanEstimateVisualizationTimeout)) { paramReadCnt++;	printf("KALMAN_ESTIMATE_VISUALIZATION_TIME_THRESHOLD = %d\n", mKalmanEstimateVisualizationTimeout); }
//	if(config.getValue_double("KALMAN_MEASUREMENT_LIFETIME_FOR_TRACKING", mKalmanMeasurementLifetimeForTracking)) { paramReadCnt++;	printf("KALMAN_MEASUREMENT_LIFETIME_FOR_TRACKING = %f\n", mKalmanMeasurementLifetimeForTracking); }
	if(config.getValue_double("KALMAN_INITIALIZATION_TIME_THRESHOLD", mKalmanInitThreshold_time)) { paramReadCnt++;	printf("KALMAN_INITIALIZATION_TIME_THRESHOLD = %f\n", mKalmanInitThreshold_time); }
	if(config.getValue_double("KALMAN_INITIALIZATION_DIST_THRESHOLD", mKalmanInitThreshold_dist)) { paramReadCnt++;	printf("KALMAN_INITIALIZATION_DIST_THRESHOLD = %f\n", mKalmanInitThreshold_dist); }

	// -- Store-specific parameters
	string monitoringAP_IPsFileName;
	if(config.getValue_string("MONITORING_AP_IP_ADDR_LIST_FILENAME", monitoringAP_IPsFileName)) {
		paramReadCnt++;
        mMonitoringAP_IPsFilePath << APP_CONFIG_ROOT_PATH << "/" << WiFiTracker_APP_NAME << "/" << APP_STORE_ROOT_PATH << "/" << mTargetStoreName << "/" << monitoringAP_IPsFileName;
		printf("MONITORING_AP_IP_ADDR_LIST_FILENAME = %s\n", mMonitoringAP_IPsFilePath.str().c_str());
	}

	string knownAPsFilename;
	if(config.getValue_string("KNOWN_AP_LIST_FILENAME", knownAPsFilename)) {
		paramReadCnt++;
        mKnownApListFilePath << APP_CONFIG_ROOT_PATH << "/" << WiFiTracker_APP_NAME << "/" << APP_STORE_ROOT_PATH << "/" << mTargetStoreName << "/" << knownAPsFilename;
		printf("KNOWN_AP_LIST_FILENAME = %s\n", mKnownApListFilePath.str().c_str());
	}

	string knownServiceAgentsFilename;
	if(config.getValue_string("KNOWN_SERVICE_AGENT_LIST_FILENAME", knownServiceAgentsFilename)) {
		paramReadCnt++;
        mKnownServiceAgentFilePath << APP_CONFIG_ROOT_PATH << "/" << WiFiTracker_APP_NAME << "/" << APP_STORE_ROOT_PATH << "/" << mTargetStoreName << "/" << knownServiceAgentsFilename;
		printf("KNOWN_SERVICE_AGENT_LIST_FILENAME = %s\n", mKnownServiceAgentFilePath.str().c_str());
	}

		// -- GUI parameters
		if(config.getValue_string("IMG_MAP_NAME", mImgMapName)) { paramReadCnt++;	printf("IMG_MAP_NAME = %s\n", mImgMapName.c_str()); }
		if(config.getValue_string("STORE_LAYOUT_FILENAME", mStoreLayoutFilename)) { paramReadCnt++;	printf("STORE_LAYOUT_FILENAME = %s\n", mStoreLayoutFilename.c_str()); }

		if(config.getValue_int("STORE_LAYOUT_ORIGINAL_IMAGE_X", mStoreLayoutOriginalImgSize_x)) { paramReadCnt++;	printf("STORE_LAYOUT_ORIGINAL_IMAGE_X = %d\n", mStoreLayoutOriginalImgSize_x); }
		if(config.getValue_int("STORE_LAYOUT_ORIGINAL_IMAGE_Y", mStoreLayoutOriginalImgSize_y)) { paramReadCnt++;	printf("STORE_LAYOUT_ORIGINAL_IMAGE_Y = %d\n", mStoreLayoutOriginalImgSize_y); }
		if(config.getValue_double("STORE_LAYOUT_PIXEL_PER_FT", mStoreLayoutPixelPerFt)) { paramReadCnt++;	printf("STORE_LAYOUT_PIXEL_PER_FT = %.2f\n", mStoreLayoutPixelPerFt); }

		if(config.getValue_int("STORE_WIDTH_IMG_x_MAX", mStoreWidth_img_x_max) && config.getValue_int("STORE_WIDTH_IMG_y_MAX", mStoreWidth_img_y_max)) {
			paramReadCnt += 2;

			printf("(STORE_WIDTH_IMG_x_MAX, STORE_WIDTH_IMG_y_MAX) = (%d, %d)\n", mStoreWidth_img_x_max, mStoreWidth_img_y_max);

			mStoreWidth_PHY_x = (mStoreLayoutOriginalImgSize_x / mStoreLayoutPixelPerFt)*30.48;		// -- Pixel to ft to cm
			mStoreWidth_PHY_y = (mStoreLayoutOriginalImgSize_y / mStoreLayoutPixelPerFt)*30.48;		// -- Pixel to ft to cm
		}

		if(config.getValue_int("IN_STORE_MAP_BOUNDINGBOX_MARGIN", mInStoreMapBBMargin)) { paramReadCnt++;	printf("IN_STORE_MAP_BOUNDINGBOX_MARGIN = %d\n", mInStoreMapBBMargin); }

		// -- Periodic processing intervals
		if(config.getValue_float("PERIODIC_MAP_REFRESH_INTERVAL", mPeriodicInterval_GuiMapRefresh)) { paramReadCnt++;	printf("PERIODIC_MAP_REFRESH_INTERVAL = %.2f\n", mPeriodicInterval_GuiMapRefresh); }
		if(config.getValue_float("PERIODIC_REPORT_TRACKING_INTERVAL", mPeriodicInterval_TrackingReport)) { paramReadCnt++;	printf("PERIODIC_REPORT_TRACKING_INTERVAL = %.2f\n", mPeriodicInterval_TrackingReport); }
		if(config.getValue_int("PERIODIC_CLEAR_UP_DEVICE_INTERVAL", mPeriodicInterval_DevClearUp)) { paramReadCnt++;	printf("PERIODIC_CLEAR_UP_DEVICE_INTERVAL = %d\n", mPeriodicInterval_DevClearUp); }

		// -- Calibration parameters        
        if(config.getValue_bool("WIFI_CALIB_AUTOMATIC_BEST_RSS_OFFSET_DISCOVERY", mFindBestRssOffset)) { paramReadCnt++;	printf("WIFI_CALIB_AUTOMATIC_BEST_RSS_OFFSET_DISCOVERY = %d\n", mFindBestRssOffset); }
        if(config.getValue_int("WIFI_CALIB_AUTOMATIC_BEST_RSS_OFFSET_DISCOVERY_MIN_RSS_RANGE", mFindBestRssOffsetMinRss)) { paramReadCnt++;	printf("WIFI_CALIB_AUTOMATIC_BEST_RSS_OFFSET_DISCOVERY_MIN_RSS_RANGE = %d\n", mFindBestRssOffsetMinRss); }
        if(config.getValue_int("WIFI_CALIB_AUTOMATIC_BEST_RSS_OFFSET_DISCOVERY_MAX_RSS_RANGE", mFindBestRssOffsetMaxRss)) { paramReadCnt++;	printf("WIFI_CALIB_AUTOMATIC_BEST_RSS_OFFSET_DISCOVERY_MAX_RSS_RANGE = %d\n", mFindBestRssOffsetMaxRss); }
		if(config.getValue_bool("WIFI_CALIB_AUTOMATIC_PER_DEV_RSS_OFFSET_ADJUSTMENT", mAutomaticPerDevRssOffsetAdjustment)) { paramReadCnt++;	printf("WIFI_CALIB_AUTOMATIC_PER_DEV_RSS_OFFSET_ADJUSTMENT = %d\n", mAutomaticPerDevRssOffsetAdjustment); }

		ostringstream calibfilename;
		string calibfolder;
		if(config.getValue_string("CALIB_PARAM_FILE_PATH", calibfolder)) {
			printf("CALIB_PARAM_FILE_PATH = %s\n", calibfolder.c_str());

            mCalibFilePath << APP_CONFIG_ROOT_PATH << "/" << WiFiVisualizer_APP_NAME << "/" << APP_STORE_ROOT_PATH << "/" << mTargetStoreName << "/" << calibfolder;
			ostringstream cmd; cmd << "mkdir " << mCalibFilePath.str(); system(cmd.str().c_str());	// -- create calib folder
			paramReadCnt++;
		}

		string calibfilenameprefix;
		if(config.getValue_string("CALIB_PARAM_FILE_NAME_PREFIX", calibfilenameprefix)) {
			printf("CALIB_PARAM_FILE_NAME_PREFIX = %s\n", calibfilenameprefix.c_str());

			mCalibFilePathPrefixFull << mCalibFilePath.str() << "/" << calibfilenameprefix;
			paramReadCnt++;
		}
        if(config.getValue_int("CALIB_MAX_RANSAC_TRY", mCalibRansacTry_max)) { paramReadCnt++; printf("CALIB_MAX_RANSAC_TRY = %d\n", mCalibRansacTry_max); }
        if(config.getValue_int("CALIB_MAX_NUM_SAMPLE_DISTANCE_PER_BIN", mCalibSampleDistPerBin_max)) { paramReadCnt++; printf("CALIB_MAX_NUM_SAMPLE_DISTANCE_PER_BIN = %d\n", mCalibSampleDistPerBin_max); }
        if(config.getValue_int("CALIB_MIN_NUM_MEASUREMENT_PER_BIN", mCalibMeasPerBin_min)) { paramReadCnt++; printf("CALIB_MIN_NUM_MEASUREMENT_PER_BIN = %d\n", mCalibMeasPerBin_min); }
        if(config.getValue_int("CALIB_MAX_NUM_MEASUREMENT_PER_BIN", mCalibMeasPerBin_max)) { paramReadCnt++; printf("CALIB_MAX_NUM_MEASUREMENT_PER_BIN = %d\n", mCalibMeasPerBin_max); }
        if(config.getValue_int("CALIB_MIN_DATA_SAMPLES_FOR_LEAST_SQUARE", mCalibDataSamplesForLeastSquare_min)) { paramReadCnt++; printf("CALIB_MIN_DATA_SAMPLES_FOR_LEAST_SQUARE = %d\n", mCalibDataSamplesForLeastSquare_min); }
        if(config.getValue_int("CALIB_FAIL_CNT_MAX_ALLOWED", mCalibFailCntAllowed_max)) { paramReadCnt++; printf("CALIB_FAIL_CNT_MAX_ALLOWED = %d\n", mCalibFailCntAllowed_max); }

		// -- Kalman filter parameters for RSS estimation
		if(config.getValue_double("KALMAN_CALIB_RSS_PROCESS_NOISE_VARIANCE", mKalmanCalibRssProcessNoise_var)) { paramReadCnt++;	printf("KALMAN_CALIB_RSS_PROCESS_NOISE_VARIANCE = %f\n", mKalmanCalibRssProcessNoise_var); }
		if(config.getValue_double("KALMAN_CALIB_RSS_MEASUREMENT_NOISE_VARIANCE", mKalmanCalibRssMeasurementNoise_var)) { paramReadCnt++;	printf("KALMAN_CALIB_RSS_MEASUREMENT_NOISE_VARIANCE = %f\n", mKalmanCalibRssMeasurementNoise_var); }
		if(config.getValue_double("KALMAN_CALIB_RSS_MIN_COV_COEFFICENT_POS", mKalmanCalibRssCovCoeff_pos_min)) { paramReadCnt++;	printf("KALMAN_CALIB_RSS_MIN_COV_COEFFICENT_POS = %f\n", mKalmanCalibRssCovCoeff_pos_min); }
		if(config.getValue_double("KALMAN_CALIB_RSS_MIN_COV_COEFFICENT_ACCEL", mKalmanCalibRssCovCoeff_accel_min)) { paramReadCnt++;	printf("KALMAN_CALIB_RSS_MIN_COV_COEFFICENT_ACCEL = %f\n", mKalmanCalibRssCovCoeff_accel_min); }

		// -- IoT parameters
		if(config.getValue_int("IOT_PERIODIC_STATE_REPORTING_INTERVAL", mPeriodicStateReportingInterval)) { paramReadCnt++; printf("PERIODIC_STATE_REPORTING_INTERVAL = %d\n", mPeriodicStateReportingInterval); }


	bool ret = false;
	if(paramReadCnt != (int)config.paramSize()) {
		fprintf(stderr, "!! paramReadCnt[%d] != config.paramSize() [%d] !!\n", paramReadCnt, config.paramSize());
		ret = false;
	} else
		ret = true;

	mCalibEnoughMeasBinsToKickOffCalib_min = (int)(CALIB_NUM_DISTANCE_BINS*0.8 + 0.5);

	return ret;

}


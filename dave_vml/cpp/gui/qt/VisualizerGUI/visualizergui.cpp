#include "../VisualizerGUI/visualizergui.h"

#include "ui_visualizergui.h"

#include "modules/utility/TimeKeeper.hpp"

// -- Refer to https://github.com/nlohmann/json
#include "json.hpp"

using json = nlohmann::json;

extern pthread_mutex_t gMutex;

// -- Reference: https://john.nachtimwald.com/2015/05/02/effective-threading-using-qt/

VisualizerGUI::VisualizerGUI(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::VisualizerGUI),
    mMapSize(-1,-1)
{
    ui->setupUi(this);
    ui->StartButton->setCheckable(true);
    ui->ThingShadowDesiredSend->setCheckable(true);

    mpWiFi_Visualizer = NULL;
    mCurrentTrackerIndex = -1;

    initializeGUI();

    // -- Display three buttons on the widget
    setWindowFlags(windowFlags() | Qt::Window
    | Qt::WindowMinimizeButtonHint
    | Qt::WindowMaximizeButtonHint
    | Qt::WindowCloseButtonHint);

    // -- Add tracker list, TODO: Load from a file
    {
        mTrackerList.clear();

        mTrackerList.push_back("vmhq-16801-trac001");
        mTrackerList.push_back("giant-6072-trac001");
        mTrackerList.push_back("petsns-231-trac001");
        mTrackerList.push_back("petsns-342-trac001");
        mTrackerList.push_back("carealot-001-trac001");

        mTrackerList.push_back("carealot-001-omni001");
        mTrackerList.push_back("carealot-001-omni004");
        mTrackerList.push_back("carealot-001-omni005");

        mTrackerList.push_back("petsns-231-fac001");
        mTrackerList.push_back("petsns-231-fac003");
        mTrackerList.push_back("petsns-231-fac004");

        mTrackerList.push_back("petsns-342-fac001");
        mTrackerList.push_back("petsns-342-fac002");
        mTrackerList.push_back("petsns-342-fac003");

        for(vector<string>::iterator it = mTrackerList.begin(); it != mTrackerList.end(); ++it) {
            string trackerName = *it;
            ui->TrackerThingName->addItem(tr(trackerName.c_str()));
        }
    }

    // -- Add tracker list, TODO: Load from a file
    {
        vector<string> appList;

        appList.push_back("wifiTrack");
        appList.push_back("connectorApp");

        //appList.push_back("testApp");
        //appList.push_back("sampleApp");
        appList.push_back("wifiRSS");
        appList.push_back("demoApp");
        appList.push_back("visionApp");
        appList.push_back("doorApp");
        appList.push_back("healthApp");
        appList.push_back("wifiViz");


        for(vector<string>::iterator it = appList.begin(); it != appList.end(); ++it) {
            string trackerName = *it;
            ui->AppType->addItem(tr(trackerName.c_str()));
        }
    }

}

VisualizerGUI::~VisualizerGUI()
{
    quitTracker(true);

    delete ui;

    //delete mpTimerMethod1;
}

void VisualizerGUI::initializeGUI() {
    mThingShadowMsg_wifiTrack_cnt = 0;
    mThingShadowMsg_connectorApp_cnt = 0;
    mIsRunning = false;

    // clean viewer
    ui->openCVViewer->clearImage();
    ui->ThingShadowDesired->clear();
    ui->ThingShadowReported->clear();
    ui->DebugOutput->clear();
    ui->FontScale->setValue(1.0);

    mMapSize.width = -1;
    mMapSize.height = -1;
}

void VisualizerGUI::updateThingShadowMsg() {
    ostringstream thingShadowMsgStr;
    string thingShadowTopicStr;

    pthread_mutex_lock(&gMutex);
    thingShadowMsgStr << mpWiFi_Visualizer->GetThingShadowMsg();
    thingShadowTopicStr = mpWiFi_Visualizer->GetThingShadowTopic();
    pthread_mutex_unlock(&gMutex);

    try {

        // -- Read the JSON message and parse
        json shadowJson = json::parse(thingShadowMsgStr.str());

        //std::cout << std::setw(2) << shadowJson << endl;

        json shadowJson_reported = json::parse(shadowJson["state"]["reported"].dump());
        json shadowJson_clientToken = json::parse(shadowJson["clientToken"].dump());

        json shadowJson_reported_wifiTrack;
        json shadowJson_reported_connectorApp;

        if(shadowJson_reported.find(mpWiFi_Visualizer->mAppType_Tracker) != shadowJson_reported.end()) {
            shadowJson_reported_wifiTrack = shadowJson_reported[mpWiFi_Visualizer->mAppType_Tracker];
        }

        if(shadowJson_reported.find(mpWiFi_Visualizer->mAppType_Connector) != shadowJson_reported.end()) {
            shadowJson_reported_connectorApp = shadowJson_reported[mpWiFi_Visualizer->mAppType_Connector];
        }

        mThingShadowMsgStr_extracted.str("");
        mThingShadowMsgStr_extracted.clear();

        mThingShadowMsgStr_extracted    //<< "Topic: " << thingShadowTopicStr << endl
                                        //<< "Token: " << shadowJson_clientToken << endl
                                        << "---------------------------- Extracted Msg ----------------------------" << endl;
        if(shadowJson_reported_wifiTrack.size() > 0) {
            mPrevTime_ThingShadowReported_wifiTrack = gTimeSinceStart();
            mThingShadowMsg_wifiTrack_cnt++;
            mThingShadowMsgStr_extracted    << "\"" << mpWiFi_Visualizer->mAppType_Tracker << "\":" << std::setw(2) << shadowJson_reported_wifiTrack << endl;
        }

        if(shadowJson_reported_connectorApp.size() > 0) {
            mPrevTime_ThingShadowReported_connectorApp = gTimeSinceStart();
            mThingShadowMsg_connectorApp_cnt++;
            mThingShadowMsgStr_extracted    << "\"" << mpWiFi_Visualizer->mAppType_Connector << "\":" << std::setw(2) << shadowJson_reported_connectorApp << endl;
        }



    } catch (std::invalid_argument& e) {		// -- Handle exception when the received msg does not comply with JSON standard
        // -- TODO: Implement how to handle this exception. Should we let the sender know about this?
        std::cout << "updateThingShadowMsg() invalid_argument: " << e.what() << '\n';
    } catch (std::out_of_range& e) {	// -- Handle exception when a non-existing filed is accessed.
        // -- TODO: Implement how to handle this exception. Should we let the sender know about this?
        std::cout << "updateThingShadowMsg() out of range: " << e.what() << '\n';
    } catch (std::domain_error& e) {	// -- Handle exception when an incompatible conversion is attempted (e.g., float --> string).
        // -- TODO: Implement how to handle this exception. Should we let the sender know about this?
        std::cout << "updateThingShadowMsg() domain error: " << e.what() << '\n';
    }

}

void VisualizerGUI::updateImage()
{
    if(mIsRunning == false)
        return;

    double currTime = gTimeSinceStart();        // -- Unit: [sec]
    int timeElaped_wifiTrack = (int)(currTime - mPrevTime_ThingShadowReported_wifiTrack);
    int timeElaped_connectorApp = (int)(currTime - mPrevTime_ThingShadowReported_connectorApp);
    static int timeElaped_prev_wifiTrack = 0;

    // -- Update the ThingShadow-Reported status
    if(mThingShadowMsg_wifiTrack_cnt > 0 && fabs(timeElaped_wifiTrack - timeElaped_prev_wifiTrack) >= 2) {
        timeElaped_prev_wifiTrack = timeElaped_wifiTrack;

        ostringstream thingShadowReportedDisplay;

        if(mThingShadowMsg_wifiTrack_cnt > 0) {
            mThingShadowMsgStr_time_wifiTrack.str(""); mThingShadowMsgStr_time_wifiTrack.clear();
            mThingShadowMsgStr_time_wifiTrack << "[wifiTrack] Rcved " << mThingShadowMsg_wifiTrack_cnt << " th msg @ " << timeElaped_wifiTrack << " sec ago" << endl;

            thingShadowReportedDisplay << mThingShadowMsgStr_time_wifiTrack.str();
        }

        if(mThingShadowMsg_connectorApp_cnt > 0) {
            mThingShadowMsgStr_time_connectorApp.str(""); mThingShadowMsgStr_time_connectorApp.clear();
            mThingShadowMsgStr_time_connectorApp << "[connectorApp] Rcved " << mThingShadowMsg_connectorApp_cnt << " th msg @ " << timeElaped_connectorApp << " sec ago" << endl;

            thingShadowReportedDisplay << mThingShadowMsgStr_time_connectorApp.str();
        }

        if(mThingShadowMsg_connectorApp_cnt > 0 || mThingShadowMsg_wifiTrack_cnt > 0)
            thingShadowReportedDisplay << mThingShadowMsgStr_extracted.str();

        QString thingShadowMsg = QString::fromStdString(thingShadowReportedDisplay.str());
        ui->ThingShadowReported->setPlainText(thingShadowMsg);
    }

    // show the image
    cv::Mat image, image_resized, image_resized_colorCorrected;

//    mpTracker->getGrabbedImage().copyTo(image);
//    mpTracker->drawTrackingRegions(image);

    //pthread_mutex_lock(&gMutex);
    mpWiFi_Visualizer->GetMapImage().copyTo(image);
    //pthread_mutex_unlock(&gMutex);

    // -- Resize the image while maintaining the aspect ratio
    if(mMapSize.height < 0 || mMapSize.width < 0) {
        // -- Calculate the aspect ratio of the original image
        float aspectRatio = image.size().width / (float)image.size().height;
        CvSize qtFrameSize((int)ui->openCVViewer->baseSize().width(), (int)ui->openCVViewer->baseSize().height());
        //CvSize qtFrameSize((int)ui->openCVViewer->geometry().width(), (int)ui->openCVViewer->geometry().height());

        if(qtFrameSize.height*aspectRatio > qtFrameSize.width) {
            // -- The source image should be resized to the frame width
            mMapSize.width = qtFrameSize.width;
            mMapSize.height = qtFrameSize.width / aspectRatio;
        } else {
            // -- The source image should be resized to the frame height
            mMapSize.height = qtFrameSize.height;
            mMapSize.width = qtFrameSize.height * aspectRatio;
        }

        if(mMapSize.height < 0 || mMapSize.width < 0)
            return;

        // -- Reset the OpenCvViewer's size
        ui->openCVViewer->setFixedSize(QSize(mMapSize.width, mMapSize.height));
    }

    // -- Resize image to fit the OpenCV viewer
    cv::resize(image, image_resized, mMapSize);

    // -- Convert OpenCV-default BGR to QT-default RGB
    cvtColor(image_resized, image_resized_colorCorrected, CV_BGR2RGB);

    ui->openCVViewer->showImage(image_resized_colorCorrected);


    //image.release();
}

void VisualizerGUI::on_actionSave_triggered()
{
//    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"), QString(),
//            tr("Text Files (*.txt);;C++ Files (*.cpp *.h)"));

//    if (!fileName.isEmpty()) {
//        QFile file(fileName);
//        if (!file.open(QIODevice::WriteOnly)) {
//            // error message
//        } else {
//            QTextStream stream(&file);
//            stream << ui->textEdit->toPlainText();
//            stream.flush();
//            file.close();
//        }
//    }
}

void VisualizerGUI::quitTracker(bool sendRealtimeMsg) {

    mIsRunning = false;

    if(mpWiFi_Visualizer == NULL || mMapSize.width < 0 || mMapSize.height < 0)
        return;

    initializeGUI();

       // -- Change and send the desired state to the remote tracker so that it start sending real-time locs data
    if(sendRealtimeMsg == true) {

        // -- Change the previous messages fonts to gray-color
        DebugOutputChangeFontSetting();

        // -- Font setting for this round of messages
        ui->DebugOutput->setTextColor(Qt::black);
        ui->DebugOutput->setFont(QFont("Courier", 8, QFont::Bold));

        QString thingShadowMsg;
        string stdOutMsg;
        string realTimeSendMsg = "{\"RealTimeReport\":false}";

        stdOutMsg = mpWiFi_Visualizer->ControlMsg_Send_toDev(realTimeSendMsg, wifiTrack);
        thingShadowMsg = "-- [ControlMsg] Sent successfully!";
        ui->DebugOutput->append(QString::fromStdString(stdOutMsg));
        ui->DebugOutput->append(thingShadowMsg);
        sleep(1);

        stdOutMsg =mpWiFi_Visualizer->ControlMsg_Send_toDev(realTimeSendMsg, connectorApp);
        thingShadowMsg = "-- [ControlMsg] Sent successfully!";
        ui->DebugOutput->append(QString::fromStdString(stdOutMsg));
        ui->DebugOutput->append(thingShadowMsg);
        sleep(1);

        stdOutMsg = mpWiFi_Visualizer->ThingShadow_Desired_Send(realTimeSendMsg, wifiTrack);
        thingShadowMsg = "-- [Desired State] Sent successfully!";
        ui->DebugOutput->append(QString::fromStdString(stdOutMsg));
        sleep(1);

        stdOutMsg = mpWiFi_Visualizer->ThingShadow_Desired_Send(realTimeSendMsg, connectorApp);
        thingShadowMsg = "-- [Desired State] Sent successfully!";
        ui->DebugOutput->append(QString::fromStdString(stdOutMsg));
    }

    //mWorkerThread->terminate();

    mpWiFi_Visualizer->InitiateKillProcess();
    //delete mpWiFi_Visualizer;

    //delete mWorkerThread;

    // -- Need to have some sleep. Otherwise, the QT app would crash.
    sleep(3);

    mWorkerThread->quit();
}

void VisualizerGUI::on_StartButton_clicked()
{

    //uint trackerIndex = ui->TrackerThingName->currentIndex();

    ui->StartButton->setChecked(true);

//        if(mpWiFi_Visualizer != NULL) {
//            // -- Quit the existing tracker before initiating the new tracker
//            //quitTracker((bool)(mCurrentTrackerIndex != trackerIndex));
//            quitTracker(true);

//        } else {

            initializeGUI();
//        }

        QThread *newThread = new QThread;
        mWorkerThread = newThread;

        //mpWiFi_Visualizer = std::make_shared<WiFi_Shopper_Visualizer>("");
        WiFi_Shopper_Visualizer *newVisualizer = new WiFi_Shopper_Visualizer("");
        mpWiFi_Visualizer = newVisualizer;

        // -- Initialize with the input from QT
        mpWiFi_Visualizer->InitTargetNames(ui->TrackerThingName->currentText().toStdString());

        mpWiFi_Visualizer->moveToThread(mWorkerThread);

        connect(mWorkerThread, SIGNAL(started()), mpWiFi_Visualizer, SLOT(run()));
        connect(mpWiFi_Visualizer, SIGNAL(finished()), mWorkerThread, SLOT(quit()));
        connect(mpWiFi_Visualizer, SIGNAL(finished()), mpWiFi_Visualizer, SLOT(deleteLater()));
        connect(mWorkerThread, SIGNAL(finished()), mWorkerThread, SLOT(deleteLater()));

        connect(mpWiFi_Visualizer, SIGNAL(updateImage()), this, SLOT(updateImage()));
        connect(mpWiFi_Visualizer, SIGNAL(updateThingShadowMsg()), this, SLOT(updateThingShadowMsg()));

        mWorkerThread->start();

        gStartTime();		// -- Mark the initial time

        mPrevTime_ThingShadowReported_wifiTrack = gTimeSinceStart();

        mpWiFi_Visualizer->mKalmanEstimateVisualizationTimeout = ui->TrajVizTime->value();

        mIsRunning = true;

        // -- TODO: Get the current desired state from AWS IoT and show
        {

        }

    ui->StartButton->setChecked(false);

}

void VisualizerGUI::on_ShowSensors_toggled(bool checked)
{
    if(mIsRunning == false)
        return;

    pthread_mutex_lock(&gMutex);
    mpWiFi_Visualizer->mShowSensors = checked;
    pthread_mutex_unlock(&gMutex);
}

void VisualizerGUI::on_FontScale_valueChanged(double arg1)
{
    if(mIsRunning == false)
        return;

    pthread_mutex_lock(&gMutex);
    mpWiFi_Visualizer->mFontSizeScale = arg1;
    pthread_mutex_unlock(&gMutex);
}

// -- Change the font of the existing texts in the DebugOutput browser
// -- Refer to http://stackoverflow.com/questions/27716625/qtextedit-change-font-of-individual-paragraph-block
void VisualizerGUI::DebugOutputChangeFontSetting() {

    // For block management
    QTextDocument *doc = ui->DebugOutput->document();

    for(int i=0; i<doc->blockCount(); i++) {
        // Locate the 1st block
        QTextBlock block = doc->findBlockByNumber(i);

        // Initiate a copy of cursor on the block
        // Notice: it won't change any cursor behavior of the text editor, since it
        //         just another copy of cursor, and it's "invisible" from the editor.
        QTextCursor cursor(block);

        // Set background color
        QTextBlockFormat blockFormat = cursor.blockFormat();
        //blockFormat.setBackground(QColor(Qt::white));
        cursor.setBlockFormat(blockFormat);

        // Set font
        //for (QTextBlock::iterator it = cursor.block().begin(); !(it.atEnd()); ++it)
        for (QTextBlock::iterator it = cursor.block().begin(); it != cursor.block().end(); ++it)
        {
            QTextCharFormat charFormat = it.fragment().charFormat();
            charFormat.setFont(QFont("Courier", 8, QFont::Light));

            QTextCursor tempCursor = cursor;
            tempCursor.setPosition(it.fragment().position());
            tempCursor.setPosition(it.fragment().position() + it.fragment().length(), QTextCursor::KeepAnchor);
            tempCursor.setCharFormat(charFormat);
        }
    }

}

void VisualizerGUI::on_ThingShadowDesiredSend_clicked()
{

    string desiredStr = ui->ThingShadowDesired->toPlainText().toUtf8().constData();

    // -- Keep it pressed, and release it once the 'send' operation is done.
    ui->ThingShadowDesiredSend->setChecked(true);

    if(desiredStr.size() > 0)
        desiredStr.erase(std::remove(desiredStr.begin(), desiredStr.end(), '\n'), desiredStr.end());

    bool ret = false;
    string stdOutMsg;

    try {

        // -- Read the JSON message and parse
        json desiredJson = json::parse(desiredStr);

//        if(0) {
//            printf("+++++++ [VisualizerGUI] ThingShadow-Desired Message  ++++++++++++++++\n");
//            std::cout << std::setw(2) << desiredJson << '\n';
//            printf("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
//        }

        // -- Pass the argument only if it is a valid JSON and only if it is not empty.
        // -- If the string is not a valid JSON, then calling desiredJson.empty() would generate an exception.
        if(desiredJson.empty() != true)
            stdOutMsg = mpWiFi_Visualizer->ThingShadow_Desired_Send(desiredJson.dump(), wifiTrack);

        ret = true;

    } catch (std::invalid_argument& e) {		// -- Handle exception when the received msg does not comply with JSON standard
        // -- TODO: Implement how to handle this exception. Should we let the sender know about this?
        std::cout << "on_ThingShadowDesiredSend_clicked() invalid_argument: " << e.what() << '\n';
    } catch (std::out_of_range& e) {	// -- Handle exception when a non-existing filed is accessed.
        // -- TODO: Implement how to handle this exception. Should we let the sender know about this?
        std::cout << "on_ThingShadowDesiredSend_clicked() out of range: " << e.what() << '\n';
    } catch (std::domain_error& e) {	// -- Handle exception when an incompatible conversion is attempted (e.g., float --> string).
        // -- TODO: Implement how to handle this exception. Should we let the sender know about this?
        std::cout << "on_ThingShadowDesiredSend_clicked() domain error: " << e.what() << '\n';
    }

    DebugOutputChangeFontSetting();

    // -- Show the current result text
    if(ret == false) {
        QString thingShadowMsg = "-- ERROR: [Shadow: Desired] Not valid JSON msg";
        //ui->DebugOutput->setFontUnderline(true);
        ui->DebugOutput->setTextColor(Qt::red);
        ui->DebugOutput->setFont(QFont("Courier", 8, QFont::Bold));
        ui->DebugOutput->append(thingShadowMsg);
        //ui->DebugOutput->setPlainText(thingShadowMsg);
    } else {
        QString thingShadowMsg = "-- [Shadow: Desired] Sent successfully!";
        //ui->DebugOutput->setFontUnderline(false);
        ui->DebugOutput->setTextColor(Qt::black);
        ui->DebugOutput->setFont(QFont("Courier", 8, QFont::Bold));
        ui->DebugOutput->append(QString::fromStdString(stdOutMsg));
        ui->DebugOutput->append(thingShadowMsg);
        //ui->DebugOutput->setPlainText(thingShadowMsg);
    }


    // -- Release the button since the 'send' operation is done.
    ui->ThingShadowDesiredSend->setChecked(false);
}

void VisualizerGUI::on_ShadowDelete_clicked()
{
    // -- Keep it pressed, and release it once the 'send' operation is done.
    ui->ThingShadowDesiredSend->setChecked(true);

    DebugOutputChangeFontSetting();

    string stdOutMsg = mpWiFi_Visualizer->ThingShadow_Delete();

    {
        QString thingShadowMsg = "-- [Shadow: Delete] Sent successfully!";
        //ui->DebugOutput->setFontUnderline(false);
        ui->DebugOutput->setTextColor(Qt::black);
        ui->DebugOutput->setFont(QFont("Courier", 8, QFont::Bold));
        ui->DebugOutput->append(QString::fromStdString(stdOutMsg));
    }

    // -- Release the button since the 'send' operation is done.
    ui->ThingShadowDesiredSend->setChecked(false);

}

void VisualizerGUI::on_StartCalib_clicked()
{
    mpWiFi_Visualizer->SendWiFiCalibMsg(WIFI_CALIBRATION);
}

void VisualizerGUI::on_QuitCalib_clicked()
{
    mpWiFi_Visualizer->SendWiFiCalibMsg(WIFI_TRACKING);
}

void VisualizerGUI::on_VerifyCalib_clicked()
{
    mpWiFi_Visualizer->SendWiFiCalibMsg(WIFI_CALIB_VERIFICATION);
}

void VisualizerGUI::on_ShowCalib_clicked()
{

}

void VisualizerGUI::on_QuitButton_clicked()
{
    quitTracker(true);

    sleep (5);

    exit(0);
}

void VisualizerGUI::on_StopButton_clicked()
{
    quitTracker(true);
}

void VisualizerGUI::on_DebugIoT_clicked(bool checked)
{
    mpWiFi_Visualizer->mDebugIoT = checked;
}

void VisualizerGUI::on_ControlMsgSendButton_clicked()
{
    int appType = -1;
    string appTypeStr = ui->AppType->currentText().toStdString();

    if(appTypeStr.compare("testApp") == 0) {
        appType = testApp;
    } else if(appTypeStr.compare("sampleApp") == 0) {
        appType = sampleApp;
    } else if(appTypeStr.compare("wifiRSS") == 0) {
        appType = wifiRSS;
    } else if(appTypeStr.compare("demoApp") == 0) {
        appType = demoApp;
    } else if(appTypeStr.compare("visionApp") == 0) {
        appType = visionApp;
    } else if(appTypeStr.compare("doorApp") == 0) {
        appType = doorApp;
    } else if(appTypeStr.compare("healthApp") == 0) {
        appType = healthApp;
    } else if(appTypeStr.compare("wifiTrack") == 0) {
        appType = wifiTrack;
    } else if(appTypeStr.compare("wifiViz") == 0) {
        appType = wifiViz;
    } else if(appTypeStr.compare("connectorApp") == 0) {
        appType = connectorApp;
    }

    string controlStr = ui->ControlMsgTextBox->toPlainText().toUtf8().constData();

    if(controlStr.size() > 0)
        controlStr.erase(std::remove(controlStr.begin(), controlStr.end(), '\n'), controlStr.end());

    bool ret = false;
    string stdOutMsg;

    try {

        // -- Read the JSON message and parse
        json controlJson = json::parse(controlStr);

//        if(0) {
//            printf("+++++++ [VisualizerGUI] ControlMsg Message  ++++++++++++++++\n");
//            std::cout << std::setw(2) << controlJson << '\n';
//            printf("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
//        }

        // -- Pass the argument only if it is a valid JSON and only if it is not empty.
        // -- If the string is not a valid JSON, then calling controlJson.empty() would generate an exception.
        if(controlJson.empty() != true)
            stdOutMsg = mpWiFi_Visualizer->ControlMsg_Send_toDev(controlJson.dump(), appType);

        ret = true;

    } catch (std::invalid_argument& e) {		// -- Handle exception when the received msg does not comply with JSON standard
        // -- TODO: Implement how to handle this exception. Should we let the sender know about this?
        std::cout << "on_ControlMsgSendButton_clicked() invalid_argument: " << e.what() << '\n';
    } catch (std::out_of_range& e) {	// -- Handle exception when a non-existing filed is accessed.
        // -- TODO: Implement how to handle this exception. Should we let the sender know about this?
        std::cout << "on_ControlMsgSendButton_clicked() out of range: " << e.what() << '\n';
    } catch (std::domain_error& e) {	// -- Handle exception when an incompatible conversion is attempted (e.g., float --> string).
        // -- TODO: Implement how to handle this exception. Should we let the sender know about this?
        std::cout << "on_ControlMsgSendButton_clicked() domain error: " << e.what() << '\n';
    }

    DebugOutputChangeFontSetting();

    // -- Show the current result text
    if(ret == false) {
        QString controlMsgBack = "-- ERROR: [Control Msg] Not valid JSON msg";
        //ui->DebugOutput->setFontUnderline(true);
        ui->DebugOutput->setTextColor(Qt::red);
        ui->DebugOutput->setFont(QFont("Courier", 8, QFont::Bold));
        ui->DebugOutput->append(controlMsgBack);
    } else {
        QString controlMsgBack = "-- [Control Msg] Sent successfully!";
        //ui->DebugOutput->setFontUnderline(false);
        ui->DebugOutput->setTextColor(Qt::black);
        ui->DebugOutput->setFont(QFont("Courier", 8, QFont::Bold));
        ui->DebugOutput->append(QString::fromStdString(stdOutMsg));
        ui->DebugOutput->append(controlMsgBack);
    }

}
/**
 * @brief VisualizerGUI::on_ShadowGet_clicked
 * TODO: Implement subscribe to get/accepted topic to actually get the current thing shadow
 */
void VisualizerGUI::on_ShadowGet_clicked()
{
    mpWiFi_Visualizer->ThingShadow_Get();
}

void VisualizerGUI::on_TrajVizTime_valueChanged(double arg1)
{
    mpWiFi_Visualizer->mKalmanEstimateVisualizationTimeout = arg1;
}

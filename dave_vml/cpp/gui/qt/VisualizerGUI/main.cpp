#include <QApplication>
#include "../VisualizerGUI/visualizergui.h"

pthread_mutex_t gMutex;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    VisualizerGUI w;
    w.show();

    return a.exec();
}

/*
 * WiFi_Shopper_Visualizer.hpp
 *
 *  Created on: Oct 4, 2016
 *      Author: paulshin
 */

#ifndef SRC_PROJECTS_WIFI_SHOPPER_VISUALIZER_WIFI_SHOPPER_VISUALIZER_HPP_
#define SRC_PROJECTS_WIFI_SHOPPER_VISUALIZER_WIFI_SHOPPER_VISUALIZER_HPP_

#include "projects/WiFi_Shopper_Tracker/WiFi_Shopper_Tracker.hpp"

#include <QObject>

using namespace cv;
using namespace std;

// -- Note: "Multiple Inheritance Requires QObject to Be First",
class WiFi_Shopper_Visualizer : public QObject, public WiFi_Shopper_Tracker {

     Q_OBJECT

public:
	WiFi_Shopper_Visualizer(string trackerId_suffix);
	virtual ~WiFi_Shopper_Visualizer();

    void InitTargetNames(string targetName);

    void PacketStreamParsing();
        Mat&  GetMapImage(void) {
            return mImg_map_final;
        }

    string GetThingShadowMsg() {
        return mCurrentThingShadowMsg.str();
    }

    string GetThingShadowTopic() {
        return mCurrentThingShadowTopic;
    }

    void loadTestbedList(string testbedList);
    bool IsThisTestbed(string storeAddr);

    void InitiateKillProcess();
    void SendWiFiCalibMsg(unsigned int mode);

    string ControlMsg_Send_toDev(string controlStr, unsigned int appType);
    string ThingShadow_Desired_Send(string desiredStr, unsigned int appType);
    string ThingShadow_Delete();
    string ThingShadow_Get();

    string mTrackerThingName;
    string mTargetStoreName, mTargetStoreName_slash, mTargetDevName;

    string mAppType_Viz, mAppType_Connector;
    string mAwsIotCertDirectory, mAwsIotMqttHost;

public slots:
    void run(bool showGUI = false);

signals:
    void updateImage();
    void updateThingShadowMsg();
    void finished();

protected:

    ostringstream mCurrentThingShadowMsg;
    string mCurrentThingShadowTopic;

	pthread_t mDataPacketStreamParsing_thread;
	pthread_t mStatePacketStreamParsing_thread;

    unsigned int mMqttPubMsgCnt;

    deque<string> mTestbedList;

	static void *ThingShadow_PacketStreamParsingThread(void *apdata);
	static void *Data_PacketStreamParsingThread(void *apdata);

	void ThingShadow_PacketStreamParsing();
	void Data_PacketStreamParsing();

	bool ParseSetting(string configFile);

};


#endif /* SRC_PROJECTS_WIFI_SHOPPER_VISUALIZER_WIFI_SHOPPER_VISUALIZER_HPP_ */

#-------------------------------------------------
#
# Project created by QtCreator 2016-10-05T15:44:43
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = WifiPostProcessingGUI
TEMPLATE = app

CONFIG += c++11     # ISO C++ 2011
CONFIG += qt

SOURCES += main.cpp\
    WiFiPostProcessor.cpp \
    WiFiPostProcessorGUI.cpp

HEADERS  += \
    WiFiPostProcessor.h \
    Astar.hpp \
    WiFiPostProcessorGUI.h

FORMS    += WifiPostProcessingGUI.ui

# vml libs
INCLUDEPATH += ../../../include
LIBS    += -L../../../../../build/lib/Debug -lvmlutility -lvmliot

# MySQL
SOURCES += ../../../src/modules/db/mysql_connector.cpp
HEADERS += ../../../include/modules/db/mysql_connector.hpp

# OpenCV GL viewer
INCLUDEPATH += ../opencv
SOURCES += ../opencv/cqtopencvviewergl.cpp
HEADERS += ../opencv/cqtopencvviewergl.h

# OpenCV and boost
INCLUDEPATH += /usr/local/include
LIBS += -L/usr/local/lib

# boost
LIBS += -lboost_system

# misc
QMAKE_CXXFLAGS += -std=c++11 -pthread
LIBS += -lpthread -lmosquitto -lmosquittopp

# dlib
INCLUDEPATH += ../../../external/dlib-19.1
LIBS += -L../../../../external/dlib-19.1/dlib/build
LIBS += -ldlib

# JSON for modern C++
INCLUDEPATH += ../../../external/jsonByNlohmann

# OpenCV
LIBS += -lopencv_video -lopencv_videoio -lopencv_calib3d -lopencv_imgcodecs -lopencv_highgui -lopencv_imgproc -lopencv_core -lopencv_features2d

# MySQL Client Lib
LIBS += -lmysqlcppconn

## IoT
INCLUDEPATH += ../../../external/aws-iot-sdk/aws_iot_src/shadow
INCLUDEPATH += ../../../external/aws-iot-sdk/aws_iot_src/utils

# WiFi Tracker source codes
SOURCES += ../../../src/projects/WiFi_Shopper_Tracker/GUI.cpp
SOURCES += ../../../src/projects/WiFi_Shopper_Tracker/MonitoringAP.cpp
SOURCES += ../../../src/projects/WiFi_Shopper_Tracker/RemoteDevice.cpp
SOURCES += ../../../src/projects/WiFi_Shopper_Tracker/WiFi_Shopper_Tracker_main.cpp
SOURCES += ../../../src/projects/WiFi_Shopper_Tracker/WiFi_Shopper_Tracker_iot.cpp
SOURCES += ../../../src/projects/WiFi_Shopper_Tracker/WiFi_Shopper_Tracker_calib.cpp
SOURCES += ../../../src/projects/WiFi_Shopper_Tracker/WiFi_Shopper_Tracker_localization.cpp


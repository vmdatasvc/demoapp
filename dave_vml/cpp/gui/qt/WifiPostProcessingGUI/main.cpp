#include <QApplication>
#include "../WifiPostProcessingGUI/WiFiPostProcessorGUI.h"

pthread_mutex_t gMutex;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    WifiPostProcessingGUI w;
    w.show();

    return a.exec();
}

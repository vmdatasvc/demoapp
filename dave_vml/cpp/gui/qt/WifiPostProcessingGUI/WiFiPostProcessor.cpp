#include <opencv2/features2d.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "projects/WiFi_Shopper_Tracker/GUI.h"
#include "WiFiPostProcessor.h"

#define DEBUG_LEVEL     1
#define DEBUG_LEVEL_HMM         1
#define DEBUG_LEVEL_FILTERING   1
#define DEBUG_LEVEL_VITERBI     0
#define DEBUG_LEVEL_RECONSTRUCT 1


#define HMM_STORE_ZONES_FILENAME    "ZoneMapColorCoded.png"

// -- Probability Distribution parameters
#define MEAUREMENT_PROB_MEAN    0
#define MEAUREMENT_PROB_STD     600         // -- Unit: [cm]
#define TRANSITION_PROB_LAMBDA  3.5

#define VITERBI_MAX_STATE_INTERVAL      300         // -- Unit: [cm]
#define VITERBI_MIN_STATE_INTERVAL      50          // -- Unit: [cm]
#define CANDIDATE_SIZE_AVG_MAX      4
#define SELF_TRANSITION_PROB        0.5         // -- TODO: Need to be thought-out again!

#define TRACK_LOCATION_DURATION_MIN     30      // -- Unit: [sec]
#define TRACK_LOCATION_CNT_MIN          5       // -- Unit: [ea]
#define FILTER_MIN_SURVIVAL_RATE        0.5     // -- If the # of locations after track filtering gets smaller than this ratio of the # of location in the original track, then the whole track will be discarded
#define FILTER_TIME_WINDOW_THRESH       60      // -- Unit: [sec]
#define FILTER_DIST_RADIUS              1000    // -- Unit: [cm]
#define FILTER_TIME_RESOLUTION_MIN      3.0      // -- Unit: [sec]
//#define FILTER_OUTAGE_WINDOW_LENGTH     3       // -- Unit: [ea]

#define TRACK_MAX_SHOPPER_SPEED         3 //10      // -- Unit: [m/s]
#define ADD_RANDOMNESS_MAX_TRY_COUNT    10
#define SHOPPER_DETECTION_DELAY         (5*60)        // -- Unit: [sec]


// -- Four directions to explore
array<SquareGrid::Location, 4> SquareGrid::DIRS {Location{1, 0}, Location{0, -1}, Location{-1, 0}, Location{0, 1}};

WiFiPostProcessor::WiFiPostProcessor(string trackerId_suffix) : WiFi_Shopper_Tracker (trackerId_suffix)
{

    mGridType = SQUARE_GRID;
    mSelectedTrackIndex = 0;
    mRandomnessRadius = 4 * 30.48;     // -- Unit: [cm] Note: 1 ft == 30.48 cm
    mTrackZoom = 1.0;
    mTrackOffset_x = 0;
    mTrackOffset_y = 0;

    mShowContourConnectivity = false;
    mShowZoneBoundary = false;
    mShowHmmStates = false;
    mShowHmmStateConnectivity = false;
    mShowAstarWalls = false;
    mShowSampleAstarPath = false;
    mShowRawZoneMap = false;
    mShowSampleClosestZoneAndState = false;

    mShowSampleInitialTrack = false;
    mShowSampleTrackOffset = false;
    mShowSampleTrackZoomed = false;
    mShowSampleFilteredTrack = false;
    mShowSampleViterbiPath = false;
    mShowSampleReconstructedPath = false;
    mShowSampleRealisticPath = false;

    mShowInitialRawTrack = false;
    mShowTrackOffset = false;
    mShowTrackZoomed = false;
    mShowFilteredTrack_PerTrack = false;
    mShowViterbiPath_PerTrack = false;
    mShowReconstructedPath_PerTrack = false;
    mShowRealisticPath_PerTrack = false;


    mSampleLocation1 = Point(0,0);

    mAstarMap = NULL;

    mHmmStateInterval = -1;      // -- Set it negative so that algorithm determine the max interval automatically
    mAdjacencyThreshold = 10;     // -- TODO: Configure

        // -- Default debug Mode Selection
        mDebugVerboseLevel = 0;
        mDebugDeviceStatusVerboseMode = 0;
        mDebugTrackerStatus = true;
        mDebugCalib = false;
        mDebugLocalization = false;
        mDebugLocalizationErrorCode = false;

    mIsThereSomeProcessing = 0;
    mFoundDevice.clear();

    mShowSensors = true;
    mFontSizeScale = 1.0;

//    mNormalDist = normal_distribution<double>(MEAUREMENT_PROB_MEAN, MEAUREMENT_PROB_STD);
//    mExponentialDist = exponential_distribution<double>(TRANSITION_PROB_LAMBDA);
}

WiFiPostProcessor::~WiFiPostProcessor() {}

void WiFiPostProcessor::InitializeTrackerDevice(string trackerDeviceName) {

    mTargetDevName = trackerDeviceName;

    // -- Tracker ID should look like vmhq-16801-{tracker name in dev_name.txt}-{pid}.
    mTrackerID.str("");
    mTrackerID << mTargetStoreName << "-" << mTargetDevName << "-" << getpid();

    mTrackerThingName.str("");
    mTrackerThingName << mTargetStoreName << "-" << mTargetDevName;
}

// -- Extract Store name (like "vmhq-16801") from Store name (like "vmhq_16801")
void WiFiPostProcessor::InitializeStore(string storeName) {

    {
        vector<string> storenametokens = vml::split(storeName, "_");
        mTargetStoreName = storenametokens[0];
        mTargetStoreName.append("-");
        mTargetStoreName.append(storenametokens[1]);

        mTargetStoreName_slash = storenametokens[0];
        mTargetStoreName_slash.append("/");
        mTargetStoreName_slash.append(storenametokens[1]);

        //mTargetDevName = trackerDeviceName;

        printf("Store Name = %s\n",mTargetStoreName.c_str());
    }


    mShowMapVisualization = true;

    mAppStoreName = mTargetStoreName;

    // -- Tracker ID should look like vmhq-16801-{tracker name in dev_name.txt}-{pid}.
    //mTrackerID << mTargetStoreName << "-" << mTargetDevName << "-" << getpid();

    // -- Read config file
    {
        ostringstream configFilename;
        configFilename << APP_CONFIG_ROOT_PATH << "/" << WiFiTracker_APP_NAME << "/" << APP_STORE_ROOT_PATH << "/" << mTargetStoreName << "/" << APP_CONFIG_FILE;
        if(ParseSetting(configFilename.str()) == false) {
            fprintf(stderr, "[StoreMap] ParseSetting() failed\n");
            exit(0);
        }
    }

    // -- Read IP addresses of APs
    if(mMonitoringAP_IPsFilePath.str().find(".txt") != std::string::npos)
        ReadMonitoringAPs_text(mMonitoringAP_IPsFilePath.str());
    else if(mMonitoringAP_IPsFilePath.str().find(".csv") != std::string::npos)
        ReadMonitoringAPs_csv(mMonitoringAP_IPsFilePath.str());
    else {
        fprintf(stderr, "[WiFi_Shopper_Visualizer] Error MONITORING_AP_IP_ADDR_LIST_FILENAME [%s] is not a valid file name/path", mMonitoringAP_IPsFilePath.str().c_str());
        exit(0);
    }

    ostringstream storeFilename;
    storeFilename << APP_CONFIG_ROOT_PATH << "/" << WiFiTracker_APP_NAME << "/" << APP_STORE_ROOT_PATH << "/" << mTargetStoreName << "/" << mStoreLayoutFilename;

    // -- Generate initial map and phy-to-img conversion function
    InitialMapGeneration(storeFilename.str());

    ostringstream hmmStoreFilename;
    hmmStoreFilename << APP_CONFIG_ROOT_PATH << "/" << WiFiTracker_APP_NAME << "/" << APP_STORE_ROOT_PATH << "/" << mTargetStoreName << "/" << HMM_STORE_ZONES_FILENAME;

    // -- Generate Zone Map for HMM modeling
    InitializeHmmMap(storeFilename.str(), hmmStoreFilename.str());


        // -- Add entrance/exit locations
        // -- TODO: Need to change this
        mCheckoutLocations.clear();
        if(mTargetStoreName.compare("carealot-001") == 0) {
            mDoorLocation = ConvertMapCoordToPhyCoord(Point2f(700,950));       // -- Phy Map coordinates
            mCheckoutLocations.push_back(ConvertMapCoordToPhyCoord(Point2f(530,850)));       // -- Phy Map coordinates
            mCheckoutLocations.push_back(ConvertMapCoordToPhyCoord(Point2f(480,860)));       // -- Phy Map coordinates
            mCheckoutLocations.push_back(ConvertMapCoordToPhyCoord(Point2f(420,860)));       // -- Phy Map coordinates
            mCheckoutLocations.push_back(ConvertMapCoordToPhyCoord(Point2f(360,870)));       // -- Phy Map coordinates
        } else if(mTargetStoreName.compare("petsns-231") == 0) {
            mDoorLocation = ConvertMapCoordToPhyCoord(Point2f(510,930));       // -- Phy Map coordinates
            mCheckoutLocations.push_back(ConvertMapCoordToPhyCoord(Point2f(640,850)));       // -- Phy Map coordinates
            mCheckoutLocations.push_back(ConvertMapCoordToPhyCoord(Point2f(510,830)));       // -- Phy Map coordinates
        } else if(mTargetStoreName.compare("petsns-342") == 0) {
            mDoorLocation = ConvertMapCoordToPhyCoord(Point2f(570,850));       // -- Phy Map coordinates
            mCheckoutLocations.push_back(ConvertMapCoordToPhyCoord(Point2f(960,720)));       // -- Phy Map coordinates
            mCheckoutLocations.push_back(ConvertMapCoordToPhyCoord(Point2f(850,720)));       // -- Phy Map coordinates
        }

    // -- Generate a sample track and perform post processing
    {

        // -- Create a sample wifi track
        {
            // -- Assign a unique color for visualization
            mSampleWifiTrack.r = rand()%200;
            mSampleWifiTrack.g = rand()%200;
            mSampleWifiTrack.b = rand()%200;

            mSampleWifiTrack.dev_id = "sample001001";
            mSampleWifiTrack.start_time = 100000;
            mSampleWifiTrack.duration = 75;

            WiFiPostData_t loc;

            loc.posPhy.x = 500; loc.posPhy.y = 500; loc.localtime = mSampleWifiTrack.start_time; mSampleWifiTrack.track.push_back(loc);
            loc.posPhy.x = 900; loc.posPhy.y = 1700; loc.localtime = mSampleWifiTrack.start_time+10; mSampleWifiTrack.track.push_back(loc);
            loc.posPhy.x = 1500; loc.posPhy.y = 2700; loc.localtime = mSampleWifiTrack.start_time+20; mSampleWifiTrack.track.push_back(loc);
            loc.posPhy.x = 1700; loc.posPhy.y = 2900; loc.localtime = mSampleWifiTrack.start_time+50; mSampleWifiTrack.track.push_back(loc);
            loc.posPhy.x = 700; loc.posPhy.y = 4200; loc.localtime = mSampleWifiTrack.start_time+53.5; mSampleWifiTrack.track.push_back(loc);
            loc.posPhy.x = 2100; loc.posPhy.y = 900; loc.localtime = mSampleWifiTrack.start_time+70; mSampleWifiTrack.track.push_back(loc);
            loc.posPhy.x = 3000; loc.posPhy.y = 4500; loc.localtime = mSampleWifiTrack.start_time+72; mSampleWifiTrack.track.push_back(loc);

            mSampleWifiTrack.cnt = mSampleWifiTrack.track.size();
        }

        // -- Perform post-processing on the sample data
        PerformPostProcessing_Sample();

    }

    // -- Displays the initial image of the room
    DrawMap();

    // -- Calculate the coordinates of the sensors on the store layout/map
    for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it) {
        WiFiSensor_short_Ptr os = *it;

        Point mapPos = ConvertPhyCoordToMapCoord(os->mSensorPos_x, os->mSensorPos_y);

        os->mSensorPos_x_map = mapPos.x;
        os->mSensorPos_y_map = mapPos.y;
    }

}

double WiFiPostProcessor::compute_pairwise_distance(vector<Point> cont1 , vector<Point> cont2) {
    double distMin = numeric_limits<double>::max();
    for(uint i=0; i<cont1.size(); i++)
        for(uint j=0; j<cont2.size(); j++) {
            double dist = (pow(cont1[i].x - cont2[j].x,2) + pow(cont1[i].y - cont2[j].y,2));
            if(distMin > dist)
                distMin = dist;
        }
    return sqrt(distMin);
}

// -- TODO: Instead of a grid setting, define the HMM States in a uniformly-randomly distributed manner
bool WiFiPostProcessor::DefineHmmStates() {

    bool isAllZonesHaveStates = false;   // -- Check if all the zones have at least a single state in each of them.
    bool isZoneConnectivityCorrect = false;

    double stateInterval = mHmmStateInterval;

    // -- if mHmmStateInterval == 0, then find the minimum # of states in such a way that all the zones have at least a state in each of them. So, Let the while loop start with a large mHmmStateInterval value.
    if(mHmmStateInterval <= 0) {
        stateInterval = 500;//ConvertPhyDistanceToMapDistance(VITERBI_MAX_STATE_INTERVAL);       // -- Unit: [pixel], A number big enough to start with

        //printf("DefineHmmStates(): stateInterval init = %lf, mPhyToMap_x_ratio = %lf\n", stateInterval, mPhyToMap_x_ratio);
    }

    if(stateInterval < ConvertPhyDistanceToMapDistance(VITERBI_MIN_STATE_INTERVAL) || stateInterval <= 0)
        return false;

    // -- Define states and assign them to the corresponding zones
    // -- Make sure all the zones have at least a state. Keep reducing the state interval if not all zones have at least a state.
    bool isBounced = false;
    while(1) {

        // -- Initialize HMM States
        for(uint i = 0; i< mZones.size(); i++)
            mZones[i].states.clear();

        // -- Deallocate memory if allocated already in the previous round
        if(mAstarMap != NULL)
            delete(mAstarMap);

        // -- Create empty map for A*
        mAstarMap = new GridWithWeights(mImg_map_init_zone.size().width/(float)stateInterval+2, mImg_map_init_zone.size().height/(float)stateInterval+2);

        int y, x;
        mStateMap.clear();
        mAstarMapZoneId.clear();
        y=0;
        for(int h=(stateInterval*0.5+0.5); h<(int)mImg_map_init_zone.size().height; h += (stateInterval+0.5f), y++) {
            x=0;
            vector<Point2f> states;
            vector<int> zoneIdVec;

            for(int w=(stateInterval*0.5+0.5); w<(int)mImg_map_init_zone.size().width; w += (stateInterval+0.5f), x++) {
                Point2f pt(w,h);
                bool isInsideAnyZone = false;
                bool isInsideAnyHoles = false;

                int zoneId = -1;

                for( uint i = 0; i< mZones.size(); i++ ) {
                    double dist = pointPolygonTest(mZones[i].contourPts, pt, true);
                    bool isInsideZone = dist >= 0 || fabs(dist) < stateInterval*0.2;    // -- TODO: Would 0.2 be a good margin? How to make this automatically adjusted?
                    bool isInsideHoles = false;

                    for( uint j = 0; j< mHoles.size(); j++ ) {
                        // -- Check only the holes belong to the parent contour i
                        if( i!= j && mHoles[j].contourHierarchy[3] == mZones[i].contourIndex) {
                            if(pointPolygonTest(mHoles[j].contourPts, pt, false) > 0) {
                                isInsideHoles = true;
                                isInsideAnyHoles = true;
                                break;
                            }
                        }
                    }

                    // -- If inside the polygon and not in one of the holes, then assign the state to a zone
                    if(isInsideZone == true && isInsideHoles == false) {

                        // -- Add a bit of random noise
                        pt.x += ((float)(rand()%200)-100)/10000.f;     // -- range [-0.01, 0.01]
                        pt.y += ((float)(rand()%200)-100)/10000.f;     // -- range [-0.01, 0.01]

                        Point indPair = Point(x,y);     // -- This index will match with the index in mStateMap

                        mZones[i].states.push_back(make_pair(pt, indPair));
                        isInsideAnyZone = true;
                        zoneId = (int)i;
                        break;
                    }
                }

                states.push_back(pt);
                zoneIdVec.push_back(zoneId);

                if(isInsideAnyZone == false || isInsideAnyHoles == true) {
                    // -- Insert as walls
                    mAstarMap->walls.insert(SquareGrid::Location { x, y });
                }
            }

            // -- Store all the states to the StateMap and
            mStateMap.push_back(states);
            mAstarMapZoneId.push_back(zoneIdVec);
        }

        // -- Pass zone matrices
        mAstarMap->setZondIdMap(mAstarMapZoneId);
        mAstarMap->setZoneAdjacency(mZoneAdjacency);

        // -- Check if all the zones have at least a single state in each of them.
        isAllZonesHaveStates = true;
        for(uint i = 0; i< mZones.size(); i++)
            if(mZones[i].states.size() == 0) {
                isAllZonesHaveStates = false;
                break;
            }

        // -- Check if the zone connectivity is still preserved
        isZoneConnectivityCorrect = true;
        {
            // -- Create ZoneAdjacency Map from the state connectivity

            // -- Initialize
            vector<vector<bool> > zoneAdj = mZoneAdjacency;   // -- Adjacency matrix among zones. Size: [# of zones] by [# of zones]
            for(uint i=0; i<zoneAdj.size(); i++) {
                for(uint j=0; j<zoneAdj[i].size(); j++)
                    zoneAdj[i][j] = false;
                zoneAdj[i][i] = true;
            }

            for (int y = 0; y != mAstarMap->height; ++y) {
              for (int x = 0; x != mAstarMap->width; ++x) {
                SquareGrid::Location id {x, y};

                // -- Consider only valid states
                if(mAstarMap->walls.count(id))
                    continue;

                vector<SquareGrid::Location> neighbors = mAstarMap->neighbors(id);
                for(uint i=0; i<neighbors.size(); i++) {
                    zoneAdj[mAstarMap->getZoneId(id)][mAstarMap->getZoneId(neighbors[i])] = true;
                    zoneAdj[mAstarMap->getZoneId(neighbors[i])][mAstarMap->getZoneId(id)] = true;
                }

              }
            }


            // -- See if this zone adjacency map matches with the original zone adjacency map
            for(uint i=0; i<zoneAdj.size(); i++)
                for(uint j=i+1; j<zoneAdj[i].size(); j++)
                    if(zoneAdj[i][j] != mZoneAdjacency[i][j]) {
                        isZoneConnectivityCorrect = false;
                        break;
                    }

        }


        if(isAllZonesHaveStates == false || isZoneConnectivityCorrect == false) {
            // -- Adjust the state interval
            if(isBounced == false)
                stateInterval *= 0.98;
            else
                stateInterval *= 1.02;

            if(stateInterval < ConvertPhyDistanceToMapDistance(VITERBI_MIN_STATE_INTERVAL))
                isBounced = true;

            if(stateInterval > 500) {

                if(DEBUG_LEVEL_HMM >= 0)
                    printf("@@@@ DefineHmmStates(): Something very very wrong @@@@ stateInterval = %.2f, mHmmStateInterval = %.2f @@@@\n", (double)stateInterval, mHmmStateInterval);

                return false;
            }
        } else {

            // -- Get out of forever-loop
            break;
        }

        if(DEBUG_LEVEL_HMM > 1)
            printf("stateInterval = %.2f\n", stateInterval);
    }

    // -- Update value
    mHmmStateInterval = stateInterval+0.5;

    return true;

}

// -- Create the adjacency matrix among zones
void WiFiPostProcessor::CreateZoneAdjacencyMap() {

    mZoneAdjacency.clear();

    for(uint i=0; i<mZones.size(); ++i) {
        vector <bool> adjacencyPerZone;
        for(uint j=0; j<mZones.size(); ++j) {
            bool isAdjacent = false;
            if(mDistBtwZones(i,j) >=0 && mDistBtwZones(i,j) < mAdjacencyThreshold) {
                isAdjacent = true;
            }
            adjacencyPerZone.push_back(isAdjacent);
        }
        mZoneAdjacency.push_back(adjacencyPerZone);
    }

}

void WiFiPostProcessor::InitializeHmmMap(string storeFileName, string zoneFileName) {
    // -- Map Cropping and Resizing
    Mat img_src = imread(storeFileName);
    Mat img_src_zone = imread(zoneFileName);

    if(!img_src.data)  {                             // Check for invalid input
        cout <<  "Could not open or find the image: " << storeFileName << std::endl ;
        return;
    }

    if(!img_src_zone.data)  {                             // Check for invalid input
        cout <<  "Could not open or find the image: " << zoneFileName << std::endl ;
        return;
    }

    Mat img_src_zone_resized;
    resize(img_src_zone, img_src_zone_resized, cvSize(img_src.size().width, img_src.size().height));

    Mat img_src_zone_resized_cropped;
    img_src_zone_resized(mStoreMapRoiMask).copyTo(img_src_zone_resized_cropped);

    // -- Resize to match to the adjusted store map
    Mat img_resized_final;
    resize(img_src_zone_resized_cropped, img_resized_final, cvSize((int)ceil(mStoreWidth_IMG_x), (int)ceil(mStoreWidth_IMG_y)));

        //imshow("Zones Init", img_resized_final);

    // -- Remove the background
    int saturation_low_threshold = 50;  // -- TODO: Configure
    Mat imgHSV, imgHSV_filtered;
    cvtColor(img_resized_final, imgHSV, cv::COLOR_BGR2HSV);
    inRange(imgHSV, Scalar(0, saturation_low_threshold, 0), Scalar(255, 255, 255), imgHSV_filtered);

        //imshow("Zones Init Mask", imgHSV_filtered);

    img_resized_final.copyTo(mImg_map_init_zone, imgHSV_filtered);

        //imshow("Zones Init BG Removed", mImg_map_init_zone);

    // -- Find color blobs to defined zones
    {

        Mat imgGray;
        RNG rng(12345);

        // first convert the image to grayscale
        cvtColor(mImg_map_init_zone, imgGray, COLOR_BGR2GRAY);

        // then adjust the threshold to actually make it binary
        threshold(imgGray, mImg_HmmMap_noiseReduced, 1, 255, THRESH_BINARY);

            //imshow( "Binary Img", mImg_HmmMap_noiseReduced );

        // -- Remove noise
        {
            int morph_elem = MORPH_RECT;
            int morph_size = 1;
            int operation = MORPH_OPEN;

            Mat element = getStructuringElement( morph_elem, Size( 2*morph_size + 1, 2*morph_size+1 ), Point( morph_size, morph_size ) );

            /// Apply the specified morphology operation
            morphologyEx( mImg_HmmMap_noiseReduced, mImg_HmmMap_noiseReduced, operation, element );

                //imshow( "Noise Removed", mImg_HmmMap_noiseReduced );
        }

        // -- Find contours
        Mat imgContour;
        mImg_HmmMap_noiseReduced.copyTo(imgContour);
        vector<vector<Point> > zoneContours;
        vector<Vec4i> zoneContourHierarchy;
        findContours(imgContour, zoneContours, zoneContourHierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_NONE, Point(0, 0) );

        for(uint i=0; i<zoneContours.size(); ++i) {
            Zone_t zone;
            zone.contourPts = zoneContours[i];
            zone.contourIndex = i;
            zone.contourHierarchy = Vec4i(zoneContourHierarchy[i]);

            // -- Zones are outer contours only
            if(zone.contourHierarchy[3] < 0) {
                zone.color = Scalar(rng.uniform(0, 200)+25,rng.uniform(0, 200)+25,rng.uniform(0, 200)+25);
                mZones.push_back(zone);
            } else {
                mHoles.push_back(zone);
            }
        }
    }

        // -- Find the closest distance between contour points
        mDistBtwZones = Mat1f(mZones.size(), mZones.size(), -1.f);     // -- Default distance set to be -1
        {
            for(uint i=0; i<mZones.size(); ++i) {
                mDistBtwZones(i,i) = 0;
            }

            for(uint i=0; i<mZones.size()-1; ++i) {

                const vector<Point>& firstContour = mZones[i].contourPts;
                for(uint j=i+1; j<mZones.size(); ++j) {

                    const vector<Point>& secondContour = mZones[j].contourPts;
                    float d = compute_pairwise_distance(firstContour, secondContour);
                    mDistBtwZones(i,j) = mDistBtwZones(j,i) = d;
                }
            }

                    // -- DEBUG: Print the distance matrix
                    if(DEBUG_LEVEL_HMM > 1) {
                        printf("### Cloest Dist btw Zones: \n");
                        for(uint i=0; i<mZones.size(); ++i) {
                            for(uint j=0; j<mZones.size(); ++j) {
                                printf("%3.0f ", mDistBtwZones(i,j));
                            }
                            printf("\n");
                        }
                        printf("\n");
                    }

        }

        // -- Create the adjacency matrix among zones
        CreateZoneAdjacencyMap();

        // -- Find the distance between the center of zones
        {
            mDistBtwZones_center = Mat1f(mZones.size(), mZones.size(), -1.f);     // -- Default distance set to be -1

            /// Get the moments
            vector<Moments> mu(mZones.size() );
            for(uint i = 0; i < mZones.size(); i++) { mu[i] = moments( mZones[i].contourPts, false ); }

            ///  Get the mass centers:
            for(uint i = 0; i < mZones.size(); i++) { mZones[i].contourCenter = Point2f( mu[i].m10/mu[i].m00 , mu[i].m01/mu[i].m00); }

            for(uint i=0; i<mZones.size(); ++i) {
                mDistBtwZones_center(i,i) = 0;
            }

            for(uint i=0; i<mZones.size()-1; ++i) {
                for(uint j=i+1; j<mZones.size(); ++j) {
                    float d = sqrt(pow(mZones[i].contourCenter.x - mZones[j].contourCenter.x,2) + pow(mZones[i].contourCenter.y - mZones[j].contourCenter.y,2));
                    mDistBtwZones_center(i,j) = mDistBtwZones_center(j,i) = d;
                }
            }

                    // -- DEBUG: Print the distance matrix
                    if(DEBUG_LEVEL_HMM > 1) {
                        printf("### Dist btw Center of Zones: \n");
                        for(uint i=0; i<mZones.size(); ++i) {
                            for(uint j=0; j<mZones.size(); ++j) {
                                printf("%3.0f ", mDistBtwZones_center(i,j));
                            }
                            printf("\n");
                        }
                        printf("\n");
                    }
        }

        // -- Simplify contours
        int epsilon = 1;        // -- TODO: Configure
        for(uint i = 0; i< mZones.size(); i++) {
            approxPolyDP(mZones[i].contourPts, mZones[i].contourPts, epsilon, true);
        }

        // -- Define states and assign them to the corresponding zones
        DefineHmmStates();

        mSampleAstarPath_startZone = 1;
        mSampleAstarPath_endZone = mZones.size() - 1;

}

void WiFiPostProcessor::DrawTrack(WiFiTrack_t *track, bool randomColor) {

    if(track->track.size() == 0)
        return;

    // -- Mark the trajectories passed in the past
    Point pt_curr, pt_prev = Point(-999,-999);

    cv::Scalar dev_color;

    if(randomColor == true) {
        dev_color = CV_RGB(rand()%200, rand()%200, rand()%200);
    } else
        dev_color = CV_RGB(track->r, track->g, track->b);

    for(deque<WiFiPostData_t>::iterator it = track->track.begin(); it != track->track.end(); ++it)
    {
        // -- Tracked Position
        if(pt_prev.x == -999 && pt_prev.y == -999) {
            pt_prev = ConvertPhyCoordToMapCoord((*it).posPhy.x, (*it).posPhy.y);
            circle(mImg_map, pt_prev, 4 /*radius*/, dev_color, 2 /*thickness*/, 16, 0);
        } else {
            pt_curr = ConvertPhyCoordToMapCoord((*it).posPhy.x, (*it).posPhy.y);
            line(mImg_map, pt_prev, pt_curr, dev_color,2,8,0);
            circle(mImg_map, pt_curr, 4 /*radius*/, dev_color, 2 /*thickness*/, 16, 0);
            pt_prev = pt_curr;
        }
    }

    // -- Mark the last location on top of the shown trajectories (as a small red dot)
    WiFiPostData_t *lastLoc = &*(track->track.end()-1);
    if(lastLoc->posPhy.x != 0 && lastLoc->posPhy.y != 0) {
        circle(mImg_map,ConvertPhyCoordToMapCoord(lastLoc->posPhy.x, lastLoc->posPhy.y), 4+1 /*radius*/, CV_RGB(255,0,0), CV_FILLED, 16,0);
        circle(mImg_map,ConvertPhyCoordToMapCoord(lastLoc->posPhy.x, lastLoc->posPhy.y), 4+1 /*radius*/, CV_RGB(0,100,0), 1, 16,0);

        char text[99];
        sprintf(text, "[%s] %d", track->dev_id.c_str(), track->cnt);
        Point pt = ConvertPhyCoordToMapCoord(lastLoc->posPhy.x, lastLoc->posPhy.y);
        putText(mImg_map, text, Point(pt.x + 5, pt.y -3), 0, 0.4*mFontSizeScale,CV_RGB(255,0,0), 1, CV_AA);
    }

}

// -- Convert from a A* map (i.e., an index map) point to a store map (i.e., a physical map) point
Point WiFiPostProcessor::ConvertAstarMapPtToStoreMapPt(SquareGrid::Location pt_a) {
    return Point(mStateMap[get<1>(pt_a)][get<0>(pt_a)].x+0.5, mStateMap[get<1>(pt_a)][get<0>(pt_a)].y+0.5);
}

// -- Convert from a store map (i.e., a physical map) point to a A* map (i.e., an index map) point
SquareGrid::Location WiFiPostProcessor::ConvertStoreMapPtToAstarMapPt(Point pt_s) {
    return SquareGrid::Location {pt_s.x / mHmmStateInterval, pt_s.y / mHmmStateInterval};
}

// -- Return the closest point to a zone
tuple<Zone_t*, Point2f, Point> WiFiPostProcessor::findClosestZoneAndPoint(Point2f pt) {
    Point2f pt_closest_storeMap = Point2f(-1, -1);
    Point pt_closest_indexMap = Point(-1, -1);

    // -- See if the test pouint is inside any zone
    Zone_t *zoneClosest = NULL;
    for(uint i=0; i<mZones.size(); i++) {
        if(isInsidePolygonVector(pt, mZones[i].contourPts) == true) {
            zoneClosest = &mZones[i];
            break;
        }
    }

    // -- If so find the closest state defined inside the particular zone
    if(zoneClosest != NULL) {
        double distMin = numeric_limits<double>::max();
        for(uint i=0; i<zoneClosest->states.size(); i++) {
            Point2f state = zoneClosest->states[i].first;
            double dist = (pow(state.x - pt.x,2) + pow(state.y - pt.y,2));
            if(distMin > dist) {
                distMin = dist;
                pt_closest_storeMap = state;
                pt_closest_indexMap = zoneClosest->states[i].second;
            }
        }

        return make_tuple(zoneClosest, pt_closest_storeMap, pt_closest_indexMap);
    }
    // -- If not, find the zone that are closest to the test point
    else {
        // -- Find a zone that has the closest point on the zone to the test point by calculating the closest distance to all the edges of each zone
        Point2f pt_closestToZone_storeMap[mZones.size()];
        double distToZones[mZones.size()];

        // -- Use isClosestPointInsideLineSegment() function to find the closest edge of a zone and the closest point on the edge. And calculate the closest distance to the zone.
        double MIN_EDGE_LENGTH = 10;    // -- TODO: Configure!
        for(uint i=0; i<mZones.size(); i++) {
            distToZones[i] = numeric_limits<double>::max();
            for(uint j=1; j<mZones[i].contourPts.size()+1; j++) {
                Point2f pt_prev = mZones[i].contourPts[j-1];
                Point2f pt_curr = mZones[i].contourPts[j%mZones[i].contourPts.size()];

                double edgeLength = (pow(pt_prev.x - pt_curr.x,2) + pow(pt_prev.y - pt_curr.y,2));
                if(edgeLength > MIN_EDGE_LENGTH*MIN_EDGE_LENGTH) {
                    Point2f P_proj = findClosestPointToLine(pt, pt_prev, pt_curr);
                    double dist = sqrt(pow(pt.x - P_proj.x,2) + pow(pt.y - P_proj.y,2));

                    // -- Check if the projected pouint is inside the line segment and the closest so far
                    if(P_proj.x <= max(pt_prev.x, pt_curr.x) && P_proj.x >= min(pt_prev.x, pt_curr.x)
                            && P_proj.y <= max(pt_prev.y, pt_curr.y) && P_proj.y >= min(pt_prev.y, pt_curr.y)
                            && distToZones[i] > dist) {

                        pt_closestToZone_storeMap[i] = P_proj;
                        distToZones[i] = dist;

//                            printf("        Found --> (i, distToZones[i]) = (%d, %.2f), {pt_prev, pt_curr, p_proj} = {(%.0f,%.0f) (%.0f,%.0f) (%.0f,%.0f)}\n",
//                                    i, distToZones[i], pt_prev.x, pt_prev.y, pt_curr.x, pt_curr.y, P_proj.x, P_proj.y);
                    }
                }
            }

            // -- Check if one of the vertices is the closest point
            {
                // -- Search through the vertices
                for(uint j=0; j<mZones[i].contourPts.size(); j++) {
                   double dist = sqrt(pow(pt.x - mZones[i].contourPts[j].x,2) + pow(pt.y - mZones[i].contourPts[j].y,2));
                   if(distToZones[i] > dist) {
                       pt_closestToZone_storeMap[i] = mZones[i].contourPts[j];
                       distToZones[i] = dist;
                   }
                }

                //printf("        Among vertices: (i, distToZones[i]) = (%d, %.2f)\n", i, distToZones[i]);
            }

//            if(isFound == true) {
//                circle(mImg_map, pt_closestToZone[i], 9, CV_RGB(0,0,155), 2, 16,0);
//                //printf("Final: (i, distToZones[i]) = (%d, %.2f)\n\n", i, distToZones[i]);
//            } else {
//                //printf("Final: (i, distToZones[i]) = (%d, NOT FOUND!)\n\n", i);
//            }
        }

        // -- Find the closest zone to the test point
        double distMin = numeric_limits<double>::max();
        int indexMinZone = -1;
        for(uint i=0; i<mZones.size(); i++) {
            if(distMin > distToZones[i]) {
                distMin = distToZones[i];
                indexMinZone = i;
            }
        }

        // -- Find the closest state of the zone to the test point
        if(indexMinZone >= 0) {
            zoneClosest = &mZones[indexMinZone];

            double distMin = numeric_limits<double>::max();
            for(uint i=0; i<zoneClosest->states.size(); i++) {
                Point2f state = zoneClosest->states[i].first;
                double dist = sqrt(pow(state.x - pt.x,2) + pow(state.y - pt.y,2));
                if(distMin > dist) {
                    distMin = dist;
                    pt_closest_storeMap = state;
                    pt_closest_indexMap = zoneClosest->states[i].second;
                }
            }

            return make_tuple(zoneClosest, pt_closest_storeMap, pt_closest_indexMap);
        } else {
            return make_tuple((Zone_t*)NULL, pt_closest_storeMap, pt_closest_indexMap);
        }

    }
}

// -- Find the zone index that the state belongs to (on Store Map)
int WiFiPostProcessor::FindZone_StoreMap(Point2f state) {
    for( uint i = 0; i< mZones.size(); i++ ) {
        for(uint j=0; j<mZones[i].states.size(); j++) {
            if(state == mZones[i].states[j].first)
                return i;
        }
    }
    return -1;
}

// -- Find the zone index that the state belongs to (on Index Map, which is A* Map)
int WiFiPostProcessor::FindZone_IndexMap(Point state) {
    for( uint i = 0; i< mZones.size(); i++ ) {
        for(uint j=0; j<mZones[i].states.size(); j++) {
            if(state == mZones[i].states[j].second)
                return i;
        }
    }
    return -1;
}

// -- Given a point on the store map, return the corresponding point on the index map (aka Astar Map)
Point WiFiPostProcessor::ConvertStoreMapPtToIndexMapPt(Point2f pt) {
    for( uint i = 0; i< mZones.size(); i++ ) {
        for(uint j=0; j<mZones[i].states.size(); j++) {
            if(pt == mZones[i].states[j].first)
                return mZones[i].states[j].second;
        }
    }

 return Point(-1,-1);

}

// -- Given a point on the index map (aka Astar Map), return the corresponding point on the store map
Point2f WiFiPostProcessor::ConvertIndexMapPtToStoreMapPt(Point pt) {
    for( uint i = 0; i< mZones.size(); i++ ) {
        for(uint j=0; j<mZones[i].states.size(); j++) {
            if(pt == mZones[i].states[j].second)
                return mZones[i].states[j].first;
        }
    }

 return Point2f(-1,-1);
}


// -- Return a list of candidate states from a location, loc, within the radius on Store Map
vector<tuple<Zone_t*, Point2f, Point> > WiFiPostProcessor::GetCandidateList(Point2f loc, double radius) {
    vector<tuple<Zone_t*, Point2f, Point> > candList;

    if(DEBUG_LEVEL_VITERBI > 1)
        printf("\nGetCandidateList(): loc = (%.0f, %.0f) w/ Radius = %.2f\n", loc.x, loc.y, radius);

    for( uint i = 0; i< mZones.size(); i++ ) {
        for(uint j=0; j<mZones[i].states.size(); j++) {
            Point2f state = mZones[i].states[j].first;
            //double dist = sqrt(pow(loc.x - state.x,2)+pow(loc.y - state.y,2));        // -- Eucledian distance
            double dist = (fabs(loc.x - state.x)+fabs(loc.y - state.y))*0.5;            // -- Manhattan distance
            if(dist <= radius) {
                candList.push_back(make_tuple(&mZones[i], mZones[i].states[j].first, mZones[i].states[j].second));

                if(DEBUG_LEVEL_VITERBI > 1)
                    printf("    -- GetCandidateList(): candList[%d] = (%.0f, %.0f)\n", (int)candList.size()-1, state.x, state.y);
            }
        }
    }


    return candList;
}

// -- Return the emission probability of the state from a location, loc, on Store Map
// -- It is modelled as a Gaussian
double WiFiPostProcessor::GetEmissionProb(Point2f loc, Point state) {
    double dist = sqrt(pow(loc.x - state.x,2)+pow(loc.y - state.y,2));

    double prob = ( 1.0f / ( ConvertPhyDistanceToMapDistance(MEAUREMENT_PROB_STD) * sqrt(2*M_PI) ) ) * exp( -0.5 * pow( (dist - ConvertPhyDistanceToMapDistance(MEAUREMENT_PROB_MEAN))/ConvertPhyDistanceToMapDistance(MEAUREMENT_PROB_STD), 2.0 ) );

    return prob;
}


// -- Return the shortest travel distance on Store Map from Candidate State I to Candidate State J via route path using A* algorithm
double WiFiPostProcessor::GetRouteDistance(Point I, Point J) {

    // -- TODO: This is not supposed to be needed, but whenever I or J is (0,0), it would cause a huge memory usage increase. So, I temporarily put a condition here. Need to be revisited again.
    if(I.x <= 0 || I.y <= 0 || J.x <= 0 || J.y <= 0)
        return numeric_limits<double>::max();

    // -- Get the route from state A to state B using A* algorithm
    SquareGrid::Location start{I.x, I.y};
    SquareGrid::Location goal{J.x, J.y};

    unordered_map<SquareGrid::Location, SquareGrid::Location> came_from;
    unordered_map<SquareGrid::Location, double> cost_so_far;

    // -- Perform A* search
    a_star_search(*mAstarMap, start, goal, came_from, cost_so_far);

    return cost_so_far[goal];

//    // -- Show the found shortest path in the command line
//    vector<SquareGrid::Location> path = reconstruct_path(start, goal, came_from);

//    // -- Calculate the travel distance along the found shortest path on the store map
//    double distanceAlongPath = 0;
//    {
//        SquareGrid::Location pt_prev, pt_curr;
//        pt_prev = path[0];
//        for(uint i=1; i<path.size(); i++) {
//            pt_curr = path[i];

//            Point pt1 = ConvertAstarMapPtToStoreMapPt(pt_prev);
//            Point pt2 = ConvertAstarMapPtToStoreMapPt(pt_curr);

//            distanceAlongPath += sqrt(pow(pt1.x - pt2.x,2) + pow(pt1.y - pt2.y,2));

//            pt_prev = pt_curr;
//        }
//    }

//    return distanceAlongPath;
}

// -- Return the transition probability from location A to location B via Candidate I and J
double WiFiPostProcessor::GetTransitionProb(Point2f A, Point2f B, Point I, Point J) {

    // -- Get the shortest travel distance from State I to State J using A* algorithm
    double distanceAlongPath = GetRouteDistance(I, J);

    // -- Euclidean distance between two states
    double distanceEuc = sqrt(pow(A.x - B.x,2) + pow(A.y - B.y,2));

    // -- Transition probability is defined as the ratio between Euclidean distance and the path distance as in Lou et. al.
    // -- TODO: There could be a better definition of Transition probability like in Newson et. al.
    double transProb = 0;

    if(distanceEuc == 0) {
        transProb = 1.0f - SELF_TRANSITION_PROB;
    } else {
        if(distanceAlongPath > 0) {
            transProb = distanceEuc / distanceAlongPath;
        } else {
            //transProb = std::min((double)1.0f, distanceEuc / (double)(mHmmStateInterval*2.f));
            transProb = SELF_TRANSITION_PROB;
        }
    }

    return transProb;
}

WiFiErrorType_t WiFiPostProcessor::AddOffsetInTrajectory(WiFiTrack_t &wifiTrack) {


    //if(mTrackOffset_x != 0 || mTrackOffset_y != 0)
        for(uint i=0; i<wifiTrack.track.size(); i++) {
            wifiTrack.track[i].posPhy = Point2f(wifiTrack.track[i].posPhy.x + mTrackOffset_x, wifiTrack.track[i].posPhy.y + mTrackOffset_y);
        }

    return SUCCESSFUL;
}

WiFiErrorType_t WiFiPostProcessor::AddZoomInTrajectory(WiFiTrack_t &wifiTrack) {
    Point2f imgCenter = Point2f(mImg_map_init_zone.size().width*0.5, mImg_map_init_zone.size().height*0.5);

    for(uint i=0; i<wifiTrack.track.size(); i++) {

        Point2f imgLoc = ConvertPhyCoordToMapCoord(wifiTrack.track[i].posPhy);

        // -- Move the img center to origin
        Point2f transLoc = Point2f(imgLoc.x - imgCenter.x, imgLoc.y - imgCenter.y);

        // -- Computer the distance to the origin of the map
        double distToOrigin = sqrt(pow(transLoc.x,2) + pow(transLoc.y,2));

        Point2f unitVec = Point2f(transLoc.x / distToOrigin, transLoc.y / distToOrigin);

        // -- Zoom
        Point2f transLoc_zoomed = Point2f(unitVec.x*distToOrigin*mTrackZoom, unitVec.y*distToOrigin*mTrackZoom);

        // -- Move back
        Point2f transformedLoc = Point2f(transLoc_zoomed.x + imgCenter.x, transLoc_zoomed.y + imgCenter.y);

        wifiTrack.track[i].posPhy = ConvertMapCoordToPhyCoord(transformedLoc);
    }

    return SUCCESSFUL;
}

WiFiErrorType_t WiFiPostProcessor::FilterRawTrajectory(WiFiTrack_t &wifiTrack) {

    // -- Filter based on # of points criteria
    if(wifiTrack.cnt < TRACK_LOCATION_CNT_MIN) {
        if(DEBUG_LEVEL_FILTERING > 0)
            printf("FilterRawTrajectory(): Failed because wifiTrack.cnt < TRACK_LOCATION_CNT_MIN (%d, %d)\n", wifiTrack.cnt, TRACK_LOCATION_CNT_MIN);
        return TOO_SMALL_TRACK_POINT;
    }

    // -- Filter based on Duration
    if(wifiTrack.duration < TRACK_LOCATION_DURATION_MIN) {
        if(DEBUG_LEVEL_FILTERING > 0)
            printf("FilterRawTrajectory(): Failed because wifiTrack.duration < TRACK_LOCATION_DURATION_MIN (%.0f, %d)\n", wifiTrack.duration, TRACK_LOCATION_DURATION_MIN);
        return TOO_SHORT_TRACK_DURATION;
    }

    double t0 = wifiTrack.start_time;

    // -- Filter based on outages

        // -- Find the best starting point
        int startingPtIndex = -1;

        double timeWindowLength = FILTER_TIME_WINDOW_THRESH;   // -- Unit: [sec]
        double distRadius = FILTER_DIST_RADIUS;   // -- Unit: [cm]

        while(startingPtIndex < 0) {
            int density[wifiTrack.track.size()] = {0};

            for(uint i=0; i<wifiTrack.track.size(); i++) {
                Point2f loc_i = wifiTrack.track[i].posPhy;
                double dt_i = wifiTrack.track[i].localtime - t0;

                int cnt = 0;    // -- # of points within the time window

                for(uint j=i; j<wifiTrack.track.size(); j++) {
                    Point2f loc_j = wifiTrack.track[j].posPhy;
                    double dt_j = wifiTrack.track[j].localtime - t0;

                    double timeDiff = fabs(dt_j - dt_i);
                    double dist = (pow(loc_i.x - loc_j.x,2) + pow(loc_i.y - loc_j.y,2));

                    if(timeDiff < timeWindowLength && dist < distRadius*distRadius)
                        cnt++;
                }

                density[i] = cnt;
            }

            // -- Find the max density point
            int maxInd = -1;
            int maxDensity = 0;
            for(uint i=0; i<wifiTrack.track.size(); i++) {
                if(density[i] > maxDensity) {
                    maxDensity = density[i];
                    maxInd = i;
                }
            }

            startingPtIndex = maxInd;

            if(DEBUG_LEVEL_FILTERING > 1)
                printf("FilterRawTrajectory(): startingPtIndex = %d among %d total cnt w/ density = %d\n", startingPtIndex, (int)wifiTrack.track.size(), maxDensity);

            distRadius *= 1.05;
            timeWindowLength *= 1.05;
        }


        if(startingPtIndex < 0) {
            if(DEBUG_LEVEL_FILTERING > 0)
                printf("FilterRawTrajectory(): Failed because startingPtIndex < 0\n");
            return INVALID_START_PT_INDEX;
        }

        // -- Track after filtering
        deque<WiFiPostData_t> track_filtered = wifiTrack.track;

        // -- Given a starting point, start filtering the track in both directions toward the both edges
        double dist, dt, speed;

        // -- Positive-time direction
        for(uint i=startingPtIndex; i<wifiTrack.track.size()-1; i++) {
            // -- Check if the move to the next consecutive pouint is physically feasible move
            dist = sqrt(pow(wifiTrack.track[i].posPhy.x - wifiTrack.track[i+1].posPhy.x,2) + pow(wifiTrack.track[i].posPhy.y - wifiTrack.track[i+1].posPhy.y,2));
            dt = fabs(wifiTrack.track[i+1].localtime - wifiTrack.track[i].localtime);
            speed = dist / dt;

            // -- If it is not a feasible move, then find the next feasible move.
            if(
                (dt > FILTER_TIME_RESOLUTION_MIN && speed >= TRACK_MAX_SHOPPER_SPEED*100.f)
                 || (dt <= FILTER_TIME_RESOLUTION_MIN && dist > TRACK_MAX_SHOPPER_SPEED*dt)
                ) {

                if(DEBUG_LEVEL_FILTERING > 1)
                    printf("# FilterRawTrajectory(): Found a too speedy movement between [%d]th location (%.0f, %.0f) and [%d]th location (%.0f, %.0f)\n", i, wifiTrack.track[i].posPhy.x, wifiTrack.track[i].posPhy.y, i+1, wifiTrack.track[i+1].posPhy.x, wifiTrack.track[i+1].posPhy.y);

                for(uint j=i+1; j<wifiTrack.track.size(); j++) {
                    dist = sqrt(pow(wifiTrack.track[i].posPhy.x - wifiTrack.track[j].posPhy.x,2) + pow(wifiTrack.track[i].posPhy.y - wifiTrack.track[j].posPhy.y,2));
                    dt = fabs(wifiTrack.track[j].localtime - wifiTrack.track[i].localtime);
                    speed = dist / dt;

                    Point2f diffVec = Point2f(wifiTrack.track[j].posPhy.x - wifiTrack.track[i].posPhy.x, wifiTrack.track[j].posPhy.y - wifiTrack.track[i].posPhy.y);
                    double diffVec_norm = sqrt(pow(wifiTrack.track[j].posPhy.x - wifiTrack.track[i].posPhy.x,2) + pow(wifiTrack.track[j].posPhy.y - wifiTrack.track[i].posPhy.y,2));
                    Point2f unitVec = Point2f(diffVec.x/diffVec_norm, diffVec.y/diffVec_norm);
                    double totalTime = fabs(wifiTrack.track[j].localtime - wifiTrack.track[i].localtime);

                    // -- If jth is the last point, then correct it with assumption of the max speed
                    if(j == wifiTrack.track.size()-1) {
                        speed = diffVec_norm / totalTime;
                        double weight = std::min((double)1.0f, TRACK_MAX_SHOPPER_SPEED*100.f / speed);
                        track_filtered[j].posPhy = Point2f(wifiTrack.track[i].posPhy.x + weight*diffVec_norm*unitVec.x, wifiTrack.track[i].posPhy.y + weight*diffVec_norm*unitVec.y);

                        if(DEBUG_LEVEL_FILTERING > 1)
                            printf("        --> FilterRawTrajectory(): LAST POINT: speed = %.2f, but TRACK_MAX_SHOPPER_SPEED = %.2f\n", speed, TRACK_MAX_SHOPPER_SPEED*100.f);

                    } else
                    // -- If find the next sensible location, then interpolate the intermediate points from i+1 to j-1.
                    if((dt > FILTER_TIME_RESOLUTION_MIN && speed < TRACK_MAX_SHOPPER_SPEED*100.f)
                        || (dt <= FILTER_TIME_RESOLUTION_MIN && dist <= TRACK_MAX_SHOPPER_SPEED*dt)
                        ){

                        if(DEBUG_LEVEL_FILTERING > 1) {
                            printf("    --> FilterRawTrajectory(): Found a sensible location at [%d]th location (%.0f, %.0f)\n", j, wifiTrack.track[j].posPhy.x, wifiTrack.track[j].posPhy.y);
                            printf("                               UnitVec between [%d] and [%d] = (%f, %f), and totalTime = %.2f \n", i, j, unitVec.x, unitVec.y, totalTime);
                        }

                        for(uint k=i+1; k<=j; k++) {

                            if(DEBUG_LEVEL_FILTERING > 1)
                                printf("        --> FilterRawTrajectory(): Correct a location at [%d]th location, from (%.0f, %.0f) --> ", k, wifiTrack.track[k].posPhy.x, wifiTrack.track[k].posPhy.y);

                            double weight = fabs(wifiTrack.track[k].localtime - wifiTrack.track[i].localtime) / totalTime;
                            track_filtered[k].posPhy = Point2f(wifiTrack.track[i].posPhy.x + weight*diffVec_norm*unitVec.x, wifiTrack.track[i].posPhy.y + weight*diffVec_norm*unitVec.y);

                            if(DEBUG_LEVEL_FILTERING > 1)
                                printf(" to (%.0f, %.0f)\n", track_filtered[k].posPhy.x, track_filtered[k].posPhy.y);
                        }


                        i = max((uint)0,j-1);
                        break;
                    }
                }
            }
        }

        // -- Negative-time direction
        for(uint i=startingPtIndex; i>0; i--) {
            // -- Check if the move to the next consecutive pouint is physically feasible move
            dist = sqrt(pow(wifiTrack.track[i].posPhy.x - wifiTrack.track[i-1].posPhy.x,2) + pow(wifiTrack.track[i].posPhy.y - wifiTrack.track[i-1].posPhy.y,2));
            dt = fabs(wifiTrack.track[i-1].localtime - wifiTrack.track[i].localtime);
            speed = dist/ dt;

            //printf("FilterRawTrajectory(): Speed at [%d] location = %.2f [m/s]\n", (int)i, speed/100.f);

            if(
                    (dt > FILTER_TIME_RESOLUTION_MIN && speed >= TRACK_MAX_SHOPPER_SPEED*100.f)
                    || (dt <= FILTER_TIME_RESOLUTION_MIN && dist > TRACK_MAX_SHOPPER_SPEED*dt)
                    ) {

                if(DEBUG_LEVEL_FILTERING > 1)
                    printf("# FilterRawTrajectory(): Found a too speedy movement between [%d]th location (%.0f, %.0f) and [%d]th location (%.0f, %.0f)\n", i, wifiTrack.track[i].posPhy.x, wifiTrack.track[i].posPhy.y, i-1, wifiTrack.track[i-1].posPhy.x, wifiTrack.track[i-1].posPhy.y);

                for(int j=i-1; j>=0; j--) {
                    dist = sqrt(pow(wifiTrack.track[i].posPhy.x - wifiTrack.track[j].posPhy.x,2) + pow(wifiTrack.track[i].posPhy.y - wifiTrack.track[j].posPhy.y,2));
                    dt = fabs(wifiTrack.track[j].localtime - wifiTrack.track[i].localtime);
                    speed = dist / dt;

                    Point2f diffVec = Point2f(wifiTrack.track[j].posPhy.x - wifiTrack.track[i].posPhy.x, wifiTrack.track[j].posPhy.y - wifiTrack.track[i].posPhy.y);
                    double diffVec_norm = sqrt(pow(wifiTrack.track[j].posPhy.x - wifiTrack.track[i].posPhy.x,2) + pow(wifiTrack.track[j].posPhy.y - wifiTrack.track[i].posPhy.y,2));
                    Point2f unitVec = Point2f(diffVec.x/diffVec_norm, diffVec.y/diffVec_norm);
                    double totalTime = fabs(wifiTrack.track[j].localtime - wifiTrack.track[i].localtime);

                    // -- If jth is the last point, then correct it with assumption of the max speed
                    if(j == 0) {
                        speed = diffVec_norm / totalTime;
                        double weight = std::min((double)1.0f, TRACK_MAX_SHOPPER_SPEED*100.f / speed);
                        track_filtered[j].posPhy = Point2f(wifiTrack.track[i].posPhy.x + weight*diffVec_norm*unitVec.x, wifiTrack.track[i].posPhy.y + weight*diffVec_norm*unitVec.y);

                        if(DEBUG_LEVEL_FILTERING > 1)
                            printf("        --> FilterRawTrajectory(): LAST POINT: speed = %.2f, but TRACK_MAX_SHOPPER_SPEED = %.2f\n", speed, TRACK_MAX_SHOPPER_SPEED*100.f);

                    } else
                    // -- If find the next sensible location, then interpolate the intermediate points from i-1 to j.
                    if((dt > FILTER_TIME_RESOLUTION_MIN && speed < TRACK_MAX_SHOPPER_SPEED*100.f)
                        || (dt <= FILTER_TIME_RESOLUTION_MIN && dist <= TRACK_MAX_SHOPPER_SPEED*dt)
                        ){

                        if(DEBUG_LEVEL_FILTERING > 1) {
                            printf("    --> FilterRawTrajectory(): Found a sensible location at [%d]th location (%.0f, %.0f)\n", j, wifiTrack.track[j].posPhy.x, wifiTrack.track[j].posPhy.y);
                            printf("                               UnitVec between [%d] and [%d] = (%f, %f), and totalTime = %.2f \n", i, j, unitVec.x, unitVec.y, totalTime);
                        }

                        for(int k=i-1; k>=j; k--) {

                            if(DEBUG_LEVEL_FILTERING > 1)
                                printf("        --> FilterRawTrajectory(): Correct a location at [%d]th location, from (%.0f, %.0f) --> ", k, wifiTrack.track[k].posPhy.x, wifiTrack.track[k].posPhy.y);

                            double weight = fabs(wifiTrack.track[k].localtime - wifiTrack.track[i].localtime) / totalTime;
                            track_filtered[k].posPhy = Point2f(wifiTrack.track[i].posPhy.x + weight*diffVec_norm*unitVec.x, wifiTrack.track[i].posPhy.y + weight*diffVec_norm*unitVec.y);

                            if(DEBUG_LEVEL_FILTERING > 1)
                                printf(" to (%.0f, %.0f)\n", track_filtered[k].posPhy.x, track_filtered[k].posPhy.y);
                        }


                        i = min(j+1,(int)wifiTrack.track.size()-1);
                        break;
                    }
                }
            }
        }

        // -- Copy the filtered track into wifiTrack
        wifiTrack.track = track_filtered;

    return SUCCESSFUL;
}

// -- Given a wifiTrack, find the most probable shopping path. This does not include interpolation yet.
WiFiErrorType_t WiFiPostProcessor::FindBestRoute_Viterbi(WiFiTrack_t &wifiTrack) {

    if( wifiTrack.track.size() == 0)
        return TOO_SMALL_TRACK_POINT;

    double viterbiRadius = mHmmStateInterval*1.1f;//ConvertPhyDistanceToMapDistance(VITERBI_RADIUS_MIN);

    // -- Check if there is at least a single candidate for all the locations, and adjust the radius for finding the candidate states in Viterbi
    while(1) {
        vector<tuple<Zone_t*, Point2f, Point> > candList;   // -- A list of candidate states
        double candListSizeAvg = 0;
        bool isAtleastOneCandPerTrackPt = true;
        candListSizeAvg = 0;

        for(uint t=0; t<wifiTrack.track.size(); t++) {
            // -- Given a location, find the candidate list of states and their associated zones
            candList = GetCandidateList(Point2f(ConvertPhyCoordToMapCoord(wifiTrack.track[t].posPhy)), viterbiRadius);
            candListSizeAvg += candList.size();

            if(candList.size() == 0) {
                isAtleastOneCandPerTrackPt = false;

                if(DEBUG_LEVEL_VITERBI > 1)
                    printf("!!! candList.size() == 0 when viterbiRadius = %.2f, t = %d \n", viterbiRadius, t);

                break;
            }
        }
        candListSizeAvg /= (double)wifiTrack.track.size();

        if(isAtleastOneCandPerTrackPt == true
                || candListSizeAvg > CANDIDATE_SIZE_AVG_MAX
                || viterbiRadius > mHmmStateInterval*2.f
                ) {

            if(DEBUG_LEVEL_VITERBI > 0)
                printf("[Viterbi] viterbiRadius = %.2f [cm] when candListSizeAvg = %.2f\n", viterbiRadius, candListSizeAvg);

            break;
        } else
            viterbiRadius *= 1.1;
    };

    // -- Add the door locations as both entrance and exit points to each wifiTrack, and also checkout location
    {
        int rx, ry;
        WiFiPostData_t enterPt, exitPt;

        rx = rand()%600 - 300; ry = rand()%600 - 300;
        enterPt.posPhy = Point2f(mDoorLocation.x + rx, mDoorLocation.y + ry);

        rx = rand()%600 - 300; ry = rand()%600 - 300;
        exitPt.posPhy = Point2f(mDoorLocation.x + rx, mDoorLocation.y + ry);

        wifiTrack.cnt += 2;
        wifiTrack.start_time -= SHOPPER_DETECTION_DELAY;
        wifiTrack.duration += SHOPPER_DETECTION_DELAY*2;

        enterPt.localtime = wifiTrack.start_time;
        exitPt.localtime = wifiTrack.start_time + wifiTrack.duration + SHOPPER_DETECTION_DELAY*2;

        // -- Add checkout location when exit
        {
            WiFiPostData_t checkoutPt;
            uint ind = (rand()%((int)((double)mCheckoutLocations.size()*1.15)*100))/100.f+0.5f;       // -- TODO: Need to roll a dice based on the actual data on how many visitors actually visits the checkout area.

            if(ind < mCheckoutLocations.size()) {
                checkoutPt.posPhy = mCheckoutLocations[ind];
                checkoutPt.localtime = wifiTrack.start_time + wifiTrack.duration + SHOPPER_DETECTION_DELAY*1.5;
                wifiTrack.track.push_back(checkoutPt);
            }
        }

        // -- Add entrance/exit locations
        wifiTrack.track.push_back(exitPt);
        wifiTrack.track.push_front(enterPt);
    }


    vector<tuple<Zone_t*, Point2f, Point> > candList_prev;  // -- A list of candidate states in the previous round (i.e., at t-1)
    vector<double> estimatedFinalProb_prev;                 // -- Estimated Final Prob at t-1

    for(uint t=0; t<wifiTrack.track.size(); t++) {

        vector<tuple<Zone_t*, Point2f, Point> > bestCandList;   // -- For each candidate state at t, this keeps the best candidate state found at t-1

        // -- Given a location, find the candidate list of states and their associated zones
        vector<tuple<Zone_t*, Point2f, Point> > candList_curr = GetCandidateList(ConvertPhyCoordToMapCoord(wifiTrack.track[t].posPhy), viterbiRadius);

        if(candList_curr.size() == 0) {

            if(DEBUG_LEVEL_VITERBI > 0)
                printf("@@@@ NO CANDIDATE LIST at t = %d, posPhy = (%.0f, %.0f) @@@@ \n", t, wifiTrack.track[t].posPhy.x, wifiTrack.track[t].posPhy.y);


            candList_curr.push_back(findClosestZoneAndPoint(ConvertPhyCoordToMapCoord(wifiTrack.track[t].posPhy)));
            //continue;
        }

        vector<double> estimatedFinalProb_curr;     // -- For each current (i.e,. at t) candidate state, j, this keeps the probability of reaching to this candidate with the shortest path (which gives the highest probability among other possible paths)

        // -- If t == 0, then initialize the probability only with the measurement probability
        if(t == 0) {

            for(uint j=0; j<candList_curr.size(); j++) {
                double maxProb_j = GetEmissionProb(ConvertPhyCoordToMapCoord(wifiTrack.track[t].posPhy), get<1>(candList_curr[j]));

                if(DEBUG_LEVEL_VITERBI > 2)
                    printf("t = %d,  Cand[%d] MeasProb = %f\n", (int)t, (int)j, maxProb_j);

                estimatedFinalProb_curr.push_back(maxProb_j);
            }

        } else {

            // -- For each candidate states
            for(uint j=0; j<candList_curr.size(); j++) {
                tuple<Zone_t*, Point2f, Point> bestCand_prev = make_tuple((Zone_t*)NULL, Point2f(-1,-1), Point(-1,-1));
                double maxProb_j = 0;

                if(DEBUG_LEVEL_VITERBI > 2)
                    printf("t = %d,  candList_curr[%d] \n", (int)t, (int)j);

                if(candList_prev.size() > 0) {
                        for(uint k=0; k<candList_prev.size(); k++) {

                            // -- Compute the probability of reaching this candidate state, j, from a previous candidate state, k
                            double p_prev = estimatedFinalProb_prev[k];                                            // -- Estimated Final Prob at t-1

                            Point2f A = ConvertPhyCoordToMapCoord(wifiTrack.track[t-1].posPhy);
                            Point2f B = ConvertPhyCoordToMapCoord(wifiTrack.track[t].posPhy);
                            Point I = ConvertStoreMapPtToIndexMapPt(get<1>(candList_prev[k]));
                            Point J = ConvertStoreMapPtToIndexMapPt(get<1>(candList_curr[j]));
                            double p_trans = GetTransitionProb(A, B, I, J);   // -- Transition Prob from t-1 to t

                            double p_emis = GetEmissionProb(ConvertPhyCoordToMapCoord(wifiTrack.track[t].posPhy), get<1>(candList_curr[j]));                                         // -- Measurement Prob at t

                            double p_temp = p_prev * p_trans * p_emis;

                            p_temp *= 1000;        // -- Not to underflow

                            // -- Find the best previous (i.e., at t-1) candidate state, k, that gives the shortest path (i.e., yields the highest probability of reaching this candidate state, j)
                            if(p_temp > maxProb_j) {
                                maxProb_j = p_temp;
                                bestCand_prev = candList_prev[k];

                                if(DEBUG_LEVEL_VITERBI > 2)
                                    printf("    --  candList_prev[%d]: maxProb_j = %f \n", (int)k, maxProb_j);
                            }

                        }   // -- End for(k =

                        if(DEBUG_LEVEL_VITERBI > 2)
                            printf("    ==> Prob = %f and Best Cand = (%.0f, %0.f)\n", maxProb_j, get<1>(bestCand_prev).x, get<1>(bestCand_prev).y);

                    estimatedFinalProb_curr.push_back(maxProb_j);
                    bestCandList.push_back(bestCand_prev);
                } // -- End if(candList_prev.size() > 0)
                else {
                    if(DEBUG_LEVEL_VITERBI > 1)
                        printf("#### (t, j) = (%d,%d): candList_prev.size() = 0 !!!!!! \n", (int)t, (int)j);
                }
            }   // -- End for(j =


            // -- Find the best candidate in this round (i.e., at t)
            {
                double maxProb1 = 0;
                tuple<Zone_t*, Point2f, Point> bestCand = make_tuple((Zone_t*)NULL, Point2f(-1,-1), Point(-1,-1));
                int bestJ = -1;
                for(uint j=0; j<estimatedFinalProb_curr.size(); j++) {
                    if(estimatedFinalProb_curr[j] > maxProb1) {
                        maxProb1 = estimatedFinalProb_curr[j];
                        bestCand = bestCandList[j];
                        bestJ = j;
                    }
                }

                if(bestJ >= 0) {
                    wifiTrack.track[t-1].zone = get<0>(bestCand);       // -- Corresponding Zone
                    wifiTrack.track[t-1].posStore = get<1>(bestCand);   // -- the coordinates of the corresponding state in StoreMap coordinate system
                    wifiTrack.track[t-1].posIndex = get<2>(bestCand);   // -- the coordinates of the corresponding state in IndexMap coordinate system

                    // -- DEBUG
                    if(DEBUG_LEVEL_VITERBI > 1)
                        if(wifiTrack.track[t-1].posIndex.x == -1 && wifiTrack.track[t-1].posIndex.y == -1) {
                            printf("wifiTrack.track[%d]: posIndex NOT ASSIGNED\n", t-1);
                        }


                    // -- For the last tracked location, add to the route list
                    if(t == wifiTrack.track.size()-1) {
                        wifiTrack.track[t].zone = get<0>(candList_curr[bestJ]);         // -- Corresponding Zone
                        wifiTrack.track[t].posStore = get<1>(candList_curr[bestJ]);     // -- the coordinates of the corresponding state in StoreMap coordinate system
                        wifiTrack.track[t].posIndex = get<2>(candList_curr[bestJ]);     // -- the coordinates of the corresponding state in IndexMap coordinate system

                        // -- DEBUG
                        if(DEBUG_LEVEL_VITERBI > 1)
                            if(wifiTrack.track[t].posIndex.x == -1 && wifiTrack.track[t].posIndex.y == -1) {
                                printf("wifiTrack.track[%d]: posIndex NOT ASSIGNED\n", t);
                            }
                    }
                } else {

                    // -- DEBUG
                    if(DEBUG_LEVEL_VITERBI > 1)
                        printf("@@@ bestJ < 0, when t=%d\n", t);

                }
            }

        } // -- End if(t == 0)

        estimatedFinalProb_prev = estimatedFinalProb_curr;
        candList_prev = candList_curr;

    } // -- End for(t =

    return SUCCESSFUL;
}

// -- Given a route path from running a Viterbi algorithm, reconstruct the entire route path by interpolating in-between states using A* algorithm
WiFiErrorType_t WiFiPostProcessor::ReconstructRoutePath_StoreMap(WiFiTrack_t &wifiTrack) {

    //vector<tuple<Zone_t*, Point2f, Point> > totalShoppingRoute;

    if(wifiTrack.track.size() <= 1)
        return TOO_SMALL_TRACK_POINT;

    if(DEBUG_LEVEL_RECONSTRUCT > 2)
        printf("ReconstructRoutePath_StoreMap(): \n");

    // -- Reset the storage
    wifiTrack.refinedTrack.clear();

    // -- Given a ViterbiRoutePath, reconstruct the whole shopping path with interpolation
    for(uint i=1; i<wifiTrack.track.size(); i++) {

        // -- Get the route from state A to state B using A* algorithm
        SquareGrid::Location start{wifiTrack.track[i-1].posIndex.x, wifiTrack.track[i-1].posIndex.y};
        SquareGrid::Location goal{wifiTrack.track[i].posIndex.x, wifiTrack.track[i].posIndex.y};

        // -- TODO: This is not supposed to be needed, but whenever I or J is (0,0), it would cause a huge memory usage increase. Need to be revisited.
        if(get<0>(goal) <= 0 || get<1>(goal) <= 0)
            continue;

        double timeDurationInSegment = wifiTrack.track[i].localtime - wifiTrack.track[i-1].localtime;

        unordered_map<SquareGrid::Location, SquareGrid::Location> came_from;
        unordered_map<SquareGrid::Location, double> cost_so_far;

        // -- Perform A* search
        a_star_search(*mAstarMap, start, goal, came_from, cost_so_far);

        // -- Show the found shortest path in the command line
        vector<SquareGrid::Location> pathInSegment_IndexMap = reconstruct_path(start, goal, came_from);

        // -- Convert to Store Map coordinates
        for(uint j=0; j<pathInSegment_IndexMap.size(); j++) {

            WiFiPostData_t newPt = wifiTrack.track[i-1];

            newPt.localtime += timeDurationInSegment * j / (double)pathInSegment_IndexMap.size();
            newPt.posIndex = Point(get<0>(pathInSegment_IndexMap[j]), get<1>(pathInSegment_IndexMap[j]));
            newPt.posStore = ConvertIndexMapPtToStoreMapPt(newPt.posIndex);
            newPt.posPhy = ConvertMapCoordToPhyCoord(newPt.posStore);

            // -- Check if this pouint is redundant
            if(wifiTrack.refinedTrack.size() > 0) {
                Point2f lastPoint = (wifiTrack.refinedTrack.end()-1)->posStore;

                if(newPt.posStore != lastPoint)
                    wifiTrack.refinedTrack.push_back(newPt);
            } else {
                wifiTrack.refinedTrack.push_back(newPt);
            }

            if(DEBUG_LEVEL_RECONSTRUCT > 2)
                printf("     --> (%.0f, %.0f)\n", newPt.posStore.x, newPt.posStore.y);
        }

    }

    return SUCCESSFUL;

}

// -- Given a reconstructed path, add some random variation on each state to look realistic
WiFiErrorType_t WiFiPostProcessor::AddRandomNoiseToRoutePath_StoreMap(WiFiTrack_t &wifiTrack) {
    double randomNoiseRadius = ConvertPhyDistanceToMapDistance(mRandomnessRadius); // -- Unit: [pixel]

    //vector<tuple<Zone_t*, Point2f, Point> > reconstPath_w_noise = reconstPath;

    if(wifiTrack.refinedTrack.size() == 0)
        return TOO_SMALL_TRACK_POINT;

    for(uint i=0; i<wifiTrack.refinedTrack.size(); i++) {

        Point2f pt, pt_half;

        // -- The resulting point should be inside one of the valid zones
        bool isInsideAnyZone = false;
        int tryCnt = 0;
        do {

            double rx = ((rand()%(int(randomNoiseRadius*100*2.f))) - randomNoiseRadius*100)/100.f;
            double ry = ((rand()%(int(randomNoiseRadius*100*2.f))) - randomNoiseRadius*100)/100.f;

            pt = Point2f(wifiTrack.refinedTrack[i].posStore.x + rx, wifiTrack.refinedTrack[i].posStore.y + ry);
            pt_half = Point2f(wifiTrack.refinedTrack[i].posStore.x + rx*0.5, wifiTrack.refinedTrack[i].posStore.y + ry*0.5);

//            pt = wifiTrack.refinedTrack[i].posStore + Point2f(rx,ry);
//            pt_half = wifiTrack.refinedTrack[i].posStore + Point2f(rx*0.5,ry*0.5);
            //pt = Point2f(get<1>(reconstPath[i]).x + rx, get<1>(reconstPath_w_noise[i]).y + ry);
            //pt_half = Point2f(get<1>(reconstPath[i]).x + rx*0.5, get<1>(reconstPath_w_noise[i]).y + ry*0.5);    // -- In case that the previous point and the next resulting random point are in a zone, the line connecting the two points may cross a hole or non-zone.


            for( uint i = 0; i< mZones.size(); i++ ) {
                double dist = pointPolygonTest(mZones[i].contourPts, pt, true);
                double dist_half = pointPolygonTest(mZones[i].contourPts, pt_half, true);
                bool isInsideZone = dist >= 0 && dist_half >= 0;
                bool isInsideHoles = false;

                for( uint j = 0; j< mHoles.size(); j++ ) {
                    // -- Check only the holes belong to the parent contour i
                    if( i!= j && mHoles[j].contourHierarchy[3] == mZones[i].contourIndex) {
                        if(pointPolygonTest(mHoles[j].contourPts, pt, false) > 0 || pointPolygonTest(mHoles[j].contourPts, pt_half, false) > 0) {
                            isInsideHoles = true;
                            break;
                        }
                    }
                }

                // -- If inside the polygon and not in one of the holes, then assign the state to a zone
                if(isInsideZone == true && isInsideHoles == false) {
                    isInsideAnyZone = true;
                    break;
                }
            }

            tryCnt++;
        } while(!(isInsideAnyZone == true || tryCnt >= ADD_RANDOMNESS_MAX_TRY_COUNT));

        //get<1>(reconstPath_w_noise[i]) = pt;
        wifiTrack.refinedTrack[i].posStore = pt;

    }

    return SUCCESSFUL;
}

void WiFiPostProcessor::DrawMap() {

    if(mShowRawZoneMap) {
        mImg_map_init_zone.copyTo(mImg_map);
    } else {
        mImg_map_init.copyTo(mImg_map);
    }

    // -- Draw contours
    if(mShowZoneBoundary) {
        for( uint i = 0; i< mZones.size(); i++ ) {
            drawContours(mImg_map, vector<vector<Point> >(1, mZones[i].contourPts), 0, CV_RGB(mZones[i].color.val[0], mZones[i].color.val[1], mZones[i].color.val[2]), 3, 8, vector<Vec4i>(1,mZones[i].contourHierarchy), 0, Point() );

            // -- Draw hole boundaries with the same color of the zone that the holes belong to
            for( uint j = 0; j< mHoles.size(); j++ ) {
                if(mHoles[j].contourHierarchy[3] == mZones[i].contourIndex)
                    drawContours(mImg_map, vector<vector<Point> >(1, mHoles[j].contourPts), 0, CV_RGB(mZones[i].color.val[0], mZones[i].color.val[1], mZones[i].color.val[2]), 3, 8, vector<Vec4i>(1,mHoles[j].contourHierarchy), 0, Point() );
            }

            // -- Mark the zone number
            {
                char text[99];
                sprintf(text, "[%d]", (int)i);
                putText(mImg_map, text, Point(mZones[i].contourCenter.x - 10, mZones[i].contourCenter.y + 10), 0, 0.4*mFontSizeScale,CV_RGB(255,0,0), 1, CV_AA);
            }


        }
    }

    // -- Show sensor locations
    if(mShowSensors)
        if(mRunningWiFiSensor.size() > 0)
            for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it) {
                WiFiSensor_short_Ptr ap = *it;

                // -- Show sensors only if the sensor is not working correctly
                show_sensors(ap, mImg_map, mFontSizeScale);
            }

    if(mShowAstarWalls && mAstarMap != NULL) {

        for (int y = 0; y != mAstarMap->height; ++y) {
          for (int x = 0; x != mAstarMap->width; ++x) {
            SquareGrid::Location id {x, y};

            if (mAstarMap->walls.count(id)) {
                circle(mImg_map, ConvertAstarMapPtToStoreMapPt(id), 4, CV_RGB(0,0,0), 1, 16,0);
            }

          }
        }
    }

    // -- Show the connectivity among the HMM states (i.e., edges among vertices)
    if(mShowHmmStateConnectivity && mAstarMap != NULL) {

        for (int y = 0; y != mAstarMap->height; ++y) {
          for (int x = 0; x != mAstarMap->width; ++x) {
            SquareGrid::Location id {x, y};

            // -- Consider only valid states
            if(mAstarMap->walls.count(id))
                continue;

            vector<SquareGrid::Location> neighbors = mAstarMap->neighbors(id);
            for(uint i=0; i<neighbors.size(); i++)
                line(mImg_map, ConvertAstarMapPtToStoreMapPt(id), ConvertAstarMapPtToStoreMapPt(neighbors[i]), CV_RGB(0,0,255),1,8,0);

          }
        }

    }

    // -- Show HMM States
    if(mShowHmmStates && mAstarMap != NULL) {

        // -- Show all the potential state locations
        if(0) {
            printf("mStateMap.size() = %d\n", (int)mStateMap.size());
            for(uint i = 0; i< mStateMap.size(); i++) {
                printf("-- mStateMap[%d].size() = %d\n", (int)i, (int)mStateMap[i].size());
                for(uint j = 0; j< mStateMap[i].size(); j++) {
                    printf("(%d,%d) ", (int)mStateMap[i][j].x, (int)mStateMap[i][j].y);
                    circle(mImg_map, Point(mStateMap[i][j].x, mStateMap[i][j].y), 4, CV_RGB(200,100,100), 1, 16,0);
                }
                printf("\n");
            }
            printf("\n");
        }

        // -- Show all the valid states
        for( uint i = 0; i< mZones.size(); i++ ) {
            for(uint j=0; j<mZones[i].states.size(); j++)
                circle(mImg_map, Point(mZones[i].states[j].first.x, mZones[i].states[j].first.y), 4, mZones[i].color, CV_FILLED, 16,0);
        }
    }

    // -- Draw contour centers
    if(mShowContourConnectivity) {
        for(uint i = 0; i < mZones.size(); i++) {
            circle(mImg_map,Point(mZones[i].contourCenter.x, mZones[i].contourCenter.y), 3, CV_RGB(255,0,0), CV_FILLED, 16,0);
        }

        // -- Show connectivity among zones
        {           
            for(uint i=0; i<mZones.size()-1; ++i) {
                for(uint j=i+1; j<mZones.size(); ++j) {
                    if(mZoneAdjacency[i][j] == true) {
                        line(mImg_map, mZones[i].contourCenter, mZones[j].contourCenter, CV_RGB(0,0,255),2,16,0);
                    }
                }
            }
        }
    }

    // -- DEBUG for HMM Testing
    if(mShowSampleAstarPath && mAstarMap != NULL) {
        SquareGrid::Location start{mZones[mSampleAstarPath_startZone].states[0].second.x, mZones[mSampleAstarPath_startZone].states[0].second.y};
        SquareGrid::Location goal{mZones[mSampleAstarPath_endZone].states[0].second.x, mZones[mSampleAstarPath_endZone].states[0].second.y};

        unordered_map<SquareGrid::Location, SquareGrid::Location> came_from;
        unordered_map<SquareGrid::Location, double> cost_so_far;

        // -- Check if the map conversion works fine
        if(0) {
            Point pt_goal = ConvertAstarMapPtToStoreMapPt(goal);
            printf("Index Map Pt(%d, %d) --> Store Map Pt(%d, %d)", get<0>(goal), get<1>(goal), pt_goal.x, pt_goal.y);
            SquareGrid::Location pt_goal_back = ConvertStoreMapPtToAstarMapPt(pt_goal);
            printf(" ==> Index Map Pt(%d, %d)\n", get<0>(pt_goal_back), get<1>(pt_goal_back));
        }

        // -- Perform A* search
        a_star_search(*mAstarMap, start, goal, came_from, cost_so_far);

        // -- Show movement direction map in the command line
        draw_grid(*mAstarMap, 2, nullptr, &came_from);
        std::cout << std::endl;

        // -- Show movement cost map in the command line
        draw_grid(*mAstarMap, 3, &cost_so_far, nullptr);
        std::cout << std::endl;

        // -- Show the found shortest path in the command line
        vector<SquareGrid::Location> path = reconstruct_path(start, goal, came_from);
        draw_grid(*mAstarMap, 3, nullptr, nullptr, &path);

        // -- Draw the found shortest path on the store map
        {
            SquareGrid::Location pt_prev, pt_curr;
            pt_prev = path[0];
            for(uint i=0; i<path.size(); i++) {
                pt_curr = path[i];

                line(mImg_map, ConvertAstarMapPtToStoreMapPt(pt_prev), ConvertAstarMapPtToStoreMapPt(pt_curr), CV_RGB(255,0,255),5,16,0);

                pt_prev = pt_curr;
            }

            // -- Mark the goal
            circle(mImg_map,ConvertAstarMapPtToStoreMapPt(goal), 9, CV_RGB(155,0,0), 3, 16,0);
        }
    }

    if(mShowSampleClosestZoneAndState && mAstarMap != NULL) {

        // -- Generate a sample location in store map image coordinate
        mSampleLocation1 = Point(rand()%mStoreWidth_IMG_x, rand()%mStoreWidth_IMG_y);

        if(DEBUG_LEVEL > 0)
            printf("findClosestZoneAndPoint(): Sample point = (%d, %d)\n", mSampleLocation1.x, mSampleLocation1.y);

        // -- Given a location, find the closest zone and state in the zone
        tuple<Zone_t*, Point2f, Point> closestState = findClosestZoneAndPoint(Point2f(mSampleLocation1));

        if(get<0>(closestState) != NULL) {
            if(DEBUG_LEVEL > 0)
                printf("--> findClosestZoneAndPoint(): Closest state = (%d, %d)\n", (int)get<1>(closestState).x, (int)get<1>(closestState).y);

            line(mImg_map, mSampleLocation1, get<1>(closestState), CV_RGB(0,255,0),7,16,0);
            circle(mImg_map, mSampleLocation1, 12, CV_RGB(155,0,0), 3, 16,0);

        } else {
            if(DEBUG_LEVEL > 0)
                printf("--> findClosestZoneAndPoint(): CANNOT find closest zone nor state!! \n");
        }
    }

    if(mShowSampleInitialTrack) {
        // -- Draw the track
        DrawTrack(&mSampleWifiTrack);
    }

    if(mShowSampleTrackOffset) {
        // -- Draw the track
        DrawTrack(&mSampleWifiTrack_AfterOffsetting, true);
    }

    if(mShowSampleTrackZoomed) {
        // -- Draw the track
        DrawTrack(&mSampleWifiTrack_AfterZooming, true);
    }

    if(mShowSampleFilteredTrack) {
        // -- Draw the track
        DrawTrack(&mSampleWifiTrack_AfterFiltering, true);
    }


    if(mShowSampleViterbiPath && mAstarMap != NULL) {

        // -- Draw the route path
        DrawViterbiPath(&mSampleWifiTrack_AfterViterbi);

    } // -- End if(mShowSampleViterbiPath && mAstarMap != NULL)


    if(mShowSampleReconstructedPath && mAstarMap != NULL) {

        // -- Draw the total shopping path
        DrawReconstructedPath(&mSampleWifiTrack_AfterReconstruction);
    }

    if(mShowSampleRealisticPath && mAstarMap != NULL) {

        // -- Draw the total shopping path that are made realistic
        DrawReconstructedPath(&mSampleWifiTrack_AfterMakingRealistic);

    }


    if(mShowInitialRawTrack) {

        // -- Show all the tracks
        if(mSelectedTrackIndex < 0) {
            for(deque<WiFiTrack_t >::iterator it = mWifiTracks.begin(); it != mWifiTracks.end(); ++it) {
                WiFiTrack_t *track = &*it;
                DrawTrack(track);
            }
        }
        // -- Show one track
        else if(mSelectedTrackIndex < (int)mWifiTracks.size()) {
            DrawTrack(&mWifiTracks[mSelectedTrackIndex]);
        }
    }

    if(mShowTrackOffset) {

        // -- Show all the tracks
        if(mSelectedTrackIndex < 0) {
            for(deque<WiFiTrack_t >::iterator it = mWifiTracks.begin(); it != mWifiTracks.end(); ++it) {
                WiFiTrack_t *track = &*it;
                DrawTrack(track, true);
            }
        }
        // -- Show one track
        else if(mSelectedTrackIndex < (int)mWifiTracks.size()) {
            DrawTrack(&mWifiTrackSelected_AfterOffsetting, true);
        }
    }


    if(mShowTrackZoomed) {
        if(mSelectedTrackIndex < 0) {
            for(deque<WiFiTrack_t >::iterator it = mWifiTracks.begin(); it != mWifiTracks.end(); ++it) {
                WiFiTrack_t *track = &*it;
                DrawTrack(track, true);
            }
        } else if(mSelectedTrackIndex < (int)mWifiTracks.size()) {
            // -- Draw one filtered track
            DrawTrack(&mWifiTrackSelected_AfterZooming, true);
        }
    }

    if(mShowFilteredTrack_PerTrack && mAstarMap != NULL) {
        if(mSelectedTrackIndex < 0) {
            for(deque<WiFiTrack_t >::iterator it = mWifiTracks.begin(); it != mWifiTracks.end(); ++it) {
                WiFiTrack_t *track = &*it;
                DrawTrack(track, true);
            }
        } else if(mSelectedTrackIndex < (int)mWifiTracks.size()) {
            // -- Draw one filtered track
            DrawTrack(&mWifiTrackSelected_AfterFiltering, true);
        }
    }

    if(mShowViterbiPath_PerTrack && mAstarMap != NULL) {

        if(mSelectedTrackIndex < 0) {
            for(deque<WiFiTrack_t >::iterator it = mWifiTracks.begin(); it != mWifiTracks.end(); ++it) {
                WiFiTrack_t *track = &*it;
                // -- Draw the route path
                DrawViterbiPath(track);
            }
        } else if(mSelectedTrackIndex < (int)mWifiTracks.size()) {
            // -- Draw the route path
            DrawViterbiPath(&mWifiTrackSelected_AfterViterbi);
        }

    }

    if(mShowReconstructedPath_PerTrack && mAstarMap != NULL) {

        if(mSelectedTrackIndex < 0) {
            for(deque<WiFiTrack_t >::iterator it = mWifiTracks.begin(); it != mWifiTracks.end(); ++it) {
                WiFiTrack_t *track = &*it;
                // -- Draw the total shopping path
                DrawReconstructedPath(track);
            }
        } else if(mSelectedTrackIndex < (int)mWifiTracks.size()) {
            // -- Draw the routh path
            DrawReconstructedPath(&mWifiTrackSelected_AfterReconstruction);

        }
    }

    if(mShowRealisticPath_PerTrack && mAstarMap != NULL) {

        // -- Draw the total shopping path that are made realistic
        if(mSelectedTrackIndex < 0) {
            for(deque<WiFiTrack_t >::iterator it = mWifiTracks.begin(); it != mWifiTracks.end(); ++it) {
                WiFiTrack_t *track = &*it;
                // -- Draw the total shopping path
                DrawReconstructedPath(track);
            }
        } else if(mSelectedTrackIndex < (int)mWifiTracks.size()) {
            // -- Draw the routh path
            DrawReconstructedPath(&mWifiTrackSelected_AfterMakingRealistic);

        }
    }


    // -- TEMP
    circle(mImg_map, ConvertPhyCoordToMapCoord(mDoorLocation), 12, CV_RGB(255,0,100), -1, 16,0);


    mImg_map.copyTo(mImg_map_final);
}

void WiFiPostProcessor::DrawViterbiPath(WiFiTrack_t *wifiTrack) {

    if(wifiTrack->track.size() == 0)
        return;

    // -- Draw the routh path
    for(uint t=0; t<wifiTrack->track.size(); t++) {
        circle(mImg_map, wifiTrack->track[t].posStore, 12, CV_RGB(0,155,100), 3, 16,0);

        // -- Draw correspondences
        line(mImg_map, wifiTrack->track[t].posStore, ConvertPhyCoordToMapCoord(wifiTrack->track[t].posPhy), CV_RGB(0,0,255),2,16,0);
    }

    // -- Mark the last one
    circle(mImg_map, wifiTrack->track[wifiTrack->track.size()-1].posStore, 9, CV_RGB(155,100,0), 3, 16,0);
}

void WiFiPostProcessor::DrawReconstructedPath(WiFiTrack_t *wifiTrack) {

    if(wifiTrack->refinedTrack.size() == 0)
        return;

    for(uint i=1; i<wifiTrack->refinedTrack.size(); i++) {
        line(mImg_map, wifiTrack->refinedTrack[i-1].posStore, wifiTrack->refinedTrack[i].posStore, CV_RGB(0,255,0),2,16,0);
    }

    for(uint i=0; i<wifiTrack->refinedTrack.size(); i++) {
        circle(mImg_map, wifiTrack->refinedTrack[i].posStore, 5, CV_RGB(0,155,100), 2, 16,0);
    }
    // -- Mark the last one
    circle(mImg_map, wifiTrack->refinedTrack[wifiTrack->refinedTrack.size()-1].posStore, 4, CV_RGB(255,0,0), CV_FILLED, 16,0);
}

void WiFiPostProcessor::PerformPostProcessing_Sample() {
    int errorType = -99;

    ResetPostProcessing_Sample();

    mSampleWifiTrack_AfterOffsetting = mSampleWifiTrack;

    // -- Add offset to the trajectory to compensate any systematic bias
    if((errorType = AddOffsetInTrajectory(mSampleWifiTrack_AfterOffsetting)) < 0) {
        if(DEBUG_LEVEL > 0)
            cout << "### AddOffsetInTrajectory(): Error type = " << errorType << endl;
        return;
    }

    mSampleWifiTrack_AfterZooming = mSampleWifiTrack_AfterOffsetting;


    // -- Zoom the trajectory to compensate any systematic bias
    if((errorType = AddZoomInTrajectory(mSampleWifiTrack_AfterZooming)) < 0) {
        if(DEBUG_LEVEL > 0)
            cout << "### AddZoomInTrajectory(): Error type = " << errorType << endl;
        return;
    }

    mSampleWifiTrack_AfterFiltering = mSampleWifiTrack_AfterZooming;

    // -- Filter the trajectory based on the physical constraints of shopper movement
    if((errorType = FilterRawTrajectory(mSampleWifiTrack_AfterFiltering)) < 0) {
        if(DEBUG_LEVEL > 0)
            cout << "### FilterRawTrajectory(): Error type = " << errorType << endl;
        return;
    }

    mSampleWifiTrack_AfterViterbi = mSampleWifiTrack_AfterFiltering;

    // -- Find the most probable shopping path
    if((errorType = FindBestRoute_Viterbi(mSampleWifiTrack_AfterViterbi)) < 0) {
        if(DEBUG_LEVEL > 0)
            cout << "### FindBestRoute_Viterbi(): Error type = " << errorType << endl;
        return;
    }

    mSampleWifiTrack_AfterReconstruction = mSampleWifiTrack_AfterViterbi;

    // -- Given a Viterbi path, further reconstruct the entire path by interpolating each segment of path using A* algorithm
    if((errorType = ReconstructRoutePath_StoreMap(mSampleWifiTrack_AfterReconstruction)) < 0) {
        if(DEBUG_LEVEL > 0)
            cout << "### ReconstructRoutePath_StoreMap(): Error type = " << errorType << endl;
        return;
    }

    mSampleWifiTrack_AfterMakingRealistic = mSampleWifiTrack_AfterReconstruction;

    // -- Given a reconstructed path, add some random variation on each state to look realistic
    if((errorType = AddRandomNoiseToRoutePath_StoreMap(mSampleWifiTrack_AfterMakingRealistic)) < 0) {
        if(DEBUG_LEVEL > 0)
            cout << "### AddRandomNoiseToRoutePath_StoreMap(): Error type = " << errorType << endl;
        return;
    }

}

bool WiFiPostProcessor::PerformPostProcessing(int trackIndex) {
    int errorType;

    if(trackIndex >= 0 && trackIndex < (int)mWifiTracks.size()) {


        // -- Add offset to the trajectory to compensate any systematic bias
        if((errorType = AddOffsetInTrajectory(mWifiTracks[trackIndex])) < 0) {
            if(DEBUG_LEVEL > 0)
                cout << "### AddOffsetInTrajectory(): Error type = " << errorType << endl;
            return false;
        }

        // -- Zoom the trajectory to compensate any systematic bias
        if((errorType = AddZoomInTrajectory(mWifiTracks[trackIndex])) < 0) {
            if(DEBUG_LEVEL > 0)
                cout << "### AddZoomInTrajectory(): Error type = " << errorType << endl;
            return false;
        }

        // -- Filter the trajectory based on the physical constraints of shopper movement
        if((errorType = FilterRawTrajectory(mWifiTracks[trackIndex])) < 0) {
            if(DEBUG_LEVEL > 0)
                cout << "### FilterRawTrajectory(): Error type = " << errorType << endl;
            return false;
        }

        // -- Find the most probable shopping path
        if((errorType = FindBestRoute_Viterbi(mWifiTracks[trackIndex])) < 0) {
            if(DEBUG_LEVEL > 0)
                cout << "### FindBestRoute_Viterbi(): Error type = " << errorType << endl;
            return false;
        }

        // -- Given a Viterbi path, further reconstruct the entire path by interpolating each segment of path using A* algorithm
        if((errorType = ReconstructRoutePath_StoreMap(mWifiTracks[trackIndex])) < 0) {
            if(DEBUG_LEVEL > 0)
                cout << "### ReconstructRoutePath_StoreMap(): Error type = " << errorType << endl;
            return false;
        }

        // -- Given a reconstructed path, add some random variation on each state to look realistic
        if((errorType = AddRandomNoiseToRoutePath_StoreMap(mWifiTracks[trackIndex])) < 0) {
            if(DEBUG_LEVEL > 0)
                cout << "### AddRandomNoiseToRoutePath_StoreMap(): Error type = " << errorType << endl;
            return false;
        }
    }

     return true;}

bool WiFiPostProcessor::PerformPostProcessing_Logging(int trackIndex) {
    int errorType;

    if(trackIndex >= 0 && trackIndex < (int)mWifiTracks.size()) {

        mWifiTrackSelected_AfterOffsetting = mWifiTracks[trackIndex];

        // -- Add offset to the trajectory to compensate any systematic bias
        if((errorType = AddOffsetInTrajectory(mWifiTrackSelected_AfterOffsetting)) < 0) {
            if(DEBUG_LEVEL > 0)
                cout << "### AddOffsetInTrajectory(): Error type = " << errorType << endl;
            return false;
        }

        mWifiTrackSelected_AfterZooming = mWifiTrackSelected_AfterOffsetting;

        // -- Zoom the trajectory to compensate any systematic bias
        if((errorType = AddZoomInTrajectory(mWifiTrackSelected_AfterZooming)) < 0) {
            if(DEBUG_LEVEL > 0)
                cout << "### ZoomRawTrajectory(): Error type = " << errorType << endl;
            return false;
        }

        mWifiTrackSelected_AfterFiltering = mWifiTrackSelected_AfterZooming;

        // -- Filter the trajectory based on the physical constraints of shopper movement
        if((errorType = FilterRawTrajectory(mWifiTrackSelected_AfterFiltering)) < 0) {
            if(DEBUG_LEVEL > 0)
                cout << "### FilterRawTrajectory(): Error type = " << errorType << endl;
            return false;
        }

        mWifiTrackSelected_AfterViterbi = mWifiTrackSelected_AfterFiltering;

        // -- Find the most probable shopping path
        if((errorType = FindBestRoute_Viterbi(mWifiTrackSelected_AfterViterbi)) < 0) {
            if(DEBUG_LEVEL > 0)
                cout << "### FindBestRoute_Viterbi(): Error type = " << errorType << endl;
            return false;
        }

        mWifiTrackSelected_AfterReconstruction = mWifiTrackSelected_AfterViterbi;

        // -- Given a Viterbi path, further reconstruct the entire path by interpolating each segment of path using A* algorithm
        if((errorType = ReconstructRoutePath_StoreMap(mWifiTrackSelected_AfterReconstruction)) < 0) {
            if(DEBUG_LEVEL > 0)
                cout << "### ReconstructRoutePath_StoreMap(): Error type = " << errorType << endl;
            return false;
        }

        mWifiTrackSelected_AfterMakingRealistic = mWifiTrackSelected_AfterReconstruction;

        // -- Given a reconstructed path, add some random variation on each state to look realistic
        if((errorType = AddRandomNoiseToRoutePath_StoreMap(mWifiTrackSelected_AfterMakingRealistic)) < 0) {
            if(DEBUG_LEVEL > 0)
                cout << "### AddRandomNoiseToRoutePath_StoreMap(): Error type = " << errorType << endl;
            return false;
        }
    }

    return true;
}

void WiFiPostProcessor::ResetPostProcessing_Sample() {
    mSampleWifiTrack_AfterFiltering = mSampleWifiTrack;
    mSampleWifiTrack_AfterViterbi = mSampleWifiTrack;
    mSampleWifiTrack_AfterReconstruction = mSampleWifiTrack;
    mSampleWifiTrack_AfterMakingRealistic = mSampleWifiTrack;
}

void WiFiPostProcessor::ResetPostProcessing(int ind) {
    if(ind >= 0 && ind < (int)mWifiTracks.size()) {
        mWifiTrackSelected_AfterFiltering = mWifiTracks[ind];
        mWifiTrackSelected_AfterViterbi = mWifiTracks[ind];
        mWifiTrackSelected_AfterReconstruction = mWifiTracks[ind];
        mWifiTrackSelected_AfterMakingRealistic = mWifiTracks[ind];
    }
}

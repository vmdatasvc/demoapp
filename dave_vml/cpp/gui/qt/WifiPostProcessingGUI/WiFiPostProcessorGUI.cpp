/*
 * WifiPostProcessingGUI.cpp
 *
 *  Created on: Nov 28, 2016
 *      Author: paulshin
 */

/* Standard C++ includes */
#include <stdlib.h>
#include <iostream>
#include <unistd.h>

/*
  Include directly the different
  headers from cppconn/ and mysql_driver.h + mysql_util.h
  (and mysql_connection.h). This will reduce your build time!
*/
#include "mysql_connection.h"
    #include <cppconn/driver.h>
    #include <cppconn/exception.h>
    #include <cppconn/resultset.h>
    #include <cppconn/statement.h>

#include "modules/utility/TimeKeeper.hpp"
#include "modules/utility/LocalClock.hpp"

#include "../WifiPostProcessingGUI/WiFiPostProcessorGUI.h"
#include "ui_WifiPostProcessingGUI.h"

// -- Refer to https://github.com/nlohmann/json
#include "json.hpp"
using json = nlohmann::json;

#include "Astar.hpp"

using namespace std;


// -- Reference: https://john.nachtimwald.com/2015/05/02/effective-threading-using-qt/

WifiPostProcessingGUI::WifiPostProcessingGUI(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::WifiPostProcessingGUI),
    mMapSize(-1,-1)
{
    mStoreMap = NULL;

    ui->setupUi(this);

    /// https://doc-snapshots.qt.io/4.8/qabstractspinbox.html#keyboardTracking-prop
    /// If keyboard tracking is enabled (the default), the spinbox emits the valueChanged() signal while the new value is being entered from the keyboard.
    /// E.g. when the user enters the value 600 by typing 6, 0, and 0, the spinbox emits 3 signals with the values 6, 60, and 600 respectively.
    /// If keyboard tracking is disabled, the spinbox doesn't emit the valueChanged() signal while typing. It emits the signal later, when the return key is pressed, when keyboard focus is lost, or when other spinbox functionality is used, e.g. pressing an arrow key.
    ui->spinBox_AdjacencyThreshold->setKeyboardTracking(false);
    ui->doubleSpinBox_Randomness->setKeyboardTracking(false);
    ui->spinBox_StateInterval->setKeyboardTracking(false);
    ui->SpinBox_EndZone->setKeyboardTracking(false);
    ui->SpinBox_StartZone->setKeyboardTracking(false);
    ui->SelectTrackSpinBox->setKeyboardTracking(false);

    InitializeGUI();

    // -- Display three buttons on the widget
    setWindowFlags(windowFlags() | Qt::Window
    | Qt::WindowMinimizeButtonHint
    | Qt::WindowMaximizeButtonHint
    | Qt::WindowCloseButtonHint);

    // -- Initialize the Store Name list
    {
        // -- Get the Schema (i.e., Database) list from AWS RDS
        vector<string> schemaList_Testbed = mDb.GetSchemaListFromAwsRds(true);
        vector<string> schemaList_Production = mDb.GetSchemaListFromAwsRds(false);

        // -- DEBUG
        if(1) {
            cout << " mDb.GetSchemaListFromAwsRds() for Testbed = " << endl;
            for(uint i=0; i<schemaList_Testbed.size(); i++)
                cout << schemaList_Testbed[i] << endl;

            cout << endl;

            cout << " mDb.GetSchemaListFromAwsRds() for Production = " << endl;
            for(uint i=0; i<schemaList_Production.size(); i++)
                cout << schemaList_Production[i] << endl;

            cout << endl;
        }

        // -- Set the Schema list to the dropdown menu
        for(uint i=0; i<schemaList_Testbed.size(); i++)
            ui->DropMenu_StoreName_Testbed->addItem(tr(schemaList_Testbed[i].c_str()));

        // -- Set the Schema list to the dropdown menu
        for(uint i=0; i<schemaList_Production.size(); i++)
            ui->DropMenu_StoreName_Production->addItem(tr(schemaList_Production[i].c_str()));
    }

    // -- Add tracker list, TODO: Load from a file
//    {
//        mTrackerList.clear();

//        mTrackerList.push_back("");
//        mTrackerList.push_back("vmhq-16801-trac001");
//        mTrackerList.push_back("giant-6072-trac001");
//        mTrackerList.push_back("petsns-231-trac001");
//        mTrackerList.push_back("petsns-342-trac001");
//        mTrackerList.push_back("carealot-001-trac001");

//        mTrackerList.push_back("carealot-001-omni001");
//        mTrackerList.push_back("carealot-001-omni004");
//        mTrackerList.push_back("carealot-001-omni005");

//        mTrackerList.push_back("petsns-231-fac001");
//        mTrackerList.push_back("petsns-231-fac003");
//        mTrackerList.push_back("petsns-231-fac004");

//        mTrackerList.push_back("petsns-342-fac001");
//        mTrackerList.push_back("petsns-342-fac002");
//        mTrackerList.push_back("petsns-342-fac003");

//        for(vector<string>::iterator it = mTrackerList.begin(); it != mTrackerList.end(); ++it) {
//            string trackerName = *it;
//            ui->TrackerDevice->addItem(tr(trackerName.c_str()));
//        }
//    }

}

WifiPostProcessingGUI::~WifiPostProcessingGUI()
{
    if(mStoreMap != NULL)
        delete mStoreMap;

    if(mDb.IsInitialized() == true) {
        mDb.DisconnectToLocalDB();
    }

    delete ui;
}

void WifiPostProcessingGUI::InitializeGUI() {

    // clean viewer
    ui->openCVViewer->clearImage();
    ui->DebugOutput->clear();
//    ui->DropMenu_StoreName_Production->clear();
//    ui->DropMenu_StoreName_Testbed->clear();
    ui->TrackerDevice->clear();

    mMapSize.width = -1;
    mMapSize.height = -1;

}

void WifiPostProcessingGUI::UpdateImage()
{
    //double currTime = gTimeSinceStart();        // -- Unit: [sec]

    // show the image
    cv::Mat image, image_resized, image_resized_colorCorrected;

    mStoreMap->GetMapImage().copyTo(image);

//    namedWindow("temp", CV_WINDOW_AUTOSIZE);
//    imshow("temp",image);

    // -- Resize the image while maintaining the aspect ratio
    if(mMapSize.height < 0 || mMapSize.width < 0) {
        // -- Calculate the aspect ratio of the original image
        float aspectRatio = image.size().width / (float)image.size().height;
        CvSize qtFrameSize((int)ui->openCVViewer->baseSize().width(), (int)ui->openCVViewer->baseSize().height());

        if(qtFrameSize.height*aspectRatio > qtFrameSize.width) {
            // -- The source image should be resized to the frame width
            mMapSize.width = qtFrameSize.width;
            mMapSize.height = qtFrameSize.width / aspectRatio;
        } else {
            // -- The source image should be resized to the frame height
            mMapSize.height = qtFrameSize.height;//    // -- Add tracker list, TODO: Load from a file

            mMapSize.width = qtFrameSize.height * aspectRatio;
        }

        if(mMapSize.height < 0 || mMapSize.width < 0)
            return;


        ui->openCVViewer->setFixedSize(QSize(mMapSize.width, mMapSize.height));
    }

    // -- Resize image to fit the OpenCV viewer
    cv::resize(image, image_resized, mMapSize);

    // -- Convert OpenCV-default BGR to QT-default RGB
    cvtColor(image_resized, image_resized_colorCorrected, CV_BGR2RGB);

    ui->openCVViewer->showImage(image_resized_colorCorrected);
}

// -- Change the font of the existing texts in the DebugOutput browser
// -- Refer to http://stackoverflow.com/questions/27716625/qtextedit-change-font-of-individual-paragraph-block
void WifiPostProcessingGUI::DebugOutputChangeFontSetting() {

    // For block management
    QTextDocument *doc = ui->DebugOutput->document();

    for(int i=0; i<doc->blockCount(); i++) {
        // Locate the 1st block
        QTextBlock block = doc->findBlockByNumber(i);

        // Initiate a copy of cursor on the block
        // Notice: it won't change any cursor behavior of the text editor, since it
        //         just another copy of cursor, and it's "invisible" from the editor.
        QTextCursor cursor(block);

        // Set background color
        QTextBlockFormat blockFormat = cursor.blockFormat();
        //blockFormat.setBackground(QColor(Qt::white));
        cursor.setBlockFormat(blockFormat);

        // Set font
        //for (QTextBlock::iterator it = cursor.block().begin(); !(it.atEnd()); ++it)
        for (QTextBlock::iterator it = cursor.block().begin(); it != cursor.block().end(); ++it)
        {
            QTextCharFormat charFormat = it.fragment().charFormat();
            charFormat.setFont(QFont("Courier", 8, QFont::Light));

            QTextCursor tempCursor = cursor;
            tempCursor.setPosition(it.fragment().position());
            tempCursor.setPosition(it.fragment().position() + it.fragment().length(), QTextCursor::KeepAnchor);
            tempCursor.setCharFormat(charFormat);
        }
    }

}

void WifiPostProcessingGUI::on_SyncButton_clicked()
{

    // -- Keep it pressed, and release it once the operation is done.
    ui->SyncButton->setChecked(true);

    if(mDb.IsInitialized() == false) {
        cout << "ERROR: DB is not Selected" << endl;

        // -- Release the button since the operation is done.
        ui->SyncButton->setChecked(false);

        return;
    }

    // -- Get the entire table from AWS RDS and import to the local DB
    mDb.BackupAwsRdsToLocalDB();

    if(mDb.IsInitialized() == true) {
        mDb.DisconnectToLocalDB();
    }

    // -- Connect to the local DB
    mDb.ConnectToLocalDB();

    // -- Retrieve the table info and update the display
    mDb.GetRawTrackTable();

    // -- Retrieve and parse the entire table
    mDb.GetAllRawTracks(mStoreMap->mWifiTracks);

    UpdateTableInfo();

    // -- Release the button since the operation is done.
    ui->SyncButton->setChecked(false);


}

void WifiPostProcessingGUI::on_TrackerDevice_currentTextChanged(const QString &arg1) {

//    if(ui->TrackerDevice->count() <= 1)
//        return;

    if(mDb.IsInitialized() == true) {
        mDb.DisconnectToLocalDB();
    }

    // -- Tracker Device Name (e.g., omni001)
    string deviceName = arg1.toStdString();

    if(deviceName.size() == 0)
        return;

    // -- Initialize with the input from QT
    mStoreMap->InitializeTrackerDevice(deviceName);
    ui->Display_TrackerThingName->setText(QString::fromStdString(mStoreMap->mTrackerThingName.str()));

    // -- Sync the value on GUI with the actual values that may be updated
    {
        ui->spinBox_StateInterval->setValue((int)(mStoreMap->mHmmStateInterval+0.5));

        ui->SpinBox_StartZone->setValue(mStoreMap->mSampleAstarPath_startZone);
        ui->SpinBox_StartZone->setMaximum(mStoreMap->mZones.size()-1);

        ui->SpinBox_StartZone->setValue(mStoreMap->mSampleAstarPath_endZone);
        ui->SpinBox_StartZone->setMaximum(mStoreMap->mZones.size()-1);
    }

    mDb.Initialize(deviceName);

    // -- Connect to the local DB
    mDb.ConnectToLocalDB();

    // -- Retrieve the table info and update the display
    mDb.GetRawTrackTable();

    // -- Retrieve and parse the entire table
    mDb.GetAllRawTracks(mStoreMap->mWifiTracks);

    // -- Sync the value on GUI with the actual values that may be updated
    UpdateTableInfo();

    // -- Designate a table for post-processing
    mDb.UsePostProcessingTable();

    // -- Retrieve the corresponding store map and show it
    mStoreMap->DrawMap();
    UpdateImage();

}

//void WifiPostProcessingGUI::on_TrackerDevice_currentTextChanged(const QString &arg1) {

//    // -- Initialize GUI display to adapt to the new store layout image
//    InitializeGUI();

//    if(mDb.IsInitialized() == true) {
//        mDb.DisconnectToLocalDB();
//    }

//    if(mStoreMap != NULL) {
//        delete mStoreMap;
//    }

//    // -- ThingName (e.g., carealot-001-omni001)
//    string thingName = arg1.toStdString();

//    if(thingName.size() == 0)
//        return;

//    mStoreMap = new WiFiPostProcessor;

//    // -- Initialize with the input from QT
//    mStoreMap->Initialize(thingName);

//    // -- Sync the value on GUI with the actual values that may be updated
//    {
//        ui->spinBox_StateInterval->setValue((int)(mStoreMap->mHmmStateInterval+0.5));

//        ui->SpinBox_StartZone->setValue(mStoreMap->mSampleAstarPath_startZone);
//        ui->SpinBox_StartZone->setMaximum(mStoreMap->mZones.size()-1);

//        ui->SpinBox_StartZone->setValue(mStoreMap->mSampleAstarPath_endZone);
//        ui->SpinBox_StartZone->setMaximum(mStoreMap->mZones.size()-1);
//    }


//    mDb.Initialize(thingName);

//    // -- Connect to the local DB
//    mDb.ConnectToLocalDB();

//    // -- Retrieve the table info and update the display
//    mDb.GetRawTrackTable();

//    // -- Retrieve and parse the entire table
//    mDb.GetAllRawTracks(mStoreMap->mWifiTracks);

//    // -- Sync the value on GUI with the actual values that may be updated
//    UpdateTableInfo();

//    // -- Designate a table for post-processing
//    mDb.UsePostProcessingTable();

//    // -- Retrieve the corresponding store map and show it
//    mStoreMap->DrawMap();
//    UpdateImage();


//}


void WifiPostProcessingGUI::UpdateTableInfo()
{
    QString text;

    text = QString::asprintf("%d", (int)mStoreMap->mWifiTracks.size());
    ui->TableRowSize->setText(text);

    int cnt = 0;
    for(uint i=0; i< mStoreMap->mWifiTracks.size(); i++)
        if(mStoreMap->mWifiTracks[i].refinedTrack.size() > 0)
            ++cnt;

    text = QString::asprintf("%d", cnt);
    ui->Table_ProcessedSize->setText(text);

    // -- Convert the start time to a readable format
    if(mStoreMap->mWifiTracks.size() > 0) {
        mLocalClock.updateTime(mStoreMap->mWifiTracks.begin()->track.begin()->localtime, 0);
        ui->StartTimeEdit->setText(QString::fromStdString(mLocalClock.mDateTime_str));

        mLocalClock.updateTime(((mStoreMap->mWifiTracks.end()-1)->track.end()-1)->localtime, 0);
        ui->EndTimeEdit->setText(QString::fromStdString(mLocalClock.mDateTime_str));
    }
}

void WifiPostProcessingGUI::PerformForAll() {

    gSaveTime();

    // -- Delete and create again
    mDb.CreatePostProcessingTable();

    for(uint i=0; i<mStoreMap->mWifiTracks.size(); i++) {

        // -- Perform post-processing for a wifiTrack
        if(mStoreMap->PerformPostProcessing(i)) {

            printf("Processed %dth trajectory among %d: Successful\n", i, (uint)mStoreMap->mWifiTracks.size());

            // -- Insert the resulting trajectory into MySQL table
            mDb.AddTrackToPostProcessingTable(mStoreMap->mWifiTracks[i]);
        } else {

            printf("Processed %dth trajectory among %d: FAILED\n", i, (uint)mStoreMap->mWifiTracks.size());
        }

        std::fflush(stdout);
    }

    double elapsedTime = gTimeSinceLastSaved();

    printf("============ PerformForAll() took %.2f [sec] (i.e., %.2f [min]) to process %d tracks ==> %d tracks per min ================== \n", elapsedTime, elapsedTime/60.f, (int)mStoreMap->mWifiTracks.size(), (int)(mStoreMap->mWifiTracks.size()/(elapsedTime/60.f)));

    UpdateTableInfo();
}

void WifiPostProcessingGUI::on_PrintAllTable_clicked()
{
    // -- Print the entire table
    mDb.PrintAllRawTracks();
}

void WifiPostProcessingGUI::on_ShowSensors_toggled(bool checked)
{
    mStoreMap->mShowSensors = checked;

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_FontScale_valueChanged(double arg1)
{
    mStoreMap->mFontSizeScale = arg1;

    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_SelectTrackSpinBox_valueChanged(int arg1)
{
    mStoreMap->ResetPostProcessing(arg1);

    // -- Don't perform the processing for all without explicit command.
    if(arg1 < 0) {

        PerformForAll();

    } else {

        mStoreMap->mSelectedTrackIndex = arg1;

        // -- Perform post-processing for a wifiTrack
        if(mStoreMap->PerformPostProcessing_Logging(mStoreMap->mSelectedTrackIndex)) {

            // -- Insert the resulting trajectory into MySQL table
            mDb.DeleteTrackToPostProcessingTable(mStoreMap->mWifiTrackSelected_AfterMakingRealistic);
            mDb.AddTrackToPostProcessingTable(mStoreMap->mWifiTrackSelected_AfterMakingRealistic);
        }

    }

    UpdateTableInfo();

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_checkBox_ZoneConnectivity_clicked(bool checked)
{
    mStoreMap->mShowContourConnectivity = checked;

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_checkBox_Zones_clicked(bool checked)
{
    mStoreMap->mShowZoneBoundary = checked;

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_checkBox_HmmStates_clicked(bool checked)
{
    mStoreMap->mShowHmmStates = checked;

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}


void WifiPostProcessingGUI::on_checkBox_StatesConnectivity_clicked(bool checked)
{
    mStoreMap->mShowHmmStateConnectivity = checked;

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_checkBox_AstarWalls_clicked(bool checked)
{
    mStoreMap->mShowAstarWalls = checked;

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_checkBox_SampleAstarPath_clicked(bool checked)
{
    mStoreMap->mShowSampleAstarPath = checked;

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_SpinBox_StartZone_valueChanged(int arg1)
{
    mStoreMap->mSampleAstarPath_startZone = arg1;

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_SpinBox_EndZone_valueChanged(int arg1)
{
    mStoreMap->mSampleAstarPath_endZone = arg1;

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_spinBox_AdjacencyThreshold_editingFinished()
{
    mStoreMap->mAdjacencyThreshold = ui->spinBox_AdjacencyThreshold->value();

    // -- Re-define the HMM states
    mStoreMap->CreateZoneAdjacencyMap();

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_checkBox_RawZoneMap_clicked(bool checked)
{
    mStoreMap->mShowRawZoneMap = checked;

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_checkBox_SampleClosestState_clicked(bool checked)
{
    mStoreMap->mShowSampleClosestZoneAndState = checked;

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_checkBox_SampleViterbiPath_clicked(bool checked)
{
    mStoreMap->mShowSampleViterbiPath = checked;

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_checkBox_SampleReconstructedPath_clicked(bool checked)
{
    mStoreMap->mShowSampleReconstructedPath = checked;

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_spinBox_StateInterval_valueChanged(int arg1)
{
    mStoreMap->mHmmStateInterval = arg1;

    // -- Re-define the HMM states
    mStoreMap->DefineHmmStates();

    ui->spinBox_StateInterval->setValue((int)(mStoreMap->mHmmStateInterval));

    // -- Perform post-processing for a wifiTrack
    mStoreMap->ResetPostProcessing(mStoreMap->mSelectedTrackIndex);
    mStoreMap->PerformPostProcessing_Logging(mStoreMap->mSelectedTrackIndex);

    // -- Perform Post processing on the sample wifiTrack
    mStoreMap->PerformPostProcessing_Sample();

    UpdateTableInfo();

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_checkBox_InitialRawTrack_clicked(bool checked)
{
    mStoreMap->mShowInitialRawTrack = checked;

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_checkBox_Filtering_clicked(bool checked)
{
    mStoreMap->mShowFilteredTrack_PerTrack = checked;

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_checkBox_ViterbiPath_clicked(bool checked)
{
    mStoreMap->mShowViterbiPath_PerTrack = checked;

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_checkBox_ReconstructedPath_clicked(bool checked)
{
    mStoreMap->mShowReconstructedPath_PerTrack = checked;

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_Button_PerformForAll_clicked()
{
    // -- Keep it pressed, and release it once the operation is done.
    ui->Button_PerformForAll->setChecked(true);

    PerformForAll();

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();

    // -- Release the button since the operation is done.
    ui->Button_PerformForAll->setChecked(false);
}

void WifiPostProcessingGUI::on_checkBox_ShowSampleRealisticPath_clicked(bool checked)
{

    mStoreMap->mShowSampleRealisticPath = checked;

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_checkBox_RealisticPath_PerTrack_clicked(bool checked)
{
    mStoreMap->mShowRealisticPath_PerTrack = checked;

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_doubleSpinBox_Randomness_valueChanged(double arg1)
{
    mStoreMap->mRandomnessRadius = arg1 * 30.48;        // -- Convert [ft] to [cm]

    // -- Perform post-processing for a wifiTrack
    mStoreMap->ResetPostProcessing(mStoreMap->mSelectedTrackIndex);
    mStoreMap->PerformPostProcessing_Logging(mStoreMap->mSelectedTrackIndex);

    // -- Perform Post processing on the sample wifiTrack
    mStoreMap->PerformPostProcessing_Sample();

    UpdateTableInfo();

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_checkBox_SampleInitialTrack_clicked(bool checked)
{
    mStoreMap->mShowSampleInitialTrack = checked;

    // -- Perform Post processing on the sample wifiTrack
    mStoreMap->PerformPostProcessing_Sample();

    mDb.DeleteTrackToPostProcessingTable(mStoreMap->mSampleWifiTrack_AfterMakingRealistic);
    mDb.AddTrackToPostProcessingTable(mStoreMap->mSampleWifiTrack_AfterMakingRealistic);

    UpdateTableInfo();

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_checkBox_SampleFilteredTrack_clicked(bool checked)
{
    mStoreMap->mShowSampleFilteredTrack = checked;

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_Button_ResetTable_clicked()
{
    // -- Keep it pressed, and release it once the operation is done.
    ui->Button_ResetTable->setChecked(true);

    // -- Create an empty table to store the trajectories after post-processing
    mDb.CreatePostProcessingTable();

    ui->Button_ResetTable->setChecked(false);
}

void WifiPostProcessingGUI::on_Button_LoadTable_clicked()
{
    // -- Keep it pressed, and release it once the operation is done.
    ui->Button_LoadTable->setChecked(true);

    // -- Retrieve the table info and update the display
    mDb.GetProcessedTrackTable();

    // -- Retrieve and parse the entire table
    mDb.GetAllProcessedTracks(mStoreMap->mWifiTracks);

    UpdateTableInfo();

    ui->Button_PerformForAll->setChecked(false);
}

void WifiPostProcessingGUI::on_Button_ExportTable_clicked()
{
    // -- Keep it pressed, and release it once the operation is done.
    ui->Button_ExportTable->setChecked(true);

    // -- Export table to '.sql.gz' format
    mDb.ExportTableFromLocalDB();

    ui->Button_ExportTable->setChecked(false);
}

void WifiPostProcessingGUI::on_doubleSpinBox_TrackZoom_valueChanged(double arg1)
{
    mStoreMap->mTrackZoom = arg1;

    // -- Perform post-processing for a wifiTrack
    mStoreMap->ResetPostProcessing(mStoreMap->mSelectedTrackIndex);
    mStoreMap->PerformPostProcessing_Logging(mStoreMap->mSelectedTrackIndex);

    // -- Perform Post processing on the sample wifiTrack
    mStoreMap->PerformPostProcessing_Sample();

    UpdateTableInfo();

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_checkBox_TrackZooming_clicked(bool checked)
{
    mStoreMap->mShowTrackZoomed = checked;

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_checkBox_SampleTrackZooming_clicked(bool checked)
{
    mStoreMap->mShowSampleTrackZoomed = checked;

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_checkBox_TrackOffsetting_clicked(bool checked)
{
    mStoreMap->mShowTrackOffset = checked;

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_checkBox_SampleTrackOffsetting_clicked(bool checked)
{
    mStoreMap->mShowSampleTrackOffset = checked;

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_spinBox_Offset_x_valueChanged(int arg1)
{
    mStoreMap->mTrackOffset_x = arg1 * 100;     // -- Unit: [cm]

    // -- Perform post-processing for a wifiTrack
    mStoreMap->ResetPostProcessing(mStoreMap->mSelectedTrackIndex);
    mStoreMap->PerformPostProcessing_Logging(mStoreMap->mSelectedTrackIndex);

    // -- Perform Post processing on the sample wifiTrack
    mStoreMap->PerformPostProcessing_Sample();

    UpdateTableInfo();

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_spinBox_Offset_y_valueChanged(int arg1)
{
    mStoreMap->mTrackOffset_y = arg1 * 100;     // -- Unit: [cm]

    // -- Perform post-processing for a wifiTrack
    mStoreMap->ResetPostProcessing(mStoreMap->mSelectedTrackIndex);
    mStoreMap->PerformPostProcessing_Logging(mStoreMap->mSelectedTrackIndex);

    // -- Perform Post processing on the sample wifiTrack
    mStoreMap->PerformPostProcessing_Sample();

    UpdateTableInfo();

    // -- Refresh the display
    mStoreMap->DrawMap();
    UpdateImage();
}

void WifiPostProcessingGUI::on_DropMenu_StoreName_Testbed_currentTextChanged(const QString &storeName)
{

    if(ui->DropMenu_StoreName_Testbed->count() <= 1)
        return;

    // -- Initialize GUI display to adapt to the new store layout image
    InitializeGUI();

    // -- Get the Table list from AWS RDS in a particular Schema (i.e., Database)
    vector<string> deviceList_Testbed = mDb.GetTrackerDeviceListFromAwsRds(storeName.toStdString(), true);

    // -- DEBUG
    if(1) {
        cout << " mDb.GetTableListFromAwsRds() for Testbed = " << endl;
        for(uint i=0; i<deviceList_Testbed.size(); i++)
            cout << deviceList_Testbed[i] << endl;

        cout << endl;
    }

    if(mStoreMap != NULL) {
        delete mStoreMap;
    }

    mStoreMap = new WiFiPostProcessor;

    // -- Initialize with the input from QT
    mStoreMap->InitializeStore(mDb.GetDbName());


    ui->TrackerDevice->clear();
    for(uint i=0; i<deviceList_Testbed.size(); i++)
        ui->TrackerDevice->addItem(tr(deviceList_Testbed[i].c_str()));
}

void WifiPostProcessingGUI::on_DropMenu_StoreName_Production_currentTextChanged(const QString &storeName)
{
    if(ui->DropMenu_StoreName_Production->count() <= 1)
        return;

    // -- Initialize GUI display to adapt to the new store layout image
    InitializeGUI();

    // -- Get the Table list from AWS RDS in a particular Schema (i.e., Database)
    vector<string> deviceList_Production = mDb.GetTrackerDeviceListFromAwsRds(storeName.toStdString(), false);

    // -- DEBUG
    if(1) {
        cout << " mDb.GetTableListFromAwsRds() for Production = " << endl;
        for(uint i=0; i<deviceList_Production.size(); i++)
            cout << deviceList_Production[i] << endl;

        cout << endl;
    }

    if(mStoreMap != NULL) {
        delete mStoreMap;
    }

    mStoreMap = new WiFiPostProcessor;

    // -- Initialize with the input from QT
    mStoreMap->InitializeStore(mDb.GetDbName());


    ui->TrackerDevice->clear();
    for(uint i=0; i<deviceList_Production.size(); i++)
        ui->TrackerDevice->addItem(tr(deviceList_Production[i].c_str()));

}

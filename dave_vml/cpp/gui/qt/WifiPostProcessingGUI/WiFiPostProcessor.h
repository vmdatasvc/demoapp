#ifndef WIFIPOSTPROCESSOR_H
#define WIFIPOSTPROCESSOR_H

#include <random>
#include <sstream>
#include "projects/WiFi_Shopper_Tracker/WiFi_Shopper_Tracker.hpp"
#include "Astar.hpp"

using namespace cv;
using namespace std;

// -- The state placement type
enum HMM_State_Type_t { SQUARE_GRID = 1 };

enum WiFiErrorType_t {
    SUCCESSFUL = 1,
    TOO_SMALL_TRACK_POINT = -1,
    TOO_SHORT_TRACK_DURATION = -2,
    INVALID_START_PT_INDEX = -3
};

class WiFiPostProcessor : public WiFi_Shopper_Tracker
{
public:
    WiFiPostProcessor(string trackerId_suffix = "001");
    virtual ~WiFiPostProcessor();

    void InitializeStore(string storeName);
    void InitializeTrackerDevice(string trackerDeviceName);
    void DrawMap();
    void DrawTrack(WiFiTrack_t *track, bool randomColor = false);
    void DrawViterbiPath(WiFiTrack_t *wifiTrack);
    void DrawReconstructedPath(WiFiTrack_t *wifiTrack);

    void InitializeHmmMap(string storeFileName, string zoneFileName);
    bool DefineHmmStates();
    void CreateZoneAdjacencyMap();
    Point ConvertAstarMapPtToStoreMapPt(SquareGrid::Location pt_a);
    SquareGrid::Location ConvertStoreMapPtToAstarMapPt(Point pt_s);
    tuple<Zone_t*, Point2f, Point> findClosestZoneAndPoint(Point2f pt);

    // -- Major Post-processing functions
    WiFiErrorType_t AddOffsetInTrajectory(WiFiTrack_t &wifiTrack);
    WiFiErrorType_t AddZoomInTrajectory(WiFiTrack_t &wifiTrack);
    WiFiErrorType_t FilterRawTrajectory(WiFiTrack_t &wifiTrack);
    WiFiErrorType_t FindBestRoute_Viterbi(WiFiTrack_t &wifiTrack);
    WiFiErrorType_t ReconstructRoutePath_StoreMap(WiFiTrack_t &wifiTrack);
    WiFiErrorType_t AddRandomNoiseToRoutePath_StoreMap(WiFiTrack_t &wifiTrack);

    bool PerformPostProcessing(int trackIndex = -1);
    bool PerformPostProcessing_Logging(int trackIndex = -1);
    void PerformPostProcessing_Sample();

    void ResetPostProcessing_Sample();
    void ResetPostProcessing(int ind);

    int FindZone_IndexMap(Point state);
    int FindZone_StoreMap(Point2f state);
    Point2f ConvertIndexMapPtToStoreMapPt(Point pt);
    Point ConvertStoreMapPtToIndexMapPt(Point2f pt);

    Mat&  GetMapImage(void) {
        return mImg_map_final;
    }

    vector<tuple<Zone_t*, Point2f, Point> > GetCandidateList(Point2f loc, double radius);
    double GetEmissionProb(Point2f loc, Point state);
    double GetRouteDistance(Point I, Point J);
    double GetTransitionProb(Point2f A, Point2f B, Point I, Point J);

    deque<WiFiTrack_t> mWifiTracks;

    Mat mImg_HmmMap_noiseReduced;

    string mTargetStoreName, mTargetStoreName_slash, mTargetDevName;
    stringstream mTrackerThingName;

    vector<Zone_t> mZones, mHoles;
    int mAdjacencyThreshold;
    vector<vector<bool> > mZoneAdjacency;   // -- Adjacency matrix among zones. Size: [# of zones] by [# of zones]
    Mat1f mDistBtwZones;                    // -- Unit: [pixel] The closest Euclidean distance between zones
    Mat1f mDistBtwZones_center;             // -- Unit: [pixel] The Euclidean distance between the center of zones
    vector<vector<Point2f> > mStateMap;     // -- Map that stores the coordinates of the states

    GridWithWeights *mAstarMap;             // -- Map that stores the index of the states. A* algorithm will be run on this map, rather than mStateMap.
    vector<vector<int> > mAstarMapZoneId;   // --  zone id on index map. Size: [# of zones] by [# of zones]

    bool mShowRawZoneMap, mShowContourConnectivity, mShowZoneBoundary, mShowHmmStates, mShowHmmStateConnectivity, mShowAstarWalls, mShowSampleClosestZoneAndState, mShowSampleAstarPath;
    bool mShowSampleInitialTrack, mShowSampleTrackOffset, mShowSampleTrackZoomed, mShowSampleFilteredTrack, mShowSampleViterbiPath, mShowSampleReconstructedPath, mShowSampleRealisticPath;
    bool mShowInitialRawTrack, mShowTrackOffset, mShowTrackZoomed, mShowFilteredTrack_PerTrack, mShowViterbiPath_PerTrack, mShowReconstructedPath_PerTrack, mShowRealisticPath_PerTrack;
    double mHmmStateInterval;      // -- Unit: [pixel]
    int mSampleAstarPath_startZone, mSampleAstarPath_endZone;
    Point mSampleLocation1, mSampleLocation2;

    int mGridType;
    double mRandomnessRadius;     // -- Unit: [ft]

    WiFiTrack_t mSampleWifiTrack;
    WiFiTrack_t mSampleWifiTrack_AfterOffsetting;
    WiFiTrack_t mSampleWifiTrack_AfterZooming;
    WiFiTrack_t mSampleWifiTrack_AfterFiltering;
    WiFiTrack_t mSampleWifiTrack_AfterViterbi;
    WiFiTrack_t mSampleWifiTrack_AfterReconstruction;
    WiFiTrack_t mSampleWifiTrack_AfterMakingRealistic;

    int mSelectedTrackIndex;
    WiFiTrack_t mWifiTrackSelected_AfterOffsetting;
    WiFiTrack_t mWifiTrackSelected_AfterZooming;
    WiFiTrack_t mWifiTrackSelected_AfterFiltering;
    WiFiTrack_t mWifiTrackSelected_AfterViterbi;
    WiFiTrack_t mWifiTrackSelected_AfterReconstruction;
    WiFiTrack_t mWifiTrackSelected_AfterMakingRealistic;

    // -- Ad-hoc heuristics
    Point2f mDoorLocation;
    deque<Point2f> mCheckoutLocations;
    double mTrackZoom;
    int mTrackOffset_x, mTrackOffset_y;         // -- Unit: [cm]


private:

    double compute_pairwise_distance(vector<Point> cont1 , vector<Point> cont2);

    Mat mImg_map_init_zone;

//    default_random_engine mDistGenerator;
//    normal_distribution<double> mNormalDist;
//    exponential_distribution<double> mExponentialDist;

};

#endif // WIFIPOSTPROCESSOR_H

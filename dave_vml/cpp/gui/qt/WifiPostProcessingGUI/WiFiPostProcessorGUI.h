/*
 * WifiPostProcessingGUI.hpp
 *
 *  Created on: Nov 28, 2016
 *      Author: paulshin
 */

#ifndef GUI_QT_WIFIPOSTPROCESSINGGUI_WIFIPOSTPROCESSINGGUI_HPP_
#define GUI_QT_WIFIPOSTPROCESSINGGUI_WIFIPOSTPROCESSINGGUI_HPP_

#include <QMainWindow>
#include <QTimer>
#include <QFileDialog>
#include <QFile>
#include <QMessageBox>
#include <QTextStream>
#include <QtWidgets>
#include <QComboBox>
#include <QObject>

// opencv include
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>

#include <vector>
#include <deque>

#include <modules/db/mysql_connector.hpp>
#include <modules/wifi/MessageType.hpp>

#include "../WifiPostProcessingGUI/WiFiPostProcessor.h"

using namespace std;

namespace Ui {
class WifiPostProcessingGUI;
}

class WifiPostProcessingGUI : public QMainWindow
{
    Q_OBJECT

public:
    explicit WifiPostProcessingGUI(QWidget *parent = 0);
    ~WifiPostProcessingGUI();



private slots:

    void on_SyncButton_clicked();
    void on_TrackerDevice_currentTextChanged(const QString &arg1);
    void on_PrintAllTable_clicked();
    void on_ShowSensors_toggled(bool checked);
    void on_FontScale_valueChanged(double arg1);
    void on_SelectTrackSpinBox_valueChanged(int arg1);
    void on_checkBox_ZoneConnectivity_clicked(bool checked);
    void on_checkBox_Zones_clicked(bool checked);
    void on_checkBox_HmmStates_clicked(bool checked);
    void on_checkBox_StatesConnectivity_clicked(bool checked);
    void on_checkBox_AstarWalls_clicked(bool checked);
    void on_checkBox_SampleAstarPath_clicked(bool checked);
    void on_SpinBox_StartZone_valueChanged(int arg1);
    void on_SpinBox_EndZone_valueChanged(int arg1);
    void on_spinBox_AdjacencyThreshold_editingFinished();
    void on_checkBox_RawZoneMap_clicked(bool checked);
    void on_checkBox_SampleClosestState_clicked(bool checked);
    void on_checkBox_SampleViterbiPath_clicked(bool checked);
    void on_checkBox_SampleReconstructedPath_clicked(bool checked);
    void on_spinBox_StateInterval_valueChanged(int arg1);
    void on_checkBox_InitialRawTrack_clicked(bool checked);
    void on_checkBox_Filtering_clicked(bool checked);
    void on_checkBox_ViterbiPath_clicked(bool checked);
    void on_checkBox_ReconstructedPath_clicked(bool checked);

    void on_Button_PerformForAll_clicked();

    void on_checkBox_ShowSampleRealisticPath_clicked(bool checked);

    void on_checkBox_RealisticPath_PerTrack_clicked(bool checked);

    void on_doubleSpinBox_Randomness_valueChanged(double arg1);

    void on_checkBox_SampleInitialTrack_clicked(bool checked);

    void on_checkBox_SampleFilteredTrack_clicked(bool checked);

    void on_Button_ResetTable_clicked();

    void on_Button_LoadTable_clicked();

    void on_Button_ExportTable_clicked();

    void on_doubleSpinBox_TrackZoom_valueChanged(double arg1);

    void on_checkBox_TrackZooming_clicked(bool checked);

    void on_checkBox_SampleTrackZooming_clicked(bool checked);

    void on_checkBox_TrackOffsetting_clicked(bool checked);

    void on_checkBox_SampleTrackOffsetting_clicked(bool checked);

    void on_spinBox_Offset_x_valueChanged(int arg1);

    void on_spinBox_Offset_y_valueChanged(int arg1);

    void on_DropMenu_StoreName_Testbed_currentTextChanged(const QString &arg1);

    void on_DropMenu_StoreName_Production_currentTextChanged(const QString &arg1);

private:
    Ui::WifiPostProcessingGUI *ui;

    CvSize mMapSize;
    //vector<string> mTrackerList;
    LocalClock mLocalClock;

    QThread     *mWorkerThread;
    mysql_connector mDb;
    WiFiPostProcessor    *mStoreMap;

    void InitializeGUI();
    void DebugOutputChangeFontSetting();
    void UpdateTableInfo();
    void PerformForAll();

    void UpdateImage();
    void DrawMap();
};


#endif /* GUI_QT_WIFIPOSTPROCESSINGGUI_WIFIPOSTPROCESSINGGUI_HPP_ */

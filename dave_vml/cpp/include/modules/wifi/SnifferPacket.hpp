/*
 * SnifferPacket.hpp
 *
 *  Created on: Jan 24, 2017
 *      Author: jwallace
 */

#ifndef SNIFFERPACKET_HPP_
#define SNIFFERPACKET_HPP_

#include <string>
#include <vector>
#include <iostream>

#include <stdint.h>

//#include <boost/array.hpp>

#include <boost/serialization/access.hpp>
//#include <boost/serialization/string.hpp>

// A class designed to be sent over the wire from sniffers to the aggregators on the tracker

class SnifferPacket {

	// send to/from data stream
	friend class boost::serialization::access;
	template <class Archive>
	void serialize(Archive& ar, const unsigned int version){
		ar & type;
		ar & channel;
		ar & mac_str;
		//for(unsigned int i=0; i<6; i++){
		//	ar & mac[i];
		//}
		ar & rss;
	}

public:
	enum DeviceType : uint8_t {
		Invalid = 0x0,
		Station = 0x1,
		AP = 0x2,
		ServiceAgent = 0x3,
		NullDevice = 0x4,
		OmniSensr = 0x5
	};

public:
	SnifferPacket();

	// construct from string representation of MAC
	SnifferPacket(DeviceType t, unsigned char channel, const std::string& mac, int8_t rss);
	// construct from raw MAC
	SnifferPacket(DeviceType t, unsigned char channel, const uint8_t* mac, int8_t rss);
	~SnifferPacket() {}

	// NOTE: The default copy ctor/assignment operator does the Right Thing

	// print a string representation
	void print(std::ostream& os) const;

	const std::string& getMAC_str() const {return mac_str;}
	//const uint8_t* getMAC_raw() const {return &mac[0];}
	DeviceType getType() const {return type;}
	unsigned char getChannel() const {return channel;}
	int8_t getRSS() const {return rss;}

private:

	DeviceType type;
	unsigned char channel;

	//boost::array<uint8_t, 6> mac;
	std::string mac_str;
	int8_t rss;

};

#endif /* INCLUDE_MODULES_WIFI_SNIFFERPACKET_HPP_ */

/*
 * TrackerPacket.hpp
 *
 *  Created on: Jan 24, 2017
 *      Author: jwallace
 */

#ifndef TRACKERPACKET_HPP_
#define TRACKERPACKET_HPP_

#include "SnifferPacket.hpp"

#include <string>
#include <iostream>

#include <boost/serialization/access.hpp>
#include <boost/serialization/string.hpp>

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/time_serialize.hpp>

// a serializable packet designed to be sent from the aggregator to the tracker

class TrackerPacket {
	// send to/from data stream
	friend class boost::serialization::access;
	template <class Archive>
	void serialize(Archive& ar, const unsigned int version){
		ar & timestamp;
		ar & capture_name;
		ar & payload;
	}

public:
	// use default values of everything
	TrackerPacket() {}

	TrackerPacket(const SnifferPacket& sniff, const boost::posix_time::ptime& ts, const std::string& name)
		: payload(sniff), timestamp(ts), capture_name(name) {}

	~TrackerPacket() {}

	// Again, default copy ctor / assignment operator is good here

	// less than operator for comparison (sort by timestamp)
	bool operator<(const TrackerPacket& o) const{ return timestamp < o.timestamp; }

	void print(std::ostream& os) const;

	const SnifferPacket& getPayload() const {return payload;}
	const boost::posix_time::ptime& getTimestamp() const {return timestamp;}
	const std::string& getName() const {return capture_name;}

private:
	// The payload captured
	SnifferPacket payload;
	// The time of capture (according to the aggregator)
	boost::posix_time::ptime timestamp;
	// The name of the aggregator
	std::string capture_name;

};


#endif /* INCLUDE_MODULES_WIFI_TRACKERPACKET_HPP_ */

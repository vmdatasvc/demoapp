/*
 * MessageType.hpp
 *
 *  Created on: Jun 30, 2014
 *      Author: pshin
 */

#ifndef MESSAGETYPE_HPP_
#define MESSAGETYPE_HPP_

#include <string>
#include <list>
#include <deque>
#include <memory>
#include <opencv2/core.hpp>

//enum OS_type {ANDROID = 0x11, IOS = 0x12, OS_ETC = 0x13};		// -- Operating System type
enum device_type {Station = 0x1, AP = 0x2, ServiceAgent = 0x3, NullDevice = 0x4, OmniSensr = 0x5};	// -- Station is a client (sending Probe Request), while AP is an access point ((sending Probe Response) and ServiceAgent is not a shopper. The one whose manufacturer is not on the OUI list is marked as NullDevice.
enum wifi_protocol_type {B_802_11 = 0x21, G_802_11 = 0x22, N_802_11 = 0x23};

using namespace std;

// -- for a single OS
typedef struct {
	string addr;
	double timestamp;
	float rss;
} OmniSensr_runtime_t;

// -- for a single location. For a location a, we create a fingerprint.
typedef struct {
	float x;		// -- World coordinate of the marker position
	float y;		// -- World coordinate of the marker position
	list<OmniSensr_runtime_t> sensors;
} fingerprint_runtime_t;


	// -- for a single measurement
	typedef struct {
		double timestamp;
		float dist;		// -- Distance from the source to the OS
		int rss;		// -- RSS from the source to the OS
		int channel;	// -- WiFi channel used
		string src;			// -- SerialNumber of the source node
		string capturedBy;	// -- SerialNumber of the node who captures the packet
		int x;			// -- Unit: cm, X location of the source device in physical world
		int y;			// -- Unit: cm, Y location of the source device in physical world
	} calibMeas_t;

	// -- for a single OmniSensr
	typedef struct {
//		string addr;
		string serialNum;
		float x;		// -- Unit: cm. Physical location (i.e., (world coordinate) of this OmniSensr
		float y;		// -- Unit: cm. Physical location of this OmniSensr
		list<calibMeas_t> meas;	// -- raw RSS measurements
//		float mu;		// -- mean of the Gaussian distribution of the RSS measurements from a particular fingerprint location measured at this OmniSensr
//		float std;		// -- std of the Gaussian distribution of the RSS measurements from a particular fingerprint location measured at this OmniSensr

		float calib_a;	// -- Parameters for RSS-to-distance conversion by dist = exp(calib_a*rss + calib_b). Distance unit is centimeter
		float calib_b;	// -- Parameters for RSS-to-distance conversion by dist = exp(calib_a*rss + calib_b). Distance unit is centimeter
	} calibOmniSensr_t;


	// -- for a single location. For a location a, we create a fingerprint.
	typedef struct {
		float x;		// -- World coordinate of the marker position
		float y;		// -- World coordinate of the marker position
	//	struct tm time_start;	// -- Time that started recording
	//	struct tm time_end;		// -- Time that ended recording
		int time_v_start;	// -- Unit: sec Time that started recording
		int time_v_end;		// -- Unit: sec Time that ended recording
		string timestring_start, timestring_end;

		list<calibOmniSensr_t> sensors;
	} fingerprint_t;

//typedef struct {
//        int x;
//        int y;
//}   cv::Point;

// -- To store each location of shopper
typedef struct {
        cv::Point	pos;        // -- Coordinates in Physical store, Unit: [cm]
        double localtime;           // -- Measurement time at the local store
	bool isReported;

	// -- temp.
        cv::Point	sensr_center;
}  TrackedPos_t_;

typedef struct {
	string addr_prefix;
	string manuf;			// -- Manufacturer of the WiFi device
}   OUI_Lookup_t;

typedef struct {
	clock_t	begin;			// -- Time when begins
	clock_t	last_heard;		// -- Time that has the last activity via this connection
}  connection_t;

typedef struct  {
	int year, month, date, hour, minute;
	float second;
}  timestamp_t;

typedef struct  {
	string src;			// -- The MAC address of the source of the signal. Formated MAC address (e.g., 08:86:3b:81:7b:da)
	string dst;
	string bssid;
	string capturedBy;		// -- The IP address of the Sniffer who captured the RSS measurement. (e.g., SerialNumber or 10.250.1.42)
//	string SN;			// -- Serial Number of the packet sender (this is detected only for wifi calib packets)
	float distToAP;				// -- Estimated distance from the AP captured this packet
	timestamp_t timestamp;
	//double timstamp_at_AP;
	double localtime;
	//float timediff;			// -- timediff from the previous measurement
	int channel;
	int rss;				// -- [dB]
	int deviceType;			// -- If the src is an AP or a Station
	int protocol;			// -- 802.11 protocol type among b/g/n
	char protocol_s[4];			// -- 802.11 protocol type among b/g/n
}  tcpdump_t;

typedef std::shared_ptr<tcpdump_t> tcpdump_t_Ptr;

typedef struct  {
	string addr;
	int channel;	// -- Applicable only for APs
	//int osType;		// -- Operating System (OS) type among {Android, iOS, etc}

}  Node_info_t;

// -- To store the WiFi Tracker's state info
typedef struct {
	unsigned int	noTrackedShoppers;
	unsigned int	noTrackedShoppersWithinArea;
	unsigned int	noTrackedAPs;
	unsigned int	noTrackedServiceAgent;
	unsigned int	noTrackedDevs;
	unsigned int	noTrackedDevs_NULL;		// -- # of tracked devices whose manufacturer is NULL
	unsigned int	maxTrackedDevs;
	unsigned int	maxBufferSize_ap;
	unsigned int	avgBufferSize_ap;
	unsigned int	maxBufferSize_dev;
	float			avgBufferSize_dev;
	unsigned int 	avgPeriodicProcessingRate;
	unsigned int 	noKnownServiceAgents;
	unsigned int 	noKnownAPs;
	unsigned int	WiFiSensorAvailability; // -- Unit: [%]

	int	runngingTime_in_min;

} TrackState_t;

// -- To store the performance evaluation info
typedef struct {
	int x, y;				// -- Location of the ground truth
	deque<float>	distErr;	// -- List of the error distance from the ground truth location
	float mu, std;			// -- Mean and standard deviation of the distErr for this position
} AccuracyEval_Point_t;

//typedef struct {
//	deque<AccuracyEval_Point_t> locs;
//} AccuracyEval_t;



// -- Below is for WiFi Post Processing ---------------------------------------

// -- To store the information about zones defined in store maps
typedef struct {
    vector<cv::Point> contourPts;   // --  A list of contour points
    int contourIndex;           // -- Index of the contour in the original contour vector. This is the reference for the hierarchy
    cv::Vec4i   contourHierarchy;   // -- [next contour index, prev contour index, first child contour index, parent contour index]
    cv::Point2f contourCenter;      // -- Center of mass
    vector<pair<cv::Point2f, cv::Point> > states;       // -- States defined within the zone region {actual x/y coordinates in StoreMap, i/j indices of this state in the state map
    cv::Scalar color;

    //int zoneId;     // -- Zone ID if there is
} Zone_t;

typedef struct {
    cv::Point2f posPhy;     // -- raw location in physical coordinate system, Unit: [cm]
    double localtime;       // -- Measurement localtime

    // -- Coordinates of the corresponding states found by Viterbi algorithm in the HMM model
    cv::Point2f posStore;   // -- the coordinates of the corresponding state in StoreMap coordinate system, Unit: [pixel]
    cv::Point posIndex;     // -- the coordinates of the corresponding state in IndexMap (i.e., A* map) coordinate system, Unit: [index]
    Zone_t*     zone;       // -- The Zone that the corresponding state belongs to

}  WiFiPostData_t;

// --
typedef struct {
    uint r, g, b;   // -- Randomly assign a unique color for visualization

    // -- Data from tracker
    string dev_id;      // -- ID of the device of the shopper
    double start_time;  // -- Start time of the track
    uint cnt;           // -- # of locations
    double duration;    // -- Shopping trip duration
    deque<WiFiPostData_t> track;   // -- Initial Raw Tracks obtained by parsing "A4" in the raw track table

    // -- Metadata generated in the post-processing
    deque<WiFiPostData_t> refinedTrack;   // -- Final Tracks after post-processing






    // -- Final refined track
    //vector<tuple<Zone_t*, cv::Point2f, cv::Point> > refinedTrack;       // -- {zone_id, coordinate in StoreMap, coordinate in IndexMap, time spent [sec]}

    // -- Zone Sequence info
    //deque<pair<int, double> > zoneSeq;
} WiFiTrack_t;

#endif /* MESSAGETYPE_HPP_ */

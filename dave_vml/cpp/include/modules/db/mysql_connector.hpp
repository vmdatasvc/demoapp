/*
 * mysql_connector.hpp
 *
 *  Created on: Nov 28, 2016
 *      Author: paulshin
 */

#ifndef INCLUDE_MODULES_DB_MYSQL_CONNECTOR_HPP_
#define INCLUDE_MODULES_DB_MYSQL_CONNECTOR_HPP_

#include <string>
#include <deque>

#include <stdexcept>
#include <boost/scoped_ptr.hpp>

/*
  Include directly the different
  headers from cppconn/ and mysql_driver.h + mysql_util.h
  (and mysql_connection.h). This will reduce your build time!
*/
#include "mysql_connection.h"
    #include <cppconn/driver.h>
    #include <cppconn/exception.h>
    #include <cppconn/resultset.h>
    #include <cppconn/statement.h>
    #include "cppconn/resultset_metadata.h"
    #include <cppconn/prepared_statement.h>

#include "modules/wifi/MessageType.hpp"

using namespace std;

class mysql_connector {

public:

	mysql_connector();
	~mysql_connector();

    vector<string> GetSchemaListFromAwsRds(bool isForTestbed);
    vector<string> GetTrackerDeviceListFromAwsRds(string storeName, bool isForTestbed);

	void Initialize(string thingName);

	void loadTestbedList(string testbedList);
	bool IsThisTestbed(string storeAddr);

    bool ExportTableFromLocalDB();
	bool BackupAwsRdsToLocalDB();
    bool ConnectToLocalDB();
    void DisconnectToLocalDB();
    bool GetRawTrackTable();
    bool GetProcessedTrackTable();
    bool PrintAllRawTracks();

    void ParseRawTrackFromDB(string trackStr, WiFiTrack_t &wifiTrack);
    void ParseProcessedTrackFromDB(string trackStr, WiFiTrack_t &wifiTrack);
    WiFiTrack_t GetRawTrackAtARow(uint rowNo);
    bool GetAllRawTracks(deque<WiFiTrack_t> &wifiTracks);
    bool GetAllProcessedTracks(deque<WiFiTrack_t> &wifiTracks);
    bool AddToExistingTracks(deque<WiFiTrack_t> &wifiTracks, WiFiTrack_t &aTrack);

    bool UsePostProcessingTable();
    bool CreatePostProcessingTable();
    bool AddTrackToPostProcessingTable(WiFiTrack_t &wifiTrack);
    bool DeleteTrackToPostProcessingTable(WiFiTrack_t &wifiTrack);


	bool IsInitialized() { return mIsInitialized; }

        int GetTableRowSize() {
            mTableRowSize = mTableResult_Raw->rowsCount();
            return mTableRowSize;
        }
        int GetTableColumnSize() {
            mTableColumnSize = mTableMetaData->getColumnCount();
            return mTableColumnSize;
        }

    string GetDbName() { return mDbName; }

private:

    bool mIsInitialized, mIsConnectedToLocalDB;
    string mDbName, mTableName, mTableNamePost, mAwsDbUrl, mAwsDbId, mAwsDbPwd;

	deque<string> mTestbedList;

    sql::Driver *mLocalDbDriver;
    sql::Connection *mLocalDbConn;
    sql::ResultSet *mTableResult_Raw;
    sql::ResultSet *mTableResult_Processed;
    sql::ResultSetMetaData *mTableMetaData;

       int mTableRowSize, mTableColumnSize;
};




#endif /* INCLUDE_MODULES_DB_MYSQL_CONNECTOR_HPP_ */

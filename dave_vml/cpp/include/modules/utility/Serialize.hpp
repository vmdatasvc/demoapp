/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
* Serialize.hpp
 *
 *  Created on: Oct 2, 2015
 *      Author: yyoon
 */

#ifndef SERIALIZE_HPP_
#define SERIALIZE_HPP_

#include <iostream>
#include <fstream>
#include <opencv2/core/core.hpp>

#include "core/Error.hpp"

namespace vml {

#ifdef _WIN32
typedef _Longlong int64_t;
#endif

class serializer
{
	std::string bytearray;
public:
	serializer() {}
	~serializer() {}

	friend class deserializer;

	size_t  size() {
		return bytearray.size();
	}

	void clear() {
		bytearray.clear();
	}

	template <class T> void serialize_numeric(T scalar);
	template <class T> void serialize_array(T * array, int arraysize);
	template <class T> void serialize_vector(const std::vector<T> &arr);
	template <class T> void serialize_cvMat(const cv::Mat_<T> &mat);
	void serialize_cvMatGen(const cv::Mat &mat);
	void serialize_string(const std::string &astr);

	int save(const std::string filename);
	void ostream_hex_byte_array(std::ostream &os);
	void save_hex_byte_array(const std::string filename);
	void dump(std::string &target);

};

template <class T> inline
void serializer::serialize_numeric(T scalar)
{
	// save scalar to a temp variable to enable
	// const serialization
	T anum = scalar;
	bytearray.append((char*)&anum, sizeof(T));
}

template <class T> inline
void serializer::serialize_array(T * array, int arraysize)
{
	bytearray.append((char*)&arraysize,sizeof(int));
	if (arraysize > 0) {
		bytearray.append((char*)array,sizeof(T)*arraysize);
	}
}

template <class T> inline
void serializer::serialize_vector(const std::vector<T> &arr)
{
	size_t  arraysize = arr.size();
	T*  temparray = new T[arraysize];
	memcpy(temparray,arr.data(),sizeof(T)*arraysize);
	bytearray.append((char*)&arraysize,sizeof(size_t));
	bytearray.append((char*)temparray,sizeof(T)*arraysize);
	delete[] temparray;
}

template <class T> inline
void serializer::serialize_cvMat(const cv::Mat_<T> &mat)
{
	int nrows=mat.rows,ncols=mat.cols;
	bytearray.append((char*)&nrows,sizeof(int));
	bytearray.append((char*)&ncols,sizeof(int));
	bytearray.append((char*)mat.data,sizeof(T)*nrows*ncols);
}

inline void serializer::serialize_cvMatGen(const cv::Mat &mat)
{
	int mtype = mat.type(),nrows=mat.rows,ncols=mat.cols;
	size_t mstep = mat.step;
	bytearray.append((char*)&mtype,sizeof(int));
	bytearray.append((char*)&nrows,sizeof(int));
	bytearray.append((char*)&ncols,sizeof(int));
	bytearray.append((char*)&mstep,sizeof(size_t));
	for (int i=0; i<nrows; i++) {
		char* mptr = (char*)mat.ptr(i);
		bytearray.append(mptr,mstep);
	}
}

inline void serializer::serialize_string(const std::string &astr)
{
	size_t  strsize = astr.size();
	bytearray.append((char*)&strsize,sizeof(size_t));
	bytearray.append(astr);
}

// prints byte array in hexadecimal format to array initialization format
// used for embedding byte array
inline void serializer::ostream_hex_byte_array(std::ostream &os)
{
	os << "{";
	for (std::string::const_iterator i=bytearray.begin(); i<bytearray.end(); i++) {
		os << "0x" << std::hex << (int)(unsigned char)*i << ",";
		// add new line in every 20 bytes
		if ( (int)(i-bytearray.begin()) % 20 == 0) {
			os << std::endl;
		}
	}
	// backtrack stream one byte to remove trailing ','
	int64_t pos = os.tellp();
	os.seekp(pos-1);
	os << "};";
}

inline int serializer::save(const std::string filename)
{
	// open output file
	std::ofstream savefile;
	savefile.open(filename.c_str(),std::ios::out | std::ios::binary);
	if (!savefile.is_open()) {
		VML_ERROR(MsgCode::IOErr, "Unable to open " + filename + " for writing");
		return EXIT_FAILURE;
	}

	// save byte array sequence
	size_t  nbytes = bytearray.size();
	savefile.write((char*)&nbytes,sizeof(size_t));
	savefile.write(bytearray.data(),nbytes);
	savefile.close();
	return EXIT_SUCCESS;
}

inline void serializer::save_hex_byte_array(const std::string filename)
{
	std::ofstream savefile;
	savefile.open(filename.c_str(), std::ios::out);
	if (!savefile.is_open()) {
		VML_ERROR(MsgCode::IOErr, "Unable to open " + filename + " for writing");
	}

	ostream_hex_byte_array(savefile);
	savefile.close();
}

inline void serializer::dump(std::string &target)
{
	target = bytearray;
}


class deserializer
{
	char            *current_ptr;
	std::string     bytearray;
public:
	deserializer():current_ptr(NULL) {}
	deserializer(const unsigned char array[], const size_t nbytes);
	deserializer(const std::string &array);
	deserializer(serializer &is);
	~deserializer() {}

	friend class serializer;

	void init(const unsigned char array[], const size_t nbytes);
	void init(const std::string &array);
	void init(serializer &is);

	template <class T> void deserialize_numeric(T & scalar);
	template <class T> void deserialize_array(T* &array, int & arraysize);
	template <class T> void deserialize_vector(std::vector<T> &arr);
	template <class T> void deserialize_cvMat(cv::Mat_<T> &mat);
	void deserialize_cvMatGen(cv::Mat &mat);
	void deserialize_string(std::string &astr);

	int load(const std::string filename);

	int nrBytesRead() {
		return( current_ptr - (char*)bytearray.data() );
	};
	int nrBytesTotal() {
		return( bytearray.size() );
	};
};

// for embedded objects
inline deserializer::deserializer(const unsigned char array[], const size_t nbytes)
{
	bytearray.resize(nbytes);
	bytearray.assign((char*)array,nbytes);
	current_ptr = (char*)bytearray.data();
};

inline deserializer::deserializer(const std::string &array)
{
	// for scoping problem, it might be safer to copy string
	// contents to local bytearray
	bytearray = array;
	current_ptr = (char*)bytearray.data();
};

inline deserializer::deserializer(serializer &is) {
	bytearray = is.bytearray;
	current_ptr = (char*)bytearray.data();
};

// for initializing after construction
inline void deserializer::init(const unsigned char array[], const size_t nbytes)
{
	bytearray.clear();
	bytearray.resize(nbytes);
	bytearray.assign((char*)array,nbytes);
	current_ptr = (char*)bytearray.data();
}

inline void deserializer::init(const std::string &array)
{
	bytearray.clear();
	bytearray = array;
	current_ptr = (char*)bytearray.data();
}

inline void deserializer::init(serializer &is)
{
	bytearray.clear();
	bytearray = is.bytearray;
	current_ptr = (char*)bytearray.data();
}

template <class T> inline
void deserializer::deserialize_numeric(T & scalar)
{
	memcpy(&scalar,current_ptr,sizeof(T));
	current_ptr += sizeof(T);
}

template <class T> inline
void deserializer::deserialize_array(T* &array, int & arraysize)
{
	memcpy(&arraysize,current_ptr,sizeof(int));
	current_ptr += sizeof(int);
	if (arraysize > 0) {
		array = new T[arraysize];
		memcpy(array,current_ptr,sizeof(T)*arraysize);
		current_ptr += sizeof(T)*arraysize;
	}
	else {
		// set zero size array pointer as NULL for
		// future deletion check
		array = NULL;
	}

}

template <class T> inline
void deserializer::deserialize_vector(std::vector<T> &arr)
{
	size_t vecsize;
	memcpy(&vecsize,current_ptr,sizeof(size_t));
	current_ptr += sizeof(size_t);
	arr.resize(vecsize);
	memcpy(arr.data(),current_ptr,sizeof(T)*vecsize);
	current_ptr += sizeof(T)*vecsize;
}

template <class T> inline
void deserializer::deserialize_cvMat(cv::Mat_<T> &mat)
{
	int nrows,ncols;
	memcpy(&nrows,current_ptr,sizeof(int));
	current_ptr += sizeof(int);
	memcpy(&ncols,current_ptr,sizeof(int));
	current_ptr += sizeof(int);
	mat.create(nrows,ncols);
	memcpy(mat.data,current_ptr,sizeof(T)*nrows*ncols);
	current_ptr += sizeof(T)*nrows*ncols;
}

inline void deserializer::deserialize_cvMatGen(cv::Mat &mat)
{
	int mtype,nrows,ncols;
	size_t mstep;
	memcpy(&mtype, current_ptr, sizeof(int));
	current_ptr += sizeof(int);
	memcpy(&nrows, current_ptr, sizeof(int));
	current_ptr += sizeof(int);
	memcpy(&ncols, current_ptr, sizeof(int));
	current_ptr += sizeof(int);
	memcpy(&mstep, current_ptr, sizeof(size_t));
	current_ptr += sizeof(size_t);
	mat.create(nrows,ncols,mtype);
	VML_ASSERT(mat.step == mstep);
	for (int i=0; i<nrows; i++) {
		char* mptr = (char*)mat.ptr(i);
		memcpy(mptr, current_ptr, mstep);
		current_ptr += mstep;
	}
}

inline void deserializer::deserialize_string(std::string &astr)
{
	size_t strsize;
	memcpy(&strsize, current_ptr, sizeof(size_t));
	current_ptr += sizeof(size_t);
	astr.resize(strsize);
	astr.assign(current_ptr,strsize);
	current_ptr += strsize;
}

inline int deserializer::load(const std::string filename)
{
	// open input file
	std::ifstream loadfile;
	loadfile.open(filename.c_str(),std::ios::in | std::ios::binary);
	if (!loadfile.is_open()) {
		VML_ERROR(MsgCode::IOErr, "Unable to open " + filename + " for reading");
		return EXIT_FAILURE;
	}

	size_t nbytes;
	loadfile.read((char*)&nbytes,sizeof(size_t));
	bytearray.resize(nbytes);
	char*   temparray = new char[nbytes];
	loadfile.read(temparray, nbytes);
	loadfile.close();
	bytearray.assign(temparray,nbytes);
	current_ptr = (char*)bytearray.data();
	return EXIT_SUCCESS;
}


} // of namespace vml

#endif /* SERIALIZE_HPP_ */

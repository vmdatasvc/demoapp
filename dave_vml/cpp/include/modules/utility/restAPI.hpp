/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * cloudinaryAPI.hpp
 *
 *  Created on: Oct 3, 2016
 *      Author: Dave Donghun kim
 *
 *  requirement:
 *  	install libcurl (sudo apt-get install libcul4-openssl-dev)
 */

#ifndef RESTAPI_HPP
#define RESTAPI_HPP

#include <map>
#include <ctime>
#include <sstream>
#include <curl/curl.h>
#include <modules/utility/sha1.h>

#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>

#include <../external/jsonByNlohmann/json.hpp>

namespace vml {

class RestAPI
{

	struct CLPARAMS
	{
		std::string url;
		std::string cloud_name;
		std::string api_key;
		std::string api_secret;
		std::string public_id;
		std::string timestamp;
		std::string signature;
		std::string upload_preset;
		bool signedOption;
	};

	struct SFTPPARAMS
	{
		std::string url;
		std::string user;
		std::string password;
		std::string userpwd;
	};

	struct FtpFile {
	  const char *filename;
	  FILE *stream;
	};

	struct MemoryStruct {
	  char *memory;
	  size_t size;
	};

public:
	RestAPI();
	~RestAPI();

	// For accessing Cloudinary
	void setConfig(std::string APIKey, std::string APISecret, std::string cloudName, std::string uploadPreset);

	long uploadBufferedData(std::string pid, bool singedOpt, char *imgBuffPtr, long imgBuffLength);
	long uploadFile(std::string pid, bool singedOpt, std::string filename );


	// For accessing ftp over ssl server
	void setConfigFTPS(std::string url, std::string username, std::string password);

	long uploadBufferedDataFTPS(std::string pid, bool singedOpt, char *imgBuffPtr, long imgBuffLength);
	long uploadFileFTPS(std::string pid, std::string filename );

	// For accessing ftp over ssh server
	void setConfigSFTP(std::string url, std::string username, std::string password);

	long uploadBufferedDataSFTP(std::string pid, bool singedOpt, char *imgBuffPtr, long imgBuffLength);
	bool uploadFileSFTP(std::string pid, std::string filename );

	static std::size_t fwriteCB(void *buffer, std::size_t size, std::size_t nmemb, void *stream);
	bool downloadFileSFTP(std::string filename, std::string targetfilePath);

	static std::size_t WriteMemoryCB(void *contents, std::size_t size, std::size_t nmemb, void *userp);
	bool downloadBufferedDataSFTP(std::string filename, cv::Mat &fdata);
	// For Amazon S3 storage later

	static int writer(char *data, std::size_t size, std::size_t nmemb, std::string *writerData);
	bool reckognitionAPICall(const std::string& api_addr_base, const std::map<std::string,std::string>& query_config,nlohmann::json &response); //, Json::Value* response);

	// variables
	CLPARAMS mParams;
	SFTPPARAMS mParamsSFTP;

private:



};

} // of namespace vml

#endif  // CLOUDINARYAPI_HPP

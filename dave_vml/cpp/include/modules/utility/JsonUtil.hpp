#include <iostream>
#include "json.hpp"
#include <string>
#include <iostream>
#include <fstream>

class JsonUtil {
public:
	// -- Read from path and stores in json output, return 0 if successful
	static int readJson(std::string path, nlohmann::json &output) {
		int ret = 0;
		std::filebuf fb;
		if (fb.open(path.c_str(), std::ios::in)) {
			std::istream is(&fb);
			try {
				output << is;
			}
			catch (...) {
				std::cout << "Failed to parse file \"" << path << "\"" << std::endl;
			}
			fb.close();
		}
		else {
			std::cout << "Failed to open file \"" << path << "\"" << std::endl;
			ret = 1;
		}
		return ret;
	}
	// -- Write input json to path, return 0 if successful
	static int writeJson(std::string path, nlohmann::json &input) {
		int ret = 0;
		std::filebuf fb;
		if (fb.open(path.c_str(), std::ios::out)) {
			std::ostream os(&fb);
			os << input.dump(4);
			fb.close();
		}
		else {
			std::cout << "Failed to write to file \"" << path << "\"" << std::endl;
			ret = 1;
		}
		return ret;
	}
	// -- Create a json based on the patch and assign it to update json
	// -- handles "add" and "replace" operations in the patch
	static void addPatchToJson(nlohmann::json patch, nlohmann::json &update) {
		std::string path = patch["path"];
		if (path.length() == 0)
			return;
		size_t idx;
		size_t prev = 0;
		nlohmann::json *pointer;
		pointer = &update;
		std::string key;
		while (path.find("/", prev + 1) != std::string::npos) {
			idx = path.find("/", prev + 1);
			key = path.substr(prev + 1, idx - prev - 1);
			if ((*pointer)[key] == NULL)
				(*pointer)[key] = {};
			pointer = &((*pointer)[key]);
			prev = idx;
		}
		key = path.substr(prev + 1);
		(*pointer)[key] = patch["value"];
	}
};

#include <iostream>
#include <string>
#include <vector>
#include <hiredis/hiredis.h>
#include <boost/utility.hpp>

class redis_wrapper : boost::noncopyable{
public:
	redis_wrapper();
	~redis_wrapper();
	int connect();
	void disconnect();
	int getInteger(std::string key);
	std::string getString(std::string key);
	int set(std::string key, std::string val);
	int list_rpush(std::string key, std::string val);
	int list_lpush(std::string key, std::string val);
	std::string list_rpop(std::string key);
	std::string list_lpop(std::string key);
	std::string list_select(std::string key, int idx);
	int list_len(std::string key);
	std::vector<std::string> list_range(std::string key, int idx1, int idx2);
	void del(std::string key);
	int list_set(std::string key, int idx, std::string val);
	bool checkConnection();
	void* command(const char *format);
private:
	std::string redis_host;
	int redis_port;
	redisContext *c;
	bool connected;
};

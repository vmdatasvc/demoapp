/*
 * LocalClock.hpp
 *
 *  Created on: Sep 9, 2015
 *      Author: pshin
 */

#ifndef LocalClock_HPP_
#define LocalClock_HPP_

#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
//#include <string.h>
#include <iostream>

using namespace std;

class LocalClock {

public:
	LocalClock();
	virtual ~LocalClock();

	struct timeval mTv;
	struct timeval mInitTv;
	struct tm* mPtm;
	struct tm* mInitPtm;

	string mDateTime_str;
	string mDateOnly_str;
	string mTimeOnly_str;

	double mDateTime_lf;
	double mInitDateTime_lf;

	void updateTime(double datetime_lf = -1, int precision = 3);

private:

};



#endif /* LocalClock_HPP_ */

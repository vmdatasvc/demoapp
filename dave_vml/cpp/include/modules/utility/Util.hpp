#ifndef UTIL_H
#define UTIL_H

#include <algorithm>
#include <cmath>
#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <numeric>
#include <set>
#include <string>
#include <vector>
#include <sys/types.h>
#include <sys/stat.h>

#ifdef _WIN32
#include <direct.h>
#else
#include <unistd.h>
#endif

#include <opencv2/core/core.hpp>

// file separator definition
#ifdef _WIN32
#define	FILESEPCHAR '\\'
#define FILESEPSTR	"\\"
#else
#define FILESEPCHAR	'/'
#define FILESEPSTR  "/"
#endif

namespace vml {

// OpenCV doesn't have function to enforce a point in Rect. So here it is
template<class _T>
cv::Point_<_T> enforcePointInRect(cv::Point_<_T> p, cv::Rect_<_T> r)
{
	cv::Point_<_T> tp = p;
	if (tp.x < r.tl().x) tp.x = r.tl().x;
	if (tp.y < r.tl().y) tp.y = r.tl().y;
	if (tp.x >= r.br().x) tp.x = r.br().x-1;
	if (tp.y >= r.br().y) tp.y = r.br().y-1;

	return tp;
}

/** class for file name manipulation*/
class FileParts {
	// members
	std::string fullpathstr;
	std::string filenamestr;
	std::string extentionstr; // extentionstr should start with '.'
	std::string lowerextentionstr; // lowercase version, for convenience
	std::string pathstr; // pathstr should end with '/'

	void verifypath(void);
	void verifyextention(void);
	void reconstruct_fullpath(void);

public:
	FileParts();
	FileParts(const char *FileParts);
	FileParts(const std::string &FileParts);
	FileParts(const char *path, const char *name);
	FileParts(const std::string &path, const std::string &name);
	FileParts(const char *path, const char *name, const char *ext);
	FileParts(const std::string &path, const std::string &name, const std::string &ext);

	void init(const std::string &path);
	void init(const std::string &path, const std::string &name);
	void init(const std::string &path, const std::string &name, const std::string &ext);

	const std::string & ext(void) const;
	const std::string & lowerext(void) const;
	const std::string & filename(void) const;
	const std::string & path(void) const;
	const std::string & fullpath(void) const;

	void setpath(const std::string newpath);
	void prependpath(const std::string subpathstr);
	void appendpath(const std::string subpathstr);
	void setfilename(const std::string newfilename);
	void setextention(const std::string newextention);
	void appendfilename(const std::string appendstr);
	void appendextention(const std::string appendstr);
	void setpath(const char* newpath);
	void setfilename(const char* newfilename);
	void setextention(const char* newextention);
	void appendfilename(const char* appendstr);
	void appendextention(const char* appendstr);

	bool pathExists();
	void checkNCreatePath();

	bool isSet(void) {
		return (!fullpathstr.empty());
	}

}; // of class FileParts

inline FileParts::FileParts()
{
	// default constructor. doing nothing
}

inline FileParts::FileParts(const char *FileParts)
{
	std::string temp(FileParts);
	init(temp);
}

inline FileParts::FileParts(const std::string &FileParts)
{
	init(FileParts);
}

inline FileParts::FileParts(const char *path, const char *name)
{
	init(std::string(path),std::string(name));
}

inline FileParts::FileParts(const std::string &path, const std::string &name)
{
	init(path,name);
}

inline FileParts::FileParts(const char *path, const char *name, const char *ext)
{
	init(std::string(path),std::string(name),std::string(ext));
}

inline FileParts::FileParts(const std::string &path, const std::string &name, const std::string &ext)
{
	init(path,name,ext);
}

inline void FileParts::init(const std::string &FileParts)
{
	fullpathstr = FileParts;
	// scan FileParts and find where the last filesep character is
	// currently use '/' as filesep.
	size_t pfound = fullpathstr.find_last_of(FILESEPCHAR);
	if (pfound < fullpathstr.length()) {
		// store in pathstr
		pathstr = fullpathstr.substr(0,pfound+1);
		filenamestr = fullpathstr.substr(pfound+1);
	}
	else {
		// No path
		pathstr = "." + std::string(FILESEPSTR);
		filenamestr = fullpathstr;
	}

	// scan FileParts and find where the last . character is
	size_t efound = filenamestr.rfind('.');
	if (efound < filenamestr.length()) {
		// store the rest in extention str
		extentionstr = filenamestr.substr(efound);

		lowerextentionstr = extentionstr;
		for (unsigned int i=0; i<lowerextentionstr.size(); i++)
			lowerextentionstr[i] = tolower(lowerextentionstr[i]);

		//std::transform(extentionstr.begin(), extentionstr.end(),
		//             lowerextentionstr.begin(), (int(*)(int)) tolower);

		// Remove found extension from filename
		filenamestr = filenamestr.substr(0,efound);
	}

	verifypath();
	verifyextention();
}

inline void FileParts::init(const std::string &path, const std::string &name)
{
	// check if the end of path is not filesep
	if (path[path.length()-1] != FILESEPCHAR) {
		init(path+FILESEPSTR+name);
	}
	else {
		init(path+name);
	}
}

inline void FileParts::init(const std::string &path,const std::string &name,const std::string &ext)
{
	// check if the first char of ext is not '.'
	if (ext[0] != '.') {
		init(path, name+std::string(".")+ext);
	}
	else {
		init(path,name+ext);
	}
}

inline void FileParts::verifypath(void)
{
	if (pathstr[pathstr.length()-1] != FILESEPCHAR)
	{
		pathstr += FILESEPSTR;
	}
}

inline void FileParts::verifyextention(void)
{
	if (!extentionstr.empty() && extentionstr[0] != '.')
	{
		extentionstr = "." + extentionstr;
	}
}

inline const std::string & FileParts::ext(void) const
{
	return(extentionstr);
}

inline const std::string & FileParts::lowerext(void) const
{
	return(lowerextentionstr);
}

inline const std::string & FileParts::filename(void) const
{
	return(filenamestr);
}

inline const std::string & FileParts::path(void) const
{
	return(pathstr);
}

inline const std::string & FileParts::fullpath(void) const
{
	return(fullpathstr);
}

inline void FileParts::setpath(const std::string newpath)
{
	pathstr = newpath;
	verifypath();
	reconstruct_fullpath();
}

inline void FileParts::prependpath(const std::string subpathstr)
{
	// verify subpathstr
	if (subpathstr[subpathstr.length()-1] != FILESEPCHAR) {
		pathstr = subpathstr+FILESEPSTR+pathstr;
	}
	else {
		pathstr = subpathstr+pathstr;
	}
	verifypath();
	reconstruct_fullpath();
}

inline void FileParts::appendpath(const std::string subpathstr)
{
	if (subpathstr[subpathstr.length()-1] != FILESEPCHAR) {
		pathstr += subpathstr+FILESEPSTR;
	}
	else {
		pathstr += subpathstr;
	}
	verifypath();
	reconstruct_fullpath();
}

inline void FileParts::setfilename(const std::string newfilename)
{
	filenamestr = newfilename;
	reconstruct_fullpath();
}

inline void FileParts::setextention(const std::string newextention)
{
	extentionstr = newextention;
	verifyextention();
	reconstruct_fullpath();
}

inline void FileParts::appendfilename(const std::string appendstr)
{
	filenamestr += appendstr;
	reconstruct_fullpath();
}

inline void FileParts::appendextention(const std::string appendstr)
{
	extentionstr += appendstr;
	reconstruct_fullpath();
}

inline void FileParts::setfilename(const char* newfilename)
{
	setfilename(std::string(newfilename));
}

inline void FileParts::setextention(const char* newextention)
{
	setextention(std::string(newextention));
}

inline void FileParts::appendfilename(const char* appendstr)
{
	appendfilename(std::string(appendstr));
}

inline void FileParts::appendextention(const char* appendstr)
{
	appendextention(std::string(appendstr));
}

inline void FileParts::reconstruct_fullpath(void)
{
	fullpathstr = pathstr + filenamestr + extentionstr;
}

inline bool FileParts::pathExists()
{
	struct stat bf;
	return (!stat(pathstr.c_str(),&bf));
}

inline void FileParts::checkNCreatePath()
{
	if (!pathExists())
	{
		std::string cmd("mkdir -p \""+pathstr+"\"");
		system(cmd.c_str());
	}
}


#if 0
// FileParts is a subclass of std::string, so no need to use fullpath() method
class FileParts : public std::string
{
	// members
//  std::string fullpathstr;
//  std::string filenamestr;
//  std::string extentionstr; // extentionstr should start with '.'
//  std::string lowerextentionstr; // lowercase version, for convenience
//  std::string pathstr; // pathstr should end with '/'

	size_t  pos_path_end;
	size_t  pos_ext_begin;

	void init();
	std::string &lowerstr(std::string& instr);

	void verifypath(void);
	void verifyextention(void);
	void reconstruct_fullpath(void);

public:
	FileParts();
	FileParts(const char *FileParts);
	FileParts(const std::string &FileParts);
	FileParts(const char *path, const char *name);
	FileParts(const std::string &path, const std::string &name);
	FileParts(const char *path, const char *name, const char *ext);
	FileParts(const std::string &path, const std::string &name, const std::string &ext);

	void init(const std::string &path);
	void init(const std::string &path, const std::string &name);
	void init(const std::string &path, const std::string &name, const std::string &ext);

	const std::string ext(void) const;
	const std::string lowerext(void) const;
	const std::string filename(void) const;
	const std::string path(void) const;
	const std::string fullpath(void) const;

	void setpath(const std::string newpath);
	void setfilename(const std::string newfilename);
	void setextention(const std::string newextention);
	void appendfilename(const std::string appendstr);
	void appendextention(const std::string appendstr);
	void setpath(const char* newpath);
	void setfilename(const char* newfilename);
	void setextention(const char* newextention);
	void appendfilename(const char* appendstr);
	void appendextention(const char* appendstr);

}; // of class FileParts

inline FileParts::FileParts() :
		pos_path_end(0),
		pos_ext_begin(0)
{
	// default constructor. doing nothing
}

inline FileParts::FileParts(const char *FileParts):std::string(FileParts)
{
	init();
}

inline FileParts::FileParts(const std::string &FileParts):std::string(FileParts)
{
	init();
}

inline FileParts::FileParts(const char *path, const char *name)
{
	init(std::string(path),std::string(name));
}

inline FileParts::FileParts(const std::string &path, const std::string &name)
{
	init(path,name);
}

inline FileParts::FileParts(const char *path, const char *name, const char *ext)
{
	init(std::string(path),std::string(name),std::string(ext));
}

inline FileParts::FileParts(const std::string &path, const std::string &name, const std::string &ext)
{
	init(path,name,ext);
}

inline void FileParts::init()
{
	// scan string and find where the last filesep character is
	// currently use '/' as filesep.
	size_t pos_path_end = find_last_of('/');
	if (pos_path_end == length()) {
		// No path
		insert(0,"./");
		pos_path_end = 1;
	}

	// scan FileParts and find where the last . character is
	size_t pos_ext_begin = rfind('.');
	if (pos_ext_begin == 0) {
		// no ext
		pos_ext_begin = length();
	}

	verifypath();
	verifyextention();
}

inline void FileParts::init(const std::string &path)
{
	clear();
	append(path);
	init();
}

inline void FileParts::init(const std::string &path, const std::string &name)
{
	clear();
	append(path);
	// check if the end of path is not filesep
	if (path[path.length()-1] != '/') {
		append("/");
	}
	append(name);
	init();
}

inline void FileParts::init(const std::string &path,const std::string &name,const std::string &ext)
{
	clear();
	init(path,name);
	if (ext[0] != '.') {
		append(".");
	}
	append(ext);
	init();
}

inline void FileParts::verifypath(void)
{
//  if (pathstr[pathstr.length()-1] != '/')
//  {
//      pathstr += "/";
//  }
}

inline void FileParts::verifyextention(void)
{
	// probably not needed
}

inline const std::string FileParts::ext(void) const
{
	if (pos_ext_begin < length()) {
		return(substr(pos_ext_begin));
	}
	else {
		return(std::string(""));
	}

}

inline const std::string FileParts::lowerext(void) const
{
	std::string lowerextentionstr = ext();
	for (unsigned int i=0; i<lowerextentionstr.size(); i++)
		lowerextentionstr[i] = tolower(lowerextentionstr[i]);

	return(lowerextentionstr);
}

inline const std::string FileParts::filename(void) const
{
	return(substr(pos_path_end+1,pos_ext_begin-pos_path_end));
}

inline const std::string FileParts::path(void) const
{
	return(substr(0,pos_path_end+1));
}

inline const std::string FileParts::fullpath(void) const
{
	return(*this);
}

inline void FileParts::setpath(const std::string newpath)
{
//  pathstr = newpath;
//  verifypath();
//  reconstruct_fullpath();

	//
	replace(0,pos_path_end, <#const _Tp __old_value#>, <#const _Tp __new_value#>)
}

inline void FileParts::setfilename(const std::string newfilename)
{
	filenamestr = newfilename;
	reconstruct_fullpath();
}

inline void FileParts::setextention(const std::string newextention)
{
	extentionstr = newextention;
	verifyextention();
	reconstruct_fullpath();
}

inline void FileParts::appendfilename(const std::string appendstr)
{
	filenamestr += appendstr;
	reconstruct_fullpath();
}

inline void FileParts::appendextention(const std::string appendstr)
{
	extentionstr += appendstr;
	reconstruct_fullpath();
}

inline void FileParts::setfilename(const char* newfilename)
{
	setfilename(std::string(newfilename));
}

inline void FileParts::setextention(const char* newextention)
{
	setextention(std::string(newextention));
}

inline void FileParts::appendfilename(const char* appendstr)
{
	appendfilename(std::string(appendstr));
}

inline void FileParts::appendextention(const char* appendstr)
{
	appendextention(std::string(appendstr));
}

inline void FileParts::reconstruct_fullpath(void)
{
	fullpathstr = pathstr + filenamestr + extentionstr;
}
#endif

// vmlRootPath
//
/** This func returns a particular subpath of the main vml root directory.
 For example, to grab a specific image in the testVectors folder,
 you can call it like this:
 string s = vmlRootPath("test/testVectors/test_file.jpg");
*/
inline const std::string vmlRootPath(const std::string &subPath)
{
	std::string vmlRootPath;
	const       std::string vmlStr("vml");
	char        path[1024];

	// Find the last occurrence of "vml" in the path.
	std::string pathStr(path);
	std::string::size_type loc = pathStr.rfind(vmlStr);

	// Nothing found? Just get out as cleanly as possible.
	if (loc == std::string::npos)
	{
		std::cerr << "Warning: vml path not found" << std::endl;
		return std::string("");
	}

	// Make new string from path by clipping off
	// everything after "vml/"
	path[loc + vmlStr.length() + 1] = 0;

	// Append subpath and return.
	vmlRootPath = std::string(path);
	if (vmlRootPath[vmlRootPath.length() - 1] != '/')
		vmlRootPath += "/";
	vmlRootPath += subPath;

	return vmlRootPath;
}


// saving cv::Mat in raw binary serialization
// can be read in octave using octReadCvMat
void save_raw(const cv::Mat &mat,
              const char* savename);

// saving generic 3D arrays
template <typename T>
void save_raw(const T *map,
		      const int height,
		      const int width,
		      const int nbands,
		      const char* savename);

// saving generic 1D arrays
template <typename T>
void save_raw(const T *data,
              const int size,
              const char* savename);

// saving generic 2D arrays
template <typename T>
void save_raw(const T **data,
			  const int dim1,
			  const int dim2,
			  const char *savename);

} // of namespace vml

namespace std {
// a utility template class for making 2D matrix of generic types
// this is a row major matrix
// currently have minimum required methods and operators.
// feel free to add more methods/operators as needed
	template<class T>
	class matrix : vector<T>
	{
		// private member
		size_t  nrows;
		size_t  ncols;

	public:
		// constructors
		matrix()
			:vector<T>(),
			 nrows(0),
			 ncols(0) {};

		matrix(size_t rows, size_t cols, const T& value = T())
			:vector<T>(rows*cols, value),
			 nrows(rows),
			 ncols(cols) {};

		size_t rows() {
			return nrows;
		};
		size_t cols() {
			return ncols;
		};

		void resize (size_t n_r, size_t n_c, T c = T())
		{
			nrows = n_r;
			ncols = n_c;
			vector<T>::resize(n_r*n_c, c);
		};

		// access operators
//		T & operator() (size_t r, size_t c)
//		{
//			return vector<T>::at(r*ncols+c);
//		};

		// for integer indexing..
		T & operator() (int r, int c)
		{
			return vector<T>::at((size_t)r*ncols+(size_t)c);
		};

		// for unsigned int indexing..
		T & operator() (unsigned int r, unsigned int c)
		{
			return vector<T>::at(r*ncols+c);
		};
	};

} // of namespace std

#endif  // UTIL_H

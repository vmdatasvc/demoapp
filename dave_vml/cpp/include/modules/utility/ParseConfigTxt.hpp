/*
 * ParseConfigTxt.hpp
 *
 *  Created on: May 18, 2016
 *      Author: paulshin
 */

#ifndef INCLUDE_MODULES_UTILITY_PARSECONFIGTXT_HPP_
#define INCLUDE_MODULES_UTILITY_PARSECONFIGTXT_HPP_

#include <string>
#include <map>

#include <boost/lexical_cast.hpp>


class ParseConfigTxt {
public:

	ParseConfigTxt(const std::string& configpath);
	~ParseConfigTxt() {};

	unsigned int paramSize() {
		return mParams.size();
	}

	template<class T>
	bool getValue(const std::string& param, T& value) const;

	// NOTE: The following functions are deprecated; please use the single getValue above
	bool getValue_int(const std::string& param, int &value) const { return getValue(param, value);}
	bool getValue_long(const std::string& param, long int &value) const {return getValue(param, value);}
	bool getValue_uint(const std::string& param, uint &value) const {return getValue(param, value);}
	bool getValue_float(const std::string& param, float &value) const {return getValue(param, value);}
	bool getValue_double(const std::string& param, double &value) const {return getValue(param, value);}
	bool getValue_bool(const std::string& param, bool &value) const {return getValue(param, value);}
	bool getValue_string(const std::string& param, std::string &value) const {return getValue(param, value);}
	bool getValue_charArr(const std::string& param, char *value, unsigned int buffLength) const;


private:
	void parse(const std::string& configpath);

	std::map<std::string, std::string> mParams;		// -- {param_name, param_value}

};


template <class T>
bool ParseConfigTxt::getValue(const std::string& param, T& value) const{
	bool success = false;
	std::map<std::string, std::string>::const_iterator it = mParams.find(param);
	if(it != mParams.end()){
		try{
			value =boost::lexical_cast<T>(it->second);
			success = true;
		} catch(boost::bad_lexical_cast& e){}
	}

	return success;
}


#endif /* INCLUDE_MODULES_UTILITY_PARSECONFIGTXT_HPP_ */


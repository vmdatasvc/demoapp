/*
 * vml_geometry.hpp
 *
 *  Created on: Jul 12, 2016
 *      Author: paulshin
 */

#ifndef INCLUDE_MODULES_UTILITY_VML_GEOMETRY_HPP_
#define INCLUDE_MODULES_UTILITY_VML_GEOMETRY_HPP_

#include "modules/utility/vml_math.hpp"

/*
dot = x1*x2 + y1*y2      # dot product
det = x1*y2 - y1*x2      # determinant
angle = atan2(det, dot)  # atan2(y, x) or atan2(sin, cos)

Calculate the angular difference between two points
Return: [-180, 180] degree
*/
double AngleInTwoPoints(int x1, int y1, int x2, int y2) {
	double dot = x1*x2 + y1*y2;      // dot product
	double det = x1*y2 - y1*x2;      // determinant
	double angle = atan2(det, dot);  // atan2(y, x) or atan2(sin, cos)

	angle = angle*180/PI;

	return angle;
}


#endif /* INCLUDE_MODULES_UTILITY_VML_GEOMETRY_HPP_ */

/*
 * vml_math.hpp
 *
 *  Created on: Jul 12, 2016
 *      Author: paulshin
 */

#ifndef INCLUDE_MODULES_UTILITY_VML_MATH_HPP_
#define INCLUDE_MODULES_UTILITY_VML_MATH_HPP_


#define PI 	3.14159265358979

// -- Refer to http://stackoverflow.com/questions/2114797/compute-median-of-values-stored-in-vector-c
// Get the median of an unordered set of numbers of arbitrary
// type without modifying the underlying dataset.
template <typename It>
typename std::iterator_traits<It>::value_type Median(It begin, It end)
{
    using T = typename std::iterator_traits<It>::value_type;
    std::vector<T> data(begin, end);
    std::nth_element(data.begin(), data.begin() + data.size() / 2, data.end());
    return data[data.size() / 2];
}

#endif /* INCLUDE_MODULES_UTILITY_VML_MATH_HPP_ */

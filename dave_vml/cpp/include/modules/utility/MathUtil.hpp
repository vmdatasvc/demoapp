/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
/*
 * MathUtil.hpp
 *
 *  Created on: Mar 10, 2016
 *      Author: yyoon
 */

#ifndef MATHUTIL_HPP
#define MATHUTIL_HPP

#include <cmath>
#include <vector>

#include <opencv2/core.hpp>

namespace vml {

float MahalanobisDistance(float x1, float y1, float x2, float y2, float Sinv[]);

// use Gonzalez's farthest point clustering to find epsilon covering of a pointset
//
template <class T>
void epsilonCoveringFarthestFirst(std::vector<T> &x, float epsilon, std::vector<T> &y)
{
	unsigned int N = x.size();

	std::vector<T> *_x;
	bool delete_x = false;
	if (&x == &y)
	{
		_x = new std::vector<T>;
		_x->resize(x.size());
		for (unsigned int i=0;i<x.size();i++)
		{
			(*_x)[i] = x[i];
		}
		delete_x = true;
	}
	else
	{
		_x = &x;
	}

	y.clear();

	// add the first point
	y.push_back((*_x)[0]);

	for (int i=1;i<N;i++)
	{
		float max_dist = -1.f;
		int max_index = -1;
		for (unsigned int j=0;j<y.size();j++)
		{
			double _dist = y[j].dist((*_x)[i]);
			if (_dist > epsilon && _dist > max_dist)
			{
				max_dist = _dist;
				max_index = j;
			}
		}

		if (max_index >= 0)
		{
			// add j to the cover
			y.push_back((*_x)[i]);
		}

	}

	if (delete_x)
	{
		delete _x;
	}
}

template <class T, class C>
void onlineClustering(std::vector<T> &x, float radius, std::vector<C> &clusters)
{
	// assuming cluster_idx is already allocated. No sanity check.

	// make sure cluster centers is empty
//	cluster_centers.clear();

	// add the first element to cluster centers
//	cluster_centers.push_back(C(x[0]));
//	num_clusters = 1;
//
	for (unsigned int i=0;i<x.size();i++)
	{
		bool in_cluster = false;
		for (int j=0;j<clusters.size();j++)
		{
			if (clusters[j].dist(x[i]) < radius)
			{
				// add this element to the cluster
				in_cluster = true;
				clusters[j].reavg(x[i]);
//				cluster_idx[i] = j;
			}
		}

		if (!in_cluster)
		{
		 	// add this element to the set of clusters
			clusters.push_back(C(x[i]));
//			num_clusters++;
		}
	}
}

bool calcContourAxes(std::vector<cv::Point> &contour, cv::Point2f &major, cv::Point2f &minor, float &major_length, float &minor_length);

// calculate distance from point p0, to line segment (p1,p2)
// if p0 is out of the line segment range, (meaning if the angle between p1->p2 and p1->p0 is obtuse) dist will be negative
template<typename T>
double lineSegmentDistance(cv::Point_<T> p0,cv::Point_<T> p1, cv::Point_<T> p2)
{
	if (p1 == p2)
	{
		return static_cast<double>(-1);
	}
	const double	epsilon = 1e-5;
	cv::Point_<T> v1 = p0-p1;
	cv::Point_<T> v2 = p2-p1;
	double dotp = static_cast<double>(v1.x*v2.x+v1.y*v2.y);
	if ( dotp < 0.f)
	{
		// this is in obtuse angle. return positive distance
		return static_cast<double>(-1);
	}

	v1 = p0-p2;
	v2 = p1-p2;
	dotp = static_cast<double>(v1.x*v2.x+v1.y*v2.y);
	if (dotp < 0.f)
	{
		return static_cast<double>(-1);
	}

	double crossp = fabs(static_cast<double>(v2.x*v1.y-v2.y*v1.x));

	if (crossp < epsilon)
	{
		// this means the point and the linesegment is (almost)colinear
		// from the two point check above, it's clear that p0 is inbetween p1 and p2;
		return epsilon;
	}

	double v2norm = sqrt(static_cast<double>(v2.x*v2.x+v2.y*v2.y));
	return crossp/v2norm;
}

// check if two line segments intersect, and if so, optionally return the intersection point
// reference: http://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
template<typename T>
bool lineSegmentIntersect(cv::Point_<T> l1_b, cv::Point_<T> l1_e,
		cv::Point_<T> l2_b, cv::Point_<T> l2_e,
		cv::Point_<T> *intersection)
{
	cv::Point_<T>	x = l2_b - l1_b;
	cv::Point_<T>	d1 = l1_e - l1_b;
	cv::Point_<T>	d2 = l2_e - l2_b;

	double denom = static_cast<double>(d1.x*d2.y-d1.y*d2.x);	// d1 X d2
	double t_numer = static_cast<double>(x.x*d2.y - x.y*d2.x);	// x X d2
	double u_numer = static_cast<double>(x.y*d1.x - x.x*d1.y);	// -x X d1
	if (fabs(denom) < 1e-8)		// some sufficient epsilon
	{
		if (fabs(t_numer) < 1e-8)
		{
			// Case1: the two lines are collinear
			double d1_mag_sq = static_cast<double>(d1.x*d1.x+d1.y*d1.y);
			double t0 = static_cast<double>(x.x*d1.x+x.y*d1.y)/d1_mag_sq;
			double t1 = t0+static_cast<double>(d2.x*d1.x+d2.y*d1.y)/d1_mag_sq;
			if (fabs(t0)>1.f && fabs(t1)>1.f)
			{
				// two line segments are disjoint
				return false;
			}
			else
			{
				// two line segments overlap. but a single intersection point is not specified.
				// report t0
				if (intersection)
				{
					*intersection = l1_b + t0*d1;
				}
				return true;
			}
		}
		else{
			// Case2: this line segments are parallel to each other.
			return false;
		}
	}
	else
	{
		double t = t_numer/denom;
		double u = u_numer/denom;

		if ((t>=0.f)&&(t<=1.f)&&(u>=0.f)&&(u<=1.f))
		{
			// Case3: these line segments intersect
			if (intersection)
			{
				*intersection = l1_b+t*d1;
			}
			return true;
		}
		else
		{
			// Case4: these two line segments don't intersect
			return false;
		}
	}
}

// check if two lines that are defined by given line segments intersect, and if so, optionally return the intersection point
// http://stackoverflow.com/questions/7446126/opencv-2d-line-intersection-helper-function
template<typename T>
bool lineIntersect(cv::Point_<T> l1_b, cv::Point_<T> l1_e, cv::Point_<T> l2_b, cv::Point_<T> l2_e, cv::Point_<T> *intersection)
{
	// TODO: this is not implemented yet.
	return false;
}

} // of namespace vml

#endif  // MATHUTIL_HPP

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * MultiTargetFaceTracker.hpp
 *
 *  Created on: Jan 26, 2016
 *      Author: dkim
 */

#ifndef MULTITARGETFACETRACKER_HPP_
#define MULTITARGETFACETRACKER_HPP_

#include <opencv2/core/core.hpp>
#include <core/Settings.hpp>
#include <modules/ml/FaceDetector.hpp>
#include <modules/ml/FaceBaseTracker.hpp>
#include <modules/ml/FaceTrajectory.hpp>
#include <modules/ml/FaceData.hpp>
#include <modules/ml/Recognition.hpp>
#include <modules/iot/IOTCommunicator.hpp>

namespace vml {

class FaceModuleManager;

class MultiTargetFaceTracker {
public:

	MultiTargetFaceTracker();
	~MultiTargetFaceTracker();

	void initializeVideo(std::string fname);
	void initializeModules();

	void setVideoModule(boost::shared_ptr<FaceVideoModule> video_module);

	// settings methods
	void setIOTCommunicator(boost::shared_ptr<IOT_Communicator> iotc);
	void readSettings(const std::string& fname,bool merge);
	void setSettings(boost::shared_ptr<SETTINGS> settings);
	boost::shared_ptr<SETTINGS> getSettings() {return mpSettings;}

	// single thread
	void 			singleThreadStart();
	static void* 	singleProcessThread(void *apdata);
	void 			singleProcessLoop(void);

	// multithreading methods
	// 2 threads methods
	void 			_2ThreadPipeliningStart();
	static void* 	_2ThreadPipeliningDetectorThread(void *apdata);
	void 			_2ThreadPipeliningDetectorLoop(void);
	static void* 	_2ThreadPipeliningUpdateThread(void *apdata);
	void 			_2ThreadPipeliningUpdateLoop(void);

	// 3 threads methods
	void 			_3ThreadPipeliningStart();
	static void* 	_3ThreadPipeliningFrameGrabThread(void *apdata);
	void 			_3ThreadPipeliningFrameGrabLoop(void);
	static void* 	_3ThreadPipeliningDetectorThread(void *apdata);
	void 			_3ThreadPipeliningDetectorLoop(void);
	static void* 	_3ThreadPipeliningUpdateThread(void *apdata);
	void 			_3ThreadPipeliningUpdateLoop(void);

	// method to stop threads
	void stopThreads();
	void stopThreads(int numThread);
	bool isRunning() {return !mStopThreads;}

	bool process(void);	// grab a new image frame and update the current target list

	const cv::Mat&  getGrabbedImage(void);
//	const cv::Mat&  getForeground(void);
//	const cv::Mat&  getBlobImage(void);
//	const cv::Mat& 	getBackground(void);
	const cv::Mat& 	getBackProjection(void);

//	void getUndistortImage(cv::Mat &cur_undist);

	// indicators
	int getCurrentFrameNumber();
	unsigned int	getNumActiveTargets();
	unsigned int	getNumTotalTargets();
	unsigned int	getNumInactiveTargets();
	unsigned int	getNumCompletedTargets();

	void getCompletedTrajectories(std::vector<FaceTrajectory> &trajlist);
	void getCompletedFaceData(std::vector<TargetFaceData> &fdlist);

	// display methods

	void drawActiveTargets(cv::Mat &display_image);
	void drawActiveTargetsWithDMG(cv::Mat &display_image);
	void drawActiveTrajectories(cv::Mat &display_image);
	void drawValidNewTargets(cv::Mat &display_image);

//	void drawUndistortedActiveTargets(cv::Mat &display_image);
	void drawInactiveTargets(cv::Mat &display_image);
	void drawDetectedTargets(cv::Mat &display_image);
	void drawActiveTargetMap(cv::Mat &display_image);
	void drawTrackingRegions(cv::Mat &display_image);

	void setTrackingMethod(const FaceTrackingMethods tm);


private:

	// send a heartbeat
	void sendHeartBeat(void);

	unsigned int nFrames;
	bool 		mDoDemoRecog;
	bool		mDoDemoRecogEnd;

	// multithreading members
	pthread_t	mProcessorThread;
	pthread_t	mGrabberThread;
	pthread_t	mDetectorThread;
	pthread_t	mUpdateThread;

	pthread_attr_t mThreadAttr;

	bool		mStopThreads;
	bool		mbGrabReady;
	bool		mbUpdateReady;
	bool		mbDetectorReady;
	bool		mbMultithreadsOn;

	// completed face data
	std::vector<TargetFaceData>	mCompletedFaceData;

	// heatbeat
	double 	mHeartbeatInterval;
	double 	mPreviousHeartbeatTime;
	double 	mMaxHeartbeatIntervalProportion;

	// cacheing vision module pointers
	boost::shared_ptr<SETTINGS>					mpSettings;
	boost::shared_ptr<FaceVideoModule>			mpVideoModule;
	boost::shared_ptr<FaceDetector>				mpFaceDetector;
	boost::shared_ptr<FaceTargetManager>		mpFaceTargetManager;
	boost::shared_ptr<aitvml::Recognition>		mpDMGRecognizer;
	boost::shared_ptr<IOT_Communicator>			mpIOTCommunicator;

};

} // of namespace vml

#endif

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * FaceTrajectory.hpp
 *
 *  Created on: Aug 3, 2016
 *      Author: dkim
 */

#ifndef FACETRAJECTORY_HPP_
#define FACETRAJECTORY_HPP_

#include "opencv2/core/core.hpp"

namespace vml {

class FaceTrajectory {
public:
	struct TrajectoryPoint {

		float dist(const TrajectoryPoint other,bool floor_length = false) const;
		cv::Point	undistort() const;

		cv::Point	loc;
		double		time;
	};

	FaceTrajectory():mID(0) {}
	~FaceTrajectory() {}

	void addTrajectoryPoint(cv::Point loc, double t);
	void setID(unsigned int id) {mID=id;}

	// return trajectory for output
	const std::vector<TrajectoryPoint> & getTrajectory(void) const {return mTrajectoryPts;}
//	const std::vector<TrajectoryPoint> & getTrajectory(void) const;

	// return first point time
	double getStartTime(void) const;

	// return last point time
	double getEndTime(void) const;

	// motion model here
	// currently just use the previous location and enlarged window size

	// merging trajectories
	void append(FaceTrajectory &tj);
	void prepend(FaceTrajectory &tj);

	int nodeCount() const {return mTrajectoryPts.size();}
	float length(bool floor_length = false) const;	//
	double duration() const;	// returns the duration of the trajectory in second(s)

	// Report method
	std::string toXmlString(unsigned int decimals) const;

	void draw(cv::Mat &disp_image) const;

private:

	unsigned int					mID;
	std::vector<TrajectoryPoint>	mTrajectoryPts;
};

} // of namespace vml

//#endif

#endif


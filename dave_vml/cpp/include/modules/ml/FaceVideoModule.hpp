/****************************************************************************//*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * FaceVideoModule.hpp
 *
 *  Created on: Aug 3, 2016
 *      Author: dkim
 */

#ifndef VML_FACEVIDEOMODULE_HPP_
#define VML_FACEVIDEOMODULE_HPP_

#include <core/Video.hpp>

namespace vml {

struct FaceFrame
{
	FaceFrame(int rows, int cols);

	// methods
	bool grab(cv::VideoCapture &vc, double epoch_time);
	const double frametime(void) const {return time;}

	void copyTo(FaceFrame &ot);

	// members
	cv::Mat 	image;
	cv::Mat 	undistorted_image;
	//RGBbImage		image;
	//RGBbImage		undistorted_image;
	double	time;
};

class FaceVideoModule: public VideoBuffer<FaceFrame>
{
public:
	
	FaceVideoModule();
	//FaceVideoModule(boost::shared_ptr<SETTINGS> settings);
	virtual ~FaceVideoModule();
	
	//virtual void initialize(std::string video_file_name);
	virtual void initialize(boost::shared_ptr<SETTINGS> settings,std::string video_file_name);
	virtual bool grabFrame(void);

	// methods for camera model
	void getCurrentUndistortedImage(cv::Mat &undist_img);
	void undistortImage(cv::Mat &input_img,cv::Mat &undistorted_image);

	boost::shared_ptr<CameraModelOpenCV> getCameraModel(void) {return mpCameraModel;}

	// for the ROI region
	bool insideTrackRegionBox(cv::Rect box);
	bool insideTrackRegion(cv::Point pos);
	bool insideStartKillRegion(cv::Point pos);
	bool insideNoStartRegion(cv::Point pos);

	void drawTrackRegions(cv::Mat &img);

	// enforcing points to be inside window
	void enforceWindowRect(std::vector<cv::Point> &pts);
	void enforceWindowRect(cv::Point &pt);
	cv::Point enforceWindowRect(const cv::Point pt);

	// enforcing rectangle to be inside window
	void enforceWindowRect(cv::Rect &rect) const {rect &= mWindowRect;}

	// get trackingRegion
	std::vector<cv::Point> getTrackingRegion(void) {return mTrackingRegion;}

	float getFrameRate();

	// preallocate temp frames
	void allocateTempFrames(int numThread);

	//** MultiThreading methods - 2 threads
	// Methods for new frame buffer allocation per image frame
	void setGrabFrameToCurrent(void);

	// methods for in-place image frame process
	void copyGrabFrameToCurrent(void);
	void copyCurrentToGrabFrame(void);
	boost::shared_ptr<FaceFrame> getGrabFrame(void){return mpGrabFrame;}

	// Methods for new frame buffer allocation per image frame
	bool grabToGrabFrame(void);
	void setGrabFrameToDetectorFrame(void);
	void setDetectorFrameToCurrent(void);

	// methods for in-place image frame process
	bool grabInGrabFrame(void);
	void copyGrabFrameToDetectorFrame(void);
	void copyDetectorFrameToCurrent(void);
	void copyCurrentToDetectorFrame(void);
	boost::shared_ptr<FaceFrame> getDetectorFrame(void){return mpDetectorFrame;}


private:	

	cv::Rect	mWindowRect;

	// contour for tracking region
	std::vector<cv::Point>	mTrackingRegion;
	// contours for no start region
	std::vector<std::vector<cv::Point> >	mNoStartRegion;
	std::vector<cv::Point> mStartKillRegion;
	//float	mStartRegionOffset;

	// member goes here if necessary
	boost::shared_ptr<CameraModelOpenCV>	mpCameraModel;
	boost::shared_ptr<SETTINGS> mpSettings;

	// for multithreads
	boost::shared_ptr<FaceFrame>		mpGrabFrame;
	boost::shared_ptr<FaceFrame>		mpDetectorFrame;
	boost::shared_ptr<FaceFrame>		mpUpdateFrame;

};

} // of namespace vml

#endif

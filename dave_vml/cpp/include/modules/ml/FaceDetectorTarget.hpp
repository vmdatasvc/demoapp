/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * FaceDetectorTarget.hpp
 *
 *  Created on: Aug 8, 2016
 *      Author: dkim
 */

#ifndef FACEDETECTORTARGET_HPP_
#define FACEDETECTORTARGET_HPP_

#include "opencv2/core/core.hpp"
#include <boost/shared_ptr.hpp>


namespace vml {

class FaceVideoModule;
class FaceModuleManager;

class FaceDetectorTarget
{
	friend class FaceTargetManager;
	friend class DetectedTargetContoursAreaCompare;
	friend class DetectedTargetContour1AreaCompare;
	friend class DetectedTargetContour2AreaCompare;
public:

	FaceDetectorTarget(cv::Rect fd);
	~FaceDetectorTarget();

	float EuclideanDistance(FaceDetectorTarget &ot) const;
	float EuclideanDistance(cv::Point pos) const;

	cv::Point getCenter() const {return mCenter;}

	void setInvalid() {mValid = false;}
	bool isValid() {return mValid;}

	void evalRegionStatus();

	bool isInTrackRegion() {return mInTrackRegion;}
	bool isInStartRegion() {return mInStartRegion;}
	bool isInTrackStartRegion() {return (mInTrackRegion||mInStartRegion);}


	// contour represents the convex hull
	std::vector<std::vector<cv::Point> >		contours;
	//std::vector<boost::shared_ptr<std::vector<cv::Point> > >	contours;	// pointers to constituting contours
	int						contours_area;			// sum of areas of the contituting contours
	std::vector<cv::Point> 	contour1;		// optional contour of the target. Could be convex hull
	int						contour1_area;
	std::vector<cv::Point> 	contour2;  		// optional contour. Could be expected person shape based on the centroid location
	int						contour2_area;
	//cv::Point				center;			// centroid of the detected target
	cv::Rect				bounding_box;	// bounding box of the target
	bool					valid;			// flag for validation

	cv::Point				mCenter;



private:
	bool mValid;	// flag used for clearing
	bool mInTrackRegion;
	bool mInStartRegion;

	// Pointers to Vision Modules
	boost::shared_ptr<FaceVideoModule>	 	mpVideoModule;		// cacheing pointer for saving access time


};

// functor for detected target contours area comparison
class DetectedTargetContoursAreaCompare {
public:
	int operator() (const boost::shared_ptr<FaceDetectorTarget> ct1,
			const boost::shared_ptr<FaceDetectorTarget> ct2)
	{
		return (ct1->contours_area > ct2->contours_area);
	}
};

class DetectedTargetContour1AreaCompare {
public:
	int operator() (const boost::shared_ptr<FaceDetectorTarget> ct1,
			const boost::shared_ptr<FaceDetectorTarget> ct2)
	{
		return (ct1->contour1_area > ct2->contour1_area);
	}
};

class DetectedTargetContour2AreaCompare {
public:
	int operator() (const boost::shared_ptr<FaceDetectorTarget> ct1,
			const boost::shared_ptr<FaceDetectorTarget> ct2)
	{
		return (ct1->contour2_area > ct2->contour2_area);
	}
};


} // of namespace vml
#endif

#ifndef SVMC_HPP
#define SVMC_HPP

#include <modules/ml/AITPort/Image.hpp>

#include "modules/ml/SVM2.hpp"        // debugging purpose

using namespace std;

namespace aitvml
{

class SVMC
{

public:

 // SVM<unsigned char> gSvm;

  SVM<float> gSvm;

  string modelFile;
  int imgWidth, imgHeight;

  // Constructor
  SVMC():imgWidth(0),imgHeight(0)
  {
  }

  // Destructor
  virtual ~SVMC()
  {
  }
  
  void init(string modelFile, int imgWidth, int imgHeight);

  void correctLeqImage(aitvml::Image8 faceImage, aitvml::Image8& outImage, float X, float Y, float S, float O);
  float classify(aitvml::Image8 faceImage, aitvml::Image8& outImage, float X, float Y, float S, float O);
  float classify(aitvml::Image8 faceImage);
};

} // namespace aitvml

#endif //SVMC_HPP

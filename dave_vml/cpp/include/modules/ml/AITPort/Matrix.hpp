#ifndef _MATRIX_HPP_
#define _MATRIX_HPP_

//#pragma once

#include <math.h>

#include <iostream>
#include <fstream>

#include <modules/ml/AITPort/BasicMatrix.hpp>

/**
 * This class carries the expected functionality of a matrix
 * class. At this point it is assumed that the data type
 * <class T> are of a numerical type 
 * (i.e., int, float, double, complex).
 */

namespace aitvml
{

template<class T> class Matrix : public BasicMatrix<T>
{
public:
  Matrix(void) : BasicMatrix<T>() {};
  explicit Matrix(const uint size) : BasicMatrix<T>(size) {};
  Matrix(const uint rows, const uint cols) : BasicMatrix<T>(rows,cols) {};
  Matrix<T>& id(void);
  Matrix<T>& zero(void) { this->setAll(T(0)); return *this; };
  Matrix<T>& mult(const T value);
  Matrix<T>& mult(const Matrix<T>& A);
  Matrix<T>& mult(const Matrix<T>& A,const Matrix<T>& B);
  Matrix<T>& sub(const T value);
  Matrix<T>& sub(const Matrix<T>& A);
  Matrix<T>& sub(const Matrix<T>& A,const Matrix<T>& B);
  Matrix<T>& absSub(const Matrix<T>& A,const Matrix<T>& B);
  Matrix<T>& add(const T value);
  Matrix<T>& add(const Matrix<T>& A);
  Matrix<T>& add(const Matrix<T>& A,const Matrix<T>& B);
  Matrix<T>& div(const T value);

  Matrix<T>& elemMult(const Matrix<T>& A);
  Matrix<T>& elemMult(const Matrix<T>& A,const Matrix<T>& B);

  Matrix<T>& elemDiv(const Matrix<T>& A);
  Matrix<T>& elemDiv(const Matrix<T>& A,const Matrix<T>& B);

  void swapRows(int row_a, int row_b);
  void swapCols(int col_a, int col_b);

  double dot(const Matrix<T>& A);

  T det(void);
  T detFast(T *tmp1,uint *tmp2);
  T inv(void);
  T invFast(Matrix<T> &mx,
    	    Matrix<T> &t1, 
	    Matrix<unsigned int> &t2); 

  int svd(Matrix<T>& U, Matrix<T>& V);

  void QRdecomp(Matrix<T>& Q, Matrix<T>& R, int method = 0);

  T norm(void) const;
  T minimum(void) const;
  T minimum(int& r, int& c) const;
  T maximum(void) const;
  T maximum(int& r, int& c) const;

  double sum(void) const;

  double mean(void) const;
  double stdev(void) const;

  Matrix<T>& operator=(const int zeroVal) { PVASSERT(zeroVal==0); return zero(); }

  Matrix<T>& operator+=(const T value) { return add(value); }
  Matrix<T>& operator+=(const Matrix<T>& A) { return add(A); }

  Matrix<T>& operator-=(const T value) { return sub(value); }
  Matrix<T>& operator-=(const Matrix<T>& A) { return sub(A); }

  Matrix<T>& operator*=(const T value) { return mult(value); }
  Matrix<T>& operator*=(const Matrix<T>& A) { return mult(A); }
  
  Matrix<T>& operator/=(const T value) { return div(value); }
  

protected:

  void LUBackSubst(T *b,uint *indx);
  void LUdecomp(uint *indx,T *vv,int &d);

  void QRdecomp_Givens(Matrix<T>& Q, Matrix<T>& R);
  void QRdecomp_Householder(Matrix<T>& Q, Matrix<T>& R);

private:
  T pythag(T a, T b);
};


/**
 ** 
 ** Protected Helper Functions
 **
 */

template<class T> void Matrix<T>::LUBackSubst(T *b,uint *indx)
{

  int i,j,ii=-1,ip;
  T sum;

  for(i=0;i<(int)this->nrows;i++)
    {
      ip=indx[i];
      sum=b[ip];
      b[ip]=b[i];
      if (ii>=0)
    for(j=ii;j<=i-1;j++) sum -= this->mat[i][j]*b[j];
      else 
	if(sum) ii=i;
      b[i]=sum;
    }
  for(j=this->nrows-1;j>=0;j--)
    {
      sum=b[j];
      for (i=j+1;i<(int)this->nrows;i++) sum -= this->mat[j][i]*b[i];
      b[j]=sum/this->mat[j][j];
    }
}

template<class T> void Matrix<T>::LUdecomp(uint *indx,T *vv,int &d)
{
  const T TINY=(T)1e-20;
  uint i,imax,j,k;
  T big,dum,sum,temp;

  d=1;
  for(i=0;i<this->nrows;i++)
    {
      big=0;
      for (j=0;j<this->nrows;j++)
    if ((temp=(T)fabs(this->mat[i][j])) > big) big=temp;
      if (big == 0)
	{
	  d=0;
	  return;
	}
      vv[i]=(T)1.0/big;
  }
  for(j=0;j<this->nrows;j++)
    {
      for (i=0;i<j;i++) 
	{
      sum=this->mat[i][j];
      for (k=0;k<i;k++) sum-=this->mat[i][k]*this->mat[k][j];
      this->mat[i][j]=sum;
	}
      big=0;
      for (i=j;i<this->nrows;i++)
	{
      sum=this->mat[i][j];
      for (k=0;k<j;k++) sum-=this->mat[i][k]*this->mat[k][j];
      this->mat[i][j]=sum;
	  if((dum=vv[i]*(T)fabs(sum))>=big) 
	    {
	      big=dum;
	      imax=i;
	    }
	}
      if(j!=imax) 
	{
      for (k=0;k<this->nrows;k++)
	    {
          dum=this->mat[imax][k];
          this->mat[imax][k]=this->mat[j][k];
          this->mat[j][k]=dum;
	    }
	  d = -d;
	  vv[imax]=vv[j];
	}
      indx[j]=imax;
      if (this->mat[j][j]==0) this->mat[j][j]=TINY;
      if (j != this->nrows-1)
	{
      dum=(T)1.0/this->mat[j][j];
      for (i=j+1;i<this->nrows;i++) this->mat[i][j]*=dum;
	}
    }
}

/**
 * id()
 * 
 * Set all diagonal elements to 1, all others to zero.
 *
 * <input> Variable depending on method.
 *
 * void
 *
 * <output>
 *
 * void
 *
 */
template<class T> Matrix<T>& Matrix<T>::id(void)
{
  uint r,c;
  T *dst=this->pointer();
    
  for(r=0;r<this->nrows;r++)
    for(c=0;c<this->ncols;c++)
      {
	if(r==c) *(dst++)=1;
	else *(dst++)=0;
      }

  return *this;
}

/**
 * mult()
 * 
 * Perform matrix or scalar multiplication.
 *
 * Overloaded methods:
 *
 *    mult(const T factor)         : multiply every element with 'factor'
 *    mult(const Matrix<T>& A)   : 'this'='this'*A;
 *    mult(const Matrix<T>& A,B) : 'this'=A*B;
 *
 * <input> Variable depending on method.
 *
 * const T factor        : scalar value
 * const Matrix<T>& A  : matrix
 * const Matrix<T>& B  : matrix
 *
 * <output>
 *
 * void
 *
 * <error handling>
 * 
 * If a dimension mismatch occurs, the program is terminated.
 *
 */
template<class T> Matrix<T>& Matrix<T>::mult(const T factor)
{
  uint i,s=this->nrows*this->ncols;
  T *dst=this->pointer();
    
  for(i=0;i<s;i++) *(dst++)*=factor;

  return *this;
}

template<class T> Matrix<T>& Matrix<T>::mult(const Matrix<T>& A)
{
  uint i,j,k;
  T **mx;

  if(this->ncols!=A.nrows)
    {
      PvUtil::exitError("Matrix.mult: Matrix dimensions don't agree!\n");
    }

  mx=this->mx_alloc(this->nrows,A.ncols);

  for(i=0;i<this->nrows;i++)
    for(j=0;j<A.ncols;j++)
      {
	mx[i][j]=0;
    for(k=0;k<this->ncols;k++)
	  {
        mx[i][j]+=this->mat[i][k]*A.mat[k][j];
	  }
      }

  this->mx_delete();
  this->mx_set(mx,this->nrows,A.ncols);

  return *this;
}

template<class T> Matrix<T>& Matrix<T>::mult(const Matrix<T>& A,const Matrix<T>& B)
{
  uint i,j,k;

  if(A.ncols!=B.nrows) 
    {
      PvUtil::exitError("Matrix.mult: Matrix dimensions don't agree!\n");
    }

  this->mx_assert(A.nrows,B.ncols);

  for(i=0;i<this->nrows;i++)
    for(j=0;j<this->ncols;j++)
      {
    this->mat[i][j]=A.mat[i][0]*B.mat[0][j];
	for(k=1;k<A.ncols;k++)
	  {
        this->mat[i][j]+=A.mat[i][k]*B.mat[k][j];
	  }
      }

  return *this;
}

/**
 * add()
 * 
 * Perform matrix or scalar addition.
 *
 * Overloaded methods:
 *
 *    add(const T value)          : add every element with 'value'
 *    add(const Matrix<T>& A)   : 'this'='this'+A;
 *    add(const Matrix<T>& A,B) : 'this'=A+B;
 *
 * <input> Variable depending on method.
 *
 * const T value         : scalar value
 * const Matrix<T>& A  : matrix
 * const Matrix<T>& B  : matrix
 *
 * <output>
 *
 * void
 *
 * <error handling>
 * 
 * If a dimension mismatch occurs, the program is terminated.
 *
 */
template<class T> Matrix<T>& Matrix<T>::add(const T value)
{
  uint i,s=this->nrows*this->ncols;
  T *dst=this->pointer();
    
  for(i=0;i<s;i++) *(dst++)+=value;

  return *this;
}

template<class T> Matrix<T>& Matrix<T>::add(const Matrix<T>& A)
{
  uint i,s=this->nrows*this->ncols;
  T *dst=this->pointer(),*src=A.pointer();

  if(this->ncols!=A.ncols || this->nrows!=A.nrows)
    {
      PvUtil::exitError("Matrix.add: Matrix dimensions don't agree!\n");
    }

  for(i=0;i<s;i++) *(dst++)+=*(src++);

  return *this;
}

template<class T> Matrix<T>& Matrix<T>::add(const Matrix<T>& A,const Matrix<T>& B)
{
  uint i,s=A.nrows*A.ncols;
  T *dst,*src1,*src2;

  if(B.ncols!=A.ncols || B.nrows!=A.nrows) 
    {
      PvUtil::exitError("Matrix.add: Matrix dimensions don't agree!\n");
    }

  this->mx_assert(A.nrows,A.ncols);

  src1=A.pointer();
  src2=B.pointer();
  dst=this->pointer();

  for(i=0;i<s;i++) *(dst++)=*(src1++)+*(src2++);

  return *this;
}

/**
 * sub()
 * 
 * Perform matrix or scalar addition.
 *
 * Overloaded methods:
 *
 *    sub(const T value)          : sub 'value' from every element
 *    sub(const Matrix<T>& A)   : 'this'='this'-A;
 *    sub(const Matrix<T>& A,B) : 'this'=A-B;
 *
 * <input> Variable depending on method.
 *
 * const T value         : scalar value
 * const Matrix<T>& A  : matrix
 * const Matrix<T>& B  : matrix
 *
 * <output>
 *
 * void
 *
 * <error handling>
 * 
 * If a dimension mismatch occurs, the program is terminated.
 *
 */
template<class T> Matrix<T>& Matrix<T>::sub(const T value)
{
  uint i,s=this->nrows*this->ncols;
  T *dst=this->pointer();
    
  for(i=0;i<s;i++) *(dst++)-=value;

  return *this;
}

template<class T> Matrix<T>& Matrix<T>::sub(const Matrix<T>& A)
{
  uint i,s=this->nrows*this->ncols;
  T *dst=this->pointer(),*src=A.pointer();

  if(this->ncols!=A.ncols || this->nrows!=A.nrows)
    {
      PvUtil::exitError("Matrix.sub: Matrix dimensions don't agree!\n");
    }

  for(i=0;i<s;i++) *(dst++)-=*(src++);

  return *this;
}

template<class T> Matrix<T>& Matrix<T>::sub(const Matrix<T>& A,const Matrix<T>& B)
{
  uint i,s=A.nrows*A.ncols;
  T *dst,*src1,*src2;

  if(B.ncols!=A.ncols || B.nrows!=A.nrows) 
    {
      PvUtil::exitError("Matrix.sub: Matrix dimensions don't agree!\n");
    }

  this->mx_assert(A.nrows,A.ncols);

  src1=A.pointer();
  src2=B.pointer();
  dst=this->pointer();

  for(i=0;i<s;i++) *(dst++)=*(src1++)-*(src2++);

  return *this;
}

template<class T> Matrix<T>& Matrix<T>::absSub(const Matrix<T>& A,const Matrix<T>& B)
{
  uint i,s=A.nrows*A.ncols;
  T *dst,*src1,*src2;

  if(B.ncols!=A.ncols || B.nrows!=A.nrows) 
  {
    PvUtil::exitError("Matrix.sub: Matrix dimensions don't agree!\n");
  }

  mx_assert(A.nrows,A.ncols);

  src1=A.pointer();
  src2=B.pointer();
  dst=this->pointer();

  for(i=0;i<s;i++) 
  {
    if(*(src1) > *(src2))
      *(dst++)=*(src1++)-*(src2++);
    else
      *(dst++)=*(src2++)-*(src1++);
  }

  return *this;
}

/**
 * div()
 * 
 * Perform scalar division. Divide every element by a given 'value'.
 *
 * <input>
 *
 * const T value         : scalar value
 *
 * <output>
 *
 * void
 *
 * <error handling>
 * 
 * n/a
 *
 */
template<class T> Matrix<T>& Matrix<T>::div(const T value)
{
  uint i,s=this->nrows*this->ncols;
  T *dst=this->pointer();
    
  for(i=0;i<s;i++) *(dst++)/=value;

  return *this;
}

/**
 * elemMult()
 * 
 * Perform matrix element-wise multiplication.
 *
 * Overloaded methods:
 *
 *    elemMult(const Matrix<T>& A)   : 'this'='this'.*A;
 *    elemMult(const Matrix<T>& A,B) : 'this'=A.*B;
 *
 * <input> Variable depending on method.
 *
 * const Matrix<T>& A  : matrix
 * const Matrix<T>& B  : matrix
 *
 * <output>
 *
 * void
 *
 * <error handling>
 * 
 * If a dimension mismatch occurs, the program is terminated.
 *
 */
template<class T> Matrix<T>& Matrix<T>::elemMult(const Matrix<T>& A)
{
  uint i,s=this->nrows*this->ncols;
  T *dst=this->pointer(),*src=A.pointer();

  if(this->ncols!=A.ncols || this->nrows!=A.nrows)
    {
      PvUtil::exitError("Matrix:elemMult: Matrix dimensions don't agree!\n");
    }

  for(i=0;i<s;i++) *(dst++) *= (*(src++));

  return *this;
}

template<class T> Matrix<T>& Matrix<T>::elemMult(const Matrix<T>& A,const Matrix<T>& B)
{
  uint i,s=A.nrows*A.ncols;
  T *dst,*src1,*src2;

  if(B.ncols!=A.ncols || B.nrows!=A.nrows) 
    {
      PvUtil::exitError("Matrix.sub: Matrix dimensions don't agree!\n");
    }

  mx_assert(A.nrows,A.ncols);

  src1=A.pointer();
  src2=B.pointer();
  dst=this->pointer();

  for(i=0;i<s;i++) *(dst++)=(*(src1++)) * (*(src2++));

  return *this;
}

/**
 * elemDiv()
 * 
 * Perform matrix element-wise division.
 *
 * Overloaded methods:
 *
 *    elemMult(const Matrix<T>& A)   : 'this'='this'./A;
 *    elemMult(const Matrix<T>& A,B) : 'this'=A./B;
 *
 * <input> Variable depending on method.
 *
 * const T value         : scalar value
 * const Matrix<T>& A  : matrix
 * const Matrix<T>& B  : matrix
 *
 * <output>
 *
 * void
 *
 * <error handling>
 * 
 * If a dimension mismatch occurs, the program is terminated.
 *
 */
template<class T> Matrix<T>& Matrix<T>::elemDiv(const Matrix<T>& A)
{
  uint i,s=this->nrows*this->ncols;
  T *dst=this->pointer(),*src=A.pointer();

  if(this->ncols!=A.ncols || this->nrows!=A.nrows)
    {
      PvUtil::exitError("Matrix.sub: Matrix dimensions don't agree!\n");
    }

  for(i=0;i<s;i++) *(dst++) /= *(src++);

  return *this;
}

template<class T> Matrix<T>& Matrix<T>::elemDiv(const Matrix<T>& A,const Matrix<T>& B)
{
  uint i,s=A.nrows*A.ncols;
  T *dst,*src1,*src2;

  if(B.ncols!=A.ncols || B.nrows!=A.nrows) 
    {
      PvUtil::exitError("Matrix.sub: Matrix dimensions don't agree!\n");
    }

  mx_assert(A.nrows,A.ncols);

  src1=A.pointer();
  src2=B.pointer();
  dst=this->pointer();

  for(i=0;i<s;i++) *(dst++)=(*(src1++)) / (*(src2++));

  return *this;
}

/**
 * norm()
 * 
 * Return the L2 norm of all matrix elements combined, i.e.
 * norm=sqrt(sum a_i^2), i=0..rows*cols-1.
 * 
 * Note: A function sqrt must be defined for the type T.
 *
 * <input>
 *
 * void
 *
 * <output>
 *
 * T : norm of the matrix
 *
 * <error handling>
 * 
 * n/a
 *
 */
template<class T> T Matrix<T>::norm(void) const
{
  T v,norm;
  T *src=this->pointer();
  uint i,s=this->ncols*this->nrows;

  norm=0;

  for(i=0;i<s;i++)
    {
      v=*(src++);
      norm+=v*v;
    }

  return((T)sqrt(norm));
}

/**
 * minimum()
 * 
 * Return the smallest element of the matrix.
 *
 * <input>
 *
 * void
 *
 * <output>
 *
 * T : minimum value
 *
 * <error handling>
 * 
 * n/a
 *
 */
template<class T> T Matrix<T>::minimum(void) const
{
  uint i,s=this->ncols*this->nrows;
  T *src=this->pointer();
  T c,mini=*src;
  
  for(i=1;i<s;i++)
    {
      c=*(src++);
      if(c<mini) mini=c;
    }

  return mini;
}

/**
 * minimum()
 * 
 * Return the smallest element of the matrix.
 *
 * <input>
 *
 * void
 *
 * <output>
 *
 * T : minimum value
 * r : row of maximum value
 * c : column of maximum value
 *
 * <error handling>
 * 
 * n/a
 *
 */
template<class T> T Matrix<T>::minimum(int& r, int& c) const
{
  uint i,j,s=this->ncols*this->nrows;
  T *src=this->pointer();
  T v,mini=*src;
  
  r=c=0;

  //for(i=1;i<s;i++)
  for(i=0;i<this->ncols;i++)
    for(j=0;j<this->nrows;j++)
    {
      v=*(src++);
      if(v<mini) 
      {
        mini=v;
        r=j;
        c=i;
      }
    }

  return mini;
}

/**
 * maximum()
 * 
 * Return the largest element of the matrix.
 *
 * <input>
 *
 * void
 *
 * <output>
 *
 * T : maximum value
 *
 * <error handling>
 * 
 * n/a
 *
 */
template<class T> T Matrix<T>::maximum(void) const
{
  uint i,s=this->ncols*this->nrows;
  T *src=this->pointer();
  T c,maxi=*src;
  
  for(i=1;i<s;i++)
    {
      c=*(src++);
      if(c>maxi) maxi=c;
    }

  return maxi;
}


/**
 * maximum()
 * 
 * Return the largest element of the matrix.
 *
 * <input>
 *
 * void
 *
 * <output>
 *
 * T : maximum value
 * r : row of maximum value
 * c : column of maximum value
 *
 * <error handling>
 * 
 * n/a
 *
 */
template<class T> T Matrix<T>::maximum(int& r, int& c) const
{
  uint i,j,s=this->ncols*this->nrows;
  T *src=this->pointer();
  T v,maxi=*src;
  
  r=c=0;

  //for(i=1;i<s;i++)
  for(j=0;j<this->nrows;j++)
    for(i=0;i<this->ncols;i++)
    {
      v=*(src++);
      if(v>maxi) 
      {
        maxi=v;
        r=j;
        c=i;
      }
    }

  return maxi;
}


/**
 * sum()
 * 
 * Return the sum of all elements of the matrix.
 *
 * <input>
 *
 * void
 *
 * <output>
 *
 * double : sum value
 *
 * <error handling>
 * 
 * n/a
 *
 */
template<class T> double Matrix<T>::sum(void) const
{
  uint i,s=this->nrows*this->ncols;
  T *src=this->pointer();
  double sum=0;

  for(i=0;i<s;i++)
    {
      sum+=*(src++);
    }

  return sum;
}

/**
 * mean()
 * 
 * Return the average value of all elements of the matrix.
 *
 * <input>
 *
 * void
 *
 * <output>
 *
 * double : mean value
 *
 * <error handling>
 * 
 * n/a
 *
 */
template<class T> double Matrix<T>::mean(void) const
{
  uint i,s=this->nrows*this->ncols;
  T *src=this->pointer();
  double avg=0;

  for(i=0;i<s;i++)
    {
      avg+=*(src++);
    }

  return avg/s;
}

/**
 * stdev()
 * 
 * Return the standard deviation of all elements.
 *
 * <input>
 *
 * void
 *
 * <output>
 *
 * double : stdev value
 *
 * <error handling>
 * 
 * n/a
 *
 */
template<class T> double Matrix<T>::stdev(void) const
{
  uint i,s=this->nrows*this->ncols;
  T *src=this->pointer();
  double m=mean();
  double x,std=0;

  for(i=0;i<s;i++)
    {
      x=*(src++)-m;
      std+=x*x;
    }

  return std/s;
}


/**
 * dot()
 * 
 * Return the dot product of two vector matrices: dot = A*B.
 * Both matrices have to be single row or column matrix
 * 
 * <input>
 *
 * Matrix<T> B
 *
 * <output>
 *
 * T : dot product
 *
 * <error handling>
 * 
 * If either matrix is not a single colum or single row matrix, it will throw exception
 *
 */

template<class T> double Matrix<T>::dot(const Matrix<T>& B)
{
  PVASSERT((this->nrows==1 || this->ncols==1) && (B.nrows==1 || B.ncols==1) && (this->nrows*this->ncols == B.nrows*B.ncols));

  T* src1 = this->pointer();
  T* src2 = B.pointer();

  double sum=0;

  for(int i=0;i<this->ncols*this->nrows;i++)
    sum+=(*src1++)*(*src2++);

  return sum;
}

/**
 * det()
 * 
 * Return the determinant of the matrix.
 * 
 * The contents of the matrix will be destroyed! 
 *
 * For matrices of size greater than 3x3, two memory allocations 
 * are neccessary. If that is undesireable, the user
 * can use detFast().
 *
 * <input>
 *
 * void
 *
 * <output>
 *
 * T : determinant
 *
 * <error handling>
 * 
 * If the matrix is not squared, the program exits.
 *
 */
template<class T> T Matrix<T>::det(void)
{
  T *A=this->pointer();

  if(this->nrows!=this->ncols)
    {
      PvUtil::exitError("Matrix.det: Matrix not squared!\n");
    }

  // Use Cramers rule to perform fast inversion for cases 1x1,2x2,3x3.
  switch(this->nrows)
    {
    case 1: return A[0];
    case 2: return A[0]*A[3]-A[1]*A[2];
    case 3: return (A[0]*(A[3*1+1]*A[3*2+2]-A[3*1+2]*A[3*2+1])
		    -A[3*0+1]*(A[3*1+0]*A[3*2+2]-A[3*1+2]*A[3*2+0])
		    +A[3*0+2]*(A[3*1+0]*A[3*2+1]-A[3*1+1]*A[3*2+0]));
    default:
      {
	T d;
	uint i;
	int id;
    uint *indx=new uint [this->nrows];
    T *vv=new T [this->nrows];
	T *src;
	
	LUdecomp(indx,vv,id);
	
	d=(T)id;
	
    src=this->pointer();
	
    for(i=0;i<this->nrows;i++)
	  {
	    d*=*src;
        src+=this->nrows+1;
	  }

	delete [] vv;
	delete [] indx;
	return d;
      }
    }
}

/**
 * detFast()
 * 
 * Return the determinant of the matrix.
 * 
 * The contents of the matrix will be destroyed! 
 *
 * The user has to supply two arrays, that the procedure uses
 * as working memory. Both arrays must have the same length as
 * the matrix has rows/cols. One of type unsigned int, the other
 * of type T.
 *
 * <input>
 *
 * uint *indx : array [0..nrows] of type unsigned integers 
 * T    *vv   : array [0..nrows] of type T
 *
 * <output>
 *
 * T : determinant
 *
 * <error handling>
 * 
 * If the matrix is not squared, the program exits.
 *
 */
template<class T> T Matrix<T>::detFast(T *vv, uint *indx)
{
  T *A=this->pointer();

  if(this->nrows!=this->ncols)
    {
      PvUtil::exitError("Matrix.det: Matrix not squared!\n");
    }

  // Use Cramers rule to perform fast inversion for cases 1x1,2x2,3x3.
  switch(this->nrows)
    {
    case 1: return A[0];
    case 2: return A[0]*A[3]-A[1]*A[2];
    case 3: return (A[0]*(A[3*1+1]*A[3*2+2]-A[3*1+2]*A[3*2+1])
		    -A[3*0+1]*(A[3*1+0]*A[3*2+2]-A[3*1+2]*A[3*2+0])
		    +A[3*0+2]*(A[3*1+0]*A[3*2+1]-A[3*1+1]*A[3*2+0]));
    default:
      {
	T d;
	uint i;
	int id;
	T *src;

	LUdecomp(indx,vv,id);
	
	d=(T)id;
	
    src=this->pointer();
	
    for(i=0;i<this->nrows;i++)
	  {
	    d*=*src;
        src+=this->nrows+1;
	  }

	return d;
      }
    }
}

/**
 * inv()
 * 
 * Return the inverse of this matrix. For dimensions greater than 3x3, 
 * four memory allocations have to be performed. To avoid memory
 * allocations, the method invFast() can be used.
 *
 * <input>
 *
 * void
 *
 * <output>
 *
 * T: determinant of the matrix
 *
 * <error handling>
 * 
 * A return value of zero indicates, that the matrix is singular.
 * If the matrix is not squared, the program exits.
 *
 */
template<class T> T Matrix<T>::inv(void)
{
  T d;
  T *A=this->pointer();

  if(this->nrows!=this->ncols)
    {
      PvUtil::exitError("Matrix.inv: Matrix not squared!\n");
    }

  // Use Cramers rule to perform fast inversion for cases 1x1,2x2,3x3.
  switch(this->nrows)
    {
    case 1: 
      {
	d=A[0];
	A[0]=1/d;
	break;
      }
    case 2: 
      {	
	A[2]=-A[2];
	A[1]=-A[1];
	d=A[3];
	A[3]=A[0];
	A[0]=d;
	d=d*A[3]-A[1]*A[2];
	if(d==0) break;
	A[0]/=d;
	A[1]/=d;
	A[2]/=d;
	A[3]/=d;
	break;
      }
    case 3:
      {
	d=(A[0]*(A[3*1+1]*A[3*2+2]-A[3*1+2]*A[3*2+1])
	   -A[3*0+1]*(A[3*1+0]*A[3*2+2]-A[3*1+2]*A[3*2+0])
	   +A[3*0+2]*(A[3*1+0]*A[3*2+1]-A[3*1+1]*A[3*2+0]));
	T md;
	T B[9];
	
	if(d==0) break;

	d=1/d;
	md=-d;

	B[3*0+0]=d* (A[3*1+1]*A[3*2+2]-A[3*1+2]*A[3*2+1]);
	B[3*0+1]=md*(A[3*0+1]*A[3*2+2]-A[3*2+1]*A[3*0+2]);
	B[3*0+2]=d* (A[3*0+1]*A[3*1+2]-A[3*1+1]*A[3*0+2]);
	
	B[3*1+0]=md*(A[3*1+0]*A[3*2+2]-A[3*2+0]*A[3*1+2]);
	B[3*1+1]=d* (A[3*0+0]*A[3*2+2]-A[3*2+0]*A[3*0+2]);
	B[3*1+2]=md*(A[3*0+0]*A[3*1+2]-A[3*0+2]*A[3*1+0]);
	
	B[3*2+0]=d* (A[3*1+0]*A[3*2+1]-A[3*1+1]*A[3*2+0]);
	B[3*2+1]=md*(A[3*0+0]*A[3*2+1]-A[3*0+1]*A[3*2+0]);
	B[3*2+2]=d* (A[3*0+0]*A[3*1+1]-A[3*0+1]*A[3*1+0]);

	A[0]=B[0];A[1]=B[1];A[2]=B[2];
	A[3]=B[3];A[4]=B[4];A[5]=B[5];
	A[6]=B[6];A[7]=B[7];A[8]=B[8];

	break;
      }
    default:
      {
	Matrix<T> A(*this);
	uint i,j;
	int id;

    T *col=new T [this->nrows];
    uint *indx=new uint [this->nrows];
	
	A.LUdecomp(indx,col,id);
	
	d=(T)id;
    for(i=0;i<this->nrows;i++) d*=A.mat[i][i];
	if(d!=0) 
	  {
        for(j=0;j<this->nrows;j++)
	      {
        for(i=0;i<this->nrows;i++) col[i]=0;
		col[j]=1;
		A.LUBackSubst(col,indx);
        for(i=0;i<this->nrows;i++) this->mat[i][j]=col[i];
	      }
	  }
	delete [] indx;
	delete [] col;
      }
    }

  return d;
}

/**
 * invFast()
 * 
 * Return the inverse of a specified matrix. 
 * In additon, the user has to supply 
 * arrays that are used for temporary storage.
 *
 * Note: invFast() destorys the contents of mx!
 *
 * <input>
 *
 * Matrix<T> &mx               : matrix of which to calculate the inverse
 * Matrix<unsigned int> &tmp1  : tmp row vector [0..nrows] of 
 *		    		   type unsigned integers 
 * Matrix<T> &tmp2             : tmp row vector [0..nrows] of type T
 *
 * <output>
 *
 * T: determinant of the matrix
 *
 * <error handling>
 * 
 * A return value of zero indicates, that the matrix is singular.
 * If the matrix is not squared, the program exits.
 *
 */
template<class T> T Matrix<T>::invFast(Matrix<T> &mx,
					 Matrix<T> &t1, 
					 Matrix<unsigned int> &t2)
{
  T *tmp1;
  unsigned int *tmp2;

  t1.resize(1,mx.nrows);
  t2.resize(1,mx.nrows);

  tmp1=t1.pointer();
  tmp2=t2.pointer();

  this->resize(mx.nrows,mx.ncols);
  if(this->nrows!=this->ncols)
    {
      PvUtil::exitError("Matrix.inv: Matrix not squared!\n");
    }

  T d;
  T *A=mx.pointer();
  T *B=this->pointer();

  // Use Cramers rule to perform fast inversion for cases 1x1,2x2,3x3.
  switch(this->nrows)
    {
    case 1: 
      {
	d=A[0];
	B[0]=1/d;
	break;
      }
    case 2: 
      {	
	B[0]=A[3];
	B[1]=-A[2];
	B[2]=-A[1];
	B[3]=A[0];

	d=A[0]*A[3]-A[1]*A[2];
	if(d==0) break;
	B[0]/=d;
	B[1]/=d;
	B[2]/=d;
	B[3]/=d;
	break;
      }
    case 3:
      {
	d=(A[0]*(A[3*1+1]*A[3*2+2]-A[3*1+2]*A[3*2+1])
	   -A[3*0+1]*(A[3*1+0]*A[3*2+2]-A[3*1+2]*A[3*2+0])
	   +A[3*0+2]*(A[3*1+0]*A[3*2+1]-A[3*1+1]*A[3*2+0]));
	T md;
	
	if(d==0) break;

	d=1/d;
	md=-d;

	B[3*0+0]=d* (A[3*1+1]*A[3*2+2]-A[3*1+2]*A[3*2+1]);
	B[3*0+1]=md*(A[3*0+1]*A[3*2+2]-A[3*2+1]*A[3*0+2]);
	B[3*0+2]=d* (A[3*0+1]*A[3*1+2]-A[3*1+1]*A[3*0+2]);
	
	B[3*1+0]=md*(A[3*1+0]*A[3*2+2]-A[3*2+0]*A[3*1+2]);
	B[3*1+1]=d* (A[3*0+0]*A[3*2+2]-A[3*2+0]*A[3*0+2]);
	B[3*1+2]=md*(A[3*0+0]*A[3*1+2]-A[3*0+2]*A[3*1+0]);
	
	B[3*2+0]=d* (A[3*1+0]*A[3*2+1]-A[3*1+1]*A[3*2+0]);
	B[3*2+1]=md*(A[3*0+0]*A[3*2+1]-A[3*0+1]*A[3*2+0]);
	B[3*2+2]=d* (A[3*0+0]*A[3*1+1]-A[3*0+1]*A[3*1+0]);
	break;
      }
    default:
      {
	uint i,j;
	int id;
	
	mx.LUdecomp(tmp2,tmp1,id);
	
	d=(T)id;
    for(i=0;i<this->nrows;i++) d*=mx.mat[i][i];
	if(d!=0) 
	  {
        for(j=0;j<this->nrows;j++)
	      {
        for(i=0;i<this->nrows;i++) tmp1[i]=0;
		tmp1[j]=1;
		mx.LUBackSubst(tmp1,tmp2);
        for(i=0;i<this->nrows;i++) this->mat[i][j]=tmp1[i];
	      }
	  }
      }
    }

  return d;
}

//
// From Numerical Recipes
//

#define SIGN(a,b) ((b) >= 0.0 ? fabs(a) : -fabs(a))

template <class T> T Matrix<T>::pythag(T a, T b)
{
	T absa,absb;
	absa=(T)fabs(a);
	absb=(T)fabs(b);    
	if (absa > absb) 
    {
      T d = absb/absa;
      return (T)(absa*sqrt(1.0+d*d));
    }
	else 
    {
      T d = absa/absb;
      return (T)((absb == 0.0 ? 0.0 : absb*sqrt(1.0+d*d)));
    }
}

//template <class T> void Matrix<T>::svd/*(float **a, int m, int n, */float w*, float **v)
// calculates the singular value decomposition of this matrix
// IN: this matrix (mxn)
// OUT: this matrix=U(mxm), D(1,m), V(nxn)
//Note: this method DOES OVERWRITE this matrix with U
template <class T> int Matrix<T>::svd(Matrix<T>& D, Matrix<T>& V)
{
  int flag,i,its,j,jj,k,l,nm;
  T anorm,c,f,g,h,s,scale,x,y,z,*rv1;

  T** a=this->pointerpointer();
  int m=this->nrows;
  int n=this->ncols;

  T*  w=D.pointer();
  T** v=V.pointerpointer();

  rv1 = new T[n];


  g=scale=anorm=0.0;
  for (i=0;i<n;i++) {
    l=i+1;
    rv1[i]=scale*g;
    g=s=scale=0.0;
    if (i < m) {
      for (k=i;k<m;k++) scale += (T)fabs(a[k][i]);
      if (scale) {
        for (k=i;k<m;k++) {
          a[k][i] /= scale;
          s += a[k][i]*a[k][i];
        }
        f=a[i][i];
        g = -(T)(SIGN(sqrt(s),f));
        h=f*g-s;
        a[i][i]=f-g;
        for (j=l;j<n;j++) {
          for (s=0.0,k=i;k<m;k++) s += a[k][i]*a[k][j];
          f=s/h;
          for (k=i;k<m;k++) a[k][j] += f*a[k][i];
        }
        for (k=i;k<m;k++) a[k][i] *= scale;
      }
    }
    w[i]=scale *g;
    g=s=scale=0.0;
    if (i < m && i != (n-1)) {
      for (k=l;k<n;k++) scale += (T)fabs(a[i][k]);
      if (scale) {
        for (k=l;k<n;k++) {
          a[i][k] /= scale;
          s += a[i][k]*a[i][k];
        }
        f=a[i][l];
        g = -(T)(SIGN(sqrt(s),f));
        h=f*g-s;
        a[i][l]=f-g;
        for (k=l;k<n;k++) rv1[k]=a[i][k]/h;
        for (j=l;j<m;j++) {
          for (s=0.0,k=l;k<n;k++) s += a[j][k]*a[i][k];
          for (k=l;k<n;k++) a[j][k] += s*rv1[k];
        }
        for (k=l;k<n;k++) a[i][k] *= scale;
      }
    }
    anorm=(T)((std::max)(anorm,(T)(fabs(w[i])+fabs(rv1[i]))));
  }
  for (i=(n-1);i>=0;i--) 
  {
    if (i < (n-1)) 
    {
      if (g) 
      {
        for (j=l;j<n;j++)
          v[j][i]=(a[i][j]/a[i][l])/g;
        for (j=l;j<n;j++) {
          for (s=0.0,k=l;k<n;k++) s += a[i][k]*v[k][j];
          for (k=l;k<n;k++) v[k][j] += s*v[k][i];
        }
      }
      for (j=l;j<n;j++) v[i][j]=v[j][i]=0.0;
    }
    v[i][i]=1.0;
    g=rv1[i];
    l=i;
  }

  for (i=(std::min)(m-1,n-1);i>=0;i--) 
  {
    l=i+1;
    g=w[i];
    for (j=l;j<n;j++) a[i][j]=0.0;
    if (g) 
    {
      g=(T)(1.0/g);
      for (j=l;j<n;j++) 
      {
        for (s=0.0,k=l;k<m;k++) s += a[k][i]*a[k][j];
        f=(s/a[i][i])*g;
        for (k=i;k<m;k++) a[k][j] += f*a[k][i];
      }
      for (j=i;j<m;j++) a[j][i] *= g;
    } 
    else
      for (j=i;j<m;j++) a[j][i]=0.0;
    ++a[i][i];
  }

  for (k=n-1;k>=0;k--)
  {
    for (its=0;its<30;its++) 
    {
      flag=1;
      for (l=k;l>=0;l--) {
        nm=l-1;
        if ((T)(fabs(rv1[l])+anorm) == anorm) {
          flag=0;
          break;
        }
        if ((T)(fabs(w[nm])+anorm) == anorm) break;
      }
      if (flag) {
        c=0.0;
        s=1.0;
        for (i=l;i<=k;i++) {
          f=s*rv1[i];
          rv1[i]=c*rv1[i];
          if ((T)(fabs(f)+anorm) == anorm) break;
          g=w[i];
          h=pythag(f,g);
          w[i]=h;
          h=(T)(1.0/h);
          c=g*h;
          s = -f*h;
          for (j=0;j<m;j++)
          {
            y=a[j][nm];
            z=a[j][i];
            a[j][nm]=y*c+z*s;
            a[j][i]=z*c-y*s;
          }
        }
      }
      z=w[k];
      if (l == k) {
        if (z < 0.0) {
          w[k] = -z;
          for (j=0;j<n;j++) v[j][k] = -v[j][k];
        }
        break;
      }
      if (its == 30-1) 
      {      	
        delete rv1;
        return -1;
      }
      x=w[l];
      nm=k-1;
      y=w[nm];
      g=rv1[nm];
      h=rv1[k];
      f=(T)(((y-z)*(y+z)+(g-h)*(g+h))/(2.0*h*y));
      g=pythag(f,1.0);
      f=(T)(((x-z)*(x+z)+h*((y/(f+SIGN(g,f)))-h))/x);
      c=s=1.0;
      for (j=l;j<=nm;j++) 
      {
        i=j+1;
        g=rv1[i];
        y=w[i];
        h=s*g;
        g=c*g;
        z=pythag(f,h);
        rv1[j]=z;
        c=f/z;
        s=h/z;
        f=x*c+g*s;
        g = g*c-x*s;
        h=y*s;
        y *= c;
        for (jj=0;jj<n;jj++) {
          x=v[jj][j];
          z=v[jj][i];
          v[jj][j]=x*c+z*s;
          v[jj][i]=z*c-x*s;
        }
        z=pythag(f,h);
        w[j]=z;
        if (z) {
          z=(T)(1.0/z);
          c=f*z;
          s=h*z;
        }
        f=c*g+s*y;
        x=c*y-s*g;
        for (jj=0;jj<m;jj++)
        {
          y=a[jj][j];
          z=a[jj][i];
          a[jj][j]=y*c+z*s;
          a[jj][i]=z*c-y*s;
        }
      }
      rv1[l]=0.0;
      rv1[k]=f;
      w[k]=x;
    }
  }

  delete [] rv1;

  return 0;
}

#undef SIGN

template <class T> void Matrix<T>::QRdecomp(Matrix<T>& Q, Matrix<T>& R, int method)
{
  switch(method)
  {
  case 0:
    QRdecomp_Givens(Q, R);
    break;
  case 1:
    QRdecomp_Householder(Q, R);
    break;
  default:
      QRdecomp_Givens(Q, R);
  }
}

template <class T> void Matrix<T>::QRdecomp_Givens(Matrix<T>& Q, Matrix<T>& R)
{
    Matrix<T> q_copy, r_copy, r, rt;
    unsigned int row, col;
    T x, y, xydist, negsine, cosine;

    //make sure this is a square matrix
    PVASSERT(this->nrows==this->ncols);

    /* initialize the matrices */
    //matrix_malloc(&q_copy, q->rows, q->cols);
    q_copy.resize(this->nrows, this->ncols);
    //matrix_malloc(&r_copy, r->rows, r->cols);
    r_copy.resize(this->nrows, this->ncols);
    //matrix_malloc(&R, a->rows, a->cols);
    r.resize(this->nrows, this->ncols);
    //matrix_malloc(&Rt, a->cols, a->rows);
    rt.resize(this->nrows, this->ncols);
    //matrix_identity(q);
    Q.resize(this->nrows,this->ncols);
    Q.id();
    //matrix_copy(a, r);
    R = (*this);

    /* find each of the Givens rotation matrices */
    for (col=0; col<this->ncols-1; col++)
    {
        for (row=col+1; row<this->nrows; row++)
        {
            /* only compute a rotation matrix if the corresponding entry in
               the matrix is nonzero */
            //y = MATRIX_GET_ELEMENT(r, row, col);
            y = R(row,col);
            if (y != 0.0)
            {
                /* find the values of -sin and cos */
                //x = MATRIX_GET_ELEMENT(r, col, col);
                x = R(col,col);
                xydist = sqrt(x * x + y * y);
                negsine = y / xydist;
                cosine = x / xydist;
                
                /* calculate the R and R^t matrices */
                //matrix_identity(&R);
                r.id();
                //MATRIX_GET_ELEMENT(&R, col, col) = cosine;
                r(col,col) = cosine;
                //MATRIX_GET_ELEMENT(&R, col, row) = negsine;
                r(col,row) = negsine;
                //MATRIX_GET_ELEMENT(&R, row, col) = -negsine;
                r(row,col) = -negsine;
                //MATRIX_GET_ELEMENT(&R, row, row) = cosine;
                r(row,row) = cosine;
                //matrix_transpose(&R, &Rt);
                rt.transpose(r);

                /* "add" R to the q and r matrices */
                //matrix_copy(q, &q_copy);
                q_copy = Q;
                //matrix_copy(r, &r_copy);
                r_copy = R;
                //matrix_multiplication(&q_copy, &Rt, q);
                Q.mult(q_copy, rt);
                //matrix_multiplication(&R, &r_copy, r);
                R.mult(r, r_copy);
            }
        }
    }
}


template <class T> void Matrix<T>::QRdecomp_Householder(Matrix<T>& Q, Matrix<T>& R)
{
  PvUtil::exitError("Matrix::QRdecomp_Householder() is not implemented!");
}


//
// Operators
//

template <class T> Matrix<T> operator+(const Matrix<T>& m1, const Matrix<T>& m2)
{
  return Matrix<T>(m1.rows(),m1.cols()).add(m1,m2);
}

template <class T> Matrix<T> operator+(const Matrix<T>& m1, const T v)
{
  Matrix<T> tmp(m1);
  tmp += v;
  return tmp;
}

template <class T> Matrix<T> operator+(const T v, const Matrix<T>& m1)
{
  Matrix<T> tmp(m1);
  tmp += v;
  return tmp;
}

template <class T> Matrix<T> operator-(const Matrix<T>& m1, const Matrix<T>& m2)
{
  return Matrix<T>(m1.rows(),m1.cols()).sub(m1,m2);
}

template <class T> Matrix<T> operator-(const Matrix<T>& m1, const T v)
{
  Matrix<T> tmp(m1);
  tmp -= v;
  return tmp;
}

template <class T> Matrix<T> operator-(const T v, const Matrix<T>& m1)
{
  Matrix<T> tmp(m1);
  tmp += -v;
  return tmp;
}

template <class T> Matrix<T> operator*(const Matrix<T>& m1, const Matrix<T>& m2)
{
  return Matrix<T>(m1.rows(),m1.cols()).mult(m1,m2);
}

template <class T> Matrix<T> operator*(const Matrix<T>& m1, const T v)
{
  Matrix<T> tmp(m1);
  tmp *= v;
  return tmp;
}

template <class T> Matrix<T> operator*(const T v, const Matrix<T>& m1)
{
  Matrix<T> tmp(m1);
  tmp *= v;
  return tmp;
}

template <class T> Matrix<T> operator/(const Matrix<T>& m1, const T v)
{
  Matrix<T> tmp(m1);
  tmp /= v;
  return tmp;
}

template <class T> Matrix<T> elemMult(const Matrix<T>& m1, const Matrix<T>& m2)
{
  return Matrix<T>(m1.rows(),m1.cols()).elemMult(m1,m2);
}

template <class T> Matrix<T> elemDiv(const Matrix<T>& m1, const Matrix<T>& m2)
{
  return Matrix<T>(m1.rows(),m1.cols()).elemDiv(m1,m2);
}


template <class T> void Matrix<T>::swapRows(int row_a, int row_b)
{  
  PVASSERT(row_a<this->nrows && row_b<this->nrows);

  int i;  
  T* aItr;
  T* bItr;

  aItr = &(this->mat[row_a][0]);
  bItr = &(this->mat[row_b][0]);

  for(i=0; i<this->ncols; i++, aItr++, bItr++)
  {
    T t;
    t = *aItr;
    *aItr = *bItr;
    *bItr = t;
  }
}

template <class T> void Matrix<T>::swapCols(int col_a, int col_b)
{  
  PVASSERT(col_a<this->ncols && col_b<this->ncols);

  int i;  
  T* aItr;
  T* bItr;

  aItr = &(this->mat[0][col_a]);
  bItr = &(this->mat[0][col_b]);

  for(i=0; i<this->nrows; i++, aItr+=this->ncols, bItr+=this->ncols)
  {
    T t;
    t = *aItr;
    *aItr = *bItr;
    *bItr = t;
  }
}

} // namespace aitvml


#endif  // #ifndef _MATRIX_HPP_

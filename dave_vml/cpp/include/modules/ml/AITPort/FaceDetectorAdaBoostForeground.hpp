/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef FaceDetectorAdaBoostForeground_HPP
#define FaceDetectorAdaBoostForeground_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

#include "modules/ml/AITPort/String.hpp"

// LOCAL INCLUDES
//

#include "modules/ml/AITPort/FaceDetector.hpp"


#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include <iostream>
#include <stdio.h>

//#include "cv.h"
//#include "cvaux.h"
//#include <iostream.h>

// FORWARD REFERENCES
//

#include "modules/ml/AITPort/FaceDetectorAdaBoost_fwd.hpp"

namespace aitvml
{

namespace vision
{


/**
 * Implements AdaBoost face detector. Uses OpenCV implementation.
 * @par Responsibilities:
 * - Implements AdaBoost face detector. Uses OpenCV implementation
 * @par Design Considerations:
 * - Here you can itemize some justifications for your decisions. It will help
 * others to understand why your class is the way it is.
 * @remarks
 * Some special comments or warnings about this class.
 * @see TheirClass, OurClass (other related classes).
 */
class FaceDetectorAdaBoostForeground : public FaceDetector
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  FaceDetectorAdaBoostForeground();

  /**
   * Destructor
   */
  virtual ~FaceDetectorAdaBoostForeground();

  /**
   * Initialization.
   */
  void init(const std::string modelName, double scaleFactor = 1.2);

  //
  // OPERATIONS
  //

public:

  /**
   * Perform the actual detection.
   */
  void detect(const Image8 &image, bool asynch = true);
  void detect(const Image8 &image, const Image8 &foregroundImage, bool asynch = true);

  //
  // ACCESS
  //

public:

  /**
   * @see mNumNeighbors.
   */
  void setNumNeighbors(const int n) { mNumNeighbors = n; }

  //
  // INQUIRY
  //

  //
  // ATTRIBUTES
  //
  CvSize minSize, maxSize;

protected:

  cv::CascadeClassifier	mpCascade;

  bool mIsInitialized;

  /**
   * Number of Bodies that must be in the neighborhood to tell that
   * there is a Face in a particular location. This is a parameter
   * for cvHaarDetectObjects().
   */
  int mNumNeighbors;

  double mScaleFactor;

};


}; // namespace vision

}; // namespace aitvml

#endif // FaceDetectorAdaBoostForeground_HPP


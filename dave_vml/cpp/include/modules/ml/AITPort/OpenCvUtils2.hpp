/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * OpenCVUtils2.hpp
 *
 *  Created on: Jan 12, 2016
 *      Author: dkim
 */

#ifndef OpenCvUtils2_HPP
#define OpenCvUtils2_HPP

#include <boost/utility.hpp>
#include <modules/ml/AITPort/Image.hpp>

#include "opencv2/core/core.hpp"
#include "modules/ml/AITPort/String.hpp"

namespace aitvml
{

namespace vision
{


/**
 * This class contain functions that facilitate the interaction between
 * VideoMining's classes and Intel's OpenCV.
 * @remark OpenCV should be used only to experiment, we should write
 * or own classes that implement the functionality possibly using the
 * Intel IPP library.
 */
class OpenCvUtils2 : private boost::noncopyable
{

public:

	static void cvtMatToImage32(cv::Mat imat, aitvml::Image8 *img);
	static void cvtImage32ToMat(aitvml::Image32 img, cv::Mat *imat);

	static void cvtMatToImage8(cv::Mat imat, aitvml::Image8 *img);
	static void cvtImage8ToMat(aitvml::Image8 img, cv::Mat *imat);

};


}; // namespace vision

}; // namespace aitvml

#endif // OpenCvUtils2_HPP


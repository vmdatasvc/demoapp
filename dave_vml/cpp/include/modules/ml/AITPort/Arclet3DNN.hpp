#ifndef ARCLET3DNN_HPP
#define ARCLET3DNN_HPP

#include "modules/ml/AITPort/ModelPerson.hpp"
#include "modules/ml/AITPort/Matrix.hpp"

// For Torch3
/*
#include "torch3/src/DiskMatDataSet.h"
#include "torch3/src/ClassFormatDataSet.h"
#include "torch3/src/ClassNLLCriterion.h"
#include "torch3/src/MSECriterion.h"
#include "torch3/src/OneHotClassFormat.h"
#include "torch3/src/ClassMeasurer.h"
#include "torch3/src/MSEMeasurer.h"

#include "torch3/src/StochasticGradient.h"
#include "torch3/src/KFold.h"

#include "torch3/src/ConnectedMachine.h"
#include "torch3/src/Linear.h"
#include "torch3/src/Tanh.h"
#include "torch3/src/LogSoftMax.h"

#include "torch3/src/MeanVarNorm.h"
#include "torch3/src/DiskXFile.h"
#include "torch3/src/CmdLine.h"
#include "torch3/src/SafeRandom.h"
*/
#include "DiskMatDataSet.h"
#include "ClassFormatDataSet.h"
#include "ClassNLLCriterion.h"
#include "MSECriterion.h"
#include "OneHotClassFormat.h"
#include "ClassMeasurer.h"
#include "MSEMeasurer.h"

#include "StochasticGradient.h"
#include "KFold.h"

#include "ConnectedMachine.h"
#include "Linear.h"
#include "Tanh.h"
#include "LogSoftMax.h"

#include "MeanVarNorm.h"
#include "DiskXFile.h"
#include "CmdLine.h"
#include "SafeRandom.h"

#include "modules/ml/AITPort/lssvr.hpp"

//#define DEMOGRAPHICS_RECOGNITION_DEBUG

namespace aitvml
{

class Arclet3DNN
{

public:

  typedef struct _FileParams
  {
    char *valid_file;
    char *file;

    int max_load;
    int max_load_valid;
    realv accuracy;
    realv learning_rate;
    realv decay;
    int max_iter;
    int the_seed;

    char *dir_name;
    char *model_file;
    int k_fold;
    bool binary_mode;
    bool regression_mode;
    int class_against_the_others;
  } FileParams;

  FileParams mFileParams;

  Torch::SafeRandom mRandom;

  Torch::CmdLine mCmd;

  std::string mModelListFile, mModelDir, mLsvFile;
  int numInputs;
  int numTargets;
  int numHiddenUnits1, numHiddenUnits2, numHiddenUnits3;
  realv weight_decay;

  std::vector<ModelPersonPtr> mModel3D;
//  Arclet3D arclet[1000];
	Lsv *mLsv;
  std::vector< Torch::ConnectedMachine * > mMlp;
  std::vector< Torch::MeanVarNorm * > mMvNorm;
  std::vector< int > numInputTmp;
  int numModel;
  int numCloseModel;
  int maxNumModel;

  float mMidEyeX, mMidEyeY, mScale, mOrien;
  FILE *fp;
  float mEstMidEyeX, mEstMidEyeY, mEstMidEyeMouthDist, mEstOrientation;
  
  // Constructor
  Arclet3DNN()
  {
	  numInputs=0;
	  numTargets=0;
	  numHiddenUnits1=0;
	  numHiddenUnits2=0;
	  numHiddenUnits3=0;
	  weight_decay=0.f;
	  mLsv=NULL;

	  numModel=0;
	  numCloseModel=0;
	  maxNumModel=0;

	  mMidEyeX=0.f;
	  mMidEyeY=0.f;
	  mScale=0.f;
	  mOrien=0.f;

	  fp=NULL;
	  mEstMidEyeX=0.f;
	  mEstMidEyeY=0.f;
	  mEstMidEyeMouthDist=0.f;
	  mEstOrientation=0.f;
  }

  // Destructor
  virtual ~Arclet3DNN()
  {
    finish();
  }
  
  void init(std::string modelListFile_, std::string modelDir_);
  void init(std::string mModelListFile_, std::string mModelDir_, std::string mLsvFile_);

  void finish()
  {
    for (unsigned int i = 0; i < mMlp.size(); i++)
    {
      delete mMlp[i];
    }
    mMlp.clear();
    for (unsigned int i = 0; i < mMvNorm.size(); i++)
    {
      delete mMvNorm[i];
    }
    mMvNorm.clear();
    mModel3D.clear();
    numInputTmp.clear();
  }

  void get_args();
  void applyModel(int numEstModel, int wgt1, int wgt2, Image8 faceImage);
  void estimateModel(int numEstModel, int wgt1, int wgt2, Image8 faceImage, float detecX, float detecY, int detecW, int detecH);
  void estimateModel(int numEstModel, Image8 faceImage, float detecX, float detecY, int detecW, int detecH);
  void estimateModel(Image8 faceImage, float detecX, float detecY, int detecW, int detecH);
  void estimateRefined(int numEstModel, Image8 faceImage, Image8& outImage, float X, float Y, float S, float O);

  void affine(float *vecX, float *vecY, float scale, Matrix<float> rotMat, Matrix<float> rotCenter, Matrix<float> trans);
};

typedef boost::shared_ptr<Arclet3DNN> Arclet3DNNPtr;


} // namespace aitvml

#endif
//void affine(float *vecX, float *vecY, float scale, Matrix<float> rotMat, Matrix<float> rotCenter, Matrix<float> trans);

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef PVCOORD_HPP
#define PVCOORD_HPP

#include <math.h>

#include "modules/ml/AITPort/PvUtil.hpp"

#include <iostream>

#ifndef uint
//#define uint unsigned int
typedef unsigned int uint;
#endif

namespace aitvml
{

/*********************************************************************
 ** 
 ** Vector3 Template Class
 **
 *********************************************************************/

template<class T> class Vector3
{
public:

  Vector3(const T x0, const T y0, const T z0 = 0) : x(x0), y(y0), z(z0) {};

  Vector3(void) : x(0),y(0),z(0) {};

  Vector3(const Vector3& v0) : x(v0.x),y(v0.y),z(v0.z) {};

  virtual ~Vector3(void) {};

  Vector3& operator= (const Vector3& v0) {
    if(this != &v0) {
      x = v0.x; 
      y = v0.y; 
      z = v0.z;
    }
    return *this;
  };
	
	bool operator== (const Vector3& v0) const {
		return ((x == v0.x) && (y == v0.y) && (z == v0.z));
	}

  bool operator!= (const Vector3& v0) const {
		return ((x != v0.x) || (y != v0.y) || (z != v0.z));
  }

  T *pointer(void) { return &x; };

  void set(const T x0, const T y0, const T z0 = 0) {
    x = x0; y = y0; z = z0;
  }

  // Parenthesis operator x = v(0), y = v(1), z = v(2)
#ifdef PVBOUNDCHECK
  T& operator()(const uint i) { PVASSERT(i>=0&&i<=2); return (&x)[i];};
  T operator()(const uint i) const { PVASSERT(i>=0&&i<=2); return (&x)[i];};
#else
  T& operator()(const uint i) { return (&x)[i];};
  T operator()(const uint i) const { return (&x)[i];};
#endif

  void zero(void) { set(0,0,0);};

  // Scalar multiplication and division
  inline void mult(const T value);
  inline void div(const T value);

  // Dot product Multiplication
  inline T dot_prod(const Vector3& A) const;

  // Cross product Multiplication
  inline void x_prod(const Vector3& A);
  inline void x_prod(const Vector3& A,const Vector3& B);

  // Substraction and addition
  inline void sub(const T value);
  inline void sub(const Vector3 &A);
  inline void sub(const Vector3 &A,const Vector3 &B);
  inline void add(const T value);
  inline void add(const Vector3 &A);
  inline void add(const Vector3 &A,const Vector3 &B);

  // The square norm of the vector sqrt(x^2+y^2+z^2)
  inline T norm(void) const;

  // Divide x,y,z by the norm
  inline void normalize(void);

  // The square of the euclidian distance between two points
  friend T dist2(const Vector3 &A, const Vector3 &B) {
    Vector3 v(A.x-B.x,A.y-B.y,A.z-B.z);
    return v.x*v.x + v.y*v.y + v.z*v.z;
  };

  // Make this the projection of vector B into vector A
  inline void project(const Vector3 &A, const Vector3 &B);

  //The absolute angle between two vectors
  friend T angle(const Vector3 &A, const Vector3 &B)
  {
	  Vector3<T> a,b;
	  a=A; a.normalize();
	  b=B; b.normalize();	
	  
	  return acos(a.dot_prod(b));
  };


  //The signed angle between two vectors.
  // If B is clockwise of A the angle will be positive
  // If B is counter clockwise of A the angle will be negative
  friend T angle2(const Vector3 &A, const Vector3 &B)
  {
  	Vector3<T> a,b,p;
	T alpha, dot, s;
	
	a=A; a.normalize();
	b=B; b.normalize();	
	
	dot=a.dot_prod(b);
	
	if(dot>1.0 || dot<-1.0)
		return 0;
	else
		alpha = acos(dot);

  	//s=b.x*a.z-a.x*b.z;	  
    s=b.dot_prod(a);
	
	if(s<0) return -alpha;
	else return alpha;
	
	return 0;
  };

  // I/O operators
  friend std::ostream& operator<<(std::ostream &stream, const Vector3& s) {
    stream << s(0) << " " << s(1) << " " << s(2);
    return stream;
  };

  friend std::istream& operator>>(std::istream &stream, Vector3& s) {  
    stream >> s(0) >> s(1) >> s(2);
    return stream;
  };

public:

  T x,y,z;

};

/*********************************************************************
 ** 
 ** Public Member Functions
 **
 *********************************************************************/

/*********************************************************************
 * mult(), div()
 * 
 * Perform scalar multiplication and division
 *
 * <input>
 *
 * const T factor        : scalar value
 *
 * <output>
 *
 * void
 *
 * <error handling>
 * 
 * Nothing
 *
 *********************************************************************/
template<class T> void Vector3<T>::mult(const T factor)
{
  x *= factor; y *= factor; z *= factor;
}

template<class T> void Vector3<T>::div(const T val)
{
  PVASSERT(val);
  x /= val; y /= val; z /= val;
}

/*********************************************************************
 * dot_prod(), x_prod()
 * 
 * Perform dot or cross product multiplication.
 *
 * Overloaded methods:
 *
 *    dot_prod(const Vector3<T>& A)    : s = 'this' . A;
 *    x_prod(const Vector3<T>& A)      : 'this' = 'this' x A;
 *    x_prod(const Vector3<T>& A,B)    : 'this' = A x B;
 *
 * <input> Variable depending on method.
 *
 * const Vector3<T>& A   : vector
 * const Vector3<T>& B   : vector
 *
 * <output>
 *
 * void
 *
 * <error handling>
 * 
 * Nothing
 *
 *********************************************************************/

template<class T> T Vector3<T>::dot_prod(const Vector3<T>& A) const
{
  return x*A.x+y*A.y+z*A.z;
}

template<class T> void Vector3<T>::x_prod(const Vector3<T>& A)
{
  set(y*A.z-A.y*z,
      A.x*z-x*A.z,
      x*A.y-A.x*y);
}

template<class T> void Vector3<T>::x_prod(const Vector3<T>& A,
					   const Vector3<T>& B)
{
  set(A.y*B.z-B.y*A.z,
      B.x*A.z-A.x*B.z,
      A.x*B.y-B.x*A.y);
}

/*********************************************************************
 * add(), sub()
 * 
 * Perform additions and substractions.
 *
 * Overloaded methods:
 *
 *    add(const T s)                   : 'this' = 'this' + s
 *    add(const Vector3<T>& A)         : 'this' = 'this' + A;
 *    add(const Vector3<T>& A,B)       : 'this' = A + B;
 *    sub(const T s)                   : 'this' = 'this' - s
 *    sub(const Vector3<T>& A)         : 'this' = 'this' - A;
 *    sub(const Vector3<T>& A,B)       : 'this' = A - B;
 *
 * <input> Variable depending on method.
 *
 * const T s             : scalar
 * const Vector3<T>& A   : vector
 * const Vector3<T>& B   : vector
 *
 * <output>
 *
 * void
 *
 * <error handling>
 * 
 * Nothing
 *
 *********************************************************************/

template<class T> void Vector3<T>::sub(const T val)
{
  x -= val; y -= val; z -= val;
}

template<class T> void Vector3<T>::sub(const Vector3<T>& A)
{
  set(x-A.x, y-A.y, z-A.z);
}

template<class T> void Vector3<T>::sub(const Vector3<T>& A,
					       const Vector3<T>& B)
{
  set(A.x-B.x, A.y-B.y, A.z-B.z);
}

template<class T> void Vector3<T>::add(const T val)
{
  x += val; y += val; z += val;
}

template<class T> void Vector3<T>::add(const Vector3<T>& A)
{
  set(x+A.x, y+A.y, z+A.z);
}

template<class T> void Vector3<T>::add(const Vector3<T>& A,
					       const Vector3<T>& B)
{
  set(A.x+B.x, A.y+B.y, A.z+B.z);
}

/*********************************************************************
 * norm()
 * 
 * Calculate the square norm of the vector sqrt(x^2+y^2+z^2)
 *
 * <input> 
 *
 * Nothing
 *
 * <output>
 *
 * const T : scalar
 *
 * <error handling>
 * 
 * Nothing
 *
 *********************************************************************/

template<class T> T Vector3<T>::norm(void) const
{
  return (T)sqrt(x*x+y*y+z*z);
}

/*********************************************************************
 * normalize()
 * 
 * Normalize the vector by dividing each element by the norm. 
 * Vectors of zero length are not normalized (function does not throw an 'divide by zero' exception)
 *
 * <input> 
 *
 * Nothing
 *
 * <output>
 *
 * Nothing
 *
 * <error handling>
 * 
 * Nothing
 *
 *********************************************************************/

template<class T> void Vector3<T>::normalize(void)
{
	T n=norm();
	if(n>0) div(n);
}

/*********************************************************************
 * Project()
 * 
 * Calculate the projection of B into A
 *
 * <input> 
 *
 * const Vector3<T>& A   : vector
 * const Vector3<T>& B   : vector
 *
 * <output>
 *
 * nothing
 *
 * <error handling>
 * 
 * Nothing
 *
 *********************************************************************/

template<class T> void Vector3<T>::project(const Vector3 &A, const Vector3 &B)
{
  // *this = A*(A.B)/norm(A)
  *this = A;
  T n=A.norm();
  if(n>0)
  {
	mult(A.dot_prod(B)/n);
	//div(n);
  }
}


/*********************************************************************
 * Angle()
 * 
 * Calculate the smallest angle between two vectors
 *
 * <input> 
 *
 * const Vector3<T>& A   : vector
 * const Vector3<T>& B   : vector
 *
 * <output>
 *
 * float angle
 *
 * <error handling>
 * 
 * Nothing
 *
 *********************************************************************/
/*
template<class T> T angle(const Vector3 &A, const Vector3 &B)
{
	Vector3<T> a,b;
	a=A; a.normalize();
	b=B; b.normalize();	

	return acos(a.dot_prod(b));
}
*/

// Shorcut Macros

typedef Vector3<int> Vector3i;
typedef Vector3<unsigned int> Vector3ui;
typedef Vector3<float> Vector3f;
typedef Vector3<double> Vector3d;

// Let's define an interface for Vector2 to be implemented later

template<class T> class Vector2 : public Vector3<T> {};
typedef Vector3i Vector2i;
typedef Vector3ui Vector2ui;
typedef Vector3f Vector2f;
typedef Vector3d Vector2d;

} // namespace aitvml

#endif

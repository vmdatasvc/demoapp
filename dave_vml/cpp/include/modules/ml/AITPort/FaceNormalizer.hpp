/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef FaceNormalizer_HPP
#define FaceNormalizer_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

#include <modules/ml/AITPort/String.hpp>
#include <modules/ml/AITPort/FacialFeatureDetector.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

#include "modules/ml/AITPort/FaceNormalizer_fwd.hpp"

namespace aitvml
{


/**
 * This class is not yet documented.
 * Here is a more detailed description of the class. Next come the 
 * Responsibilities of the class. Try to minimize the responsibilities, this
 * will help in creating a good design. Next, optionally write any design patterns
 * that were used for this class. Refer to the "Design Patterns" book by Gamma et al.
 * @par Responsibilities:
 * - The First Responsibility
 * - The Second Responsibility:
 *   - A sub-responsibility
 *   - Another sub-responsibility
 * - The Third Responsibility
 * @par Patterns:
 * - The first design pattern
 * - The second design pattern
 * @par Design Considerations:
 * - Here you can itemize some justifications for your decisions. It will help
 * others to understand why your class is the way it is.
 * @remarks
 * Some special comments or warnings about this class.
 * @see TheirClass, OurClass (other related classes).
 */
class FaceNormalizer : private boost::noncopyable
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  FaceNormalizer();

  /**
   * Destructor
   */
  virtual ~FaceNormalizer();

  //
  // OPERATIONS
  //

public:


  //
  // ACCESS
  //
protected:

  FacialFeatureDetector ffd;


public:

  /* Normalize the face according to the eye location
     Postion of left eye is fixed, image is rotated such that 
     right eye is horizontal to it at a predetermined distance
  */

  // Facial Feature detector decoupled
  void faceNormalize(const Image8 &headImage,
                     Image8& normalizedImage,
                     Vector2f leftEye, Vector2f RightEye);

  //  Facial Feature Detector being called in this function
  bool faceNormalize(const Image8 &faceImage, 
                     const Image8 &headImage,
                     Image8& normalizedImage);


  //
  // INQUIRY
  //

public:


  //
  // ATTRIBUTES
  //

protected:


};


}; // namespace aitvml

#endif // FaceNormalizer_HPP


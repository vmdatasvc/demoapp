/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef AIT_strutil_HPP
#define AIT_strutil_HPP

#include <string>
#include <vector>

namespace aitvml
{

/**
 * Return a copy of string in with all occurrences of substring strOld replaced by strNew.
 * @remark This function is not very efficient, and should not be used for performance-critical
 * routines.
 */
std::string replace(const std::string& in, const std::string& strOld, const std::string& strNew);

/**
 * Return a list of the words of the string s. If the second argument specifies a 
 * string to be used as the word separator. The returned list will then have one 
 * more item than the number of non-overlapping occurrences of the separator in the 
 * string.
 * For example split("a,b,,c",",") = ["a","b","","c"]
 */
std::vector<std::string> split(const std::string& str, const std::string& token);

/**
 * Separate the given string into non empty strings using each character in
 * token_list as a delimiter. If to characters in the token list are consecutive
 * it will not return an empty item.
 * For example, tokenize("1-2.3--4",".-") = ["1","2","3","4"]
 */
std::vector<std::string> tokenize(const std::string& str, const std::string& token_list);

/**
 * Join several strings into one string, and use the second argument as separator.
 * @param v [in] vector of strings.
 * @param token [in] token used to join the elements.
 * Example: join(["1","2"],"/") = "1/2"
 */
std::string join(const std::vector<std::string>& v, const std::string& token);

/**
 * Normalize the path to match the unix conventions. This function replaces back-slashes
 * for forward slashes, and eliminates consecutive slashes.
 * @remark if the file input starts with two backslashes, it won't modify that section
 * in order to support windows UNC paths.
 */
std::string normalize_path(const std::string& in);

/**
 * First normalize the path name and separate the file name for the directory name.
 * It returns a vector with the directory as the first argument and the file name 
 * as the second. The directory name will include the final '/' character unless 
 * it is empty. That means that join(split_path(x),"") == normalize_path(x)
 * For example:
 * - split_path("a.txt") = ["","a.txt"]
 * - split_path("./a.txt") = ["./","a.txt"]
 * - split_path(".\a.txt") = ["./","a.txt"]
 * - split_path(".\a.txt") = ["./","a.txt"]
 * - split_path("/usr/local/") = ["/usr/local/",""]
 * - split_path("/usr/local") = ["/usr/","local"]
 * @remark The behavior with UNC names is undefined.
 */
std::vector<std::string> split_path(const std::string& str);

/**
 * Get the extension of the file.
 * Examples:
 * - get_file_extension("a.txt") = ".txt"
 * - get_file_extension("a") = ""
 * - get_file_extension("/usr/a.txt") = ".txt"
 * - get_file_extension("/usr/a.1.txt") = ".txt"
 */
std::string get_file_extension(const std::string& str);

/**
 * Get the file name. Remove any path information.
 * This is equivalent to split_path(str)[1].
 * - get_file_name("a.txt") = "a.txt"
 * - get_file_name("a") = "a"
 * - get_file_name("/usr/a.txt") = "a.txt"
 * - get_file_name("/usr/a.1.txt") = "a.1.txt"
 */
std::string get_file_name(const std::string& str);

/**
 * Get the file name without the extension. Remove any path information.
 * This is equivalent to split_path(str)[1].
 * - get_file_name_without_extension("a.txt") = "a"
 * - get_file_name_without_extension("a") = "a"
 * - get_file_name_without_extension("/usr/a.txt") = "a"
 * - get_file_name_without_extension("/usr/a.1.txt") = "a.1"
 */
std::string get_file_name_without_extension(const std::string& str);

/**
 * A string containing the combined paths. If one of the specified paths is a zero-length string, 
 * this method returns the other path. If path2 contains an absolute path, this method returns path2.
 * If path1 does not end with a valid separator character ('/' or '\\'), '/' is appended to path1 
 * before concatenation.
 */
std::string combine_path(const std::string& path1, const std::string& path2);

/**
 * Return true if the path specified is a full path. Full paths are paths that
 * start with "/", "\\\\" (UNC in windows), or contains a ":" as the second character.
 */
bool is_full_path(const std::string& path);

/**
 * Change to lowercase using the standard library tolower() for each char.
 */
std::string tolower(const std::string& in);

/**
 * Strip the given tokens from the beginning and the end of the string.
 * By default it will remove spaces. The result is returned.
 */
std::string strip(const std::string& in, const std::string& tokens = " \n\r\t");

/**
 * Escape XML characters.
 * @example xml_escape("<x>") == "&lt;x&gt;"
 * @remark Currently the only characters that are escaped are '<' and '>'.
 */
std::string xml_escape(const std::string& in);

} // namespace aitvml

#endif

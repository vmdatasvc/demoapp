/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef image_proc_HPP
#define image_proc_HPP

// SYSTEM INCLUDES
//

// AIT INCLUDES
//

#include <modules/ml/AITPort/PvUtil.hpp>
#include <modules/ml/AITPort/PvImageProc.hpp>
#include <modules/ml/AITPort/Vector3.hpp>
#include <modules/ml/AITPort/Image.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//


namespace aitvml
{

namespace image_proc
{

typedef enum
{
  SMOOTH_MEAN,
	SMOOTH_MEDIAN,
  SMOOTH_GAUSSIAN
} SmoothMode;

/**
 * Smooth a grayscale image using the provided mode and mask size.
 * @param in [in] Input image.
 * @param out [out] Output image.
 * @param mode [in] Can be SMOOTH_MEAN or SMOOTH_GAUSSIAN.
 * @param maskSize [in] Size of the mask for this square filter. The
 * anchor will be located at the center pixel. For MEAN filters, the
 * size must be odd. For Gaussian filters, the size can only be 3 and 5.
 * @remark the border resulting from the mask will not be changed.
 */
bool smoothImage(const Image8& in, Image8& out, 
                 const SmoothMode mode = SMOOTH_MEAN, const int maskSize = 3);

/**
 * Perform an 'in place' smoothing.
 * @remark This function does not really operates in place. It creates a temporary
 * matrix, initializes it with zeroes and then applies the operation on the 
 * original image. Finally, it copies the result to the destination image. The border
 * will then be black.
 */
bool smoothImage(Image8& img, 
                 const SmoothMode mode = SMOOTH_MEAN, const int maskSize = 3);

/**
 * Same as the Grayscale counterpart.
 */
bool smoothImage(const Image32& in, Image32& out, 
                 const SmoothMode mode = SMOOTH_MEAN, const int maskSize = 3);

/**
 * Same as the Grayscale counterpart.
 */
bool smoothImage(Image32& img, 
                 const SmoothMode mode = SMOOTH_MEAN, const int maskSize = 3);


/**
 * Get the mean and the variance.
 */
void calcMeanStdDev(const Image8& img, float& mean, float& stdDev);

/**
 * Get the mean and the variance.
 */
void calcMeanStdDev(const Image32& img, Vector3f& mean, Vector3f& stdDev);

/**
* copy the specified channel of the RGB image to the gray image
* if no channel is specified, all three channels will used to calculate a gray value
*/
void copyRGB2Gray(Image32 &in, Image8& out, int channel = -1);

/**
* copy the gray image into the specified channel of the RGB image
* if no channel is specified, all three channels will be copied to
*/
void copyGray2RGB(Image8 &in, Image32& out, int channel = -1);

}; // namespace image_proc

}; // namespace aitvml

#endif // image_proc_HPP


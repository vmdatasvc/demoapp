/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
#ifndef FACEDETECTOR_HPP
#define FACEDETECTOR_HPP

//#include "modules/ml/AITPort/PvUtil.hpp"	   // DV
#include "modules/ml/AITPort/Image.hpp"
#include "modules/ml/AITPort/Vector3.hpp"
#include "modules/ml/AITPort/Face.hpp"

#define FD_MAX_NR_OF_FACES 128

namespace aitvml
{

/**
 * Base class for all the face detectors.
 * @par Responsibilities
 * - Store the results of the face detector.
 * - Know the minimum, maximum and training size of the face detector.
 */
class FaceDetector
{
public:
  FaceDetector(void);
  virtual ~FaceDetector(void);

  // initialization
  void initialize(void);

  // un-initialization
  void unInitialize(void);

  // process gray scale image
  virtual void detect(const Image8 &image,bool asynch=false) = 0;

  // return # of faces found after calling detect()
  unsigned int nrOfFaces(void) { return nFaces; }

  // return reference to n-th face
  aitvml::Face &face(unsigned int n) { return faceArray[n]; }

  /**
   * Set the minimum desired size of a face in pixels.
   * This helps to improve performance.
   */
  void setMinFaceSize(const Vector2i& s) { mMinFaceSize = s; }

  /**
   * Set the maximum desired size of a face in pixels.
   * This helps to improve performance.
   */
  void setMaxFaceSize(const Vector2i& s) { mMaxFaceSize = s; }

  /**
   * Set a mask that filters out regions of the image. Only regions
   * that have nonZero pixels are scanned. This mask is assumed to be
   * binary with values 0 and 1.
   * @param pMask [in] Grayscale image that indicates the areas that need
   * to be scanned by non-zero pixels. Set this to NULL to remove the current
   * mask.
   * @param areaFraction [in] The fraction of the pixels that must be
   * non-zero in the mask in order to find a face. For example if this number
   * is 0.5, only squares that have non-zeros in half of the pixels will be
   * counted.
   */
  void setMask(const Image8* pMask, const float areaFraction);

  /**
   * Flag to use resample for the minimum face size. If it is not set,
   * the original image will be scanned, and the minimum window size
   * will be passed to the OpenCV library.
   */
  void setResampleForMinSize(bool s) { mResampleForMinSize = s; }

protected:

  /**
   * Resample the given grayscale image to improve performance.
   * This function will read the mMinSize and the mTrainingMinSize
   * and resample the image accordingly.
   * @param in [in] Input image.
   * @param out [out] Resulting Image. If the resulting image would
   * resize at more than 95% of the original size, the image is
   * not resampled.
   * @return The resampleFactor of the original image. It is less or
   * equal than 1. 1 means that the original image was not resampled, 
   * and the output should not be used.
   */
  float resampleForMinSize(const Image8& in, Image8& out);

protected:
  bool isInitialized;

  unsigned int nFaces;
  aitvml::Face *faceArray;

  /**
   * This is the processing mask integral image.
   * @see setMask
   */
  Image<unsigned short> *mpMaskIntegralImage;

  /**
   * Indicates the fraction of the area that must be nonzero
   * in order to detect a face in that region.
   */
  float mMaskAreaFraction;

  /**
   * Minimum face size that this detector can
   * handle. Depends on the training data. The units are
   * pixels.
   */
  Vector2i mTrainingMinSize;

  /**
   * Minimum desired size of a face in pixels.
   * This helps to improve performance.
   */
  Vector2i mMinFaceSize;

  /**
   * Maximum desired size of a face in pixels.
   * This helps to improve performance.
   */
  Vector2i mMaxFaceSize;

  /**
   * @see setResampleForMinSize
   */
  bool mResampleForMinSize;
};

} // namespace aitvml

#endif



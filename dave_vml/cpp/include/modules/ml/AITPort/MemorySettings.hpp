/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef AIT_MemorySettings_HPP
#define AIT_MemorySettings_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread/thread.hpp>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>

// AIT INCLUDES
//

#include <modules/ml/AITPort/String.hpp>
#include <modules/ml/AITPort/XmlParser.hpp>

// LOCAL INCLUDES
//

#include "modules/ml/AITPort/strutil.hpp"

// FORWARD REFERENCES
//

#include "modules/ml/AITPort/MemorySettings_fwd.hpp"

namespace aitvml
{

/**
 * Node struct.
 */

struct NodeStruct;
typedef boost::shared_ptr<NodeStruct> NodePtr;

typedef struct NodeStruct
{
  NodeStruct()
  {
    hasTemplate = false;
    isShared = false;
    allUsers = false;
    nodeType = UNDEFINED;
  }
  bool hasTemplate;
  bool isShared;
  bool allUsers;
  enum { UNDEFINED, MACRO, PARAM, FOLDER } nodeType;
  std::string label;
  std::string value;
  std::vector<NodePtr> subNodes;
} Node;


/**
 * SettingsHandler.
 */

class SettingsHandler : public Handler 
{ 
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  SettingsHandler(const std::string& appPath,std::vector<NodePtr> nodeStack);
  
  /**
   * Destructor
   */
  virtual ~SettingsHandler(void);


  //
  // OPERATIONS
  //

public:

  virtual void clear(void);
  std::string topTag(void);
  virtual void start(const std::string& ns, const std::string& tag, const char **attr);
  virtual void cdata(const XML_Char *s, Int len);
  virtual void end(const std::string& ns, const std::string& tag);

  /**
   * Gets a pointer to the root node of the memory structure created
   * by the XML parser.
   * @return A node pointer to the root node.
   */
  NodePtr getData();

  /**
   * Handles a folder tag.
   * @param label [in] The label of the folder node.
   */
  void tagFolder(const std::string& label);
  
  /**
   * Handles a macro tag.
   * @param label [in] The label of the macro node.
   * @param value [in] The value of the macro node.
   */
  void tagMacro(const std::string& label, const std::string& value);

  /**
   * Handles a param tag.
   * @param label [in] The label of the param node.
   * @param value [in] The value of the param node.
   * @param isShared [in] If true, the value comes from the registry,
   * if false, the value comes from memory.
   * @param allUsers [in] 
   */
  void tagParam(const std::string& label, const std::string& value, 
    bool isShared, bool allUsers);
  
  /**
   * Handles an include tag.
   * @param attr [in] Array of attributes for this tag.
   */
  void tagInclude(const char **attr);

  
  //
  // OPERATIONS
  //

protected:

  /**
   * Looks for node by label in a given node's subnodes.
   * @param pLookNode [in] The node we whose subnodes we will search.
   * @param label [in] The label of the subnode we are looking for.
   * @return A node pointer, if the node is not found returns NULL.
   */
  NodePtr findNode(NodePtr pLookNode, std::string label);

  /**
   * Constructs a directory path from a stack of strings.
   * @return A path as a string.
   */
  std::string pathFromStack();

  
  //
  // ATTRIBUTES
  //
  
protected:

  /**
   * Stack of XML tags.
   */
  std::vector<std::string> mTagStack;
  
  /**
   * Stack of node pointers.
   */
  std::vector<NodePtr> mNodeStack;

  /**
   * Number of node pointers that need popped off the stack of node pointers.
   */
  std::vector<int> mPopOff;

  /**
   * Root node of the memory structure.
   */
  NodePtr mRootNode;

  /**
   * Current path of the XML settngs file being parsed.
   */
  std::string mCrntPath;
  
};


/**
 * MemorySettings.
 */

class MemorySettings : private boost::noncopyable
{
  //
  // LIFETIME
  //

protected:

  /**
   * Constructor.
   */
  MemorySettings();

  
  //
  // LIFETIME
  //

public:

  /**
   * Destructor
   */
  virtual ~MemorySettings();

  /**
   * Checks if the instance pointer is NULL, if so then it
   * dynamically creates a new MemorySettings object, otherwise, 
   * it returns the pointer to the one and only instance of the class.
   * @return A pointer to the instance of the class.
   */
  static boost::shared_ptr<MemorySettings> instance();


  //
  // OPERATIONS
  //

public:

  /**
   * Retreives a string from the settings
   * @param key [in] Name of the variable, can be a path. The path is separated by
   * '/' characters. (don't use the '\\' character)
   * @param defaultValue [in] If the variable is not on the registry, this value is returned
   * @param writeDefault [in] If this flag is true (default) it will store the default
   * value in the registry when no values are specified.
   * @return Returns the value of the string stored or the default value
   * @remark Any variables defined in the retreived value are resolved using replaceVars().
   * Values written are not replaced, this means that if the defaultValue has a variable,
   * the variable with the enclosing symbols ('<>') will be written in the registry.
   */
  std::string getString(const std::string& key, const std::string& defaultValue, bool writeDefault = true);

  /**
   * This version throws when the parameter is not defined.
   * @trhows SettingNotFoundException if the parameter doesn't exist.
   */
  std::string getString(const std::string& key);

  /**
   * Saves a string on the settings
   * @param key [in] Name of the variable, can be a path
   * @param val [in] Value to be stored
   */
  void setString(const std::string& key, const std::string& val);

  /**
   * Parse the XML settings file.
   * @param fname [in] File name of the file to be parsed, default is "settings.xml".
   * @param merge [in] Indicates if the current settings should be merged with the
   * existing settings.
   */
  void parseXml(const std::string& fname, bool merge);

  /**
   * Parse an XML string.
   * @param xmlStr [in] String containing XML to parsed.
   * @param merge [in] Indicates if the current settings should be merged with the
   * existing settings.
   */
  void parseMemoryXml(const std::string& xmlStr, bool merge);

  /**
   * Replace macros in the given string.
   * @param value [in] String containing macros to be replaced.
   * @param currentPath [in] Current path to begin searching for macros.
   * @return Returns the string will the replaced macro values.
   */
  std::string replaceVars(const std::string& value, const std::string& currentPath);

  /**
   * Replace macros in a given string starting from startIndex.
   * @param value [in] Value containing macros.
   * @param startIndex [in] Position within the value to start looking for the next macro.
   * @param currentPath [in] Current path to begin searching for macros.
   * @return 
   */
  std::string replaceVars(const std::string& value, unsigned int startIndex, std::vector<NodePtr>& stack);

  /**
   * Clear the settings.
   */
  void reset();

  /**
   * Delete a given parameter from the memory tree.
   */
  void deleteValue(const std::string& key);

  /**
   * Initialize the memory settings so all queries will now be handled by
   * the memory settings.
   */
  void setMemorySettings();

  /**
   * Write the memory node out to file in XML format.
   * @param fname [in] File name of the file to be created from the memory structure.
   */
  void writeXml(const std::string& fname, bool resolveMacros = false);

  /**
   * Write the memory node out to a stream in XML format.
   * @param os [in] output stream. Can be std::cout.
   */
  void writeXml(std::ostream& os, bool resolveMacros = false);

  //
  // OPERATIONS
  //

protected:

  /**
   * Finds a macro string by name and return the appropriate macro value.
   * @param macroName [in] The name of the macro to be used.
   * @param currentPath [in] The current path of the parameter with a macro 
   * value that needs replaced.
   * @return A string representing the macro's value.
   */
  std::string getMacroValue(const std::string& macroName, std::vector<NodePtr>& stack);

  /**
   * Build a stack of nodes based on the string path.
   * @param path [in] The path from mAppPath to search for.
   * @return A node pointer to the path.
   */
  void pathToStack(const std::string& path, std::vector<NodePtr>& stack);

  /**
   * Adds a node to memory.
   * @param keyPath [in] The key or path and key to add to the memory structure.
   * @param value [in] The value of the parameter to add.
   */
  void addNode(const std::string& keyPath, const std::string& value);

  /**
   * Recursive function that prints a node's subnodes.
   * @param outs [in] Output stream for printing.
   * @param pNode [in] Node to be printed.
   */
  void printXml(std::ostream& outs, std::vector<NodePtr>& stack, bool resolveMacros);

private:

  std::string getString(const std::string& key, const std::string& defaultValue, 
    bool writeDefault,
    bool throwIfNotFound);


  
  //
  // ACCESS
  //

public:

  //
  // ATTRIBUTES
  //

protected:

  /**
   * Root node of the memory structure.
   */
  NodePtr mRootNode;

  /**
   * Mutex
   */
  boost::mutex mMutex;

  /**
   * Pointer to singleton instance of MemorySettings class.
   */
  static boost::shared_ptr<MemorySettings> mpInstance;

};


}; // namespace aitvml

#endif // MemorySettings_HPP


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef DateTime_fwd_HPP
#define DateTime_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace aitvml
{

class DateTime;
typedef boost::shared_ptr<DateTime> DateTimePtr;

}; // namespace aitvml

#endif // DateTime_fwd_HPP


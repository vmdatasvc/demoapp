/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef PVCONCOMPLAB_H
#define PVCONCOMPLAB_H

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

#include <modules/ml/AITPort/Image.hpp>
#include "modules/ml/AITPort/PvUtil.hpp"

namespace aitvml
{

/*********************************************************************
 ** 
 ** Connected Component Labeling Template Class
 **
 *********************************************************************
 ** 
 ** Description:
 **
 ** This class allows to calculate the connected components of a gray
 ** image. The connected components are stored in an array
 ** and can be accessed via the class. The actual PvCompsT class
 ** is a template of the components. A default component PvComp is
 ** provided and should be used as the base class of any
 ** enhanced component types (e.g., a class supporting insertion
 ** into a tree to generate aspect graphs).
 ** -Nils
 **
 *********************************************************************/
template<class T> class PvCompsT : public Image<unsigned int>
{
protected:
  T *comps ;               // list of comps
  unsigned int ncomps;     // # of comps
  unsigned int *labels;    // workspace that can hold labels
  unsigned int labelSize;  // size of workspace

  unsigned char *auxImage; // helper image for region growing

  void allocWorkSpace(Image8 &image);  // function that allocates necessary workspace

public:
  // constructor doesn't really do anything
  PvCompsT(void) : comps(NULL), ncomps(0), labels(NULL),
                   labelSize(0), auxImage(NULL) { };  
  // destructor
  ~PvCompsT(void);

  // performs the component labeling
  void label(Image8 &image, const int borderColor=-1);

  // number of connected components found in the image
  unsigned int numberOfComps(void) { return ncomps;};

  // accesses the n-th component after labeling is done
  T &comp(unsigned int n);

  // removes all components with less than <size> pixel
  void removelessthan(unsigned int size);

  // removes the largest component
  void removemax(void);

  // create a binary image with every pixel set to 1 that
  // is a valid component
  void createBinary(Image8 &bin);

  // save an image with all the components color coded
  void saveLabelledImage(char *fileName);
};


/*********************************************************************
 ** 
 ** Component Class
 **
 *********************************************************************
 ** 
 ** Description:
 **
 ** Standard component type that holds the information about each
 ** component.
 ** More powerful components can be created that should provide the 
 ** same members as the one below or inherits the members from
 ** PvComp.
 ** -Nils
 **
 *********************************************************************/
class PvComp
{
 public:
  unsigned int num;     // component-id
  unsigned char color;  // color of component
  unsigned int size;    // total size of component in pixel
  float xc,yc;          // center of mass
  unsigned int x,y;     // coordinate of one of the pixels in the component
  unsigned int xmin,ymin,xmax,ymax; // bbox

  // clears all entries
  void clear(void)       
    {
      size=0;xc=yc=0.0;
      x=y=0;
      xmin=ymin=UINT_MAX;
      xmax=ymax=0;
    };  
};


/*********************************************************************
 ** 
 ** Component labeling class type with standard template type PvComp.
 **
 *********************************************************************/
typedef PvCompsT<class PvComp> PvComponents;

/*********************************************************************
 *
 * DESTRUCTOR
 * 
 *********************************************************************/
template<class T> PvCompsT<T>::~PvCompsT(void)
{
  if(labels!=NULL) delete [] labels;
  if(auxImage!=NULL) delete [] auxImage;
  if(comps!=NULL) delete [] comps;  
}

/*********************************************************************
 * allocWorkSpace()
 * 
 * Allocates workspace for the member label(). It only performs
 * actual allocations if the workspace is increasing or if no space
 * has been allocated before. 
 *
 * <input> 
 * 
 * Image8 &image: image to be labeled => yields size of workspace
 *
 * <output>
 *
 * void
 *
 * <error handling>
 * 
 * TODO
 *
 *********************************************************************/
template<class T> void PvCompsT<T>::allocWorkSpace(Image8 &image)
{
  unsigned int w=image.width(),h=image.height(),size=w*h;
  
  if(labels!=NULL && auxImage!=NULL && size>=labelSize) return;
  if(labels!=NULL) delete [] labels;
  if(auxImage!=NULL) delete [] auxImage;

  labels=new unsigned int[size];
  auxImage=new unsigned char[size];

  labelSize=size;
  
  return;
}

/*********************************************************************
 * label()
 *
 * Performs component labeling. This function uses a 4 neighborhood.
 * 
 * <input> 
 *
 * Image8 &image : image to be labeled
 *
 * <output>        
 *
 * void
 * 
 * <changes>
 *          information about the connected components is stored in
 *          an array of comps 
 *         
 *********************************************************************/
template<class T> void PvCompsT<T>::label(Image8 &image, int borderColor)
{
  unsigned int i,x,y;

  register unsigned int lx,l1,l2,l;
  unsigned int f;
  unsigned int w=image.width(),h=image.height(),s=w*h;
  unsigned int iLabels=0;
  unsigned int *labimg;
  unsigned char pix=0,ppix;

  unsigned int count=0;

  // allocate workspace if neccessary (i.e., if not still present from before)
  allocWorkSpace(image);

  // clear component vector
  if(comps!=NULL) delete [] comps;

  // make sure image has the same size as 'image'
  mx_assert(h,w);

  labimg=pointer();

  // set the 1 pixel border of the image to the value borderColor
  // if desired by the user
  if(borderColor>=0)
  {
    for(i=0;i<w;i++)
    {
      image(i)=borderColor;
      image(s-i-1)=borderColor;
    }
    for(i=0;i<s;i+=w)
    {
      image(i)=borderColor;
      image(i+w-1)=borderColor;
    }
  }

  // LABEL IMAGE (three pass (non-recursive) approach)

  // PASS #1
  i=0;
  for(y=0;y<h;y++)
    {
    for(x=0;x<w;x++)
      {     
        ppix=pix;
        pix=image(i);
        l1= x && ppix==pix ? labimg[i-1] : UINT_MAX;
        l2= y && image(i-w)==pix ? labimg[i-w] : UINT_MAX;
	if(l1!=UINT_MAX)
	  {
	    if(l2!=UINT_MAX && l2!=l1)
	      {
		// l=SWAP(l,l1,l2)
		// PvMSG("[%d,%d]\n",x,y);
		l=l1;
		f=0;
		count++;
		while((lx=labels[l])!=l1)
		  {
		    if(lx==l2) { f=1; break;}
		    if((l=labels[lx])==l1) { l=lx; break; }
		    if(l==l2) { f=1; break;}
		  }
		if(f==0)
		  {
		    f=labels[l1];
		    labels[l1]=labels[l2];
		    labels[l2]=f;
		    labimg[i]=labels[l1];	      
		  }
		else
		  {
		    labimg[i]=l1;	      
		  } 		
	      }
	    else
	      {
					labimg[i]=l1;	      
	      }
	  }
	else if(l2!=UINT_MAX)
	  {
	    labimg[i]=l2;
	  }
	else
	  {
	    labimg[i]=iLabels;	
	    labels[iLabels]=iLabels;
	    iLabels++;
	  }
	i++;
      }
    }    
  // PASS #2
  l=0;
  for(i=0;i<iLabels;i++)
    {
      if(labels[i]>=i) 
	{
	  l1=i;
	  while(labels[l1]!=l)
	    {
	      f=labels[l1];
	      labels[l1]=l;
	      l1=f;
	    }
	  l++;
	}
    }

  //printf("%3d ");
  //PvMSG("count=%d\n",count);
  comps=new T [l];
  ncomps=l;

  for(i=0;i<ncomps;i++)
    {
      comps[i].clear();
    }
  
  // PASS #3
  i=0;
  for(y=0;y<h;y++)
    {
      for(x=0;x<w;x++)
	{
	  l=labimg[i];
	  l=labels[l];
	  labimg[i]=l;
	  if(comps[l].size==0)
	    {
	      comps[l].x=x;
	      comps[l].y=y;
	      comps[l].color=image(i);
	    }
	  comps[l].xc+=float(x);
	  comps[l].yc+=float(y);
	  comps[l].size++;
	  if(x<comps[l].xmin) comps[l].xmin=x;
	  if(x>comps[l].xmax) comps[l].xmax=x;
	  if(y<comps[l].ymin) comps[l].ymin=y;
	  if(y>comps[l].ymax) comps[l].ymax=y;
	  i++;
	}
    }

  for(i=0;i<ncomps;i++)
    {
      comps[i].num=i;
      comps[i].xc/=comps[i].size;
      comps[i].yc/=comps[i].size;
    }
}

/*********************************************************************
 * PvCompsT<T>::comp
 * 
 * <input> 
 *          unsigned int n   : number of the component to be accessed
 *
 * <output>        
 *          T &        : reference to the n-th component
 * 
 * <changes>
 *          nothing
 *         
 *********************************************************************/

template<class T> T &PvCompsT<T>::comp(unsigned int n)
{
  if(n>=ncomps)
    {
      PvUtil::exitError("PvCompsT: Error accessing invalid component!");
    }
  return comps[n];
}


/*********************************************************************
 * PvCompsT<T>::removeiflessthan
 * 
 * <description>
 *              
 * Removes all comps that have a size less than <size>
 * 
 * <input> 
 *          unsigned int size   : size threshold
 *
 * <output>        
 *          void
 * 
 * <changes>
 *          component array
 *         
 *********************************************************************/

template<class T> void PvCompsT<T>::removelessthan(unsigned int size)
{
  unsigned int i=0;

  while(i<ncomps)
    {
      if(comps[i].size<size)
	{
	  comps[i]=comps[--ncomps];	  
	  comps[i].num=i;
	}
      else
	{
	  comps[i].num=i;
	  i++;
	}
    }
}

/*********************************************************************
 * PvCompsT<T>::removemax
 * 
 * <description>
 *              
 * Removes the the biggest component.
 * 
 * <input> 
 *          void
 *
 * <output>        
 *          void
 * 
 * <changes>
 *          component array
 *         
 *********************************************************************/

template<class T> void PvCompsT<T>::removemax(void)
{
  unsigned int i=0,maxi=0,max=comps[0].size;

  for(i=0;i<ncomps;i++)
    {
      if(comps[i].size>max)
	{
	  max=comps[i].size;
	  maxi=i;
	}
    }

  i=maxi;

  comps[i]=comps[--ncomps];	  
  comps[i].num=i;
}

/*********************************************************************
 * PvCompsT<T>::createBinary
 * 
 * Create a binary image with every pixel set to one that is
 * a valid connected component.
 * 
 * <input> 
 *          Image8 &bin : ref to gray image to be created
 *
 * <output>        
 *          void
 *         
 *********************************************************************/
template<class T> void PvCompsT<T>::createBinary(Image8 &bin)
{
  unsigned int w=width(),h=height(),s=w*h;
  unsigned int i=0,max;
  unsigned char *labels;
  unsigned int *image=pointer();

  if(ncomps==0) 
  {
    bin.setAll(0);
    return;
  }

  max=comps[0].num;

  for(i=0;i<ncomps;i++)
    {
      if(comps[i].num>max) max=comps[i].num;
    }

  labels=new unsigned char [max+1];

  for(i=0;i<ncomps;i++)
    {
      labels[comps[i].num]=1;
    }

  bin.resize(w,h);

  for(i=0;i<s;i++)
  {
    if(image[i]<=max) bin(i)=0;
    else bin(i)=labels[image[i]];
  }

  delete [] labels;
}

template<class T> void PvCompsT<T>::saveLabelledImage(char *fileName)
{
  int size=width()*height();

  Image8 image(width(),height());

  for(int i=0;i<size;i++)
    {
      image(i)=(unsigned char)(*this)(i);
    }

  image.save(fileName);
}

} // namespace aitvml

#endif

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef PVRECT_HPP
#define PVRECT_HPP

#include <iostream>

#include <math.h>

#include "modules/ml/AITPort/PvUtil.hpp"
#include "modules/ml/AITPort/Vector3.hpp"

#ifndef uint
//#define uint unsigned int
typedef unsigned int uint;
#endif

namespace aitvml
{

/*********************************************************************
 ** 
 ** Rectangle Template Class
 **
 *********************************************************************
 ** 
 ** Description:
 **
 *********************************************************************/

template<class T> class Rectangle
{
public:

  Rectangle(const T xx0, const T yy0, const T xx1, const T yy1) { set(xx0,yy0,xx1,yy1); };

  Rectangle(void) { set(0,0,0,0); };

  Rectangle(const Rectangle& v0) { x0 = v0.x0; y0 = v0.y0; x1 = v0.x1; y1 = v0.y1;};

  virtual ~Rectangle(void) {};

  Rectangle& operator= (const Rectangle& v0) {
    if(this != &v0) {
		  x0 = v0.x0; 
		  y0 = v0.y0; 
		  x1 = v0.x1; 
		  y1 = v0.y1;          
    }
    return *this;
  };
	
	bool operator== (const Rectangle& v0) const {
		return ((x0 == v0.x0) && (y0 == v0.y0) && (x1 == v0.x1) && (y1 == v0.y1));
	}

  bool operator!= (const Rectangle& v0) const { return !(*this == v0); }

  void set(const T xx0, const T yy0, const T xx1, const T yy1) {
    x0 = xx0; y0 = yy0; x1 = xx1; y1 = yy1;
  }

  void zero(void) { set(0,0,0,0); };

  T width() const { return x1 - x0; };
  T height() const { return y1 - y0; };

  void setPos(T x, T y) { set(x, y, x + width(), y + height()); };
  void setPos(Vector3<T>& pt) { setPos(pt(0),pt(1)); };

  /// @todo Fix this function. something to do with Vector2/Vector3
  Vector3<T> getPos() { return Vector3<T>(x0,y0); };
  Vector3<T> getCenter() { return Vector3<T>((x1+x0)/2,(y1+y0)/2); };

  void setSize(T w, T h) { set(x0, y0, x0 + w, y0 + h); };
  void setSize(Vector3<T>& pt) { setSize(pt(0),pt(1)); };
  Vector3<T> getSize() { return Vector3<T>(width(),height()); };

  /// check if the supplied position is inside the rectangle
  bool inside(const Vector3<T> &pos) const { return (pos(0)>=x0 && pos(0)<x1 && pos(1)>=y0 && pos(1)<y1); };

  /// check if the supplied rectangle is inside this rectangle
  bool inside(const Rectangle<T> &subRect) const { return (subRect.x0>=x0 && subRect.x0<=x1 && subRect.y0>=y0 && subRect.y0<=y1 &&
                                                        subRect.x1>=x0 && subRect.x1<=x1 && subRect.y1>=y0 && subRect.y1<=y1); };

  /// Intersect the two rectangles. returns true if they intersect.
  bool intersect(const Rectangle<T> &r1, const Rectangle<T> &r2)
  {
    x0 = (std::max)(r1.x0,r2.x0);
    y0 = (std::max)(r1.y0,r2.y0);
    x1 = (std::min)(r1.x1,r2.x1);
    y1 = (std::min)(r1.y1,r2.y1);

    return (width() > 0 && height() > 0);
  }

  /// Intersect the two rectangles. returns true if they intersect.
  bool intersect(const Rectangle<T> &r1)
  {
    return intersect(r1,Rectangle<T>(*this));
  }

  /// Calculate the union of r1 and r2, and store it in this.
  void join(const Rectangle<T> &r1, const Rectangle<T> &r2)
  {
    x0 = std::min(r1.x0,r2.x0);
    y0 = std::min(r1.y0,r2.y0);
    x1 = std::max(r1.x1,r2.x1);
    y1 = std::max(r1.y1,r2.y1);    
  }

  /// Calculate the union of r1 and this, and store it in this.
  void join(const Rectangle<T> &r1)
  {
    join(r1,Rectangle<T>(*this));
  }

  // I/O operators
  friend std::ostream& operator<<(std::ostream &stream, const Rectangle& s) {
    stream << s.x0 << " " << s.y0 << " " << s.x1 << " " << s.y1;
    return stream;
  };

  friend std::istream& operator>>(std::istream &stream, Rectangle& s) {  
    stream >> s.x0 >> s.y0 >> s.x1 >> s.y1;
    return stream;
  };

  /// Left Coordinate
  T x0;

  /// Top (or bottom?) coordinate (smaller value). Depends on Y axis direction.
  T y0;

  /// Right Coordinate. In the discrete case this coordinate is outside the rectangle.
  T x1;

  /// Bottom (or top?) coordinate (bigger value). Depends on Y axis direction. In the discrete case this coordinate is outside the rectangle.
  T y1;  

};

// Shorcut Macros

typedef Rectangle<int> Rectanglei;
typedef Rectangle<float> Rectanglef;
typedef Rectangle<double> Rectangled;

/**
 * This utility function saves a lot of typing. It is used to change the type
 * of a rectangle and scale it to the given size. The most common use is
 * to transform rectangles from float image coordinates in the range 0..1, to
 * integer coordinates in the range [w,h].
 * @param inRect [in] Rectangle to transform.
 * @param w [in] Factor to multiply the x elements of inRect by.
 * @param h [in] Factor to multiply the y elements of inRect by.
 * @return A new rectangle with the converted type and elements { inRect.x0*w, inRect.y0*h,
 * inRect.x1*w, inRect.y1*h }
 * @par Example:
 * @code 
 *   Rectanglef rectf(0.5f,0.5f,1.0f,1.0f);
 *   Rectanglei recti;
 *   transform(rectf,recti,320,240);
 *   PVASSERT(recti == Rectanglei(160,120,320,240));
 * @endcode
 */
template <typename InType, typename OutType>
Rectangle<OutType> transform(const Rectangle<InType>& inRect, const OutType w, const OutType h)
{
  return Rectangle<OutType>(static_cast<OutType>(inRect.x0*w),static_cast<OutType>(inRect.y0*h),
                         static_cast<OutType>(inRect.x1*w),static_cast<OutType>(inRect.y1*h));
}

} // namespace ait

#endif

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef IlluminationGradient_fwd_HPP
#define IlluminationGradient_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace aitvml
{

namespace vision
{

class IlluminationGradient;
typedef boost::shared_ptr<IlluminationGradient> IlluminationGradientPtr;

}; // namespace vision

}; // namespace aitvml

#endif // IlluminationGradient_fwd_HPP


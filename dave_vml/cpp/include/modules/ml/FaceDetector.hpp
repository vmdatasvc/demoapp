/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * FaceDetector.hpp
 *
 *  Created on: Jan 8, 2016
 *      Author: dkim
 */

#ifndef FACEDETECTOR_HPP_
#define FACEDETECTOR_HPP_


#include <core/Image.hpp>
#include <core/Settings.hpp>

#include <modules/ml/FaceDetectorTarget.hpp>
#include <modules/ml/FaceVideoModule.hpp>
#include <modules/ml/FaceModuleManager.hpp>
#include "opencv2/objdetect/objdetect.hpp"
#include "modules/ml/mlfeature.hpp"
#include "modules/ml/mlsvm.hpp"

namespace vml {

class FaceDetector
{
public:

	FaceDetector();
	~FaceDetector();

	// initialize
	void initialize(boost::shared_ptr<FaceVideoModule> video_buffer,
				boost::shared_ptr<Settings> settings);

	// detect methods
	bool detect(void);
	bool detectInGrabFrame(void);
	bool detectInDetectorFrame(void);

	// From FaceBaseDetector
	void getDetectedTargetList(std::vector<boost::shared_ptr<FaceDetectorTarget> > &d);
	void getBlobMap(cv::Mat &bmap) const {mBlobMap.copyTo(bmap);}

	// display
	void drawValidTargets(cv::Mat &image) const;

	void getDetectedFaceRects(std::vector<cv::Rect> &d) const;
	void getDetectedFalsePositiveFaceRects(std::vector<cv::Rect> &d) const;

	// filtering false positive faces
	void FPFiltering(const cv::Mat &image, std::vector<cv::Rect> &facevec);
	bool FPFilteringByColor(cv::Mat &cimg);
	bool FPFilteringByHOG(cv::Mat &cimg);


private:

	// members
	cv::CascadeClassifier	mpCascade;

    float scaleFactor;
	int minNeighbors;
	int minFaceSize;
	int maxFaceSize;
    bool mFPFiltering;
    int mFPFilterFeature;

	// other modules
	boost::shared_ptr<FaceVideoModule>	mpVideoBuffer;

	// temporary list for Face points
	std::vector<std::vector<cv::Point> > mTempContourList;

	// temporary list for Face rect
	std::vector<cv::Rect>	mFaces;
	std::vector<cv::Rect>	mFacesAF; 				// after filtering
	std::vector<cv::Rect>	mFalsePositiveFaces; 	// after filtering

	// detected Face map
	cv::Mat_<unsigned char>		mFaceMap;

	// for a face filter
    boost::shared_ptr<vml::MLSVM> psvm;
    boost::shared_ptr<vml::MLFeature> pfeature;

    // from FaceBaseDetector
	//int		mMaxTargets;
	//int		mMinTargetArea;

	// list of detected target
	std::vector<boost::shared_ptr<FaceDetectorTarget> >	mDetectedList;

	// buffer
	//std::vector<boost::shared_ptr<FaceDetectorTarget> >	mDetectedListBuffer;

	// detected blob map
	cv::Mat_<unsigned char>		mBlobMap;


};

} // of namespace vml
#endif

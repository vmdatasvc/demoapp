/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * FaceModuleManager.hpp
 *
 *  Created on: Aug 2, 2016
 *      Author: dkim
 *      
 */

#ifndef FACEMODULEMANAGER_HPP_
#define FACEMODULEMANAGER_HPP_

#include <boost/shared_ptr.hpp>
#include <opencv2/core/core.hpp>
#include <core/Image.hpp>

#include <core/Settings.hpp>
#include <modules/ml/FaceTargetManager.hpp>
#include <modules/ml/Recognition.hpp>

namespace vml {

// forward decl.
class FaceVideoModule;
class FaceDetector;
//class FaceTargetManager;
//class aitvml::Recognition;

// Factory class for vision modules
// NOTE: It's a design decision if all the pointers to the modules should be static.
//  If static, the modules are not thread-safe, so need to enforce tread-safety
//  currently, module pointers are not static, and each thread should create its own
//  factory instance.
class FaceModuleManager
{
public:
	FaceModuleManager() {};
	~FaceModuleManager() {};

	// singleton instance access
	static FaceModuleManager* instance();

	void registerSettings(boost::shared_ptr<SETTINGS> settings);
	boost::shared_ptr<SETTINGS> getSettings(void);

	void registerVideoModule(boost::shared_ptr<FaceVideoModule> vb);
	boost::shared_ptr<FaceVideoModule> getVideoModule(void);

	void registerDetector(boost::shared_ptr<FaceDetector> det);
	boost::shared_ptr<FaceDetector> getDetector(void);

	void registerFaceTargetManager(boost::shared_ptr<FaceTargetManager> tm);
	boost::shared_ptr<FaceTargetManager> getFaceTargetManager(void);

	void registerDMGRecognizer(boost::shared_ptr<aitvml::Recognition> drg);
	boost::shared_ptr<aitvml::Recognition> getDMGRecognizer(void);




private:

	boost::shared_ptr<SETTINGS>					mpSettings;
	boost::shared_ptr<FaceVideoModule>			mpVideoBuffer;
	boost::shared_ptr<FaceDetector>				mpDetector;
	boost::shared_ptr<FaceTargetManager>		mpFaceTargetManager;
	boost::shared_ptr<aitvml::Recognition> 		mpDMGRecognizer;
};

} // of namespace vml

#endif

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * FaceBaseTracker.hpp
 *
 *  Created on: Jan 26, 2016
 *      Author: dkim
 */

#ifndef FACEBASETRACKER_HPP_
#define FACEBASETRACKER_HPP_

#include <opencv2/core/core.hpp>
#include <opencv2/video/tracking.hpp>
#include <boost/shared_ptr.hpp>
#include <core/Settings.hpp>
//#include <core/CameraCalibration.hpp>

#include <modules/ml/FaceDetectorTarget.hpp>

#define USE_CAMSHIFT 0

namespace vml {

class FaceVideoModule;
class FaceDetector;
class FaceDetectorTarget;
class MLFeature;

// Trackers
enum FaceTrackingMethods {
	FaceTrackerFeature_HIST_HSV,
	FaceTrackerFeature_HIST_YCrCb,
	FaceTrackerFeature_HIST_rgCHROM,
	FaceTrackerFeature_HIST_RGB,
	FaceTrackerFeature_DLIB,
	FaceTrackerFeature_Hybrid
};

enum TrackingStatusEnum {
	FaceTrackingStatus_Initialized,
	FaceTrackingStatus_InStart,	// region status
	FaceTrackingStatus_InTrack,
	FaceTrackingStatus_Invalid,
	FaceTrackingStatus_Active,	// track status
	FaceTrackingStatus_Inactive,
	FaceTrackingStatus_Finished
};


class FaceBaseTracker
{
public:
	FaceBaseTracker(boost::shared_ptr<SETTINGS> settings);
	virtual ~FaceBaseTracker() {}

//	virtual void initialize(const cv::Mat &input_image,
//				cv::Rect init_rect,
//				std::vector<std::vector<cv::Point> > *init_contour) = 0;
	void initialize(boost::shared_ptr<FaceDetectorTarget> dt);

	virtual void initializeTrackerFeature(boost::shared_ptr<FaceDetectorTarget> dt) = 0;

	TrackingStatusEnum update(cv::Mat *disp_map);
	virtual bool updateTrackerFeature(cv::Mat *disp_map,cv::Point &new_center) = 0;

	TrackingStatusEnum reactivate(std::vector<boost::shared_ptr<FaceDetectorTarget> > &dt_list);
	TrackingStatusEnum reactivateStatus(boost::shared_ptr<FaceDetectorTarget> &maxLikelihood_dt, bool target_found);
	TrackingStatusEnum reactivateStatus(boost::shared_ptr<FaceDetectorTarget> &maxLikelihood_dt);

	bool verifyActiveTargetFromDetection( boost::shared_ptr<FaceDetectorTarget> &dt);

	//virtual bool update(const cv::Mat &input_image,
	//			cv::Mat *disp_map) = 0;

	void updateSearchWindowExternal(void) {updateSearchWindow();}
	void setTrackBox(cv::Rect tBox) {mTrackBox = tBox;}
	cv::Rect getCropRect(void) const {return mCropRect;}
	cv::Rect getTrackBox(void) const {return mTrackBox;}

	cv::Point getPosition(void) const {return mCenter;}
	float	getConfidence(void) const {return mConfidence;}

	virtual void drawTrackBox(cv::Mat &disp_image) = 0;
	virtual void updateTargetActivityMap(cv::Mat &activity_map);

	static FaceTrackingMethods readTrackingMethod(boost::shared_ptr<Settings> settings);
	static void setTrackingMethod(const FaceTrackingMethods tm, boost::shared_ptr<Settings> settings);

	bool isInTrackRegion() {return mRegionStatus == FaceTrackingStatus_InTrack;}
	bool isInStartRegion() {return mRegionStatus == FaceTrackingStatus_InStart;}
	bool isInTrackStartRegion() {return (mRegionStatus == FaceTrackingStatus_InTrack || mRegionStatus == FaceTrackingStatus_InStart);}	// this is not needed cause start region always enclose t

	// For reactivation of a target
	float calculateReactivationLikelihood(boost::shared_ptr<FaceDetectorTarget> dt);

	// For using the Kalman filter
	void updateKalmanFilter(cv::Point newCenter, bool success);
	float calculateLocationLikelihoodKF(cv::Point pos);
	float calculateLocationLikelihoodKF(int x, int y);

	// Reconnect the lost track of a face
	//TrackingStatusEnum reactivate(std::vector<boost::shared_ptr<FaceDetectorTarget> > &dt_list);

	// for external call
	unsigned int getInactiveFrameCount(void) {return mInactiveFrameCount;}
	unsigned int getInactiveTimeOutValue(void) {return mcInactiveTimeout;}
	void increaseInactiveFrameCount(void) {mInactiveFrameCount++;}

protected:

	void initializeKalman(cv::Rect trackRect);

	void calcLocationUncertaintyKF();

	void updateSearchWindow();
	void updateContourMask(std::vector<std::vector<cv::Point> > contour);
	//void updateContourMask(std::vector<std::vector<cv::Point> > *contour);

	// search window parameters
	const float		mSearchWindowRatio;	// must be strictly larger than 1

	// image dimension for checking search window bound
	int	rows;
	int	cols;


#if USE_CAMSHIFT
	cv::RotatedRect			mTrackBox;
#else
	cv::Rect				mTrackBox;	// tight bounding box of target
#endif

	cv::Rect 				mCropRect;	// search window
	cv::Point 				mCenter;	// image location of the target
	int						mArea;
	float					mConfidence;	// tracking confidence value

	cv::Mat					mContourMask;	// for more precise contour mask

	// new parameters at 08/09/16
	TrackingStatusEnum		mRegionStatus;
	TrackingStatusEnum		mTrackStatus;

	bool 					mUseKF;				// usability of Kalman filter
	///////////////////////////////////////////////////////
	// Kalman filter members
	cv::KalmanFilter		mKF;
	float					mKFInitialStateUncertaintyLoc;
	float					mKFInitialStateUncertaintyVel;
	float					mKFProcessNoiseLoc;
	float					mKFProcessNoiseVel;
	float					mKFMeasurementNoise;
	cv::Mat					mKFMeasurement;
	cv::Point				mKFPrediction;
	float					mLocationUncertaintyInvKF[3];	// from Kalman filter
	///////////////////////////////////////////////////////

//	bool					mReactivated;
	int						mInactiveFrameCount;

	// constant members
	const int				mcInactiveTimeout;
//	const float				mcReactivateLikelihoodThreshold;

	cv::Point				prevPosition;
	std::vector<cv::Point>	prevSearchAreaContour;

	boost::shared_ptr<FaceVideoModule>	mpVideoModule;	//caching pointer for saving access
	boost::shared_ptr<FaceDetector>		mpFaceDetector;	//caching pointer to FaceDetector
	boost::shared_ptr<MLFeature>		mpFeature;
};

} // of namespace vml

#endif

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * FaceTarget.hpp
 *
 *  Created on: Jan 28, 2016
 *      Author: dkim
 */

#ifndef FACETARGET_HPP_
#define FACETARGET_HPP_

#include <sys/time.h>

#include "opencv2/core/core.hpp"

//#include "modules/ml/FaceBaseDetector.hpp"
#include "modules/ml/FaceDetector.hpp"
#include "modules/ml/FaceBaseTracker.hpp"
#include "modules/ml/FaceTrajectory.hpp"
#include "modules/ml/Recognition.hpp"
#include "modules/ml/FaceData.hpp"
#include "modules/ml/mlfeature.hpp"
#include "modules/ml/mlsvm.hpp"

namespace vml {

class FaceDetectorTarget;

class FaceTarget {	// base class for visual tracking of a face target 
	friend class FaceTargetManager;
public:

	FaceTarget(unsigned int id);
	~FaceTarget() {}

	// virtual method for target matching or searching
	bool verifyFaceDetection(const cv::Mat &input_image,  cv::Rect &box, cv::Mat &faceimage);

	TrackingStatusEnum updateTargetStatus(cv::Mat &bpImage);
	TrackingStatusEnum reactivateTarget(std::vector<boost::shared_ptr<FaceDetectorTarget> > &dt_list);

	bool verifyActiveTarget(boost::shared_ptr<FaceDetectorTarget> &dt);
	float verifyFaceSimilarity(boost::shared_ptr<FaceDetectorTarget> &dt);

	bool verifyTargetStatus(const cv::Mat &foreground_image);

	//void initializeFeature(const cv::Mat &input_image);
	void initialize(boost::shared_ptr<FaceDetectorTarget> dt);

	double getLastTime(void) const;
	cv::Point getPosition() const;

	// Save the CFaceData (initially local place)
	void storeFaceData( std::string path, int numFaces);
	void completeFaceData(int numFaces);
	void completeFaceDataDMG(boost::shared_ptr<aitvml::Recognition> recognizer, int numFaces);
	void DMGDecisionFusion(const std::vector<SingleFaceDataType> dmgvec, DMGData &dmgFusion);

	//void selectFaces(const std::vector<SingleCFaceDataType> fvec, int numFaces, std::vector<int> &sidx);
	void selectFaces(int numFaces, std::vector<int> &sidx);

	// for PCA process
	cv::Mat formatImagesForPCA(const std::vector<cv::Mat> &data);

	// display methods
	void drawTarget(cv::Mat &image) const;
	void drawTrajectory(cv::Mat &image) const {mTrajectory.draw(image);}

	void updateTargetActivityMap(cv::Mat &activity_map);

	int getDetectionInactiveFrameCount(void) {return mDetectionInactiveFrameCount;}
	void resetDetectionInactiveFrameCount() {mDetectionInactiveFrameCount=0;}
	void incDetectionInactiveFrameCount() {mDetectionInactiveFrameCount++;}

private:
	unsigned int			mID;	// target id
	bool					mActive;	// active target or not?
	// geometric features
	std::vector<cv::Point>  mCvxHull;	// initial convex hull
	std::vector<std::vector<cv::Point> >	mContours; // initial contours of the aggregated blobs

	std::vector<cv::Point>	mEstimatedPersonShape;	// estimated person shape convex hull based on the target center

	boost::shared_ptr<FaceBaseTracker>		mpTracker;

	// for verifyTargetStatus in terms of face detection
	boost::shared_ptr<FaceDetector>			mpDetector;

	// added at 08/09/16
	boost::shared_ptr<FaceVideoModule>		mpVideoModule;
	boost::shared_ptr<SETTINGS>				mpSettings;

	//boost::shared_ptr<aitvml::Recognition>  mpRecognizer;

	// for verify detected face from the tracked output
	boost::shared_ptr<MLFeature> 		mpFeature;
	//boost::shared_ptr<MLSVM> 			mpSVM;

	// target trajectory
	FaceTrajectory				mTrajectory;

	// target image data
	TargetFaceData					mFaceData;

	// Minimum threshold to keep the target active
	// proportion of the detected blob area to the size of the track box
//	const	float			mcTargetVerificationProportionToTrackbox;
	float  					mcVerfyFaceSimiliarityThreshold;
	float 					mcReactivateLikelihoodThreshold;

	int 					mDetectionInactiveFrameCount;
	int						mDetectionInactiveFrameCountTh;
};


} // of namespace vml

#endif

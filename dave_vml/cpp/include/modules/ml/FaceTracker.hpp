/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * FaceTracker.hpp
 *
 *  Created on: Jan 25, 2016
 *      Author: dkim
 */

#ifndef FACETRACKER_HPP_
#define FACETRACKER_HPP_

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "core/Settings.hpp"
#include "modules/ml/FaceBaseTracker.hpp"

#include "dlib/image_processing.h"

//#include "modules/ml/FaceDetector.hpp"
//#include "modules/ml/FaceTarget.hpp"
//#include "modules/ml/Recognition.hpp"

//#include <sys/time.h>

namespace vml {


#define HSHIST_TRACKBOX_COLOR			cv::Scalar(255,0,0)
#define YCRCBHIST_TRACKBOX_COLOR		cv::Scalar(0,255,0)
#define RGCHROMHIST_TRACKBOX_COLOR		cv::Scalar(0,255,255)
#define RGBHIST_TRACKBOX_COLOR			cv::Scalar(0,0,255)
#define DLIBCORRELATION_TRACKBOX_COLOR	cv::Scalar(255,255,0)

// forward decl
class FaceDetectorTarget;

class SingleFaceTrackerHSHist: public FaceBaseTracker
{
public:
    SingleFaceTrackerHSHist(boost::shared_ptr<Settings> settings);
    virtual ~SingleFaceTrackerHSHist() {}

//    virtual void initialize(const cv::Mat &input_image,
//            cv::Rect init_rect,
//            std::vector<std::vector<cv::Point> > *init_contour);
    //    virtual bool update(const cv::Mat &input_image,
//            cv::Mat *disp_map);

    virtual void initializeTrackerFeature(boost::shared_ptr<FaceDetectorTarget> dt);
    virtual bool updateTrackerFeature(cv::Mat *disp_map,cv::Point &new_center);
    float evalBPmap2(cv::Mat &backproj, cv::Mat &fLikelihood);

    virtual void drawTrackBox(cv::Mat &disp_image);

    float evalBPmap(cv::Mat &bp_image);

private:

    // members for face tracker
    const int		mcHBins;
    const int		mcSBins;
    const float		mcHRangeMin;
    const float		mcHRangeMax;
    const float		mcSRangeMin;
    const float		mcSRangeMax;

    cv::Mat					mHistogram;	// target color histogram.

    bool 	mUseCamShift;
    float 	terminationTH;
    float 	ratioBP;
};

class SingleFaceTrackerCrCbHist: public FaceBaseTracker
{
public:
	SingleFaceTrackerCrCbHist(boost::shared_ptr<Settings> settings);
	virtual ~SingleFaceTrackerCrCbHist() {}

//	virtual void initialize(const cv::Mat &input_image,
//			cv::Rect init_rect,
//			std::vector<std::vector<cv::Point> > *init_contour);
	//	virtual bool update(const cv::Mat &input_image,
	//			cv::Mat *disp_map);

	virtual void initializeTrackerFeature(boost::shared_ptr<FaceDetectorTarget> dt);
	virtual bool updateTrackerFeature(cv::Mat *disp_map,cv::Point &new_center);
    float evalBPmap2(cv::Mat &backproj, cv::Mat &fLikelihood);

	virtual void drawTrackBox(cv::Mat &disp_image);
    float evalBPmap(cv::Mat &bp_image);

private:

    // members for face tracker
    const int		mcCrBins;
    const int		mcCbBins;
    const float		mcCrRangeMin;
    const float		mcCrRangeMax;
    const float		mcCbRangeMin;
    const float		mcCbRangeMax;

    cv::Mat			mHistogram;	// target color histogram.

    bool 	mUseCamShift;
    float 	terminationTH;
    float 	ratioBP;
};


class SingleFaceTrackerRGChromHist: public FaceBaseTracker
{
public:
	SingleFaceTrackerRGChromHist(boost::shared_ptr<Settings> settings);
	virtual ~SingleFaceTrackerRGChromHist() {}

//	virtual void initialize(const cv::Mat &input_image,
//			cv::Rect init_rect,
//			std::vector<std::vector<cv::Point> > *init_contour);

	virtual void initializeTrackerFeature(boost::shared_ptr<FaceDetectorTarget> dt);
    virtual bool updateTrackerFeature(cv::Mat *disp_map,cv::Point &new_center);
    float evalBPmap2(cv::Mat &backproj, cv::Mat &fLikelihood);

	// update returns true if the updated target has high confidence
	// false otherwise.
//	virtual bool update(const cv::Mat &input_image,
//			cv::Mat *disp_map);

	virtual void drawTrackBox(cv::Mat &disp_image);
    float evalBPmap(cv::Mat &bp_image);

private:

    const int		mcRGBins;
	const float		mcColorRangeMin;
	const float		mcColorRangeMax;	// avoid saturated colors

	cv::Mat					mHistogram;

    bool mUseCamShift;
    float 	terminationTH;
    float 	ratioBP;
};

class SingleFaceTrackerRGBHist: public FaceBaseTracker
{
public:
	SingleFaceTrackerRGBHist(boost::shared_ptr<Settings> settings);
	virtual ~SingleFaceTrackerRGBHist() {}

//	virtual void initialize(const cv::Mat &input_image,
//			cv::Rect init_rect,
//			std::vector<std::vector<cv::Point> > *init_contour);
	//	virtual bool update(const cv::Mat &input_image,
	//			cv::Mat *disp_map);

	virtual void initializeTrackerFeature(boost::shared_ptr<FaceDetectorTarget> dt);
	virtual bool updateTrackerFeature(cv::Mat *disp_map,cv::Point &new_center);
    float evalBPmap2(cv::Mat &backproj, cv::Mat &fLikelihood);

	virtual void drawTrackBox(cv::Mat &disp_image);
    float evalBPmap(cv::Mat &bp_image);

private:

	const int		mcRGBBins;
	const float		mcColorRangeMin;
	const float		mcColorRangeMax;	// avoid saturated colors

	cv::Mat					mRHistogram;
	cv::Mat					mGHistogram;	// for three channel histograms
	cv::Mat					mBHistogram;

    bool 	mUseCamShift;
    float 	terminationTH;
    float 	ratioBP;
};

class SingleFaceTrackerDlib: public FaceBaseTracker
{
public:
	SingleFaceTrackerDlib(boost::shared_ptr<Settings> settings);
	virtual ~SingleFaceTrackerDlib() {}

//	virtual void initialize(const cv::Mat &input_image,
//			cv::Rect init_rect,
//			std::vector<std::vector<cv::Point> > *init_contour);

	//	virtual bool update(const cv::Mat &input_image,
	//			cv::Mat *disp_map);


	virtual void initializeTrackerFeature(boost::shared_ptr<FaceDetectorTarget> dt);
    virtual bool updateTrackerFeature(cv::Mat *disp_map,cv::Point &new_center);

	virtual void drawTrackBox(cv::Mat &disp_image);

private:

	// dlib::correlation tracker
	dlib::correlation_tracker	mDlibTracker;

	const float	mcBoundingBoxRatio;

};



} // of namespace vml

#endif

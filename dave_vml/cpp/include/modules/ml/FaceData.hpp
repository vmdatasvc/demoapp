/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * FaceData.hpp
 *
 *  Created on: Jan 28, 2016
 *      Author: dkim
 */

#ifndef FACEDATA_HPP_
#define FACEDATA_HPP_

#include "opencv2/core/core.hpp"
#include <modules/ml/jsoncpp/jsoncpp.hpp>

namespace vml {

typedef struct {
//struct DMGData {
	float 		genderScore;   	//  {male, female}
	float		ageScore;			//	{child, young adult, adult, senior}
	float		ethnicityScore[4];		// 	{African American, Caucacian, Oriental, Hispanic}

	int genderLabel;
	int ageLabel;
	int ethnicityLabel;

	std::string genderString;
	std::string ageString;
	std::string ethnicityString;
} DMGData;

typedef struct {

	cv::Mat		data;
//	cv::Rect    box;
	cv::Point	loc;
	cv::Point 	faceSize;
	double		time;
	// demographics confidence values
	float 		genderScore;   	//  {male, female}
	float		ageScore;			//	{child, young adult, adult, senior}
	float		ethnicityScore[4];		// 	{African American, Caucacian, Oriental, Hispanic}

	int genderLabel;
	int ageLabel;
	int ethnicityLabel;

	std::string genderString;
	std::string ageString;
	std::string ethnicityString;

} SingleFaceDataType;

class TargetFaceData
{
public:
	// FaceData id
	//unsigned int id;
	std::string id;		// need a string format for uuid

	// final demographics labels and their own confidence values by decision fusion
	unsigned int mGenderLabelFusion;
	unsigned int mAgeLabelFusion;
	unsigned int mEthnicityLabelFusion;

	float mGenderScoreFusion;
	float mAgeScoreFusion;
	float mEthnicityScoreFusion[4];

	std::string mGenderStringFusion;
	std::string mAgeStringFusion;
	std::string mEthnicityStringFusion;

	// output data for a completed target
	//std::stringstream 		msJson;
	Json::Value 			mvJson;
	//std::vector<cv::Mat> 	mFacesEnd;

	TargetFaceData():mGenderLabelFusion(0),mAgeLabelFusion(0),mEthnicityLabelFusion(0),mGenderScoreFusion(0),mAgeScoreFusion(0)
	{
		mEthnicityScoreFusion[0]=0.0;
		mEthnicityScoreFusion[1]=0.0;
		mEthnicityScoreFusion[2]=0.0;
		mEthnicityScoreFusion[3]=0.0;
	}
	~TargetFaceData() {}

	void addSingleFaceData(cv::Mat face, cv::Point loc, cv::Point fsize, double t);
	void addSingleFaceDataDMG(cv::Mat face, cv::Point loc, cv::Point fsize, double t, DMGData dmg);

	// return face data for output
	const std::vector<SingleFaceDataType> & getFaceData(void) {return mFaceDataVec;}

	const std::vector<SingleFaceDataType> & getSelectedFaceData(void) {return mSelectedFaceDataVec;}
	void addSelectedFaceDataVec(SingleFaceDataType fdv) {mSelectedFaceDataVec.push_back(fdv);}
	void clearSelectedFaceDataVec() {mSelectedFaceDataVec.clear();}
	void clearFaceDataVec() {mFaceDataVec.clear();}

private:
	// depending on the memory issue, need to save data to a disk and erase it from memory
	// then, need to connection between data images and class instance (?)
	std::vector<SingleFaceDataType>	mFaceDataVec;
	std::vector<SingleFaceDataType>	mSelectedFaceDataVec;
};

} // of namespace vml

#endif

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * FaceTarget.hpp
 *
 *  Created on: Jan 28, 2016
 *      Author: dkim
 */

#ifndef FACETARGETMANAGER_HPP_
#define FACETARGETMANAGER_HPP_

#include <sys/time.h>

#include "opencv2/core/core.hpp"

//#include "modules/ml/FaceBaseDetector.hpp"
#include <modules/ml/FaceDetector.hpp>
#include <modules/ml/FaceDetectorTarget.hpp>
#include "modules/ml/FaceBaseTracker.hpp"
#include "modules/ml/FaceTrajectory.hpp"
#include "modules/ml/Recognition.hpp"
#include "modules/ml/FaceData.hpp"
#include "modules/ml/FaceTarget.hpp"

#define NOT_A_VALID 	-10

namespace vml {
class FaceModuleManager;
class FaceTarget;
//class FaceDetectorTarget;

class FaceTargetManager {
public:

	FaceTargetManager();
	~FaceTargetManager() {}

	void initialize(boost::shared_ptr<FaceVideoModule> video,
			boost::shared_ptr<FaceDetector> target_detector,
			boost::shared_ptr<aitvml::Recognition>  target_recognizer,
			boost::shared_ptr<Settings> settings);

	bool hasTargets() {return (not mTargetList.empty());}

	void updateTargets(void);

	// recognizer from the targetList
	void DMGRecognitionFromBox(boost::shared_ptr<FaceTarget> at_ptr, const boost::shared_ptr<FaceFrame> current_frame , cv::Rect box);

	// add face data to the targetList
	void addFaceDataFromBox(boost::shared_ptr<FaceTarget> at_ptr, const boost::shared_ptr<FaceFrame> current_frame, cv::Rect box);

	// target manipulation methods
	//void addNewTargets(std::vector<boost::shared_ptr<FaceDetectorTarget> > &blob_list);

	void getDetectedTargetList();
	void addNewTargetsFromDetector();
	void verifyActiveTargetFromDetectedTarget(boost::shared_ptr<FaceTarget> activeTarget, boost::shared_ptr<FaceDetectorTarget> dt);
//	void removeTarget();

	void setTrackingMethod(FaceTrackingMethods hm) {mTrackingMethod = hm;}

	cv::Mat	&getBackProjImage(void) {return mBackProjImage;}

	void retrieveCompletedFaceData(std::vector<TargetFaceData>& fdlist);
	void retrieveCompletedTrajectories(std::vector<FaceTrajectory> &trajlist);	// change return type accordingly
	void clearCompletedTargetList(void) {mCompletedTargetList.clear();}

	/* class FaceTrajectory */
	unsigned int getNumActiveTargets(void) {return mnActiveTargets;}
	unsigned int getNumTotalTargets(void);
	unsigned int getNumInactiveTargets(void);
	unsigned int getNumCompletedTargets(void) {return mCompletedTargetList.size();}

	// Demographics module related functions
	// display for demographics on the Active targets
	void drawActiveTargets(cv::Mat &image) const;
	void drawActiveTrajectories(cv::Mat &image) const;
	void drawActiveTargetsWithDMG(cv::Mat &image) const;

private:

	// helpers
	void targetTrackerAssignHelper(boost::shared_ptr<FaceTarget> target,
			boost::shared_ptr<FaceDetectorTarget> blob);

	// members
	std::map<unsigned int,boost::shared_ptr<FaceTarget> > mTargetList;	//
	std::vector<boost::shared_ptr<FaceTarget> >			mCompletedTargetList;

	unsigned int				mLastID;

	unsigned int				mnActiveTargets;	// number of active targets. To save time for checking this
	unsigned int 				mnInactiveTargets;  // number of inactive targets.

	cv::Mat_<int>				mInactiveTargetMap;		// for managing inactive targets
	cv::Mat_<unsigned char>		mTargetActivityMap;

	cv::Mat	mBackProjImage;	// for debugging

	FaceTrackingMethods	mTrackingMethod;

	// other modules
	boost::shared_ptr<FaceVideoModule> 		mpVideoBuffer;
	boost::shared_ptr<FaceDetector>			mpFaceDetector;
	boost::shared_ptr<Settings>				mpSettings;
	boost::shared_ptr<aitvml::Recognition>  mpDMGRecognizer;

	// for multi-threads
	std::vector<boost::shared_ptr<FaceDetectorTarget> > detected_target_list;

	// const parameters
	//double	 	mInactiveTargetInterval;
	//int			mInactiveMapWindowRadius;
	//bool		mVerifyTarget;

	// for saving FaceData
	int 		mStoreFaceNumber;
	bool 		mStoreTarget;
	std::string mStoreTargetPath;

	float 		mStoreImageRatio;

	bool		mDoDMGRecognition;
	bool		mDoDMGRecognitionEnd;

	//float 	mNewTargetDistanceThreshold;

};

} // of namespace vml

#endif

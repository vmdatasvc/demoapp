/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * mlsvm.hpp
 *
 *  Created on: Aug 8, 2016
 *      Author: dkim
 */

#ifndef MLSVM_HPP
#define MLSVM_HPP

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/ml/ml.hpp>

#include "boost/shared_ptr.hpp"
#include <core/Settings.hpp>

#include <modules/ml/libsvm.h>
#include <float.h>

#define USE_LIBSVM

//#define MLSVM_DEBUG    // print out the predict_label by svm_predict

namespace vml {

class MLSVM
{
public:
	MLSVM(boost::shared_ptr<vml::Settings> settings);      // with setting file
    ~MLSVM();

    void 	setParams(boost::shared_ptr<vml::Settings> settings);

#ifdef USE_LIBSVM
    double 	predict(cv::Mat testData);
    double 	predict(std::vector<float> featVec);
    void 	predictFromFile(std::FILE *input, std::FILE *output);
    int 	loadModelFromFile(std::string filename);

    float 	getTestAccuracy(){ return testAcc;}
#endif

#ifdef USE_OPENCV_SVM
    void    train(cv::Mat trainDataMat, cv::Mat labelDataMat);
    float   predict(cv::Mat testData);
    void    saveModelToFile(std::string filename);

    cv::Ptr<cv::ml::SVM> pSVM;
    std::vector<float> decriptorVector;
#endif

//protected:
private:

#ifdef USE_LIBSVM
    // For training
    struct svm_parameter param;
    struct svm_problem prob;
    struct svm_model *model;
    struct svm_node *x_space;
    int cross_validation;
    int nr_fold;

    // For prediction
    struct svm_node *x;
    int max_nr_attr = 64;


    int predict_probability;

    char *pline;
    int max_line_len;


    char* readline(std::FILE *input);
    void  exit_input_error(int line_num);
    float testAcc;

    float scaleL;
    float scaleU;
#endif


};

} // of namespace vml
#endif // MLSVM_H

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * VisionSiteSetup.hpp
 *
 *  Created on: Jan 29, 2017
 *      Author: yyoon
 */

#ifndef VISIONSITESETUP_HPP_
#define VISIONSITESETUP_HPP_

#include <opencv2/core/core.hpp>
#include <boost/shared_ptr.hpp>

#include <map>

namespace vml {

class PostProcessingVisionTrajectory;
class CameraModelOpenCV;
class VisionSiteCalibration;
class VisionCameraCalibration;
class FurnitureLines;

enum VisionPostProcessingJoinType
{
	VisionPostProcessing_OverlapJoin,
	VisionPostProcessing_JR
};

class TrajectorySet
{
	// base class of trajectory set

public:

	struct JRThresholdStruct
	{
		JRThresholdStruct(double mint,double maxt,float d):min_time_diff(mint),max_time_diff(maxt),distance(d) {}
		double	min_time_diff;
		double	max_time_diff;
		float	distance;
	};

	TrajectorySet(std::string name,int id, int level_id, boost::shared_ptr<FurnitureLines> pf):mName(name),
			mLevelID(level_id),
			mID(id),
			mJoinMethod(VisionPostProcessing_OverlapJoin),
			mJODistanceThreshold(25.f) {}
	~TrajectorySet() {}

	void addTrajectoryFromCSVLine(std::string);

	void setName(std::string name) { mName = name;}
	void setID(int id) { mID = id;}

	std::string getName()	{return mName;}
	int			getID()		{return mID;}

	void filterShortTrajectory(float short_length_threshold);
	void filterCartTrajectory(double start_time_window,
			float dist_threshold,
			float front_and_back_dist_threshold);
	void convertToFloorPlan(VisionSiteCalibration *site_calib);
	void convertToWorld(VisionSiteCalibration *site_calib);
	void convertWorldToFloorPlan(VisionSiteCalibration *site_calib);

	void parseJoinSetting(std::string join_setting_string);

	void addSubSet(boost::shared_ptr<TrajectorySet> p_subset);

	void aggregate(std::vector<boost::shared_ptr<PostProcessingVisionTrajectory> > &aggregated_list);
	void incrementalJoin();
	void overlapJoin();
	void rectJoin(JRThresholdStruct thresholds);
	void join();
	void recursiveJoin();
	void compact();	// delete invalid trajectory and compact the list

	void readFromFile(std::string file_name);
	void readFromDBTable();
	void saveToFile(std::string filename);
	void saveToDBTable();

	void resetSetNameToTrajectories();

	// for visualization purpose
	void displayTrajectory(cv::Mat &display_image);

protected:
	std::string	mName;
	int			mLevelID;
	int			mID;
	// storage that stores the list of trajectories
	std::vector<boost::shared_ptr<PostProcessingVisionTrajectory> >	mTrajectoryList;

	std::vector<boost::shared_ptr<TrajectorySet> > mSubSetList;

	// for joining params
	VisionPostProcessingJoinType	mJoinMethod;

	// for overlap join
	float	mJODistanceThreshold;

	// for JR
	std::vector<JRThresholdStruct>	mJRThresholdVec;

	// for filtering out restricted area
	boost::shared_ptr<FurnitureLines> mpFurniture;
};


// aisle and camera set classes

//class TrajectorySetLevel0: public TrajectorySet
//{
//	// class that holds trajectory sets
//public:
//
//	TrajectorySetLevel0() {}
//	~TrajectorySetLevel0() {}
//
//	void filterShortTrajectory();
//	void filterCartTrajectory();
//	void convertToFloorPlan(VisionSiteCalibration *site_calib);
//
//};

//class TrajectorySetLevel1 : public TrajectorySet
//{
//public:
//	TrajectorySetLevel1() {}
//	~TrajectorySetLevel1() {}
//
//	void addLevel0(boost::shared_ptr<TrajectorySetLevel0> p);
//
//private:
//	std::vector<boost::shared_ptr<TrajectorySetLevel0> > mLevel0SetList;
//};
//
//class TrajectorySetLevel2 : public TrajectorySet
//{
//public:
//	TrajectorySetLevel2() {}
//	~TrajectorySetLevel2() {}
//
//	void addLevel1(boost::shared_ptr<TrajectorySetLevel1> p);
//
//private:
//
//	std::vector<boost::shared_ptr<TrajectorySetLevel1> > mLevel1SetList;
//
//};
//
//class TrajectorySetLevel3 : public TrajectorySet
//{
//public:
//
//	TrajectorySetLevel3() {}
//	~TrajectorySetLevel3() {}
//
//	void addLevel2(boost::shared_ptr<TrajectorySetLevel2> p);
//
//private:
//
//	std::vector<boost::shared_ptr<TrajectorySetLevel2> >	mLevel2SetList;
//};

class FurnitureLines
{
public:
	struct Line
	{
		Line(cv::Point2f b_,cv::Point2f e_):b(b_),e(e_){}

		cv::Point2f	b;
		cv::Point2f e;
	};

	FurnitureLines();
	~FurnitureLines();

	void readfromFile(std::string file_name);
	void addFurnitureLine(cv::Point2f a, cv::Point2f b);

	bool doesIntersect(cv::Point2f l1b,cv::Point2f l1e);

	// for debugging
	void draw(cv::Mat disp_image);

private:
	std::vector<Line>	mLines;
};

// Vision Camera coverage
class VisionCameraCoverage
{
public:

	struct CoverageModel
	{
		float		camera_height;
		float		coverage_width;
		float		coverage_height;
		float		undistorted_image_width;
	};

	struct CameraModelInfo
	{
		std::string name;
		float		ccd_width;
		float		ccd_height;
		float		focal_length;
		std::vector<double>		distortion_parameters;
		std::string	stream_url;
		std::string	config_url;
		std::string	username;
		std::string	password;
		std::vector<CoverageModel>	coverages;
	};

	VisionCameraCoverage();
	~VisionCameraCoverage();


	void readFromXMLFile(std::string filename);

	bool getCoverage(std::string type, CameraModelInfo &cminfo);

private:

	std::map<std::string,CameraModelInfo>	mVisionCoverageMap;
};

// class that contains intrinsic (camera model) and extrinsic camera calibration
class VisionCameraCalibration
{
public:
	VisionCameraCalibration(std::string name,
			std::string type,
			cv::Point2f	location,
			float	camera_height,
			float	orientation,
			float	focalLength,
			cv::Point2f	ccdSize,
			cv::Point2i	imageSize,
			boost::shared_ptr<CameraModelOpenCV> p_camera_model);
	~VisionCameraCalibration();

	void projectToWorld(std::vector<cv::Point2f> &input_list,std::vector<cv::Point2f> &output_list);
	std::string getName() {return mName;}

private:
	std::string mName;
	std::string mType;
	cv::Point2f	mLocation;
	float		mCameraHeight;
	float	mOrientation;
	float	mFocalLength;
	cv::Point2f	mCcdSize;
	cv::Point2i	mImageSize;
	boost::shared_ptr<CameraModelOpenCV>	mpCameraModel;

	cv::Mat_<float>		mRotation;		// rotation matrix
	cv::Point3f	mTranslation;
};

class VisionSiteCalibration
{
public:

	VisionSiteCalibration();
	~VisionSiteCalibration();

	void readFromXMLFiles(std::string site_calibration_filename,
			std::string camera_coverage_filename);

//	void convertFromWorldToFloorPlan(std::vector<boost::shared_ptr<PostProcessingVisionTrajectory> > &trajectory_list);
	boost::shared_ptr<VisionCameraCalibration>	getCalibration(std::string camera_name);

	cv::Mat	getHomography() {return mFloorHomography;}

private:
	std::vector<cv::Point2f>	mLocal;
	std::vector<cv::Point2f>	mWorld;

	// define world 2 floorplan homography
	cv::Mat			mFloorHomography;

	VisionCameraCoverage									mVisionCameraCoverage;
	std::map<std::string,boost::shared_ptr<CameraModelOpenCV> >	mCameraModelMap;
	std::map<std::string,boost::shared_ptr<VisionCameraCalibration> >	mpVisionCameraCalibrationMap;
};

// parsing helper function
// read parenthesis enclosed and comma separated coords
// example: "(2.5,3.1,0.3)" -> [2.5 2.1 0.3]
void parseCoords(std::string coords_str, std::vector<float> &coords);
void parseCoords(std::string coords_str, std::vector<int> &coords);
void parseFloatList(std::string float_list_str, std::vector<float> &flist);
void parseDoubleList(std::string float_list_str, std::vector<double> &flist);

} // of namespace vml

#endif

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * BaseVisionTarget.hpp
 *
 *  Created on: Jan 24, 2015
 *      Author: yyoon
 */

#ifndef BASEVISIONTARGET_HPP_
#define BASEVISIONTARGET_HPP_

#ifndef _WIN32
#include <sys/time.h>
#endif

#include <boost/shared_ptr.hpp>
#include <core/Video.hpp>
#include <modules/vision/VisionTrajectory.hpp>
#include <modules/vision/BaseVisionTracker.hpp>

namespace vml {

class BaseVisionTarget {	// base class for visual tracking target
	friend class VisionTargetManager;
public:

	BaseVisionTarget();
	virtual ~BaseVisionTarget() {}

	// pure virtuals
	virtual bool updateTargetStatus(cv::Mat &backprojectionimage) = 0;
	virtual bool verifyTargetStatus() = 0;
	// check the similarity to the other target. Return true if similar enough
	virtual bool checkSimilarity(boost::shared_ptr<BaseVisionTarget> pt) = 0;

	// merge two targets
	virtual void merge(boost::shared_ptr<BaseVisionTarget> pt);

	double getLastTime(void) const;
	cv::Point getPosition() {return mPosition;}

	// display methods
	virtual void drawTarget(cv::Mat &image) const;
	virtual void updateTargetActivityMap(cv::Mat &activity_map) = 0;

protected:
	bool		mActive;	// active target or not?

	// tracker target attribute
	// Position of the target - the only geometric feature of base target type
	cv::Point 	mPosition;
	boost::shared_ptr<BaseVisionTracker>	mpTracker;
	// target trajectory
	VisionTrajectory				mTrajectory;
};

} // of namespace vml

#endif

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * BaseDetectedTarget.hpp
 *
 *  Updated on Jan 5, 2016
 *      Author: yyoon
 *
 */

#ifndef BASEDETECTEDTARGET_HPP_
#define BASEDETECTEDTARGET_HPP_

#include <boost/shared_ptr.hpp>
#include <opencv2/core/core.hpp>
#include <core/Settings.hpp>

namespace vml {

class BaseDetectedTarget
{
public:

	BaseDetectedTarget(boost::shared_ptr<SETTINGS> settings);
	virtual ~BaseDetectedTarget();

	virtual void drawTarget(cv::Mat &image) const = 0;

	bool checkTargetActivityOverlap(cv::Mat &target_activity_map);
protected:

	cv::Point	mPosition;
	cv::Rect	mDetectedBoundingRect;

	const float	mcDetectionOverlapWithTrackingTargetThreshold;

	boost::shared_ptr<SETTINGS> mpSettings;
};

} // of namespace vml

#endif

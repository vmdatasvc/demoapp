/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * BaseDetector.hpp
 *
 *  Created on: Oct 6, 2015
 *  Updated on Jan 5, 2016
 *      Author: yyoon
 *
 */

#ifndef BASEDETECTOR_HPP_
#define BASEDETECTOR_HPP_

#include <boost/shared_ptr.hpp>
#include <opencv2/core/core.hpp>
//#include <modules/vision/VisionModuleManager.hpp>
#include <core/Video.hpp>
//#include <modules/vision/VisionTargetManager.hpp>

namespace vml {

class VisionModuleManager;

// Abstract base class for target detectors
class BaseDetector
{
public:
	BaseDetector(boost::shared_ptr<VisionModuleManager> vf);
	virtual ~BaseDetector();

	// virtual methods
	virtual void initialize(void) = 0;
	// NOTE: detect() must update mBlobMap for target tracking
	// TODO: think about how to enforce this.
	virtual	void detect(void) = 0;	// detect new targets

	// select appropriate targets from detected list and move them to target manager
	virtual void addNewTargetsToTargetManager(void) = 0;

	virtual bool verifyForegroundActivity(cv::Rect box) = 0;
	virtual void drawDetectedTargets(cv::Mat &image) const = 0;

protected:
	// parameters
	int		mMaxTargets;
	int		mMinTargetArea;

	boost::shared_ptr<VisionModuleManager>	mpVisionModuleManager;
};

} // of namespace vml

#endif

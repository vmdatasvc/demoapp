/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * VisionTrajectory.hpp
 *
 *  Created on: Jan 24, 2015
 *      Author: yyoon
 */

#ifndef VISIONTRAJECTORY_HPP_
#define VISIONTRAJECTORY_HPP_

#ifdef _WIN32
#include <Trajectory.hpp>
//namespace vml {
//typedef ait::vision::Trajectory				VisionTrajectory;
//typedef ait::vision::Trajectory::Nodes		VisionNodes;
//typedef ait::vision::Trajectory::Node		VisionNode;
//}
#endif

#include <boost/shared_ptr.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

namespace vml {

class CameraModelOpenCV;
class VisionCameraCalibration;
class FurnitureLines;
class VisionTrajectory {
public:
	struct TrajectoryPoint {

		float dist(const TrajectoryPoint other) const;
		cv::Point2f	undistort() const;

		static TrajectoryPoint interpolate(TrajectoryPoint b, TrajectoryPoint e, double time);

		cv::Point2f	loc;
		double		time;
	};

	VisionTrajectory():mValid(true),mID(0) {}
	~VisionTrajectory() {}

	bool isValid() {return mValid;}
	void setInvalid() {mValid = false;}
	void setValid() {mValid = true;}

	void addTrajectoryPoint(cv::Point loc, double t);
	void setID(unsigned int id) {mID=id;}



#ifdef _WIN32
	void convertToAITTrajectory(ait::vision::Trajectory &ait_traj);
#endif

	// return trajectory for output
//	const std::vector<TrajectoryPoint> & getTrajectory(void) const;

	// return first point time
	double getStartTime(void) const;
	// return last point time
	double getEndTime(void) const;
	// return duration
	double getDuration(void) const;

	TrajectoryPoint getFrontPoint() const { return mTrajectoryPts.front();}
	TrajectoryPoint getBackPoint() const { return mTrajectoryPts.back();}

	// motion model here
	// currently just use the previous location and enlarged window size

	// merging trajectories
	void append(VisionTrajectory &tj);
	void prepend(VisionTrajectory &tj);

	unsigned int nodeCount() const {return mTrajectoryPts.size();}
	// accumulated distance between each node pair
	float length() const;

	// Report method
	std::string toXmlString(unsigned int decimals) const;
	std::string toCSVString(unsigned int decimals) const;

	// Display methods
	void draw(cv::Mat &disp_image, cv::Scalar line_color = CV_RGB(125,0,255)) const;
	// for display trajectory
	friend std::ostream& operator<<(std::ostream& os, const VisionTrajectory &obj);

protected:
	bool							mValid;	// validity of this trajectory. Flag used for joining
	unsigned int					mID;
	std::vector<TrajectoryPoint>	mTrajectoryPts;
};

// functor for sorting trajectories by their start time in ascending order
class VisionTrajectoryStartTimeCompare {
public:
	int operator() (const boost::shared_ptr<VisionTrajectory> ct1,
			const boost::shared_ptr<VisionTrajectory> ct2)
	{
		return (ct1->getStartTime() < ct2->getStartTime());
	}
};


// vision trajectory for post processing
class PostProcessingVisionTrajectory : public VisionTrajectory
{
public:
	PostProcessingVisionTrajectory() {mValid = false;}
	PostProcessingVisionTrajectory(std::string set_name):mSetName(set_name) {}
	~PostProcessingVisionTrajectory() {}

	// code for trajectory post processing
	void undistort(boost::shared_ptr<CameraModelOpenCV> p_cam_model);
	void convertToWorld(boost::shared_ptr<VisionCameraCalibration> p_calibration);
	void applyHomography(cv::Mat homography);

	// distance measures for merging and joining
	float minDistance(PostProcessingVisionTrajectory& other, bool &in);
	float minDistanceWithTimeDomain(PostProcessingVisionTrajectory& other);
	float endPointDistance(PostProcessingVisionTrajectory& other);
	float endPointDistanceWithTimeDomain(PostProcessingVisionTrajectory& other);

	float distToFront(TrajectoryPoint other);
	float distToBack(TrajectoryPoint other);

	// merge the other trajectory to this
	bool joinTrajectoryByOverlap(boost::shared_ptr<PostProcessingVisionTrajectory> other,
			boost::shared_ptr<FurnitureLines> p_furniture);

	bool joinTrajectoryByRect(boost::shared_ptr<PostProcessingVisionTrajectory> other,
			float dist_thresh,
			float time_thresh,
			boost::shared_ptr<FurnitureLines> p_f);

	void merge(boost::shared_ptr<PostProcessingVisionTrajectory> other) {this->append(*other);}

	void setSetName(std::string set_name) {mSetName = set_name;}
	std::string getSetName() {return mSetName;}
	bool belongToSameSet(boost::shared_ptr<PostProcessingVisionTrajectory> other) {return (this->mSetName == other->mSetName);}

	// TODO: toRDS
	// read methods
	void fromCSVString(std::string csvstr);
	void fromXmlString(std::string xmlstr, double start_time);
	// TODO: fromRDS

private:
	std::string			mSetName;
};

} // of namespace vml

//#endif

#endif

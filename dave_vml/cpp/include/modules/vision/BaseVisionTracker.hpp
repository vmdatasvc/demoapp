/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * BaseVisionTracker.hpp
 *
 *  Created on: Jan 24, 2016
 *      Author: yyoon
 */

#ifndef BASEVISIONTRACKER_HPP_
#define BASEVISIONTRACKER_HPP_

//#define VISIONTRACKERS_DEBUG_SHOW		// uncomment this line to enable debug output window

#include <opencv2/core/core.hpp>
#include <opencv2/video/tracking.hpp>
#include <boost/shared_ptr.hpp>

//#include <core/Video.hpp>
#include <core/Settings.hpp>
//#include <modules/vision/VisionTrackerMethods.hpp>
#include <modules/vision/PersonShapeModel.hpp>

namespace vml {

class VisionVideoModule;
class VisionDetector;
class VisionDetectorTarget;

// TrackingMethods enum
enum TrackingMethods {
	TrackingMethod_HIST_HSV,
	TrackingMethod_HIST_RGB,
//	TrackingMethod_DLIB_Correlation_Tracking,  // no longer supported
	TrackingMethod_KDE,
	TrackingMethod_HybridHist
};

enum TrackingStatusEnum {
	TrackingStatus_Initialized,
	TrackingStatus_InStart,	// region status
	TrackingStatus_InTrack,
	TrackingStatus_Invalid,
	TrackingStatus_Active,	// track status
	TrackingStatus_Inactive,
	TrackingStatus_Finished
};

class BaseVisionTracker
{
public:

	BaseVisionTracker(const int id, boost::shared_ptr<SETTINGS> settings);

	virtual ~BaseVisionTracker() {}

	void initialize(boost::shared_ptr<VisionDetectorTarget> dt);
	virtual void initializeTrackerFeature(boost::shared_ptr<VisionDetectorTarget> dt) = 0;

	TrackingStatusEnum reactivate(std::vector<boost::shared_ptr<VisionDetectorTarget> > &dt_list);

	TrackingStatusEnum update(cv::Mat *disp_map);
	virtual bool updateTrackerFeature(cv::Mat *disp_map,cv::Point &new_center) = 0;

	void updateKalmanFilter(cv::Point newPosition, bool update_success);

	cv::Rect getCropRect(void) const {return mCropRect;}

	cv::Point getCenter(void) const {return mCenter;}
	cv::Point getPosition(void) const {return mPosition;}
	float	getConfidence(void) const {return mConfidence;}

	virtual void drawTracker(cv::Mat &disp_image) = 0;
	void getHumanShapeMap(cv::Mat &hmap);
	void getCroppedHumanShapeMap(cv::Mat &hmap);

	static TrackingMethods readTrackingMethod(boost::shared_ptr<SETTINGS> settings);

	static void setTrackingMethod(const TrackingMethods tm, boost::shared_ptr<SETTINGS> settings);

	bool isInTrackRegion() {return mRegionStatus == TrackingStatus_InTrack;}
	bool isInStartRegion() {return mRegionStatus == TrackingStatus_InStart;}
	bool isInTrackStartRegion() {return (mRegionStatus == TrackingStatus_InTrack || mRegionStatus == TrackingStatus_InStart);}	// this is not needed cause start region always enclose track region
	float searchAreaMahalanobisDistance(cv::Point p1,cv::Point p2);

	int	getInactiveFrameCount() const {return mInactiveFrameCount;}

	float calculateLocationLikelihoodKF(cv::Point pos);
	float calculateLocationLikelihoodKF(int x, int y);

	boost::shared_ptr<VisionDetectorTarget> mergeAndFindCandidateBlob(cv::Mat likelihood_map);

protected:
	void drawTrackerHelper(cv::Mat &disp_image, cv::Scalar dcolor);

	bool isInSearchArea(cv::Point position);
	void calcLocationUncertaintyKF();

	const int				mID;	// id not needed for tracking, but for debugging
	const int 				rows;
	const int				cols;

	// search window parameters
	const float				mSearchWindowRatio;	// must be strictly larger than 1

	TrackingStatusEnum		mRegionStatus;
	TrackingStatusEnum		mTrackStatus;

//	cv::Rect				mTrackBox;	// tight bounding box of target
	cv::Rect 				mCropRect;	// search window
	cv::Point 				mCenter;	// center of the trackbox (or person shape)
	cv::Point 				mPosition;	// considered position of the target. This is the position of the foot of the shopper

	///////////////////////////////////////////////////////
	// Kalman filter members
	cv::KalmanFilter		mKF;
	float					mKFInitialStateUncertaintyLoc;
	float					mKFInitialStateUncertaintyVel;
	float					mKFProcessNoiseLoc;
	float					mKFProcessNoiseVel;
	float					mKFMeasurementNoise;
	cv::Mat					mKFMeasurement;
	cv::Point				mKFPrediction;
	float					mLocationUncertaintyInvKF[3];	// from Kalman filter
	///////////////////////////////////////////////////////

	bool					mReactivated;
	int						mInactiveFrameCount;

	PersonShapeModel		mPersonShape;


	int						mArea;		// area of target - obsolete
	float					mConfidence;	// tracking confidence value - obsolete

	// constant members
	const float				mcMergeLikelihoodThreshold;
	const float				mcMatchLikelihoodThreshold;
	const int				mcInactiveTimeout;
	const float				mcReactivateLikelihoodThreshold;

	// for display
	bool					mDisplayCurrentPosition;
	std::vector<cv::Point>  bestMatchingBlob;
	cv::Point				prevPosition;
	std::vector<cv::Point>	prevSearchAreaContour;
	std::vector<cv::Point>	prevPersonShape;


	boost::shared_ptr<VisionVideoModule> 	mpVideoModule;		// cacheing pointer for saving access time
	boost::shared_ptr<VisionDetector> 		mpVisionDetector;	// cacheing pointer to VisionDetector

#ifdef VISIONTRACKERS_DEBUG_SHOW
public:

	static cv::Mat	mDisplayImage;
	static const char *displayname;

	void debugShow(cv::Mat &dispimage);
#endif

};

} // of namespace vml

#endif

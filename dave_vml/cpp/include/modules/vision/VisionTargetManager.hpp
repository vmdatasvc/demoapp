/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * VisionTargetManager.hpp
 *
 *  Created on: Jan 24, 2015
 *      Author: yyoon
 */

#ifndef VISIONTARGETMANAGER_HPP_
#define VISIONTARGETMANAGER_HPP_

//#include <modules/vision/VisionModuleManager.hpp>

//#define VISIONMANAGER_DEBUG_SHOW

#ifndef _WIN32
#include <sys/time.h>
#endif

#include <opencv2/core/core.hpp>
#include <boost/shared_ptr.hpp>
#include <map>
#include <modules/vision/BaseVisionTracker.hpp>
#include <modules/vision/VisionTrajectory.hpp>
#include <core/Settings.hpp>

#ifdef _WIN32
#include <Trajectory.hpp>
#endif


namespace vml {

class VisionModuleManager;
class VisionTrackerTarget;

class VisionTargetManager {
public:

	VisionTargetManager();
	~VisionTargetManager() {}

	void initialize(boost::shared_ptr<SETTINGS> settings);

	bool hasTargets() {return (!mTargetMap.empty());}

	void updateTargets(void);
	// target manipulation methods
//	void addNewTarget(boost::shared_ptr<VisionDetectorTarget> pt);

	void addNewTargetsFromDetector();
//	void removeTarget();

	void completeAllTargets();

#ifdef _WIN32
	void retrieveCompletedTrajectories(std::vector<ait::vision::Trajectory> &trajlist);	// change return type accordingly
#else
	void retrieveCompletedTrajectories(std::vector<VisionTrajectory> &trajlist);	// change return type accordingly
#endif

	// display
	void drawActiveTargets(cv::Mat &image) const;
	void drawActiveTrajectories(cv::Mat &image) const;
//	void drawUndistortedActiveTargets(cv::Mat &image) const;
	void drawInactiveTargets(cv::Mat &image) const;
	void drawAllTargets(cv::Mat &image) const;

	/* class VisionTrajectory */
	unsigned int getNumActiveTargets(void) {return mnActiveTargets;}
	unsigned int getNumTotalTargets(void);
	unsigned int getNumInactiveTargets(void);
	unsigned int getNumCompletedTargets(void) {return mCompletedTargetList.size();}

	cv::Mat	&getBackProjImage(void) {return mBackProjImage;}
//	cv::Mat &getTargetActivityMap(void) {return mTargetActivityMap;}

private:

	// members
	std::map<unsigned int,boost::shared_ptr<VisionTrackerTarget> > mTargetMap;	//
	std::vector<boost::shared_ptr<VisionTrackerTarget> >			mCompletedTargetList;

	unsigned int				mLastID;

	unsigned int				mnActiveTargets;	// number of active targets. To save time for checking this
	unsigned int				mnInactiveTargets;	// number of inactive targets.

//	cv::Mat_<int>				mInactiveTargetMap;		// for managing inactive targets
//	cv::Mat_<unsigned char>		mTargetActivityMap;

	cv::Mat	mBackProjImage;	// for debugging

	TrackingMethods	mTrackingMethod;

	double		mLastTimeCheck;

	// const parameters
	int			mInactiveMapWindowRadius;
	bool		mVerifyTarget;
	float		mTrajectoryLengthThreshold;
	double		mTrajectoryDurationThreshold;
	float		mNewTargetMahalanobisDistanceThreshold;

	unsigned int	mNewTrajectoryID;


	boost::shared_ptr<SETTINGS> mpSettings;

#ifdef VISIONMANAGER_DEBUG_SHOW
public:

	static cv::Mat	mDisplayImage;
	static const char *displayname;

	void debugShow(cv::Mat &dispimage);
	void debugShow(void);
#endif


};

} // of namespace vml

#endif

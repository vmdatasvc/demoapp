/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * PersonShapeModel.hpp
 *
 *  Created on: Jun 22, 2016
 *      Author: yyoon
 */

#ifndef VML_PERSONSHAPEMODEL_HPP_
#define VML_PERSONSHAPEMODEL_HPP_

#include <core/Video.hpp>

namespace vml {

class PersonShapeModel
{
public:

	PersonShapeModel();
	~PersonShapeModel();
	
	void initialize(boost::shared_ptr<SETTINGS> settings,
					boost::shared_ptr<VisionVideoModule> video_module);

//	void estimateUndistortedPersonShapeConvexHull(int x, int y, std::vector<cv::Point> &cvxhull); // not complete yet
	void estimatePersonShape(int x, int y);
	void estimatePersonShape(cv::Point center);

	// this method calculates pca, and inverse-cov for mahalanobis distance, etc.
	// NOTE: this is seperated from estimatePersonShape cause for some person shape routine, this is not needed, so
	//       this calculation is called when necessary.
	bool updateShapeInfo();

	cv::Point getHeadPosition() const {return mHeadPosition;}
	cv::Point getFootPosition() const {return mFootPosition;}
	cv::Point getCenterPosition() const {return mCenter;}

	cv::Point getFootPosFromCenter(cv::Point center) const;
	cv::Point getHeadPosFromCenter(cv::Point center) const;

	cv::Rect getBoundingRect() const {return mBoundingRect;}

	float	 getFloorRadius() const;

	// accessors for shape info
	int  getArea() const {return mArea;}
	float getLength() const {return mLength;}

	cv::Point2f getMajorAxis() const {return mMajorAxis;}
	cv::Point2f getMinorAxis() const {return mMinorAxis;}
	float getMajorAxisLength() const {return mMajorAxisLength;}
	float getMinorAxisLength() const {return mMinorAxisLength;}

	// distance measures
	float EuclideanDistance(PersonShapeModel &other) const;
	float EuclideanDistance(cv::Point other) const;
	float MahalanobisDistance(PersonShapeModel &other) const;
	float MahalanobisDistance(cv::Point other) const;

	// drawing functions
	void draw(cv::Mat &image,cv::Scalar color) const;

	// humann shape map
	void drawOccupancyMap(cv::Mat &hmap) const;
	void drawOccupancyMapCropped(cv::Mat &hmap,cv::Rect crop_rect) const;

	// just estimate human shape map from given center without updating the status
	void estimateOccupancyMap(cv::Mat &hmap, cv::Point temp_center) const;
	void estimateOccupancyMapCropped(cv::Mat &hmap, cv::Point temp_center, cv::Rect crop_rect) const;

private:	

	// data members
	std::vector<cv::Point>	mPersonShapeContour;	// estimated person shape convex hull based on the target center
//	cv::Mat					mPersonMap;	// map of estimated person shape	// not needed
	cv::Point				mCenter;
	cv::Point				mHeadPosition;
	cv::Point				mFootPosition;
	cv::Rect				mBoundingRect;

	// contour shape info
	// NOTE: WARNING!!! these info is only available after calling calcShapeInfo();
	int						mArea;	// area of estimated person shape
	float					mLength;
	cv::Point2f				mMajorAxis;
	float					mMajorAxisLength;
	cv::Point2f				mMinorAxis;
	float					mMinorAxisLength;
	float					mSinv[3];

	// parameters for person shape estimation
	float	mPersonRadius;
	float	mPersonHeight;

	// accessing pointers to the required modules
	boost::shared_ptr<CameraModelOpenCV>	mpCameraModel;
	boost::shared_ptr<VisionVideoModule>	mpVideoModule;
	boost::shared_ptr<Settings>				mpSettings;
};

} // of namespace vml

#endif

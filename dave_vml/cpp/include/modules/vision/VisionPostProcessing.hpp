/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * VisionPostProcessing.hpp
 *
 *  Created on: Jan 29, 2017
 *      Author: yyoon
 */

#ifndef VISIONPOSTPROCESSING_HPP_
#define VISIONPOSTPROCESSING_HPP_

#include <opencv2/core/core.hpp>
#include <boost/shared_ptr.hpp>

#include <modules/vision/VisionSiteSetup.hpp>
#include <core/Settings.hpp>
#include <core/CameraCalibration.hpp>

namespace vml {

#define SCREENSHOTWINDOWNAME	"ScreenShot Display"
#define FLOORPLANWINDOWNAME		"Trajectory Display"

// use global instance of settings
//boost::shared_ptr<SETTINGS>	getSettings();

// data structure for defining camera sets(aisles), groups and store
class VisionPostProcessor
{
public:

	VisionPostProcessor():mShotTrajectoryFilterThreshold(50.f),
	mCartTrajectoryFilterStartTimeWindow(0.6f),
	mCartTrajectoryFilterDistThreshold(5.f),
	mCartTrajectoryFilterFrontBackDistThreshold(23.f),
	mbSaveIntermediateResult(false) {}
	~VisionPostProcessor();

	void initialize(std::string date,
			std::string data_path,
			std::string level1_join_setting,
			std::string level2_join_setting,
			std::string level3_join_setting);
	void setLevel1(std::string data_path, std::string join_setting);
	void setLevel2(std::string data_path, std::string join_setting);
	void setLevel3(std::string data_path, std::string join_setting);
	void setCalibration(std::string calib_path, std::string coverage_path);
	void setFurniture(std::string data_path);
	void readTrajectories(std::string data_path);
	void setDate(double date);
	void setDate(std::string date) {mDateString = date;}

	void processLevel0(std::string save_path);
	void joinLevel1(std::string save_path);
	void joinLevel2(std::string save_path);
	void joinLevel3(std::string save_path);

	void run(std::string save_path);

	void setDisplay(bool turn_on);
	void setDisplayDimension(unsigned int _w, unsigned int _h);
	void setSaveIntermediateResult(bool flag) { mbSaveIntermediateResult = flag;}

private:

	std::string			mDateString;

	// trajectory tree
	boost::shared_ptr<TrajectorySet>	mpTrajectoryTreeRoot;

	// maps to the each layer of the tree
	std::map<int,boost::shared_ptr<TrajectorySet> >	mTrajectorySetLevel0Map;
	std::map<int,boost::shared_ptr<TrajectorySet> >	mTrajectorySetLevel1Map;
	std::map<int,boost::shared_ptr<TrajectorySet> >	mTrajectorySetLevel2Map;

	boost::shared_ptr<FurnitureLines>						mpFurnitureLines;

	VisionSiteCalibration									mSiteCalibration;

	// private thresholds
	float		mShotTrajectoryFilterThreshold;
	double		mCartTrajectoryFilterStartTimeWindow;
	float		mCartTrajectoryFilterDistThreshold;
	float		mCartTrajectoryFilterFrontBackDistThreshold;

	bool		mbSaveIntermediateResult;

};

// for display
namespace VisionPostProcDisplay	// for vision post processing display
{
void setDisplayDimension(unsigned int _w, unsigned int _h);
cv::Mat&	getFloorplanImage();
cv::Mat&	getScreenShotImage();
bool isDisplayOn();
void showDispImage(cv::Mat &disp_image, const char *window_name, int wait_time = 500);
}

} // of namespace vml

#endif

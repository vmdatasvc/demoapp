/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * VisionTrackerTarget.hpp
 *
 *  Created on: Jan 24, 2015
 *      Author: yyoon
 */

#ifndef VISUALTRACKINGTARGET_HPP_
#define VISUALTRACKINGTARGET_HPP_

#include <boost/shared_ptr.hpp>
#include <opencv2/core.hpp>
#include <core/Settings.hpp>
#include <core/Video.hpp>
#include <modules/vision/VisionTrajectory.hpp>
#include <modules/vision/BaseVisionTracker.hpp>

namespace vml {

// forward declaration
class VisionDetector;
class VisionDetectorTarget;
class VisionModuleManager;


class VisionTrackerTarget
{	// base class for visual tracking target

	friend class VisionTargetManager;

public:

	VisionTrackerTarget(const int id);
	virtual ~VisionTrackerTarget() {}

	// methods
	// initializers for detection targets
	void initialize(boost::shared_ptr<VisionDetectorTarget> dt);
	// other detection target initializer goes here

	// virtual method for target matching or searching
	TrackingStatusEnum updateTargetStatus(cv::Mat &bpimage);
	TrackingStatusEnum reactivateTarget(std::vector<boost::shared_ptr<VisionDetectorTarget> > &dt_list);
	bool checkSimilarity(boost::shared_ptr<VisionTrackerTarget> pt);


	// methods for merging
	void merge(boost::shared_ptr<VisionTrackerTarget> pt);

	double getLastTime(void) const;
	cv::Point getPosition() const;
	int	getID() const {return mID;}

	// drawing methods
	void drawTarget(cv::Mat &image) const;
//	void drawUndistortedTarget(cv::Mat &image) const;
//	void updateTargetActivityMap(cv::Mat &activity_map);
	void drawDetectedTarget(cv::Mat &image) const;	// draw detection
	void drawTrajectory(cv::Mat &image) const {mTrajectory.draw(image);}

private:
	const int								mID;	//

	bool									mValid;
	bool									mActive;	// active target or not?

	// target trajectory
	VisionTrajectory						mTrajectory;
	// pointer to the tracker
	boost::shared_ptr<BaseVisionTracker>	mpTracker;

	// Pointers to Vision Modules
	boost::shared_ptr<VisionVideoModule>	 	mpVideoModule;		// cacheing pointer for saving access time
	boost::shared_ptr<VisionDetector> 			mpTargetDetector;	// for foreground activity verification
	boost::shared_ptr<SETTINGS> 				mpSettings;

};

} // of namespace vml

#endif

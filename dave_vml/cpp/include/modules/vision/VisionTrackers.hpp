/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * VisionTrackers.hpp
 *
 *  Created on: Nov 30, 2015
 *      Author: yyoon
 */

#ifndef VISIONTRACKERS_HPP_
#define VISIONTRACKERS_HPP_

#include <opencv2/core/core.hpp>
#include <boost/shared_ptr.hpp>
#include <core/Video.hpp>
#include <core/Settings.hpp>
#include <modules/vision/BaseVisionTracker.hpp>

#if 0
// dlib tracker no longer supported
#include "dlib/image_processing.h"
#endif

namespace vml {

#define HSHIST_TRACKBOX_COLOR			CV_RGB(0,0,255)
#define RGCHROMHIST_TRACKBOX_COLOR		CV_RGB(255,0,0)
#define RBCHROM_TRACKBOX_COLOR			CV_RGB(0,0,255)
#define RGBHIST_TRACKBOX_COLOR			CV_RGB(0,255,255)
#define DLIBCORRELATION_TRACKBOX_COLOR	CV_RGB(255,255,0)

// forward decl
class VisionDetectorTarget;

class HSHistogramTracker: public BaseVisionTracker
{
public:
	HSHistogramTracker(const int id, boost::shared_ptr<SETTINGS> settings);
	virtual ~HSHistogramTracker() {}

	virtual void initializeTrackerFeature(boost::shared_ptr<VisionDetectorTarget> dt);

	virtual bool updateTrackerFeature(cv::Mat *disp_map, cv::Point &new_center);

	virtual void drawTracker(cv::Mat &disp_image);

private:
	// for debugging
//	int				mCount;

	// for HIST_HSV
	const int		mcHBins;
	const int		mcSBins;
	const float		mcHRangeMin;
	const float		mcHRangeMax;
	const float		mcSRangeMin;
	const float		mcSRangeMax;

	cv::Mat					mHistogram;	// target color histogram.
};

class RGBHistogramTracker: public BaseVisionTracker
{
public:
	RGBHistogramTracker(const int id, boost::shared_ptr<SETTINGS> settings);
	virtual ~RGBHistogramTracker() {}

	virtual void initializeTrackerFeature(boost::shared_ptr<VisionDetectorTarget> dt);

	virtual bool updateTrackerFeature(cv::Mat *disp_map, cv::Point &new_center);

	virtual void drawTracker(cv::Mat &disp_image);

private:
	const int		mcRGBBins;
	const float		mcColorRangeMin;
	const float		mcColorRangeMax;	// avoid saturated colors

	cv::Mat					mRHistogram;
	cv::Mat					mGHistogram;	// for three channel histograms
	cv::Mat					mBHistogram;

};

// HSV-RGB hybrid histogram-backprojection-based tracker
// a.k.a, "The Batman"
class HybridHistogramTracker: public BaseVisionTracker
{
public:
	HybridHistogramTracker(const int id, boost::shared_ptr<SETTINGS> settings);
	virtual ~HybridHistogramTracker() {}

	virtual void initializeTrackerFeature(boost::shared_ptr<VisionDetectorTarget> dt);

	virtual bool updateTrackerFeature(cv::Mat *disp_map, cv::Point &new_center);

	virtual void drawTracker(cv::Mat &disp_image);

private:
	// for debugging
	int				mCount;

	const float			mcSaturationLowThreshold;
	bool			mIsHSHistTracker;

	// TODO: convert the following const params as static const later to save memory
	// hs histogram params
	const int		mcHBins;
	const int		mcSBins;
	const float		mcHRangeMin;
	const float		mcHRangeMax;
	const float		mcSRangeMin;
	const float		mcSRangeMax;

	// RGB histogram params
	const int		mcRGBBins;
	const float		mcRGBRangeMin;
	const float		mcRGBRangeMax;

	cv::Mat			mHistogram;	// target color histogram. Also R hist for RGB
	cv::Mat			mHistogramG;
	cv::Mat			mHistogramB;

};

class KDETracker: public BaseVisionTracker
{
public:
	KDETracker(const int id, boost::shared_ptr<SETTINGS> settings);
	virtual ~KDETracker();

	virtual void initialize(boost::shared_ptr<VisionDetectorTarget> dt);

	// update returns true if the updated target has high confidence
	// false otherwise.
	virtual TrackingStatusEnum update(cv::Mat *disp_map);
	virtual TrackingStatusEnum reactivate(std::vector<boost::shared_ptr<VisionDetectorTarget> > &dt_list);

	virtual void drawTracker(cv::Mat &disp_image);

	// definition of struct for Gaussian KDE approach
	struct CIELabPoint
	{
		CIELabPoint():a(0),b(0) {}	// default constructor
		CIELabPoint(double _a, double _b):a(_a),b(_b) {}

		// distance and reaverage for clustering
		double dist(CIELabPoint other);
//		void reavg(RBChromPoint other);

		double a;
		double b;
	};

	struct CIELabCluster
	{
		CIELabCluster():center(CIELabPoint(0,0)),mass(0.f) {}
		CIELabCluster(CIELabPoint ap):center(ap),mass(1.f) {}

		double dist(CIELabPoint ap) {return center.dist(ap);}
		void reavg(CIELabPoint other);

		CIELabPoint 	center;
		double			mass;	// cluster mass

	};

private:

	int mCount;	// for debugging

	double							mKernelBandwidth;
//	double							mKernelClusterRadius;
//	std::vector<CIELabCluster> 	mKernelClusters;
	std::vector<CIELabPoint>		mKernelSource;	// TODO: make this circular buffer for later
	double*							mKernelWeight;

	// save kernel weights

};


#if 0
class LogColorGradientHistogramTracker: public BaseVisionTracker
{
public:
	LogColorGradientHistogramTracker(boost::shared_ptr<SETTINGS> settings);
	virtual ~LogColorGradientHistogramTracker() {}

	virtual void initialize(boost::shared_ptr<VisionDetectorTarget> dt);

	// update returns true if the updated target has high confidence
	// false otherwise.
	virtual TrackingStatusEnum update(cv::Mat *disp_map);

	virtual void drawTracker(cv::Mat &disp_image);

private:
	const int		mcRGBins;
	const float		mcColorRangeMin;
	const float		mcColorRangeMax;	// avoid saturated colors

	cv::Mat					mHistogram;

};
#endif

#if 0  // Dlib tracker no longer supported
class DlibCorrelationTracker: public BaseVisionTracker
{
public:
	DlibCorrelationTracker(boost::shared_ptr<SETTINGS> settings);
	virtual ~DlibCorrelationTracker() {}

	void initialize(const cv::Mat &image,boost::shared_ptr<VisionDetectorTarget> dt);

	virtual TrackingStatus update(cv::Mat *disp_map);

	virtual void drawTracker(cv::Mat &disp_image);

private:
	// dlib::correlation tracker
	dlib::correlation_tracker	mDlibTracker;

	const float	mcBoundingBoxRatio;
};
#endif

} // of namespace vml

#endif

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * VisionVideoModule.hpp
 *
 *  Created on: Mar 22, 2016
 *      Author: yyoon
 */

#ifndef VML_VISIONVIDEOMODULE_HPP_
#define VML_VISIONVIDEOMODULE_HPP_

#include <core/Video.hpp>

namespace vml {

struct VisionFrame
{
	VisionFrame(int rows, int cols);

	// methods
#ifdef _WIN32
	bool grab(ait::Image32& img, double curTime);
#else
	bool grab(cv::VideoCapture &vc, double epoch_time);
#endif

	void copyTo(VisionFrame &ot);

	// members
	cv::Mat		image;
	cv::Mat		blurredImage;
	cv::Mat		undistorted_image;
	cv::Mat		foreground_image;
	double	time;
};

class VisionVideoModule: public VideoBuffer<VisionFrame>
{
public:

	VisionVideoModule();
	virtual ~VisionVideoModule();
	
#ifdef _WIN32
	virtual void initialize(ait::Image32& img,int buffer_size);
	virtual bool grabFrame2(ait::Image32& img, double curFTime);
	// for background training
	bool isTraining();
#else
	virtual void initialize(boost::shared_ptr<SETTINGS> settings,std::string video_file_name);

	// preallocate temp frames
	void allocateTempFrames();

	// Grab methods
	virtual bool grabFrame(void);	// single grab
	//** MultiThreading methods - 2 threads
	// Methods for new frame buffer allocation per image frame
	bool grabAndBackgroundSubtractToGrabFrame(void);
	void setGrabFrameToCurrent(void);

	// methods for in-place image frame process
	bool grabAndBackgroundSubtractInGrabFrame(void);
	void copyGrabFrameToCurrent(void);

	//** Multithreading methods - 3 threads
	// Methods for new frame buffer allocation per image frame
	bool grabToGrabFrame(void);
	void setGrabFrameToBackSubFrame(void);
	void setBackSubFrameToCurrent(void);

	// methods for in-place image frame process
	bool grabInGrabFrame(void);
	void copyGrabFrameToBackSubFrame(void);
	void copyBackSubFrameToCurrent(void);

	void backgroundSubtractionInBackSubFrame(void);

	boost::shared_ptr<VisionFrame>	getBackSubFrame() {return mpBackSubFrame;};

#endif

	// methods for camera model
	void copyCurrentUndistortedImageTo(cv::Mat &undist_img);
	void getCurrentTrackRegionImage(cv::Mat &img);

	void undistortImage(cv::Mat &input_img,cv::Mat &undistorted_image);

	boost::shared_ptr<CameraModelOpenCV> getCameraModel(void) {return mpCameraModel;}

	bool insideTrackRegion(cv::Point pos);
	bool insideStartKillRegion(cv::Point pos);
	bool insideNoStartRegion(cv::Point pos);

	void drawTrackRegions(cv::Mat &img);
	void drawNoStartRegions(cv::Mat &img);

	// enforcing points to be inside window
	void enforceWindowRect(std::vector<cv::Point> &pts);
	void enforceWindowRect(cv::Point &pt);
	cv::Point enforceWindowRect(const cv::Point pt);

	// enforcing rectangle to be inside window
	void enforceWindowRect(cv::Rect &rect) const {rect &= mWindowRect;}

	float getPersonRadiusPixel() const {return mCenterPersonRadiusInPixel;}

	float getFrameRate();

	bool isValid() const {return ((mInitialized)&&(!mTrackingRegion.empty())&&(mpCameraModel->isValid()));}

private:	

	std::string	mVideoURL;
	cv::Rect	mWindowRect;
	float	mBlurSigma;

	// contour for tracking region
	std::vector<cv::Point>	mTrackingRegion;
	// contours for no start region
	std::vector<std::vector<cv::Point> >	mNoStartRegion;
	std::vector<cv::Point> mStartKillRegion;
	float	mStartRegionOffset;

	float	mCenterPersonRadiusInPixel;	// measured person radius in image pixel unit at the center of image. Used for distance-related thresholds

#if CV_MAJOR_VERSION == 2
	boost::shared_ptr<cv::BackgroundSubtractorMOG2>	mpBgSubtractor;
#elif CV_MAJOR_VERSION == 3
	cv::Ptr<cv::BackgroundSubtractorMOG2> mpBgSubtractor;
#endif

#ifdef _WIN32
	int mFramesTrained;
	int mFramesToTrain;
#endif


	bool	mShadowDetect;		// shadow detection enabled for background subtraction

	// for multithreading grabbing
	boost::shared_ptr<VisionFrame>		mpGrabFrame;
	boost::shared_ptr<VisionFrame>		mpBackSubFrame;
	boost::shared_ptr<VisionFrame>		mpProcessFrame;

	boost::shared_ptr<CameraModelOpenCV>	mpCameraModel;
	boost::shared_ptr<Settings>				mpSettings;

};

} // of namespace vml

#endif

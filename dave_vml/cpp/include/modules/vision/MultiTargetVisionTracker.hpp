/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * MultiTargetVisionTracker.hpp
 *
 *  Created on: Oct 6, 2015
 *      Author: yyoon
 */

#ifndef MULTITARGETVISIONTRACKER_HPP_
#define MULTITARGETVISIONTRACKER_HPP_

#include <opencv2/core/core.hpp>
#include <core/Settings.hpp>
#include <modules/vision/BaseVisionTracker.hpp>
#include <modules/vision/VisionTrajectory.hpp>

namespace vml {

class VisionModuleManager;

class MultiTargetVisionTracker {
public:

#ifdef _WIN32
	MultiTargetVisionTracker(boost::shared_ptr<VisionVideoModule> vbp,
			boost::shared_ptr<SETTINGS> settings);
#else
	MultiTargetVisionTracker(std::string video_url, boost::shared_ptr<SETTINGS> settings);
#endif

	~MultiTargetVisionTracker();

	void initializeModules();

	// settings methods
//	void setSettings(boost::shared_ptr<SETTINGS> settings);
	boost::shared_ptr<SETTINGS> getSettings() {return mpSettings;}
//	void setVideoModule(boost::shared_ptr<VisionVideoModule> video_module);
	void setIOTCommunicator(boost::shared_ptr<IOT_Communicator> iotc);

#ifdef _WIN32
	bool process(ait::Image32& img, double frameTime);
	bool trackerReady();
#else
	bool process(void);	// grab a new image frame and update the current target list
#endif

	// multithreading methods
#ifndef _WIN32
	// 2 threads methods
	void 			_2ThreadPipeliningStart();
	static void* 	_2ThreadPipeliningFrameGrabThread(void *apdata);
	void 			_2ThreadPipeliningFrameGrabLoop(void);
	static void* 	_2ThreadPipeliningProcessThread(void *apdata);
	void 			_2ThreadPipeliningProcessLoop(void);

	// 3 threads methods
	void 			_3ThreadPipeliningStart();
	static void* 	_3ThreadPipeliningFrameGrabThread(void *apdata);
	void 			_3ThreadPipeliningFrameGrabLoop(void);
	static void* 	_3ThreadPipeliningBackgroundSubtractThread(void *apdata);
	void 			_3ThreadPipeliningBackgroundSubtractLoop(void);
	static void* 	_3ThreadPipeliningProcessThread(void *apdata);
	void 			_3ThreadPipeliningProcessLoop(void);

	static void* 	emptyProcessThread(void *apdata);
	void 			emptyProcessLoop(void);

	// method to stop threads
	void stopThreads();
	bool isRunning() {return !mStopThreads;}

#endif

	const cv::Mat&  getGrabbedImage(void);
	const cv::Mat&  getForeground(void);
	const cv::Mat&  getBlobImage(void);
	const cv::Mat& getBackProjection(void);

	void copyBackgroundImageTo(cv::Mat &bgimg) const;
	void copyUndistortImageTo(cv::Mat &cur_undist);

	// indicators
	unsigned int	getCurrentFrameNumber();
	unsigned int	getNumActiveTargets();
	unsigned int	getNumTotalTargets();
	unsigned int	getNumInactiveTargets();
	unsigned int	getNumCompletedTargets();
	float 			getFrameRate() {return mpVideoModule->getFrameRate();}


#ifdef _WIN32
	void getCompletedTrajectories(std::vector<ait::vision::Trajectory> &trajlist);
	void finish(std::vector<ait::vision::Trajectory> &trajlist);
#else
	void getCompletedTrajectories(std::vector<VisionTrajectory> &trajlist);
	void MultithreadPipeliningGetCompletedTrajectories(std::vector<VisionTrajectory> &trajlist);
#endif
	// display methods
	void drawActiveTargets(cv::Mat &display_image);
	void drawActiveTrajectories(cv::Mat &display_image);
//	void drawUndistortedActiveTargets(cv::Mat &display_image);
	void drawInactiveTargets(cv::Mat &display_image);
	void drawDetectedTargets(cv::Mat &display_image);
	void drawActiveTargetMap(cv::Mat &display_image);
	void drawTrackingRegions(cv::Mat &display_image);
	void drawNoStartRegions(cv::Mat &display_image);

	void displayTracker(cv::Mat &display_image);

	void setTrackingMethod(const TrackingMethods hm);

#ifdef _WIN32
	bool mHandlersInitialized;
#endif

private:

	// private helper
	void sendHeartBeat();

	// members
	std::string	mVideoURL;

	unsigned int nFrames;

#ifndef _WIN32
	// multithreading members
	pthread_t	mGrabberThread;
	pthread_t	mBackSubThread;
	pthread_t	mProcessorThread;
	pthread_t	mEmptyThread;
	pthread_attr_t mThreadAttr;
	bool		mStopThreads;
	bool		mbGrabReady;
	bool		mbBgSubReady;
	bool		mbBufferDone;

	// completed trajectory buffer
	std::vector<VisionTrajectory> mCompletedTrajectory;
#endif
	bool		mbMultithreadsOn;
	bool		mbDisplayReady;

	double		mHeartBeatInterval;
	double		mMaxHeartBeatIntervalProportion;
	double		mPreviousHeartbeatTime;

	// cacheing vision module pointers
	boost::shared_ptr<SETTINGS>					mpSettings;
	boost::shared_ptr<VisionVideoModule>		mpVideoModule;
	boost::shared_ptr<VisionDetector>			mpDetector;
	boost::shared_ptr<VisionTargetManager>		mpVisionTargetManager;
	boost::shared_ptr<IOT_Communicator>			mpIOTCommunicator;


};

} // of namespace vml

#endif

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * VisionDetector.hpp
 *
 *  Created on: Oct 6, 2015
 *  Updated on: Jan 5, 2016
 *      Author: yyoon
 */

#ifndef VISIONDETECTOR_HPP_
#define VISIONDETECTOR_HPP_

#include <opencv2/core/version.hpp>
#include <opencv2/core.hpp>
#include <opencv2/video/background_segm.hpp>
#include <boost/shared_ptr.hpp>
#include <core/Image.hpp>
#include <core/Settings.hpp>

//#define VISIONDETECTOR_DEBUG_SHOW

namespace vml {

class VisionVideoModule;
class VisionModuleManager;
class VisionTargetManager;
class VisionDetectorTarget;

// Target detector using background subtraction
class VisionDetector
{
public:

	VisionDetector();
	~VisionDetector();

	// methods for registering modules
	void initialize(boost::shared_ptr<SETTINGS> settings);

	// methods
	void detectForeground(void);	// use background subtraction to detect foreground map
	void detectTargets(void);		// cluster foreground blobs to detect and construct targets
//	void addNewTargetsToTargetManager(void);

#if CV_MAJOR_VERSION == 2
	boost::shared_ptr<cv::BackgroundSubtractorMOG2> getBackgroundSubtractor() {return mpBgSubtractor;}
#elif CV_MAJOR_VERSION == 3
	cv::Ptr<cv::BackgroundSubtractorMOG2> getBackgroundSubtractor() {return mpBgSubtractor;};
#endif

	const cv::Mat &getForegroundImage() const {return mForeground;}
	const cv::Mat &getBlobImage() const {return mBlobImage;}

	void copyBackgroundImageTo(cv::Mat &bgimg) const;
	void copyForegroundImageTo(cv::Mat &bgimg) const;
	void copyBlobImageTo(cv::Mat &bgimg) const;

	// masking off foreground image
	void maskOffForegroundImage(cv::Mat &mask);  // when mask has the same size as foreground image
	void maskOffForegroundImage(cv::Mat &mask, cv::Rect crop_rect); // when mask is a cropped image
	void maskOffForegroundImage(cv::Rect mask_rect);	// when we want to mask off a rectangular region

	const std::vector<boost::shared_ptr<VisionDetectorTarget> >*
		getDetectedTargetList(void) const {return &mDetectedTargetList;}

	// this will return all the current detected targets and clear the stored list.
	void retrieveDetectedTargets(std::vector<boost::shared_ptr<VisionDetectorTarget> > &dt_list);

//	void clearInvalidTargets(void);

	// display
	void drawDetectedTargets(cv::Mat &image) const;
private:
	// helper methods
	void deleteInvalidTargets();

private:

	typedef struct {
		int		contour_label;
		float	distance;
	} ContourDistanceType;

	// functor for contour distance sort
	class ContourDistCompare {
	public:
		int operator() (const ContourDistanceType &ct1,
				const ContourDistanceType &ct2)
		{
			return (ct1.distance < ct2.distance);
		}
	};
	// members

#if CV_MAJOR_VERSION == 2
	boost::shared_ptr<cv::BackgroundSubtractorMOG2>	mpBgSubtractor;
#elif CV_MAJOR_VERSION == 3
	cv::Ptr<cv::BackgroundSubtractorMOG2> mpBgSubtractor;
#endif

	int	mRows;
	int mCols;

#ifdef BGSUBSAMPLE
	// subsampled dimension
	int			mRowsSub;
	int			mColsSub;
#endif

	cv::Mat							mBackground;	// background image. updated each frame.
	cv::Mat							mForeground;	// stores processed foreground
	cv::Mat							mBlobImage;


	int		mMaxTargets;

	// morphological filter parameters
	int		mOpenSERadius;
	int		mCloseSERadius;


	// params
	int	  mContourAreaMinThreshold;
	float mMergeMahalanobisDistanceThreshold;
	// list of detected target
	std::vector<boost::shared_ptr<VisionDetectorTarget> >	mDetectedTargetList;

// For displaying debug window for vision detector
#ifdef VISIONDETECTOR_DEBUG_SHOW
public:
	// debug display output
	cv::Mat	mDisplayImage;
	static const char *displayname;

	void debugShow(cv::Mat &dispimage);
#endif

	boost::shared_ptr<VisionVideoModule> mpVideoModule;
	boost::shared_ptr<SETTINGS> mpSettings;

};

} // of namespace vml
#endif

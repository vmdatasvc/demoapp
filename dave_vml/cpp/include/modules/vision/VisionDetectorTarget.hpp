/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * VisionDetectorTarget.hpp
 *
 *  Updated on: Jan 5, 2016
 *      Author: yyoon
 */

#ifndef VISIONDETECTORTARGET_HPP_
#define VISIONDETECTORTARGET_HPP_

#include <opencv2/core/version.hpp>
#include <boost/shared_ptr.hpp>
#include <core/Image.hpp>
#include <core/Settings.hpp>
#include <modules/vision/PersonShapeModel.hpp>

namespace vml {

//template<class T> class VideoBuffer;
class VisionVideoModule;
class VisionModuleManager;
class VisionDetector;

// Target detector using background subtraction
class VisionDetectorTarget
{
	friend class VisionDetectorTargetAreaCompare;
	friend class VisionDetectorTargetLikelihoodCompare;
public:

	VisionDetectorTarget(std::vector<cv::Point> &contour,
			boost::shared_ptr<SETTINGS> settings);
	~VisionDetectorTarget();

	float EuclideanDistance(VisionDetectorTarget &ot) const;
	float EuclideanDistance(cv::Point pos) const;
	float MahalanobisDistance(VisionDetectorTarget &ot) const;
	float MahalanobisDistance(cv::Point p) const;
	// Check the blob detection merge criteria and merge if the criteria is met, otherwise do nothing
	void checkBlobMergeCriteriaAndMerge(boost::shared_ptr<VisionDetectorTarget> other);
	void invalidateIfSmall();
	void fillConvexHullMap(cv::Mat_<uchar> &map);

	// accessors
	int getArea() {return mCvxHullArea;}
	cv::Point getCenter() const {return mCenter;}
	cv::Point getPosition() const {return mPosition;}
	void getTrackerInitializerInfo(cv::Rect &init_rect, cv::Mat &contour_map);
	void getBlobContourMap(const cv::Rect crop_rect, cv::Mat &contour_map);


	void drawTarget(cv::Mat &image) const;	// draw detection

	bool checkTargetActivityOverlap(cv::Mat &target_activity_map);

	void setInvalid() {mValid = false;}
	bool isValid() {return mValid;}
	bool needMerge() {return mNeedMerge;}

	bool isInTrackRegion() {return mInTrackRegion;}
	bool isInStartRegion() {return mInStartRegion;}
	bool isInTrackStartRegion() {return (mInTrackRegion||mInStartRegion);}

	float blobToPersonAreaRatio() {return (float)mContoursArea/mPersonShape.getArea();}

	void evaluateLikelihood(cv::Rect croprect, const cv::Mat linklihoodmap);
	float getLikelihood() {return mLikelihood;}
	void mergeForTrackUpdate(boost::shared_ptr<VisionDetectorTarget> other);

	// for display purpose
	void getCvxHull(std::vector<cv::Point> &outvec);

private:

	void evalRegionStatus();
	void updateEstimatedPersonShape();

	bool mValid;	// flag used for clearing
	bool mNeedMerge;

	bool mInTrackRegion;
	bool mInStartRegion;
	int	 mStartRegionNumber;

	cv::Point	mCenter;	// center of detected target mass
	cv::Point	mPosition;	// foot position of the detected target
	cv::Rect	mDetectedBoundingRect;

	std::vector<cv::Point>  				mCvxHull;	// initial convex hull
	int										mCvxHullArea;
	std::vector<std::vector<cv::Point> >	mContours;	// initial contours of the aggregated blobs
	int										mContoursArea;

//	std::vector<cv::Point>					mEstimatedPersonShape;	// estimated person shape convex hull based on the target center
//	int										mEstimatedPersonArea;	// area of the estimated person shape
//	float									mEstimatedPersonLength;	// max of fitted RotatedRect size
//	float									mEstimatedPersonSinv[3];	// inverse covariance for Mahalanobis distance. Need three cause it's symmetric.
	PersonShapeModel						mPersonShape;

	// area threshold
	float		mAreaThresholdHighRatio;

	// for tracker update
	float									mLikelihood;	// likelihood of whatever passed to evaluateLikelihood

	// Pointers to Vision Modules
	boost::shared_ptr<VisionVideoModule>	 	mpVideoModule;		// cacheing pointer for saving access time
	boost::shared_ptr<VisionDetector> 			mpTargetDetector;	// for foreground activity verification
	boost::shared_ptr<SETTINGS> 				mpSettings;
};

// functor for detected target sorting
class VisionDetectorTargetAreaCompare {
public:
	int operator() (const boost::shared_ptr<VisionDetectorTarget> ct1,
			const boost::shared_ptr<VisionDetectorTarget> ct2)
	{
		return (ct1->mCvxHullArea > ct2->mCvxHullArea);
	}
};

class VisionDetectorTargetLikelihoodCompare
{
public:
	int operator() (const boost::shared_ptr<VisionDetectorTarget> ct1,
			const boost::shared_ptr<VisionDetectorTarget> ct2)
	{
		return (ct1->mLikelihood > ct2->mLikelihood);
	}
};

} // of namespace vml
#endif

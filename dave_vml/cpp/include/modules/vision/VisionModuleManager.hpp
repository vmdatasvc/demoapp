/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * VisionModuleManager.hpp
 *
 *  Created on: Feb 8, 2016
 *      Author: yyoon
 *
 */

#ifndef VISIONMODULEMANAGER_HPP_
#define VISIONMODULEMANAGER_HPP_

#include <boost/shared_ptr.hpp>
#include <opencv2/core/core.hpp>
#include <core/Image.hpp>

#include <core/Settings.hpp>

// IOT_Communicator is not in vml namespace
class IOT_Communicator;

namespace vml {

// forward decl.
class VisionVideoModule;
class VisionDetector;
class VisionTargetManager;

// Factory class for vision modules
// NOTE: It's a design decision if all the pointers to the modules should be static.
//  If static, the modules are not thread-safe, so need to enfoce tread-safety
//  currently, module pointers are not static, and each thread should create its own
//  factory instance.
class VisionModuleManager
{
public:
	VisionModuleManager() {};
	~VisionModuleManager() {};

	// singleton instance access
	static VisionModuleManager* instance();

	void registerSettings(boost::shared_ptr<SETTINGS> settings);
	boost::shared_ptr<SETTINGS> getSettings(void);

	void registerVideoModule(boost::shared_ptr<VisionVideoModule> vb);
	boost::shared_ptr<VisionVideoModule> getVideoModule(void);

	void registerDetector(boost::shared_ptr<VisionDetector> det);
	boost::shared_ptr<VisionDetector> getDetector(void);

	void registerVisionTargetManager(boost::shared_ptr<VisionTargetManager> tm);
	boost::shared_ptr<VisionTargetManager> getVisionTargetManager(void);

	void registerIOTCommunicator(boost::shared_ptr<IOT_Communicator> iotc);
	boost::shared_ptr<IOT_Communicator> getIOTCommunicator(void);

private:

	boost::shared_ptr<SETTINGS>					mpSettings;
	boost::shared_ptr<VisionVideoModule>		mpVideoBuffer;
	boost::shared_ptr<VisionDetector>			mpDetector;
	boost::shared_ptr<VisionTargetManager>		mpVisionTargetManager;
	boost::shared_ptr<IOT_Communicator>			mpIOTCommunicator;
};

} // of namespace vml

#endif

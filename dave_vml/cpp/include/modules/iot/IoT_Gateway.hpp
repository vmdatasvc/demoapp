/*
 * IoT_Gateway.hpp
 *
 *  Created on: Mar 3, 2016
 *      Author: paulshin
 */

#ifndef INCLUDE_MODULES_IOT_IOT_GATEWAY_HPP_
#define INCLUDE_MODULES_IOT_IOT_GATEWAY_HPP_

#include "stdint.h"
#include "modules/iot/IoT_Common.hpp"

#include "aws_iot_error.h"
#include "aws_iot_shadow_json_data.h"

#include <vector>

// -- For MQTT
#include "modules/iot/MQTT-client-cpp.hpp"
#include "modules/iot/Dev_Common.hpp"

#include "json.hpp"

// -- The IoT_Gateway class inherits MQTT communication capability from a Mosquitto MQTT library, MosqMQTT.
class IoT_Gateway : public MosqMQTT {

private:

	static std::vector<devState_t > mDevState;

	std::string mMqttConfigPath;
	std::string mMqttPubTopic_State;
	std::string mMqttPubTopic_Data;

	DeviceInfo devinfo;

	std::string mNIC;
	std::string mSoftwareVersion;

	double	mInitialTime;
	pthread_attr_t mThreadAttr;
	pthread_t mMessageSending_thread;
	double mDefaultReportInterval;

	unsigned int mStateMessageId;
	std::ostringstream mStateMsgToSend;
	std::vector<std::pair<std::string,std::string> > mDataMsgToSend;
	float mStateUpdateIntervalScale;

	void (*mDeltaStateCallbackFunc)(std::string msg);
	void (*mControlChannelCallbackFunc)(std::string topic, std::string msg);

	static void *MessageSendingThread(void *apData);
	void MessageSending_Loop();


			// -- Parameters and functions for MosqMQTT class
			bool mIsMqttConnected;

			void on_message(const struct mosquitto_message *message);
			void on_connect(int rc);
			void on_disconnect(int rc);
			void on_subscribe(int mid, int qos_count, const int *granted_qos);

public:

	IoT_Gateway(const char * _id = "1", bool clean_session = false);
	~IoT_Gateway();

	bool Run(std::string appID, std::string topic_state, std::string topic_data,
			std::string iot_gateway_configfile);
	void SendStateInfo();
	VM_IoT_Error_t SendState(const std::string& appName, const nlohmann::json& data);
	VM_IoT_Error_t SendData(const std::string &msg);
	VM_IoT_Error_t SendData(const std::string &topic_suffix, const std::string &msg);

	void RegisterDeltaCallbackFunc(void (*IoT_DeltaState_CallBack_func)(std::string msg)) {
		mDeltaStateCallbackFunc = IoT_DeltaState_CallBack_func;
        }

	void RegisterControlCallbackFunc(void (*IoT_ControlChannel_CallBack_func)(std::string topic, std::string msg)) {
		mControlChannelCallbackFunc = IoT_ControlChannel_CallBack_func;

	}

	const std::string& GetMyIpAddr() const {
		return devinfo.GetMyIpAddr_();
	}
	const std::string& GetMySN() const {
		return devinfo.GetMySN_();
	}
	const std::string& GetMyStoreName() const {
		return devinfo.GetMyStoreName_();
	}
	const std::string& GetMyDevName() const {
		return devinfo.GetMyDevName_();
	}
	std::string GetMyStoreName_dash() const {
		return devinfo.GetMyStoreName_dash_();
	}

	size_t AddStateParam(unsigned int type, std::string key, void *value,
			void (*cb)(std::string key, std::string value), double reportInterval);

	double GetStateParamReportInterval(std::string key);
	double GetStateParamReportInterval(void *valuePtr);
	bool UpdateStateParamReportInterval(std::string key, double newReportInterval);
	bool UpdateStateParamReportInterval(void *valuePtr, double newReportInterval);

	void SetStateParamReportIntervalScale(double scale) { mStateUpdateIntervalScale = scale; }
	void ResetStateParamReportIntervalScale() { mStateUpdateIntervalScale = 1.0f; }

	static devState_t *FindStateParam(std::string paramKey);

};



#endif /* INCLUDE_MODULES_IOT_IOT_GATEWAY_HPP_ */

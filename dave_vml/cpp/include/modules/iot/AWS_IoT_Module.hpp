/*
 * AWS_IoT_Module.hpp
 *
 *  Created on: Feb 2, 2016
 *      Author: paulshin
 */

#ifndef SRC_MODULES_AWS_IOT_MODULE_HPP_
#define SRC_MODULES_AWS_IOT_MODULE_HPP_

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>

#include <signal.h>
#include <memory.h>
#include <sys/time.h>
#include <limits.h>

#include <pthread.h>

#include <string>
#include <memory>
#include <iostream>
#include <sstream>

#include <vector>
#include <deque>

extern "C" {
	#include "aws_iot_log.h"
	#include "aws_iot_version.h"
	#include "aws_iot_shadow_interface.h"
	#include "aws_iot_shadow_json_data.h"
	#include "aws_iot_mqtt_interface.h"

	#include "aws_iot_config.h"
}

#include "modules/iot/IoT_Common.hpp"

// -- Refer to https://github.com/nlohmann/json
#include "json.hpp"

using json = nlohmann::json;

#define MAX_LENGTH_OF_UPDATE_JSON_BUFFER 7500//450		// -- Paul: Max ThingShadow size is 8KB at AWS IoT, so this should be smaller than that. Paul: I've had problem if I set this larger than 510. Need to check!
#define MAX_STAT_BUFFER_SIZE			9999

using namespace std;

extern pthread_mutex_t _gMutexx_;

struct PubMsg_t {
	QoSLevel qos;
	string topic;
	string msg;
	unsigned int appType;
};

// -- Thing Shadow parameter type and its value
template<typename T>
struct ShadowParam_t  {
	jsonStruct_t json;
	T value;
	string key;
	unsigned int appType;
	bool needToSend;
	char c_arr[512];
};

struct SubParam_t {
	MQTTSubscribeParams param;
	string topic;
	char c_arr[512];
};

class MQTT_Publisher {
public:
	MQTT_Publisher() {
		mPubMsg_Params = MQTTMessageParamsDefault;
		mPublisherParams = MQTTPublishParamsDefault;
	}

	~MQTT_Publisher() {}

	void AddPayloadMsg(string msg) {
		mPubMsg_Payload = msg;
		mPubMsg_Params.PayloadLen = mPubMsg_Payload.size();
		mPubMsg_Params.pPayload = (char *)mPubMsg_Payload.c_str();
	};

	IoT_Error_t Publish(QoSLevel qos, string &topic, string &msg) {
		IoT_Error_t rc;
		mPubMsg_Params.qos = qos;
		AddPayloadMsg(msg);
		mPublisherParams.pTopic = (char *)topic.c_str();
		mPublisherParams.MessageParams = mPubMsg_Params;

		rc = aws_iot_mqtt_publish(&mPublisherParams);

		mPubMsg_Payload.clear();

		return rc;
	}

private:
	MQTTPublishParams mPublisherParams;
	MQTTMessageParams mPubMsg_Params;

	string mPubMsg_Payload;
};

class AWS_IoT_Module {

public:

	AWS_IoT_Module();
	virtual ~AWS_IoT_Module();

	void ThingShadow_Init(char *thingName, char *myClientID, char *hostUrl, unsigned int hostPort, char *certDirect
			, char *rootCaFileName, char *certFileName, char *privateKeyFileName, unsigned int awsMqttCommandTimeout_ms, unsigned int awsTlsHandshakeTimeout_ms);
	IoT_Error_t Shadow_ConnectInit();
	IoT_Error_t	Shadow_Disconnect() { return aws_iot_shadow_disconnect(&mMqttClient); }

	int Shadow_addParam(jsonStruct_t *param, unsigned int appType);

	void Shadow_reset(unsigned int appType) {
		pthread_mutex_lock(&_gMutexx_);
		mAppParamsToBeReportedAtThisRound[appType].clear();
		pthread_mutex_unlock(&_gMutexx_);
	};

	void SetShadowUpdateCallbackPtr(fpActionCallback_t cbPtr) {
		mShadowUpdateCallbackPtr = cbPtr;
	}

	void MQTT_publish(QoSLevel qos, string topic, string msg, unsigned int appType) {

		PubMsg_t pubMsg;
		pubMsg.qos = qos;
		pubMsg.topic = topic;
		pubMsg.msg = msg;
		pubMsg.appType = appType;

		pthread_mutex_lock(&_gMutexx_);
		mPubMsg_Queue_in.push_back(pubMsg);
		pthread_mutex_unlock(&_gMutexx_);
	}

	void MQTT_subscribe(QoSLevel qos, string topic, iot_message_handler cb) {
		SubParam_t subParams;

		subParams.topic = topic;
		subParams.param.mHandler = cb;
		//subParams.param.pTopic = (char *)(subParams.topic.c_str());

		// -- Added this because casting string to char* didn't work
		subParams.param.pTopic = new char[subParams.topic.length() + 1];
		strcpy(subParams.param.pTopic, subParams.topic.c_str());

//		strncpy(subParams.c_arr, topic.c_str(), sizeof(subParams.c_arr));
//		subParams.c_arr[sizeof(subParams.c_arr) - 1] = '\0';
//		subParams.param.pTopic = subParams.c_arr;

		//subParams.param.pTopic = subParams.c_topic;
		subParams.param.qos = qos;

		mMQTT_Subscriber.push_back(subParams);
	}

	string GetDeltaStateMsg() {
		string ret;
		ret = mDeltaMsgJson.dump();
		return ret;
	}

	bool IsDeltaStateMsgArrived() {
		bool ret;
		ret = mIsDeltaMsgRecived;
		return ret;
	}

	void SetDeltaStateMsgArrived(bool flag) {
		mIsDeltaMsgRecived = flag;
	}

	void SetParameters( bool needToEraseThingShadow,
						float periodicShadowReportInterval,
						float minPeriodicDataPublishInterval,
						float periodicFullReportInterval,
						float periodicRefreshDesiredInterval
						) {

		pthread_mutex_lock(&_gMutexx_);
			mNeedToEraseThingShadow = needToEraseThingShadow;
			mPeriodicShadowReportInterval = periodicShadowReportInterval;
			mMinPeriodicDataPublishInterval = minPeriodicDataPublishInterval;
			mPeriodicFullReportInterval = periodicFullReportInterval;
			mPeriodicRefreshDesiredInterval = periodicRefreshDesiredInterval;
		pthread_mutex_unlock(&_gMutexx_);
	}

	void QuickOneTimeStateUpdate() {
		mPrevTime_shadowUpdate -= mPeriodicShadowReportInterval;
	}

	float GetParam_ShadowReportInterval() { return (float)max((double)MIN_STATE_REPORT_INTERVAL, (double)mPeriodicShadowReportInterval*mStateUpdateIntervalScale); };

	void SetStateParamReportIntervalScale(double scale) { mStateUpdateIntervalScale = scale; }
	void ResetStateParamReportIntervalScale() { mStateUpdateIntervalScale = 1.0f; }


	IoT_Error_t Run();

	IoT_Error_t Shadow_Get() { return aws_iot_shadow_get(&mMqttClient, gMyThingName, mShadowUpdateCallbackPtr, NULL, 4, true); 	}
	IoT_Error_t Shadow_Delete() { return aws_iot_shadow_delete(&mMqttClient, gMyThingName, mShadowUpdateCallbackPtr, NULL, 4, true); 	}
	void Shadow_Reset_LastReceivedVersion() { aws_iot_shadow_reset_last_received_version(); }

	deque<unsigned int> &AtShadowMsgStat() {return mStat_ShadowPubMsgSizes; }
	deque<unsigned int> &AtDataMsgStat() {return mStat_DataPubMsgSizes; }

	json UpdateReport(IoT_Error_t &rc);
	void ParseParamsFromBuffer(IoT_Error_t &rc);
//	IoT_Error_t SendGetMsg() {
//		string msg = "{\"a\": 1}";
//		return mMQTT_Publisher.Publish((QoSLevel)1, mDesiredTopic, msg);
//	}


	void SetShadowUpdateTopic(string topic) {
		mShadowUpdateTopic = topic;
	}

	json GetPreviousReport();

protected:

	static fpActionCallback_t	mShadowUpdateCallbackPtr;
	static bool	mIsDeltaMsgRecived;
	static json mDeltaMsgJson;
	static bool mRecover;
	static bool mLoadDataFromDB;
	string mShadowUpdateTopic;

	bool mNeedToEraseThingShadow;
	float mPeriodicShadowReportInterval;
	float mStateUpdateIntervalScale;
	float mMinPeriodicDataPublishInterval;
	float mPeriodicFullReportInterval;
	float mPeriodicRefreshDesiredInterval;

	// -- Location of the certificates and keys for TLS authentication
	char mRootCA[PATH_MAX + 1];
	char mClientCRT[PATH_MAX + 1];
	char mClientKey[PATH_MAX + 1];

	char mCertDirectory[PATH_MAX + 1];	// -- Location of the certificates for TLS authentication
	char mHostAddress[255];				// -- URL to the AWS IoT server
	uint32_t mMQTT_Port;				// -- Port at the AWS IoT server

	vector<PubMsg_t> mPubMsg_Queue_in;
	vector<PubMsg_t> mPubMsg_Queue_out;

	void SaveDataMsgToDB();

	pthread_attr_t mThreadAttr;
	pthread_t mPeriodicProcessing_thread;

	static void *PeriodicProcessingThread(void *apData);
	void PeriodicProcessing_Loop();

	double	mInitialTime;

	MQTT_Publisher mMQTT_Publisher;
	vector<SubParam_t> mMQTT_Subscriber;

	ShadowParameters_t mThingShadowInfo;	// -- Info to connect to the corresponding Thing Device in AWS IoT server

	MQTTClient_t mMqttClient;

	deque<unsigned int> mStat_ShadowPubMsgSizes;	// -- Unit: [byte]
	deque<unsigned int> mStat_DataPubMsgSizes;	// -- Unit: [byte]

	/*
	 * @note The delta message is always sent on the "state" key in the json
	 * @note Any time messages are bigger than AWS_IOT_MQTT_RX_BUF_LEN the underlying MQTT library will ignore it. The maximum size of the message that can be received is limited to the AWS_IOT_MQTT_RX_BUF_LEN
	 */
	char mJsonDocumentBuffer_Delta[SHADOW_MAX_SIZE_OF_RX_BUFFER];
	size_t mJsonDocumentBuffer_Delta_size;

	//bool *mMessageArrivedOnDelta;		// -- Set on when DeltaCallback() is called

	char mJsonDocumentBuffer_Periodic[MAX_LENGTH_OF_UPDATE_JSON_BUFFER];
	size_t mJsonDocumentBuffer_Periodic_size;

	// -- Thing Shadow parameters, including (1) Parameters tunable by 'desired' value from Thing Shadow and (2) Report-only data (i.e., one-way reporting from the device to Thing Shadow at AWS IoT)
	// -- Note that you can make the data 'report-only' by pointing the callback to NULL
	std::vector<jsonStruct_t *> mAppParamsToBeReportedAtThisRound[MAX_APP_TYPE_NUM];		// -- {isSent, jsonStruct_t *}

	jsonStruct_t mAppParam_arr[MAX_APP_TYPE_NUM][MAX_STATE_PARAM_NUM_PER_APP];
	unsigned int mAppParam_size[MAX_STATE_PARAM_NUM_PER_APP];


	// -- Delta message for "state"
	jsonStruct_t mDeltaObject;

	// -- Callback function for delta message for "state"
	static void DeltaCallback(const char *pJsonValueBuffer, const uint32_t valueLength, jsonStruct_t *pContext);

	double mPrevTime_shadowUpdate;

};



#endif /* SRC_MODULES_AWS_IOT_MODULE_HPP_ */

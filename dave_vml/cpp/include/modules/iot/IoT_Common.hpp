/*
 * IoT_Common.hpp
 *
 *  Created on: March 3, 2016
 *      Author: paulshin
 */

#ifndef MODULE_IOT_COMMON_HPP_
#define MODULE_IOT_COMMON_HPP_

#include <string>

/* Control messages can be sent in multiple levels: (a) to all the apps of any node in the world, (b) to an app of all nodes in the world, (c) to an app of all nodes in a particular store, (d) to an app of this particular node
 * for (a), the control messages can be published to 'control' for all the apps of all the nodes
 * For (b), the control messages can be published to 'control/<app_id>'	(e.g., control/wifiRSS)
 * For (c), the control messages should be published to 'control/<app_id>/<store_name>'	(e.g., control/wifiRSS/vmhq/16801)
 * For (d), the control messages should be published to 'control/<app_id>/<store_name>/<device_name>' (e.g., control/wifiRSS/vmhq/16801/cam001)
 */
#define AWS_IOT_CONTROL_CHANNEL_PREAMBLE	"control"

/*
 * The Delta Thing Shadow messages from the AWS IoT Cloud will be available from this channel at the internal MQTT broker
 */
#define AWS_IOT_SHADOW_DELTA_CHANNEL_NAME	"state/delta"

/*
 * OmniSensr-side applications wouldn't need to care about this since AWS_IoT_Gateway will use this to prepend to the state topics that the OmniSensr-side applications use to publish their state to the internal MQTT broker.
 * And, the OmniSensr_AWS_IoT_Connector will subscribe all the nested topics from this channel to aggregate the state info from all the OmniSensr-side applications.
 */
#define AWS_IOT_APP_STATE_CHANNEL_PREAMBLE	"state"

/*
 * OmniSensr-side applications wouldn't need to care about this since AWS_IoT_Gateway will use this to publish data to a topic 'data/<app_id>/<store_name_prefix>/<store_name_suffix>/<device_name>/<trackerId_suffix (if you specify)>' (e.g., data/wifiRss/vmhq/16801/cam001 and data/wifiRss/vmhq/16801/cam001/001)
 * IoT_Gateway::SendData() also supports that the data message can also be sent in a subtopic named by 'data/<app_id>/<store_name_prefix>/<store_name_suffix>/<device_name>/<trackerId_suffix (if you specify)>/<subtopic (if you specify)>' (e.g., data/wifiRss/vmhq/16801/cam001/realtime and data/wifiRss/vmhq/16801/cam001/001/calib)
 */
#define AWS_IOT_APP_DATA_CHANNEL_PREAMBLE	"data"

/*
 * This will be prepended by AWS_IOT_APP_STATE_CHANNEL_PREAMBLE (e.g., 'state'), thus will become 'AWS_IOT_APP_STATE_CHANNEL_PREAMBLE/APP_ID_blabla' (e.g., 'state/sample') (see AWS_IOT_APP_STATE_CHANNEL_PREAMBLE in IoT_Common.hpp)
 * These are reserved App IDs for specific Apps.
 *
 * To add a new app, please (1) add its APP_ID_NEW_APP macro below, (2) add the corresponding AppType_t below, and (3) update MosqMQTT_d::on_message() function in OmniSensr_AWS_IoT_Connector.cpp.
 */
#define APP_ID_WIFI_PROCESSOR	"wifiRSS"		// -- App ID for WiFi_PacketProcessor
#define APP_ID_DEMO				"demoApp"		// -- App ID for Demographics
#define APP_ID_VISION_TRACKER	"visionApp"		// -- App ID for Vision Tracker
#define APP_ID_DOOR_COUNTER		"doorApp"		// -- App ID for Door Counter
#define APP_ID_HEALTH			"healthApp"		// -- App ID for Device Health App
#define APP_ID_WIFI_TRACKER		"wifiTrack"		// -- App ID for WiFi_Shopper_Tracker
#define APP_ID_WIFI_VISUALIZER	"wifiViz"		// -- App ID for WiFi_Shopper_Visualizer
#define APP_ID_CONNECTOR		"connectorApp"	// -- App ID for OmniSensor_AWS_IoT_Connector
#define APP_ID_IR_CONTROLLER	"irCtrl"		// -- App ID for OmniSensr_IR_Controller

#define APP_ID_SAMPLE_APP		"sampleApp"		// -- App ID for Sample App
#define APP_ID_TEST				"testApp"		// -- App ID for a temporary test app

// -- Don't change the numerals below. These numbers are referenced by AWS_IoT_Module
typedef enum {
	testApp = 1,
	sampleApp = 2,	// -- 2
	wifiRSS = 3,	// -- 3
	demoApp = 4,	// -- 4
	visionApp = 5,	// -- 5
	doorApp = 6,	// -- 6
	healthApp = 7,	// -- 7
	wifiTrack = 8,	// -- 8
	wifiViz = 9,	// -- 9
	connectorApp = 10, // -- 10
				// -- Max enum must be less than MAX_APP_TYPE_NUM.
} AppType_t;

enum OperationModeType {
	WIFI_TRACKING = 0,
	WIFI_CALIBRATION = 1,
	WIFI_CALIB_VERIFICATION = 2
};

typedef enum {
	NO_PROBLEM = 0,
	TOO_LARGE_PAYLOAD = 1
} VM_IoT_Error_t;

#define MAX_APP_TYPE_NUM				20				// -- Max # of App types. Note: Max enum in AppType_t must be less than this.
#define MAX_STATE_PARAM_NUM_PER_APP		99				// -- Max # of State Parameter for each App.
#define MIN_STATE_REPORT_INTERVAL		10				// -- Unit: [sec]. The state report interval in any app cannot be less than this.

#define MAX_AWS_IOT_DATA_MSG_SIZE		63//127			// -- Unit: [KB]. AWS IoT cloud has limit on the size of the message, which is 128 KB.

/*
 * The type of devState_t should be one of the followings EXCEPT SHADOW_JSON_STRING and SHADOW_JSON_OBJECT since they are not yet supported as of aws-iot-sdk ver 1.1.0
	typedef enum {
		SHADOW_JSON_INT32,
		SHADOW_JSON_INT16,
		SHADOW_JSON_INT8,
		SHADOW_JSON_UINT32,
		SHADOW_JSON_UINT16,
		SHADOW_JSON_UINT8,
		SHADOW_JSON_FLOAT,
		SHADOW_JSON_DOUBLE,
		SHADOW_JSON_BOOL,
		SHADOW_JSON_STRING,
		SHADOW_JSON_OBJECT
	} JsonPrimitiveType;
*/

struct devState_t {
	unsigned int type;		// -- type of devState_t should be one of the JsonPrimitiveType (shown above) EXCEPT SHADOW_JSON_STRING and SHADOW_JSON_OBJECT since they are not yet supported as of aws-iot-sdk ver 1.1.0
	std::string key;		// -- key for Thing Shadow. This must be ONE word, for example, "wifi-state", NOT "wifi state".
    void *value;		// -- Actual value
    void (*cb_func)(std::string key, std::string value);
    bool reportVal;
    double reportInterval;		// -- Unit: [sec]. This state parameter will be periodically reported at every this interval
    double time_lastReported;	// -- Unit: [sec]. Indicates when is the latest time that this state was reported
};

#define IOT_FLOAT_PRECISION			5		// -- In AWS IoT SDK, the precision of a float variable is only up to 5 decimal points, even though the float-type state in Thing Shadow message can transfer a much granular number as a string.
#define IOT_DOUBLE_PRECISION		12		// -- In AWS IoT SDK, the precision of a double variable is only up to 12 decimal points, even though the double-type state in Thing Shadow message can transfer a much granular number as a string.

const int MAX_IOT_STATE_TOKENS_PER_LINE = 1023;	// -- Max # of tokens in a line of tcpdump input
const char* const AWS_IOT_THINGSHADOW_DELIMITER = "\"{}:";


#endif /* MODULE_IOT_COMMON_HPP_ */

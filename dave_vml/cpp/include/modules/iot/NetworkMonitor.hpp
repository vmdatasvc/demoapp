#ifndef MODULES_IOT_NETWORKMONITOR_HPP
#define MODULES_IOT_NETWORKMONITOR_HPP

#include <libudev.h>

#include <netinet/in.h>

#include <map>
#include <string>

#include <boost/function.hpp>


class NetworkMonitor{

	// essentially a struct so we can hold things other than a pair in the map
	class IFStatus{
	public:

		unsigned char status;
		struct in_addr addr;
		uint8_t mac[6];

		IFStatus(){
			status = 0;
			// default in_addr
			memset(mac, 0, 6);
		}
	};

public:

	// A function returning nothing and taking no arguments to do a callback
	typedef boost::function<void (void)> callback_t;


	NetworkMonitor(callback_t change_in = NULL, unsigned int nsec=-1, callback_t panic_in = NULL);
	~NetworkMonitor();
	
private:
	// No copying or assigning!
	NetworkMonitor(const NetworkMonitor&);
	NetworkMonitor& operator=(const NetworkMonitor&);
	
public:
	
	void update_wired_status();	
	bool all_up() const;
	bool any_up() const;
	struct in_addr get_ipaddr() const;
	std::string get_ipaddr_str() const { return format_ipv4_addr(get_ipaddr()); }
	const uint8_t* get_macaddr() const;
	std::string get_macaddr_str() const { return format_mac_addr(get_macaddr()); }
	
	void run();
	
	// let's make threading easy!
	void operator()(){ run(); }

	static std::string format_ipv4_addr(struct in_addr);
	static std::string format_mac_addr(const unsigned char* mac_addr);
	
private:
	void send_linkrequest(void* buf, int len);
	void get_wired_devices();
	void socket_setup();
	bool read_status();

	static bool is_wired(struct udev_device* dev);

	//! default function to execute
	static void default_action();

	//! Action to take upon change
	const callback_t change_action;
	
	//! Number of seconds to wait before panicing.
	const unsigned int panic_sec;
	//! Action to take upon panic
	const callback_t panic_action;
	
	//! map of wired if index to status and IPv4 address
	std::map<int, IFStatus> wired_status;
	
	//! rtnetlink socket
	int sock;
	
	// link bitflags consts
	static const unsigned char LINK_UP = 0x1;
	static const unsigned char ADDR_UP = 0x2;
	static const unsigned char ALL_UP = (LINK_UP | ADDR_UP);
};

#endif

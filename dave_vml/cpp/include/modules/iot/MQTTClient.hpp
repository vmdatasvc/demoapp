/*
 * MQTTReceiver.h
 *
 *  Created on: Jan 9, 2017
 *      Author: jwallace
 */

#ifndef INCLUDE_MODULES_IOT_MQTTCLIENT_H_
#define INCLUDE_MODULES_IOT_MQTTCLIENT_H_

#include <mosquitto.h>

#include <map>
#include <string>

#include <boost/function.hpp>
#include <boost/thread.hpp>
#include <boost/regex.hpp>

class MQTTClient{

public:

	MQTTClient(const char* hostname="localhost", int port=1883, int keepalive=30,
				const char *username = NULL, const char *password=NULL,
				const char *cafile=NULL, const char *capath=NULL, const char *certfile=NULL, const char *keyfile=NULL);

	// If you haven't seen this before, it is a way of making the base class abstract without actually
	// having any pure virtual functions.
	virtual ~MQTTClient() = 0;

private:
	// No copying or assignment
	MQTTClient(const MQTTClient&);
	MQTTClient& operator=(const MQTTClient&);

public:
	void start();
	void run();
	void stop();
	bool isConnected() const {return connected;}

protected:

	virtual void resubscribe() {}

	struct mosquitto* client;

private:

	void setConnected(bool param) {connected = param;}

	bool connected;

private:
	// basic functionality callbacks used by libmosquitto
	// NOTE: no "on_publish", "on_message", or "on_subscribe", as they will be defined in the
	// sender or receiver classes
	// NOTE: The 2nd "void*" parameter will ALWAYS be a pointer to the current class
	static void connect_callback(mosquitto*, void*, int);
	static void disconnect_callback(mosquitto*, void*, int);
	static void log_callback(mosquitto*, void*, int, const char*){}

	// I need to call mosquitto_lib_init() before I do anything.  However, this call is NOT thread-safe
	// So, in order to ensure that I call the function exactly ONCE in a single thread, I initialize
	// the following variable with the return code of mosquitto_lib_init().  Because this is a static,
	// we ensure that this will be called on initialization of the class itself, which occurs
	// BEFORE the entry into main(), which is guaranteed to occur in a single thread.
	//
	// The only downside to this method is that I don't know precisely when the method will be called
	// in relation to other static variables in other classes.  Thus, if there were another class that
	// had a static variable that used this variable, this method would not work.
	const static int LIBINIT_VAL;
};

class MQTTSender : public MQTTClient {

public:
	MQTTSender(const char* hostname="localhost", int port=1883, int keepalive=60,
			const char *username = NULL, const char *password=NULL,
			const char *cafile=NULL, const char *capath=NULL, const char *certfile=NULL, const char *keyfile=NULL);
	virtual ~MQTTSender() {}

	int publish(const std::string& topic, const std::string& payload, int qos=0);
private:
	MQTTSender(const MQTTSender&);
	MQTTSender& operator=(const MQTTSender&);

	static void publish_callback(mosquitto*, void*, int) {}

};

class MQTTReceiver : public MQTTClient {
public:

	//! A callback taking the mosquitto message, and performs some action
	typedef boost::function<void (const mosquitto_message*)> recv_callback;

	MQTTReceiver(const char* hostname="localhost", int port=1883, int keepalive=60,
			const char *username = NULL, const char *password=NULL,
			const char *cafile=NULL, const char *capath=NULL, const char *certfile=NULL, const char *keyfile=NULL);
	virtual ~MQTTReceiver();

private:
	//! No copying or assigning
	MQTTReceiver(const MQTTReceiver&);
	MQTTReceiver& operator=(const MQTTReceiver&);

public:
	int subscribe(const std::string& topic, recv_callback sub_action, int qos=0);
	int unsubscribe(const std::string& topic);

protected:
	virtual void resubscribe();

private:

	void processMessage(const mosquitto_message *msg);

	void lock() { action_lock.lock();}
	void unlock() {action_lock.unlock();}

	static boost::regex convertTopicPattern(const std::string& pattern);

	static void subscribe_callback(mosquitto*, void*, int, int, const int*){}
	static void message_callback(mosquitto*, void*, const mosquitto_message*);

	//! mutex to lock the topic_actions data structure
	boost::mutex action_lock;

	typedef std::multimap<boost::regex, recv_callback> action_map_t;
	action_map_t topic_actions;
	std::map<std::string, int> subscribe_topics;

};

#endif /* INCLUDE_MODULES_IOT_MQTTRECEIVER_H_ */

/*
 * IOTCommunicator.hpp
 *
 *  Created on: Jan 12, 2017
 *      Author: jwallace
 */

#ifndef INCLUDE_MODULES_IOT_IOTCOMMUNICATOR_HPP_
#define INCLUDE_MODULES_IOT_IOTCOMMUNICATOR_HPP_

#include <string>
#include <vector>

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/function.hpp>

#include "Dev_Common.hpp"
#include "MQTTClient.hpp"

class IOT_Communicator {
public:
	typedef boost::function<void(const std::string&)> callback_t;

	IOT_Communicator(const std::string& appName, const std::string& awsPrefix="");
	virtual ~IOT_Communicator();

private:
	// No copying or assignment!
	IOT_Communicator(const IOT_Communicator&);
	IOT_Communicator& operator=(const IOT_Communicator&);

public:

	template <class T>
	bool sendState(const T& data, bool set_desired=false);

	template <class T>
	bool sendData(const T& msg, const std::string& subtopic=""){
		return publishMsg(msg, dataTopic, subtopic);
	}

	template <class T>
	bool sendPrivateData(const T& msg, const std::string& subtopic=""){
		return publishMsg(msg, privateTopic, subtopic);
	}

	template <class T>
	bool sendError(const T& msg, bool fatal=true, bool restart=false);

	bool sendHeartbeat(unsigned int interval=300);

	bool RegisterDeltaCallback(callback_t delta_action);
	bool RegisterControlCallback(callback_t control_action);

	const std::string& getSerial() const {
		return devinfo.GetMySN_();
	}

	const std::string& getStoreLoc() const {
		return devinfo.GetMyStoreName_();
	}
	const std::string& getDeviceName() const {
		return devinfo.GetMyDevName_();
	}

/*
 * Commented out, re-enable only as needed!
private:

	const std::string& GetMyIpAddr() const {
		return devinfo.GetMyIpAddr_();
	}
	const std::string& GetMySN() const {
		return devinfo.GetMySN_();
	}

	*/

private:

	DeviceInfo devinfo;

	MQTTSender send;
	MQTTReceiver recv;

	const std::string appName;

	std::vector<std::string> controlTopics;
	std::string deltaTopic;
	std::string dataTopic;
	std::string statusTopic;
	std::string heartbeatTopic;
	std::string crashTopic;
	std::string privateTopic;

	template <class T>
	bool publishMsg(const T& msg, const std::string& topic, const std::string& subtopic="");

	static std::string getTimestamp();

	static void translator(callback_t action, const struct mosquitto_message *message);
	//static void control_translator(control_cb_t control_action, const struct mosquitto_message *message);
};

template <class T>
bool IOT_Communicator::sendState(const T& data, bool set_desired){
	std::ostringstream data_os;
	data_os << "{ \"state\": { \"reported\": { \"" << appName << "\":" << data;
	if(set_desired){
		data_os << "}, \"desired\": { \"" << appName << "\":" << data;
	}
	data_os << "}}}";
	return (send.publish(statusTopic, data_os.str()) == MOSQ_ERR_SUCCESS);
}

template <class T>
bool IOT_Communicator::sendError(const T& msg, bool fatal, bool restart){
	std::ostringstream data_os;

	// default action, when fatal == true and restart == false
	std::string action = "stop";
	// if fatal == false, simply a report
	if(!fatal){
		action = "report";
	// if fatal == true and restart == true, then request restart
	} else if(restart){
		action = "restart";
	}

	data_os << "{\"date\":\"" << getTimestamp() << "\",";
	data_os << "\"name\":\"" << appName << "\",";
	data_os << "\"actionRequested\":\"" << action << "\",";
	data_os << "\"info\":\"" << msg << "\"}";

	return (send.publish(crashTopic, data_os.str()) == MOSQ_ERR_SUCCESS);

}

template <class T>
bool IOT_Communicator::publishMsg(const T& msg, const std::string& topic, const std::string& subtopic){
	std::string topic_out = topic;
	if(subtopic.size() > 0){
		topic_out += "/" + subtopic;
	}

	std::ostringstream data_os;
	data_os << msg;

	return (send.publish(topic_out, data_os.str()) == MOSQ_ERR_SUCCESS);
}

#endif /* INCLUDE_MODULES_IOT_IOTCOMMUNICATOR_HPP_ */

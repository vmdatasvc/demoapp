/*
 * Dev_Common.hpp
 *
 *  Created on: Sep 8, 2016
 *      Author: paulshin
 */

#ifndef INCLUDE_MODULES_IOT_DEV_COMMON_HPP_
#define INCLUDE_MODULES_IOT_DEV_COMMON_HPP_

#include <string>

#include <boost/thread.hpp>

#include "NetworkMonitor.hpp"

class DeviceInfo {

public:
	DeviceInfo();
	virtual ~DeviceInfo();

// copying/assignment probably bad!
// (if you MUST, then probably a good idea to use a boost::shared_ptr on the mNetMonitor)
private:
	DeviceInfo(const DeviceInfo&);
	DeviceInfo& operator=(const DeviceInfo&);

public:

	const std::string& GetMyIpAddr_() const { return mAP_IPaddr; }
	const std::string& GetMACAddr_() const { return mMAC_addr; }
	const std::string& GetMySN_() const { return mSerialNumber; }
	const std::string& GetMyStoreName_() const { return mStoreName; }		// -- e.g., vmhq/16801
	const std::string& GetMyDevName_() const { return mDevName; }

	std::string GetMyStoreName_dash_() const;

protected:

	std::string mAP_IPaddr;
	std::string mMAC_addr;
	std::string mStoreName;
	std::string mSerialNumber;
	std::string mDevName;

private:

	NetworkMonitor* mNetMonitor;
	boost::thread* mMonitorThread;

	// Update the IP address (and any other networking info)
	// - use this as a callback in the mNetMonitor
	void updateNetworkInfo();

};

#endif /* INCLUDE_MODULES_IOT_DEV_COMMON_HPP_ */

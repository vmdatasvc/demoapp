#include <string>
#include <map>
#include <boost/date_time/posix_time/posix_time.hpp>

class IOTUtility {
public:
	IOTUtility() {
		setAppNameType();
	};

	std::string getAppType(std::string appName) {
		std::map<std::string, std::string>::iterator it = nameToType.find(appName);
		return (it == nameToType.end()) ? "Invalid appName" : it->second;
	}

	std::string getAppName(std::string appType) {
		std::map<std::string, std::string>::iterator it = typeToName.find(appType);
		return (it == typeToName.end()) ? "Invalid appType" : it->second;
	}
	static std::string getTimestamp(){
		return boost::posix_time::to_iso_string(boost::posix_time::second_clock::universal_time());
	}
private:
	std::map<std::string, std::string> nameToType;
	std::map<std::string, std::string> typeToName;
	void setAppNameType() {
		nameToType["testApp"]		=	"1";
		nameToType["sampleApp"]		=	"2";
		nameToType["wifiRSS"]		=	"3";
		nameToType["demoApp"]		=	"4";
		nameToType["visionApp"]		=	"5";
		nameToType["doorApp"]		=	"6";
		nameToType["healthApp"]		=	"7";
		nameToType["wifiTrack"]		=	"8";
		nameToType["wifiViz"]		=	"9";
		nameToType["connectorApp"]	=	"10";

		typeToName["1"]		=	"testApp";
		typeToName["2"]		=	"sampleApp";
		typeToName["3"]		=	"wifiRSS";
		typeToName["4"]		=	"demoApp";
		typeToName["5"]		=	"visionApp";
		typeToName["6"]		=	"doorApp";
		typeToName["7"]		=	"healthApp";
		typeToName["8"]		=	"wifiTrack";
		typeToName["9"]		=	"wifiViz";
		typeToName["10"]	=	"connectorApp";
	}
};

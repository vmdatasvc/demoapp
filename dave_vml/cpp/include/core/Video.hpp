/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
/*
 * Video.hpp
 *
 *  Created on: Oct 6, 2015
 *      Author: yyoon
 */

#ifndef VML_VIDEO_HPP_
#define VML_VIDEO_HPP_

#ifdef _WIN32
#include <WinSock2.h>
#include <stdint.h>
struct timezone
{
  int  tz_minuteswest; /* minutes W of Greenwich */
  int  tz_dsttime;     /* type of dst correction */
};
int gettimeofday(struct timeval * tp, struct timezone * tzp);
#else
#include <sys/time.h>
#endif

#include <boost/shared_ptr.hpp>
#include <opencv2/highgui/highgui.hpp>

#ifdef _WIN32
#include <core/Image.hpp>
#include <core/CameraCalibration.hpp>
#include <CircularBuffer.hpp>
#include <PvUtil.hpp>
#include <ait/Image.hpp>
#else
#include <opencv2/video.hpp>
#include <opencv2/videoio.hpp>
#include <core/Image.hpp>
#include <core/CameraCalibration.hpp>
#include <legacy/pv/CircularBuffer.hpp>
#endif

//#include <modules/vision/VisionModuleManager.hpp>
#include <modules/utility/TimeKeeper.hpp>
#include <core/Error.hpp>

#include <core/Settings.hpp>

// Video buffer
namespace vml {

#ifdef _WIN32
typedef boost::shared_ptr<cv::Mat> matPtr;
#endif

template<class T>	// templatized for capturing frame type
class VideoBuffer
{
public:

	VideoBuffer();	// constructor for compatibility
	virtual ~VideoBuffer();

#ifdef _WIN32
	virtual void initialize(ait::Image32& img,int buffer_size);
	virtual bool grabFrame2(ait::Image32& img, double curFTime);
#else
	virtual void initialize(boost::shared_ptr<SETTINGS> settings,std::string video_file_name);
	virtual bool grabFrame(void);
	boost::shared_ptr<T> grabAndReturnFrame(void);	// grab frame method that doesn't insert into circular buffer
	bool grabInFrame(boost::shared_ptr<T> frame);
#endif

	void setCurrentFrame(boost::shared_ptr<T> frame);
	const boost::shared_ptr<T> currentFrame(void) const;
	const boost::shared_ptr<T> prevFrame(void) const;
	const boost::shared_ptr<T> nthPrevFrame(int index) const;	// current+index'th frame

	boost::shared_ptr<T> currentFrame(void);


#ifndef _WIN32
	bool rewind(int start_frame); // disable rewind for vms
#endif

	int	getRows() {return mRows;}
	int getCols() {return mCols;}
	int getPixelCount() {return mnPixels;}
	float getDiagonal() {return mDiagonal;}
	int getCurrentFrameNumber() {return mCurrentFrameNumber;}
	bool isInitialized() {return mInitialized;}

#ifdef _WIN32
    void copyPvImage32ToIplTmp(ait::Image32& img, IplImage *pIplImage);
	IplImage* convertPvImage32ToIplTmp(ait::Image32& img);
	static matPtr Image32ToMat(ait::Image32& img);
#endif

protected:
	// members
	bool	mInitialized;

	ait::CircularBuffer<boost::shared_ptr<T> >		mFrameBuffer;

#ifdef _WIN32
	matPtr	mImagePtr;
#else
	cv::VideoCapture*			mpVideoCapture;
	cv::Mat	mImage;		// for capturing and etc
#endif

	int	mRows;
	int	mCols;
	int mnPixels;		// number of pixels
	float mDiagonal;	// image diagonal length
//	int mImageFormat;
	int	mBufferSize;

	int	mCurrentFrameNumber;
	double	mEpochTime;

	boost::shared_ptr<SETTINGS>				mpSettings;
};	// of class VideoBuffer

template<class T> VideoBuffer<T>::VideoBuffer() :
		mInitialized(false),
		mpVideoCapture(NULL),
		mRows(0),
		mCols(0),
		mnPixels(0),
		mDiagonal(0.f),
//		mImageFormat(0),
		mBufferSize(0),
		mCurrentFrameNumber(0),
		mEpochTime(-1.f)
{
}

template<class T> VideoBuffer<T>::~VideoBuffer()
{
#ifndef _WIN32
	if (mInitialized)
	{
		mpVideoCapture->release();
	}
	delete(mpVideoCapture);
#endif
}

#ifdef _WIN32
template<class T> void VideoBuffer<T>::initialize(boost::shared_ptr<SETTINGS> settings,ait::Image32& img,int buffer_size)
{
	mpSettings = settings;
	mBufferSize = settings->getInt("VideoBuffer/bufferSize",1,false);

	mImagePtr = Image32ToMat(img);

    mRows = mImagePtr->rows;
    mCols = mImagePtr->cols;
    mnPixels = mRows*mCols;
	mDiagonal = sqrt((float)((mRows*mRows)+(mCols*mCols)));

//    mImageFormat = mImagePtr->type();

    mFrameBuffer.setCapacity(buffer_size);

    mInitialized = true;
    mCurrentFrameNumber = 0;

}

template<class T> matPtr VideoBuffer<T>::Image32ToMat(ait::Image32& img)
{

	return ((matPtr) new cv::Mat(img.height(),img.width(),CV_8UC4,(void *)img.pointer()));
//	cv::Mat tMat(img.height(), img.width(), CV_8UC3, (void*)img.pointer(),img.width()*3);
//	cv::Mat tMat(img.height(),img.width(),CV_8UC3);
//	cv::cvtColor(*mImagePtr,tMat,CV_BGRA2RGB);
//	imwrite("C:\\foo.jpg",*mImagePtr);
//	imwrite("C:\\foo1.jpg",tMat);
}

template<class T> bool VideoBuffer<T>::grabFrame2(ait::Image32& img, double curFTime)
{
	//mImage = cv::cvarrToMat(convertPvImage32ToIplTmp(img));
	mImagePtr = Image32ToMat(img);

	boost::shared_ptr<T>	tFramePtr;
	tFramePtr.reset(new T(mRows,mCols));
	tFramePtr->grab(img,curFTime);

//	tImagePtr->time = mEpochTime + mVideoCapture.get(CV_CAP_PROP_POS_MSEC)*1e-3;
//	cv::cvtColor(*mImagePtr,tImagePtr->image,CV_BGRA2RGB);
//	int mImg = mImagePtr->type();
//	int tImg = tImagePtr->image.type();
	// convert mImage to tImage
/*
if (mImagePtr->type() != tImagePtr->image.type())
	{
		// for converting image depth.
		// for float or double type images, value range is [0,1]
                //float	normFactor = 1.f;
		int	mImageDepth = mImagePtr->type() & CV_MAT_DEPTH_MASK;
		int tImageDepth = tImagePtr->image.type() & CV_MAT_DEPTH_MASK;
		// conversion needed
		mImagePtr->convertTo(tImagePtr->image,tImagePtr->image.type());
		if ((tImageDepth == CV_32F) || (tImageDepth == CV_64F))
		{
			if (mImageDepth == CV_8U)
			{
				tImagePtr->image /= 255.f;
			}
			else if (mImageDepth == CV_16U)
			{
				tImagePtr->image /= 65535.f;
			}
		}
	}
	else {

		// just deep copying should do it
		mImagePtr->copyTo(tImagePtr->image);
	}

	// convert to RGB
	cv::cvtColor(tImagePtr->image,tImagePtr->image,CV_BGR2RGB);
*/
	// push back
	mFrameBuffer.push_front(tFramePtr);
	mCurrentFrameNumber++;

	return true;
}

#else
template<class T> void VideoBuffer<T>::initialize(boost::shared_ptr<SETTINGS> settings,std::string video_file_name)
{
	mInitialized = false;
	mpSettings.swap(settings);
	mBufferSize = mpSettings->getInt("VideoBuffer/bufferSize",1,false);

	// open video capture
    if (mpVideoCapture)
    {
    	if (mpVideoCapture->isOpened())
    	{
    		std::cout << "Closing previous video stream" << std::endl;
            mpVideoCapture->release();
    	}
    	std::cout << "Deleting previous video stream" << std::endl;
    	delete(mpVideoCapture);
    }

    mpVideoCapture = new cv::VideoCapture;

    // check if video url is address (file path) or device number
    // Assuming if the video_file_name string starts with a number, that's device number
    bool openSuccess = false;
    int	nTrial = 0;
    do
    {
    	std::cout << "Opening video at " << video_file_name << std::endl;
        if (isdigit(video_file_name[0]))
        {
            openSuccess = mpVideoCapture->open(atoi(video_file_name.c_str()));
        }
        else
        {
            openSuccess = mpVideoCapture->open(video_file_name);
        }

        if (!openSuccess)
        {
        	nTrial++;
        	std::cout << "Failed to open video. Tried " << nTrial << " times.";
        	if (nTrial < 10)
        	{
        		std::cout << ".. Retrying in 2 sec..." << std::endl;
        		sleep(2);
        	}
        	else
        	{
        		std::cout << std::endl;
        	}
        }
    } while ((!openSuccess)&&(nTrial<10));

    if (!openSuccess)
    {
    	// error
    	VML_ERROR(MsgCode::IOErr,"Cannot open video file: " << video_file_name)
    	mInitialized = false;
    	return;
    }

    *mpVideoCapture >> mImage;

    mRows = mImage.rows;
    mCols = mImage.cols;
    mnPixels = mRows*mCols;
    mDiagonal = sqrt(mRows*mRows+mCols*mCols);

    std::cout << "Video dimension is (" << mRows << "," << mCols << ")." << std::endl;

//    mImageFormat = mImage.type();

    mFrameBuffer.setCapacity(mBufferSize);

    mInitialized = true;

    mCurrentFrameNumber = 0;

    // NOTE: epoch time should be defined as the time when the video sequence
    // was starting to record, but temporarily define it as when the video
    // was opened.
    // Copy of PvUtil::time()
    mEpochTime = gCurrentTime();
    std::cout << "New video epoch time is " << mEpochTime << std::endl;
}

template<class T> bool VideoBuffer<T>::grabFrame()
{
	VML_ASSERT(mInitialized);

	boost::shared_ptr<T> tFramePtr;
	tFramePtr.reset(new T(mRows,mCols));
	if (not tFramePtr->grab(*mpVideoCapture,mEpochTime))
	{
		return false;
	}

	// push new frame to the buffer
	mFrameBuffer.push_front(tFramePtr);
	mCurrentFrameNumber++;

	return true;
}

template<class T> boost::shared_ptr<T> VideoBuffer<T>::grabAndReturnFrame()
{
	VML_ASSERT(mInitialized);

	boost::shared_ptr<T> tFramePtr;
	tFramePtr.reset(new T(mRows,mCols));
	if (not tFramePtr->grab(*mpVideoCapture,mEpochTime))
	{
		// if failed to grab a new frame, return null pointer
		return boost::shared_ptr<T>();
	}
	else
	{
		mCurrentFrameNumber++;
		return tFramePtr;
	}
}

template<class T> bool VideoBuffer<T>::grabInFrame(boost::shared_ptr<T> frame)
{
	VML_ASSERT(mInitialized);

	if (not frame->grab(*mpVideoCapture,mEpochTime))
	{
		// if failed to grab a new frame, return null pointer
		return false;
	}
	else
	{
		mCurrentFrameNumber++;
		return true;
	}
}

template<class T> bool VideoBuffer<T>::rewind(int start_frame)
{
	// rewind image grabber to the designated frame
	// return true, if success. false if not

	mCurrentFrameNumber = start_frame;
	mEpochTime = gCurrentTime();
	return mpVideoCapture->set(CV_CAP_PROP_POS_FRAMES, (double)start_frame);
}
#endif

template<class T> void VideoBuffer<T>::setCurrentFrame(boost::shared_ptr<T> frame)
{
	mFrameBuffer.push_front(frame);
	mCurrentFrameNumber++;
}

template<class T> const boost::shared_ptr<T> VideoBuffer<T>::currentFrame() const
{
	return(mFrameBuffer.front());
}

template<class T> boost::shared_ptr<T> VideoBuffer<T>::currentFrame()
{
	return(mFrameBuffer.front());
}

template<class T> const boost::shared_ptr<T> VideoBuffer<T>::prevFrame() const
{
	return(mFrameBuffer[1]);
}

template<class T> const boost::shared_ptr<T> VideoBuffer<T>::nthPrevFrame(int index) const
{
	// return relatively indexed frame from buffer.
	// ex) 0 returns current one, 1 returns previous one, ...
	VML_ASSERT(not (index<0));

	return(mFrameBuffer[index]);
}


} // of namespace vml

#endif /* VIDEO_HPP_ */

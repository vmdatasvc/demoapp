/*
 * OmniSensrApp.hpp
 *
 *  Created on: Jan 10, 2017
 *      Author: jwallace
 */

#ifndef INCLUDE_PROJECTS_OMNISENSR_SAMPLEAPP_OMNISENSRAPP_HPP_
#define INCLUDE_PROJECTS_OMNISENSR_SAMPLEAPP_OMNISENSRAPP_HPP_

#include <string>
#include <map>

#include <iostream>

#include <boost/function.hpp>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

// -- Refer to https://github.com/nlohmann/json
// used to parse delta messages
#include "json.hpp"

#include "modules/utility/ParseConfigTxt.hpp"
// NOTE: technically, I could forward define the ParseConfigTxt class, but I don't wanna

#include "modules/iot/IOTCommunicator.hpp"

class OmniSensrApp {
public:

	typedef boost::function< void(const std::string &)> callback_t;

	OmniSensrApp(const std::string& name, const std::string& configFile);
	virtual ~OmniSensrApp();

private:
	// No copying or assignment
	OmniSensrApp(const OmniSensrApp&);
	OmniSensrApp& operator=(const OmniSensrApp&);

public:

	void Run();

protected:

	// main processing loop code goes here
	virtual void start() = 0;

	// initialize the configuration of your app
	// by default, do nothing
	virtual unsigned int initConfig(ParseConfigTxt& parser) {return 0;};

	// save the current state to a file.  By default, do nothing
	// inverse operation of initConfig
	virtual void saveState(std::ostream& os) const {}

	// process a command message.  By default, do nothing.
	virtual void processCommand(const std::string& msg) {}

	// register and unregister delta message callbacks
	bool registerDeltaCallback(const std::string& variable, callback_t delta_fn);
	bool unregisterDeltaCallback(const std::string& variable);

	// register a variable, using the default action for a delta callback
	// mainly intended as an alternative to the DeltaCallback for "simple" cases
	// NOTE: it is assumed that T is an atomic variable
	// (or at least thread-safe and convertible from string)
	template <class T>
	bool registerVariable(const std::string& name, T& var);

	// register a variable, using provided getter/setter functions
	// Intended for more complex cases where thread safety must be
	// enforced when modifying T.
	// NOTE: T must be copy-constructible and the json class needs to
	// know how to handle it!
	template <class T>
	bool registerVariable(const std::string& name, typename boost::function<const T&(void)> getter_fn, boost::function<bool(const std::string&)> setter_fn);

	// Update the state of a thingshadow variable
	template <class T>
	bool updateState(const std::string& name, const T& val);

	// send the state of a single variable
	bool sendState(const std::string& variable);

	// send a partial JSON as the state
	bool sendPartialState(const nlohmann::json& state);

	// send the entire reported state
	bool sendFullState();

	// send some data
	template <class T>
	void sendData(const T& msg, const std::string& subtopic = ""){
		IoT_Comm.sendData(msg, subtopic);
	}

	// send data on a private channel (not going to AWS)
	template <class T>
	void sendPrivateData(const T& msg, const std::string& subtopic = ""){
		IoT_Comm.sendPrivateData(msg, subtopic);
	}

	// save the settings to the configuration file
	bool SaveSetting() const;

	// send a heartbeat
	bool heartbeat(unsigned int interval=300) {return IoT_Comm.sendHeartbeat(interval);}

	// send an error message
	bool sendError(const std::string& msg, bool fatal=true, bool restart=false) {return IoT_Comm.sendError(msg, fatal, restart);}

	// get the serial number from the IOTCommunicator
	const std::string& getSerialNumber() const { return IoT_Comm.getSerial(); }

	// get the device Name from the IOTCommunicator
	const std::string& getDeviceName() const { return IoT_Comm.getDeviceName(); }

	// get the store and location from the IOTCommunicator
	const std::string& getStoreLocation() const { return IoT_Comm.getStoreLoc(); }

	// convert the given timestamp into double seconds since the epoch
	static double convertTime(const boost::posix_time::ptime& timestamp);

	//! the name of the app, must set in child ctor
	const std::string appName;

	//! The current state of the app, as used in the ThingShadow
	nlohmann::json appState;

	//! a lock to protect appState
	boost::recursive_mutex state_lock;

private:

	// parse the configuration file and any app-wide settings
	bool ParseSetting();

	// periodically send the state
	void sendFullStateLoop();

	// save app configuration parameters
	void saveAppState(std::ostream& os) const;

	//! sleep reporting thread
	boost::thread* reportingThread;

	//! the path to the configuration file
	const std::string configPath;

	//! interval on which to send the full state (set to -1 to essentially sleep forever)
	int stateReportInterval;

	IOT_Communicator IoT_Comm;

	//IoT_Gateway	mIoTGateway;

	typedef std::multimap<std::string, callback_t> callback_map_t;

	// a map of variables to actions to take
	boost::mutex delta_cb_lock;
	callback_map_t delta_callback_map;

	// callback used to modify a simple registered variable
	// will be used in conjunction with boost::bind
	template <class T>
	void setVariable(const std::string& name, T& var, const std::string& msg);

	template <class T>
	void setVariable(const std::string& name, typename boost::function<const T&(void)> getter_fn, boost::function<bool(const std::string&)> setter_fn, const std::string& msg);

	// dispatch functions to pass to the IoTGateway class
	void deltaState_callback(const std::string& deltaMsg);
	void control_callback(const std::string& msg);
};

template <class T>
bool OmniSensrApp::registerVariable(const std::string& name, T& var){
	// first attempt to register the callback
	bool retval = registerDeltaCallback(name, boost::bind(&OmniSensrApp::setVariable<T>, this, name, boost::ref(var), _1));

	// if the callback is successful, then add to the appState
	if(retval){
		state_lock.lock();
		appState[name] = var;
		state_lock.unlock();
	}

	return retval;
}

template <class T>
bool OmniSensrApp::registerVariable(const std::string& name,
		typename boost::function<const T&(void)> getter,
		boost::function<bool(const std::string&)> setter){

	bool retval = registerDeltaCallback(name, boost::bind(&OmniSensrApp::setVariable<T>, this, name, getter, setter, _1));
	T tmpval = getter();

	if(retval){
		state_lock.lock();
		appState[name] = tmpval;
		state_lock.unlock();
	}

	return retval;
}

template <class T>
void OmniSensrApp::setVariable(const std::string& name, T& var, const std::string& msg){
	T tmp_val;
	try{
		tmp_val = boost::lexical_cast<T>(msg);
		state_lock.lock();
		var = tmp_val;
		appState[name] = var;
		sendState(name);
		state_lock.unlock();
	} catch(boost::bad_lexical_cast& e) {}
}

template <class T>
void OmniSensrApp::setVariable(const std::string& name,
		typename boost::function<const T&(void)> getter_fn,
		boost::function<bool(const std::string&)> setter_fn,
		const std::string& msg){

	bool success = setter_fn(msg);
	T tmpval = getter_fn();
	if(success){
		state_lock.lock();
		appState[name] = tmpval;
		sendState(name);
		state_lock.unlock();
	}

}

template <class T>
bool OmniSensrApp::updateState(const std::string& name, const T& val){
	bool retval = false;
	state_lock.lock();
	appState[name] = val;
	retval = sendState(name);
	state_lock.unlock();
	return retval;
}

#endif /* INCLUDE_PROJECTS_OMNISENSR_SAMPLEAPP_OMNISENSRAPP_HPP_ */

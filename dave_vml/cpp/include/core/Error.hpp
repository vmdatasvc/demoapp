/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
/*
 * Error.hpp
 *
 *  Created on: Oct 1, 2015
 *      Author: yyoon
 */

#ifndef ERROR_HPP_
#define ERROR_HPP_

#ifdef _WIN32

#include <PvUtil.hpp>
#include <sstream>

#define VML_ERROR(code,msg)		{std::stringstream errmsg; errmsg << "VML Error: " << msg << std::endl; PvUtil::exitError(errmsg.str().c_str());}
#define VML_WARN(code,msg)		{std::stringstream errmsg; errmsg << "VML Warning: " << msg << std::endl; PvUtil::error(errmsg.str().c_str());}
#define VML_ASSERT(expr)            PVASSERT(expr)

#else
#include <iostream>
#include <sstream>
#include <stdexcept>

#ifdef _WIN32
#include <PvUtil.hpp>
#endif

#define WARN_BUFFER_LEN 1024

namespace vml {

namespace MsgCode
{
	enum Code
	{
		// success
		success             =   0,

		// error codes
		unknownErr          =   1,  // default Erring
		IOErr               =   2,
		memAllocErr         =   3,
		dataAllocErr        =   4,  // data allocation error
		logicErr            =   5,  // logic err
		runTimeErr          =   6,  // runtime error
		assignErr           =   7,  // assignment error
		sizeMisMatchErr     =   8,  // size mismatch error
		typeMisMatchErr     =   9,  // type Mismatch
		unknownMethodErr    =  10,  // unknown method error
		notImplementedErr   =  12,  // not implemented error
		numericalErr        =  13,
		badDataErr          =  14,
		methodFailedErr     =  15,
		notSupportedErr     =  16,
		paramErr            =  17,
		outOfBoundsErr      =  18,
		imgSizeErr          =  19,  // bad image size
		unknownTypeErr      =  20,
		assertionFailedErr  =  21,
		deployErr           =  22,
		openCVErr           =  23,
		assertErr			=  24,
		// More error code comes here
		DisplayErr          = 100,  // must be of highest value
	};
}; // NAMESPACE MsgCode

/** Logger description goes here*/
class Logger
{
public:

	/*Default constructor*/
	Logger();

	~Logger();

	bool startLogging(const std::string fname,
	                  std::ios_base::openmode mode = std::ios_base::app);

	bool startLogging(std::ostream *os = & std::cerr);

	bool startLogging(std::ofstream *fs);

	void stopLogging();

	void outputMessage(const std::string &msg);
	void logMessage(const std::string &msg);

	static std::string getTime();

private:
	std::ofstream       *mFileStream;
	std::ostream        *mOStream;

	Logger(const Logger &rhs);
	Logger& operator = (const Logger &rhs);

	bool                mIsSetInternally;

}; // CLASS Logger

// Throwable error object that also stores one of our message codes and defines
// static methods for issuing errors, warnings, and messages in the right way
// depending on type of build.
class Error : public std::runtime_error
{
public:

	//default constructor
	Error(const std::string &what_arg, const MsgCode::Code errCode);

	inline MsgCode::Code code( void ) const {
		return mCode;
	}

	static void issueWarning(MsgCode::Code code,
	                         std::string msg, int line);

	static void issueError(MsgCode::Code code,
	                       std::string msg, int line);

	static int cvErrorHandler(int status, const char* err_msg,
	                          int line, void* userdata);

	static void message(const char *msg, ... );
	static void message(const std::string &msg);
	static void message(const std::stringstream &msg);

	static std::string getTime();

	static Logger mLog;

protected:
	static void write_message(const std::string &msg);

	MsgCode::Code mCode;

}; // CLASS Error

} // of namespace vml

// These do the right things internally w.r.t. exposing information depending on
// Debug/Release/Deploy mode:
// (Note that VML_FILECODE is baseically basename(__FILE__) in Debug and
//  a magical hex code in Release.)
#ifdef _WIN32
#define VML_ERROR(code,msg)		PvUtil::error(msg)
#define VML_WARN(code,msg)		PVMSG(msg)
#define VML_ASSERT(expr)		PVASSERT(expr)
#define VML_MSG(msg)			PVMSG(msg)
#else
#define VML_ERROR(code,msg)         { std::stringstream tmp; \
                                    tmp << msg << std::endl;\
                                    vml::Error::issueError(code,tmp.str(),\
                                    __LINE__);\
                                    }

#define VML_WARN(code,msg)          { std::stringstream tmp; \
                                    tmp << msg;\
                                    vml::Error::issueWarning(code,tmp.str(),\
                                    __LINE__);\
                                    }

//#ifndef NDEBUG
// In DEBUG Mode: build in messages and assertions.

// This will be safe even in VMLMEX mode, unlike "regular" assert():
#define VML_ASSERT(expr)            { \
                                    if (! bool(expr)) { \
                                    vml::Error::issueError(vml::MsgCode::assertErr, \
                                                           "Assertion failed", \
                                                           __LINE__); \
                                    } \
                                    }



#define VML_MSG(msg)                { std::stringstream tmp; \
                                    tmp << msg << std::endl; \
                                    vml::Error::message(tmp);\
                                    }
//#endif // NDEBUG
#endif

#endif // ifdef _WIN32
#endif /* ERROR_HPP_ */

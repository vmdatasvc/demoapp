/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * CameraCalibration.hpp
 *
 *  Created on: Nov 16, 2015
 *      Author: yyoon
 */

#ifndef CAMERACALIBRATION_HPP_
#define CAMERACALIBRATION_HPP_

#include <opencv2/core.hpp>
#include <opencv2/calib3d.hpp>
#include <boost/shared_ptr.hpp>

#include <core/Settings.hpp>

#ifdef _WIN32
#include <WinSock2.h>
#else
#include <sys/time.h>
#endif
#include <math.h>


namespace vml {

// person shape renderer using camera calibration parameter

#ifdef _WIN32
const float M_PI=3.1415926535897932384626433832795f;
#endif

class CameraModelOpenCV
{
public:

	CameraModelOpenCV(int rows, int cols, boost::shared_ptr<SETTINGS> settings);
	CameraModelOpenCV(int rows,
			int cols,
			std::string name,
			float camera_height,
			float ccd_width,
			float ccd_height,
			float focal_length,
			std::vector<double> distortion_parameters);	// constructor for legacy postprocessing
	~CameraModelOpenCV();

	bool isValid() {return mValid;}

	void undistortImage(cv::Mat &image_input, cv::Mat &image_output);
	cv::Point undistortPoint(cv::Point p);
	cv::Point distortPoint(cv::Point p);
	cv::Point2f undistortPoint(cv::Point2f p);
	cv::Point2f distortPoint(cv::Point2f p);

	void undistortPoints(const std::vector<cv::Point2f> &input_points, std::vector<cv::Point2f> &output_points);
	void distortPoints(const std::vector<cv::Point2f> &input_points, std::vector<cv::Point2f> &output_points);
	void undistortPoints(const std::vector<cv::Point> &input_points, std::vector<cv::Point> &output_points);
	void distortPoints(const std::vector<cv::Point> &input_points, std::vector<cv::Point> &output_points);
	float getLenzOffsetZoomCompensation() {return LENS_OFFSET_ZOOM_COMPENSATION;}

	// geometric projection methods
	// projection of circle on the floor
	void perspectiveCircleProjection(cv::Point center, float radius, std::vector<cv::Point> &projected_circle);
	// projection of 3D cylinder on the floor. center is the center of mass of the 3D cylinder
	void perspectiveCylinderProjection(cv::Point center,float cylinder_radius,float cylinder_height,std::vector<cv::Point> &projected_cylinder);
	// projection of point lying on the center axis of the cylinder. center is the center of mass of the 3D cylinder
	cv::Point perspectiveCylinderAxisPointProjection(cv::Point center,float length_multiple, float cylinder_height);
	// back projection of point to world coordinate for post processing
	void perspectiveBackProjection(std::vector<cv::Point2f> &input_list, std::vector<cv::Point3f> &output_list);

	enum CameraParameterIndexOpenCV {
		CP_D1 = 0,	// k1
		CP_D2,		// k2
		CP_D3,		// p1
		CP_D4,		// p2
		CP_D5,		// k3
		CP_D6,		// ??
		CP_FX,
		CP_FY,
		CP_CISW,
		CP_CISH,
		CP_LOZC,
		CP_USF,
		CP_FE,
		CP_SIZE
	};

private:

	// helper for non-fisheye distortion
	cv::Point2f nonFishEyeDistortion(cv::Point2f p);

	// validity of this model
	bool	mValid;

	// image parameter
	const int		mRows;
	const int		mCols;
	// Camera parameters
	std::vector<double> cam_params;

	// instrinsic parameters
	float	fx;
	float	fy;
	float	cx;
	float	cy;
	int		calibImageSize_width;
	int		calibImageSize_height;
	float	LENS_OFFSET_ZOOM_COMPENSATION;
	float	UNDISTORT_SCALE_FACTOR;
	bool	isFishEye;
	double	k1;	// for non-fisheye distortion
	double	k2;
	double	k3;
	double	k4;
	cv::Mat_<float>	mK;	// intrinsic camera matrix
	cv::Mat_<float> mKNew; // matrix taking account the zoom factor
	cv::Mat mD;	// distortion parameters

	// NOTE: For currently perpendicular camera setup, camera height suffices for extrinsic camera calibration.
	//       For more generic extrinsic setup, we need to fully integrate extrinsic parameters. (rotation and translation)
	float	mCameraHeight;

	// For legacy post processing calibration model
	// For legacy calibration model

	// reserved for full camera extrinsic transformation.
	cv::Mat_<float> mX;	// extrinsic camera matrix - to world coordinate frame.

	// For some mysterious reason, estimated person shape doesn't fit real one with camera calib params.
	// The following factor tunes it to fit. This should be chosen emperically.
	float	mZoomCompensationFactor;

	// perspective projection params
	float	mCircleInc;

	// members for camera coverage info
	std::string		mCameraModelName;
	float			mCCDWidth;
	float			mCCDHeight;
	float			mFocalLength;
};

// TODO: Add legacy camera model (Axis212) for backward compatibility.

} // of namespace vml

#endif

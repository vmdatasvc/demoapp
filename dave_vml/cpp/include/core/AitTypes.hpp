/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef __AITTYPES_HPP__
#define __AITTYPES_HPP__

#include <opencv2/core/core.hpp>

// conversion of opencv types to ait compatible types

namespace vml
{

// vectors
typedef	cv::Vec2i	Vector2i;
typedef	cv::Vec3i	Vector3i;
typedef cv::Vec2f	Vector2f;
typedef cv::Vec3f	Vector3f;

// rects
//typedef cv::Rect_<float>	Rectanglef;
//typedef cv::Rect_<int>		Rectanglei;

template <typename _Tp>
class Rectangle:public cv::Rect_<_Tp>
{
public:
	Rectangle();
	Rectangle(_Tp _x0, _Tp _x1, _Tp _y0, _Tp _y1);

	_Tp getx0() const {return x0;}
	_Tp getx1() const {return x1;}
	_Tp gety0() const {return y0;}
	_Tp gety1() const {return y1;}

	void setval(const _Tp _x0,
			const _Tp _x1,
			const _Tp _y0,
			const _Tp _y1);

	void update();

private:
	_Tp x0;
	_Tp x1;
	_Tp y0;
	_Tp y1;
};

template<typename _Tp>
Rectangle<_Tp>::Rectangle():
	x0(this->x),
	x1(this->x+this->width),
	y0(this->y),
	y1(this->y+this->height)
{
}

template<typename _Tp>
Rectangle<_Tp>::Rectangle(_Tp _x0,
		_Tp _x1,
		_Tp _y0,
		_Tp _y1):
		x0(_x0),
		x1(_x1),
		y0(_y0),
		y1(_y1)
{
	update();
}

template <typename _Tp>
inline void Rectangle<_Tp>::setval(const _Tp _x0,
			const _Tp _x1,
			const _Tp _y0,
			const _Tp _y1)
{
	assert(_x1>_x0);
	assert(_y1>_y0);
	x0 = _x0;
	x1 = _x1;
	y0 = _y0;
	y1 = _y1;
	update();
}

template <typename _Tp>
inline void Rectangle<_Tp>::update()
{
	this->x = x0;
	this->y = y0;
	this->height = x1-x0;
	this->width = y1-y0;
}

typedef Rectangle<float>	Rectanglef;
typedef Rectangle<int>		Rectanglei;

} // of namespace vml

#endif  // __AITTYPES_HPP__

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef __XmlParser_hpp__
#define __XmlParser_hpp__


#pragma warning(disable:4786) 

#include <stdio.h>
#include <ctype.h>
#include <expat.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>

// Forward reference to the class XmlParser. This is necessary
// because.  The Handler class uses XmlParser and the XmlParser class uses
// the Handler class.
class XmlParser;


/**
 * This is a base class for all Xml Handler Classes.  The developer is responsible 
 * for implementing a subclass in order to handler their specific Xml messages.
 */
class Handler {
protected:
  XmlParser *parser;

public:

  // Constructors
  Handler (XmlParser* xp = NULL) { parser = xp; }

  // Destructors
  virtual ~Handler() {}
  
  // Setters
  void setParser (XmlParser* xp) { parser = xp; }
        
  /// Called when the parser encounters a start tag
  virtual void start (const std::string& ns,
                      const std::string& tag, const char **attr) = 0;

  /// Called when the parser encounters cdata between a start and end tag
  virtual void cdata (const XML_Char *s, int len) = 0;

  /// Called when the parser encounters an end tag.
  virtual void end (const std::string& ns, const std::string& tag) = 0;

  /// Called when the parser encounters an Xml Processing instruction.
  virtual void processInstruction (const XML_Char *target, const XML_Char *data) {};
};

/**
 * This class wraps the functionality of the expat Xml parser library.
 */
class XmlParser {
  public:
    enum XState {
      x_none = 0,
      x_start = 1,
      x_body,
      x_end
    };

    typedef std::vector<Handler*> HandlerStack;
    typedef std::map<std::string,Handler*> HandlerMap;
    
  protected:
    /// This is a stack of the currently used handlers.  It is used internally.
    HandlerStack activeHandlers;
    /**
     * This is a list of Handler Objects that are used for processing specific Xml tags.
     * This list is here for special processing of tags.  It is usually not used.
     */
    HandlerMap tagHandlers;
    /// This is the main list of Handler Objects.
    HandlerMap nsHandlers;
    /// This is the list of Xml Processing Instruction Handlers.
    HandlerMap piHandlers;
    /// This is a list of special Handler Objects. If there are any spy Handlers present they are used before any other Handler.
    HandlerStack spyHandlers;
    /// This is the default Handler Object.
    Handler *defaultHandler;
    XmlParser *parentParser;
    /// Specifies what state the XmlParser is currently in. it is used internally.
    XState state;
    /// This is the actual expat Xml Parser.
    XML_Parser parser;
    /// Flag to determine if the XmlParser is Ok
    bool ok;
    /// This flag is used to determine if the most recent Xml String was well formed.
    bool wellFormed;

  public:
    // Constructors
    XmlParser ();

    // Destructors
    virtual ~XmlParser();

    /// sets the XmlParser's parent parser
    void setParentParser (XmlParser *pp) { parentParser = pp; }

    /// Returns whether the recent Xml string was well formed
    bool isWellFormed () { return wellFormed; }

    /// Sets the wellFormed flag
    void setWellFormed (bool flag) { wellFormed = flag; }
    
    /// parses the given file
    bool parseFile( const std::string& fname );

    // bool parse( istream& is );

    /// parses the given string
    bool parse( std::string& str );

    /// start is called by the actual expat start callback function
    void start (const std::string& ns, const std::string& tag, const char **attr);

    /// cdata is called by the actual expat cdata callback function
    void cdata (const XML_Char *s, int len);

    /// end is called by the actual expat end callback function
    void end   (const std::string& ns, const std::string& tag);

    /// processInstruction is called by the actual expat process instruction callback function
    void processInstruction (const XML_Char *target, const XML_Char *data);

    /// register the given spy handler
    void registerSpyHandler (Handler* hand);

    /// register the given tag handler
    void registerTagHandler (const std::string& tag, Handler* hand);

    /// register the given processing handler
    void registerProcessingHandler (const std::string& key, Handler* hand);

    /// register the given namespace handler
    void registerNamespaceHandler (const std::string& ns, Handler* hand);

    /// returns the handler associated with the given namespace and tag
    Handler* handlerForElement (const std::string& ns, const std::string& tag);
};

#endif

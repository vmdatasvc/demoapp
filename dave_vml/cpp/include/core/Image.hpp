/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef __IMAGE_HPP__
#define __IMAGE_HPP__

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <limits>
#include <numeric>
#include <stdexcept>
#include <stdio.h>
#include <vector>

// 3rd party includes
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <core/Error.hpp>


namespace vml {

static const float  cMinImageValue  = 1e-5;



template <class T>
inline T vml_log(const T &val, const T min_val = cMinImageValue) {
	if (val > min_val)
		return std::log(val);
	else
		return std::log(min_val);
}

/* ========== */
#ifdef _WIN32
typedef _Longlong int64_t;
#endif

template <class T> class GrayValue;

// NOTE: 4-channel type pixels are for legacy compatibility
template <class T>
class RGBAValue : public cv::Vec<T,4>
{
public:
	//T   r,g,b,a;

	RGBAValue() {}
	RGBAValue(RGBAValue &RGBAValue) :
		cv::Vec<T,4>(RGBAValue.r(), RGBAValue.g(), RGBAValue.b(), RGBAValue.a())
	{
	}
	RGBAValue(const RGBAValue &RGBAValue) :
		cv::Vec<T,4>(RGBAValue.r(), RGBAValue.g(), RGBAValue.b(), RGBAValue.a())
	{
	}
	RGBAValue(T _r, T _g, T _b, T _a) :
		cv::Vec<T,4>(_r, _g, _b, _a)
	{
	}
	RGBAValue(T v) :
		cv::Vec<T,4>(v, v, v, v)
	{
	}
	RGBAValue(T *v) : cv::Vec<T,4>(v[0], v[1], v[2], v[3]) {}

	inline T & r(void) {
		return this->val[0];
	}
	inline T & g(void) {
		return this->val[1];
	}
	inline T & b(void) {
		return this->val[2];
	}
	inline T & a(void) {
		return this->val[3];
	}

	inline T r(void) const {
		return this->val[0];
	}
	inline T g(void) const {
		return this->val[1];
	}
	inline T b(void) const {
		return this->val[2];
	}
	inline T a(void) const {
		return this->val[3];
	}

	inline void operator=(const cv::Vec<T,4> &vec) {
		r() = vec.val[0];
		g() = vec.val[1];
		b() = vec.val[2];
		a() = vec.val[3];
	}

	inline void operator /= (const RGBAValue<T> &p) {
		r() /= p.r();
		g() /= p.g();
		b() /= p.b();
	}

	// cast operator
	inline void operator() (const RGBAValue<T> p)
	{
		r() = T(p.r());
		g() = T(p.g());
		b() = T(p.b());
	}

	inline void set(T _r, T _g, T _b, T _a) {
		r() = _r;
		g() = _g;
		b() = _b;
		a() = _a;
	}
	inline void set(T v) {
		r() = v;
		g() = v;
		b() = v;
	}

	inline void set(const RGBAValue &_Pixel) {
		r() = _Pixel.r();
		g() = _Pixel.g();
		b() = _Pixel.b();
		a() = _Pixel.a();
	};

};	// of class RGBAValue

typedef class RGBAValue<uchar>	RGBAb;

template <typename T> class RGBValue : public cv::Vec<T,3>
{
public:
	//T   r,g,b;

	RGBValue() {}
	RGBValue(RGBValue &RGBValue) :
		cv::Vec<T,3>(RGBValue.r(), RGBValue.g(), RGBValue.b())
	{
	}
	RGBValue(const RGBValue &RGBValue) :
		cv::Vec<T,3>(RGBValue.r(), RGBValue.g(), RGBValue.b())
	{
	}
	RGBValue(T _r, T _g, T _b) :
		cv::Vec<T,3>(_r, _g, _b)
	{
	}
	RGBValue(T v) :
		cv::Vec<T,3>(v, v, v)
	{
	}
	RGBValue(T *v) : cv::Vec<T,3>(v[0], v[1], v[2]) {}
	RGBValue(const GrayValue<T> &gv);

	inline T & r(void) {
		return this->val[0];
	}
	inline T & g(void) {
		return this->val[1];
	}
	inline T & b(void) {
		return this->val[2];
	}

	inline T r(void) const {
		return this->val[0];
	}
	inline T g(void) const {
		return this->val[1];
	}
	inline T b(void) const {
		return this->val[2];
	}

	inline friend RGBValue<T> operator+(const T l, const RGBValue<T> &r) {
		return RGBValue<T>(l + r.b(), l + r.g(), l + r.r(), r.a());
	}
	inline friend RGBValue<T> operator+(const RGBValue<T> &l, const RGBValue<T> &r) {
		return RGBValue<T>(l.b() + r.b(), l.g() + r.g(), l.r() + r.r(), l.a());
	}
	inline friend RGBValue<T> operator+(const RGBValue<T> &l, const T r) {
		return RGBValue<T>(l.b() + r, l.g() + r, l.r() + r, l.a());
	}
	inline void operator += (const T x) {
		r() += x;
		g() += x;
		b() += x;
	}
	inline void operator += (const short x) {
		r() += (T) x;
		g() += (T) x;
		b() += (T) x;
	}
	inline void operator += (const RGBValue<T> &p) {
		r() += p.r();
		g() += p.g();
		b() += p.b();
	}

	inline friend RGBValue<T> operator-(const T l, const RGBValue<T> &r) {
		return RGBValue<T>(l - r.r(), l - r.g(), l - r.b());
	}
	inline friend RGBValue<T> operator-(const RGBValue<T> &l, const RGBValue<T> &r) {
		return RGBValue<T>(l.r() - r.r(), l.g() - r.g(), l.b() - r.b());
	}
	inline friend RGBValue<T> operator-(const RGBValue<T> &l, const T r) {
		return RGBValue<T>(l.r() - r, l.g() - r, l.b() - r);
	}

	inline void operator -= (const T x) {
		r() -= x;
		g() -= x;
		b() -= x;
	}
	inline void operator -= (const RGBValue<T> &p) {
		r() -= p.r();
		g() -= p.g();
		b() -= p.b();
	}

	inline friend RGBValue<T> operator*(const T left, const RGBValue<T> &right) {
		return RGBValue<T>(left*right.r(), left*right.g(), left*right.b());
	}
	inline friend RGBValue<T> operator*(const RGBValue<T> &left, const RGBValue<T> &right) {
		return RGBValue<T>(left.r() * right.r(), left.g() * right.g(), left.b() * right.b());
	}
	inline friend RGBValue<T> operator*(const RGBValue<T> &left, const T right) {
		return RGBValue<T>(left.r() * right, left.g() * right, left.b() * right);
	}
	inline void operator *= (const T x)  {
		r() *= x;
		g() *= x;
		b() *= x;
	}

	inline void operator *= (const RGBValue<T> &p) {
		r() *= p.r();
		g() *= p.g();
		b() *= p.b();
	}

	inline void operator=(const T  x) {
		r() = x;
		g() = x;
		b() = x;
	}
	inline void operator=(const RGBValue<T> &_Pixel) {
		r() = _Pixel.r();
		g() = _Pixel.g();
		b() = _Pixel.b();
	}
	inline void operator=(const cv::Vec3b &vec) {
		r() = vec.val[0];
		g() = vec.val[1];
		b() = vec.val[2];
	}

	inline bool operator>(const RGBValue<T> &p) const {
		return (r()>p.r() && g()>p.g() && b()>p.b());
	}
	inline bool operator<(const RGBValue<T> &p) const {
		return (r()<p.r() && g()<p.g() && b()<p.b());
	}

	inline bool operator>=(const RGBValue<T> &p) const {
		return (r()>=p.r() && g()>=p.g() && b()>=p.b());
	}
	inline bool operator<=(const RGBValue<T> &p) const {
		return (r()<=p.r() && g()<=p.g() && b()<=p.b());
	}

	inline bool operator>(const T &p) const {
		return (r()>p && g()>p && b()>p);
	}
	inline bool operator<(const T &p) const {
		return (r()<p && g()<p && b()<p);
	}

	inline bool operator>=(const T &p) const {
		return (r()>=p && g()>=p && b()>=p);
	}
	inline bool operator<=(const T &p) const {
		return (r()<=p && g()<=p && b()<=p);
	}

	inline bool operator==(const T &p) const {
		return (r() == p && g() == p && b() == p);
	}
	inline bool operator==(const RGBValue<T> &p) const {
		return (r() == p.r() && g() == p.g() && b() == p.b());
	}

	inline friend RGBValue<T> operator/(const T l, const RGBValue<T> &r) {
		return RGBValue<T>(l / r.r(), l / r.g(), l / r.b());
	}
	inline friend RGBValue<T> operator/(const RGBValue<T> &l, const RGBValue<T> &r) {
		return RGBValue<T>(l.r() / r.r(), l.g() / r.g(), l.b() / r.b());
	}
	inline friend RGBValue<T> operator/(const RGBValue<T> &l, const T r) {
		return RGBValue<T>(l.r() / r, l.g() / r, l.b() / r);
	}
	inline friend RGBValue<T> operator/(const RGBValue<T> &l, const int r) {
		return RGBValue<T>(l.r() / r, l.g() / r, l.b() / r);
	}
	inline void operator /= (const int rhs) {
		r() /= rhs;
		g() /= rhs;
		b() /= rhs;
	}
	inline void operator /= (const T x) {
		r() /= x;
		g() /= x;
		b() /= x;
	}
	inline void operator /= (const RGBValue<T> &p) {
		r() /= p.r();
		g() /= p.g();
		b() /= p.b();
	}
	inline void operator /= (const GrayValue<T> &p) {  // for intensity normalization
		r() /= p.v();
		g() /= p.v();
		b() /= p.v();
	}

	// cast operator
	inline void operator() (const RGBValue<T> p)
	{
		r() = T(p.r());
		g() = T(p.g());
		b() = T(p.b());
	}

	inline void clear() {
		r() = 0.0;
		g() = 0.0;
		b() = 0.0;
	};
	inline void zero() {
		r() = 0.0;
		g() = 0.0;
		b() = 0.0;
	};
	inline void set(T _r, T _g, T _b) {
		r() = _r;
		g() = _g;
		b() = _b;
	};
	inline void set(T v) {
		r() = v;
		g() = v;
		b() = v;
	};

	inline void set(const RGBValue<T> &_Pixel) {
		r() = _Pixel.r();
		g() = _Pixel.g();
		b() = _Pixel.b();
	};

	inline void minUpdate(T _r, T _g, T _b)
	{
		r() = (_r < r()) ? _r : r();
		g() = (_g < g()) ? _g : g();
		b() = (_b < b()) ? _b : b();
	}

	inline void absMinUpdate(T _r, T _g, T _b)
	{
		r() = (fabs(_r) < fabs(r())) ? _r : r();
		g() = (fabs(_g) < fabs(g())) ? _g : g();
		b() = (fabs(_b) < fabs(b())) ? _b : b();
	}

	inline void minUpdate(const RGBValue<T> &_Pixel)
	{
		r() = (_Pixel.r() < r()) ? _Pixel.r() : r();
		g() = (_Pixel.g() < g()) ? _Pixel.g() : g();
		b() = (_Pixel.b() < b()) ? _Pixel.b() : b();
	}

	inline void absMinUpdate(const RGBValue<T> &_Pixel)
	{
		r() = (fabs(_Pixel.r()) < fabs(r())) ? _Pixel.r() : r();
		g() = (fabs(_Pixel.g()) < fabs(g())) ? _Pixel.g() : g();
		b() = (fabs(_Pixel.b()) < fabs(b())) ? _Pixel.b() : b();
	}

	inline void maxUpdate(T _r, T _g, T _b)
	{
		r() = (_r > r()) ? _r : r();
		g() = (_g > g()) ? _g : g();
		b() = (_b > b()) ? _b : b();
	}

	inline void maxUpdate(const RGBValue<T> &_Pixel)
	{
		r() = (_Pixel.r() > r()) ? _Pixel.r() : r();
		g() = (_Pixel.g() > g()) ? _Pixel.g() : g();
		b() = (_Pixel.b() > b()) ? _Pixel.b() : b();
	}

	inline void meanUpdate(T _r, T _g, T _b, const unsigned int N)
	{
		r() = (r() * N + _r) / (N + 1.0);
		g() = (g() * N + _g) / (N + 1.0);
		b() = (b() * N + _b) / (N + 1.0);
	}

	inline void meanUpdate(const RGBValue<T> &_Pixel, const unsigned int N)
	{
		r() = (r() * N + _Pixel.r()) / (N + 1.0);
		g() = (g() * N + _Pixel.g()) / (N + 1.0);
		b() = (b() * N + _Pixel.b()) / (N + 1.0);
	}

	inline void meanUpdate(const RGBValue<T> &_Pixel, const double &currWeight,
	                       const double &weightSoFar)
	{
		r() = (r() * weightSoFar + _Pixel.r() * currWeight) / (weightSoFar + currWeight);
		g() = (g() * weightSoFar + _Pixel.g() * currWeight) / (weightSoFar + currWeight);
		b() = (b() * weightSoFar + _Pixel.b() * currWeight) / (weightSoFar + currWeight);
	}

	inline void meanUpdate(const RGBValue<T> &_Pixel, const RGBValue<T> &currWeight,
	                       const RGBValue<T> &weightSoFar)
	{
		r() = (r() * weightSoFar.r() + _Pixel.r() * currWeight.r()) / (weightSoFar.r() + currWeight.r());
		g() = (g() * weightSoFar.g() + _Pixel.g() * currWeight.g()) / (weightSoFar.g() + currWeight.g());
		b() = (b() * weightSoFar.b() + _Pixel.b() * currWeight.b()) / (weightSoFar.b() + currWeight.b());
	}

	// returns true if the this is between the two specified values
	inline bool isBetween(const RGBValue<T> &lowVal, const RGBValue<T> &highVal)
	{
		if ((lowVal.r() > r()) || (r() > highVal.r())) return false;
		if ((lowVal.g() > g()) || (g() > highVal.g())) return false;
		if ((lowVal.b() > b()) || (b() > highVal.b())) return false;
		return true;
	}

	inline T min(void) const
	{
		T m = (r() < g()) ? r() : g();
		return (m < b()) ? m : b();
	}

	inline int minBand(T *val) const // optional T * argument to hold min value
	{
		T m;
		int band;

		if (r() < g())
		{
			m = r();
			band = 0;
		}
		else
		{
			m = g();
			band = 1;
		}

		if (b() < m)
		{
			m = b();
			band = 2;
		}

		if (val != NULL) *val = m;

		return band;
	}

	inline T max(void) const
	{
		T m = (r() > g()) ? r() : g();
		return (m > b()) ? m : b();
	}

	inline int maxBand(T *val) const // optional T * argument to hold max value
	{
		T m;
		int band;

		if (r() > g())
		{
			m = r();
			band = 0;
		}
		else
		{
			m = g();
			band = 1;
		}

		if (b() > m)
		{
			m = b();
			band = 2;
		}

		if (val != NULL) *val = m;

		return band;
	}

	inline float length(void) const
	{
		float _r=(float)r(),_g=(float)g(), _b=(float)b();
		float   sum = (_r * _r + _g * _g + _b * _b);
		return (sum > 0.0 ? std::sqrt(sum) : 0.0);
	}

	inline float lengthSquared(void) const
	{
		float _r=(float)r(),_g=(float)g(), _b=(float)b();
		float   sum = (_r * _r + _g * _g + _b * _b);
		return (sum > 0.0 ? sum : 0.0);
	}

	inline T intensity(void) const {
		unsigned int mean;
		mean = (r()+g()+b())/3;
		return (T)(mean>255?255:mean);
	}

	inline unsigned int intensityWeighted(const RGBValue<T> &_PixelWeights) const {
		return (_PixelWeights.r()*r() + _PixelWeights.g()*g() + _PixelWeights.b()*b());
	}

	inline T normalize(void)
	{
		float length = this->length();
		if (length != 0.0) {
			r() = (T)((float)r()/length);
			g() = (T)((float)g()/length);
			b() = (T)((float)b()/length);
		}
		return length;
	}

	inline void rg_chrom(void)
	{
		// this is not type-safe, cause rg_chrom is only meaningful for float type image.
		// user should be careful not to call this on non-float image
		float intensity = this->intensity();
		r() /= intensity;
		g() /= intensity;
		// b() is not calculated cause shouldn't be used.
	}

	// following methods are mainly for float type pixels
	inline void setlog(const float min_val = cMinImageValue)
	{
		r() = vml_log(r(),min_val);
		g() = vml_log(g(),min_val);
		b() = vml_log(b(),min_val);
	}

	inline void setexp(void)
	{
		r() = expLUT(r());
		g() = expLUT(g());
		b() = expLUT(b());
	}

	inline void setpow(float x)
	{
		r() = powLUT(r(), x);
		g() = powLUT(g(), x);
		b() = powLUT(b(), x);
	}

	inline void setabs() {
		r() = std::abs(r());
		g() = std::abs(g());
		b() = std::abs(b());
	}

	inline void setinv() {
		r() = (r() == 0.f)? 0.f : 1.f/r();
		g() = (g() == 0.f)? 0.f : 1.f/g();
		b() = (b() == 0.f)? 0.f : 1.f/b();
	}

	inline RGBValue<T> abs(void) const {
		RGBValue<T> p(*this);
		p.setabs();
		return p;
	}

	inline void setsign() {
		r() = (r() > 0) ? 1.0 : -1.0;
		g() = (g() > 0) ? 1.0 : -1.0;
		b() = (b() > 0) ? 1.0 : -1.0;
	}

	inline RGBValue<T> sign(void) const {
		RGBValue<T> p(*this);
		p.setsign();
		return p;
	}


	inline float lorentzian(T sigma) const {
		float sum = 0.f;
		float _r=(float)r(), _g=(float)g(), _b=(float)b();
		sum += log(1+_r*_r/(2*sigma*sigma));
		sum += log(1+_g*_g/(2*sigma*sigma));
		sum += log(1+_b*_b/(2*sigma*sigma));
		return sum/3.0f;
	}

	static	const	int		nbands = 3;
};	// of class RGBValue

template <class T> inline std::ostream &operator<<(std::ostream &os, const RGBValue<T> &right) {
	os << std::setprecision(6) << std::setw(10) << right.r() << " ";
	os << std::setprecision(6) << std::setw(10) << right.g() << " ";
	os << std::setprecision(6) << std::setw(10) << right.b() << " ";
	return os;
}

// Euclidean distance between _Pixel values
// code from TypeC.cpp
template <class T> float EuclideanDistanceSquared(const RGBValue<T> &A, const RGBValue<T> &B)
{
	// RGB Euclidean distance squared
	RGBValue<T> delta(A.r()-B.r(),A.g()-B.g(),A.b()-B.b());
	return((delta.r()*delta.r()+delta.g()*delta.g()+delta.b()*delta.b())/3.f);
}

template <class T> float EuclideanDistanceSquaredLog(const RGBValue<T> &A, const RGBValue<T> &B)
{
	// RGB Euclidean distance squared
	RGBValue<T> delta(vml_log(A.r()) - vml_log(B.r()), vml_log(A.g()) - vml_log(B.g()) , vml_log(A.b()) - vml_log(B.b()) );
	return((delta.r()*delta.r()+delta.g()*delta.g()+delta.b()*delta.b())/3.f);
}

// Compute distance between a line and a point
template <class T> float distancePointToLineRGB(const RGBValue<T> &pointLine1, const RGBValue<T> &pointLine2, const RGBValue<T> &point)
{
	RGBValue<T> vec_a = point - pointLine1;
	RGBValue<T> vec_b_unit = pointLine2 - pointLine1;
	vec_b_unit /= (float) cv::norm(vec_b_unit);

	float vec_cos = vec_a.dot(vec_b_unit);
	return sqrt( std::max(0.0f,vec_a.lengthSquared() - vec_cos*vec_cos) );
}

// real pixel value definitions
typedef class RGBValue<uchar> RGBb;
typedef class RGBValue<ushort> RGBs;
typedef class RGBValue<float> RGBf;
typedef class RGBValue<double> RGBd;

} // of namespace vml

// Make OpenCV understand RGBValue's depth/channels so we can
// call it's functions directly on TImages without a cast.
namespace cv {

	template<typename T>
	class DataType<vml::RGBValue<T> >
	{
	public:
		typedef cv::Vec<T,3> value_type;
		typedef cv::Vec<typename DataType<T>::work_type, 3> work_type;
		typedef T channel_type;
		enum { depth = DataDepth<channel_type>::value, channels = 3,
			   fmt = ((channels-1)<<8) + DataDepth<channel_type>::fmt,
			   type = CV_MAKETYPE(depth, channels)
			 };
	};

}

namespace vml {

template <typename T> class GrayValue : public cv::Vec<T, 1>
{
public:
//  T v;

	inline GrayValue() {}
	inline GrayValue(RGBValue<T> &rgb) {
		v() = rgb.intensity();
	}
	inline GrayValue(const RGBValue<T> &rgb) {
		v() = rgb.intensity();
	}
	inline GrayValue(T r, T g, T b) {
		v() = (r + g + b) / 3.0;
	}
	inline GrayValue(T _v) {
		v() = _v;
	}
	inline GrayValue(T *vin) {
		v() = vin[0];
	}

	inline T & v( void ) {
		return this->val[0];
	}
	inline T v(void) const {
		return this->val[0];
	}

	inline friend GrayValue<T> operator+(const float l, const GrayValue<T> &r)
	{
		return GrayValue<T>(l + r.v());
	}
	inline friend GrayValue<T> operator+(const GrayValue<T> &l, const GrayValue<T> &r)
	{
		return GrayValue<T>(l.v() + r.v());
	}
	inline friend GrayValue<T> operator+(const GrayValue<T> &l, const float r)
	{
		return GrayValue<T>(l.v() + r);
	}
	inline void operator += (const float x)
	{
		v() += x;
	}
	inline void operator += (const double x)
	{
		v() += (float) x;
	}
	inline void operator += (const GrayValue<T> &p)
	{
		v() += p.v();
	}

	inline friend GrayValue<T> operator-(const float l, const GrayValue<T> &r)
	{
		return GrayValue<T>(l - r.v());
	}
	inline friend GrayValue<T> operator-(const GrayValue<T> &l, const GrayValue<T> &r)
	{
		return GrayValue<T>(l.v() - r.v());
	}
	inline friend GrayValue<T> operator-(const GrayValue<T> &l, const float r)
	{
		return GrayValue<T>(l.v() - r);
	}
	inline void operator -= (const float x)
	{
		v() -= x;
	}
	inline void operator -= (const GrayValue<T> &gv)
	{
		v() -= gv.v();
	}

	inline friend GrayValue<T> operator*(const float left, const GrayValue<T> &right)
	{
		return GrayValue<T>(left * right.v());
	}
	inline friend GrayValue<T> operator*(const GrayValue<T> &left, const GrayValue<T> &right)
	{
		return GrayValue<T>(left.v() * right.v());
	}
	inline friend GrayValue<T> operator*(const GrayValue<T> &left, const float right)
	{
		return GrayValue<T>(left.v() * right);
	}
	inline friend GrayValue<T> operator*(const GrayValue<T> &left, const double right)
	{
		return GrayValue<T>(left.v() * right);
	}
	inline void operator *= (const float x)
	{
		v() *= x;
	}
	inline void operator *= (const double x)
	{
		v() *= x;
	}
	inline void operator *= (const GrayValue<T> &gv)
	{
		v() *= gv.v();
	}
//  void operator *= (const ImageAccessor &p);

	inline void operator=(const T x) {
		v() = x;
	}
//	inline void operator=(const float  x) {
//		v() = x;
//	}
//	inline void operator=(const double x) {
//		v() = x;
//	}
//
	inline bool operator>(const GrayValue<T> &p) const {
		return (v() > p.v());
	}
	inline bool operator<(const GrayValue<T> &p) const {
		return (v() < p.v());
	}

	inline bool operator>=(const GrayValue<T> &p) const {
		return (v() >= p.v());
	}
	inline bool operator<=(const GrayValue<T> &p) const {
		return (v() <= p.v());
	}

	inline bool operator>(const float &p) const {
		return (v() > p);
	}
	inline bool operator<(const float &p) const {
		return (v() < p);
	}

	inline bool operator>=(const float &p) const {
		return (v() >= p);
	}
	inline bool operator<=(const float &p) const {
		return (v() <= p);
	}

	inline bool operator==(const float &p) const {
		return (v() == p);
	}
	inline bool operator==(const GrayValue<T> &p) const {
		return (v() == p.v());
	}

	inline friend GrayValue<T> operator/(const float l, const GrayValue<T> &r)
	{
		return GrayValue<T>(l / r.v());
	}
	inline friend GrayValue<T> operator/(const GrayValue<T> &l, const GrayValue<T> &r)
	{
		return GrayValue<T>(l.v() / r.v());
	}
	inline friend GrayValue<T> operator/(const GrayValue<T> &l, const float r)
	{
		return GrayValue<T>(l.v() / r);
	}
	inline void operator /= (const float x)
	{
		v() /= x;
	}
	inline void operator /= (const double x)
	{
		v() /= x;
	}
	inline void operator /= (const GrayValue<T> &gv)
	{
		v() /= gv.v();
	}
//  void operator /= (const ImageAccessor &p);

	// cast operator
	inline void operator() (const GrayValue<T> p)
	{
		v() = T(p.v());
	}

	inline void clear() {
		v() = 0.0;
	};
	inline void zero() {
		v() = 0.0;
	};
	inline void set(const GrayValue<T> &gv) {
		v() = gv.v();
	}
	inline void set(float _r, float _g, float _b) {
		v() = (_r + _g + _b) / 3.0;
	}
	inline void set(float _v) {
		v() = _v;
	};

	inline float maxBand() const {
		return v();
	}

	inline void minUpdate(GrayValue<T> &gv)
	{
		v() = (gv.v() < v()) ? gv.v() : v();
	}

	inline void absMinUpdate(GrayValue<T> &gv)
	{
		v() = (fabs(gv.v()) < fabs(v())) ? gv.v() : v();
	}

	inline void minUpdate(const GrayValue<T> &gv)
	{
		v() = (gv.v() < v()) ? gv.v() : v();
	}

	inline void absMinUpdate(const GrayValue<T> &gv)
	{
		v() = (fabs(gv.v()) < fabs(v())) ? gv.v() : v();
	}

	inline void maxUpdate(GrayValue<T> &gv)
	{
		v() = (gv.v() > v()) ? gv.v() : v();
	}

	inline void maxUpdate(const GrayValue<T> &gv)
	{
		v() = (gv.v() > v()) ? gv.v() : v();
	}

	inline void meanUpdate(const GrayValue<T> &gv, const unsigned int N)
	{
		v() = (v() * N + gv.v()) / (N + 1.0);
	}

	inline void meanUpdate(float _v, const unsigned int N)
	{
		v() = (v() * N + _v) / (N + 1.0);
	}

	inline void meanUpdate(const GrayValue<T> &gv, const double &currWeight,
	                       const double &weightSoFar)
	{
		v() = (v() * weightSoFar + gv.v() * currWeight) / (weightSoFar + currWeight);
	}

	inline void meanUpdate(const GrayValue<T> &gv, const GrayValue<T> &currWeight,
	                       const GrayValue<T> &weightSoFar)
	{
		v() = (v() * weightSoFar.v() + gv.v() * currWeight.v()) / (weightSoFar.v() + currWeight.v());
	}

	// returns true if the this is between the two specified values
	inline bool isBetween(const GrayValue<T> &lowVal, const GrayValue<T> &highVal)
	{
		return ((lowVal.v() < v()) && (v() < highVal.v()));
	}

	float min(void) const {
		return v();
	}
	int minBand(float *val) const // optional float * argument to hold min value
	{
		if (val != NULL) *val = v();
		return 0; // always the first and only band
	}

	float max(void) const {
		return v();
	}
	int maxBand(float *val) const // optional float * argument to hold max value
	{
		if (val != NULL) *val = v();
		return 0; // always the first and only band
	}

	inline float length(void) const
	{
		return fabs(v());

		// I think this is a bug: why would the length of a scalar value
		// be its square root?
		//return (v > 0.0 ? std::sqrt(v) : 0.0);

		// I think this is also a bug: the length of a scalar value must be nonnegative,
		// hence it should be absolute value of itself
	}

	inline float lengthSquared(void) const
	{
		return v()*v();
	}

	inline float intensity(const bool isLog = false) const {
		// GrayValue<T> does not need to check if the image is in log space
		// but to make this template compatible with TImageAccessor template
		// let it take bool argument and do nothing
		return v();
	}

	inline float intensityWeighted(const RGBValue<T> & rgbWeights) const {
		return v()*(rgbWeights.r()+rgbWeights.g()+rgbWeights.b());
	}

	inline void cartesianToPolar(GrayValue<T> &gv)
	{
		float dx=v();
		float dy=gv.v();
		float tempDx=dx*dx;
		float tempDy=dy*dy;
		v()=sqrt(tempDx+tempDy);


		if (dx==0 && dy==0)
			gv.v()=0;
		else
			gv.v()=atan2f(dy,dx);

	}

	inline void normalize(void)
	{
		v() = T(1);
	}

	inline void setlog(const float min_val = cMinImageValue)
	{
		v() = vml_log(v(),min_val);
	}

	inline void setexp(void)
	{
		v() = expLUT(v());
	}

	inline void setpow(float x)
	{
		v() = powLUT(v(), x);
	}

	inline void setabs() {
		v() = std::abs(v());
	}

	inline void setinv() {
		v() = (v() == 0.f) ? 0.f : 1.0f/v();
	}

	inline GrayValue<T> abs(void) const {
		GrayValue<T> p(*this);
		p.setabs();
		return p;
	}

	inline void setsign() {
		v() = (v() > 0) ? 1.0 : -1.0;
	}

	inline GrayValue<T> sign(void) const {
		GrayValue<T> p(*this);
		p.setsign();
		return p;
	}

	// grayscale saturation should not be defined but
	// this function is needed for PIXELTYPE templates
	// grayscale saturation should be 0
	inline T saturation(void) const {
		return T(0);
	}

	inline float lorentzian(float sigma) const {
		return log(1+this->val[0]*this->val[0]/(2*sigma*sigma));
	}

	inline void rbf(const GrayValue<T> mean,const float sigma) {
		float d = this->val[0]-mean.val[0];
		this->val[0] = expf(d*d/(-2.f*sigma));
	}

	static const int	nbands=1;
};	// of GrayValue

template <class T> inline std::ostream &operator<<(std::ostream &os, const GrayValue<T> &right) {
	os << std::setprecision(6) << std::setw(10) << right.v();
	return (os);
}

template <class T> float EuclideanDistanceSquared(const GrayValue<T> &A,const GrayValue<T> &B)
{
	float   delta = A.v()-B.v();
	return(delta*delta);
}

template <class T> float EuclideanDistanceSquaredLog(const GrayValue<T> &A,const GrayValue<T> &B)
{
	float   delta = vml_log(A.v()) - vml_log(B.v());
	return(delta*delta);
}

// real gray value definitions
typedef class GrayValue<uchar> Grayb;
typedef class GrayValue<ushort> Grays;
typedef class GrayValue<float> Grayf;
typedef class GrayValue<double> Grayd;

} // of namespace vml

// Make OpenCV understand GrayValue's depth/channels so we can
// call it's functions directly on our GrayscaleImages without a cast.
namespace cv {

	template<typename T>
	class DataType<vml::GrayValue<T> >
	{
	public:
		//typedef vml::GrayValue value_type;
		typedef cv::Vec<T,1> value_type;
		typedef cv::Vec<typename DataType<T>::work_type, 1> work_type;
		typedef T channel_type;
		enum { depth = DataDepth<channel_type>::value, channels = 1,
		       fmt = ((channels-1)<<8) + DataDepth<channel_type>::fmt,
		       type = CV_MAKETYPE(depth, channels)
		     };
	};

}


namespace vml {

// TImage definitions
#define TDATA   ((T *) this->data)
#ifndef FDATA
#define FDATA   ((float *) this->data)
#endif

template <class T> class TImage : public cv::Mat_<T>
{
private:

	// prevent copying
	//TImage(const TImage &src);

public:
	/********** constructor/deconstructor functions **********/
	// with no arguments, blank image
	TImage() {}

	// allocates an image with data space
	TImage(unsigned trows, unsigned tcols)
		: cv::Mat_<T>(trows, tcols)
	{
		//this-> allocate(trows, tcols);
	}

	TImage(unsigned trows, unsigned tcols, T *_data)
		: cv::Mat_<T>(trows, tcols, _data)
	{
		//sizecache = trows*tcols;
	}

	// The following constructor and operator() support opencv-style
	// cropping / ROI-extraction.
	TImage(const TImage<T> &image, const cv::Rect &roi,
	       const bool copyData = false)
		: cv::Mat_<T>(image, roi)
	{
		if(copyData) {
			TImage<T> tempImg(this->rows, this->cols);
			this->copyTo(tempImg);
			*this = tempImg;
		}
	}
	inline TImage<T> operator()(const cv::Rect& roi,
	                            const bool copyData = false ) const
	{
		return TImage<T>(*this, roi, copyData);
	}

	// deconstructor, frees all data
	virtual ~TImage() {}

	inline bool isRGB(void) const {return (T::channels==3);}
	inline int nbands(void) const {return (T::channels);}

	// a text descriptor for the image
	char descriptor[256];

	// extra duplicate accessors that are a little more readable (and signed)
	inline int64_t width(void)      const {
		return (int64_t) this->cols;
	}
	inline int64_t height(void)     const {
		return (int64_t) this->rows;
	}
	inline int64_t pixelCount(void) const {
		return (int64_t) (this->rows * this->cols);
	}

	// returns reference to a single data value given index/band
	inline T &get (const unsigned index) {
		return TDATA[index];
	}
	inline T &get (const unsigned index) const {
		return TDATA[index];
	}
	inline T &operator() (const unsigned index) {
		return(TDATA[index]);
	}

	// returns reference to a single data value given row/col/band
	inline T &get (const unsigned row, const unsigned col) {
		return TDATA[row * this->cols + col];
	}
	inline T &get (const unsigned row, const unsigned col) const {
		return TDATA[row * this->cols + col];
	}
	inline T &operator() (const unsigned row, const unsigned col) {
		return TDATA[row * this->cols + col];
	}
	inline T operator() (const unsigned row, const unsigned col) const {
		return TDATA[row * this->cols + col];
	}

	// returns reference to a single data value given Point2D/band
	inline T &get (const cv::Point2i &loc) {
		return TDATA[loc.y * this->cols + loc.x];
	}
	inline T &operator() (const cv::Point2i &loc) {
		return TDATA[loc.y * this->cols + loc.x];
	}

	// Bilinear interpolation
	T interp(const float ri, const float ci, const float outOfBounds = -1.f) const
	{
		T interpVal(outOfBounds);

		if(ri >= 0.f && ci >= 0.f && ri < float(this->rows) && ci < float(this->cols))
		{
			const unsigned int c0 = std::floor(ci);
			const unsigned int r0 = std::floor(ri);

			const float dr = ri - float(r0);
			const float dc = ci - float(c0);

			// Start with upper-left point:
			interpVal = this->get(r0, c0);
			interpVal *= (1.0 - dr)*(1.0 - dc);

			// Add point below:
			T neighborVal( this->get(r0 + 1, c0) );
			neighborVal *= dr*(1.0 - dc);
			interpVal += neighborVal;

			// Add point to th right:
			neighborVal = this->get(r0, c0 + 1);
			neighborVal *= dc*(1.0 - dr);
			interpVal += neighborVal;

			// Add point below and to the right:
			neighborVal = this->get(r0 + 1, c0 + 1);
			neighborVal *= dr*dc;
			interpVal += neighborVal;

		}

		return interpVal;
	}

	/* Ambiguous with operator() (unsigned, unsigned)
	inline T operator() (float xi, float yi, float outOfBounds = -1.f) const {
	    return this->interp(xi, yi, outOfBounds);
	}
	 */


	inline T min(void) const
	{
		VML_ASSERT(this->isContinuous());
		T  minvalue(std::numeric_limits<float>::max());
		T *data = (T *) this->data;

		for (int i=0; i<pixelCount(); i++)
			minvalue.minUpdate(data[i]);

		return minvalue;
	}

	inline void min(TImage<T> &otherImg)
	{
		VML_ASSERT(this->isContinuous() && otherImg.isContinuous());
		T *thisData  = (T *) this->data;
		T *otherData = (T *) otherImg.data;

		for (int i=0; i<pixelCount(); i++)
		{
			if (otherData[i] < thisData[i])
				thisData[i] = otherData[i];
		}
		return;
	}

	inline void max(TImage<T> &otherImg)
	{
		VML_ASSERT(this->isContinuous() && otherImg.isContinuous());
		T *thisData  = (T *) this->data;
		T *otherData = (T *) otherImg.data;

		for (int i=0; i<pixelCount(); i++)
		{
			if (otherData[i] > thisData[i])
				thisData[i] = otherData[i];
		}
		return;
	}

	// Replace any pixels greater than val with val
	inline void minUpdate(T val)
	{
		VML_ASSERT(this->isContinuous());
		T *data = (T *) this->data;

		for (int i=0; i<pixelCount(); i++)
			data[i].minUpdate(val);
	}

	// Replace any pixels less than val with val
	inline void maxUpdate(T val)
	{
		VML_ASSERT(this->isContinuous());
		T *data = (T *) this->data;

		for (int i=0; i<pixelCount(); i++)
			data[i].maxUpdate(val);
	}


	inline T max(void) const
	{
		VML_ASSERT(this->isContinuous());
		T  maxvalue(-std::numeric_limits<float>::max());
		T *data = (T *) this->data;

		for (int i=0; i<pixelCount(); i++)
			maxvalue.maxUpdate(data[i]);

		return maxvalue;
	}


	// Find the specified (fractional) percentile value, using optional pre-allocated buffer
	// (Pre-allocated buffer must be large enough to hold original image's pixels)
	float percentile(const float frac, float *user_buffer=NULL) const
	{

		// Special case: if p==1.0, just use max:
		if (frac==1.0f)
			return this->max().max();

		float *buffer;
		bool free_buffer = false;
		int64_t numData = this->pixelCount()*this->channels();

		if (!user_buffer) {
			buffer = new float[numData];
			free_buffer = true;
		} else {
			buffer = user_buffer;
		}
		memcpy(buffer, this->data, numData*sizeof(float));

		float *perc = buffer + std::min(numData-1, (int64_t)(ceil(frac*numData)));

		std::nth_element(buffer, perc, buffer+numData);

		float returnVal = *perc;

		if (free_buffer)
			delete[] buffer;

		return returnVal;
	}

	inline float percentile(const float frac, const cv::Mat_<uchar> &mask,
	                        float *user_buffer = NULL) const
	{
		return maskedPercentile(frac, mask, user_buffer);
	}

	float maskedPercentile(const float frac, const cv::Mat_<uchar> &mask,
	                       float *user_buffer=NULL) const
	{
		VML_ASSERT(mask.size() == this->size());

		float *buffer;
		bool free_buffer = false;
		if (!user_buffer) {
			buffer = new float[this->pixelCount()*this->channels()];
			free_buffer = true;
		} else {
			buffer = user_buffer;
		}

		int64_t numData = 0;
		int nrows = mask.rows, ncols = mask.cols;
		if (mask.isContinuous() && this->isContinuous()) {
			nrows = 1;
			ncols *= mask.rows;
		}
		for (int i=0; i<nrows; i++) {
			const uchar *mask_i = mask.ptr(i);
			T *this_i = (T *) this->ptr(i);
			for (int j=0; j<ncols; j++) {
				if (mask_i[j]) {
					((T *)buffer)[numData++] = this_i[j];
				}
			}
		}

		numData *= this->channels();

		float *perc = buffer + std::min(numData-1, (int64_t)(ceil(frac*numData)));

		std::nth_element(buffer, perc, buffer+numData);

		float returnVal = *perc;

		if (free_buffer)
			delete[] buffer;

		return returnVal;
	}

	/**  Support vector of percentile requests?
	void percentile(std::vector<float> &frac_list,
	                std::vector<float> &value_list,
	                T *user_buffer=NULL)
	{
	    std::vector<float>::iterator frac;
	    std::vector<float>::iterator value;

	    for(frac = frac_list.begin(), value = value_list.begin();
	        frac != frac_list.end(); frac++, value++)
	    {
	        *value = this->percentile(*frac, user_buffer);
	    }
	}
	 */

	void histStretch(float low, float high, float *user_buffer=NULL)
	{
		float low_value, high_value;
		low_value = this->percentile(low,  user_buffer);
		if (high < 1.0f)
			high_value = this->percentile(high, user_buffer);

		*this -= low_value;

		if (high < 1.0f)
		{
			float denom = high_value - low_value;
			if (denom>0.0f)
				*this /= denom;
		}

		return;
	}

	void histStretch(float low, float high, cv::Mat_<uchar> &mask,
	                 float *user_buffer=NULL)
	{
		float low_value, high_value;

		low_value = this->maskedPercentile(low, mask, user_buffer);
		if (high < 1.0f)
			high_value = this->maskedPercentile(high, mask, user_buffer);

		*this -= low_value;

		if (high < 1.0f)
		{
			float denom = high_value - low_value;
			if (denom>0.0f)
				*this /= denom;
		}

		return;
	}


	// returns the average intensity value of the whole image
	// no log check. it's user's responsibility to unlog the image before average
	T avgPixelValue(const bool isLog = false) const
	{
		VML_ASSERT(this->isContinuous());

		double    sum = 0.f;
		int64_t pixels = pixelCount();
		T    *pixel_data = TDATA;

		for (int i = 0; i < pixels; i ++)
		{
			sum += (double)*pixel_data;
			pixel_data ++;
		}

		sum /= (double) pixels;

		return T(sum);
	}

	T avgPixelValue(const cv::Mat_<uchar> &mask) const
	{
		VML_ASSERT(mask.rows == this->rows &&
		           mask.cols == this->cols);

		double sum;
		int64_t numData = 0;

		int nrows = mask.rows, ncols = mask.cols;
		if (mask.isContinuous() && this->isContinuous()) {
			nrows = 1;
			ncols *= mask.rows;
		}
		for (int i=0; i<nrows; i++) {
			const uchar *mask_i = mask.ptr(i);
			T *this_i = (T *) this->ptr(i);
			for (int j=0; j<ncols; j++) {
				if (mask_i[j]) {
					sum += (double)this_i[j];
					numData++;
				}
			}
		}

		if(numData==0)
			return T(-1.f);
		else
			return T(sum / (double) numData);
	}

	T avgPixelValue (cv::Rect bounds) const
	{
		double   sum(0.0);
		const int boundWidth = bounds.width;

		for (int row = bounds.y; row < bounds.x; row ++)
		{
			T *rowPtr = &this->get(row, bounds.x);

			for (int col = 0; col < boundWidth ; col ++)
			{
				sum += (double)*rowPtr;
				rowPtr ++;
			}
		}

		sum /= (double) bounds.area();

		return T(sum);
	}

	template<typename T2>
	TImage<T> &operator = (T2 val)
	{
		for (int i=0; i<pixelCount(); i++)
			TDATA[i] = val;

		return *this;
	}

	// divide one image by another
	TImage<T> &operator /= (const TImage<T> &other)
	{
		VML_ASSERT(this->isContinuous());
		VML_ASSERT(other.size() == this->size());

		for (int i=0; i<pixelCount(); i++)
			TDATA[i] /= ((T*)other.data)[i];

		return *this;
	}

	// likewise multiply one image by another
	TImage<T> &operator *= (const TImage<T> &other)
	{
		VML_ASSERT(this->isContinuous());
		VML_ASSERT(other.size() == this->size());

		for (int i=0; i<pixelCount(); i++)
			TDATA[i] *= ((T*)other.data)[i];

		return *this;
	}

	template<typename _T>
	void grayscalePointwiseImageProduct (const TImage<GrayValue<_T> > &intensityImg)
	{
		VML_ASSERT(this->isContinuous());
		VML_ASSERT(intensityImg.size() == this->size());

		T *image_data = (T *) this->data;
		GrayValue<_T> *orig_data = (GrayValue<_T> *) intensityImg.data;

		for (int i=0; i<this->pixelCount(); i++)
		{
			*image_data++ *= *orig_data++;
		}
	}

	template<typename _T>
	void grayscalePointwiseImageDivide (const TImage<GrayValue<_T> > &intensityImg)
	{
		VML_ASSERT(this->isContinuous());
		VML_ASSERT(intensityImg.size() == this->size());

		T *image_data = (T *) this->data;
		GrayValue<_T> *orig_data = (GrayValue<_T> *) intensityImg.data;

		for (int i=0; i<this->pixelCount(); i++)
		{
			*image_data++ /= *orig_data++;
		}
	}

	/*
	TImage<T> & grayscalePointwiseImageDivide (const GrayscaleImage &other)
	{
		VML_ASSERT(this->isContinuous());
		VML_ASSERT(other.size() == this->size());

		for (int i=0; i<pixelCount(); i++)
			TDATA[i] /= ((float*)other.data)[i];

		return *this;
	}
*/

	// likewise, add one image by another
	TImage<T> &operator += (const TImage<T> &other)
	{
		VML_ASSERT(this->isContinuous());
		VML_ASSERT(other.size() == this->size());

		for (int i=0; i<pixelCount(); i++)
			TDATA[i] += ((T*)other.data)[i];

		return *this;
	}

	// likewise, subtract one image by another
	TImage<T> &operator -= (const TImage<T> &other)
	{
		VML_ASSERT(this->isContinuous());
		VML_ASSERT(other.size() == this->size());

		for (int i=0; i<pixelCount(); i++)
			TDATA[i] -= ((T*)other.data)[i];

		return *this;
	}

	// returns an ImageAccessor to an individual pixel given an index
	//  inline ImageAccessor get(unsigned index) const {
	//      ImageAccessor ia(*this, index);
	//      return(ia);
	//  }

	// returns an ImageAccessor to an individual pixel given a row/col
	//  ImageAccessor get(unsigned row, unsigned col) const {
	//      ImageAccessor ia(*this, row, col);
	//      return(ia);
	//  }

	// returns an ImageAccessor to an individual pixel given a Point2D
	//  ImageAccessor get(const Point2Di &loc) const {
	//      ImageAccessor ia(*this, loc);
	//      return(ia);
	//  }

	// fills the pixel with the data
	//  Pixel &get(unsigned index, Pixel &pix) const;
	//  Pixel &get(unsigned row, unsigned col, Pixel &pix) const;
	//  Pixel &get(const Point2Di &loc,  Pixel &pix) const;

	// grabs vectors of Pixels
	//  void get(unsigned rowStart, unsigned colStart, unsigned dRow, unsigned dCol, std::vector<Pixel> &pix) const;
	//  void get(const Point2Di &anchor, int dRow, int dCol, std::vector<Pixel> &pix) const;
	//  void get(const Box2D<int> &box, std::vector<Pixel> &pix) const;

	// creates vectors of ImageAccessors
	//  void get(unsigned rowStart, unsigned colStart, unsigned dRow, unsigned dCol, std::vector<ImageAccessor> &a) const;
	//  void get(Point2Di anchor, int dRow, int dCol, std::vector<ImageAccessor> &a) const;
	//  void get(const Box2D<int> &box, std::vector<ImageAccessor> &a) const;

	// fills the pixel with the data using the specified channels to compress the data
	//  Pixel &get(unsigned index, Pixel &pix, const ChannelList &channel) const;
	//  Pixel &get(unsigned row, unsigned col, Pixel &pix, const ChannelList &channel) const;
	//  Pixel &get(const Point2Di &loc, Pixel &pix, const ChannelList &channel) const;

	// grabs vectors of Pixels using channels
	//  void get(unsigned rowStart, unsigned colStart, unsigned dRow, unsigned dCol, std::vector<Pixel> &pix, const ChannelList &channel) const;
	//  void get(const Point2Di &anchor, int dRow, int dCol, std::vector<Pixel> &pix, const ChannelList &channel) const;
	//  void get(const Box2D<int> &box, std::vector<Pixel> &pix, const ChannelList &channel) const;

	// grabs the specified band of data
	//  template<class T>
	//  void get(Map2D<T> &map, unsigned band) const;

	// grabs the specified channel of data
	//  template<class T>
	//  void get(Map2D<T> &map, Channel &channel) const;

	// fills the standard container with pixel data
	// input iterators should be containers of Point2Di
	// data container should be a container of things with a [] operator that accept floats
//	template<typename Index_Iterator, typename Data_Iterator>
//	void get_iterator(Index_Iterator start, Index_Iterator end, Data_Iterator pix) const;
//
//	// same thing, but with a channel argument
//	template<typename Index_Iterator, typename Data_Iterator>
//	void get_iterator(Index_Iterator start, Index_Iterator end, Data_Iterator pix, std::vector<Channel> &channel) const;

	// same thing, but the data gets dumped to a 1-row image
	// give a "squarify function" or have write a write function that squares the data

	// gets an interpolated value doing bilinear interpolation
	inline float getInterpolate(float y, float x, int band) const
	{
		VML_ASSERT(band < this->channels());

		// there will be four pixels from which we take weighted averages
		int c1 = int(x);
		int r1 = int(y);
		int c2 = int(ceil(x));
		int r2 = int(ceil(y));

		// make sure everything is in bounds
		c1 = std::max(0, std::min(c1, int(this->cols-1)));
		c2 = std::max(0, std::min(c2, int(this->cols-1)));
		r1 = std::max(0, std::min(r1, int(this->rows-1)));
		r2 = std::max(0, std::min(r2, int(this->rows-1)));

//		TImageAccessor<T>   r1c1(*this, r1, c1);
//		TImageAccessor<T>   r1c2(*this, r1, c2);
//		TImageAccessor<T>   r2c1(*this, r2, c1);
//		TImageAccessor<T>   r2c2(*this, r2, c2);

		float fracX = x - int(x);
		float fracY = y - int(y);

//		float interp1 = (1.0 - fracX) * r1c1[band] + fracX * r1c2[band];
//		float interp2 = (1.0 - fracX) * r2c1[band] + fracX * r2c2[band];
		float interp1 = (1.0 - fracX) * *this(r1,c1) + fracX * *this(r1,c2);
		float interp2 = (1.0 - fracX) * *this(r2,c1) + fracX * *this(r2,c2);

		return interp1 * (1 - fracY) + interp2 * fracY;
	}

	//Set the whole image to a particular value
	template <typename _Tp> void set(const GrayValue<_Tp> &pix)
	{
		T*	ia = (T*)this->data;
		for (int64_t i = 0; i < pixelCount(); i ++)
		{
			// for RGBImage, pixels are set to pix gray value
			*ia = pix; //->set(pix);
			++ ia;
		}
	}

	//Set the whole image to a particular value
	template <typename _Tp> void set(const RGBValue<_Tp> &pix)
	{
		T* ia = (T*)this->data;
		for (int64_t i = 0; i < pixelCount(); i ++)
		{
			// for GrayscaleImage, pixels are set to average value of RGB pix
			*ia = pix; //->set(pix);
			++ ia;
		}
	}

	void set(T v)
	{
		T* ia = (T*)this->data;
		for (int64_t i = 0; i < pixelCount(); i ++)
		{
			*ia = v; //->set(v);
			++ ia;
		}
	}

	// same as minUpdate
//	void setmin(T v)
//	{
//		T* ia = (T*)this->data;
//		for (int64_t i = 0; i < pixelCount(); i ++)
//		{
//			if (*ia<v)
//				*ia = v; //->set(v);
//			++ ia;
//		}
//	}

	void setZeroMinThreshold(T v)
	{
		T* ia = (T*)this->data;
		for (int64_t i=0; i < pixelCount(); i++)
		{
			if (*ia < v)
				*ia = 0;
			++ia;
		}
	}


	// sets the data with the RGBValue
	template <typename _T> void set(unsigned index, const RGBValue<_T> &pix);
	template <typename _T> void set(unsigned row, unsigned col, const RGBValue<_T> &pix);
	template <typename _T> void set(const cv::Point2i &loc, const RGBValue<_T> &pix);
	template <typename _T> void set(unsigned rowStart, unsigned colStart, unsigned dRow, unsigned dCol, const std::vector<RGBValue<_T> > &pix);
	template <typename _T> void set(const cv::Point2i &anchor, unsigned dRow, unsigned dCol, const std::vector<RGBValue<_T> > &pix);
	template <typename _T> void set(const cv::Rect &box, const std::vector<RGBValue<_T> > &pix);

	// sets the current image based on an existing image or an existing image
	void set(const TImage<T> &src)
	{
		/*
		// Throw out old pixels, allocate new ones and copy.
		this->allocate(src.rows, src.cols);
		memmove(this->data, src.data, this->cols * this->rows * sizeof (T));
		*/
		src.copyTo(*this);
	}

	// Logs all bands, for values < t, uses a linear toe slope
	// NOTE: probably valid only for float type image
	TImage<T> &log_toe(const float t = 1.f/255.f)
	{
		float a = 1.f / t;
		float b = std::log(t) - 1.f;

		int64_t end = pixelCount() * this->channels();
		for (int64_t  i= 0; i < end; i ++) {
			if (FDATA[i] > t)
				FDATA[i] = std::log(FDATA[i]);
			else
				FDATA[i] = a * (FDATA[i]) + b;
		}

		return *this;
	}

	// Logs all bands, clamp minimum to log(min_val) [default min_val = 1e-5]
	TImage<T> &log(const float min_val = cMinImageValue)
	{
		int64_t end = pixelCount() * this->channels();
		for (int64_t  i= 0; i < end; i ++)
			FDATA[i] = vml_log(FDATA[i],min_val);

		return *this;
	}

	TImage<T> &exp(void)
	{
		int nrows = this->rows, ncols = this->cols;
		if (this->isContinuous()) {
			ncols *= nrows;
			nrows = 1;
		}
		for (int i=0; i < nrows; i++)
		{
			T *data_i = (T *) this->ptr(i);
			for (int j=0; j < ncols; j++)
			{
				data_i[j].setexp();
			}
		}

		return *this;
	}

	//pow all bands
	TImage<T> &pow(const float &p)
	{
		int nrows = this->rows, ncols = this->cols;
		if (this->isContinuous()) {
			ncols *= nrows;
			nrows = 1;
		}
		for (int i=0; i < nrows; i++)
		{
			T *data_i = (T *) this->ptr(i);
			for (int j=0; j < ncols; j++)
			{
				data_i[j].setpow(p);
			}
		}

		return *this;
	}

	TImage<T> &sqrt() {
		int64_t end = pixelCount() * this->channels();
		for (int64_t i=0; i<end; i++) {
			FDATA[i] = (float) std::sqrt((float) FDATA[i]);
		}

		return *this;
	}

	TImage<T> &sqrtClipToZero() {
		int64_t end = pixelCount() * this->channels();
		for(int64_t i=0; i<end; i++) {
			FDATA[i] = (float) std::sqrt(std::max(0.f, (float) FDATA[i]));
		}

		return *this;
	}

	void cartesianToPolar(TImage<T> & Iy)
	{
		int nrows = this->rows, ncols = this->cols;
		VML_ASSERT(Iy.size()==this->size());
		if (this->isContinuous()) {
			ncols *= nrows;
			nrows = 1;
		}
		for (int i=0; i < nrows; i++)
		{
			T *data_i = (T *) this->ptr(i);
			T *otherData_i = (T *) Iy.ptr(i);
			for (int j=0; j < ncols; j++)
			{
				data_i[j].cartesianToPolar(otherData_i[j]);
			}
		}
	}

	template <typename _T>
	TImage<T> &setToWeightedAverage(const TImage<GrayValue<_T> > & weights, const TImage<T> &otherImage)
	{
		int nrows = this->rows, ncols = this->cols;
		VML_ASSERT(otherImage.size()==this->size());
		if (this->isContinuous()) {
			ncols *= nrows;
			nrows = 1;
		}
		for (int i=0; i < nrows; i++)
		{
			T *data_i = (T *) this->ptr(i);
			T *otherData_i = (T *) otherImage.ptr(i);
			float * weightData_i = (float *) weights.ptr(i);
			for (int j=0; j < ncols; j++)
			{
				//data_i[j].setpow(p);
				data_i[j]*=weightData_i[j];
				data_i[j]+=otherData_i[j]*(1-weightData_i[j]);
			}
		}

		return *this;
	}

	TImage<T> & fastApproximateGaussianBlur(TImage<T> &outImage,float inSigma,int iterations = 3) const
	{
		float twelveTimesBlurSigmaSquared=12.0f*inSigma*inSigma;

		float bestGuessFloat;
		int bestGuess;



		for(int blurIteration=0; blurIteration<iterations; blurIteration++)
		{
			bestGuessFloat = std::sqrt(1+(twelveTimesBlurSigmaSquared/((float)(iterations-blurIteration))));

			if (bestGuessFloat-floor(bestGuessFloat)>0.5)
				bestGuess=ceil(bestGuessFloat);
			else
				bestGuess=floor(bestGuessFloat);

			//TVS_MSG("Approximating Gaussian Blur Sigma "<< inSigma <<" with a box filter of width "<< bestGuess<<" for blur iteration "<<blurIteration<< std::endl);

			// do the blur
			if (blurIteration==0)
				cv::boxFilter(*this,outImage,-1,cv::Size(bestGuess,bestGuess));
			else
				cv::boxFilter(outImage,outImage,-1,cv::Size(bestGuess,bestGuess));

			//subtract that part of twelveTimesBlurSigma:
			twelveTimesBlurSigmaSquared-=(((float)bestGuess)*((float)bestGuess))-1;
		}
		return outImage;
	}

	TImage<T> & gaussianBlur(TImage<T> &outImage,float inSigma) const
	{

		int filterDim = int(ceil(inSigma*3))*2+1;
		cv::Size blurSize(filterDim, filterDim);
		cv::GaussianBlur(*this, outImage,
		                 blurSize, inSigma, inSigma);
		return outImage;
	}

	template <typename _T>
	TImage<T> & weightedApproximateGaussianBlur(TImage<T> &outImage,TImage<GrayValue<_T> > &weightImage,float inSigma)
	{

		//weightedImg = weights .* img;

		this->copyTo(outImage);
		//outImage*=(weightImage);
		outImage.grayscalePointwiseImageProduct(weightImage);
		//(weightImage);



		//blurred = gaussianBlur(weightedImg, sigma); %pretending gaussianBlur is an actual function

		outImage.fastApproximateGaussianBlur(outImage,inSigma);

		TImage<GrayValue<_T> > blurredWeights (this->rows,this->cols);
		weightImage.fastApproximateGaussianBlur(blurredWeights,inSigma);

		outImage.grayscalePointwiseImageDivide(blurredWeights);
		return outImage;
	}

	TImage<T> &sqare() {
		int64_t end = pixelCount() * this->channels();
		for (int64_t i=0; i<end; i++) {
			FDATA[i] = (float) ((float) FDATA[i])*((float) FDATA[i]);
		}

		return *this;
	}

	template <typename _T>
	void intensity(TImage<GrayValue<_T> > &intensityImg) const
	{
		VML_ASSERT(this->isContinuous() && intensityImg.isContinuous());

		if (intensityImg.rows != this->rows || intensityImg.cols != this->cols)
		{
			VML_ERROR (MsgCode::sizeMisMatchErr, "Destination grayscale image should"
			           "match this image's size!");
		}

		GrayValue<_T> *data_inten = (GrayValue<_T> *) intensityImg.data;
		T *data_orig = (T *) this->data;

		for (int i=0; i<this->pixelCount(); i++)
		{
			data_inten[i].v() = data_orig[i].intensity();
		}
	}

	template <typename _T>
	void intensityWeighted(TImage<GrayValue<_T> > &intensityImg,const RGBValue<_T> & rgbWeights) const
	{
		VML_ASSERT(this->isContinuous() && intensityImg.isContinuous());

		if (intensityImg.rows != this->rows || intensityImg.cols != this->cols)
		{
			VML_ERROR (MsgCode::sizeMisMatchErr, "Destination grayscale image should"
			           "match this image's size!");
		}

		GrayValue<_T> *data_inten = (GrayValue<_T> *) intensityImg.data;
		T *data_orig = (T *) this->data;

		for (int i=0; i<this->pixelCount(); i++)
		{
			data_inten[i].v() = data_orig[i].intensityWeighted(rgbWeights);
		}

		return;
	}

	template <typename _T>
	void intensityAsMin(TImage<GrayValue<_T> > &intensityImg) const
	{
		VML_ASSERT(this->isContinuous() && intensityImg.isContinuous());

		if (intensityImg.rows != this->rows || intensityImg.cols != this->cols)
		{
			VML_ERROR (MsgCode::sizeMisMatchErr, "Destination grayscale image should"
			           "match this image's size!");
		}

		GrayValue<_T> *data_inten = (GrayValue<_T> *) intensityImg.data;
		T *data_orig = (T *) this->data;

		for (int i=0; i<this->pixelCount(); i++)
		{
			data_inten[i].v() = data_orig[i].min();
		}

		return;
	}

	template <typename _T>
	void intensityAsMax(TImage<GrayValue<_T> > &intensityImg) const
	{
		VML_ASSERT(this->isContinuous() && intensityImg.isContinuous());

		if (intensityImg.rows != this->rows || intensityImg.cols != this->cols)
		{
			VML_ERROR (MsgCode::sizeMisMatchErr, "Destination grayscale image should"
			           "match this image's size!");
		}

		GrayValue<_T> *data_inten = (GrayValue<_T> *) intensityImg.data;
		T *data_orig = (T *) this->data;

		for (int i=0; i<this->pixelCount(); i++)
		{
			data_inten[i].v() = data_orig[i].max();
		}

		return;
	}

	template <typename _T>
	inline void intensity(cv::Mat_<float> &intensityImg)
	{
		VML_ASSERT(intensityImg.channels()==1);
		VML_ASSERT(intensityImg.isContinuous());

		TImage<GrayValue<_T> > gray_img(this->rows, this->cols,
		                        (GrayValue<_T> *) intensityImg.data);
		intensity(gray_img);
	}

	template <typename _T> void getChannel(int k, TImage<GrayValue<_T> > &channel);

	template <typename _T> void setChannel(int k, const TImage<GrayValue<_T> > &channel);

	void rg_chrom(void)
	{
		// check if the depth is 3
		VML_ASSERT(T::nbands == 3);
		VML_ASSERT(this->isContinuous());

		T *image_data = (T*)this->data;
		for (int i=0;i<this->pixelCount(); i++)
		{
			image_data->rg_chrom();
		}
	}

	friend class ImageIO;
};

// image definitions
typedef class TImage<RGBAb>	RGBAbImage;

typedef	class TImage<RGBb>	RGBbImage;
typedef class TImage<RGBs>	RGBsImage;
typedef class TImage<RGBf>	RGBfImage;
typedef class TImage<RGBd>  RGBdImage;

typedef class TImage<Grayb>	GraybImage;
typedef class TImage<Grays>	GraysImage;
typedef class TImage<Grayf>	GrayfImage;
typedef class TImage<Grayd> GraydImage;

} // of namespace vml


#endif

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef LogWriter_HPP
#define LogWriter_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <string>

// AIT INCLUDES
//

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

namespace ait 
{

/**
 * Write entries in a log file named 'main.log'. This class
 * does not check for size limits on the file.
 */
class LogWriter : private boost::noncopyable
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  LogWriter();
  LogWriter(const std::string& filename, bool logTime = true);  

  /**
   * Destructor
   */
  virtual ~LogWriter();

  //
  // OPERATIONS
  //

public:

  /**
   * Write an entry in the log.
   */
  virtual void write(const std::string& str);

  /**
   * Writes all characters still in memory to log file
   */
  void flush();

  //
  // ACCESS
  //

public:


  //
  // INQUIRY
  //

public:


  //
  // ATTRIBUTES
  //

protected:
	FILE *mStream;
	
private:	
  /**
   * Buffers for time and date
   */
  char mDbuffer [9];  //these are size nine right out of the MSDN library example
  char mTbuffer [9];  
  
  std::string mFilename;
  bool mLogTime;
};


}; // namespace ait

#endif // LogWriter_HPP


#ifndef _IMAGE_FWD_HPP_
#define _IMAGE_FWD_HPP_

//#pragma once

#include <boost/shared_ptr.hpp>

namespace ait
{

class Image32;
typedef boost::shared_ptr<Image32> Image32Ptr;

class Image8;
typedef boost::shared_ptr<Image8> Image8Ptr;

}; // namespace ait


#endif 

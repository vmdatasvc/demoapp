#ifndef _BASICMATRIX_HPP_
#define _BASICMATRIX_HPP_

//#pragma once

#include <legacy/pv/PvUtil.hpp>

#include <stdio.h>
#include <stdlib.h>

#include <string>
#include <fstream>
#include <iostream>

#ifndef uint
    //#define uint unsigned int
    typedef unsigned int uint;
#endif


namespace ait
{

/**
 *  Base Matrix Class. This basic class carries the minimal functionality 
 *  of a matrix class. At this point it is NOT assumed that the data type
 *  <class T> have any capability beyond being assignable.
 */
template<class T> class BasicMatrix
{
public:

  explicit BasicMatrix(const int size);

  explicit BasicMatrix(const uint size);

  BasicMatrix(const uint rows,const uint cols);

  /// Constructs a 1x1 matrix using mx_new()
  BasicMatrix(void) { mx_new(1,1); }; 
  BasicMatrix(const BasicMatrix& mx);
  virtual ~BasicMatrix(void);

  /// Resize 'this' matrix using mx_assert()
  void resize(const uint rows,const uint cols) { mx_assert(rows,cols); };

  /// Resize 'this' matrix calling resize(1,size).
  void resize(const uint size) { resize(1,size); };

  inline BasicMatrix& operator= (const BasicMatrix& mx);

  bool operator==(const BasicMatrix<T>& rhs) const;

  bool operator!=(const BasicMatrix<T>& rhs) const;

  /// Return a pointer to the 1D array with the matrix data.
  T *pointer(void) const { return mat[0]; };
  /// Return a pointer to the 2D array with the matrix data.
  T **pointerpointer(void) const { return mat; };

#ifdef PVBOUNDCHECK
  T& operator()(const uint r, const uint c) { mx_check(r,c);return mat[r][c];};
  T operator()(const uint r, const uint c) const { mx_check(r,c);return mat[r][c];};
  T& operator()(const uint pos) { mx_check(pos);return mat[0][pos];};
  T operator()(const uint pos) const { mx_check(pos);return mat[0][pos];};
#else
  /// Returns a reference to the element M(r,c)
  T& operator()(const uint r, const uint c) { return mat[r][c];};
  /// Returns a copy of the element M(r,c)
  T operator()(const uint r, const uint c) const { return mat[r][c];};
  /// Returns a reference to the element M(pos) (1D)
  T& operator()(const uint pos) { return mat[0][pos];};
  /// Returns a copy of the element M(pos) (1D)
  T operator()(const uint pos) const { return mat[0][pos];};
#endif

  /// Set this value only if it is between bounds.
  void safe_set(const uint r, const uint c, const T val) 
  { 
    if (r>=0 && r < rows() && c>= 0 && c<cols()) mat[r][c] = val;
  };

  /// Return the number of rows
  uint rows(void) const { return nrows;}
  /// Return the number of columns
  uint cols(void) const { return ncols;}
  /// Returns rows()*cols()
  uint size(void) const { return nrows*ncols; }

  BasicMatrix<T>& setAll(const T val);
  BasicMatrix<T>& setAll(const T val, const int fromrow, const int fromcol, const int torow, const int tocol);
  BasicMatrix<T>& transpose(void);
  BasicMatrix<T>& transpose(const BasicMatrix& mx);

  BasicMatrix<T>& crop(const BasicMatrix& mx,
	    const uint fromrow,const uint fromcol,
	    const uint torow,const uint tocol);
  
  BasicMatrix<T>& crop(const uint fromrow,const uint fromcol,
	    const uint torow,const uint tocol);
  
  BasicMatrix<T>& getRow(BasicMatrix<T>& mx, int i);
  BasicMatrix<T>& getCol(BasicMatrix<T>& mx, int i);

  BasicMatrix<T>& setRow(BasicMatrix<T>& mx, int i);
  BasicMatrix<T>& setCol(BasicMatrix<T>& mx, int i);

  void printf(const char *format) const;
  int fprintf(FILE *fid,const char *format) const;
  int save(const char *name,const char *format) const;
  int load(const char *name,const char *format);

  friend std::ostream & operator<<(std::ostream &stream, const BasicMatrix<T> &s)
  {
    uint r,c;
    stream << s.nrows << ' ' << s.ncols << '\n';
    for(r=0;r<s.nrows;r++)
    {
      for(c=0;c<s.ncols;c++)
      {
        stream << s(r,c) << ' ';
      }
      stream << '\n';
    }
    
    return stream;
  }

  friend std::istream & operator>>(std::istream &stream, BasicMatrix<T> &s)
  {
    uint nr,nc;
    uint r,c;
    stream >> nr >> nc;
    
    s.mx_assert(nr,nc);
    
    for(r=0;r<nr;r++)
    {
      for(c=0;c<nc;c++)
      {
        stream >> s(r,c);
      }
    }
    
    return stream;
  }

protected:

  T **mat;          ///< pointer to row index array
  uint nrows;       ///< # of rows
  uint ncols;       ///< # of cols
  bool freeMat;     ///< free matrix memory flag (internal use)

  T **mx_alloc(const uint rows,const uint cols) const;
  void mx_set(T **mx,const uint rows,const uint cols);
  void mx_new(const uint rows,const uint cols);
  void mx_delete(void);
  void mx_reallocate(const uint rows,const uint cols);
  inline void mx_assert(const uint rows,const uint cols);
  inline void mx_copy(const BasicMatrix& mx);
  void mx_check(const uint r, const uint c) const;
  void mx_check(const uint pos) const;

public:

  void mx_fromarray(T *array,const uint rows,const uint cols,const uint colSkip=0);  
};

/*********************************************************************
 ** 
 ** Private Helper Functions
 **
 *********************************************************************/

/**
 * Allocate a new row index whose elements point to the rows
 * of a newly created matrix [both the index and the matrix are 
 * allocated for this function].
 * Allocates new data arrays. More specifically it allocates 
 * a new row index whose elements point to the rows of a newly 
 * created matrix. Both the index and the matrix are allocated for
 * this function.
 * @param rows [in] size of matrix to be allocated
 * @param cols [in] size of matrix to be allocated
 * @return pointer to row index array pointing to the newly
 *       allocated matrix
 * @remark If memory can't be allocated, the function reports 
 * the error and calls exit().
 */
template<class T> T **BasicMatrix<T>::mx_alloc(const uint rows,
						 const uint cols) const
{
  uint i;
  T **mx;
  T *pmx;

  assert(rows>0 && cols>0);

#ifdef PV_REPORT
  report.allocCount++;
  PVTIME start=PvUtil::time();
#endif

  // allocate new row index array
  mx=new T*[rows];
  
  if(mx==NULL)
    {
      PvUtil::exitError("BasicMatrix: Error allocating memory for row index of size %d !",rows);
    }

  // allocate actual matrix elements
  pmx=new T[cols*rows];

  if(pmx==NULL)
    {
      delete [] pmx;
      PvUtil::exitError("BasicMatrix: Error allocating memory for matrix of size %dx%d !",rows,cols);
    }

  // initialize row index
  for(i=0;i<rows;i++)
    {
      mx[i]=pmx;
      pmx+=cols;
    }

#ifdef PV_REPORT
  report.allocTime+=PvUtil::time()-start;
#endif

  return mx;
}


/**
 * Initialize member variables with the given parameters.
 * Given a row index array pointing to a matrix array and 
 * dimensions, initialize the member variables.
 * @param T [in] pointer to row index array pointing to matrix
 * @param rows,cols [in] size of matrix
 */
template<class T> void BasicMatrix<T>::mx_set(T **mx,
						const uint rows,
						const uint cols)
{
  mat=mx;
  ncols=cols;
  nrows=rows;
  freeMat = true;
}

/**
 * Allocate new matrix and initialize member variables.
 * [this function only calls mx_alloc() followed by mx_set()].
 * Allocate and initialize a matrix of given dimensions.
 * Essentially the functionality of the constructor.
 * @param rows,cols [in] size of matrix
 * @return pointer to index array
 * @remark Error handling is the same as mx_alloc().
 * @see mx_alloc()
 * @see mx_set()
 */
template<class T> void BasicMatrix<T>::mx_new(const uint rows,
						const uint cols)
{
  T **mx;
  mx=mx_alloc(rows,cols);
  mx_set(mx,rows,cols);
}

/**
 * Delete row index and matrix array. Free the row index array and the matrix array.
 * Essentially the functionality of the destructor. The function will not delete the
 * matrix contents if the flag freeMat is set, this is used to handle matrices whose
 * data resides in memory allocated by other libraries (e.g.: frame grabber).
 * @remark No exceptions can occur
 */
template<class T> void BasicMatrix<T>::mx_delete(void)
{
  if (freeMat) delete [] mat[0];
  delete [] mat;

#ifdef PV_REPORT
  report.freeCount++;
#endif
}

/**
 * Delete all arrays used by 'this' and allocate new ones
 * with the new dimensions. Mainly used for resizing a matrix.
 * @param rows,cols [in] size of matrix
 * @remark Error handling is the same as mx_alloc().
 */
template<class T> void BasicMatrix<T>::mx_reallocate(const uint rows,
						       const uint cols)
{
  mx_delete();
  mx_new(rows,cols);

#ifdef PV_REPORT
  report.resizeCount++;
#endif
}

/**
 * If matrix not of the specified dimensions, it is deleted and reallocated.
 * Make sure that 'this' has the right size. If not, reallocate
 * with the new dimensions.
 * @param rows,cols [in] dimensions 'this' matrix should have
 * @remark Error handling is the same as mx_alloc().
 * @see resize()
 */
template<class T> void BasicMatrix<T>::mx_assert(const uint rows,
						   const uint cols)
{
  if(ncols!=cols || nrows!=rows) mx_reallocate(rows,cols);
}

/**
 * Check if the coordinates r and c (or index) are within the bounds.
 * This function is called if the macro PVBOUNDCHECK is defined.
 * @param r,c [in] row, col
 * @remark If the spec. coords are not within bounds, the program aborts.
 */
template<class T> void BasicMatrix<T>::mx_check(const uint r, const uint c) const
{
  if(r>=nrows || c>=ncols)
  {
    PvUtil::exitError("BasicMatrix: Coordinates [%d,%d] out of bound! Aborting!",r,c);
    abort();
  }
}

/**
 * Check if the coordinates r and c (or index) are within the bounds.
 * This function is called if the macro PVBOUNDCHECK is defined.
 * @param pos [in] index of the 1D matrix contents array.
 * @remarks If the spec. coords are not within bounds, the program aborts.
 */
template<class T> void BasicMatrix<T>::mx_check(const uint pos) const
{
  if(pos>=nrows*ncols)
  {
    PvUtil::exitError("BasicMatrix: Index %d out of bound! Aborting!",pos);
    abort();
  }
}


/**
 * Make 'this' an identical copy of the specified matrix.
 * @param mx [in] matrix of which to create a copy
 * @remark 'this' and mx *MUST* have the same size!
 */
template<class T> void BasicMatrix<T>::mx_copy(const BasicMatrix<T>& mx)
{
  uint i,s=nrows*ncols;

  T *dst=pointer();
  T *src=mx.pointer();

  for(i=0;i<s;i++) 
	  *(dst++)=*(src++);
}

/**
 * Given a pointer to an array of matrix elements and dimensions,
 * initialize 'this' matrix by using the given data.
 * Only a row index array is allocated, for the actual matrix, the
 * given array is used.
 * Prior to calling this function, the memory used by 'this' matrix 
 * must be deleted.
 * @param array [in] array of matrix elements 
 * @param rows,cols [in] size of matrix
 * @param colSkip [in] amount added to index for each new row
 * @remark If the row array couldn't be allocated, the function prints out 
 * an error message and calls exitError().
 */
template<class T> void BasicMatrix<T>::mx_fromarray(T *array,uint rows,uint cols,const uint colSkip)
{
  uint i;
  uint skip=cols+colSkip;
  T **mx;

  // allocate row index array
  mx=new T*[rows];

  if(mx==NULL)
    {
      PvUtil::exitError("BasicMatrix: Error allocating memory for Index of size %d !",rows);
    }

  // initialize index array
  for(i=0;i<rows;i++)
    {
      mx[i]=array;
      array+=skip;
    }

  mx_delete();

  // initialize member variables
  mx_set(mx,rows,cols);

  // Set the free memory flag to false. This memory is external
  freeMat = false;
}

/*********************************************************************
 ** 
 ** Public Member Functions
 **
 *********************************************************************/

/**
 * Constructs a matrix of size 1 x size. This constructor should
 * be used when matrices are going to be treated as vectors.
 * @param size [in] number of columns for the matrix.
 * @remark This function was created to allow the compiler to work
 * with numerical constants that are (at least in MSVC) const int.
 */
template<class T> BasicMatrix<T>::BasicMatrix(const int size)
{
  mx_new(1,static_cast<uint>(size));
}

/**
 * Constructs a matrix of size 1 x size. This constructor should
 * be used when matrices are going to be treated as vectors.
 * @param size [in] number of columns for the matrix.
 * @remark Error handling is the same as mx_alloc().
 */
template<class T> BasicMatrix<T>::BasicMatrix(const uint size)
{
  mx_new(1,size);
}

/**
 * Constructs a matrix of given dimensions. If no dimensions are 
 * specified, a matrix of size (1,1) is constructed.
 * @param rows,cols [in] size of matrix
 * @remark Error handling is the same as mx_alloc().
 */
template<class T> BasicMatrix<T>::BasicMatrix(const uint rows,
						  const uint cols)
{
  mx_new(rows,cols);
}

/** 
 * Constructs a copy of a given matrix.
 * @param mx [in] matrix to create a copy of.
 * @remark Error handling is the same as mx_alloc().
 */
template<class T> BasicMatrix<T>::BasicMatrix(const BasicMatrix<T>& mx)
{
  mx_new(mx.nrows,mx.ncols);
  mx_copy(mx);
}


/**
 * getRow(Matrix<T> mx, int i)
 * 
 * Create single column matrix from i-th row of matrix a
 *
 * <input> Variable depending on method.
 *
 * Matrix<T> a 
 * int i 
 *
 * <output>
 *
 * void
 *
 */
 template<class T> BasicMatrix<T>& BasicMatrix<T>::getRow(BasicMatrix<T>& mx, int i)
 {
   int k,l=0;
   int c=mx.ncols;

   resize(1, c);
   T *dst=pointer();
   T *src=mx.pointer();

   for(k=i*c;k<(i+1)*c;k++,l++)
     dst[l] = src[k];
 
   return *this;
 }

 
/**
 * getCol(Matrix<T> a, int i)
 * 
 * Create single column matrix from i-th column of matrix a
 *
 * <input> Variable depending on method.
 *
 * Matrix<T> a 
 * int i 
 *
 * <output>
 *
 * void
 *
 */
 template<class T> BasicMatrix<T>& BasicMatrix<T>::getCol(BasicMatrix<T>& mx, int i)
 {
   int k,l=0;
   int c=mx.ncols;
   int r=mx.nrows;

   resize(1, r);
   T *dst=pointer();
   T *src=mx.pointer();

   for(int k=i;k<r*c;k+=c,l++)
     dst[l] = src[k];
 
   return *this;
 }

 
/**
 * setRow(BasicMatrix<T> mx, int i)
 * 
 * Assign single column matrix a to i-th row of this matrix
 *
 * <input> Variable depending on method.
 *
 * BasicMatrix<T> a : single column matrix 
 * int i :  i-th row of this matrix
 *
 * <output>
 *
 * void
 *
 */
 template<class T> BasicMatrix<T>& BasicMatrix<T>::setRow(BasicMatrix<T>& mx, int i)
 {
   int k,l=0;
   int c=mx.ncols;

   PVASSERT(c==ncols);
   
   T *dst=pointer();
   T *src=mx.pointer();

   for(k=i*c;k<(i+1)*c;k++,l++)
     dst[k] = src[l];
 
   return *this;
 }


/**
 * setCol(BasicMatrix<T> mx, int i)
 * 
 * Assign single column matrix a to i-th col of this matrix
 *
 * <input> Variable depending on method.
 *
 * BasicMatrix<T> a : single column matrix 
 * int i :  i-th row of this matrix
 *
 * <output>
 *
 * void
 *
 */
 template<class T> BasicMatrix<T>& BasicMatrix<T>::setCol(BasicMatrix<T>& mx, int i)
 {
   int k,l=0;
   int c=mx.ncols;

   PVASSERT(c==nrows);
   
   T *dst=pointer();
   T *src=mx.pointer();

   for(int k=0,l=i;k<c;k++,l+=ncols)
     dst[l] = src[k];
 
   return *this;
 }

/**
 * Destructs 'this' matrix.
 * @remark No exception can occur.
 */
template<class T> BasicMatrix<T>::~BasicMatrix(void)
{
  mx_delete();
}

/**
 * Makes a statement like 'a=b;' possible, with a,b being matrices.
 * @param mx [in] the matrix to create a copy of.
 * @return reference to 'this', to enable 'a=b=c;'
 * @remark Error handling is the same as mx_alloc().
 */
template<class T> BasicMatrix<T>& BasicMatrix<T>::operator=
                                           (const BasicMatrix<T>& mx)
{
  if(this != &mx)
    {
      mx_assert(mx.nrows,mx.ncols);
      mx_copy(mx);
    }
  return *this;
}

/**
 * Check if two matrices are equal by comparing its elements.
 * @param m1,m2 [in] matrices to be compared.
 * @return true if they are equal, false otherwise.
 */
template<class T> bool 
BasicMatrix<T>::operator==(const BasicMatrix<T>& rhs) const
{
  if (rows() != rhs.rows()) return false;
  if (cols() != rhs.cols()) return false;

  uint s=nrows*ncols;

  T *p1=pointer();
  T *p2=rhs.pointer();

  while(s-- > 0)
  {
	  if (!((*(p1++))==(*(p2++)))) return false;
  }

  return true;
}

/**
 * Check if two matrices are differnet. Is the negation of ==.
 */
template<class T> bool 
BasicMatrix<T>::operator!=(const BasicMatrix<T>& rhs) const
{
  return !(*this == rhs);
}

/**
 * Transpose 'this' matrix.
 * @remark Error handling is the same as mx_alloc().
 */
template<class T> BasicMatrix<T>& BasicMatrix<T>::transpose(void)
{
  uint i,j;
  T tmp;
  T **mx;

  if(nrows==ncols)
    {
      // matrix is squared
      for(i=0;i<nrows;i++)
	for(j=i+1;j<ncols;j++)
	  {
	    tmp=mat[i][j];
	    mat[i][j]=mat[j][i];
	    mat[j][i]=tmp;
	  }
    }
  else
    {
      // matrix is not squared, delete and reallocate

      // first allocate new arrays for transposed matrix
      mx=mx_alloc(ncols,nrows);
      
      // transpose+copy
      for(i=0;i<nrows;i++)
	for(j=0;j<ncols;j++)
	  {
	    mx[j][i]=mat[i][j];
	  }

      // save dimensions
      i=nrows;
      j=ncols;
      
      // delete 'this'
      mx_delete();

      // set new member variables
      mx_set(mx,j,i);
    }

  return *this;
}

/**
 * Transpose the given matrix and store the result into 'this'.
 * @param mx [in] matrix to be transposed.
 * @remark Error handling is the same as mx_alloc().
 */
template<class T> BasicMatrix<T>& BasicMatrix<T>::transpose(const BasicMatrix<T> &mx)
{
  uint i,j;

  if(this==&mx)
    transpose();
  else
  {
    mx_assert(mx.ncols,mx.nrows);
    for(i=0;i<nrows;i++)
      for(j=0;j<ncols;j++)
      {
	mat[i][j]=mx.mat[j][i];
      }
  }

  return *this;
}

/**
 * Sets all elements of 'this' to a given value.
 * @param val [in] value
 * @remark Error handling is the same as mx_alloc().
 */
template<class T> BasicMatrix<T>& BasicMatrix<T>::setAll(const T val)
{
  uint i,s=nrows*ncols;

  T *dst=pointer();

  for(i=0;i<s;i++){
	  *(dst++)=val;
  }

  return *this;
}

/**
 * Sets all elements of a subregion of 'this' to a given value.
 * @param val [in] value
 * @param fromrow [in] first row of submatrix
 * @param fromcol [in] first col of submatrix
 * @param torow [in] last row of submatrix
 * @param tocol [in] last col of submatrix
 * @remark Error handling is the same as mx_alloc().
 */
template<class T> BasicMatrix<T>& BasicMatrix<T>::setAll(const T val, const int fromrow, const int fromcol, const int torow, const int tocol)
{
  T *data=pointer()+fromrow*cols()+fromcol;
  const int crows=torow-fromrow+1;
  const int ccols=tocol-fromcol+1;
  const int skip=cols()-ccols;
  int r,c;

  for(r=0;r<crows;r++)
  {
    for(c=0;c<ccols;c++)
    {
      *data=val;
      data++;
    }
    data+=skip;
  }

  return *this;
}

/**
 * Set 'this' matrix to the specified sub-matrix of 'mx'.
 * @param mx [in] matrix to obtain sub matrix from
 * @param fromrow [in] first row of submatrix
 * @param fromcol [in] first col of submatrix
 * @param torow [in] last row of submatrix
 * @param tocol [in] last col of submatrix
 * @remark Error handling is the same as mx_alloc().
 */
template<class T> BasicMatrix<T>& BasicMatrix<T>::crop(const BasicMatrix<T> &mx,
					      const uint fromrow,const uint fromcol,
					      const uint torow,const uint tocol)
{
  uint r,c;
  T *src,*dst;

  if(&mx==this)
    {
      return crop(fromrow,fromcol,torow,tocol);
    }

  // make sure 'this' matrix has the correct size
  mx_assert(torow-fromrow+1,tocol-fromcol+1);

  // get direct pointer to destination to speed things up
  dst=pointer();

  // copy sub matrix
  for(r=fromrow;r<=torow;r++) 
    {
      src=mx.mat[r]+fromcol;
      for(c=fromcol;c<=tocol;c++) *(dst++)=*(src++);
    }
  return *this;
}

/**
 * Crop 'this' matrix and set the result into 'this'.
 * @param fromrow [in] first row of submatrix
 * @param fromcol [in] first col of submatrix
 * @param torow [in] last row of submatrix
 * @param tocol [in] last col of submatrix
 * @remark Error handling is the same as mx_alloc().
 */
template<class T> BasicMatrix<T>& BasicMatrix<T>::crop(const uint fromrow,
					      const uint fromcol,
					      const uint torow,
					      const uint tocol)
{
  uint r,c;
  T **mx;
  T *src,*dst;

  // allocate new memory
  mx=mx_alloc(torow-fromrow+1,tocol-fromcol+1);

  // get direct pointer to destination to speed things up
  dst=mx[0];

  // copy sub matrix
  for(r=fromrow;r<=torow;r++) 
    {
      src=mat[r]+fromcol;
      for(c=fromcol;c<=tocol;c++) *(dst++)=*(src++);
    }

  // delete 'this'
  mx_delete();

  // initialize member variables
  mx_set(mx,torow-fromrow+1,tocol-fromcol+1);

  return *this;
}

/**
 * Write this matrix in ascii format to (FILE *). Each element of the
 * matrix is written according to a formatting string that
 * follows the printf-standard. For example a matrix of integers
 * would be written using the format string "%%d ". The space 
 * seperates adjacent elements in one row from each other.
 * @param fid [in] handle to file to which the matrix shall be written
 * @param format [in] printf style formatting information.
 * @return Returns 'false' if an error occurred, 'true' if successful.
 */
template<class T> int BasicMatrix<T>::fprintf(FILE *fid, 
						 const char *format) const
{
  unsigned int r,c;
    
  for(r=0;r<nrows;r++)
    {
      for(c=0;c<ncols;c++)
	{
	  if(::fprintf(fid,format,mat[r][c])<0) return 0;
	}
      if(::fprintf(fid,"\n")<0) return 0;
    }

  return 1;
}

/**
 * Print this matrix in ascii format to stdout. Each element of the
 * matrix is printed according to a formatting string that
 * follows the printf-standard. For example a matrix of integers
 * would be printed using the format string "%%d ". The space 
 * seperates adjacent elements in one row from each other.
 * @param format [in] printf style formatting information.
 * @remark The errors that can occur while priniting out the data, are
 * NOT handled.
 */
template<class T> void BasicMatrix<T>::printf(const char *format) const
{
  using namespace std;

  unsigned int r,c;
  
  // does user want to print with streams?
  if(format[0]==0)
  {
    for(r=0;r<nrows;r++)
    {
      std::cout << "[ ";
      for(c=0;c<ncols;c++)
      {
        std::cout << mat[r][c] << " ";
      }
      std::cout << "]" << std::endl;
    }
    std::cout << std::endl;
  }
  else
  {
    for(r=0;r<nrows;r++)
    {
      PVMSG("[ ");
      for(c=0;c<ncols;c++)
      {
        PVMSG(format,mat[r][c]);
      }
      PVMSG("]\n");
    }
    PVMSG("\n");
  }
}

/**
 * Save this matrix in ascii format to the file specified.
 * Each element of the matrix is saved according to a formatting string that
 * follows the printf-standard. For example a matrix of integers
 * would be saved using the format string "%%d ". The space 
 * seperates adjacent elements in one row from each other.
 * "%%g\t" would save the matrix separating the elements with tabs
 * @param name [in] filename
 * @param format [in] printf style formatting information.
 * @return Returns 'false' if an error occurred, 'true' if successful.
 */
template<class T> int BasicMatrix<T>::save(const char *name, const char *format) const
{
  FILE *fid;

  fid=fopen(name,"w");

  if(fid==NULL) return 0;

  if(!fprintf(fid,format)) return 0;

  if(fclose(fid)!=0) return 0;

  return 1;
}

/**
 * Load a matrix in ascii format from the file specified.
 * Each element of the matrix is read according to a formatting string that
 * follows the scanf-standard. For example a matrix of integers
 * would be read using the format string "%%d". 
 *
 * Implementation: The function first scans the file to get the
 * number of columns and the number of rows. It then reads the file.
 * Thus the file is scanned twice which is potentially quite expensive.
 *
 * @param name [in] filename
 * @param format [in] scanf style formatting information.
 * @return Returns 'false' if an error occurred, 'true' if successful.
 */
template<class T> int BasicMatrix<T>::load(const char *name,const char *format)
{
  FILE *fid;
  unsigned int i,j;
  int c,cont,wspc,wspc_prev,n,m,nc;
  T tmp;

  fid=fopen(name,"r");

  if(fid==NULL)
    {
      PvUtil::exitError("BasicMatrix: File '%s' not found in load()!",name);
      return 0;
    }
  
  // find the number of columns in the file

  c=' ';cont=1;
  wspc=1;
  n=0; // # of columns
  while(cont)
    {
      c=fgetc(fid);

      // if whitespace, set whitespace flag and continue
      if(c==' ' || c=='\t')
	{
	  wspc=1;
	  continue;
	}

      // if not LF, CR or EOF, continue
      if(c!='\n' && c!='\r' && c!=EOF) cont=1;
      else cont=0;

      // if white space flag==1 this is the first time
      // a non white space character is encountered after a series
      // of white spaces => new number
      if(cont && wspc==1)
	{
	  wspc=0;n++;
	}
    }

  fseek(fid,0,SEEK_SET);

  c=' ';cont=1;
  wspc=1;        // is the last character a whitespace?
  wspc_prev=1;   // was the one before the current character a whitespace?
  m=0;nc=0; // # of rows
  while(cont)
    {
      c=fgetc(fid);

      // if whitespace
      if(c==' ' || c=='\t') 
	 wspc=1;
      else if(c=='\n' || c=='\r' || c==EOF) // if CR or LF
	{
	  if(n==nc) m++;
	  nc=0;
	  wspc=wspc_prev=1;

	  // if not EOF, continue
	  if(c!=EOF) cont=1; 
	  else cont=0;
	}
      else
      {
	// non whitespace encountered
	wspc=0;
      }

      // if white space flag==1 this is the first time
      // a non white space character is encountered after a series
      // of white spaces => new number
      if(cont && !wspc && wspc_prev)
	{	  
	  nc++;
	}

      wspc_prev=wspc;
    }

  fseek(fid,0,SEEK_SET);

  mx_assert(m,n);
  
  for(i=0;i<nrows;i++)
    {
      for(j=0;j<ncols;j++)
	{
	  c=fscanf(fid,format,&tmp);
	  mat[i][j]=tmp;
	  if(c!=1)
	    {
	      PvUtil::exitError("BasicMatrix: error, too few entries in file that appears");
	      PvUtil::exitError("               to have %d rows and %d cols.",m,n);
	      fclose(fid);
	      return 0;
	    }
	}
    }

  c=fscanf(fid,format,&tmp);
  if(c==1)
    {
      PvUtil::exitError("BasicMatrix: error, too many entries in file that appears");
      PvUtil::exitError("               to have %d rows and %d cols.",m,n);
      fclose(fid);
      return 0;
    }
  fclose(fid);

  return 1;
}

} // namespace ait

#endif   // #ifndef _BASICMATRIX_HPP_

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef PVIMAGECONVERTER_HPP
#define PVIMAGECONVERTER_HPP
#include <legacy/pv/ait/Image.hpp>

namespace ait
{

class PvImageConverter
{
public:
  static void convert(const Image32 &in, Image8 &out);
  static void convert(const Image8 &in, Image32 &out);
  static void convert(const Image8 &in, Image<int> &out);
  static void convertBinary(const Image8 &in, Image32 &out);
};

}

#endif



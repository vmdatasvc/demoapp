/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef PVIMAGEARCDEFS_HPP
#define PVIMAGEARCDEFS_HPP

// MACROS FOR CONVERTING UNSIGNED INT TO OTHER R,G,B,A,...
#ifdef WIN32
#define BLUE(pixel) ((pixel) & 0xff)
#define GREEN(pixel) (((pixel)>>8) & 0xff)
#define RED(pixel) (((pixel)>>16) & 0xff)
#define ALPHA(pixel) (((pixel)>>24) & 0xff)
#define R_IDX 2
#define G_IDX 1
#define B_IDX 0
#define A_IDX 3
#define B_GLIDX 2
#define G_GLIDX 1
#define R_GLIDX 0
#define A_GLIDX 3
#define PV_RGB(r,g,b) (((r)<<16)+((g)<<8)+(b))
#define PV_RGBA(r,g,b,a) (((a)<<24)+((r)<<16)+((g)<<8)+(b))
#else
#define RED(pixel) ((pixel) & 0xff)
#define GREEN(pixel) (((pixel)>>8) & 0xff)
#define BLUE(pixel) (((pixel)>>16) & 0xff)
#define ALPHA(pixel) (((pixel)>>24) & 0xff)
#define R_IDX 1
#define G_IDX 2
#define B_IDX 3
#define A_IDX 0
#define PV_RGB(r,g,b) (((b)<<16)+((g)<<8)+(r))
#define PV_RGBA(r,g,b,a) (((a)<<24)+((r)<<16)+((g)<<8)+(b))
#endif

#endif


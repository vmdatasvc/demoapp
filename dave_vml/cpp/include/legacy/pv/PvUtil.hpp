/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef PVUTIL_HPP
#define PVUTIL_HPP

// This include will take care of standarizing some important compiler and library
// features, such as defining properly min/max in MSVC++.
#include <boost/config.hpp>

#include <time.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#ifndef WIN32
#include "legacy/types/LinuxWindowsConversion.hpp"
#endif

#include "legacy/pv/LogWriter.hpp"

// If compiling 'old' applications, this must be defined
#ifdef OLD_AIT_CODE
#ifdef WIN32
#include <windows.h>
#endif
using namespace std;
#endif

// Shorthand defines
#ifndef WIN32
#ifndef uint
/**
 * Took this out because it does not compile on Linux
 #include <sys/bsd_types.h>
**/
/**
 * Added this line to accomodate Linux
 **/
typedef unsigned int uint;
#ifndef uchar
typedef unsigned char uchar; // in sys/bsd_types.h is only "unchar"
#endif // uchar
#endif //uint
#endif

// Bit oriented types
#ifndef dword_t
typedef unsigned long dword_t;
typedef unsigned short word_t;
typedef unsigned char byte_t;
#endif

typedef double PVTIME;

#define PVASSERT(x) assert(x)

/*
void BP(unsigned int num);
*/

const int PVMSG_USE_ANSI=1;
const int PVMSG_USE_TRACE=2;
const int PVMSG_USE_CONSOLE=3;
const int PVMSG_USE_OUTFUNC=4;
const int PVMSG_USE_LOGTOFILE=5;
const int PVMSG_USE_NOTHING=6;
const int PVMSG_USE_SOCKET = 7;

extern int PVMSG_METHOD;
extern void (*PVMSG_FUNC)(char *);
extern void *PVMSG_HANDLE;

void PVMSG(const char *msg, ...);

// Some Math Macros

#ifndef M_PI
const float M_PI=3.1415926535897932384626433832795f;
#endif

const double degOverRad=180.0/M_PI;
const double radOverDeg=M_PI/180.0;
extern bool g_logFileAlso;

/**
 * Deployment mode affects what to do on PvUtil::exitError():
 * - 0 = Display errors on the screen.
 * - 1 = Write errors on log and exit silently.
 * - 2 = Throw an exception on error with the error message.
 * - 3 = Do nothing, just exit.
 */
extern int PvDeploymentMode;

namespace ait
{
// Fast round routine for 32 bit processors. A simple
// cast to int will call the __ftol which has big performance
// impact. This version is inexact but consistently off by 0.25 
// see method round_exact (below) for a slightly slower but exact version
inline int round(float f)
{
  f += (float)(3<<21);
  return (*((int *)&f) - 0x4ac00000) >> 1;
}

// Fast round routine for 32 bit processors. A simple
// cast to int will call the __ftol which has big performance
// impact. This version behaves exactly like floor(x)
inline int round_exact(float f)
{
  f -= 0.25; //ensures same behavior as floor()
  f += (float)(3<<21);
  return (*((int *)&f) - 0x4ac00000) >> 1;
}

}

inline double DEG(double rad) { return degOverRad*rad; }
inline double RAD(double deg) { return radOverDeg*deg; }

// Scope of for in Visual C++ 6.0 is non-standard. This is a fix:
#if (_MSC_VER <= 1200)
#ifndef for
#define for if (0) {} else for
#endif
#endif


/**
 * Basic utility functions.
 */
class PvUtil
{
public:

  /**
   * Exit the application with an error message.
   */
  static void exitError(const char *msg, ...);

  /**
   * Display an error message, but don't exit the application.
   */
  static void error(const char *msg, ...);

  /**
   * Display a yes-no-cancel message
   */
  static int question(const char *msg, ...);

  // These functions should be removed.
  static void setErrorHandle(void *hand) { PVMSG_HANDLE=hand; }
  static void setCONSOLE(bool logFileAlso_in = false){ PVMSG_METHOD=PVMSG_USE_CONSOLE; g_logFileAlso = logFileAlso_in;}
  static void setSOCKET(char* thisIPaddr_in, unsigned short thisPort_in,char* thatIPaddr_in, unsigned short thatPort_in,bool logFileAlso_in = false); 
  static void setTRACE(void) { PVMSG_METHOD=PVMSG_USE_TRACE; }
  static void setANSI(void) { PVMSG_METHOD=PVMSG_USE_ANSI; }
  static void setOUTFUNC(void (*func)(char *)) { PVMSG_METHOD=PVMSG_USE_OUTFUNC;PVMSG_FUNC=func; }
  static void setLOGTOFILE(void) { PVMSG_METHOD=PVMSG_USE_LOGTOFILE; }
  static void setNOTHING(void) { PVMSG_METHOD=PVMSG_USE_NOTHING; }
  static void setLog(ait::LogWriter *pLog);

  /**
   * Sleep for the given number of seconds. Can be a fraction.
   * The resolution is system dependant.
   */
  static void sleep(const double sec);

  /**
   * Return the current UTC time in number of seconds since 
   * Epoch, which is a specific day in the past (system dependant).
   * This time will be based on the current process time. If the
   * system clock is changed, this time will keep running forward,
   * therefore it is guaranteed to never jump or move backwards. Note
   * however that after a period as short as a couple of hours, the CPU time
   * can drift with respect to an atomic clock. If the system is constantly
   * synchronizing with a network clock, it is better to use PvUtil::systemTime().
   * @remark use DateTime to convert this value to a readable time.
   */
  static double time();

  /**
   * This is the same as time() but it is less efficient and
   * the time is always obtained from the system clock time.
   * This means that if the system clock is changed this time will
   * also change. Be careful because this means that this time
   * can go backwards or jump forward as a result of a system
   * time change.
   */
  static double systemTime();


#ifdef WIN32
  /**
   * This function tanslates the return value from timeGetTime() to 
   * the PvUtil::time() format.
   */
  static double timeFromTimeGetTime(unsigned long windowsTime);
#endif

  /**
   * Return true if the file exists.
   */
  static bool fileExists(const char *fname);
  

  ///////////////////////////////////////////////////////////////////
  // some math functions
  ///////////////////////////////////////////////////////////////////

  // calc the log of a number that is a power of two 
  // (i.e., intLog2(1)=0, intLog2(2)=1, intLog2(4)=2, intLog2(8)=3, ...)
  // Note: intLog2(3)==<error> for example
  static unsigned int intLog2(unsigned int n);

private:

  static ait::LogWriter *mpLogWriter;

};


/*****************************************************************************************************
 * Macros for the main routine of the programs. It takes care of handling exceptions in release
 * mode. PvUtil::exitError will not show a dialog if the PvDeploymentMode flag is set (global variable), 
 * but write into a file if an error occurs
 * Usage:
 * PV_MAIN_HEAD(string commandLine)
 * {
 *   ,..
 * }
 * PV_MAIN_TAIL
 *****************************************************************************************************/

#ifdef _DEBUG

#ifdef WIN32
#define PV_MAIN_HEAD(CMDLINE) \
int APIENTRY WinMain(HINSTANCE hCurrentInst, HINSTANCE hPreviousInst, LPSTR lpszCmdLine, int nCmdShow)\
{\
  CMDLINE = lpszCmdLine;
#else
#define PV_MAIN_HEAD(CMDLINE) \
int main(int ac, char** av)\
{
#endif

#define PV_MAIN_TAIL \
  return 0;\
}

#else

#ifdef WIN32 
#define PV_MAIN_HEAD(CMDLINE) \
int APIENTRY WinMain(HINSTANCE hCurrentInst, HINSTANCE hPreviousInst, LPSTR lpszCmdLine, int nCmdShow)\
{\
try {\
  CMDLINE = lpszCmdLine;

#else
#define PV_MAIN_HEAD(CMDLIND) \
int main(int ac, char** av)\
{\
   try{\

#endif

#define PV_MAIN_TAIL \
} catch (std::string str) { PvDeploymentMode = 3; PvUtil::exitError("Unhandled Exception: [%s]",str.c_str()); \
} catch (std::exception e) { PvDeploymentMode = 3; PvUtil::exitError("Unhandled Exception: [%s]",e.what()); \
} catch (...) { PvDeploymentMode = 3; PvUtil::exitError("Unhandled Exception!!!"); } \
  return 0;\
}

#endif

#endif



/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef PVRANDOM_HPP
#define PVRANDOM_HPP

#define NTAB 32

namespace ait
{

class PvRandom
{
protected:

  // standard floating point random number generator(s)
  float ran1(long &idum);

  // record of first call flags and seeds;
  bool uniform0to1_first;
  long uniform0to1_seed;

  bool uniformMin1to1_first;
  long uniformMin1to1_seed;

  bool gaussRand_first;
  long gaussRand_seed;

public:

  static const int UNIFORM0TO1;
  static const int UNIFORMMIN1TO1;
  static const int GAUSSRAND;
  static const int UNIFORM0TON;

  PvRandom() : uniform0to1_first(true),uniformMin1to1_first(true),
    gaussRand_first(true), iy(0) {};

  /// uniformly distributed random number in the interval [0,1]
  float uniform0to1(void);

  /// uniformly distributed random number in the interval [-1,1]
  float uniformMin1to1(void);

  /// uniformly distributed integer random number in the interval [0,n-1]
  int uniform0toN(int n);

  /// normal gaussian distribution with mean zero and variance 1
  float gaussRand(void);

  /// uniformly distributed random number in the interval [min,max]
  float uniformInterval(float min,float max) { return uniform0to1()*(max-min)+min; }

  /// uniformly distributed random number from the set {min,...,max}
  int uniformInterval(int min,int max) 
    { 
      int n=int(uniform0to1()*(max-min+1)+min);
      if(n>max) n=max; else if(n<min) n=min; 
      return n;
    }

  /// method to get the random number seed
  long getSeed(int func);

  /// method to set the random number seed
  void setSeed(int func, long value);

private:

  long iy;
  long iv[NTAB];

};

} // namespace ait

#endif

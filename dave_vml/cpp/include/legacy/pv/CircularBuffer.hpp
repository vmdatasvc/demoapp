/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef CircularBuffer_HPP
#define CircularBuffer_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#ifdef WIN32
#include <exception>
#else
#include <stdexcept>
#endif
#include <vector>


// AIT INCLUDES
//

#include <legacy/types/String.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//


namespace ait 
{


/**
 * Simple implementation of a circular buffer. It has a capacity and elements
 * can only be inserted using push_back(), retrieved using front() and removed
 * using pop_front(). No iterators are implemented. The operator [] gives access
 * to the elements. All the operations, except copy are O(1).
 * @remark This is not compatible with STL containers.
 */
template<class T> class CircularBuffer
{
  //
  // LIFETIME
  //

public:

  typedef unsigned int size_type;

  /**
   * Construct a circular buffer with capacity 1.
   */
  CircularBuffer()
  {
    setCapacity(1);
  }

  /**
   * Constructor with capacity.
   * @param capacity [in] The size of the circular buffer.
   */
  CircularBuffer(const size_type capacity)
  {
    setCapacity(capacity);
  }

  /**
   * Copy Constructor
   */
  CircularBuffer(const CircularBuffer<T>& from)
  {
    copy(from);
  }

  /**
   * Destructor
   */
  virtual ~CircularBuffer() {}

  //
  // OPERATORS
  //

public:

  /**
   * Assignment operator.
   * @param from [in] The value to assign to this object.
   * @return A reference to this object.
   */
  CircularBuffer& operator=(const CircularBuffer<T>& from)
  {
    if (this != &from)
    {
      copy(from);
    }

    return *this;
  }

public:

  /**
   * Test if two circular buffers are equal.
   * For two buffers to be equal they must have the same
   * size and capacity, and each element must be equal.
   * However, the internal mFront and mBack pointers can
   * be different; in other words, the elements can be shifted
   * in the mElement array.
   */
  bool operator==(const CircularBuffer<T>& other) const
  {
    if (size() != other.size() || capacity() != other.capacity()) return false;

    int i;
    for (i = 0; i < size(); i++)
    {
      if ((*this)[i] != other[i]) return false;
    }

    return true;
  }

  /**
   * Access random to the elements of the buffer.
   * @param idx [in] The index of the element to
   * return starting from the front. Therefore 
   * idx[0] == front(). The maximum value is idx[size()-1].
   * @throw std::out_of_range() if the index is not valid.
   */
  const T& operator[](const size_type idx) const
  {
    if (idx >= mSize || idx < 0) throw std::out_of_range("index out of range");
    size_type i = mFront + idx;
    if (i >= mElements.size()) i -= mElements.size();
    return mElements[i];
  }

  /**
   * Access random to the elements of the buffer.
   * @param idx [in] The index of the element to
   * return starting from the front. Therefore 
   * idx[0] == front(). The maximum value is idx[size()-1].
   * @throw std::out_of_range() if the index is not valid.
   */
  T& operator[](const size_type idx)
  {
    if (idx >= mSize || idx < 0) throw std::out_of_range("index out of range");
    size_type i = mFront + idx;
    if (i >= mElements.size()) i -= mElements.size();
    return mElements[i];
  }

  //
  // OPERATIONS
  //

public:

  /**
   * Return the number of elements inserted in the buffer.
   */
  size_type size() const
  {
    return mSize;
  }

  /**
   * Return the capacity of the buffer.
   */
  size_type capacity() const
  {
    return mElements.size();
  }

  /**
   * Return true if the buffer is empty, false otherwise.
   */
  bool empty() const
  {
    return mSize == 0;
  }

  /**
   * Remove all the elements.
   * @remark The elements are not destroyed, instead the size
   * counter is reset.
   */
  void clear()
  {
    mSize = 0;
    mBack = 0;
    mFront = 0;
  }

  /**
   * Insert an element at the end of the circular buffer.
   * If the circular buffer is full, it will overwrite the
   * first element, that is the front() element.
   */
  void push_back(const T& x)
  {
    if (mSize == mElements.size()) 
    {
      pop_front();
    }
    mSize++;
    mElements[mBack] = x;
    mBack++;
    if (mBack == mElements.size()) mBack = 0;
  }

  /**
   * Insert an element at the beginning of the circular buffer.
   * If the circular buffer is full, it will overwrite the
   * last element, that is the back() element.
   */
  void push_front(const T& x)
  {
    if (mSize == mElements.size()) 
    {
      pop_back();
    }
    mSize++;
    if (mFront == 0)
    {
      mFront = mElements.size()-1;
    }
    else
    {
      mFront--;
    }
    mElements[mFront] = x;
  }

  /**
   * Remove the front element.
   * @throw std::logic_error() if the list is empty.
   */
  void pop_front()
  {
    if (empty()) throw std::logic_error("The circular buffer is empty");

    mSize--;
    // call destructor explicitly to remove dangled element
    mElements[mFront].~T();
    mFront++;
    if (mFront == mElements.size()) mFront = 0;
  }

  /**
   * Remove the back element.
   * @throw std::logic_error() if the list is empty.
   */
  void pop_back()
  {
    if (empty()) throw std::logic_error("The circular buffer is empty");

    mSize--;
    mElements[mBack].~T();
    if (mBack == 0)
    {
      mBack = mElements.size()-1;
    }
    else
    {
      mBack--;
    }
  }

  /**
   * Retrieve the first element in the buffer list.
   * @throw std::logic_error() if the list is empty.
   */
  const T& front() const
  {
    if (empty()) throw std::logic_error("The circular buffer is empty");
    return mElements[mFront];
  }

  /**
   * Retrieve the last element in the buffer list.
   * @throw std::logic_error() if the list is empty.
   */
  const T& back() const
  {
    if (empty()) throw std::logic_error("The circular buffer is empty");
    return mElements[mBack == 0 ? capacity()-1 : mBack-1];
  }

  /**
   * Set the capacity of the buffer.
   * @remark This will force a clear operation.
   */
  void setCapacity(size_type n)
  {
    clear();
    mElements.resize(n);
  }

  //
  // ATTRIBUTES
  //

protected:

  /**
   * Current size of the buffer.
   */
  size_type mSize;

  /**
   * Index of the first element in the container.
   */
  size_type mFront;

  /**
   * Index of one past the last element in the container.
   */
  size_type mBack;

  /**
   * The container of the elements.
   */
  std::vector<T> mElements;

private:

  void copy(const CircularBuffer& from)
  {
    mSize = from.mSize;
    mFront = from.mFront;
    mBack = from.mBack;
    mElements = from.mElements;
  }

};

}; // namespace ait

#endif // CircularBuffer_HPP


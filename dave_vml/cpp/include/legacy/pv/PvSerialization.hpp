/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef __PvSerialization_HPP__
#define __PvSerialization_HPP__
//////////////////////////////////////////////////////////////////////////
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/version.hpp>

#include <ait/Vector3.hpp>
#include <ait/Rectangle.hpp>
#include <ait/BasicMatrix.hpp>
#include <ait/Matrix.hpp>
#include <ait/Image.hpp>

/**
* Functions for enabling Simple Object Access Protocol for pv library.
*
* @par Responsibilities:
* - Support SOAP for PvLibrary.
*
* @Remark
*   Note: Serialization of floating point numbers will result in rounding off 
*         precision problems (typical problems of dealing with floating point operations).
*
* @test
*   The testing code for this class and Serialization.hpp is given in:
*   <root>\ai\src\test\test_executive\TestSerialization.cpp and <root>\ai\src\test\test_executive\SerializationDataStructure.hpp
*/

namespace boost
{
  namespace serialization
  {
    /**
     * Implementation of Serialization/Deserialization for Vector3<T> library.
     * Note: Serialization of floats/doubles may lead to precision rounding off problems.
     */
    template<class Archive, class T>
      void serialize(Archive& ar, Vector3<T>& point, const unsigned int version)
    {
      T& x = point.x;
      T& y = point.y;
      T& z = point.z;
      ar & BOOST_SERIALIZATION_NVP(x);
      ar & BOOST_SERIALIZATION_NVP(y);
      ar & BOOST_SERIALIZATION_NVP(z);
    }

    /**
     * Implementation of Serialization/Deserialization for Rectangle<T> library.
     * Note: Serialization of floats/doubles may lead to precision rounding off problems.
     */
    template<class Archive, class T>
      void serialize(Archive& ar, Rectangle<T>& rect, const unsigned int version)
    {
      T& x0 = rect.x0;
      T& y0 = rect.y0;
      T& x1 = rect.x1;
      T& y1 = rect.y1;
      ar  & BOOST_SERIALIZATION_NVP(x0)
          & BOOST_SERIALIZATION_NVP(y0)
          & BOOST_SERIALIZATION_NVP(x1)
          & BOOST_SERIALIZATION_NVP(y1);
    }

    //////////////////////////////////////////////////////////////////////////
    /// PvBasicMatrixSerialization
    /**
     * Implementation of Serialization/Deserialization for BasicMatrix<T> library.
     * It stores the complete BasicMatrix in the archives.
     *
     * @NOTE: When the matrix is saved in an XML archive, then the name tags would be 
     *        the names of the local variables and not the Class Member. The value is 
     *        correct but just not the same. This happens, because the templates do
     *        not properly store the name of the class data members. This could be 
     *        rectified by defining "make_nvp" for the template serialization.
     */
    template<class Archive, class T>
      void serialize(Archive& ar, BasicMatrix<T>& matrix, const unsigned int version)
    {
      boost::serialization::split_free(ar, matrix, version);
    }

    template<class Archive, class T>
      void save(Archive& ar, const BasicMatrix<T>& matrix, const unsigned int version)
    {
      uint nr = matrix.rows();
      uint nc = matrix.cols();
      ar << BOOST_SERIALIZATION_NVP(nr);
      ar << BOOST_SERIALIZATION_NVP(nc);

      uint i, s=nr*nc;
      T *dst=matrix.pointer();
      for(i=0;i<s;i++) 
        ar << BOOST_SERIALIZATION_NVP(*(dst++));
    }

    template<class Archive, class T>
      void load(Archive& ar, BasicMatrix<T>& matrix, const unsigned int version)
    {
      uint nr = 0;
      uint nc = 0;
      ar >> BOOST_SERIALIZATION_NVP(nr);
      ar >> BOOST_SERIALIZATION_NVP(nc);

      matrix.resize(nr, nc);
      uint i, s=nr*nc;
      T *dst=matrix.pointer();
      for(i=0;i<s;i++) 
        ar >> BOOST_SERIALIZATION_NVP(*(dst++));
    }
    //////////////////////////////////////////////////////////////////////////
    

    /**
     * Implementation of Serialization/Deserialization for Matrix<T> library.
     * It is a wrapper function for using BasicMatrix Serialization.
     */
    template<class Archive, class T>
      void serialize(Archive& ar, Matrix<T>& matrix, const unsigned int version)
    {
      BasicMatrix<T>& basicMatrix = matrix;
      serialize(ar, basicMatrix, version);
    }

    /**
     * Implementation of Serialization/Deserialization for Image<T> library.
     * It is a wrapper function for using BasicMatrix Serialization.
     */
    template<class Archive, class T>
      void serialize(Archive& ar, Image<T>& matrix, const unsigned int version)
    {
      BasicMatrix<T>& basicMatrix = matrix;
      serialize(ar, basicMatrix, version);
    }

    /**
     * Implementation of Serialization/Deserialization for Image8 library.
     * It is a wrapper function for using BasicMatrix Serialization.
     */
    template<class Archive>
      void serialize(Archive& ar, Image8& matrix, const unsigned int version)
    {
      BasicMatrix<unsigned char>& basicMatrix = matrix;
      serialize(ar, basicMatrix, version);
    }

    //////////////////////////////////////////////////////////////////////////
    /// Image32
    /**
     * Implementation of Serialization/Deserialization for Image8 library.
     */
    template<class Archive>
      void serialize(Archive& ar, Image32& matrix, const unsigned int version)
    {
      BasicMatrix<unsigned int>& basicMatrix = matrix;
      serialize(ar, basicMatrix, version);

      boost::serialization::split_free(ar, matrix, version);
    }

    template<class Archive>
      void save(Archive& ar, const Image32& matrix, const unsigned int version)
    {
      bool alpha = matrix.getAlphaFlag();
      int internalFormat = matrix.getInternalFormat();
      ar << BOOST_SERIALIZATION_NVP(alpha);
      ar << BOOST_SERIALIZATION_NVP(internalFormat);
    }

    template<class Archive>
      void load(Archive& ar, Image32& matrix, const unsigned int version)
    {
      bool alpha;
      int internalFormat;
      ar >> BOOST_SERIALIZATION_NVP(alpha);
      ar >> BOOST_SERIALIZATION_NVP(internalFormat);

      matrix.setAlphaFlag(alpha);
      matrix.setInternalFormat(internalFormat);
    }
    //////////////////////////////////////////////////////////////////////////

  };//namespace serialization
};//namespace boost

//////////////////////////////////////////////////////////////////////////
#endif __PvSerialization_HPP__
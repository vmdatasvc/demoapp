#ifndef PVMATRIXCONVERTER_HPP
#define PVMATRIXCONVERTER_HPP
#include <legacy/pv/ait/Matrix.hpp>

namespace ait
{

class PvMatrixConverter
{
  static void convert(const Matrix<double> &in, Matrix<float> &out);
};

} // namespace ait

#endif



/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef PVSPLINE_HPP
#define PVSPLINE_HPP

#include <legacy/pv/ait/Matrix.hpp>

namespace ait
{

class PvSpline
{
protected:

  bool pointsValid;

  unsigned int channels;
  unsigned int n;

  Matrix<float> points;
  Matrix<float> derivatives;

public:
  PvSpline(void) : pointsValid(false) {}

  void setControlPoints(Matrix<float> &points);
  void interpolate(float t, Matrix<float> &x);
};

} // namespace ait

#endif

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef PVCUBE_HPP
#define PVCUBE_HPP

#include <iostream>

#include <math.h>

#include "PvUtil.hpp"
#include <ait/Vector3.hpp>

#ifndef uint
#define uint unsigned int
#endif

namespace ait
{

/*********************************************************************
 ** 
 ** PvCube Template Class
 **
 *********************************************************************
 ** 
 ** Description:
 **
 *********************************************************************/

template<class T> class PvCube
{
public:

  PvCube(const Vector3<T> p0, const Vector3<T> p1) { set(p0,p1); };
	
  PvCube(void) { set(Vector3<T>(0,0,0),Vector3<T>(0,0,0)); };

  PvCube(const PvCube& v) { p0 = v.p0; p1 = v.p1; };

  virtual ~PvCube(void) {};

  PvCube& operator= (const PvCube& v) {
    if(this != &v) {
		  p0 = v.p0; 
		  p1 = v.p1; 
    }
    return *this;
  };
	
	bool operator== (const PvCube& v) const {
		return ((p0 == v.p0) && (p1 == v.p1) );
	}

  bool operator!= (const PvCube& v) const { return !(*this == v); }

  void set(const Vector3<T>& pp0, const Vector3<T>& pp1) {   
PVASSERT(!(pp0.x>pp1.x || pp0.y>pp1.y || pp0.z>pp1.z));
		p0 = pp0; p1 = pp1;
		pos.sub(pp1,pp0);
  }

  void zero(void) { set(Vector3<T>(0,0,0),Vector3<T>(0,0,0)); };

  T width(int dim) const { return p1(dim) - p0(dim); };

	void setPos(T x, T y, T z) { setPos(Vector3<T>(x,y,z)); };
  void setPos(Vector3<T>& pt) { p0.sub(pos);p0.add(p); p1.sub(pos);p1.add(p); pos=p;};

	Vector3<T> getPos() { return pos; };

  void setSize(T wx, T wy, T wz) { 
		set(Vector3<T>(pos(0)-wx/2,pos(1)-wy/2,pos(2)-wz/2), Vector3<T>(pos(0)+wx/2,pos(1)+wy/2,pos(2)+wz/2)); 
	};
  void setSize(Vector3<T>& pt) { setSize(pt(0),pt(1),pt(2)); };
  Vector3<T> getSize() { return Vector3<T>(width(0),width(1),width(2)); };

  /// check if the supplied position is inside the cube
  bool inside(const Vector3<T> &p) const { return (p>=p0 && p<=p1); };

  /// check if the supplied cube is inside this cube
  bool inside(const PvCube<T> &subRect) const { return (subRect.p0>=p0 && subRect.p1<=p1); };


  // I/O operators
  friend std::ostream& operator<<(std::ostream &stream, const PvCube& s) {
    stream << s.p0 << " " << s.p1;
    return stream;
  };

  friend std::istream& operator>>(std::istream &stream, PvCube& s) {  
    stream >> s.p0 >> s.p1;
    return stream;
  };

	Vector3<T> p0;  //closer lower left corner
	Vector3<T> p1;  //farther upper right corner
	Vector3<T> pos; //cube center
};

// Shorcut Macros

typedef PvCube<int> PvCubei;
typedef PvCube<float> PvCubef;
typedef PvCube<double> PvCubed;

} // namespace ait

#endif

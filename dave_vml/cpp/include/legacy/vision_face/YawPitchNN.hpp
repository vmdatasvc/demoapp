#include "ModelPerson.hpp"
#include "DiskMatDataSet.h"
#include "ClassFormatDataSet.h"
#include "ClassNLLCriterion.h"
#include "MSECriterion.h"
#include "OneHotClassFormat.h"
#include "ClassMeasurer.h"
#include "MSEMeasurer.h"

#include "StochasticGradient.h"
#include "KFold.h"

#include "ConnectedMachine.h"
#include "Linear.h"
#include "Tanh.h"
#include "LogSoftMax.h"

#include "MeanVarNorm.h"
#include "DiskXFile.h"
#include "CmdLine.h"
#include "Random.h"

#include "lssvr.hpp"

using namespace std;
using namespace Torch;

class YawPitchNN
{

public:

  CmdLine cmd;

  string modelFile, modelDir;
  int imgWidth, imgHeight;
  int n_inputs;
  int n_targets;
  int n_hu1, n_hu2, n_hu3;
  real weight_decay;

  Lsv *lsv;
  ConnectedMachine mlp;
  MeanVarNorm *mvNorm;

  FILE *fp;
  float estYaw, estPitch;
  
  // Constructor
  YawPitchNN()
  {
  }

  // Destructor
  virtual ~YawPitchNN()
  {
  }
  
  void init(string modelFile_, int imgWidth_, int imgHeight_);

  void get_args();
  void estimateYawPitch(Image8 faceImage, Image8& outImage, float X, float Y, float S, float O);
};
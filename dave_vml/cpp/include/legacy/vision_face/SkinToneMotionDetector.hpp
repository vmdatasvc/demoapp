/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef SkinToneMotionDetector_HPP
#define SkinToneMotionDetector_HPP

// SYSTEM INCLUDES
//
#include <boost/utility.hpp>
#include <string>
#include <vector>

// AIT INCLUDES
//

#include <legacy/pv/ait/Image.hpp>
#include <legacy/pv/PvImageConverter.hpp>
#include "legacy/vision_face/SkinToneDetector.hpp"


// LOCAL INCLUDES
//

//#include "SkinToneMotionDetector_fwd.hpp"

// FORWARD REFERENCES
//

//#include "SkinToneMotionDetector_fwd.hpp"

namespace ait 
{

namespace vision
{


/**
 * This vision module is not yet documented.
 * @par Responsibilities:
 * - Write the responsibilities here.
 */
class SkinToneMotionDetector
{
public:

  //
  // LIFETIME
  //

  /**
   * Constructor. The constructor creates a quick instance. Expensive operations
   * are executed in the init() function.
   */
  SkinToneMotionDetector();

  /**
   * Destructor
   */
  virtual ~SkinToneMotionDetector();

public:

protected:

  //
  // ACCESS
  //

//	Image32 mMotionImage;

public:

	void detectSkinToneMotion(Image32* curImage, Image32* prevImage, Image8* skinToneImage, Image8* motionImage);

};


}; // namespace vision

}; // namespace ait

#endif // SkinToneMotionDetector_HPP



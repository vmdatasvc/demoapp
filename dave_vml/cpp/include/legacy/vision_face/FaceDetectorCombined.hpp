/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef FaceDetectorCombined_HPP
#define FaceDetectorCombined_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

#include "legacy/types/String.hpp"
#include "legacy/vision_face/FaceDetector.hpp"
#include "legacy/vision_face/FaceDetectorAdaBoost.hpp"
#include "legacy/vision_face/FaceDetectorNN.hpp"

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

//#include "FaceDetectorCombined_fwd.hpp"

namespace ait 
{

namespace vision
{


/**
 * This class is not yet documented.
 * Here is a more detailed description of the class. Next come the 
 * Responsibilities of the class. Try to minimize the responsibilities, this
 * will help in creating a good design. Next, optionally write any design patterns
 * that were used for this class. Refer to the "Design Patterns" book by Gamma et al.
 * @par Responsibilities:
 * - The First Responsibility
 * - The Second Responsibility:
 *   - A sub-responsibility
 *   - Another sub-responsibility
 * - The Third Responsibility
 * @par Patterns:
 * - The first design pattern
 * - The second design pattern
 * @par Design Considerations:
 * - Here you can itemize some justifications for your decisions. It will help
 * others to understand why your class is the way it is.
 * @remarks
 * Some special comments or warnings about this class.
 * @see TheirClass, OurClass (other related classes).
 */
class FaceDetectorCombined : public FaceDetector
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  FaceDetectorCombined();

  /**
   * Destructor
   */
  virtual ~FaceDetectorCombined();

  /**
   * Initialization.
   */
  void init(std::string adaModelFile);

  //
  // OPERATIONS
  //

public:

  //
  // ACCESS
  //

public:

  /**
   * Return member1
   */
  void detect(const Image8 &grayImage, bool asynch = true);

  /**
   * Call FaceDetectorAdaBoost::setNumNeighbors();
   */
  void setNumNeighbors(int n) { mFaceDetectAB.setNumNeighbors(n); }


public:

  /**
   * Ask if this object has been initialized (the init() function has been called).
   */
  Bool isInitialized() const { return mIsInitialized; }

  //
  // IO
  //


protected:

  /**
   * Tells if this object has been initialized.
   */
  Bool mIsInitialized;

  /**
   * Member description.
   */
  FaceDetectorAdaBoost mFaceDetectAB;

  /**
   * Member description.
   */
  FaceDetectorNN mFaceDetectNN;

};


}; // namespace vision

}; // namespace ait

#endif // FaceDetectorCombined_HPP


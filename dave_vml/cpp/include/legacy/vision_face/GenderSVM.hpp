#ifndef GENDERSVM_HPP
#define GENDERSVM_HPP

#include <legacy/pv/ait/Image.hpp>

#include "legacy/math/SVM.hpp"

using namespace std;

namespace ait
{

class GenderSVM
{

public:

 // SVM<unsigned char> gSvm;

  SVM<float> gSvm;

  string modelFile;
  int imgWidth, imgHeight;

  // Constructor
  GenderSVM()
  {
  }

  // Destructor
  virtual ~GenderSVM()
  {
  }
  
  void init(string modelFile, int imgWidth, int imgHeight);

  void correctLeqImage(Image8 faceImage, Image8& outImage, float X, float Y, float S, float O);
  float classify(Image8 faceImage, Image8& outImage, float X, float Y, float S, float O);
  float classify(Image8 faceImage);
};

} // namespace ait

#endif //GENDERSVM_HPP

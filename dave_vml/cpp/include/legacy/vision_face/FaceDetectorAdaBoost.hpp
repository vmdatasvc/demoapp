/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef FaceDetectorAdaBoost_HPP
#define FaceDetectorAdaBoost_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

#include "legacy/types/String.hpp"    // DV

// LOCAL INCLUDES
//

#include "FaceDetector.hpp"

//#include "cv.h"
//#include "cvaux.h"
//#include <iostream.h>

// FORWARD REFERENCES
//

#include "FaceDetectorAdaBoost_fwd.hpp"

namespace ait 
{

namespace vision
{


/**
 * Implements AdaBoost face detector. Uses OpenCV implementation.
 * @par Responsibilities:
 * - Implements AdaBoost face detector. Uses OpenCV implementation
 * @par Design Considerations:
 * - Here you can itemize some justifications for your decisions. It will help
 * others to understand why your class is the way it is.
 * @remarks
 * Some special comments or warnings about this class.
 * @see TheirClass, OurClass (other related classes).
 */
class FaceDetectorAdaBoost : public FaceDetector
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  FaceDetectorAdaBoost();

  /**
   * Destructor
   */
  virtual ~FaceDetectorAdaBoost();

  /**
   * Initialization.
   */
  void init(const std::string modelName);

  //
  // OPERATIONS
  //

public:

  /**
   * Perform the actual detection.
   */
  void detect(const Image8 &image, bool asynch = true);

  //
  // ACCESS
  //

public:

  /**
   * @see mNumNeighbors.
   */
  void setNumNeighbors(const int n) { mNumNeighbors = n; }

  //
  // INQUIRY
  //



  //
  // ATTRIBUTES
  //

protected:

#ifdef OLDCV
  //CvHidHaarClassifierCascade* mpCascade;

  CvHaarClassifierCascade* mpCascade;

#else	// For ver 2.4.11
  cv::CascadeClassifier	*mpCascade;

#endif

  bool mIsInitialized;

  /**
   * Number of faces that must be in the neighborhood to tell that
   * there is a face in a particular location. This is a parameter
   * for cvHaarDetectObjects().
   */
  int mNumNeighbors;

};


}; // namespace vision

}; // namespace ait

#endif // FaceDetectorAdaBoost_HPP


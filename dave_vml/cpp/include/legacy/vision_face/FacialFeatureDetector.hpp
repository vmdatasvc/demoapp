/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
/*
  TO ADD THIS NEW MODULE TO THE VISION FRAMEWORK YOU HAVE TO REGISTER BEFORE INITIALIZING
  VISION BY CALLING:

  1.- Add this line in the beginning of your main file

#include "FacialFeatureDetector.hpp"

  2.- Add the following in the beginning of the main function.

  vision::VisionFactory::instance()->registerModule("FacialFeatureDetector",
    vision::VisionFactory::ModuleCreatorPtr(new vision::FacialFeatureDetector::Creator()));

  3.- DELETE THIS COMMENT.
*/

/*******************************************************************************************/
/*                   (C) Copyright Advanced Interface Technologies, Inc.                   */
/*                                  All Rights Reserved                                    */
/*******************************************************************************************/

#ifndef FacialFeatureDetector_HPP
#define FacialFeatureDetector_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//
#include <ait/Image.hpp>


namespace ait 
{

namespace vision
{

class FacialFeatureDetector 
{
  
public:

  FacialFeatureDetector();
  virtual ~FacialFeatureDetector();

public:

  virtual void process();
  void EyeDetect(Image8 &faceImage);

protected:


public:

  
};


}; // namespace vision

}; // namespace ait

#endif // FacialFeatureDetector



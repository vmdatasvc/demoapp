#include "legacy/vision_face/ModelPerson.hpp"

#include "DiskMatDataSet.h"
#include "ClassFormatDataSet.h"
#include "ClassNLLCriterion.h"
#include "MSECriterion.h"
#include "OneHotClassFormat.h"
#include "ClassMeasurer.h"
#include "MSEMeasurer.h"

#include "StochasticGradient.h"
#include "KFold.h"

#include "ConnectedMachine.h"
#include "Linear.h"
#include "Tanh.h"
#include "LogSoftMax.h"

#include "MeanVarNorm.h"
#include "DiskXFile.h"
#include "CmdLine.h"
#include "Random.h"

using namespace ait;

class SparseGeometricNN
{

public:

  Torch::CmdLine cmd;

  std::string modelListFile, modelDir, imageListFile;
  int n_inputs;
  int n_targets;
  int n_hu1, n_hu2, n_hu3;
  real weight_decay;

  ModelPerson mPerson[200];
  Torch::ConnectedMachine mlp[200];
  std::vector< Torch::MeanVarNorm * > mvNorm;
  int numInputTmp[200];
  int numModel;

  float midEyeX, midEyeY, scale, orien;
  FILE *fp;
  float estMidEyeX, estMidEyeY, estMidEyeMouthDist, estOrientation;
  float est1MidEyeX, est1MidEyeY, est1MidEyeMouthDist, est1Orientation;
  float est2MidEyeX, est2MidEyeY, est2MidEyeMouthDist, est2Orientation;

  
  // Constructor
  SparseGeometricNN()
  {
  }

  // Destructor
  virtual ~SparseGeometricNN()
  {
  }
  
//  SparseGeometricNN(std::string modelListFile_, std::string modelDir_);
  void init(std::string modelListFile_, std::string modelDir_);

  void get_args();
  void applyModel(int numEstModel, int wgt1, int wgt2, Image8 faceImage);
  void estimateModel(int numEstModel, int wgt1, int wgt2, Image8 faceImage, float detecX, float detecY, int detecW, int detecH);
  void estimateModel(int numEstModel, Image8 faceImage, float detecX, float detecY, int detecW, int detecH);
  void estimateModel(Image8 faceImage, float detecX, float detecY, int detecW, int detecH);
  void estimateModelFeaturesInWindows(int numEstModel, int wgt1, int wgt2, Image8 faceImage, 
	  float gtLeftEyeX, float gtLeftEyeY, float gtRightEyeX, float gtRightEyeY, 
	  float gtNoseX, float gtNoseY, float gtMouthX, float gtMouthY,
	  float detecX, float detecY, int detecW, int detecH);
};

void affine(float *vecX, float *vecY, float scale, Matrix<float> rotMat, Matrix<float> rotCenter, Matrix<float> trans);

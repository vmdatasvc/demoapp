#ifndef SPARSEYAWPITCHNN_HPP
#define SPARSEYAWPITCHNN_HPP

#include "legacy/vision_face/ModelPerson.hpp"

#include "torch3/src/DiskMatDataSet.h"
#include "torch3/src/ClassFormatDataSet.h"
#include "torch3/src/ClassNLLCriterion.h"
#include "torch3/src/MSECriterion.h"
#include "torch3/src/OneHotClassFormat.h"
#include "torch3/src/ClassMeasurer.h"
#include "torch3/src/MSEMeasurer.h"

#include "torch3/src/StochasticGradient.h"
#include "torch3/src/KFold.h"

#include "torch3/src/ConnectedMachine.h"
#include "torch3/src/Linear.h"
#include "torch3/src/Tanh.h"
#include "torch3/src/LogSoftMax.h"

#include "torch3/src/MeanVarNorm.h"
#include "torch3/src/DiskXFile.h"
#include "torch3/src/CmdLine.h"
#include "torch3/src/Random.h"

#include "legacy/vision_face/lssvr.hpp"

namespace ait
{

class SparseYawPitchNN
{

public:

  typedef struct _FileParams
  {
    char *valid_file;
    char *file;

    int max_load;
    int max_load_valid;
    real accuracy;
    real learning_rate;
    real decay;
    int max_iter;
    int the_seed;

    char *dir_name;
    char *model_file;
    int k_fold;
    bool binary_mode;
    bool regression_mode;
    int class_against_the_others;
  } FileParams;

  FileParams mFileParams;

  Torch::SafeRandom mRandom;
  
  Torch::CmdLine mCmd;

  std::string mModelListFile, mModelDir, mLsvFile;
  int imgWidth, imgHeight;
  int numInputs;
  int numTargets;
  int numHiddenUnits1, numHiddenUnits2, numHiddenUnits3;
  real weight_decay;

  Lsv *mLsv;
//  Torch::ConnectedMachine mMlp[25];
  std::vector< Torch::ConnectedMachine * > mMlp;
  std::vector< Torch::MeanVarNorm *> mMvNorm;
//  float mModelYaw[25], mModelPitch[25];
  std::vector< float > mModelYaw;
  std::vector< float > mModelPitch;
//  int numInputTmp[25];
  int numModel;
  int numCloseModel;

  float mYaw, mPitch;
  FILE *fp;
  float mEstYaw, mEstPitch;
  
  // Constructor
  SparseYawPitchNN()
  {
  }

  // Destructor
  virtual ~SparseYawPitchNN()
  {
    finish();
  }
  
  void init(std::string modelListFile_, std::string modelDir_, int imgWidth_, int imgHeight_);
  void init(std::string modelListFile_, std::string modelDir_, std::string lsvFile_, int imgWidth_, int imgHeight_);

  void finish()
  {
    for (int i = 0; i < mMlp.size(); i++)
    {
      delete mMlp[i];
    }
    mMlp.clear();
    for (int i = 0; i < mMvNorm.size(); i++)
    {
      delete mMvNorm[i];
    }
    mMvNorm.clear();
    mModelYaw.clear();
    mModelPitch.clear();
  }

  void get_args();
  void SparseYawPitchNN::estimateYawPitch(int numEstModel, Image8 faceImage, Image8& outImage, float X, float Y, float S, float O);
  void affine(float *vecX, float *vecY, float scale, Matrix<float> rotMat, Matrix<float> rotCenter, Matrix<float> trans);
};

typedef boost::shared_ptr<SparseYawPitchNN> SparseYawPitchNNPtr;

//void affine(float *vecX, float *vecY, float scale, Matrix<float> rotMat, Matrix<float> rotCenter, Matrix<float> trans);

} // namespace ait

#endif

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef FaceVerifyClassifyMod_HPP
#define FaceVerifyClassifyMod_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

#include <String.hpp>
#include <ait/Image.hpp>
#include <PvImageConverter.hpp>

// LOCAL INCLUDES
//

#include "RealTimeMod.hpp"

// FORWARD REFERENCES
//

//#include "FaceVerifyClassifyMod_fwd.hpp"
#include <ForegroundSegmentMod.hpp>
#include <face_detector/FaceDetectorAdaBoost.hpp>
#include <face_detector/FaceDetectorAdaBoostForeground.hpp>
#include "facial_geometry_estimator/Arclet3DNN.hpp"
#include "face_classifier/GenderTorchSVM.hpp"
#include "face_classifier/GenderSVM.hpp"
#include "facial_geometry_estimator/SparseYawPitchNN.hpp"
#include "facial_geometry_estimator/ModelPerson.hpp"
#include <CircularBuffer.hpp>
#include "face_detector/SkinToneDetector.hpp"
#include <VmsPacket_fwd.hpp>

#include <face_detector/FaceDetectorCombined.hpp>
#include <face_detector/FaceDetectorCombined_fwd.hpp>

#include "face_classifier/GenderTorchNN.hpp"

namespace ait 
{

namespace vision
{


/**
 * This vision module is not yet documented.
 * @par Responsibilities:
 * - Write the responsibilities here.
 */
class FaceVerifyClassifyMod : public RealTimeMod
{
public:

  class FaceData
  {
  public:

    FaceData(int mId0, int isFrontLeftRight0, double time0, unsigned int x0, unsigned int y0, 
      unsigned int w0, unsigned int h0) : mId(mId0), isFrontLeftRight(isFrontLeftRight0), x(x0), y(y0), w(w0), h(h0), 
      time(time0), mFaceMarginPixels(10)
    {
    }
  
    FaceData(double time0, unsigned int x0, unsigned int y0, 
      unsigned int w0, unsigned int h0) : x(x0), y(y0), w(w0), h(h0), 
      time(time0), mFaceMarginPixels(10)
    {
    }
  
    /**
     * An integer id associated with each face, useful for debugging.
     */
    int mId;

    // 0 frontal, 1 left, 2 right, 3 face detectors
    int isFrontLeftRight;
	
    unsigned int x;
    unsigned int y;
    unsigned int w;
    unsigned int h;

    //midpoint of segment connecting 2 eyes
    float ex;
    float ey;

    //distance between (above point) and uypper lip
    float sz;
    float or;

    //yaw of face
    float yw;

    //pitch of face
    float pt;

    //previous yaw, previous pitch
    float prYw;
    float prPt;

	//whether the face is frontal based on the 3D pose estimation
	int is3DFrontal;

    double time;

    Image8 mFaceChip;

    //integer values of demographics classification
    unsigned int iGender;
    unsigned int iEthnicity;
    unsigned int iAge;

    //scores of demographics classification
    float mGenderScore, mAfrScore, mCauScore, mHisScore, mOriScore;
    float mAge;
    unsigned int getId() {return mId;}

    //decide wither the given query window has an overlap with the 'this' window
    bool isContaining(int queryX, int queryY, int queryW, int queryH, bool enforceSize = false)
    {
      int q0X = queryX;        // Upper left corner
      int q0Y = queryY;
      int q1X = q0X + queryW;  // Upper right corner
      int q1Y = q0Y;
      int q2X = q0X;           // Lower left corner
      int q2Y = q0Y + queryH; 
      int q3X = q1X;           // Lower right corner
      int q3Y = q2Y;

      bool overlap = false;
      bool greaterThan = false;

      int startX = this->x;
      int endX = this->x + this->w;
      int startY = this->y;
      int endY = this->y + this->h;

//      PVMSG("%d<->%d && %d<->%d\n",startX,endX,startY,endY);
//      PVMSG("(%d,%d) (%d,%d) (%d,%d) (%d,%d)\n",q0X,q0Y,q1X,q1Y,q2X,q2Y,q3X,q3Y);
//
      if(startX <= q0X && q0X <= endX && startY <= q0Y && q0Y <= endY)
      {
        overlap = true;
      }
      if(startX <= q1X && q1X <= endX && startY <= q1Y && q1Y <= endY)
      {
        overlap = true;
      }
      if(startX <= q2X && q2X <= endX && startY <= q2Y && q2Y <= endY)
      {
        overlap = true;
      }
      if(startX <= q3X && q3X <= endX && startY <= q3Y && q3Y <= endY)
      {
        overlap = true;
      }
      
      if(queryW <= this->w && queryH <= this->h)
        greaterThan = true;

//      if(enforceSize)
//        overlap = overlap && greaterThan;

      return overlap;
    }

    Rectanglei getRect()
    {
      return Rectanglei(x,y,x+w,y+h);
    }

    const int mFaceMarginPixels;

    Image32Ptr createFaceImage(const Image32& fullImage)
    {
      Rectanglei cropRect(x-mFaceMarginPixels,y-mFaceMarginPixels,
        x+w+mFaceMarginPixels,y+h+mFaceMarginPixels);
      cropRect.intersect(Rectanglei(0,0,fullImage.width(),fullImage.height()));
      Image32Ptr pFaceImage;
      pFaceImage.reset(new Image32(cropRect.width(),cropRect.height()));
      pFaceImage->crop(fullImage,cropRect.x0,cropRect.y0,cropRect.width(),cropRect.height());
      return pFaceImage;
    }

  };

  typedef boost::shared_ptr<FaceData> FaceDataPtr;

  class FaceTrack
  {
  public:

    unsigned int getId() {return mId;}

    FaceTrack(unsigned int id, unsigned int maxSize = 5000)
    {
      mId = id;
      mData.setCapacity(maxSize);
      mGenderScore = 0.0;
      mAfrScore = 0.0;
      mCauScore = 0.0;
      mHisScore = 0.0;
      mOriScore = 0.0;
      numAdBFace = 1;
      lastAdBFace = 0;
      numInstances = 0;
//      isClassified = false;
      realTimeDataSent = false;
      mAgeSum = 0.0;
//      mFaceChip.resize(24,24);
      mFaceChip.resize(30,30);
      mFaceChip.setAll(0);
      mMaxFaceImages = 5;
    }

    void addFace(const Image32& img, FaceDataPtr pFace)
    {
      if (mFaceImages.size() < mMaxFaceImages -1)
      {
        mFaceImages.push_back(pFace->createFaceImage(img));
      }
      mData.push_back(pFace);
    }

    unsigned int numFaces() {return mData.size();}

    double lastFaceTime()
    {
      if (mData.size() == 0)
        return 0.0;
      else return mData.back()->time;
    }    

    double firstFaceTime()
    {
      if (mData.size() == 0)
      {
        return 0.0;
      }
      else
      {
        return mData.front()->time;
      }
    }

    double lastAdBFaceTime()
    {
      if(numAdBFace==0)
        return 0.0;
      else
        return mData[lastAdBFace]->time;
    }    

    FaceDataPtr getLatestFace() const { return mData.back(); }

	FaceDataPtr getFace(int i) const { return mData[i]; }

    Vector2f getPredictedPos(double time) const
    {      
      PVASSERT(mData.size() > 0);

      if (mData.size() == 1)
      {
        Vector2f pos;
        pos.x = mData.back()->x;
        pos.y = mData.back()->y;
        return pos;
      }
      else
      {
        int last = mData.size() - 1;
        double xDelta = mData[last]->x - mData[last-1]->x;
        double yDelta = mData[last]->y - mData[last-1]->y;

        double timeDiff = mData[last]->time - mData[last-1]->time;
        PVASSERT(timeDiff != 0.0);
//        xDelta /= timeDiff;
//        yDelta /= timeDiff;

        // Use constant time delta to make the prediction safer
        double timeChange = 1.0;//time - mData[last]->time;
        PVASSERT(timeChange > 0.0);

        Vector2f predictedPos;
        predictedPos.x = mData[last]->x + (int)(xDelta*timeChange);
        predictedPos.y = mData[last]->y + (int)(yDelta*timeChange);
      
        return predictedPos;
      }
    }

    float getPredictedSize(double time) const
    {      
      PVASSERT(mData.size() > 0);

      if (mData.size() == 1)
      {
        int size;
        size = mData.back()->w;
        return size;
      }
      else
      {
        int last = mData.size() - 1;
 
        double timeDiff = mData[last]->time - mData[last-1]->time;
        PVASSERT(timeDiff != 0.0);

        double timeChange = time - mData[last]->time;
        PVASSERT(timeChange > 0.0);

        float predictedSize;
        predictedSize = mData[last]->w * (1.0 + timeChange*0.125);
      
//        PVMSG("last sz=%f w=%d predicted=%f",mData[last]->sz,mData[last]->w,predictedSize);

        return predictedSize;
      }
    }

    int getSizeVariation(bool useNormalizedDistanceValues = false)
    {      
      PVASSERT(mData.size() > 0);

      int accumSize = 0.0;

      for(int i=0; i<mData.size()-1; i++)
      {
        int diff = abs((int)mData[i+1]->w - (int)mData[i]->w);
		if(useNormalizedDistanceValues)
			diff /= mData[i+1]->w;
        accumSize += diff;

//          PVMSG("%d - %d = %d -> %d, ",mData[i]->w,mData[i-1]->w,diff,accumSize);
      }
        return accumSize;

    }

    float getTraveledDist(void)
    {      
      PVASSERT(mData.size() > 0);
      float dist, accumDist = 0.0;

      if (mData.size() == 1)
      {
        accumDist = 0.0;
      }
      else
      {
        for(int i=1; i<mData.size(); i++)
        {
//          if(mData[i]->isFrontLeftRight==0)
          {
            double xDelta = (float)mData[i]->x - (float)mData[i-1]->x;
            double yDelta = (float)mData[i]->y - (float)mData[i-1]->y;
            dist = sqrt(xDelta*xDelta + yDelta*yDelta) / ((float)mData[i]->w);
            accumDist += dist;
//          PVMSG("(%d %d):(%f %f) %f-%f, ",mData[i-1]->x,mData[i-1]->y,xDelta,yDelta,dist,accumDist);
          }
        }
      }
      return accumDist;
    }

    float getTotalDisplacement(bool useNormalizedDistanceValues = false)
    {      
//      int trackSize = mData.size();

//      PVMSG("numFace: %d lastAdBFace: %d\n",mData.size(),lastAdBFace);
//      PVASSERT(trackSize > 0);
      float dist;

      if(lastAdBFace<=0)
        dist = 0.0;
      else
      {
        float xDelta = (float)mData[lastAdBFace]->x - (float)mData[0]->x;
        float yDelta = (float)mData[lastAdBFace]->y - (float)mData[0]->y;
        dist = sqrt(xDelta*xDelta + yDelta*yDelta);
		if(useNormalizedDistanceValues)
			dist /= 0.5 * (mData[0]->w + mData[lastAdBFace]->w);
      }
      return dist;
	}

    Rectanglei getLastFaceRect()
    {
      if (!mData.empty())
      {
        return mData.back()->getRect();
      }
      else
      {
        return Rectanglei();
      }
    }

  static void paintDottedLine(Image32& img, Vector2i p0, Vector2i p1, Uint32 color)
  {
    const int lineSegmentLength = 6;

    bool horizontal = p0.x != p1.x;
  
    if (horizontal)
    {
      for (int x = p0.x; x < p1.x; x++)
      {
        if (x % lineSegmentLength != 0)
        {
          img.safe_set(x,p0.y,color);
        }
      }
    }
    else
    {
      for (int y = p0.y; y < p1.y; y++)
      {
        if (y % lineSegmentLength != 0)
        {
          img.safe_set(p0.x,y,color);
        }
      }
    }

    int divby = 6;

    Vector2i center;
    center.add(p0,p1);
    center.div(2);

    Vector2i pp(p0.x-center.x,p0.y-center.y);
    Vector2i pp0(center.x+pp.y/divby,center.y+pp.x/divby);
    Vector2i pp1(center.x-pp.y/divby,center.y-pp.x/divby);

  //  img.line(pp0.x,pp0.y,pp1.x,pp1.y,color);
  }

  static void paintFancyRect(Image32& img, Rectanglei rect, Uint32 color)
  {
    paintDottedLine(img,Vector2i(rect.x0,rect.y0),Vector2i(rect.x1,rect.y0),color);
    paintDottedLine(img,Vector2i(rect.x1,rect.y0),Vector2i(rect.x1,rect.y1),color);
    paintDottedLine(img,Vector2i(rect.x0,rect.y0),Vector2i(rect.x0,rect.y1),color);
    paintDottedLine(img,Vector2i(rect.x0,rect.y1),Vector2i(rect.x1,rect.y1),color);
  }

  static void paintColorFilteredRect(Image32& img, Rectanglei rect, unsigned int color)
  {
	int cropWidth, cropHeight;

	cropWidth = (int)(0.0*(rect.x1 - rect.x0));
	cropHeight = (int)(0.0*(rect.y1 - rect.y0));

	int cropX0 = std::max(0, rect.x0 - cropWidth);
	int cropX1 = std::min<int>(img.width(), rect.x1 + cropWidth);
	int cropY0 = std::max(0, rect.y0 - cropHeight);
	int cropY1 = std::min<int>(img.height(), rect.y1 + cropHeight);

	Image32 cchip;
	cchip.crop(img, cropX0, cropY0, cropX1 - cropX0, cropY1 - cropY0);

//    Image32 cchip(4*cropWidth, 4*cropHeight);

	Image32* chip = &cchip;
	unsigned int* pixels = chip->pointer();
	
	unsigned char colorB = (((color)) & 0xff);
	unsigned char colorG = (((color)>>8) & 0xff);
	unsigned char colorR = (((color)>>16) & 0xff);

	for(int j = 0; j < chip->height(); j++)
	{
		for(int i = 0; i < chip->width(); i++)
		{
			unsigned int pixel = pixels[j*chip->width() + i];
			unsigned char iB = (((pixel)) & 0xff);
			unsigned char iG = (((pixel)>>8) & 0xff);
			unsigned char iR = (((pixel)>>16) & 0xff);

			float norm = sqrt((double)(iR*iR + iG*iG + iB*iB))/3;

			unsigned char vB = (unsigned char) (colorB * norm / 200);
			unsigned char vG = (unsigned char) (colorG * norm / 200);
			unsigned char vR = (unsigned char) (colorR * norm / 200);

			unsigned char inorm = (unsigned char)(0.5*norm);
			pixels[j*chip->width() + i] = PV_RGB(vR, vG, vB);
//			pixels[j*chip->width() + i] = PV_RGB(inorm, inorm, inorm);
		}
	}

	img.paste(cchip, rect.x0 - cropWidth, rect.y0 - cropHeight);
  }

    void draw(Image32& img, unsigned int col = PV_RGB(255,0,0), int doVisualizeHistory = 0, bool demoVisualization = false, bool visualizeFacialFeatures = false, bool doPose = false)
    {
      int visualStart;
      if(doVisualizeHistory)
        visualStart = 0;
      else
        visualStart = mData.size()-1;
      for (int i = visualStart; i < mData.size(); i++)
	  {
        int x, y, w, h;
        float ex, ey, sz, or, yw, pt;

        x = mData[i]->x;
        y = mData[i]->y;

        ex = mData[i]->ex;
        ey = mData[i]->ey;
          
        if(i==0)
        {
          w = mData[i]->w;
          h = mData[i]->h;
          
          sz = mData[i]->sz;
          or = mData[i]->or;
        
          yw = mData[i]->yw;
          pt = mData[i]->pt;
        }
        else // Temporal smoothing of sizes and 3d pose for stable visualization
        {
          w = (mData[i]->w + mData[i-1*0]->w)/2;
          h = (mData[i]->h + mData[i-1*0]->h)/2;
          
          sz = (mData[i]->sz + mData[i-1*0]->sz)/2;
          or = (mData[i]->or + mData[i-1*0]->or)/2;
        
          yw = (mData[i]->yw + mData[i-1*0]->yw)/2;
          pt = (mData[i]->pt + mData[i-1*0]->pt)/2;
        }

        unsigned int dcol;

        // Color scheme based on classification
        if(mGenderScore < 0) // Female
        {
          dcol=PV_RGB(255,0,0);
          //if(mEthnicityString=="African") // African
          //  dcol=PV_RGB(255,0,0);   
          //if(mEthnicityString=="Hispanic") // Hispanic
          //  dcol=PV_RGB(255,127,0);   
          //if(mEthnicityString=="Caucasian") // Caucasian
          //  dcol=PV_RGB(255,255,0);   
          //if(mEthnicityString=="Oriental") // Oriental
          //  dcol=PV_RGB(127,255,0);   
        }
       else if(mGenderScore > 0) // Male
        {
          dcol=PV_RGB(0,0,255);
          //if(mEthnicityString=="African") // African
          //  dcol=PV_RGB(0,0,255);   
          //if(mEthnicityString=="Hispanic") // Hispanic
          //  dcol=PV_RGB(0,127,255);   
          //if(mEthnicityString=="Caucasian") // Caucasian
          //  dcol=PV_RGB(0,255,255);   
          //if(mEthnicityString=="Oriental") // Oriental
          //  dcol=PV_RGB(0,255,127);   

        }  
	   else
	   {
		   dcol=PV_RGB(128,128,128);
	   }
      
       if(mData[i]->isFrontLeftRight < 3)
       {
//         img.ellipse((2*x + w)/2 + adBScale*(ex-30.0), (2*y + h)/2 + adBScale*(ey-30.0-4.28),
//           12.5*totScale, 12.5*totScale, col);
         float adBScale, totScale, px, py;

         if(!demoVisualization)
         {

           if(doVisualizeHistory==2)
              img.rectangle(x, y, x+w, y+h, dcol);
            else
              img.rectangle(x, y, x+w, y+h, col);

            if(visualizeFacialFeatures)
            {
              adBScale = (float)(w/30.0);
              totScale = adBScale*(sz/12.5);
              px = (2*x + w)/2 + adBScale*(ex-30.0);
              py = (2*y + h)/2 + adBScale*(ey-30.0-4.28);
              ModelPerson a3nnPerson(px, py, 12.5*totScale, or);
	  	  
  	         a3nnPerson.computeFeatureLocations();
             a3nnPerson.drawRGBFace(img, 2.0+0*totScale, dcol);
            }
         }
         else
         {
           paintFancyRect(img, Rectanglei(x, y, x+w, y+h),PV_RGB(255,0,0));


		   // Another demo visualization scheme to cover the faces with gender-encoded color
	   //			paintColorFilteredRect(img, Rectanglei(x, y, x+w, y+h), dcol);
         }


         //draw the (yaw, pitch) output visualization only when these values were estimated
         if(doPose && visualizeFacialFeatures)
         {
           //dot(s) showing the direction of the face in the top left corner
//           img.circle(40, 40, 20, PV_RGB(0,0,0));
//           img.ellipseFill(40 - 20*sin(2*yw), 40 - 20*sin(2*pt), 2, 2, col);

           //diver's goggle-like circle showing where the person is facing
		   if(mData[i]->is3DFrontal)
		   {
//			img.circle(px, py + totScale*5, 7.5*totScale, PV_RGB(0,255,0));
//			img.ellipseFill(px - 15*totScale*sin(yw), py - 30*totScale*sin(pt-0.15) + totScale*5, 5*totScale, 5*totScale, PV_RGB(0,255,0));

		    img.line(px - 3*totScale, py, px + 3*totScale, py, PV_RGB(0,255,0));
		    img.line(px, py - 3*totScale, px, py + 3*totScale, PV_RGB(0,255,0));
		   }
		   else
			img.circle(px - 15*totScale*sin(yw), py - 30*totScale*sin(pt-0.25), 12.5*totScale, PV_RGB(0,0,255));
         }
//         img.ellipse(px, py, 12.5*totScale*cos(2*yw), 12.5*totScale*cos(2*pt), col);

         if(0)
         {
            Image32 mRGBFaceChip(30,30);
  
            for(int y=0; y<mRGBFaceChip.height(); y++)
              for(int x=0; x<mRGBFaceChip.width(); x++)
              {
                unsigned char pix = round(mFaceChip(x,y)/numInstances);
                mRGBFaceChip(x,y) = PV_RGB(pix,pix,pix);
               }
           if(0*30*mId < img.width())
             img.paste(mRGBFaceChip, 0*30*mId, 0);
           
//           PvImageConverter::convert(mData[visualStart]->mFaceChip, mRGBFaceChip);
//           if(30*(mId+1) < img.width())
//             img.paste(mRGBFaceChip, 30*(mId), 0);
         }

       }
       else
       {
//         img.ellipse((2*x+w)/2, (2*y+h)/2, w/2, h/2, PV_RGB(255,255,255));
//        img.rectangle(x, y, x+w, y+h, PV_RGB(255,255,255));
//        img.rectangle(x, y, x+w, y+h, col);
       }
      }
    }

    void saveFaceImages(const std::string& fname, float imageQuality)
    {
      int imgW = 0, imgH = 0;
      std::vector<Image32Ptr>::iterator iFaceImg;
      for (iFaceImg = mFaceImages.begin(); iFaceImg != mFaceImages.end(); ++iFaceImg)
      {
        imgW += (*iFaceImg)->width();
        imgH = std::max(imgH,(int)(*iFaceImg)->height());
      }
	  imgW += mFaceChip.width();
	  imgH = std::max(imgH, (int)(mFaceChip.height()));
	
      Image32 img(imgW,imgH);
      img.setAll(PV_RGB(0,0,0));
      int x = 0;
      for (iFaceImg = mFaceImages.begin(); iFaceImg != mFaceImages.end(); ++iFaceImg)
      {
        img.paste(**iFaceImg,x,0);
        x += (*iFaceImg)->width();
      }
	  
	  Image32 chip(mFaceChip.width(), mFaceChip.height());
	  for(int y=0; y<mFaceChip.height(); y++)
		  for(int x=0; x<mFaceChip.width();x++)
		  {
			  unsigned char pix = (unsigned char)(mFaceChip(x,y)/numInstances);
			  chip(x,y) = PV_RGB(pix,pix,pix);
		  }
	
	  img.paste(chip, x, 0);
	  img.savejpg(fname.c_str(),(int)min(100.0,imageQuality*100.0));
    }

    void saveFaceChip(const std::string& fname, float imageQuality)
    {
		Image32 img(mFaceChip.width(), mFaceChip.height());

	    for(int y=0; y<mFaceChip.height(); y++)
		  for(int x=0; x<mFaceChip.width();x++)
			{
				unsigned char pix = (unsigned char)(mFaceChip(x,y)/numInstances);
				img(x,y) = PV_RGB(pix,pix,pix);
			}
      img.savejpg(fname.c_str(),(int)min(100.0,imageQuality*100.0));
    }

    void segmentViewership(float timeThreshold)
    {
		float sumGap = 0.0;
        for(int i=0; i<mData.size()-1; i++)
        {
          float gap = (float)mData[i+1]->time - (float)mData[i]->time;
//		  PVMSG("%f - %f = %f\n",mData[i+1]->time,mData[i]->time,gap); 
          if(gap > timeThreshold)
          {
             sumGap += gap;
          }
        }
        mViewershipTime = lastFaceTime() - firstFaceTime() - sumGap;  
    }

  protected:
    CircularBuffer<FaceDataPtr> mData;
    unsigned int mId;
    int mMaxFaceImages;

  public:
    Image<float> mFaceChip;
    float mGenderScore, mAfrScore, mCauScore, mHisScore, mOriScore;
    float mAgeSum, mAge;
    std::string mGenderString, mEthnicityString, mAgeString;
    std::string mGenderAttribute, mEthnicityAttribute, mAgeAttribute;
    
    //total number of AdaBoost detected faces inside track
    int numAdBFace;
    int numInstances;
      
    //id of the last such face
    int lastAdBFace;

    //total amount of viewership
    float mViewershipTime;

	//the proportion of the 3DFrontal faces versus the AdaBoost faces
	float m3DFrontalFaceRatio;

	//the degree of being false-face, measured using the NonFaceFilter
	float mNonFaceScore;


    std::vector<Image32Ptr> mFaceImages;

//    //flag indicating whether the track has been classified
//    bool isClassified;

    /**
     * This flag indicates if the real time event classification
     * has been sent for the current track. Only one classification
     * will be sent per track.
     */
    bool realTimeDataSent;
  };

  typedef boost::shared_ptr<FaceTrack> FaceTrackPtr; 

  
  class FaceTrackScore
  {
  public:

    FaceDataPtr mFace;
    FaceTrackPtr mTrack;
    float mScore;
    bool isActive;

    // a list of top pairs is generated, when the top of the list is selected, mark all other pairs that are using this face or track

    FaceTrackScore(FaceDataPtr face_, FaceTrackPtr track_, float score_)
    {
      mFace = face_;
      mTrack = track_;
      mScore = score_;
      isActive = true;
    }

    void deactivate()
    {
      isActive = false;
    }

    bool isactive() {return isActive;};
  
    FaceDataPtr getFacePtr() {return mFace;};
    FaceTrackPtr getTrackPtr() {return mTrack;};

    float getScore() {return mScore;}
  };

  typedef boost::shared_ptr<FaceTrackScore> FaceTrackScorePtr; 

  class cmpScores
  {
    public:
    bool operator() (FaceTrackScorePtr s1, FaceTrackScorePtr s2)
    {
        return (*s1).getScore() > (*s2).getScore();        // cmp emulating "less than"
    }
  };

  //
  // LIFETIME
  //

  /**
   * Constructor. The constructor creates a quick instance. Expensive operations
   * are executed in the init() function.
   */
  FaceVerifyClassifyMod();

  /**
   * Destructor
   */
  virtual ~FaceVerifyClassifyMod();

  /**
   * Provide dependency information for the ExecutionGraph.
   * @see VisionMod::initExecutionGraph for more info.
   */
  virtual void initExecutionGraph(std::list<VisionModPtr>& srcModules);

  /**
   * Initialization function.
   */
  virtual void init();

  /**
   * Finalization function
   */
  virtual void finish();

  //
  // OPERATIONS
  //

public:

  /**
   * Process this module.
   */
  virtual void process();

  /**
   * Visualize this module.
   */
  virtual void visualize(Image32& img);

  void classifyFace(Image8 pFaceImg, FaceDataPtr pFace, FaceTrackPtr pTrack);

  void localizeFace(Image8 grayFrame, FaceDataPtr pFace);
  void localizeFace(Image8 grayFrame, FaceDataPtr pFace, FaceTrackPtr pTrack);
  void poseEstimateFace(Image8 pFaceImg, FaceDataPtr pFace);
  void accumulateFaceChip(FaceDataPtr pFace, FaceTrackPtr pTrack);

protected:

  //
  // ACCESS
  //
  static double evaluate(FaceDataPtr pFace1, FaceTrackPtr pTrack);

  void terminateTrack(FaceTrackPtr pTrack, Image8 grayImage = Image8(1,1));

public:

  /**
   * Get the static name of this module. This name is used to name the type of this module.
   * The constructor sets the member variable mName to this value.
   * @remark This static function is only implemented for concrete classes.
   */
  static const char *getModuleStaticName() { return "FaceVerifyClassifyMod"; }

  //
  // INQUIRY
  //

public:

  //
  // ATTRIBUTES
  //
  Image32 faceImage;

  //recorded when module starts, used for remembering offset information
  double initTime;

  double time;
  int counter;
  
protected:

  ForegroundSegmentModPtr mpForegroundSegmentMod;

  //foreground segmented image (0 or 255)
  Image8  mFrameBinary;
  int videoWidth, videoHeight;

  FaceDetectorCombinedPtr mpFrontCombinedFD;
  FaceDetectorAdaBoostForegroundPtr mpFrontFD; 
  std::vector<FaceTrackPtr> mTracks;
  unsigned int mTrackId;

  //just the filename of input video, not entire path (without extension), used to generate output csv filename
  std::string mVideoFileString;

  //output filename (currently outputs demographics)
  std::string mFaceXYWHFile;

  //neural network of some sort
  Arclet3DNN mA3NN;
  Arclet3DNN mRefineA3NN;

  //number of models in use by above neural network
  int numA3NNModel;  
  int numRefineA3NNModel;  

  //3d pose estimation neural network
  SparseYawPitchNN mYPNN;
  int numYPModel;

  //svms for classification
//  GenderTorchSVM mGSvm; // Gender
//  GenderTorchSVM mAfrSvm, mCauSvm, mHisSvm, mOriSvm; // African, Caucasian, Hispanic, Oriental
//  GenderTorchSVM mAgeSvm;// Age
 
  GenderSVM mGSvm;
  GenderSVM mAfrSvm, mCauSvm, mHisSvm, mOriSvm;
  GenderSVM mAgeSvm;

  GenderSVM mNonFaceSvm;

  //FILE*
  FILE *fpFaceXYWH;

  //skin tone detector 
  SkinToneDetector mSkinToneDetec;
  bool mDoSkinToneDetection;
  //skin tone segmented image
  Image8 mSkinTone;
  //the size of the skin tone region in color space
  float mSkinRegionRadius;
  bool mDoSkinToneMotionDetection;

  Image32 mPrevFrame;

  int doGender;
  int doEthnicity;
  int doAge;

  int doNonFaceFilter;
  int doPose;
  int doVisualizeHistory;
  int doRecordLabelsToCSV;
//  int doMotionSegment;
//  int doSkinSegment;
  int showSkinToneWindow;
  float frameSkipFactor;
  float mFaceMatchThreshold;
  float mNonFaceThres;

  float mFaceImageQuality;

//the threshold yaw and pitch angle to determine whether the face is frontal (is3DFrontal)
  float m3DFrontalYawThres;
  float m3DFrontalPitchThres;

  /**
   * Minimum ratio (proportion) of 3D frontal faces out of all the detected faces in a track, so that the track is to be determined as an impression
   */
  float mMin3DFrontalFaceRatio;

 //vector< Vector2i > mFaceHistory;

  /**
   * This is the class that generates events in Vms format.
   */
  VmsPacketPtr mpVmsPacket;

  /**
   * Send events to any application that expects demographic classification
   * in real time.
   */
  int mRealTimeEventsPort;

  /**
   * Minimum number of faces for real time classification.
   */
  double mRealTimeMinTrackFaces;

  /**
   * Minimum track age for real time classification.
   */
  double mRealTimeMinTrackAge;

  /**
   * Minimum track distance traveled.
   */
  double mRealTimeMinTraveledDist;

  /**
   * Minimum track displacement from start to end.
   */
  double mRealTimeMinTotalDisplacement;

  /**
   * Minimum track total (accumulated) face size variations.
   */
  double mRealTimeMinTotalSizeVariation;


  /**
   * The number of seconds the tracker waits for a new face until it terminates a dormant track
   */
  double mRealTimeTrackLatency;

  /**
   * Allow to resize the face view.
   */
  Rectanglei mFaceViewRect;

  /**
   * Visualization background image.
   */
  Image32Ptr mpBackgroundImage;

  /**
   * Display the demo rectangle around face or the debug output.
   */
  bool mDemoVisualization;

  /**
   * Display facial feature marks.
   */
  bool mVisualizeFacialFeatures;

  /**
   * Use this region of interest for the face detection.
   */
  Rectanglei mRoi;

  /**
   * Faces detected on the last frame (used only for demo visualization).
   */
  std::vector<Rectanglei> mLastFrameFaces;

  /**
   * Minimum face size for classification.
   */
  Vector2i mMinFaceClassificationSize;

  /**
   * Unknown range (min,max) for the gender classifier.
   */
  Vector2f mGenderUnknownRange;

  /**
   * See settings file.
   */
  std::string mFaceImagesOutputFolder;

  /**
   * See settings file.
   */
  bool mFaceImagesSendVmsAttribute;

  /**
   * See settings file.
   */
  int mFaceImagesMaxFacesPerHour;

  /**
   * See settings file.
   */
  bool mDoSegmentViewership;

  /*
   * See settings file.
   */
  float mViewershipLatency;

  /**
   * See settings file.
   */
  bool mClassifyAppearanceModelOnly;

  /**
   * See settings file.
   */
  float mUpscaleFactor;

  /**
   * See settings file.
   */
  std::string mCSVOutputPath;

  /**
   * See settings file.
   */
  bool mUseFaceDetectorCombined;

  /**
   * See settings file.
   */
  float mFaceDetectScaleFactor;

  float mSkinToneDownSampleFactor;

  bool mUseNormalizedDistanceValues;

};


}; // namespace vision

}; // namespace ait

#endif // FaceVerifyClassifyMod_HPP



/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
#ifndef FACEDETECTORNN_HPP
#define FACEDETECTORNN_HPP

//void Track_Init();
//void Track_UnInit();

// defined in Modules
#include "FaceDetector.hpp"

#include <boost/shared_ptr.hpp>

namespace ait
{

// Forward declare the face detector.
class NeuralNetFaceDetector;
typedef boost::shared_ptr<NeuralNetFaceDetector> NeuralNetFaceDetectorPtr;



class FaceDetectorNN : public FaceDetector
{
protected:
  bool isInitialized;

  bool equalizeImage;  //If True, equalizes the incoming image by
						//histogram equalization.

  NeuralNetFaceDetectorPtr mpFaceDetector;

public:
  FaceDetectorNN(void);
  virtual ~FaceDetectorNN(void);

  // initialization
  void initialize(void);

  // un-initialization
  void unInitialize(void);

  // Number of milliseconds to sleep between iterations to 
  // avoid that this thread eats all the CPU.
  void setSleepMilliseconds(int t);

  // process gray scale image
  //
  // implements pure virtual function in FaceDetect.cpp
  virtual void detect(const Image8 &image, bool asynch=false);

  //setEqualizeImage
  void setEqualizeImage(bool equlizeInputImage){equalizeImage = equlizeInputImage;};
};

} // namespace ait

#endif

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef FaceDetectorAdaBoost_fwd_HPP
#define FaceDetectorAdaBoost_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class FaceDetectorAdaBoost;
typedef boost::shared_ptr<FaceDetectorAdaBoost> FaceDetectorAdaBoostPtr;

class FaceDetectorAdaBoostForeground;
typedef boost::shared_ptr<FaceDetectorAdaBoostForeground> FaceDetectorAdaBoostForegroundPtr;

}; // namespace vision

}; // namespace ait

#endif // FaceDetectorAdaBoost_fwd_HPP


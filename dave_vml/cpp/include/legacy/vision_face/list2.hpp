/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ListIncluded
#define ListIncluded


template <class T>
class ListNode
{
//  friend ListClass<T>;

  public:
  T *prev, *next;

  ListNode();
  ~ListNode();
};

template<class T>
class ListClass
{
  friend void operator<<(ListClass<T> &, T *);
  public:
  T *first, *last;
  int length;

  ListClass();
  ~ListClass();
  void addBefore(T *ptr, T *place);
  void addAfter(T *ptr, T *place);
  void addFirst(T *ptr);
  void addLast(T *ptr);
  void deleteNode(T *ptr);
  void deleteNodes();
  void unchain(T *ptr);
  int empty() { return (first==NULL); }
  void reverseList();
  void swap(T *ptr1, T *ptr2);
  void sort(int (*comp)(T *, T *));
  void quickSort(int (*comp)(T *, T *), T *mPtr, T *nPtr, int m, int n);
  void append(ListClass<T> *list0);
};



#endif

///////////////////////////////////////////list.hpp/////////////////////////////////////////////////////
#ifndef ListCodeIncluded
#define ListCodeIncluded

////////////////////////////////////////////////////////////list.cpp/////////////////////////////////////////
//#include "list2.hpp"

template<class T>
ListNode<T>::ListNode()
{
  prev=NULL;
  next=NULL;
}

template<class T>
ListNode<T>::~ListNode()
{
  prev=NULL;
  next=NULL;
}

template<class T>
ListClass<T>::ListClass()
{
  first=NULL;
  last=NULL;
  length=0;
}

template<class T>
ListClass<T>::~ListClass()
{
  deleteNodes();
}

template<class T>
void ListClass<T>::addFirst(T *ptr)
{
  ptr->next=first;
  ptr->prev=NULL;
  if (first!=NULL) first->prev=ptr;
  first=ptr;
  if (last==NULL) last=ptr;
  length++;
}

template<class T>
void ListClass<T>::addLast(T *ptr)
{
  ptr->prev=last;
  if (last!=NULL) last->next=ptr;
  ptr->next=NULL;
  last=ptr;
  if (first==NULL) first=ptr;
  length++;
}

template<class T>
void ListClass<T>::addAfter(T *ptr, T *place)
{
  if (place==NULL) addFirst(ptr); else
  {
    ptr->next=place->next;
    if (place->next!=NULL)
      place->next->prev=ptr; else
        last=ptr;
      place->next=ptr;
    ptr->prev=place;
    length++;
  }
}

template<class T>
void ListClass<T>::addBefore(T *ptr, T *place)
{
  if (place==NULL) addLast(ptr); else
  {
    ptr->prev=place->prev;
    if (place->prev!=NULL)
      place->prev->next=ptr; else
        first=ptr;
    place->prev=ptr;
    ptr->next=place;
    length++;
  }
}

template<class T>
void ListClass<T>::unchain(T *ptr)
{
#ifdef Audit
  T *tem=first;
  while (tem!=NULL && tem!=ptr) tem=tem->next;
  if (tem==NULL)
    exit(1);
#endif
  if (ptr->prev!=NULL) ptr->prev->next=ptr->next; else
    first=ptr->next;
  if (ptr->next!=NULL) ptr->next->prev=ptr->prev; else
    last=ptr->prev;
  ptr->prev=ptr->next=NULL;
  length--;
}

template<class T>
void ListClass<T>::deleteNode(T *ptr)
{
  unchain(ptr);
  delete ptr;
}

template<class T>
void ListClass<T>::deleteNodes()
{
  T *ptr=first;
  T *next;
  while (ptr!=NULL)
  {
    next=ptr->next;
    deleteNode(ptr);
    ptr=next;
  }
}

//template<class T>
//int ListClass<T>::empty()
//{
//  return (length==0);
//}

template<class T>
void ListClass<T>::reverseList()
{
  T *ptr=first;
  while (ptr!=NULL)
  {
    T *tem=ptr->prev;
    ptr->prev=ptr->next;
    ptr->next=tem;
    ptr=ptr->prev;
  }
  ptr=first;
  first=last;
  last=ptr;
}


// This function exchanges two nodes in a doubly linked list.  Its a mess.  :-)
// Don't bother trying to understand it, its beyond the capacity of Homo
// sapiens.

template<class T>
void ListClass<T>::swap(T *ptr1, T *ptr2)
{
  T *ptr1Prev=ptr1->prev;
  T *ptr2Prev=ptr2->prev;
  T *ptr1Next=ptr1->next;
  T *ptr2Next=ptr2->next;
  T *ptr;

  if (ptr1Prev!=NULL) ptr1Prev->next=ptr2;
  if (ptr1Next!=NULL) ptr1Next->prev=ptr2;
  if (ptr2Prev!=NULL) ptr2Prev->next=ptr1;
  if (ptr2Next!=NULL) ptr2Next->prev=ptr1;
  ptr=ptr1->prev;
  ptr1->prev=ptr2->prev;
  ptr2->prev=ptr;
  ptr=ptr1->next;
  ptr1->next=ptr2->next;
  ptr2->next=ptr;
  if (first==ptr2) first=ptr1; else
    if (first==ptr1) first=ptr2;
  if (last==ptr2) last=ptr1; else
    if (last==ptr1) last=ptr2;
}

// Run the quicksort procedure on the list

template<class T>
void ListClass<T>::sort(int (*comp)(T *, T *))
{
  if (length>1) quickSort(comp,first,last,0,length-1);
}

// This is the infamous quicksort procedure.  Its kind of strange looking,
// to deal with doubly linked lists, and because of these modifications 
// probably doesn't work with the asymptotic time O(n log n), but I'm not
// sure on this.

template<class T>
void ListClass<T>::quickSort(int (*comp)(T *, T *), T *mPtr, T *nPtr, int m, int n)
{
  int i=m;
  int j=n;
  T *iPtr=mPtr;
  T *jPtr=nPtr;
  T *kPtr=mPtr;
#ifdef Debug
  printf("Sort %d %d,",m,n);
  mPtr->print();
  printf(" ");
  nPtr->print();
  printf("\n");
#endif
  if (m<n)      // If there are elements to sort
  {
    do {
      do {      // Shift pointers up and down the list
        i++;
        iPtr=iPtr->next;
      } while (i<=n && comp(kPtr,iPtr)>0);
      while (j>=m && comp(jPtr,kPtr)>0)
      {
        j--;
        jPtr=jPtr->prev;
      } 
      if (i<j)  // Do we have to swap?
      {
        swap(iPtr,jPtr);     // Do it
        if (iPtr==nPtr) nPtr=jPtr; else if (jPtr==nPtr) nPtr=iPtr;
        if (iPtr==mPtr) mPtr=jPtr; else if (jPtr==mPtr) mPtr=iPtr;
        T *tem=iPtr;
        iPtr=jPtr;
        jPtr=tem;
#ifdef Debug
        printf("Loop: ");
        print();
#endif
      }
    } while (i<j);
    if (m<j)   // Yet another swap?
    {
      swap(mPtr,jPtr);     // Do it
      if (iPtr==nPtr) nPtr=jPtr; else if (jPtr==nPtr) nPtr=iPtr;
      if (iPtr==mPtr) mPtr=jPtr; else if (jPtr==mPtr) mPtr=iPtr;
      T *tem=mPtr;
      mPtr=jPtr;
      jPtr=tem;
#ifdef Debug
      printf("Swap: ");
      print();
#endif
      quickSort(comp,mPtr,jPtr->prev,m,j-1);  // Sort one sublist
    }
    if (j<n) quickSort(comp,jPtr->next,nPtr,j+1,n); // Sort other sublist
  }
#ifdef Debug
  printf("Done.\n");
#endif
}

template<class T>
void ListClass<T>::append(ListClass<T> *list0)
{
  if (empty())
  {
    first=list0->first;
    last=list0->last;
  } else if (!list0->empty())
  {
    last->next=list0->first;
    list0->first->prev=last;
    last=list0->last;
  }
  list0->first=NULL;
  list0->last=NULL;
  length+=list0->length;
  list0->length=0;
}

template<class T>
void operator<<(ListClass<T> &list0, T *node)
{
  list0.addLast(node);
}



////////////////////////////////////////////////////////////list.cpp/////////////////////////////////////////
#endif

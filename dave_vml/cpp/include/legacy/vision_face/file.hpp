
#ifndef file_h
#define file_h

#include <stdio.h>
#include <string.h>
#include <errno.h>

#define OPEN( fp, name )                                        \
{                                                               \
  if( (fp) != 0 )                                               \
  {                                                             \
    fprintf( stderr, "\n\n%s %d:\n", __FILE__, __LINE__ );      \
    fprintf( stderr, "Bug -- trying to open '%s' for reading,"  \
                     " but the file pointer is not 0\n",        \
                     name );                                    \
    goto err;                                                   \
  }                                                             \
  (fp) = fopen( name, "rb" );                                   \
  if( (fp) == 0 )                                               \
  {                                                             \
    fprintf( stderr, "\n\n%s %d:\n", __FILE__, __LINE__ );      \
    fprintf( stderr, "Cannot open '%s' for reading -- %s\n",    \
                     name, strerror( errno ) );                 \
    goto err;                                                   \
  }                                                             \
}

#define CREATE( fp, name )                                      \
{                                                               \
  if( (fp) != 0 )                                               \
  {                                                             \
    fprintf( stderr, "\n\n%s %d:\n", __FILE__, __LINE__ );      \
    fprintf( stderr, "Bug -- trying to create '%s',"            \
                     " but the file pointer is not 0\n",        \
                     name );                                    \
    goto err;                                                   \
  }                                                             \
  (fp) = fopen( name, "wb" );                                   \
  if( (fp) == 0 )                                               \
  {                                                             \
    fprintf( stderr, "\n\n%s %d:\n", __LINE__, __FILE__ );      \
    fprintf( stderr, "Cannot create '%s' -- %s\n",              \
                     name, strerror( errno ) );                 \
    goto err;                                                   \
  }                                                             \
}

#define MODIFY( fp, name )                                      \
{                                                               \
  if( (fp) != 0 )                                               \
  {                                                             \
    fprintf( stderr, "\n\n%s %d:\n", __FILE__, __LINE__ );      \
    fprintf( stderr, "Bug -- trying to open '%s' for modifying," \
                     " but the file pointer is not 0\n",        \
                     name );                                    \
    goto err;                                                   \
  }                                                             \
  (fp) = fopen( name, "r+b" );                                  \
  if( (fp) == 0 )                                               \
  {                                                             \
    fprintf( stderr, "\n\n%s %d:\n", __FILE__, __LINE__ );      \
    fprintf( stderr, "Cannot open '%s' for modifying -- %s\n",  \
                     name, strerror( errno ) );                 \
    goto err;                                                   \
  }                                                             \
}

#define READ( fp, v )                                           \
{                                                               \
  if( (fp) == 0 )                                               \
  {                                                             \
    fprintf( stderr, "\n\n%s %d:\n", __FILE__, __LINE__ );      \
    fprintf( stderr, "Bug -- trying to read from a null"        \
                     "file pointer\n" );                        \
    goto err;                                                   \
  }                                                             \
  if( fread( (v), sizeof( *(v) ), 1, (fp) ) != 1 )              \
  {                                                             \
    fprintf( stderr, "\n\n%s %d:\n", __FILE__, __LINE__ );      \
    fprintf( stderr, "Failed to read one item from a file" );   \
    goto err;                                                   \
  }                                                             \
}

#define READ_ARRAY( fp, v, n )                                  \
{                                                               \
  if( (fp) == 0 )                                               \
  {                                                             \
    fprintf( stderr, "\n\n%s %d:\n", __FILE__, __LINE__ );      \
    fprintf( stderr, "Bug -- trying to read from a null"        \
                     "file pointer\n" );                        \
    goto err;                                                   \
  }                                                             \
  if( fread( (v), sizeof( *(v) ), (n), (fp) ) != (n) )          \
  {                                                             \
    fprintf( stderr, "\n\n%s %d:\n", __FILE__, __LINE__ );      \
    fprintf( stderr, "Failed to read %d items from a file",     \
                     (n) );                                     \
    goto err;                                                   \
  }                                                             \
}

#define WRITE( fp, v )                                          \
{                                                               \
  if( (fp) == 0 )                                               \
  {                                                             \
    fprintf( stderr, "\n\n%s %d:\n", __FILE__, __LINE__ );      \
    fprintf( stderr, "Bug -- trying to write to a null"         \
                     "file pointer\n" );                        \
    goto err;                                                   \
  }                                                             \
  if( fwrite( (v), sizeof( *(v) ), 1, (fp) ) != 1 )             \
  {                                                             \
    fprintf( stderr, "\n\n%s %d:\n", __FILE__, __LINE__ );      \
    fprintf( stderr, "Failed to write one item to a file" );    \
    goto err;                                                   \
  }                                                             \
}

#define WRITE_ARRAY( fp, v, n )                                 \
{                                                               \
  if( (fp) == 0 )                                               \
  {                                                             \
    fprintf( stderr, "\n\n%s %d:\n", __FILE__, __LINE__ );      \
    fprintf( stderr, "Bug -- trying to write to a null"         \
                     "file pointer\n" );                        \
    goto err;                                                   \
  }                                                             \
  if( fwrite( (v), sizeof( *(v) ), (n), (fp) ) != (n) )         \
  {                                                             \
    fprintf( stderr, "\n\n%s %d:\n", __FILE__, __LINE__ );      \
    fprintf( stderr, "Failed to write %d items to a file",      \
                     (n) );                                     \
    goto err;                                                   \
  }                                                             \
}

#define SEEK( fp, loc )                                         \
{                                                               \
  if( (fp) == 0 )                                               \
  {                                                             \
    fprintf( stderr, "\n\n%s %d:\n", __FILE__, __LINE__ );      \
    fprintf( stderr, "Bug -- trying to seek on a null"          \
                     "file pointer\n" );                        \
    goto err;                                                   \
  }                                                             \
  if( fseek( (fp), (loc), SEEK_SET ) != 0 )                     \
  {                                                             \
    fprintf( stderr, "\n\n%s %d:\n", __FILE__, __LINE__ );      \
    fprintf( stderr, "Seek to location %d failed", (loc) );     \
    goto err;                                                   \
  }                                                             \
}

#define SEEK_TO_END( fp )                                       \
{                                                               \
  if( (fp) == 0 )                                               \
  {                                                             \
    fprintf( stderr, "\n\n%s %d:\n", __FILE__, __LINE__ );      \
    fprintf( stderr, "Bug -- trying to seek on a null"          \
                     "file pointer\n" );                        \
    goto err;                                                   \
  }                                                             \
  if( fseek( (fp), 0L, SEEK_END ) != 0 )                        \
  {                                                             \
    fprintf( stderr, "\n\n%s %d:\n", __FILE__, __LINE__ );      \
    fprintf( stderr, "Seek to end of file failed" );            \
    goto err;                                                   \
  }                                                             \
}

#define CLOSE( fp )                                             \
{                                                               \
  if( (fp) != 0 )                                               \
    fclose( (fp) );                                             \
  (fp) = 0;                                                     \
}

#endif

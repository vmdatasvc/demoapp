#ifndef ARCLET3DNN_HPP
#define ARCLET3DNN_HPP

#include <legacy/vision_face/ModelPerson.hpp>

// For Torch3 
#include "torch3/src/DiskMatDataSet.h"
#include "torch3/src/ClassFormatDataSet.h"
#include "torch3/src/ClassNLLCriterion.h"
#include "torch3/src/MSECriterion.h"
#include "torch3/src/OneHotClassFormat.h"
#include "torch3/src/ClassMeasurer.h"
#include "torch3/src/MSEMeasurer.h"

#include "torch3/src/StochasticGradient.h"
#include "torch3/src/KFold.h"

#include "torch3/src/ConnectedMachine.h"
#include "torch3/src/Linear.h"
#include "torch3/src/Tanh.h"
#include "torch3/src/LogSoftMax.h"

#include "torch3/src/MeanVarNorm.h"
#include "torch3/src/DiskXFile.h"
#include "torch3/src/CmdLine.h"
#include "torch3/src/SafeRandom.h"

#include "legacy/vision_face/lssvr.hpp"

namespace ait
{

class Arclet3DNN
{

public:

  typedef struct _FileParams
  {
    char *valid_file;
    char *file;

    int max_load;
    int max_load_valid;
    real accuracy;
    real learning_rate;
    real decay;
    int max_iter;
    int the_seed;

    char *dir_name;
    char *model_file;
    int k_fold;
    bool binary_mode;
    bool regression_mode;
    int class_against_the_others;
  } FileParams;

  FileParams mFileParams;

  Torch::SafeRandom mRandom;

  Torch::CmdLine mCmd;

  std::string mModelListFile, mModelDir, mLsvFile;
  int numInputs;
  int numTargets;
  int numHiddenUnits1, numHiddenUnits2, numHiddenUnits3;
  real weight_decay;

  std::vector<ModelPersonPtr> mModel3D;
//  Arclet3D arclet[1000];
	Lsv *mLsv;
  std::vector< Torch::ConnectedMachine * > mMlp;
  std::vector< Torch::MeanVarNorm * > mMvNorm;
  std::vector< int > numInputTmp;
  int numModel;
  int numCloseModel;
  int maxNumModel;

  float mMidEyeX, mMidEyeY, mScale, mOrien;
  FILE *fp;
  float mEstMidEyeX, mEstMidEyeY, mEstMidEyeMouthDist, mEstOrientation;
  
  // Constructor
  Arclet3DNN()
  {
  }

  // Destructor
  virtual ~Arclet3DNN()
  {
    finish();
  }
  
  void init(std::string modelListFile_, std::string modelDir_);
  void init(std::string mModelListFile_, std::string mModelDir_, std::string mLsvFile_);

  void finish()
  {
    for (int i = 0; i < mMlp.size(); i++)
    {
      delete mMlp[i];
    }
    mMlp.clear();
    for (int i = 0; i < mMvNorm.size(); i++)
    {
      delete mMvNorm[i];
    }
    mMvNorm.clear();
    mModel3D.clear();
    numInputTmp.clear();
  }

  void get_args();
  void applyModel(int numEstModel, int wgt1, int wgt2, Image8 faceImage);
  void estimateModel(int numEstModel, int wgt1, int wgt2, Image8 faceImage, float detecX, float detecY, int detecW, int detecH);
  void estimateModel(int numEstModel, Image8 faceImage, float detecX, float detecY, int detecW, int detecH);
  void estimateModel(Image8 faceImage, float detecX, float detecY, int detecW, int detecH);
  void estimateRefined(int numEstModel, Image8 faceImage, Image8& outImage, float X, float Y, float S, float O);

  void affine(float *vecX, float *vecY, float scale, Matrix<float> rotMat, Matrix<float> rotCenter, Matrix<float> trans);
};

typedef boost::shared_ptr<Arclet3DNN> Arclet3DNNPtr;

} // namespace ait

#endif
//void affine(float *vecX, float *vecY, float scale, Matrix<float> rotMat, Matrix<float> rotCenter, Matrix<float> trans);

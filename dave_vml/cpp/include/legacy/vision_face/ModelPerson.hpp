/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ModelPerson_HPP
#define ModelPerson_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>
#include <string>
#include <vector>
#include <iostream>

#include <string.h>
// AIT INCLUDES
//
#include "legacy/pv/ait/Image.hpp"
#include "legacy/pv/PvImageConverter.hpp"
#include "legacy/vision_tools/PvImageProc.hpp"
#include "legacy/pv/PvUtil.hpp"
#include "legacy/vision_tools/IlluminationGradient.hpp"
#include "legacy/vision_tools/FaceNormalizer.hpp"
#include "legacy/pv/ait/Image_fwd.hpp"

namespace ait
{

/**
* This class provides hypothetical facial geometry (location, scale, orientation) and 
* a mapping to corresponding facial feature locations
* Major functions: 
*  1. Compute facial feature locations from facial geometry
*  2. Compute crop sizes for facial feature image chips
*  3. Given a image (or an image file), crop out facial feature image chips
*/
class ModelPerson : private boost::noncopyable
{
public:

	// Face geometry - input parameters
	float mMidEyeX;
	float mMidEyeY;
	float mOrientation;
	float mMidEyeMouthDist;

	// Old members from GtPerson class -- necessary?
	float mFacePosX; // Left
	float mFacePosY; // Top
	float mFaceSizeX;
	float mFaceSizeY;

	// Facial feature locations
	float mLeftEyeX;
	float mLeftEyeY;
	float mRightEyeX;
	float mRightEyeY;
	float mNoseX;
	float mNoseY;
	float mMouthX;
	float mMouthY;

	// Facial feature crop size

	float mEyeCropWidth;
	float mEyeCropHeight;
	float mNoseCropWidth;
	float mNoseCropHeight;
	float mMouthCropWidth;
	float mMouthCropHeight;

	// Facial component images
	Image8 leftEyeImage;
	Image8 rightEyeImage;
	Image8 noseImage;
	Image8 mouthImage;

	float score;

	std::string mFileName;

public:

	/**
	* Constructor.
	*/
	ModelPerson()
	{
	}

	ModelPerson(float midEyeX, float midEyeY, float dist, float orientation)
	{
		mMidEyeX = midEyeX;
		mMidEyeY = midEyeY;
		mMidEyeMouthDist = dist;
		mOrientation = orientation;

		mEyeCropWidth = 5.625f*(mMidEyeMouthDist/12.5f);
		mEyeCropHeight = 4.5f*(mMidEyeMouthDist/12.5f);
		mNoseCropWidth = 4.5f*(mMidEyeMouthDist/12.5f);
		mNoseCropHeight = 4.5f*(mMidEyeMouthDist/12.5f);
		mMouthCropWidth = 9.0f*(mMidEyeMouthDist/12.5f);
		mMouthCropHeight = 4.5f*(mMidEyeMouthDist/12.5f);
	}

	/**
	* Destructor
	*/
	virtual ~ModelPerson()
	{
	}

public:

	void copy(ModelPerson *target)
	{
		target->mMidEyeX = mMidEyeX;
		target->mMidEyeY = mMidEyeY;
		target->mMidEyeMouthDist = mMidEyeMouthDist;
		target->mOrientation = mOrientation;

		target->mEyeCropWidth = 5.625f*(target->mMidEyeMouthDist/12.5f);
		target->mEyeCropHeight = 4.5f*(target->mMidEyeMouthDist/12.5f);
		target->mNoseCropWidth = 4.5f*(target->mMidEyeMouthDist/12.5f);
		target->mNoseCropHeight = 4.5f*(target->mMidEyeMouthDist/12.5f);
		target->mMouthCropWidth = 9.0f*(target->mMidEyeMouthDist/12.5f);
		target->mMouthCropHeight = 4.5f*(target->mMidEyeMouthDist/12.5f);
	}

	/**
	* Compute facial feature locations according to the given (position, size, orientation)
	*/
	void computeFeatureLocations(void)
	{
		Matrix<float> input(2,1), output(2,1);
		Matrix<float> rotation(2,2), center(2,1);	

		center(0,0) = mMidEyeX; center(1,0) = mMidEyeY;
		rotation(0,0) = cos(mOrientation); rotation(0,1) = -sin(mOrientation); 
		rotation(1,0) = sin(mOrientation); rotation(1,1) = cos(mOrientation);

		input(0,0) = (float)-0.48; input(1,0) = (float)0;
		output = mMidEyeMouthDist*(rotation*input) + center;
		mLeftEyeX = output(0,0); mLeftEyeY = output(1,0);

		input(0,0) = (float)+0.48; input(1,0) = (float)0;
		output = mMidEyeMouthDist*(rotation*input) + center;
		mRightEyeX = output(0,0); mRightEyeY = output(1,0);

		input(0,0) = (float)0; input(1,0) = (float)0.57;
		output = mMidEyeMouthDist*(rotation*input) + center;
		mNoseX = output(0,0); mNoseY = output(1,0);

		input(0,0) = (float)0; input(1,0) = (float)1.0;
		output = mMidEyeMouthDist*(rotation*input) + center;
		mMouthX = output(0,0); mMouthY = output(1,0);

		//	printf("model features: %f %f %f %f %f %f %f %f\n",mLeftEyeX,mLeftEyeY,mRightEyeX,mRightEyeY,mNoseX,mNoseY,mMouthX,mMouthY);
	}

	void rectangleFeatures(Image8& faceImage)
	{
		faceImage.rectangle(ait::round(mLeftEyeX-mEyeCropWidth), ait::round(mLeftEyeY-mEyeCropWidth), ait::round(mLeftEyeX+mEyeCropWidth), ait::round(mLeftEyeY+mEyeCropHeight), 255);
		faceImage.rectangle(ait::round(mRightEyeX-mEyeCropWidth), ait::round(mRightEyeY-mEyeCropWidth), ait::round(mRightEyeX+mEyeCropWidth), ait::round(mRightEyeY+mEyeCropHeight), 255);
		faceImage.rectangle(ait::round(mNoseX-mNoseCropWidth), ait::round(mNoseY-2*mNoseCropHeight), ait::round(mNoseX+mNoseCropWidth), ait::round(mNoseY+mNoseCropHeight), 255);
		faceImage.rectangle(ait::round(mMouthX-mMouthCropWidth), ait::round(mMouthY-mMouthCropHeight), ait::round(mMouthX+mMouthCropWidth), ait::round(mMouthY+mMouthCropHeight), 255);	
	}

	void rectangleRGBFeatures(Image32& faceImage, unsigned int col)
	{
		faceImage.rectangle(ait::round(mLeftEyeX-mEyeCropWidth), ait::round(mLeftEyeY-mEyeCropWidth), ait::round(mLeftEyeX+mEyeCropWidth), ait::round(mLeftEyeY+mEyeCropHeight), col);
		faceImage.rectangle(ait::round(mRightEyeX-mEyeCropWidth), ait::round(mRightEyeY-mEyeCropWidth), ait::round(mRightEyeX+mEyeCropWidth), ait::round(mRightEyeY+mEyeCropHeight), col);
		faceImage.rectangle(ait::round(mNoseX-mNoseCropWidth), ait::round(mNoseY-2*mNoseCropHeight), ait::round(mNoseX+mNoseCropWidth), ait::round(mNoseY+mNoseCropHeight), col);
		faceImage.rectangle(ait::round(mMouthX-mMouthCropWidth), ait::round(mMouthY-mMouthCropHeight), ait::round(mMouthX+mMouthCropWidth), ait::round(mMouthY+mMouthCropHeight), col);	
	}

	void drawRGBFeaturesBoundingRect(Image32& faceImage, Rectanglei& normFaceRect, unsigned int col)
	{
		faceImage.rectangle(normFaceRect, col);
	}

	void circleFeatures(Image8& faceImage)
	{
		faceImage.circle(ait::round(mLeftEyeX), ait::round(mLeftEyeY), 2, 255);
		faceImage.circle(ait::round(mRightEyeX), ait::round(mRightEyeY), 2, 255);
		faceImage.circle(ait::round(mNoseX), ait::round(mNoseY), 2, 255);
		faceImage.circle(ait::round(mMouthX), ait::round(mMouthY), 2, 255);	
	}

	void circleRGBFeatures(Image32& faceImage, const float rad, unsigned int col)
	{
		faceImage.circle(ait::round(mLeftEyeX), ait::round(mLeftEyeY), rad, col);
		faceImage.circle(ait::round(mRightEyeX), ait::round(mRightEyeY), rad, col);
		faceImage.circle(ait::round(mNoseX), ait::round(mNoseY), rad, col);
		faceImage.circle(ait::round(mMouthX), ait::round(mMouthY), rad, col);	
	}

	void drawRGBFace(Image32& faceImage, const float rad, unsigned int col)
	{
		Matrix<float> input(2,1), output(2,1);
		Matrix<float> rotation(2,2), center(2,1);	

		center(0,0) = mMidEyeX; center(1,0) = mMidEyeY;
		rotation(0,0) = cos(mOrientation); rotation(0,1) = -sin(mOrientation); 
		rotation(1,0) = sin(mOrientation); rotation(1,1) = cos(mOrientation);

		input(0,0) = (float)-0.36; input(1,0) = (float)1.0;
		output = mMidEyeMouthDist*(rotation*input) + center;

		faceImage.circle(ait::round(mLeftEyeX), ait::round(mLeftEyeY), rad, col);
		faceImage.circle(ait::round(mRightEyeX), ait::round(mRightEyeY), rad, col);
		faceImage.line(ait::round(mNoseX), ait::round(mNoseY), 
			ait::round(0.5f*(mLeftEyeX+mRightEyeX)), ait::round(0.5f*(mLeftEyeY+mRightEyeY)), col);
		faceImage.line(ait::round(output(0,0)),ait::round(output(1,0)),
			ait::round(2.0f*mMouthX-output(0,0)),ait::round(2.0f*mMouthY-output(1,0)), col);	
	}

	// Utility function common to some of the specialized crop functions
	void setImageComponents(Vector2f& leftEyeImgageSize, Vector2f& rightEyeImageSize,
		Vector2f& noseImageSize, Vector2f& mouthImageSize)
	{
		leftEyeImage.resize(ait::round(leftEyeImgageSize.x),ait::round(leftEyeImgageSize.y));
		rightEyeImage.resize(ait::round(rightEyeImageSize.x),ait::round(rightEyeImageSize.y));
		noseImage.resize(ait::round(noseImageSize.x),ait::round(noseImageSize.y));
		mouthImage.resize(ait::round(mouthImageSize.x),ait::round(mouthImageSize.y));
	}

	// Utility function common to the specialized crop functions
	void basicCrop(Image8& faceImage)
	{
		leftEyeImage.crop(faceImage, ait::round(mLeftEyeX-mEyeCropWidth), ait::round(mLeftEyeY-mEyeCropWidth), ait::round(2*mEyeCropWidth+1), ait::round(mEyeCropWidth+mEyeCropHeight+1));
		rightEyeImage.crop(faceImage, ait::round(mRightEyeX-mEyeCropWidth), ait::round(mRightEyeY-mEyeCropWidth), ait::round(2*mEyeCropWidth+1), ait::round(mEyeCropWidth+mEyeCropHeight+1));
		noseImage.crop(faceImage, ait::round(mNoseX-mNoseCropWidth), ait::round(mNoseY-2*mNoseCropHeight), ait::round(2*mNoseCropWidth+1), ait::round(3*mNoseCropHeight+1));
		mouthImage.crop(faceImage, ait::round(mMouthX-mMouthCropWidth), ait::round(mMouthY-mMouthCropHeight), ait::round(2*mMouthCropWidth+1), ait::round(2*mMouthCropHeight+1));	
	}

	void cropDumpFeatureImages(Image8& faceImage)
	{
		/*
		Image8 leftEyeImage;
		Image8 rightEyeImage;
		Image8 noseImage;
		Image8 mouthImage;

		leftEyeImage.crop(faceImage, ait::round(mLeftEyeX-mEyeCropWidth), ait::round(mLeftEyeY-mEyeCropWidth), ait::round(2*mEyeCropWidth+1), mEyeCropWidth+mEyeCropHeight+1);
		rightEyeImage.crop(faceImage, ait::round(mRightEyeX-mEyeCropWidth), ait::round(mRightEyeY-mEyeCropWidth), ait::round(2*mEyeCropWidth+1), mEyeCropWidth+mEyeCropHeight+1);
		noseImage.crop(faceImage, ait::round(mNoseX-mNoseCropWidth), ait::round(mNoseY-2*mNoseCropHeight), 2*mNoseCropWidth+1, 3*mNoseCropHeight+1);
		mouthImage.crop(faceImage, ait::round(mMouthX-mMouthCropWidth), ait::round(mMouthY-mMouthCropHeight), 2*mMouthCropWidth+1, 2*mMouthCropHeight+1);	
		*/

		setImageComponents(Vector2f(1.0f,1.0f),Vector2f(1.0f,1.0f),
				Vector2f(1.0f,1.0f),Vector2f(1.0f,1.0f));

		basicCrop(faceImage);

		leftEyeImage.printf("%d ");
		rightEyeImage.printf("%d ");
		noseImage.printf("%d ");
		mouthImage.printf("%d ");

		leftEyeImage.save("leftEye.png");
		rightEyeImage.save("rightEye.png");
		noseImage.save("nose.png");
		mouthImage.save("mouth.png");
	}

	void cropfprintfFeatureImages(Image8& faceImage, FILE *fpLeftEye, FILE *fpRightEye,
		FILE *fpNose, FILE *fpMouth, float X, float Y, float scale, float orien)
	{
		/*
		Image8 leftEyeImage(2*mEyeCropWidth, mEyeCropWidth+mEyeCropHeight+1);
		Image8 rightEyeImage(2*mEyeCropWidth, mEyeCropWidth+mEyeCropHeight+1);
		Image8 noseImage(mNoseCropWidth, mNoseCropHeight+1);
		Image8 mouthImage(mMouthCropWidth, mMouthCropHeight+1);

		leftEyeImage.crop(faceImage, ait::round(mLeftEyeX-mEyeCropWidth), ait::round(mLeftEyeY-mEyeCropWidth), ait::round(2*mEyeCropWidth+1), mEyeCropWidth+mEyeCropHeight+1);
		rightEyeImage.crop(faceImage, ait::round(mRightEyeX-mEyeCropWidth), ait::round(mRightEyeY-mEyeCropWidth), ait::round(2*mEyeCropWidth+1), mEyeCropWidth+mEyeCropHeight+1);
		noseImage.crop(faceImage, ait::round(mNoseX-mNoseCropWidth), ait::round(mNoseY-2*mNoseCropHeight), 2*mNoseCropWidth+1, 3*mNoseCropHeight+1);
		mouthImage.crop(faceImage, ait::round(mMouthX-mMouthCropWidth), ait::round(mMouthY-mMouthCropHeight), 2*mMouthCropWidth+1, 2*mMouthCropHeight+1);	
		*/

		setImageComponents(Vector2f(2*mEyeCropWidth, mEyeCropWidth+mEyeCropHeight+1),
			Vector2f(2*mEyeCropWidth, mEyeCropWidth+mEyeCropHeight+1),
			Vector2f(mNoseCropWidth, mNoseCropHeight+1),
			Vector2f(mMouthCropWidth, mMouthCropHeight+1));

		basicCrop(faceImage);

		leftEyeImage.fprintf(fpLeftEye, "%d "); 
		fprintf(fpLeftEye,"%f %f %f %f\n",X-mMidEyeX,Y-mMidEyeY,scale-mMidEyeMouthDist,orien-mOrientation);

		rightEyeImage.fprintf(fpRightEye, "%d "); 
		fprintf(fpRightEye,"%f %f %f %f\n",X-mMidEyeX,Y-mMidEyeY,scale-mMidEyeMouthDist,orien-mOrientation);

		noseImage.fprintf(fpNose, "%d "); 
		fprintf(fpNose,"%f %f %f %f\n",X-mMidEyeX,Y-mMidEyeY,scale-mMidEyeMouthDist,orien-mOrientation);

		mouthImage.fprintf(fpMouth, "%d "); 
		fprintf(fpMouth,"%f %f %f %f\n",X-mMidEyeX,Y-mMidEyeY,scale-mMidEyeMouthDist,orien-mOrientation);
	}

	void cropPrintAllImage(Image8& faceImage)
	{
		/*
		Image8 leftEyeImage(2*mEyeCropWidth, mEyeCropWidth+mEyeCropHeight+1);
		Image8 rightEyeImage(2*mEyeCropWidth, mEyeCropWidth+mEyeCropHeight+1);
		Image8 noseImage(mNoseCropWidth, mNoseCropHeight+1);
		Image8 mouthImage(mMouthCropWidth, mMouthCropHeight+1);

		leftEyeImage.crop(faceImage, ait::round(mLeftEyeX-mEyeCropWidth), ait::round(mLeftEyeY-mEyeCropWidth), ait::round(2*mEyeCropWidth+1), mEyeCropWidth+mEyeCropHeight+1);
		rightEyeImage.crop(faceImage, ait::round(mRightEyeX-mEyeCropWidth), ait::round(mRightEyeY-mEyeCropWidth), ait::round(2*mEyeCropWidth+1), mEyeCropWidth+mEyeCropHeight+1);
		noseImage.crop(faceImage, ait::round(mNoseX-mNoseCropWidth), ait::round(mNoseY-2*mNoseCropHeight), 2*mNoseCropWidth+1, 3*mNoseCropHeight+1);
		mouthImage.crop(faceImage, ait::round(mMouthX-mMouthCropWidth), ait::round(mMouthY-mMouthCropHeight), 2*mMouthCropWidth+1, 2*mMouthCropHeight+1);	
		*/

		setImageComponents(Vector2f(2*mEyeCropWidth, mEyeCropWidth+mEyeCropHeight+1),
			Vector2f(2*mEyeCropWidth, mEyeCropWidth+mEyeCropHeight+1),
			Vector2f(mNoseCropWidth, mNoseCropHeight+1),
			Vector2f(mMouthCropWidth, mMouthCropHeight+1));

		basicCrop(faceImage);

		leftEyeImage.printf("%d "); 
		rightEyeImage.printf("%d "); 
		noseImage.printf("%d "); 
		mouthImage.printf("%d "); 
	}

	void cropfprintAllImage(Image8& faceImage, FILE *fp, float X, float Y, float scale, float orien)
	{
		/*
		Image8 leftEyeImage(2*mEyeCropWidth, mEyeCropWidth+mEyeCropHeight+1);
		Image8 rightEyeImage(2*mEyeCropWidth, mEyeCropWidth+mEyeCropHeight+1);
		Image8 noseImage(mNoseCropWidth, mNoseCropHeight+1);
		Image8 mouthImage(mMouthCropWidth, mMouthCropHeight+1);

		leftEyeImage.crop(faceImage, ait::round(mLeftEyeX-mEyeCropWidth), ait::round(mLeftEyeY-mEyeCropWidth), ait::round(2*mEyeCropWidth+1), mEyeCropWidth+mEyeCropHeight+1);
		rightEyeImage.crop(faceImage, ait::round(mRightEyeX-mEyeCropWidth), ait::round(mRightEyeY-mEyeCropWidth), ait::round(2*mEyeCropWidth+1), mEyeCropWidth+mEyeCropHeight+1);
		noseImage.crop(faceImage, ait::round(mNoseX-mNoseCropWidth), ait::round(mNoseY-2*mNoseCropHeight), 2*mNoseCropWidth+1, 3*mNoseCropHeight+1);
		mouthImage.crop(faceImage, ait::round(mMouthX-mMouthCropWidth), ait::round(mMouthY-mMouthCropHeight), 2*mMouthCropWidth+1, 2*mMouthCropHeight+1);	
		*/

		setImageComponents(Vector2f(2*mEyeCropWidth, mEyeCropWidth+mEyeCropHeight+1),
			Vector2f(2*mEyeCropWidth, mEyeCropWidth+mEyeCropHeight+1),
			Vector2f(mNoseCropWidth, mNoseCropHeight+1),
			Vector2f(mMouthCropWidth, mMouthCropHeight+1));

		basicCrop(faceImage);

		leftEyeImage.fprintf(fp, "%d "); 
		rightEyeImage.fprintf(fp, "%d "); 
		noseImage.fprintf(fp, "%d "); 
		mouthImage.fprintf(fp, "%d "); 

		fprintf(fp,"%f %f %f %f\n",X-mMidEyeX,Y-mMidEyeY,scale-mMidEyeMouthDist,orien-mOrientation);
		//		printf("%f %f %f %f %f %f %f %f\n",mLeftEyeX,mLeftEyeY,mRightEyeX,mRightEyeY,mNoseX,mNoseY,mMouthX,mMouthY);
	}

	void cropfprintAllImage(Image8& faceImage, FILE *fp, float X, float Y, float scale, float orien,
		bool isLeftEyeIn, bool isRightEyeIn, bool isNoseIn, bool isMouthIn, bool isAllIn)
	{
		/*
		Image8 leftEyeImage(2*mEyeCropWidth, mEyeCropWidth+mEyeCropHeight+1);
		Image8 rightEyeImage(2*mEyeCropWidth, mEyeCropWidth+mEyeCropHeight+1);
		Image8 noseImage(mNoseCropWidth, mNoseCropHeight+1);
		Image8 mouthImage(mMouthCropWidth, mMouthCropHeight+1);

		leftEyeImage.crop(faceImage, ait::round(mLeftEyeX-mEyeCropWidth), ait::round(mLeftEyeY-mEyeCropWidth), ait::round(2*mEyeCropWidth+1), mEyeCropWidth+mEyeCropHeight+1);
		rightEyeImage.crop(faceImage, ait::round(mRightEyeX-mEyeCropWidth), ait::round(mRightEyeY-mEyeCropWidth), ait::round(2*mEyeCropWidth+1), mEyeCropWidth+mEyeCropHeight+1);
		noseImage.crop(faceImage, ait::round(mNoseX-mNoseCropWidth), ait::round(mNoseY-2*mNoseCropHeight), 2*mNoseCropWidth+1, 3*mNoseCropHeight+1);
		mouthImage.crop(faceImage, ait::round(mMouthX-mMouthCropWidth), ait::round(mMouthY-mMouthCropHeight), 2*mMouthCropWidth+1, 2*mMouthCropHeight+1);	
		*/

		setImageComponents(Vector2f(2*mEyeCropWidth, mEyeCropWidth+mEyeCropHeight+1),
			Vector2f(2*mEyeCropWidth, mEyeCropWidth+mEyeCropHeight+1),
			Vector2f(mNoseCropWidth, mNoseCropHeight+1),
			Vector2f(mMouthCropWidth, mMouthCropHeight+1));

		basicCrop(faceImage);


		leftEyeImage.fprintf(fp, "%d "); 
		rightEyeImage.fprintf(fp, "%d "); 
		noseImage.fprintf(fp, "%d "); 
		mouthImage.fprintf(fp, "%d "); 

		fprintf(fp,"%f %f %f %f %d %d %d %d %d\n",X-mMidEyeX,Y-mMidEyeY,scale-mMidEyeMouthDist,orien-mOrientation,
			isLeftEyeIn, isRightEyeIn, isNoseIn, isMouthIn, isAllIn);
		//		printf("%f %f %f %f %f %f %f %f\n",mLeftEyeX,mLeftEyeY,mRightEyeX,mRightEyeY,mNoseX,mNoseY,mMouthX,mMouthY);
	}

	void cropfprintAllImage(Image8& faceImage, FILE *fp, std::string gender, std::string ethnicity)
	{
		/*
		Image8 leftEyeImage(2*mEyeCropWidth, mEyeCropWidth+mEyeCropHeight+1);
		Image8 rightEyeImage(2*mEyeCropWidth, mEyeCropWidth+mEyeCropHeight+1);
		Image8 noseImage(mNoseCropWidth, mNoseCropHeight+1);
		Image8 mouthImage(mMouthCropWidth, mMouthCropHeight+1);

		leftEyeImage.crop(faceImage, ait::round(mLeftEyeX-mEyeCropWidth), ait::round(mLeftEyeY-mEyeCropWidth), ait::round(2*mEyeCropWidth+1), mEyeCropWidth+mEyeCropHeight+1);
		rightEyeImage.crop(faceImage, ait::round(mRightEyeX-mEyeCropWidth), ait::round(mRightEyeY-mEyeCropWidth), ait::round(2*mEyeCropWidth+1), mEyeCropWidth+mEyeCropHeight+1);
		noseImage.crop(faceImage, ait::round(mNoseX-mNoseCropWidth), ait::round(mNoseY-2*mNoseCropHeight), 2*mNoseCropWidth+1, 3*mNoseCropHeight+1);
		mouthImage.crop(faceImage, ait::round(mMouthX-mMouthCropWidth), ait::round(mMouthY-mMouthCropHeight), 2*mMouthCropWidth+1, 2*mMouthCropHeight+1);	
		*/

		setImageComponents(Vector2f(2*mEyeCropWidth, mEyeCropWidth+mEyeCropHeight+1),
			Vector2f(2*mEyeCropWidth, mEyeCropWidth+mEyeCropHeight+1),
			Vector2f(mNoseCropWidth, mNoseCropHeight+1),
			Vector2f(mMouthCropWidth, mMouthCropHeight+1));

		basicCrop(faceImage);

		leftEyeImage.fprintf(fp, "%d "); 
		rightEyeImage.fprintf(fp, "%d "); 
		noseImage.fprintf(fp, "%d "); 
		mouthImage.fprintf(fp, "%d "); 
		//	faceImage.fprintf(fp, "%d "); 

		fprintf(fp,"%s %s\n",gender.c_str(),ethnicity.c_str());
		//		printf("%f %f %f %f %f %f %f %f\n",mLeftEyeX,mLeftEyeY,mRightEyeX,mRightEyeY,mNoseX,mNoseY,mMouthX,mMouthY);
	}

	void cropPassAllImage(Image8& faceImage, unsigned char *pass)
	{
		/*
		Image8 leftEyeImage;
		Image8 rightEyeImage;
		Image8 noseImage;
		Image8 mouthImage;

		leftEyeImage.crop(faceImage, ait::round(mLeftEyeX-mEyeCropWidth), ait::round(mLeftEyeY-mEyeCropWidth), ait::round(2*mEyeCropWidth+1), ait::round(mEyeCropWidth+mEyeCropHeight+1));
		rightEyeImage.crop(faceImage, ait::round(mRightEyeX-mEyeCropWidth), ait::round(mRightEyeY-mEyeCropWidth), ait::round(2*mEyeCropWidth+1), ait::round(mEyeCropWidth+mEyeCropHeight+1));
		noseImage.crop(faceImage, ait::round(mNoseX-mNoseCropWidth), ait::round(mNoseY-2*mNoseCropHeight), ait::round(2*mNoseCropWidth+1), ait::round(3*mNoseCropHeight+1));
		mouthImage.crop(faceImage, ait::round(mMouthX-mMouthCropWidth), ait::round(mMouthY-mMouthCropHeight), ait::round(2*mMouthCropWidth+1), ait::round(2*mMouthCropHeight+1));	
		*/

		setImageComponents(Vector2f(2*mEyeCropWidth, mEyeCropWidth+mEyeCropHeight+1),
			Vector2f(2*mEyeCropWidth, mEyeCropWidth+mEyeCropHeight+1),
			Vector2f(mNoseCropWidth, mNoseCropHeight+1),
			Vector2f(mMouthCropWidth, mMouthCropHeight+1));

		basicCrop(faceImage);


		int eyeDim = leftEyeImage.width()*leftEyeImage.height();
		int noseDim = noseImage.width()*noseImage.height();
		int mouthDim = mouthImage.width()*mouthImage.height();

		unsigned char *pt;
		pt = leftEyeImage.pointer();
		memcpy(pass, pt, eyeDim);
		pt = rightEyeImage.pointer();
		memcpy(pass+eyeDim, pt, eyeDim);
		pt = noseImage.pointer();
		memcpy(pass+2*eyeDim, pt, noseDim);
		pt = mouthImage.pointer();
		memcpy(pass+2*eyeDim+noseDim, pt, mouthDim);
	}

	void getRgbFaceImage(Image32& faceImage)
	{
		Image32 image;
		image.load(mFileName.c_str());
		faceImage.resize(ait::round(mFaceSizeX),ait::round(mFaceSizeY));
		faceImage.crop(image,ait::round(mFacePosX),ait::round(mFacePosY),
			ait::round(mFaceSizeX),ait::round(mFaceSizeY));
	};

	void getFacialFeatures(Vector3f& leftEye,Vector3f& rightEye,
		Vector3f &nose,Vector3f& mouth)
	{
		leftEye.x = mLeftEyeX;
		leftEye.y = mLeftEyeY;
		rightEye.x = mRightEyeX;
		rightEye.y = mRightEyeY;
		nose.x = mNoseX;
		nose.y = mNoseY;
		mouth.x = mMouthX;
		mouth.y = mMouthY;
	}

    void getFacialFeatures(float& eyeWidth, float& eyeHeight,                           
                           float& noseWidth, float& noseHeight,
                           float& mouthWidth, float& mouthHeight)
    {
      eyeWidth    = mEyeCropWidth;
      eyeHeight   = mEyeCropHeight;
      mouthWidth  = mMouthCropWidth;
      mouthHeight = mMouthCropHeight;
      noseWidth   = mNoseCropWidth;
      noseHeight  = mNoseCropHeight;
    }

	int getGrayFaceImage(Image8& faceImage)
	{
		Image32 image;
		Image8 grayImage;
		image.load(mFileName.c_str());

		PvImageConverter::convert(image,grayImage);
		faceImage.resize(ait::round(mFaceSizeX),ait::round(mFaceSizeY));
		if(grayImage.width()>(mFacePosX+mFaceSizeX)&& grayImage.height()>(mFacePosY+mFaceSizeY))
		{
			faceImage.crop(grayImage,ait::round(mFacePosX),ait::round(mFacePosY),
				ait::round(mFaceSizeX),ait::round(mFaceSizeY));
			faceImage.rescale(20,20,3);
			PvImageProc::EqualizeImage(faceImage);
			return(1);
		}
		else
			return(0);
	};

	bool isImageFeaturesInModelWindows(float imLeftEyeX, float imLeftEyeY, float imRightEyeX, float imRightEyeY,
		float imNoseX, float imNoseY, float imMouthX, float imMouthY, 
		bool *isLeftEyeIn, bool *isRightEyeIn, bool *isNoseIn, bool *isMouthIn)
	{
		bool isAllIn = false;
		*isLeftEyeIn = false, *isRightEyeIn = false, *isNoseIn = true, *isMouthIn = false;
		//		bool isAllIn = true, isLeftEyeIn = true, isRightEyeIn = true, isNoseIn = true, isMouthIn = true;

		if(fabs(imLeftEyeX - mLeftEyeX) < 0.85f*mEyeCropWidth && fabs(imLeftEyeY - mLeftEyeY) < 0.85f*mEyeCropHeight)
			*isLeftEyeIn = true;
		if(fabs(imRightEyeX - mRightEyeX) < 0.85f*mEyeCropWidth && fabs(imRightEyeY - mRightEyeY) < 0.85f*mEyeCropHeight)
			*isRightEyeIn = true;
		if(fabs(imNoseX - mNoseX) < 0.85*mNoseCropWidth && fabs(imNoseY - mNoseY) < 0.85*mNoseCropHeight)
			*isNoseIn = true;
		if(fabs(imMouthX - mMouthX) < 0.85f*mMouthCropWidth && fabs(imMouthY - mMouthY) < 0.85f*mMouthCropHeight)
			*isMouthIn = true;

		isAllIn = *isLeftEyeIn && *isRightEyeIn && *isNoseIn && *isMouthIn;


		if(0)
			printf("%f:%f %f:%f-%d %f:%f %f:%f-%d %f:%f %f:%f-%d --> %d\n",
			imLeftEyeX,mLeftEyeX,imLeftEyeY,mLeftEyeY,*isLeftEyeIn,
			imRightEyeX,mRightEyeX,imRightEyeY,mRightEyeY,*isRightEyeIn,
			imMouthX,mMouthX,imMouthY,mMouthY,*isMouthIn,isAllIn);
		return isAllIn;
	}

	bool isImageFeaturesInModelWindows(float imLeftEyeX, float imLeftEyeY, float imRightEyeX, float imRightEyeY,
		float imNoseX, float imNoseY, float imMouthX, float imMouthY)
	{
		bool isLeftEyeIn, isRightEyeIn, isNoseIn, isMouthIn, isAllIn;

		isAllIn = isImageFeaturesInModelWindows(imLeftEyeX, imLeftEyeY, imRightEyeX, imRightEyeY,
			imNoseX, imNoseY, imMouthX, imMouthY, &isLeftEyeIn, &isRightEyeIn, &isNoseIn, &isMouthIn);
		if(1)
			printf("%f:%f %f:%f-%d %f:%f %f:%f-%d %f:%f %f:%f-%d --> %d\n",
			imLeftEyeX,mLeftEyeX,imLeftEyeY,mLeftEyeY,isLeftEyeIn,
			imRightEyeX,mRightEyeX,imRightEyeY,mRightEyeY,isRightEyeIn,
			imMouthX,mMouthX,imMouthY,mMouthY,isMouthIn,isAllIn);
		return isAllIn;
	}

	//
	// ACCESS
	//

	void setPersonParam(float midEyeX, float midEyeY, float dist, float orientation)
	{
		mMidEyeX = midEyeX;
		mMidEyeY = midEyeY;
		mMidEyeMouthDist = dist;
		mOrientation = orientation;

		mEyeCropWidth = 5.625f*(mMidEyeMouthDist/12.5f);
		mEyeCropHeight = 4.5f*(mMidEyeMouthDist/12.5f);
		mNoseCropWidth = 4.5f*(mMidEyeMouthDist/12.5f);
		mNoseCropHeight = 4.5f*(mMidEyeMouthDist/12.5f);
		mMouthCropWidth = 9.0f*(mMidEyeMouthDist/12.5f);
		mMouthCropHeight = 4.5f*(mMidEyeMouthDist/12.5f);
	}

	// Set the bounding box that encloses the registered facial features.
	// computeFeatureLocations() must be called before this function.
	void setNormFaceRect(Rectanglei& normFaceRect, Vector2f& eyeOffset, Vector2f& mouthOffset)
	{
		normFaceRect.set(ait::round(mLeftEyeX - eyeOffset.x*mEyeCropWidth),
			ait::round(std::min(mLeftEyeY - eyeOffset.y*mEyeCropWidth,mRightEyeY - eyeOffset.y*mEyeCropWidth)),
			ait::round(mRightEyeX + eyeOffset.x*mEyeCropWidth),
			ait::round(mMouthY + mouthOffset.y*mMouthCropHeight));
	}

};


/// Smart pointer to ModelPerson
typedef boost::shared_ptr<ModelPerson> ModelPersonPtr;

/// Vector of ModelPersonPtr
typedef std::vector<ModelPersonPtr> ModelPersonPtrList;

}; // namespace ait

#endif // ModelPerson_HPP


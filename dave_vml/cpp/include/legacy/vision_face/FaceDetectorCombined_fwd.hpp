/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef FaceDetectorCombined_fwd_HPP
#define FaceDetectorCombined_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class FaceDetectorCombined;
typedef boost::shared_ptr<FaceDetectorCombined> FaceDetectorCombinedPtr;

}; // namespace vision

}; // namespace ait

#endif // FaceDetectorCombined_fwd_HPP


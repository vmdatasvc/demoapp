/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef FaceDetectClassifyMod_HPP
#define FaceDetectClassifyMod_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

#include <String.hpp>
#include <ait/Image.hpp>
#include <PvImageConverter.hpp>

// LOCAL INCLUDES
//

#include "RealTimeMod.hpp"

// FORWARD REFERENCES
//

//#include "FaceDetectClassifyMod_fwd.hpp"
#include <ForegroundSegmentMod.hpp>
#include <face_detector/FaceDetectorAdaBoost.hpp>
#include <face_detector/FaceDetectorAdaBoostForeground.hpp>
#include "facial_geometry_estimator/Arclet3DNN.hpp"
#include "face_classifier/GenderTorchSVM.hpp"
#include "face_classifier/GenderSVM.hpp"
#include "facial_geometry_estimator/SparseYawPitchNN.hpp"
#include "facial_geometry_estimator/ModelPerson.hpp"
#include <CircularBuffer.hpp>
#include "face_detector/SkinToneDetector.hpp"
#include <VmsPacket_fwd.hpp>

#include "face_classifier/GenderTorchNN.hpp"

namespace ait 
{

namespace vision
{


/**
 * This vision module is not yet documented.
 * @par Responsibilities:
 * - Write the responsibilities here.
 */
class FaceDetectClassifyMod : public RealTimeMod
{
public:

  class FaceData
  {
  public:

    FaceData(int mId0, int isFrontLeftRight0, double time0, unsigned int x0, unsigned int y0, 
      unsigned int w0, unsigned int h0) : mId(mId0), isFrontLeftRight(isFrontLeftRight0), x(x0), y(y0), w(w0), h(h0), 
      time(time0), mFaceMarginPixels(10)
    {
    }
  
    FaceData(double time0, unsigned int x0, unsigned int y0, 
      unsigned int w0, unsigned int h0) : x(x0), y(y0), w(w0), h(h0), 
      time(time0), mFaceMarginPixels(10)
    {
    }
  
    /**
     * An integer id associated with each face, useful for debugging.
     */
    int mId;

    // 0 frontal, 1 left, 2 right, 3 face detectors
    int isFrontLeftRight;

    unsigned int x;
    unsigned int y;
    unsigned int w;
    unsigned int h;

    //midpoint of segment connecting 2 eyes
    float ex;
    float ey;

    //distance between (above point) and uypper lip
    float sz;
    float or;

    //yaw of face
    float yw;

    //pitch of face
    float pt;

    //previous yaw, previous pitch
    float prYw;
    float prPt;

    double time;

    Image8 mFaceChip;

    //integer values of demographics classification
    unsigned int iGender;
    unsigned int iEthnicity;
    unsigned int iAge;

    //scores of demographics classification
    float mGenderScore, mAfrScore, mCauScore, mHisScore, mOriScore;
    float mAge;
    std::string mGenderString, mEthnicityString, mAgeString;

    unsigned int getId() {return mId;}

    Rectanglei getRect()
    {
      return Rectanglei(x,y,x+w,y+h);
    }

    const int mFaceMarginPixels;

    Image32Ptr createFaceImage(const Image32& fullImage)
    {
      Rectanglei cropRect(x-mFaceMarginPixels,y-mFaceMarginPixels,
        x+w+mFaceMarginPixels,y+h+mFaceMarginPixels);
      cropRect.intersect(Rectanglei(0,0,fullImage.width(),fullImage.height()));
      Image32Ptr pFaceImage;
      pFaceImage.reset(new Image32(cropRect.width(),cropRect.height()));
      pFaceImage->crop(fullImage,cropRect.x0,cropRect.y0,cropRect.width(),cropRect.height());
      return pFaceImage;
    }

	  static void paintDottedLine(Image32& img, Vector2i p0, Vector2i p1, Uint32 color)
  {
    const int lineSegmentLength = 6;

    bool horizontal = p0.x != p1.x;
  
    if (horizontal)
    {
      for (int x = p0.x; x < p1.x; x++)
      {
        if (x % lineSegmentLength != 0)
        {
          img.safe_set(x,p0.y,color);
        }
      }
    }
    else
    {
      for (int y = p0.y; y < p1.y; y++)
      {
        if (y % lineSegmentLength != 0)
        {
          img.safe_set(p0.x,y,color);
        }
      }
    }

    int divby = 6;

    Vector2i center;
    center.add(p0,p1);
    center.div(2);

    Vector2i pp(p0.x-center.x,p0.y-center.y);
    Vector2i pp0(center.x+pp.y/divby,center.y+pp.x/divby);
    Vector2i pp1(center.x-pp.y/divby,center.y-pp.x/divby);

  //  img.line(pp0.x,pp0.y,pp1.x,pp1.y,color);
  }

  static void paintFancyRect(Image32& img, Rectanglei rect, Uint32 color)
  {
    paintDottedLine(img,Vector2i(rect.x0,rect.y0),Vector2i(rect.x1,rect.y0),color);
    paintDottedLine(img,Vector2i(rect.x1,rect.y0),Vector2i(rect.x1,rect.y1),color);
    paintDottedLine(img,Vector2i(rect.x0,rect.y0),Vector2i(rect.x0,rect.y1),color);
    paintDottedLine(img,Vector2i(rect.x0,rect.y1),Vector2i(rect.x1,rect.y1),color);
  }

    void draw(Image32& img, bool demoVisualization = false, bool visualizeFacialFeatures = false, bool doPose = false)
	{
         unsigned int dcol;

        // Color scheme based on classification
        if(mGenderString=="Female") // Female
        {
          dcol=PV_RGB(255,0,0);
          if(mEthnicityString=="African") // African
            dcol=PV_RGB(255,0,0);   
          if(mEthnicityString=="Hispanic") // Hispanic
            dcol=PV_RGB(255,127,0);   
          if(mEthnicityString=="Caucasian") // Caucasian
            dcol=PV_RGB(255,255,0);   
          if(mEthnicityString=="Oriental") // Oriental
            dcol=PV_RGB(127,255,0);   
        }
       else  // Male
        {
          dcol=PV_RGB(0,0,255);
          if(mEthnicityString=="African") // African
            dcol=PV_RGB(0,0,255);   
          if(mEthnicityString=="Hispanic") // Hispanic
            dcol=PV_RGB(0,127,255);   
          if(mEthnicityString=="Caucasian") // Caucasian
            dcol=PV_RGB(0,255,255);   
          if(mEthnicityString=="Oriental") // Oriental
            dcol=PV_RGB(0,255,127);   

        }  
      
        {
//         img.ellipse((2*x + w)/2 + adBScale*(ex-30.0), (2*y + h)/2 + adBScale*(ey-30.0-4.28),
//           12.5*totScale, 12.5*totScale, col);
         float adBScale, totScale, px, py;

         if(!demoVisualization)
         {

            img.rectangle(x, y, x+w, y+h, dcol);

            if(visualizeFacialFeatures)
            {
              adBScale = (float)(w/30.0);
              totScale = adBScale*(sz/12.5);
              px = (2*x + w)/2 + adBScale*(ex-30.0);
              py = (2*y + h)/2 + adBScale*(ey-30.0-4.28);
              ModelPerson a3nnPerson(px, py, 12.5*totScale, or);
	  	  
  	         a3nnPerson.computeFeatureLocations();
             a3nnPerson.drawRGBFace(img, 2.0+0*totScale, dcol);
            }
         }
         else
         {
           paintFancyRect(img, Rectanglei(x, y, x+w, y+h),PV_RGB(255,0,0));
//           img.rectangle(x, y, x+w, y+h, PV_RGB(255,0,0));
         }


         //draw the (yaw, pitch) output visualization only when these values were estimated
         if(doPose)
         {
           //dot(s) showing the direction of the face in the top left corner
           img.circle(40, 40, 20, PV_RGB(0,0,0));
           img.ellipseFill(40 - 20*sin(2*yw), 40 - 20*sin(2*pt), 2, 2, dcol);

           //diver's goggle-like circle showing where the person is facing
           img.circle(px - 15*totScale*sin(yw), py - 30*totScale*sin(pt-0.25), 12.5*totScale, PV_RGB(255,0,0));
         }
//         img.ellipse(px, py, 12.5*totScale*cos(2*yw), 12.5*totScale*cos(2*pt), col);
    }
	}
  };

  typedef boost::shared_ptr<FaceData> FaceDataPtr;



    void saveFaceImages(const std::string& fname, float imageQuality)
    {

    }

  //
  // LIFETIME
  //

  /**
   * Constructor. The constructor creates a quick instance. Expensive operations
   * are executed in the init() function.
   */
  FaceDetectClassifyMod();

  /**
   * Destructor
   */
  virtual ~FaceDetectClassifyMod();

  /**
   * Provide dependency information for the ExecutionGraph.
   * @see VisionMod::initExecutionGraph for more info.
   */
  virtual void initExecutionGraph(std::list<VisionModPtr>& srcModules);

  /**
   * Initialization function.
   */
  virtual void init();

  /**
   * Finalization function
   */
  virtual void finish();

  //
  // OPERATIONS
  //

public:

  /**
   * Process this module.
   */
  virtual void process();

  /**
   * Visualize this module.
   */
  virtual void visualize(Image32& img);
  void localizeFace(Image8 grayFrame, FaceDataPtr pFace);
  void poseEstimateFace(Image8 pFaceImg, FaceDataPtr pFace);
  void classifyFace(Image8 pFaceImg, FaceDataPtr pFace);
 
protected:

  //
  // ACCESS
  //
 
public:

  /**
   * Get the static name of this module. This name is used to name the type of this module.
   * The constructor sets the member variable mName to this value.
   * @remark This static function is only implemented for concrete classes.
   */
  static const char *getModuleStaticName() { return "FaceDetectClassifyMod"; }

  //
  // INQUIRY
  //

public:

  //
  // ATTRIBUTES
  //
  Image32 faceImage;

  //recorded when module starts, used for remembering offset information
  double initTime;

  double time;
  int counter;
  
protected:

  std::vector<FaceDataPtr> mFaces;
  ForegroundSegmentModPtr mpForegroundSegmentMod;

  //foreground segmented image (0 or 255)
  Image8  mFrameBinary;
  int videoWidth, videoHeight;

//  FaceDetectorAdaBoost mFrontFD; 
  FaceDetectorAdaBoostForegroundPtr mpFrontFD; 
 
  //just the filename of input video, not entire path (without extension), used to generate output csv filename
  std::string mVideoFileString;

  //output filename (currently outputs demographics)
  std::string mFaceXYWHFile;

  //neural network of some sort
  Arclet3DNN mA3NN;
  Arclet3DNN mRefineA3NN;

  //number of models in use by above neural network
  int numA3NNModel;  
  int numRefineA3NNModel;  

  //3d pose estimation neural network
  SparseYawPitchNN mYPNN;
  int numYPModel;

  GenderSVM mGSvm;
  GenderSVM mAfrSvm, mCauSvm, mHisSvm, mOriSvm;
  GenderSVM mAgeSvm;

  //FILE*
  FILE *fpFaceXYWH;

  //skin tone detector 
  SkinToneDetector mSkinToneDetec;
  bool mDoSkinToneDetection;
  //skin tone segmented image
  Image8 mSkinTone;

  int doGender;
  int doEthnicity;
  int doAge;
  int doPose;
  int doVisualizeHistory;
  int doRecordLabelsToCSV;
//  int doMotionSegment;
//  int doSkinSegment;
  int showSkinToneWindow;
  bool mDoSegmentViewership;

  /*
   * See settings file.
   */
  float mViewershipLatency;

  /**
   * See settings file.
   */
  bool mClassifyAppearanceModelOnly;

  float mFrameSkipFactor;
  float mFrameRate;
  float mFaceMatchThreshold;

  float mFaceImageQuality;

  /**
   * This is the class that generates events in Vms format.
   */
  VmsPacketPtr mpVmsPacket;

  /**
   * Send events to any application that expects demographic classification
   * in real time.
   */
  int mRealTimeEventsPort;

  /**
   * Allow to resize the face view.
   */
  Rectanglei mFaceViewRect;

  /**
   * Visualization background image.
   */
  Image32Ptr mpBackgroundImage;

  /**
   * Display the demo rectangle around face or the debug output.
   */
  bool mDemoVisualization;

  /**
   * Display facial feature marks.
   */
  bool mVisualizeFacialFeatures;

  /**
   * Use this region of interest for the face detection.
   */
  Rectanglei mRoi;

  /**
   * Faces detected on the last frame (used only for demo visualization).
   */
  std::vector<Rectanglei> mLastFrameFaces;

  /**
   * Minimum face size for classification.
   */
  Vector2i mMinFaceClassificationSize;

  /**
   * Unknown range (min,max) for the gender classifier.
   */
  Vector2f mGenderUnknownRange;

  /**
   * See settings file.
   */
  std::string mFaceImagesOutputFolder;

  /**
   * See settings file.
   */
  bool mFaceImagesSendVmsAttribute;

  /**
   * See settings file.
   */
  int mFaceImagesMaxFacesPerHour;

 
};


}; // namespace vision

}; // namespace ait

#endif // FaceDetectClassifyMod_HPP



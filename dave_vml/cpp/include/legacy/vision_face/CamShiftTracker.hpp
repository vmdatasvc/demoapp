#ifndef _CamShiftTracker_h
#define  _CamShiftTracker_h 1

#include "opencv2/opencv.hpp"
//#include "cv.h"
//#include "cvaux.h"
//#include "highgui.h"
#include <legacy/pv/ait/Image.hpp>
//#include "CvCamShiftTracker.hpp"

namespace ait
{

class CamShiftTracker 
{
  public:
    // Parameter: How many frames when user hits calibrate command
    int mId;

    // Our objects
    CvCamShiftTracker *mCsTracker;

    // State variables
    bool mFirstTime;
    // For face tracking, use smaller (90%) tracking window than the face detection window 
    bool mFaceWindowAdjustment;
    int numCalibrate;

    // We save result when we call recognizer
    CvPoint mTracCenter;
    CvRect mTracRect;
    CvSize mSize;

    // Saved for mouse callback
    CvRect mIinputRect;
    int imageHeight;
    int numTracking;
    int mFrontLeftRight;

    IplImage *mIplImage, *mIplMaskImage;

    // Constructor
    CamShiftTracker(int mId_, int width_, int height_);
    CamShiftTracker(int mId_, int width_, int height_, int x, int y, int w, int h, int frontLeftRight);

    // Destructor
    ~CamShiftTracker();

//    void init(int nCalib_);

    unsigned int getId();

    CvPoint getLoc ();

    void Calibrate (int nframes);

    void Track(Image32 *image, CvRect faceRect);

    void Track(Image32 *image, CvRect faceRect, Image8 *mask);
};

} // namespace ait

#endif

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
#ifndef NeuralNetFaceDetector_HPP
#define NeuralNetFaceDetector_HPP

#define WINNT

#define FaceHeight 19
#define FaceWidth 19
#define FaceSize 283

#ifdef WIN32
#include <io.h>
#endif
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stddef.h>
#include <math.h>
#include <assert.h>
#ifdef WIN32
#include <direct.h>
#endif
#include "legacy/pv/PvUtil.hpp"
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "Mat2.hpp"
#include "system2.hpp"
#include "fft2.hpp"
#include "list2.hpp"
#include <legacy/pv/ait/Image.hpp>

#include <vector>
#include <string>
#include <list>
#include <map>

#define ckfree(p) free(p)
#define ckalloc(s) malloc(s)

/*
 * When there are this many entries per bucket, on average, rebuild
 * the hash table to make it larger.
 */

#define REBUILD_MULTIPLIER	3


/*
 * The following macro takes a preliminary integer hash value and
 * produces an index into a hash tables bucket list.  The idea is
 * to make it so that preliminary values that are arbitrarily similar
 * will end up in different buckets.  The hash function was taken
 * from a random-number generator.
 */

#define RANDOM_INDEX(tablePtr, i) \
    (((((long) (i))*1103515245) >> (tablePtr)->downShift) & (tablePtr)->mask)


#define TanhUnitType 0
#define BiasUnitType 5
#define InputUnitType 5

namespace ait
{

struct FastForwardUnit;

struct FastForwardConnection {
  float weight;
  float activation;
  FastForwardUnit *from;
};
// typedef std::vector<FastForwardConnection*> vFfcType;

struct FastForwardConnectionRef {
  FastForwardConnection *conn;
  int from, to;
};

struct FastForwardUnit {
  int numInputs, numOutputs, type;
  FastForwardConnection *connections;
//  vFfcType vTo;

  void setActivation(float);
  float getActivation() const;
protected:
  float activation;
};

struct FastUnitInfo {
  int x, y, group;
};

struct FastGroupInfo {
  int startx, starty, endx, endy;
  int numUnits;
  int firstUnit;
};


struct FastForwardStruct {
  int patterns;
  int numUnits, numInputs, numHiddens, numOutputs;
  int firstInput;
  int firstHidden;
  int firstOutput;
  int numConns;
  int numGroups;
  FastUnitInfo *unitInfo;
  FastGroupInfo *groupInfo;
  FastForwardUnit *unitList;
  FastForwardConnectionRef *connList;
  FastForwardConnection *connections;
  ~FastForwardStruct();
};

struct Detection
{
  int x, y, s, orient;
  double output;
  Detection(int xx, int yy, int ss, double o, int dir) {
    x=xx; y=yy; s=ss; output=o; orient=dir;
  }
  // default copy and assignment are fine
};




struct AccumElement : public ListNode<AccumElement>
{
  // LIFECYCLE
  AccumElement(int x, int y, int s, int v) :
  mX(x), mY(y), mS(s), mV(v)
  {
  }
  // default copy and assignment are fine

  // OPERATORS
  void setValue(int v) { mV = v; }

  // ACCESSORS
  int getX() const { return mX; }
  int getY() const { return mY; }
  int getS() const { return mS; }
  int getValue() const { return mV; }
protected:
  int mX;
  int mY;
  int mS;
  int mV;
};







#define NotAnImageType 0
#define ByteImageType 1

typedef unsigned char Byte;

typedef struct AccumKey {
  AccumKey(int x = 0, int y = 0, int s = 0) :
mX(x), mY(y), mS(s)
{};
// default assignment and copy constructor are fine
// comparison operator so it can be used in a map
friend bool operator<(const AccumKey& l, const AccumKey& r);
protected:
  int mX;
  int mY;
  int mS;
} AccumKeyType;
  
class NeuralNetFaceDetector
{
public:
  // LIFECYCLE
  //Constructor.
  NeuralNetFaceDetector();
  
  //Destructor
  virtual ~NeuralNetFaceDetector();
	
  // OPERATIONS
  bool init();
  void finalize();

  void setSleepMilliseconds(int t) { mSleepMilliseconds = t; }

  // Function to count and locate all faces in an image.  Input buffer
  // is an array of unsigned char, 1 byte per pixel indicating the
  // intensity.  The width and height are the width and height in
  // pixels.  The return value is the number of faces found.  If locs is
  // NULL, the actual locations are discarded.  Otherwise, if no faces
  // were found, *locs is set to NULL.  If faces were found, *locs is
  // set to a pointer to an array of struct FaceLocation.  It is the
  // responsibility of the caller to free() this array.  The structures
  // will also contain the locations of the eyes, if they are found.
  // The left and right designations are from the user's point of view.
  // The flags left and right are set to 1 if that eye is detected, 0 if
  // it is not detected, or -1 if the face is to small to attempt
  // detection.
  
  /* Structure used to return the location of a face and eyes */
  struct FaceEyeLocation {
    int x1, y1, x2, y2;
    int leftx, lefty, rightx, righty;
    int left, right;
  };

  int Track_FindAllFaces(Image8 &image, struct FaceEyeLocation **locs, int asynch);

  /**
   * Set the integral image mask.
   * @see FaceDetector::setMask().
   * @remark The mask memory is not managed by this class. The caller
   * is responsible for deleting the memory, and for keeping it while
   * this class needs it (inside the Track_FindAllFaces function).
   */
  void setInclusionIntegralImageMask(Image<unsigned short> *pMask, float areaFraction)
  {
    mpInclusionIntegralImageMask = pMask;
    mInclusionAreaFactor = areaFraction;
  }
  
protected:
  //
  // Internal typedefs and classes
  //
  typedef std::map<AccumKeyType, AccumElement*> AccumElementMap;
  typedef AccumElementMap::iterator AemIterator;

  typedef std::list<Detection> DetectionList;
  typedef DetectionList::iterator DlIterator;

  enum { mSearchMaskWidth = 30 };
  enum { mSearchMaskHeight = 30 };

  /* Search for an eye.  Given an image of pixels, run the eye detector
  everywhere in that image, and return the centroid of the eye
  detections.  Other inputs are: a guess about the width of the eye
  (at least 22 pixels), and whether you are looking for the left eye
  (in the image; this means looking for the person's right eye).
  Returns 1 if an eye is detected, 0 otherwise */
  
  int SearchEye(int width, int height, unsigned char *buffer,
    int eyeWidthGuess, int leftEye,
    int *x, int *y);
  
    /* 
    
      Function to count and locate all faces in an image.
      Input buffer is an array of unsigned char, 3 bytes per
      pixel indicating the red, green, and blue values.  The 
      width and height are the width and height in pixels.
      The return value is the number of faces found.  If locs
      is NULL, the actual locations are discarded.  Otherwise,
      if no faces were found, *locs is set to NULL.  If faces
      were found, *locs is set to a pointer to an array of
      struct FaceLocation.  It is the responsibility of the caller
      to free() this array. 
      
  */
  
  /* 
  It tries to provide more
  detections, as follows:  First, run the candidate detector everywhere,
  recording the detection locations.  Then, sort the candidates from best
  to worst, without applying a threshold.  Finally, apply the verification
  networks to these candidates, and send the results out via a callback
  function.  This callback function (which the user must provide) can 
  return 1 (meaning continue searching) or 0 (stop now).
  
    Like the previous function, this one takes a color image as input.
    One new parameter: the number of verification networks that
    must respond positively for a candidate to be reported.  This must
    be a number between 0 and 2.  0 means report all candidates, no
    matter what; 1 means at least one verifier must give a response > 0,
    and 2 menas both must respond positively (at the same place).
    
      The arguments provided to the callback function are as follows:
      candidateRating: output of candidate network on face
      cx, yc, sc: location of candidate detection (upper left coordinates,
      size of search area; note that the search area is 30 pixels
      (scaled by the current resolution), and that the face is
      really a 20 pixel region somewhere inside that box.)
      verifyRating1: maximum output of verifier 1 over search area
      x1, y1, s1: location of maximum for verifier 1
      verifyRating2: maximum output of verifier 2 over search area
      x2, y2, s2: location of maximum for verifier 2
      
        The return value should be 1 to continue searching, or zero to 
        have the search function (FindFacesAllCandidates) return 
        immediately.
        
          Although all the candidates returned will be unique, the more 
          refined positions from the verification networks may not be--candidates
          that are close enough to one another may actually be the same face,
          and this will be picked up by the verification networks.
  */
  
  /* Path to the data files that the detector uses (under directory
  "data" in the distribution. */
  
  
  /*********************************************************/
  /* Interfaces designed for the face/eye tracking project */
  /*********************************************************/
  
  /* Path to the data files that the detector uses (under directory
  "data" in the distribution. */
  //char *Track_DetectorPath;
  
  /* Function to count and locate all faces in an image.
  Input buffer is an array of unsigned char, 1 byte per
  pixel indicating the intensity.  The 
  width and height are the width and height in pixels.
  The return value is the number of faces found.  If locs
  is NULL, the actual locations are discarded.  Otherwise,
  if no faces were found, *locs is set to NULL.  If faces
  were found, *locs is set to a pointer to an array of
  struct FaceLocation.  It is the responsibility of the caller
  to free() this array. 
  The structures will also contain the locations of the 
  eyes, if they are found.  The left and right designations
  are from the user's point of view.  The flags left and right
  are set to 1 if that eye is detected, 0 if it is not detected,
  or -1 if the face is to small to attempt detection. */
  
  //int Track_FindAllFaces(int width, int height, unsigned char *buffer,
  //                              struct FaceEyeLocation **locs,int asynch);
  
  /* Like the above function, but also takes parameters restricting the
  search to a rectangular region of the image, and restricts the size
  of the face that will be searched for.  The face must be completely
  within the specified window. */
  
  
  void  SaveDetections(DetectionList *detectionList, Image8 *image,
                     int x, int y, int width, int height,
                     int level,
                     double scale, double output, int orientation);

  // USED
  // Search for the eyes in the face.  Inputs are the image pyramid, 
  // the face location and scale.  Returns with the two eye positions.
  // If the return value is 0, then the face is considered to be too 
  // small for the eye detector to be effective.  If the function returns
  // 1, then the eye detector has been run.  However, in this case the
  // x and y positions being set to -1 indicate that no eye was detected.
  
  int SearchEyes(int numScales, Image8 *imagePyramid[], Image8 *mask,
    int xx, int yy, int s, int basenet,
    int *eyex1, int *eyey1, int *eyex2, int *eyey2);
  
  int SearchLeftEye(Image8 *image, Image8 *mask, double eyewidth,
    int *xeye, int *yeye, int leftEye);
  
  void ZoomByteImage(Image8 *image, double scale, Image8 *src);
  
  // USED
  // Fill a rectangular region of a byte image with the given value
  
  void FillRectangle(Image8 *image, int x1, int y1, int x2, int y2, int color);
  
  // USED
  // Resample an image, according to the following rule: if any of the
  // pixels in the source image which coorespond to a pixel in the
  // destination image are zero, then set the destination pixel to zero.
  // Otherwise, set the pixel to 255.  scale<1 makes the image smaller.
  // The idea is that 0 means a pixel location should be scanned, while
  // 255 means that a face has already been found and need not be checked
  // again.  Destination can be the same as the source.
  // The scaling factor is determined by SCALE, and the size of the
  // destination image is controlled by newWith and newHeight;
  
  void ResampleByteImage(Image8 *image, Image8 *src,
    int newWidth, int newHeight, double scale);
  
  // USED
  // Scale the given byte image down by a factor of 1.2, putting the result
  // in the given destination (which can be the same as the source).  The
  // scaling uses bilinear interpolation, implemented by two steps of linear
  // interpolation: first scaling in the X direction, then in the Y direction.
  
  void Interpolate(Image8 *dest, Image8 *src, double scaleX, double scaleY);
  
  void MirrorByteImage(Image8 *image);
  
  // USED
  // The lighting correction matrix is used to fit an affine function to 
  // the intensity values in a window
  
  void InitializeLightingCorrectionMatrix();
  
  // USED
  // Given a candidate detection location and the image pyramid, search around
  // in that candidate to try and find out whether there is really a face there.
  // Uses two networks (merged together), both of which have to agree on a
  // detection.  If both networks agree on multiple locations, the centroid of
  // those locations is computed.
  
  int FindNewLocation(int numScales, Image8 *imagePyramid[], Image8 *mask,
    int x, int y, int s, int dx, int dy, int ds, int step,
    int basenet,
    int *newX, int *newY, int *news);
  
  // USED
  // Compute a forward pass through the given neural network, and return 
  // the value of the first output unit (which is most useful if the network
  // only has a single output).
  float FastForwardPass(FastForwardStruct *net);
  
  FastForwardStruct *FastLoad(char *netFile);
  
  // USED
  // Given a list network file names, add a ".wet" to the name, and load
  // the weights from those files.  This function loads multiple sets of
  // weights into a single network, and assuming that the same set of network
  // architectures has been loaded by FastLoadMerge.
  void FastLoadMergeWeights(FastForwardStruct *fastnet,
    int numNetworks, char *nets[]);
  
  // USED
  // Load one or more networks into a single network.  The input is the number
  // of networks to load, and either a list of names of network files (to which
  // ".net" will be appended), or a list of FILE pointers from which the networks
  // will be read.  The networks are merged such that they share inputs, then
  // the hidden units are grouped together, and finally the outputs from each
  // network are listed one after another.
  FastForwardStruct *FastLoadMerge(int numNetworks, char *nets[], FILE **netFile = NULL);
  
  void FastReadActivations(FastForwardStruct *net, char *filename);
  
  float *FastInputWeights(FastForwardStruct *net, int from, int to,
    int *width, int *height, int *num);
  
  // USED
  // Get the value of some location in the detection pyramid.  If that location
  // has not already been allocated, create it and set it to zero.
  
  int FuzzyVoteAccumGet(int x, int y, int s);
  
  // USED
  // Get the value of some location in the detection pyramid.  If that location
  // does not exist, then return zero (but do not allocate that location).
  
  int FuzzyVoteAccumCheck(int x, int y, int s);
  
  // USED
  // Zero a value in the detection pyramid. If the value doesn't exist, don't
  // create it since the default is zero. Also, add that location to the list of locations with
  // that value.  
  void FuzzyVoteAccumZero(int x, int y, int s);
  
  // USED
  // Increment a value in the detection pyramid; if that location does not exist,
  // then create it.  Also, add that location to the list of locations with
  // that value.  
  void FuzzyVoteAccumIncr(int x, int y, int s);
  
  // USED 
  // Given some point in the detection pyramid, locate all 6-connection 
  // locations with a value greater than or equal to the specified amount,
  // and find their centroid.  The locations in the centroid are set to zero.
  // Centroid in scale is computed by averaging the pyramid levels at which
  // the faces are detected.
  void FindCentroidAccum(int numImages, int s, int x, int y,
    int minVal, int currVal,
    double *totalS, double *totalX, double *totalY,
    double *total);
  
  // USED
  // Search the given image using the 30x30 candidate detector.  The levelmask
  // is used to indicate which portions of the image to not bother searching,
  // because a face has already been found there.  When a face is found, 
  // the callback function is called with informatoin about the detection.
  
  void SearchFaster(Image8 *image, Image8 *levelmask, int level,
    DetectionList *detectionList, double threshold = 0.0);
  
  void FindCentroid(Image8 *images[], int numImages,
    int s, int x, int y, int val,
    double *totalS, double *totalX, double *totalY,
    double *total);
  
#ifdef __GNUC__
  class ListClass<AccumElement>;
  class ListNode<AccumElement>;
#endif
  
  // USED 
  // Given some point in the detection pyramid, locate all 6-connection 
  // locations with a value greater than or equal to the specified amount,
  // and find their centroid.  The locations in the centroid are set to zero.
  // Centroid in scale is computed by averaging the pyramid levels at which
  // the faces are detected.
  
  void FindCentroidAccum(int numImages, int s, int x, int y, int val,
    double *totalS, double *totalX, double *totalY,
    double *total);
  
  // USED
  // Implementation of the arbitration mechanisms described in the paper.
  // Inputs are lists of detections, the width and height of the image that
  // is being processed, a callback function used to report the arbitration
  // results, and a bunch of parameters for the arbitration itself.
  
  void FuzzyVote2(int width, int height, int numLocFiles,
    DetectionList detections[],
    DetectionList *detectionList,
    int spread, int search, int collapse, int overlap,
    int filterodd, Image8 *mask);
  
  // USED
  // Allocate an array of pointers to neural networks.  This array is global
  // (yuck!).  If an array already exists, it and the networks it points to
  // are deleted.
  
  void FastAllocateNetworks(int num);
  
  // USED
  // Load a network, and place it in the array created by FastAllocateNetworks
  
  void FastInitNetwork(int num, int numNetworks, char *networkList[]);
  
  // Returns max(b, min(a, c))
  int clamp(int a, int b, int c)
  {
    return std::max(b, std::min(a, c));
  }


  
  //
  // ATTRIBUTES
  //

  // scale factors applied in constructing pyramid
  static double mPyramidScaleX; 
  static double mPyramidScaleY;

  // An array of lists.  Each lists contains all the detection location/scales
  // with a particular value.  This allows the detections to be scanned in
  // order from highest to lowest with practically no overhead.
  ListClass<AccumElement> mBins[256];
  
  AccumElementMap mAccumElementMap;

  // Eye and face mask
  Image8 mEyeMask;
  Image8 mFaceMask;

  //mem vars.
  int verbose;
  int mInitialized;
  std::string mDataPath;
  bool mLightingCorrectionMatrixInitialized;

  FastForwardStruct** mRealFastList;
  int mRealNumNetworks;
  Mat LightingCorrectionMatrix;

  // Number of milliseconds to sleep between iterations to 
  // avoid that this thread eats all the CPU.
  int mSleepMilliseconds;

  /**
   * Integral image of the mask that indicates where to look for faces.
   * @see FaceDetector::setMask()
   */
  Image<unsigned short> *mpInclusionIntegralImageMask;

  /**
   * Fraction of the area of the mask that must be set in order to look for a face.
   * @see FaceDetector::setMask()
   */
  float mInclusionAreaFactor;
  
};

//end class

} // namespace ait

#endif





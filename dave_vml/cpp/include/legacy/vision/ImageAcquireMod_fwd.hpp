/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ImageAcquireMod_fwd_HPP
#define ImageAcquireMod_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class ImageAcquireMod;
typedef boost::shared_ptr<ImageAcquireMod> ImageAcquireModPtr;
class ImageAcquireMsg;
typedef boost::shared_ptr<ImageAcquireMsg> ImageAcquireMsgPtr;

}; // namespace vision

}; // namespace ait

#endif // ImageAcquireMod_fwd_HPP


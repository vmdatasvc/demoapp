/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VisionFactory_fwd_HPP
#define VisionFactory_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class VisionFactory;
typedef boost::shared_ptr<VisionFactory> VisionFactoryPtr;

}; // namespace vision

}; // namespace ait

#endif // VisionFactory_fwd_HPP


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VisionMod_fwd_HPP
#define VisionMod_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class VisionMod;
typedef boost::shared_ptr<VisionMod> VisionModPtr;

}; // namespace vision

}; // namespace ait

#endif // VisionMod_fwd_HPP


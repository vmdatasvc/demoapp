/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ImageAcquireMod_HPP
#define ImageAcquireMod_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>
#include <legacy/pv/ait/Vector3.hpp>
//#include <Benchmark.hpp>
//#include <VisualSensorId.hpp>
#include <legacy/pv/CircularBuffer.hpp>
#include <legacy/media/ImageAcquireProperty.hpp>
#include <legacy/media/ImageAcquireDeviceBase.hpp>

// LOCAL INCLUDES
//

#include "legacy/vision/FrameInfo.hpp"
//#include "legacy/vision/RealTimeMod.hpp"
#include "legacy/vision/VisionMod.hpp"
//#include "ImageAcquireMsg.hpp"

// FORWARD REFERENCES
//

#include <legacy/media/ImageAcquireDeviceBase_fwd.hpp>
//#include <BackgroundSubIRFlashing_fwd.hpp>
//#include "legacy/vision/ImageAcquireMod_fwd.hpp"

// use opencv
//#include <opencv2/opencv.hpp>
//#include <opencv2/core/core.hpp>
//#include <opencv2/video/video.hpp>

namespace ait 
{

namespace vision
{


/**
 * Get video from source.
 * @par Responsibilities:
 * - Get one frame from the input device at a time.
 */
  class ImageAcquireMod  : public VisionMod	// previously RealtimeMod
  {
  public:

    //
    // LIFETIME
    //

    /**
    * Constructor. The constructor creates a quick instance. Expensive operations
    * are executed in the init() function.
    */
    ImageAcquireMod();

    /**
    * Destructor
    */
    virtual ~ImageAcquireMod();

    /**
    * This function will initialize the frame rate for this module.
    */
//    virtual void initExecutionGraph(std::list<VisionModPtr>& srcModules);

    /**
    * Initialization function.
    */
    void init();

    /**
    * Finalization function
    */
    void finish();

    //
    // OPERATIONS
    //

  public:

    /**
    * Grab a frame and copy it to the visualization frame (if enabled).
    * In addition, this function calculates the multi-resolution images.
    */
    virtual void process();

    /**
    * Get the current frame
    */
    Image32& getCurrentFrame();

    /**
    * Get a smart pointer to the current frame
    */
    Image32Ptr getCurrentFramePtr();

    /**
    * Get current frame time in seconds.
    */
    double getCurrentFrameTime()
    {
      return mCurrentFrameTimeStamp;
    }

    /**
    * Get the current frame information.
    * @remark Image field in the info is not modified. It is not
    * possible to get a smart pointer to the image. Use getCurrentFrame()
    * instead.
    */
    void getCurrentFrameInfo(FrameInfo& info);

    /**
    * Get the current Visualization Frame from the image acquire module.
    */
    Image32& getVisualizationFrame();

    /**
    * Get a frame from the history at a certain resolution.
    * @param backIndex [in] Number of frames to go back in history (0 = current frame; 
    * 1 = prev frame; 2 = prev to prev frame...)
    * @param subSampleLevel [in] Levels of subsampling (0 = current widthxheight; 
    * 1 = with*0.5xheight*0.5; ...)
    */
    Image32& getHistoryFrame(Int backIndex, Int subSampleLevel = 0);

    /**
    * Get the frames per seconds of this device.
    */
    virtual double getFramesPerSecond() const {return mDeviceFps / (float)mSkipFrameInterval;}

    /**
    * Set the synchronization master for lowest-level synchronization.
    */
//    virtual void setSyncMaster(ImageAcquireModPtr pMaster);

    /**
    * Get a property from the camera. This property can be changed to modify
    * the camera settings. This operation is not thread safe.
    */
    ImageAcquirePropertyPtr getProperty(const ImageAcquireProperty::Code code);

    /**
    * Change the size of the visualization frame. If this function is called
    * more than once, it will keep the maximum specified size.
    */
    void setVisualizationFrameSize(const Vector2i& size);

    /**
    * Get the total number of frames in the video file. If this
    * is a camera, the return value will be zero. This is equivalent
    * to the number of times process() will be called for each module
    * if the execution is not interrupted.
    */
    int getTotalNumberOfFrames();

  protected:
    /**
    * Initialize the ring buffers according to the member variables.
    */
    void initRingBuffers();

    /**
    * Release ring buffers to allow reinitialization
    */
    void releaseRingBuffers();

    /**
    * Initialize the specified device.
    * @param deviceID [in] Name of the device from the ImageAcquire database.
    */
    void initDevice(const std::string& deviceID);

    /**
    * Check interactive flags for this module.
    */
    void notifySettingsChange(const std::string& notifyString);

    /**
    * Called from process() normally
    */
    void normalProcess();

    //
    // ACCESS
    //

  public:

    /**
    * Get the static name of this module. This name is used to name the type of this module.
    * The constructor sets the member variable mName to this value.
    * @remark This static function is only implemented for concrete classes.
    */
    static const char *getModuleStaticName() { return "ImageAcquireMod"; }

    /**
    * Get the registered size of the frame.
    */
    const Vector2i& getFrameSize() const { return mFrameSize; }

    /**
    * Get the visual sensor id.
    */
//    VisualSensorId getVisualSensorId() const { return mVisualSensorId; }

    /**
    * Get the number of the current frame.
    */
    int getCurrentFrameNumber() 
    {
      //PVMSG("CURRENT FRAME NO: %d\n", mCurrentFrameIndex);   
      return mCurrentFrameIndex * mSkipFrameInterval;
    }

    //
    // INQUIRY
    //

  public:

    /**
    * Returns true if the current video feed comes from a live 
    * continuous video source.
    */
    bool isLiveVideoSource();

    virtual Bool isAsync() const {return true;}	// for research, it is always async
    //
    // ATTRIBUTES
    //

  protected:

    typedef CircularBuffer<Image32Ptr> FrameHistory;
    typedef boost::shared_ptr<FrameHistory> FrameHistoryPtr;

    /**
    * Device for image acquisition.
    */
    ImageAcquireDeviceBasePtr mpDevice;

    /**
    * Array of circular buffers for frames. Each circular buffer contains a resolution 
    * (e.g.: 0 = 320x240, 1 = 160x120, 2 = 80x60, etc.)
    */
    std::vector<FrameHistoryPtr> mMultiResFrames;

    /**
    * Number of subscale images to generate from each frame. After capturing a frame, 
    * the images will be resampled.
    */
    int mNumSubScaleLevels;

    /**
    * Number of frames to keep in the circular buffer.
    */
    int mNumHistoryFrames;

    /**
    * Size of the frame buffer.
    */
    Vector2i mFrameSize;

    /**
    * Visualization frame.
    */
    Image32Ptr mpVisualizationFrame;

    /**
    * Frame index counter. Starts in zero. At 30 Fps it will last more
    * than 20 years to overflow.
    */
    Uint32 mCurrentFrameIndex;

    /**
    * Time stamp for the last frame.
    */
    double mCurrentFrameTimeStamp;

    /**
    * Time stamp for the first frame.
    */
    double mFirstFrameTimeStamp;

    /**
    * Benchmark.
    */
//    Benchmark mBenchmark;

    /**
    * Visual sensor Id.
    */
//    VisualSensorId mVisualSensorId;

    /**
    * Frames per second obtained from the device. This is the rate at which
    * videos should run.
    */
    double mDeviceFps;

    /**
    * Reference to the sync master.
    */
    ImageAcquireModPtr mpSyncMaster;

    /**
    * This indicates if the current is a real time
    * device such as a camera, or a device that can run faster
    * than real time. This is used to calculate the current time.
    * If it is a real time device, the current time is calculated
    * from the system clock. Otherwise it is calculated by
    * multiplying the frame index by the frame rate.
    */
    bool mIsRealTimeDevice;

    /**
    * Keeps track of how many frames are skipped by the ImageAcquireDevice.
    * This variable is set within initDevice()
    */
    int mSkipFrameInterval;

    /**
    * If enabled, the start time for processed videos will become the value
    * specified by "startTimestamp", instead of simply
    * using PvUtil::time()
    *
    * The format is YYYYMMDD_HHMMSS
    */
    bool mUseStartingTimestamp;

    /**
    * A value (representing seconds from the epoch) that will be used
    * as the start time for the internal module clock.
    */
    double mStartTimestampTime;

  };


}; // namespace vision

}; // namespace ait

#endif // ImageAcquireMod_HPP



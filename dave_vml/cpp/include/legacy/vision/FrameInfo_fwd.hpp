/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef FrameInfo_fwd_HPP
#define FrameInfo_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class FrameInfo;
typedef boost::shared_ptr<FrameInfo> FrameInfoPtr;

}; // namespace ait

#endif // FrameInfo_fwd_HPP


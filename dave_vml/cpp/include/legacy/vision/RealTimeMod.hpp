/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef RealTimeMod_HPP
#define RealTimeMod_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>
#include <legacy/pv/ait/Image.hpp>
#include <legacy/pv/ait/Image_fwd.hpp>

// LOCAL INCLUDES
//

#include "legacy/vision/VisionMod.hpp"

// FORWARD REFERENCES
//

#include "RealTimeMod_fwd.hpp"
#include "ImageAcquireMod_fwd.hpp"

namespace ait 
{

namespace vision
{


/**
 * Modules that are executed at frame rate.
 * @par Responsibilities.
 * - Process the current frame without degrading the frame rate.
 * - Define frame rate.
 * - Keep a reference to the respective ImageAcquire module(s).
 * - Simple interface for getting the current frame and frame counter.
 * @par Patterns:
 * Template Method (all virtual functions), prototype (definition of messages).
 */
class RealTimeMod : public VisionMod
{
public:

  //
  // LIFETIME
  //

  /**
   * Constructor
   */
  RealTimeMod(const std::string& moduleName);

  /**
   * Destructor
   */
  virtual ~RealTimeMod();

  /**
   * Init the graph structure. Called before and after initialization.
   */
  virtual void initExecutionGraph(std::list<VisionModPtr>& srcModules);

  /**
   * Initialization. Get the reference to the image acquire module.
   */
  virtual void init();

  /**
   * Finalization. Release the reference to the image acquire module.
   */
  virtual void finish();

  //
  // OPERATIONS
  //

public:


  /**
   * Return the number of frames per seconds that this module runs in.
   * The default implementation will get the first image acquisition module frame rate.
   */
  virtual double getFramesPerSecond() const;

  /**
   * Get a reference to the image acquire module.
   * For modules that require more than one camera, the index will be used to identify the
   * desired camera.
   * @param index [in] Index of the image acquisition module in this module.
   */
  ImageAcquireModPtr getImageAcquireModule();

  /**
   * Return a const version of the image acquire module.
   */
  const ImageAcquireMod * const getImageAcquireModule() const;

  /**
   * Get the current frame from the image acquire module.
   * This call is equivalent to getImageAcquireModule()->getCurrentFrame().
   */
  Image32& getCurrentFrame();

  /**
   * Get the current visualization frame from the image acquire module.
   * This call is equivalent to getImageAcquireModule()->getVisualizationFrame().
   */
  Image32& getVisualizationFrame();

  /**
   * Get the current time. Equivalent to 
   * getImageAcquireModule()->getCurrentFrameTime();
   */
  double getCurrentTime();

  /**
   * Get the current frame number.
   */
  int getCurrentFrameNumber();

  /**
   * Get the visual sensor id for this thread. Shortcut for
   * getImageAcquireModule()->getVisualSensorId();
   */
//  VisualSensorId getVisualSensorId();
  
  //
  // ACCESS
  //

public:


  //
  // INQUIRY
  //

public:

  /**
   * Return always false because any derived module is not asynchronous.
   */
  virtual Bool isAsync() const { return false; }

  /**
   * Return true, this is a real time module.
   * @remark Although this information can be obtained using RTTI, this
   * is more efficient and simpler to read.
   */
  virtual bool isRealTime() const { return true; }

  //
  // ATTRIBUTES
  //

protected:

  /**
   * Reference to the Image Acquire module.
   */
  ImageAcquireModPtr mpImageAcquireMod;

};


}; // namespace vision

}; // namespace ait

#endif // RealTimeMod_HPP



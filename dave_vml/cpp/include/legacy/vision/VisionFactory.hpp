/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VisionFactory_HPP
#define VisionFactory_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <boost/thread/thread.hpp>
#include <map>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>
#include <legacy/pv/PvUtil.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

#include "legacy/vision/VisionFactory_fwd.hpp"
#include "legacy/vision/VisionMod_fwd.hpp"

namespace ait 
{

/**
 * Call this function to register intelligence modules.
 * @remark This static member was created to reduce the size of 
 * executables. In many cases applications do not use all the
 * vision modules.
 */
void registerIntelligenceModules();

/**
 * Call this function to register interaction modules.
 * @remark This static member was created to reduce the size of 
 * executables. In many cases applications do not use all the
 * vision modules.
 */
void registerInteractionModules();

namespace vision
{

/**
 * Get modules, or create modules if requested.
 * @par Responsibilities:
 * - Create the module class instances.
 * - Get a reference to existing modules.
 * - Parse the full names for modules.
 * - Register new modules.
 * @par Patterns:
 * - Factory
 * - Singleton
 * @remark
 * This class is thread safe. This means that module references can be obtained
 * from any thread. Note however that the modules are generally not thread safe.
 */
class VisionFactory : private boost::noncopyable
{

public:

  /**
   * This is the base functor to create new modules.
   */
  class ModuleCreator
  {
  public:
    virtual VisionMod *operator ()() = 0;
  };
  typedef boost::shared_ptr<ModuleCreator> ModuleCreatorPtr;

  /**
   * This template class instantiates every creator for each function.
   */
  template <class T>
  class Creator: public ait::vision::VisionFactory::ModuleCreator 
  { 
  public: 
    virtual ait::vision::VisionMod *operator ()() { return new T(); } 
  };

  //
  // LIFETIME
  //

public:
  /**
   * Constructor.
   */
  VisionFactory();

  /**
   * Destructor
   */
  virtual ~VisionFactory();

  /**
   * Singleton instance access.
   */
  static VisionFactory *instance();

  //
  // OPERATIONS
  //

public:


  /**
   * Get a reference to a module. This member will look for the module in the internal
   * set of references to active modules. The module will not be initialized by this
   * routine. If the module can't be obtained or created, it will exit the program with 
   * an error.
   * @param exePathName [in] name of the execution path where the module is located.
   * @param moduleName [in] name of the module to reference.
   * @param pMod [out] pointer to the allocated module.
   * @remark
   * Prefer the use of VisionMod::getModule() if inside a module, that function is more
   * intelligent than this one.
   * This function will first call teh garbageCollect() member, as a result if a module
   * is completely de-referenced and then immediately referenced, it will destroy the 
   * instance and create a new one.
   */
  template <class T> void getModule(const std::string& exePathName, 
                                    const std::string& moduleName, 
                                    boost::shared_ptr<T>& pMod)
  {
    VisionModPtr pVMod;

    getModuleAux(exePathName, moduleName, pVMod);

    pMod = boost::dynamic_pointer_cast<T>(pVMod);

    if (pMod.get() == NULL)
    {
      PvUtil::exitError("Invalid type for module %s",moduleName.c_str());
    }
  }

  /**
   * Template version of the previous member function. This function provides a simpler interface
   * for getModule by means of templates. Otherwise the user would have to use C++ casting operators
   * to get the correct type of vision module. The name of the module is obtained using 
   * getModuleStaticName().
   * @param exePathName [in] name of the execution path where the module is located.
   * @param pMod [out] pointer to the allocated module.
   * @par Example:
   * @code 
   *   // This code gets the image acquire module instance in the execution path named 'Camera0'.
   *   VisionFactory& vf = *VisionFactory::instance();
   *   ImageAcquireModPtr pMod;
   *   vf.getModule("Camera0",pMod);
   * @endcode
   * @remark
   * Prefer the use of VisionMod::getModule() if inside a module, that function is more
   * intelligent than this one.
   * @see getModule
   */
  template <class T> void getModule(const std::string& exePathName, boost::shared_ptr<T>& pMod)
  {
    getModule(exePathName, T::getModuleStaticName(), pMod);
  }

  /**
   * Register a module constructor. If a module is registered multiple times, only the last
   * registration will be kept.
   * @param Functor creator.
   */
  void registerModule(const std::string& modName, ModuleCreatorPtr creatorFunctor);

  /**
   * Destroy unused modules. This function will check if the shared pointer is unique (is not
   * referenced by any other instance), and if this is the case, it will remove it from the
   * respective ModulesMap. If the ModulesMap is empty for the given execution path, it will
   * remove that entry as well. To check if the shared pointer is unique, boost::shared_ptr
   * provides the member unique().
   */
  void garbageCollect();

  /**
   * Parse a full name into execution path name and module name.
   * @param fullName [in] The composed name.
   * @param executionPathName [out] The execution path name. If none was specified, return "".
   * @param moduleName [out] Name of the module.
   */
  static void parseFullName(const std::string& fullName, std::string& executionPathName, std::string& moduleName);

private:

  /**
   * Function that implements the module search.
   * @see getModule
   */
  void getModuleAux(const std::string& exePathName, const std::string& moduleName, VisionModPtr& pMod);

  /**
   * Create a module in the given execution path.
   * @param srcExePathName [in] name of the execution path where the module is located.
   * @param moduleName [in] name of the module to reference.
   * @return newModule.
   */
  VisionMod* createModule(const std::string& exePathName, const std::string& moduleName);

  //
  // ACCESS
  //

public:

  //
  // ATTRIBUTES
  //

protected:

  /// Map from Module names to Module Pointers.
  typedef std::map<std::string,VisionModPtr> ModulesMap;

  /// Pointer to a module map.
  typedef boost::shared_ptr<ModulesMap> ModulesMapPtr;

  /// Map from paths names to a map of {ModuleName:ModulePtr}.
  typedef std::map<std::string,ModulesMapPtr> PathsMap;

  /// Map to registered module creator functors.
  typedef std::map<std::string,ModuleCreatorPtr> ModuleCreatorsMap;

  typedef boost::mutex Mutex;
  typedef boost::mutex::scoped_lock Lock;

  /**
   * This map contains the set of active modules that have been referenced
   * with getModule. The string format is <ExecutionPath>:<ModuleName>.
   */
  PathsMap mPaths;

  /**
   * This map contains the set of registered vision modules.
   */
  ModuleCreatorsMap mModuleCreators;

  /**
   * This object will be used to ensure thread safety.
   */
  Mutex mMutex;

};

}; // namespace vision

}; // namespace ait

/**
 * This macro is a shorthand for registering modules.
 */
#define AIT_REGISTER_VISION_MODULE(modClassName) \
  ait::vision::VisionFactory::instance()->registerModule(modClassName::getModuleStaticName(), \
    ait::vision::VisionFactory::ModuleCreatorPtr(new ait::vision::VisionFactory::Creator<modClassName>()))

#endif // VisionFactory_HPP


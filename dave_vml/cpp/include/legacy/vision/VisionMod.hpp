/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VisionMod_HPP
#define VisionMod_HPP

// SYSTEM INCLUDES
//

#include <vector>
#include <list>
#include <boost/utility.hpp>
#include <boost/thread/thread.hpp>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>
#include <legacy/pv/ait/Image.hpp>
#include <legacy/low_level/Settings.hpp>

// LOCAL INCLUDES
//

#include <legacy/vision/ImageAcquireMod_fwd.hpp>
#include <legacy/vision/VisionFactory.hpp>
//#include "VisionMsg.hpp"
//#include "MsgQueue.hpp"

// FORWARD REFERENCES
//

#include "VisionMod_fwd.hpp"

namespace ait 
{

namespace vision
{

/**
 * Base class for all vision modules.
 * @par Responsibilities:
 * - Process input.
 * - Draw visual feedback in frame image.
 * - Contain the Settings object to set parameters for this module.
 * - Initialize and destroy the internal data.
 * - Provide access other vision modules
 * - Message Handling:
 *   - Have a message queue to receive messages.
 *   - Keep a list of receivers for outgoing messages.
 *   - Send messages to all receivers.
 * - Define execution information: 
 *   - Define source execution dependencies.
 * @par Patterns:
 * - Template Method (all virtual functions)
 */
class VisionMod : private boost::noncopyable
{
public:

  //
  // LIFETIME
  //

  /**
   * Constructor
   */

  VisionMod();

  VisionMod(const std::string& moduleName);

  /**
   * Destructor
   */
  virtual ~VisionMod();

  /**
   * Object initialization
   */
  virtual void init();

  /**
   * Object deallocation
   */
  virtual void finish();

  /**
   * This member is to provide dependency information to the execution graph.
   * @param srcModules [out] Reference to the source modules.
   * Use VisionMod::getModule() to get the references to these modules.
   * The only operations allowed here should be related to building the graph
   * structure. Those include determining the frame rate, setting the priority,
   * and filling the list of source dependencies.
   * @remark This function will be called multiple times while building the graph
   * and enabling source dependencies. In particular it is called before the
   * module is initialized.
   */
//  virtual void initExecutionGraph(std::list<VisionModPtr>& srcModules);
  virtual void initExecution();

  //
  // OPERATIONS
  //

public:

  /**
   * Execute processing for this iteration step.
   */
  virtual void process();

  /**
   * Process all the messages from the queue.
   */
  virtual void processMessages();

  /**
   * Display visual feedback in camera view frame.
   */
  virtual void visualize(Image32& frame);

  /**
   * Settings change notification. On every frame, before calling process,
   * the settings key InteractiveSettings/interactiveNotification. The change
   * is detected by the control module, and a flag is set to notify all the
   * modules.
   * @param notifyString [in] std::string that hints the type of change that 
   * was made. A module can use this string to know if the change refers
   * to any of its variables.
   */
  virtual void notifySettingsChange(const std::string& notifyString) {}

  /**
   * Enable/disable visualization for this module
   */
  void setVisualize(Bool set);

  /**
   * Set the execution thread name.
   * @param pathName [in] Name of the execution path. "Camera0" is the name of the path
   * by default.
   */
  void setExecutionPath(const std::string& pathName);

  /**
   * Get a reference of a module.
   * If the module does not exists, it will be created and initialized.
   * @param name [in] Name of the module. This name can be prefixed with the name of
   * the execution path and a colon. The module is searched using the following steps:
   * - If an execution path was specified, look for the module in the thread. Go to the last step.
   * - Look for the module in the same thread. If found goto last step.
   * - Look for the module in other threads.
   * - If the module was found, return a reference to it. Otherwise, create it in the specified
   * thread (or in the same thread as the 'this' module if none was specified).
   * @param pMod [out] reference to the module.
   * @remark If the module could not be created, the application exits with error.
   */
  template <class T> void getModule(const std::string& name, boost::shared_ptr<T>& pMod)
  {
    std::string pathName, modName;

    VisionFactory::parseFullName(name,pathName,modName);
    if (pathName.empty()) pathName = getExecutionPathName();
    VisionFactory::instance()->getModule(pathName, modName, pMod);
  }

  /**
   * Add an output message queue.
   * Other modules can use this function to request output messages to be sent to them.
   * @remark This function is thread safe; however, if a module is enabled before
   * adding the output message queue, the receiving module might miss some messages.
   * Applications should add the message queues before enabling the modules.
   */
//  void addOutputMsgQueue(MsgQueuePtr pDest);

  /**
   * Put a message in this module's input message queue.
   * @param pMsg [in] pointer to the message to be sent.
   */
//  void putMsg(VisionMsgPtr pMsg);

protected:

  /**
   * Send message to the destination modules.
   * This copy will send a reference to the message to each destination module.
   * The reference will be shared.
   */
//  virtual void sendMessage(VisionMsgPtr pMsg);

  //
  // ACCESS
  //

public:


  /**
   * Get the complete display name for this module, this is <ExecutionPath>:<ModuleName>.
   */
  std::string getFullName() const { return getExecutionPathName() + ":" + getModuleName(); }

  /**
   * Get the name of this module.
   */
  const std::string& getModuleName() const { return mName; }

  /**
   * Get the name of the execution path of this module.
   */
  const std::string& getExecutionPathName() const { return mPathName; }

  /**
   * Get the parent module for this submodule. It is not a smart pointer because it would create a circular dependency.
   * @see setParentModule
   */
  VisionMod* getParentModule();

  /**
   * Set the parent module.
   * SubModules can only have one parent module. Changing
   * the parent module is illegal.
   * @remark The difference between normal vision modules and sub-modules is that
   * the execution graph will not call process() and visualize() for these modules.
   * The source dependencies of sub-modules are propagated to the parent object.
   * The parent object is responsible for calling the necessary functions to process
   * the input. It can make any type of synchronous calls because a sub-module can
   * only have one parent and it won't be executed asynchronously.
   */
  void setParentModule(VisionModPtr& pParent);

  /**
   * Get the input message queue of this module.
   */
//  MsgQueuePtr getInputMsgQueue();

  /**
   * The execution priority defines the order in which modules are executed.
   * The priority ranges from 0 (Highest) to 10 (Lowest). All modules have
   * default priority of 5.
   * @remark Don't use this flag to set execution dependency orders between
   * specific modules. See initExecutionGraph() for that. The priority is used
   * for example by the VideoWriter module that must be executed after all
   * other modules to get a proper visualization.
   * @remark Priorities override dependencies. Inconsistent priorities can 
   * break dependencies. For example, if SampleMod with priority 5, has 
   * VideoWriter with priority 9 as a source dependency, SampleMod will be 
   * executed first because it has higher priority.
   */
  void setExecutionPriority(int p)
  {
    mExecutionPriority = p;
  }

  /**
   * Return the execution priority.
   * @see setExecutionPriority().
   */
  int getExecutionPriority()
  {
    return mExecutionPriority;
  }

  /**
   * Get the frame rate of this module. The frame rate is defined to be zero
   * if the module doesn't have a defined frame rate.
   */
  virtual double getFramesPerSecond() const { return 0; }

  /**
   * Return the settings.
   */
  Settings& getSettings();

  //
  // INQUIRY
  //

public:

  /**
   * Ask if this object has been initialized (the init() function has been called).
   */
  Bool isInitialized() const { return mIsInitialized; }

  /**
   * Flag to tell if this module is a sub module.
   * @see setParentModule
   */
  Bool isSubModule();

  /**
   * Return true if this module is visualizing information.
   * @todo Register visualization flags in the settings.
   */
  Bool isVisualize();

  /**
   * Return true if this module is asynchronous.
   */
  virtual Bool isAsync() const = 0;

  /**
   * Return true if this is a real time module.
   */
  virtual bool isRealTime() const { return false; }

  /**
   * Get a reference to the image acquire module.
   * For modules that require more than one camera, the index will be used to identify the
   * desired camera.
   * @param index [in] Index of the image acquisition module in this module.
   */
  ImageAcquireModPtr getImageAcquireModule();

  /**
   * Return a const version of the image acquire module.
   */
  const ImageAcquireMod * const getImageAcquireModule() const;

  /**
   * Get the current frame from the image acquire module.
   * This call is equivalent to getImageAcquireModule()->getCurrentFrame().
   */
  Image32& getCurrentFrame();

  /**
   * Get the current visualization frame from the image acquire module.
   * This call is equivalent to getImageAcquireModule()->getVisualizationFrame().
   */
  Image32& getVisualizationFrame();

  /**
   * Get the current time. Equivalent to
   * getImageAcquireModule()->getCurrentFrameTime();
   */
  double getCurrentTime();

  double testreturn();

  /**
   * Get the current frame number.
   */
  int getCurrentFrameNumber();


  //
  // ATTRIBUTES
  //

protected:

  typedef boost::mutex Mutex;
  typedef boost::mutex::scoped_lock Lock;

  /**
   * Name of this module.
   */
  std::string mName;

  /**
   * Name of the execution path of this module.
   */
  std::string mPathName;

  /**
   * Tells if this object has been initialized.
   */
  Bool mIsInitialized;

  /**
   * Input message queue.
   */
//  MsgQueuePtr mpInputMsgQueue;

  /**
   * Output message queues.
   */
//  std::list<MsgQueuePtr> mOutputMsgQueues;

  /**
   * Mutex to make sendMessages and addOutputMsgQueue thread safe.
   */
  Mutex mQueuesMutex;

  /**
   * Settings object to read/save parameters.
   */
  Settings mSettings;

  /**
   * Visualization feedback flag.
   */
  Bool mVisualizeFlag;

  /**
   * Pointer to the parent module. Note that this can not be a smart pointer, otherwise 
   * it would create circular dependencies.
   */
  VisionMod *mpParentModule;

  /**
   * Execution priority.
   * @see setExecutionPriority().
   */
  int mExecutionPriority;

  // NEW: pointer to ImageAcquireMod is moved to here from RealtimeMod
  // NOTE: all vision mod now do not inherit from RealTimeMod
  //       Execution path should be manually set by invoking source mods in init phase.
  ImageAcquireModPtr mpImageAcquireMod;

};


}; // namespace vision

}; // namespace ait

#endif // VisionMod_HPP



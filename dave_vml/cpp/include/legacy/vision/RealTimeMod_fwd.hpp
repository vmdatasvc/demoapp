/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef RealTimeMod_fwd_HPP
#define RealTimeMod_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class RealTimeMod;
typedef boost::shared_ptr<RealTimeMod> RealTimeModPtr;

}; // namespace vision

}; // namespace ait

#endif // RealTimeMod_fwd_HPP


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef SmoothAnisotropic_HPP
#define SmoothAnisotropic_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/shared_ptr.hpp>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>
#include <legacy/pv/ait/Vector3.hpp>
#include <legacy/pv/CircularBuffer.hpp>
#include <legacy/vision_tools/SmoothTrajectory.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

// Shriram

namespace ait 
{


/**
 * This class implements a method to filter positions 
 */
class SmoothAnisotropic : public SmoothTrajectory<float>
{
  //
  // LIFETIME
  //

public:

  typedef Matrix<float> PvMatrixt;

  /**
   * Constructor.
   */
  SmoothAnisotropic()
  {	  
  }

  /**
   * Destructor
   */
  virtual ~SmoothAnisotropic()
  {
  }

  //
  // OPERATIONS
  //

public:


  /**
   * Initialization of the algorithm.
   * @param k [in] gradient function to determine the conduction coefficient.
   * @param paramLambda [in] weighting parameter. 
   */
  void init(int k, float lambda)
  {
    mParamK = k;
    mParamLambda = lambda;
    mLastDiff = 0;
    reset();
  }

  /**
   * Filter the current trajectory. Use state() to retreive the filtered position.
   * @param v [in] new position.
   * @param v [out] filtered position.
   */
  virtual void update(PvMatrixt& v)
  {
    //PVMSG("--- (%f,%f,%f) -> ",v(0),v(1),v(2));
    PvMatrixt newFilteredPos;
    float conductionCoeff;
    float multFactor = 1;
    float diff=0;   	
    int s=v.cols();
    float *d = new float[s];
    
    if(mLastFilteredPos.cols() != s)
    {
      mLastFilteredPos.resize(s);
      mLastFilteredPos = v;
    }
    
    for(int k=0;k<s;k++)
    {
      d[k] = (v(k) - mLastFilteredPos(k));
      diff+=d[k]*d[k];
    }
    
    //diff = sqrt(diff);
    //conductionCoeff = 1 / (1 + (diff * diff / (mParamK * mParamK)));
    conductionCoeff = 1 / (1 + (diff / (mParamK * mParamK)));
    
    mFilteredPosMatrix.resize(s);
    newFilteredPos.resize(s);
    
    for(int k=0;k<s;k++)
    {
      newFilteredPos(k) = mLastFilteredPos(k) + mParamLambda * conductionCoeff * d[k];
      mFilteredPosMatrix(k) = newFilteredPos(k);
    }
    
    mLastFilteredPos = newFilteredPos;
    
    v = mFilteredPosMatrix;
    
    delete [] d;
    
    //    PVMSG("(%f,%f,%f)\n",v(0),v(1),v(2));
  }


  /**
   * Filter the current trajectory. Use state() to retreive the filtered position.
   * @param v [in] new position.
   * @param v [out] filtered position.
   */
  /*
  virtual void update_old(PvMatrixt& v)
  {
    //PVMSG("--- (%f,%f,%f) -> ",v(0),v(1),v(2));

    Vector3f newFilteredPos;
    //Vector3f conductionCoeff;
    float conductionCoeff;
    float d0, d1, d2;
    float multFactor = 1;
    
    d0 = (v(0) - mLastFilteredPos(0));
    d1 = (v(1) - mLastFilteredPos(1));
    d2 = (v(2) - mLastFilteredPos(2));


//    PVMSG("(%f,%f,%f) ->",mLastFilteredPos(0), mLastFilteredPos(1), mLastFilteredPos(2));

//    PVMSG("(%f,%f,%f) ->",d0,d1,d2);

    float diff = sqrt(d0*d0 + d1*d1 + d2*d2);

    //conductionCoeff = exp(- (diff * diff) / (mParamK * mParamK));
    conductionCoeff = 1 / (1 + (diff * diff / (mParamK * mParamK)));

    //conductionCoeff(0) = exp(- (d0 * d0 * multFactor * multFactor) / (mParamK * mParamK));
    //conductionCoeff(1) = exp(- (d1 * d1 * multFactor * multFactor) / (mParamK * mParamK));
    //conductionCoeff(2) = exp(- (d2 * d2 * multFactor * multFactor) / (mParamK * mParamK));

    //conductionCoeff(0) = 1 / (1 + ((d0 * d0)/ (mParamK * mParamK)));
    //conductionCoeff(1) = 1 / (1 + ((d1 * d1)/ (mParamK * mParamK)));
    //conductionCoeff(2) = 1 / (1 + ((d2 * d2)/ (mParamK * mParamK)));

    //newFilteredPos(0) = mLastFilteredPos(0) + mParamLambda * conductionCoeff(0) * d0;
    //newFilteredPos(1) = mLastFilteredPos(1) + mParamLambda * conductionCoeff(1) * d1;
    //newFilteredPos(2) = mLastFilteredPos(2) + mParamLambda * conductionCoeff(2) * d2;

    newFilteredPos(0) = mLastFilteredPos(0) + mParamLambda * conductionCoeff * d0;
    newFilteredPos(1) = mLastFilteredPos(1) + mParamLambda * conductionCoeff * d1;
    newFilteredPos(2) = mLastFilteredPos(2) + mParamLambda * conductionCoeff * d2;

    //PVMSG("(%f,%f,%f) -> ",(double)(mParamLambda * conductionCoeff(0) * d0),
    //                       (double)(mParamLambda * conductionCoeff(1) * d1),
    //                       (double)(mParamLambda * conductionCoeff(2) * d2));

    mFilteredPosMatrix.resize(3);
    mFilteredPosMatrix(0) = newFilteredPos(0);
    mFilteredPosMatrix(1) = newFilteredPos(1);
    mFilteredPosMatrix(2) = newFilteredPos(2);

    mLastFilteredPos = newFilteredPos;

    v = mFilteredPosMatrix;

//    PVMSG("(%f,%f,%f)\n",v(0),v(1),v(2));
  }
*/

  /**
    mLastDiff = diff;
   * Return the current state. Must call update() first.
   * @return last calculated value.
   */
  virtual const PvMatrixt &state(void) { return mFilteredPosMatrix; };

  /**
   * Reset.
   */
  virtual void reset() 
  {
  }


  //
  // ACCESS
  //

public:


  /**
   * @return w0 == 1 && w1 == 0 if it is in local tracking mode.
   * w0==0 && w1==1 otherwise.
   */
  virtual void getWeights(float* w0, float* w1) 
  { 
//    if (mIsLocalTrack)
//    {
//      *w0 = 1; *w1 = 0; 
//    }
//    else
//    {
//      *w0 = 0; *w1 = 1; 
//    }
  }

  //
  // INQUIRY
  //

public:


  //
  // ATTRIBUTES
  //

protected:

  /**
   * Current filtered position.
   */
  PvMatrixt mFilteredPosMatrix;

  /**
   * Last position after filtering.
   */
  //Vector3f mLastFilteredPos;
  PvMatrixt mLastFilteredPos;

  int mParamK;
  float mParamLambda;
  float mLastDiff;

};

/// Smart pointer to SmoothAnisotropic
typedef boost::shared_ptr<SmoothAnisotropic> SmoothAnisotropicPtr;

}; // namespace ait

#endif // SmoothAnisotropic_HPP


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef SvmClassifier_HPP
#define SvmClassifier_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>
#include <legacy/pv/ait/Matrix.hpp>
#include <legacy/math/SVM.hpp>

// LOCAL INCLUDES
//

#include "legacy/vision_tools/BinaryClassifier.hpp"

// FORWARD REFERENCES
//

#include "legacy/vision_tools/SvmClassifier_fwd.hpp"

namespace ait 
{

namespace vision
{

/**
 * Support Vector Machine based classifier. This classifier uses 
 * the SVM class to perform two-class classifications on a vector.
 * @par Responsibilities:
 * - Perform SVM classifications.
 */
class SvmClassifier : public BinaryClassifier
{
  //
  // LIFETIME
  //

public:

  class Sample : public Classifier::Sample
  {
  public:
    /// Vector of floating point values to be classified.
    Matrix<float> *pVector;
  };

  typedef boost::shared_ptr<Sample> SamplePtr;

  class Result : public Classifier::Result
  {
  public:
    /**
     * This is the distance from the svm boundary.
     */
    float distance;
  };

  typedef boost::shared_ptr<Result> ResultPtr;

  /**
   * Constructor.
   */
  SvmClassifier();

  /**
   * Destructor
   */
  virtual ~SvmClassifier();

  /**
   * Initialize the SVM classifier.
   * @param pLabels [in] Group of labels for this classifier.
   * @param paramFile [in] Name of the file to read the SVM parameters from.
   * @param positiveCLassLabel [in] Value of the label for the
   * class that lies in the positive side of the SVM boundary.
   * @param negativeCLassLabel [in] Value of the label for the
   * class that lies in the negative side of the SVM boundary.
   * @param kernelType [in] Type of kernel. See SVM class for doc.
   * @param uncertaintyGap [in] Uncertanty gap. See SVM class for doc.
   * @param vectorSize [in] Size of each vector in the SVM.
   */
  void init(ClassifierLabelsPtr pLabels,
            const std::string& paramFile,
            Label positiveClassLabel = ClassifierLabels::UNKNOWN,
            Label negativeClassLabel = ClassifierLabels::UNKNOWN,
            char kernelType = 0,
            double kernelParameter = 0,
            float uncertaintyGap = 0,
            int vectorSize = 0);

  //
  // OPERATIONS
  //

public:

  /// Create a results class. This is a factory pattern.
  virtual Classifier::ResultPtr createResult() { return Classifier::ResultPtr(new Result); }

  /**
   * Perform the classification.
   */
  virtual void classify(Sample& sample, Result& result);

  // Abstract member implementation.
  virtual void classify(Classifier::Sample& sample, Classifier::Result& result);

  
  /**
   * Return true if the distance is in the uncertainty range 
   * [MinNegative*UncertaintyGap,MaxPositive*UncertaintyGap].
   * @param distance [in] Distance from the SVM.
   */
  bool isUnknown(double distance)
  {
    return mpSvm->isUnknown(distance);
  }

  //
  // ACCESS
  //

public:

  //
  // INQUIRY
  //

public:

  //
  // ATTRIBUTES
  //

protected:

  /**
   * Pointer to the Support Vector Machine.
   */
  boost::shared_ptr< SVM<float> > mpSvm;

};

}; // namespace vision

}; // namespace ait

#endif // SvmClassifier_HPP


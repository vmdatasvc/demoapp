/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef BackgroundSubIRFlashing_HPP
#define BackgroundSubIRFlashing_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <opencv2/cv.h>
#include <opencv2/cv.hpp>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>
#include <legacy/pv/ait/Image_fwd.hpp>
#include <legacy/pv/ait/BasicMatrix.hpp>
#include <legacy/pv/ait/Image.hpp>
#include <legacy/pv/CircularBuffer.hpp>
//#include <Settings.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

#include "BackgroundSubIRFlashing_fwd.hpp"

namespace ait 
{

namespace vision
{


/**
 * This class is not yet documented.
 * Here is a more detailed description of the class. Next come the 
 * Responsibilities of the class. Try to minimize the responsibilities, this
 * will help in creating a good design. Next, optionally write any design patterns
 * that were used for this class. Refer to the "Design Patterns" book by Gamma et al.
 * @par Responsibilities:
 * - The First Responsibility
 * - The Second Responsibility:
 *   - A sub-responsibility
 *   - Another sub-responsibility
 * - The Third Responsibility
 * @par Patterns:
 * - The first design pattern
 * - The second design pattern
 * @par Design Considerations:
 * - Here you can itemize some justifications for your decisions. It will help
 * others to understand why your class is the way it is.
 * @remarks
 * Some special comments or warnings about this class.
 * @see TheirClass, OurClass (other related classes).
 */
class BackgroundSubIRFlashing : private boost::noncopyable
{
  //
  // LIFETIME
  //

  typedef CircularBuffer<Image32Ptr> FrameHistory;

public:

  /**
   * Constructor.
   */
  BackgroundSubIRFlashing();

  /**
   * Destructor
   */
  virtual ~BackgroundSubIRFlashing();

  /**
   * Initialization.
   */
  void init(int w, int h); // TODO: init with allParams

  void uninitialize();

  void process(Image32Ptr pImage1, Image32Ptr pImage2, FrameHistory& fh);

  void visualize(Image32& vf);

  //
  // OPERATIONS
  //

public:

  void displayThresholdHistogram(Image32& vf);

  void displayBlownHighlights(Image32& vf, Image32& mask);


protected:

  /**
  * Used to find the mean red value of an image to determine which image is lit
  * when infrared light subtraction is used
  * @param img [in] Image to find the mean red value;
  */
  int getImgRedMean(Image32Ptr& img);

  /**
  * Subtracts the unlit image from the lit image and masks the lit image based
  * on thresholding of the result.
  * @param pLitImage [in] Pointer to the lit image;
  * @param pUnlitImage [in] Pointer to the unlit image;
  * @param [out] Threshold value
  */
  unsigned int subtractAndThreshold(Image32Ptr& pLitImage, Image32Ptr& pUnlitImage, FrameHistory& fh);


private:

  /**
  * Mask litImage based on the mask. subResult is initially a subtraction result
  * of the unlit image from the lit image. A threshold is applied to subResult 
  * to create a mask which is applied to the lit image.
  * This is also where the mask is encoded in the Alpha channel.
  * @param litImage [in] Reference to the lit image;
  * @param subResult [in] Reference to the subtraction result
  * @param [out] Threshold value
  */
  unsigned int thresholdImg(Image32& litImage, Image32& subResult);

  uint getBlownHighlights(const Image32& img, Image32& mask, int checkChannel = -1);

  //
  // ACCESS
  //

public:


  //
  // INQUIRY
  //

public:

  /**
   * Ask if this object has been initialized (the init() function has been called).
   */
  bool isInitialized() const { return mIsInitialized; }


  //
  // ATTRIBUTES
  //

protected:

  /**
   * Tells if this object has been initialized.
   */
  bool mIsInitialized;

private:

  /**
  * Settings object to read/save parameters.
  */
  Settings mSettings;

  /**
  * Determines if the original frame or the masked frame should be
  * returned on a call to getCurrentFrame
  */
  bool mInfraredDoThreshold;

  /**
  * Determines if the bit mask should be placed in the alpha channel of each frame
  */
  bool mInfraredMaskInAlpha;

  /**
  * Preset Threshold value for removing pixels based on subtraction
  */
  int mInfraredThreshold;

  /**
  * Adjusted Threshold value for removing pixels based on subtraction
  */
  unsigned int mThreshold;

  /**
  * Sets whether the foreground mask gets dilated and eroded to fill gaps
  */
  bool mDoDialateAndErode;

  /**
  * To show or not to show is answered by this flag
  */  
  bool mDisplayBlownHighlights;
  bool mDisplayHistogram;
  bool mDisplayHistogramThresholds;

  /**
  /* Threshold at which to consider pixel values to be blown out (over saturated)
  */
  unsigned int mBlownHighlightThreshold;

  /**
  * To enable smoothing the subtracted image histogram
  */
  bool mSmoothHistogram;

  /**
  * To enable smoothing the subtracted difference image 
  */
  bool mSmoothDiffImage;

  /**
  * To enable reading values from the registry every frame
  */
  bool mEnableRealTimeParamAdjust;

  /**
  * Save various stages of the thresholding process (debug purpose only)
  */
  bool mSaveFrameHistory;

  /**
  /* Used for doing the flash subtraction
  */
  Image32 mDiffImage;
  Image8 mTmpGrayImage1;
  Image8 mTmpGrayImage2;
  Image32 mBlownHighlights;

  /**
  /* Array holding a 256 bin histogram
  */
  unsigned int mHistogram[256];
  unsigned int mHistogramStep;
  unsigned int mMeanShiftStartIdx;
  float mFindThresholdPercentage;

  /**
  /* Foreground segmentation mask
  */
  BasicMatrix<unsigned char> mMask;
  BasicMatrix<unsigned char> mTmpMask;			
  CvSize mMaskSize;

  int mMorphOpenKernelSize;
  int mMorphOpenIterations;
  int mMorphCloseKernelSize;
  int mMorphCloseIterations;
  int mMorphDilateKernelSize;
  int mMorphDilateIterations;

  int mSmoothHistogramKernelSize;

  IplImage* mIplMask;
  IplImage* mIplTmpMask;

  IplConvKernel* mShapeOpenKernel;
  IplConvKernel* mShapeCloseKernel;
  IplConvKernel* mShapeDilateKernel;			

  //test
  unsigned int threshold1;
  unsigned int threshold2;
  unsigned int peak1;
  unsigned int peak2;
};


}; // namespace vision

}; // namespace ait

#endif // BackgroundSubIRFlashing_HPP


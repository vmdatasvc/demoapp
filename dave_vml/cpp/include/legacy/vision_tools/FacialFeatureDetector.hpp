/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef FacialFeatureDetector_HPP
#define FacialFeatureDetector_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>
#include <legacy/pv/ait/Image.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

#include "legacy/vision_tools/FacialFeatureDetector_fwd.hpp"

namespace ait 
{


/**
 * This class is not yet documented.
 * Here is a more detailed description of the class. Next come the 
 * Responsibilities of the class. Try to minimize the responsibilities, this
 * will help in creating a good design. Next, optionally write any design patterns
 * that were used for this class. Refer to the "Design Patterns" book by Gamma et al.
 * @par Responsibilities:
 * - The First Responsibility
 * - The Second Responsibility:
 *   - A sub-responsibility
 *   - Another sub-responsibility
 * - The Third Responsibility
 * @par Patterns:
 * - The first design pattern
 * - The second design pattern
 * @par Design Considerations:
 * - Here you can itemize some justifications for your decisions. It will help
 * others to understand why your class is the way it is.
 * @remarks
 * Some special comments or warnings about this class.
 * @see TheirClass, OurClass (other related classes).
 */
class FacialFeatureDetector : private boost::noncopyable
{
  //
  // LIFETIME
  //

public:

  /**
   * Explain this structure.
   */
  typedef struct
  {
  public:
    /// Left eye with respect to the image (eye on the left of the image).
    Vector2f leftEye;
    /// Right eye with respect to the image (eye on the right of the image).
    Vector2f rightEye;
  } FacialFeatureLocation;

  /**
   * Constructor.
   */
  FacialFeatureDetector();

  /**
   * Destructor
   */
  virtual ~FacialFeatureDetector();

  //
  // OPERATIONS
  //

public:

  /**
   * Document here!
   */
  bool eyeLocate(const Image8 &faceImage, FacialFeatureLocation& locations);

  /**
   * Document here!
   */
  

  //
  // ACCESS
  //

public:


  //
  // INQUIRY
  //

public:


  //
  // ATTRIBUTES
  //

protected:

};


}; // namespace ait

#endif // FacialFeatureDetector_HPP


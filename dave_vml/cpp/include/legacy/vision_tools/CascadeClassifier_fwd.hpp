/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef CascadeClassifier_fwd_HPP
#define CascadeClassifier_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class CascadeClassifier;
typedef boost::shared_ptr<CascadeClassifier> CascadeClassifierPtr;

}; // namespace vision

}; // namespace ait

#endif // CascadeClassifier_fwd_HPP


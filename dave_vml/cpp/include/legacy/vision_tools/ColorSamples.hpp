/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef COLORSAMPLES_HPP
#define COLORSAMPLES_HPP

#include <legacy/pv/ait/Image.hpp>
#include <legacy/vision_tools/PvImageProc.hpp>
#include <legacy/pv/PvUtil.hpp>

namespace ait
{

class ColorSamples
{
private:

  // current number of color samples
  unsigned int nrOfSamples;

  // image containing color samples
  Image32 sampleStorage;

  // color space used for color statistics
  PvImageProc::colorSpaceConst colorSpace;

  void markFreeSampleSlots(void);
  void resize(const unsigned int minNewSize);

public:
  ColorSamples(void) : nrOfSamples(0), sampleStorage(64,64) { markFreeSampleSlots(); }
  ~ColorSamples(void) { }

  //Copy.
  ColorSamples& operator=(const ColorSamples& rightSide);

  void setColorSpace(PvImageProc::colorSpaceConst space);

  void addSamples(const Image32 &image);
  
  void addSample(const unsigned int sample);

  unsigned int size(void) const { return nrOfSamples; }
  
  void clear(void) { nrOfSamples=0; markFreeSampleSlots(); }

  unsigned int sample(const unsigned int s) const { return sampleStorage(s); }

  Image32 &sampleImage(void) { return sampleStorage; }
  
  void removeOutliers(float keepFraction, unsigned int logBinSize);

  void meanAndVariance(Vector3f &mean, Vector3f &var,PvImageProc::colorSpaceConst space=PvImageProc::INVALID_SPACE);

  void visualize(void);
};

} // namespace ait

#endif

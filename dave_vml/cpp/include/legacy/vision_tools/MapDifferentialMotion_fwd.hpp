/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef MapDifferentialMotion_fwd_HPP
#define MapDifferentialMotion_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class MapDifferentialMotion;
typedef boost::shared_ptr<MapDifferentialMotion> MapDifferentialMotionPtr;

}; // namespace vision

}; // namespace ait

#endif // MapDifferentialMotion_fwd_HPP


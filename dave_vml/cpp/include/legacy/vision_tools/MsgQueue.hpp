/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef MsgQueue_HPP
#define MsgQueue_HPP

// SYSTEM INCLUDES
//

#include <list>
#include <boost/utility.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/condition.hpp>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

//#include "legacy/VisionMsg_fwd.hpp"
//#include "MsgQueue_fwd.hpp"
#include "legacy/pv/PvUtil.hpp"

namespace ait 
{

namespace vision
{

/**
 * This is a thread-safe container for vision messages.
 * @par Responsibilities:
 * - Store messages in a queue.
 * - Control concurrency.
 */
class MsgQueue : private boost::noncopyable
{
public:

  //
  // LIFETIME
  //

  /**
   * Constructor
   */
  MsgQueue();

  /**
   * Destructor
   */
  virtual ~MsgQueue();

  //
  // OPERATIONS
  //

public:

  /**
   * Put an element in the queue.
   */
  void put(VisionMsgPtr pMsg);

  /**
   * Get an element from the queue.
   * This template that simplifies syntax and performs type conversion (using RTTI)
   * @return true if an element was obtained, false if the queue is empty.
   */
  template <class T> Bool get(boost::shared_ptr<T>& pMsg)
  {
    VisionMsgPtr pVMsg;

    if (!get0(pVMsg)) return false;

    pMsg = boost::shared_dynamic_cast<T>(pVMsg);

    if (pMsg.get() == NULL)
    {
      PvUtil::exitError("Invalid type for message");
    }

    return true;
  }


  /**
   * Clear the queue.
   */
  void clear();

  /**
   * Set the notify synchronization variables. These pointers will not be
   * destroyed by this class. This function is not thread safe, it should
   * be called right after construction.
   */
  void setNotifyVars(boost::mutex *pMutex, boost::condition *pCond);

protected:

  /**
   * Get an element from the queue.
   * @return true if an element was obtained, false if the queue is empty.
   */
  Bool get0(VisionMsgPtr& pMsg);

  //
  // ACCESS
  //

public:


  //
  // INQUIRY
  //

public:

  Bool isEmpty();

  //
  // ATTRIBUTES
  //

protected:

  typedef std::list<VisionMsgPtr> VisionMessages;
  typedef boost::mutex Mutex;
  typedef boost::mutex::scoped_lock Lock;
  

  /**
   * The message queue list.
   */
  VisionMessages mMsgs;

  /**
   * Synchronization variable.
   */
  mutable Mutex mMutex;

  /**
   * Notification synchronization mutex.
   */
  boost::mutex* mpNotifyMutex;

  /**
   * Notification condition variable.
   */
  boost::condition* mpNotifyCond;

  //
  // IO
  //

public:

  /**
   * String representation of this object.
   */
  virtual std::string output() const;

  virtual int size() const;

};


}; // namespace vision

}; // namespace ait

#endif // MsgQueue_HPP



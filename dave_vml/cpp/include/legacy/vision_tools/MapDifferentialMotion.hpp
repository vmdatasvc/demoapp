/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef MapDifferentialMotion_HPP
#define MapDifferentialMotion_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

//#include <Settings.hpp>
#include <legacy/types/String.hpp>
#include <legacy/pv/ait/Matrix.hpp>
#include <legacy/pv/ait/Vector3.hpp>
#include <legacy/vision_tools/SmoothTrajectory.hpp>


// LOCAL INCLUDES
//

#include <legacy/low_level/Settings.hpp>

// FORWARD REFERENCES
//

#include "legacy/vision_tools/MapDifferentialMotion_fwd.hpp"

namespace ait 
{

namespace vision
{


/**
 * Map the differential motion of a body part (head, hand etc.)
 * to a cursor displacement (normalized).
 * @par Responsibilities:
 * - Compute cursor displacement by applying smoothing or 
 *   sensitization scheme (or both) to the input displacement.
 * @remarks
 */
class MapDifferentialMotion : private boost::noncopyable
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  MapDifferentialMotion();

  /**
   * Destructor
   */
  virtual ~MapDifferentialMotion();

  /**
   * Initialization of smoothing filter and 
	 * sensitization scheme.
   */
  void init();


  //
  // OPERATIONS
  //

public:

	/**
	 * Compute the cursor displacement.
	 * @param disp [in] Displacement of body part
	 * @param cursorDisp [out] Cursor displacement
	 */
	void computeCursorDisp(const Vector3f& disp, Vector3f& cursorDisp);

private:

	/**
	 * Get input settings
	 */
	void getSettings();

  //
  // ACCESS
  //

public:


  //
  // INQUIRY
  //

public:

  /**
   * Ask if this object has been initialized (the init() function has been called).
   */
  bool isInitialized() const { return mIsInitialized; }


  //
  // ATTRIBUTES
  //

protected:

  /**
   * Tells if this object has been initialized.
   */
  bool mIsInitialized;

	/**
	 * Smoothing filter 
	 */
	typedef boost::shared_ptr< SmoothTrajectory<float> > SmoothTrajectoryPtr;
	SmoothTrajectoryPtr mpSmoothTrajectory;

	/**
	 *	Cursor position and control parameters
	 */
	Vector2f mUnFilteredMousePos, mFilteredMousePos, mPrevFilteredMousePos;
	Vector3f mHeadMotionSensitivity, mHeadMotionMinSensitivity, mJitterThreshold;
	float mMinHeadDist;

	/**
	 * Tells if smoothing, sensitivity etc should be turned on.
	 */
	bool mEnableSmoothing, mEnableRunningAvg, mEnableThresholding, mEnableWeightedSensitivity;

	/**
	 * Registry settings
	 */
	Settings mSettings;

	/**
	 * Tells if settings should be read only during init or read continuously 
	 * during processing.
	 */
	bool mReadSettingsOnlyOnInit;

};


}; // namespace vision

}; // namespace ait

#endif // MapDifferentialMotion_HPP


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef MotionDetectFrameDiff_HPP
#define MotionDetectFrameDiff_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>
#include <legacy/vision_tools/MotionDetect.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

#include "legacy/vision_tools/MotionDetectFrameDiff_fwd.hpp"

namespace ait 
{


/**
 * Detect motion using two image differences.
 */
class MotionDetectFrameDiff : public MotionDetect
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  MotionDetectFrameDiff();

  /**
   * Destructor
   */
  virtual ~MotionDetectFrameDiff();

  /**
   * Initialization.
   * @param thres1 [in] Minimum value of the pixels to be counted as motion.
   * @param thres2 [in] Fraction of the pixels in the image that pass the
   * value if thres1.
   * @param calculateImage [in] Calculate the binary image with the pixels
   * that were counted. Useful for visualization. It can be retreived
   * with getMotionEnergy().
   */
  void init(float thres1 = 10, float thres2 = .2f, bool calcImage = false);

  //
  // OPERATIONS
  //

public:
  /**
   * Update the motion information.
   * @param inputImage [in] New image to be added to the model.
   */
  virtual void update(const Image8& inputImage);

  /**
   * Update the motion information using fast, direct memory access.
   * @param inputImage [in] New image to be added to the model.
   */
  virtual void update_fast(const Image8& inputImage);

  /**
   * Reset the motion model.
   */
  virtual void reset();

  //
  // ACCESS
  //

public:

  /**
   * Get the motion energy image.
   */
  virtual const Image8& getMotionEnergy();

  //
  // INQUIRY
  //

public:

  /**
   * Return true if there is motion in the current image.
   */
  virtual bool isMoving();

  //
  // ATTRIBUTES
  //

protected:


  /**
   * Flag that indicates if motion is present.
   */
  bool mIsMotionPresent;

  /**
   * Previous frame.
   */
  Image8 mPrevImage;

  /**
   * Threshold 1. see init().
   */
  int mThreshold1;

  /**
   * Threshold 2. see init().
   */
  float mThreshold2;

  /**
   * Flag to calculate the motion energy image.
   */
  bool mCalcEnergyImage;

  /**
   * Motion energy image.
   */
  Image8 mMotionEnergy;
};


}; // namespace ait

#endif // MotionDetectFrameDiff_HPP


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef SMOOTH_TRAJECTORY_HPP
#define SMOOTH_TRAJECTORY_HPP

namespace ait
{

/**
 * This is an abstract base class to support smoothing filters.
 */
template <class T> class SmoothTrajectory
{
public:

  /**
   * Destructor.
   */
  virtual ~SmoothTrajectory() {};

  /**
   * Given an actual measurement z(k), update the measurement
   * to reflect the actual observation.
   * @param z [in] observation vector z(k)
   * @param z [out] corrected observation vector z(k) = current state.
   */
  virtual void update(Matrix<T> &z) = 0;

  /**
   * Obtain the current state
   */
  virtual const Matrix<T> &state(void) = 0;

  /**
   * Reset function.
   */
  virtual void reset() {};

  /**
   * Return weight values that are dependant on the method.
   * These values help to visualize the state of the filter.
   */
  virtual void getWeights(T* w0, T* w1) { *w0 = 0; *w1 = 0; }
};

} // namespace ait

#endif //SMOOTH_TRAJECTORY_HPP


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VMsgQueue_HPP
#define VMsgQueue_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>

// AIT INCLUDES
//

#include <String.hpp>

// LOCAL INCLUDES
//

#include "VMsg.hpp"
#include "MsgQueue.hpp"

// FORWARD REFERENCES
//


namespace ait 
{

/**
 * This is a queue of vision messages.
 */
class VMsgQueue : private vision::MsgQueue
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  VMsgQueue() {};

  /**
   * Destructor
   */
  virtual ~VMsgQueue() {};

  //
  // OPERATIONS
  //

public:


  /**
   * Put an element in the queue.
   */
  void put(VMsgPtr pMsg)
  {
    MsgQueue::put(pMsg);
  }

  /**
   * Get an element from the queue.
   * @return true if an element was obtained, false if the queue is empty.
   */
  Bool get(VMsgPtr& pVMsg)
  {
    vision::VisionMsgPtr pMsg;
    Bool gotIt = MsgQueue::get(pMsg);
    if (gotIt) 
    {
      pVMsg = boost::shared_dynamic_cast<VMsg>(pMsg);
    }
    return gotIt;
  }

  /**
   * Clear the queue.
   */
  void clear()
  {
    MsgQueue::clear();
  }

  //
  // ACCESS
  //

public:


  //
  // INQUIRY
  //

public:


  //
  // ATTRIBUTES
  //

protected:

  //
  // IO
  //

public:

  /**
   * String representation of this object.
   */
  virtual std::string output() const { return MsgQueue::output(); }

 
};

/// Smart pointer to VMsgQueue
typedef boost::shared_ptr<VMsgQueue> VMsgQueuePtr;

}; // namespace ait

#endif // VMsgQueue_HPP


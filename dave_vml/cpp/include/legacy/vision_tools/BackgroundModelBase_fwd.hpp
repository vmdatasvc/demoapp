/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef BackgroundModelBase_fwd_HPP
#define BackgroundModelBase_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class BackgroundModelBase;
typedef boost::shared_ptr<BackgroundModelBase> BackgroundModelBasePtr;

}; // namespace ait

#endif // BackgroundModelBase_fwd_HPP


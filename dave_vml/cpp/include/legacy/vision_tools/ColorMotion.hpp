/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef COLORMOTION_HPP
#define COLORMOTION_HPP

//#include <Settings.hpp>

#include "legacy/vision_tools/DistanceMeasure.hpp"

#include <legacy/pv/ait/Image.hpp>
#include <legacy/pv/ait/Vector3.hpp>
#include <legacy/vision_tools/PvImageProc.hpp>

#include <legacy/vision_tools/ColorRange.hpp>

#include <legacy/low_level/Settings.hpp>

namespace ait
{

typedef unsigned int cmType;

class ColorMotion : public Image<cmType>
{
protected:

  ait::Settings set;

  int motionOneStdDev;

  bool parametersValid;

  int xPos, yPos;

  int cMaskSize,mMaskSize;
  
  float pC,pM;

  DistanceMeasure dm;

  float historyWeight;
  Image<cmType> history;

public:
  ColorMotion(void) : set(),
                      parametersValid(false),
                      motionOneStdDev(set.getInt("motionOneStdDev [intensity difference]",4*40)),
                      historyWeight(0)
  {}

  /**
   * Sets parameters of segmentation procedure.
   *
   * @param colMaskSize Parameter that determines size of mask for smoothing color information. Final size is (2*colMaskSize+1)^2.
   * @param motMaskSize Parameter that determines size of mask for smoothing motion information. Final size is (2*motMaskSize+1)^2.
   * @param colorFrac Weight to give to color cue [0,1].
   * @param motionFrac Weight to give to motion cue [0,1].
   * @param historyWeight Optionally, the class can combine the results of the current segmentation with a history of previous frames, resulting in a smoother surface. Valid values are 0 and 0.5 for optimization.
   */
  void setParameters(unsigned int colMaskSize, unsigned int motMaskSize, float colorFrac, float motionFrac, float historyWeight=0);

  void setMotionOneStdDev(unsigned int value) { motionOneStdDev=value; }

  /**
   * Performs color motion segmentation using a RGB color model.
   *
   * @param colImage Current full sized frame.
   * @param motImage Current subsampled frame.
   * @param motImagePrev Previous subsampled frame.
   * @param x X-Postion of ROI in which to perfrom segmentation.
   * @param y Y-Postion of ROI in which to perfrom segmentation.
   * @param w Width of ROI in which to perfrom segmentation.
   * @param h Height of ROI in which to perfrom segmentation.
   * @param mean Mean RGB color of color model to use for segmentation.
   * @param var Variance of color (RGB) to use for segmentation.
   */
  void segment(Image32 &image,Image32 &motImage,Image32 &motImagePrev, unsigned int x, unsigned int y,
                          unsigned int w, unsigned int h, Vector3f &mean, Vector3f &var);

  /**
   * Visualize color motion segmentation into given frame.
   *
   * @param frame Frame to visualize into.
   * @param range Color range coding to use.
   * @see ColorRange
   */
  void visualize(Image32 &frame, ColorRange &range);

  float xOffset(void) { return (float)xPos; }
  float yOffset(void) { return (float)yPos; }

  /**
   * Given a position in the color motion probability field
   * that is assumed to contain a single blob like object at the
   * given position (defined by the max in the prop.), this function 
   * refines the position, by fitting a parametric surface to the
   * probability field in the window of size w x h and extracting the 
   * max of that function.
   * 
   * @param pos hypothesized position
   * @param w width of window over which to do the matching
   * @param h height of window over which to do the matching
   * @returns pos refined position
   */
  void refinePosition(Vector2f &pos, unsigned int w, unsigned int h);
};

} // namespace ait

#endif

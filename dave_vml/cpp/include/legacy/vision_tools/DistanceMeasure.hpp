/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef DISTANCEMEASURE_HPP
#define DISTANCEMEASURE_HPP

#include <legacy/pv/ait/Vector3.hpp>

namespace ait
{

class DistanceMeasure
{
protected:

  bool initializedFlag;

  Vector3f mean,var;

  float *lut0,*lut1,*lut2;
  unsigned int *iLut0,*iLut1,*iLut2;

  void calculateLut(float *lut,const float m,const float v, const float c);
  void calculateIntLut(void);

public:
  DistanceMeasure(void);
  ~DistanceMeasure(void);

  bool isInitialized(void) { return initializedFlag; }
  void init(Vector3f &mean, Vector3f &var);

  unsigned int measure(const unsigned char r,const unsigned char g,const unsigned char b) const 
  {
    return (unsigned int)(lut0[r]*lut1[g]*lut2[b] );
  }
  unsigned int measureInt(const unsigned char r,const unsigned char g,const unsigned char b) const 
  {
    return (unsigned int)((((iLut0[r]*iLut1[g])>>16)*iLut2[b])>>16);
  }
};

} // namespace ait

#endif

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef FrameInfo_HPP
#define FrameInfo_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>
#include <legacy/pv/ait/Image.hpp>
#include <legacy/pv/ait/Image_fwd.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

#include "legacy/vision_tools/FrameInfo_fwd.hpp"

namespace ait 
{

/**
 * Information about a source frame.
 * @par Responsibilities:
 * - Save a reference to the source frame.
 * - Save the time stamp.
 * - Save the frame index.
 */
class FrameInfo : private boost::noncopyable
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  FrameInfo()
  {
  }

  /**
   * Destructor
   */
  virtual ~FrameInfo()
  {
  }

  //
  // OPERATIONS
  //

public:


  //
  // ACCESS
  //

public:

  /**
   * Get the source frame image.
   */
  Image32Ptr getImage()
  {
    return mpSourceImage;
  }

  /**
   * Set the source frame image.
   */
  void setImage(Image32Ptr& pImg)
  {
    mpSourceImage = pImg;
  }

  /**
   * Get offset from the source frame used to crop the source image (in pixels).
   */
  const Vector2i& getSourceOffset() const
  {
    return mSourceOffset;
  }

  /**
   * Set offset from the source frame used to crop the source image (in pixels).
   */
  void setSourceOffset(const Vector2i& p)
  {
    mSourceOffset = p;
  }

  /**
   * Get the source frame time stamp.
   */
  Double getTimeStamp()
  {
    return mTimeStamp;
  }

  /**
   * Set the source frame time stamp.
   */
  void setTimeStamp(Double t)
  {
    mTimeStamp = t;
  }

  /**
   * Get the source frame index.
   */
  Uint getFrameIndex()
  {
    return mIndex;
  }

  /**
   * Set the source frame index.
   */
  void setFrameIndex(Uint i)
  {
    mIndex = i;
  }


  //
  // INQUIRY
  //

public:


  //
  // ATTRIBUTES
  //

protected:

  /**
   * Pointer to the source image.
   */
  Image32Ptr mpSourceImage;

  /**
   * Offset from the source frame used to crop the source image (in pixels).
   */
  Vector2i mSourceOffset;

  /**
   * Time stamp obtained with PvUtil::time()
   */
  Double mTimeStamp;

  /**
   * Frame index.
   */
  Uint mIndex;

 

};


}; // namespace ait

#endif // FrameInfo_HPP


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef SiftFeatureMatching_fwd_HPP
#define SiftFeatureMatching_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class SiftFeatureMatching;
typedef boost::shared_ptr<SiftFeatureMatching> SiftFeatureMatchingPtr;

}; // namespace vision

}; // namespace ait

#endif // SiftFeatureMatching_fwd_HPP


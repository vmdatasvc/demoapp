/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VideoWriterMod_HPP
#define VideoWriterMod_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

#include <String.hpp>
#include <ait/Image.hpp>
#include <PvCanvasImage.hpp>

#include <VideoWriter.hpp>

// LOCAL INCLUDES
//

#include "VideoWriterMsg.hpp"
#include "RealTimeMod.hpp"

// FORWARD REFERENCES
//

#include "VideoWriterMod_fwd.hpp"

namespace ait 
{

namespace vision
{


/**
 * Write input to disk.
 * @par Responsibilities:
 * - Write every frame to disk.
 */
class VideoWriterMod : public RealTimeMod
{
public:

  //
  // LIFETIME
  //

  /**
   * Constructor. The constructor creates a quick instance. Expensive operations
   * are executed in the init() function.
   */
  VideoWriterMod();

  /**
   * Destructor
   */
  virtual ~VideoWriterMod();

  /**
   * Initialization function.
   */
  virtual void init();

  /**
   * Finalization function
   */
  virtual void finish();

  /**
   * Provide dependency information for the ExecutionGraph.
   * @see VisionMod::initExecutionGraph for more info.
   */
  virtual void initExecutionGraph(std::list<VisionModPtr>& srcModules);

  //
  // OPERATIONS
  //

public:

  /**
   * Write the current frame to disk.
   */
  virtual void process();

  /**
   * Write the visualization frame to disk.
   */
  virtual void visualize(Image32& img);

  /**
   * Process the incoming vision messages.
   */
  virtual void processMessages();

protected:

  /**
   * Process incoming message.
   */
  virtual void processMsg(VideoWriterMsg& msg);

private:

  /**
   * Replace the date template.
   */
  std::string processFileName(const std::string& in, double currentTimeStamp);

  /**
   * Write the frame in the file.
   */
  void writeFrame(Image32& img);

  /**
   * Write a snapshot
   */
  void writeSnapshot(Image32& img);

  /**
   * Add Machine ID, Camera ID and Timestamp
   */
  void writeIdTimeStamp(PvCanvasImage& img, double timestamp);

  /**
   * Close the current video and create a new one.
   */
  void resetVideo();

  /**
   * Close the current video.
   */
  void closeVideo();

  /**
   * Check if the disk is full.
   */
  bool isDiskFull();

  //
  // ACCESS
  //

public:

  /**
   * Get the static name of this module. This name is used to name the type of this module.
   * The constructor sets the member variable mName to this value.
   * @remark This static function is only implemented for concrete classes.
   */
  static const char *getModuleStaticName() { return "VideoWriterMod"; }

  //
  // INQUIRY
  //

public:

  //
  // ATTRIBUTES
  //

protected:

  /**
   * Capture Rate for Images.
   * @remark This is used to setup the counter in the loop which decides which images to store.
   */
  Uint mCaptureRate;

  /**
   * Capture Rate Counter for Images.. Have to be defined as a class variable
   * because visualize function is called asyncronously.
   * @remark This is used to setup the counter in the loop which decides which images to store.
   */
  Uint mCaptureRateCounter;

  /**
   * Name format for the frame. 
   */
  std::string mFrameNameFormat;

  /**
   * Name of the last video file.
   */
  std::string mLastVideoFileName;

  /**
   * Name of the Images.  
   */
  std::string mImageName;
  
  /**
   * Current frame number.
   */
  Int mFrameCounter;

  /**
   * Maximum Frames to save.
   */
  Int mMaxFrames;

  /**
   * Number of levels to subsample.
   */
  Int mSubSampleLevel;

  /**
   * Video writer avi reference.
   */
  VideoWriterPtr mpVideoWriter;

  /**
   * Name of the video file to write out.
   */
  std::string mVideoNameFormat;

  /**
   * If this flag is true, it will write the visualization frame.
   * Visualization must be on in the system.
   */
  bool mWriteVisualization;

  /**
   * Record flag. True if this module is currently recording.
   */
  bool mIsRecording;

  /**
   * Write Id and Timestamp flag. Writes ID and timestamp if true. 
   */
  bool mAddIdTimeStampFlag;

  /**
   * Make output videos no longer than this size. If the video is longer,
   * it will brake it in to sections of this length.
   */
  double mPartitionSize;

  /**
   * This indicates the time when it should start recording. This is to
   * ensure more precision.
   */
  double mStartRecordingTime;

  /**
   * Compression settings file.
   */
  std::string mCompressionSettingsFname;

  /**
   * Time when the next partition must be created.
   */
  double mNextPartitionTime;

  /**
   * See settings/VideoWritingMod.xml for an explanation.
   * This is a work around a bug in the video writer.
   */
  int mExtraVideoWriterFrameOffset;

  /**
   * The time stamp of the last frame. This is used to easily detect if
   * the system clock has been modified.
   */
  double mLastFrameTime;

  /**
   * Minimum disk space to write (in MB)
   */
  double mMinDiskSpace;

  /**
   * Boolean flag indicating that the disk is full.
   */
  bool mIsDiskFull;

  /**
   * When to check the disk full.
   */
  double mNextDiskFullCheckTime;

  /**
   * This flag indicates that a snapshot was requested.
   */
  bool mDoSnapshot;

  /**
   * This is the list of frames that will be extracted
   * from the visualization into image files.
   */
  std::vector<int> mExtractFrameList;

  /**
   * This number indicates the index of the next frame
   * to extract in the mExtractFrameList.
   */
  int mNextFrameToExtractIndex;

};


}; // namespace vision

}; // namespace ait

#endif // VideoWriterMod_HPP



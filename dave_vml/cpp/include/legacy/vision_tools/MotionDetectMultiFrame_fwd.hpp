/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef MotionDetectMultiFrame_fwd_HPP
#define MotionDetectMultiFrame_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class MotionDetectMultiFrame;
typedef boost::shared_ptr<MotionDetectMultiFrame> MotionDetectMultiFramePtr;

}; // namespace ait

#endif // MotionDetectMultiFrame_fwd_HPP


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef SmoothFiniteImpulseResponse_HPP
#define SmoothFiniteImpulseResponse_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/shared_ptr.hpp>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>
#include <legacy/pv/ait/Vector3.hpp>
#include <legacy/pv/ait/Matrix.hpp>
//#include <CircularBuffer.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

// Ingmar

namespace ait 
{


/**
 * This class implements a method to low-pass filter positions 
 *
 * Time domain filtering by convolution with an impulse response.
 * Example smoothing with a rectangular window is done by filling H with
 * the value 1/nh at all places.
 *
 */
class SmoothFiniteImpulseResponse : public SmoothTrajectory<float>
{
  //
  // LIFETIME
  //

public:
 
  /**
   * Constructor.
   */
	SmoothFiniteImpulseResponse() : test(0)
  {
	  filterBankSet=false;
	  bufin = NULL;
	  bufout = NULL;

	  PVMSG("Test=%d\n", test++);
  }

  /**
   * Destructor
   */
  virtual ~SmoothFiniteImpulseResponse()
  {
	if(bufin!=NULL) delete bufin;
	if(bufout!=NULL) delete bufout;
  }

  //
  // OPERATIONS
  //

public:
  
  /**
   * Filter the current trajectory. Use state() to retreive the filtered position.
   * @param obsIn [in] new position.
   * @param obsIn [out] filtered position.
   */
  virtual void update(Matrix<float>& obsIn)
  {
	  Matrix<float> obsOut;

	  if(!filterBankSet) {obsOut=obsIn; return;}
	  
	  //expect a single input sample
	  assert(obsIn.rows()==1);
	  assert(obsIn.cols()>0);
	  
	  float fy, ffy;
	  unsigned int m;
	  int nd;
	  
	  if(buf.cols()!=obsIn.cols()) 
	  {
		  PVMSG("### buf.cols()=%d != obsIn.cosl()=%d\n", buf.cols(), obsIn.cols());
		  buf.resize(nh,obsIn.cols());
		  //    fy.resize(1,obsIn.cols());
		  oldy.resize(1,obsIn.cols());
	  }
	  
	  obsOut.resize(1,obsIn.cols());
	  
	  //if not yet enough observation to start continous filtering, wait till buffer full
	  if(!bufFull)
	  {
		  for(unsigned int c=0;c<obsIn.cols();c++)
		  {
			  buf(npi,c) = obsIn(c);
			  //PVMSG("->buf(%d,%d)=%f\n", npi,c,buf(npi,c));
		  }
		  
		  npi++;
		  
		  if(npi==nh)
		  {
			  npi=0;
			  bufFull=true;
		  }
		  else
			  obsOut=obsIn;
	  }
	  else
	  {
		  for(unsigned int c=0;c<obsIn.cols();c++)
		  {
			  //    PVMSG("Filter::fir filer seq col=%d\n", c);
			  nd  = 0;
			  
			  //PVMSG(" Filter::fir io=%d\n",io);
			  //while(ii < na)
			  //{
			  buf(npi,c) = obsIn(c);
			  //      PVMSG("buf(%d,%d)=%f\n", npi,c,buf(npi,c));
			  //bufin[npi] = arrin[ii++];
			  
			  fy = 0;
			  for(m = 0; m < nh; m++)
			  {
				  nd = ((m+npi+1) % nh);
				  fy = fy + buf(nd,c) * h(m);
				  //PVMSG("buf(%d,%d)=%f\n",nd,c,buf(nd,c));
			  }
			  ffy = fy;
			  //      PVMSG("ffy=%f\n",ffy);
			  fy = 0.5f * ((float)iadd * fy + (float)isub * oldy(c));
			  oldy(c) = ffy;
			  
			  obsOut(c) = fy;
			  //arrout[io++] = fy;
		  }
		  npi++;
		  npi = npi % nh;
		  //}    
	  }  
	  
	  mFilteredPosMatrix = obsOut;
	  obsIn = obsOut;	  
  }
  
  
  virtual void update(std::vector<Vector3f>& v, unsigned int a=3)
  {
	  Matrix<float> obsIn, obsOut;	  
	  unsigned int i,j;

	  if(!filterBankSet) {return;}
	  
	  obsIn.resize(v.size()*a);
	  for(i=0;i<v.size();i++)
		  for(j=0;j<a;j++)
			  obsIn(i*a+j)=v[i](j);
	  
	
	  //expect a single input sample
	  assert(obsIn.rows()==1);
	  assert(obsIn.size()>0);
	  
	  float fy, ffy;
	  unsigned int m;
	  int nd;
	  
	  if(buf.cols()!=obsIn.cols()) 
	  {
		  PVMSG("### buf.cols()=%d != obsIn.cosl()=%d\n", buf.cols(), obsIn.cols());
		  buf.resize(nh,obsIn.cols());
		  //    fy.resize(1,obsIn.cols());
		  oldy.resize(1,obsIn.cols());
	  }
	  
	  obsOut.resize(1,obsIn.cols());
	  
	  //if not yet enough observation to start continous filtering, wait till buffer full
	  if(!bufFull)
	  {
		  for(unsigned int c=0;c<obsIn.cols();c++)
		  {
			  buf(npi,c) = obsIn(c);
			  //PVMSG("->buf(%d,%d)=%f\n", npi,c,buf(npi,c));
		  }
		  
		  npi++;
		  
		  if(npi==nh)
		  {
			  npi=0;
			  bufFull=true;
		  }
		  else
			  obsOut=obsIn;
	  }
	  else
	  {
		  for(unsigned int c=0;c<obsIn.cols();c++)
		  {
			  //    PVMSG("Filter::fir filer seq col=%d\n", c);
			  nd  = 0;
			  
			  //PVMSG(" Filter::fir io=%d\n",io);
			  //while(ii < na)
			  //{
			  buf(npi,c) = obsIn(c);
			  //      PVMSG("buf(%d,%d)=%f\n", npi,c,buf(npi,c));
			  //bufin[npi] = arrin[ii++];
			  
			  fy = 0;
			  for(m = 0; m < nh; m++)
			  {
				  nd = ((m+npi+1) % nh);
				  fy = fy + buf(nd,c) * h(m);
				  //PVMSG("buf(%d,%d)=%f\n",nd,c,buf(nd,c));
			  }
			  ffy = fy;
			  //      PVMSG("ffy=%f\n",ffy);
			  fy = 0.5f * ((float)iadd * fy + (float)isub * oldy(c));
			  oldy(c) = ffy;
			  
			  obsOut(c) = fy;
			  //arrout[io++] = fy;
		  }
		  npi++;
		  npi = npi % nh;
		  //}    
	  }  
	  
	  //mFilteredPosMatrix = obsOut;
	  //obsIn = obsOut;	  

	  for(i=0;i<v.size();i++)
		  for(j=0;j<a;j++)
			  v[i](j)= obsOut(i*a+j);
	  
  }
  


  /**
  mLastDiff = diff;
  * Return the current state. Must call update() first.
  * @return last calculated value.
  */  
  virtual const Matrix<float> &state(void) { return mFilteredPosMatrix; };
 
  
  int filterSequence(std::vector<Vector3f> &seqIn, std::vector<Vector3f> &seqOut, unsigned int a=3)
  {
	if(!filterBankSet || h.cols()>=seqIn.size()) {seqOut.assign(seqIn.begin(), seqIn.end()); return -1;}
	  
	  assert(h.rows()==1 && h.cols()>0);	  
	  assert(seqIn.size()>0);
	  
	  unsigned int na=seqIn.size();
	  
	  int idel, nd, nl;
      unsigned int np2;
	  float fy, ffy, oldy;
	  int k1;
      unsigned int i,ii,k,m,io,npi;
	  
	  //seqOut.resize(na,seqIn.cols());
	  seqOut.resize(na);
	  
	  for(unsigned int c=0;c<a;c++)
	  {
		  //PVMSG("Filter::fir filer seq col=%d\n", c);
		  iodd = nh % 2;
		  np2 = nh/2;          
		  iadd = 1 + iodd;
		  isub = 1 - iodd;
		  nd  = 0;
		  ii = 0;
		  
		  k = 0;
		  for(i=0; i < nh; i++)
		  {
			  bufin[k++] = seqIn[ii++](c);
			  //bufin[k++] = seqIn(ii++,c);
			  //bufin[k++] = arrin[ii++];
		  }
		  
		  //PVMSG(" Filter::fir iodd=%d, np2=%d\n", iodd, np2);
		  oldy = 0.0;
		  for(k=1-iodd; k < (np2+1); k++)
		  {
			  idel = k - np2;
			  fy = 0.0;
			  for(m = 0; m < nh; m++){
				  k1 = m + idel;
				  if(k1 <= 0) k1 = -k1;
				  fy = fy + bufin[k1] * h(m);
			  }
			  ffy = fy;
			  if((k == 1) && (iodd == 0)) oldy = fy;
			  fy = 0.5f * ((float)iadd * fy + (float)isub * oldy);
			  oldy = ffy;
			  bufout[k] = fy;
		  }
		  
		  if(iodd != 1) bufout[0] = bufout[1];
		  
		  //PVMSG(" Filter::fir assign first %d elements to seqOut\n", k-1+iodd);
		  //for(io = 0; io < k; io++)
		  
		  for(io = 0; io < k-1+iodd; io++)
		  {
			  //PVMSG(" Filter::fir seqOut(%d,%d) = %f\n", io, c, bufout[io+1-iodd]);
			  seqOut[io](c) = bufout[io+1-iodd];
			  //seqOut(io,c) = bufout[io+1-iodd];
			  //arrout[io] = bufout[io];
		  }
		  
		  npi = 0;
		  
		  //PVMSG(" Filter::fir io=%d\n",io);
		  while(ii < na)
		  {
			  bufin[npi] = seqIn[ii++](c);
			  //bufin[npi] = seqIn(ii++,c);
			  //bufin[npi] = arrin[ii++];
			  
			  fy = 0.;
			  for(m = 0; m < nh; m++){
				  nd = ((m+npi+1) % nh);
				  fy = fy + bufin[nd] * h(m);
			  }
			  ffy = fy;
			  
			  fy = 0.5f * ((float)iadd * fy + (float)isub * oldy);
			  oldy = ffy;
			  
			  //PVMSG(" Filter::fir seqOut(%d,%d) = %f\n", io, c, fy);
			  seqOut[io++](c) = fy;
			  //seqOut(io++,c) = fy;
			  //arrout[io++] = fy;
			  npi++;
			  npi = npi % nh;
		  }
		  
		  nl = nd + nh;
		  nd = nd + 2;
		  nd = nd % nh;
		  
		  //PVMSG(" Filter::fir io=%d\n",io);
		  for(k = 0; k < np2; k++)
		  {
			  fy = 0.;
			  for(m = 0; m < nh; m++)
			  {
				  
				  k1 = nd + m;
				  if(k1 >= nl) k1 = 2 * nl - k1;
				  if(k1 >= (int)nh) k1 = k1 - nh;
				  fy = fy + bufin[k1] * h(m);
			  }
			  nd++;
			  ffy = fy;
			  
			  fy = 0.5f * ((float)iadd * fy + (float)isub * oldy);
			  oldy = ffy;
			  //PVMSG(" Filter::fir seqOut(%d,%d) = %f\n", io, c, fy);
			  seqOut[io++](c) = fy;
			  //seqOut(io++,c) = fy;
			  //arrout[io++] = fy;
		  }
	  }
	  
	  return 0;
   }


  int filterSequence(Matrix<float> &seqIn, Matrix<float> &seqOut)
  {
	  if(!filterBankSet || h.cols()>=seqIn.rows()) {seqOut=seqIn; return -1;}
	  
	  assert(h.rows()==1 && h.cols()>0);	  
	  assert(seqIn.cols()>0);
	  
	  unsigned int na=seqIn.rows();
	  
	  int idel,nd, nl;
      unsigned int np2;
	  float fy, ffy, oldy;
      int k1;
	  unsigned int i,ii,k,m,io,npi;
	  
	  seqOut.resize(na,seqIn.cols());
	  
	  for(unsigned int c=0;c<seqIn.cols();c++)
	  {
		  //PVMSG("Filter::fir filer seq col=%d\n", c);
		  iodd = nh % 2;
		  np2 = nh/2;
		  iadd = 1 + iodd;
		  isub = 1 - iodd;
		  nd  = 0;
		  ii = 0;
		  
		  k = 0;
		  for(i=0; i < nh; i++)
		  {
			  bufin[k++] = seqIn(ii++,c);
			  //bufin[k++] = arrin[ii++];
		  }
		  
		  //PVMSG(" Filter::fir iodd=%d, np2=%d\n", iodd, np2);
		  oldy = 0.0;
		  for(k=1-iodd; k < (np2+1); k++)
		  {
			  idel = k - np2;
			  fy = 0.0;
			  for(m = 0; m < nh; m++){
				  k1 = m + idel;
				  if(k1 <= 0) k1 = -k1;
				  fy = fy + bufin[k1] * h(m);
			  }
			  ffy = fy;
			  if((k == 1) && (iodd == 0)) oldy = fy;
			  fy = 0.5f * ((float)iadd * fy + (float)isub * oldy);
			  oldy = ffy;
			  bufout[k] = fy;
		  }
		  
		  if(iodd != 1) bufout[0] = bufout[1];
		  
		  //PVMSG(" Filter::fir assign first %d elements to seqOut\n", k);
		  //for(io = 0; io < k; io++)
		  for(io = 0; io < k-1+iodd; io++)
		  {
			  seqOut(io,c) = bufout[io+1-iodd];
			  //arrout[io] = bufout[io];
		  }
		  
		  npi = 0;
		  
		  //PVMSG(" Filter::fir io=%d\n",io);
		  while(ii < na)
		  {
			  bufin[npi] = seqIn(ii++,c);
			  //bufin[npi] = arrin[ii++];
			  
			  fy = 0.;
			  for(m = 0; m < nh; m++){
				  nd = ((m+npi+1) % nh);
				  fy = fy + bufin[nd] * h(m);
			  }
			  ffy = fy;
			  
			  fy = 0.5f * ((float)iadd * fy + (float)isub * oldy);
			  oldy = ffy;
			  
			  seqOut(io++,c) = fy;
			  //arrout[io++] = fy;
			  npi++;
			  npi = npi % nh;
		  }
		  
		  nl = nd + nh;
		  nd = nd + 2;
		  nd = nd % nh;
		  
		  //PVMSG(" Filter::fir io=%d\n",io);
		  for(k = 0; k < np2; k++)
		  {
			  fy = 0.;
			  for(m = 0; m < nh; m++)
			  {
				  
				  k1 = nd + m;
				  if(k1 >= nl) k1 = 2 * nl - k1;
				  if(k1 >= (int)nh) k1 = k1 - nh;
				  fy = fy + bufin[k1] * h(m);
			  }
			  nd++;
			  ffy = fy;
			  
			  fy = 0.5f * ((float)iadd * fy + (float)isub * oldy);
			  oldy = ffy;
			  //PVMSG(" Filter::fir seqOut(%d,%d) = %f\n", io, c, fy);
			  seqOut(io++,c) = fy;
			  //arrout[io++] = fy;
		  }
	  }
	  
	  return 0;
  }
  
   
  /**
  * Reset.
  */
  virtual void reset() 
  {
	  cont_num_obs=0; 
	  npi=1;
	  oldy.zero();
	  bufFull=false;
  }
  
  //implements a digital low-pass filter with a boxcar impulse response
  void setFilterBank(Matrix<float> &fb)
  {
	  h=fb; 
	  assert(h.rows()==1 && h.cols()>0);
	  
	  nh=h.cols();
	  iodd = nh % 2;
	  iadd = 1 + iodd;
	  isub = 1 - iodd;
	  
	  bufin = new float [h.cols()];
	  bufout = new float [h.cols()];
	  
	  buf.resize(h.cols(),2);
	  fy.resize(1,2);
	  oldy.resize(1,2);
	  
	  reset();
	  
	  filterBankSet=true; 
  }
  

  //
  // ACCESS
  //
  bool isFilterBankSet() {return filterBankSet;};


public:


  /**
   * @return w0 == 1 && w1 == 0 if it is in local tracking mode.
   * w0==0 && w1==1 otherwise.
   */
  virtual void getWeights(float* w0, float* w1) 
  { 
  }

  //
  // INQUIRY
  //

public:


  //
  // ATTRIBUTES
  //

protected:

  /**
   * Current filtered position.
   */
  Matrix<float> mFilteredPosMatrix;


private:
  bool filterBankSet;
  Matrix<float> h;

  int iodd;
  int iadd;
  int isub;
  unsigned int nh;
  unsigned int npi;

  int test;

  Matrix<float> fy;
  Matrix<float> oldy;
  Matrix<float> buf;
  bool bufFull;

  float *bufin, *bufout;

  int cont_num_obs;

};

/// Smart pointer to SmoothAnisotropic
typedef boost::shared_ptr<SmoothFiniteImpulseResponse> SmoothFiniteImpulseResponsePtr;

}; // namespace ait

#endif // SmoothFiniteImpulseResponse_HPP_HPP


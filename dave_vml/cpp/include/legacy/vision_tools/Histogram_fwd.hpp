/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef Histogram_fwd_HPP
#define Histogram_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class Histogram;
typedef boost::shared_ptr<Histogram> HistogramPtr;

}; // namespace ait

#endif // Histogram_fwd_HPP


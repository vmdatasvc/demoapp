/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ColorEdgeDetectionBase_HPP
#define ColorEdgeDetectionBase_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//
#include <legacy/types/String.hpp>
#include <legacy/pv/ait/Image.hpp>
#include <legacy/pv/ait/Rectangle.hpp>

// LOCAL INCLUDES
//
#include <legacy/low_level/Settings.hpp>
// FORWARD REFERENCES
//

#include "legacy/vision_tools/ColorEdgeDetectionBase_fwd.hpp"

namespace ait 
{


class ColorEdgeDetectionBase : private boost::noncopyable
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
	ColorEdgeDetectionBase(){};

  /**
   * Destructor
   */
	virtual ~ColorEdgeDetectionBase(){};


  //
  // OPERATIONS
  //

public:
	/**
	* Initialization.
	*/
	virtual 
		//void init(const std::string& settingsPath, const Image32 &inImage) = 0;
//        void init(const std::string& settingsPath, const int w, const int h) = 0;
	      void init(Settings &set, const int w, const int h) = 0;
	/**
	* process function	
	*/
	virtual
		void process(Image32 &inImage) = 0;

    virtual
        void process(Image32 &inImage, Rectangle<int>& rect) = 0;
	/**
	* clear operation	
	*/
	virtual
		void clear() = 0;

  //
  // ACCESS
  //

public:
  /**
	* Retrieve a reference to the gradient image	
	*/
	virtual
      Image32& GetGradientImage() = 0;	
    /**
	* Retrieve a reference to the orientation image	
	*/	
    virtual
      Image<float>& GetOrientationImage() = 0;

	/**
	* Retrieve ONLY magnitude of the gradient image	
	*/
	virtual
		void GetGradientImage(Image32 & outImage) = 0;
	/**
	* Retrieve the orientation image	
	*/
	virtual
		//void GetOrientationImage(Image32 &outImage) = 0;
        void GetOrientationImage(Image<float> &outImage) = 0;
	/**
	*  get the complete gradient image (x grad, y grad and magnitude)
	*/
	virtual
		void GetGradientImage(Image8& outImage) = 0;


  //
  // INQUIRY
  //

public:
	/**
	* Ask if this object has been initialized (the init() function has been called).
	*/
	virtual 
		bool IsInitialized() const  = 0;//{ return mIsInitialized; }

  //
  // ATTRIBUTES
  //

protected:

};


}; // namespace ait

#endif // ColorEdgeDetectionBase_HPP


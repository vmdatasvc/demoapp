/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VmsPacket_HPP
#define VmsPacket_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <map>

// AIT INCLUDES
//

#include <String.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

#include "VmsPacket_fwd.hpp"
#include "RealTimeMod_fwd.hpp"

namespace ait 
{


/**
 * Packets of data with mainly the Event informations sent from 
 * the vision modules to the VMS framework using sockets.
 * Most of the setup information will be in the settings.
 */
class VmsPacket : private boost::noncopyable
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  VmsPacket();

  /**
   * Constructor.
   */
  VmsPacket(ait::vision::RealTimeMod *pMod);

  /**
   * Destructor
   */
  virtual ~VmsPacket();

  //
  // OPERATIONS
  //

public:

  /**
   * Build a packet that initialize the connection parameters.
   */
  void initConnection(int listenPort);

  /**
   * Enable the VMS Gui to send pause messages to this application.
   * Typically this is called after the module has been initialized.
   */
  void allowPause();

  /**
   * Begin an event based on timestamps, typically obtained from 
   * ImageAcquireModule::getCurrentTime().
   */
  void beginEventTime(double startTimestamp, double duration, int eventChannelId = -1);

public:
  /**
   * Add one attribute and its value.
   */
  void addAttribute(const std::string& id, const std::string& value);

  /**
   * Add a LongString attribute type and its value. This will also escape all
   * XML characters.
   */
  void addLongStringAttribute(const std::string& id, const std::string& value);

  /**
   * Add a BinaryData type. This will also encode the data using Base64 encoding.
   * @param id [in] The id of the attribute (integer).
   * @param data [in] The binary data to send.
   * @param dataLength [in] The length of the data.
   */
  void addBinaryDataAttribute(const std::string& id, const Uint8 *data, int dataLength);

  /**
   * Use this call to indicate that this event channel will notify
   * completion intervals. If this call is not made, it is assumed
   * that events are generated continuously, and that no events will
   * be generated past the end of the last event. This information
   * is used mainly in the Composite Event channels.
   */
  void enableCompletionNotification(int eventChannelId = -1);

  /**
   * Use this function to indicate when the event channel has
   * completely processed all data up to the given timestamp.
   * This means that no events will be generated that have the
   * start or the end lesser than the provided timestamp.
   */
  void notifyProcessCompletion(double timestamp, int eventChannelId = -1);

  /**
   * Add a string into the XML. Use this to add extended attributes.
   */
  void addString(const std::string& str);

  /**
   * Finish the event.
   */
  void endEvent();

  /**
   * Send progress information.
   * @param p [in] New progress value in the range [0..1]
   * @remark 1 should be sent only when the application has
   * really finished.
   * @remark Do not update progress between startEvent() and endEvent().
   */
  void updateProgress(double p);

  /**
   * Call this when the process is terminated.
   */
  void finish();

  /**
   * Send the current message to the listener.
   */
  void sendXml(bool forceSend = false);

protected:
  /**
   * Begin sending a new event.
   */
  void beginEvent(Int64 startFrame, Int64 duration, int eventChannelId = -1);

private:
  /**
   * Get string ids given event channel id. (use settings also).
   */
  std::string getIdAttributes(int eventChannelId);

  /**
   * Convert from timestamp to frames.
   */
  Int64 timeToAbsoluteFrames(double timestamp);

  /**
   * Update the system clock. We want to make this call explicit
   * to avoid using one clock value for the beginning of the
   * event, and another value for the end of the event.
   */
  void updateSystemTime();

  //
  // ACCESS
  //

public:


  //
  // INQUIRY
  //

public:

  //
  // ATTRIBUTES
  //

protected:

  /**
   * The XML string that represents this event.
   */
  std::string mXmlMessage;

  /**
   * The timestamp of the last message sent.
   */
  double mLastMessageTimeTamp;

  /**
   * The current progress value.
   */
  double mProgress;

  /**
   * A reference to the module that sends the packets
   * in calculating the begin and end frame according to the VMS
   * packets.
   */
  ait::vision::RealTimeMod* mpRealTimeMod;

  /**
   * This flag indicates if the begin tag for events is open.
   */
  bool mIsEventTagOpen;

  /**
   * The extended attributes.
   */
  std::string mExtendedAttributes;

  /**
   * PvUtil::systemTime() - PvUtil::time() when this class was created.
   */
  double mSystemTimeToTimeOffset;

  typedef std::map<std::string,std::string> AttributeMap;

  /**
   * Attributes and id for the current event.
   */
  AttributeMap mEventAttributes;

  /**
   * Start time for current event.
   */
  double mEventStartTime;

  /**
   * Duration for current event.
   */
  double mEventDuration;

  /**
   * This flag will force using real time events even if the image
   * acquire device is not a live source.
   */
  bool mForceLiveSource;

  /**
   * Use timestamps instead of sending the frames since epoch.
   */
  bool mUseTimeStamps;

};


}; // namespace ait

#endif // VmsPacket_HPP


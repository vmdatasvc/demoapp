/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef RUNNING_AVG_TRAJECTORY_HPP
#define RUNNING_AVG_TRAJECTORY_HPP

#include <vector>
#include <legacy/math/matalgo.hpp>
#include <legacy/pv/ait/Matrix.hpp>

#include "legacy/vision_tools/SmoothTrajectory.hpp"
#include "legacy/math/SmoothStepFunc.hpp"
#include "legacy/math/RunningStatsMatrix.hpp"

namespace ait
{

/**
 * This class filters the given vector by weighting the running average of the last
 * historySize positions and the current position depending on the variance. For large
 * variances (the position is changing significantly) the algorithm will favor the current
 * position, and for small variances, the algorithm will favor the running mean position.
 * The two positions (mean, current) are blended in a function that assigns weights to each
 * of these values depending on the variance.
 */
template <class T> class RunningAvgTrajectory : public SmoothTrajectory<T>
{

public:

  typedef Matrix<T> PvMatrixt;

  virtual ~RunningAvgTrajectory() {};

  /**
   * Initialization of the algorithm.
   * @param historySize [in] number of position vectors to record in the history array.
   * @param meanVar [in] Co-Variance matrix that describes the maximum covariance to consider 
   * only the running mean as the filtered position.
   * @param currentVar [in] Co-Variance matrix that describes the minimum covariance to consider 
   * only the last position as the filtered position.
   * @remark A scalar value is used to compare the variances. This value is the determinant of
   * the square of each matrix. For diagonal matrices, this is equivalent to the square norm of the 
   * diagonal.
   */
  void init(int historySize0, const PvMatrixt& meanVar, const PvMatrixt& currentVar)
  {
    mStats.init(historySize0,true);

		setRange(meanVar,currentVar);

    reset();
  }

  /**
   * Filter the current trajectory. Use state() to retreive the filtered position.
   * @param v [in] new position.
   * @param v [out] filtered position.
   * @remark The resulting position is vNew = (1-T(var2))*mean + T(var2)*v, where T is the soft step function
   * that depends on the variance (square norm of).
   */
  virtual void update(PvMatrixt& v)
  {
    mStats.addElement(v);

    PvMatrixt mean = mStats.getMean();
    const PvMatrixt& var = mStats.getVar();

    // Get a scalar value of the variance, in this case the sum of the var vector.

    T varScalar = 0;
    unsigned int j;
    for (j = 0; j < v.size(); j++)
    {
      varScalar += var(j);
    }

		//PVMSG("%f %f %f\n",v(0),v(1),varScalar);

    // Calculate the filtered position as mSmoothVal = (1 - T(var2))*mean + T(var2)*v
    // Where T is the smooth step function that will be 1 for large variances and 0 for small variances.
    mCurrentWeight = mMeanCurrentFunc.eval(varScalar);
    mMeanWeight = 1 - mCurrentWeight;

	  mean.mult(mMeanWeight);
    mSmoothVal = v;
    mSmoothVal.mult(mCurrentWeight);
    mSmoothVal.add(mean);

    // Return the value in the input parameter
    v = mSmoothVal;
  }

  /**
   * Return the current state. Must call update() first.
   * @return last calculated value.
   */
  virtual const PvMatrixt &state(void) { return mSmoothVal; };

  /**
	 * Evaluate the smooth step function used by the filter.
	 * @param vec [in] Vector of (x,y) pairs where x is known
	 * and y has to be evaluated.
	 * @param vec [out] Vector of (x,y) pairs where y has been
	 * evaluated.
	 */
	virtual void eval(std::vector<Vector3<T> >& vec)
	{
		for (unsigned int j=0; j<vec.size(); j++)
		{
			vec[j].y = mMeanCurrentFunc.eval(vec[j].x);
		}
	}

  /**
   * Get the weights of each value.
   */
  virtual void getWeights(T* pMeanWeight, T* pCurrentWeight)
  {
    if (pMeanWeight) *pMeanWeight = mMeanWeight;
    if (pCurrentWeight) *pCurrentWeight = mCurrentWeight;
  }

	/**
	 * Get the running mean and variance. Must call update() first.	
	 */
	virtual void getRunningStats(PvMatrixt& mean, PvMatrixt& variance)
	{
		mean = mStats.getMean();
		variance = mStats.getVar();
  }

	/**
	 * Get the min and max range of the smooth step function.
	 */
	virtual void getRange(T& minX, T& maxX)
	{
		mMeanCurrentFunc.getRange(minX, maxX);
  }

  /**
	 * Sets the min and max thresholds
	 * @param meanVar [in] Co-Variance matrix that describes the maximum covariance to consider 
	 * only the running mean as the filtered position.
	 * @param currentVar [in] Co-Variance matrix that describes the minimum covariance to consider 
	 * only the last position as the filtered position.
	 */
	virtual void setRange(const PvMatrixt& meanVar, const PvMatrixt& currentVar)
	{
		PvMatrixt meanVar2 = meanVar;
		meanVar2.mult(meanVar);
		mMeanVar = meanVar2(0,0) + meanVar2(1,1) + meanVar2(2,2);
		//mMeanVar = meanVar2.det();

		PvMatrixt currentVar2 = currentVar;
		currentVar2.mult(currentVar);
		mCurrentVar = currentVar2(0,0) + currentVar2(1,1) + currentVar2(2,2);
		//mCurrentVar = currentVar2.det();

		mMeanCurrentFunc.setRange(mMeanVar,mCurrentVar);
	}

  /**
   * Deletes the accumulated positions.
   */
  void reset()
  {
    mStats.reset();
  }


private:

  /**
   * Running statistics that calculate the mean and variance.
   */
  RunningStatsMatrix<PvMatrixt> mStats;

  /// Step function used to compare variances.
  SmoothStepFunc<T> mMeanCurrentFunc;

  /**
   * The thresholds in one dimension.
   */
  T mMeanVar, mCurrentVar;
  
  /**
   * The resulting filtered position.
   */
  PvMatrixt mSmoothVal;

  /**
   * The current weights of each parameter.
   */
  T mMeanWeight, mCurrentWeight;
};

}; // namespace ait

#endif //RUNNING_AVG_TRAJECTORY_HPP

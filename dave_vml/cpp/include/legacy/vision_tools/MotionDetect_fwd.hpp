/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef MotionDetect_fwd_HPP
#define MotionDetect_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class MotionDetect;
typedef boost::shared_ptr<MotionDetect> MotionDetectPtr;

}; // namespace ait

#endif // MotionDetect_fwd_HPP


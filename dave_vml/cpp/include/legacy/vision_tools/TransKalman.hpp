/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef TRANSKALMAN_HPP
#define TRANSKALMAN_HPP

#include <legacy/math/PvKalman.hpp>
#include <legacy/pv/ait/Matrix.hpp>
#include <legacy/pv/ait/Vector3.hpp>
#include <legacy/math/Transformer.hpp>

#ifdef WIN32
#include <windows.h>
#endif

//#include "LinuxWindowsConversion.hpp"

#include <list>

namespace ait
{

class TransKalman;

class TransKalmanRepEntry
{
public:
  std::string fileName;
  int transMethod;
  PvKalman kalman;
};

typedef std::list<class TransKalmanRepEntry> TKRList;

class TransKalmanRepository
{
public:
  mutable CRITICAL_SECTION classLock;
  TKRList rep;

  TransKalmanRepository(void) { InitializeCriticalSection(&classLock); }
  ~TransKalmanRepository(void) { DeleteCriticalSection(&classLock); }

  bool findAndGet(std::string fileName,TransKalman &kalman);
  void store(TransKalman &kalman,std::string fileName);
};

/*********************************************************************
 ** 
 ** TransKalman Class
 **
 *********************************************************************
 ** 
 ** Description:
 **
 ** This class implements a Kalman filter with a coordinate transformer
 ** that processes the incoming coordinates before they are kalman 
 ** filtered.
 **
 *********************************************************************/
class TransKalman : public PvKalman
{
protected:
  static TransKalmanRepository rep;

protected:
  int transMethod;
  float varP0,varPx,varROff,varRDiag;

  Transformer<float> *pTrans;

  // observation vector
  Matrix<float> obs,z;

  void postInit(const Vector2f &coord);
    

public:
  // n = dimension of state space, m = dimension of observations
  TransKalman(void);
  ~TransKalman(void);

  // initialize the various matrices
  void init(const Vector2f &coord,const char *fileName);
  void init(const Vector2f &coord,
            const Matrix<float> A,
            const Matrix<float> B,
            float varP0,float varPx,float varRDiag,float varROff,int transMethod);

  // predict next observation
  void predictNext(Vector2f &coord);

  void predict(Vector2f &coord);
  void update(Vector2f &coord);

  friend bool TransKalmanRepository::findAndGet(std::string fileName,TransKalman &kalman);
  friend void TransKalmanRepository::store(TransKalman &kalman,std::string fileName);
};

} // namespace ait

#endif

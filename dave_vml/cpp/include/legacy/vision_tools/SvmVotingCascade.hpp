/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef SvmVotingCascade_HPP
#define SvmVotingCascade_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <boost/thread/thread.hpp>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>

// LOCAL INCLUDES
//

#include "legacy/vision_tools/CascadeClassifier.hpp"
#include "legacy/vision_tools/SvmVoting.hpp"

// FORWARD REFERENCES
//

#include "legacy/vision_tools/SvmVotingCascade_fwd.hpp"

namespace ait 
{

namespace vision
{


/**
 * Voting for a cascade of Svm classifiers.
 * @remark The members of this class are thread safe to allow for merge
 * operations from the main thread.
 * @see SvmVoting
 */
class SvmVotingCascade : private boost::noncopyable
{
  //
  // LIFETIME
  //

public:

  /**
   * Map of node id's to SvmClassifier cascade.
   */
 typedef std::map<CascadeClassifier::NodeId,vision::SvmVotingPtr> Cascade;

  /**
   * Constructor.
   */
  SvmVotingCascade();

  /**
   * Destructor
   */
  virtual ~SvmVotingCascade();

  
  void init(CascadeClassifierPtr pCascade, int maxVotingSamples);

  //
  // OPERATIONS
  //

public:


  /**
   * Add one sample, a result from a cascade of SVM classifiers.
   * @remark It assumes that all the classifiers are Svms.
   */
  void addSample(const CascadeClassifier::Result& r);

  /**
   * Return the current robust result.
   */
  Label getResult();

  /**
   * Merge the samples from the incoming votes.
   */
  void merge(const SvmVotingCascade& from);


private:

  /**
   * Calculate the results from the voting samples.
   */
  void calcResult();

  //
  // ACCESS
  //

public:

  //
  // INQUIRY
  //

public:


  //
  // ATTRIBUTES
  //

protected:

  typedef boost::mutex Mutex;
  typedef boost::mutex::scoped_lock Lock;

  /**
   * Map of SvmClassifiers.
   */
  Cascade mCascade;

  /**
   * Resulting label after voting.
   */
  Label mResultLabel;

  /**
   * Cascade classifier.
   */
  CascadeClassifierPtr mpCascadeClassifier;

  /**
   * Synchronization variable.
   */
  mutable Mutex mMutex;
};


}; // namespace vision

}; // namespace ait

#endif // SvmVotingCascade_HPP


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef HistogramBackgroundModel_fwd_HPP
#define HistogramBackgroundModel_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class HistogramBackgroundModel;
typedef boost::shared_ptr<HistogramBackgroundModel> HistogramBackgroundModelPtr;

}; // namespace ait

#endif // HistogramBackgroundModel_fwd_HPP


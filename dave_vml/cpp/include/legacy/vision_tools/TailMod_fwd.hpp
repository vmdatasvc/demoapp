/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef TailMod_fwd_HPP
#define TailMod_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class TailMod;
typedef boost::shared_ptr<TailMod> TailModPtr;

}; // namespace vision

}; // namespace ait

#endif // TailMod_fwd_HPP


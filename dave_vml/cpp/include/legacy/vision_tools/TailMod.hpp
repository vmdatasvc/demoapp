/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef TailMod_HPP
#define TailMod_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>
#include <legacy/pv/ait/Image.hpp>

// LOCAL INCLUDES
//

//#include "VisionMod.hpp"

// FORWARD REFERENCES
//

//#include "TailMod_fwd.hpp"

namespace ait 
{

namespace vision
{


/**
 * This module does nothing! The execution graph uses it as the last
 * module to execute for all vision modules. This module has execution
 * priority lower than any other module (max int), to ensure that it is executed
 * last.
 */
class TailMod : public VisionMod
{
public:

  //
  // LIFETIME
  //

  /**
   * Constructor. The constructor creates a quick instance. Expensive operations
   * are executed in the init() function.
   */
  TailMod();

  /**
   * Destructor
   */
  virtual ~TailMod();

  /**
   * Set the priority to the maximum (invalid for any other module).
   */
  virtual void initExecutionGraph(std::list<VisionModPtr>& srcModules);

  //
  // OPERATIONS
  //

public:

  //
  // ACCESS
  //

public:

  /**
   * Get the static name of this module. This name is used to name the type of this module.
   * The constructor sets the member variable mName to this value.
   * @remark This static function is only implemented for concrete classes.
   */
  static const char *getModuleStaticName() { return "TailMod"; }

  /**
   * Set the frame rate of the control module. This function is used by 
   * ExecutionGraph::build() to set the frame rate to the maximum frame
   * rate of all modules.
   */
  void setFramesPerSecond(double fps)
  {
    mFps = fps;
  }

  /**
   * Return the number of frames per second for this module.
   */
  virtual double getFramesPerSecond() const
  {
    return mFps;
  }

  //
  // INQUIRY
  //

public:

  /**
   * This module is not asynchrounous.
   */
  virtual bool isAsync() const { return false; }

  //
  // ATTRIBUTES
  //

protected:

  /**
   * The frame rate for this module.
   */
  double mFps;

};


}; // namespace vision

}; // namespace ait

#endif // TailMod_HPP



/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef BackgroundModelBase_HPP
#define BackgroundModelBase_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

#include <legacy/pv/ait/Image_fwd.hpp>
#include <legacy/pv/ait/Vector3.hpp>

// LOCAL INCLUDES
//

#include <legacy/low_level/Settings.hpp>

// FORWARD REFERENCES
//

#include "legacy/vision_tools/BackgroundModelBase_fwd.hpp"

namespace ait 
{


/**
 * This class is not yet documented.
 * Here is a more detailed description of the class. Next come the 
 * Responsibilities of the class. Try to minimize the responsibilities, this
 * will help in creating a good design. Next, optionally write any design patterns
 * that were used for this class. Refer to the "Design Patterns" book by Gamma et al.
 * @par Responsibilities:
 * - The First Responsibility
 * - The Second Responsibility:
 *   - A sub-responsibility
 *   - Another sub-responsibility
 * - The Third Responsibility
 * @par Patterns:
 * - The first design pattern
 * - The second design pattern
 * @par Design Considerations:
 * - Here you can itemize some justifications for your decisions. It will help
 * others to understand why your class is the way it is.
 * @remarks
 * Some special comments or warnings about this class.
 * @see HistogramBackgroundModel for an example.
 */
class BackgroundModelBase : private boost::noncopyable
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  BackgroundModelBase();

  /**
   * Destructor
   */
  virtual ~BackgroundModelBase();

  /**
   * Initialization. Inheritors must call this from their init function.
   */
  virtual void init(const std::string& settingsPath);  // init with all params later

  //
  // OPERATIONS
  //

public:

  /**
   * Pixel-wise Background modeling. This operation doesn't
   * perform foreground segmentation.
   */
  virtual void  update(const Image8Ptr pInputImage);

  /**
   * Pixel-wise Background modeling. This operation doesn't
   * perform foreground segmentation.
   */
  virtual void  update(const Image32Ptr pInputImage);

	/**
   * Segmentation of the foreground pixels from the background model
   */
	virtual void foregroundSegment(const Image8Ptr pInputImage, 
    Image8Ptr pForegroundImage);

	/**
   * Segmentation of the foreground pixels from the background model
   */
	virtual void foregroundSegment(const Image32Ptr pInputImage, 
    Image8Ptr pForegroundImage);

  /**
   * Make this model valid. If the model is not valid, foreground
   * segmentation can not be done. For example, the model can be built
   * over several frames and then become valid after some period has
   * elapsed.
   */
  virtual void validate() = 0;

  /**
   * Save the background model to the specified location
   */
  virtual void saveModel(std::string filename) {}

  /**
   * Loads the background model from the specified location
   */
  virtual Vector2i loadModel(std::string filename) { return Vector2i(0,0); }

  //
  // ACCESS
  //

public:


  /**
   * Get the number of frames that this model has
   * used to learn this background.
   */
  virtual int getNumFrames() = 0;

  //
  // INQUIRY
  //

public:

  /**
   * Indicate if this model has been initialized, that is, if the
   * initialize() function has been called.
   */
  virtual bool isInitialized() = 0;

  //
  // ATTRIBUTES
  //

protected:

};


}; // namespace ait

#endif // BackgroundModelBase_HPP


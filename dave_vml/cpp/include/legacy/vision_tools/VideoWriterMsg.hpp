/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VideoWriterMsg_HPP
#define VideoWriterMsg_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>

// AIT INCLUDES
//

#include <String.hpp>

// LOCAL INCLUDES
//

#include "VisionMsg.hpp"

// FORWARD REFERENCES
//

namespace ait 
{

namespace vision
{


/**
 * VideoWriterMod vision messages.
 * @par Responsibilities:
 * - Type of information to be carried in these messages.
 */
class VideoWriterMsg : public VisionMsg
{
public:

  //
  // LIFETIME
  //

  /**
   * Constructor.
   */
  VideoWriterMsg(VisualSensorId vid = VisualSensorId(), double timeStamp = 0) 
    : VisionMsg(vid,timeStamp)
  {
  }

  /**
   * Destructor
   */
  virtual ~VideoWriterMsg()
  {
  }

  //
  // OPERATIONS
  //

public:

  //
  // ACCESS
  //

public:

  //
  // INQUIRY
  //

public:

  //
  // IO
  //

public:

  //
  // ATTRIBUTES
  //

public:

  enum
  {
    /**
     * Send this to start recording. If it is already recording, it will ignore it.
     */
    RECORD             = 0x00000010,
    /**
     * Send this to stop recording. If record is sent when paused, it will resume recording on 
     * the same file. If it is already stopped, it will ignore it.
     */
    PAUSE              = 0x00000020,
    /**
     * Send this to stop recording and close the file. New record messages will create a new file.
     */
    STOP               = 0x00000040,
    /**
     * Take a snapshot and save it in the location specified in the settings.
     */
    SNAPSHOT           = 0x00000080
  };

protected:

};

/// Smart pointer to VideoWriterMsg
typedef boost::shared_ptr<VideoWriterMsg> VideoWriterMsgPtr;

}; // namespace vision

}; // namespace ait

#endif // VideoWriterMsg_HPP


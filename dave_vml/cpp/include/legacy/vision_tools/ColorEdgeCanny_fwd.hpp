/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ColorEdgeCanny_fwd_HPP
#define ColorEdgeCanny_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class ColorEdgeCanny;
typedef boost::shared_ptr<ColorEdgeCanny> ColorEdgeCannyPtr;

}; // namespace ait

#endif // ColorEdgeCanny_fwd_HPP


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef BinaryClassifier_fwd_HPP
#define BinaryClassifier_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class BinaryClassifier;
typedef boost::shared_ptr<BinaryClassifier> BinaryClassifierPtr;

}; // namespace vision

}; // namespace ait

#endif // BinaryClassifier_fwd_HPP


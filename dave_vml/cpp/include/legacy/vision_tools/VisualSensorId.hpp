/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VisualSensorId_HPP
#define VisualSensorId_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>

// AIT INCLUDES
//

#include <String.hpp>
#include <PvUtil.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//


namespace ait 
{


/**
 * This class is an utility to work with visual sensor IDs. Visual sensor Ids are
 * 32 bits values that are divided into 3 components. The most significant 16 bits
 * are the location Id (store), the next 6 bit define the sensor type, that is specific for
 * the particular store, and the last 10 bits represent the id of the position of the sensor
 * in the given store, starting from 0. This means that the maximum values are:
 * - max(locationId) = 65535
 * - max(sensorTypeId) = 63
 * - max(positionId) = 1023
 * @par Responsibilities:
 * - Store a visual sensor ID.
 * - Transform from and to a string.
 * - Parse the components.
 */
class VisualSensorId
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  VisualSensorId() : mId(0)
  {
  }

  /**
   * Copy Constructor
   */
  VisualSensorId(const VisualSensorId& from) : mId(from.mId) {}

  /**
   * Constructor from components.
   */
  VisualSensorId(const Uint32 locationId, const Uint32 sensorTypeId, const Uint32 positionId)
  {
    setId(locationId,sensorTypeId,positionId);
  }

  /**
   * Destructor
   */
  ~VisualSensorId()
  {
  }

  //
  // OPERATORS
  //

public:

  /**
   * Assignment operator.
   * @param from [in] The value to assign to this object.
   * @return A reference to this object.
   */
  VisualSensorId& operator=(const VisualSensorId& from)
  {
    if (this != &from)
    {
      mId = from.mId;
    }
    return *this;
  }

  /**
   * Comparison
   */
  bool operator==(const VisualSensorId& from) const
  {
    return mId == from.mId;
  }

  /**
   * Difference
   */
  bool operator!=(const VisualSensorId& from) const
  {
    return !(*this == from);
  }

  //
  // OPERATIONS
  //

public:


  //
  // ACCESS
  //

public:

  /**
   * Get the 32 bit value.
   */
  Uint32 getId() const
  {
    return mId;
  }

  /**
   * Set the 32 bit value.
   */
  void setId(const Uint32 id)
  {
    mId = id;
  }

  /**
   * Get the id in the form of a string.
   */
  std::string getStringId() const
  {
    char buf[20];

    sprintf(buf,"%u.%u.%u",getLocationId(),getSensorTypeId(),getPositionId());

    return std::string(buf);
  }

  /**
   * Get the id in the form of a string.
   */
  void setId(const std::string& s)
  {
    Uint32 locationId,sensorTypeId,positionId;
    sscanf(s.c_str(),"%u.%u.%u",&locationId,&sensorTypeId,&positionId);
    setId(locationId,sensorTypeId,positionId);
  }

  /**
   * Set the Id by components.
   */
  void setId(const Uint32 locationId, const Uint32 sensorTypeId, const Uint32 positionId)
  {
    PVASSERT((locationId < (2<<16)) && (sensorTypeId < (2<<6)) && (positionId < (2<<10)));

    mId = (locationId << 16) | (sensorTypeId << 10) | positionId;
  }

  /**
   * Get the store id.
   */
  Uint32 getLocationId() const
  {
    return mId >> 16;
  }

  /**
   * Get the sensor type id.
   */
  Uint32 getSensorTypeId() const
  {
    return (mId >> 10) & 0x3F;
  }

  /**
   * Get the sensor id.
   */
  Uint32 getPositionId() const
  {
    return mId & 0x3FF;
  }

  //
  // INQUIRY
  //

public:


  //
  // ATTRIBUTES
  //

protected:

  /**
   * The 32 bit visual sensor id value.
   */
  Uint32 mId;

};

/// Smart pointer to VisualSensorId
typedef boost::shared_ptr<VisualSensorId> VisualSensorIdPtr;

}; // namespace ait

#endif // VisualSensorId_HPP


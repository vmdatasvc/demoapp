/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef CascadeClassifier_HPP
#define CascadeClassifier_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <map>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>
#include <legacy/low_level/Settings.hpp>

// LOCAL INCLUDES
//

#include "legacy/vision_tools/Classifier.hpp"
#include "legacy/vision_tools/BinaryClassifier.hpp"

// FORWARD REFERENCES
//

#include "legacy/vision_tools/CascadeClassifier_fwd.hpp"

namespace ait 
{

namespace vision
{

/**
 * Cascade of multiple class classifiers. A cascade is a
 * tree of classifiers that are executed according to the result
 * of the parent classifier.
 * @par Responsibilities:
 * - Index the classifiers according to the key.
 * @par Example: This example will create a cascade classifier with
 * two classifiers.
 * @code 
 *   BinaryClassifierPtr pC1(new Classifier1());
 *   BinaryClassifierPtr pC2(new Classifier2());
 *   CascadeClassifier cc;
 *
 *   ClassifierLabelsPtr pLabels(new ClassifierLabels());
 *   pLabels->add("Class1");
 *   pLabels->add("NotClass1");
 *   pLabels->add("Class2");
 *   pLabels->add("Class3");
 *
 *   pC1->init(pLabels,pLabels->getLabel()
 *
 *   cc.addBinaryClassifier("1",pC1,"","1.1");
 *   cc.addBinaryClassifier("1.1",pC2,"","");
 *
 *   CascadeClassifier::Sample s;
 *   s.sampleCascade["1"] = s1;
 *   CascadeClassifier::Result r;
 *
 *   cc.classify(s,r);
 * @endcode
 */
class CascadeClassifier : public Classifier
{
public:

  //
  // LIFETIME
  //

public:

  /**
   * Id of the classifier. It is a string in the form "1.c2.c3.c4", where c# is the
   * identifier of a classifier label result. The classifier "1" is the root classifier.
   */
  typedef std::string NodeId;

  /**
   * Node from label map.
   */
  typedef std::map<Label,NodeId> LabelNodeMap;

  /**
   * Information stored per node.
   */
  class Node
  {
  public:
    /**
     * This map is used to know the id of the next node from the result
     * of a classifier.
     */
    LabelNodeMap nextNodeFromLabel;

    /**
     * Pointer to the actual classifier.
     */
    ClassifierPtr pClassifier;

  };

  /**
   * Pointer to a Node.
   */
  typedef boost::shared_ptr<Node> NodePtr;
  

  /**
   * Map of nodes from the node id.
   */
  typedef std::map<NodeId, NodePtr> CascadeMap;

  typedef std::map<NodeId,Classifier::SamplePtr> SampleMap;

  class Sample : public Classifier::Sample
  {
  public:
    SampleMap sampleCascade;
  };

  typedef std::map<NodeId,Classifier::ResultPtr> ResultMap;

  class Result : public Classifier::Result
  {
  public:
    ResultMap resultCascade;
  };

  /**
   * Constructor.
   */
  CascadeClassifier();

  /**
   * Destructor
   */
  virtual ~CascadeClassifier();

  /**
   * Initialization.
   */
  void init();

  /**
   * Initialize from registry settings. Works only for svm cascades. 
   * The labels provided will be populated from the registry and/or 
   * the model files.
   */
  virtual void initSvm(ClassifierLabelsPtr pLabels, Settings& set);

private:

  /**
   * Initialize one node of the cascade from the registry.
   */
  void initSvmNode(Settings& set, const NodeId& id);


  //
  // OPERATIONS
  //

public:

  /**
   * Create a results class. This is a factory pattern.
   */
  virtual Classifier::ResultPtr createResult() { return Classifier::ResultPtr(new Result); }

  /**
   * Perform the cascade classification.
   */
  virtual void classify(Sample& sample, Result& result);

  /**
   * Implementation of the virtual member. This will cast into the specific cascade
   * Sample and Result classes.
   * @param sample [in] cascade of samples.
   * @param result [in] cascade or results in the members, and the result of the
   * deepest node is in the base members of this result instance.
   * @remark If any classifier in the cascade returns unknown, the cascade will stop, and the
   * classifier will return the Unknown value. If the sample is not specified for every
   * node, the sample of the parent will be used. At least a sample must be specified for
   * the root.
   */
  virtual void classify(Classifier::Sample& sample, Classifier::Result& result);

  /**
   * Add a binary classifier to the cascade.
   * @param id [in] String name of the cascade. "0" is the root classifier.
   * @param pClassifier [in] Pointer to the classifier.
   */
  void addBinaryClassifier(const NodeId& id, BinaryClassifierPtr pClassifier, 
                           const NodeId& positiveNode, const NodeId& negativeNode)
  {
    NodePtr pNode(new Node());
    pNode->pClassifier = pClassifier;
    if (!positiveNode.empty())
    {
      pNode->nextNodeFromLabel[pClassifier->getPositiveLabel()] = positiveNode;
    }
    if (!negativeNode.empty())
    {
      pNode->nextNodeFromLabel[pClassifier->getNegativeLabel()] = negativeNode;
    }
    mCascade[id] = pNode;
  }

  /**
   * Get a particular classifier from the NodeId.
   */
  ClassifierPtr getClassifier(const NodeId& id)
  {
    if (mCascade.find(id) == mCascade.end())
    {
      PvUtil::exitError("Invalid NodeId %s",id.c_str());
    }
    return mCascade[id]->pClassifier;
  }

  /**
   * Return the id of the root. The root name must be unified.
   */
  NodeId getRootId()
  {
    return "1";
  }

  //
  // ACCESS
  //

public:

  /**
   * Access the internal classifiers.
   */
  CascadeMap& getCascadeMap()
  {
    return mCascade;
  }


  //
  // INQUIRY
  //

public:

  //
  // ATTRIBUTES
  //

protected:

  /**
   * The map of cascade classifier pointers.
   */
  CascadeMap mCascade;

};

}; // namespace vision

}; // namespace ait

#endif // CascadeClassifier_HPP


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef OpenCvUtils_HPP
#define OpenCvUtils_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <legacy/pv/ait/Image.hpp>

//#include "opencv2/opencv.hpp"
#include <cv.h>

// AIT INCLUDES
//

#include "legacy/types/String.hpp"

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

namespace ait 
{

namespace vision
{


/**
 * This class contain functions that facilitate the interaction between
 * VideoMining's classes and Intel's OpenCV.
 * @remark OpenCV should be used only to experiment, we should write
 * or own classes that implement the functionality possibly using the
 * Intel IPP library.
 */
class OpenCvUtils : private boost::noncopyable
{

public:

  /**
   * Create a new RGB Ipl image containing the pixels in the source image.
   * Use cvReleaseImage() to dispose the provided data.
   */
  static IplImage *convertPvImageToIpl(const Image32& img);

  /**
   * Copy the pixels of the Image to the provided Ipl image.
   * It is assumed that the destination Ipl image is created and
   * has the same dimensions and color settings as the source
   * image.
   */
  static void copyPvImageToIpl(const Image32& img, IplImage *pIplImage);

  /**
   * Copy the pixels of the Ipl image to the provided Image image.
   */
  static void copyIplToPvImage(IplImage *pIplImage, Image32& img);

  /**
   * Create a new GRAY Ipl image containing the pixels in the source image.
   * Use cvReleaseImage() to dispose the provided data.
   */
  static IplImage *convertPvImageToIpl(const Image8& img);

  /**
   * Copy the pixels of the Image to the provided Ipl image.
   * It is assumed that the destination Ipl image is created and
   * has the same dimensions and color settings as the source
   * image.
   */
  static void copyPvImageToIpl(const Image8& img, IplImage *pIplImage);

  /**
   * Copy the pixels of the Ipl image to the provided Image image.
   */
  static void copyIplToPvImage(IplImage *pIplImage, Image8& img);

};


}; // namespace vision

}; // namespace ait

#endif // OpenCvUtils_HPP


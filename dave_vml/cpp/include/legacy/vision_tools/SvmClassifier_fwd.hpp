/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef SvmClassifier_fwd_HPP
#define SvmClassifier_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class SvmClassifier;
typedef boost::shared_ptr<SvmClassifier> SvmClassifierPtr;

}; // namespace vision

}; // namespace ait

#endif // SvmClassifier_fwd_HPP


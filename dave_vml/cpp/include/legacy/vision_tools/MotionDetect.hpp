/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef MotionDetect_HPP
#define MotionDetect_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <list>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>
#include <legacy/pv/ait/Image.hpp>
#include <legacy/pv/ait/Image_fwd.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

#include "legacy/vision_tools/MotionDetect_fwd.hpp"

namespace ait 
{


/**
 * Detect motion energy per pixel in a sequence of images.
 */
class MotionDetect : private boost::noncopyable
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  MotionDetect();

  /**
   * Destructor
   */
  virtual ~MotionDetect();

  //
  // OPERATIONS
  //

public:

  /**
   * Update the motion information.
   * @param inputImage [in] New image to be added to the model.
   */
  virtual void update(const Image8& inputImage) = 0;

  /**
   * Update the motion information faster through direct memory access.
   * @param inputImage [in] New image to be added to the model.
   */
  virtual void update_fast(const Image8& inputImage) = 0;

  /**
   * Reset the motion model.
   */
  virtual void reset() = 0;

  //
  // ACCESS
  //

public:

  /**
   * Get the motion energy image.
   */
  virtual const Image8& getMotionEnergy() = 0;

  //
  // INQUIRY
  //

public:

  /**
   * Return true if there is motion in the current image.
   */
  virtual bool isMoving() = 0;

  //
  // ATTRIBUTES
  //

protected:

};


}; // namespace ait

#endif // MotionDetect_HPP


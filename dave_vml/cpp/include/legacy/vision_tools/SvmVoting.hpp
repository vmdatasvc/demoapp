/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef SvmVoting_HPP
#define SvmVoting_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>
#include <legacy/math/RunningStats.hpp>

// LOCAL INCLUDES
//
#include <legacy/vision_tools/SvmClassifier.hpp>

// FORWARD REFERENCES
//

#include "legacy/vision_tools/SvmVoting_fwd.hpp"

namespace ait 
{

namespace vision
{


/**
 * This class accumulates results of the SvmClassifiers and returns a robust
 * result. It aggregates the distance to the boundary of several SVM results,
 * and removes the outlayers, the robust result is defined by the sign of the
 * accumulated distance.
 */
class SvmVoting : private boost::noncopyable
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  SvmVoting();

  /**
   * Destructor
   */
  virtual ~SvmVoting();

  /**
   * Initialization.
   * @param pSvm [in] Reference to the Svm classifier.
   * @param maxVotingSamples [in] Maximum number of elements to accumulate.
   * It works as a circular buffer.
   */
  void init(SvmClassifierPtr pSvm, int maxVotingSamples);

  //
  // OPERATIONS
  //

public:

  /**
   * Add one sample, a result from the SVM classifier.
   */
  void addSample(const SvmClassifier::Result& r);

  /**
   * Return the current robust result.
   */
  Label getResult();

  /**
   * Robust decision value from accumulated results.
   */
  double getRobustDistance() { return mMedianDistance; }

  /**
   * Merge the samples from the incoming votes.
   */
  void merge(const SvmVoting& from);

private:
  
  /**
   * Calculate the result of the voting.
   */
  void calcResult();

  //
  // ACCESS
  //

public:


  //
  // INQUIRY
  //

public:


  //
  // ATTRIBUTES
  //

protected:

  /**
   * Accumulative decision values from the SVM.
   */
  RunningStats<float> mDistanceStats;

  /**
   * Reference to the Svm classifier.
   */
  SvmClassifierPtr mpSvm;

  /**
   * Resulting label.
   */
  Label mResultLabel;

  /**
   * The robust mean distance from the accumulated samples.
   */
  double mMedianDistance;

};


}; // namespace vision

}; // namespace ait

#endif // SvmVoting_HPP


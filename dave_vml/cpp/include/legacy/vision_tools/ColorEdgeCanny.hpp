/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ColorEdgeCanny_HPP
#define ColorEdgeCanny_HPP

#define PI 3.14159265358979
#define PI_2 1.57079632679489

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//
#include "legacy/vision_tools/ColorEdgeDetectionBase.hpp"
#include <legacy/pv/ait/Image.hpp>
#include <legacy/types/String.hpp>
#include <vector>
//#include "Settings.hpp"
#include "legacy/vision_tools/PvImageProc.hpp"
#include "legacy/vision_tools/ColorConvert.hpp"
#include "legacy/vision_tools/image_proc.hpp"
#include "legacy/vision_tools/Histogram.hpp"
//#include "Benchmark.hpp"
// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

#include "ColorEdgeCanny_fwd.hpp"

namespace ait 
{
namespace vision
{

class ColorEdgeCanny : public ait::ColorEdgeDetectionBase
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  ColorEdgeCanny();

  /**
   * Destructor
   */
  virtual ~ColorEdgeCanny();


  //
  // OPERATIONS
  //

public:
	/**
	* Initialization.
	*/
	void init(const Settings &set, const int w, const int h);
	/**
	 * process function	
	 */
	void process(Image32 &inImage);

    void process(Image32 &inImage, Rectangle<int>& rect);

	/**
	 * clear operation	
	 */
	void clear();

  //
  // ACCESS
  //

public:
   /**
	* Retrieve a reference to the gradient image	
	*/
	Image32& GetGradientImage() { return gradientImage; }
	/**
	* Retrieve the gradient image	
	*/
	void GetGradientImage(Image32 & outImage) { outImage = gradientImage; }
    /**
	* Retrieve a reference to the orientation image	
	*/	
    Image<float>& GetOrientationImage() { return orientationImage;}
	/**
	* Retrieve the orientation image	
	*/
	//void GetOrientationImage(Image32 &outImage) { outImage = orientationImage;	}
    void GetOrientationImage(Image<float> &outImage) { outImage = orientationImage;	}
	/**
	* Retrieve ONLY the magnitude of the color edges. If the 'onlyGrayScaleEdgesDesired'
	* flag in turned ON then this method retrieves the magnitude of the grayscale edges
	*/
	void GetGradientImage(Image8& outImage);
	/**
	* retrieve ONLY the magnitudes of color edges (independent of the luminance edges)	
	* ****USE DYNAMIC CAST TO CALL THIS FUNCTION. BASE CLASS DOES NOT KNOW OF THIS
	*     FUNCTION*****
	*/
	void GetColorGradient(Image8& outImage);
	/*
	*	retrieve the entire color gradient image
	* ****USE DYNAMIC CAST TO CALL THIS FUNCTION. BASE CLASS DOES NOT KNOW OF THIS
	*     FUNCTION*****
	*/
	void GetColorGradient(Image32& outImage)
	{ 
		if(onlyGrayScaleEdgesDesired)
			outImage.setAll(0);
		else
			outImage = chrominanceEdge; 
	}
  //
  // INQUIRY
  //

public:

  /**
   * Ask if this object has been initialized (the init() function has been called).
   */
  bool IsInitialized() const { return mIsInitialized; }


  //
  // ATTRIBUTES
  //

protected:

  /**
   * Tells if this object has been initialized.
   */
  bool mIsInitialized;
	/**
	* image dimensions. if (rescale == 1) rows = numOfRows/2 and cols = numOfCols/2
	 */
	int numOfRows, numOfCols, rows, cols, r, c;
	/**
	* start markers for row and column	
	*/
	int rInitial, cInitial;
	int initPos, initPosCh; //the intitial position for starting all computations
	int pos, //position of pixel that is currently under processing
		pos1,  //position of pixel exactly 1 row behind of pixel at position 'pos'
		pos2,  //position of pixel exactly 1 row ahead of pixel at position 'pos'
		offSet;//the offset to add each of the above positions to skip the remaining cols
					 //in the current row and jump the same number of cols in the next row.
		//luminance and chrominance component
	int northEastDiff, southEastDiff, hDiff, vDiff;
	int fX, fY;
	unsigned char fMag, fMagcb, fMagcr, fMagLu, fMagCh;
	int fXcb, fYcb, fXcr, fYcr;
	float orientationY, temp, multFactor;
	unsigned int tempMag;
	//fusing edges
	unsigned char fMagXY, fMagYY, fMagX, fMagXC, fMagYC, fMagY;
	int unweightedXC, unweightedYC, unweightedXY, unweightedYY;
	/**
	 * settings object	
	 */
	Settings mSettings;
	/**
	* choice of color space and rescale
	 */
	std::string colorSpace;
	bool rescale, onlyGrayScaleEdgesDesired;
	float chMagBooster, LuMagLimiter;
	/**
	 * minimum edge magnitude
	 */
	int mMinSignificantEdgeThreshold;
	/**
	 * target colorspace image and pointer to data
	 */
	Image32 targetColSpaceImage, gradientImage, histImage;
    //Image32 orientationImage;
    Image<float> orientationImage;
	Image32 luminanceEdge, chrominanceEdge;
	unsigned char *pTargetColSpaceImageData, *pGradientImageData, /* *pOrientationImageData,*/
		*pLuminanceEdgesImageData, *pChrominanceEdgeImageData;
    //unsigned int *pOrientationImageData;
    float *pOrientationImageData;
	
    Histogram hist;
	float percentile;
	Image<Uint> gradMag;
	unsigned char maxMag, minMag; //maximum and minimum edge magnitudes

//	Benchmark mBenchmark, mBenchmarkLuminanceEdge, mBenchmarkChrominanceEdge, mBenchmarkFuseEdges,
//		mBenchmarkPercentile;
	bool enableBenchmark;



protected:
	void readVarsFromRegistry();
	void convertToTargetColorSpace(Image32 &inImage);
	void EdgeByGradient();
	void EdgesOfLuminanceComponent();
	void EdgesOfColorComponents();

	void FuseEdgeResponses();
	void RescaledFuseEdges();
	void scaleFusedEdgeResponse();
		void findPercentile(float&);
	//void ThresholdGradientImage();
		//Uint computeThreshold();
};

}; // namespace vision

}; // namespace ait

#endif // ColorEdgeCanny_HPP


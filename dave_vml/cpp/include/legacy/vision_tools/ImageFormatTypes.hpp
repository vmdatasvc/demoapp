
#ifndef ImageFormatTypes_HPP
#define ImageFormatTypes_HPP

namespace ait
{

class ImageFormatTypes
{

public:

  /**
   * Image pixel format identifier. These codes match the IPP UMC library
   */
  enum _ImageFormatTypes
  {

      NONE    = -1,
      YV12    = 0,    // Planar Y, U, V, 8 bit per component
      NV12    ,       // Planar merged Y, U->V, 8 bit per component
      YUY2    ,       // Composite Y->U->Y->V 8 bit per component
      UYVY    ,       // Composite U->Y->V->Y 8 bit per component
      YUV411  ,       // Planar Y, U, V, 8 bit per component
      YUV420  ,       // Planar Y, U, V, 8 bit per component (trained peolpe use only)
      YUV422  ,       // Planar Y, U, V,
      YUV444  ,       // Planar Y, U, V,
      YUV420M ,       // Planar merged Y, U->V, 8 bit per component (trained peolpe use only)
      YUV422M ,       // Planar merged Y, U->V, (trained peolpe use only)
      YUV444M ,       // Planar merged Y, U->V, (trained peolpe use only)
      RGB32   ,       // Composite B->G->R->A 8 bit per component
      RGB24   ,       // Composite B->G->R 8 bit per component
      RGB565  ,       // Composite B->G->R 5 bit per B & R, 6 bit per G
      RGB555  ,       // Composite B->G->R->A, 5 bit per component, 1 bit per A
      RGB444          // Composite B->G->R->A, 4 bit per component

  };

};

}


#endif // ImageFormatTypes_HPP
/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef SvmVotingCascade_fwd_HPP
#define SvmVotingCascade_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class SvmVotingCascade;
typedef boost::shared_ptr<SvmVotingCascade> SvmVotingCascadePtr;

}; // namespace vision

}; // namespace ait

#endif // SvmVotingCascade_fwd_HPP


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef SvmVoting_fwd_HPP
#define SvmVoting_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class SvmVoting;
typedef boost::shared_ptr<SvmVoting> SvmVotingPtr;

}; // namespace vision

}; // namespace ait

#endif // SvmVoting_fwd_HPP


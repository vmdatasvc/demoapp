/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef Classifier_fwd_HPP
#define Classifier_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class Classifier;
typedef boost::shared_ptr<Classifier> ClassifierPtr;

}; // namespace vision

}; // namespace ait

#endif // Classifier_fwd_HPP


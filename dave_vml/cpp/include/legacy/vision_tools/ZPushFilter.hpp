/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ZPushFilter_HPP
#define ZPushFilter_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/shared_ptr.hpp>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>
#include <legacy/pv/ait/Vector3.hpp>
#include <legacy/pv/CircularBuffer.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

// Shriram

namespace ait 
{


/**
 * This class implements a method to filter positions 
 */
class ZPushFilter : public SmoothTrajectory<float>
{
  //
  // LIFETIME
  //

public:

  typedef Matrix<float> PvMatrixt;

  /**
   * Constructor.
   */
  ZPushFilter()
  {
  }

  /**
   * Destructor
   */
  virtual ~ZPushFilter()
  {
  }

  //
  // OPERATIONS
  //

public:


  /**
   * Initialization of the algorithm.
   */
  void init(int k, float lambda)
  {
    paramK = k;
    paramLambda = lambda;
    mHistory.setCapacity(5);
    mDiffHistory.setCapacity(5);
    reset();
  }

  /**
   * Filter the current trajectory. Use state() to retreive the filtered position.
   * @param v [in] new position.
   * @param v [out] filtered position.
   */
  virtual void update(PvMatrixt& v)
  {
    mHistory.push_back(Vector3f(v(0),v(1),v(2)));

    Vector3f newFilteredPos;
    float xDiff, yDiff, zDiff;
    float conductionCoeff;
    float val = 0.5;
    
    mDiffHistory.push_back(Vector3f(fabs(v(0) - mLastFilteredPos(0)), 
                                     fabs(v(1) - mLastFilteredPos(1)), 
                                     fabs(v(2) - mLastFilteredPos(2))));

    newFilteredPos(0) = mLastFilteredPos(0);
    newFilteredPos(1) = mLastFilteredPos(1);
    newFilteredPos(2) = v(2);
    
    if(mHistory.size() == 5)
    {
      xDiff = 0;
      yDiff = 0;
      zDiff = 0;
      val =0.5;

      for(int i =4; i >= 0; i--)
      {
        xDiff += val * mDiffHistory[i](0);
        yDiff += val * mDiffHistory[i](1);
        zDiff += val * mDiffHistory[i](2);

        val = val/2;
      }
      
      if(zDiff > 0.5 * (xDiff + yDiff))
      {
        conductionCoeff = 1 - 1/(1 + (zDiff * zDiff * (paramK * paramK)));                
        newFilteredPos(0) += paramLambda * conductionCoeff * xDiff;
        newFilteredPos(1) += paramLambda * conductionCoeff * yDiff;

        PVMSG("(%f,%f) ->",paramLambda * conductionCoeff * xDiff, paramLambda * conductionCoeff * yDiff);

      }
    }
    
    mFilteredPosMatrix.resize(3);
    mFilteredPosMatrix(0) = newFilteredPos(0);
    mFilteredPosMatrix(1) = newFilteredPos(1);
    mFilteredPosMatrix(2) = newFilteredPos(2);
    
    mLastFilteredPos = newFilteredPos;
    
    v = mFilteredPosMatrix;
    
    PVMSG("(%f,%f,%f)\n",v(0),v(1),v(2));
  }
  
  /**
  mLastDiff = diff;
  * Return the current state. Must call update() first.
  * @return last calculated value.
  */
  virtual const PvMatrixt &state(void) { return mFilteredPosMatrix; };
  
  /**
   * Reset.
   */
  virtual void reset() 
  {
    mHistory.clear(); 
  }


  //
  // ACCESS
  //

public:


  /**
   * @return w0 == 1 && w1 == 0 if it is in local tracking mode.
   * w0==0 && w1==1 otherwise.
   */
  virtual void getWeights(float* w0, float* w1) 
  { 
//    if (mIsLocalTrack)
//    {
//      *w0 = 1; *w1 = 0; 
//    }
//    else
//    {
//      *w0 = 0; *w1 = 1; 
//    }
  }

  //
  // INQUIRY
  //

public:


  //
  // ATTRIBUTES
  //

protected:

  /**
   * Current filtered position.
   */
  PvMatrixt mFilteredPosMatrix;

  /**
   * Last position after filtering.
   */
  Vector3f mLastFilteredPos;

  /**
   * Buffer for the position history. These positions are before
   * filtering.
   */
  CircularBuffer<Vector3f> mHistory;
  CircularBuffer<Vector3f> mDiffHistory;

  int paramK;
  float paramLambda;

};

/// Smart pointer to ZPushFilter
typedef boost::shared_ptr<ZPushFilter> ZPushFilterPtr;

}; // namespace ait

#endif // ZPushFilter_HPP


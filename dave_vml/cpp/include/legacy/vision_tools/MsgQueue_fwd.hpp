/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef MsgQueue_fwd_HPP
#define MsgQueue_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class MsgQueue;
typedef boost::shared_ptr<MsgQueue> MsgQueuePtr;

}; // namespace vision

}; // namespace ait

#endif // MsgQueue_fwd_HPP


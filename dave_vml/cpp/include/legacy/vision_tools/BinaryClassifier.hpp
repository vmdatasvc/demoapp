/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef BinaryClassifier_HPP
#define BinaryClassifier_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>

// LOCAL INCLUDES
//

#include "legacy/vision_tools/Classifier.hpp"

// FORWARD REFERENCES
//

#include "legacy/vision_tools/BinaryClassifier_fwd.hpp"

namespace ait 
{

namespace vision
{


/**
 * A classifier with two classes.
 */
class BinaryClassifier : public Classifier
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  BinaryClassifier();

  /**
   * Destructor
   */
  virtual ~BinaryClassifier();

  //
  // OPERATIONS
  //

public:


  //
  // ACCESS
  //

public:

  /**
   * Set positive and negative labels.
   */
  void init(ClassifierLabelsPtr pLabels, Label positive, Label negative)
  {
    Classifier::init(pLabels);
    mPositiveLabel = positive;
    mNegativeLabel = negative;
  }

  /**
   * Get positive label.
   */
  Label getPositiveLabel() const
  {
    return mPositiveLabel;
  }

  /**
   * Return the negative label.
   */
  Label getNegativeLabel() const
  {
    return mNegativeLabel;
  }

  //
  // INQUIRY
  //

public:


  //
  // ATTRIBUTES
  //

protected:

  /**
   * Positive Label identifier.
   */
  Label mPositiveLabel;

  /**
   * Negative Label identifier.
   */
  Label mNegativeLabel;

};


}; // namespace vision

}; // namespace ait

#endif // BinaryClassifier_HPP


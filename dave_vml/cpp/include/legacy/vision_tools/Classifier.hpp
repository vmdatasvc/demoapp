/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef Classifier_HPP
#define Classifier_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>

// LOCAL INCLUDES
//

#include "legacy/vision_tools/ClassifierLabels.hpp"

// FORWARD REFERENCES
//

#include "legacy/vision_tools/Classifier_fwd.hpp"

namespace ait 
{

namespace vision
{

/**
 * Abstract base classifier class. An abstract classifier
 * is defined by samples that will be classified into a discrete
 * number of classes, that are identified by labels. The result
 * of the classification will return the label and the confidence
 * of the classification. Inherited classes can extend the results
 * to more information.
 * @par Responsibilities:
 * - Provide a base class for every classifier.
 * - Provide a base class for the sample.
 * - Provide a base class for the Result.
 * - Maintain a reference to the label values.
 */
class Classifier : private boost::noncopyable
{
  //
  // LIFETIME
  //

public:

  /**
   * Base sample class. A sample is the input to any
   * classifier. Each classifier defines its own sample
   * types.
   */
  class Sample
  {
  public:
    /// This empty virtual destructor will make this class polymorphic.
    virtual ~Sample() {}
  };
  typedef boost::shared_ptr<Sample> SamplePtr;

  /**
   * Result of the classifier. The result corresponds to a
   * sample instance.
   */
  class Result
  {
  public:
    /// Label of the result.
    Label label;
    /// Confidence of the result in the interval [0..1]
    float confindence;
    /// This empty virtual destructor will make this class polymorphic.
    virtual ~Result() {}
  };
  typedef boost::shared_ptr<Result> ResultPtr;

  /**
   * Constructor.
   */
  Classifier();

  /**
   * Destructor
   */
  virtual ~Classifier();

protected:
  /**
   * Initialization.
   * @param [in] Labels used for this classifier.
   */
  void init(ClassifierLabelsPtr pLabels);

  //
  // OPERATIONS
  //

public:

  /**
   * Create an instance of the results class. This is a factory pattern.
   */
  virtual ResultPtr createResult() = 0;

  /**
   * Perform the classification.
   * @param sample [in] input sample for the classifier.
   * @param result [out] result of the classification.
   */
  virtual void classify(Sample& sample, Result& result) = 0;

  //
  // ACCESS
  //

public:


  /**
   * Get the labels.
   */
  ClassifierLabelsPtr getLabels() { return mpLabels; }

  //
  // INQUIRY
  //

public:

  /**
   * Ask if this object has been initialized (the init() function has been called).
   */
  Bool isInitialized() const { return mIsInitialized; }


  //
  // ATTRIBUTES
  //

protected:

  /**
   * Tells if this object has been initialized.
   */
  Bool mIsInitialized;

  /**
   * Labels for this classifier.
   */
  ClassifierLabelsPtr mpLabels;

};

}; // namespace vision

}; // namespace ait

#endif // Classifier_HPP


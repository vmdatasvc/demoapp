/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VisionMsg_fwd_HPP
#define VisionMsg_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class VisionMsg;
typedef boost::shared_ptr<VisionMsg> VisionMsgPtr;

}; // namespace vision

}; // namespace ait

#endif // VisionMsg_fwd_HPP


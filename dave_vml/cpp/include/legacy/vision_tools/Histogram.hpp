/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/


#ifndef HISTOGRAM_HPP
#define HISTOGRAM_HPP

#include <legacy/vision_tools/PvImageProc.hpp>
#include <legacy/types/AitBaseTypes.hpp>
#include <legacy/pv/PvRandom.hpp>

#include "legacy/vision_tools/Histogram_fwd.hpp"

namespace ait
{

/**
 * This class calculates 1 dimensional and n dimensional histograms
 * from single or multichannel images.
 */
class Histogram : public Matrix<Uint>
{
public:

  /**
   * Constructor.
   */
  Histogram();

  /**
   * Assignment operator.
   */
  Histogram& operator=(const Histogram& from);

  /**
   * Allocate a 2D histogram. This will store compute a histogram of each
   * channel separately in a matrix. Each column of the matrix will represent
   * the histogram of each channel.
   * @param numChannels [in] Number of color channels.
   * @param numBins [in] Number of bins per channel.
   */
  void allocate(Uint numChannels, Uint numBins);
  
  /**
   * Allocate an N dimensional histogram. The dimension is given by the size of
   * the input vector. The number of bins per dimension can be different.
   * @param numBins [in] vector with size of each channel.
   * @param isMultidimensional [in] indicates if the histogram is 
   */
  void allocate(const Matrix<Uint>& numBins, Bool isMultidimensional = false);

  /**
   * Return the number of channels for this histogram.
   */
  Uint getNumChannels(void) const { return mNumBins.size();}

  /**
   * Return the number of samples used to compute this histogram.
   */
  Uint getNumSamples(void) const { return mNumSamples; }

  /**
   * Return if the histogram is Multidimensional.
   */
  Bool isMultidimensional() const { return mIsMultidimensional; }

  /**
   * Return the size for each bin.
   */
  const Matrix<Uint>& getNumBins() const { return mNumBins; }

  /**
   * Compute the histogram for each channel.
   * @param img [in] image to be analyzed.
   * @param roi range of the image used to compute the pixels.
   */
  void compute(const Image32& img, Rectanglei roi = Rectanglei(0,0,0,0));

  /**
   * Compute the histogram single gray channel.
   * @param img [in] gray image to be analyzed.
   * @param roi range of the image used to compute the pixels.
   */
  void compute(const Image8& img, Rectanglei roi = Rectanglei(0,0,0,0));

  /**
   * Compute the histogram given the indices.
   * @param img [in] image of histogram indices to be analyzed.
   */
  void compute(const Image<Uint>& img, Rectanglei roi = Rectanglei(0,0,0,0));

  /**
   * Compute the indices of the histogram given a multi channel image.
   * Later a single channel image can be used to compute the histogram
   * faster.
   */
  void computeIndices(const Image32& inputImage, 
                      Image<Uint>& indexImage, 
                      Rectanglei roi = Rectanglei(0,0,0,0));

  /**
   * Compute a single index given a multichannel pixel.
   */
  Uint computeIndex(const Uint32 inputPixel)
  {
    if (!mIsMultidimensional)
    {
      PvUtil::exitError("Histogram::computeIndices only works with multidimensional histograms");
    }

    const Uint8 *pData = (const Uint8*)(&inputPixel);
    return (static_cast<Uint>(pData[R_IDX])/(256/mNumBins(0)))*mNumBins(1)*mNumBins(2) + 
           (static_cast<Uint>(pData[G_IDX])/(256/mNumBins(1)))*mNumBins(2) +
           (static_cast<Uint>(pData[B_IDX])/(256/mNumBins(2)));
  }

  /**
   * Get the maximum per channel.
   *
   * @param vMax [out] Column vector with the maximum of each channel
   */
  void getMax(Matrix<Uint>& vMax);
  
    /**
   * Get the weighted center per channel.
   *
   * @param vMax [out] Column vector with the mean color of each channel
   */
  void getWeightedCenter(Matrix<Uint>& vMax);

  /**
   * Normalize this histogram to make it equivalent to a histogram using
   * the given number of samples.
   * @param [in] New number of samples this histogram should be equivalent to.
   */
  void normalize(Uint newNumSamples);
  
  /**
   * Paint a histogram in the image.
   */
  void plot(Image32& img, Uint h, Uint stepWidth=1, Uint maxValue=0);
  
  typedef enum
  {
    ABSOLUTE_DIFFERENCE,
    SQUARE_DISTANCE,
    INTERSECTION,
    EARTH_MOVER_DISTANCE
  } CompareMethodLabel;

  /**
   * Compare two histograms.
   */
  static Double compare(const Histogram& lhs, const Histogram& rhs, CompareMethodLabel method);

private:

  /**
   * Number of bins per channel.
   */
  Matrix<Uint> mNumBins;

  /**
   * Flag that indicates if the histogram is Multidimensional.
   */
  Bool mIsMultidimensional;

  /**
   * Number of samples that were used to compute the histogram.
   */
  Uint mNumSamples;

};

}; // namespace ait

#endif //HISTOGRAM_HPP

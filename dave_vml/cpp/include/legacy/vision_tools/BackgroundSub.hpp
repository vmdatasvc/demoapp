#ifndef BackgroundSub_HPP
#define BackgroundSub_HPP

#ifdef __ICL
// This intel compiler optimization is not working,
// the resulting image is shifted.
//#define ICL_OPTIMIZATION
#endif

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>

#include <legacy/pv/ait/Image.hpp>
#include <legacy/pv/ait/Image_fwd.hpp>

#include <legacy/low_level/Settings.hpp>

namespace ait 
{

/**
 * This class learns the background and performs segmentation
 * on a sequence of images. This class stores two models internally,
 * one is the current valid model, and the other is the model in
 * construction.
 * @remark This class does not perform smoothing and color space
 * transofrmations. However, results are better when these operations
 * are applied. It is the responsibility of the caller to make these
 * operations.
 */
class BackgroundSub
{
protected:

  typedef struct
  {
    float ch1Mean;
    float ch2Mean;
    float ch3Mean;
    float dummy0; // This ensures data alignment for SIMD optimizations.
    float ch1Var;
    float ch2Var;
    float ch3Var;
    float dummy1; // This ensures data alignment for SIMD optimizations.
  } PixelStats;

	class BackgroundModel
	{
  public:

    BackgroundModel() : numFrames(0) {}
    ~BackgroundModel()
    {
#ifdef ICL_OPTIMIZATION
      if (stats.size() > 1)
      {
        _mm_free(stats.pointer());
      }
#endif  
    }
    bool isInitialized() { return stats.size() != 1; }

    Image<PixelStats> stats;
		
    int numFrames;
  };

	typedef boost::shared_ptr<BackgroundModel> BackgroundModelPtr;

public:

  /**
   * Constructor.
   */
	BackgroundSub();

  /**
   * Destructor.
   */
	~BackgroundSub();

	/**
	 * Initialization function. Initializes from the settings.
	 */
  void init(Settings &set);

	/**
	 * This function resets the model that is being built. The valid
   * background model remains active.
	 */
	void reset();

	/**
	 * This will reset all the models, including the current valid model.
	 */
	void resetValid();

	/**
	 * This function will perform segmentation on the given image.
   * @param image [in] The image to process.
   * @perem learnNewModel [in] Indicates that the class should use
   * this image to learn a new background.
   * @remark Learning the background is a very expensive operation,
   * the caller might use other methods such as motion detection
   * to determine if the background should be learned.
   * @remark The body of this template is defined here because the
   * MS VC++ 6.0 compiler does not support out of class definitions
   * of template members.
	 */
  template <class ImageType> void 
  process(const ImageType &image, bool learnNewModel)
  { 
    // If the image size has changed, reset everything.
    if (mForegroundImage.width() != image.width() || 
        mForegroundImage.height() != image.height())
    {
      resetValid();
      mForegroundImage.resize(image.width(), image.height());    
    }

    mModelUpdated = false;

    if (learnNewModel)
    {
      if (!mpBuildModel->isInitialized())
      {
        backgroundInitialize(image,mpBuildModel);
        mModelUpdateCounter = mModelUpdateInterval-1;
      }
      else
      {
        if (!mpValidModel.get() || mModelUpdateCounter == 0)
        {
          backgroundModel(image,mpBuildModel);
          if (mpBuildModel->numFrames >= mNumBackgroundFrames/mModelUpdateInterval ||
              (!mpValidModel.get() && mpBuildModel->numFrames >= mInitNumFrames))
          {
            mpValidModel = mpBuildModel;
            reset();
            mModelUpdated = true;
          }
        }
      }
    }
  
    mForegroundImage.setAll(0);
  
    if (mpValidModel.get())
    {
      foregroundSegment(image,mForegroundImage);
    }
  
    mModelUpdateCounter--;
    if (mModelUpdateCounter < 0) mModelUpdateCounter = mModelUpdateInterval-1;
  };

  /**
   * Return true if a background model has been learned.
   */
  bool isBackgroundLearned() { return mpValidModel.get() != 0; }

  /**
   * Get the resulting foreground image.
   */
  const Image8& getForegroundImage() { return mForegroundImage; }

  /**
   * This flag indicates that the valid model was changed in the last call to 
   * process().
   */
  bool isModelUpdated() { return mModelUpdated; }

  /**
   * Return true if the foreground image has substantial number of pixels.
   */
  bool isForegroundPresent() const { return mIsForegroundPresent; }

  /**
   * Returns the percentage which is Foreground Image	
   */
  float getForegroundPercentage() const 
  {
    return mForegroundPercentage;
  }


protected:
  
  /**
   *	Pixel-wise Background modeling
   */
  void  backgroundModel(const Image32 &inputImage, BackgroundModelPtr &model);

	/**
   *	Segmentation of the foreground pixels from the background model
   */
	void foregroundSegment(const Image32 &inputImage, Image8 &foregroundImage);

	/**
   *	Initialize the bimodal background model
   *  Each pixel is a combination of atmost two gaussian models
   */
  void  backgroundInitialize(const Image32 &inputImage, BackgroundModelPtr &model);

  /**
   *	Pixel-wise Background modeling
   */
  void  backgroundModel(const Image8 &inputImage, BackgroundModelPtr &model);

	/**
   *	Segmentation of the foreground pixels from the background model
   */
	void foregroundSegment(const Image8 &inputImage, Image8 &foregroundImage);

	/**
   *	Initialize the bimodal background model
   *  Each pixel is a combination of atmost two gaussian models
   */
  void  backgroundInitialize(const Image8 &inputImage, BackgroundModelPtr &model);


public:

  /**
   * Return the foreground pixel threshold.
   */
  float getForegroundPixelThreshold() const { return mForegroundPixelThreshold; }

  void setForegroundPixelThreshold(float f) { mForegroundPixelThreshold = f; }


protected:

  /**
   * This is the number of frames required to learn a new model. This
   * represent the number of times that the process() function must be
   * called, and it is not dependent on the mModelUpdateInterval variable.
   * The actual number of frames used in the model is mNumBackgroundFrames/mModelUpdateInterval.
   */
	int mNumBackgroundFrames;

	/**
	 *	(mBackgroundLearnThreshold * sigma) forms the bound of the adaptive model
	 */
	int mBackgroundLearnThreshold;

	/**
	 * Threshold to seperate valid foreground detection from random noise
   * as a fraction of image size. This is used to detect the presence of
   * foreground objects.
	 */
	float mForegroundPixelThreshold;

  /**
   * Minimum value of the foreground model after magnification.
   * Range from 0 to 255. All values smaller than this are clamped
   * to zero.
   */
  float mMinForegroundThreshold;

  /**
   * Model that is already valid.
   */
  BackgroundModelPtr mpValidModel;

  /**
   * Model in construction.
   */
	BackgroundModelPtr mpBuildModel;

  /**
   * Segmentation image. 0 = background; 255 = foreground. Values
   * are in the [0..255] range.
   */
	Image8 mForegroundImage;

  /**
   * The distance from the model is multiplied by this factor
   * before thresholding by mMinForegroundThreshold and 255.
   */
	float mMagnificationFactor;

  /**
   * This flag indicates that the valid model was changed in the last call to 
   * process().
   */
  bool mModelUpdated;

  /**
   * Model update inverval. The interval to update the background model. 1
   * means every frame, 2 is every other frame, etc.
   */
  int mModelUpdateInterval;

  /**
   * Counter to the next update, to use with mModelUpdateInterval.
   */
  int mModelUpdateCounter;

  /**
   * This flag indicates if the amount of foreground is greater than a threshold
   */
  bool mIsForegroundPresent;

  /**
   * This is the number of frames required to learn the first model. This will allow
   * the system to learn the model quickly when the application starts, or when 
   * the settings are changed.
   */
  int mInitNumFrames;

  /**
   * Contains the Percentage of the image which is foreground
   * Note: the value is between 0 and 1, 
   *  For example, 0.2 denotes that 20% of the image has foreground.
   */
  float mForegroundPercentage;
};

}; // namespace ait

#endif // BackgroundSub_HPP



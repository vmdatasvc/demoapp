/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef BackgroundLearner_HPP
#define BackgroundLearner_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

#include <list>

#include <legacy/low_level/Settings_fwd.hpp>
// AIT INCLUDES
//

#include <legacy/types/String.hpp>
#include <legacy/pv/ait/Image_fwd.hpp>
#include <legacy/vision_tools/image_proc.hpp>

// LOCAL INCLUDES
//
#include <legacy/pv/ait/Image.hpp>
#include "legacy/vision_tools/BackgroundModelBase.hpp"

#include <legacy/vision_tools/ColorConvert.hpp>

// FORWARD REFERENCES
//

#include "legacy/vision_tools/BackgroundLearner_fwd.hpp"

namespace ait 
{


/**
 * Base class for background foreground segmentation classes.
 * @par Responsibilities:
 * - Provide basic interfaces for color and grayscale segmentations.
 * - Handle an active model and the learning model.
 */
class BackgroundLearner : private boost::noncopyable
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  BackgroundLearner();

  /**
   * Destructor
   */
  virtual ~BackgroundLearner();

  /**
   * Initialization.
   */
  virtual void init(const std::string& settingsPath);
//  virtual void init(Settings &set);

  /**
   * Save the current background model to the specified file
   */
  virtual void saveModel(std::string filename);

  /**
   * Load the current background model from the specified file
   */
  virtual void loadModel(std::string filename);

protected:
  
  /**
   * Create the background model according to the mBackgroundModelClassName.
   */
  BackgroundModelBasePtr createBackgroundModel();

  BackgroundModelBasePtr createBackgroundModel(std::string settingsPathName);


  //
  // OPERATIONS
  //

public:

	/**
	 * This function resets the model that is being built. The valid
   * background model remains active.
	 */
	virtual void reset();

	/**
	 * This will reset all the models, including the current valid model.
	 */
	virtual void resetValid();

  /**
   * This function resets the update intervals to start from the beginning
   * of the list specified in init. i.e. 'numberOfBackgroundFrames',
   'modelUpdateInterval'
   */
  virtual void resetIntervals();

	/**
	 * This function will perform segmentation on the given image
   * and update the model if necessary. The model might not get
   * updated if the updateInterval variable in the Settings is set 
   * to a number larger than 1.
   * @param image [in] The image to process.
   * @perem learnNewModel [in] Indicates that the class should use
   * this image to learn a new background. On some scenarios, the
   * user might know this image has too much motion or other characteristics
   * that make it better to ignore this image when learning.
	 */  

  virtual void process(const Image32Ptr pImg, bool learnNewModel = true);

	/**
	 * This function will perform segmentation on the given image
   * and update the model if necessary. The model might not get
   * updated if the updateInterval variable in the Settings is set 
   * to a number larger than 1.
   * @param image [in] The image to process.
   * @perem learnNewModel [in] Indicates that the class should use
   * this image to learn a new background. On some scenarios, the
   * user might know this image has too much motion or other characteristics
   * that make it better to ignore this image when learning.
	 */
  
  virtual void process(const Image8Ptr pImg, bool learnNewModel = true);  

  /**
   * Return true if a background model has been learned.
   */
  virtual bool isBackgroundLearned() { return mpValidModel.get() != 0; }

  /**
   * Get the resulting foreground image.
   */
  virtual const Image8Ptr getForegroundImage() { return mpForegroundImage; }

  /**
   * Get the resulting foreground image.
   */
  virtual const Image8Ptr getForegroundNormalizedImage() { return mpForegroundNormalizedImage; }

  /**
   * This flag indicates that the valid model was changed in the last call to 
   * process().
   */
  virtual bool isModelUpdated() { return mModelUpdated; }

protected:

  /**
   * Template to handle both the gray and color images in one function.
   */
  
  template <class ImageType> void 
  processImplementation(boost::shared_ptr<ImageType> pImage, bool learnNewModel)
  {
    // If the image size has changed, reset everything.
    if (!mpForegroundImage.get() || 
        mpForegroundImage->width() != pImage->width() || 
        mpForegroundImage->height() != pImage->height())
    { 
      resetValid();
      mpForegroundImage.reset(new Image8(pImage->width(), pImage->height()));           
    }

    mModelUpdated = false;    

    if (learnNewModel)
    {       

      if (!mpBuildModel.get())
      {
        mpBuildModel = createBackgroundModel();
        mModelUpdateCounter = *miUpdateInterval - 1;
      }
      else
      {
        if (mModelUpdateCounter == 0)
        {
          mpBuildModel->update(pImage);
          if (mpBuildModel->getNumFrames() >= *miNumFrames)
          {
            mpBuildModel->validate();
            mpValidModel = mpBuildModel;
            reset();
            
            miNumFrames++;
            miUpdateInterval++;

            if (miNumFrames == mNumBackgroundFrames.end()) miNumFrames--;
            if (miUpdateInterval == mModelUpdateInterval.end()) miUpdateInterval--;
            mModelUpdated = true;
          }
        }
      }
    }   
  
    mpForegroundImage->setAll(0);
  
    if (mpValidModel.get())
    { 
      mpValidModel->foregroundSegment(pImage,mpForegroundImage);
      if (mMedianFilterSize > 1)
      {
        image_proc::smoothImage(*mpForegroundImage,image_proc::SMOOTH_MEDIAN,mMedianFilterSize);
      }
    }
  
    mModelUpdateCounter--;
    if (mModelUpdateCounter < 0) mModelUpdateCounter = *miUpdateInterval - 1;  
  };

  void 
  processImplementationHack(boost::shared_ptr<Image32> pColorImage, bool learnNewModel)
  {   
    const Image32& colorImg = *pColorImage;

    Image8Ptr pIntensityImage(new Image8());
    

    ColorConvert::convert(*pColorImage, PvImageProc::RGB_SPACE, *pIntensityImage, PvImageProc::GRAY_SPACE);
    //const Image8& intenseImg = *pIntensityImage;

    //Hack, make a normalized G image and use it
    Image8Ptr pNormalizedImg(new Image8(pColorImage->width(), pColorImage->height()));
    Image8& normImg = *pNormalizedImg;      
    
    for (int y = 0; y < normImg.height(); y++)
      for (int x = 0; x < normImg.width(); x++)
      {        
        unsigned int const r = RED(colorImg(x,y));
        unsigned int const g = GREEN(colorImg(x,y));
        unsigned int const b = BLUE(colorImg(x,y));

        normImg(x,y) = round(g/(float)(r+g+b)*255.0);
      }      
      //(*pNormalizedImg) = *pIntensityImage;

    // If the image size has changed, reset everything.
    if (!mpForegroundImage.get() || 
        mpForegroundImage->width() != pColorImage->width() || 
        mpForegroundImage->height() != pColorImage->height())
    { 
      resetValid();
      mpForegroundImage.reset(new Image8(pColorImage->width(), pColorImage->height()));           
      mpForegroundNormalizedImage.reset(new Image8(pColorImage->width(), pColorImage->height()));   
    }

    mModelUpdated = false;    

    if (learnNewModel)
    {       

      if (!mpBuildModel.get())
      {
        mpBuildModel = createBackgroundModel();
        mpBuildNormalizedModel = createBackgroundModel("NormalizedHistogramBackgroundModel");

        mModelUpdateCounter = *miUpdateInterval - 1;
      }
      else
      {
        if (mModelUpdateCounter == 0)
        {
          
          //mpBuildModel->update(pImage);   
          
          //PVMSG("%d %d\n", normImg.height(), normImg.width());          

          //update the models
          mpBuildModel->update(pIntensityImage);
          mpBuildNormalizedModel->update(pNormalizedImg);


          if (mpBuildModel->getNumFrames() >= *miNumFrames)
          {

            PVMSG("SWITCHING BG MODEL!\n");

            mpBuildModel->validate();
            mpBuildNormalizedModel->validate();

            mpValidModel = mpBuildModel;
            mpValidNormalizedModel = mpBuildNormalizedModel;
            reset();
            
            miNumFrames++;
            miUpdateInterval++;

            if (miNumFrames == mNumBackgroundFrames.end()) miNumFrames--;
            if (miUpdateInterval == mModelUpdateInterval.end()) miUpdateInterval--;
            mModelUpdated = true;
          }
        }
      }
    }   
  
    mpForegroundImage->setAll(0);
    mpForegroundNormalizedImage->setAll(0);
  
    if (mpValidModel.get())
    { 
      mpValidModel->foregroundSegment(pIntensityImage,mpForegroundImage);
      mpValidNormalizedModel->foregroundSegment(pNormalizedImg,mpForegroundNormalizedImage);

      if (mMedianFilterSize > 1)
      {
        image_proc::smoothImage(*mpForegroundImage,image_proc::SMOOTH_MEDIAN,mMedianFilterSize);
        image_proc::smoothImage(*mpForegroundNormalizedImage,image_proc::SMOOTH_MEDIAN,mMedianFilterSize);
      }
    }
  
    mModelUpdateCounter--;
    if (mModelUpdateCounter < 0) mModelUpdateCounter = *miUpdateInterval - 1;  
  };

  //
  // ACCESS
  //

public:

  virtual BackgroundModelBasePtr getValidModel() {return mpValidModel;}


  //
  // INQUIRY
  //

public:

  /**
   * Ask if this object has been initialized (the init() function has been called).
   */
  bool isInitialized() const { return mIsInitialized; }


  //
  // ATTRIBUTES
  //

protected:

  /**
   * Tells if this object has been initialized.
   */
  bool mIsInitialized;

  /**
   * This is the number of frames required to learn a new model. This
   * represent the number of times that the process() function must be
   * called, and it is not dependent on the mModelUpdateInterval variable.
   * The actual number of frames used in the model is mNumBackgroundFrames/mModelUpdateInterval.
   */
  std::list<int>::const_iterator miNumFrames;
  
  /**
   * A list containing the number of background frames to process   
   */
  std::list<int> mNumBackgroundFrames;

  /**
   * This is the number of frames required to learn the first model. This will allow
   * the system to learn the model quickly when the application starts, or when 
   * the settings are changed.
   */
  
   /*
  int mInitNumFrames;
  */

  /**
   * Model that is already valid.
   */
  BackgroundModelBasePtr mpValidModel;

  /**
   * Model in construction.
   */
	BackgroundModelBasePtr mpBuildModel;

  /**
   * Model that is already valid.
   */
  BackgroundModelBasePtr mpValidNormalizedModel;

  /**
   * Model in construction.
   */
	BackgroundModelBasePtr mpBuildNormalizedModel;


  /**
   * Segmentation image. 0 = background; 255 = foreground. Values
   * are in the [0..255] range.
   */
	Image8Ptr mpForegroundImage;

  Image8Ptr mpForegroundNormalizedImage;

  /**
   * This flag indicates that the valid model was changed in the last call to 
   * process().
   */
  bool mModelUpdated;

  /**
   * Model update inverval. The interval to update the background model. 1
   * means every frame, 2 is every other frame, etc.
   */
  std::list<int>::const_iterator miUpdateInterval;

  /**
   * List of update intervals that will be used
   */
  std::list<int> mModelUpdateInterval;

  /**
   * Counter to the next update, to use with mModelUpdateInterval.
   */
  int mModelUpdateCounter;

  /**
   * Name of the background model class used. This class must inherit from
   * BackgroundModelBase.
   */
  std::string mBackgroundModelClassName;

  /**
   * The settings path used to initualize the models.
   */
  std::string mSettingsPath;
  /**
   * Median filter mask size to postprocess smooth image. This
   * helps to eliminate salt and pepper noise.
   */
  int mMedianFilterSize;

};


}; // namespace ait

#endif // BackgroundLearner_HPP


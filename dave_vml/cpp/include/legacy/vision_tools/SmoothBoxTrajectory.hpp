/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef SmoothBoxTrajectory_HPP
#define SmoothBoxTrajectory_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/shared_ptr.hpp>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>
#include <legacy/pv/ait/Vector3.hpp>
#include <legacy/pv/CircularBuffer.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//


namespace ait 
{


/**
 * This class implements a method to filter positions 
 */
class SmoothBoxTrajectory : public SmoothTrajectory<float>
{
  //
  // LIFETIME
  //

public:

  typedef Matrix<float> PvMatrixt;

  /**
   * Constructor.
   */
  SmoothBoxTrajectory()
  {
  }

  /**
   * Destructor
   */
  virtual ~SmoothBoxTrajectory()
  {
  }

  //
  // OPERATIONS
  //

public:


  /**
   * Initialization of the algorithm.
   * @param historySize [in] number of position vectors to record in the history array.
   * @param boxSize [in] Size of the box for local filtering. 
   */
  void init(int historySize, const Vector3f& boxSize)
  {
    mHistory.setCapacity(historySize);
    mBoxSize = boxSize;
    reset();
  }

  /**
   * Filter the current trajectory. Use state() to retreive the filtered position.
   * @param v [in] new position.
   * @param v [out] filtered position.
   */
  virtual void update(PvMatrixt& v)
  {
    // This is the outline:

    //  curBB = Current bounding box of the last n positions.
    //  maxBB = Maximum bounding box to filter.
    //  pos = Sample to filter
    //  lastPos = Position in the last frame before filtering.
    //  filterPos = Filtered position
    //  lastFilterPos = Last position after filtering.
    //
    //  - If trackMode == LOCAL:
    //    - If pos inside curBB:
    //      - deltaPos = pos-lastPos
    //      - newFilterPos = lastFilteredPos+deltaPos/2
    //    - Else:
    //      - newFilterPos = pos
    //      - trackMode = GLOBAL
    //  - Else (trackMode == GLOBAL):
    //    - Calc BB = bounding box in the last n frames.
    //    - newFilterPos = pos
    //    - If size(BB) < size(maxBB):
    //      - trackMode = LOCAL
    //      - curBB = box with the same center as BB.
    //  - lastPos = pos
    //  - lastFilterPos = filterPos
    //  - filterPos = newFilterPos

    mHistory.push_back(Vector3f(v(0),v(1),v(2)));

    Vector3f newFilteredPos;

    if (mIsLocalTrack)
    {
      PVASSERT(mHistory.size() > 1);

      // Check if the position is inside the current local box.
      if (v(0) >= mMinLocalBox(0) && v(0) <= mMaxLocalBox(0) &&
          v(1) >= mMinLocalBox(1) && v(1) <= mMaxLocalBox(1) &&
          v(2) >= mMinLocalBox(2) && v(2) <= mMaxLocalBox(2))
      {
        const Vector3f& lastPos = mHistory[mHistory.size()-2];
        Vector3f deltaPos(v(0)-lastPos(0),v(1)-lastPos(1),v(2)-lastPos(2));
        deltaPos.mult(0.5f);
        newFilteredPos.add(mLastFilteredPos,deltaPos);
      }
      else
      {
        newFilteredPos.set(v(0),v(1),v(2));
        mIsLocalTrack = false;
      }
    }
    else
    {
      if (mHistory.size() == mHistory.capacity())
      {
        // Calculate current box for the last frames.
        Vector3f minBox = mHistory[0];
        Vector3f maxBox = mHistory[0];

        int i;
        for (i = 1; i < mHistory.size(); i++)
        {
          const Vector3f& p = mHistory[i];
          minBox.set((std::min)(minBox(0),p(0)),
                     (std::min)(minBox(1),p(1)),
                     (std::min)(minBox(2),p(2)));
          maxBox.set((std::max)(maxBox(0),p(0)),
                     (std::max)(maxBox(1),p(1)),
                     (std::max)(maxBox(2),p(2)));
        }

        // Check if this box is smaller than our minimum size.
        if (maxBox(0)-minBox(0) <= mBoxSize(0) &&
            maxBox(1)-minBox(1) <= mBoxSize(1) &&
            maxBox(2)-minBox(2) <= mBoxSize(2))
        {
          mIsLocalTrack = true;
          // Calculate the local tracking box around the center of this box.
          Vector3f c((minBox(0)+maxBox(0))*0.5f,
                      (minBox(1)+maxBox(1))*0.5f,
                      (minBox(2)+maxBox(2))*0.5f);
          mMinLocalBox.set(c(0)-mBoxSize(0)*0.5f,
                           c(1)-mBoxSize(1)*0.5f,
                           c(2)-mBoxSize(2)*0.5f);
          mMaxLocalBox.set(c(0)+mBoxSize(0)*0.5f,
                           c(1)+mBoxSize(1)*0.5f,
                           c(2)+mBoxSize(2)*0.5f);
        }
      }
      newFilteredPos.set(v(0),v(1),v(2));
    }

    mFilteredPosMatrix.resize(3);
    mFilteredPosMatrix(0) = newFilteredPos(0);
    mFilteredPosMatrix(1) = newFilteredPos(1);
    mFilteredPosMatrix(2) = newFilteredPos(2);

    mLastFilteredPos = newFilteredPos;

    v = mFilteredPosMatrix;
  }

  /**
   * Return the current state. Must call update() first.
   * @return last calculated value.
   */
  virtual const PvMatrixt &state(void) { return mFilteredPosMatrix; };

  /**
   * Reset.
   */
  virtual void reset() 
  {
    mIsLocalTrack = false;
    mHistory.clear(); 
  }


  //
  // ACCESS
  //

public:


  /**
   * @return w0 == 1 && w1 == 0 if it is in local tracking mode.
   * w0==0 && w1==1 otherwise.
   */
  virtual void getWeights(float* w0, float* w1) 
  { 
    if (mIsLocalTrack)
    {
      *w0 = 1; *w1 = 0; 
    }
    else
    {
      *w0 = 0; *w1 = 1; 
    }
  }

  //
  // INQUIRY
  //

public:


  //
  // ATTRIBUTES
  //

protected:

  /**
   * Current filtered position.
   */
  PvMatrixt mFilteredPosMatrix;

  /**
   * Box size.
   */
  Vector3f mBoxSize;

  /**
   * Current local tracking box.
   */
  Vector3f mMinLocalBox, mMaxLocalBox;

  /**
   * Flag that indicates if the tracking mode is local.
   */
  bool mIsLocalTrack;

  /**
   * Last position after filtering.
   */
  Vector3f mLastFilteredPos;

  /**
   * Buffer for the position history. These positions are before
   * filtering.
   */
  CircularBuffer<Vector3f> mHistory;

};

/// Smart pointer to SmoothBoxTrajectory
typedef boost::shared_ptr<SmoothBoxTrajectory> SmoothBoxTrajectoryPtr;

}; // namespace ait

#endif // SmoothBoxTrajectory_HPP


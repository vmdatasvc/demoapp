/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VisionMsg_HPP
#define VisionMsg_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

#include <ObjWithFlags.hpp>
#include <String.hpp>
#include <VisualSensorId.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

#include "VisionMsg_fwd.hpp"

namespace ait 
{

namespace vision
{

/**
 * Base class for all vision messages.
 * @par Responsibilities:
 * - Template for all vision messages.
 * - Contain end of frame messages.
 */
class VisionMsg : public ObjWithFlags
{
public:

  //
  // LIFETIME
  //

  /**
   * Constructor
   */
  VisionMsg(VisualSensorId vid, double timeStamp)
  {
    mVisualSensorId = vid;
    mTimeStamp = timeStamp;
  }

  /**
   * Destructor
   */
  virtual ~VisionMsg()
  {
  }

  /**
   * Clone
   */
  virtual VisionMsg *clone()
  {
    return NULL;
  }

  //
  // ACCESS
  //

public:

  /**
   * The visual sensor that generated this event.
   */
  void setVisualSensorId(const VisualSensorId id)
  {
    mVisualSensorId = id;
  }
  
  /**
   * The visual sensor that generated this event.
   */
  VisualSensorId getVisualSensorId() const
  {
    return mVisualSensorId;
  }
  
  /**
   * Return the time stamp of this event.
   */
  double getTimeStamp() const
  {
    return mTimeStamp;
  }

  /**
   * Set the time stamp for this event.
   * Usually the time stamp is obtained from the image acquire module.
   */
  void setTimeStamp(const double ts)
  {
    mTimeStamp = ts;
  }

  //
  // INQUIRY
  //

public:

  //
  // ATTRIBUTES
  //

public:

  enum
  {
    /**
     * This flag is used to notify asynchronous modules that all data
     * from a given frame has been transmitted. The asynchronous modules
     * must send back a message marked with this flag when they are done 
     * processing with the current frame.
     */
    END_OF_FRAME            = 0x00000001
  };

protected:

  /**
   * The visual sensor Id from where this message was obtained.
   */
  VisualSensorId mVisualSensorId;

  /**
   * Timestamp of this event. If not assigned it will default to 0.
   */
  double mTimeStamp;

};


}; // namespace vision

}; // namespace ait

#endif // VisionMsg_HPP



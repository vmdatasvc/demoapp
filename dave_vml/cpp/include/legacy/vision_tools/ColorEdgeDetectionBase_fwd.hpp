/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ColorEdgeDetectionBase_fwd_HPP
#define ColorEdgeDetectionBase_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class ColorEdgeDetectionBase;
typedef boost::shared_ptr<ColorEdgeDetectionBase> ColorEdgeDetectionBasePtr;

}; // namespace ait

#endif // ColorEdgeDetectionBase_fwd_HPP


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef SiftFeatureMatching_HPP
#define SiftFeatureMatching_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <vector>
#include <list>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>
#include <legacy/pv/ait/Image.hpp>
//#include <Settings.hpp>

// LOCAL INCLUDES
//
#include <legacy/pv/ait/Vector3.hpp>
#include <legacy/low_level/Settings.hpp>

// FORWARD REFERENCES
//
#include <boost/shared_ptr.hpp>
#include "legacy/vision_tools/SiftFeatureMatching_fwd.hpp"

class KeypointSt;

namespace ait 
{

namespace vision
{

  /*---------------------------- Structures --------------------------------*/

  
  /* Data structure for a simple keypoint for export.
  */
  struct SIFTkeypoint {
    unsigned int id;            /* identifier for keypoint */
    unsigned int matched_id;    /* identifier for matching keypoint */
    Vector3f p;                /* normalized location of keypoint in test image */
    Vector3f p_orig;           /* normalized location of keypoint in training image */
    float scale, ori;           /* Scale and orientation (range [-PI,PI]) */
  } ;


/**
 * This class is not yet documented.
 * Here is a more detailed description of the class. Next come the 
 * Responsibilities of the class. Try to minimize the responsibilities, this
 * will help in creating a good design. Next, optionally write any design patterns
 * that were used for this class. Refer to the "Design Patterns" book by Gamma et al.
 * @par Responsibilities:
 * - The First Responsibility
 * - The Second Responsibility:
 *   - A sub-responsibility
 *   - Another sub-responsibility
 * - The Third Responsibility
 * @par Patterns:
 * - The first design pattern
 * - The second design pattern
 * @par Design Considerations:
 * - Here you can itemize some justifications for your decisions. It will help
 * others to understand why your class is the way it is.
 * @remarks
 * Some special comments or warnings about this class.
 * @see TheirClass, OurClass (other related classes).
 */

class SiftFeatureMatching : private boost::noncopyable
{
  
  /* Data structure for a keypoint.  Lists of keypoints are linked
  by the "next" field.
  */
  class KeypointSt
  {
  public:

    struct match {boost::shared_ptr<KeypointSt> key; float dist;};

    unsigned int id;
    float row, col;             /* Subpixel location of keypoint. */
    float scale, ori;           /* Scale and orientation (range [-PI,PI]) */
    //int *descrip;               /* Vector of descriptor values */
    std::vector<int> descrip;
    boost::shared_ptr<KeypointSt> next;           /* Pointer to next keypoint in list. */
    
    std::list<match> bestMatchList;
    std::list<match>::iterator currentMatchItr;    
  };  


  /* Data structure for a float image.
  */
  struct ImageSt {
    int rows, cols;          /* Dimensions of image. */
    float **pixels;          /* 2D array of image pixels. */
    struct ImageSt *next;    /* Pointer to next image in sequence. */
  };
 
  typedef ImageSt *Image;

public:

  typedef boost::shared_ptr<KeypointSt> KeypointPtr;
//  typedef struct match {KeypointPtr key; float dist;};

  /**
   * Constructor.
   */
  SiftFeatureMatching();
  
  /**
   * Destructor
   */
  ~SiftFeatureMatching();

  /**
   * Initialization.
   */
  void init(std::string settingsPath);

	void loadTrainingPattern(std::string imageFilename="");


  //
  // OPERATIONS
  //

public:

  void matchFeatures(Image32& image, std::vector<SIFTkeypoint> &featurePointList, bool drawKeys=false);

  void matchFeatures(Image32& image1, Image32& image2, std::vector<SIFTkeypoint> &matchedFeaturePoints1, std::vector<SIFTkeypoint> &matchedFeaturePoints2, bool drawKeys);

  void findFeatures(Image32& image, std::vector<SIFTkeypoint> &featurePoints, bool drawKeys=false);

protected:

  void matchFeatures(Image32& image, std::list<KeypointPtr> &trainingPatternKeys, std::list<KeypointPtr> &featurePointMatchList, bool drawKeys);

  void matchFeatures(std::list<KeypointPtr> &testPatternKeys, std::list<KeypointPtr> &trainingPatternKeys, std::list<KeypointPtr> &featurePointMatchList);

  void FindMatches(Image8 &im1, KeypointPtr keys1, Image8 &im2, KeypointPtr keys2);

  KeypointPtr CheckForMatch(KeypointPtr key, std::list<KeypointPtr> &klist);

  void checkMatchConsistency(std::list<KeypointPtr> &list);

  int Dist(KeypointPtr k1, KeypointPtr k2);

  Image32& CombineImagesVertically(Image32 &im1, Image32 &im2);


  //
  // INQUIRY
  //

public:

  /**
   * Ask if this object has been initialized (the init() function has been called).
   */
  bool isInitialized() const { return mIsInitialized; }


private:

  float** AllocMatrix(int rows, int cols);

  void SkipComments(FILE *fp);  

  void loadFeatureKeys(const char* filename, std::list<KeypointPtr> &featureList);

  void ReadKeyFile(const char *filename, std::list<KeypointPtr> &featureList);
  void ReadKeys(FILE *fp, std::list<KeypointPtr> &featureList);  
  
  Image CreateImage(int rows, int cols);
  void ConvertPvRGBImage(Image32 &imgIn, Image imgOut);
  void ConvertImage(Image imgIn, Image32 &imgOut);

  Image ReadPGM(const char *filename);
  Image ReadPGM(FILE *fp);
  void WritePGM(const char *filename, Image image);
  void WritePGM(FILE *fp, Image image);
  
  void WritePGM(const char *filename, Image32 &image);
  void WritePGM(FILE *fp, Image32 &image);

  //void DrawLine(Image image, int r1, int c1, int r2, int c2);

  
 /**
  * Tells if this object has been initialized.
  */
  bool mIsInitialized;

/* whether we use PCA-SIFT (=1) or SIFT keys(=0) */
int PCAKEYS;

/* length of descriptor vector */
int DLEN;

  Settings mSettings;

  int mTrainingPatternImageWidth;
  int mTrainingPatternImageHeight;

  Image32 mTrainingPatternPvImage;
  Image mTrainingPatternImage;
  std::list<KeypointPtr> mTrainingPatternKeys;
  std::list<KeypointPtr> mTestPatternKeys;

  bool mMatchMethodIsNEAREST_ONLY;

  float mSIFT_MULT;
  float mPCA_MULT;

  int mSIFT_THRESH;
  int mPCA_THRESH;

  bool mDoConsistencyCheck;

};

}; // namespace vision

}; // namespace ait

#endif // SiftFeatureMatching_HPP

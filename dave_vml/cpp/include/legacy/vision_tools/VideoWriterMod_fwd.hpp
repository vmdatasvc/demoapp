/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VideoWriterMod_fwd_HPP
#define VideoWriterMod_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class VideoWriterMod;
typedef boost::shared_ptr<VideoWriterMod> VideoWriterModPtr;

class VideoWriterMsg;
typedef boost::shared_ptr<VideoWriterMsg> VideoWriterMsgPtr;

}; // namespace vision

}; // namespace ait

#endif // VideoWriterMod_fwd_HPP


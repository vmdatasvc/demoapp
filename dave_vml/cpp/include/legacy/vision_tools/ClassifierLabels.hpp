/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ClassifierLabels_HPP
#define ClassifierLabels_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>
#include <vector>

// AIT INCLUDES
//

#include <legacy/pv/PvUtil.hpp>
#include <legacy/types/String.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//


namespace ait 
{

/**
 * This is a label type. To get a string value, use the Classifier Labels.
 */
typedef int Label;

/**
 * This class is a container of strings that represent class labels.
 * This is to be used with the discrete classifiers.
 * @par Responsibilities:
 * - Assign a string label to each class.
 * - Define two special labels named Unknown and Unclassified.
 */
class ClassifierLabels : private boost::noncopyable
{
  //
  // LIFETIME
  //

public:

  /// Unknown and Unclassified labels. Other user defined labels start from 1.
  enum
  {
    UNCLASSIFIED = -1,
    UNKNOWN = 0
  };

  /**
   * Constructor.
   */
  ClassifierLabels()
  {
    mLabels.push_back("unclassified");
    mLabels.push_back("unknown");
  }

  /**
   * Destructor
   */
  virtual ~ClassifierLabels()
  {
  }

  //
  // OPERATIONS
  //

public:


  //
  // ACCESS
  //

  /**
   * Add a label to the group.
   * @return The new label integer value.
   */
  Label add(const std::string& label)
  {
    mLabels.push_back(label);
    return getNumLabels();
  }

  /**
   * Return the number of labels in this group, not counting Unknown and Unclassified.
   */
  int getNumLabels() const
  {
    return mLabels.size() - 2;
  }

  /**
   * Return the string that represents the given label.
   */
  const std::string& getLabelString(Label label) const
  {
    if (label >= getNumLabels()+1 || label < UNCLASSIFIED)
    {
      PvUtil::exitError("Label is out of range!");
    }
    return mLabels[label+1];
  }

  /**
   * Return the label corresponding to the given string.
   */
  Label getStringLabel(const std::string& labelName) const
  {
    std::vector<std::string>::const_iterator i;

    i = std::find(mLabels.begin(),mLabels.end(),labelName);
    if (i == mLabels.end())
    {
      PvUtil::exitError("Label name not found");
    }

    return i - mLabels.begin() - 1;
  }

public:


  //
  // INQUIRY
  //

public:


  //
  // ATTRIBUTES
  //

protected:

  /**
   * Vector of strings that holds the labels.
   */
  std::vector<std::string> mLabels;

};

/// Smart pointer to ClassifierLabels
typedef boost::shared_ptr<ClassifierLabels> ClassifierLabelsPtr;
typedef boost::shared_ptr<const ClassifierLabels> ClassifierLabelsCPtr;

}; // namespace ait

#endif // ClassifierLabels_HPP


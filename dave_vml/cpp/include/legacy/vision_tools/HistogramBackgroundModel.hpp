/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef HistogramBackgroundModel_HPP
#define HistogramBackgroundModel_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <vector>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>
#include <legacy/pv/ait/Image.hpp>
#include <legacy/pv/ait/Image_fwd.hpp>

// LOCAL INCLUDES
//

#include "legacy/vision_tools/BackgroundModelBase.hpp"

// FORWARD REFERENCES
//

#include "legacy/vision_tools/HistogramBackgroundModel_fwd.hpp"

namespace ait 
{


/**
 * This class is not yet documented.
 * Here is a more detailed description of the class. Next come the 
 * Responsibilities of the class. Try to minimize the responsibilities, this
 * will help in creating a good design. Next, optionally write any design patterns
 * that were used for this class. Refer to the "Design Patterns" book by Gamma et al.
 * @par Responsibilities:
 * - The First Responsibility
 * - The Second Responsibility:
 *   - A sub-responsibility
 *   - Another sub-responsibility
 * - The Third Responsibility
 * @par Sample Usage:
 * @code
 *  using namespace ait;
 *  
 *  VideoReaderAVI vreader;
 *  BackgroundModelBasePtr pModel(new HistogramBackgroundModel());
 *  pModel->init("MyTest/BackgroundModel");
 *
 *  Uint w,h;
 *  vreader.init(w,h,"test.avi");
 *
 *  Image32Ptr pFrame(new Image32(w,h));
 *  Image8Ptr pGrayFrame(new Image8(w,h));
 *
 *  for (int frameNum = 0; frameNum < 10000; frameNum+= 300)
 *  {
 *    vreader.setCurrentFrame(frameNum);
 *    vreader.readFrame(*pFrame);
 *
 *    ColorConvert::convert(*pFrame,PvImageProc::RGB_SPACE,*pGrayFrame,PvImageProc::GRAY_SPACE);
 *
 *    pModel->update(pGrayFrame);
 *  }
 *
 *  vreader.finish();
 *
 *  pModel->validate();
 *
 *  Image8Ptr pForeground(new Image8(w,h));
 *  pModel->foregroundSegment(pGrayFrame,pForeground);
 *
 *  pForeground->save("segmented.pgm");
 * @endcode
 */
class HistogramBackgroundModel : public BackgroundModelBase
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  HistogramBackgroundModel();

  /**
   * Virtual destructor.
   */
  virtual ~HistogramBackgroundModel();

  /**
   * Initialization.
   */
  virtual void init(const std::string& settingsPath);

  //
  // OPERATIONS
  //

public:

  /**
   * Pixel-wise Background modeling. This operation doesn't
   * perform foreground segmentation.
   */
  virtual void  update(const Image8Ptr pInputImage);

	/**
   *	Segmentation of the foreground pixels from the background model
   */
	virtual void foregroundSegment(const Image8Ptr pInputImage, 
    Image8Ptr pForegroundImage);

  /**
   * Calculate the thresholds for the model.
   */
  virtual void validate();

  /**
   * Visualization.
   */
  void visualize();

  /**
   * Save the background model to the specified location
   */
  virtual void saveModel(std::string filename);

  /**
   * Loads the background model from the specified location
   */
  virtual Vector2i loadModel(std::string filename);

  /**
   * Draw the highest of the two thresholds on the image
   */
  virtual void drawMaxThresh(Image8& img) const;

protected:

	/**
   *	Initialize the background model
   */
  virtual void  initialize(const Image8Ptr pInputImage);

  /**
   * Build the two thresholds for the given histogram. The histogram might be
   * modified after this operation.
   */
  void buildThresholds(Uint16 *histogram, 
    Uint8& min0, Uint8& max0, 
    Uint8& min1, Uint8& max1);

  /**
   * Get the histogram for pixel x,y.
   */
  inline Uint16* getHistogram(const int x, const int y)
  {
    int i = ((y*mImageSize.x+x)*mNumHistogramBins);
    if (i >= mHistograms.size())
    {
      return NULL;
    }
    return mHistograms.pointer()+i;
  }

  //
  // ACCESS
  //

public:

  /**
   * Get the number of frames that this model has
   * used to learn this background.
   */
  virtual int getNumFrames()
  {
    return mNumFrames;
  }

  //
  // INQUIRY
  //

public:

  /**
   * Indicate if this model has been initialized.
   */
  virtual bool isInitialized()
  {
    return mModelInitialized;
  };

  //
  // ATTRIBUTES
  //

protected:

  /**
   * Store all histograms in a single matrix.
   * This will provide better memory management and
   * cache coherence. Each histogram is organized
   * per row.
   */
  Matrix<Uint16> mHistograms;

  /**
   * Thresholds per pixel.
   */
  class Thresholds
  {
  public:
    Uint8 min0;
    Uint8 max0;
    Uint8 min1;
    Uint8 max1;
  };

  /**
   * Min thresholds for each model.
   */
  Image<Thresholds> mThresholds;

  /**
   * Number of frames used to learn this background.
   */
  int mNumFrames;

  /**
   * Number of bins for each pixel histogram.
   */
  int mNumHistogramBins;

  /**
   * Visualization flag.
   */
  bool mSaveVisualization;

  /**
   * Store the input image size.
   */
  Vector2i mImageSize;

  /**
   * Margin used to expand the thresholds of the background
   * model. This margin is the difference of intensity in
   * the pixel, and its value is from 1 to 255. If it is
   * zero, it will become 1.
   */
  int mHistogramMargin;

  /**
   * Flag that indicates if this model has been initialized.
   */
  bool mModelInitialized;

  bool mUseOneCluster;
};


}; // namespace ait

#endif // HistogramBackgroundModel_HPP


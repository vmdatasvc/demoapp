/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef MotionDetectFrameDiff_fwd_HPP
#define MotionDetectFrameDiff_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class MotionDetectFrameDiff;
typedef boost::shared_ptr<MotionDetectFrameDiff> MotionDetectFrameDiffPtr;

}; // namespace ait

#endif // MotionDetectFrameDiff_fwd_HPP


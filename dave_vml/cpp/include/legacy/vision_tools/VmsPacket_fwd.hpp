/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VmsPacket_fwd_HPP
#define VmsPacket_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class VmsPacket;
typedef boost::shared_ptr<VmsPacket> VmsPacketPtr;

}; // namespace ait

#endif // VmsPacket_fwd_HPP


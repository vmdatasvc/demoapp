/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef COLORRANGE_HPP
#define COLORRANGE_HPP

#include <legacy/pv/ait/Image.hpp>

namespace ait
{

class ColorRange
{
public:
  
  enum schemeConst { ICE, FIRE, JOY, SCHEMES };

protected:
  schemeConst scheme;
  Image32 range;

  void initRanges(void);

public:
  ColorRange(schemeConst s) : range(SCHEMES,256*3), scheme(s)
  {
    initRanges();
  }


  unsigned int operator()(unsigned int i) { return range(scheme,i); };
};

} // namespace ait

#endif

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef MotionDetectMultiFrame_HPP
#define MotionDetectMultiFrame_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>

// LOCAL INCLUDES
//

#include "legacy/vision_tools/MotionDetect.hpp"

// FORWARD REFERENCES
//

#include "legacy/vision_tools/MotionDetectMultiFrame_fwd.hpp"

namespace ait 
{


/**
 * Detect motion energy per pixel in a sequence of images.
 */
class MotionDetectMultiFrame : public MotionDetect
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  MotionDetectMultiFrame();

  /**
   * Destructor
   */
  virtual ~MotionDetectMultiFrame();

  /**
   * Initialize
   * @param nFrames [in] Number of frames to learn the background motion model.
   */
  void init(int nFrames)
  {
    mNumFrames = nFrames;
    reset();
  }

  //
  // OPERATIONS
  //

public:

  /**
   * Update the motion information.
   * @param inputImage [in] New image to be added to the model.
   */
  virtual void update(const Image8& inputImage);

  /**
   * Reset the motion model.
   */
  virtual void reset();

  //
  // ACCESS
  //

public:

  /**
   * Get the motion energy image.
   */
  virtual const Image8& getMotionEnergy() { return mMotionEnergy; }

  //
  // INQUIRY
  //

public:

  /**
   * Return true if there is motion in the current image.
   */
  virtual bool isMoving() { PvUtil::exitError("Not Implemented!"); return false; }

  //
  // ATTRIBUTES
  //

protected:

  typedef boost::shared_ptr< Image<float> > VarImagePtr;

  /**
   * The number of frames in the background model.
   */
  int mNumFrames;

  /**
   * The accumulative sum of values.
   */
  Image<Int32> mSum;

  /**
   * The accumulative sum of variances.
   */
  Image<float> mVarSum;

  /**
   * The motion energy for this frame.
   */
  Image8 mMotionEnergy;

  /**
   * The buffer of images.
   */
  std::list<Image8Ptr> mImageBuffer;

  /**
   * This buffer stores the variances.
   */
  std::list<VarImagePtr> mVarBuffer;

};


}; // namespace ait

#endif // MotionDetectMultiFrame_HPP


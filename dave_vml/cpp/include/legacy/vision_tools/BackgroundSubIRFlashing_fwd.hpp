/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef BackgroundSubIRFlashing_fwd_HPP
#define BackgroundSubIRFlashing_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class BackgroundSubIRFlashing;
typedef boost::shared_ptr<BackgroundSubIRFlashing> BackgroundSubIRFlashingPtr;

}; // namespace vision

}; // namespace ait

#endif // BackgroundSubIRFlashing_fwd_HPP


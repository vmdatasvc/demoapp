/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VisualSensorInfo_HPP
#define VisualSensorInfo_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>

// AIT INCLUDES
//

#include <String.hpp>
#include <VisualSensorId.hpp>

// LOCAL INCLUDES
//

#include "PersonComponent.hpp"

// FORWARD REFERENCES
//

#include "Person_fwd.hpp"

namespace ait 
{

/**
 * Contain information about the visual sensor (i.e. camera) that the person was detected by.
 * @par Responsibilities:
 * - Know the visual sensor id.
 * @remarks
 * The visual sensor id is a key to a table in the central database where the location of the visual sensor is stored.
 * By utilizing this information our applications can determine where a person was detected.
 * Future work in developing similarity metrics for a person, can allow us to track a person's movements from one
 * visual sensor to another without the field of views overlapping.
 */
class VisualSensorInfo : public PersonComponent
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  VisualSensorInfo()
  {
  }

  /**
   * Destructor
   */
  virtual ~VisualSensorInfo()
  {
  }

  /**
   * Clone operator.
   */
  virtual PersonComponent* clone()
  {
    VisualSensorInfo *p = new VisualSensorInfo();
    *p = *this;
    return p;
  }

  //
  // OPERATIONS
  //

public:

  /**
   * Copy operator.
   * Only the reference to the pointers will be copied, not the instances.
   */
  VisualSensorInfo& operator=(const VisualSensorInfo& from)
  {
    if (this != &from)
    {
      mId = from.mId;
    }
    return *this;
  }

  //
  // ACCESS
  //

public:

  /**
   * Return the PersonComponent type identificator.
   * @see PersonComponent::getNewTypeId()
   */
  static const Uint getTypeId()
  {
    return mTypeId;
  }

  /**
   * Return the PersonComponent type identificator.
   * @remark This virtual method calls the class static method getTypeId()
   */
  virtual const Uint getObjectTypeId()
  {
    return VisualSensorInfo::getTypeId();
  }

  /**
   * Get the total visual sensor id that the person was detected by.
   */
  VisualSensorId getId()
  {
    return mId;
  }

  /**
   * Set the visual sensor id.
   */
  void setId(VisualSensorId id)
  {
    mId = id;
  }

public:


  //
  // INQUIRY
  //

public:

  //
  // ATTRIBUTES
  //

protected:

  /**
   * PersonComponent type id. It is registered with PersonComponent::getNewId()
   */
  static Uint mTypeId;

  /**
   * Visual sensor id.
   */
  VisualSensorId mId;



};

/// Smart pointer to LifeInfo
typedef boost::shared_ptr<VisualSensorInfo> VisualSensorInfoPtr;

}; // namespace ait

#endif // VisualSensorInfo_HPP


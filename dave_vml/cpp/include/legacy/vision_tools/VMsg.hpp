/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VMsg_HPP
#define VMsg_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>

// AIT INCLUDES
//

#include <String.hpp>
#include <VisualSensorId.hpp>
#include <ait/Vector3.hpp>
#include <ait/Rectangle.hpp>
// LOCAL INCLUDES
//

#include "VisionMsg.hpp"

// FORWARD REFERENCES
//


namespace ait 
{


/**
 * This is a message for the application.
 */
class VMsg : public vision::VisionMsg
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  VMsg(VisualSensorId vid = VisualSensorId(), double timeStamp=0) : VisionMsg(vid,timeStamp) {};

  /**
   * Destructor
   */
  virtual ~VMsg() {};

  //
  // OPERATORS
  //

public:

  //
  // OPERATIONS
  //

public:


  //
  // ACCESS
  //

public:

  void setParameter(const std::string& paramLabel, float value)
  {
	  std::map<std::string, float>::iterator itr;

	  itr = ((mParameters.insert(std::map<std::string, float>::value_type(paramLabel,0))).first);	  
	  
	  itr->second = value;
  }

  float getParameter(const std::string& paramLabel)
  {
	  std::map<std::string, float>::iterator itr;
	  
	  itr = ((mParameters.insert(std::map<std::string, float>::value_type(paramLabel,0))).first);
	  
	  return itr->second;
  }

  //
  // INQUIRY
  //

public:


  //
  // ATTRIBUTES
  //

public:

  enum
  {
    /**
     * A video has reached the end, and started looping.
     */
    VIDEO_END_FRAME         = 0x00000020,

    /**
     * Application is exiting. All modules should have finished at this point.
     */
    EXIT_APPLICATION        = 0x00000200
  };

protected:

  /**
   * Generic parameters.
   */
  std::map<std::string, float> mParameters;

};

/// Smart pointer to VMsg
typedef boost::shared_ptr<VMsg> VMsgPtr;

}; // namespace ait

#endif // VMsg_HPP


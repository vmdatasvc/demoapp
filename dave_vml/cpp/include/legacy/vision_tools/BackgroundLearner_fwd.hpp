/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef BackgroundLearner_fwd_HPP
#define BackgroundLearner_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class BackgroundLearner;
typedef boost::shared_ptr<BackgroundLearner> BackgroundLearnerPtr;

}; // namespace ait

#endif // BackgroundLearner_fwd_HPP


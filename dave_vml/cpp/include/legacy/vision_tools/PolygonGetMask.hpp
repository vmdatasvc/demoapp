#ifndef PolygonGetMask_HPP
#define PolygonGetMask_HPP

// SYSTEM INCLUDES
//

#include <vector>

// AIT INCLUDES
//

#include <legacy/pv/ait/Image.hpp>
#include <legacy/pv/ait/Vector3.hpp>
#include <legacy/pv/ait/Rectangle.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

//#include "PolygonGetMask_fwd.hpp" //need for just a function?

namespace ait
{
namespace vision
{

/**
 *	getMask receives a list of pixel coordinates and a region of interest,
 *  and draws a polygon mask on the given image, which is resized to match
 *. The region of interest. Pixels inside the polygon are white, and pixels
 *  outside the polygon are black.
 *
 *  @par Responsibilities:
 *  - produce a proper bitmask given the coordinates and region of interest
 *  - handles both complex (lines cross) and simple (convex/concave) polygons
 *
 *  @param pixels A series of points in pixel coordinates which form the 
 *  vertices of the polygon. The polygon is formed by connecting successive 
 *  vertices. NOTE: the last vertex is automatically connected to the first
 *  @roi The region of interest. Only the parts of the polygon that reside
 *  within roi will be considered for the bitmask, which is resized
 *  to match the dimensions of roi. If no coordinates are given, nothing will
 *  be done. A single point will be plotted for one coordinate, and a line
 *  will be drawn when two coordinates are given.
 *  @img The image object which the bitmask will be written to. The image is
 *  resized to match the region of interest (roi)
 *
 *  @remarks Since we are dealing with pixel coordinates, the origin is 
 *  assumed to be at the top left. Also, the behavior for coinciding points 
 *  which immediately follow each other in the pixels vector has not been 
 *  tested.
 */
void getMask(
  const std::vector<Vector2f>& pixels,
  const Rectanglef& roi,
  Image8& img);

}; // namespace vision

}; //namespace ait

#endif //PolygonGetMask_HPP


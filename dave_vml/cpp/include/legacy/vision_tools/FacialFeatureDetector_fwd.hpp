/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef FacialFeatureDetector_fwd_HPP
#define FacialFeatureDetector_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class FacialFeatureDetector;
typedef boost::shared_ptr<FacialFeatureDetector> FacialFeatureDetectorPtr;

}; // namespace ait

#endif // FacialFeatureDetector_fwd_HPP


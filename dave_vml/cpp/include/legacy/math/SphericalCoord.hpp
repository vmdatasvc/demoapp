/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef SphericalCoord_HPP
#define SphericalCoord_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>
#include <math.h>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>
#include <legacy/pv/ait/Vector3.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//


namespace ait 
{


/**
 * Transform to and from a cubical volume to a bended cube.
 * See interactive_volume_mapping.doc for documentation on what 
 * this class does.
 */
template<class T> class SphericalCoord
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  SphericalCoord() : mIsInitialized(false) {};

  /**
   * Destructor
   */
  virtual ~SphericalCoord() {};

  /**
   * Initialization.
   */
  void init(const Vector3<T>& sphereCenter,
            const Vector3<T>& cuboidMin,
            const Vector3<T>& cuboidMax)
  {
    PVASSERT(cuboidMin(0) < cuboidMax(0) && 
             cuboidMin(1) < cuboidMax(1) &&
             cuboidMin(2) < cuboidMax(2));
    mSphereCenter = sphereCenter;
    mCuboidMin = cuboidMin;
    mCuboidMax = cuboidMax;

    Vector3<T> pivot0(mCuboidMax);
    pivot0.sub(mSphereCenter);
    mSphereMin = cartesianToSpherical(pivot0);

    Vector3<T> pivot1(mCuboidMin(0),mCuboidMin(1),mCuboidMax(2));
    pivot1.sub(mSphereCenter);
    mSphereMax = cartesianToSpherical(pivot1);

    mSphereMax(0) = mSphereMin(0)+mCuboidMax(2) - mCuboidMin(2);

    pivot0 = mCuboidMax;
    pivot1.set(mCuboidMin(0),mCuboidMin(1),mCuboidMax(2));
    Vector3<T> test0 = cartesianToNormSpherical(pivot0);
    Vector3<T> test00 = normSphericalToCartesian(test0);
    Vector3<T> test1 = cartesianToNormSpherical(pivot1);
    Vector3<T> test11 = cartesianToNormSpherical(test1);

    mIsInitialized = true;
  }

  //
  // OPERATIONS
  //

public:

  /**
   * Transform from cartesian (x,y,z) to spherical (r,theta,phi) 
   * coordinate system, applying the axis interchange described in the
   * documentation to avoid singularities.
   */
  static Vector3<T> cartesianToSpherical(const Vector3<T>& P)
  {
    const T r = sqrt(P(0)*P(0) + P(1)*P(1) + P(2)*P(2));
    return Vector3<T>(r,acos(P(1)/r),atan2(-P(2),P(0)));
  }

  /**
   * Transform from spherical (r,theta,phi) to cartesian (x,y,z) 
   * coordinate system.
   */
  static Vector3<T>  sphericalToCartesian(const Vector3<T>& S)
  {
    const T sinTheta = sin(S(1));
    const T cosTheta = cos(S(1));
    const T sinPhi = sin(S(2));
    const T cosPhi = cos(S(2));
    return Vector3<T>(
      S(0)*sinTheta*cosPhi,
      S(0)*cosTheta,
      -S(0)*sinTheta*sinPhi);
  }

  /**
   * Convert from cartesian to normalized spherical.
   */
  Vector3<T> cartesianToNormSpherical(const Vector3<T>& P)
  {
    // Translate point to put the sphere center in the origin.
    Vector3<T> P1(P(0)-mSphereCenter(0),
                   P(1)-mSphereCenter(1),
                   P(2)-mSphereCenter(2));

    // Convert to spherical coords.
    Vector3<T> S1 = cartesianToSpherical(P1);

    // Normalize and return
    return Vector3<T>(1-(S1(2)-mSphereMin(2))/(mSphereMax(2)-mSphereMin(2)),
                       (S1(1)-mSphereMin(1))/(mSphereMax(1)-mSphereMin(1)),
                       1-(S1(0)-mSphereMin(0))/(mSphereMax(0)-mSphereMin(0)));
  }

  /**
   * Convert from normalized spherical to cartesian.
   */
  Vector3<T> normSphericalToCartesian(const Vector3<T>& S)
  {
    // De-normalize.
    Vector3<T> S1(mSphereMin(0)+(1-S(2))*(mSphereMax(0)-mSphereMin(0)),
                   mSphereMin(1)+S(1)*(mSphereMax(1)-mSphereMin(1)),
                   mSphereMin(2)+(1-S(0))*(mSphereMax(2)-mSphereMin(2)));

    // Convert to cartesian coords.
    Vector3<T> P1(sphericalToCartesian(S1));

    // Translate to the original position
    return Vector3<T>(P1(0)+mSphereCenter(0),
                       P1(1)+mSphereCenter(1),
                       P1(2)+mSphereCenter(2));
  }
  
  //
  // ACCESS
  //

public:


  //
  // INQUIRY
  //

public:

  /**
   * Ask if this object has been initialized (the init() function has been called).
   */
  Bool isInitialized() const { return mIsInitialized; }


  //
  // ATTRIBUTES
  //

protected:

  /**
   * Extent of the 3D cuboid in cartesian coordinates. V0,V1 in the documentation.
   */
  Vector3<T> mCuboidMin, mCuboidMax;

  /**
   * Center of the sphere in cartesian coordinates. C in the documentation.
   */
  Vector3<T> mSphereCenter;

  /**
   * Extent of the bended cuboid in spherical coordinates. S0,S1 in the 
   * documentation.
   */
  Vector3<T> mSphereMin, mSphereMax;

  bool mIsInitialized;

};

}; // namespace ait

#endif // SphericalCoord_HPP


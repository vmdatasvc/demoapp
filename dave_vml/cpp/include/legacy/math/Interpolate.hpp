/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef Interpolate_HPP
#define Interpolate_HPP

// SYSTEM INCLUDES
//

#include <malloc.h>

//#define USE_EXTERNAL_BOOST_METHOD
#ifdef USE_EXTERNAL_BOOST_METHOD
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include "ludecomposition.h"
#endif

#include <float.h>
#include <vector>
#include <cmath>

// AIT INCLUDES
//
#include <legacy/pv/ait/Matrix.hpp>
#include <legacy/pv/ait/Vector3.hpp>
#include "legacy/pv/PvUtil.hpp"

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//


namespace ait 
{


/**
 * This class is not yet documented.
 * Here is a more detailed description of the class. Next come the 
 * Responsibilities of the class. Try to minimize the responsibilities, this
 * will help in creating a good design. Next, optionally write any design patterns
 * that were used for this class. Refer to the "Design Patterns" book by Gamma et al.
 * @par Responsibilities:
 * - The First Responsibility
 * - The Second Responsibility:
 *   - A sub-responsibility
 *   - Another sub-responsibility
 * - The Third Responsibility
 * @par Patterns:
 * - The first design pattern
 * - The second design pattern
 * @par Design Considerations:
 * - Here you can itemize some justifications for your decisions. It will help
 * others to understand why your class is the way it is.
 * @remarks
 * Some special comments or warnings about this class.
 * @see TheirClass, OurClass (other related classes).
 */
//
//static double tps_base_func(double r)
//{
//  if ( r == 0.0 )
//    return 0.0;
//  else
//    return r*r * log(r);
//}
//
//
//static double tps_base_func2(double r)
//{
//  if ( r == 0.0 )
//    return 0.0;
//  else
//    return r;
//}

template<class T> class Interpolate
{

  
static double tps_base_func(double r)
{
  if ( r == 0.0 )
    return 0.0;
  else
    return r*r * log(r);
}


static double tps_base_func2(double r)
{
  if ( r == 0.0 )
    return 0.0;
  else
    return r;
}

  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  Interpolate()
  {};

  /**
   * Destructor
   */
  virtual ~Interpolate()
  {
  };

  /**
   * Initialization for a 2D mesh interpolation.
   *
   * X and Y specify the 2d mesh grid points (has to be equally spaced)
   * Z represent the data values for the given grid points 
   * Grid points for which no data values are available, an entry of NaN can be provided in Z
   *
   * Interpolation Method
   * Note: currently only "nearest" neighbor or "linear" interpolation schema is implemented
   *
   * Possible methods:
   * 'nearest': chooses the closest data point on provided grid
   * 'linear':  performs a linear interpolation between a set of three 2D data points
   */
  void init(Matrix<T> &X, Matrix<T> &Y, Matrix<T> &Z, char* method)
  {
	  PVASSERT(X.rows()>0 && X.cols()>1);
	  PVASSERT(Y.rows()>0 && Y.cols()>1);
	 
        mX = X;
	    mY = Y;
	    mZ = Z;
     	  
	  if(strcmp(method,"nearest")==0)
		  mMethod = NN;
	  else
		  if(strcmp(method,"linear")==0)
			  mMethod = LINEAR;
		  else
			  PvUtil::exitError("Interpolate::init(): only 'nearest' and  'linear' interpolation method supported!");
		  
		  mMaxX = mX.maximum();
		  mMinX = mX.minimum();
		  
		  mMaxY = mY.maximum();
		  mMinY = mY.minimum();
		  
		  mNumGridStepsX = X.cols()-1;
		  mNumGridStepsY = Y.rows()-1;
		  
		  mGridStepsPerUnitX = ((double)mNumGridStepsX)/(mMaxX-mMinX);
		  mGridStepsPerUnitY = ((double)mNumGridStepsY)/(mMaxY-mMinY);
		  
		  mIsInitialized = true;
  };
 
  /**
   * Initialization for a 2D mesh interpolation.
   *
   * controlPoints specifies a list of control points through which a surface is fitted, then
   * a grid with uniformly distributed control points is obtained through interpolation of this surface.
   * The number of grid lines is given by gridStepsX and gridStepsY
   *
   * Interpolation Method
   * Note: currently only "nearest" neighbor or "linear" interpolation schema is implemented
   *
   * Possible methods:
   * 'nearest': chooses the closest data point on provided grid
   * 'linear':  performs a linear interpolation between a set of three 2D data points
   */

  void init(std::vector<Vector3d>& controlPoints, int gridWidth, int gridHeight, char* method, double reg=0.0)
  {
    double be = griddata(controlPoints, mX, mY, mZ, gridWidth, gridHeight, reg);
    PVMSG("Bending Energy = %f\n", be);

    mX.save("mX.txt", "%f ");
    mY.save("mY.txt", "%f ");
    mZ.save("mZ.txt", "%f ");

    if(strcmp(method,"nearest")==0)
      mMethod = NN;
    else
      if(strcmp(method,"linear")==0)
        mMethod = LINEAR;
      else
        //PvUtil::exitError("Interpolate::init(): only 'nearest' and  'linear' interpolation method supported!");
        PVMSG("Interpolate::init(): only 'nearest' and  'linear' interpolation method supported!");

    mMaxX = mX.maximum();
    mMinX = mX.minimum();

    mMaxY = mY.maximum();
    mMinY = mY.minimum();

    mNumGridStepsX = mX.cols()-1;
    mNumGridStepsY = mY.rows()-1;

    mGridStepsPerUnitX = mNumGridStepsX/(mMaxX-mMinX);
    mGridStepsPerUnitY = mNumGridStepsY/(mMaxY-mMinY);

    //test
    T z;
    Matrix<T> tZ(mZ);
    double err=0.0;
    for(int i=0;i<mX.rows();i++)
      for(int j=0;j<mX.cols();j++)
      {
        getInterpolatedValue(mX(i,j), mY(i,j), z);
        err+=fabs(mZ(i,j)-z);
        tZ(i,j)=z;
      }
    tZ.save("mZ.txt", "%f ");
    PVMSG("Test error = %f\n", err);

    mIsInitialized = true;
  };

  
/*
 *  Calculate Thin Plate Spline (TPS) weights from
 *  control points and build a new height grid by
 *  interpolating with them.
 */
static
double 
griddata(std::vector<Vector3d>& controlPoints, Matrix<T> &Xout, Matrix<T> &Yout, Matrix<T> &Zout, int gridWidth, int gridHeight, double regularization)
{
   // You We need at least 3 points to define a plane
  PVASSERT(controlPoints.size()>2);

  Vector3d s(controlPoints[0]);
  Vector3d pt_min(s), pt_max(s);
  for(int i=0;i<controlPoints.size();i++)
  {
    Vector3d pt(controlPoints[i]);    
    if(pt.x<pt_min.x) pt_min.x=pt.x;
    if(pt.y<pt_min.y) pt_min.y=pt.y;
    if(pt.z<pt_min.z) pt_min.z=pt.z;
    if(pt.x>pt_max.x) pt_max.x=pt.x;
    if(pt.y>pt_max.y) pt_max.y=pt.y;
    if(pt.z>pt_max.z) pt_max.z=pt.z;
  }

  double dx=pt_max.x-pt_min.x;
  double dz=pt_max.z-pt_min.z;  
  pt_min.x-=dx/gridWidth;
  pt_max.x+=dx/gridWidth;
  pt_max.x+=dx/gridWidth;
  pt_min.z-=dz/gridHeight;
  pt_max.z+=dz/gridHeight;
  pt_max.z+=dz/gridHeight;
  

  unsigned p = controlPoints.size();

  // Allocate the matrix and vector
#ifdef USE_EXTERNAL_BOOST_METHOD
  boost::numeric::ublas::matrix<double> mtx_l(p+3, p+3);
  boost::numeric::ublas::matrix<double> mtx_v(p+3, 1);
  boost::numeric::ublas::matrix<double> mtx_orig_k(p, p);
#else
  Matrix<double> mtx_l(p+3, p+3);
  Matrix<double> mtx_v(p+3, 1);
  Matrix<double> mtx_orig_k(p, p);
#endif

  // Fill K (p x p, upper left of L) and calculate
  // mean edge length from control points
  //
  // K is symmetrical so we really have to
  // calculate only about half of the coefficients.
  double a = 0.0;
  for ( unsigned i=0; i<p; ++i )
  {
    for ( unsigned j=i+1; j<p; ++j )
    {
      Vector3d pt_i = controlPoints[i];
      Vector3d pt_j = controlPoints[j];
      pt_i.y = pt_j.y = 0;
      pt_i.sub(pt_j);
      double elen = pt_i.norm();
      mtx_l(i,j) = mtx_l(j,i) =
        mtx_orig_k(i,j) = mtx_orig_k(j,i) =
          tps_base_func(elen);
      a += elen * 2; // same for upper & lower tri
    }
  }
  a /= (double)(p*p);

  // Fill the rest of L
  for ( unsigned i=0; i<p; ++i )
  {
    // diagonal: reqularization parameters (lambda * a^2)
    mtx_l(i,i) = mtx_orig_k(i,i) =
      regularization * (a*a);

    // P (p x 3, upper right)
    mtx_l(i, p+0) = 1.0;
    mtx_l(i, p+1) = controlPoints[i].x;
    mtx_l(i, p+2) = controlPoints[i].z;

    // P transposed (3 x p, bottom left)
    mtx_l(p+0, i) = 1.0;
    mtx_l(p+1, i) = controlPoints[i].x;
    mtx_l(p+2, i) = controlPoints[i].z;
  }
  // O (3 x 3, lower right)
  for ( unsigned i=p; i<p+3; ++i )
    for ( unsigned j=p; j<p+3; ++j )
      mtx_l(i,j) = 0.0;


  // Fill the right hand vector V
  for ( unsigned i=0; i<p; ++i )
    mtx_v(i,0) = controlPoints[i].y;
  mtx_v(p+0, 0) = mtx_v(p+1, 0) = mtx_v(p+2, 0) = 0.0;

  PVMSG("Solve Linear System\n");

  // Solve the linear system "inplace"
#ifdef USE_EXTERNAL_BOOST_METHOD
  if (0 != LU_Solve(mtx_l, mtx_v))      
#else
  if (0 != Solve(mtx_l, mtx_v))      
#endif
  {
    PvUtil::exitError( "Interpolate:: Singular matrix! Aborting." );    
  }

  PVMSG("Interpolate grid heights\n");

  // Interpolate grid heights
  Xout.resize(gridWidth, gridHeight);
  Yout.resize(gridWidth, gridHeight);
  Zout.resize(gridWidth, gridHeight);
  double fw = 1.0/gridWidth;
  double fh = 1.0/gridHeight;
  double wx = pt_max.x-pt_min.x;
  double hz = pt_max.z-pt_min.z;
  
  for ( int x=0; x<gridWidth; ++x )
  {
    for ( int z=0; z<gridHeight; ++z )
    {      
      double x1 = pt_min.x+(fw*x)*wx;
      double z1 = pt_min.z+(fh*z)*hz;

      double h = mtx_v(p+0, 0) + mtx_v(p+1, 0)*x1 + mtx_v(p+2, 0)*z1;
      Vector3d pt_i, pt_cur(x1, 0, z1);
      for ( unsigned i=0; i<p; ++i )
      {
        pt_i = controlPoints[i];
        pt_i.y = 0;
        pt_i.sub(pt_cur);
        double elen = pt_i.norm();
        h += mtx_v(i,0) * tps_base_func(elen);
      }
      Xout(x,z) = pt_cur.x;
      Yout(x,z) = pt_cur.z;
      Zout(x,z) = h;
    }
  }

  // Calc bending energy
#ifdef USE_EXTERNAL_BOOST_METHOD
  boost::numeric::ublas::matrix<double> t( p, 1 );
  for ( int i=0; i<p; ++i )
    t(i,0) = mtx_v(i,0);
  boost::numeric::ublas::matrix<double> be = boost::numeric::ublas::prod( boost::numeric::ublas::prod<boost::numeric::ublas::matrix<double> >( trans(t), mtx_orig_k ), t );
  return be(0,0);
#endif

  return 0.0;
}

// Solve a linear equation system a*x=b using inplace LU decomposition.
//
// Stores x in 'b' and overwrites 'a' (with a pivotted LUD).
//
// Matrix 'b' may have any (>0) number of columns but
// must contain as many rows as 'a'.
//
// Possible return values:
//  0=success
//  1=singular matrix
//  2=a.rows != b.rows
//  3=b.cols != 1
static int Solve(Matrix<double>& a, Matrix<double>& b )
{
  // This routine is originally based on the public domain draft for JAMA,
  // Java matrix package available at http://math.nist.gov/javanumerics/jama/
  uint count=0;

  if (a.rows() != b.rows())
    return 2;

  if(b.cols() != 1)
    return 3;

  int cols_a = a.cols(), rows_a = a.rows();

  int pivsign = 0;
  int* piv = (int*)alloca( sizeof(int) * rows_a);
  
  PVMSG("PART 1: DECOMPOSITION\n");
  // PART 1: DECOMPOSITION
  //
  // For an m-by-n matrix A with m >= n, the LU decomposition is an m-by-n
  // unit lower triangular matrix L, an n-by-n upper triangular matrix U,
  // and a permutation vector piv of length m so that A(piv,:) = L*U.
  // If m < n, then L is m-by-m and U is m-by-n.
  {
    // Use a "left-looking", dot-product, Crout/Doolittle algorithm.
    for (int i = 0; i < rows_a; ++i)
      piv[i] = i;
    pivsign = 1;
        
    double time_start=PvUtil::time();
   
    // Outer loop.
    for (int j=0; j<cols_a; ++j)
    {
      double t_start=PvUtil::time();
      
      // Make a copy of the j-th column to localize references.
      double* aItrStart = &(a(0,j));

      // Apply previous transformations.
      for (int i = 0; i < rows_a; ++i)
      {
        double* bItrStart = &(a(i,0));

        // This dot product is very expensive.
        // Optimize for SSE2?        
        double* aItr = aItrStart;
        double* bItr = bItrStart;
        double sum = 0.0;        
        int kmax = (i<=j)?i:j;

        while( kmax-- > 0 )
        {
          sum += (*bItr) * (*aItr);
          bItr++;
          aItr+=cols_a;
        }
        bItrStart[j] = aItrStart[i*cols_a] -= sum;
      }

      // Find pivot and exchange if necessary.
      //
      // Slightly optimized version of:
      //  for (int i = j+1; i < m; ++i)
      //    if ( fabs(LUcolj[i]) > fabs(LUcolj[p]) )
      //      p = i;
      int p = j;
      double coljp_abs = aItrStart[p*cols_a];      
      double* aItr = aItrStart + (j+1)*cols_a;
      for (int f=j+1;f<rows_a;f++,aItr+=cols_a)
      {
        if (fabs(*aItr) > coljp_abs)
        {
          p = f;
          coljp_abs = fabs(aItrStart[p*cols_a]);
        }
      }

      if (p != j)
      {
        //swapRows(a, j, a, p);
        a.swapRows(j,p);

        int tmp = piv[p];
        piv[p] = piv[j];
        piv[j] = tmp;
        pivsign = -pivsign;
      }

      // Compute multipliers.      
      if (j < rows_a && a(j,j) != 0.0)
      {
        double* bItr = aItrStart; bItr+=j*cols_a;
        double* aItr = bItr+cols_a;
        for (int i = j+1; i < rows_a; ++i, aItr+=cols_a)
          *aItr /= *bItr;
      }

      double t_end=PvUtil::time();
      if(j>0) PVMSG("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b");
      PVMSG("Itr: %d, Time elapsed = %4.4f, Time remaining = %4.4f", count++, t_end-time_start, ((double)(cols_a-j-1))* ((t_end-time_start)/(j+1)));                                       
    }
    PVMSG("\n");
  }
  
  PVMSG("PART 2: SOLVE\n");
  // PART 2: SOLVE

  // Check singluarity
  for (int j = 0; j < cols_a; ++j)
    if (a(j,j) == 0)
      return 1;

  // Reorder b according to pivotting
  for (int i=0; i<rows_a; ++i)
  {
    if ( piv[i] != i )
    {
      //swapRows(b,i,b,piv[i]);
      b.swapRows(i,piv[i]);

      for ( int j=i; j<rows_a; ++j )
        if ( piv[j] == i )
        {
          piv[j] = piv[i];
          break;
        }
    }
  }

  // Solve L*Y = B(piv,:)
  double* akItr = b.pointer();
  for (int k=0; k<cols_a; ++k, akItr++)
  {
    double* aiItr = akItr; aiItr++;
    for (int i = k+1; i < cols_a; ++i, aiItr++)
    {
      double aik = a(i,k);
      *aiItr -= *akItr * aik;
    }
  }

  // Solve U*X = Y;
  akItr = &(b(cols_a-1,0));
  for (int k=cols_a-1; k>=0; --k, akItr--)
  {
    *akItr *= 1.0/a(k,k); 
    
    double* aiItr = b.pointer();
    for (int i=0; i<k; ++i, aiItr++)
    {
      double aik = a(i,k);
      *aiItr -= *akItr * aik;
    }
  }

  return 0;
}

/*
 *  Calculate Thin Plate Spline (TPS) weights from
 *  control points and build a new height grid by
 *  interpolating with them.
 *
 *  This is a friend function to allow access to this 
 *  function without having to create an object of this class
 */
friend double createGridData(std::vector<Vector3d>& controlPoints, Matrix<float> &Xout, Matrix<float> &Yout, Matrix<float> &Zout, int gridWidth, int gridHeight, double reg=0.0)
{
  return this->griddata(controlPoints, Xout, Yout, Zout, gridWidth, gridHeight, reg);  
};


  
/*
 *  Calculate the value at any a given 2D location (x,y) 
 *  through interpolation of given mesh data
 *
 *  This is a friend function to allow access to this 
 *  function without having to create an object of this class
 */
 static bool getInterpolatedValue(T x, T y, T &z, Matrix<T>& X, Matrix<T>& Y, Matrix<T>& Z, int method)
  {
    T maxX = X.maximum();
    T minX = X.minimum();

    T maxY = Y.maximum();
    T minY = Y.minimum();

    T numGridStepsX = X.cols()-1;
    T numGridStepsY = Y.rows()-1;

    double gridStepsPerUnitX = ((double)numGridStepsX)/(maxX-minX);
    double gridStepsPerUnitY = ((double)numGridStepsY)/(maxY-minY);

    z=0;

    //check if data point falls outside bounds
    if(x<minX || x>maxX || y<minY || y>maxY)		
      return false;


    //get the index of the closest data point on the grid
    float xIdx = (float)((x-minX)*gridStepsPerUnitX);
    float yIdx = (float)((y-minY)*gridStepsPerUnitY);


    switch(method)
    {	
    case NN:
      {			
        int xIdxRnd = round_exact(xIdx+0.5f)-1; //round truncates to nearest smallest pos number
        int yIdxRnd = round_exact(yIdx+0.5f)-1;

        z = Z(xIdxRnd, yIdxRnd); 

        if(_isnan(z)) //it is possible that no value exists for a given data point						
        {
          z=0;
          return false; 
        }
        break;
      }

    case LINEAR: //actually does a bilinear interpolation
      {
        int xIdxRnd0 = round_exact(xIdx);
        int yIdxRnd0 = round_exact(yIdx);

        if(xIdxRnd0<Z.rows()-1 && yIdxRnd0<Z.cols()-1)
        {
          T p0z = Z(xIdxRnd0,yIdxRnd0);
          T p1z = Z(xIdxRnd0+1,yIdxRnd0);
          T p2z = Z(xIdxRnd0,yIdxRnd0+1);
          T p3z = Z(xIdxRnd0+1,yIdxRnd0+1);

          T udx = X(xIdxRnd0+1,yIdxRnd0) - X(xIdxRnd0,yIdxRnd0);
          T udy = Y(xIdxRnd0,yIdxRnd0+1) - Y(xIdxRnd0,yIdxRnd0);
          T dx = (x-X(xIdxRnd0,yIdxRnd0))/udx;
          T dy = (y-Y(xIdxRnd0,yIdxRnd0))/udy;

          T a10 = p1z-p0z;
          T a01 = p2z-p0z;
          T a11 = p0z - p1z - p2z + p3z;

          z = p0z + a10*dx + a01*dy + a11*dx*dy;
        }
        break;
      }
    }

    return true;
  };

  //
  // OPERATIONS
  //

public:


  bool getInterpolatedValue(T x, T y, T &z)
  {
    z=0;

    //check if data point falls outside bounds
    if(x<mMinX || x>mMaxX || y<mMinY || y>mMaxY)		
      return false;


    //get the index of the closest data point on the grid
    float xIdx = (float)((x-mMinX)*mGridStepsPerUnitX);
    float yIdx = (float)((y-mMinY)*mGridStepsPerUnitY);


    switch(mMethod)
    {	
    case NN:
      {			
        int xIdxRnd = round_exact(xIdx+0.5f)-1; //round truncates to nearest smallest pos number
        int yIdxRnd = round_exact(yIdx+0.5f)-1;

        z = mZ(xIdxRnd, yIdxRnd); 

        if(_isnan(z)) //it is possible that no value exists for a given data point						
        {
          z=0;
          return false; 
        }
        break;
      }

    case LINEAR: //actually does a bilinear interpolation
      {
        int xIdxRnd0 = round_exact(xIdx);
        int yIdxRnd0 = round_exact(yIdx);

        if(xIdxRnd0<mZ.rows()-1 && yIdxRnd0<mZ.cols()-1)
        {
          T p0z = mZ(xIdxRnd0,yIdxRnd0);
          T p1z = mZ(xIdxRnd0+1,yIdxRnd0);
          T p2z = mZ(xIdxRnd0,yIdxRnd0+1);
          T p3z = mZ(xIdxRnd0+1,yIdxRnd0+1);

          T udx = mX(xIdxRnd0+1,yIdxRnd0) - mX(xIdxRnd0,yIdxRnd0);
          T udy = mY(xIdxRnd0,yIdxRnd0+1) - mY(xIdxRnd0,yIdxRnd0);
          T dx = (x-mX(xIdxRnd0,yIdxRnd0))/udx;
          T dy = (y-mY(xIdxRnd0,yIdxRnd0))/udy;

          T a10 = p1z-p0z;
          T a01 = p2z-p0z;
          T a11 = p0z - p1z - p2z + p3z;

          z = p0z + a10*dx + a01*dy + a11*dx*dy;
        }
        break;
      }
    }

    return true;
  };


  //
  // ACCESS
  //

public:


  //
  // INQUIRY
  //

public:

  /**
   * Ask if this object has been initialized (the init() function has been called).
   */
  bool isInitialized() const { return mIsInitialized; }


  //
  // ATTRIBUTES
  //
  public:

	/**
	 * Interpolation Method
	 * Note: currently only a "linear" interpolation schema is implemented
	 *
	 * Possible methods:
	 * 'linear':  performs a linear interpolation between a set of three data points
	 */
	enum interpolationMethods
	{
		LINEAR,
			NN
	} mMethod;


protected:

  /**
   * Tells if this object has been initialized.
   */
  bool mIsInitialized;

private:
	
	/**
	 * Data members to hold the 2D sample data that is to be interpolated.
	 *
	 * X and Y specify the 2d mesh grid points (not neccessarly equally spaced)
	 * Z represent the data values for the given grid points 
	 * Grid points for which no data values are available, an entry of NaN can be provided in Z
	 */
	Matrix<T> mX;
	Matrix<T> mY;
	Matrix<T> mZ;

	T mMaxX;
	T mMinX;
	T mMaxY;
	T mMinY;
	
	unsigned int mNumGridStepsX;
	unsigned int mNumGridStepsY;

	double mGridStepsPerUnitX;
	double mGridStepsPerUnitY;



/// Smart pointer to Interpolate
//typedef Interpolate<float> InterpolateFloat;
//typedef boost::shared_ptr<InterpolateFloat> InterpolateFloatPtr;
//
//
//static double createGridData(std::vector<Vector3d>& controlPoints, Matrix<float> &Xout, Matrix<float> &Yout, Matrix<float> &Zout, int gridWidth, int gridHeight, double reg)
//{
//  return Interpolate<float>::griddata(controlPoints, Xout, Yout, Zout, gridWidth, gridHeight, reg);  
//}


};

}; // namespace ait


#endif // Interpolate_HPP


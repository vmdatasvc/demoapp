/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef TRANSFORMER_HPP
#define TRANSFORMER_HPP

#include <legacy/pv/ait/Matrix.hpp>
#include <legacy/pv/PvUtil.hpp>

namespace ait
{

enum tranConst { TR_ID,TR_COMB_4,TR_COMB_3,TR_COMB,TR_NEWTON,TR_NEWTON_4,TR_DIFF,TR_TRAJ };

/*********************************************************************
 ** 
 ** Transformer Class
 **
 *********************************************************************
 ** 
 ** Description:
 **
 ** Virtual base class that is the basis for general coordinate 
 ** transformation classes. The capability of these classes
 ** is in general to be able to transform streams of coordinates.
 ** For example, the coordinates (x(i),y(i),i=0,...,N-1) can be
 ** transformed to the velocities and accelerations 
 ** (vx(i), vy(i), ax(i), ay(i), i=0,...,N-3).
 **
 ** The general problem is that non-local transforms need several
 ** input values to produce one output value. These vary with varying 
 ** transforms. The base Transformer class works with arbitrary numbers
 ** that have to be supplied by the classes that inherit their 
 ** base functionality from this base class.
 **
 ** In particular, class implementations will have to provide
 **
 **    - inputRows()         : # of input vectors needed for one output vector
 **    - inputCols()         : # of columns the input vector must have
 **                            can be zero if arbitrary numbers possible
 **                            Example: 3 for 3D rotations, 0 for differentiation
 **    - outputCols(inCols)  : # of columns each outpus vector will have
 **                            This number can vary with inCols.
 *********************************************************************/
template<class T> class Transformer
{
protected:

  int secRows;
  int secFirstRow;
  Matrix<T> sec;

  void init(int inCols=0);
  inline void checkInCols(int inCols);

public:

  // number of input vectors needed to produce a transformed output vector
  // (e.g., two for simple differentiation)
  virtual int inputRows(void)=0;

  // number of components each input vector must have (0 indicates arbitrary numbers)
  // (e.g., zero for differentiation, three for 3D rotation)
  virtual int inputCols(void)=0;

  // number of components each output vector will have (may or may not depend on input)
  // (e.g., "inCols" for differentiation, 2*"inCols" for x->(v,a) transform)
  virtual int outputCols(int inCols)=0;

  // Transforms the whole input 'in' to the output 'out'.
  void transform(Matrix<T> &in, Matrix<T> &out);

  // produces exactly one output row in out(outRow,.) by
  // transforming the appropritate # of
  // input rows in(inRow,.),in(inRow-1,.),... (dep. on inputRows() ).
  virtual void transformSection(Matrix<T> &in, int inRow, 
				Matrix<T> &out,int outRow)=0;

  // Transforms the whole input 'in' to the output 'out'.
  bool transformIncremental(Matrix<T> &in, Matrix<T> &out);
};

/*********************************************************************
 * init()
 * 
 * Initializes a ring buffer that stores the last 'bufferSize'
 * number of input coordinates.
 * bufferSize has to be at least of size inputRows(). If zero, 
 * it will be set to inputRows().
 * 
 * <input> 
 *
 * int bufferSize : Capacity of buffer
 *
 *********************************************************************/
template<class T> void Transformer<T>::init(int inCols)
{
  int bufferSize=inputRows();
  
  sec.resize(bufferSize,inCols);

  secRows=0;
  secFirstRow=0;
}

/*********************************************************************
 * checkInCols()
 * 
 * Check if the value <inCols> is compatible with 
 * inputCols(). If inputCols() is zero, any value is valid.
 *
 * If the value is found to be invalid, the program terminates.
 *
 * <input> 
 *
 * int inCols : value to check
 *
 *********************************************************************/
template<class T> void Transformer<T>::checkInCols(int inCols)
{
  if(inCols!=inputCols() && inputCols()!=0)
    {
      PvUtil::exitError("Transformer: Error, wrong number of components in input. Is %d, should be %d",inCols, inputCols());
    }
}

/*********************************************************************
 * transform()
 * 
 * Transform the whole matrix in to the matrix out.
 *
 * <input> 
 *
 * Matrix<T> &in,&out:  input and output matrices
 *
 *********************************************************************/
template<class T> void Transformer<T>::transform(Matrix<T> &in, Matrix<T> &out)
{
  int i;
  int inRows=in.rows();
  int inCols=in.cols();
  int outRows=inRows-inputRows()+1;
  int outCols=outputCols(inCols);

  checkInCols(inCols);
  out.resize(outRows,outCols);

  for(i=0;i<outRows;i++)
    {
      transformSection(in,i+inputRows()-1,out,i);
    }
}

/*********************************************************************
 * transform()
 * 
 * Transform the whole matrix in to the matrix out.
 *
 * <input> 
 *
 * Matrix<T> &in,&out:  input and output matrices
 *
 *********************************************************************/
template<class T> bool Transformer<T>::transformIncremental(Matrix<T> &in, Matrix<T> &out)
{
  unsigned int r,c;

  if(in.rows()*in.cols()!=sec.cols())
  {
    PvUtil::exitError("Transformer.transformIncremental(): Error, input size different from construction inCols.");
  }

  for(r=1;r<sec.rows();r++)
  {
    for(c=0;c<sec.cols();c++)
    {
      sec(r-1,c)=sec(r,c);
    }
  }
  for(c=0;c<sec.cols();c++)
  {
    sec(sec.rows()-1,c)=in(c);
  }

  if(secRows<sec.rows())
  {
    secRows++;
    return false;
  }

  //sec.printf("%g ");
  out.resize(outputCols(sec.cols()),1);

  transformSection(sec,sec.rows()-1,out,0);

  //out.printf("%g ");
  return true;
}

/*********************************************************************
 ** 
 ** An identity transformer.
 **
 *********************************************************************/
template<class T> class IdTransformer : public Transformer<T>
{
public:
  IdTransformer(int inCols=0) { this->init(inCols); }

  // number of input vectors needed to produce a transformed output vector
  // (e.g., two for simple differentiation)
  int inputRows(void) { return 1; }

  // number of components each input vector must have (0 indicates arbitrary numbers)
  // (e.g., zero for differentiation, three for 3D rotation)
  int inputCols(void) { return 0; }

  // number of components each output vector will have (may or may not depend on input)
  // (e.g., "inCols" for differentiation, 2*"inCols" for x->(v,a) transform)
  int outputCols(int inCols) { return inCols; };

  void transformSection(Matrix<T> &in, int inRow, 
						Matrix<T> &out,int outRow)
  {
      for(unsigned int i=0;i<in.cols();i++)
	  {
		  out(outRow,i)=in(inRow,i);
	  }
  }
};

/*********************************************************************
 ** 
 ** Simple differentiating transformer.
 **
 *********************************************************************/
template<class T> class DiffTransformer : public Transformer<T>
{
public:
  DiffTransformer(int inCols=0) { this->init(inCols); }

  // number of input vectors needed to produce a transformed output vector
  // (e.g., two for simple differentiation)
  int inputRows(void) { return 2; }

  // number of components each input vector must have (0 indicates arbitrary numbers)
  // (e.g., zero for differentiation, three for 3D rotation)
  int inputCols(void) { return 0; }

  // number of components each output vector will have (may or may not depend on input)
  // (e.g., "inCols" for differentiation, 2*"inCols" for x->(v,a) transform)
  int outputCols(int inCols) { return inCols; };

  void transformSection(Matrix<T> &in, int inRow, 
						Matrix<T> &out,int outRow)
  {
      for(unsigned int i=0;i<in.cols();i++)
	  {
		  out(outRow,i)=in(inRow,i)-in(inRow-1,i);
	  }
  }
};

/*********************************************************************
 ** 
 ** Transformer that simply combines two rows of input into
 ** a single row in the output.
 **
 ** E.g., (a)               (a b)
 **       (b)      =>       (b c)
 **       (c)               (c d)
 **       (d)
 **
 *********************************************************************/
template<class T> class CombineTransformer : public Transformer<T>
{
public:
  CombineTransformer(int inCols=0) { this->init(inCols); }

  // number of input vectors needed to produce a transformed output vector
  // (e.g., two for simple differentiation)
  int inputRows(void) { return 2; }

  // number of components each input vector must have (0 indicates arbitrary numbers)
  // (e.g., zero for differentiation, three for 3D rotation)
  int inputCols(void) { return 0; }

  // number of components each output vector will have (may or may not depend on input)
  // (e.g., "inCols" for differentiation, 2*"inCols" for x->(v,a) transform)
  int outputCols(int inCols) { return inCols*2; };

  void transformSection(Matrix<T> &in, int inRow, 
	  Matrix<T> &out,int outRow)
  {
      for(unsigned int i=0;i<in.cols();i++) 
      {
		  out(outRow,i)=in(inRow,i);
		  out(outRow,i+in.cols())=in(inRow-1,i);
      }
  }
};

/*********************************************************************
 ** 
 ** Transformer that simply combines three rows of input into
 ** a single row in the output.
 **
 ** E.g., (a)               (a b c)
 **       (b)      =>       (b c d)
 **       (c)               (c d e)
 **       (d)
 **       (e)
 **
 *********************************************************************/
template<class T> class Combine3Transformer : public Transformer<T>
{
public:
  Combine3Transformer(int inCols=0) { this->init(inCols); }

  // number of input vectors needed to produce a transformed output vector
  // (e.g., two for simple differentiation)
  int inputRows(void) { return 3; }

  // number of components each input vector must have (0 indicates arbitrary numbers)
  // (e.g., zero for differentiation, three for 3D rotation)
  int inputCols(void) { return 0; }

  // number of components each output vector will have (may or may not depend on input)
  // (e.g., "inCols" for differentiation, 2*"inCols" for x->(v,a) transform)
  int outputCols(int inCols) { return inCols*3; };

  void transformSection(Matrix<T> &in, int inRow, 
	  Matrix<T> &out,int outRow)
  {
      for(unsigned int i=0;i<in.cols();i++) 
      {
		  out(outRow,i)=in(inRow,i);
		  out(outRow,i+in.cols())=in(inRow-1,i);
		  out(outRow,i+2*in.cols())=in(inRow-2,i);
      }
  }
};

/*********************************************************************
 ** 
 ** Transformer that simply combines four rows of input into
 ** a single row in the output.
 **
 *********************************************************************/
template<class T> class Combine4Transformer : public Transformer<T>
{
public:
  Combine4Transformer(int inCols=0) { this->init(inCols); }

  // number of input vectors needed to produce a transformed output vector
  // (e.g., two for simple differentiation)
  int inputRows(void) { return 4; }

  // number of components each input vector must have (0 indicates arbitrary numbers)
  // (e.g., zero for differentiation, three for 3D rotation)
  int inputCols(void) { return 0; }

  // number of components each output vector will have (may or may not depend on input)
  // (e.g., "inCols" for differentiation, 2*"inCols" for x->(v,a) transform)
  int outputCols(int inCols) { return inCols*4; };

  void transformSection(Matrix<T> &in, int inRow, 
	  Matrix<T> &out,int outRow)
  {
      for(unsigned int i=0;i<in.cols();i++) 
      {
		  out(outRow,i)=in(inRow,i);
		  out(outRow,i+in.cols())=in(inRow-1,i);
		  out(outRow,i+2*in.cols())=in(inRow-2,i);
		  out(outRow,i+3*in.cols())=in(inRow-3,i);
      }
  }
}; 

/*********************************************************************
 ** 
 ** Transformer that transforms positions into pos, velocity, acceleration.
 **
 *********************************************************************/
template<class T> class NewtonTransformer : public Transformer<T>
{
public:
  NewtonTransformer(int inCols=0) { this->init(inCols); }

  // number of input vectors needed to produce a transformed output vector
  // (e.g., two for simple differentiation)
  int inputRows(void) { return 3; }

  // number of components each input vector must have (0 indicates arbitrary numbers)
  // (e.g., zero for differentiation, three for 3D rotation)
  int inputCols(void) { return 0; }

  // number of components each output vector will have (may or may not depend on input)
  // (e.g., "inCols" for differentiation, 2*"inCols" for x->(v,a) transform)
  int outputCols(int inCols) { return inCols*3; };

  void transformSection(Matrix<T> &in, int inRow, 
    Matrix<T> &out,int outRow)
  {
    for(unsigned int i=0;i<in.cols();i++) 
    {
      out(outRow,i)=in(inRow,i);
      out(outRow,i+in.cols())=in(inRow,i)-in(inRow-2,i);
      out(outRow,i+2*in.cols())=(in(inRow,i)-2*in(inRow-1,i)+in(inRow-2,i));
    }
  }
}; 

/*********************************************************************
 ** 
 ** Transformer that transforms positions into 
 ** 
 ** pos + the first three derivatives
 **
 *********************************************************************/
template<class T> class Newton4Transformer : public Transformer<T>
{
public:
  Newton4Transformer(int inCols=0) { this->init(inCols); }

  // number of input vectors needed to produce a transformed output vector
  // (e.g., two for simple differentiation)
  int inputRows(void) { return 4; }

  // number of components each input vector must have (0 indicates arbitrary numbers)
  // (e.g., zero for differentiation, three for 3D rotation)
  int inputCols(void) { return 0; }

  // number of components each output vector will have (may or may not depend on input)
  // (e.g., "inCols" for differentiation, 2*"inCols" for x->(v,a) transform)
  int outputCols(int inCols) { return inCols*4; };

  void transformSection(Matrix<T> &in, int inRow, 
    Matrix<T> &out,int outRow)
  {
    for(unsigned int i=0;i<in.cols();i++) 
    {
      out(outRow,i)=in(inRow,i);
      out(outRow,i+in.cols())=0.5f*(in(inRow-1,i)+in(inRow-1,i)-in(inRow-2,i)-in(inRow-3,i));
      out(outRow,i+2*in.cols())=(in(inRow,i)-in(inRow-1,i)-in(inRow-2,i)+in(inRow-3,i));
      out(outRow,i+3*in.cols())=(in(inRow,i)-3*in(inRow-1,i)+3*in(inRow-2,i)-in(inRow-3,i));
    }
  }
}; 

/*********************************************************************
 ** 
 ** Transformer that transforms (x,y) positions into
 ** trajectory properties that are independent of orientation.
 ** 
 *********************************************************************/
template<class T> class TrajectoryTransformer : public Transformer<T>
{
public:
  TrajectoryTransformer(int inCols=0) { this->init(inCols); }

  // number of input vectors needed to produce a transformed output vector
  // (e.g., two for simple differentiation)
  int inputRows(void) { return 4; }

  // number of components each input vector must have (0 indicates arbitrary numbers)
  // (e.g., zero for differentiation, three for 3D rotation)
  int inputCols(void) { return 2; }

  // number of components each output vector will have (may or may not depend on input)
  // (e.g., "inCols" for differentiation, 2*"inCols" for x->(v,a) transform)
  int outputCols(int inCols) { return 5; };

  void transformSection(Matrix<T> &in, int inRow, 
    Matrix<T> &out,int outRow)
  {
    int j;
    for(unsigned int i=0;i<in.cols();i++) 
    {
      T x[4],y[4],d[4];
      T xp0,xp1,dp0,dp1,dd,dd0,dd1,dd2;
      int k=0;

      for(j=0;j<4;j++)
      {
        x[j]=in(inRow-j,0);
        y[j]=in(inRow-j,1);
        if(j==0)
        {
          out(outRow,k++)=x[0];
          out(outRow,k++)=y[0];
        }
        x[j]=x[j]-x[0];
        y[j]=y[j]-y[0];
        d[j]=T(sqrt(x[j]*x[j]+y[j]*y[j]));
      }
      
      xp0=x[2]*y[1]-x[1]*y[2];
      xp1=x[3]*y[2]-x[2]*y[3];
      dp0=x[1]*x[2]-y[1]*y[2];
      dp1=x[2]*x[3]-y[2]*y[3];
      dd=(d[1]+d[2]+d[3])/3.0f;
      dd0=d[3]-d[2];
      dd1=d[2]-d[1];
      dd2=dd1-dd0;

      j=0;

      out(outRow,k++)=dd;
      out(outRow,k++)=dp0;
      out(outRow,k++)=xp0;
      /*
      out(outRow,k++)=dd0;
      out(outRow,k++)=dd1;
      out(outRow,k++)=dd2;
      out(outRow,k++)=dp1;
      out(outRow,k++)=xp1;
      */
    }
  }
}; 

} // namespace ait

#endif

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ParticleCloud_HPP
#define ParticleCloud_HPP

#include <ait/Matrix.hpp>
#include <PvRandom.hpp>
#include <StatUtils.hpp>

namespace ait
{

/**
 * This class implements an optimization algorithm based on evaluating 
 * sample points with a n-dimensional gaussian distribution around the
 * estimated solution.
 */
template<class T> class ParticleCloud
{
public:

  /**
   * Constructor.
   */
  ParticleCloud() 
  : mNDim(0), 
    mNParticles(0), 
    mpValues(NULL), 
    mpPosterior(NULL), 
    mNumPhases(1),
		mAccumWeight(0),
    mIsTracking(false)
  {
  }

  /**
   * Destructor.
   */
  ~ParticleCloud() 
  { 
    initialize(0,0); 
  }

  /**
   * Set the parameters for the algorithm.
   * @param nParticles [in] number of particles to track.
   * @param A [in] matrix defining the deterministic dynamics of the system for prediction.
   * @param B [in] matrix defining the noise distribution.
   * @param numEvalPhases [in] number of iterations to evaluate the function.
   * Usually this is needed to precompute ranges, or normalize partial results before
   * computing the final value.
   * @param percentile [in] Percentile of the particles that must be considered for the weighted
   * average. 1 means all particles, 0.5 is half of the particles.
   */
  void setParams(int nParticles0, const Matrix<T>& A0, const Matrix<T>& B0, int numEvalPhases = 1,
                 T percentile = 1);

  /**
   * Perform one iteration of the algorithm.
   * @param x [in] initial value of the algorithm.
   * @param x [out] result of the iteration.
   * @param funk [in] function that weights each of the particles. The format of this function is
   * myEvalFunction(T *particleValues, void *objectPointer).
   * @param ptr [in] pointer that will be passed to the function, can be used to pass the instance pointer.
   * @remarks On the first step (or after calling reset()), the parameter x is used as a seed particle. All
   * particles will spread from this one. The function funk will be called once per particle, with the
   * first parameter as a pointer to the status vector values.
   */
  void step(Matrix<T> &x, T (*funk)(T *, void *), void *ptr)
  {
    step(x,funk,NULL,ptr);
  }

  /**
   * Perform one iteration of the algorithm.
   * @param x [in] initial value of the algorithm.
   * @param x [out] result of the iteration.
   * @param funk [in] function that weights each of the particles. The format of this function is
   * myEvalFunction(T *particleValues, int particleIndex, int phaseIndex, void *objectPointer).
   * The particle index is the index of the particle that is currently being evaluated; the phase
   * index is the index of the evaluation phase.
   * @param ptr [in] pointer that will be passed to the function, can be used to pass the instance pointer.
   * @remarks On the first step (or after calling reset()), the parameter x is used as a seed particle. All
   * particles will spread from this one. The function funk will be called once per particle, with the
   * first parameter as a pointer to the status vector values.
   */
  void step(Matrix<T> &x, T (*funk)(T *, int, int, void *), void *ptr)
  {
    step(x,NULL,funk,ptr);
  }

  /// Reset the particles.
  virtual void reset() { mIsFirstStep = true; }

  /// Return the number of particles
  int getNumParticles() { return mNParticles; }

  /// Returns the dimension of the state vector.
  int getDimension() { return mNDim; }

  /// Returns a pointer to a particle vector values.
  T *getParticle(int i) { return mpValues+i*mNDim; }

  /// Returns the weight of a particle.
  T getParticleWeight(int i) { return mpPosterior[i]; }

	// Returns the evaluation weight of all particles
	T getAccumEvalWeight() {return mAccumWeight;}

  /// Returns the variance of the particle system.
  Matrix<T>& getVariance() { return mVar; }

  /// Returns the matrix B given in setParams().
  Matrix<T>& getNoiseCoVariance() { return mB; }

  /// Return the number of phases per iteration step.
  int getNumPhases() { return mNumPhases; }

  /// Return true if it is tracking.
  bool isTracking() { return mIsTracking; }

protected:
  /**
   * Generate the particle cloud with the noise multiplied by the
   * factor provided.
   */
  void generateCloud(const Matrix<T> &seed, T noiseFactor);

protected:

  /// Dimension of the problem
  unsigned int mNDim;

  /// Number of particles for the condensation
  unsigned int mNParticles;

  /**
   * Percentile that represents the percentage of the particles
   * that must be considered for the final distribution. Only the
   * top % particles will be considered.
   */
  T mPercentile;

  /// Values of the particles
  T *mpValues;

  /// Posteriori weights
  T *mpPosterior;

	/// accum. eval. weight of all particles
	T mAccumWeight;

  /// Coefficient matrices for prediction
  Matrix<T> mA,mB;

  /// Random Number generators
  PvRandom mGaussRandom;

  /// First step flag
  bool mIsFirstStep;

  /// Value of the last result
  Matrix<T> mLastResult;

  /// Value of the previous result
  Matrix<T> mPrevResult;

  /// Value of the variance.
  Matrix<T> mVar;

  /// Number of phases per iteration step.
  unsigned int mNumPhases;

  /**
   * This flags indicates if an object is being tracked. An
   * object is considered to be tracked if there are particles
   * with weight greater than zero.
   */
  bool mIsTracking;

  /**
   * This noise factor will grow when the solution can not be found,
   * once a solution is found, it will shrink to 1.
   */
  T mNoiseFactor;

  /// Predict the values of each particle using the dynamic information (matrices A and B).
  virtual void predict() {};

private:

  /**
   * Initialize the algorithm
   * @param nParticles [in] Number of particles to track
   * @param nDim [in] Dimension of the state vector
   */
  void initialize(int nParticles0, int nDim0);

  void binarySearch();

  /// Step prototype.
  void step(Matrix<T> &x, T (*funk)(T *, void *), 
                            T (*funkPhase)(T *, int, int, void *), 
                            void *ptr);

};

template<class T> void ParticleCloud<T>::initialize(int nParticles0, int nDim0)
{
  if (mNParticles != nParticles0 || mNDim != nDim0)
  {
    if (mpValues)
    {
      delete [] mpValues; mpValues = NULL;
      delete [] mpPosterior; mpPosterior = NULL;
    }
  }

  mNParticles = nParticles0;
  mNDim = nDim0;

  if (!mNParticles || !mNDim)
  {
    return;
  }

  if (!mpValues)
  {
    mpValues = new T[mNParticles*mNDim];
    mpPosterior = new T[mNParticles];
  }

  mIsFirstStep = true;

}

template<class T> void ParticleCloud<T>::setParams(int nParticles0, const Matrix<T>& A0, const Matrix<T>& B0, int numEvalPhases,
                                                   T percentile)
{
  initialize(nParticles0,A0.rows());

  mA = A0;
  mB = B0;

  mNumPhases = numEvalPhases;
  mPercentile = percentile;
}

template<class T> void ParticleCloud<T>::generateCloud(const Matrix<T> &seed, T noiseFactor)
{
  Matrix<T> T0,T1(mB.rows(),1),T2,xNew;
  unsigned int i,j;

  mNoiseFactor = noiseFactor;

  for (i = 0; i < mNParticles; i++)
  {
    T0.mx_fromarray(mpValues+i*mNDim,1,mNDim);

    xNew = seed;

    for(j=0;j<T1.rows();j++)
    {
      T1(j)=mGaussRandom.gaussRand();
    }

    T2.mult(mB,T1);

    if (mNoiseFactor != 1)
    {
      T2.mult(mNoiseFactor);
    }

    T2.transpose();

    T0.add(xNew,T2);
  }
}

template<class T> void ParticleCloud<T>::step(Matrix<T> &x,
                                             T (*funk)(T *, void *), 
                                             T (*funkPhase)(T *, int, int, void *), 
                                             void *ptr)
{

  unsigned int i,j;

  if (mIsFirstStep)
  {
    generateCloud(x,5);
  }
  else
  {
    Matrix<T> T0,D;
    //unsigned int i;

    for (i = 0; i < mNParticles; i++)
    {
      T0.mx_fromarray(mpValues+i*mNDim,1,mNDim);

      D.sub(x,mPrevResult);
      T0.add(T0,D);
    }
  }

  // Measure and weight the new position
  T accumNorm = 0;

  // Call the evaluation function mNumPhases times.
  for (j = 0; j < mNumPhases; j++)
  {
    accumNorm = 0;
    for (i = 0; i < mNParticles; i++)
    {
      // Depending on the prototype used, a different function
      // will be used.
      if (funk)
      {
        mpPosterior[i] = funk(mpValues+i*mNDim,ptr);
      }
      else
      {
        mpPosterior[i] = funkPhase(mpValues+i*mNDim,i,j,ptr);
      }
      accumNorm += mpPosterior[i];
    }
  }

	mAccumWeight = accumNorm;

  if (accumNorm != 0)
  {
    // Calculate the accumulated value that must be normalized, and clear 
    // the values below the threshold (minVal)
    T newAccum = 0;
    if (mPercentile < 1)
    {
      T minVal = StatUtils::computeMth((Uint)((1-mPercentile)*mNParticles),
                                       mpPosterior,mNParticles,false);

      for (i = 0; i < mNParticles; i++)
      {
        if (mpPosterior[i] >= minVal)
        {
          newAccum += mpPosterior[i];
        }
        else
        {
          mpPosterior[i] = 0;
        }
      }
    }
    else
    {
      newAccum = accumNorm;
    }

    // Normalize
    for (i = 0; i < mNParticles; i++)
    {
      mpPosterior[i] = mpPosterior[i]/newAccum;
    }
    mIsTracking = true;
  }
  else
  {
    mIsTracking = false;
    T p = ((T)1)/mNParticles;
    for (i = 0; i < mNParticles; i++)
    {
      mpPosterior[i] = p;
    }
  }

  // Reduce the noise if an object is being tracked.
  if (mIsTracking && mNoiseFactor > 1)
  {
    generateCloud(x,1);
  }

  mPrevResult = x;

  // Estimate the new position (weighted mean, 1st moment)
  x.setAll(0);
  T *pX = x.pointer();
  for (i = 0; i < mNParticles; i++)
  {
    for (j = 0; j < mNDim; j++)
    {
      pX[j] += mpPosterior[i]*mpValues[i*mNDim+j];
    }
  }

  // Estimate the variance (2nd moment)
  mVar.resize(mNDim,1);
  mVar.setAll(0);
  for (i = 0; i < mNParticles; i++)
  {
    for (j = 0; j < mNDim; j++)
    {
      //      TElem tmp = x(j)-mpValues[i*mNDim+j];
      T tmp = x(j)-mpValues[i*mNDim+j];
      mVar(j) += mpPosterior[i]*tmp*tmp;
    }
  }

  // Copy last result.
  mLastResult.resize(1,mNDim);
  for (j = 0; j < mNDim; j++)
  {
    mLastResult(j) = pX[j];
  }

  if (mIsFirstStep) mIsFirstStep = false;
}

} // namespace ait

#endif // ParticleCloud_HPP

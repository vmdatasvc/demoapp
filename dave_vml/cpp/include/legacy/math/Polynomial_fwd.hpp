/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef Polynomial_fwd_HPP
#define Polynomial_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class Polynomial;
typedef boost::shared_ptr<Polynomial> PolynomialPtr;

}; // namespace ait

#endif // LineSegment_fwd_HPP

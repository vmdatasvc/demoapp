/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef StatUtils_HPP
#define StatUtils_HPP

// SYSTEM INCLUDES
//

#include <vector>
#include <algorithm>
#include <boost/utility.hpp>
#include <legacy/types/AitBaseTypes.hpp>

// AIT INCLUDES
//

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

#ifdef COMPUTE_SQUARE
#undef COMPUTE_SQUARE
#endif

#ifdef COMPUTE_CUBE
#undef COMPUTE_CUBE
#endif

#ifdef COMPUTE_FOURTH
#undef COMPUTE_FOURTH
#endif

/// Compute the square of the give number
#define COMPUTE_SQUARE(x) ((x)*(x))

/// Compute the cube of the given number
#define COMPUTE_CUBE(x) ((x)*(x)*(x))

/// Compute the given number raised to the fourth power
#define COMPUTE_FOURTH(x) ((x)*(x)*(x)*(x))

namespace ait 
{

  namespace StatUtils
  {

    /**
     * Get the median of the values in the given vector.  This is done in
     * O(n) time.
     * @par Responsibilities:
     * - Compute Median.
     * @remarks
     * If the number of elements in values is even then the median will be n/2.
     * If the number of elements in values is odd then the median will be (n-1)/2.
     * If the rearrange flag is true then the elements of the vector will be rearranged.  
     * If the rearrange flag is false then a copy is made of the vector.
     */

  	//template<class T> T computeMth(const Uint m, std::vector<T>& values, const bool rearrange = true);


    /**
     * Get the mth largest value in the given vector.  This is done in
     * O(n) time.
     * @par Responsibilities:
     * - Compute mth largest value
     * @remarks
     * If the rearrange flag is true then the elements of the vector will be rearranged.
     * If the rearrange flag is false then a copy is made of the vector.
     */
    template<class T> T computeMth(const Uint m, std::vector<T>& values, const bool rearrange = true)
    {
      if (rearrange)
      {
        std::nth_element(values.begin(),values.begin()+m,values.end());
        return values[m];
      }
      else
      {
        std::vector<T> v1(values);
        return computeMth(m,v1,true);
      }
    }


    template<class T> T computeMedian(std::vector<T>& values, const bool rearrange = true)
    {
      Uint n = values.size();
      Uint k = ((n % 2) == 0) ? (n / 2) : ((n - 1) / 2);
      return computeMth(k,values,rearrange);
    }

    /**
     * Call computeMedian(Get the median of the values in the given vector.  This is done in
     * O(n) time.
     * @par Responsibilities:
     * - Compute Median.
     * @remarks
     * If the number of elements in values is even then the median will be n/2.
     * If the number of elements in values is odd then the median will be (n-1)/2.
     * If the rearrange flag is true then the elements of the vector will be rearranged.  
     * If the rearrange flag is false then a copy is made of the vector.
     */
    template<class T> T computeMedian(const std::vector<T>& values)
    {
      return computeMedian(const_cast<std::vector<T>&>(values),false);
    }

    /**
     * Get the median of the values in the given array.  This is done in
     * O(n) time.
     * @par Responsibilities:
     * - Compute Median.
     * @remarks
     * If the number of elements in values is even then the median will be n/2.
     * If the number of elements in values is odd then the median will be (n-1)/2.
     * If the rearrange flag is true then the elements of the array will be rearranged.  
     * If the rearrange flag is false then a copy is made of the array.
     */
    template<class T> T computeMedian(T* values, const Uint n, const bool rearrange = true)
    {
      Uint k = ((n % 2) == 0) ? (n / 2) : ((n - 1) / 2);
      return computeMth(k,values,n,rearrange);
    }

//    /**
//     * Get the mth largest value in the given vector.  This is done in
//     * O(n) time.
//     * @par Responsibilities:
//     * - Compute mth largest value
//     * @remarks
//     * If the rearrange flag is true then the elements of the vector will be rearranged.
//     * If the rearrange flag is false then a copy is made of the vector.
//     */
//    template<class T> T computeMth(const Uint m, std::vector<T>& values, const bool rearrange = true)
//    {
//      if (rearrange)
//      {
//        std::nth_element(values.begin(),values.begin()+m,values.end());
//        return values[m];
//      }
//      else
//      {
//        std::vector<T> v1(values);
//        return computeMth(m,v1,true);
//      }
//    }

    /**
     * Get the mth largest value in the given array.  This is done in
     * O(n) time.
     * @par Responsibilities:
     * - Compute mth largest value
     * @remarks
     * If the rearrange flag is true then the elements of the array will be rearranged.  
     * If the rearrange flag is false then a copy is made of the array.
     */
    template<class T> T computeMth(const Uint m, T* values, const Uint n, const bool rearrange = true)
    {
      if (rearrange)
      {
        std::nth_element(values,values+m,values+n);
        return values[m];
      }
      else
      {
        std::vector<T> sv(values,values+n);
        return computeMth(m,sv,true);
      }
    }

    /**
     * Compute the mean value of the given vector.  This is done in
     * O(n) time.
     * @par Responsibilities:
     * - Compute the mean
     * @remarks
     */
    template<class T> T computeMean(const std::vector<T>& values)
    {
      T rv = static_cast<T>(0);
      Uint n = values.size();
      Uint i;

      for(i = 0; i < n; ++i)
      {
        rv += values[i];
      }

				rv /= n;

      return rv;
    }

    /**
     * Compute the mean value of the given array.  This is done in
     * O(n) time.
     * @par Responsibilities:
     * - Compute the mean
     * @remarks
     */
    template<class T> T computeMean(const T* values, const Uint n)
    {
      T rv = static_cast<T>(0);
      Uint i;

      for(i = 0; i < n; ++i)
      {
        rv += values[i];
      }

				rv /= n;

      return rv;
    }

    /**
     * Compute the variance, standard deviation and the absolute deviation given vector, given the mean.  
     * This is done in O(n) time.
     * @par Responsibilities:
     * - Compute the mean, standard deviation and absolute deviation
     * @remarks
     */
    template<class T> T computeVariance(const std::vector<T>& values, const T& mean, T& sdev, T& adev)
    {
      T var = static_cast<T>(0);
      Uint n = values.size();
      Uint i;

      for(i = 0; i < n; ++i)
      {
        var += COMPUTE_SQUARE((values[i] - mean));
        adev += fabs((values[i] - mean));
      }

				var /= (n - 1);
				sdev = sqrt(var);
				adev /= n;

      return var;
    }

    /**
     * Compute the variance, standard deviation and the absolute deviation given array, given the mean.  
     * This is done in O(n) time.
     * @par Responsibilities:
     * - Compute the mean, standard deviation and absolute deviation
     * @remarks
     */
    template<class T> T computeVariance(const T* values, const Uint n, const T& mean, T& sdev, T& adev)
    {
      T var = static_cast<T>(0);
      Uint i;

      for(i = 0; i < n; ++i)
      {
        var += COMPUTE_SQUARE((values[i] - mean));
        adev += fabs((values[i] - mean));
      }

				var /= (n - 1);
				sdev = sqrt(var);
				adev /= n;

      return var;
    }

    /**
     * Compute the skewness of the distribution, given the mean and standard deviation
     * This is done in O(n) time.
     * @par Responsibilities:
     * - Compute the skewness
     * @remarks
     */
    template<class T> T computeSkew(const std::vector<T>& values, const T& mean, const T& sdev)
    {
      T skew = static_cast<T>(0);
      Uint n = values.size();
      Uint i;

      for(i = 0; i < n; ++i)
      {
        skew += COMPUTE_CUBE(((values[i] - mean) / sdev));
      }

      skew /= n;

      return skew;
    }

    /**
     * Compute the skewness of the distribution, given the mean and standard deviation
     * This is done in O(n) time.
     * @par Responsibilities:
     * - Compute the skewness
     * @remarks
     */
    template<class T> T computeSkew(const T* values, const Uint n, const T& mean, const T& sdev)
    {
      T skew = static_cast<T>(0);
      Uint i;

      for(i = 0; i < n; ++i)
      {
        skew += COMPUTE_CUBE(((values[i] - mean) / sdev));
      }

      skew /= n;

      return skew;
    }

    /**
     * Compute the kurtosis of the distribution, given the mean and standard deviation
     * This is done in O(n) time.
     * @par Responsibilities:
     * - Compute the kurtosis
     * @remarks
     */
    template<class T> T computeKurtosis(const std::vector<T>& values, const T& mean, const T& sdev)
    {
      T kurt = static_cast<T>(0);
      Uint n = values.size();
      Uint i;

      for(i = 0; i < n; ++i)
      {
        kurt += COMPUTE_FOURTH(((values[i] - mean) / sdev));
      }

      kurt = (kurt / n) - 3;

      return kurt;
    }

    /**
     * Compute the kurtosis of the distribution, given the mean and standard deviation
     * This is done in O(n) time.
     * @par Responsibilities:
     * - Compute the kurtosis
     * @remarks
     */
    template<class T> T computeKurtosis(const T* values, const Uint n, const T& mean, const T& sdev)
    {
      T kurt = static_cast<T>(0);
      Uint i;

      for(i = 0; i < n; ++i)
      {
        kurt += COMPUTE_FOURTH(((values[i] - mean) / sdev));
      }

      kurt = (kurt / n) - 3;

      return kurt;
    }

  }; // namespace StatUtils

}; // namespace ait

#endif // StatUtils_HPP


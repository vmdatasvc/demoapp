/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef Polygon_fwd_HPP
#define Polygon_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{
  
class Polygon;
typedef boost::shared_ptr<Polygon> PolygonPtr;

}; // namespace ait

#endif // TrajectoryEvents_fwd_HPP

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef Polynomial_HPP
#define Polynomial_HPP

#include "Polynomial_fwd.hpp"

//STL includes
#include <vector>

#include <math.h>

//TODO: temporarily disable annoying warnings
//#pragma warning( disable : 4275 )
//#include <complex>
#include <list>

namespace ait
{ 

class Polynomial
{

protected:
  class Complex
  {
  public:    

    double realPart() const {return mReal;}
    double imagPart() const {return mImag;}

    Complex() {};
    Complex(double r, double i)
    {
      mReal = r;
      mImag = i;
    }

    Complex operator+(const Complex& other) const
    {
      return Complex(mReal+other.mReal,mImag+other.mImag);
    }

    Complex& operator+=(const Complex& other)
    {
      mReal += other.mReal;
      mImag += other.mImag;

      return *this;
    }

    Complex operator-(const Complex& other) const
    {
      return Complex(mReal-other.mReal,mImag-other.mImag);
    }

    Complex& operator-=(const Complex& other)
    {
      mReal -= other.mReal;
      mImag -= other.mImag;

      return *this;
    }

    Complex operator*(const Complex& other) const
    {      
      return Complex(mReal*other.mReal - mImag*other.mImag, mImag*other.mReal + mReal*other.mImag);
    }

    Complex& operator*=(const Complex& other)
    {
      double newReal = mReal*other.mReal - mImag*other.mImag;
      double newImag = mImag*other.mReal + mReal*other.mImag;

      mReal = newReal;
      mImag = newImag;

      return *this;
    }

    //scalar
    Complex operator*(double other) const
    {      
      return Complex(mReal*other, mImag*other);
    }

    Complex& operator*=(double other)
    {
      mReal *= other;
      mImag *= other;

      return *this;
    }

    Complex conj()
    {
      return Complex(mReal, -mImag);
    }

    Complex operator/(const Complex& other) const
    {    
      double denom = other.mReal * other.mReal + other.mImag * other.mImag;

      double numReal = mReal*other.mReal + mImag*other.mImag;
      double numImag = mImag*other.mReal - mReal*other.mImag;

      return Complex(numReal/denom,numImag/denom);

    }   

    static Complex pow(const Complex& val, int exponent)
    {
      assert(exponent >= 0);

      if (exponent == 0)
        return Complex(1.0,0.0);

      Complex result = val;

      for (int i = 1; i < exponent; i++)
      {
        result *= val;
      }

      return result;      
    }

    static double abs(const Complex& val)
    {
      return sqrt(val.mReal * val.mReal + val.mImag * val.mImag);
    }

  protected:
    double mReal;
    double mImag;
  };

public:  

  // LIFECYCLE

  /**
   * Initialize a polynomial with a list of coefficients
   */
  Polynomial(const std::vector<double>& coeff);
  
  /**
   * Default constructor. Init must be used sometime in the future.
   */
  Polynomial() {};
  
  // OPERATIONS

  /**
   * Initialize the polynomial. This is only required for polynomials
   * instantiated via the default constructor. The coefficients are ordered
   * from greatest power to lowest. For example, [4,3,2,1] creates the
   * polynomial 4x^3 + 3x^2 + 2x + 1
   *
   */
  virtual void init(const std::vector<double>& coeff);

  /**
   * Initialize the polynomial. This is only required for polynomials
   * instantiated via the default constructor. The coefficients are ordered
   * from greatest power to lowest. For example, [4,3,2,1] creates the
   * polynomial 4x^3 + 3x^2 + 2x + 1
   *
   */
  virtual void init(const std::list<double>& coeff);
  
  /**
   * Generate p(x), where p is the polynomial function. The domain is the 
   * set of real numbers.
   */
  virtual double evaluate(double val) const;
  
  
  /**
   * This member function is an unecessary copy of evaluate(double)
   * because of some problems with MSVC 6.0 STL implementation.
   * Generates p(x), where p is the polynomial function. The domain
   * is the set of complex numbers.
   */  
  virtual Polynomial::Complex evaluate(const Polynomial::Complex& val) const;

  //TODO: DOC
  // Finds X for P(x) = val
  virtual double solve(double val) const;

  //ACCESS
  
  /**
   * Returns the monic version of this polynomial.
   */
  virtual Polynomial getMonic() const;


  /**
   * Returns a list of real roots satisfying the equation
   * p(x) = 0. For a given polynomial, there may or may not exist
   * a real root.
   */
  virtual std::vector<double> getRealRoots() const;

  /**  
   * Returns a list of roots satisfying the equation
   * p(x) = 0. The roots returned are all in complex form.
   */
  virtual std::vector<Complex> getRoots() const;  
  
protected:

  /**
   * The polynomial's coefficients. For example, [4,3,2,1] represents the
   * polynomial 4x^3 + 3x^2 + 2x + 1   
   */
  std::vector<double> mCoefficients;

  

};

}; // namespace ait

#endif // Polynomial_HPP

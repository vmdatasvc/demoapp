/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef StereoUtils_HPP
#define StereoUtils_HPP


//#define	sgn(x)	( (x) >= 0 ? 1 : -1 )

#define EPS 1.2e-6

// SYSTEM INCLUDES
//

//#include <vector>
//#include <algorithm>
//#include <boost/utility.hpp>
//#include <AitBaseTypes.hpp>

// AIT INCLUDES
//
#include <ait/Vector3.hpp>
#include <ait/Matrix.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

namespace ait 
{

  namespace StereoUtils
  {
    /**
    * 
    * 
    * @par Responsibilities:
    * 
    * @remarks
    */
    //template<class T> T computeKurtosis(const T* values, const Uint n, const T& mean, const T& sdev)
    //{
    //}

    /*void setCameraParameters(Matrix<float> projMatrix0, Matrix<float> projMatrix1, Matrix<float> camCenter0, Matrix<float> camCenter1)
    {
      mCamA0.crop(projMatrix0,0,0,2,2);
      mCamA0.inv();

      mCamA1.crop(projMatrix1,0,0,2,2);
      mCamA1.inv();

      mCamCenter0_M = camCenter0;
      mCamCenter1_M = camCenter1;
      mCamCenter0_V.set(mCamCenter0_M(0), mCamCenter0_M(1), mCamCenter0_M(2));
      mCamCenter1_V.set(mCamCenter1_M(0), mCamCenter1_M(1), mCamCenter1_M(2));
    }*/

    template<class T> bool get3DPointFrom2DPoints(Vector3<T> &pt2D_0, Vector3<T> &pt2D_1, Vector3<T>& pt3D)
    {      
      Vector3<T> pt0, pt1;

      // Calculate the 3D point of the 2D points on the retinal image plane of cam0 and cam1
      getRayThrough2DPoint(pt2D_0, mCamCenter0_M, mCamA0, pt0);
      getRayThrough2DPoint(pt2D_1, mCamCenter1_M, mCamA1, pt1);


      //calc new 3D ellbow point
      return get3DPointFromRays(mCamCenter0_V, pt0, mCamCenter1_V, pt1, pt3D);
    }



    template<class T> void getRayThrough2DPoint(const Vector3<T>& pt, const Matrix<T>& camCenter, const Matrix<T>& rotationMatrix, Vector3<T>& ray)
    {
      Matrix<T> P;
      Matrix<T> B(3,1);	
      B(0,0)=pt(0);
      B(1,0)=pt(1);
      B(2,0)=1.0;

      P.mult(rotationMatrix,B);	

      P.add(camCenter);
      ray.set(P(0,0), P(1,0), P(2,0));	
    }


    template<class T> bool get3DPointFromRays(const Vector3<T>& camCenter0, const Vector3<T>& ray0, const Vector3<T>& camCenter1, const Vector3<T>& ray1, Vector3<T>& pt)
    {
      Vector3<T> pa, pb;
      double mua, mub;

      if(calcLineIntersection3D(camCenter0,ray0, camCenter1, ray1, pa, pb, mua, mub))
      {
        pt=pa;
        pt.add(pb);
        pt.div(2.0);		
        if(mua<0 && mub>0)
        {
          //PVMSG("in get3DPointFromRays(): intersection of the two rays lies behind the camera center!\n");
          return false;
        }
      }
      else 
      {
        //PVMSG("in get3DPointFromRays(): intersection of the two rays failed (parallel?)\n");
        return false;
      }

      return true;
    }

    template<class T> bool calcLineIntersection3D(Vector3<T> p1, Vector3<T> p2, Vector3<T> p3, Vector3<T> p4, 
      Vector3<T>& pa, Vector3<T>& pb,
      double& mua, double& mub)
    {
      Vector3<T> p13,p43,p21;
      double d1343,d4321,d1321,d4343,d2121;
      double numer,denom;

      p13.x = p1.x - p3.x;
      p13.y = p1.y - p3.y;
      p13.z = p1.z - p3.z;
      p43.x = p4.x - p3.x;
      p43.y = p4.y - p3.y;
      p43.z = p4.z - p3.z;
      if ((fabs(p43.x) < EPS && fabs(p43.y) < EPS && fabs(p43.z) < EPS))
				return false;
 
			p21.x = p2.x - p1.x;
      p21.y = p2.y - p1.y;
      p21.z = p2.z - p1.z;
      if ((fabs(p21.x)  < EPS && fabs(p21.y)  < EPS && fabs(p21.z)  < EPS))
				return false;

      d1343 = p13.x * p43.x + p13.y * p43.y + p13.z * p43.z;
      d4321 = p43.x * p21.x + p43.y * p21.y + p43.z * p21.z;
      d1321 = p13.x * p21.x + p13.y * p21.y + p13.z * p21.z;
      d4343 = p43.x * p43.x + p43.y * p43.y + p43.z * p43.z;
      d2121 = p21.x * p21.x + p21.y * p21.y + p21.z * p21.z;

      denom = d2121 * d4343 - d4321 * d4321;
      if (fabs(denom) < EPS)
				return false;

      numer = d1343 * d4321 - d1321 * d4343;

      mua = numer / denom;
      mub = (d1343 + d4321 * mua) / d4343;

      pa.x = p1.x + (float) mua * p21.x;
      pa.y = p1.y + (float) mua * p21.y;
      pa.z = p1.z + (float) mua * p21.z;
      pb.x = p3.x + (float) mub * p43.x;
      pb.y = p3.y + (float) mub * p43.y;
      pb.z = p3.z + (float) mub * p43.z;

			return true;
    }
  
    template<class T> bool calcLinePlaneIntersection(Vector3<T> p1, Vector3<T> n1, Vector3<T> p2, Vector3<T> n2, Vector3<T>& ptIntersect)     
    {
      Vector3<T> n;
      Vector3<T> w;
      w.sub(p1,p2);

      T det = n2.dot_prod(n1);
      
      if(det==0)
        return false;
      
      T s = -n2.dot_prod(w)/det;

      ptIntersect=p1;
      n=n1;
      n.mult(s);
      ptIntersect.add(n);
      
      return true;
    }

    

    /**
    * Project a point from 3D to 2D using the given homogenous projection matrix.
    * @remark p2D must be of size 3, although the 3rd value is invalid.
    */  
    template<class T> void projectPoint(T* p3D, const Matrix<T>& P, T* p2D)
    {  
      p2D[0] = P(0,0)*p3D[0] + P(0,1)*p3D[1] + P(0,2)*p3D[2] + P(0,3);
      p2D[1] = P(1,0)*p3D[0] + P(1,1)*p3D[1] + P(1,2)*p3D[2] + P(1,3);
      p2D[2] = P(2,0)*p3D[0] + P(2,1)*p3D[1] + P(2,2)*p3D[2] + P(2,3);

      p2D[0] /= p2D[2];
      p2D[1] /= p2D[2];
    }  

  }; // namespace StereoUtils

}; // namespace ait

#endif // StereoUtils_HPP


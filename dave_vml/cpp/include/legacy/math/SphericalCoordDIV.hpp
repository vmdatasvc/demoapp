/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef SphericalCoordDIV_HPP
#define SphericalCoordDIV_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>
#include <math.h>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>

// LOCAL INCLUDES
//
#include <legacy/pv/ait/Vector3.hpp>

// FORWARD REFERENCES
//


namespace ait 
{


/**
 * Transform to and from a cubical volume to a bended cube.
 * See interactive_volume_mapping.doc for documentation on what 
 * this class does.
 */
template<class T> class SphericalCoordDIV
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  SphericalCoordDIV() : mIsInitialized(false) {};

  /**
   * Destructor
   */
  virtual ~SphericalCoordDIV() {};

  /**
   * Initialization.
   */
  void init(const Vector3<T>& sphereCenter,
            const Vector3<T>& cuboidMin,
            const Vector3<T>& cuboidMax)
  {
    PVASSERT(cuboidMin(0) < cuboidMax(0) && 
             cuboidMin(1) < cuboidMax(1) &&
             cuboidMin(2) < cuboidMax(2));
    mSphereCenter = sphereCenter;
	mSphereRotation.set(0,0);
    mCuboidMin = cuboidMin;
    mCuboidMax = cuboidMax;

    Vector3<T> pivot0(mCuboidMax);
    pivot0.sub(mSphereCenter);
    mSphereMin = cartesianToSpherical(pivot0);

    Vector3<T> pivot1(mCuboidMin(0),mCuboidMin(1),mCuboidMax(2));
    pivot1.sub(mSphereCenter);
    mSphereMax = cartesianToSpherical(pivot1);

    mSphereMax(0) = mSphereMin(0)+mCuboidMax(2) - mCuboidMin(2);

    pivot0 = mCuboidMax;
    pivot1.set(mCuboidMin(0),mCuboidMin(1),mCuboidMax(2));
    Vector3<T> test0 = cartesianToNormSpherical(pivot0);
    Vector3<T> test00 = normSphericalToCartesian(test0);
    Vector3<T> test1 = cartesianToNormSpherical(pivot1);
    Vector3<T> test11 = cartesianToNormSpherical(test1);

    mIsInitialized = true;
  }

  
 /**
   * Initialization.
   */
  void init(const Vector3<T>& sphereCenter, 
			const Vector3<T>& sphereRotation, 
            const Vector3<T>& sphereMin,
            const Vector3<T>& sphereMax)
  {
    PVASSERT(sphereMin(0) < sphereMax(0) && 
             sphereMin(1) < sphereMax(1) &&
             sphereMin(2) < sphereMax(2));
    mSphereCenter = sphereCenter;
	mSphereRotation = sphereRotation;

	mSphereMin=sphereMin;
	mSphereMax=sphereMax;

	mSinAlpha0 = sin(mSphereRotation(0));
	mCosAlpha0 = cos(mSphereRotation(0));
	mSinBeta0 = sin(mSphereRotation(1)); 
	mCosBeta0 = cos(mSphereRotation(1));

	mSinAlpha1 = sin(-mSphereRotation(0));
	mCosAlpha1 = cos(-mSphereRotation(0));
	mSinBeta1 = sin(-mSphereRotation(1)); 
	mCosBeta1 = cos(-mSphereRotation(1));

/*
    Vector3<T> pivot0(0,0,0);
    Vector3<T> pivot1(1,1,1);
    Vector3<T> pivot2(0.5,0.25,0.48);

	Vector3<T> test0 = cartesianToNormSpherical(pivot0);
    Vector3<T> test00 = normSphericalToCartesian(test0);
    
	Vector3<T> test1 = cartesianToNormSpherical(pivot1);
    Vector3<T> test11 = normSphericalToCartesian(test1);	
	
	Vector3<T> test2 = cartesianToNormSpherical(pivot2);
    Vector3<T> test22 = normSphericalToCartesian(test2);	
*/	
  }
  //
  // OPERATIONS
  //

public:

  /**
   * Transform from cartesian (x,y,z) to spherical (r,theta,phi) 
   * coordinate system, applying the axis interchange described in the
   * documentation to avoid singularities.
   */
  static Vector3<T> cartesianToSpherical(const Vector3<T>& P)
  {
    const T r = sqrt(P(0)*P(0) + P(1)*P(1) + P(2)*P(2));
    return Vector3<T>(r,acos(P(1)/r),atan2(-P(2),P(0)));
  }

  /**
   * Transform from spherical (r,theta,phi) to cartesian (x,y,z) 
   * coordinate system.
   */
  static Vector3<T>  sphericalToCartesian(const Vector3<T>& S)
  {
    const T sinTheta = sin(S(1));
    const T cosTheta = cos(S(1));
    const T sinPhi = sin(S(2));
    const T cosPhi = cos(S(2));
    return Vector3<T>(
      S(0)*sinTheta*cosPhi,
      S(0)*cosTheta,
      -S(0)*sinTheta*sinPhi);
  }

   static Vector3<T>  sphericalToCartesian2(const Vector3<T>& S)
  {
    const T sinTheta = sin(S(1));
    const T cosTheta = cos(S(1));
    const T sinPhi = sin(S(2));
    const T cosPhi = cos(S(2));
    return Vector3<T>(
      S(0)*sinTheta*cosPhi,
      S(0)*sinTheta*sinPhi,
	  S(0)*cosTheta);
  }

  /**
   * Convert from cartesian to normalized spherical.
   */
  Vector3<T> cartesianToNormSpherical(const Vector3<T>& P)
  {

    // Translate point to put the sphere center in the origin.
    Vector3<T> P1(P(0)-mSphereCenter(0),
                   P(1)-mSphereCenter(1),
                   P(2)-mSphereCenter(2));	

	// Rotate point to allign with cuboid major orientation
	Vector3<T> P2(mCosAlpha0*P1.x-mSinAlpha0*P1.z,
					-mSinBeta0*mSinAlpha0*P1.x+mCosBeta0*P1.y-mSinBeta0*mCosAlpha0*P1.z,
					+mCosBeta0*mSinAlpha0*P1.x+mSinBeta0*P1.y+mCosBeta0*mCosAlpha0*P1.z);

    // Convert to spherical coords.
    Vector3<T> S1 = cartesianToSpherical(P2);

    // Normalize and return
    /*return Vector3<T>(1-(S1(0)-mSphereMin(0))/(mSphereMax(0)-mSphereMin(0)),
                       (S1(1)-mSphereMin(1))/(mSphereMax(1)-mSphereMin(1)),
                       1-(S1(2)-mSphereMin(2))/(mSphereMax(2)-mSphereMin(2)));
	*/
	
	return Vector3<T>((mSphereMax(0)-S1(0))/(mSphereMax(0)-mSphereMin(0)),
                       (S1(1)-mSphereMin(1))/(mSphereMax(1)-mSphereMin(1)),
                       (-S1(2)-mSphereMin(2))/(mSphereMax(2)-mSphereMin(2)));
  }

  /**
   * Convert from normalized spherical to cartesian.
   */
  Vector3<T> normSphericalToCartesian(const Vector3<T>& S)
  {
    // De-normalize.
    Vector3<T> S1(mSphereMax(0)-S(0)*(mSphereMax(0)-mSphereMin(0)),
                   mSphereMin(1)+S(1)*(mSphereMax(1)-mSphereMin(1)),
                   -(mSphereMin(2)+S(2)*(mSphereMax(2)-mSphereMin(2))));


    // Convert to cartesian coords.
    Vector3<T> P1(sphericalToCartesian(S1));


	// Rotate point to allign with cuboid major orientation
	Vector3<T> P2(mCosAlpha1*P1.x-mSinAlpha1*P1.z,
					-mSinBeta1*mSinAlpha1*P1.x+mCosBeta1*P1.y-mSinBeta1*mCosAlpha1*P1.z,
					+mCosBeta1*mSinAlpha1*P1.x+mSinBeta1*P1.y+mCosBeta1*mCosAlpha1*P1.z);

	
    // Translate to the original position
    Vector3<T> P3(P2(0)+mSphereCenter(0),
					P2(1)+mSphereCenter(1),
					P2(2)+mSphereCenter(2));

	return P3;

  }

  Vector3<T> center()
  {
	  return mSphereCenter;
  }

  Vector3<T> orientation()
  {
	  return mSphereRotation;
  }

  void reset(const Vector3<T>& sphereCenter, 
			const Vector3<T>& sphereRotation)
  {
    mSphereCenter = sphereCenter;
	mSphereRotation = sphereRotation;
 
	mSphereCenter = sphereCenter;
	mSphereRotation = sphereRotation;
	
	mSinAlpha0 = sin(mSphereRotation(0));
	mCosAlpha0 = cos(mSphereRotation(0));
	mSinBeta0 = sin(mSphereRotation(1)); 
	mCosBeta0 = cos(mSphereRotation(1));

	mSinAlpha1 = sin(-mSphereRotation(0));
	mCosAlpha1 = cos(-mSphereRotation(0));
	mSinBeta1 = sin(-mSphereRotation(1)); 
	mCosBeta1 = cos(-mSphereRotation(1));

  }
  
  //
  // ACCESS
  //

public:


  //
  // INQUIRY
  //

public:

  /**
   * Ask if this object has been initialized (the init() function has been called).
   */
  Bool isInitialized() const { return mIsInitialized; }


  //
  // ATTRIBUTES
  //

protected:

  /**
   * Extent of the 3D cuboid in cartesian coordinates. V0,V1 in the documentation.
   */
  Vector3<T> mCuboidMin, mCuboidMax;

  /**
   * Center of the sphere in cartesian coordinates. C in the documentation.
   */
  Vector3<T> mSphereCenter;

  
  /**
   * Orientation (Rotation) of cuboid in cartesian coordinates. not in documentation yet.
   */
  Vector3<T> mSphereRotation;


  /**
   * Extent of the bended cuboid in spherical coordinates. S0,S1 in the 
   * documentation.
   */
  Vector3<T> mSphereMin, mSphereMax;

  bool	mIsInitialized;

private:

	T mSinAlpha0;
	T mCosAlpha0;
	T mSinBeta0;
	T mCosBeta0;

	T mSinAlpha1;
	T mCosAlpha1;
	T mSinBeta1;
	T mCosBeta1;

};

}; // namespace ait

#endif // SphericalCoord_HPP


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef Homography2D_fwd_HPP
#define Homography2D_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class Homography2D;
typedef boost::shared_ptr<Homography2D> Homography2DPtr;

}; // namespace ait

#endif // Homography2D_fwd_HPP


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef matalgo_HPP
#define matalgo_HPP

#include <cmath>
#include <legacy/pv/ait/Matrix.hpp>

namespace ait
{

/**
 * Return the square of the euclidean distance between m1 and m2, 
 * interpreted as 1 dimensional vectors.
 */
template <class T> double dist2(const Matrix<T>& m1, const Matrix<T>& m2)
{
  if (m1.size() != m2.size()) PvUtil::exitError("Matrix dimensions do not match");

  double d = 0;
  for (int i = 0; i < m1.size(); i++)
  {
    d += (m1(i)-m2(i))*(m1(i)-m2(i));
  }
  return d;
}

/**
 * Return the the euclidean distance between m1 and m2, 
 * interpreted as 1 dimensional vectors.
 */
template <class T> double dist(const Matrix<T>& m1, const Matrix<T>& m2)
{
  return std::sqrt(dist2(m1,m2));
}

}; // namespace ait

#endif // matalgo_HPP

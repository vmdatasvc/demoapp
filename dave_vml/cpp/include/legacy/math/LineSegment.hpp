/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/


#ifndef LineSegment_HPP
#define LineSegment_HPP

//
// FORWARD INCLUDES
//

#include <legacy/math/LineSegment_fwd.hpp>
#include <legacy/pv/ait/Image.hpp>
#include <legacy/pv/ait/Vector3.hpp>

namespace ait
{

/**
 * LineSegment provides a higher level interface for dealing with
 * line segments. Segments have two points, a starting and end point.
 * The order of these two points is important for determining 
 * relative positions and when using the peturbed intersects() function.
 *
 * @Responsibilities:
 *   - Convenience functions:
 *      - Do two segments intersect?
 *      - What is the distance from a point to a segment?
 *	
 * @remarks
 * The intersects() member function which takes a segment and a point
 * as parameters finds a 'peturbed' intersection. See the functions
 * documentation below. Also, ()'s can be used to retreive  and change 
 * the endpoints that compose the segment.
 *  
 */
class LineSegment
{
  //
  // LIFECYCLE
  //

public:
  /**
   *	Default constructor, constructs a segment from (0,0) to (1,1)
   */
  LineSegment() : mStartPoint(Vector2f(0.0,0.0)), 
    mEndPoint(Vector2f(1.0,1.0)) {};

  /**
   * Constructor - constructs a segment from a to b	
   */
  LineSegment(const Vector2f& a, const Vector2f& b) : 
    mStartPoint(a), mEndPoint(b) {};

  //using default copy constructor

  //
  // OPERATORS
  //   
  
  Vector2f operator()(int index) const
  {
    assert(index == 0 || index == 1);
    if (index == 0) return mStartPoint;
    else return mEndPoint;    
  }

  Vector2f& operator()(int index)
  {
    assert(index == 0 || index == 1);
    if (index == 0) return mStartPoint;
    else return mEndPoint;    
  }

  /**
   * Compares whether or not two segments are equal. NOTE:
   * the starting and endpoints of a segment must be ordered
   * identically. e.g. (0,0)-(1,1) != (1,1)-(0,0)
   */
  bool operator==(const LineSegment& other) const
  {
    return (other(0) == mStartPoint && other(1) == mEndPoint);
  }

  bool operator!=(const LineSegment& other) const
  {
    return !operator==(other);
  }
  
  //
  // OPERATIONS
  //

  enum
  {
    POINT_LEFT,
    POINT_RIGHT,
    POINT_FORWARD_EXTENSION,
    POINT_BACKWARD_EXTENSION,
    POINT_ON_SEGMENT
  };

  /**
   * Draws the line segment to the given RGBImage. Used primarily for
   * testing/visualization.
   * @param img [out] The image to draw to
   * @param color [in] The color to draw the segment, default is black
   */

  void draw(Image8& img, unsigned int color = 0) const;
  void draw(Image32& img, unsigned int color = PV_RGB(0,0,0)) const;

  /**
   * Returns the distance to the given point, and the points
   * relative position to the segment, assuming that one is travelling from
   * the startpoint (first point fed to constructor) to the endpoint.
   *
   * Possible relative positions:
   * LineSegment.POINT_LEFT - left of the segment
   * LineSegment.POINT_RIGHT - right of the segment
   * LineSegment.POINT_FORWARD_EXTENSION - same line, in front
   * LineSegment.POINT_BACKWARD_EXTENSION - same line, behind
   * LineSegment.POINT_ON_SEGMENT - point rests on the segment
   *
   * @param point [in] The point to consider
   * @param relativePosition [out] is set to one of the above-mentioned
   * values
   */
  float distanceToPoint(const Vector2f& point, int& relativePosition) const;

  /**
   * Returns the length of the segment	
   */
  float length()
  {
    return sqrt(dist2(mStartPoint, mEndPoint));
  }

  /**
   * Returns the coordinate of the point on the line segment closest to the
   * given point. 
   */
  Vector2f closestPoint(const Vector2f& p);

  /**
   * Returns the midpoint of the line segment
   */
  Vector2f midpoint() const;
  
  //
  // INQUIRY
  //  

  /**
   * This function is used to determine or not two segments intersect
   * each other. This function does not check for a true intersection, but
   * rather 'peturbs' points slightly to eliminate all degenerate cases.
   * 
   * At least on of other's endpoints must lie to the 'left' of *this for
   * it to be considered. This constraint is necessary to eliminate all
   * degenerate cases. Thusly, overlapping segments will return false.
   *
   *
   * @param other [in] The segment to test against
   * @param intersectPoint[out] This variable is set to the intersection
   * point if it exists, otherwise it is unchanged.
   * @return true if a nondegenerate intersection occurs, false otherwise
   */
  bool intersects(const LineSegment& other, Vector2f& intersectPoint) const;

  //
  // ATTRIBUTES
  //

protected:
  /**
   * The starting point of a segment
   */
  Vector2f mStartPoint;
  /**
   * The ending point of a segment	
   */
  Vector2f mEndPoint;
};

}; //namespace ait


#endif // LineSegment_HPP

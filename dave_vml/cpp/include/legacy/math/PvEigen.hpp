/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
#ifndef PVEIGEN_HPP
#define PVEIGEN_HPP

#include "PvUtil.hpp"
#include <ait/Matrix.hpp>

namespace ait
{

template<class T> class PvEigen
{
private:
  static void jacobi(Matrix<T> &a, Matrix<T> &d,
		      Matrix<T> &v, unsigned int *nrot);

  static void eigsrt(Matrix<T> &d, Matrix<T> &v);

public:
  static unsigned int eigenRealSymm(Matrix<T>& a,Matrix<T>& v,Matrix<T>& d);

};

template<class T> void PvEigen<T>::jacobi(Matrix<T> &a, Matrix<T> &d,
					  Matrix<T> &v, unsigned int *nrot)
{
#define ROTATE(a,i,j,k,l) g=a(i,j);h=a(k,l);a(i,j)=g-s*(h+g*tau);\
	                  a(k,l)=h+s*(g-h*tau)

  int n,j,iq,ip,i;
  T tresh,theta,tau,t,sm,s,h,g,c,*b,*z;

  n=a.rows();
  if(n!=a.cols())
    {
      PvUtil::exitError("PvEigen.jacobi: Error, matrix not squared.");
    }

  b=new T [n];
  z=new T [n];

  for (ip=0;ip<n;ip++) 
    {
      for (iq=0;iq<n;iq++) v(ip,iq)=0.0;
      v(ip,ip)=1.0;
    }

  for (ip=0;ip<n;ip++) 
    {
      b[ip]=d(0,ip)=a(ip,ip);
      z[ip]=0.0;
    }

  *nrot=0;
  for (i=0;i<50;i++) 
    {
      sm=0.0;
      for (ip=0;ip<n-1;ip++) 
	{
	  for (iq=ip+1;iq<n;iq++) sm += fabs(a(ip,iq));
	}
      if (sm == 0.0) 
	{
	  delete [] z;
	  delete [] b;
	  return;
	}
      if (i < 3)
	tresh=0.2*sm/(n*n);
      else
	tresh=0.0;
    for (ip=0;ip<n-1;ip++) 
      {
	for (iq=ip+1;iq<n;iq++) 
	  {
	    g=100.0*fabs(a(ip,iq));
	    if (i > 3 && (T)(fabs(d(0,ip))+g) == (T)fabs(d(0,ip))
		&& (T)(fabs(d(0,iq))+g) == (T)fabs(d(0,iq)))
	      a(ip,iq)=0.0;
	    else if (fabs(a(ip,iq)) > tresh) {
	      h=d(0,iq)-d(0,ip);
	      if ((T)(fabs(h)+g) == (T)fabs(h))
		t=(a(ip,iq))/h;
	      else {
		theta=0.5*h/(a(ip,iq));
		t=1.0/(fabs(theta)+sqrt(1.0+theta*theta));
		if (theta < 0.0) t = -t;
	      }
	      c=1.0/sqrt(1+t*t);
	      s=t*c;
	      tau=s/(1.0+c);
	      h=t*a(ip,iq);
	      z[ip] -= h;
	      z[iq] += h;
	      d(0,ip) -= h;
	      d(0,iq) += h;
	      a(ip,iq)=0.0;
	      for (j=0;j<=ip-1;j++) 
		{
		  ROTATE(a,j,ip,j,iq);
		}
	      for (j=ip+1;j<=iq-1;j++) 
		{
		  ROTATE(a,ip,j,j,iq);
		}
	      for (j=iq+1;j<n;j++) 
		{
		  ROTATE(a,ip,j,iq,j);
		}
	      for (j=0;j<n;j++) 
		{
		  ROTATE(v,j,ip,j,iq);
		}
	      ++(*nrot);
	    }
	  }
      }
    for (ip=0;ip<n;ip++) 
      {
	b[ip] += z[ip];
	d(0,ip)=b[ip];
	z[ip]=0.0;
      }
    }
  PvUtil::exitError("PvEigen.jacobi: Error, too many iterations.");
#undef ROTATE
}

template<class T> void PvEigen<T>::eigsrt(Matrix<T> &d, Matrix<T> &v)
{
  int n=d.cols();
  int k,j,i;
  T p;
  
  for (i=0;i<n-1;i++) 
    {
      p=d(0,k=i);
      for (j=i+1;j<n;j++) if (d(0,j) >= p) p=d(0,k=j);
      if (k != i) 
	{
	  d(0,k)=d(0,i);
	  d(0,i)=p;
	  for (j=0;j<n;j++) 
	    {
	      p=v(j,i);
	      v(j,i)=v(j,k);
	      v(j,k)=p;
	    }
	}
    }
}

template<class T> unsigned int PvEigen<T>::eigenRealSymm(Matrix<T>& a,Matrix<T>& v,Matrix<T>& d)
{
  unsigned int rows,cols;
  unsigned int nrot;

  rows=a.rows(); 
  cols=a.cols();

  if(rows!=cols)
    {
      PvUtil::exitError("PvEigen.eigenRealSymm: Error, matrix not symmetric.");
    }

  v.resize(rows,rows);
  d.resize(1,rows);
  
  jacobi(a,d,v,&nrot);
  eigsrt(d,v);

  return nrot;
}

} // namespace ait

#endif


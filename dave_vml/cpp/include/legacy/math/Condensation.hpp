/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef Condensation_HPP
#define Condensation_HPP

#include <vector>
#include <legacy/pv/ait/Matrix.hpp>
#include <legacy/pv/PvRandom.hpp>

#include <cstring>

namespace ait
{

/**
 * This class implements the condensation algorithm. See M. Isard and A. Blake "CONDENSATION - conditional 
 * density propagation for visual tracking", Int. J. Computer Vision, 1998.
 */
template<class T> class Condensation
{
public:

  /**
   * Constructor.
   */
  Condensation() 
  : nDim(0), 
    nParticles(0), 
    pPriorValues(NULL), 
    pPostValues(NULL), 
    pPrior(NULL), 
    pPosterior(NULL), 
    mNumPhases(1) ,
		mAccumWeight(0)
  {
  }

  /**
   * Destructor.
   */
  ~Condensation() 
  { 
    initialize(0,0); 
  }

  /**
   * Set the parameters for the algorithm.
   * @param nParticles [in] number of particles to track.
   * @param A [in] matrix defining the deterministic dynamics of the system for prediction.
   * @param B [in] matrix defining the noise distribution.
   * @param numEvalPhases [in] number of iterations to evaluate the function.
   * Usually this is needed to precompute ranges, or normalize partial results before
   * computing the final value.
   */
  void setParams(int nParticles0, const Matrix<T>& A0, const Matrix<T>& B0, int numEvalPhases = 1);

  /**
   * Set the parameters for the algorithm. Use this variation when no matrices are used for the
   * noise and prediction model. Usually done by inherited classes that override the predict()
   * step.
   * @param nParticles [in] number of particles to track.
   * @param dimension [in] The dimension of the vector that we will optimize.
   * @param numEvalPhases [in] number of iterations to evaluate the function.
   * Usually this is needed to precompute ranges, or normalize partial results before
   * computing the final value.
   */
  void setParams(int nParticles0, int nDimension, int numEvalPhases = 1);

  /**
   * Perform one iteration of the algorithm.
   * @param x [in] initial value of the algorithm.
   * @param x [out] result of the iteration.
   * @param funk [in] function that weights each of the particles. The format of this function is
   * myEvalFunction(T *particleValues, void *objectPointer).
   * @param ptr [in] pointer that will be passed to the function, can be used to pass the instance pointer.
   * @remarks On the first step (or after calling reset()), the parameter x is used as a seed particle. All
   * particles will spread from this one. The function funk will be called once per particle, with the
   * first parameter as a pointer to the status vector values.
   */
  void step(Matrix<T> &x, T (*funk)(T *, void *), void *ptr)
  {
    step(x,funk,NULL,ptr);
  }

  /**
   * Perform one iteration of the algorithm.
   * @param x [in] initial value of the algorithm.
   * @param x [out] result of the iteration.
   * @param funk [in] function that weights each of the particles. The format of this function is
   * myEvalFunction(T *particleValues, int particleIndex, int phaseIndex, void *objectPointer).
   * The particle index is the index of the particle that is currently being evaluated; the phase
   * index is the index of the evaluation phase.
   * @param ptr [in] pointer that will be passed to the function, can be used to pass the instance pointer.
   * @remarks On the first step (or after calling reset()), the parameter x is used as a seed particle. All
   * particles will spread from this one. The function funk will be called once per particle, with the
   * first parameter as a pointer to the status vector values.
   */
  void step(Matrix<T> &x, T (*funk)(T *, int, int, void *), void *ptr)
  {
    step(x,NULL,funk,ptr);
  }

  /// Reset the particles.
  virtual void reset() { isFirstStep = true; }

  /// Return the number of particles
  int getNumParticles() { return nParticles; }

  /// Returns the dimension of the state vector.
  int getDimension() { return nDim; }

  /// Returns a pointer to a particle vector values.
  T *getParticle(int i) { return pPostValues+i*nDim; }

  /// Returns the weight of a particle.
  T getParticleWeight(int i) { return pPosterior[i]; }

  //Set the weight of the particle
  void setParticleWeight(int i, T particleWeight){pPrior[i]= particleWeight;}

	// Returns the evaluation weight of all particles
	T getAccumEvalWeight() {return mAccumWeight;}

  /// Returns the variance of the particle system.
  Matrix<T>& getVariance() { return var; }

  /// Returns the matrix B given in setParams().
  Matrix<T>& getNoiseCoVariance() { return B; }

  /// Return the number of phases per iteration step.
  int getNumPhases() { return mNumPhases; }

  /**
   * Indicate if all weights are zero after executing the step.
   */
  bool areAllWeightsZero() { return mAllWeightsAreZero; }
  
protected:

  /// Dimension of the problem
  int nDim;

  /// Number of particles for the condensation
  int nParticles;

  /// Values of the particles
  T *pPriorValues;

  /// Values of the particles
  T *pPostValues;

  /// Priori weights
  T *pPrior;

  /// Posteriori weights
  T *pPosterior;

	/// accum. eval. weight of all particles
	T mAccumWeight;

  /// Posteriori Cumulative Weight Values
  T *pCumProb;

  /// Coefficient matrices for prediction
  Matrix<T> A,B;

  /// Random Number generators
  PvRandom uniRandom, gaussRandom;

  /// Pre-generated uniform random numbers
  std::vector<T> uniRandomValues;

  /// Pre-generated gaussian random numbers
  std::vector<T> gaussRandomValues;
  int curGaussValue;

  /// First step flag
  bool isFirstStep;

  /// Value of the last result
  Matrix<T> lastResult;

  /// Value of the 2nd order variance.
  Matrix<T> var;

  /// Number of phases per iteration step.
  int mNumPhases;

  /// Predict the values of each particle using the dynamic information (matrices A and B).
  virtual void predict();

  /**
   * Variable that indicates if all the particle weights are equal to zero.
   */
  bool mAllWeightsAreZero;

private:

  /**
   * For efficiency this class precomputes a set of random values in a large array.
   * this will save some cycles from the PvRandom class.
   */
  void precomputeRandomValues();


private:

  /**
   * Initialize the algorithm
   * @param nParticles [in] Number of particles to track
   * @param nDim [in] Dimension of the state vector
   */
  void initialize(int nParticles0, int nDim0);

  void binarySearch();

  /// Step prototype.
  void step(Matrix<T> &x, T (*funk)(T *, void *), 
                            T (*funkPhase)(T *, int, int, void *), 
                            void *ptr);

};

template<class T> void Condensation<T>::initialize(int nParticles0, int nDim0)
{
  if (nParticles != nParticles0 || nDim != nDim0)
  {
    if (pPriorValues)
    {
      delete [] pPriorValues; pPriorValues = NULL;
      delete [] pPostValues; pPostValues = NULL;
      delete [] pPrior; pPrior = NULL;
      delete [] pPosterior; pPosterior = NULL;
      delete [] pCumProb; pCumProb = NULL;
    }
  }

  nParticles = nParticles0;
  nDim = nDim0;

  if (!nParticles || !nDim)
  {
    return;
  }

  if (!pPriorValues)
  {
    pPriorValues = new T[nParticles*nDim];
    pPostValues = new T[nParticles*nDim];
    pPrior = new T[nParticles];
    pPosterior = new T[nParticles];
    pCumProb = new T[nParticles+1];
  }

  isFirstStep = true;

}

template<class T> void Condensation<T>::precomputeRandomValues()
{
  // Generate Uniform Random Numbers
  uniRandomValues.resize(nParticles);
  int i;
  for (i = 0; i < uniRandomValues.size(); i++)
  {
    uniRandomValues[i] = (T)uniRandom.uniform0to1();
  }
  
  // Generate Gaussian Random Numbers
  gaussRandomValues.resize(nParticles*nDim+1);
  for (i = 0; i < gaussRandomValues.size(); i++)
  {
    gaussRandomValues[i] = (T)gaussRandom.gaussRand();
  }
  curGaussValue = 0;
}

template<class T> void Condensation<T>::setParams(int nParticles0, const Matrix<T>& A0, const Matrix<T>& B0, int numEvalPhases)
{
  initialize(nParticles0,A0.rows());

  A = A0;
  B = B0;

  mNumPhases = numEvalPhases;

  precomputeRandomValues();
}

template<class T> void Condensation<T>::setParams(int nParticles0, int nDimension, int numEvalPhases)
{
  initialize(nParticles0,nDimension);
  mNumPhases = numEvalPhases;

  precomputeRandomValues();
}

template<class T> void Condensation<T>::step(Matrix<T> &x,
                                             T (*funk)(T *, void *), 
                                             T (*funkPhase)(T *, int, int, void *), 
                                             void *ptr)
{

  int i,j;

  mAllWeightsAreZero = false;

  if (isFirstStep)
  {
    for (i = 0; i < nParticles; i++)
    {
      pPrior[i] = i == 0 ? 1.0f : 0;
    }
    memcpy(pPriorValues,x.pointer(),sizeof(T)*nDim);
  }
  else
  {
    // Swap pointers
    T *pTmp = pPriorValues;
    pPriorValues = pPostValues;
    pPostValues = pTmp;

    pTmp = pPrior;
    pPrior = pPosterior;
    pPosterior = pTmp;
  }

  // Calculate cumulative from prior
  pCumProb[nParticles] = 1;
  for (i = nParticles-1; i>=0; i--)
  {
    pCumProb[i] = pCumProb[i+1]-pPrior[i];
    if (pCumProb[i] < 0) pCumProb[i] = 0;
  }
  pCumProb[0]=0;
  pCumProb[nParticles]=1.1f;

  // Calculate the new position for the samples.
  binarySearch();

  // Predict the new values for the particles
  predict();

  // Measure and weight the new position
  T accumNorm = 0;

  // Call the evaluation function mNumPhases times.
  for (j = 0; j < mNumPhases; j++)
  {
    accumNorm = 0;
    for (i = 0; i < nParticles; i++)
    {
      // Depending on the prototype used, a different function
      // will be used.
      if (funk)
      {
        pPosterior[i] = funk(pPostValues+i*nDim,ptr);
      }
      else
      {
        pPosterior[i] = funkPhase(pPostValues+i*nDim,i,j,ptr);
      }
      accumNorm += pPosterior[i];
    }
  }

	mAccumWeight=accumNorm;

  // Normalize
  if (accumNorm != 0)
  {
    for (i = 0; i < nParticles; i++)
    {
      pPosterior[i] = pPosterior[i]/accumNorm;
    }
  }
  else
  {
    mAllWeightsAreZero = true;
    T p = ((T)1)/nParticles;
    for (i = 0; i < nParticles; i++)
    {
      pPosterior[i] = p;
    }
  }

  // Estimate the new position (weighted mean, 1st moment)
  x.setAll(0);
  T *pX = x.pointer();
  for (i = 0; i < nParticles; i++)
  {
    for (j = 0; j < nDim; j++)
    {
      pX[j] += pPosterior[i]*pPostValues[i*nDim+j];
    }
  }

  // Estimate the variance (2nd moment)
  var.resize(nDim,1);
  var.setAll(0);
  for (i = 0; i < nParticles; i++)
  {
    for (j = 0; j < nDim; j++)
    {
      //      TElem tmp = x(j)-pPostValues[i*nDim+j];
      T tmp = x(j)-pPostValues[i*nDim+j];
      var(j) += pPosterior[i]*tmp*tmp;
    }
  }

  // Copy last result.
  lastResult.resize(nDim,1);
  for (j = 0; j < nDim; j++)
  {
    lastResult(j) = pX[j];
  }

  if (isFirstStep) isFirstStep = false;
}

template<class T> void Condensation<T>::binarySearch()
{
  int i,j;
  int left,right;
  T r;

  for(i=0;i<nParticles;i++)
  {
    r=uniRandomValues[i]; //(T)uniRandom.uniform0to1();

    left=0;
    right=nParticles;

    // Invariant: pCumProb(left)<=r && pCumProb(right)>r

    while(right>left+1)
    {
      j=(left+right)/2;

      if(pCumProb[j]<=r) left=j;
      if(pCumProb[j]>r) right=j;
    }
  
    j=left;

    if(pCumProb[j]>r) j--;

    // Set new values
    memcpy(pPostValues+i*nDim,pPriorValues+j*nDim,sizeof(T)*nDim);
    pPosterior[i] = pPrior[j];
    // Note that now the posterior array will hold the prior values in the new position.
  }
}

template<class T> void Condensation<T>::predict()
{
  Matrix<T> T0,T1,T2,xNew;
  int i,j;

  T1.resize(B.rows(),1);

  for (i = 0; i < nParticles; i++)
  {
    T0.mx_fromarray(pPostValues+i*nDim,nDim,1);

    xNew.mult(A,T0);

    for(j=0;j<T1.rows();j++)
    {
      T1(j)=gaussRandomValues[curGaussValue++];//gaussRandom.gaussRand();
      if (curGaussValue == gaussRandomValues.size()) curGaussValue = 0;
    }

    if (isFirstStep)
    {
      // Spread particles by multiplying the noise.
      T1.mult(5);
    }

    T2.mult(B,T1);

    T0.add(xNew,T2);
  }
}

} // namespace ait

#endif // Condensation_HPP

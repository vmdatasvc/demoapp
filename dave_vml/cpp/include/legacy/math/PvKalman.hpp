/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef PVKALMAN_HPP
#define PVKALMAN_HPP

#include <legacy/pv/ait/Matrix.hpp>
#include "legacy/vision_tools/SmoothTrajectory.hpp"

namespace ait
{

/*********************************************************************
 ** 
 ** PvKalman Class
 **
 *********************************************************************
 ** 
 ** Description:
 **
 ** This class implements a Kalman filter.
 **
 *********************************************************************/
class PvKalman : public SmoothTrajectory<float>
{
protected:
  Matrix<float> I;	    // identity
  Matrix<float> K;	    // Kalman gain
  Matrix<float> H;	    // measurement matrix
  Matrix<float> H_T;	    //  - " - transpose
  Matrix<float> A;	    // propagation matrix
  Matrix<float> A_T;	    //  - " - transpose
  Matrix<float> x1,x2;    // states
  Matrix<float> R;        // measurement noise
  Matrix<float> Q;        // process noise
  Matrix<float> P1;	    // a priori estimate
  Matrix<float> P2;	    // a posteriori estimate
  Matrix<float> var;	    // variance of measurement errors

  Matrix<float> t0,t1,t2;

  Matrix<float> tinv1;
  Matrix<unsigned int> tinv2;
  Matrix<float> t3,t4,t5;

  void calcGain(void);

public:
  // n = dimension of state space, m = dimension of observations
  PvKalman(unsigned int n, unsigned int m);
  PvKalman(void) {};
  ~PvKalman(void) {};

  // resize all matrices according to n and m
  void setSize(unsigned int n, unsigned int m);

  // initialize the various matrices
  void init(const Matrix<float> &x0,const Matrix<float> &P0,const Matrix<float> &A0,
	    const Matrix<float> &H0,const Matrix<float> &Q0,const Matrix<float> &R0);

  // predict next observation
  void predictNext(Matrix<float> &z);

  void predict(Matrix<float> &z);
  void update(Matrix<float> &z);

  // returns the current state vector
  const Matrix<float> &state(void) { return x2; };

  // returns the current gain vector
  const Matrix<float> &gain(void) { return K; };

  PvKalman& operator= (const PvKalman& kalman);

  void setQ(const Matrix<float> &Q0);  

  void setR(const Matrix<float> &R0);  

  void setState(const Matrix<float> &S);
};

} // namespace ait

#endif

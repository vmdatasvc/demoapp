/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef Polygon_HPP
#define Polygon_HPP

// SYSTEM INCLUDES
//

#include <vector>

// AIT INCLUDES
//

#include <legacy/math/LineSegment.hpp>
#include <legacy/pv/ait/Rectangle.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//


#include "Polygon_fwd.hpp"

namespace ait 
{

/*
 * Distance from the intersected point on an edge to the start of the segment 
 */

struct EdgeDist
{
  double distance;
  int edgeNum;
};

bool sortEdgeDist(const EdgeDist& a, const EdgeDist& b);

/*
bool sortEdgeDist(const EdgeDist& a, const EdgeDist& b)
{
  return (a.distance < b.distance);
}
*/


/*
bool sortEdgeDist(const EdgeDist& a, const EdgeDist& b)
{
  return (a.distance < b.distance);
}
*/

/**
 * The polygon class provides a higher level interface for dealing
 * with polygons.
 *  
 * @par Responsibilities:
 * - Handle all polygons (simple and complex)
 * - Provide convenience functions:
 *   - Is a point inside a polygon?, etc 
 *
 * @par Design Considerations:
 * - The idea is too keep the class as simple and useful as possible. 
 * This class utilizes the Segment class as much as possible, and should
 * be extendable. 
 * @remarks
 * This class does not check to ensure that the points of the polygon
 * are distinct. Sending a vertex list of identical points may will
 * generate unpredictable results.
 * 
 * @see Segment
 */

class Polygon
{
  //
  // LIFECYCLE
  //

public:
  /**
   *	Constructor - constructs a polygon with the given vertices.
   *  @See Polygon::setVertices
   */
  Polygon(const std::vector<Vector2f>& verts);
  
  /**
   * The default constructor
   */
  Polygon() {};

  
  //
  // OPERATORS
  //

  //TODO: add equality operators, etc
  const Polygon& operator = (const Polygon& rhs);

  //
  // OPERATIONS
  //

  /**
   *	setVertices clears the polygons vertex list, and
   *  replaces it with the given vertex list
   *
   *  @param verts [in] a vector of points that becomes
   *  the new vertices of the polygon.
   */
  void setVertices(const std::vector<Vector2f>& verts);

  /**
  *	returns the vertices of the current polygon geometry
  */
  void getVertices(std::vector<Vector2f>& verts);

  void translate(Vector2f deltaMovement);

  /**
  *	Rotate the polygon around the designated point
  */
  void rotate(float radian, Vector2f rotationCenter);

  void rotateAndRound(float radian, Vector2f rotationCenter);


  //
  // ACCESS
  //

  /**
   * Returns the LineSegment represented by the edgeNum argument.
   * The first two points fed to the polygon class compose edge 0,
   * the second and third points compose edge 1, and so forth.
   */
  LineSegment getEdge(int edgeNum) const
  {
    assert(edgeNum >= 0 && edgeNum < mSides.size());
    
    return mSides[edgeNum];
  }

  Vector2f getBoundingBox(int num) const
  {
	  assert(num == 0 || num == 1 );
	  if(num==0)
		  return mTopLeft;
	  else
	    return mBottomRight;
  }

  /**
   * Returns the number of sides/edges the polygon has.	
   */
  int numEdges() const {return mSides.size();}

  //
  // OPERATIONS
  //

  /**
   * Returns the distance from the given point to the closest edge of the 
   * polygon. 
   */
  float distanceNearestEdge(const Vector2f& point) const;
  

  /**
   * Returns a vector containing the sides of the polygon crossed by
   * the LineSegment, sorted by the order in which the line segment
   * crosses them if travelling from its start to end points.
   */
  std::vector<int> edgesCrossed(const LineSegment& ls) const;


  /**
   * Returns true if the given line segment intersects this polygon.
   * @param ls [in] The given LineSegment
   * @param intersectPoint [out] The closest point of intersection from the segments
   * start point
   */
  bool intersects(const LineSegment& ls, Vector2f& intersectPoint) const;

  /**
   * Draw the polygon onto the given image with the given color
   */
  void draw(Image32& img, unsigned int color) const; 
  void draw(Image8& img, unsigned int color) const; 


  /**
   * Fill this polygon in the given image with the given color
   * Note: this does not draw the boundary of the polygon (use draw function)
   */
  void fill(Image32& img, unsigned int color) const;
  void fill(Image8& img, unsigned int color) const;

  void process(void (*funk)(int, int, void*), void* data);

  /**
   * Returns a bounding box for the polygon	
   */

  Rectanglef getBoundingBox() const 
  {
    return Rectanglef(mTopLeft.x, mTopLeft.y, mBottomRight.x, mBottomRight.y) ;
  }


  //
  // INQUIRY
  //  

  /**
   * isPointInside tests to see whether or not the given point is inside
   * the polygon. This function uses a form of the crossings test.
   *
   * @param point [in] The point to check 
   * @return true if entirely within, false if entirely outside.
   * A point on the border may return either true or false.
   */
  bool isPointInside(const Vector2f& point) const;

private:

  typedef struct tEdge {
    int yUpper;
    float xIntersect;
    float dxPerScan;
    struct tEdge * next;
  } Edge;

  //void scanFill (Image32& img, unsigned int color, std::vector<Vector2i>& pts);
  void fill(Image32& img, unsigned int color, std::vector<Vector2i>& pts) const;
  void fillScan (Image32& img, unsigned int color, int scan, Edge * active) const;

  void fill(Image8& img, unsigned int color, std::vector<Vector2i>& pts) const;
  void fillScan (Image8& img, unsigned int color, int scan, Edge * active) const;

  void process(std::vector<Vector2i>& pts, void (*funk)(int, int, void*), void* data);
  void processScan(int scan, Edge * active, void (*funk)(int, int, void*), void* data);

  void buildActiveList(int scan, Edge * active, Edge * edges[]) const;
  void buildEdgeList(std::vector<Vector2i>& pts, Edge * edges[]) const;
  void makeEdgeRec(Vector2i& lower, Vector2i& upper, int yComp, Edge * edge, Edge * edges[]) const;
  int  yNext (int k, std::vector<Vector2i>& pts) const;
  void insertEdge (Edge * list, Edge * edge) const;


  void resortActiveList(Edge * active) const;
  void updateActiveList(int scan, Edge * active) const;
  void deleteAfter(Edge * q) const;
  
  // 
  // ATTRIBUTES
  //  

protected:  
  /**
   *	A vector containing segment objects
   */
  std::vector<LineSegment> mSides;
  
  /**
   * The top left of an internal bounding box
   */
  Vector2f mTopLeft;
  /**
   * The bottom right of an internal bounding box
   */
  Vector2f mBottomRight;

  /**
   * Does the polygon object have some vertices yet/is initialized
   */  
};

}; // namespace ait

#endif // Polygon_HPP


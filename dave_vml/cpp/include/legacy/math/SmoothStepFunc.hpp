/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef SMOOTH_STEP_FUNC_HPP
#define SMOOTH_STEP_FUNC_HPP

namespace ait
{

/**
 * Use this class to create fuzzy membership functions, as used in fuzzy logic. For example
 * it could be used to avoid hard thresholds in decision functions. This class uses a cubic
 * polynomial to calculate the 'soft' portion.
 */
template<class T> class SmoothStepFunc
{
public:

  /**
   * Defines the range of the 'soft' portion.
   * @param minX [in] start of the soft portion.
   * @param maxX [in] end of the soft portion.
   */
  void setRange(T minX, T maxX)
  {
    PVASSERT(minX <= maxX);

    x0 = minX;
    x1 = maxX;

    if (x0 == x1) return;

    T x10_3 = x1-x0;
    x10_3 = x10_3*x10_3*x10_3;

    a = ((T)-2)/x10_3;
    b = 3*(x1+x0)/x10_3;
    c = -6*x0*x1/x10_3;
    d = -(((a*x0 + b)*x0 + c)*x0);
  }

	/**
	 * Get the range that defines the 'soft' portion
	 * @param minX [out] start of soft portion
	 * @param maxX [out] end of soft portion
	 */
	void getRange(T& minX, T& maxX)
	{
		minX = x0;
		maxX = x1;
	}

  /**
   * This method evaluates the soft step function.
   * @param x [in] Point to evaluate.
   * @return 0 if x <= x0, 1 if x >= x1, f(x) otherwise. Where f(x) is a polynomial with f(x0) = 0, f(x1) = 1, f'(x0) = f'(x1) = 0.
   */
  T eval(T x)
  {
    if (x <= x0) return 0;
    if (x >= x1) return 1;

    return d + (c + (b + a*x)*x)*x;
  }

protected:

  /// Minimum fuzzy value
  T x0;

  /// Maximum fuzzy value
  T x1;

  /// Cubic polynomial coefficients
  T a,b,c,d;

};

/**
 * This class is the same concept as SmoothStepFunc, but it uses a line to calculate the
 * 'soft' portion.
 */
template<class T> class SmoothStepFuncLinear
{
public:

  /**
   * Defines the range of the 'soft' portion.
   * @param minX [in] start of the soft portion.
   * @param maxX [in] end of the soft portion.
   */
  void setRange(T minX, T maxX)
  {
    PVASSERT(minX <= maxX);

    x0 = minX;
    x1 = maxX;

    if (x0 == x1) return;

    b = -x0/(x1-x0);
    a = ((T)1)/(x1-x0);
  }

	/**
	 * Get the range that defines the 'soft' portion
	 * @param minX [out] start of soft portion
	 * @param maxX [out] end of soft portion
	 */
	void getRange(T& minX, T& maxX)
	{
		minX = x0;
		maxX = x1;
	}


  /**
   * This method evaluates the soft step function.
   * @param x [in] Point to evaluate.
   * @return 0 if x <= x0, 1 if x >= x1, f(x) otherwise. Where f(x) is a polynomial with f(x0) = 0, f(x1) = 1, f'(x0) = f'(x1) = 0.
   */
  T eval(T x)
  {
    if (x <= x0) return 0;
    if (x >= x1) return 1;

    return b + a*x;
  }

protected:

  /// Minimum fuzzy value
  T x0;

  /// Maximum fuzzy value
  T x1;

  /// Line interpolation coefficients
  T a, b;

};

} // namespace ait

#endif // SMOOTH_STEP_FUNC_HPP

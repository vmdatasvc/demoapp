/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef LineSegment_fwd_HPP
#define LineSegment_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class LineSegment;
typedef boost::shared_ptr<LineSegment> LineSegmentPtr;

}; // namespace ait

#endif // LineSegment_fwd_HPP

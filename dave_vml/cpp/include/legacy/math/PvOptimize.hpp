/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef PVOPTIMIZE_HPP
#define PVOPTIMIZE_HPP

#include <ait/Matrix.hpp>

namespace ait
{

template<class T> class PvOptimize
{
protected:
  static T amotry(T **p, T *y, T *psum, int ndim, T (*funk)(T *,void *), void *, int ihi, T fac, T *ptry);

public:
  static int simplex(Matrix<T> &x,Matrix<T> &dx,T (*funk)(T *, void *), void *ptr
			               ,T ftol, unsigned int *nfunk
                     ,const unsigned int NMAX=5000);
};

template<class T> T PvOptimize<T>::amotry(T **p, T *y, T *psum, int ndim,
			                                    T (*funk)(T *, void *), void *ptr, int ihi, T fac, T *ptry)
{
  int j;
  T fac1,fac2,ytry;
  
  fac1=(1.0-fac)/ndim;
  fac2=fac1-fac;
  for (j=0;j<ndim;j++) ptry[j]=psum[j]*fac1-p[ihi][j]*fac2;
  ytry=(*funk)(ptry,ptr);
  if (ytry < y[ihi]) 
  {
    y[ihi]=ytry;
    for (j=0;j<ndim;j++) 
    {
      psum[j] += ptry[j]-p[ihi][j];
      p[ihi][j]=ptry[j];
    }
  }
  
  return ytry;
}

template<class T> int PvOptimize<T>::simplex(Matrix<T> &x,Matrix<T> &dx,T (*funk)(T *, void *),void *ptr,
			       T ftol, unsigned int *nfunk
			      ,const unsigned int NMAX)
{
#define GET_PSUM for (j=0;j<ndim;j++) \
  {for (sum=0.0,i=0;i<mpts;i++) sum += p[i][j];psum[j]=sum;}
#define SWAP(a,b) {swap=(a);(a)=(b);(b)=swap;}
  
  int ndim=x.cols(),i,ihi,ilo,inhi,j,mpts=ndim+1;
  
  T rtol,sum,swap,ysave,ytry,*psum;
  Matrix<T> pmat(ndim+1,ndim);
  T *y;
  T **p=pmat.pointerpointer();
  
  psum=new T [ndim];
  y=new T [ndim+1];

  T *ptry = new T [ndim];

  // initialize simplex
  for(i=0;i<ndim+1;i++)
  {
    for(j=0;j<ndim;j++) pmat(i,j)=x(j);
    
    if(i>0) pmat(i,i-1)+=dx(i-1);
    
    y[i]=funk(&pmat(i,0),ptr);
  }
  
  *nfunk=0;
  GET_PSUM;
  for (;;) {
    ilo=0;
    ihi = y[0]>y[1] ? (inhi=1,0) : (inhi=0,1);
    for (i=0;i<mpts;i++) {
      if (y[i] <= y[ilo]) ilo=i;
      if (y[i] > y[ihi]) {
        inhi=ihi;
        ihi=i;
      } else if (y[i] > y[inhi] && i != ihi) inhi=i;
    }
    rtol=2.0*fabs(y[ihi]-y[ilo])/(fabs(y[ihi])+fabs(y[ilo]));
    if (rtol < ftol) {
      SWAP(y[0],y[ilo]);
      for (i=0;i<ndim;i++) SWAP(p[0][i],p[ilo][i])
        break;
    }
    if (*nfunk >= NMAX)
    {
      //PvUtil::exitError("NMAX exceeded");
      for(i=0;i<ndim;i++)
      {
        x(0,i)=pmat(0,i);
      }
      
      delete [] psum;
      delete [] y;
      return 0;
    }
    *nfunk += 2;
    ytry=amotry(p,y,psum,ndim,funk,ptr,ihi,-1.0f,ptry);
    if (ytry <= y[ilo])
      ytry=amotry(p,y,psum,ndim,funk,ptr,ihi,2.0f,ptry);
    else if (ytry >= y[inhi]) {
      ysave=y[ihi];
      ytry=amotry(p,y,psum,ndim,funk,ptr,ihi,0.5f,ptry);
      if (ytry >= ysave) {
        for (i=0;i<mpts;i++) {
          if (i != ilo) {
            for (j=0;j<ndim;j++)
              p[i][j]=psum[j]=0.5*(p[i][j]+p[ilo][j]);
            y[i]=(*funk)(psum,ptr);
          }
        }
        *nfunk += ndim;
        GET_PSUM;
      }
    } else --(*nfunk);
  }
  
  for(i=0;i<ndim;i++)
  {
    x(0,i)=pmat(0,i);
  }
  
  delete [] psum;
  delete [] y;
  delete [] ptry;
  
  return 1;
#undef SWAP
#undef GET_PSUM
}

} // namespace ait

#endif



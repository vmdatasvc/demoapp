/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef EarthMoverDistance_HPP
#define EarthMoverDistance_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <functional>
#include <algorithm>
#include <vector>


// AIT INCLUDES
//
#include <legacy/pv/ait/Matrix.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

namespace ait 
{

class EarthMoverDistance
{

public:

  template<class T>
  struct emdDist1 : std::binary_function<T, T, double> 
  {
    double operator() (const T& a, const T& b) const
    {
      return fabs(static_cast<double>(a-b));
    }
  };

  template<class T>
  struct emdDist2 : std::binary_function<T, T, double> 
  {
    double operator() (const T& a, const T& b) const
    {
      return (a-b)*(a-b);
    }
  };


  /**
   * Calculate the earth most distance between two vectors.
   * @param h1 [in] pointer to the first vector's frist value.
   * @param n1 [in] size of the first vector.
   * @param h2 [in] pointer to the second vector's first value.
   * @param n2 [in] size of the second vector.
   * @param dist [in] function to compare the vector indices. The format
   * of this function is either a double (*dist)(const T a, const T b) or
   * or a binary_function. For example EarthMoversDistance::emdDist2<int>,
   * @return The EMD value.
   */
  template<class T, class Pr> 
  static double emd(const T *h1, const int n1, 
                    const T *h2, const int n2,
                    Pr dist)
  {
    int i1=0,i2=0;
    double v1,v2;
    double work=0.0;
    double sum1=0.0,sum2=0.0;
    double invSum1,invSum2;

    for(i1=0;i1<n1;i1++) sum1+=h1[i1];
    for(i2=0;i2<n2;i2++) sum2+=h2[i2];

    invSum1=1.0/sum1;
    invSum2=1.0/sum2;

    i1=i2=0;

    v1=h1[i1]*invSum1;
    v2=h2[i2]*invSum2;

    while(i1<n1 && i2<n2)
    {
      if(v1==v2)
      {
        work+=v1*dist(i1,i2);
        i1++;i2++;
        if(i1<n1 && i2<n2) { v1=h1[i1]*invSum1; v2=h2[i2]*invSum2; }
      }
      else if(v1>v2)
      {
        work+=v2*dist(i1,i2);
        v1-=v2;
        i2++;
        if(i2<n2) v2=h2[i2]*invSum2;
      }
      else
      { 
        work+=v1*dist(i1,i2);
        v2-=v1;
        i1++;
        if(i1<n1) v1=h1[i1]*invSum1;
      }
    }

    return work;
  }

  /**
   * This function is the same as the previous, but assumes that the
   * function is emdDist1.
   * @see emd().
   */
  template<class T> 
  static double emd(const T *h1, const int n1, 
                    const T *h2, const int n2)
  {
    return emd(h1,n1,h2,n2,emdDist1<T>());
  }

  /**
   * Same as the previous function but works with matrices (evaluated as a vector)
   * and a distance function.
   * @see emd().
   */
  template<class T, class Pr> 
  static double emd(const Matrix<T>& h1, const Matrix<T>& h2, Pr dist)
  {
    return emd(h1.pointer(),h1.size(),
               h2.pointer(),h2.size(),dist);
  }

  /**
   * Same as the previous function but works with matrices (evaluated as a vector)
   * and the default distance function.
   * @see emd().
   */
  template<class T> 
  static double emd(const Matrix<T>& h1, const Matrix<T>& h2)
  {
    return emd(h1.pointer(),h1.size(),
               h2.pointer(),h2.size());
  }

  /**
   * Same as the previous function but works with STL vectors
   * and a distance function.
   * @see emd().
   */
  template<class T, class Pr> 
  static double emd(const std::vector<T>& h1, const std::vector<T>& h2, Pr dist)
  {
    return emd(&h1[0],h1.size(),
               &h2[0],h2.size(),dist);
  }

  /**
   * Same as the previous function but works with STL vectors
   * and the default distance function.
   * @see emd().
   */
  template<class T> 
  static double emd(const std::vector<T>& h1, const std::vector<T>& h2)
  {
    return emd(&h1[0],h1.size(),
               &h2[0],h2.size());
  }

}; // class EarthMoverDistance

}; // namespace ait

#endif // EarthMoverDistance_HPP


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef RunningStats_HPP
#define RunningStats_HPP

// SYSTEM INCLUDES
//

#include <vector>
#include <boost/utility.hpp>

// AIT INCLUDES
//

#include <legacy/math/StatUtils.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

namespace ait 
{

/**
 * Calculation of running mean, median and variace.
 * This template class can calculate the running average, and optionally the
 * running square variance, and optionally the median of the collected values. 
 * Running means that each time an object is added, 
 * accumulated values are modified in order to make each adition O(1) in the
 * number of elements.
 * The median is calculated in O(number of inserted elements) time
 * @par Responsibilities:
 * - Calculate the mean, median and variance of inserted elements.
 * - Keep the number of elements below a maximum size.
 * @remarks
 * After a large number of insertions, the mean and average will be calculated
 * in O(N) to compensate for numerical errors in the accumulators.
 */
template <class T> class RunningStats : private boost::noncopyable // This hides the default copy constructor and assignment op.
{
public:

  //
  // LIFETIME
  //

  /// Constructor
  RunningStats() : mIsInitialized(false) {};

  /// Destructor
  virtual ~RunningStats() {};

  /// Object initialization
  void init(Int historySize, Bool calcVariance = true)
  {
    mHistorySize = historySize;
    mCalcVariance = calcVariance;

    reset();

    mIsInitialized = true;
  }

  //
  // OPERATORS
  //

public:

  //
  // OPERATIONS
  //

public:

  /// Add an element to the accumulated statistics.
  void addElement(T v)
  {
    T diff;

    // Save the new entry in the history.
    if (mHistory.size() < mHistorySize)
    {
      mHistory.push_back(v);
      if (mCalcVariance) mHistoryVarDiff.push_back(v); // this value will be replaced.
    }
    else
    {
      // Remove the entry to be replaced from the accumulators

      mMeanAccum -= mHistory[mCurHistory];
      if (mCalcVariance) mVarAccum -= mHistoryVarDiff[mCurHistory];
    }

    if (mCurIteration-- == 0)
    {
      mCurIteration = mRecalculateIterations;

      // Replace the entry with the new value
      mHistory[mCurHistory] = v;
      mCurHistory = (mCurHistory+1)%mHistorySize;

      // Calculate the mean of the history

      int i;

      mMeanAccum = mHistory[0];
      for (i = 1; i < mHistory.size(); i++)
      {
        mMeanAccum += mHistory[i];
      }
      mMean = mMeanAccum / mHistory.size();

      // Calculate the square variance

      if (mCalcVariance)
      {
        mVarAccum = 0;
        for (i = 0; i < mHistory.size(); i++)
        {
          diff = mMean - mHistory[i];
          diff *= diff;
          mVarAccum += diff;
          mHistoryVarDiff[i] = diff;
        }
        mVar = mVarAccum / mHistory.size();
      }
    }
    else
    {
      // Update accumulated average and square variance calculation
      mMeanAccum += v;
      mMean = mMeanAccum / mHistory.size();
      mHistory[mCurHistory] = v;

      if (mCalcVariance)
      {
        diff = mMean-v;
        diff *= diff;
        mVarAccum += diff;
        mVar = mVarAccum / mHistory.size();
        mHistoryVarDiff[mCurHistory] = diff;
      }

      mCurHistory = (mCurHistory+1)%mHistorySize;
    }

  }

  /// Clear the accumulated mean and variance.
  void reset()
  {
    mCurHistory = 0;
    mCurIteration = 0;
    mHistory.clear();
    mHistoryVarDiff.clear();
    mMeanAccum = 0;
    mVarAccum = 0;
    mMean = 0;
    mVar = 0;
  }

  //
  // ACCESS
  //

public:

  /// Get the mean
  const T& getMean() const { return mMean; }

  /**
   * Get the median.
   * @todo Currently this function is O(n). Make this a running median.
   */ 
  const T getMedian() const 
  {
    return ait::StatUtils::computeMedian(mHistory);
  }
          
  /// Get the square variance
  const T& getVar() const { return mVar; }

  /// Get the size of the history
  Int getHistorySize() const { return mHistorySize; }

  /// Get the current number of samples
  Int getNumSamples() const { return mHistory.size(); }

  /// Returns true if the current number of samples is equal to the history size.
  Bool isFilled() const { return mHistory.size() == mHistorySize; }

  /**
   * Return the sample at the given position.
   * @param index [in] Position of the sample with respect to the oldest entry 
   * in the history.
   */
  const T& getSample(Int index) const 
  { 
    return mHistory[(mCurHistory-1+index)%getNumSamples()]; 
  }

  //
  // INQUIRY
  //

public:

  /// Ask if this object has been initialized (the init() function has been called).
  Bool isInitialized() const { return mIsInitialized; }

  //
  // ATTRIBUTES
  //

protected:

  /// Number of positions to record in the history array.
  unsigned int mHistorySize;

  /// Pointer to the next slot of the history to be used.
  int mCurHistory;

  /// Circular buffer that stores the states.
  std::vector<T> mHistory;

  /// Circular buffer that stores the differences for updating the variance accumulator.
  std::vector<T> mHistoryVarDiff;

  /// Number of iterations to recalculate accumulated average (to avoid rounding errors to accumulate too much)
  int mRecalculateIterations, mCurIteration;

  /// The accumulated sum for mean
  T mMeanAccum;

  /// The current mean
  T mMean;

  /// The accumulated sum for the square variance
  T mVarAccum;

  /// The current variance
  T mVar;

  /// Flag to calculate the variance
  Bool mCalcVariance;

  /// Initialization flag
  Bool mIsInitialized;

};

}; // namespace ait

#endif // RunningStats_HPP


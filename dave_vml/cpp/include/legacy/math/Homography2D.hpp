/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef Homography2D_HPP
#define Homography2D_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <vector>

// AIT INCLUDES
//
#include <legacy/pv/ait/Matrix.hpp>
#include <legacy/pv/ait/Vector3.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

#include "legacy/math/Homography2D_fwd.hpp"

namespace ait 
{


/**
 * This class is not yet documented.
 * Here is a more detailed description of the class. Next come the 
 * Responsibilities of the class. Try to minimize the responsibilities, this
 * will help in creating a good design. Next, optionally write any design patterns
 * that were used for this class. Refer to the "Design Patterns" book by Gamma et al.
 * @par Responsibilities:
 * - The First Responsibility
 * - The Second Responsibility:
 *   - A sub-responsibility
 *   - Another sub-responsibility
 * - The Third Responsibility
 * @par Patterns:
 * - The first design pattern
 * - The second design pattern
 * @par Design Considerations:
 * - Here you can itemize some justifications for your decisions. It will help
 * others to understand why your class is the way it is.
 * @remarks
 * Some special comments or warnings about this class.
 * @see TheirClass, OurClass (other related classes).
 */
  
class Homography2D
{
  //
  // LIFETIME
  //

public:
  
  typedef std::vector<Vector3f>::iterator iteratorPvCoord3f;


  /**
   * Constructor.
   */
  Homography2D();

  /**
   * Copy Constructor
   */
  Homography2D(const Homography2D& from);

  /**
   * Destructor
   */
  virtual ~Homography2D();

  //
  // OPERATORS
  //

public:

  /**
   * Assignment operator.
   * @param from [in] The value to assign to this object.
   * @return A reference to this object.
   */
  Homography2D& operator=(const Homography2D& from);

  /**
   * Comparison operator.
   * @param from [in] The value to compare this class with.
   * @return true if classes are equal, false otherwise.
   */
  bool operator==(const Homography2D& from) const;

  /**
   * Difference operator. This is the negation of the comparison.
   */
  bool operator!=(const Homography2D& from) const;

  //
  // OPERATIONS
  //

public:

  /**
   * Compute Homography from four or more point correspondences.
   * @param points_ref [in] Points of reference plane (at least three non-collinear)
   * @param points_proj [in] Points of projection plane (at least three non-collinear)   
   */
  bool compute(std::vector<Vector3f>& points_ref, std::vector<Vector3f>& points_proj);
  
  /**
   * Do the transformation (projection) of a point in the projection plane into the reference plane 
   * @param ptIn [in] Point in projection plane
   * @param ptOut [out] Point in reference plane
	 * @remark ptIn and ptOut must not reference to the same object
   */
  void transform(const Vector3f& ptIn, Vector3f& ptOut);

	/**
	* Do the transformation (projection) of a point in the projection plane into the reference plane 
	* @param ptInOut [in]  Point in projection plane
	* @param ptInOut [out] Point in reference plane
	*/
	void transform(Vector3f& ptInOut);

	/**
	* Invert Homography 	
	*/
	void invert();

  /**
   * Print homography    
   */
  void printf(const char *format) const {mHomography.printf(format);};

private:
  
  void normalization2D(std::vector<Vector3f>& points, std::vector<Vector3f>& normPoints, Matrix<float>& normalizationMatrix);
  
  bool DLT(Matrix<float>& matrix, Matrix<float>& transformationMatrix);
  
  void populate(Matrix<float>& matrix, std::vector<Vector3f>& imagePoints, std::vector<Vector3f>& groundPoints);
  
  void convert(Matrix<float>& matrix, int nrows, int ncols);
  
  void singularSorting(float** u, float* s, float** v, int m, int n);


  //
  // ACCESS
  //

public:


  //
  // INQUIRY
  //

public:


  //
  // ATTRIBUTES
  //

protected:

  Matrix<float> mHomography;

};


}; // namespace ait

#endif // Homography2D_HPP


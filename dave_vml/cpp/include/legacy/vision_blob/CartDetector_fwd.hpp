
#pragma once

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class CartDetector;
typedef boost::shared_ptr<CartDetector> CartDetectorPtr;

}; // namespace vision

}; // namespace ait


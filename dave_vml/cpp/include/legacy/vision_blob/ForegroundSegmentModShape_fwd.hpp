/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ForegroundSegmentModShape_fwd_HPP
#define ForegroundSegmentModShape_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class ForegroundSegmentModShape;
typedef boost::shared_ptr<ForegroundSegmentModShape> ForegroundSegmentModShapePtr;

}; // namespace vision

}; // namespace ait

#endif // ForegroundSegmentModShape_fwd_HPP



#pragma once

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <list>
#include <vector>

// AIT INCLUDES
//
#include <legacy/vision_blob/SimplePolygon.hpp>
#include <legacy/math/Polygon.hpp>
#include <legacy/vision_tools/ColorConvert.hpp>
#include <legacy/vision_blob/CartEdgeModel.hpp>
#include <legacy/pv/ait/Image.hpp>
#include <legacy/pv/PvImageConverter.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

#include "legacy/vision_blob/CartDetector_fwd.hpp"

namespace ait 
{

namespace vision
{


/**
 * @par Responsibilities:
 * - Create a cart model that consists of a different edgeCartModel for each region 
 *   in the image and each of the desired orientations
 * - Detects & stores the location of carts in the image
 * @par Design Considerations:
 * 
 * @remarks
 * 
 * @see 
 * - cartEdgeModel
 */
class CartDetector : private boost::noncopyable
{
  //
  // LIFETIME
  //
  
  //Class for Cart Candidates
  class CartCand
  {
  public:
    ait::Polygon mOuterPolygon;
    ait::Polygon mCartPolygon;
    int mOrientation;
    Vector2f mCentroid;
    float mScore;

    // Copy operator for the cart candidates
    const CartCand& operator = (const CartCand& rhs)
    {
      mOuterPolygon = rhs.mOuterPolygon;
      mCartPolygon = rhs.mCartPolygon;
      mOrientation = rhs.mOrientation;
      mCentroid = rhs.mCentroid;
      mScore = rhs.mScore;
      return *this;
    }

  };

public:

  /**
   * Constructor.
   */
  CartDetector();

  /**
   * Destructor
   */
  virtual ~CartDetector();


  //
  // OPERATIONS
  //

public:
  // Initialize the settings
  void initializeSettings(std::list<ait::SimplePolygon>& cartShapePolyList, std::list<ait::SimplePolygon>& cartRegionList,
                          std::list<double>& cartOrientationList, float detectThres, float innerThres, float outerThres, 
						  float cornerThres, float inMargin, float outMargin, ait::Vector2f scaleFactor);

  // Construct the masks -- it is best to be called before the video loop
  void buildCartModel(void);

  // Construct the list of foreground pixels. Can jump the pixels by foregroundJump in both x and y directions
  void constructForegroundList(Image8 &foregroundImage, int foregroundJump);

  // Apply the cart model to the edge image, and store the detected carts in a vector of polygons. Can jump edge pixels by maskJump.
  void detectCarts(std::vector< ait::Polygon > &carts, Image8 edgeImage, Image32 &scoreImage, int maskJump);
  
  // Finds the best cart locations while removing overlap in carts
  void removeOverlap(std::vector< CartCand > &candidates);

  void removeCarts(Image8& img, Image8& cartImg, std::vector<ait::Polygon>& detectedCartPolygons);


  //
  // ACCESS
  //

public:


  //
  // INQUIRY
  //

public:


  //
  // ATTRIBUTES
  //

protected:
 
  // The list of foreground pixels
  std::vector< Vector3f > mForegroundList;

  // List of Cart shapes for different regions in the image
  std::vector<ait::PolygonPtr> mCartShapes;
  
  // List of different regions in the image
  std::vector<ait::PolygonPtr> mCartRegionPtrs;

  // List of orientations used for detecting carts
  std::vector<double> mOrientations;

  int mInMargin, mOutMargin;

  // The list of cart masks, each element represent one of the four directions
  ait::BasicMatrix <CartEdgeModelPtr> mCartModel;

  // The list of candidate carts
  std::vector< CartCand > mCandidates;

  // The threshold to determine initial cart candidates
  float mDetectThres;

  float mOuterThres;

  float mInnerThres;

  float mCornerThres;
};


}; // namespace vision

}; // namespace ait


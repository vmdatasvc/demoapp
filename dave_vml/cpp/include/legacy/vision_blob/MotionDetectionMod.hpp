/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef MotionDetectionMod_HPP
#define MotionDetectionMod_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

#include <list>
#include <map>

#include <legacy/vision_blob/SimplePolygon.hpp>
#include <legacy/pv/ait/Rectangle.hpp>


//#include <cv.h>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>
#include <legacy/pv/ait/Image.hpp>
#include <legacy/pv/ait/Matrix.hpp>
//#include <VmsPacket.hpp>

// LOCAL INCLUDES
//

//#include "legacy/vision/RealTimeMod.hpp"
#include "legacy/vision/VisionMod.hpp"

// FORWARD REFERENCES
//

#include "legacy/vision_blob/MotionDetectionMod_fwd.hpp"

#include <legacy/vision_blob/ForegroundSegmentMod_fwd.hpp>
#include <legacy/pv/CircularBuffer.hpp>

namespace ait 
{

namespace vision
{


//class MotionEvent;

/**
 * This vision module is not yet documented.
 * @par Responsibilities:
 * - Write the responsibilities here.
 */
class MotionDetectionMod : public VisionMod
{
 
public:

   class MotionEvent;

  //
  // LIFETIME
  //

  /**
   * Constructor. The constructor creates a quick instance. Expensive operations
   * are executed in the init() function.
   */
  MotionDetectionMod();

  /**
   * Destructor
   */
  virtual ~MotionDetectionMod();

  /**
   * Provide dependency information for the ExecutionGraph.
   * @see VisionMod::initExecutionGraph for more info.
   */
//  virtual void initExecutionGraph(std::list<VisionModPtr>& srcModules);
  virtual void initExecution();

  /**
   * Initialization function.
   */
  virtual void init();

  /**
   * Finalization function
   */
  virtual void finish();

  //
  // ACCESS
  //

  
  SimplePolygon getEventPolygon(int evt) {return mEventPolygon[evt]; }

  MotionEvent getEventState(int evt) {return mEvent[evt]; }

  int numEvents() {return mEventChannels.size(); }

  //TODO: TEMP

  double getMotionScore() const {return mMotionScore;}
  //END TEMP

  

  //
  // OPERATIONS
  //

public:

  /**
   * Process this module.
   */
  virtual void process();

  /**
   * Visualize this module.
   */
  virtual void visualize(Image32& img);

  /**
   * Return a pointer to the filtered motion image	
   */
  Image8Ptr getFilteredMotion() {return mpFilteredMotion;}

  /**
   * Returns the current scale factor
   */
  virtual Vector2f getScaleFactor() {return mScaleFactor;}

  /**
   * Sets the scale factor, this will essentially reset the motion module.
   * It will then use the new scaling factor
   */
  virtual void setScaleAndRoi(Vector2f scaleFactor, Rectanglei roi);
  

  virtual Image8Ptr getRoiFrame(Image32Ptr pImg);

  virtual Rectanglei getRoiRect() {return mRoiRect;}

  /**
   * Returns motion history TODO: BETTER DOC
   */
  virtual boost::shared_ptr<const Image<double> > getMotionHistory()   
  {     
    return mpLastMotion;
  } 

protected:

  /**
   * Calculate the noise level	
   */

  float estimateNoise();

  

  //
  // ACCESS
  //

public:

  /**
   * Get the static name of this module. This name is used to name the type of this module.
   * The constructor sets the member variable mName to this value.
   * @remark This static function is only implemented for concrete classes.
   */
  static const char *getModuleStaticName() { return "MotionDetectionMod"; }

  //
  // INQUIRY
  //

public:

  //
  // ATTRIBUTES
  //

public:

  class MotionEvent
  {
  public:
    MotionEvent() : isActive(false), startTime(-1), endTime(-1) {}

    bool isActive;
    Int64 startTime;
    Int64 endTime;    
  };
protected:

  /**
   * The VMS packet.
   */
//  VmsPacket mVmsPacket;

  /**
   * List of event channels to send corresponding maps to	
   */
  std::list<int> mEventChannels;

  std::map<int, SimplePolygon> mEventPolygon;  
  std::map<int, Rectanglef> mEventRect;
  std::map<int, Image8> mEventMask;

  std::map<int, MotionEvent> mEvent;

    
  //Rectanglei mRegionOfInterest;
  
  Image8 mDifferenceFrame;
  Image8 mPreviousDiff;

  Image8 mPreviousFrame;

  //Image8 mFinalDiff; 

  Image8Ptr mpFilteredMotion;


  //Image8 mMotionMask;

  Rectanglei mRoiRect;


  //std::list<Image8> mDiffHistory;
  //int mMaxHistory;

  Image8 mAccumulationFrame;  

  int mDiffThreshold;

  /*
  int mHistoryBufferSize;
  std::list<Image8> mHistoryBuffer;

  int mHistoryCounter;
  int mHistoryUpdateInteral;
  */

  //int mNoiseCount;
  //int mNoiseAdjustInterval;

  bool mPrintMotionLevels;
  float mMotionSensitivity;
  
  bool mFirstFrame;
  Vector2f mScaleFactor;
  
  bool mSaveEvents;
  std::fstream mOutFile;

  int mWidth;
  int mHeight;

  //For visualization

  Image32 mVisDiff;

  float mExtensionTime;  

  //Holds history of motion data
  //Image<double> mLastMotion;
  boost::shared_ptr<Image<double> > mpLastMotion;

  Image32 mLastMotionImage;
  double mMaxHistoryTime;

  //TODO: TEMP
  double mMotionScore;
  //END TEMP


};


}; // namespace vision

}; // namespace ait

#endif // MotionDetectionMod_HPP



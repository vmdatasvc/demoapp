/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef BackgroundModelCb_fwd_HPP
#define BackgroundModelCb_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class BackgroundModelCb;
typedef boost::shared_ptr<BackgroundModelCb> BackgroundModelCbPtr;

}; // namespace vision

}; // namespace ait

#endif // BackgroundModelCb_fwd_HPP


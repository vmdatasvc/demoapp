/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef BlobTrackerModShape_fwd_HPP
#define BlobTrackerModShape_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class BlobTrackerModShape;
typedef boost::shared_ptr<BlobTrackerModShape> BlobTrackerModShapePtr;

}; // namespace vision

}; // namespace ait

#endif // BlobTrackerModShape_fwd_HPP


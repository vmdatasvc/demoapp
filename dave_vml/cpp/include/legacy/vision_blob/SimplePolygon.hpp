/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef SimplePolygon_HPP
#define SimplePolygon_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>
#include <vector>

// AIT INCLUDES
//


#include <list>

#include <legacy/pv/ait/Vector3.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//


namespace ait 
{

typedef std::vector<Vector2f> SimplePolygon;
typedef std::list<SimplePolygon> SimplePolygonList;

/**
 * Convert a from a string to a polygon of 2D points. The format is
 * a series of comma separated values. No other delimiters should be used.
 * Optionally all the points can be enclosed by parenthesis (not each point).
 * For example "12,23,23,54,23,54" is a 3 point polygon.
 */
void convert(const std::string& str, SimplePolygon& p);

/**
 * Convert a string into a list of simple polygons	
 */
SimplePolygonList convertList(const std::string& str);

}; // namespace ait

#endif // SimplePolygon_HPP


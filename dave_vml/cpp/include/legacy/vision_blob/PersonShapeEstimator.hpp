/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef PersonShapeEstimator_HPP
#define PersonShapeEstimator_HPP

#include "legacy/vision_blob/PersonShapeEstimator_fwd.hpp"

#include <legacy/math/Polynomial.hpp>

//WINDOWS ONLY INCLUDES
#ifdef WIN32
#include <windows.h>
#include <gl/gl.h>
#include <gl/glut.h>
#endif

#include <legacy/low_level/Settings.hpp>

//STL include
#include <string>

#include <legacy/pv/ait/Image.hpp>
//#include <ait/Image_fwd.hpp>

namespace ait
{

namespace vision
{

class PersonShapeEstimator
{
public:

  // LIFECYCLE

  /**
   * Default Constructor
   */
  PersonShapeEstimator();


  /**
   * Virtual deconstructor.
   */
  virtual ~PersonShapeEstimator()
  {
    destroyRenderingSurface();
  }

  // OPERATIONS

  /**
   * Applies the camera distortion to a world image, converting it into
   * an image as seen from the camera view. If a portion of the world
   * image that should be seen from the camera does not exist, a blue
   * pixel is drawn instead. The ideal size of the world image relative to
   * the camera image is dependent on lens/camera parameters.
   *
   * @param img The distorted camera view is drawn onto this image
   * @param worldImg An undistorted image of the world.
   */  
  virtual void applyCameraDistortion(Image32& img, 
                                     const Image32& worldImg) const;

  /**
   * Applies the camera distortion to a world image, converting it into
   * an image as seen from the camera view. If a portion of the world
   * image that should be seen from the camera does not exist, a blue
   * pixel is drawn instead. The ideal size of the world image relative to
   * the camera image is dependent on lens/camera parameters. This version
   * is faster than the Image32 version.
   *
   * @param img The distorted camera view is drawn onto this image
   * @param worldImg An undistorted image of the world.
   */  
  virtual void applyCameraDistortion(Image8& img, 
                                     const Image8& worldImg) const;

  /** 
   * @see applyCameraDistortion
   * @note This version applies the distortion only to the specified region
   * within rect
   */
  virtual void applyCameraDistortion(Image32& img, 
                                     const Image32& worldImg, 
                                     Rectanglei rect) const;

  /** 
   * @see applyCameraDistortion
   * @note This version applies the distortion only to the specified region
   * within rect
   */
  virtual void applyCameraDistortion(Image8& img, 
                                     const Image8& worldImg, 
                                     Rectanglei rect) const; 

  /**
   * This function 'erases' the foreground segmentation at the given pixel
   * coordinate by setting to zero the region corresponding to the
   * shape mask for that location.
   *
   * @param img The image to clean
   * @param offset The offset used when the origin of img does not match the
   * internal mask array origin, due to translation/region of interest/
   * cropping.
   */
  virtual void cleanForegroundSegmentation(int x, int y, 
                                           Image8& img, 
                                           Vector2i offset);

  /**
   * This function draws the person pixels in the image
   *
   * @param img The image to draw to
   * @param color The pixel colors to draw
   * @param offset The offset used when the origin of img does not match the
   * internal mask array origin, due to translation/region of interest/
   * cropping.
   */
  virtual void drawPersonPixels(int x, int y, int color, 
                                           Image32& img, 
                                           Vector2i offset);

  /**
   * Draw an image as seen from a camera, taking into account
   * lens distortion.
   *
   * @param x X position in image coordinates of the person's feet.
   * @param y Y position in image coordinates of the person's feet
   * @param img The person is drawn onto this image.
   */
  virtual void drawPersonCamera(int x, int y, Image32& img);

  /**
   * Draw an image as seen from a camera, taking into account
   * lens distortion. This version is faster than the Image32
   * version.
   *
   * @param x X position in image coordinates of the person's feet.
   * @param y Y position in image coordinates of the person's feet
   * @param img The person is drawn onto this image.
   */
  virtual void drawPersonCamera(int x, int y, Image8& img);

  /**
   * Draw a person in world/3d space.
   *
   * @param xPos xPos in world coordinates
   * @param yPos yPos in world coordinates
   * @param useColors Should the person be drawn with random colors, or white.
   */
  virtual void drawPersonWorld(float xPos, float yPos, bool useColors);  

  /**
   * Filter an input image with the shape masks. For each pixel, the mask is 
   * compared with the source image. The new pixel value is equal to the
   * 'number of src pixels that match mask pixels / total number of mask
   * pixels'.
   *
   * @param img The filtered output image will be written here.
   * @param srcImg The source image for comparison, typically foreground
   * segmentation
   * @param offset The offset from the origin, to be used for region of
   * interests or where the origin of the src image does not match
   * the origin of the mask images.
   */
  virtual void filterImage(Image8& img, 
                           const Image8& srcImg, 
                           Vector2i offset);

  /**
   * Filter an input image with an outline of the shape masks.
   */
  virtual void filterImageOutline(Image8& img, 
                                  const Image8& srcImg, 
                                  const Image8& fullSegMask,
                                  Vector2i offset);

  /**
  * Filter an input image with an outline of the shape masks.
  */
  virtual void filterImageOutlineLowRes(Image8& img, 
    const Image8& srcImg, 
    const Image8& fullSegMask,
    Vector2i offset);

  /**
   * This function must be called first.
   *
   * @param settingsPath The settings path which relevant parameters
   * will be read from.
   */
  virtual void init(const std::string& settingsPath);

  /**
   * Load masks that were previously saved to disk.
   *
   * @param path The folder where the masks and masks.txt
   * file was previously saved to.
   */
  virtual void loadMasks(const std::string& path);

  /**
   * Save the mask images already in memory to folder on the local hard drive.
   * A masks.txt file will be saved in this folder with the
   * relevant images.
   */
  virtual void saveMasks(const std::string path);

  /**
   * Create new mask images and save them to the specified path.
   */
  virtual void saveMasksToFolder(const std::string& path);

  /**
   * Visualize the world, and store the result in img.
   */
  virtual void visualizeWorld(Image32& img);  

  virtual void visualizeFootLocations(Image32& img, const std::vector<Vector2i>& footLoc);

  virtual Image<Vector2f> getWorldCoordsVector(){return mRealWorldCoords;}
  

  //ACCESS 

  /**
   * Return a pointer to the mask at the given x,y pixel.
   * If the pointer is null, then that means that a mask does not exist for
   * the given pixel.
   */
  virtual Image8Ptr getMaskImage(int x, int y)
  {
    if (x < 0)
      x = 0;
    if (x >= mMaskImage.width())
      x = mMaskImage.width() - 1;
    if (y < 0)
      y = 0;
    if (y >= mMaskImage.height())
      y = mMaskImage.height() - 1;


    assert(x >= 0 && x < mMaskImage.width());
    assert(y >= 0 && y < mMaskImage.height());    

    return mMaskImage(x,y).pMaskImage;
  }

  /**
   * Generate the outline masks from the originals
   */
  virtual void generateMaskOutline();

  /**
   * Return the bounding box of the mask for the given x,y pixel
   */
  virtual Rectanglei getMaskRect(int x, int y) const
  {
    //TODO: TEMPORARY FIX FOR WHEN x,y requested is outside of range
    if (x < 0)
      x = 0;
    if (x >= mMaskImage.width())
      x = mMaskImage.width() - 1;
    if (y < 0)
      y = 0;
    if (y >= mMaskImage.height())
      y = mMaskImage.height() - 1;    

    assert(x >= 0 && x < mMaskImage.width());
    assert(y >= 0 && y < mMaskImage.height());    

    return mMaskImage(x,y).maskRect;
  }

  void setMask(int x, int y, Image8Ptr pImg, Rectanglei r, int personPixels)
  {
    mMaskImage(x,y).maskRect = r;
    mMaskImage(x,y).personPixels = personPixels;
    mMaskImage(x,y).pMaskImage = pImg;
  }
  
  /**
   * Grabs a pixel from the world image based on its location
   * in the distorted image, and returns its pixel value.
   * The Image32 version uses interpolation for nicer images.   * 
   *
   * @param x X pixel position in distorted image.
   * @param y Y pixel position in distorted image.
   * @param worldImg The (undistorted) image used for reference.
   */
  virtual unsigned int getUndistortedPixel(int x, int y,     
                                           const Image32& worldImg) const;

  /**
   * Grabs a pixel from the world image based on its location
   * in the distorted image, and returns its pixel value.
   * The PvGray version does not interpolate pixel values, and runs faster
   * than its Image32 counterpart.
   *
   * @param x X pixel position in distorted image.
   * @param y Y pixel position in distorted image.
   * @param worldImg The (undistorted) image used for reference.
   */
  virtual unsigned int getUndistortedPixel(int x, int y, 
                                           const Image8& worldImg) const;

  /**
   * Translate image coordinates into world coordiantes.
   *
   * @return A Vector2f object containing equivalent world coordiantes.
   */
  virtual Vector2f getWorldCoords(int x, int y) const; 

  /**
   * Set woorld world coordinate values.
   */
  virtual void setWorldCoords(int x, int y, float wx, float wy)
  {
    mRealWorldCoords(x,y).set(wx,wy);
  }

/**
   * Translate image coordinates into world coordinates.
   *
   * @return A Vector2f object containing equivalent world coordiantes and 123456.0 for any point that does not intersect with 3D plane.
   */
  virtual Vector2f safeGetWorldCoords(int x, int y) const;

protected:  

  /**
   * A very basic mask structure
   */
  struct mask
  {   
    /**
     * Pointer to mask object. May be null if the mask does not exist.
     */
    Image8Ptr pMaskImage;
    /**
     * Rectangle describing masks location in image coordinate space
     */
    Rectanglei maskRect;
    /**
     * Number of white pixels in each mask.
     */
    int personPixels;

    /**
     * Print the struct's data to the screen. Useful for debugging.
     */
    void print()
    {
      PVMSG("personPixels: %d\n", personPixels);
      
      if (personPixels > 0)
      {
        assert(pMaskImage.get());
        PVMSG("maskRect: %d %d %d %d\n", maskRect.x0, 
                                         maskRect.y0, 
                                         maskRect.x1, 
                                         maskRect.y1);             

        PVMSG("pMaskImage: %d %d\n", pMaskImage->width(), 
                                     pMaskImage->height());
      }
      else
      {
        PVMSG("maskRect: not available\n", maskRect.x0, 
                                           maskRect.y0,
                                           maskRect.x1, 
                                           maskRect.y1);      

        PVMSG("pMaskImage: not available\n");
      }
    }
  };

  /**
   * Create mask images for the upper left image quadrant.
   */
  virtual void createMasks();

  /**
   * Creates a surface that OpenGl may render to.
   * 
   * @param useColor Set to true when generating visualiztions, otherwise
   * set to false for faster processing.   
   */
  virtual void createRenderingSurface(bool useColor);

  /**
   * Destroys the rendering surface created for OpenGl
   */
  virtual void destroyRenderingSurface();

  /**
   * Modify mask b to match mask a, except flipped horizontally about the
   * origin.
   */
  virtual void flipHoriz(const mask& a, mask& b);

  /**
   * Modify mask b to match mask a, except flipped vertically about the
   * origin.
   */
  virtual void flipVert(const mask& a, mask& b); 

  /**
   * Generates a mask for a person whose foot location is (x,y) in the 
   * distorted camera view image.
   */
  virtual void generateMask(int x, int y); 

  /**
   * Get the distorted bounding box for a given pixel. This bounding box takes
   * into account the lens distortion and projection transformations. It should
   * surround the distorted mask shape.
   */
  virtual Rectanglei getDistortedBoundingBox(int x, int y) const; 

  /**
   * Create a folder name based on the input parameters to the person shape
   * estimator. The folder is intended to be unique for each set of parameters.   
   */
  virtual std::string getFolderFromParameters();

  /**
   * Load the masks from the XML file format.
   */
  void loadXmlMasks(const std::string& path);

  /**
   * Extract the rendered scene from the internal pixel buffer, and convert
   * it to a Image32. This function assumes that the internal pixel
   * buffer was initialized with the appropriate bit depth.
   *
   * @param img The resulting output image.
   */
  virtual void getRenderedScene(Image32& img);

  /**
   * Extract the rendered scene from the internal pixel buffer, and convert
   * it to a Image8. This function assumes that the internal pixel
   * buffer was initialized with the appropriate bit depth.
   *
   * @param img The resulting output image.
   */
  virtual void getRenderedScene(Image8& img);    

  /**
  * Initializes the 3d world.
  */
  virtual void initGl();  

  /**
  * Populate all four image quadrants with masks by mirroring
  * the top left quadrant masks. This is much faster than calculating
  * masks for the entire image.
  */
  virtual void mirrorQuadrants();

  /**
   * A Image array containing the mask information
   */
  Image<mask> mMaskImage;

  /**
   * A Image array containing the mask outline information
   */
  Image<mask> mMaskOutlineImage;

  /**
   * Basic settings object. Vision settings are saved here.
   */   
  Settings mSettings; 

  /**
   * If true, masks will be auto generated and loaded if they do not already
   * exist for the given set of input parameters.
   */
  bool mAutoGenerateMasks;

  /**
   * The root folder to store all auto generated masks in.
   */
  std::string mAutoGenerateFolder;
  
  /**
   * Set to true after init() has been called.
   */
  bool mIsInitialized;

  /**
   * The image width that the mask information is saved at.
   */
  int mImageWidth;

  /**
   * The image height that the mask information is saved at.
   */
  int mImageHeight;

  /**
   * The max of mImageWidth and mImageHeight
   */
  int mLargeAxis;

  /**
   * The height of the camera. This setting is unitless, but should
   * share the same unit as the other input parameters.
   */
  float mCameraHeight;

  /**
   * The simulated person height. This setting is unitless, but should
   * share the same unit as the other input parameters.
   */
  float mPersonHeight;

  /**
   * The simulated person radius. This setting is unitless, but should
   * share the same unit as the other input parameters.   
   */
  float mPersonRadius;

  /**
   * The width of the CCD image sensor. This setting is unitless, but should
   * share the same unit as the other input parameters.
   */
  float mCameraCcdWidth;

  /**
   * The height of the CCD image sensor. This setting is unitless, but should
   * share the same unit as the other input parameters.
   */
  float mCameraCcdHeight;

  /**
   * The focal length of the camera or image sensor. This setting is unitless,
   * but should share the same unit as the other input parameters.
   */
  float mCameraFocalLength;

  /**
   * The polynomial used to correct for lens distortion.
   */
  Polynomial mPolynomial;

  /**
   * The width of the internal openGL rendering surface.
   */
  int mRenderImageWidth;

  /**
   * The height of the internal openGL rendering surface.
   */
  int mRenderImageHeight;

  /**
   * This boolean determines whether or not the masks will be cropped at the
   * scene edge. Uncropped masks extend outside the actual viewable area. This
   * setting can be used to adjust the way the filtering works. When on,
   * individuals standing partially offscreen that match their on-screen
   * mask will receive a high score.   
   */
  bool mCropMasks;

  /**
   * Camera rotation about the X axis
   */
  double mCameraRotX;

  /**
   * The number of pixels per openGL unit coordinate.
   */
  //float mPixelsPerCoord;


  Image<Vector2f> mRealWorldCoords;

  /**
   * Display advanced debugging information if set to true
   */
  bool mVerbose;

  //windows specific code
#ifdef WIN32
  /**
   * A windows device handle used to initialize openGL
   */
  HDC mHdc;
  /**
   * A bitmap device used for offscreen openGL rendering.
   * @note This form of offscreen rendering is very slow, the alternative
   * may be to use p-buffers or some other hardware accelerated method in the
   * future.
   */
  HBITMAP mHbm;

  /**
   * The actual offscreen memory array written to by openGL.
   */
  DWORD* mpImageBuffer;

  HGDIOBJ mSelectResult;
  HGLRC mHglrc;
#endif

};

}; // namespace vision

}; // namespace ait

#endif // PersonShapeEstimator_HPP


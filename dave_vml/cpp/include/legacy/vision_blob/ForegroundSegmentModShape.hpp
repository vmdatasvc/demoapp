/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ForegroundSegmentModShape_HPP
#define ForegroundSegmentModShape_HPP

#include <legacy/vision_blob/BackgroundModelCb.hpp>


// SYSTEM INCLUDES
//

// AIT INCLUDES
//


// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

#include "legacy/vision_blob/ForegroundSegmentModShape_fwd.hpp"

#include "legacy/vision_blob/ForegroundSegmentMod.hpp"



namespace ait 
{

namespace vision
{

/**
 * This vision module is not yet documented.
 * @par Responsibilities:
 * - Write the responsibilities here.
 */
class ForegroundSegmentModShape : public ForegroundSegmentMod
{
public:

  ForegroundSegmentModShape();


  virtual void init();

   /**
   * Process this module.
   */
  virtual void process();

  /**
   * Visualize this module.
   */
  virtual void visualize(Image32& img);

  /**
   * Return a pointer to an object holding the scaled and cropped colorRoiFrame
   */
  virtual Image32Ptr getColorRoiFrame(Image32Ptr pImg);

  /**
   * Return a pointer to the foreground segmentation image
   */
  virtual Image8Ptr getForeground() {return mpForeground;}
  

  static const char *getModuleStaticName() { return "ForegroundSegmentModShape"; }

protected:

  /**
   * This image holds the foreground segmented image
   */
  Image8Ptr mpForeground;

  /**
   * If true, histogram equalization will be applied if the input is a grayscale image
   */
  bool mEqualizeGrayImage;

  /**
   * If true, the Codebook algorithm will be used for segmentation.
   */
  bool mUseCodebook;

  /**
   * This variable is set to true on init(), so that actions may be performed on the first frame
   */
  bool mFirstCodebookPass;
  //double mLastModelSwapTime;

  /**
   * How often a frame will be sent to the codebook background model if it is enabled
   */
  double mCodebookLearnInterval;

  /**
   * The last time a frame was learned by codebook.
   */
  double mCodebookLastLearnTime;

  /**
   * When using codebook, this variable contains the initial stack of learning times.
   * For example, a stack of:
   *    60.0
   *    300.0
   *    600.0
   *
   * would mean that each frame, 3 codebook objects are updated. After the 
   * first 60 seconds the first model is set as the active model, and popped 
   * from the list. Then two models are updated. The last interval is always 
   * reinserted after being popped to ensure that we are always learning 
   * the background.
   */
  std::list<double> mCodebookStartupList;


  /**
   * This object is used to store mask information that is sent to the
   * codebook object to specify which pixels may be learned.
   */
  Image8 mWhiteMask;

  /**
   * A pointer to the active codebook model.
   */
  BackgroundModelCbPtr mpActiveCb;

  /**
   * The codebook startup list is copied into here initially, and the
   * behavior is identical. This list can be changed during runtime
   * to influence the learning of the different models.
   */
  std::list<BackgroundModelCbPtr> mLearningCbList;
  
  /**
   * Use histogram equalization with codebook
   */
  bool mUseEqualizeCodebook;


  std::list<Vector2f> mAvgIntensity;  //x is avg value and y is frequency

  float mModelIntensity;

  bool mUseEdge;


  //Variables for motion based learning
  int mMotionLearningCounter;

  Image8 mROIMask;

  Image8 mPastImage;

  int mPixelThreshold;

  float mFullAreaThreshold;

  bool mUseMotionHackLearning;

  int mLearningTemporalThreshold;

  int mTotalFrames;

  
  
};


}; // namespace vision

}; // namespace ait

#endif // ForegroundSegmentModShape_HPP



/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef MultiPerson_fwd_HPP
#define MultiPerson_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class MultiPerson;
typedef boost::shared_ptr<MultiPerson> MultiPersonPtr;

}; // namespace vision

}; // namespace ait

#endif // MultiPerson_fwd_HPP


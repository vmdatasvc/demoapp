/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef MultiFrameTrackShapePS_fwd_HPP
#define MultiFrameTrackShapePS_fwd_HPP

#include <boost/shared_ptr.hpp>
#include <CircularBuffer.hpp>
#include <ait/Image_fwd.hpp>

namespace ait
{

namespace vision
{

class MultiFrameTrackShapePS;
typedef boost::shared_ptr<MultiFrameTrackShapePS> MultiFrameTrackShapePSPtr;
typedef CircularBuffer<Image8Ptr> ForegroundHistory;
typedef boost::shared_ptr< ForegroundHistory > ForegroundHistoryPtr;

}; // namespace vision

}; // namespace ait

#endif // MultiFrameTrack_fwd_HPP



#pragma once

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//
#include <legacy/math/Polygon.hpp>
#include <legacy/vision_tools/ColorConvert.hpp>
#include <legacy/pv/ait/Image.hpp>
#include <legacy/pv/PvImageConverter.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

#include "legacy/vision_blob/CartEdgeModel_fwd.hpp"

namespace ait 
{

namespace vision
{


/**
 * @par Responsibilities:
 * - Create and store edge based cart model for a given cart shape and orientation
 * - Determine the probability of the presence of a cart at a given location in the image
 * @par Patterns:
 * - The first design pattern
 * - The second design pattern
 * @par Design Considerations:
 * - 
 * @remarks
 * Work in Progress :-).
 * @see 
 * CartDetector
 */
class CartEdgeModel : private boost::noncopyable
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  CartEdgeModel();

  /**
   * Destructor
   */
  virtual ~CartEdgeModel();


  //
  // OPERATIONS
  //

public:

  // Converts the Feature Polygon into a single Edge Model Representation
  virtual void createEdgeModel(ait::Polygon& cartPolygon, float orientation, int inMargin, int outMargin);

  // Computes the probability of the presence of a cart at the cartCandidate location
  virtual float generateMatchScore(ait::Image8& edgeImage, ait::Vector2f & cartCandidate, int DetectThres);



  //
  // ACCESS
  //

public:




  //
  // INQUIRY
  //

public:



  //
  // ATTRIBUTES
  //

//protected:
public:
  
  // Store the Cart Boundary and the polygons that make up the 12 features that are used to represent the cart
  std::vector< ait::PolygonPtr > mPolygonPtrs;

  //Used to identify the feature
  std::vector< int > mFeatureId;

  //Stores the x,y co-ordinates of the white pixels that make up the mask along with the feature Id that the pixel belongs
  std::vector< Vector3i > mMask;

  //Stores the value used to normalize the value of each feature to ge the score
  std::vector< int > mFeatureScale;

  //Store the normalized score for each feature
  std::vector< float > mFeatureScore;

  //Stores the final combined score after applying all constraints
  float mFinalScore;

};


}; // namespace vision

}; // namespace ait


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef MultiFrameTrackShapePS_HPP
#define MultiFrameTrackShapePS_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//


#include "legacy/vision_blob/MultiFrameTrackShapePS_fwd.hpp"
#include <legacy/vision_blob/MultiFrameTrackShape.hpp>

#include <legacy/vision_blob/PersonShapeEstimator.hpp>
#include <legacy/math/Condensation.hpp>
#include <legacy/vision_blob/Trajectory.hpp>
#include <legacy/math/Polygon.hpp>

namespace ait 
{

namespace vision
{

typedef std::list<MultiFrameTrackShapePSPtr> TracksShapePS;
typedef boost::shared_ptr<TracksShapePS> TracksShapePSPtr;


class MultiFrameTrackShapePS: public MultiFrameTrackShape
{
  //
  // LIFETIME
  //

public:

  /**
   * Initialization.
   */
  virtual void init(int id,const std::string& settingsPath,
    const Vector2i& initPos,
    double startTime,
    double fps,
    PersonShapeEstimatorPtr pPersonShapeEstimator,
    Vector2i personShapeOffset,int personDensityValue);

  
  /**
   * Visualize.
   */
  virtual void visualize(Image32& img);

private:

  virtual void updatePastPredictedPositions(bool val);

  virtual void updateSearchRectangle();


public:
  virtual void setImage(Image32 &img){mImg.rescale(80,60);mImg=img;}

  /**
   * Return the current snake for visualization. The oldest frame is in the begining of
   * the snake. The head of the snake is the last node.
   */
  
  //Update function that is called whenever the person shape estimator finds a person
  virtual void update(Image8Ptr pForeground, double currentTime);

  //Function used to set the boolean
  virtual void setPersonPositionUsage(bool value){mUsesPersonPosition=value;}

  //Function to find the status of the boolean
  virtual bool getPersonPositionUsage(){return mUsesPersonPosition;}

  //Function used to find the person position as given by the person shape estimator
  virtual Vector2f getPersonPosition(){return mPersonPosition;}

  //Function used to set the position of the person position coordinates
  virtual void setPersonPosition(Vector2f value){mPersonPosition=value;}

  //Function to set the boolean
  virtual void setCurrentPersonPositionUsage(bool value){mCurrentUsesPersonPosition=value;if(value==true) noUseTrackAge=0;}

  //Function to get status of the boolean
  virtual bool getCurrentPersonPositionUsage(){return mCurrentUsesPersonPosition;}

  //Function to set the person density value
  virtual void setPersonDensityValue(int val){mPersonDensityValue=val;}

  //Function to get the person density value
  virtual int getPersonDensityValue(){return mPersonDensityValue;}

  //Function to modify assignment (mIsAssigned) variable
  virtual void setAssignmentVar(bool value){mIsAssigned=value;}

  virtual bool getAssignmentVar(){return mIsAssigned;}

  virtual bool getTrackStatus(){return mProbationaryTrack;}

  virtual void setTrackStatus(bool value){mProbationaryTrack = value;}

  virtual void setPersonProbabilityStatus(bool value){mPersonProbabilityStatus = value;}

  //Public member indicating how long a track has been alive without any evidence
  int noUseTrackAge;

  Vector2f getPos() { Trajectory::NodePtr temp = mpTrajectory->lastNode(); Vector3f tempPos = temp->location; return Vector2f(tempPos.x,tempPos.y); }

  virtual void setTrackInitType(bool val){mGoodTrackRegionStatus = val;}

  virtual void setCartPolygon(Polygon polygon){mCartPolygon = polygon;updateCartBoundingBox();}

  virtual void getCartPolygon(Polygon& polygon){polygon=mCartPolygon;}

  virtual Vector2f getPredictedPos();

  virtual void setPastPredictedTrajectoryStatus(bool val){mUsePredictedTrajectory=val;updatePastPredictedPositions(val);}

  virtual bool getPastPredictedTrajectoryStatus(){return mUsePredictedTrajectory;}

  virtual void getPastPredictedPositions(Vector2f& position, Vector2f& motion){position = mPastPredictedTrajectory; motion = mPastPredictedMotion;PVMSG("\n%f, %f, %d\n",position.x,position.y,mUsePredictedTrajectory);}

  virtual void resetPastPredictedTrajectoryStatus(){mUsePredictedTrajectory=false;}

  virtual void updateCartBoundingBox();

  virtual std::vector<Polygon> getCartPolygonVector(){return mCartBoundingBox;}

  virtual void getSearchRectangle(Polygon& poly){poly=mSearchRectangle;}

  

  

protected:

  
  //Image used for containing the person positions of all other tracks
  Image32 mImg;

  //Boolean that is set whenever the person position was used last frame to maintain the track
  bool mUsesPersonPosition;

  //Current person position returned by the person estimator
  Vector2f mPersonPosition;

  //Boolean that is set whenever the person shape is used to estimate position in current frame
  bool mCurrentUsesPersonPosition;

  //Variable used to hold the person density value in the filtered image
  int mPersonDensityValue;

  //Variable to hold the nature of the track - probationary/confirmed - true for probationary and false otherwise
  bool mProbationaryTrack;

  //Variable to hold the run length of high probability person
  int mRunLengthHPP;

  //Variable to hold the frequency of high probability person
  int mFreqHPP;

  //Vector to hold the person probability values for all the frames
  std::vector<int> mvPersonProbability;

  //Variable to hold bool indicating the track has been assigned
  bool mIsAssigned;

  //Variable to indicate the person probability status true for HPP and false for LPP
  bool mPersonProbabilityStatus;

  //Variable to hold the good track region in the image
  bool mGoodTrackRegionStatus;

  //Variable to hold the last convex polygon of the cart
  Polygon mCartPolygon;

  bool mUsePredictedTrajectory;

  Vector2f mPastPredictedTrajectory;

  Vector2f mPastPredictedMotion;

  std::vector<Polygon> mCartBoundingBox;

  //Directed rectangle for search region
  Polygon mSearchRectangle;

};


}; // namespace vision

}; // namespace ait

#endif // MultiFrameTrack_HPP


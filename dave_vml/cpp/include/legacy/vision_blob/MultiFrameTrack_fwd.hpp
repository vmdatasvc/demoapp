/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef MultiFrameTrack_fwd_HPP
#define MultiFrameTrack_fwd_HPP

#include <boost/shared_ptr.hpp>
#include <legacy/pv/CircularBuffer.hpp>
#include <legacy/pv/ait/Image_fwd.hpp>

namespace ait
{

namespace vision
{

class MultiFrameTrack;
typedef boost::shared_ptr<MultiFrameTrack> MultiFrameTrackPtr;
typedef CircularBuffer<Image8Ptr> ForegroundHistory;
typedef boost::shared_ptr< ForegroundHistory > ForegroundHistoryPtr;

}; // namespace vision

}; // namespace ait

#endif // MultiFrameTrack_fwd_HPP


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef MotionDetectionMod_fwd_HPP
#define MotionDetectionMod_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class MotionDetectionMod;
typedef boost::shared_ptr<MotionDetectionMod> MotionDetectionModPtr;

}; // namespace vision

}; // namespace ait

#endif // MotionDetectionMod_fwd_HPP

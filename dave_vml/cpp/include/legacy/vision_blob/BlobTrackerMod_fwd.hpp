/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef BlobTrackerMod_fwd_HPP
#define BlobTrackerMod_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class BlobTrackerMsg;
class BlobTrackerMod;
typedef boost::shared_ptr<BlobTrackerMod> BlobTrackerModPtr;

}; // namespace vision

}; // namespace ait

#endif // BlobTrackerMod_fwd_HPP


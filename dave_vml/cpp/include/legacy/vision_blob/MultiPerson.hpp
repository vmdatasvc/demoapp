/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef MultiPerson_HPP
#define MultiPerson_HPP

// SYSTEM INCLUDES
//



#include <boost/utility.hpp>

#include <boost/shared_ptr.hpp>

#include <boost/lexical_cast.hpp>

#include <vector>
#include <list>
#include <map>
#include <set>


// AIT INCLUDES
//

#include <legacy/types/String.hpp>

#include <legacy/vision_blob/Trajectory.hpp>

#include <legacy/vision_blob/BlobSizeEstimate.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

#include "legacy/vision_blob/MultiPerson_fwd.hpp"

namespace ait 
{

namespace vision
{ 


/**
 * This class processes raw trajectory input and makes inferences
 * about splits and merges in an attempt to improve the accuracy of the blob
 * tracker. A tree based approach is used. Given a series of trajectories in 
 * time, this class attempts to calculate what individual was at which
 * trajectory. The possibility of having multiple individuals being tracked
 *
 * @par Responsibilities:
 * - Transform raw trajectories into a set of paths corresponding to individuals
 *   - Clean up spurious tracks
 *   - Use split, merge, and other information to make better inferences
 * 
 * @remarks
 * This evaluation function of this class can be altered to use additional
 * information (histogram information, etc).
 * 
 */
class MultiPerson : private boost::noncopyable
{
  //
  // LIFETIME
  //

public:    

  /**
   * Constructor.
   */
  MultiPerson();

  /**
   * Destructor
   */
  virtual ~MultiPerson();

  /**
   * Initialization.
   */
  void init(const std::string& settingsPath, BlobSizeEstimatePtr pBlobSize, int width = 320, int height = 240);

protected:
  class Node;
  typedef boost::shared_ptr<Node> NodePtr;

  //
  // OPERATIONS
  //

public:

  /**
   * Process a list of trajectories, modifying the list so that it
   * contains the paths of each person (which are not identical to the paths
   * of each blob).
   */
  void processTrajectories(std::list<TrajectoryPtr>& trajList); 

  /**
   * Removes spurious tracks from consideration. This occurs before everything
   * else.
   */
  std::vector<TrajectoryPtr> removeSpuriousTrajectories(const std::list<TrajectoryPtr>& trajList);
  

protected:

  /**
   * Notification that a trajectory has been created.
   * @param pTraj The trajectory that has been created.
   */
  void beginTrack(TrajectoryPtr pTraj);

  /**
   * Notification that a trajectory has finished.
   * @param pTraj The trajectory that finished.
   * @param final Did the trajectory die a natural death (as opposed to merging)
   */
  void endTrack(TrajectoryPtr pTraj, bool final = false);

  /**
   * Notification that a group of tracks have merged with one trajectory
   */
  void mergeTrack(const std::set<TrajectoryPtr>& trajList, TrajectoryPtr pT1);

  /**
   * Notification that one track has split into several.
   */
  void splitTrack(TrajectoryPtr pT1, const std::set<TrajectoryPtr>& trajList);

  /**
   * Resets all current states and clears the graph
   */

  void resetAll();

  /**
   * Save a graph of the current system state to the given filename.
   * NOTE: requires ATT graphviz installed, and in path variable.
   */
  void saveGraph(std::string filename);
  
  /**
   * Prints raw debug information to the screen.
   */
  void printDebug();

  /**
   * When there is a split, makeDecision handles
   * deciding which person is assigned to which of the following
   * trajectories.
   */
  void makeDecision(const std::vector<int>& trajIdList);

  /**
   * Returns a value indicating the personId's suitability
   * in occupying the given node.
   */
  double evaluationFunc(Node* node, int personId);

  /**
   * Adds a person forward through the graph as long
   * as possible.
   */
  void addPersonForward(int personId, Node* node);

  /**
   * Adds a person backward in time through the graph as far as possible.
   * If a node has multiple parents, the evaluation function is used
   * to select which parent to propage this person through.
   */
  void addPersonBack(int personId, Node* node);

  /**
   * Creates a new person, and updates all relevant table entries.
   */
  int addPerson();

  /**
   * Given a personId, generate a path for this individual. Paths are accessed
   * via getProcessedPaths()
   */
  void processPerson(int personId);

  /**
   * Returns a list of paths corresponding to each person already
   * processed.
   */
  std::list<TrajectoryPtr> getProcessedPaths() {return mProcessedTraj;}  

  //
  // ACCESS
  //

public:


  //
  // INQUIRY
  //

public:

  /**
   * Ask if this object has been initialized (the init() function has been called).
   */
  bool isInitialized() const { return mIsInitialized; }


  //
  // ATTRIBUTES
  //  

protected: 

  /**
   * Node class used in the graph. Each node can have one trajectory
   * associated with it, multiple person IDs, and a count.
   */
  class Node
  {

  public:

    Node(TrajectoryPtr pTraj)
    {
      mCount = 1;
      mpTrajectory = pTraj;
    }

    virtual ~Node()
    {
      mpTrajectory.reset();
      mParents.clear();
      mChildren.clear();
      mPersons.clear();
    }

    TrajectoryPtr getTrajectory() {return mpTrajectory;}

    int parentCount() {return mParents.size(); }

    int childrenCount() {return mChildren.size();}

    void addChild(Node* pChild)
    {
      mChildren.push_back(pChild);
      pChild->addParent(this);
    }

    void addParent(Node* pParent)
    {
      mParents.push_back(pParent);
    }

    void unlink()
    {
      mParents.clear();
      mChildren.clear();
    }

    void addPersonId(int personId)  { mPersons.insert(personId); }

    int getCount() {return mCount; }

    TrajectoryPtr getTraj() {return mpTrajectory;}

    void setCount(int count) {mCount = count;}

    int getId() { return mpTrajectory->getId();}

    const std::set<int>& getPersonSet() const { return mPersons; }

    const std::vector<Node*>& getParentList() const { return mParents; }

    const std::vector<Node*>& getChildrenList() const {return mChildren; }

    void print()
    {
      std::string s;

      s = "T" + boost::lexical_cast<std::string>(mpTrajectory->getId()) + ":";

      std::set<int>::const_iterator iPerson;
      for (iPerson = mPersons.begin(); iPerson != mPersons.end(); iPerson++)      
        s += boost::lexical_cast<std::string>(*iPerson) + " ";

      s += "(Count " + boost::lexical_cast<std::string>(mCount) + ")\n\tChildren: ";

      std::vector<Node*>::const_iterator iNode;

      for (iNode = mChildren.begin(); iNode != mChildren.end(); iNode++)
        s += "T" + boost::lexical_cast<std::string>((*iNode)->getId()) + " ";

      s += "\n\tParents: ";

      for (iNode = mParents.begin(); iNode != mParents.end(); iNode++)
        s += "T" + boost::lexical_cast<std::string>((*iNode)->getId()) + " ";
      
      s  += "\n";

      PVMSG("%s\n", s.c_str());
    }

    //Used for graphviz nodes
    std::string getString()
    {
      std::string s;

      s = "T" + boost::lexical_cast<std::string>(mpTrajectory->getId()) + ":";

      std::set<int>::const_iterator iPerson;
      for (iPerson = mPersons.begin(); iPerson != mPersons.end(); iPerson++)      
        s += "P" + boost::lexical_cast<std::string>(*iPerson) + " ";

      s += "(Count " + boost::lexical_cast<std::string>(mCount) + ")";

      return s;
    }

  protected:
    std::vector<Node*> mChildren;
    std::vector<Node*> mParents;
    std::set<int> mPersons;
    int mCount;
   TrajectoryPtr mpTrajectory;
  };

  /**
   * Wrapper class used for easier result sorting when making decisions.
   */
  class evalResult
  {
  public:
    evalResult(int id, double score) {mId = id; mScore = score;}    

    bool operator<(const evalResult& other)
    {
      return mScore < other.mScore;
    }

    int getId() {return mId;}
    double getScore() {return mScore;}

  protected:
    int mId;
    double mScore;
  };

  /**
   * A decision is an object that keeps track of when it can be resolved.
   * When all trajectories related to the decision are finished, it can
   * be processed.
   */
  class Decision
  {
  public:

    Decision()
    {
    }

    virtual ~Decision()
    {
      mData.clear();
    }

    void setFinished(int id)
    {
      PVASSERT(mData.find(id) != mData.end());
      //set to finished state
      mData[id] = true;
    }

    void print()
    {
      std::string s;

      std::map<int, bool>::const_iterator iEntry;

      s += "[";

      for (iEntry = mData.begin(); iEntry != mData.end(); iEntry++)
      {
        s += aitSprintf(" (%d %s)", iEntry->first, iEntry->second ? "True" : "False");        
      }

      s += "]";

      PVMSG("%s\n", s.c_str());
    }

    void addId(int id) {mData[id] = false;}

    bool allFinished()
    {
      std::map<int, bool>::const_iterator iData;
      
      bool allDone = true;

      for (iData = mData.begin(); iData != mData.end(); iData++)
      {
        if (iData->second == false)
        {
          allDone = false;
          break;
        }
      }

      return allDone;
    }

    std::vector<int> getTrajList()
    {
      std::vector<int> l;

      std::map<int, bool>::const_iterator iData;
      for (iData = mData.begin(); iData != mData.end(); iData++)
        l.push_back(iData->first);

      return l;
    }

  protected:
    std::map<int, bool> mData;
  };

  typedef boost::shared_ptr<Decision> DecisionPtr;  

  /**
   * Tells if this object has been initialized.
   */
  bool mIsInitialized;

  /**
   * A map corresponding trajectory IDs with their associated nodes in the
   * graph.
   */
  std::map<int, NodePtr> mTrajTable;  
  
  /**
   * A map corresponding trajectory Ids with their related decisions.
   */
  std::map<int, DecisionPtr> mDecisionTable;
  
  /**
   * A map corresponding personId's to a list of trajectory Ids that they
   * are associated with.
   */
  std::map<int, std::set<int> > mPersonTable;

  /**
   * A queue of decisions that need to be processed. Older decisions precede
   * newer ones.
   */
  std::list<DecisionPtr> mDecisionQueue;

  /**
   * Table of finished trajectories.
   */
  std::vector<int> mFinishedTable;

  /**
   * Counter used for assigning person ids.
   */
  int mPersonCount;

  /**
   * A list of trajectories corresponding to processed person paths
   */
  std::list<TrajectoryPtr> mProcessedTraj;

  /**
   * Should a file ('input.png') of the trajectories given as an input
   * argument to processTrajectories() be saved in the output directory?
   */
  bool mSaveInputImages;

  /**
   * Should several files ('traj<xxx>.png') representing each stage 
   * in the decision making process be saved in the output directory?
   */
  bool mSaveTrajImages;

  /**
   * Should several files ('graph<xxx>.png') representing stage in the decision
   * making process be saved in the output directory?
   */
  bool mSaveGraphImages;

  /**
   * Should several files ('person<xxx>.png') representing each persons path
   * be saved in the output directory?
   */
  bool mSavePersonPaths;

  /**
   * Should a file ('finalTrajectories.csv') of the final trajectory output
   * be saved in the output directory?
   */
  bool mSaveFinalTrajs;

  /**
   * Should a file ('culled.png') of trajectories that made it past the culling
   * step be saved in the output directory?
   */
  bool mSaveCulledTrajImages;

  /**
   * Should a file ('good.png') of trajectories that made it past the culling
   * step be saved in the output directory?
   */
  bool mSaveGoodTrajImages;

  /**
   * Should messages be printed to the console.
   */
  bool mVerbose; 

  /**
   * Any trajectory younger than this age is culled.
   */
  double mMinTrajAge;

  /**
   * Minimum trajectory length relative to blob size. Shorter trajectories
   * culled.
   */
  double mMinTrajLength;

  /**
   * The maximum distance that two tracks will be allowed to merge, factor of blob size
   */
  double mMaxMergeDistance;

  /**
   * The maximum distance that two tracks will be allowed to split, factor of blob size
   */
  double mMaxSplitDistance;

  /**
   * A vector of colors used in visualizations
   */
  std::vector<unsigned int> mColor;

  /**
   * The height of the scene in pixels. Used for drawing visualizations.
   */
  int mSceneHeight;

  /**
   * The width of the scene in pixels. Used for drawing visualizations.
   */
  int mSceneWidth;

  /**
   * The folder that visualizations are dumped to
   */
  std::string mOutputFolder;

  /**
   * A pointer to the blob size module
   */
  BlobSizeEstimatePtr mpBlobSizeEstimate;
};


}; // namespace vision

}; // namespace ait

#endif // MultiPerson_HPP


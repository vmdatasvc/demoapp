/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef PersonShapeTrackerMod_fwd_HPP
#define PersonShapeTrackerMod_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class PersonShapeTrackerMod;
typedef boost::shared_ptr<PersonShapeTrackerMod> PersonShapeTrackerModPtr;

}; // namespace vision

}; // namespace ait

#endif // PersonShapeTrackerMod_fwd_HPP


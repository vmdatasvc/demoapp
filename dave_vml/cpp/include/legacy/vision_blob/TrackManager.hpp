/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef TrackManager_HPP
#define TrackManager_HPP

// SYSTEM INCLUDES
//

#include <list>
#include <vector>

// AIT INCLUDES
//

#include <legacy/math/Polygon.hpp>
#include <legacy/pv/ait/Rectangle.hpp>
// LOCAL INCLUDES
//

#include "legacy/vision_blob/BlobSizeEstimate.hpp"
#include "legacy/vision_blob/Trajectory.hpp"

// FORWARD REFERENCES
//

#include "legacy/vision_blob/TrackManager_fwd.hpp"

namespace ait 
{

namespace vision
{

/**
 * TrackManager handles the postprocessing of tracks. Postprocessing
 * includes compacting tracks, connecting broken tracks, detecting
 * track splits and merges, and removing spurious tracks.
 *
 * @par Responsibilities:
 * - Improve the accuracy of event detection through analysis of tracks
 */
class TrackManager
{ 
public:

  /**
   * The results class is used primarily for testing purposes, and allows
   * for a quick comparison between the choices made by the track manager
   * depending on its settings.
   */
  class Results
  {
  public:
    /**
     * Tuple is a simple structure with two attributes, from and to, and is
     * used by Results to keep track of the TrackManagers decisions.
     */
    struct Tuple
    {
      int from;
      int to;

      /**
       * Print the tuple	
       */
      void print()
      {
        std::cout << from << " - " << to;
      }

      bool operator==(const Tuple& other)
      {
        return other.from == from && other.to == to;
      }
    };
    typedef boost::shared_ptr<Tuple> TuplePtr;

    //
    // OPERATIONS
    //

    /**
     * Print the overall results	
     */
    void print()
    {
      std::cout << "Number of connects: " << mConnected.size() << std::endl;
      std::cout << "Number of merges: " << mMerged.size() << std::endl;
      std::cout << "Number of splits: " << mSplit.size() << std::endl;
      std::cout << "Number of spurious: " << mSpurious.size() << std::endl;

      std::vector<TuplePtr>::const_iterator iTuple;

      std::cout << "Connections: " << std::endl;
      for (iTuple = mConnected.begin(); iTuple != mConnected.end(); iTuple++)
      {
        std::cout << "\t";
        (*iTuple)->print();
        std::cout << std::endl;
      }

      std::cout << "Merges: " << std::endl;
      for (iTuple = mMerged.begin(); iTuple != mMerged.end(); iTuple++)
      {
        std::cout << "\t";
        (*iTuple)->print();
        std::cout << std::endl;
      }

      std::cout << "Splits: " << std::endl;
      for (iTuple = mSplit.begin(); iTuple != mSplit.end(); iTuple++)
      { 
        std::cout << "\t";
        (*iTuple)->print();
        std::cout << std::endl;
      }

      std::vector<int>::const_iterator iInt;
      std::cout << "Spurious: " << std::endl;
      for (iInt = mSpurious.begin(); iInt != mSpurious.end(); iInt++)
      {
        std::cout << "\t" << *iInt << std::endl;
      }
    }  
    
    /**
     * Read the results from a specified XML file. The basic
     * format is (the numbers are the trajectory ID's):
     *
     *  <results>
     *    <connect>
     *      <from>1<</from>
     *      <to>2</to>
     *    </connect>
     *    <connect>
     *      <from>3<</from>
     *      <to>4</to>
     *    </connect>
     *    ...
     *    <split>
     *      <from>3<</from>
     *      <to>4</to>
     *    </split>
     *    <merge>
     *      <from>3<</from>
     *      <to>4</to>
     *    </merge>
     *    ...
     *    <spurious>1</spurious>
     *
     * e.g.
     * connect trajectory 12 to 14 (from = 12, to = 14)
     * trajectory 12 splits from 11 (from = 12, to = 11)
     * trajectory 10 merges to 12 (from = 10, to = 12)
     */
    static Results readFromFile(const std::string& filename);  

    /**
     * compare these results with other results.
     * This assumes that the other results are correct (usually loaded from
     * a file generated by hand), and returns the number of false positives
     * and false negatives.
     */
    int compare(const Results& other);

    /**
     * Add a connection to these results	
     */
    void addConnect(TuplePtr pTuple)
    {
      mConnected.push_back(pTuple);
    }

    /**
     * Add a merge to these results	
     */
    void addMerge(TuplePtr pTuple)
    {
      mMerged.push_back(pTuple);
    }

    /**
     * Add a split to these results	
     */
    void addSplit(TuplePtr pTuple)
    {
      mSplit.push_back(pTuple);
    }

    /**
     * Add a spurious to these results	
     */
    void addSpurious(int val)
    {
      mSpurious.push_back(val);
    }  

  protected:
    /**
     * A list of the trajectories chosen to be connected	
     */
    std::vector<TuplePtr> mConnected;
    /**
     * A list of trajectories chosen to be merged	
     */
    std::vector<TuplePtr> mMerged;
    /**
     * A list of trajectories chosen to be split	
     */
    std::vector<TuplePtr> mSplit;
    /**
     * A list of trajectories chosen to be spurious	
     */
    std::vector<int> mSpurious;
  };
  typedef boost::shared_ptr<Results> ResultsPtr;
  
  /**
   * This class holds the settings information for the track manager.	
   */
  class Settings
  {
  public:  
            
    //Settings
    //int nodesConsidered() {return (int)mSettings[0];}

    // Weights
    /* CONNECTING BROKEN DOES NOT WORK WELL YET
    float extensionProxWeight() {return mSettings[1];}
    float directionWeight() {return mSettings[2];}    
    float predictedPosWeight() {return mSettings[3];}
    float timeBetweenWeight() {return mSettings[4];}    
    float speedWeight() {return mSettings[5];}

    float distanceAtTimeWeight() {return mSettings[6];}
    float directionAtTimeWeight() {return mSettings[7];}
    */
    
    
    /*
    float distanceBetweenWeight() {return mSettings[8];}
    float directionWeight() {return mSettings[9];}
    float speedDifferenceWeight() {return mSettings[10];}
    */
    
    float nodesConsidered;
    float splitMergeDistanceWeight;
    float compactThreshold;
   
  };
  typedef boost::shared_ptr<Settings> SettingsPtr;
  
public:
  
  /**
   * Default Constructor	
   */
  TrackManager()
  { 
    mCompactThreshold = 1.0;
    mSplitMergeDistanceWeight = 0.0;
    mFps = 15;
  }

  void init(BlobSizeEstimatePtr pBlobEst)
  {

    assert(pBlobEst.get());

    mpBlobSizeEstimate = pBlobEst;

    /*
    mRoi.x0 = (float)roi.x0;
    mRoi.x1 = (float)roi.x1;
    mRoi.y0 = (float)roi.y0;
    mRoi.y1 = (float)roi.y1;  
    */
  }
  
  //void useSettings(SettingsPtr s) {mpSettings = s;}   
  
  void printStatistics();

  void process(std::list<TrajectoryPtr>& trajList);

  void processCompact(std::list<TrajectoryPtr>& trajList);

  void connectBroken(const std::list<TrajectoryPtr>& trajList);
  void processMerge(const std::list<TrajectoryPtr>& trajList);
  void processSplit(const std::list<TrajectoryPtr>& trajList);
  void processSpurious(std::list<TrajectoryPtr>& trajList);
  void processExtrapolate(const std::list<TrajectoryPtr>& trajList);

  // TODO: merge, split, extrapolate   

  

  void setCompactThreshold(float thresh) {mCompactThreshold = thresh;}
  float getCompactThreshold() {return mCompactThreshold;}

  void setSplitMergeDistanceWeight(float weight) 
  {
    mSplitMergeDistanceWeight = weight;
  }

  float getSplitMergeDistanceWeight() {return mSplitMergeDistanceWeight;}  

  void setMaxExtrapolateDistance(float thresh) {mMaxExtrapolateDistance = thresh;}

  void setSpuriousThreshold(float thresh) {mSpuriousThreshold = thresh;}
  

  void setFps(float fps) {mFps = fps;}
  float getFps() {return mFps;}  

protected: 
  Rectanglef mRoi;
  BlobSizeEstimatePtr mpBlobSizeEstimate;

  float mCompactThreshold;
  float mSplitMergeDistanceWeight;
  float mMaxExtrapolateDistance;

  float mSpuriousThreshold;

  float mFps;
  
  

};

}; // namespace vision
}; // namespace ait

#endif // TrackManager_HPP


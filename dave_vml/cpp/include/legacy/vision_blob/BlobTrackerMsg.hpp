/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef BlobTrackerMsg_HPP
#define BlobTrackerMsg_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>

// AIT INCLUDES
//

#include <Object.hpp>
#include <String.hpp>

// LOCAL INCLUDES
//

#include "VisionMsg.hpp"

// FORWARD REFERENCES
//

namespace ait 
{

namespace vision
{


/**
 * BlobTrackerMod vision messages.
 * @par Responsibilities:
 * - Type of information to be carried in these messages.
 */
class BlobTrackerMsg : public VisionMsg
{
public:

  //
  // LIFETIME
  //

  /**
   * Constructor.
   */
  BlobTrackerMsg(VisualSensorId vid = VisualSensorId(), double timeStamp = 0) 
    : VisionMsg(vid,timeStamp)
  {
  }

  /**
   * Destructor
   */
  virtual ~BlobTrackerMsg()
  {
  }

  //
  // OPERATIONS
  //

public:

  //
  // ACCESS
  //

public:

  //
  // INQUIRY
  //

public:

  //
  // ATTRIBUTES
  //

  TrajectoryPtr getTrajectory() { return mpTrajectory; }
  void setTrajectory(TrajectoryPtr pT) { mpTrajectory = pT; }

public:

  enum
  {
    /// This is an example flag. It must start in 0x10 (the other bits are reserved for VisionMsg).
    POST_PROCESSED_TRAJECTORY             = 0x00000010,
  };

protected:

  TrajectoryPtr mpTrajectory;

};

/// Smart pointer to BlobTrackerMsg
typedef boost::shared_ptr<BlobTrackerMsg> BlobTrackerMsgPtr;

}; // namespace vision

}; // namespace ait

#endif // BlobTrackerMsg_HPP


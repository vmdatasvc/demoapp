/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef BackgroundModelCb_HPP
#define BackgroundModelCb_HPP

#include <legacy/pv/ait/Rectangle.hpp>
#include <legacy/pv/ait/Image.hpp>
#include <legacy/low_level/Settings.hpp>

#include <list>


// FORWARD REFERENCES
//
#include "legacy/vision_blob/BackgroundModelCb_fwd.hpp"

namespace ait
{
namespace vision
{

class BackgroundModelCb
{
public:

  /**
   * This class contains color, intensity, and access information
   * that is used by the codebook algorithm.
   */
  class Codeword
  {
  public:

    /**
     * Base contructor.
     *
     * @param r Red pixel value
     * @param g Green pixel value
     * @param b Blue pixel value
     * @param brightness inner_product of <r,g,b>
     * @param startTime The starting time of the inital learning period
     * @param currentTime The current time
     *
     * @note The start time is relevant for the initial learning period
     * only, and is used primarily to wrap the negative run length
     * values around the time interval. When the layered learning
     * is being used, start time is just set to the current time.
     */
    Codeword(unsigned int r, unsigned int g, unsigned int b, 
             double brightness, double startTime, double currentTime);

    //HACK, DEFAULT CONSTRUCTOR
    Codeword() {};      

    /**
     * Update the codeword.
     * @param r Red pixel value
     * @param g Green pixel value
     * @param b Blue pixel value
     * @param brightness inner_product of <r,g,b>
     * @param currentTime The current time
     */
    void update(unsigned int r, unsigned int g, unsigned int b, 
                double brightness, double currentTime);

    /**
     * This function is called only at the end of the initial learning
     * period to 'wrap' the negative run length values around if needed.
     * @param startTime The start of the learning period
     * @param endTime The end of the learning period
     */
    void wrapNegRunLength(double startTime, double endTime);

    /**
     * Return the red vector component of this codeword.
     */
    double getRed() const {return mRed;}
    /**
     * Return the green vector component of this codeword.
     */
    double getGreen() const {return mGreen;}

    /**
     * Return the blue vector component of this codeword.
     */
    double getBlue() const {return mBlue;}

    /**
     * Return the negative run length of the codeword.
     */
    double getNegRunLength() const {return mMaxNegRunLength;}

    /**
     * Returns the first time that this codeword was created/updated.
     */
    double getFirstAccessTime() const {return mFirstAccessTime;}

    /**
     * Returns the last time this codeword was updated.
     */
    double getLastAccessTime() const {return mLastAccessTime;}

    /**
     * Returns the number of times this codeword has been encountered
     */
    unsigned int getFrequency() const {return mFrequency;}

    /**
     * Return the minimum brightness value encountered.
     */
    double getMinBrightness() const {return mMinBrightness;}

    /**
     * Return the maximum brightness value encountered
     */
    double getMaxBrightness() const {return mMaxBrightness;}

  protected:
    double mMinBrightness;
    double mMaxBrightness;
    unsigned int mFrequency;
    double mMaxNegRunLength;
    double mFirstAccessTime;
    double mLastAccessTime;

    double mRed;
    double mGreen;
    double mBlue;

  };



	//
	// LIFETIME
	//
  /**
   * Create a codebook-based background model.
   * @param settingsPath The location of the settings for this model.
   */
  BackgroundModelCb(const std::string& settingsPath); 

	//
	// OPERATIONS
	//	

  /**
   * Attempt to incorporate the given frame into the background model.
   * @param img The original input frame
   * @param mask A mask image, 0-value pixels will not be learned.
   * @param currentTime The current time
   */
  void learn(const Image32& img, const Image8& mask, 
             double currentTime);

  /**
   * Perform foreground segmentation against this background model, without
   * modifying the internal model.
   * @param img The original input image
   * @param mask A mask image, foreground segmentation will not be performed
   * on 0-value pixels
   * @param outputImage The foreground segmentation is written here
   * @param currentTime The current time
   */
  void foregroundSegment(const Image32& img, const Image8& mask,
                         Image8& outputImage, double currenTime) const;

  /**
   * This function updates the background model outside of the learning scope.
   * For example, we may not learn every frame, but may want to make a decision
   * every frame regarding which parts of the model to discard. That code 
   * goes here.
   * @param The original image frame
   * @param mask A mask image, the models for the pixels with 0-value will not
   * be updated.
   * @param currentTime The current time
   */
  void update(const Image32& img, const Image8& mask, 
              double currentTime);

  /**
   * Draw a visualization representing this model.
   * @param img The original input image
   * @param mask A mask image. Visualization will not be performed
   * for the pixels that are 0-value.
   * @param outputImage The visualization is written to this image. Its
   * size may change.
   */
  void visualize(const Image32& img, const Image8& mask, 
                 Image32& outputImg) const;



  double getStartLearnDuration() const {return mStartLearnDuration;}

  void setStartLearnDuration(double val) {mStartLearnDuration = val;}

  bool isFinishedLearning() const {return mLearningStatus == FINISHED;}

protected:

  /**
   * This function is called while we are constructing the initial
   * codebook during the initial learning period.
   * @param img The initial source image
   * @param mask codebooks are not constructed where the mask pixels are
   * 0-value
   * @param currentTime The current time
   */
  void constructInitialCodebook(const Image32& img, const Image8& mask, 
                                double currentTime);

  /**
   * This function is called once the initial codebook training is completed.   
   * @param img The original source image
   * @param mask A mask image determine which pixels should be modified. 
   * 0-value pixels are ignored.
   * @param currentTime The current time
   */
  void finishedInitialCodebook(const Image32& img, const Image8& mask, 
                               double currentTime);

  /**
   * Scan through a list of codewords for a match. If a match exists, update
   * the codeword.
   * @param r Red pixel value
   * @param g Green pixel value
   * @param b Blue pixel value
   * @param brightness inner_product of <r,g,b>
   * @param cwList A list of code words
   * @param currentTime The current time
   */
  bool matchAndUpdate(unsigned int r, unsigned int g, unsigned int b, 
    double brightness, std::list<Codeword>& cwList, double currentTime);

  /**
   * This function is called within learn() each time when the initial learning
   * period is over.
   * @param img The original source image
   * @param mask A mask image, 0-value pixels are not learned.
   * @param currentTime The current time
   */
  void layeredLearning(const Image32& img, const Image8& mask, 
                       double currentTime);

  /**
   * This function returns true if an (r,g,b) vector is 'close' enough to the
   * given codeword to be considered a color match.
   * @param r Red pixel value
   * @param g Green pixel value
   * @param b Blue pixel value
   * @param cw The codeword used for comparison
   */
  bool matchColor(unsigned int r, unsigned int g, unsigned int b, const Codeword& cw) const;

  /**
   * This function is used to determine if the given brightness is
   * 'close' enough to the given codeword to be considered a match.
   */
  bool matchBrightness(double brightness, const Codeword& cw) const;

  /**
   * This function takes a given image of codebooks and performs background
   * subtraction on it and the image.
   * @param wordImg An image of codebooks
   * @param img The original source image
   * @param mask A mask image, 0-value pixels are not considered
   * @param outputImage the output image we draw the result to.
   */
  void segmentCodebook(const Image<std::list<Codeword>  >& wordImg, const Image32& img,
    const Image8& mask, Image8& outputImage) const;

  /**
   * The status of the initial learning state
   */
  enum Status {NOT_STARTED, IN_PROGRESS, FINISHED};

  /**
   * This variable keeps track of whether we are learning the initial learning
   * period or not.
   */
  Status mLearningStatus;  

  /**
   * The time that we started learning the initial learning period. Generally
   * the first time that learn() was called.
   */
  double mStartLearnTime;

  /**
   * The duration that we would like the initial learning period to last.
   */
  double mStartLearnDuration;

  /**
   * After the initial learning period, codewords with negative run lengths
   * greater than this fraction of the start learn duration are culled.
   */
  double mStartLearnMaxNrlFactor;

  /**
   * The main codebook model. Segmentation is performed on this model.
   */
  Image< std::list<Codeword> > mCodebook;

  /**
   * A secondary cache model used for layered learning.
   */
  Image< std::list<Codeword> > mCache;

  /**
   * Has learn() been called at least once?
   */
  bool mLearnedOnce;

  /**
   * The color threshold used by matchColor() to determine if a codeword is a
   * match.
   */
  double mColorThresh;

  /**
   * This value is used by matchBrightness(). It roughly approximates the
   * height of the cylinder that is used to determine if the brightness
   * levels match. It ranges from [0,1), typically [0.4,0.7].
   */
  double mAlphaScale;

  /**
   * This value is used by matchBrightness(). It should be > 1. Typical
   * values lie from 1.1 to 1.5.
   */
  double mBetaScale;


  /**
   * If a codeword has been in the cache longer than this threshold,
   * it is then promoted to the main model.
   */
  double mCachePromoteThresh;

  /**
   * If a codeword in the cache has not been matched to a pixel
   * in the image for this time, it is removed from the cache.
   */
  double mCacheRemoveThresh;

  /**
   * If a codeword in the main model has not been matched to a pixel
   * in the image for this time, it is removed from the main model.
   */
  double mModelRemoveThresh;

  /**
   * The settings class for reading values from XML or memory.
   */
  Settings mSettings;
};


} //namespace vision
} //namespace ait


#endif //BackgroundModelCb_HPP

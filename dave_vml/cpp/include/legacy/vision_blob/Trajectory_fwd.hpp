/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef Trajectory_fwd_HPP
#define Trajectory_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class Trajectory;
typedef boost::shared_ptr<Trajectory> TrajectoryPtr;


}; //namespace vision

}; // namespace ait

#endif // Trajectory_fwd_HPP


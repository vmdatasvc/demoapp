/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef TrackManager_fwd_HPP
#define TrackManager_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class TrackManager;
typedef boost::shared_ptr<TrackManager> TrackManagerPtr;

}; // namespace ait

#endif // Trajectory_fwd_HPP


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef DoorCounterMod_fwd_HPP
#define DoorCounterMod_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class DoorCounterMod;
typedef boost::shared_ptr<DoorCounterMod> DoorCounterModPtr;

}; // namespace vision

}; // namespace ait

#endif // DoorCounterMod_fwd_HPP


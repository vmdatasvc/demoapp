/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef DoorCounterMod_HPP
#define DoorCounterMod_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <list>
#include <map>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>
#include <legacy/pv/ait/Image.hpp>
//#include <Benchmark.hpp>
//#include <PvImageProc.hpp>

// LOCAL INCLUDES
//

//#include "RealTimeMod.hpp"
//#include <ImageAcquireMod.hpp>
#include "legacy/vision_blob/SimplePolygon.hpp"

#include <legacy/vision_blob/BlobTrackerMod.hpp>

#include <legacy/low_level/Settings.hpp>
// FORWARD REFERENCES

#include "legacy/vision_blob/DoorCounterMod_fwd.hpp"
#include "legacy/vision_blob/BlobTrackerMod_fwd.hpp"


namespace ait 
{

namespace vision
{

int test_int_code(int t);
/**
 * This vision module is not yet documented.
 * @par Responsibilities:
 * - Write the responsibilities here.
 */
class DoorCounterMod : public BlobTrackerMod
{
public:

  //
  // LIFETIME
  //

  /**
   * Constructor. The constructor creates a quick instance. Expensive operations
   * are executed in the init() function.
   */
  DoorCounterMod();

  /**
   * Destructor
   */
  virtual ~DoorCounterMod();  

  /**
   * Initialization function.
   */
  virtual void init(Settings &set);

  /**
   * Finalization function
   */
  virtual void finish();

  //
  // OPERATIONS
  //

public:

  /**
   * Process this module.
   */
  virtual void process();

  /**
   * Visualize this module.
   */
  virtual void visualize(Image32& img);
  

protected:

  class TrajData
  {
  public:

    /**
     * Construct a trajData object. This object is essentially a wrapper around
     * a trajectory that extracts useful information later used to classify
     * the trajectory as an enter/exit/unkown event.
     *
     * @param doorLocation a LineSegment representing layout of door in 2d
     * space
     * @param treatDoorAsPoint If true, distance to the door is treated
     * as the distance to the door segments midpoint. Otherwise, distance
     * is defined to represent the closest distance to any point on the line
     * segment.
     * @param enterDirection A unit vector specifying flow of 'enter'
     * trajectories.
     * @param pBlobSizeEst An unscaled, uncropped blob size estimator object
     * The blob size is used to normalize track distances.
     */
    TrajData(const LineSegment& doorLocation, 
             bool treatDoorAsPoint,
             const Vector2f& enterDirection,                   
             const BlobSizeEstimatePtr& pBlobSizeEst)
    {
      mDir = Vector2f(0.0,0.0);
      mDisp = -1.0;
      mStartDispFromDoor = -1.0;
      mEndDispFromDoor = -1.0;

      mTreatDoorAsPoint = treatDoorAsPoint;

      mDoorLocation = doorLocation;
      mDoorCenter = mDoorLocation.midpoint();
      mEnterDirection = enterDirection;

      assert(pBlobSizeEst.get());
      mpBlobSizeEstimate = pBlobSizeEst;
    }

    /**
     * Returns the direction of the trajectory as defined by
     * the unit displacement vector from start to end point.
     */
    Vector2f getDirection() const { return mDir; }

    /**
     * Returns the dot product of the unit trajectory direction with the door
     * direction.
     */
    double getDirWithDoorDotProduct() const { return mDotProd; }

    /**
     * Returns the displacement distance from start to end of the trajectory.     
     */     
    double getDisplacement() const { return mDisp; }

    /**
     * Returns the distance from the start of a track to the door segment
     * or door center, depending on the setting mTreatDoorAsPoint.
     */
    double getStartDispFromDoor() const { return mStartDispFromDoor; }

    /**
     * Returns the distance from the end of a track to the door segment
     * or door center, depending on the setting mTreatDoorAsPoint.
     */
    double getEndDispFromDoor() const { return mEndDispFromDoor; }

    /**
     * Returns a pointer the trajectory passed to extractData.
     */
    TrajectoryPtr getTrajectory() const { return mpTraj; }

    /**
     * Extract useful information from a the given trajectory,
     * and populate the internal member variables for later reference.
     */
    void extractData(const TrajectoryPtr& pTraj)
    {
      assert(pTraj.get());
    
      //Keep pointer to trajectory for later reference
      mpTraj = pTraj;

      //Calculate direction unit vector, (0,0) if undefined
      Vector2f startPos = pTraj->getStartPos();
      Vector2f endPos = pTraj->getEndPos();

      Vector2f dir;
      dir.x = endPos.x - startPos.x;
      dir.y = endPos.y - startPos.y;

      if (dir.x != 0.0 || dir.y != 0.0)
      {
        dir.normalize();
        mDir = dir;
        mDotProd = dir.dot_prod(mEnterDirection);

        //Calculate displacement, normalized by blob size

        //Use blob size at end of Trajectory
        float w = 0;
        float h = 0;
        mpBlobSizeEstimate->getEstimatedSize(endPos.x, endPos.y,w,h);

        float sizeDiv = (w + h)/2.0;

        float disp = sqrt(dist2(startPos,endPos));
        mDisp = disp/sizeDiv;

        float startDispFromDoor = -1.0;
        float endDispFromDoor = -1.0;

        if (mTreatDoorAsPoint)
        {
          startDispFromDoor = sqrt(dist2(startPos,mDoorCenter));
          endDispFromDoor = sqrt(dist2(endPos,mDoorCenter));
        }
        else
        {
          int pos = 0;
          startDispFromDoor = mDoorLocation.distanceToPoint(startPos,pos);
          endDispFromDoor = mDoorLocation.distanceToPoint(endPos,pos);          
        }        

        mStartDispFromDoor = startDispFromDoor/sizeDiv;
        mEndDispFromDoor = endDispFromDoor/sizeDiv;             
      }
      else
      {
        mDir = Vector2f(0.0,0.0);
        mDisp = 0.0;
      }
    }

  protected:
    /** 
     * Unit vector representing trajectory direction.
     */
    Vector2f mDir;

    /**
     * Overall trajectory displacement distance, normalized by blob size.
     */
    double mDisp;

    /**
     * Distance from start of trajectory to door or door center, normalized by
     * blob size.
     */
    double mStartDispFromDoor;

    /**
     * Distance from end of trajectory to door or door center, normalized by
     * blob size.
     */
    double mEndDispFromDoor;

    /**
     * Dot product of mDir and mEnterDirection
     */
    double mDotProd;

    /**
     *If true, distance to the door is treated
     * as the distance to the door segments midpoint. Otherwise, distance
     * is defined to represent the closest distance to any point on the line
     * segment.     
     */
    bool mTreatDoorAsPoint;

    /**
     * Pointer to trajectory object.
     */
    TrajectoryPtr mpTraj;

    /**
     * A pointer to an unscaled, uncropped blob size esitmate object. Used for
     * normalizing distance.
     */
    BlobSizeEstimatePtr mpBlobSizeEstimate;

    /**
     * The unit vector representing motion into the store.
     */
    Vector2f mEnterDirection;

    /**     
     * Midpoint of mDoorLocation
     */
    Vector2f mDoorCenter;

    /**
     * A LineSegment representing the layout of the door in 2d.
     */
    LineSegment mDoorLocation;
  };

  virtual void cleanForegroundSegmentation(Image8& img, const Vector2f& pt);

  virtual void checkOverlappingTracks();

  /**
   * Generate a repulsion vector for each track
   */
  virtual void calculateTrackForce();

  virtual Vector2f calculateTrackRepulsion(MultiFrameTrackPtr pTrack);

  virtual Vector2f pointRepulsion(const Vector2f& p1, const Vector2f& p2);

  virtual void postProcessTracks(bool force); 
  
  /**
   * Returns true if a trajectory is entering the door
   */
  virtual bool isEnterTrajectory(const TrajData& td) const;

  /**
   * Returns true if a trajectory is exiting the door
   */  
  virtual bool isExitTrajectory(const TrajData& td) const;

  virtual void generateExitEvent(const TrajData& td);

  virtual void generateEnterEvent(const TrajData& td);

  virtual void generateUnknownEvent(const TrajData& td);

  /**
   * Appends additional debug information to in/out/other events if desired.
   */
  virtual void appendDebugInformation(const TrajData& td);

  virtual void compressTraj(std::list<TrajectoryPtr>& trajList);  
 
private:

  //
  // ACCESS
  //

public:

  /**
   * Get the static name of this module. This name is used to name the type of this module.
   * The constructor sets the member variable mName to this value.
   * @remark This static function is only implemented for concrete classes.
   */
  static const char *getModuleStaticName() { return "DoorCounterMod"; }

  //
  // INQUIRY
  //

public:

  //
  // ATTRIBUTES
  //

protected: 

  /**
   * Trajectories classified as leaving the store
   */
  std::list<TrajectoryPtr> mExitTrajectories;

  /**
   * Trajectories classified as entering the store
   */
  std::list<TrajectoryPtr> mEnterTrajectories;

  /**
   * Trajectories that do not fall into entry or exit groups
   */
  std::list<TrajectoryPtr> mUnknownTrajectories; 

  /**
   * Direction vector indicating travel inside of the store
   */
  Vector2f mEnterDirection;  

  /**
   * Line segment representing threshold of door
   */
  LineSegment mDoorLocation;

  /**
   * Number of blob sizes from the door location an enter track may start from.
   */
  double mMaxStartDistance;

  /**
   * Number of blob sizes from the door location an exit track may start from
   */
  double mMaxEndDistance;

  /**
   * Minimum displacement (in blob sizes) for a track to be considered as an entry or exit event.
   */
  double mMinTrackDisplacement;

  /**
   * The maximum maximum dot product between a trajectory vector and mEnterDirection.
   * A trajectory not meeting the following requirements will not be counted:
   * For enter: dotProd > mMaxDirectionDotProduct
   * For exit: dotProd < -mMaxDirectionDotProduct
   */
  double mMaxDirectionDotProduct;  

  /**
   * The unscaled blob size estimator.
   */
  BlobSizeEstimatePtr mpUnscaledBlobSizeEstimate;

  /**
   * If enabled, additional debug information will be send along with each
   * in/out event.
   */
  bool mAdditionalDebugInformation;

  /**
   * If true, distance measurements wrt the door will be taken from the midpoint
   * of the door. If false, the closest distance to the door segment will be used.   
   */
  bool mTreatDoorAsPoint;

};

}; // namespace vision

}; // namespace ait

#endif // DoorCounterMod_HPP



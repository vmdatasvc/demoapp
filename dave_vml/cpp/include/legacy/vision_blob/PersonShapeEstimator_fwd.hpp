/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef PersonShapeEstimator_fwd_HPP
#define PersonShapeEstimator_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{
class PersonShapeEstimator;

typedef boost::shared_ptr<PersonShapeEstimator> PersonShapeEstimatorPtr;

}; // namespace vision

}; // namespace ait

#endif // PersonShapeEstimator_fwd_HPP


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ForegroundSegmentMod_fwd_HPP
#define ForegroundSegmentMod_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class ForegroundSegmentMod;
typedef boost::shared_ptr<ForegroundSegmentMod> ForegroundSegmentModPtr;

}; // namespace vision

}; // namespace ait

#endif // ForegroundSegmentMod_fwd_HPP


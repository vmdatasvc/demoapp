/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ForegroundSegmentMod_HPP
#define ForegroundSegmentMod_HPP

// SYSTEM INCLUDES
//

#include <fstream>
#include <boost/utility.hpp>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>
#include <list>
#include <map>
#include <legacy/pv/ait/Image.hpp>
//#include <Benchmark.hpp>
#include <legacy/pv/ait/Matrix.hpp>
#include <legacy/pv/CircularBuffer.hpp>

// LOCAL INCLUDES
//

#include "legacy/vision/RealTimeMod.hpp"
//#include "VmsPacket.hpp"
#include "legacy/vision_blob/SimplePolygon.hpp"

//TODO: TEMP
#include "legacy/vision_blob/MotionDetectionMod.hpp"
//END TEMP

// FORWARD REFERENCES
//

#include "legacy/vision_blob/ForegroundSegmentMod_fwd.hpp"
#include "legacy/vision_tools/BackgroundLearner_fwd.hpp"
#include "legacy/vision_blob/BlobSizeEstimate_fwd.hpp"

namespace ait 
{

namespace vision
{

//MOTION FUNCTIONS
  static Image8Ptr getDiffImage(const Image8& img1, const Image8& img2)
  {
    Image8Ptr pDiffImage(new Image8(img1.width(),img1.height()));
    for (int i = 0; i < pDiffImage->size(); i++)
    {
      int val = (int)img1(i)-(int)img2(i);
      (*pDiffImage)(i) = val < 0 ? (Uint8)-val : val;
    }
    return pDiffImage;
  }

  static Image8Ptr getDiffImage(CircularBuffer<Image8Ptr>& imgs)
  {
    const float amplification = 2.0f/(imgs.size()-1);
    //const float amplification = 5.0f/(imgs.size()-1); //TEMP AMPLIFICATION FOR EXP
    Image8Ptr pDiffImage(new Image8(imgs[0]->width(),imgs[1]->height()));
    for (int i = 0; i < pDiffImage->size(); i++)
    {
      float diff = 0;
      for (int j = 0; j < imgs.size()-1; j++)
      {
        diff += (float)(*imgs[j])(i)-(float)(*imgs[j+1])(i);
      }
      //(*pDiffImage)(i) = (Uint8)std::min(ait::round(abs(diff)*amplification),255);
      (*pDiffImage)(i) = 128+(Uint8)std::min(std::max(ait::round(diff*amplification),-128),127);
    }
    return pDiffImage;
  }


/**
 * This vision module is not yet documented.
 * @par Responsibilities:
 * - Write the responsibilities here.
 */
class ForegroundSegmentMod : public VisionMod	// For research, we do not use realtime modules
{
public:

  //
  // LIFETIME
  //

  /**
   * Constructor. The constructor creates a quick instance. Expensive operations
   * are executed in the init() function.
   */
  ForegroundSegmentMod();

  ForegroundSegmentMod(const std::string& moduleName);

  /**
   * Destructor
   */
  virtual ~ForegroundSegmentMod();

  /**
   * Provide dependency information for the ExecutionGraph.
   * @see VisionMod::initExecutionGraph for more info.
   */
//  virtual void initExecutionGraph(std::list<VisionModPtr>& srcModules);

  // set up dependency. replacement for initExecutionGraph
  virtual void initExecution();

  /**
   * Initialization function.
   */
  virtual void init();

  /**
   * Finalization function
   */
  virtual void finish();

  //
  // OPERATIONS
  //

public:

  /**
   * Process this module.
   */
  virtual void process();

  /**
   * Visualize this module.
   */
  virtual void visualize(Image32& img);

  /**
   * Given the input frame, return a new frame with the
   * ROI in the desired frame size. If the ROI is the
   * same as the frame, then return the given parameter.
   *   
   */
  virtual Image8Ptr getRoiFrame(Image32Ptr pImg);

  //virtual Image32Ptr getColorRoiFrame(Image32Ptr pImg);

  /**
   * Filter the input image based on the blob size estimates.
   */
  virtual void filterByBlobSize(const Image8& foreground, Image8& output);

  /**
   * Learn the background from a video file.
   */
  virtual void learnBackgroundFromVideo();

  /**
   * Learn the blob size from the video file.
   */
  virtual void learnBlobSize();

  /**
   * Checks periodically for new objects which should be integrated into the
   * background.
   */
  virtual void checkBackground();

  /**
   * Estimate the scaling factor of the region of interest
   * based on the average blob size for the given ROI.
   */
  virtual void estimateScaleFactor();

  /**
   * Get the name of the current video file.
   */
  virtual std::string getVideoFileName();
  
  virtual Bool isAsync() const {return true;}	// for research, it is always async
  
protected:

  virtual void saveSegmentationOutput();

  //
  // ACCESS
  //

public:

  /**
   * Get the static name of this module. This name is used to name the type of this module.
   * The constructor sets the member variable mName to this value.
   * @remark This static function is only implemented for concrete classes.
   */
  static const char *getModuleStaticName() { return "ForegroundSegmentMod"; }

  /**
   * Get the foreground segmented image.
   */
  virtual Image8Ptr getForeground();

  /**
   * Get the Filtered by blob size image.
   */
  virtual Image8Ptr getFilteredForeground();

  /**
   * Get the region of interest from the original image.
   * This is the region of interest before scaling the 
   * image.
   */
  
  virtual const Rectanglei getRoiRectangle() {return mRegionOfInterest; }

  /**
   * Get the scale factor for each axis used to resample every image.
   */
  virtual const Vector2f& getRoiScaleFactor() const { return mRoiScaleFactor; }

  /**
   * Return the blob size estimator with the ROI transformations
   * applied.
   */
  virtual BlobSizeEstimatePtr getBlobSizeEstimate() { return mpBlobSizeEstimate; }

  /**
   * Get the Roi mask. It is always of the same size as the input mask.
   */
  virtual Image8Ptr getRoiMask() { return mpRoiMask; }

  // return ImageAcquireModule pointer
  ImageAcquireModPtr getImageAcquireModule();

  MotionDetectionModPtr getMotionDetectionModule();


  //
  // INQUIRY
  //

public:

  //
  // ATTRIBUTES
  //

protected:

  /**
   * Benchmark variable.
   */
//  Benchmark mBenchmark;

  /**
   * Region of interest for this execution with respect
   * to the original frame without rescaling.
   */
  Rectanglei mRegionOfInterest;

  /**
   * Mask of the region of interest built from the 
   * mTrackingRoiPolygon.
   */
  Image8Ptr mpRoiMask;  

  /**
   * Polygon for the tracking region of interest.
   */
  std::map<int,SimplePolygon> mRoiPolygon;

  /**
   * Blob size estimator.
   */
  BlobSizeEstimatePtr mpBlobSizeEstimate;

  /**
   * Scale factor for the input image.
   */
  Vector2f mRoiScaleFactor;

  /**
   * The background learner object.
   */
  BackgroundLearnerPtr mpBackgroundLearner;

  /**
   * Filtered foreground image. This image is filtered with
   * the blob size estimate.
   * @see filterByBlobSize().
   */
  Image8Ptr mpFilteredForeground;

  /**
   * The delta of the progress. It should add to one.
   */
  double mTotalProgressUnits, mCurrentProgressUnits;

  /**
   * Event Channel for Vms messages.
   */
//  VmsPacketPtr mpVmsPacket;

  /**
   * Print debug messages to console.
   */
  bool mVerbose;

  /**
   * Enable or disable the filtering by blob size.
   */
  bool mUseBlobSize;

  /**
   * If enabled, the foreground segment module will attempt to relearn the background when there is a large amount
   * of screen area that is statically detected as foreground for a long period of time.
   */
  bool mEnableRelearnBackground;

  /**
   * The matrix used to determine whether the background should be
   * relearned
   */
  Matrix<int> mRelearnBackground; 

  /**
   * How many frames ago was a check performed to determine
   * whether or not the background should be relearned.
   */

  int mLastRelearnCheck;

  /**
   * How often is the background checked (in seconds)
   */
  float mRelearnCheckInterval;

  /**
   * The amount of a time a pixel must remain foreground before being
   * considered to be inert by checkBackground()
   */
  float mRelearnInertTime;

  /**
   * The amount of pixels (specified in number of blobs) requried to trigger 
   * the relearning of the background. E.g. a value of 3 would mean that
   * given a video, the inert pixels must be able to form at least three
   * blobs to force the background to be relearned. The pixels are weighted
   * based on the estimated blob size of their location.
   */
  float mRelearnBlobThresh;

  /**
   * A flag indicating whether the background should be continously learned.
   * If false, the background will be learned once in the beginning, and
   * later only if foreground activity triggers it.
   *
   * If true, the background will be learned continously. This is necessary
   * for nonasynchronous image devices (i.e. live camera feed).
   */
  bool mLearnBackgroundContinuously;

  /**
   * A list of event channels processed by this module	
   */
  std::list<int> mEventChannels; 

  /**
   * The height of the image frame	
   */
  int mFrameHeight;

  /**
   * The width of the image frame	
   */
  int mFrameWidth;

  /**
   * Where to save the background model on exit. If nothing is specified, the
   * background will not be saved
   */
  std::string mBackgroundLoadLoc;

  /**
   * Where to load the background model from on startup. If nothing is
   * specified an initial background model will not be loaded.
   *
   * NOTE: the modelLearnTime and frameUpdateTime settings still apply
   * be careful not to replace the loaded background a few seconds after
   * the startup with a new one based on these settings. Also, the learned
   * background is dependent on the blob size.
   */
  std::string mBackgroundSaveLoc;

  /**
   * The video source id
   */
  int mVideoSourceId;

  /**
   * Output stream for saving raw segmentation data
   */
  std::ofstream mSegOutputFile; 

  /**
   * Folder to dump raw segmentation data to
   */
  std::string mSaveSegOutputFolder;

  /**
   * The next time segmentation data will be saved to a file
   */
  double mNextSaveSegTime;

  /**
   * The interval at which segmentation data is saved to a file
   */
  double mSaveSegInterval;

  /**
   * How much time each file containing segmentation data spans
   */
  double mSaveSegFileLengthInterval;

  /**
   * The next time the segmentation file will be broken up.
   */
  double mNextBreakFileTime;

  /**
   * If true, segmentation data will be saved to a file
   */
  bool mSaveSegmentation;

  /**
   * This flag indicates if the visualization is active for
   * this module in particular.
   */
  bool mPaintVisualization;

  
  /**
   *Is the motion hack enabled
   */
  bool mMotionHack;

  /**
   * The last 'safe' image that is sent to the background learner each time
   * when the motion hack is enabled.
   */
  Image8Ptr mpLastSafeImage;

  /**
   * Pointer to motion detection mod for the motion hack
   */
  MotionDetectionModPtr mpMotionMod;


  /**
   * Dilated version of the motion image
   */
  Image8 mDilatedMotion;

  /**
   * Matrix of the last time that there was motion in mDilatedMotion
   */
  Image<float> mMotionTime;

  /**
   * A pixel must have no motion for this amount of time to be sent to the background learner
   */
  double mMinNoMotionTime;

  double mLastMotionTime;
  bool mFirstPass;
  //END TEMP

  CircularBuffer<Image8Ptr> mLastFrames;
  bool mUseMotion;
  int mNumMotionFrames;


  //Check for autoexposure
  bool mCheckForExposure;
  double mLastExposureCheck;
  double mLastMeanIntensity;
  double mLastMeanSeg;
  std::string mExposureLogFilename; 


  //minimum time transform should be used, i.e. don't recalculate when there are successive frames with no motion
  //double mMinTransformTime;
  //double mLastTransformTime;

  float mMinMotionThreshold;
 

  /*
  bool mUseNormalizedG;
  Image8 mNormalizedG; 
  */
  
  //Image8 mCombinedSeg;
  
  // New members

};


}; // namespace vision

}; // namespace ait

#endif // ForegroundSegmentMod_HPP



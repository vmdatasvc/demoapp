/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef BlobSizeEstimate_HPP
#define BlobSizeEstimate_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>
#include <legacy/pv/ait/Vector3.hpp>
#include <legacy/pv/ait/Image.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

#include "legacy/vision_blob/BlobSizeEstimate_fwd.hpp"

namespace ait 
{


/**
 * This class is not yet documented.
 * Here is a more detailed description of the class. Next come the 
 * Responsibilities of the class. Try to minimize the responsibilities, this
 * will help in creating a good design. Next, optionally write any design patterns
 * that were used for this class. Refer to the "Design Patterns" book by Gamma et al.
 * @par Responsibilities:
 * - The First Responsibility
 * - The Second Responsibility:
 *   - A sub-responsibility
 *   - Another sub-responsibility
 * - The Third Responsibility
 * @par Patterns:
 * - The first design pattern
 * - The second design pattern
 * @par Design Considerations:
 * - Here you can itemize some justifications for your decisions. It will help
 * others to understand why your class is the way it is.
 * @remarks
 * Some special comments or warnings about this class.
 * @see TheirClass, OurClass (other related classes).
 */
class BlobSizeEstimate : private boost::noncopyable
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  BlobSizeEstimate();

  /**
   * Destructor
   */
  virtual ~BlobSizeEstimate();


  //
  // OPERATIONS
  //

public:

  // Initialize the algorithm parameters.
  // Here are the parameters that worked well:
  // blobSize.initializeParameters( 
  //     mSettings.getInt("numBlockRows",8),   --> number of blocks across the image height
  //     mSettings.getInt("numBlockColumns",8),  --> number of blocks across the image width
  //     mSettings.getDouble("sizeDistributionThreshold",0.75),   --> threshold of width feature dist'n
  //     mSettings.getDouble("heightFactor",2),    --> height estimate as a factor of width estimate
  //     mHeight, mWidth);     --> image dimensions

  void initializeParameters(int numBlockRows, int numBlockColumns, 
                            double distributionThreshold, double heightFactor,
                            int height, int width);

  // update distributions using this binary image
  void learnObjectSizeXY( const Image8 & img );

  // Compute size estimates using the current distributions.
  // If makeVisualization == 1, the function takes sampleFrame (could be the current frame)
  // and overlays the boxes that shows the size estimates.
  // The overlay result is stored in Image32 mVisualization.
  // Ideally, this function should be called only after the distribution is learned.
  void computeBlockEstimates();

  // writes the per-block size estimates
  void save(const std::string & filename);

  /**
   * Visualize the current blob sizes in the given image. This image must
   * have the original size.
   */
  void visualize(Image32& img);

  /**
   * Load the size estimates from a file. This function calculates
   * the interpolated sizes from the source files. Note that this call
   * and initializeParameters are mutually exclusive.
   */
  void load(const std::string & filename);

  inline void getEstimatedSize(float pixelX, float pixelY, float& sizeW, float& sizeH) const
  {
    const int x = std::min(std::max(round(pixelX),0),(int)mBlobWidthXY.width()-1);
    const int y = std::min(std::max(round(pixelY),0),(int)mBlobWidthXY.height()-1);
    sizeW = mBlobWidthXY(x,y);
    sizeH = mBlobHeightXY(x,y);
  }

  /**
   * Returns the blob width from the center of the image. This is used by algorithms
   * that assume an overhead view.
   */
  float getCenterWidth() const 
  {
    int x = mBlobWidthXY.width()/2;
    int y = mBlobWidthXY.height()/2;
    return mBlobWidthXY(x,y);
  }

  /**
   * Returns the estimated blob width at the given coordinate	
   */

  float getBlobWidth(const Vector2f& point) const
  {
    const int x = std::min(std::max(round(point(0)),0),(int)mBlobWidthXY.width()-1);
    const int y = std::min(std::max(round(point(1)),0),(int)mBlobWidthXY.height()-1);
    return mBlobWidthXY(x, y);
  }

  /**
   * This function resamples the internal image that has the interpolated
   * values. It will also rescale the values so they represent the new size.
   */
  void scale(const Vector2f& scaleFactor);

  /**
   * Crop the internal with and height images so it can match the destination ROI.
   */
  void crop(const Rectanglei& roi);

protected:
  /**
   * This function will compute the interpolated image based on the block estimates.
   */
  void computeInterpolatedImage();

  //
  // ACCESS
  //

public:

  //
  // INQUIRY
  //

  //
  // ATTRIBUTES
  //

protected:

  Image<double> mWidthHistogram;    // distribution of object width features
  Matrix<int> mBlobWidth;    // sizes per block
  Matrix<int> mBlobHeight;

  Image<float> mBlobWidthXY;   // interpolated sizes for each pixel
  Image<float> mBlobHeightXY;


  // parameters to set
  int mNumBlockRows;
  int mNumBlockColumns;
  double mDistributionThreshold;
  double mHeightFactor;   // multiplied to the width estimate to get the height
  int mHeight;   // frame dimensions
  int mWidth;

  //Vector2f mSizeEstimate;

public:

};


}; // namespace ait

#endif // BlobSizeEstimate_HPP


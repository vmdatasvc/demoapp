/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef MultiFrameTrackShape_HPP
#define MultiFrameTrackShape_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//


#include "legacy/vision_blob/MultiFrameTrackShape_fwd.hpp"

#include <legacy/vision_blob/PersonShapeEstimator.hpp>
#include <legacy/math/Condensation.hpp>
#include <legacy/vision_blob/Trajectory.hpp>

namespace ait 
{

namespace vision
{

typedef std::list<MultiFrameTrackShapePtr> TracksShape;
typedef boost::shared_ptr<TracksShape> TracksShapePtr;


class MultiFrameTrackShape
{
  //
  // LIFETIME
  //

public:

  /**
   * The sequence of points that form a snake.
   */
  typedef std::vector<Vector2f> Snake;

  /**
   * Constructor.
   */
  MultiFrameTrackShape();

  /**
   * Destructor
   */
  virtual ~MultiFrameTrackShape();

  /**
   * Initialization.
   */
  virtual void init(int id,const std::string& settingsPath,
    const Vector2i& initPos,
    double startTime,
    double fps,
    PersonShapeEstimatorPtr pPersonShapeEstimator,
    Vector2i personShapeOffset);

  /**
   * Update the algorithm. Provide a pointer to the current foreground
   * segmentation. Note that this image must not be modified after it has
   * been passed to this class, because it will be used in future
   * iterations. This image should be already filtered by blob size.
   */
  virtual void update(Image8Ptr pForeground, double currentTime);

  /**
   * Prediction step;
   */
  virtual void predict();  

  /**
   * Optimization function.
   */
  virtual float evaluateOptimizationFunction(float* pValues, int numParticle, int numPhase);
  
  /**
   * 
   */
  virtual float evaluateForegroundMatching(float* pValues);

  
  /**
   * Visualize.
   */
  virtual void visualize(Image32& img);

  /**
   * Kill this track. Mark this track as dead, this means that the application can
   * disable this track.
   */
  virtual void kill();
  
  /**
   * Get the size of a snake node by weighting the size of all particles.
   */
  //temporarily disable because it is not being used
  //virtual Vector2f getSnakeNodeBlobSize(int idx);

  /**
   * Set the trajectory
   */
  virtual void setTrajectory(TrajectoryPtr pTraj) {mpTrajectory = pTraj;}

  /**
   * Set the force vector for perceived motion
   */
  virtual void setForceVector(const Vector2f& vec) {mForceVector = vec;}

  /**
   * Set the direction vector (indicates which direction the track should continue to travel)
   */
  virtual void setDirectionVector(const Vector2f& vec) {mDirectionVector = vec;}

  

protected:

  PersonShapeEstimatorPtr mpPersonShapeEstimator;
  Vector2i mPersonShapeOffset;

private:
  /**
   * Stub for the condensation optimization evaluation.
   */
  static float evalFunctionStub(float *pValues, int numParticle, int numPhase, void *pObject)
  {
    return (static_cast<MultiFrameTrackShape *>(pObject))->evaluateOptimizationFunction(pValues, numParticle, numPhase);
  }

  /**
   * Debug functions.
   */
  virtual void saveParticleImage(float *pValues,float weight);
  virtual void saveParticleTrajectoryImage(float *pValues,float weight);

public:

  /**
   * Return the current snake for visualization. The oldest frame is in the begining of
   * the snake. The head of the snake is the last node.
   */
  virtual Snake& getCurrentSnake() { return mSnake; }  

  /**
   * Return the position of the head of the snake.
   */
  virtual Vector2f getPos() { return mSnake.empty() ? Vector2f() : mSnake.back(); }

  /**
   * Return the size for the blob at the current location. This is
   * the blob size for the current position as returned by getPos().
   */
  virtual Vector2f getSize();

  /**
   * Return the age of this track == number of times update has been called.
   */
  virtual int getAge() { return mAge; }

  /**
   * Unique id for this track.
   */
  virtual int getId() { return mId; }

  /**
   * Get the trajectory.
   */
  virtual TrajectoryPtr getTrajectory() { return mpTrajectory; }

  /**
   * Get force vector
   */
  virtual Vector2f getForceVector() {return mForceVector;}

  /**
   * Get direction vector
   */
  virtual Vector2f getDirectionVector() {return mDirectionVector;}  

   /**
   * Ask if this object has been initialized (the init() function has been called).
   */
  virtual bool isInitialized() const { return mIsInitialized; }

  /**
   * Check if this track is alive. Tracks are killed by calling kill().
   */
  virtual bool isAlive() const { return mIsAlive; }

protected:

  /**
   * Condensation class with custom prediction function.
   */
  class ThisCondensation : public Condensation<float>
  {
  public:
    void init(int numParticles, int dimension, MultiFrameTrackShape *pMFT);
  protected:
    virtual void predict(); 
    MultiFrameTrackShape *mpMultiFrameTrack;
  };

   /**
   * Tells if this object has been initialized.
   */
  bool mIsInitialized;

  /**
   * Reference to the foreground segmented circular
   * buffer.
   */
  ForegroundHistoryPtr mpForegroundHistory;

  /**
   * Condensation object.
   */
  ThisCondensation mCondensation;  

  /**
   * Current snake.
   */
  Snake mSnake;

  /**
   * This variable indicates if the circular
   * buffer has been filled from the previous iteration.
   */
  bool mCircularBufferFilled;

  /**
   * This variable indicates if this track is alive.
   */
  bool mIsAlive;

  /**
   * The age of the track based on the number of times update
   * has been called.
   */
  int mAge;

  /**
   * Id for this track.
   */
  int mId;

  /**
   * Full track trajectory.
   */
  TrajectoryPtr mpTrajectory;

  /**
   * Start time. Typically in frames.
   */
  //double mStartTime;

  /**
   * The blob speed per frame with respect to the with of
   * the blob. This means that if the speed is 0.5, the blob
   * will be assumed to move at most half of the width in
   * each x,y direction.
   */
  float mMaxBlobSpeedPerFrame;  
  
  /**
   * Repulsion vector. A vector indicating the preferred position for the
   * for the track to travel with respect to other nodes.
   */
  Vector2f mForceVector; 

  /**
   * A vector indicating the tracks predicted motion based on its past motion.
   */
  Vector2f mDirectionVector;

  /**
   * Variable that holds the threshold limit for keeping tracks alive
   */
  int mTrackKillThreshold;

};


}; // namespace vision

}; // namespace ait

#endif // MultiFrameTrack_HPP


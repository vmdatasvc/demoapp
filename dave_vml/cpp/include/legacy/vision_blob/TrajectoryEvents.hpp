/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef TrajectoryEvents_HPP
#define TrajectoryEvents_HPP

// SYSTEM INCLUDES
//
#include <vector>

#include <boost/utility.hpp>

// AIT INCLUDES
//

#include <legacy/math/Polygon.hpp>
#include <legacy/math/LineSegment.hpp>

#include "legacy/vision_blob/BlobSizeEstimate.hpp"

//#include <String.hpp>

//fix this later
#include "legacy/vision_blob/Trajectory.hpp"

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

#include "legacy/vision_blob/TrajectoryEvents_fwd.hpp"


namespace ait 
{

namespace vision
{
  /**
   * This structure contains information regarding when a trajectory 
   * enters/leaves a polygon or intersects a single segment. The enter
   * and exit sides are the numbered edges of a polygon. Edge 0 is
   * simply the segment formed by the first vertex and the second,
   * edge 1 is the segment formed by the second vertex and the third, etc.
   *
   * When given two points, a segment is created internally instead of a 
   * polygon. When entering from the left side, enterSide is 0 and 
   * exitSide is 1, and vice-versa for the right side. If you were
   * standing on the first point, and facing the second point, the
   * 'left' side would be identical to your left.
   *  
   * If the polygon starts inside the polygon, or ends inside the polygon,
   * the enterSide or exitSide will be -1 respectively.
   */
struct PolygonEvent
{
  Trajectory trajectory;
  double beginFrame;
  double endFrame;
  int enterSide;
  int exitSide;
  
};

/**
 * This class creates trajectory events from a trajectory and a series of 
 * points which form either a line segment or a polygon. Each trajectory
 * can generate one or more events. See PolygonEvent.
 *
 * @par Responsibilities:
 * - Generate events from trajectories and polygons/line segments
 * @par Design Considerations:
 * - This class relies on LineSegment and Polygon to do most of the work.
 * @remarks
 * Some special comments or warnings about this class.
 * 
 */
class TrajectoryEvents : private boost::noncopyable
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  TrajectoryEvents();

  /**
   * Destructor
   */
  virtual ~TrajectoryEvents();

  /**
   * Initialization. Sending two points will cause events
   * that cross the line formed by those points to be detected.
   * Sending more points will allow events to be extracted from
   * the given polygon.
   */
  void init(BlobSizeEstimatePtr pBlobEst);


  //
  // OPERATIONS
  //

public:

  /**
   * Extract events from the given trajectory, based on the geometric
   * objects described by the points given to the init() member function.   
   */
  
  void extractEvents(
    const Trajectory& trajectory, 
    const std::vector<Vector2f>& eventPoly,
    std::list<PolygonEventPtr>& events);

  /**
   * Sets the threshold for how decisively a line must be crossed for an
   * event to be generated.
   */
  void setLineThreshold(float thresh) {mLineThreshold = thresh;}

  /**
   * Sets the threshold for how decisively a polygon must be entered/exited
   * for an event to be generated.
   */
  void setPolyThreshold(float thresh) {mPolyThreshold = thresh;}

  /**
   * Set the amount of time to be considered when determining cardinality.	
   */
  void setCardTimeThreshold(float thresh) {mCardTimeThreshold = thresh;}

  /**
   * Set the maximum number of events that will be generated per trajectory.
   * If zero is specified, there will be no maximum.
   */
  void setMaxEventsPerTrajectory(int maxEvs) { mMaxEventsPerTrajectory = maxEvs; }

protected:

  float mLineThreshold;
  float mPolyThreshold;
  float mCardTimeThreshold;
  int mMaxEventsPerTrajectory;

  BlobSizeEstimatePtr mpBlobSizeEstimate; 

  void createEvent(
    std::list<PolygonEventPtr>& events,
    int enterSide, 
    int exitSide, 
    double beginFrame,
    double endFrame, 
    const Trajectory& traj);

  void extractEventsLine(
    const Trajectory& trajectory,     
    std::list<PolygonEventPtr>& events);

  void extractEventsPolygon(
    const Trajectory& trajectory,     
    std::list<PolygonEventPtr>& events);



  //
  // ACCESS
  //

public:

  
  //
  // INQUIRY
  //

public:

  /**
   * Ask if this object has been initialized (the init() function has been called).
   */
  bool isInitialized() const { return mIsInitialized; }


  //
  // ATTRIBUTES
  //

protected:

  /**
   * Tells if this object has been initialized.
   */
  bool mIsInitialized;

  /**
   * The Polygon object
   */
  Polygon mPoly;

  /**
   * The LineSegment object
   */
  LineSegment mLineSeg;

  /**
   * Are we dealing with a line or a polygon
   */
  bool mIsLine;
};

}; // namespace vision
}; // namespace ait

#endif // TrajectoryEvents_HPP


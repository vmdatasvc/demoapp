/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef BlobSizeEstimate_fwd_HPP
#define BlobSizeEstimate_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class BlobSizeEstimate;
typedef boost::shared_ptr<BlobSizeEstimate> BlobSizeEstimatePtr;

}; // namespace ait

#endif // BlobSizeEstimate_fwd_HPP


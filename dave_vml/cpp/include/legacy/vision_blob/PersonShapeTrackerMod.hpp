/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef PersonShapeTrackerMod_HPP
#define PersonShapeTrackerMod_HPP

// SYSTEM INCLUDES
//

// AIT INCLUDES
//


// LOCAL INCLUDES
//
#include <legacy/vision_blob/ForegroundSegmentModShape.hpp>
//#include <ForegroundSegmentModEdge.hpp>
#include <legacy/vision_tools/BackgroundLearner.hpp>

#include <legacy/vision_blob/PersonShapeEstimator.hpp>


// FORWARD REFERENCES
//

#include <legacy/vision_blob/MultiFrameTrackShapePS.hpp>
#include <legacy/vision_tools/PvConCompLab.hpp>

#include "legacy/vision_blob/TrajectoryEvents.hpp"
//#include "BlobTrackerMsgShape.hpp"

#include "legacy/vision_blob/CartDetector.hpp"

namespace ait 
{

namespace vision
{

/**
 * This vision module is not yet documented.
 * @par Responsibilities:
 * - Write the responsibilities here.
 */
  class PersonShapeTrackerMod : public RealTimeMod
{

public:

  /**
   * Default Constructor. The constructor creates a quick instance. Expensive operations
   * are executed in the init() function.
   */
  PersonShapeTrackerMod();

  /**
   * Constructor. The constructor creates a quick instance. Expensive operations
   * are executed in the init() function.
   */
  PersonShapeTrackerMod(const std::string& moduleName);

  /**
   * Destructor
   */
  virtual ~PersonShapeTrackerMod();

  /**
   * Provide dependency information for the ExecutionGraph.
   * @see VisionMod::initExecutionGraph for more info.
   */
  virtual void initExecutionGraph(std::list<VisionModPtr>& srcModules);

  /**
   * Initialization function.
   */
  virtual void init();

  /**
   * Finalization function
   */
  virtual void finish();

  /**
   * Process this module.
   */
  virtual void process();

  /**
   * Visualize this module.
   */
  virtual void visualize(Image32& img);

  /**
   * Process the incoming vision messages.
   */
  virtual void processMessages();

  /**
   * Returns a list of the active tracks
   */
  virtual TracksShapePS getActiveTracks() {return mTracks;}


protected:

  /**
   * Process incoming message.
   */
  virtual void processMsg(BlobTrackerMsgShape* pMsg);

  /**
   * Look for new tracks in the image. The method is to scan the filtered
   * by blob size image, remove the foreground signals from current tracks
   * and look for other significant foreground regions that might be tracked
   * by other tracks.
   */
  virtual void scanForNewTracks();

  /**
   * Check if two or more tracks overlap.
   */
  virtual void checkOverlappingTracks();

  /**
   * Send a Vms event based on a polygon event.
   */
  //virtual void sendVmsEvent(PolygonEventPtr pEvent, int eventChannel);

  /**
   * Send a Vms event based on a trajectory.
   */
  virtual void sendVmsEvent(TrajectoryPtr pTrajectory, int eventChannel);

  /**
   * Perform track post-processing.
   * @param force [in] Process every track regardless of
   * the delay. This is used when the video is finished.
   */
  virtual void postProcessTracks(bool force);

  /**
   * Generate the polygon events based on a list of trajectories.
   */
  virtual void generateEvents(const std::list<TrajectoryPtr>& trajectories);

  /**
   * Kill all tracks inside the kill track polygons
   */
  virtual void killTracksInPoly();

  /**
   * Convert a trajectory from the scaled down segmentation coordinate space
   * to the full image coordinate space.
   */
  virtual void convertToImageCoord(TrajectoryPtr pTraj);

  //functions dealing with blob repulsion
  virtual Vector2f pointRepulsion(const Vector2f& p1, const Vector2f& p2);

  virtual Vector2f calculateTrackRepulsion(MultiFrameTrackShapePSPtr pTrack);

  virtual void calculateTrackForce(); 

  /**
   * Function to evaluate the Euclidean distance between two points
   */
  virtual float euclideanDist(Vector2f& temp1, Vector2f& temp2);

  /**
   * Function used for scaling of the person shape filtered image
   */
  virtual void scaleImage(Image8& inputImage, float max, float min);

  /**
   * Function used to carry out an ordered filtering of the raw segmentation
   */
  virtual void filterSegmentation(Image8& image,int order);

  /**
   * Function to create a new track
   */
  virtual void createNewTracks(int x, int y);

  /**
   * Function to check if two person shapes overlap or not
   */
  bool checkOverlappingPersonShapes(Vector2f& pos1, Vector2f pos2);

  /**
   * Function to find the real world distance between two image points
   */
  float getRealWorldDistance(Vector2i& pos1, Vector2i& pos2);

  /**
   * Scan for specific color, wrap in convex hall, and remove
   * from original segmentation.
   */
  virtual void detectCarts();

  /**
   * Set all pixels detected as carts to 0
   */
  virtual void removeCarts(Image8& img, Image8& cartImg);


  /**
   * Create 4 visualizations useful for debugging cart detection.   
   */
  virtual void removeCartsDebugVisualization(Image32& img) const;

  /**
   * Draw the detected cart polygons at the proper image scale
   */
  virtual void drawCartsOutline(Image32& img, unsigned int color) const;



  virtual void detectEdges(const Image8& img, Image8& edgeImg, 
                           unsigned int thresh) const;


  virtual void makeAdjustedEdgeImage(const Image8& edgeBackgroundImg,
                                     const Image8& edgeCurrentFrame,
                                     const Image8& foregroundSegImg,
                                     Image8& outputImg) const;

  virtual void makeAdjustedForegroundSegmentationEdgeImage(const Image8& dilatedForegroundSegmentationEdgeImg,
                                     const Image8& adjustedEdgeImg,
                                     Image8& outputImg) const;

  virtual void combineEdgesAndSeg(const Image8& filteredEdgeImg,
                                  const Image8& filteredSegImg, 
                                  Image8& outputImg,
                                  float edgeWeight) const;

  virtual void makeSpiralLookup(unsigned int width, unsigned int height, 
                                Image<unsigned int>& table) const;

  virtual bool isMaxEightNeighbor(int x, int y, Image8& img) const;

  virtual void overlayPersonMasks(Image32& img) const;

  virtual bool isNearCart(int x, int y, Image8& img);

  virtual void trackCarts();

  virtual void getCartPositions();


  /**
   * Alternate processing path that counts raw segmentation instead of doing tracking.
   * Used in grupo salinas for queue.
   */
  virtual void measureRawSegmentationHack();

  virtual void personTrackAssignment(Image8& filterImg);

  virtual void updateTracks();

  virtual void createNewTracks();

  virtual void deleteDeadTracks();

  virtual void killCartTracksOutsidePoly();

  virtual void createTracksForCarts();

  virtual void deleteDeadCartTracks();

  virtual void createNewCartTracks(int x, int y, Polygon& poly);

  virtual void updateCartTracks();

  virtual void filterSegmentationWholeImg(Image8& image, int order);

  virtual void getOccludingCartRectangles();
  
  virtual void getMaxValueOccludingRegion(Image8& filterImg);

  virtual void getOccludingCartRectanglesSloped();

  virtual void getBelowCartRegions();

  virtual bool isBelowCart(int x, int y);

  virtual bool isInOccludedRegion(int x, int y);

  virtual void personTrackAssignmentPredicted(Image8& filterImg);

  virtual void drawBisector(Image8& img, Vector2f pos1, Vector2f pos2);

  virtual void removeBelowCartRegions(Image8& img);




  
  
public:

  /**
   * Get the static name of this module. This name is used to name the type of this module.
   * The constructor sets the member variable mName to this value.
   * @remark This static function is only implemented for concrete classes.
   */
  static const char *getModuleStaticName() { return "PersonShapeTrackerMod"; }  


protected:

  /**
   * Benchmark variable.
   */
  Benchmark mBenchmark;

  /**
   * Reference to the foreground segmenter module.
   */
  ForegroundSegmentModShapePtr mpForegroundSegmentMod;

  /**
   * Upper bound time limit for post processing. If exceeded, post processing
   * occurs immediately.
   */
  double mPostProcessUpperInterval;

  /**
   * Lower bound time limit for post processing. When exceeded, post processing
   * will occur only if there are no active tracks
   */
  double mPostProcessLowerInterval;

  /**
   * The next time that post processing will be forced to occur if it does not
   * happen earlier. This is regardless of active tracks.
   */
  double mNextForcePostProcessTime;

  /**
   * The next time that post processing will occur, provided there are no active
   * tracks.
   */
  double mNextPostProcessTime;

  /**
   * The list of current active tracks.
   */
  TracksShapePS mTracks;  

  /**
   * Flag that indicates if this is a top view camera.
   * This is used to decide if the trajectories should
   * be based on the center of the blob (head for top view
   * cameras) or the bottom (feet for side cameras).
   */
  bool mIsTopViewCamera;

  /**
   * This image contains foreground responses that
   * do not belong to any tracks.
   */
  Image8Ptr mpNewTrackScanImage;

  /**
   * Maximum nodes for each snake.
   */
  int mMaxSnakeSize;

  /**
   * Print debug messages to console.
   */
  bool mVerbose;

  /**
   * Counter to generate new ids for tracks.
   */
  int mNewTrackId;

  /**
   * Maximum number of concurrent tracks.
   */
  int mMaxConcurrentTracks;

  /**
   * All trajectories ready for post-processing.
   */
  std::list<TrajectoryPtr> mPreTrajectories;

  /**
   * The minimum distance at which a new track can be created with
   * respect to existing tracks.
   */
  float mMinTrackCreationDistance;

  /**
   * Circular buffer with the last frames. That way we can
   * visualize the tail of the snake.
   * @remark Using this is very expensive! Only for visualization.
   */
  CircularBuffer<Image32Ptr> mLastColorFrames;

  /**
   * Visualize Snake Tail. Very expensive!!!
   */
  bool mVisualizeSnakeTail;

  /**
   * Event Channel for Vms messages.
   */
  VmsPacketPtr mpVmsPacket;

  /**
   * Polygon that denotes the boundaries to detect
   * events.
   */
  std::map<int,SimplePolygon> mEventsPolygon;

  /**
   * Flag that indicates whether the trajectories should be saved
   * to disk. Trajectories will be dumped into timestamped .csv files
   * in the trajectory output folder.
   */
  bool mSavePreTrajectories;

  /**
   * Folder to dump saved trajectories to.
   */
  std::string mTrajectoryOutputFolder; 

  /**
   * Flag to optionally send extended attributes, which include the
   * trajectory for each event.
   */
  bool mSendExtendedAttributes;

  /**
   * Flag to optionally send extended event data
   */
  bool mSendExtendedEventData;

  /**
   * Indicate if the events to be generated are from Pre-processed
   * trajectories.
   */
  bool mSendPreprocessTrajectories;
  
  /**
   * The TrackManager object which handles postprocessing.	
   */
  //TrackManager mTrackManager;

  /**
   * Handles generating events from given trajectories.	
   */
  //TrajectoryEvents mTrajEvents;

  /**
   * A list of event channels this module processes	
   */
  std::list<int> mEventChannels; 

  /**
   * The width of each frame
   */
  int mFrameWidth;

  /**
   * The height of each frame
   */
  int mFrameHeight;

  /**
   * A frame from the original video source
   */
  Image32 mOriginalVideo;  

  /**
   * The video source id
   */
  int mVideoSourceId;

  /**
   * Enable the motion module for additional track filtering
   */
  bool mIsMotionEnabled;

  /**
   * Points to the motion detection module.
   */
  //MotionDetectionModPtr mpMotionMod;

  /**
   * Maximum time with no motion that tracks will be created and be allowed to live
   */
  //double mMaxNoMotionTime;

  /**
   * Write Events in CSV file. This is a simple way to generate events
   * and save them on the local machine without the need of a VMS server.
   * The events will be saved in the trajectoryOutputFolder as defined
   * in the settings. 
   */
  bool mSaveCsvFileEvents;

  /**
   * Send the trajectories as the event attribute a4.
   */
  bool mSendTrajectoriesAsEventAttribute;

  /**
   * This flag indicates if the visualization is active for
   * this module in particular.
   */
  bool mPaintVisualization;

  /**
   * New tracks will onlyh be created inside of this region.
   */
  std::list<Polygon> mStartTrackPolyList;

  /**
   * A mask generated from mStartTrackPoly, scaled to the size of the 
   * foreground segmentation.
   */
  Image8 mStartTrackMask;

  /**
   * All active tracks are immediate killed in this region
   */
  std::list<Polygon> mKillTrackPolyList;

  /**
   * A mask generated from mKillTrackPoly, scaled to the size  of the
   * foreground segmenation.
   */
  Image8 mKillTrackMask;

  bool mRepelTracks;  
  
  PersonShapeEstimatorPtr mpPersonShapeEstimator;

  Image8Ptr mpFilteredForeground;

  /**
   *A vector containing the positions of all the detected person positions
   */
  std::vector<Vector3f> mPersonPositions;

  /**
   * This vector contains the thresholds that will trigger the creation of a
   * new track. The entire scene is searched at each threshold, starting from
   * the first. If the filtered foreground image is >= this threshold, a new
   * track will be created.
   */
  std::list<int> mThresholdList;  

  /**
   * A flag to be used if the raw segmentation needs to be used to derive some statistics
   */
  bool mMeasureRawSegmentation;

  /**
   * The rate at which the raw segmentation is sampled while using raw segmentation for statistics
   */
  float mMeasureRawSegRate;

  /**
   * The place where the file with the raw segmentation statistics is saved
   */
  std::string mMeasureRawSegFilePath;

  /**
   * The mask image where the raw segmentation statistics will be carried out
   */
  std::vector<Image8> mMeasureSegMask;

  /**
   * A variable to hold the current frame time
   */
  double mCurrentFrameTime;

  /**
   * A vector used to store the scaling factors used for each of the raw segmentation statistics polygons
   */
  std::vector<int> mSegmentationScaling;

  /**
   * The variable used to save the real world coordinates of the image plane as obtained from the person shape estimator
   */
  Image<Vector2f> realWorldCoords;

  /**
   * The variable for the minimum distance allowed between two detected persons
   */
  float mPersonShapeDistanceThresh; 

  /**
   * The variable for maximum distance between a detected person and a track for assignment to take place
   */
  float mPersonTrackMaxDistance;

  /**
   * The variable for maximum distance between a detected person and a track for assignment to take place
   */
  int mPersonLowerThreshold;

  /**
   * The variable for maximum time a track is kept alive without any supporting person shape detection
   */
  int noUseTrackPeriod;

  /**
   * The flag used to decide if the segmentation is ordered filtered or not
   */
  bool mFilterSegmentationFlag;

  /**
   * The order of the ordered filter used for filtering the segmentation
   */
  int mFilterSegmentationOrder;  

  /**
   * The image used to save the region in which the person detection takes place
   */
  Image8 mPersonTrackMask;

  /**
   * The region for good tracks
   */
  Image8 mGoodTrackRegionMask;

  /**
   * All person detections take place in this region only
   */
  std::list<Polygon> mPersonTrackRegionList;


  enum drawOptions {ALL, ALIVE, NONE};
  /**
   * Determine how tracks are drawn when visualized
   */  
  drawOptions mDrawTracksMode;

  /**
   * This variable holds a copy of the raw segmentation provided
   * by the ForegroundSegmentMod.
   */
  Image8Ptr mpRawSegmentationImage;

  //Edge based segmentation
  //ForegroundSegmentModEdgePtr mpEdgeMod;

  //Edge based background learner
  BackgroundLearnerPtr mpEdgeBackgroundLearner;
  
  /**
   * Threshold used to generate edges via sobel operator
   */
  unsigned int mEdgeDetectThreshold;


  Image8Ptr mpEdgeImage;
  Image8Ptr mpEdgeLearnedImage;
  Image8Ptr mpFilteredEdgeImage;

  Image8Ptr mpCombinedEdgeAndSegImage;

  //Edges removed depending on edge/learned edge/background combinations
  Image8Ptr mpAdjustedEdgeImage;

  //Color segmentation and cart detection variables below
  Image8Ptr mpColorSegmentedImage;
  Image8Ptr mpColorAndForegroundImage;
  Image8Ptr mpFilteredComponentImage;

  // Foreground Edge based Cart Detection Variables
  Image8Ptr mpDilatedForegroundSegmentationImage;
  Image8Ptr mpDilatedForegroundSegmentationEdgeImage;



  /**
   * The color used for cart detection and removal (color segmentation).
   * RGB color space.
   */
  unsigned int mSelectedColor;
  unsigned int mSelectedColor2;

  /*   
   * max distance to hue for cart classification. Because the domain wraps
   * around 360 degrees, so does this value. Range [0-360). HSV color space
   */
  unsigned int mSelectedColorDistH;

  /* max distance for saturation for cart classification. Acceptable
   * values range from [0,1]. HSV color space
   */
  unsigned int mSelectedColorDistS;

  unsigned int mSelectedColorDistVAndS;
  unsigned int mSelectedColorDistV;
  

  PvCompsT<PvComp> mConnectedComp;

  std::vector<Polygon> mCartPolygons;

  Image<unsigned int> mSpiralScanLookup;

  //Spiral scanning order table

  //Added by Rahul
  float mEdgeWeight;

  int mRegSegPersonDetectionLowerThresh;

  int mEdgeSegPersonDetectionLowerThresh;
  //Done

  //Structure used for person detection
  struct personStructure{
    int personProbability;
    Vector2f personPosition;
    bool isAssigned;
  };

  //Vector used to hold the person positions
  std::vector<personStructure> mvPersonPositions, mvPersonPositionsLowThresh;

  std::vector<personStructure> mvOccludedPersonPositions;


  //Structure for person track assignment
  struct tableAssignment{
    int trackNum;
    int personNum;
    bool HPP;
    float dist;
    bool occluded;
  };

  float mMaxPersonTrackDistance;

  virtual void rowSort(std::vector<tableAssignment>& row);

  /**
  * Connected components of cart colors must be > the below
  * size or else they will be ignored and will not be
  * removed from the segmentation.
  */
  unsigned int mMinCartComponentSize;
  unsigned int mMaxCartComponentSize;

  bool mDebug;

  std::ofstream mDebugFile;

  int mFrameNum;

  TracksShapePS mCartTracks; 

  std::vector<personStructure> mvCartPositions;

   float mMaxCartTrackDistance;

   std::vector<Rectanglei> mOccludingCartRectangles;

   std::vector<int> maxOccludingValues;

   int mOccludedPersonThresh;

   std::list<int>  mOcclusionRegions;

   std::vector<Rectanglei> mBelowCartRegions;

   std::vector<Rectanglei> mNoCartRegions;

   bool mDetectCarts;

   bool mDetectCartsUsingEdges;

   bool mUseEdgePersonDetection;

   CartDetector mCartDetector;

   Image32 mCartScoreImage;
   Image32 mCartOutImage;

   Image8 mTestEdgeImage;
   Image8 mTestAdjustedEdgeImage;
 

};


}; // namespace vision

}; // namespace ait

#endif // PersonShapeTrackerMod_HPP




#pragma once

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{

class CartEdgeModel;
typedef boost::shared_ptr<CartEdgeModel> CartEdgeModelPtr;

}; // namespace vision

}; // namespace ait


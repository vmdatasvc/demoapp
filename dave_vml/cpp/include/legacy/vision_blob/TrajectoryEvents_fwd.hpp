/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef TrajectoryEvents_fwd_HPP
#define TrajectoryEvents_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

namespace vision
{


class TrajectoryEvents;
typedef boost::shared_ptr<TrajectoryEvents> TrajectoryEventsPtr;
struct PolygonEvent;
typedef boost::shared_ptr<PolygonEvent> PolygonEventPtr;

}; // namespace vision
}; // namespace ait

#endif // TrajectoryEvents_fwd_HPP


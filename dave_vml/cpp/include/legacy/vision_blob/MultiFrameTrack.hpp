/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef MultiFrameTrack_HPP
#define MultiFrameTrack_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

#include <legacy/math/Condensation.hpp>
#include <legacy/pv/ait/Vector3.hpp>

// LOCAL INCLUDES
//

#include "legacy/vision_blob/Trajectory.hpp"

// FORWARD REFERENCES
//

#include "legacy/vision_blob/BlobSizeEstimate_fwd.hpp"
#include "legacy/vision_blob/MultiFrameTrack_fwd.hpp"

namespace ait 
{

namespace vision
{

typedef std::list<MultiFrameTrackPtr> Tracks;
typedef boost::shared_ptr<Tracks> TracksPtr;

/**
 * This class is not yet documented.
 * Here is a more detailed description of the class. Next come the 
 * Responsibilities of the class. Try to minimize the responsibilities, this
 * will help in creating a good design. Next, optionally write any design patterns
 * that were used for this class. Refer to the "Design Patterns" book by Gamma et al.
 * @par Responsibilities:
 * - The First Responsibility
 * - The Second Responsibility:
 *   - A sub-responsibility
 *   - Another sub-responsibility
 * - The Third Responsibility
 * @par Patterns:
 * - The first design pattern
 * - The second design pattern
 * @par Design Considerations:
 * - Here you can itemize some justifications for your decisions. It will help
 * others to understand why your class is the way it is.
 * @remarks
 * Some special comments or warnings about this class.
 * @see TheirClass, OurClass (other related classes).
 */
class MultiFrameTrack : private boost::noncopyable
{
  //
  // LIFETIME
  //

public:

  /**
   * The sequence of points that form a snake.
   */
  typedef std::vector<Vector2f> Snake;

  /**
   * Constructor.
   */
  MultiFrameTrack();

  /**
   * Destructor
   */
  virtual ~MultiFrameTrack();

  /**
   * Initialization.
   */
  virtual void init(int id,
    const std::string& settingsPath, 
    const Vector2i& initPos,
    BlobSizeEstimatePtr pBSE,
    double startTime,
    double fps,
    bool useAlternateTracker = false);

  //
  // OPERATIONS
  //

public:

  /**
   * Update the algorithm. Provide a pointer to the current foreground
   * segmentation. Note that this image must not be modified after it has
   * been passed to this class, because it will be used in future
   * iterations. This image should be already filtered by blob size.
   */
  virtual void update(Image8Ptr pForeground, double currentTime);

  /**
   * Prediction step;
   */
  virtual void predict();

  /**
   * Optimization function.
   */
  virtual float evaluateOptimizationFunction(float* pValues, int numParticle, int numPhase);

  /**
   * 
   */
  virtual float evaluatePathCoherence(float* pValues);

  /**
   * 
   */
  virtual float evaluateForegroundMatching(float* pValues);

  /**
   * Evaluate the overlap between other tracks. The range of
   * possible values returned is [0,1]. A value of zero
   * means that no other tracks overlap. A value of one
   * means that the track is completely overlapped.
   */
  //float evaluatePathOverlap(float *pValues);
  
  /**
   * Clean the foreground image to avoid creating new tracks in the location
   * where this track is operating.
   */
  virtual void cleanForegroundSegmentation(Image8& img);

  /**
   * Visualize.
   */
  virtual void visualize(Image32& img);

  /**
   * Kill this track. Mark this track as dead, this means that the application can
   * disable this track.
   */
  virtual void kill();

  /**
   * Get the size of a snake node by weighting the size of all particles.
   */
  virtual Vector2f getSnakeNodeBlobSize(int idx);

  /**
   * Set the trajectory
   */
  virtual void setTrajectory(TrajectoryPtr pTraj) {mpTrajectory = pTraj;}

  /**
   * Enabled the usage of motion information.
   */
  virtual void useMotionHistory(boost::shared_ptr<const Image<double> > pMotionData, double maxNoMotionTime) 
  {
    mIsMotionEnabled = true;
    mpLastMotion =  pMotionData;
    mMaxNoMotionTime = maxNoMotionTime;
  }

  /**
   * Set the force vector for perceived motion
   */
  virtual void setForceVector(const Vector2f& vec) {mForceVector = vec;}

  /**
   * Set the direction vector (indicates which direction the track should continue to travel)
   */
  virtual void setDirectionVector(const Vector2f& vec) {mDirectionVector = vec;}

protected:
  

  /**
   * Get the cardinality of the node given node using the given segmented
   * image.
   */
  virtual void calculateNodeCardinality(Trajectory::Node& node, const Image8& filteredForeground);

private:

  /**
   * Stub for the condensation optimization evaluation.
   */
  static float evalFunctionStub(float *pValues, int numParticle, int numPhase, void *pObject)
  {
    return (static_cast<MultiFrameTrack *>(pObject))->evaluateOptimizationFunction(pValues, numParticle, numPhase);
  }

  /**
   * Debug functions.
   */
  virtual void saveParticleImage(float *pValues,float weight);
  virtual void saveParticleTrajectoryImage(float *pValues,float weight);

  //
  // ACCESS
  //

public:

  /**
   * Return the current snake for visualization. The oldest frame is in the beginning of
   * the snake. The head of the snake is the last node.
   */
  virtual Snake& getCurrentSnake() { return mSnake; }  

  /**
   * Return the position of the head of the snake.
   */
  virtual Vector2f getPos() { return mSnake.empty() ? Vector2f() : mSnake.back(); }

  /**
   * Return the position of the head of the snake.
   */
  Vector2i getImagePos(int imageWidth, int imageHeight);

  /**
   * Return the size for the blob at the current location. This is
   * the blob size for the current position as returned by getPos().
   */
  virtual Vector2f getSize();

  /**
   * Return the age of this track == number of times update has been called.
   */
  virtual int getAge() { return mAge; }

  /**
   * Unique id for this track.
   */
  virtual int getId() { return mId; }

  /**
   * Get the trajectory.
   */
  virtual TrajectoryPtr getTrajectory() { return mpTrajectory; }

  /**
   * Get force vector
   */
  virtual Vector2f getForceVector() {return mForceVector;}

  /**
   * Get direction vector
   */
  virtual Vector2f getDirectionVector() {return mDirectionVector;}  

  //
  // INQUIRY
  //

public:

  /**
   * Ask if this object has been initialized (the init() function has been called).
   */
  virtual bool isInitialized() const { return mIsInitialized; }

  /**
   * Check if this track is alive. Tracks are killed by calling kill().
   */
  virtual bool isAlive() const { return mIsAlive; }


  //
  // ATTRIBUTES
  //

protected:

  /**
   * Condensation class with custom prediction function.
   */
  class ThisCondensation : public Condensation<float>
  {
  public:
    void init(int numParticles, int dimension, MultiFrameTrack *pMFT);
  protected:
    virtual void predict(); 
    MultiFrameTrack *mpMultiFrameTrack;
  };

  /**
   * Tells if this object has been initialized.
   */
  bool mIsInitialized;

  /**
   * Reference to the foreground segmented circular
   * buffer.
   */
  ForegroundHistoryPtr mpForegroundHistory;

  /**
   * Condensation object.
   */
  ThisCondensation mCondensation;

  /**
   * Blob size estimation class.
   */
  BlobSizeEstimatePtr mpBlobSizeEstimate;

  /**
   * Current snake.
   */
  Snake mSnake;

  /**
   * This variable indicates if the circular
   * buffer has been filled from the previous iteration.
   */
  bool mCircularBufferFilled;

  /**
   * This variable indicates if this track is alive.
   */
  bool mIsAlive;

  /**
   * The age of the track based on the number of times update
   * has been called.
   */
  int mAge;

  /**
   * Id for this track.
   */
  int mId;

  /**
   * Full track trajectory.
   */
  TrajectoryPtr mpTrajectory;

  /**
   * Start time. Typically in frames.
   */
  //double mStartTime;

  /**
   * The blob speed per frame with respect to the with of
   * the blob. This means that if the speed is 0.5, the blob
   * will be assumed to move at most half of the width in
   * each x,y direction.
   */
  float mMaxBlobSpeedPerFrame;

  /**
   * The ratio of expected nonzeroes in the bounding box
   * of the estimated blob size. Typically the pixels at
   * the border are not zero because the blob has an elliptical
   * shape.
   */
  float mBoundingBoxNonZeroesRatio;  

  /**
   * Will the particles use motion information in addition to segmentation data
   */
  bool mIsMotionEnabled;

  /**
   * The motion history data. Containing the time that has passed
   * since there was last motion at this location.
   */
  boost::shared_ptr<const Image<double> > mpLastMotion;

  double mMaxNoMotionTime;

  /**
   * Repulsion vector. A vector indicating the preferred position for the
   * for the track to travel with respect to other nodes.
   */
  Vector2f mForceVector;

  /**
   * A vector indicating the tracks predicted motion based on its past motion.
   */
  Vector2f mDirectionVector;


  //HACK
  /**
   * Hack used for BestBuy, basically we run an alternate version of the blobtracker if this is enabled
   */
  bool mUseAlternateTracker;

};


}; // namespace vision

}; // namespace ait

#endif // MultiFrameTrack_HPP


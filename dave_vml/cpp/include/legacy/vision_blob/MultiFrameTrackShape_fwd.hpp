/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef MultiFrameTrackShape_fwd_HPP
#define MultiFrameTrackShape_fwd_HPP

#include <boost/shared_ptr.hpp>
#include <legacy/pv/CircularBuffer.hpp>
#include <legacy/pv/ait/Image_fwd.hpp>

namespace ait
{

namespace vision
{

class MultiFrameTrackShape;
typedef boost::shared_ptr<MultiFrameTrackShape> MultiFrameTrackShapePtr;
typedef CircularBuffer<Image8Ptr> ForegroundHistory;
typedef boost::shared_ptr< ForegroundHistory > ForegroundHistoryPtr;

}; // namespace vision

}; // namespace ait

#endif // MultiFrameTrack_fwd_HPP


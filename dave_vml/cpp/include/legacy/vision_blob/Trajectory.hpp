/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef Trajectory_HPP
#define Trajectory_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <vector>
#include <list>
#include <algorithm>

// AIT INCLUDES
//

#include <legacy/pv/ait/Image.hpp>
#include <legacy/pv/ait/Vector3.hpp>
#include <legacy/math/LineSegment.hpp>
#include <legacy/pv/ait/Rectangle.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

#include "legacy/vision_blob/Trajectory_fwd.hpp"

namespace ait 
{

namespace vision
{

/**
 * This class can be used to define trajectories which are a list of 
 * locations in 2D or 3D given by a time stamp. Although it is not automatically
 * verified by this class, it is assumed that the list is sorted by time, and that
 * two nodes will not have the same time. The member function isValid() can check
 * this condition. Note that the difference between consecutive Node times is not
 * necessarily constant.
 * @par Responsibilities:
 * - Store a list with Nodes.
 * - Identify trajectories with an id.
 * - Validate trajectories
 * - Save/load trajectory in CSV files.
 * @remark If working with 2D, just instantiate Vector2f and it will automatically
 * set the z coordinate to zero.
 */
class Trajectory
{
  //
  // LIFETIME
  //

public:

  /**
   * This structure defines a trajectory node or vertex.
   */
  class Node
  {
  public:

    Node(double time0, const Vector3f& location0, const Rectanglef& bbox0) : 
        time(time0), location(location0), boundingBox(bbox0), cardinality(1) {}   

    Node(double time0, const Vector3f& location0) : 
        time(time0), location(location0), cardinality(1) {}   

    double time;
    Vector3f location;
    Rectanglef boundingBox;
    float cardinality;
  };

  typedef boost::shared_ptr<Node> NodePtr;

  /**
   * The node collection.
   */
  typedef std::list<NodePtr> Nodes; 

  /**
   * Constructor.
   */
  Trajectory(int id = -1);

  /**
   * Copy Constructor
   */
  Trajectory(const Trajectory& from);

  /**
   * Destructor
   */
  virtual ~Trajectory();

  /**
   * Deep copy clone
   */
  TrajectoryPtr clone();

  //
  // OPERATORS
  //

public: 

  /**
   * Assignment operator.
   * @param from [in] The value to assign to this object.
   * @return A reference to this object.
   */
  Trajectory& operator=(const Trajectory& from);

  /**
   * Comparison operator.
   * @param from [in] The value to compare this class with.
   * @return true if classes are equal, false otherwise.
   */
  bool operator==(const Trajectory& from) const;

  /**
   * Difference operator. This is the negation of the comparison.
   */
  bool operator!=(const Trajectory& from) const;

  //
  // OPERATIONS
  //

public:

  /**
   * Draw the image to the given RGBImage object. Used for testing.
   * @param img [in] The image to draw the line to
   * @param color [in] Color of trajectory i.e. PV_RGB(0,0,0) for black
   */
	void draw(Image32& img, unsigned int color = PV_RGB(0,0,0), double time = -1.0) const;

  /**
   * DEPRECATED: use compress()
   * This function compacts a trajectory. Points which are close together
   * are averaged into a single point (without considering time differences)
   * according to the provided threshhold.
   *
   * @param threshhold [in] The compacting threshold. Points located on or
   * within this threshold will be compacted.
   * @remarks
   * This may result in a trajectory of only one point.
   */
  void compact(float threshhold);

  
  /**
   * Compress a trajectory.
   *
   * @param distInterval The maximum distance permitted between sampled nodes
   * @param timeInterval The maximum time difference permitted between sampled nodes
   */
  TrajectoryPtr compress(double distInterval, double timeInterval);
 
  
public:

  /**
   *	Takes two trajectories and returns the combined result.
   *  The trajectories are combined simply by appending
   *  one onto the other. The new trajectory's id is
   *  min(a's id, b's id).  
   *  
   *  @param a [in] Trajectory a
   *  @param b [in] Trajectory b
   *  @param result [out] The result of combining a with b
   *
   *  @remarks
   *  The spatial or temporal order of a and b do not matter, as long
   *  as the two trajectories do not overlap in time. Thus, one of 
   *  the trajectories must be finished before the other one begins.
   */

  static void combine(const Trajectory& a, const Trajectory& b, Trajectory& result);  

  /**
   * Save into a CSV file.
   * @param fileName [in] name of the file to store the data.
   * @param trajectories [in] The trajectories to save in the file.
   * @par File Format:
   * @code 
   * TrajectoryId,Time,X,Y,Z
   * ...
   * ...
   * @endcode
   * It will be sorted by Trajectory Id and then time.
   */
  static void saveCSV(const std::string& fileName, std::list<TrajectoryPtr>& trajectories);


  /**
   * Save trajectories into a CSV file in a CSV format containing XML 
   * that describes each trajectory. This format also contains time offset
   * informat that is useful for dealing with time drift.
   *
   * @param fileName [in] name of the file to store the data.
   * @param trajectories [in] The trajectories to save in the file.
   * @par File Format:
   * @code 
   * Start Time, Duration, Trajectory (in XML format)
   * ...
   * ...
   * @endcode
   */
  static void saveXmlCsv(
    const std::string& filename, 
    const std::list<TrajectoryPtr>& trajList);

  /**
   * Load from a CSV file. See save() for file format.
   * @param fileName [in] Name of the file.
   * @param trajectories [out] New trajectories loaded from the file.
   * @remark This function has an undefined behavior if the file
   * is not in the correct format.
   */
  static std::list<TrajectoryPtr> loadCSV(const std::string& fileName);

  /**
   * Convert the trajectory to an XML string.
   * The format is:
   * <trajectory id='[Trajectory Id]' start_time=''>t0,x0,y0;t1,x1,y1; ... tn,xn,yn</trajectory>
   * Where all times are relative to the start_time.
   *
   * @param decimals Number of decimal places to keep for trajectory positions.
   * Generally this level of precision is not needed, and omitting it
   * reduces disk space and bandwidth requirements.
   */
  std::string toXmlString(unsigned int decimals = 0);

  /**
   * Create a trajectory from an XML string. The startTime
   * will override the start time defined in the xml unless
   * it is zero.
   */
  static TrajectoryPtr fromXmlString(std::string& xml, double startTime = 0);

protected:
  /**
   * This function compacts all the nodes lying on the interval
   * [start, end]. The average position is calculated, and it
   * a new node with the average position and starting time
   * replaces all of the nodes on the given interval.
   */
  void compactNodes(Nodes::iterator start, Nodes::iterator end);
 

public:    
  
  /**
   * Returns a normalized direction vector pointing to where
   * the trajectory originated from. This function is primarily used
   * for extrapolation.
   *
   * @param thresh The maximum distance between two nodes
   * of a trajectory that will be considered when computing
   * the direction vector. This function may return a zero vector,
   * and this should be handled by the calling function.
   */
  Vector2f reverseDirection(float thresh) const;
 
  /**
   * Returns a normalized direction vector pointing to where
   * the trajectory is headed. This function is primarily used
   * for extrapolation. This function may return a zero vector,
   * and this should be handled by the calling function.
   *
   * @param thresh The maximum distance between two nodes
   * of a trajectory that will be considered when computing
   * the direction vector.
   */
  Vector2f forwardDirection(float thresh) const;

  /**
   * This function is similar to forwardDirection, except
   * that it returns a vector generated from trajectory nodes at most
   * 'time' apart. The returned vector is not normalized. 
   */
  Vector2f forwardVector(float timeThresh) const;

  /**
   * This function is similar to reverseDirection, except
   * that it returns a vector generated from trajectory nodes at most
   * 'time' apart. The returned vector is not normalized. 
   */
  Vector2f reverseVector(float timeThresh) const;


  /**
   * TODO: DOC	
   */   
  Vector2f forwardVectorAtTime(float timeThresh, float time) const;    

  /**
   * Merge this trajectory with a target trajectory. This trajectory
   * will immediately follow the path of the target trajectory when
   * its own trajectory would have finished.   
   */
  void merge(const Trajectory& target);

  /**
   * Split this trajectory from the target trajectory. Instead of starting
   * from its original position, this trajectory will follow the path of 
   * the target trajectory as long as possible, and then split off along
   * its original path.
   */
  void split(const Trajectory& target);  
  

  //TODO: FIX DOCUMENTATION WHEN FINISHED CHANGING CODE HERE
  /**
   * Extrapolate the trajectory so that its new end point is 
   * located at the given location
   *   
   */
  void extrapolateForward(const Vector2f& location, float interval);
  
  /**
   * Extrapolate the trajectory so that its new start point is 
   * located at the given location.    
   * 
   */
  void extrapolateReverse(const Vector2f& location, float interval);

  /**
   * Returns the length of the trajectory from the rear.
   * See forwardDirection for an explanation regarding
   * the nodes argument.
   */
  float lengthFromRear(float thresh) const;
  
  /**
   * Returns the length of the trajectory from the front.
   * See forwardDirection for an explanation regarding
   * the nodes argument.
   */
  float lengthFromFront(float thresh) const;
  

  /**
   * Returns the time elapsed 'nodes' from the rear.
   * See forwardDirection for an explanation regarding
   * the nodes argument.
   */
  float timeFromRear(float thresh) const;  
  
  /**
   * Returns the time elapsed 'nodes' from the front.
   * See forwardDirection for an explanation regarding
   * the nodes argument.
   */
  float timeFromFront(float thresh) const;  

  /**
   * Returns the forward speed of the trajectory.
   * See forwardDirection for an explanation regarding
   * the nodes argument.
   */
  float forwardSpeed(float thresh)
  {    
    return lengthFromFront(thresh) / timeFromFront(thresh);
  }
  
  /**
   * Returns the forward speed of the trajectory.
   * See forwardDirection for an explanation regarding
   * the nodes argument.
   */
  float reverseSpeed(float thresh)
  {    
    return lengthFromRear(thresh) / timeFromRear(thresh);
  }

  /**
   * Returns the cardinality of the trajectory at the given time.
   * The cardinality is found by averaging the cardinality values of the
   * surrounding nodes.
   */
  int cardinalityAtTime(float time, float timeThresh) const;


  /**
   * Returns the length of the entire trajectory
   */
  float length() const;
  
  //
  // ACCESS
  //

  /**
    * Returns a copy of the first node
    */

  NodePtr firstNode() const
  {
    return mNodes.front();
  }

  /**
    * Returns a copy of the last node
    */
  NodePtr lastNode() const
  {
    return mNodes.back();
  }

  /**
   * Returns a pointer to the node with a time value equal to or
   * immediately succeeding time.
   */
  NodePtr nodeClosestTime(double time) const;

  /**
   * Returns the number of nodes in this trajectory 
   */

  int nodeCount() const
  {
    return mNodes.size();
  }

  /**
   * Returns the collection of nodes for this trajectory. This
   * collection can be modified from outside.
   */
  Nodes& getNodes() { return mNodes; }

  /**
   * Returns a copy of the collection of nodes for this trajectory.
   */
  Nodes getNodes() const { return mNodes; }

  /**
   * Returns a copy of all nodes with time values between startTime
   * and endTime. Nodes with values equivalent to startTime or
   * endTime are also included.
   */

  void getNodesTimeSlice(const double startTime, const double endTime, Nodes& nodelist) const;

  /**
   * Get the location of the trajectory at the given time. It will perform any
   * interpolation necessary. It will look starting from the given node.
   * @param [in] time: Time inside the trajectory.
   * @param [in/out] iStartNode: Node to start the search. It will modify to return the next
   * node that should be used to start the search if moving forward. If the time
   * desired is less than the given node, it will not find it.
   * @param [out] location: Location of the trajectory at the given time.
   * @return true if the location was found, false if the time is outside the bounds
   * of the trajectory.
   */
  bool getLocationFromTime(double time, Nodes::const_iterator& iStartNode, Vector3f& location) const;

  /**
   * Break the trajectory at the node closest to the given time. Returns the
   * latter trajectory with the given id. If the time specified is after 
   * the time range of the trajectory returned will be empty
   *
   * If the time specified is before the time range, this trajectory
   * will become empty, and a copy (with the new id) will be returned
   */

  TrajectoryPtr breakUp(double time, int id)
  {
    Nodes::iterator iNode;
  
    for (iNode = mNodes.begin(); iNode != mNodes.end(); iNode++)
    {
      if ((*iNode)->time >= time)
        break;
    }

    TrajectoryPtr pNewTraj(new Trajectory(id));
    
    //copy the latter portion of the trajectory
    std::copy(iNode, mNodes.end(), std::inserter(pNewTraj->mNodes, pNewTraj->mNodes.begin()));
    //remove these nodes from the original trajectory
    mNodes.erase(iNode, mNodes.end());

    return pNewTraj;
  }

  /**
   * Get the id of this trajectory.
   */
  int getId() const { return mId;}

  /**
   * Set the id of this trajectory.
   */
  void setId(int id) { mId = id;}

   /**
   * Get the time of the first node in the list. It will return
   * -1 if the node list is empty.
   */
  double getStartTime() const { return mNodes.empty() ? -1 : mNodes.front()->time; }

  /**
   * Get the time of the last node in the list. It will return
   * -1 if the node list is empty.
   */
  double getEndTime() const { return mNodes.empty() ? -1 : mNodes.back()->time; }


  /**
   * Returns the total age of the trajectory, or -1 if the node list is empty
   */
  double getAge() const {return mNodes.empty() ? -1 : getEndTime() - getStartTime(); }


  /**
   * Returns the start position of the trajectory
   */
  Vector3f getStartPos() const {return mNodes.empty() ? Vector3f(-1,-1) : mNodes.front()->location;}

  /**
   * Returns the start position of the trajectory
   */
  Vector3f getEndPos() const {return mNodes.empty() ? Vector3f(-1,-1) : mNodes.back()->location;}

  /**
   * Get the string representation of this trajectory in XML format. This
   * format is supported by the VMS events as an extended attribute.
   */
  std::string getXmlBlobRectangles();

  /**
   * Add a node at the end of the trajectory.
   */
  void add(NodePtr pNode) { mNodes.push_back(pNode); }

  //
  // INQUIRY
  //

public:


  /**
   * Check that the list of nodes is valid. This means that the times
   * are sorted in ascending order, are positive, and are all different.
   */
  bool isValid() const;
  
  //
  // ATTRIBUTES
  //

protected:

  /**
   * An integer that identifies this trajectory.
   */
  int mId;

  /**
   * The actual nodes.
   */
  Nodes mNodes; 

};


}; // namespace vision
}; // namespace ait

#endif // Trajectory_HPP


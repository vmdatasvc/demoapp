/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef BlobTrackerMod_HPP
#define BlobTrackerMod_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <list>
#include <map>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>
#include <legacy/pv/ait/Image.hpp>
//#include <Benchmark.hpp>
//#include <PvImageProc.hpp>

// LOCAL INCLUDES
//

#include "legacy/vision_blob/TrajectoryEvents.hpp"
//#include "RealTimeMod.hpp"
#include "legacy/vision/VisionMod.hpp"
//#include "BlobTrackerMsg.hpp"
//#include "VmsPacket.hpp"
#include "legacy/vision_blob/MultiFrameTrack.hpp"
#include "legacy/vision_blob/SimplePolygon.hpp"
#include <legacy/vision/ImageAcquireMod.hpp>
#include "legacy/vision_blob/TrackManager.hpp"
#include "legacy/vision_blob/MotionDetectionMod.hpp"

//#include "Settings.hpp"


// FORWARD REFERENCES
//

#include "legacy/vision_blob/BlobTrackerMod_fwd.hpp"
#include "legacy/vision_blob/TrajectoryEvents_fwd.hpp"
#include "legacy/vision_blob/ForegroundSegmentMod_fwd.hpp"

namespace ait 
{

namespace vision
{

/**
 * This vision module is not yet documented.
 * @par Responsibilities:
 * - Write the responsibilities here.
 */
class BlobTrackerMod : public VisionMod   // RealTimeMod not needed for our purpose. All research modules are async VisionMod
{
public:

  //
  // LIFETIME
  //

  /**
   * Default Constructor. The constructor creates a quick instance. Expensive operations
   * are executed in the init() function.
   */
  BlobTrackerMod();

  /**
   * Constructor. The constructor creates a quick instance. Expensive operations
   * are executed in the init() function.
   */
  BlobTrackerMod(const std::string& moduleName);

  /**
   * Destructor
   */
  virtual ~BlobTrackerMod();

  /**
   * Provide dependency information for the ExecutionGraph.
   * @see VisionMod::initExecutionGraph for more info.
   */
//  virtual void initExecutionGraph(std::list<VisionModPtr>& srcModules);

  /**
   * Initialization function.
   */
  virtual void init();

  /**
   * Finalization function
   */
  virtual void finish();

  //
  // OPERATIONS
  //

public:

  /**
   * Process this module.
   */
  virtual void process();

  /**
   * Visualize this module.
   */
  virtual void visualize(Image32& img);

  /**
   * Process the incoming vision messages.
   */
//  virtual void processMessages();

  /**
   * Returns a list of the active tracks
   */
  Tracks getActiveTracks() {return mTracks;}

  virtual Bool isAsync() const {return true;}

protected:

  /**
   * Process incoming message.
   */
//  virtual void processMsg(BlobTrackerMsg* pMsg);

  /**
   * Look for new tracks in the image. The method is to scan the filtered
   * by blob size image, remove the foreground signals from current tracks
   * and look for other significant foreground regions that might be tracked
   * by other tracks.
   */
  virtual void scanForNewTracks();

  /**
   * Set foreground values to zero around location pt based on the blob
   * size.
   */
  virtual void cleanForegroundSegmentation(Image8& img, const Vector2f& pt);

  /**
   * Check if two or more tracks overlap.
   */
  virtual void checkOverlappingTracks();

  /**
   * Send a Vms event based on a polygon event.
   */
//  virtual void sendVmsEvent(PolygonEventPtr pEvent, int eventChannel);

  /**
   * Send a Vms event based on a trajectory.
   */
//  virtual void sendVmsEvent(TrajectoryPtr pTrajectory, int eventChannel);

  /**
   * Perform track post-processing.
   * @param force [in] Process every track regardless of
   * the delay. This is used when the video is finished.
   */
  virtual void postProcessTracks(bool force);

  /**
   * Generate the polygon events based on a list of trajectories.
   */
//  virtual void generateEvents(const std::list<TrajectoryPtr>& trajectories);

  /**
   * Kill all tracks inside the kill track polygons
   */
  virtual void killTracksInPoly();

  /**
   * Convert a trajectory from the scaled down segmentation coordinate space
   * to the full image coordinate space.
   */
  virtual void convertToImageCoord(TrajectoryPtr pTraj);

  //functions dealing with blob repulsion
  virtual Vector2f pointRepulsion(const Vector2f& p1, const Vector2f& p2);

  virtual Vector2f calculateTrackRepulsion(MultiFrameTrackPtr pTrack);

  virtual void calculateTrackForce();  

private:

  //
  // ACCESS
  //

public:

  /**
   * Get the static name of this module. This name is used to name the type of this module.
   * The constructor sets the member variable mName to this value.
   * @remark This static function is only implemented for concrete classes.
   */
  static const char *getModuleStaticName() { return "BlobTrackerMod"; }

  //
  // INQUIRY
  //

public:

  //
  // ATTRIBUTES
  //

protected: 

  // Settings object to read/save parameters.
  // originally in VisionMod which was originally superclass of this, but
  // inheritance was removed so declared here
  Settings  mSettings;

  /**
   * Benchmark variable.
   */
//  Benchmark mBenchmark;

  /**
   * Reference to the foreground segmenter module.
   */
  ForegroundSegmentModPtr mpForegroundSegmentMod;

  /**
   * Upper bound time limit for post processing. If exceeded, post processing
   * occurs immediately.
   */
  double mPostProcessUpperInterval;

  /**
   * Lower bound time limit for post processing. When exceeded, post processing
   * will occur only if there are no active tracks
   */
  double mPostProcessLowerInterval;

  /**
   * The next time that post processing will be forced to occur if it does not
   * happen earlier. This is regardless of active tracks.
   */
  double mNextForcePostProcessTime;

  /**
   * The next time that post processing will occur, provided there are no active
   * tracks.
   */
  double mNextPostProcessTime;

  /**
   * The list of current active tracks.
   */
  Tracks mTracks;  

  /**
   * Flag that indicates if this is a top view camera.
   * This is used to decide if the trajectories should
   * be based on the center of the blob (head for top view
   * cameras) or the bottom (feet for side cameras).
   */
  bool mIsTopViewCamera;

  /**
   * This image contains foreground responses that
   * do not belong to any tracks.
   */
  Image8Ptr mpNewTrackScanImage;

  /**
   * Maximum nodes for each snake.
   */
  int mMaxSnakeSize;

  /**
   * Print debug messages to console.
   */
  bool mVerbose;

  /**
   * Counter to generate new ids for tracks.
   */
  int mNewTrackId;

  /**
   * Maximum number of concurrent tracks.
   */
  int mMaxConcurrentTracks;

  /**
   * All trajectories ready for post-processing.
   */
  std::list<TrajectoryPtr> mPreTrajectories;

  /**
   * The minimum distance at which a new track can be created with
   * respect to existing tracks.
   */
  float mMinTrackCreationDistance;

  /**
   * Circular buffer with the last frames. That way we can
   * visualize the tail of the snake.
   * @remark Using this is very expensive! Only for visualization.
   */
  CircularBuffer<Image32Ptr> mLastColorFrames;

  /**
   * Visualize Snake Tail. Very expensive!!!
   */
  bool mVisualizeSnakeTail;

  /**
   * Event Channel for Vms messages.
   */
//  VmsPacketPtr mpVmsPacket;

  /**
   * Polygon that denotes the boundaries to detect
   * events.
   */
  std::map<int,SimplePolygon> mEventsPolygon;

  /**
   * Flag that indicates whether the trajectories should be saved
   * to disk. Trajectories will be dumped into timestamped .csv files
   * in the trajectory output folder.
   */
  bool mSavePreTrajectories;

  /**
   * Folder to dump saved trajectories to.
   */
  std::string mTrajectoryOutputFolder;


  /**
   * An integer containing the maximum number of trajectories that will
   * be processed.
   */
  //int mMaxPreTrajectories;  
   

  /**
   * A list of trajectories that will be dumped to a file. This list
   * will not be used unless "savePreTrajectories [bool]" is true.
   */
  //std::list<TrajectoryPtr> mSavedTrajectories;

  /**
   * Indicate if the events to be generated are from Pre-processed
   * trajectories.
   */
  bool mSendPreprocessTrajectories;
  
  /**
   * The TrackManager object which handles postprocessing.	
   */
  TrackManager mTrackManager;

  /**
   * Handles generating events from given trajectories.	
   */
  TrajectoryEvents mTrajEvents;

  /**
   * A list of event channels this module processes	
   */
  std::list<int> mEventChannels; 

  /**
   * The width of each frame
   */
  int mFrameWidth;

  /**
   * The height of each frame
   */
  int mFrameHeight;

  /**
   * A frame from the original video source
   */
  Image32 mOriginalVideo;  

  /**
   * The video source id
   */
  int mVideoSourceId;

  /**
   * Enable the motion module for additional track filtering
   */
  bool mIsMotionEnabled;

  /**
   * Points to the motion detection module.
   */
  MotionDetectionModPtr mpMotionMod;

  /**
   * Maximum time with no motion that tracks will be created and be allowed to live
   */
  double mMaxNoMotionTime;

  /**
   * Write Events in CSV file. This is a simple way to generate events
   * and save them on the local machine without the need of a VMS server.
   * The events will be saved in the trajectoryOutputFolder as defined
   * in the settings. 
   */
  bool mSaveCsvFileEvents;

  /**
   * Send the trajectories as the event attribute a4.
   */
  bool mSendTrajectoriesAsEventAttribute;

  /**
   * This flag indicates if the visualization is active for
   * this module in particular.
   */
  bool mPaintVisualization;

  /**
   * New tracks will onlyh be created inside of this region.
   */
  std::list<Polygon> mStartTrackPolyList;

  /**
   * A mask generated from mStartTrackPoly, scaled to the size of the 
   * foreground segmentation.
   */
  Image8 mStartTrackMask;

  /**
   * All active tracks are immediate killed in this region
   */
  std::list<Polygon> mKillTrackPolyList;

  /**
   * A mask generated from mKillTrackPoly, scaled to the size  of the
   * foreground segmenation.
   */
  Image8 mKillTrackMask;

  /**
   * Use the alternate blob tracker settings
   */
  bool mUseAlternateTracker;


  bool mRepelTracks;  


};


}; // namespace vision

}; // namespace ait

#endif // BlobTrackerMod_HPP



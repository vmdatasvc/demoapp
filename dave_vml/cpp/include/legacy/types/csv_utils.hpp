/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef csv_utils_HPP
#define csv_utils_HPP

#include <string>
#include <vector>
#include <map>
#include <exception>

namespace ait 
{

/**
 * Read one line from the csv file. If it returns an empty vector
 * it means that there are no more lines to read.
 */
void readCsvLine(std::ifstream& fin, std::vector<std::string>& row);

/**
 * Write a line in the CSV file.
 */
void writeCsvLine(std::ofstream& fout, std::vector<std::string>& row);


} // namespace ait

#endif

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef __AITBASETYPES_HPP__
#define __AITBASETYPES_HPP__

#include <boost/smart_ptr.hpp>

namespace ait 
{
  /**
   * ait's boolean type
   * @todo undefine this useless type.
   */
  typedef bool Bool;

  /**
   * ait's character type
   * @todo undefine this useless type.
   */
  typedef char Char;

  /**
   * ait's unsigned character type
   */
  typedef unsigned char Uchar;

  /**
   * ait's 8 bit type
   */
  typedef char Int8;

  /**
   * ait's unsigned 8 bit type
   */
  typedef unsigned char Uint8;

  /**
   * ait's 16 bit type
   */
  typedef short Int16;

  /**
   * ait's unsigned 16 bit type
   */
  typedef unsigned short Uint16;

  /**
   * ait's 32 bit type
   */
  typedef int Int32;

  /**
   * ait's unsigned 32 bit type
   */
  typedef unsigned int Uint32;

  /**
   * ait's integer type
   * @todo undefine this useless type.
   */
  typedef int Int;

  /**
   * ait's unsigned integer type
   */
  typedef unsigned int Uint;

  /**
   * ait's long type
   * @todo undefine this useless type.
   */
  typedef long Long;

  /**
   * ait's unsigned long type
   */
  typedef unsigned long Ulong;

#ifdef WIN32
  /**
   * ait's 64 bit integer type (windows version)
   */
  typedef __int64 Int64;

  /**
   * ait's unsigned 64 bit integer type (windows version)
   */ 
  typedef unsigned __int64 Uint64;
#else

  /**
   * ait's 64 bit integer type (linux version)
   */
  typedef long long Int64;

  /**
   * ait's unsigned 64 bit integer type (linux version)
   */
  typedef unsigned long long Uint64;
#endif

  /**
   * ait's float type
   * @todo undefine this useless type.
   */
  typedef float Float;

  /**
   * ait's double type
   * @todo undefine this useless type.
   */
  typedef double Double;

  // Forward Value class declaration so that ValuePtr can be defined
  class Value;

  // Forward Object class declaration so that ObjectPtr can be defined
  class Object;

  // Forward DynamicObject class declaration so that DynamicObjectPtr can be defined
  class DynamicObject;

  // Forward Exception class declaration so that ExceptionPtr can be defined
  class Exception;

  /**
   * ValuePtr type definition
   */
  typedef boost::shared_ptr<Value> ValuePtr;

  /**
   * ObjectPtr type definition
   */
  typedef boost::shared_ptr<Object> ObjectPtr;

  /**
   * DynamicObjectPtr type definition
   */
  typedef boost::shared_ptr<DynamicObject> DynamicObjectPtr;

  /**
   * ExceptionPtr type definition
   */
  typedef boost::shared_ptr<Exception> ExceptionPtr;
};


#endif  // __AITBASETYPES_HPP__

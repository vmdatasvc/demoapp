/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef cmd_line_utils_HPP
#define cmd_line_utils_HPP

#include <string>
#include <vector>
#include <map>
#include <exception>
#include <boost/shared_ptr.hpp>

namespace ait 
{

/**
 * Dictionary of command line arguments. See parse_args() for details.
 */
typedef std::map< std::string, std::vector<std::string> > CommandLineDict;

/**
 * This class is used to return commands in the parse_args_list() function.
 */
class CommandLineArgument
{
public:
  std::string option;
  std::string value;
};

typedef boost::shared_ptr<CommandLineArgument> CommandLineArgumentPtr;

class bad_quotes : public std::exception
{
  virtual const char* what() { return "Quotes don't match"; }
};

/**
 * Parse command line arguments. The arguments must be provided in a single
 * string. It will return a map with lists of arguments. The arguments with
 * no option will be in the empty string entry of the map. This entry will
 * always be present. If there are no arguments, the list will be empty.
 * The current implementation only parses long options in the form "--option 
 * [value]". Here are some examples:
 * - parse_args("--opt1 1 --opt2 --opt3 \"the value3\"") =
 * {'':[], '--opt1':['1'], '--opt2':[], '--opt3':['the value3']}
 * - parse_args("1 2 3") = {'':['1','2','3']}
 * - parse_args("--opt1 1 --opt2 2 --opt1 3 4 5") =
 * {'':['4','5'], '--opt1':['1','3'], '--opt2':['2']}
 * @throws bad_quotes() on unmatched quotes.
 * @remarks Current implementation does not accept escaped quotes inside
 * quotes. No empty strings are allowed inside quotes.
 */
CommandLineDict 
parse_args(const std::string& args);

/**
 * This version is for C style argv, argc parameters.
 */
CommandLineDict
parse_args(int argc, char **argv);

/**
 * This function will help debugging the argument parsing process.
 * It returns a string with the argument dictionary in python style.
 */
std::string print_args(const CommandLineDict& dict);

/**
 * Parse the command line as a list of arguments. This
 * version allows to have repeated parameters, for example:
 * - parse_args_list("--opt1 1 --opt1 2 --opt2") =
 * [{"--opt1","1"},{"--opt1","2"},{"--opt2",""}]
 * - parse_args_list("1 2 3") =
 * [{"","1"},{"","2"},{"","3"}]
 */
std::vector<CommandLineArgumentPtr>
parse_args_list(const std::string& args);

/**
 * This version is for C style argv, argc parameters.
 */
std::vector<CommandLineArgumentPtr>
parse_args_list(int argc, char **argv);

/**
 * This function will help debugging the argument parsing process.
 * It returns a string with the structure content.
 */
std::string print_args(const std::vector<CommandLineArgumentPtr>& l);


} // namespace ait

#endif

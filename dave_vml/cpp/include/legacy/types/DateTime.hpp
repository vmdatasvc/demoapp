/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef DateTime_HPP
#define DateTime_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <legacy/pv/PvUtil.hpp>
#include <string>

// AIT INCLUDES
//

#include <legacy/types/String.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

#include "legacy/types/DateTime_fwd.hpp"

namespace ait 
{


/**
 * Save information about the date and time of the day using the
 * gregorian calendar and local time settings.
 * @par Responsibilities:
 * - Convert from seconds since epoch in UTC (formerly GMT) to local time.
 * @par Design Considerations:
 * - This class can not execute operations yet. It can only transform
 * from time.
 * @remarks
 * - To get the UTC number of seconds since Epoch, use PvUtil::systemTime().
 * - This library is now implemented with the C standard library and time_t.
 * This library has a resolution of 1 second, and it overflows after 
 * year 
 */
class DateTime
{
  //
  // LIFETIME
  //

public:

  /**
   * Default format string: "%a, %d %b %Y %H:%M:%S"
   */
  static const std::string defaultFormatStr;

  /**
   * SQL format string: "%Y-%m-%d %H:%M:%S"
   */
  static const std::string sqlFormatStr;

  /**
   * This format can be used for a file name with timestamp.
   * "%Y%m%d_%H%M%S"
   */
  static const std::string fileFormatStr;

  /**
   * This is an extended Sql format used in the VMS products.
   * it includes milliseconds.
   * "%Y-%m-%dT%H:%M:%S.%f"
   */
  static const std::string vmsFormatStr;

  /**
   * Structure with the current day and time information.
   * @remark The range in seconds really is 0 to 61; this accounts for 
   * leap seconds and the (very rare) double leap seconds. 
   */
  typedef struct _Struct
  {
  public:
    _Struct() : year(1900), month(1), day(1), hour(0), minute(0), second(0) {};

    int year;      ///< Four digits year (2003)
    int month;     ///< Month [1,12]
    int day;       ///< Day [1,31]
    int hour;      ///< Hour [0,23]
    int minute;    ///< Minute [0,59]
    double second; ///< Second [0,61]
    int weekday;   ///< Monday=0
    int julian;    ///< Julian day of the year [1,366]
    int daylight;  ///< Daylight savings flag.
  } Struct;

  /**
   * Default constructor initializes the given parameter to local time.
   * The default constructor initializes to local time.
   */
  DateTime(const double sec = PvUtil::systemTime())
  {
    setLocal(sec);
  }

  /**
   * Build a date based on the given date information.
   */
  DateTime(const int year, const int month, const int day, 
    const int hour = 0, const int minute = 0, const double second = 0);

  //
  // OPERATORS
  //

public:

  //
  // OPERATIONS
  //

public:

  /**
   * Fill this class with local time from the provided seconds.
   */
  bool setLocal(const double sec = PvUtil::systemTime())
  {
    return secsToLocal(sec,mStruct);
  }

  /**
   * Fill this class with UTC time from the provided seconds.
   */
  bool setUTC(const double sec = PvUtil::systemTime())
  {
    return secsToUTC(sec,mStruct);
  }

  /**
   * Fill a date-time structure with the given time in UTC.
   */
  static bool secsToUTC(const double sec, Struct& s);

  /**
   * Fill a date-time structure with the current time in UTC.
   */
  static bool getUTC(Struct& s);

  /**
   * Fill a date-time structure with the given time in local time
   * settings.
   */
  static bool secsToLocal(const double sec, Struct& s);

  /**
   * Fill a date-time structure with the current time in local time
   * settings.
   */
  static bool getLocal(Struct& s);

  /**
   * Format this object as a string.
   * The format string can have the following values:
   * - %a Locale's abbreviated weekday name.   
   * - *%A Locale's full weekday name.   
   * - %b Locale's abbreviated month name.   
   * - *%B Locale's full month name.   
   * - *%c Locale's appropriate date and time representation.   
   * - %d Day of the month as a decimal number [01,31].   
   * - %H Hour (24-hour clock) as a decimal number [00,23].   
   * - *%I Hour (12-hour clock) as a decimal number [01,12].   
   * - *%j Day of the year as a decimal number [001,366].   
   * - %m Month as a decimal number [01,12].   
   * - %M Minute as a decimal number [00,59].   
   * - *%p Locale's equivalent of either AM or PM.   
   * - %S Second as a decimal number [00,61].
   * - %f Fraction of seconds in 3 digit milliseconds [000,999]
   * - *%U Week number of the year (Sunday as the first day of the week) as a decimal number [00,53]. All days in a new year preceding the first Sunday are considered to be in week 0.   
   * - %w Weekday as a decimal number [0(Sunday),6].   
   * - *%W Week number of the year (Monday as the first day of the week) as a decimal number [00,53]. All days in a new year preceding the first Sunday are considered to be in week 0.   
   * - *%x Locale's appropriate date representation.   
   * - *%X Locale's appropriate time representation.   
   * - *%y Year without century as a decimal number [00,99].   
   * - %Y Year with century as a decimal number.   
   * - *%Z Time zone name (or by no characters if no time zone exists).   
   * - %% A literal "%" character. 
   * Values with * are not implemented.
   */
  std::string format(const std::string fmt = DateTime::defaultFormatStr) const;

  /**
   * Get seconds since epoch from UTC time.
   */
  double utcToSecs() const;

  /**
   * Get seconds since epoch from local time. The only fields used from the structure
   * are: year, month, day, hour, minute, second, nanosec;
   */
  double toSeconds() const;

  /**
   * Convert to a readable and very precise string.
   * This is equivalent to calling format(DateTime::vmsFormatStr).
   */
  std::string toString() const;

  /**
   * Convert from the format DateTime::vmsFormatStr to
   * DateTime structure.
   */
  static DateTime fromString(std::string str);

  /**
   * This function will find the next time that rounds to the given
   * interval. For example if the interval given is 60 seconds, and the
   * date given is "2005-8-22 6:48:43", the result will be the date
   * "2005-8-22 6:49:00". If the interval given is 3600 seconds, the
   * corresponding output should be "2005-8-22 7:00:00".
   * The maximum interval allowed is 86400, that corresponds to 24
   * hours, otherwise it will throw a std::bad_exception.
   */
  static DateTime findNextRoundTime(const DateTime& start, double intervalInSecs);

private:
  
  std::string getElem(char fmt) const;

  //
  // ACCESS
  //

public:

  /**
   * Return the structure with the date information filled up.
   */
  const Struct& getStruct() const
  {
    return mStruct;
  }


  //
  // INQUIRY
  //

public:


  //
  // ATTRIBUTES
  //

protected:

  /**
   * Local time structure.
   */
  Struct mStruct;

};


}; // namespace ait

#endif // DateTime_HPP


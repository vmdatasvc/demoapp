/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef String_HPP
#define String_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>
#include <string>

// AIT INCLUDES
//

#include <legacy/types/AitBaseTypes.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

namespace ait 
{

/**
 * Smart pointer to a string.
 */
typedef boost::shared_ptr<std::string> StringPtr;

/**
 * This function can be used to generate a string from a sprinf format expression.
 */
std::string aitSprintf(const char *fmt, ...);

}; // namespace ait

#endif // String_HPP


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef Base64_HPP
#define Base64_HPP

#include <string>
#include <vector>
#include <AitBaseTypes.hpp>

namespace ait 
{

class Base64
{
public:
  /**
   * Encode the given data in Base64 format.
   */
  static std::string encode(const Uint8 *data, int dataLength);
  static std::string encode(const std::string& s);

  static void decode(const char *data, int dataLength, std::vector<Uint8>& out);
  static void decode(const std::string& s, std::vector<Uint8>& out);
  static void decode(const std::string& s, std::string& out);
  static std::string decode(const std::string& s);
};

} // namespace ait

#endif

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/**
 * This file provides mainly functions to work with COM strings and C++.
 * @remark To define just a constant unicode string use L"Example";
 */

#ifndef winstrutil_HPP
#define winstrutil_HPP

#ifdef WIN32

#include <string>
#include <windows.h>
#include <oleauto.h>

namespace ait 
{

/**
 * This function transforms a COM string to an ANSI string.
 */
std::string bstrToString(const BSTR bstr);

/**
 * This function transforms a COM string to an ANSI string. It will
 * reallocate the memory in the provided str object.
 */
void bstrToString(const BSTR bstr, std::string& str);

/**
 * This function creates a BSTR and copies the contents of the given
 * str.
 * @remark The returned BSTR must be freed using SysFreeString().
 * @remark For BSTR returned as [out] parameters from COM interfaces
 * use CoTaskMemFree(bstr) instead.
 */
BSTR stringToBstr(const std::string& str);

} // namespace ait

#endif // WIN32

#endif // winstrutil_hpp
/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef AsyncQueue_HPP
#define AsyncQueue_HPP

// SYSTEM INCLUDES
//

#include <list>
#include <boost/utility.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/condition.hpp>

// AIT INCLUDES
//

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

#include "PvUtil.hpp"

namespace ait 
{

/**
 * This is a thread-safe container for vision messages.
 * @par Responsibilities:
 * - Store messages in a queue.
 * - Control concurrency.
 */
template < class T >
class AsyncQueue : private boost::noncopyable
{
public:

  //
  // LIFETIME
  //

  /**
   * Constructor
   */
  AsyncQueue() {};

  /**
   * Destructor
   */
  virtual ~AsyncQueue() {};

  //
  // OPERATIONS
  //

public:

  /**
   * Put an element in the queue.
   */
  void put(T elem)
  {
    Lock lk(mMutex);
    mElems.push_back(elem);
  }

  /**
   * Get an element from the queue.
   * This template that simplifies syntax and performs type conversion (using RTTI)
   * @return true if an element was obtained, false if the queue is empty.
   */
  bool get(T& elem)
  {
    Lock lk(mMutex);

    bool wasEmpty = mElems.empty();
    if (!wasEmpty)
    {
      elem = *mElems.begin();
      mElems.pop_front();
    }

    return !wasEmpty;
  }


  /**
   * Clear the queue.
   */
  void clear()
  {
    Lock lk(mMutex);
    mElems.clear();
  }

protected:

  //
  // ACCESS
  //

public:


  //
  // INQUIRY
  //

public:

  bool isEmpty()
  {
    Lock lk(mMutex);
    return mElems.empty();
  }

  //
  // ATTRIBUTES
  //

protected:

  typedef std::list<T> QueueElements;
  typedef boost::mutex Mutex;
  typedef boost::mutex::scoped_lock Lock;
  
  /**
   * The message queue list.
   */
  QueueElements mElems;

  /**
   * Synchronization variable.
   */
  mutable Mutex mMutex;

  /**
   * Notification synchronization mutex.
   */
  boost::mutex* mpNotifyMutex;

  /**
   * Notification condition variable.
   */
  boost::condition* mpNotifyCond;

  
};


}; // namespace ait

#endif // AsyncQueue_HPP



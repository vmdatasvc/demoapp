/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef TextCodec_fwd_HPP
#define TextCodec_fwd_HPP


#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>

namespace ait
{

class TextCodec;
typedef boost::shared_ptr<TextCodec> TextCodecPtr;

}; // namespace ait

#endif // TextCodec_fwd_HPP


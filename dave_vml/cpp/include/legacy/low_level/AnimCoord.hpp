/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ANIMCOORD_HPP
#define ANIMCOORD_HPP

#include <ait/Vector3.hpp>

namespace ait
{

template<class T> class AnimCoord3
{
public:

  AnimCoord3() { timeTotal = -1; timeCurrent = 1; }
  
  void setLineSegment(const Vector3<T>& pt0, const Vector3<T>& pt1, 
                      const T time, const T blending = 1) {
    p = p0 = pt0; p1 = pt1; timeTotal = time; 
    timeCurrent = 0; blend = blending; }

  void setLineSegmentTo(const Vector3<T>& pt1, const T time, 
                        const T blending = 1) {
    if (timeTotal <= 0) {
      setLineSegment(pt1,pt1,time,blending);
    } else {
      setLineSegment(p,pt1,time,blending);
    }
  }

  void set(const Vector3<T>& pt) {
    p = p0 = p1 = pt; timeTotal = 1; timeCurrent = 1; blend = false; 
  }

  T update(const T dt) {

    timeCurrent += dt;

    T timeRemain = 0;
    if (timeCurrent > timeTotal) {
      timeRemain = timeCurrent - timeTotal;
      timeCurrent = timeTotal;
    }

    T t = timeCurrent/timeTotal;
    p.set(p0(0)*(1-t)+t*p1(0),
          p0(1)*(1-t)+t*p1(1),
          p0(2)*(1-t)+t*p1(2));

    return timeRemain;
  }

  Vector3<T>& getPos() { return p; }

  // Type conversion operator
  operator Vector3<T>() { return p; }

  T& operator()(const uint i) { return p(i);};
  T operator()(const uint i) const { return p(i);};

protected:

  T timeTotal;
  T timeCurrent;
  T blend;

  Vector3<T> p0;
  Vector3<T> p1;
  Vector3<T> p;

};

typedef AnimCoord3<float> AnimCoord3f;

typedef AnimCoord3<float> AnimCoord2f;

template<class T> class AnimVal
{
public:

  AnimVal() { timeTotal = 1; timeCurrent = 1; }
  
  void setLineSegment(const T pt0, const T pt1, 
                      const T time, const T blending = 1) {
    p = p0 = pt0; p1 = pt1; timeTotal = time; 
    timeCurrent = 0; blend = blending; }

  void setLineSegmentTo(const T pt1, const T time, 
                        const T blending = 1) {
    if (timeTotal <= 0) {
      setLineSegment(pt1,pt1,time,blending);
    } else {
      setLineSegment(p,pt1,time,blending);
    }
  }

  void set(const T pt) {
    p = p0 = p1 = pt; timeTotal = 1; timeCurrent = 1; blend = false; 
  }

  T update(const T dt) {

    timeCurrent += dt;

    T timeRemain = 0;
    if (timeCurrent > timeTotal) {
      timeRemain = timeCurrent - timeTotal;
      timeCurrent = timeTotal;
    }

    T t = timeCurrent/timeTotal;
    p = p0*(1-t)+t*p1;

    return timeRemain;
  }

  operator T() { return p; }

  T getPos() { return p; }

protected:

  T timeTotal;
  T timeCurrent;
  T blend;

  T p0;
  T p1;
  T p;

};

typedef AnimVal<float> AnimValf;

} // namespace ait

#endif

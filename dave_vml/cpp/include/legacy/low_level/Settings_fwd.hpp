/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef Settings_fwd_HPP
#define Settings_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class Settings;
typedef boost::shared_ptr<Settings> SettingsPtr;

}; // namespace ait

#endif // Settings_fwd_HPP


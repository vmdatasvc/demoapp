/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
#ifndef __FileList_HPP__
#define __FileList_HPP__

#pragma warning(disable:4786) 

#include <vector>
#include <string>

namespace ait
{
  
class FileList {
public:

  static std::vector<std::string> getFileNames(const std::string& path, const std::string& pattern = "");
};

};

#endif

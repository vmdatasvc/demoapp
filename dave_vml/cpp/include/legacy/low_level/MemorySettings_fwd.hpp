/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef MemorySettings_fwd_HPP
#define MemorySettings_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class MemorySettings;
typedef boost::shared_ptr<MemorySettings> MemorySettingsPtr;

}; // namespace ait

#endif // MemorySettings_fwd_HPP


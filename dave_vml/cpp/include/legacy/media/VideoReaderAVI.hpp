/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VIDEOREADERAVI_HPP
#define VIDEOREADERAVI_HPP

#include <VideoReader.hpp>

/**
 * This class has been deprecated.
 * @deprecated Use VideoReader instead.
 */
class VideoReaderAVI
{
public:

  VideoReaderAVI();
  ~VideoReaderAVI();

  bool init(unsigned int &w,unsigned int &h, const char *fname);
  bool readFrame(ait::Image32& frame, bool BGRMode = true);  // BGR mode is slower than RGB
                                                           // mode, but data is stored
														   // correctly, allowing savePNG, 
														   // etc to output correct images.
														   // If BGRMode == false, RGB mode
                                                           // is used.
  void setCurrentFrame(long fNum);
  void setLoopMode(bool flag=true);
  bool isFirstFrame(void);
  float getFrameRate();
  int getCurrentFrameNumber(void);
  void finish();
  int getNumFrames();

private:

  ait::VideoReaderPtr mpVideoReader;

  bool mIsLooping;
};

#endif
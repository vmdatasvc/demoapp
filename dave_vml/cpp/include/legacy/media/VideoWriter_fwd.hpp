/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VideoWriter_fwd_HPP
#define VideoWriter_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class VideoWriter;
typedef boost::shared_ptr<VideoWriter> VideoWriterPtr;

}; // namespace ait

#endif // VideoWriter_fwd_HPP


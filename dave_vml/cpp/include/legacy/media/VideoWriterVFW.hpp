/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VideoWriterVFW_HPP
#define VideoWriterVFW_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <windows.h>
#include <memory.h>
#include <mmsystem.h>
#include <vfw.h>

// AIT INCLUDES
//

#include <String.hpp>


// LOCAL INCLUDES
//

#include "VideoWriter.hpp"

// FORWARD REFERENCES
//

#include "VideoWriterVFW_fwd.hpp"

namespace ait 
{


/**
 * Video For Windows implementation of the video writer.
 * See the base class for documentation. Some codecs
 * have better implementations for compressors using this
 * interface. (i.e: XviD).
 */
class VideoWriterVFW : public VideoWriter
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  VideoWriterVFW();


  /**
   * Destructor
   */
  virtual ~VideoWriterVFW();

  //
  // OPERATIONS
  //

public:

  virtual void open(const std::string& fname);
  virtual void writeFrame(const Image32& img);
  virtual void skipFrame();
  virtual void close();
  bool loadCodecSettings(const std::string& fname, bool showDialogIfNotPresent) { return true;};
  void saveCodecSettings(const std::string& fname) {};
  bool showCodecSettingsDialog(const std::string& fname) { return true; };
  bool hasValidCodec() { return true; };
  void setOwnerWindow(Uint32 id) {};

private:

  void init(int w, int h, const char *name);

  //
  // ACCESS
  //

public:

  //
  // INQUIRY
  //

public:

  //
  // ATTRIBUTES
  //

protected:

  /**
   * File name.
   */
  std::string mFileName;

  // These are copied from the old VideoWriterAVI.
	PAVIFILE pfile;
	PAVISTREAM ps, psCompressed, psText;
  int frameCount;
  bool firstFrame;

};


}; // namespace ait

#endif // VideoWriterVFW_HPP


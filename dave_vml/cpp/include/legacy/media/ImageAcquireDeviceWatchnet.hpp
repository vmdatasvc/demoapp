/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ImageAcquireDeviceWatchnet_HPP
#define ImageAcquireDeviceWatchnet_HPP

#include "ImageAcquireDeviceBase.hpp"

class CNetSDK;
class CWnd;

namespace ait
{

class ImageAcquireDeviceWatchnet: public ImageAcquireDeviceBase
{
public:
		
	///Constructor.
	ImageAcquireDeviceWatchnet(void);
	
	//Destructor
	virtual ~ImageAcquireDeviceWatchnet(void);

  void setWatchnetProperty(ImageAcquireProperty& param, bool set);

  virtual int getTotalNumberOfFrames();

  virtual int getSkipFrameInterval() {return mSkipFrameInterval;}

  void callback(int w, int h, Uint8* pData);

protected:

  float mFps;

  /**
   * This variable allows to run the video skipping frames, therefore reducing the
   * frame rate. The advantage is that video can be processed faster using
   * this. The skip value will not affect the specified frame rate in the
   * registry. It is the responsibility of the caller to set the adequate frame
   * rate for the given skip frame interval.
   */
  int mSkipFrameInterval;

	bool initialize(int numPrevBuffers = 0, int numConsecutiveFrameGrabs = 1);
	bool unInitialize();
	int grabFrame();

private:

  CNetSDK *mpNetSdk;
	CWnd *mpCWnd;

  Image32Ptr mpFrame;
};

} // namespace ait

#endif



/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ImageAcquireDeviceAVI_HPP
#define ImageAcquireDeviceAVI_HPP

#include "legacy/media/ImageAcquireDeviceBase.hpp"

#include "legacy/low_level/Settings.hpp"

// opencv includes
#include "opencv2/opencv.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/video/video.hpp"

namespace ait
{

typedef	boost::shared_ptr<cv::VideoCapture> VideoCapturePtr;


class ImageAcquireDeviceOpenCV: public ImageAcquireDeviceBase
{

public:
		
	///Constructor.
	ImageAcquireDeviceOpenCV(void);
	
	//Destructor
	virtual ~ImageAcquireDeviceOpenCV(void);

  void setOpenCVProperty(ImageAcquireProperty& param, bool set);

  virtual int getTotalNumberOfFrames();

  virtual int getSkipFrameInterval() {return mSkipFrameInterval;}


protected:

  VideoCapturePtr mpCapture;

  PVTIME nextFrameTime;

  /**
   * The total number of frames when adding all segments.
   */
  long mTotalFrames;

  /**
   * The number of the current frame in the current video
   * file segment with respect to the beginning of the
   * video file.
   */
  long mCurrentFrame;

  float mFps;

  /**
   * This flag will either run the device at the specified frame rate in the
   * property bags, or as fast as possible.
   */
  bool mRunAtFrameRate;

  /**
   * This variable allows to run the video skiping frames, therefore reducing the
   * frame rate. The advantage is that video can be processed faster using
   * this. The skip value will not affect the specified frame rate in the
   * registry. It is the responsibility of the caller to set the adequate frame
   * rate for the given skip frame interval.
   */
  int mSkipFrameInterval;

  Settings 		mSettings;
  std::string	mVideoFileName;

public:

    void setVideoProperty(Settings &set, std::string source);

	bool initialize(int numPrevBuffers = 0, int numConsecutiveFrameGrabs = 1);
	bool unInitialize();
	int grabFrame();

	bool isFirstFrame();
	void gotoFirstFrame();

//  void loadVideoFiles(std::string fname);

private:

	
	
};

} // namespace ait

#endif



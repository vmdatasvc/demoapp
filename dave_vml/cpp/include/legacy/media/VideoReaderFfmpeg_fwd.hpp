/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VideoReaderFfMpeg_fwd_HPP
#define VideoReaderFfMpeg_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class VideoReaderFfMpeg;
typedef boost::shared_ptr<VideoReaderFfMpeg> VideoReaderFfMpegPtr;

}; // namespace ait

#endif // VideoReaderFfMpeg_fwd_HPP


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VideoWriterFfmpeg_HPP
#define VideoWriterFfmpeg_HPP

// SYSTEM INCLUDES
//

#define EMULATE_INTTYPES
#include <avformat.h>

// AIT INCLUDES
//

#include <string.hpp>


// LOCAL INCLUDES
//

#include "VideoWriter.hpp"

// FORWARD REFERENCES
//

#include "VideoWriterFfmpeg_fwd.hpp"

namespace ait 
{


/**
 * Implementation using the ffmpeg library.
 */
class VideoWriterFfmpeg : public VideoWriter
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  VideoWriterFfmpeg();


  /**
   * Destructor
   */
  virtual ~VideoWriterFfmpeg();

  //
  // OPERATIONS
  //

public:

  virtual void open(const std::string& fname);
  virtual void writeFrame(const Image32& img);
  virtual void skipFrame();
  virtual void close();
  virtual bool loadCodecSettings(const std::string& fname, bool showDialogIfNotPresent);
  virtual void saveCodecSettings(const std::string& fname);
  virtual bool showCodecSettingsDialog(const std::string& fname);
  virtual bool hasValidCodec();
  virtual void setOwnerWindow(Uint32 id);
  virtual void setAverageFrameDuration(double avgDuration);

private:

  bool isCodecInitialized() { return oc != 0; }
  void writeFirstFrame(const Image32& img);
  AVStream *add_video_stream(AVFormatContext *oc, int codec_id, const Image32& img);
  AVFrame *alloc_picture(int pix_fmt, int width, int height);
  void open_video(AVFormatContext *oc, AVStream *st);
  void write_video_frame(AVFormatContext *oc, AVStream *st, const Image32& img);

  //
  // ATTRIBUTES
  //

protected:

  /**
   * File name.
   */
  std::string mFileName;

  AVOutputFormat *fmt;
  AVFormatContext *oc;
  AVStream *video_st;
  int video_outbuf_size;
  Uint8 *video_outbuf;
  AVFrame *picture;
};


}; // namespace ait

#endif // VideoWriterFfmpeg_HPP


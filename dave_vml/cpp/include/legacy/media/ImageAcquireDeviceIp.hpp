/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ImageAcquireDeviceIp_HPP
#define ImageAcquireDeviceIp_HPP

#include "ImageAcquireDeviceBase.hpp"
#include <ImageAcquireDeviceDirectShow.hpp>
#include <CameraDriver_fwd.hpp>
#include <ICameraDriverEvents.hpp>

namespace ait
{

/**
 * Implementation of IP camera interfaces.
 * The current implementation only supports AXIS cameras.
 */
class ImageAcquireDeviceIp: public ImageAcquireDeviceBase, ICameraDriverEvents
{
public:
		
	///Constructor.
	ImageAcquireDeviceIp(void);
	
	//Destructor
	virtual ~ImageAcquireDeviceIp(void);

protected:

	bool initialize(int numPrevBuffers = 0, int numConsecutiveFrameGrabs = 1);
	bool unInitialize();
	int grabFrame();

	bool isFirstFrame();
	void gotoFirstFrame();

  virtual void onVideoFrame(int cameraId, double startTimeStamp, double endTimeStamp,
    unsigned char *data, int dataSize);

  /**
   * Thread safe error message handler.
   */
  void setLastErrorMessage(const std::string& str);

  /**
   * Thread safe error message access.
   */
  std::string getLastErrorMessage();

private:
  void tryInitDriver();

protected:

  /**
   * Pointer to the actual camera driver.
   */
  CameraDriverPtr mpCameraDriver;

  /**
   * The next frame to be written to. Incoming frames are written here.
   */
  MediaFrameInfoPtr mpNextFrame;

  /**
   * This mutex is used to protect the image frame memory access
   */
  boost::mutex mFrameReadyMutex;

  /**
   * When grabFrame() is called, threads will wait on this condition before 
   * returning. When a frame is received, this condition will be signaled.
   */
  boost::condition mIsFrameReady;

  /**
   * Save the Frames per second for easy access.
   */
  double mFps;

  /**
   * Last error message.
   */
  std::string mLastErrorMessage;

  /**
   * Next time to try to start the driver again if there was an error.
   */
  double mNextTryStartTime;

  /**
   * Time stamp of the last valid frame.
   */
  double mLastValidFrameTime, mLastFrameTime;

};

} // namespace ait

#endif



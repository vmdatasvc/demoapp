/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VideoReader_HPP
#define VideoReader_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <vector>

// AIT INCLUDES
//

#include <String.hpp>
#include <ait/Image.hpp>
#include <ait/Image_fwd.hpp>
#include <ait/Vector3.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

#include "VideoReader_fwd.hpp"

namespace ait 
{


/**
 * Base class for simple video reading.
 * @par Example:
 * This example extracts frames 500 to 600 to JPGs.
 * @code 
 *   VideoReaderPtr pVr(VideoReader::create());
 *   pVr->open("test.avi");
 *   pVr->setCurrentFrameNumber(500);
 *   Image32 img;
 *   for (int i = 0; i < 100; i++)
 *   {
 *     pVr->readFrame(img);
 *     img.save(aitSprintf("testing_%03i.jpg",i).c_str());
 *     pVr->nextFrame();
 *   }
 * @endcode
 */
class VideoReader : private boost::noncopyable
{
  //
  // LIFETIME
  //
  
protected:

  /**
   * Constructor.
   */
  VideoReader();

public:
  /**
   * Destructor. The destructor will close the file if it is open.
   */
  virtual ~VideoReader();

  /**
   * Factory pattern to build the concrete video writer.
   */
  static VideoReader *create();

  /**
   * Factory pattern to build the concrete video writer. Specify the media
   * engine used to read the file. Values can be 'IPP' or 'DirectShow'.
   */
  static VideoReader *create(const std::string& mediaEngineId);

  //
  // OPERATIONS
  //

public:

  /**
   * Open a file for reading.
   * @remark Throws std::exception if there are problems opening the file.
   */
  virtual void open(const std::string& fname) = 0;

  /**
   * Close the current file.
   */
  virtual void close() = 0;

  /**
   * Get the current frame from the file. It will read the frame indicated
   * by getCurrentFrameNumber(). It will not increment automatically the
   * frame number. You can use nextFrame() to do this.
   * @return true if a frame was read false if it is one frame past the last
   * frame, in this case no frame was read.
   */
  virtual bool readFrame(Image32& img) = 0;

  /**
   * Delete the frame index for this file (if present)
   */
  virtual void deleteIndex(const std::string& fname) {};


  //
  // ACCESS
  //

public:

  /**
   * Return the number of frames in the video.
   */
  virtual Int64 getNumberOfFrames() = 0;

  /**
   * Set the current frame.
   * @throws std::exception if the frame number is invalid.
   */
  virtual void setCurrentFrameNumber(Int64 frame) = 0;

  /**
   * Get the current frame number. The first frame is frame 0.
   * getCurrentFrameNumber() == getNumberOfFrames() indicates the
   * end of the video.
   */
  virtual Int64 getCurrentFrameNumber() = 0;

  /**
   * Get the number of frames per second on this video.
   */
  virtual double getFramesPerSecond() = 0;

  /**
   * Get the size of the video buffer. Only valid after opening the file.
   */
  virtual Vector2i getBufferSize() = 0;

  /**
   * Move to the next frame. If it is in the last frame, it will
   * stay there.
   */
  virtual void nextFrame() = 0;

  /**
   * Get the array of I-Frames for the current video. The array is
   * zero based.
   */
  virtual void getIFrames(std::vector<Int64>& iFrames)
  {
    iFrames.clear();
    iFrames.push_back(0);
  }

  /**
   * Get the progress from the I-Frame scan. This is thread safe.
   */
  virtual double getScanProgress() { return 1.0; }

  /**
   * Abort any lengthy operation. This is thread safe.
   * After calling this the reader must be closed.
   * Currently it only suspends I-Frame scanning.
   */
  virtual void abort() {}

  //
  // INQUIRY
  //

public:

  /**
   * Returns true if the video is over, that is getCurrentFrameNumber()
   * == getNumberOfFrames().
   */
  virtual bool isEndOfFile();

  //
  // ATTRIBUTES
  //

};


}; // namespace ait

#endif // VideoReader_HPP


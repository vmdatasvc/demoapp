/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ImageAcquireDeviceDirectShow_HPP
#define ImageAcquireDeviceDirectShow_HPP

#include "ImageAcquireDeviceBase.hpp"
#include "ImageAcquireProperty.hpp"
#include "Media.hpp"
#include "directshow/MediaCapture.hpp"
#include <VideoReaderAVI.hpp>

namespace ait
{

class ImageAcquireDeviceDirectShow: public ImageAcquireDeviceBase
{
public:
		
	///Constructor.
	ImageAcquireDeviceDirectShow(void);
	
	//Destructor
	virtual ~ImageAcquireDeviceDirectShow(void);

  /**
   * Low-level Synchronization.
   */
  virtual void setSyncMaster(ImageAcquireDeviceBasePtr pControllingDevice);

protected:

	MediaCapture captureDevice; ///< Media DirectShow object.

	bool isDirectShowStarted;        ///<True if started.
	int lastFrame;            ///<Which frame are we on.

	bool initialize(int numPrevBuffers = 0, int numConsecutiveFrameGrabs = 1);
	bool unInitialize();
	int grabFrame();

//	Image32*& getReferenceToFrame(int frameIndex);

	bool isFirstFrame();
	void gotoFirstFrame();

  bool mTestMode;

  /**
   * This image is used for testing. The whole module captures an
   * image, but in the end, this is the one copyed to the frame buffer.
   */
  Image32 mTestFrame;

  /**
   * This flag indicates if this device must be syncronized.
   */
  bool mIsSynchronized;
	

};

} // namespace ait

#endif



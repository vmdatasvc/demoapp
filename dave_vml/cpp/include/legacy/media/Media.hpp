/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef MEDIA_HPP
#define MEDIA_HPP

#ifdef WIN32
//#include <windows.h>
#include <mmsystem.h>
#include <dsound.h>
#include <dshow.h>
#include <commctrl.h>
#include <commdlg.h>
//#include <tchar.h>
#include <streams.h>
#endif
#include <boost/thread/mutex.hpp>

#include <directshow/MediaFilterBase.hpp>
#include <EventCallback.hpp>
#include <MediaFrameInfo.hpp>

#include <boost/shared_ptr.hpp>

#define WM_GRAPHNOTIFY  WM_USER+13

//
// Constants
//
#define VOLUME_FULL     0L
#define VOLUME_SILENCE  -10000L

namespace ait
{

/**
 * This class performs the state control for streaming media. Several
 * media instances can share one media controller. This will provide better
 * synchronization between camera captures. This is a base class. If instantiated,
 * it will not execute anything when called. This is useful for media classes
 * that are controlled externally.
 */
class MediaController
{
public:
  virtual ~MediaController() {}
  virtual void stop() {};
  virtual void play() {};
  virtual void pause() {};
};

typedef boost::shared_ptr<MediaController> MediaControllerPtr;

#ifdef WIN32
/**
 * This is the DirectX implementation for the media controller class.
 */
class MediaControllerDX : public MediaController
{
public:
  MediaControllerDX() :
      pGB(NULL),
      pCGB(NULL),
      pMC(NULL) {}

  virtual void stop()
  {
    pMC->Stop();
   OAFilterState fs;
   // Wait for the state to change.
   pMC->GetState(1000,&fs);
  }
  virtual void play()
  {
    pMC->Run();
   OAFilterState fs;
   // Wait for the state to change.
   pMC->GetState(1000,&fs);
  }
  virtual void pause()
  {
    pMC->Pause();
   OAFilterState fs;
   // Wait for the state to change.
   pMC->GetState(1000,&fs);
  }

public:
  IGraphBuilder  *pGB;              ///< DirectShow interfaces
  ICaptureGraphBuilder2 *pCGB;
  IMediaControl *pMC;
};
#endif

class Media
{
public:
  Media();
  ~Media();

  /**
   * Types of buffering and synchronization.
   */
  typedef enum
  {
    /**
     * The buffer is provided by the application, and its thread will
     * stop to wait for every frame. There is less latency since
     * frames are not copied first in buffers. This method is used
     * for interactive systems.
     */
    BLOCKING_SINGLE,

    /**
     * The filter keeps two buffers they will queue to be copied. With this
     * method less frames are lost.
     */
    BLOCKING_DOUBLE,

		/**
		* The filter keeps two buffers they will queue to be copied. 
		* With this method and a single grabFrame() two guarantied 
		* consecutive frames will be stored in the applications circular buffer.
		* With this method less frames are lost and the latency to wait for 
		* the second frame is reduced to zero if the application does need 
		* longer then one frame to call grabFrame(),
		* then if BLOCKING_DOUBLE method is used with two consecutive calls to grabFrame().
		*/
		BLOCKING_TWO_CONSECUTIVE_FRAMES,

    /**
     * There is one buffer kept by the filter, when getFrame() is called,
     * it will copy that buffer if the frame has changed, otherwise it
     * will return immediately. This method is used to play videos
     * from higher frame rate applications.
     */
    NON_BLOCKING_SINGLE

  } BufferType;

  /**
   * Set the frame rate at which media must be played.
   */
  void setFrameRate(double frameRate);

  void closeInterfaces(void);

  /**
   * Load the video and set the current frame to zero. It doesn't start playing the video.
   * If the sync flag is set, the application will wait for new frames on a call to getFrame().
   * @see getFrame().
   */
  void load(const std::string& fname, BufferType bufferType = BLOCKING_SINGLE);

  /// Play the video from the current frame
  void play(void);

  /// Play the video from the beginning of the video
  void reset(void);

  /// Returns true if the video is playing
  bool isPlaying();

  /// Pause the current video.
  void pause(void);

  /// Stop the video. Do not reset.
  void stop(void);

  /// Media update
  void update(double dt);
  
  /// Return the size in pixels of the frame buffer.
  const Vector2i getSize() const;

  /**
   * Get the current frame. If the video was loaded as blocking, this
   * function will wait until a new frame is obtained. If it is non-blocking,
   * it will return immediately if there is not a new frame.
   * @param pMediaFrameInfo [in] Frame information will be returned in this
   * structure. If the media was declared as non-blocking, and the pRGBImage
   * is set to NULL, it will modify the pointer to point to the internal buffer.
   * The calling thread is responsible for blocking the image using the
   * mutex returned by getMutex().
   * If pRGBImage is not null, and it is non-blocking, it will copy the contents
   * of the image to the provided instance.
   * @return true if the frame was changed.
   * @remark caller must allocate the media frame information.
   */
  bool getFrame(MediaFrameInfoPtr pMediaFrameInfo);

  /**
   * If the buffer mode is BLOCKING_DOUBLE, the second parameter must be used
   * to indicate the address to store the next buffer. Note that after
   * calling this function, the address of pMediaNextFrameInfo will belong 
   * to the rendering filter thread. The memory must provided must not be modified by
   * the main thread. This function should be called Use this carefully.
   */
  bool pushReadBuffer(MediaFrameInfoPtr pNextFrameInfo);

  /**
   * Return a mutex for the frame. This should be used only in non-blocking media
   * when passing an empty buffer to getFrame() in the pRGBImage field.
   * @see getFrame().
   */
  boost::mutex& getMutex();

  /// Set the volume of the sound. 0 = min. 1 = max.
  void setVolume(float volume);

  /// Set the loop flag
  void setLooped(bool loop) { loopFlag = loop; };

  /// Get the loop flag
  bool isLooped() { return loopFlag; };

  /// Get the time remaining of the clip
  double getTimeRemaining();

  /// Time to loop
  bool isLastFrame();
  bool isLastSoundFrame();

  /**
   * This function indicates if this media is controlled by other media
   * object.
   * See setExternalControl()
   */
  bool useExternalControl();

  /**
   * Return a smart pointer to the controller of this class.
   */
  MediaControllerPtr getMediaController()
  {
    return mpMediaController;
  }

  /**
   * Set another media object as the controlling object. As a result
   * the calls to run(), pause() and play() will change the status 
   * flags, but it will not call the respective filter flags.
   */
  void setExternalControl(MediaControllerPtr pController);

  /// restart sound file
  void resetSound();

  /// Set the notifyEvent
  void setNotifyEvent(EventCallback *pEC, int eventType, void *pObject);

protected:

  /**
   * Initialize the renderer filter.
   */
  void initRenderer(BufferType bt);

  ait::MediaFilterBase *mpRenderer;           ///< DShow Texture renderer
  int curFrame;

  /// Pointer to the EventCallback object to notify events.
  EventCallback *pEventCallback;

  /// Type of the event that will be sent to the EventCallback
  int callbackEventType;

  /// Object that will be sent to the eventCallback.
  void *pCallbackObject;

protected:

  enum { VOLUME_LOOKUP_TABLE_SIZE = 100 };  ///< Contains the Size of the Maximum Volume Array


  enum PLAYSTATE {Stopped, Paused, Running, Init}; ///< Current state of video stream
  void createVolumeLookupTable();   ///<Creates the Volume LookupTable to get the linear Volume from Logarithimic Volume. 

#ifdef WIN32
  BOOL      g_bAudioOnly;           ///< audio only flag
#else
  bool      g_bAudioOnly;
#endif

  LONG      g_lVolume;              ///< volume of audio
  PLAYSTATE g_psCurrent;            ///< current state of video stream

#ifdef WIN32
  // DirectShow interfaces

  IMediaEventEx  *pME;
  IBasicAudio    *pBA;
  IBasicVideo    *pBV;
  IMediaSeeking  *pMS;
  IMediaPosition *pMP;
#endif

  ///Storage space for Volume LookupTable.
  static float volumeLookupTable[VOLUME_LOOKUP_TABLE_SIZE];

  bool loopFlag;                    ///< Flag determines whether or not to loop video

  /**
   * Flag to use external control.
   */
  bool mUseExternalControl;

  /**
   * Pointer to the controller class.
   */
  MediaControllerPtr mpMediaController;

  /**
   * Type of buffering.
   */
  BufferType mBufferType;

};

} // namespace ait

#endif


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VIDEOREADERAVIOPENCV_HPP
#define VIDEOREADERAVIOPENCV_HPP

#include <boost/shared_ptr.hpp>

//#include <VideoReader.hpp>
// opencv includes
#include "opencv2/opencv.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/video/video.hpp"

#include <legacy/types/String.hpp>
#include <legacy/pv/ait/Image.hpp>
#include <legacy/pv/ait/Image_fwd.hpp>
#include <legacy/pv/ait/Vector3.hpp>

/**
 * Video reader class using opencv video
 */
class VideoReaderAVIOpenCV
{
public:

  VideoReaderAVIOpenCV();
  ~VideoReaderAVIOpenCV();

  bool init(unsigned int &w,unsigned int &h, const char *fname);
  bool readFrame(ait::Image32& frame, bool BGRMode = true);  // BGR mode is slower than RGB
                                                           // mode, but data is stored
														   // correctly, allowing savePNG, 
														   // etc to output correct images.
														   // If BGRMode == false, RGB mode
                                                           // is used.
  void setCurrentFrame(long fNum);
  void setLoopMode(bool flag=true);
  bool isFirstFrame(void);
  float getFrameRate();
  int getCurrentFrameNumber(void);
  void finish();
  int getNumFrames();

private:

typedef boost::shared_ptr<cv::VideoCapture> VideoReaderOpenCVPtr;

  //  ait::VideoReaderPtr mpVideoReader;
  VideoReaderOpenCVPtr mpVideoReader;

  bool mIsLooping;
};

#endif

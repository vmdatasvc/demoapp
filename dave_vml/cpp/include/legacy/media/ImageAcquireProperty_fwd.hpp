/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef IMAGEACQUIREPROPERTY_FWD_HPP
#define IMAGEACQUIREPROPERTY_FWD_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{
class ImageAcquireProperty;
typedef boost::shared_ptr<ImageAcquireProperty> ImageAcquirePropertyPtr;
class ImageAcquirePropertyBag;
}

#endif //IMAGEACQUIREPROPERTY_FWD_HPP

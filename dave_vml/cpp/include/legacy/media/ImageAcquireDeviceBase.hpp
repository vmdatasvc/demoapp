/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#pragma warning(disable:4786)

#ifndef ImageAcquireDeviceBase_HPP
#define ImageAcquireDeviceBase_HPP

#ifdef WIN32
#include <windows.h>
#endif

//#include "ImageAcquireDeviceInfo.hpp"
#include "legacy/media/ImageAcquireProperty.hpp"
#include <legacy/pv/ait/Image.hpp>
#include <legacy/pv/ait/Image_fwd.hpp>
#include "MediaFrameInfo.hpp"
#include <legacy/pv/CircularBuffer.hpp>

#include <string>
#include <vector>

#include "legacy/media/ImageAcquireDeviceBase_fwd.hpp"


namespace ait
{

  class ImageAcquireDeviceReference;

class ImageAcquireDeviceObserver
{
public:
  /**
   * This method will be called if defined.
   */
  virtual void close(ImageAcquireDeviceBase* pDevice) = 0;
};

class ImageAcquireDeviceBase
{
  friend class ImageAcquireDeviceReference;

public:

  ///Constructor.
  ImageAcquireDeviceBase();
  
  ///Destructor
  virtual ~ImageAcquireDeviceBase();

  /**
   * Synchronize two device at the lowest possible level. This device 
   * will become the slave, and the provided device will be the master. 
   * This can be used to synchronize cameras for interactive systems.
   * @remark this call must be made before initialization, and the
   * given device must be initialized. Currently only DIRECTSHOW devices
   * can be synchronized.
   */
  virtual void setSyncMaster(ImageAcquireDeviceBasePtr pControllingDevice) {};
  
  /** 
      Specific initialization for specified device.
  
      @param numPrevBuffers [in] Number of previous buffers to use.
    
      @return 	true false;  succeed , fail.
      @remark  If reseting device call unInitialize() first.
      @see unInitialize()
  */
  virtual bool initialize(int numPrevBuffers = 0, int numConsecutiveFrameGrabs = 1) = 0;

  /** 
      Specific de-initialization for specified device.
  
      @return 	true false;  succeed , fail.
      @remark  If reseting device call initialize() next.
      @see initialize()
  */
  virtual bool unInitialize() = 0;

  /** 
      This function calls device specific grab versions.
      The 0 position will always hold the most current frame.
      After a call to grabFrame 0 will be hold the most current grab.
      If specified 1 = prev, 2 = prev+, 3 = prev++, etc...
      No copying is done.  Movement of pointers only.
      If supported by driver, initiation of next grab is begun before function
      exits.
    
      @return 	4 = DEVICE_SPECIFIC_GRAB_FAILED, 1 = Last frame on a video.
      @remark  Call this to start the grab.  Use getReferenceToFrame, getReferenceToFrame to get references
      to the frame.  Note:  Frame is stored in grab buffers, therefore be quick when using this memory.  Buffers will be over written.
      @see getPointerToFrame() getReferenceToFrame()	
  */
  virtual int grabFrame() = 0;

  /**
   * Get the total number of frames in the video file. If this
   * is a camera, the return value will be zero.
   */
  virtual int getTotalNumberOfFrames() { return 0; }

  /** 
      Gets a pointer to the specified device's indicated frame.
      Call grabFrame() first.
      
      @param frameIndex [in] Index of frame.  0 = current,  1 = previous,  2 = previous+,  3 = etc..
      @return 	Pointer to Frame.
      @remark   Note:  Frame is stored in grab buffers, therefore be quick when using this memory.  Buffers will be over written.
      @see grabFrame()
  */
  virtual Image32* getPointerToFrame(int frameIndex);
  

  /** 
      Gets a reference to a frame.
      Call grabFrame() first.  Static position identifier.
  
      @param frameIndex [in] Index of frame.  0 = current,  1 = previous,  2 = previous+,  3 = etc..
      @return 	Pointer to Frame.
      @remark   Note:  Frame is stored in grab buffers, therefore be quick when using this memory.  Buffers will be over written.
      @see grabFrame()
  */
  virtual const Image32& getReferenceToFrame(int frameIndex);
 
  /**
   * Return the shared pointer to the frame information.
   */
  virtual MediaFrameInfoPtr getFrameInfo(int frameIndex);

  /** 
      Displays specified devices frame.  Keep calling for each grab.
    
      @param dispFrame [in] Flag = true paint window on.  false = off.
      @return Return 0 if ok.
  */
  virtual int displayFrame(bool dispFrame=false);

  /**
   * Returns the skip frame interval. The base class currently returns 1
   * (no frames are skipped). Image acquire subclasses that allow for the
   * skipping of frames (such as ImageAcquireDeviceAVI) should overwrite
   * this member function
   */
  virtual int getSkipFrameInterval() {return 1;}

	/**
	* Returns the the number of frames that are grabbed consecutively in one go
	*/
	virtual int getConsecutiveFrameNum() {return mNumConsecutiveFrameGrabs;}

  /**   
	Add device info.
	@param deviceInfo_i [in] device info object
  */
//  void setDeviceInfo(ImageAcquireDeviceInfoPtr deviceInfo_i)
//  { deviceInfo = deviceInfo_i; };

  unsigned int getFrameCounter(){return frameCounter;};

  //Dimensions.
  /** 
      Returns the specified device's current capture width setting.
      @return  Width in pixels.
  */
  unsigned int width() { return frameWidth; };
   
  /** 
      Returns the specified device's current capture height setting.
      @return  height in pixels.
  */
  unsigned int height() { return frameHeight; };  

  /// Set an alternative visualization image.
  void setAlternativeVisualizationFrame(Image32Ptr pNewFrame) { pVisualizationFrame = pNewFrame; }

  /// return the required property for this device.
  virtual ImageAcquirePropertyPtr getProperty(ImageAcquireProperty::Code p)
  {
    PVASSERT(pParams && p != ImageAcquireProperty::INVALID_CODE);
    return pParams->get(p);
  }

  /**
   * Use this function to set or change the visualization window size and
   * the visualization buffer size.
   */
  void setVisualizationFrameSize(const Vector2i& size);

  /**
   * Set global observer. This static function will assign a global observer to
   * receive the close window event. This function is not thread safe and 
   * should be called before creating any instance of this class.
   */
  static void setObserver(ImageAcquireDeviceObserverPtr pObserver);


  /**
   * Returns true if the device has been initialized successfully	
   */
  bool isInit() {return isInitialized;}

protected:

  /**
   * Initialize the circular buffer. It also allocates one more frame
   * for asynchronous frame reading.
   */
  virtual void initBuffers(int frameWidth, int frameHeight, int numPrevBuffers);

  /**
   * Capture some frames to fill the buffers with data.
   */
  virtual void fillBuffers();

  /**
   * Advance to the next frame. This moves the circular buffer.
   */
  virtual void nextFrame();

  /**
   * Observer pointer.
   */
  static ImageAcquireDeviceObserverPtr mpObserver;

protected:
  
  //Dynamic parameters.
  int numPreviousBuffers;
  
  int frameWidth, frameHeight;///<Pixel height and width of frame grabs.

  bool isInitialized;  ///<Is this class initialized properly.
  
  /// Device information for this class.
//  ImageAcquireDeviceInfoPtr deviceInfo;

  /// Device Parameters. @todo this should replace the deviceInfo class.
  ImageAcquirePropertyBag *pParams;
  
  bool displayFrameFlag;  ///<Flag for display on/off
  
  /**
   * This buffer will keep the current and previous frames. The
   * current frame is mFrames.front(), and the previous frames are 
   * mFrames[1], mFrames[2], etc. 
   * New frames are inserted in the front. The next frame in the
   * future is in mFrames.back(). This is used by the device if it can
   * read asynchronously. This frame should not be accessed by applications.
   */
  CircularBuffer<MediaFrameInfoPtr> mFrames;

  /// Alternative display frame
  Image32Ptr pVisualizationFrame;
  
  /// Frame counter
  unsigned int frameCounter;
  double paintDelay;
  double lastPaintTime;

  /// Interactive Adjustment (read from Settings)
  bool interactive;
  void processInteractive();
  void launchInteractive();

public:
  void setInteractive(bool flag);

  //
  // Visualization Functions (System dependent)
  //

protected:
  /**
   * Size of the visualization window client area in pixels.
   */
  Vector2i mVisualizationFrameSize;

  /**
   * Size of the visualization window client area.
   */
  Vector2i mWindowSize;

  int paint();
  void paintVideoFrame(Vector2i pos, Vector2i windowSz, const Image32 &image);

  unsigned char *pBitmapData;

	int mNumConsecutiveFrameGrabs;

    bool mSaveNextFrame;

#ifdef WIN32
  HINSTANCE hInstance1;
  HWND hwnd1;
  HBITMAP hBitmap;
  HDC hBitmapDC;
  HDC hDC;

  /**
   * Windows must be destroyed from the same thread that was used to create
   * them. This flag indicates the owner thread to destroy the window.
   */
  bool mDestroyWindowFlag;
#endif

private:
#ifdef WIN32  
  void createWindow(char* title, const Rectanglei& windowRect, 
    const Vector2i& bufferSize, bool hasBorder, HWND parentHwnd);
  void destroyWindow();
  static LRESULT CALLBACK windowProc(HWND hwnd,UINT uMsg,WPARAM wParam,LPARAM lParam);
  LRESULT windowProc2(HWND hwnd,UINT uMsg, WPARAM wParam,LPARAM lParam);
#endif
	
  void paint(Image32& vidFrame);

};

} // namespace ait

#endif

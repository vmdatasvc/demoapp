/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ImageAcquireDeviceReference_HPP
#define ImageAcquireDeviceReference_HPP

#include "ImageAcquireDeviceBase.hpp"

#include <string>
#include <ait/Image.hpp> //added for loading default ppm image

namespace ait
{

/**
 * This class acts as a read-only device intended to reference
 * another ImageAcquireDeviceBase type device. The purpose of this
 * class is to remove the unecessary overhead that would result
 * from initializing two distinct devices to process the same video,
 * usbcam, etc.
 */
  
class ImageAcquireDeviceReference : public ImageAcquireDeviceBase
{
public:
		
	//Constructor.
  ImageAcquireDeviceReference(ImageAcquireDeviceBasePtr device) : mpReferenceDevice(device), ImageAcquireDeviceBase()
  {
    PVASSERT(device.get());
  }
	
	//Destructor
	virtual ~ImageAcquireDeviceReference(void)
  {   
    unInitialize();
  }

protected:

	bool initialize(int numPrevBuffers = 0, int numConsecutiveFrameGrabs = 1)
  {    
    PVASSERT(mpReferenceDevice.get());
    PVASSERT(mpReferenceDevice->isInit());
    
    assert(numPrevBuffers >= 0);

	  if (isInitialized)
		  unInitialize();


    lastPaintTime = PvUtil::time();
    paintDelay = 0; //delay paints in secs.

	  //Initialize the display window.
	  displayFrameFlag = false;

	  //Initialize base parameters.
	  numPreviousBuffers = numPrevBuffers;

		//Initialize the number of consecutive frames to grab
		mNumConsecutiveFrameGrabs = numConsecutiveFrameGrabs;

    //lastFrame = -1;

	  //Initialize the resolution.
	  frameWidth = mpReferenceDevice->frameWidth;
	  frameHeight = mpReferenceDevice->frameHeight;
    
	  //Initialize buffers.
    initBuffers(frameWidth,frameHeight,numPrevBuffers);
	  frameCounter = 0;
	  isInitialized = true;	  

    fillBuffers();
	  return true;
  }

  bool unInitialize() 
  { 
    isInitialized = false;
    return true; 
  }  
  
	int grabFrame()
  {
    PVASSERT(isInitialized);
    //set the pointer to reference the other devices image

    PVASSERT(mFrames.capacity() == mpReferenceDevice->mFrames.capacity());
    PVASSERT(mFrames.size() == mpReferenceDevice->mFrames.size());   

    for (int i = 0; i < mFrames.size(); i++)
      mFrames[i]->pRGBImage = mpReferenceDevice->mFrames[i]->pRGBImage;

    nextFrame(); 
    return 0;
  }

  bool isFirstFrame()
  {
    //do nothing
    return false;
  }

	void gotoFirstFrame()
  {
    //do nothing
  }  
  
  ImageAcquirePropertyPtr getProperty(ImageAcquireProperty::Code p)
  {    
    PVASSERT(mpReferenceDevice->pParams && p != ImageAcquireProperty::INVALID_CODE);
    return mpReferenceDevice->pParams->get(p);
  }

  int getSkipFrameInterval()
  {
    return mpReferenceDevice->getSkipFrameInterval();
  }  

  int getTotalNumberOfFrames()
  {    
    return mpReferenceDevice->getTotalNumberOfFrames();    
  }


private:

  /**
   * A pointer to the device that is being referenced	
   */
  ImageAcquireDeviceBasePtr mpReferenceDevice; 
};

} // namespace ait


#endif


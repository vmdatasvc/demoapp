/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VideoWriter_HPP
#define VideoWriter_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <string>

// AIT INCLUDES
//

#include <ait/Image.hpp>
#include <AitBaseTypes.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

#include "VideoWriter_fwd.hpp"

namespace ait 
{


/**
 * Write videos into files using PvImages. This is the only class that should
 * be used to write videos. Do not use the inherited classes, instead create the
 * instances using the factory static member VideoWriter::create().
 * @par Responsibilities:
 * - Create and write frames into video files.
 * - Set some limited number of settings.
 * - Show dialog to let the user select more specific settings.
 * @see VideoReader
 */
class VideoWriter : private boost::noncopyable
{
  //
  // LIFETIME
  //

protected:

  /**
   * Constructor.
   */
  VideoWriter();

public:

  /**
   * Factory pattern to build the concrete video writer.
   */
  static VideoWriterPtr create();

  /**
   * Destructor
   */
  virtual ~VideoWriter();

  //
  // OPERATIONS
  //

public:

  /**
   * Initialization.
   * @param fname [in] Name of the file to write.
   * @remark If no codec has been loaded, it will try to load the
   * settings from "<CommonFilesPath>/data/codecs/default.codec"
   */
  virtual void open(const std::string& fname) = 0;

  /**
   * Write one frame into the video.
   */
  virtual void writeFrame(const Image32& img) = 0;

  /**
   * Skip one frame of the video. This is useful when capturing from 
   * a live source. If the time line is behind, some frames should be
   * skipped in order to synchronize. The last frame will be repeated.
   */
  virtual void skipFrame() = 0;

  /**
   * Finalize. This must be called to close the file.
   */
  virtual void close() = 0;

  /**
   * Load the codec and its settings from a file.
   * @param fname [in] name of the file to load the codec settings from.
   * @param showDialogIfNotPresent [in] If the file is not present, it will
   * display the dialog box and save the settings in the given file name.
   * @return true if ok was pressed on the dialog, or the file was
   * properly loaded. false otherwise.
   */
  virtual bool loadCodecSettings(const std::string& fname, bool showDialogIfNotPresent = true) = 0;

  /**
   * Save the codec and its settings into a file.
   */
  virtual void saveCodecSettings(const std::string& fname) = 0;

  /**
   * Show the dialog that allows to modify the codec settings in the given file.
   * @return true if ok was pressed, false otherwise.
   */
  virtual bool showCodecSettingsDialog(const std::string& fname) = 0;

  /**
   * Return true if there is a valid codec defined.
   */
  virtual bool hasValidCodec() = 0;

#ifdef WIN32

  /**
   * Set the owner window for this thread.
   */
  virtual void setOwnerWindow(Uint32 id) = 0;

#endif

  //
  // ACCESS
  //

public:

  /**
   * Set the average frame duration in seconds. This is
   * the inverse of the FPS. It must be set before opening
   * the file.
   */
  virtual void setAverageFrameDuration(double avgDuration)
  {
    mFramesPerSecond = 1.0/avgDuration;
  }

  /**
   * Set the number of frames per second.
   */
  virtual void setFramesPerSecond(double fps)
  {
    mFramesPerSecond = fps;
  }

  /**
   * Get the average frame duration in seconds.
   */
  virtual double getAverageFrameDuration()
  {
    return 1.0/mFramesPerSecond;
  }

  /**
   * Get the number of frames per second.
   */
  virtual double getFramesPerSecond()
  {
    return mFramesPerSecond;
  }

  /**
   * Set the key frame rate (override default codec settings).
   */
  //virtual void setKeyFrameRate(int i) = 0;

  /**
   * Get the current key frame rate.
   * @return The current key frame rate. -1 if it is not available.
   */
  //virtual int getKeyFrameRate() = 0;

  //
  // INQUIRY
  //

public:

  //
  // ATTRIBUTES
  //

protected:

  /**
   * The number of frames per second.
   */
  double mFramesPerSecond;

};


}; // namespace ait

#endif // VideoWriter_HPP


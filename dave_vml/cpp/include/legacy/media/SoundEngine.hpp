/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef SOUNDENGINE_HPP
#define SOUNDENGINE_HPP

#ifdef WIN32

#include <windows.h>
#include <mmsystem.h>
#include <dsound.h>

#define MAXVOL_VAL	0
#define MIDPAN_VAL	0

namespace ait
{

/// @todo Move this class to the LibGraphics library (or another library).
class SoundEngine
{

public:
  int init(HWND hwndParent);

  IDirectSound *pds;
  LPDIRECTSOUNDBUFFER pdsbPrimary;
  PWAVEFORMATEX pwfxFormat;

  SoundEngine();
  virtual ~SoundEngine();

protected:
  int initPrimarySoundBuffer();

};

#endif

extern SoundEngine gSE;

} // namespace ait;

#endif

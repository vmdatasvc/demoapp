/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#if !defined(AFX_SOUND_H__6D9D4CE1_E2AD_11D1_8322_00C0DFE41F23__INCLUDED_)
#define AFX_SOUND_H__6D9D4CE1_E2AD_11D1_8322_00C0DFE41F23__INCLUDED_

#include "SoundEngine.hpp"

namespace ait
{

#define VOL_MIN       -10000
#define VOL_MAX       0

#define PAN_MIN       -10000
#define PAN_MAX       10000

#define NOTIFY_MAX    10000

// Internal class state flags
#define FI_INTERNALF_3D		    0x00000001
#define FI_INTERNALF_HARDWARE	    0x00000002
#define FI_INTERNALF_LOOPED	    0x00000004
#define FI_INTERNALF_PLAYING	    0x00000008
#define FI_INTERNALF_LOST	    0x00000010
#define FI_INTERNALF_LOADED	    0x00000020
#define FI_INTERNALF_STATIC	    0x00000040
#define FI_INTERNALF_STREAMING	    0x00000080
#define FI_INTERNALF_INTERFACE	    0x00000200
#define FI_INTERNALF_NOTIFY         0x00002000

/// @todo Move this class to the LibGraphics library (or another library).
class Sound
{

public:

    Sound();
    Sound(char *szName);
    Sound(const Sound& snd);
    virtual ~Sound();

    int loadWave( LPSTR lpszFile, int nIndx = 0);
	int recordWave( LPSTR lpszFile, int nIndx = 0);
    void duplicate( const Sound& snd );

    void play(void);
    void play(float factor);
    void stop(void);

    inline void setVolume(LONG volume)
		    { pDSB->SetVolume(volume); }

    inline void setNotify( BOOL fNew )
		    { setInternalFlag( fNew, FI_INTERNALF_NOTIFY ); }

    inline void setPlaying( BOOL fNew )
		    { setInternalFlag( fNew, FI_INTERNALF_PLAYING ); }

    inline void setLooped( BOOL fNew )
		    { setInternalFlag( fNew, FI_INTERNALF_LOOPED ); }

    inline BOOL isNotifyOn()
		    { return (dwInternalFlags & FI_INTERNALF_NOTIFY); }

    inline BOOL isPlaying()
		    { return (dwInternalFlags & FI_INTERNALF_PLAYING ); }

    inline BOOL isLooped()
		    { return (dwInternalFlags & FI_INTERNALF_LOOPED ); }

    inline BOOL isHardware()
		    { return (dwInternalFlags & FI_INTERNALF_HARDWARE ); }

// Useful protected member functions
protected:

    virtual int newDirectSoundBuffer( void );
    void setFileName( LPTSTR lpsz, int nIndx );

    inline void setInternalFlag( BOOL fSet, DWORD dwVal )
			    { if( fSet ) dwInternalFlags |= dwVal;
				    else dwInternalFlags &= ~dwVal; }

// Member data
protected:
//public:
    SoundEngine    *pDirectSound;

    LPBYTE          pbData;		// Pointer to actual data of file.
    UINT            cbDataSize;	// Size of data.
    LPWAVEFORMATEX  pwfx;		// Pointer to waveformatex structure.
    DSBUFFERDESC    dsbd;

    DWORD           freq;

    DWORD           dwInternalFlags;  // A bit field of flags

    TCHAR           szFileName[MAX_PATH];
    int             nFileIndex;	// Index to filename, without dir.

    LPDIRECTSOUNDBUFFER	    pDSB;     // Pointer to direct sound buffer.
    LPDIRECTSOUNDNOTIFY     pDSN;     // pointer to direct sound notify instance

    DSBPOSITIONNOTIFY       dsbPosNotify;
//    BOOL                    fNotifying;
    UINT                    dwTimeNotified;

public:
	UINT size(void)
	{
		return cbDataSize;
	}

	LPBYTE dataPtr(void)
	{
		return pbData;
	}

	DWORD frequency(void)
	{
		return freq;
	}

	LPWAVEFORMATEX waveformat(void)
	{
		return pwfx;
	}

	void setSize(UINT sz)
	{
		cbDataSize = sz;
	}

	void setFrequency(DWORD fr)
	{
		freq = fr;
	}

	void setWaveformat(LPWAVEFORMATEX wvfmt)
	{
		pwfx = wvfmt;
	}

	void allocateData(void)
	{
		pbData = (BYTE *)GlobalAlloc(0x0000, cbDataSize);
	}
};

} // namespace ait

#endif	// !defined(AFX_SOUND_H__6D9D4CE1_E2AD_11D1_8322_00C0DFE41F23__INCLUDED_)



/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ACTIVECAM_HPP
#define ACTIVECAM_HPP

#include <PvUtil.hpp>
#include <ait/Vector3.hpp>

#ifdef WIN32
#include <windows.h>
#include <winnt.h>
#include <winbase.h>
#else
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#endif

#include <stdio.h>


using namespace std;


/////////////////////////////////////////////////////////////////////
//
// Fundamental macros and data structure
//
/////////////////////////////////////////////////////////////////////
#define MAXCOMMANDLENGTH      15     /* The maximum command length */
#define ERROR_READ         -1001
#define ERROR_WRITE        -1002


// USING THE COM2
#ifdef WIN32
#define DEFAULT_SERIAL_CHANNEL "COM2:"/* Serial comunications      */
#else
#define DEFAULT_SERIAL_CHANNEL "/dev/ttys2"
#endif


// DO NOT DELETE YET!!!
// Overlapped Serial Comm
//static OVERLAPPED  m_ov;


/////////////////////////////////////////////////////////////////////
//
// COMMAND PARAMETER LIST 
//
/////////////////////////////////////////////////////////////////////
enum acConst 
{ 
  // used globally
  AC_ON,            AC_OFF,           AC_ERROR,        AC_INIT,

  // used by PAN/TILT
  AC_UP,            AC_DOWN,          AC_LEFT,         AC_RIGHT,     
  AC_UPLEFT,        AC_UPRIGHT,       AC_DOWNLEFT,     AC_DOWNRIGHT,
  AC_STOP_PAN_TILT,  
  AC_ABSOLUTE_POS,  AC_RELATIVE_POS,  AC_HOME,         AC_RESET,

  // command classes
  AC_CLASS_ZOOM,          
  AC_CLASS_PANTILT, 
  AC_CLASS_FOCUS,
  AC_CLASS_WHITEBALANCE,  
  AC_CLASS_AUTOEXPOSURE,
  AC_CLASS_ZOOM_POS,
  AC_CLASS_PAN_TILT_POS,

  // used by ZOOM
  AC_STOP_ZOOM,     AC_TELE_STD,      AC_WIDE_STD, 
  AC_TELE_VAR,      AC_WIDE_VAR,      AC_DIRECT_ZOOM,

  // used by FOCUS
  AC_STOP_FOCUS,    AC_FAR,           AC_NEAR, 
  AC_AUTO_FOCUS_ON, AC_MANUAL_FOCUS_ON, AC_AUTO_MANUAL,
  AC_DIRECT_FOCUS,

  // used by WHITE BALANCE
  AC_AUTO_WB,       AC_INDOOR,        AC_OUTDOOR, 
  AC_ONEPUSH,       AC_ONEPUSH_TRIG,
  
  // used by AUTO EXPOSURE
  AC_FULL_AUTO,     AC_MANUAL,        AC_SHUTTER_PRIO, 
  AC_IRIS_PRIO,     AC_BRIGHT,

  AC_NONE

};


/////////////////////////////////////////////////////////////////////
//
// COMMAND SET LIST 
//
// This is used between low level functions 
//   in lower level than the user interface function
// Not needed for user level interface
//
/////////////////////////////////////////////////////////////////////
enum camConst
{
  CAM_FOCUS, CAM_BACKLIGHT, 

  // POSITION INQUIRY	
  CAM_ZOOM_POS,      CAM_FOCUS_POS,      CAM_SHUTTER_POS, 
  CAM_IRIS_POS,      CAM_GAIN_POS
};


/////////////////////////////////////////////////////////////////////
//
// ActiveCam Class 
//
/////////////////////////////////////////////////////////////////////
class ActiveCam {

private:

  // flag indicating whether camera is hooked up to the com port or not
  bool cameraSetup;

  // file descripter for communcating bet. com & cam
  HANDLE ser_fd;

  // Overlapped Serial Comm
#ifdef WIN32  
  OVERLAPPED  m_ov;
#endif

  // keep the current value of pan or tilt
  const int AC_KEEP_CURRENT;

  // time out value
  const int standardTimeout;     // milliseconds

  // The number of retries
  const int RETRIES;     

  // pan/tilt conversion value
  const float STEPSPERPANDEGLEFT;
  const float STEPSPERPANDEGRIGHT;
  const float STEPSPERTILTDEGUP;
  const float STEPSPERTILTDEGDOWN;

  // FOCUS
  const int MAX_FOCUS;
  const int MIN_FOCUS;

  /* Command Struct */
  class visca_command 
  {
  public:
    unsigned char pos[MAXCOMMANDLENGTH];
    int length;
  };

  bool exitWhenError;

public:

  // default constructor
  ActiveCam();
  
  // destructor
  ~ActiveCam();
  
  bool cameraPresent(void) { return cameraSetup; }
  

/////////////////////////////////////////////////////////////////////
// Essential functions for controlling the EVI_D30 camera
// IMPORTANT:
//	Absolute Value Movement : The value is NOT accumulative
/////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////
  // bool	ActiveCam::panTilt(int nextPan, int nextTilt)
  // set the pan and tilt by the nextPan and nextTilt
  // INPUT   :
  //   Pan   : -100 ~ 100 (left ~ right : from Camera point of view)
  //   Tilt  : -25  ~ 25  (down ~ up)
  //   speed : 1 ~ 127    (slow ~ fast)
  //   Home Position : (0, 0)
  //  AC_KEEP_CURRENT: keep the cur pan/tilt(Used only internally)
  // OUTPUT  : 
  /////////////////////////////////////////////////////////////////
  bool panTilt(int, int, int speed = 127);
  
  /////////////////////////////////////////////////////////////////
  // bool panTilt(acConst cmd = AC_ABSOLUTE_POS, int, int)
  // pan and tilt operation by the nextPan and nextTilt
  // INPUT   :
  //  cmd    : commands in enum acConst set
  //          AC_UP:
  //          AC_DOWN:
  //          AC_LEFT:
  //          AC_RIGHT:
  //          AC_UPLEFT:
  //          AC_UPRIGHT:
  //          AC_DOWNLEFT:
  //          AC_DOWNRIGHT:
  //          AC_STOP_PAN_TILT:
  //          AC_ABSOLUTE_POS:
  //          AC_RELATIVE_POS:
  //          AC_HOME:
  //          AC_RESET:
  //
  //   Pan   : -100 ~ 100 (left ~ right : from Camera point of view)
  //   Tilt  : -25  ~ 25  (down ~ up   )
  //   speed : 1 ~ 127    (slow ~ fast)
  //   Home Position : (0, 0)
  //  AC_KEEP_CURRENT: keep the cur pan/tilt(Used only internally)
  // OUTPUT  : 
  /////////////////////////////////////////////////////////////////
  bool panTilt(acConst cmd  = AC_ABSOLUTE_POS, 
    int nextPan  = 0, 
    int nextTilt = 0,
    int speed    = 127);
  
  /////////////////////////////////////////////////////////////////
  // Initializing EVI-D30 
  //  INPUT  : NONE
  //  OUTPUT : bool val of success
  /////////////////////////////////////////////////////////////////
  bool initialize();
  
  /////////////////////////////////////////////////////////////////
  // Resetting Pan/Tilt
  //  INPUT  : NONE
  //  OUTPUT : bool val of success
  // IMPORTANT: 
  //   the meaning of reset is set to the current position 
  //   by EVI_D30 manual, which is to repeat the last pan/tilt
  //   This command initialize the camera, meaning that
  //   the camera recalibrates itself with respect to pan/tilt
  /////////////////////////////////////////////////////////////////
  bool reset();
  
  /////////////////////////////////////////////////////////////////
  // set the zoom 
  // INPUT  
  //  cmd   : zoom command in enum acConst set
  //          AC_STOP_ZOOM : 
  //          AC_TELE_STD  : standard (fixed speed : slow)
  //          AC_WIDE_STD  :  
  //          AC_TELE_VAR  : variable speed ( 2 ~ 7 )
  //          AC_WIDE_VAR  :
  //          AC_DIRECT_ZOOM  
  //  zVal  : depends on the command
  //          AC_TELE_VAR, AC_WIDE_VAR : speed( 2 ~ 7 )
  //              2 : low
  //              7 : high
  //          AC_DIRECT_ZOOM: distance(1 ~ 1023)
  //              1   : Wide (widest)
  //              1023: Tele (narrowest)
  //          ELSE         : no useful
  // OUTPUT : bool
  /////////////////////////////////////////////////////////////////
  bool zoom(acConst cmd, int zVal = 7);
  bool zoom(int zVal);

  /////////////////////////////////////////////////////////////////
  // Important!!!
  // When adjust the focus, CHANGE the mode to Manual 
  //   AC_MANUAL_FOCUS_ON:
  //   the send Far/Near or Direct command
  //   REQUIRED BY SONY EVI-D30
  /////////////////////////////////////////////////////
  // set the FOCUS 
  // INPUT  
  //  cmd   : FOCUS command in enum acConst set
  //          AC_STOP_FOCUS:
  //          AC_FAR:
  //          AC_NEAR:
  //          AC_AUTO_FOCUS_ON:
  //          AC_MANUAL_FOCUS_ON:
  //          AC_AUTO_MANUAL:
  //          AC_DIRECT_FOCUS:
  //  fVal  : used only by AC_DIRECT_FOCUS
  //          (Infinity = 4096 ~ Close = 40959)
  // OUTPUT : bool
  /////////////////////////////////////////////////////////////////
  bool focus(acConst cmd = AC_AUTO_FOCUS_ON, int fVal = 4096);
  
  /////////////////////////////////////////////////////////////////
  // White Balance Setting
  // INPUT  : white balance command in enum acConst
  //    AC_AUTO_WB
  //    AC_INDOOR
  //    AC_OUTDOOR
  //    AC_ONEPUSH
  //    AC_ONEPUSH_TRIG
  // OUTPUT : bool
  /////////////////////////////////////////////////////////////////
  bool whiteBalance(acConst);
  
  /////////////////////////////////////////////////////////////////
  // Auto Exposure Setting
  // INPUT  : Auto Exposure command in enum acConst
  //    AC_FULL_AUTO
  //    AC_MANUAL
  //    AC_SHUTTER_PRIO
  //    AC_IRIS_PRIO
  //    AC_BRIGHT
  // OUTPUT : bool
  /////////////////////////////////////////////////////////////////
  bool autoExposure(acConst);
  
  
  
  /////////////////////////////////////////////////////////////////////
  //
  // Inquiry functions
  //
  /////////////////////////////////////////////////////////////////////
  
  /////////////////////////////////////////////////////////////////
  //  bool ActiveCam::isPowerOn()
  //  INPUT  : NONE
  //  OUTPUT : bool val of power status
  //           true  : on
  //           false : off 
  /////////////////////////////////////////////////////////////////
  bool isPowerOn();
  
  /////////////////////////////////////////////////////////////////
  //  Vector2i getPanTiltPos()
  //  INPUT  : NONE
  //  OUTPUT : Vector2i pan/tilt pos
  /////////////////////////////////////////////////////////////////
  Vector2i getPanTiltPos();
  
  /////////////////////////////////////////////////////////////////
  //  int getZoomPos()
  //  INPUT  : NONE
  //  OUTPUT : zoom position
  /////////////////////////////////////////////////////////////////
  int getZoomPos(); 
  
  /////////////////////////////////////////////////////////////////
  //  bool isAutoFocus()
  //  INPUT  : NONE
  //  OUTPUT : bool 
  //           true  : auto focus
  //           false : manual
  /////////////////////////////////////////////////////////////////
  bool isAutoFocus(); 
  
  /////////////////////////////////////////////////////////////////
  //  int getFocusPos()
  //  INPUT  : NONE
  //  OUTPUT : int Focus position
  /////////////////////////////////////////////////////////////////
  int getFocusPos(); 
  
  /////////////////////////////////////////////////////////////////
  //  int getShutterPos()
  //  INPUT  : NONE
  //  OUTPUT : int Shutter position
  /////////////////////////////////////////////////////////////////
  int getShutterPos(); 
  
  /////////////////////////////////////////////////////////////////
  //  int getIrisPos()
  //  INPUT  : NONE
  //  OUTPUT : int Iris position
  /////////////////////////////////////////////////////////////////
  int getIrisPos(); 
  
  /////////////////////////////////////////////////////////////////
  //  int getGainPos()
  //  INPUT  : NONE
  //  OUTPUT : int Gain position
  /////////////////////////////////////////////////////////////////
  int getGainPos(); 
  
  /////////////////////////////////////////////////////////////////
  //  acConst getWhiteBalance()
  //  INPUT  : NONE
  //  OUTPUT : acConst white balance value
  //           AC_AUTO_WB
  //           AC_INDOOR
  //           AC_OUTDOOR
  //           AC_ONEPUSH
  //           AC_ERROR
  /////////////////////////////////////////////////////////////////
  acConst getWhiteBalance();
  
  /////////////////////////////////////////////////////////////////
  //  acConst getAutoExposure()
  //  INPUT  : NONE
  //  OUTPUT : auto exposure value
  //           AC_FULL_AUTO
  //           AC_MANUAL
  //           AC_SHUTTER_PRIO
  //           AC_IRIS_PRIO
  //           AC_BRIGHT
  //           AC_ERROR
  /////////////////////////////////////////////////////////////////
  acConst getAutoExposure();
  
  
  /////////////////////////////////////////////////////////////////////
  //
  //  BELOW GROUP OF FUNCTIONS ARE MOSTLY LOW LEVEL FUNCTIONS
  //  RECOMMEND NOT TO BE USED BY GENERAL USERS	
  // 
  /////////////////////////////////////////////////////////////////////
private:
  
  /////////////////////////////////////////////////////////////////////
  //
  // Low Level Functions
  // Serial Communication using the port
  //
  /////////////////////////////////////////////////////////////////////
  
  int SendCmd(HANDLE, visca_command *);
  
  int SendToCam(HANDLE,visca_command *); 
  
  int ReadFromCam(HANDLE, visca_command *); 
  
  
  /////////////////////////////////////////////////////////////////
  //  int WaitForLength(HANDLE, int, visca_command *, int)
  //  Fixes the unnecessary delay time in WaitFor()
  //  Using the Win32 API function for serial comm
  //  Needs to know the length of the command it waits for
  /////////////////////////////////////////////////////////////////
  int WaitForLength(HANDLE, visca_command *, int, unsigned int timeOut=10000);//standardTimeout);
  
  int IsAck(visca_command *);
  
  int IsCpl(visca_command *);
  
  void PrintCmd(visca_command *);
  
  
  
  /////////////////////////////////////////////////////////////////////
  //
  // Init Functions
  //
  /////////////////////////////////////////////////////////////////////
  
  int Init(HANDLE, LPCSTR);
  
  int isEqual(visca_command *, visca_command *); 
  
  int SetupConnection(HANDLE);
  
  
  
  /////////////////////////////////////////////////////////////////////
  //
  // Set Functions
  //
  /////////////////////////////////////////////////////////////////////
  
  int SetAbsPanTilt(HANDLE, int, int, int);
  
  int SetRelPanTilt(HANDLE, int, int, int);
  
  int SetGeneralPanTilt(HANDLE aFd, acConst cmd);
  
  int SetStopPanTilt(HANDLE);
  
  int SetZoom(HANDLE, int);
  
  int SetZoomIn(HANDLE, int);
  
  int SetZoomOut(HANDLE, int);
  
  int SetZoomStd(HANDLE, acConst);
  
  int SetFocus(HANDLE, int);
  
  int SetFocusCmd(HANDLE, acConst);
  
  int SetWB(HANDLE, acConst);
  
  int SetAE(HANDLE, acConst);
  
  int AssignAddress(HANDLE); 
  
  int ClearInterface(HANDLE);
  
  int SetAgain(HANDLE);
  
  int SetCameraPosition(HANDLE, int);
  
  int ResetCameraPosition(HANDLE, int);
  
  int SetCameraToPresetPosition(HANDLE, int);
  
  int SetKeyLock(HANDLE, int);
  
  int SetInit(HANDLE);
  
  int SetHomePosition(HANDLE);
  
  
  
  /////////////////////////////////////////////////////////////////////
  //
  // Get Functions
  //
  /////////////////////////////////////////////////////////////////////
  
  visca_command GetAgain(HANDLE);
  
  int GetKeyLockStatus(HANDLE);
  
  int GetPowerStatus(HANDLE);
  
  int GetZoomValue(HANDLE);
  
  int GetAutoFocusMode(HANDLE);
  
  Vector2i GetPanTiltValue(HANDLE);
  
  int GetWBMode(HANDLE);
  
  int GetAEMode(HANDLE);
  
  int GetPosition(HANDLE, camConst);
  
  
};


#endif //ACTIVECAM_HPP

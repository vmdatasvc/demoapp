/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ImageAcquireDeviceGeo_HPP
#define ImageAcquireDeviceGeo_HPP

#include "ImageAcquireDeviceBase.hpp"
#include "ImageAcquireProperty.hpp"

#include <boost/thread/mutex.hpp>
#include <boost/thread/xtime.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/condition.hpp>

#include <dmipc/IDMIPCClient.h>

namespace ait
{

/**
 * Very simple class used to help obtain desired frame rates. Average
 * delays between frames are computed, and the average delay is compared
 * with the desired delay.
 */
class DelayMonitor
{
public:
  DelayMonitor()
  {
    mSize = 10;
    mDelayBuffer.setCapacity(mSize);       
  }
 
  void init(double fps)
  {
    assert(fps != 0.0);
    mDesiredDelay = 1.0/fps;
    mDelay = mDesiredDelay;
  }

  void update(double delay)
  {
    mDelayBuffer.push_back(delay);

    if (mDelayBuffer.size() == mSize)
    {
      //calculate average delay
      double sum = 0.0;
      for (int i = 0; i < mDelayBuffer.size(); i++)
        sum += mDelayBuffer[i];

      double avgDelay = sum/mSize;

      //reset buffer
      mDelayBuffer.clear();

      //update desired delay

      double diff = mDesiredDelay - avgDelay;

      if (mDesiredDelay > avgDelay)
      {
        mDelay += .001;
      }
      else if (mDesiredDelay < avgDelay)
      {
        mDelay -= .001;
      }     

      mDelay = std::max(0.0, mDelay);
    }
  }

  double getDelay() const
  {
    return mDelay;
  }

protected:
  CircularBuffer<double> mDelayBuffer;
  double mDelay;
  int mSize;
  double mDesiredDelay;
};


class ImageAcquireDeviceGeo;
typedef boost::shared_ptr<ImageAcquireDeviceGeo> ImageAcquireDeviceGeoPtr;

class GeoVisionThread;
typedef boost::shared_ptr<GeoVisionThread> GeoVisionThreadPtr;

class GeoVisionThread: public CIDMIPCClientCallBack
{

private:

  /**
   * This class will hold information about each camera registered.
   */
  class GeoVisionCameraInfo
  {
  public:
    /**
     * The user defined device FPS. This ImageAcquireDevice will attempt to match
     * this FPS setting as closely as possible.
     */
    double deviceFps;  

    /**
     * This variable stores the last time a good frame was received for the
     * the camera with an id of mCameraId.
     */
    double lastFrameTime;

    /**
     * This flag is true if the last frame was a valid frame, meaning not a
     * lost signal or blue frame.
     */
    bool isLastFrameValid;

    /**
     * This is a simple device used to limit the maximum framerate.
     */
    DelayMonitor delayMonitor;

    /**
     * Device that holds this camera.
     */
    ImageAcquireDeviceGeo *pDevice;

    /**
     * Camera Id.
     */
    int cameraId;
  };

  typedef boost::shared_ptr<GeoVisionCameraInfo> GeoVisionCameraInfoPtr;

public:

  /**
   * Singleton pattern.
   */
  static GeoVisionThreadPtr instance();

  /**
   * Constructor
   */
  GeoVisionThread();

  /**
   * This function is called from within the devices initialize()
   * function. On the first registered device, the thread will be
   * created and initialized.
   *
   * Registering the same camId multiple times is not permitted.
   *
   * This function is thread safe.
   */
  void registerDevice(int camId, ImageAcquireDeviceGeo *pDevice);

  /**
   * When the last device is unregistered, the thread is stopped.
   * 
   * This function is thread safe.
   */
  void unregisterDevice(int camId);

  /**
   * This will be the main loop of the thread.
   */
  void mainLoop();

protected:

  /**
   * Request video frames from the Geovision main system. This function
   * handles deciding which frames for which cameras to request, and attempts
   * to limit the framerate to the user-specified value.   
   */
  void requestFrames();

  double getNextFrameTime(GeoVisionCameraInfoPtr pCamInfo);

  void registerFrameTime(GeoVisionCameraInfoPtr pCamInfo, double time);

  bool mustSkipThisFrame(GeoVisionCameraInfoPtr pCamInfo, double currentTime);

  /**
   * Get the next camera information. This is based on the current
   * camera id. This call is thread safe.
   */
  GeoVisionCameraInfoPtr getNextCamera();

  /**
   * Get the camera information based on the id. If the camera
   * is not found it returns and empty pointer. This call
   * is thread safe.
   */
  GeoVisionCameraInfoPtr getCameraInfo(int camId);

  /**
   * Geovision callback function. This function is currently not used.
   */
  void __stdcall OnReply(struct DMRequest *,struct DMData *);  

  /**
   * Geovision callback function. This function handles incoming video
   * frames received from the Geovision main system.
   *
   * See Geovision documentation for details.
   *
   */
  void __stdcall OnRpyVideo(UINT iCam,IPC_RPY_CAM* pCam,LPVOID pData);  

  /**
   * Geovision callback function. This function is currently not used.
   */  
  void __stdcall OnRpyAudio(unsigned int,struct IPC_RPY_AUDIO *,void *);  

  /**
   * Geovision callback function. This function is currently not used.
   */  
  void __stdcall OnRpyTrigger(struct DMRequest *,struct DMData *);

  /**
   * This function initializes the interface between the geovision
   * main system and the current thread. It should only be called
   * by the master device.
   */
  void initDmipc();
  
  /**
   * Dispose the driver before ending the thread.
   */
  void finishDmipc();

  /**
   * This variable will indicate if the GrabFrame function should
   * be called from the main loop or from the callback.
   */
  bool mGrabFrameFromMainLoop;

  /**
   * This flag indicates if the callback has been invoked and we
   * are waiting for answers.
   */
  bool mIsCallbackInvoked;

  /**
   * The singleton instance.
   */
  static GeoVisionThreadPtr mpInstance;

  /**
   * The thread object.
   */
  boost::thread *mpThread;
  
  /**
   * The thread class for boost has to be copyable, so this will
   * be the container.
   */
  class ThreadHolder
  {
  public:
    ThreadHolder(GeoVisionThread& t) : mThread(t) {}
    GeoVisionThread& mThread;
    void operator()() { mThread.mainLoop(); }
  };

  ThreadHolder *mpThreadHolder;

  /**
   * A mapping of camera id's to their respective ImageAcquireDeviceGeo
   * objects
   */
  std::map<int, GeoVisionCameraInfoPtr> mCameraMap;  

  /**
   * An iterator specifying the next camera that a frame will be requested
   * for.
   */
  int mCurrentCameraId;

  /**
   * A pointer to the Geovision client object. This object is used to interact
   * with the Geovision main system.
   */
  CIDMIPCClient* mpIdmipcClient;

  /**
   * This structure contains information retrieved from the Geovision main 
   * system during the initialization step. See Geovision documentation for 
   * details.
   */
  IDMIPCInitData mIdmipcInitData;  

  /**
   * This mutex is shared across all instances of the class, and is used to 
   * ensure that initialize is properly called.
   */
  boost::mutex mMutex;

};

class ImageAcquireDeviceGeo: public ImageAcquireDeviceBase
{

  friend class GeoVisionThread;

public:
		
	///Constructor.
	ImageAcquireDeviceGeo(void);
	
	//Destructor
	virtual ~ImageAcquireDeviceGeo(void);
		
  /**
   * Initialize this Geovision device. An interface to the Geovision main 
   * system is created here for the master device. The first device created
   * in memory is the master, all other devices are the slaves.
   */
	virtual bool initialize(int numPrevBuffers = 0, int numConsecutiveFrameGrabs = 1);

  virtual bool unInitialize();

	virtual int grabFrame();  

  virtual int getTotalNumberOfFrames() { return 0; }  

  
protected:

  /**
   * The camera id that this ImageAcquireDeviceGeo is responsible for.
   * The first camera has an id of 1.
   */
  int mCameraId;

  /**
   * The next frame to be written to. Incoming frames are written here.
   */
  MediaFrameInfoPtr mpNextFrame;

  /**
   * This mutex is used to protect the image frame memory access
   */
  boost::mutex mFrameReadyMutex;

  /**
   * When grabFrame() is called, threads will wait on this condition before 
   * returning. When a frame is received, this condition will be signaled.
   */
  boost::condition mIsFrameReady;

  /**
   * Save the Frames per second for easy access.
   */
  double mFps;

};



} // namespace ait

#endif



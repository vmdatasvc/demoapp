/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ImageAcquireDeviceInfo_HPP
#define ImageAcquireDeviceInfo_HPP

#include "Settings.hpp"

#include <string>
#include <boost/shared_ptr.hpp>

namespace ait
{

class ImageAcquireDeviceInfo
{
public:

  //Constructor.
  ImageAcquireDeviceInfo(std::string deviceName,std::string regPath);
  
  //Destructor
  virtual ~ImageAcquireDeviceInfo(void);
  
  //Initialize this class
  void initialize(bool cascade = true);
  
  //Un - Init this class.
  void unInitialize(bool cascade = true);
  
  // Save the settings to the registry.
  void saveSettings();

  const std::string& getdeviceDriverID();
  const std::string& getdeviceName();
  const std::string& getdeviceVersion();
  int getdeviceIndex() { return deviceIndex; }
  const std::string& getFile1();
  const Vector2i& getWindowPos() { return windowPos; }
  void setWindowPos(const Vector2i& newPos) { windowPos = newPos; }
  const Vector2i& getWindowSize() { return windowSize; }
  void setWindowSize(const Vector2i& newSize) { windowSize = newSize; }
  const std::string& getRegPath() { return regPath; }

  bool windowHasBorder() { return mWindowHasBorder; }

#ifdef WIN32
  ait::Uint32 getParentHwnd() { return mHwndParent; }
#endif

  /*
  unsigned char hue;
  unsigned char saturation;
  unsigned char grabInputGain;
  unsigned char contrast;
  unsigned char brightness;
  unsigned char whiteBalance;
  */
  
  Vector2i windowPos;
  Vector2i windowSize;

protected:

  bool isInitialized;


private:

  /// Tells what device driver(class for now) to use for this card.
  std::string deviceDriverID; 

  /// Hardware name such as Matrox Meteor II / Standard  etc...
  std::string deviceName;  

  /// Hardware version info here.
  std::string deviceVersion;  

  /// Index of the device, for multiple cameras with the same name.
  int deviceIndex;

  /**
   * This will indicate the source file.
   */
  std::string file1;

  bool mWindowHasBorder;

#ifdef WIN32
  ait::Uint32 mHwndParent;
#endif

  //Put special device specific Parameters here...

  ait::Settings* registry; ///< Pointer to registry.
  std::string regPath;     ///< Reg Path.

};

typedef boost::shared_ptr<ImageAcquireDeviceInfo> ImageAcquireDeviceInfoPtr;

} // namespace ait

#endif



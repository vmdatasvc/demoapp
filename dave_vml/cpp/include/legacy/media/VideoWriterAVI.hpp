/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
#ifndef VIDEOWRITERAVI_HPP
#define VIDEOWRITERAVI_HPP

#include <VideoWriter.hpp>

/**
 * This class is obsolete, 
 * The current implementation of this class shows only 
 * @deprecated Please use VideoWriter instead.
 */
class VideoWriterAVI
{
protected:
public:

  VideoWriterAVI(void) {}

  virtual ~VideoWriterAVI() {}

  void init(int w, int h, const char *name, bool flag = true,
    std::string encodeFile = "<CommonFilesPath>\\data\\codec\\default_compressor.dat");

  void saveFrame(ait::Image32 &image);

  void finish(void);

protected:
  ait::VideoWriterPtr mpVideoWriter;
};

#endif
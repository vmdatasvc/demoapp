/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ICameraDriverEvents_HPP
#define ICameraDriverEvents_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

#include "ICameraDriverEvents_fwd.hpp"

namespace ait 
{


/**
 * This class is not yet documented.
 * Here is a more detailed description of the class. Next come the 
 * Responsibilities of the class. Try to minimize the responsibilities, this
 * will help in creating a good design. Next, optionally write any design patterns
 * that were used for this class. Refer to the "Design Patterns" book by Gamma et al.
 * @par Responsibilities:
 * - The First Responsibility
 * - The Second Responsibility:
 *   - A sub-responsibility
 *   - Another sub-responsibility
 * - The Third Responsibility
 * @par Patterns:
 * - The first design pattern
 * - The second design pattern
 * @par Design Considerations:
 * - Here you can itemize some justifications for your decisions. It will help
 * others to understand why your class is the way it is.
 * @remarks
 * Some special comments or warnings about this class.
 * @see TheirClass, OurClass (other related classes).
 */
class ICameraDriverEvents : private boost::noncopyable
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  ICameraDriverEvents();

  /**
   * Destructor
   */
  virtual ~ICameraDriverEvents();


  //
  // OPERATIONS
  //

public:

  virtual void onVideoFrame(int cameraId, double startTimeStamp, double endTimeStamp,
    unsigned char *data, int dataSize) = 0;

};


}; // namespace ait

#endif // ICameraDriverEvents_HPP


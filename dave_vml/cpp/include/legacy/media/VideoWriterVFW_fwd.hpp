/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VideoWriterVFW_fwd_HPP
#define VideoWriterVFW_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class VideoWriterVFW;
typedef boost::shared_ptr<VideoWriterVFW> VideoWriterVFWPtr;

}; // namespace ait

#endif // VideoWriterVFW_fwd_HPP


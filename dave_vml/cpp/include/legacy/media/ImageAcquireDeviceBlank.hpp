/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ImageAcquireDeviceBlank_HPP
#define ImageAcquireDeviceBlank_HPP

#include "ImageAcquireDeviceBase.hpp"

#include <ait/Image.hpp> //added for loading default ppm image

namespace ait
{

class ImageAcquireDeviceBlank: public ImageAcquireDeviceBase
{
public:
		
	//Constructor.
	ImageAcquireDeviceBlank(void);
	
	//Destructor
	virtual ~ImageAcquireDeviceBlank(void);

protected:

	bool initialize(int numPrevBuffers = 0, int numConsecutiveFrameGrabs = 1);
	bool unInitialize();
	int grabFrame();

	bool isFirstFrame();
	void gotoFirstFrame();

private:

  /**
   * Fixed image returned by the blank device.
   */
  Image32 mFrameImage;
};

} // namespace ait


#endif



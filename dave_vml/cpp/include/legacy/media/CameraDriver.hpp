/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef CameraDriver_HPP
#define CameraDriver_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <string>
#include <ait/Image.hpp>

// AIT INCLUDES
//

// LOCAL INCLUDES
//

#include "ICameraDriverEvents.hpp"

// FORWARD REFERENCES
//

#include "CameraDriver_fwd.hpp"
#include "Settings.hpp"
namespace ait 
{

/**
 * This class is used to get and control cameras or frame grabbers.
 * It is designed to control one camera at a time or also to control
 * several cameras from a single driver (some frame grabbers require
 * this).
 * The class provide means to obtain the frame grabber asyncronously.
 * @par Responsibilities
 * - Get Image data from Camera.
 * - Set camera parameters.
 * - Control syncronous and asynchronous access to camera pixels.
 * @par Design Considerations:
 * - The parameters have been implemented as strings to
 *   allow easier extensions and settings model-specific parameters.
 * @par Parameters
 * - Driver Parameters
 *   - NumCameras: Number of cameras handled by this driver.
 *   - MaxCameras: Maximum number of cameras.
 *   - UserName: User id for Network cameras.
 *   - UserPassword: Password for network cameras.
 * - Camera Parameters
 *   - DriverName: Read only driver id.
 *   - DriverVersion: String identifying the driver and version.
 *   - SourceBitCount: Bits per pixel. Only 32 is currently supported.
 *   - SourceWidth: With of the image in pixels.
 *   - SourceHeight: Height of the image in pixels.
 *   - FramesPerSecond: Frames per second. This can be a decimal value.
 *   - IpAddress: Address for network cameras.
 * - Reserved (Not implemented):
 *   - Brightness
 *   - Contrast
 *   - Hue
 *   - Saturation
 *   - Sharpness
 *   - Gamma
 *   - ColorEnable
 *   - WhiteBalance
 *   - BacklightCompensation
 *   - Gain
 *   - Pan
 *   - Tilt
 *   - Roll
 *   - Zoom
 *   - Exposure
 *   - Iris
 *   - Focus
 * @par Queries on each parameter (Not implemented)
 * <ParamName>.Min         Minimum Value if numeric (read only)
 * <ParamName>.Max         Maximum Value if numeric (read only)
 * <ParamName>.Step        Step Increments (read only)
 * <ParamName>.Auto        0|1 If it is automatic
 * <ParamName>.Available   0|1 If this driver supports this parameter (read only)
 * <ParamName>.Values      Possible string values for this parameter separated by comma (not numeric or limited values)
 * @par Threading
 * All members from this class must be called from the same thread, and the frames
 * will be returned asynchronously on a different thread
 * @see ICameraDriverEvents
 */
class CameraDriver : private boost::noncopyable
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  CameraDriver(const std::string& appPath="");

  /**
   * Destructor
   */
  virtual ~CameraDriver();

  /** 
   * Factory. Only "AXIS" is implemented.
   */
  static CameraDriver *create(const std::string& appPath = "");

  //
  // OPERATIONS
  //

public:

  virtual void init(int numCameras = 1) = 0;

  virtual void finish() = 0;

  virtual void setParam(int cameraId, const std::string& name, const std::string& value) = 0;

  virtual void setParam(const std::string& name, const std::string& value)
  {
    setParam(-1,name,value);
  }

  /**
   * Load parameters from a file. The file must be ASCII and
   * contain one parameter per line. Empty lines and lines
   * that start with the character '#' are ignored.
   * Each line contains a <name>=<value> pair.
   */
  virtual void loadParams(const std::string& fname);

  virtual const std::string getParam(int cameraId, const std::string& name) = 0;


virtual const std::string getParam(const std::string& name)
  {
    return getParam(-1,name);
  }

  /**
   * This function allows to get the value of a single param directly from the file.
   * Typically used before creating the CameraDriver instance to determine what should
   * be its type.
   */
  static std::string getParam(const std::string& fname,const std::string& paramName);

  virtual void registerEventHandler(ICameraDriverEvents *pEvents) {}

  /**
   * Set the frame size in pixels.
   */
  //void setSourceSize(const Vector2i& size);

  /**
   * Get the frame size in pixels.
   */
  //const Vector2i getSourceSize();

  /**
   * Get the FPS.
   */
  //void setFramesPerSecond(double fps);

  /**
   * Set the FPS. Note that some drivers might modify the
   * requested FPS because of device limitations.
   */
  //double getFramesPerSecond();


};


}; // namespace ait

#endif // CameraDriver_HPP


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VideoReader_fwd_HPP
#define VideoReader_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class VideoReader;
typedef boost::shared_ptr<VideoReader> VideoReaderPtr;

}; // namespace ait

#endif // VideoReader_fwd_HPP


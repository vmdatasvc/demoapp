/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ImageAcquireDeviceBase_fwd_HPP
#define ImageAcquireDeviceBase_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class ImageAcquireDeviceObserver;
typedef boost::shared_ptr<ImageAcquireDeviceObserver> ImageAcquireDeviceObserverPtr;

class ImageAcquireDeviceBase;
typedef boost::shared_ptr<ImageAcquireDeviceBase> ImageAcquireDeviceBasePtr;

}; // namespace ait

#endif // ImageAcquireDeviceBase_fwd_HPP


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ImageAcquireDeviceAVI_HPP
#define ImageAcquireDeviceAVI_HPP

#include "legacy/media/ImageAcquireDeviceBase.hpp"
//#include <VideoReader_fwd.hpp>

namespace ait
{

/**
 * Provides access to execute a single video files or a list of
 * video files.
 * To execute a list of video files, specify the name of the video
 * file list in the settings entry 'videoSegments'. This is an
 * XML file with the following format:
 * @code 
 * <?xml version="1.0"?> 
 * <video_segments>
 *   <video_files>
 *     <video_file>
 *       <!-- This file will play first from frame 10 to frame 200, and then
 *            from frame 100 to frame 205. Notice how segments can overlap. -->
 *       <file_name>video1.avi</file_name>
 *       <segments>
 *         <segment>
 *           <start_frame>10</start_frame>
 *           <end_frame>200</end_frame>
 *         </segment>
 *         <segment>
 *           <start_frame>100</start_frame>
 *           <end_frame>205</end_frame>
 *         </segment>        
 *       </segments>
 *     </video_file> 
 *     <video_file>
 *       <!-- This file will play from beginning to end -->
 *       <file_name>video2.avi</file_name>
 *     </video_file>   
 *     <video_file>
 *       <!-- This file will play from frame 1000 until the end of the file. -->
 *       <file_name>video3.avi</file_name>
 *       <segments>
 *         <segment>
 *           <start_frame>1000</start_frame>
 *         </segment>
 *       </segments>
 *     </video_file> 
 *   </video_files>
 * </video_segments>
 * @endcode
 * @remark This XML is in the same format used for the video_extractor application that allows
 * to generate one avi file from multiple files.
 */
class ImageAcquireDeviceAVI: public ImageAcquireDeviceBase
{
protected:

  //
  // These classes are used to parse the XML file list.
  //

  class VideoClip;
  class VideoFile;
  class ConfigHandler;

  typedef boost::shared_ptr<VideoFile> VideoFilePtr;
  typedef boost::shared_ptr<VideoClip> VideoClipPtr;

  typedef std::vector<VideoFilePtr> VideoFiles;
  typedef std::vector<VideoClipPtr> VideoClips;

public:
		
	///Constructor.
	ImageAcquireDeviceAVI(void);
	
	//Destructor
	virtual ~ImageAcquireDeviceAVI(void);

  void setAVIProperty(ImageAcquireProperty& param, bool set);

  virtual int getTotalNumberOfFrames();

  virtual int getSkipFrameInterval() {return mSkipFrameInterval;}


protected:

  VideoReaderPtr mpReader;

  PVTIME nextFrameTime;

  /**
   * The total number of frames when adding all segments.
   */
  long mTotalFrames;

  /**
   * The video segments to read.
   */
  VideoFiles mVideoFiles;

  /**
   * The index of the current file in the list.
   */
  int mCurrentVideoFile;

  /**
   * The index of the segment inside the current video file.
   */
  int mCurrentVideoClip;

  /**
   * The number of the current frame in the current video
   * file segment with respect to the beginning of the
   * video file.
   */
  long mCurrentFrameInClip;

  float mFps;

  /**
   * This flag will either run the device at the specified frame rate in the
   * property bags, or as fast as possible.
   */
  bool mRunAtFrameRate;

  /**
   * This variable allows to run the video skiping frames, therefore reducing the
   * frame rate. The advantage is that video can be processed faster using
   * this. The skip value will not affect the specified frame rate in the
   * registry. It is the responsibility of the caller to set the adequate frame
   * rate for the given skip frame interval.
   */
  int mSkipFrameInterval;

	bool initialize(int numPrevBuffers = 0, int numConsecutiveFrameGrabs = 1);
	bool unInitialize();
	int grabFrame();

	bool isFirstFrame();
	void gotoFirstFrame();

  void loadVideoFiles(std::string fname);

private:

	
	
};

} // namespace ait

#endif



/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VideoWriterFfmpeg_fwd_HPP
#define VideoWriterFfmpeg_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class VideoWriterFfmpeg;
typedef boost::shared_ptr<VideoWriterFfmpeg> VideoWriterFfmpegPtr;

}; // namespace ait

#endif // VideoWriterFfmpeg_fwd_HPP


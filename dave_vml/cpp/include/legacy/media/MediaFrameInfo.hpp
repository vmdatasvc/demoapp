/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef MediaFrameInfo_hpp
#define MediaFrameInfo_hpp

#include <legacy/pv/ait/Image.hpp>
#include <legacy/pv/ait/Image_fwd.hpp>

namespace ait
{

class MediaFrameInfo
{
public:

  MediaFrameInfo() : beginTime(0), endTime(0), frameIndex(0) {}

  /**
   * Begin and end times for this sample.
   */
  double beginTime, endTime;

  /**
   * Image data.
   */
  Image32Ptr pRGBImage;

  /**
   * Frame index.
   */
  int frameIndex;

};

typedef boost::shared_ptr<MediaFrameInfo> MediaFrameInfoPtr;

} // namespace ait

#endif // MediaFrameInfo_hpp

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ImageAcquireDeviceMatroxMeteor2_HPP
#define ImageAcquireDeviceMatroxMeteor2_HPP

#define MM_MAX_FRAME_BUFFERS 20

#include "ImageAcquireDeviceBase.hpp"
#include "mil.h"

namespace ait
{

class ImageAcquireDeviceMatroxMeteor2: public ImageAcquireDeviceBase
{
public:

  ///Constructor.
  ImageAcquireDeviceMatroxMeteor2(void);
  
  //Destructor
  virtual ~ImageAcquireDeviceMatroxMeteor2(void);
  
  void setMILProperty(ImageAcquireProperty& param, bool set);

protected:
  
  bool initialize(int numPrevBuffers = 0);
  bool unInitialize();
  int grabFrame();
  
  //Image32*& getReferenceToFrame(int frameIndex);
  
  bool isFirstFrame();
  void gotoFirstFrame();

private:
  
  void initBuffers(int frameWidth, int frameHeight, int numPrevBuffers);

  void checkError(void);
  
  // Matrox Image Lib Variables
  MIL_ID MilApplication,  /* Application identifier.  */
         MilSystem,       /* System identifier.       */
         MilDisplay,      /* Display identifier.      */
         MilDigitizer,    /* Digitizer identifier.    */ 
         MilImageDisp,    /* Image buffer identifier. */
         MilImageCompress;/* Image buffer identifier. */
  MIL_ID MilImage[MM_MAX_FRAME_BUFFERS];     /*  Frame Buffers */
  //MIL_ID* MilImage; //dont do this cant use new for some reason.
  
  int MilInitialization();
  
  /*
  // Handle to display view window
  HWND hWndMilDisplay;
  */
  
  std::vector<MIL_ID> vecMilIds;

  /**
   * This counter keeps the index of the next frame to read.
   */
  int mMilImageIndex;
  
  /*
  bool MilDisplayOn;  ///<compiled flag for showing the MIl display.
  bool DASdisplayOn;  ///<compiled flag for showing the DAS display.
  */
  
  
};

} //namespace ait;

#endif


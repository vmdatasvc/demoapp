/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ICameraDriverEvents_fwd_HPP
#define ICameraDriverEvents_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class ICameraDriverEvents;
typedef boost::shared_ptr<ICameraDriverEvents> ICameraDriverEventsPtr;

}; // namespace ait

#endif // ICameraDriverEvents_fwd_HPP


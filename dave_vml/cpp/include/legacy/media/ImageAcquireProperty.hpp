/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef IMAGEACQUIREPROPERTY_HPP
#define IMAGEACQUIREPROPERTY_HPP

#include <legacy/pv/PvUtil.hpp>

#include "legacy/low_level/Settings.hpp"

#include <string>
#include <map>

#include "legacy/media/ImageAcquireProperty_fwd.hpp"

namespace ait
{

class ImageAcquireProperty
{

public:

  typedef enum
  {
    INVALID_CODE = 0,
    BRIGHTNESS,                 // Camera Params Group Begin
    CONTRAST,
    HUE,
    SATURATION,
    SHARPNESS,
    GAMMA,
    COLORENABLE,
    WHITEBALANCE,
    BACKLIGHTCOMPENSATION,
    GAIN,                       // Camera Params Group End
    FPS,                        // Input format Begin
    SOURCEBITCOUNT,
    SOURCEWIDTH,
    SOURCEHEIGHT,               // Input format End
    PAN,                        // Camera Control Begin
    TILT,
    ROLL,
    ZOOM,
    EXPOSURE,
    IRIS,
    FOCUS,                      // Camera Control End
    WINDOW_POS_X,
    WINDOW_POS_Y,
    LAST_PROPERTY_CODE
  } Code;

  ImageAcquireProperty();
  ImageAcquireProperty(ImageAcquireProperty& c);

  virtual void getFromDevice() {};
  virtual void setToDevice() {};

  virtual ImageAcquireProperty *clone() { return new ImageAcquireProperty(*this); }

  const std::string getName() { return propertyNames[propertyCode]; }

  const Code getCode() { return propertyCode; }
  void setCode(const Code c) { propertyCode = c; }

  void getRange(float& min, float& max, float& step);
  void setRange(float min, float max, float step);

  float getDefault() { return defaultValue; }
  void setDefault(float val) { defaultValue = val; }

  bool getAuto() { return automatic; }
  void setAuto(bool a) { automatic = a; }

  bool getAvailable() { return available; }
  void setAvailable(bool a) { available = a; }

  float getValuef(bool getDevice = true) { if (getDevice) getFromDevice(); return value; }
  void setValuef(float val, bool setDevice = true) { value = val; if (setDevice) setToDevice(); }

  bool getInteractive() { return interactive; }
  void setInteractive(bool a) { interactive = a; }

  void writeToSettings(ait::Settings &set);
  void readFromSettings(ait::Settings &set, Code p);

  static const int getTotalProperties() { return LAST_PROPERTY_CODE; }

protected:

  Code propertyCode;
  float rangeMin;
  float rangeMax;
  float rangeStep;
  float value;
  float defaultValue;
  bool automatic;
  bool available;
  bool interactive;

private:
  static const char *propertyNames[];
};

class ImageAcquirePropertyBag
{
public:

  ImageAcquirePropertyBag(const std::string& settingsPath) : proto(new ImageAcquireProperty()), settings(settingsPath) {};
  ImageAcquirePropertyBag(const std::string& settingsPath, ImageAcquirePropertyPtr proto0) : proto(proto0), settings(settingsPath) {};
  ~ImageAcquirePropertyBag();

  void saveSettings();
  void loadSettings();

  ImageAcquirePropertyPtr get(ImageAcquireProperty::Code p);

protected:

  ait::Settings settings;
  std::map<ImageAcquireProperty::Code, ImageAcquirePropertyPtr> props;
  ImageAcquirePropertyPtr proto;
};

} // namespace ait

#endif //IMAGEACQUIREPROPERTY_HPP

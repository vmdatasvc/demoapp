/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef ImageAcquireDeviceDLL_HPP
#define ImageAcquireDeviceDLL_HPP

#include "ImageAcquireDeviceDLL_fwd.hpp"


#include <ImageAcquireDeviceBase.hpp>

#include <boost/thread/condition.hpp>
#include <boost/thread/mutex.hpp>

#include <Windows.h>

namespace ait
{

class ImageAcquireDeviceDLL : public ImageAcquireDeviceBase
{
public:  

  typedef DWORD CAMDRV_DRIVER;    

  typedef CAMDRV_DRIVER* (* CamDrvCreateType)();
  typedef int (* CamDrvDisposeType)(CAMDRV_DRIVER*);

  typedef int (* CamDrvInitType)(CAMDRV_DRIVER*, 
                                 const int camId,
                                 ImageAcquireDeviceDLL* pDevice);

  typedef int (* CamDrvGetParamType)(CAMDRV_DRIVER* drv, 
                                     const int camId, 
                                     const char* paramName, 
                                     char* value, 
                                     int maxLength);

  typedef int (* CamDrvSetParamType)(CAMDRV_DRIVER* drv,
                                     const int camId,
                                     const char* paramName,
                                     const char* value);

  CamDrvCreateType CamDrvCreate;
  CamDrvDisposeType CamDrvDispose;
  CamDrvGetParamType CamDrvGetParam;
  CamDrvSetParamType CamDrvSetParam;
  CamDrvInitType CamDrvInit;

  ///

  /**
   * Constructor
   */
  ImageAcquireDeviceDLL();

  /**
   * Destructor
   */
  virtual ~ImageAcquireDeviceDLL();

  /**
   * Initialize the DLL 
   */
  virtual bool initialize(int numPrevBuffers = 0, int numConsecutiveFrameGrabs = 1);

  /**
   * Uninitialize the DLL
   */
  virtual bool unInitialize();

  /**
   * TODO: DOC, DESCRIBE RETURN VALUES
   */
  virtual int grabFrame();

  /**
   * TODO: DOC
   */
  virtual int getTotalNumberOfFrames();

  /**
  * The next frame to be written to. Incoming frames are written here.
  */
  MediaFrameInfoPtr mpNextFrame;

  /**
  * This mutex is used to protect the image frame memory access
  */
  boost::mutex mFrameReadyMutex;

  /**
  * When grabFrame() is called, threads will wait on this condition before
  * returning. When a frame is received, this condition will be signaled.
  */
  boost::condition mIsFrameReady;

protected:

  void registerDLLFunctions();

  //handle for dynamically loading DLL files
  HINSTANCE mhLib;

  CAMDRV_DRIVER* mpCameraDriver;

  unsigned int mCameraId;  

  

  double mFps;

};

} // namespace ait

#endif //ImageAcquireDeviceDLL_HPP
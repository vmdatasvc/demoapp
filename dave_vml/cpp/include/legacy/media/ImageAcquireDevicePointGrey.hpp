/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
#ifndef ImageAcquireDevicePointGrey_hpp
#define ImageAcquireDevicePointGrey_hpp

// SYSTEM INCLUDES
//
#include <list>
#include <boost/utility.hpp>

// AIT INCLUDES
//

#include <digiclops.h>

// LOCAL INCLUDES
//

#include "ImageAcquireDeviceBase.hpp"

// FORWARD REFERENCES
//

namespace ait
{

/**
 * Acquire interface for the Point Grey Research Bumblebee camera.
 * The Bumblebee camera is treated as a pair of color cameras, ignoring its
 * stereo capability for now. We capture images from both cameras and present
 * them to the ImageAcquireControl through the ImageAcquireDeviceBase interface.
 * This class is reponsible for opening and closing the Bumblebee camera, setting
 * its capture resolution and other camera parameters, and capturing images from the
 * camera.
 * @par Responsibilities:
 * - Opening and closing camera.
 * - Capturing images from the camera
 * @par Patterns:
 * @par Design Considerations:
 * Any design decisions where dictated by the ImageAcquireDeviceBase class and the
 * Point Grey Research SDK.
 * @par Libraries
 * 
 * @remarks
 * Once we start using the Bumblebee as a "real" stereo camera this class should no
 * longer be used, and instead a class that delivers 3D data should be implemented.
 * @see ImageAcquireDeviceBase
 */
class ImageAcquireDevicePointGrey : public ImageAcquireDeviceBase, private boost::noncopyable
{
  struct CameraContext;
  class  CameraProperty;
  //
  // LIFECYCLE
  //
public:
  /// Constructor
  ImageAcquireDevicePointGrey();

  /// Destructor
	virtual ~ImageAcquireDevicePointGrey();

  //
  // OPERATORS
  //

  //
  // OPERATIONS
  //
public:
  /** 
  Specific initialization for specified device.
  
  @param numPrevBuffers [in] Number of previous buffers to use.
    
  @return 	true false;  succeed , fail.
  @remark  If reseting device call unInitialize() first.
  @see unInitialize()
  */
	bool initialize(int numPrevBuffers = 0, int numConsecutiveFrameGrabs = 1);

  /** 
  Specific de-initialization for specified device.
  
  @return 	true false;  succeed , fail.
  @remark  If reseting device call initialize() next.
  @see initialize()
  */
  bool unInitialize();

  /** 
  This function calls device specific grab versions.
  The 0 position will always hold the most current frame.
  After a call to grabFrame 0 will be hold the most current grab.
  If specified 1 = prev, 2 = prev+, 3 = prev++, etc...
  No copying is done.  Movement of pointers only.
  If supported by driver, initiation of next grab is begun before function
  exits.
    
  @return 	4 = DEVICE_SPECIFIC_GRAB_FAILED
  @remark  Call this to start the grab.  Use getPointerToFrame to get a pointer
           to the frame.  Note:  Frame is stored in grab buffers, therefore be quick 
           when using this memory.  Buffers will be over written.
  @see getPointerToFrame()
  */
  int grabFrame();

  /**
  Sets a image capture property (brightness, color balance, etc.)

  @param CameraProperty& [in/out] the property to set
  @param bool [in] true == set the property false == update property with current value
  */
  void setProperty(CameraProperty&, Bool);
  

  //Legacy Funcs.
  bool isFirstFrame() { return false; }
  void gotoFirstFrame() { }
protected:
  /**
  Allocate the frame buffers in the ring.
  */
  Bool CreateFrameBuffers();

  //
  // ATTRIBUTES
  //
protected:
  /**
  This class stores the information needed to manage a Point Grey device. 
  */
  typedef boost::shared_ptr<CameraContext> CameraContextPtr;

  /**
  The contexts for all Point Grey devices. Static because every two succesive instances of this 
  class have the same CameraContext.
  */
  static std::vector<CameraContextPtr> mvContext;

  /**
  The index of the Point Grey device this class works with. This is
  the index into the mvContext vector above.
  */
  Int mnPointGreyDevice;

  /**
  Which (of the two Point Grey cameras) this class instance works with.
  */
  Int mnCameraIndex;

  /**
  Size of the image
  */
  Int mnWidth;
  Int mnHeight;
};
};
#endif // ImageAcquireDevicePointGrey_hpp

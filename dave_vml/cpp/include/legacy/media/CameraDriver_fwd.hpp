/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef CameraDriver_fwd_HPP
#define CameraDriver_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class CameraDriver;
typedef boost::shared_ptr<CameraDriver> CameraDriverPtr;

}; // namespace ait

#endif // CameraDriver_fwd_HPP


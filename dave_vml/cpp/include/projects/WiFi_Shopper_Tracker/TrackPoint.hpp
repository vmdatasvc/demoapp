/*
 * TrackPoints.hpp
 *
 *  Created on: Jan 26, 2017
 *      Author: jwallace
 */

#ifndef WIFI_SHOPPER_TRACKER_TRACKPOINTS_HPP_
#define WIFI_SHOPPER_TRACKER_TRACKPOINTS_HPP_

#include <boost/date_time/posix_time/posix_time.hpp>

class TrackPoint {
public:
	TrackPoint(const boost::posix_time::ptime& t, float x, float y, float x_se, float y_se, float xy_cov)
		: timestamp(t), _x(x), _y(y), _x_se(x_se), _y_se(y_se), _xy_cov(xy_cov), _sent(false) {}

	// Default destructor, copy ctor, assignment operator are just fine!

	bool operator<(const TrackPoint& o) const { return timestamp < o.timestamp; }

	void setSent() {_sent = true;}

	const boost::posix_time::ptime& getTimestamp() const {return timestamp;}
	float getX() const {return _x;}
	float getY() const {return _y;}
	float getX_se() const {return _x_se;}
	float getY_se() const {return _y_se;}
	float getCov_XY() const {return _xy_cov;}
	bool isSent() const {return _sent;}

private:
	boost::posix_time::ptime timestamp; // time of the localization estimate
	float _x; // best guess for the (x,y) location
	float _y;
	float _x_se; // some error estimates in the (x,y) direction
	float _y_se;
	float _xy_cov; // The covariance between _x_se and _y_se (needed for rotating ellipses)

	// has this point been sent via realtime reporting?
	bool _sent;
};

// comparison operator for pointers to TrackPoint objects
namespace std{

template <>
struct less<TrackPoint*>{
	bool operator()(const TrackPoint* x, TrackPoint* y) const {
		return (y!=0 && x!= 0) ? (*x) < (*y) : y < x;
	}
};

}

#endif /* SRC_PROJECTS_WIFI_SHOPPER_TRACKER_TRACKPOINTS_HPP_ */

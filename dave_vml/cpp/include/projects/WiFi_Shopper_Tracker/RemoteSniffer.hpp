/*
 * RemoteSniffer.hpp
 *
 *  Created on: Jan 24, 2017
 *      Author: jwallace
 */

#ifndef SRC_PROJECTS_WIFI_SHOPPER_TRACKER_REMOTESNIFFER_HPP_
#define SRC_PROJECTS_WIFI_SHOPPER_TRACKER_REMOTESNIFFER_HPP_

#include "modules/iot/MQTTClient.hpp"

#include "modules/iot/IoT_Common.hpp"

#include <string>
#include <cmath>

#include <iostream>

class RemoteSniffer {
public:
	RemoteSniffer(const std::string& hostname, float x, float y, float z, const std::string& name, const std::string& localApp=APP_ID_WIFI_TRACKER);
	virtual ~RemoteSniffer(){}

	float getX() const {return _x;}
	float getY() const {return _y;}
	float getZ() const {return _z;}

	// A remote sniffer is alive IF (and only if)
	// Both the remote sender and receiver are connected to the
	// remote MQTT broker AND have seen at least one packet
	// AND we have received a heartbeat from the sniffer app in the
	// last 5 minues.
	bool isAlive() const {
		return remote_recv.isConnected() && saw_packet &&
			   static_cast<unsigned int>(abs((boost::posix_time::microsec_clock::universal_time() - last_seen).total_seconds())) < MAX_HEARTBEAT_INTERVAL;
	}

	bool setInjecting(bool param);

	const std::string& getName() const { return _name; }

	// get the squared euclidean distance between two sniffers
	// NOTE: we are attempting to use
	const float getSqDistance(const RemoteSniffer& o) const{
		return std::pow(_x - o.getX(), 2) + std::pow(_y - o.getY(), 2) + std::pow(_z - o.getZ(), 2);
	}

private:
	// MQTT receiver to listen to remote packets
	MQTTReceiver remote_recv;
	// MQTT sender to push commands to remote sniffer
	MQTTSender remote_send;
	// MQTT sender to push data to local broker
	MQTTSender local_send;

	// local topic to push data to
	std::string localTopic;

	// remote command topic
	std::string cmdTopic;

	// time we last saw a heartbeat from the sniffer
	boost::posix_time::ptime last_seen;

	bool saw_packet;

	// callback to transfer the packet to the local MQTT broker
	void aggregateData(const struct mosquitto_message *message);

	// callback to respond to the heartbeat.
	void heartbeatResponse(const struct mosquitto_message *message);

	const float _x;
	const float _y;
	const float _z;

	const std::string _name;

	// maximum number of seconds between a heartbeat
	// Let's set it to 5 minutes, please!
	static const unsigned int MAX_HEARTBEAT_INTERVAL = 300;
};

#endif /* SRC_PROJECTS_WIFI_SHOPPER_TRACKER_REMOTESNIFFER_HPP_ */

/*
 * Localizer.hpp
 *
 *  Created on: Jan 26, 2017
 *      Author: jwallace
 */

#ifndef WIFI_SHOPPER_TRACKER_LOCALIZER_HPP_
#define WIFI_SHOPPER_TRACKER_LOCALIZER_HPP_

#include "TrackPoint.hpp"
#include "WiFiTracker.hpp"

#include "modules/wifi/TrackerPacket.hpp"

#include <boost/array.hpp>

#include <map>
#include <string>

// NOTE: The PacketPtr is a "pointer" to a const TrackerPacket.  It is an iterator that
// must adhere to the ForwardIterable concept.

// You can assume that the following operations are defined for a TrackerPacket p:
// dereference (*p == const TrackerPacket)
// pointer operations (p->fn(), equivalient to (*p).fn())
// Single Increment (pre/post): ++p/p++
// Default construction, copy construction, and assignment

template <class PacketPtr>
class Localizer {
public:
	Localizer(WiFiTracker* t) : tracker(t) {}
	virtual ~Localizer() {}

	// Returns a newly allocated TrackPoint object based on the set of packets
	// If there was an error in localiztion, returns null
	// For convenience of preallocation, we'll also provide the number of packets in the ping storm
	virtual TrackPoint* localize(const PacketPtr& begin, const PacketPtr& end, unsigned int n_packets) = 0;


protected:
	// If you need to access anything in the WiFiTracker, you'll need to
	// add a proxy access function here.

	// I KNOW I'm going to need the calibration parameters
	const WiFiTracker::calib_param_t& getCalibParams() const {return tracker->calib_params;}

	// I'll need the maximum number of sensors, to preallocate
	unsigned int getNumSensrs() const {return tracker->remote_monitors.size();}

	// I'll need to know if I want to separate the channel measurements, or compress
	// them all into a single channel
	bool indepChannels() const {return tracker->calib_indep_channel;}

	// Get a RemoteSniffer object, given its name (or NULL if cannot be found)
	const RemoteSniffer* getRemote(const std::string& name) const{
		auto n_it = tracker->remote_name_map.find(name);
		if(n_it == tracker->remote_name_map.end()){
			return 0;
		} else {
			return n_it->second;
		}
	}

	unsigned int getMinTrilaterationSensrs() const{
		return tracker->min_trilateration_sensrs;
	}

	unsigned int getMaxTrilaterationSensrs() const{
		return tracker->max_trilateration_sensrs;
	}

	float getMaxDistance() const{
		return tracker->max_trilateration_distance;
	}

	bool reportRealtime() const{
		return tracker->realtime_reporting;
	}

	template <class T>
	void sendRealtimeData(const T& msg, const std::string& subtopic = ""){
		std::string topic="realTimeLocs";
		if(subtopic.size() > 0){
			topic += "/" + subtopic;
		}
		if(tracker->public_realtime){
			tracker->sendData(msg, topic);
		}
		tracker->sendPrivateData(msg, topic);
	}

	double convertTime(const boost::posix_time::ptime& ts){
		return tracker->convertTime(ts);
	}

private:

	WiFiTracker* tracker;
};

#endif /* INCLUDE_PROJECTS_WIFI_SHOPPER_TRACKER_LOCALIZER_HPP_ */

/*
 * WiFiAggregator.hpp
 *
 *  Created on: Feb 23, 2017
 *      Author: jwallace
 */

#ifndef WIFI_SHOPPER_TRACKER_WIFIAGGREGATOR_HPP_
#define WIFI_SHOPPER_TRACKER_WIFIAGGREGATOR_HPP_

#include "core/OmniSensrApp.hpp"

#include "RemoteSniffer.hpp"

#include <string>
#include <deque>

#include <boost/thread.hpp>
#include <boost/thread/condition.hpp>

class WiFiAggregator : public OmniSensrApp {
public:
	WiFiAggregator(const std::string& configfn);
	virtual ~WiFiAggregator();

private:
	WiFiAggregator(const WiFiAggregator&);
	WiFiAggregator& operator=(const WiFiAggregator&);

protected:

	virtual void start();

	virtual unsigned int initConfig(ParseConfigTxt& parser);

	virtual void saveState(std::ostream& os) const;

private:

	bool readCamList(const std::string& fn);


	// mutex and condition for waking the mainloop
	boost::mutex main_lock;
	boost::condition main_cond;

	unsigned int heartbeat_interval;
	std::string cam_detail_fn;

	std::deque<RemoteSniffer*> remote_monitors;

	bool killed;

	static const std::string appName;

};

#endif /* SRC_PROJECTS_WIFI_SHOPPER_TRACKER_WIFIAGGREGATOR_HPP_ */

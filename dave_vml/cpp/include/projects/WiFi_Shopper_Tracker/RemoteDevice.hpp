/*
 * RemoteDevice.hpp
 *
 *  Created on: Jun 27, 2014
 *      Author: pshin
 */

#ifndef REMOTEDEVICE_HPP_
#define REMOTEDEVICE_HPP_

#include <deque>
#include <tuple>
#include <opencv2/core.hpp>

#include "modules/wifi/MessageType.hpp"
#include "modules/utility/KalmanFilter_4S.hpp"

#include "projects/WiFi_Shopper_Tracker/Config.hpp"
#include "projects/WiFi_Shopper_Tracker/MonitoringAP.hpp"

using namespace cv;
using namespace std;

typedef struct  {
	int timediff_hist[TIMEDIFF_HIST_LENGTH]; 	// -- Histogram of time diff between consecutive two RSS samples. (Note: yet a burst of packets in a second will be considered only once)
			// -- This stat is not the same as every timediff of two consecutive measurements. A sampling period starts from the time when the previous measurement is received, and ends if a measurement is newly received after at least one second later.
			// -- If there's another or multiple measurements within a second from the previous measurement, then it will be marked and considered only once when receiving another measurement after at least one second away from the previously stored measurement.

	float timediff_cdf[gNo_timediff_bin_width];	// -- Total count percentage in each time interval bin
	unsigned int timediff_total_cnt;			// -- Total # of timediff measurements in the TimeDiff Histogram
	time_t	time_prev;
}  Temporal_Stat_t;

typedef struct  {
	int rss_hist_per_ch[MAX_CHANNELS][MAX_RSS_IND_SIZE];		// -- Histogram of RSS measurements per channel among 11 channels, and rss between 0 and -100 w/ interval 5
	int rss_hist_per_ch_cnt;					// -- Total # of RSS measurements stored in the RSS histogram so far

	int rss_hist_per_ch_r[MAX_CHANNELS][MAX_RSS_IND_SIZE];	// -- Histogram of refined RSS measurements per channel among 11 channels, and rss between 0 and -100 w/ interval 5
	int rss_hist_per_ch_r_cnt;					// -- Total # of refined RSS measurements stored in the RSS histogram so far

	int rss_hist[MAX_RSS_IND_SIZE];						// -- Histogram of RSS measurements for all the channels, and rss between 0 and -100 w/ interval 5
	int rss_hist_cnt;

	int rss_hist_filtered[MAX_RSS_IND_SIZE];						// -- Histogram of RSS measurements for all the channels, and rss between 0 and -100 w/ interval 5
	int rss_hist_filtered_cnt;
}  RSS_Stat_t;

typedef struct  {
	deque<float> distError;
	double distErrorAvg;
	double distErrorStd;
}  SpatialAccuracy_Stat_t;


class RemoteDevice {

private:

	//bool mIsActivated;

	int mColor_r, mColor_g, mColor_b;

	string	mDeviceAddr;			// -- MAC Address of this WiFi device
	string	mDeviceManuf;			// -- Manufacturer of this WiFi device
	int mDeviceType;				// -- Station, AP, ServiceAgent, or NullDevice

	//bool mIsDevOfInterest;
	double mTotalShoppingTime, mTotalShoppingTravelDist;
	int mTotalLocalizationCntDuringShopping;

	vector<tcpdump_t>	mMeasBuffer;	// -- Temporary storage of measurements to be processed after a collection process
	deque<TrackedPos_t_>	mTrackedPos;			// -- A trajectory of a mobile device in terms of (timestamp, x, y)
	int mTrackedPos_m_age;

	unsigned int mTimediff_bin_width_accumulated[gNo_timediff_bin_width];			// -- Accumulated count in each time interval bin

public:
	RemoteDevice();
	virtual ~RemoteDevice();

	bool mTobeRemoved;		// -- Indicate if this device should be deactivated and removed

	RSS_Stat_t		mRSS_stat;
	Temporal_Stat_t mTime_stat;
	SpatialAccuracy_Stat_t mAccuracyStat;

	int mChannel;				// -- If this device is an AP, then it will operate at a quite static channel. This will be the actual channel (not channel index)
	int mChannel_mismatch_cnt;	// -- Once an AP's channel is confirmed, if it is observed that the AP gets a lot of traffic in a different channel, then the confirmAPchannel process will be re-initiated.
	int mProtocolType;				// -- 802.11 protocol type among b/g/n

	double mDeviceInitialTime;		// -- The initial time that this device appeared for the first time
	double mSiteInitialTime;		// -- The initial time that this monitoring started

	KalmanFilter_4S		mKalmanPos_m;	// -- Estimate the current physical location of the device
	//KalmanFilter_4S		mKalmanAP_Signals[MAX_RUNNING_APS];	// -- Estimate the current RSS value of the device captured by each AP
	list<KalmanFilter_4S> 	mKalmanAP_Signals;				// -- Estimate the current distance of the device to the AP that captures its packets

	int mTrackCorruption_age;

	//deque<WiFiSensor_short_Ptr> mChosenSensorsForTriangulation;
	vector<tuple<WiFiSensor_short_Ptr, double, double> > mChosenSensorsForTriangulation;
	vector<pair<WiFiSensor_short_Ptr, double> > mMeasForTriangulation;
	//vector<pair<WiFiSensor_short_Ptr, double> > m_ChosenSensorsForTriangulation;
	//vector<CvPoint> mChosenSensorsForTriangulation_center;
	CvPoint mChosenSensorsForTriangulation_center_curr;
	vector<Point2f> mChosenSensorsConvexHullPts;  // Convex hull points

	float mPerDevRssCalibOffset;

	// -- Record error distance compared to ground truth
#ifdef USE_WIFI_PLAYER
	deque<AccuracyEval_Point_t> mAccuEval;
#endif

	// -- Variable for periodic buffer processing
	double time_prevBufferProcessed;		// -- Mark the time when the packet buffer is processed previously.

	void DevInitialize();

	void ClearKalmanSignalList() {	mKalmanAP_Signals.clear(); }
	void AddKalmanSignal(KalmanFilter_4S kalman) {	mKalmanAP_Signals.push_back(kalman); }
	unsigned int GetKalmanSignalListSize() { return mKalmanAP_Signals.size(); }

	void SetDevColor(int r, int g, int b) { mColor_r = r; mColor_g = g; mColor_b = b;}
	cv::Scalar GetDevColor() { return cv::Scalar(mColor_b, mColor_g, mColor_r);}

	void AddToTimeDiffHistogram(tcpdump_t meas);
	void ConstructTimeDiffCDF();

	void ShowStats_RssHist_PerChannel();
	void ShowStats_RssHist_PerChannel_Revised();
	void ShowStats_RssHist_Total();
	void ShowStats_TimeDiffHistCDF();

	void ClearMeasBuf() { mMeasBuffer.clear(); }
	unsigned int GetMeasBufSize() {	return (uint)mMeasBuffer.size(); }
	vector<tcpdump_t> *AtMeasBuf() { return &mMeasBuffer; }
	bool PutToMeasBuf(tcpdump_t meas, int maxSize);

	unsigned int GetTrackedPosBufSize() { return mTrackedPos.size();}
	void ClearTrackedPosBuf() {mTrackedPos.clear();}
	int GetTrackedPosAge() { return mTrackedPos_m_age;}
	int *AtTrackedPosAge() { return &mTrackedPos_m_age;}
	deque<TrackedPos_t_> *AtTrackedPosBuf() { return &mTrackedPos; }
	void PutToTrackedPosBuf(TrackedPos_t_ pos, int maxSize);

	string GetDevAddr() { return mDeviceAddr; }
	void SetDevAddr(string newAddr) { mDeviceAddr = newAddr; }

	//void SetDevOfInterest(bool isDevOfInterest) { mIsDevOfInterest = isDevOfInterest; }
	//bool IsDevOfInterest() { return mIsDevOfInterest; }

	string GetDevManuf() { return mDeviceManuf;}
	void SetDevManuf(string manuf) { mDeviceManuf = manuf; }
	bool IsDeviceBrandNull() { return mDeviceManuf.compare("NULL") == 0; }

	int GetDeviceType() { return mDeviceType;}
	void SetDeviceType(int type_) { mDeviceType = type_;}

	double GetTotalShoppingTime() { return mTotalShoppingTime; }
	void SetTotalShoppingTime(double shoppingTime) { mTotalShoppingTime = shoppingTime; }

	int GetTotalLocalizationDuringShopping() { return mTotalLocalizationCntDuringShopping; }
	void SetTotalLocalizationDuringShopping(int shoppingCnt) { mTotalLocalizationCntDuringShopping = shoppingCnt; }

	double GetTotalShoppingTravelDist() { return mTotalShoppingTravelDist;}
	void SetTotalShoppingTravelDist(double distTravel) { mTotalShoppingTravelDist = distTravel;}

};


#endif /* REMOTEDEVICE_HPP_ */

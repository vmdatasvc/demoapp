#ifndef __GUI_H__
#define __GUI_H__

//#define CAM_RES_X 640
//#define CAM_RES_Y 480

#define CAM_RES_X 128
#define CAM_RES_Y 128

//#define CAM_RES_X	320
//#define CAM_RES_Y	240

#include <vector>
#include <ostream>
#include <istream>

using namespace std;

//#include "clusters.h"
#include <opencv2/core.hpp>
#include <opencv2/core/core_c.h>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include "MonitoringAP.hpp"

using namespace cv;

typedef struct {
	int x;
	int y;
} point_type;

typedef vector< point_type > VCT_POINTS;


enum {DS_SHOW_MARKERS=1, DS_SHOW_SENSORS=2, DS_SHOW_FURNITURE=4, DS_SHOW_TRACK=8};
//***************************************************************************
//Draws the uncertainty ellipse
//***************************************************************************
void draw_uncertainty_ellipse(int obj_cmd, int obj_id, float x, float y, IplImage *img, CvMat *cov_matrix, CvScalar color, int room_dim_x, int room_dim_y, float chi);
//***************************************************************************
//This function draws a dashed line
//***************************************************************************
void draw_dashed_line(IplImage *img, CvPoint p1, CvPoint p2, CvScalar color);
//***************************************************************************
//This function shows the position of the ball in the room
//***************************************************************************
void show_room(float x, float y, IplImage *img,char *win_name, int room_dim_x, int room_dim_y);
void show_map(float x, float y, Mat &img, char *win_name, int room_dim_x, int room_dim_y);
//***************************************************************************
//This function shows the markers on the floor
//***************************************************************************
void show_markers(int x_bot, int y_bot, int x, int y, IplImage *img, int room_dim_x,int room_dim_y);
//***************************************************************************
//This function shows the furniture in the room
//***************************************************************************
void show_furniture(IplImage *img, int room_dim_x,int room_dim_y) ;
//***************************************************************************
//Shows the cameras on the room
//***************************************************************************
void show_cameras(void *cluster_list, int num_cameras, CvMat **H, IplImage *img, int room_dim_x, int room_dim_y);
//***************************************************************************
//Shows the WiFi sensors on the room
//***************************************************************************
void show_sensors(WiFiSensor_short_Ptr ap, Mat &img, float fontSizeScale);
//***************************************************************************
//Shows lines on the room
//***************************************************************************
void show_line(CvPoint p1, CvPoint p2, IplImage *img, CvScalar color, int room_dim_x, int room_dim_y);
//***************************************************************************
//Shows alert message
//TODO: should make struct alert a parameter
//***************************************************************************
void show_alert(int type, int xpos, int ypos, IplImage *img, int room_dim_x, int room_dim_y);
//***************************************************************************
//DEBUG: show robot destination
//***************************************************************************
void show_dest(int xpos,int ypos,IplImage *img, int room_dim_x, int room_dim_y);
//***************************************************************************
//Shows previous target positions
//***************************************************************************
void show_positions(VCT_POINTS *point,IplImage *img, int room_dim_x, int room_dim_y);

#endif

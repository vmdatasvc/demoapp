/*
 * KalmanLocalizer.hpp
 *
 *  Created on: Feb 21, 2017
 *      Author: jwallace
 */

#ifndef WIFI_SHOPPER_TRACKER_KALMANLOCALIZER_HPP_
#define WIFI_SHOPPER_TRACKER_KALMANLOCALIZER_HPP_

#include "Localizer.hpp"

template <class P>
class KalmanLocalizer : public class Localizer<P> {

public:
	KalmanLocalizer(WiFiTracker* t) : Localizer(t) {}
	virtual ~KalmanLocalizer() {}

	virtual TrackPoint* localize(const P& begin, const P& end, unsigned int n_packets);
};



#endif /* INCLUDE_PROJECTS_WIFI_SHOPPER_TRACKER_KALMANLOCALIZER_HPP_ */

/*
 * OLSLocalizer.hpp
 *
 *  Created on: Jan 27, 2017
 *      Author: jwallace
 */

#ifndef WIFI_SHOPPER_TRACKER_OLSLOCALIZER_HPP_
#define WIFI_SHOPPER_TRACKER_OLSLOCALIZER_HPP_

#include "Localizer.hpp"

// Needed for OLS regression
#include "OLSRegression.hpp"

#include "json.hpp"
#include <map>

// A class that runs localization by finding the distance to the sensor
// via ordinary least squares (hence, OLSLocalizer), then uses some trilateration
// algorithm, likely ALSO using OLS.

template <class P>
class OLSLocalizer : public Localizer<P> {

public:
	OLSLocalizer(WiFiTracker* t) : Localizer<P>(t) {}
	virtual ~OLSLocalizer() {}

	virtual TrackPoint* localize(const P& begin, const P& end, unsigned int n_packets);

};

template <class P>
TrackPoint* OLSLocalizer<P>::localize(const P& begin, const P& end, unsigned int n_packets){

	// Here, I have a number of OmniSensrs, each with a number of measurements
	// I want to construct a set of equations corresponding to:
	// RSS_ch_OS = P_ch + a_0 + a_1* log(dist_OS)
	// above, P_ch is the constant transmit power for this "ping storm" (one per channel), and
	// dist_OS is the distance to each OmniSensr that received the packet.
	// a_0 and a_1 are known quantities from the calibration.

	// Turning this into a matrix equation, we have:
	// RSS_ch_OS - a_0 = P_ch + a_1 * log(dist_OS)
	// Assuming that P is a nx3 matrix, and D is a nxm matrix (m = =# of OmniSensrs), then
	// we can define A = [ P | D ], b = RSS_ch_OS - a0, then solve A*x = b

	// NOTE: if tracker compresses measurements for all channels, I only need one P_ch variable
	bool indep_ch = this->indepChannels();

	// the actual calibration parameters
	const WiFiTracker::calib_param_t& calib_params = this->getCalibParams();

	// map of Sensor name to index of distance
	std::map<std::string, unsigned int> sensr_idx_map;

	// Matrix used
	Eigen::MatrixXf d_A = Eigen::MatrixXf::Zero(n_packets, 1 + 2*indep_ch + this->getNumSensrs());
	// Vector
	Eigen::VectorXf d_b(n_packets);
	// Weight (by R^2)
	Eigen::VectorXf d_w = Eigen::VectorXf::Ones(n_packets);

	boost::posix_time::ptime last_time;

	std::string dev_name = "";
	if(begin != end){
		dev_name = begin->getPayload().getMAC_str();
	}

	nlohmann::json input_json;
	input_json["input"] = nullptr;

	// go through the packets and set up the matrix and vector being used
	unsigned int nrow = 0;
	for(auto beg = begin; beg!= end; beg++){
		last_time = beg->getTimestamp();

		auto c_itr = calib_params.find(beg->getName());
		if(c_itr != calib_params.end()){
			unsigned int ch_idx = (beg->getPayload().getChannel() - 1) / 5 * indep_ch;
			d_A(nrow, ch_idx) = 1;

			auto m_itr = sensr_idx_map.find(beg->getName());
			if(m_itr == sensr_idx_map.end()){
				m_itr = sensr_idx_map.insert(sensr_idx_map.end(), std::pair<std::string, unsigned int>(beg->getName(), sensr_idx_map.size() + 1 + 2*indep_ch));
			}

			input_json["input"][beg->getName()].push_back({beg->getPayload().getChannel(), beg->getPayload().getRSS()});

			d_A(nrow, m_itr->second) = c_itr->second[1 + 2*ch_idx];
			d_b(nrow) = beg->getPayload().getRSS() - c_itr->second[2*ch_idx];
			d_w(nrow) = c_itr->second[6];

			++nrow;
		}
	}

	double ts = this->convertTime(last_time);

	if(this->reportRealtime()){
		input_json["name"] = dev_name;
		input_json["time"] = ts;

		this->sendRealtimeData(input_json, "input");
	}


	auto A_sub = d_A.topLeftCorner(nrow, 1 + 2*indep_ch + sensr_idx_map.size());
	auto b_sub = d_b.head(nrow);
	auto w_sub = d_w.head(nrow);

	Eigen::MatrixXf d_cov;
	Eigen::VectorXf d_x;
	float d_chisq = 0;

	OLSRegression::weighted_regression<float>(A_sub, w_sub, b_sub, d_x, d_cov, d_chisq);

	if(this->reportRealtime()){
		nlohmann::json result;
		// construct a JSON message
		result["name"] = dev_name;
		result["time"] = ts;
		result["power"] = nlohmann::json::array();
		result["power_se"] = nlohmann::json::array();
		for(int i=0; i<1+2*indep_ch; i++){
			result["power"].push_back(d_x[i]);
			result["power_se"].push_back(sqrt(d_cov(i,i)));
		}
		result["distance"] = nullptr;
		for(auto m_it = sensr_idx_map.begin(); m_it != sensr_idx_map.end(); m_it++){
			result["distance"][m_it->first] = {d_x[m_it->second], sqrt(d_cov(m_it->second, m_it->second)) };
		}

		this->sendRealtimeData(result, "distance");
	}

	//std::cout << "Dist:" << std::endl << x << std::endl;

	//TODO: Weight the next OLS regression according to the inverse variance of the distance pairs
	// Note that we here have the mean and variation of the log of the distance (i.e., distance is lognormally distributed)
	// The given X~N(\mu,\sigma), then Var[e^X] = (exp(\sigma^2) - 1) * exp(2*\mu + \sigma^2)

	std::multimap<float, const RemoteSniffer*> sensr_dist_map;
	for(auto m_it = sensr_idx_map.begin(); m_it != sensr_idx_map.end(); m_it++){
		const RemoteSniffer* rem = this->getRemote(m_it->first);
		if(rem){
			float sqdist_2d = exp(2*d_x(m_it->second)) - rem->getZ() * rem->getZ();
			if(sqdist_2d > 0){
				sensr_dist_map.insert(std::pair<float, const RemoteSniffer*>(sqdist_2d, rem));
			}
		}
	}


	// Now, pare down the sensr_dist map to those that have at least as many sensrs as I need
	// Note that this code will choose the closest sensors up to the maximum number of sensors
	unsigned int n_sensr = 0;
	unsigned int max_sensr = this->getMaxTrilaterationSensrs();
	float max_sqdist = this->getMaxDistance() * this->getMaxDistance();

	for(auto it=sensr_dist_map.begin(); it!=sensr_dist_map.end(); ){
		if(it->first > max_sqdist || n_sensr > max_sensr){
			// erase everything, INCLUDING the current
			sensr_dist_map.erase(it, sensr_dist_map.end());
			// this will break us out of the loop
			it = sensr_dist_map.end();
		} else {
			// just move on to the next element
			++it;
			++n_sensr;
		}
	}

	// If we do not have a sufficient number of sensors for trilateration, exit here
	if(sensr_dist_map.size() < this->getMinTrilaterationSensrs()){
		if(this->reportRealtime()){
			nlohmann::json result;
			result["name"] = dev_name;
			result["time"] = ts;
			result["num_sensors"] = sensr_dist_map.size();
			result["distance"] = nullptr;
			for(auto m_it = sensr_idx_map.begin(); m_it != sensr_idx_map.end(); m_it++){
				result["distance"][m_it->first] = {d_x[m_it->second], sqrt(d_cov(m_it->second, m_it->second)) };
			}

			this->sendRealtimeData(result, "error");
		}
		return 0;
	}


	// Perform trilateration.  See http://stackoverflow.com/questions/9527432/math-3d-positioning-multilateration
	// for how to linearize the solution so that we can use OLS regression.  WOOT!

	// Essentially, we have a system of eq'ns that look like:
	// (x - x_i)^2 + (y-y_i)^2 = d_i^2
	// Looking at the i and j equations together, we have:
	// (x - x_i)^2 - (x - x_j)^2 + (y - y_i)^2 - (y - y_j)^2 = d_i^2 - d_j^2
	// Expanding the algebra will cancel teh squared terms and give us:
	// 2*(-x_i + x_j)*x + x_i^2 - x_j^2 + 2*(-y_i + y_j)*y + y_i^2 - y_j^2 = d_i^2 - d_j^2
	// Moving the constants to the RHS and diving by 2 gives:
	// (x_j - x_i)*x + (y_j - y_i)*y = (d_i^2 - d_j^2 + x_j^2 - x_i^2 + y_j^2 - y_i^2)/2

	// Number of rows is n choose 2, or n*(n-1)/2
	unsigned int nrows = (sensr_dist_map.size() * (sensr_dist_map.size() - 1)) / 2;
	// Just need an x and y
	unsigned int ncols = 2;

	// 1st column is x_pos, 2nd column is y_pos
	Eigen::MatrixXf A(nrows, ncols);
	Eigen::VectorXf b(nrows);

	unsigned int nr = 0;
	for (auto it_i = sensr_dist_map.begin(); it_i != sensr_dist_map.end(); it_i++){
		for(auto it_j = it_i; it_j!= sensr_dist_map.end(); it_j++){
			if(it_j != it_i){
				float x_i = it_i->second->getX();
				float y_i = it_i->second->getY();
				float x_j = it_j->second->getX();
				float y_j = it_j->second->getY();

				A(nr, 0) = x_j - x_i;
				A(nr, 1) = y_j - y_i;

				// (d_i^2 - d_j^2 + x_j^2 - x_i^2 + y_j^2 - y_i^2)/2
				b(nr) = (it_i->first - it_j->first + x_j*x_j - x_i*x_i + y_j*y_j - y_i*y_i) / 2;

				++nr;
			}
		}
	}

	Eigen::VectorXf w = Eigen::VectorXf::Ones(nrows);

	Eigen::MatrixXf cov;
	Eigen::VectorXf x;
	float chisq;

	OLSRegression::weighted_regression<float>(A, w, b, x, cov, chisq);

	if(this->reportRealtime()){
		nlohmann::json result;
		result["name"] = dev_name;
		result["time"] = ts;
		result["x"] = x(0);
		result["y"] = x(1);
		result["x_se"] = sqrt(cov(0,0));
		result["y_se"] = sqrt(cov(1,1));
		result["cov"] = cov(0,1);

		this->sendRealtimeData(result, "points");
	}

	//std::cout << "Sol:" << std::endl << x << std::endl;

	// Now, create the TrackPoint object:
	return new TrackPoint(last_time, x(0), x(1), sqrt(cov(0,0)), sqrt(cov(1,1)), cov(0,1));

}


#endif /* WIFI_SHOPPER_TRACKER_OLSLOCALIZER_HPP_ */

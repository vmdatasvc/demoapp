/*
 * OLSRegression.hpp
 *
 *  Created on: Feb 16, 2017
 *      Author: jwallace
 */

#ifndef WIFI_SHOPPER_TRACKER_OLSREGRESSION_HPP_
#define WIFI_SHOPPER_TRACKER_OLSREGRESSION_HPP_

#include <Eigen/Core>
#include <Eigen/SVD>

class OLSRegression{
public:
	// A weighted regression, solving A*c = b, with weights w.
	// For details on the parameters, see the GSL documentation
	// https://www.gnu.org/software/gsl/manual/html_node/Multi_002dparameter-regression.html#Multi_002dparameter-regression
	// Method of interest is gsl_multifit_wlinear
	//
	// Alse, see source at: https://sourcecodebrowser.com/praat/5.1.0/gsl__multifit_8h.html#a91710ff55dc2fccf1be144c38dc48ecb
	template <class T>
	static int weighted_regression(const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& X,
			const Eigen::Matrix<T, Eigen::Dynamic, 1>& w,
			const Eigen::Matrix<T, Eigen::Dynamic, 1>& y,
			Eigen::Matrix<T, Eigen::Dynamic, 1>& c,
			Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& cov,
			T& chisq);
};

template <class T>
int OLSRegression::weighted_regression(const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& X,
		const Eigen::Matrix<T, Eigen::Dynamic, 1>& w,
		const Eigen::Matrix<T, Eigen::Dynamic, 1>& y,
		Eigen::Matrix<T, Eigen::Dynamic, 1>& c,
		Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& cov,
		T& chisq){

	// TODO: add some sanity checks
	if(X.rows() != y.rows()){
		// Number of observations in X does not match y
		return -1;
	}

	if(X.rows() != w.rows()){
		// NUmber of observations in w does not match w
		return -1;
	}

	// Get the sqrt of the weight vector
	Eigen::Matrix<T, Eigen::Dynamic, 1> sqrtw = w.array().sqrt();

	// Scale out input matrix
	// A = sqrt(w) * X
	Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> A(X);
	for(int i=0; i<A.rows(); i++){
		A.row(i) = A.row(i) * sqrtw(i);
	}

	// calculate the SVD of A
	Eigen::JacobiSVD<Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> > svd = A.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV);

	// Scale the RHS by the sqrt of the weight vector
	Eigen::Matrix<T, Eigen::Dynamic, 1> b = sqrtw.array() * y.array();

	// Solve A*x = b
	c = svd.solve(b);

	// Now, find the covariance matrix cov = (QS^-1) * (QS^-1)^T
	// Now, we will calculate V * S^-1.
	// Since S is a diagonal matrix, we'll have to do this column-wise.  Because S is a diagonal matrix,
	// inverting is easy (simply take the reciprocal of each entry).
	Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> QI = svd.matrixV();
	for(int i=0; i<QI.cols(); i++){
		QI.col(i) /= svd.singularValues()[i];
	}

	// Find the covariance matrix, which is s2 * QI * QI^T
	cov = QI * QI.transpose();

	// Now, we have a best guess estimate for (x,y), let's see if we can find the chisq value
	// First, find the residuals
	Eigen::Matrix<T, Eigen::Dynamic, 1> r = X * c - y;

	// chisq = sum_i( w_i * r_i * r_i)
	chisq = (w.array() * r.array().square()).sum();

	// success!
	return 0;
}



#endif /* INCLUDE_PROJECTS_WIFI_SHOPPER_TRACKER_OLSREGRESSION_HPP_ */

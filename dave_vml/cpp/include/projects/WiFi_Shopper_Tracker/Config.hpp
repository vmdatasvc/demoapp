/*
 * Config.hpp
 *
 *  Created on: Jun 27, 2014
 *      Author: pshin
 */

#ifndef WIFI_SHOPPER_TRACKER_CONFIG_HPP_
#define WIFI_SHOPPER_TRACKER_CONFIG_HPP_

#define WiFiTracker_APP_NAME			"WiFi_Shopper_Tracker"
#define WiFiTracker_RUNTIME_ROOT_PATH	"/home/omniadmin/omnisensr-apps/runtimeMetaData"
#define APP_CONFIG_ROOT_PATH			"/home/omniadmin/omnisensr-apps/config"
#define APP_STORE_ROOT_PATH				"Stores"

#define APP_CONFIG_FILE				"WiFi_Shopper_Tracker_config.txt"
#define KALMAN_CONFIG_FILE			"KalmanParams.txt"
#define IOT_GATEWAY_CONFIG_FILE		"/home/omniadmin/omnisensr-apps/config/IoT_Gateway/IoT_Gateway_config.txt"

// ------ DEFINE OR NOT-DEFINE -------------------------------------------------------------------------------------

//#define USE_WIFI_PLAYER			// -- If defined, then the tracker will use a recorded measurement. Otherwise, it will use real-time data stream from a store.
#define USE_MEASUREMENT_SUMMRIZATION	false

// ------ TRUE OR FALSE --------------------------------------------------------------------------------------------
#define DEBUG_FUNCTION_CALL		false
#define DEBUG_MQTT_AT_TRACKER	false

//#define	SHOW_STAT_HISTOGRAM			true
	#define	SHOW_STAT_HISTOGRAM_DEVICE_OF_INTEREST_ONLY	false
	#define	SHOW_STAT_HISTOGRAM_PER_CHANNEL_RSS			false
	#define	SHOW_STAT_HISTOGRAM_PER_CHANNEL_RSS_REVISED	false
	#define SHOW_STAT_CALCULATION_INTERVAL				30

// ----- Tracker Parameters -------------------------------------------------------------------------------------------------------------

#define DO_TRILATERATION_TRIVIAL	false
//#define USE_TRILATERATION_ALL		// -- Among {USE_TRILATERATION_ALL, USE_RADIO_FINGERPRINT_BASED_LOCALIZATION, USE_TRILATERATION_MEANSHIFT, USE_TRILATERATION_RANSAC}

// -----------------------------------------------------------------------------------------------------------------

#ifdef USE_WIFI_PLAYER
	#define START_DATE	"2015-10-07"		// -- e.g., 2015-09-09
	#define START_HOUR	12 					// -- Range: [0-23]
	#define END_DATE	"2015-10-07"		// -- e.g., 2015-09-09
	#define END_HOUR	12 					// -- Range: [0-23]

	#define CLOCK_SPEED		1.0f
	#define CLOCK_DRIFT_IN_PLAYER 	14400	// -- Unit: sec. TODO: It's weird, but there's 4hr difference between the radio map measured and the actual clock
#endif

// ------------------------------------------------------------------------------------------------------------------

#ifdef USE_WIFI_PLAYER			// -- If defined, then the tracker will use a recorded measurement. Otherwise, it will use real-time data stream from a store.
	#define	PERIODIC_BUFFER_PROCESSING_INTERVAL		(3/CLOCK_SPEED)	// -- [sec], Interval for processing temporary measurement buffer for Kalman filtering
#else
	#define	PERIODIC_BUFFER_PROCESSING_INTERVAL		1.2	// -- [sec], Interval for processing temporary measurement buffer for Kalman filtering
#endif

// ----- Factual parameters about WiFi signal measurements ----------------------
#define MAX_CHANNELS			11
#define NUM_CHANNELS_TO_CAPTURE	3		// -- We capture wifi packets in three most popular channels: 1, 6, and 11. The 4th channel will be the sum of the three channels
#define MAX_RSS_IND_SIZE		21
#define TIMEDIFF_HIST_LENGTH	600
#define	RSS_CDF_INTERVAL		5		// -- CDF percentage interval
#define	CALIB_NUM_DISTANCE_BINS		9//7		// -- # of distance ranges to be sampled from
#define CALIB_MAX_SIGNAL_DISTANCE 	5000//2500	// -- Unit: cm 	# -- TODO: This should be adjusted based on the site.
// ------------------------------------------------------------------------------

#define DEVICES_OF_INTEREST_LIST_FILENAME 	"List_DevicesOfInterest.txt"
#define	OUI_LIST_FILENAME		"OUI_Lookup.txt"
// ------------------------------------------------------------------------------

	/* Hostname and port for the MQTT broker. */
	#define MQTT_BROKER_URL "localhost"  //"iot.eclipse.org" //"m11.cloudmqtt.com" // -- Web (log-in ID, password) for CloudMQTT = (pshin@videomining.com, videomininglabs)
	#define MQTT_BROKER_PORT 1883	//1883 //17812 "17538"
	#define MQTT_BROKER_USERNAME ""	//""//"vuqmcjya" // "zgqqyxpb"
	#define MQTT_BROKER_PASSWORD ""	//""//"pSjdNK2R5YZ4" "N9VSpth3CRlO"
	#define	MQTT_BROKER_QOS		1
	#define	MQTT_BROKER_KEEP_ALIVE		60	// -- Unit: [sec]		/* How many seconds the broker should wait between sending out keep-alive messages. */


const int MAX_BUFFER = 1024*50;		// -- Max length of a line from tcpdump
const int MAX_TOKENS_PER_LINE = 500;	// -- Max # of tokens in a line of tcpdump input
//const char* const DELIMITER = " ";

// -- Kalman Filter parameters. These numbers are determined empirically.
const double kalman_signal_covariance_11 = 90*90.f;
const double kalman_signal_covariance_33 = 90*90.f;
const double kalman_position_covariance_11 = 90*90.f;
const double kalman_position_covariance_33 = 90*90.f;

// -- Kalman Filter parameters for calibration. These numbers are determined empirically.
const double kalman_calib_rss_covariance_11 = 90*90.f;
const double kalman_calib_rss_covariance_33 = 90*90.f;

const unsigned int gTimediff_bin_width[] = { 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 5, 5, 5, 10, 10, 10, 30, 30, 30, 50, 50, 100}; // -- Width of each time interval bin: 20 elements
const unsigned int gNo_timediff_bin_width = sizeof(gTimediff_bin_width)/sizeof(int);	// -- # of timediff_bin
//unsigned int gTimediff_bin_width_accumulated[gNo_timediff_bin_width] = {0};			// -- Accumulated count in each time interval bin

enum packetanalyzer_communication_type {TCPIP = 0x30, UDP = 0x31, STORAGE = 0x32};
enum GUI_model_line_type {LINE_BEGIN = 1, LINE_RIGHT = 2, LINE_LEFT = 3, LINE_UP = 4, LINE_DOWN = 5};
enum return_type {		// -- Must be a negative value for an error
	SUCCESS = 1
};
enum error_type {		// -- Must be a negative value for an error
	ERROR_NULL_OMNISENSR_POINTER = -30
	, ERROR_TOO_SMALL_MEASUREMENTS_RECEIVED		// -- -29
	, ERROR_TOO_SMALL_DATA_SAMPLES_FOR_LOCALIZATION		// -- -28
	, ERROR_TOO_SMALL_DATA_SAMPLES_FOR_CALIBRATION_FUNCTION_FITTING		// -- -27
	, ERROR_NO_BEST_CALIB_PARAM_FOUND		// -- -26
	, ERROR_MEANSHIFT_CLUSTERING_FAILURE	// -- -25
	, ERROR_LEAST_SQUARE_EXCEPTION	// -- -24
	, ERROR_LEAST_SQUARE_FAILURE	// -- -23
	, ERROR_TOO_FEW_CHOSEN_SENSORS	// -- -22
	, ERROR_TRILATERATION_FAILURE	// -- -21
	, ERROR_TOO_LARGE_ERROR_DISTANCE	// -- -20
	, ERROR_TARGET_NOT_IN_CONVEXHULL	// -- -19
	, ERROR_TARGET_NOT_IN_ROI		// -- -18
	, ERROR_KALMAN_FAILURE		// -- -17
	};

enum LocalizationAlgorithmType {
	 PLAIN_CIRCULAR = 1,
	 WEIGHTED_CIRCULAR = 2		// -- based on "Weighted Least Squares Techniques for Improved Received Signal Strength Based Localization", Paula Tarrio et al., 2011
};
//enum Localization_type {USE_TRILATERATION_ALL, USE_RADIO_FINGERPRINT_BASED_LOCALIZATION, USE_TRILATERATION_MEANSHIFT, USE_TRILATERATION_RANSAC};

enum ProcessingType {
	ADDING_NEW_DEVICE = 1,
	PERIODIC_PROCESSING = 2,
	PERIODIC_TRAJECTORY_REPORTING = 3,
	DRAWING_MAP = 4,
	CLEARING_UP_DEVICE = 5,
	CALIBRATION = 6
};


#endif /* WIFI_SHOPPER_TRACKER_CONFIG_HPP_ */




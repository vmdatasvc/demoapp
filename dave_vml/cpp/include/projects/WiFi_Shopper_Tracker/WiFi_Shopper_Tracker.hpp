/*
 * WiFi_Shopper_Tracker.hpp
 *
 *  Created on: Jul 3, 2014
 *      Author: pshin
 */

#ifndef WiFi_Shopper_Tracker_HPP_
#define WiFi_Shopper_Tracker_HPP_

#include <list>
#include <iostream>
#include <memory>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include "modules/wifi/MessageType.hpp"

#include "projects/WiFi_Shopper_Tracker/RemoteDevice.hpp"
#include "projects/WiFi_Shopper_Tracker/MonitoringAP.hpp"

// -- AWS IoT SDK files for State Parameter Type definition
#include "aws_iot_error.h"
#include "aws_iot_shadow_json_data.h"

#include "modules/iot/IoT_Common.hpp"
#include "modules/iot/IoT_Gateway.hpp"
#include "modules/iot/Dev_Common.hpp"
#include "modules/utility/LocalClock.hpp"

// -- For MQTT
#include "modules/iot/MQTT-client-cpp.hpp"

// -- Refer to https://github.com/nlohmann/json
#include "json.hpp"

using json = nlohmann::json;

using namespace cv;
using namespace std;

typedef std::shared_ptr<RemoteDevice> RemoteDevicePtr;

// -- Each MosqMQTT_fwd instance at WiFi_Sensor_short class is supposed to subscribe to a topic to an OmniSensr (that is running WiFi_PacketProcessor) and publish it to the local MQTT broker.
// -- Then, MosqMQTT_agg instance will aggregate all the data into a single stream, enabling all the messages are coming to a WiFi_Shopper_Tracker to be processed by a single process.

extern pthread_mutex_t gMutex;

class MosqMQTT_agg : public MosqMQTT {
public:

	MosqMQTT_agg(const char *id=NULL, bool clean_session=true);
	~MosqMQTT_agg();

	void on_connect(int rc);
	void on_disconnect(int rc);
};

class WiFi_Shopper_Tracker {

public:
	WiFi_Shopper_Tracker(string trackerId_suffix);
	virtual ~WiFi_Shopper_Tracker();

	bool run(bool showGUI);
	bool ParseSetting(string configFile);
	void ReadMonitoringAPs_text(string filepath);
	void ReadMonitoringAPs_csv(string filepath);
	void InitiateKillProcess();
        void InitialMapGeneration(string storeFilename);
	void SetTrackerOperationMode(unsigned int mode) {
		pthread_mutex_lock(&gMutex);
		mWiFiOperationMode = mode;
		pthread_mutex_unlock(&gMutex);
	}

	bool loadKalmanParams(string configfile);
	static bool addKalmanParams();
        string GetTrackerID() { return mTrackerID.str(); }


#if defined(USE_WIFI_PLAYER)
	double mSimulationTime;		// -- This is the current clock at the player
	char mSimulationTime_date[99];
	char mSimulationTime_time[99];
#endif

        Mat mImg_map, mImg_map_init, mImg_map_final;
        Rect mStoreMapRoiMask;

	//std::list<RemoteDevicePtr> 	mFoundDevice;
	std::deque<RemoteDevicePtr> 	mFoundDevice;

	static deque<Node_info_t> mKnownServiceAgents, mKnownAPs;
	static deque<Node_info_t> mDevicesOfInterest;
	static deque<OUI_Lookup_t> mOUI_Lookup;

	// -- for all the locations
	std::list<fingerprint_t> mRadioMap;

	std::deque<WiFiSensor_short_Ptr> 	mRunningWiFiSensor;		// -- Currently running APs
	std::deque<FILE*> mOpenedPipe;

	TrackState_t	mTrackerState;
	MosqMQTT_agg* mMQTT_Tracker;
	string 	mMQTT_subTopic;			// -- MQTT topic that this visualizer class will subscribe to get the data stream from the tracker that publishes data stream with the same topic.

        // -- Store-specific Parameters
        static string mAppStoreName;
        bool mShowSensors;
        float mFontSizeScale;
        bool mKillRequestPending;

        static bool mDebugIoT;
        static string mAppType_Tracker;

		int mKalmanEstimateVisualizationTimeout;		// -- Unit: sec

protected:


	// -- Configuration/Settings parameters -------------------------------------------------------------

		// -- Debug Mode Selection
		int mDebugVerboseLevel, mDebugDeviceStatusVerboseMode;
		bool mDebugTrackerStatus, mDebugCalib, mDebugLocalization, mDebugLocalizationErrorCode;		

		bool mCreateMeasLog;

		// -- Select what to track
		bool mOnlyDevOfInterest;	// -- If true, track only some of the devices of interest that are registered already.
		bool mTrackAP;				// -- If true, track APs as well as shoppers
		bool mTrackServiceAgent;	// -- If true, track Service Agents as well as shoppers
		bool mTrackNullDev;			// -- If true, track NullDevice as shoppers (NullDevice is a device whose manufacturer is unknown (i.e., not recognized by MAC address OUI Table)

		// -- Select what to show
                bool mShowMapVisualization;	// -- If true, show store map-based GUI/Visualization
		bool mShowNoAPs, mShowNoServiceAgent, mShowRSSMeas, mShowLocalizationPolygon, mShowTrajectory, mShowTrajectory_DevOfInt, mShowSelectedSensorCentroid;
		bool mShowRawMsg, mShowParsedMsg;

		// -- Shopper-related parameters
		int mFoundDevices_max, mMeasBufferPerDev_max, mTimeThresholdForDevAbsency, mShoppingTime_min, mShoppingTime_max, mShoppingDistance_min, mShopperDetection_min, mTrackedPoints_max, mTrackedPointsSizeToReport_min;
		float mTrajectoryPrecision_dist, mTrajectoryPrecision_time;
		bool mChannelSeperation;
		int mCalibMeasWaitPeriod_max, mCalibPeriod_max, mCalibStartWaitPeriod_min, mTimeThreshold_ServiceAgent, mServiceAgentListSize_max, mApListSize_max, mStatAccuracySize_max, mReliableDistanceInMeas_max;

		// -- Localization parameters
		double mPeriodicBufferProcessingInterval;
		int mMeanShiftRadiusExpansionCnt_max;
		int mMinNumMeasToDoLocalization;
		int mNumSensorForLocalization_max, mNumSensorForLocalization_min; 	// -- # of sensors to select for a localization
		int mNumSensorForSelection_max, mNumSensorForSelection_min;			// -- # -- Max/Min # of sensors (with stronger signals) to be considered for mean-shift based sensor selection
		double mLocalizationMaxAvgSqrErrorDistance;			// -- Unit: meter^2
		int mLocalizationAlgorithmType;		// -- among {PlainCircular = 1, WeightedCircular = 2}
		int mLocalizationBoundingBoxMargin;
		int mLocalizationMaxDistFromConvexHull;
		int mMaxDistForNearbyNeighbors;
		static bool mReportRealTimeTrackResult;

		// -- Measurement Pre-processing
		bool mMeasurementAdjustmentEnable;

		// -- Kalman filter parameters for Distance (by RSS) estimation
		bool mKalmanRssEnable;
		static double mKalmanRssProcessNoise_var, mKalmanRssMeasurementNoise_var, mKalmanRssCovCoeff_pos_min, mKalmanRssCovCoeff_accel_min;	// -- Unit: cm^2
		static double mKalmanRssMaxStdAllowed;		// -- Unit: cm
		double mKalmanTimeScale_rss;		// -- Time unit transform

		// -- Kalman filter parameters for Location estimation
		bool mKalmanPosEnable;
		static double mKalmanPosProcessNoise_var, mKalmanPosMeasurementNoise_var, mKalmanPosCovCoeff_pos_min, mKalmanPosCovCoeff_accel_min;	// -- Unit: cm^2
		double mKalmanTimeScale_pos;		// -- Time unit transform
		bool mKalmanPosFeedbackToRssEnable;
		double mKalmanPosFeedbackToRssNoiseScale;

		// -- Kalman filter general parameters
		double mKalmanMeasurementLifetimeForTracking;	// -- Unit: sec
		double mKalmanInitThreshold_time;				// -- Unit: sec
		double mKalmanInitThreshold_dist;		// -- Unit: cm

		// -- Store-specific Parameters
		ostringstream mKnownApListFilePath, mKnownServiceAgentFilePath, mMonitoringAP_IPsFilePath;

		// -- GUI parameters
		string mImgMapName, mStoreLayoutFilename;
		int mStoreLayoutOriginalImgSize_x, mStoreLayoutOriginalImgSize_y;
		double mStoreLayoutPixelPerFt;
		int mStoreWidth_img_x_max, mStoreWidth_img_y_max, mStoreWidth_PHY_x, mStoreWidth_PHY_y, mStoreWidth_IMG_x, mStoreWidth_IMG_y;
		int mInStoreMapBBMargin;

		// -- This ratio and offset can be used to convert a physical coordinate to map coordinate
		double mPhyToMap_x_ratio, mPhyToMap_y_ratio;
                Point mPhyToMap_offset;

		// -- Periodic processing intervals;
		float mPeriodicInterval_GuiMapRefresh, mPeriodicInterval_TrackingReport;
		int mPeriodicInterval_DevClearUp;

		// -- Calibration parameters
		bool mFindBestRssOffset, mAutomaticPerDevRssOffsetAdjustment;
		int mFindBestRssOffsetMaxRss, mFindBestRssOffsetMinRss, mWifiCalibRssOffset;//
		ostringstream mCalibFilePath, mCalibFilePathPrefixFull;
		int mCalibRansacTry_max, mCalibSampleDistPerBin_max,  mCalibMeasPerBin_min, mCalibMeasPerBin_max, mCalibEnoughMeasBinsToKickOffCalib_min, mCalibDataSamplesForLeastSquare_min, mCalibFailCntAllowed_max;

		// -- Kalman filter parameters for Distance (by RSS) estimation
		double mKalmanCalibRssProcessNoise_var, mKalmanCalibRssMeasurementNoise_var, mKalmanCalibRssCovCoeff_pos_min, mKalmanCalibRssCovCoeff_accel_min;

	// ----- IoT variables and functions -------------------------------------------------------------------------------

		static int mPeriodicStateReportingInterval;
		string mSoftwareVersion;

		//pthread_t mPeriodicIoTProcessing_thread;

		string mAppID;
		string mTrackerID_suffix;

//		static void *PeriodicIoTProcessingThread(void *apData);
//		void PeriodicIoTProcessing_Loop();

		// -- ######################################################################
		// -- Declare iot gateway
		static IoT_Gateway	mIoTGateway;

		// -- Define callback functions for messages from Delta State and Control Channel
		static void IoT_DeltaState_CallBack_func(string deltaMsg);
		static void IoT_ControlChannel_CallBack_func(string topic, string controlMsg);

		static void SetRealtimeMode(bool realtimeMode);


	// ------ WiFi Calibration variables and function ----------------------------------------------------

//		list<calibOmniSensr_t> CalibOmniSensrs;	 // -- For per-AP RSS-distance mapping function learning

		//bool mNeedToStopWiFiCalib, mNeedToStartWiFiCalibVerification;	//mNeedToStartWiFiCalib
		static bool mNeedToResetList_serviceAgent, mNeedToResetList_ap, mNeedToTurnOffCalib;
		static unsigned int mWiFiOperationMode;
		unsigned int mWiFiOperationMode_prev;
                double	mCalibStartTime;
                int mCalibElapsedTime;
		bool mStartWiFiCalib;		
		bool calibAddNewRSS(tcpdump_t &meas);
		int uniformlyRandomlySelectDistance(list<pair<int, float> > *sampledData_dst, WiFiSensor_short_Ptr os_selected, int channel);
		int generateRSStoDistanceMappingFunction(WiFiSensor_short_Ptr os_selected, int channel, double &residualSqrSum_avg);
		int findBestRssOffset();


	// ---------------------------------------------------------------------------------------------------

	LocalClock mClock;

	pid_t mProcessId;
	stringstream mTrackerID;

	unsigned int mIsThereSomeProcessing;

	pthread_attr_t mThreadAttr;
	pthread_t mPeriodicBufferProcessing_thread;

	static void *PeriodicBufferProcessingThread(void *apData);
	static void *PacketAnalyzerThread_TCPIP(void *apdata);

	void PeriodicBufferProcessing_Loop();

	// -- Thread-related
	pthread_t mPacketStreamParsing_thread;
	pthread_t mWiFiCalibProcessing_thread;
	void unscrambleRSSData(char *buf, int buffer_len, string &src, int &rss, int &deviceType, int &channel);
	bool parse_PacketProcessing_at_Sniffer_input(tcpdump_t *meas, char *buffer, int buffer_len);
	void SaveWiFiCalibResultVisualization(WiFiSensor_short_Ptr os, int channel);
	void SaveWiFiCalibResults(WiFiSensor_short_Ptr os, int channel);
	void SendWiFiCalibProgress();
	void SendWiFiCalibResults();

	void packetStreamParsing();
	void WiFiCalibProcessing();
	static void *PacketStreamParsingThread(void *apdata);
	static void *WiFiCalibProcessingThread(void *apdata);


	void loadKnownAPs_n_ServiceAgents();
	void loadOUIlookuptable();		// -- Load OUI look-up table to find a device manufacturer by checking the pre-fix of the MAC address
	void loadDevicesOfInterest();
	void loadRadioFingerprints();
	void loadCalibrationParams();

	void showParsedMessage(tcpdump_t *meas);
	void add_knownDeviceType(tcpdump_t *meas);
	void addCollectedMeasurementToDeviceBuffer(tcpdump_t *meas);
	bool initializeRemoteDevice_distributed(RemoteDevicePtr src, tcpdump_t *meas);

	int periodicProcessing_per_Station(RemoteDevicePtr dev);
	void periodicMeasurementProcessing_per_Station(RemoteDevicePtr dev);
	void MeasurementAdjustment();
	int ConvertChannelIntoIndex(int channel);
        Point ConvertPhyCoordToMapCoord(Point2f phyPt);
        Point ConvertPhyCoordToMapCoord(int phyX, int phyY);
        Point2f ConvertMapCoordToPhyCoord(Point2f imgPt);
        Point2f ConvertMapCoordToPhyCoord(int imgX, int imgY);
        double ConvertPhyDistanceToMapDistance(double phyDist);
        double ConvertMapDistanceToPhyDistance(double imgDist);


	KalmanFilter_4S *findKalmanSignal(RemoteDevicePtr dev, WiFiSensor_short_Ptr ap);

//	int locationEstimationBasedOnRssEstimation(RemoteDevicePtr dev, Point *pt);
        float locationEstimation_Trilateration(RemoteDevicePtr dev, Point *pt);
        //int locationEstimationBasedOnRadioFingerprint(RemoteDevicePtr dev, Point *pt);

        bool GetCentroidOfInlierSensorsUsingMeanShift(deque<Point> &positions, Point *pt, int max_interation_no, int MinPointsInACluster);
        //bool GetInlierSensorsUsingMeanShift(RemoteDevicePtr dev, vector<pair<WiFiSensor_short_Ptr, double> > &positions, Point *pt, int max_interation_no, int MinPointsInACluster);


        //float Trilateration_trivial(vector<pair<WiFiSensor_short_Ptr, double> > *rss_data, RemoteDevicePtr dev, Point *pt);
        //int Trilateration_trivial(vector<pair<WiFiSensor_short_Ptr, double> > *rss_data, RemoteDevicePtr dev, Point *pt, float *avgSquaredError_ret, float *avgError_ret);
        //float Trilateration_closest(vector<pair<WiFiSensor_short_Ptr, double> > *rss_data, RemoteDevicePtr dev, Point *pt);
        int Trilateration_closest(RemoteDevicePtr dev, Point *pt, float *avgSquaredError_ret, float *avgError_ret);
//	bool Trilateration_RANSAC(vector<pair<Point, double> > *rss_data, RemoteDevicePtr dev, Point *pt);

//	double distBtwFingerprints(fingerprint_runtime_t fp_run, fingerprint_t fp_map);
//	double distBtwFingerprints_Mahal(fingerprint_runtime_t fp_run, fingerprint_t fp_map);
	double distToInStore(int px, int py);
        Point2f findClosestPointToLine(Point2f P, Point2f LinePt0, Point2f LinePt1);

	bool isInsidePolygon(int nvert, double *vertx, double *verty, double testx, double testy);
        bool isInsidePolygonVector(Point point, vector<Point> &polyPts);
	//bool isInsideBoundingBox(int nvert, float *vertx, float *verty, int margin, float testx, float testy);
	bool isInsideBoundingBox(int nvert, float *vertx, float *verty, int margin, float testx, float testy, float &closestPtx, float &closestPty);
	bool isNearbyNeighbors(WiFiSensor_short_Ptr os, WiFiSensor_short_Ptr peer);

	void ParameterLevelColoring(float *r, float *g, float *b, float value, float max_value, float min_value);

	// -- Record error distance compared to ground truth
#ifdef USE_WIFI_PLAYER
	void DistErrorCollect(RemoteDevicePtr dev, int x, int y);
	void DistErrorStatistics(RemoteDevicePtr dev);
	void DistErrorStatisticsVisualize(RemoteDevicePtr dev);
#endif

	//void periodicProcessing_all_Stations();

	bool checkCollinearity(uint *rind, const int N_AP, RemoteDevicePtr dev);

	void visualizeRSSestimation();
	void visualizeCalibrationProgress();
	void drawMap();

        string scrambleDATA(string addr, cv::Point loc, int track_age);				// -- Scramble the MAC address and location data
        void unScrambleTrackData(const char *buf, int buf_len, string &src, cv::Point *loc, int *age);	// -- Un-scramble the shopper's MAC address and location data
	void reportTrackingResults();	// -- Periodically publish the up-to-date tracking results to a MQTT Broker
	//void on_connect(int rc);

	RemoteDevicePtr findDevice(string addr);		// -- Input: addr of a device, Output: the pointer to the device
	Node_info_t *findAP(string addr);
	Node_info_t *findServiceAgent(string addr);
	WiFiSensor_short_Ptr findRunningWiFiSensor(string addr);
	WiFiSensor_short_Ptr findRunningWiFiSensor(int x, int y);
	bool isDevicesOfInterest(string addr);
	string findManufacturer(string addr);

	void clearUpDeviceList();

	const string currentDateTime();
	string getRunningTime();
	void calculateTrackStats();

	void SendCompletedTrajectory(RemoteDevicePtr dev);
	void SendCompletedTrajectory_summarized(RemoteDevicePtr dev);
	void SimplifyTrajectory(RemoteDevicePtr dev, float precision_dist /*Unit: meter*/, float precision_time /*Unit: sec*/);

};


#endif /* WiFi_Shopper_Tracker_HPP_ */

/*
 * SampleLocalizer.hpp
 *
 *  Created on: Jan 26, 2017
 *      Author: jwallace
 */

#ifndef WIFI_SHOPPER_TRACKER_SAMPLELOCALIZER_HPP_
#define WIFI_SHOPPER_TRACKER_SAMPLELOCALIZER_HPP_

#include "Localizer.hpp"

#include <limits>

template <class PacketPtr>
class SampleLocalizer : public Localizer<PacketPtr> {
public:
	SampleLocalizer(const WiFiTracker* t) : Localizer<PacketPtr>(t),
		curr_max_rss(std::numeric_limits<float>::min()), \
		curr_avg_rss(0) {}
	virtual ~SampleLocalizer() {}

	virtual TrackPoint* localize(const PacketPtr& begin, const PacketPtr& end, unsigned int n_packets);

private:

	void addRunningMaxRSS(const PacketPtr& p);
	void addRunningAvgRSS(const PacketPtr& p, unsigned int n_meas);

	float curr_max_rss;
	float curr_avg_rss;
};

template <class PacketPtr>
TrackPoint* SampleLocalizer<PacketPtr>::localize(const PacketPtr& begin, const PacketPtr& end, unsigned int n_packets){

	// Why do I need this??  I thought I could just access the calib_params pointer directly
	// though the base class..
	// Oh well, one little line won't kill us!
	const WiFiTracker::calib_param_t& params = this->getCalibParams();

	curr_max_rss = std::numeric_limits<float>::min();

	// as long as this is not Nan or Inf, we're fine!
	curr_avg_rss = 0;

	boost::posix_time::ptime maxTime;

	unsigned int n_meas = 0;
	for(auto it = begin; it != end; it++){
		addRunningMaxRSS(it);
		addRunningAvgRSS(it, n_meas++);

		// NOTE: since I KNOW that these are in timestamp order, the maxTime is
		// ALWAYS the next one.
		maxTime = it->getTimestamp();
	}

	// If I was provided an empty list of return values, I certainly can't localize!
	TrackPoint* retval = 0;
	if(n_meas != 0){
		// base it on the calib parameters for the 1st packet
		auto cit = params.find(begin->getName());
		if(cit != params.end()){
			// base it all on channel 1:
			float avg_dist = exp( (curr_avg_rss - cit->second[1]) / cit->second[0]);
			float min_dist = exp( (curr_max_rss - cit->second[1]) / cit->second[0]);
			retval = new TrackPoint(maxTime, avg_dist, min_dist, 0, 0, 0);
		}
	}

	return retval;
}

template <class PacketPtr>
void SampleLocalizer<PacketPtr>::addRunningMaxRSS(const PacketPtr& p){
	curr_max_rss = std::max(curr_max_rss, static_cast<float>(p->getPayload().getRSS()) );
}

template <class PacketPtr>
void SampleLocalizer<PacketPtr>::addRunningAvgRSS(const PacketPtr& p, unsigned int n_meas){
	curr_avg_rss = (curr_avg_rss*n_meas + p->getPayload().getRSS())/static_cast<float>(n_meas + 1);
}


#endif /* INCLUDE_PROJECTS_WIFI_SHOPPER_TRACKER_SAMPLELOCALIZER_HPP_ */

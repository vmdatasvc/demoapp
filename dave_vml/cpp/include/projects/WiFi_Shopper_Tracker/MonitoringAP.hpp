/*
 * MonitoringAP_short.hpp
 *
 *  Created on: Jun 30, 2014
 *      Author: pshin
 */

#ifndef MonitoringAP_short_HPP_
#define MonitoringAP_short_HPP_


#include <cstdlib>
#include <cstdio>
#include <pthread.h>
#include <string>
#include <memory>
#include <list>

#include "modules/wifi/MessageType.hpp"
#include "modules/iot/MQTT-client-cpp.hpp"
#include "modules/utility/KalmanFilter_4S.hpp"
#include "projects/WiFi_Shopper_Tracker/Config.hpp"

using namespace std;

typedef struct  {
	string SN;
	list<calibMeas_t> measBuffer;
}  calibMeasPerPeer_t;

// -- Each MosqMQTT_fwd instance is supposed to subscribe to a topic to an OmniSensr (that is running WiFi_PacketProcessor) and publish it to the local MQTT broker.
// -- Then, MosqMQTT_agg instance will aggregate all the data into a single stream, enabling all the messages are coming to a WiFi_Shopper_Tracker to be processed by a single process.
class MosqMQTT_fwd : public MosqMQTT {
public:

	MosqMQTT_fwd(const char *id=NULL, bool clean_session=true);
	~MosqMQTT_fwd();

	bool mIsConnected;

	void on_connect(int rc);
	void on_disconnect(int rc);
	void on_message(const struct mosquitto_message *msg);
};


class WiFiSensor_short {

public:
	WiFiSensor_short();
	virtual ~WiFiSensor_short();

	string mDevName;
	string mIPaddr;		// -- IP address (e.g., 10.250.1.7, 255.255.255.255)
	string mSerialNumber;
	int	mSensorPos_x, mSensorPos_y, mSensorPos_z;			// -- The location of the AP in the store
	int	mSensorPos_x_map, mSensorPos_y_map;	// -- The coordinate of the AP in the store map

	double		time_MsglastReceived;		// -- Unit: sec

	bool mIsCurrentlyReceivingMessages;

	bool mKillRequestPending;
        pthread_attr_t mThreadAttr;
        pthread_t mStreamProcessing_thread;

	deque<string> mNearbyNeighbors;

	// -- Parameters for RSS-to-distance conversion by dist = exp(mCalib_a*rss + mCalib_b). Distance unit is centimeter
//	float mCalib_a_Android;
//	float mCalib_b_Android;
//	float mCalib_a_iOS;
//	float mCalib_b_iOS;
//	float mCalib_a_all;
//	float mCalib_b_all;

		// -- WiFi Calibration parameters/measurements
		string mCalibParamFilename;
		bool mWiFiCalibStarted[NUM_CHANNELS_TO_CAPTURE];
		list<calibMeas_t> mCalibMeas[NUM_CHANNELS_TO_CAPTURE][CALIB_NUM_DISTANCE_BINS];			// -- a list of raw RSS measurements for each of three channels (i.e., 1, 6, 11)
		list<calibMeas_t> mCalibMeas_sampled[NUM_CHANNELS_TO_CAPTURE][CALIB_NUM_DISTANCE_BINS];	// -- a list of raw RSS measurements that are sampled uniformly randomly for each of three channels (i.e., 1, 6, 11)
		list<calibMeasPerPeer_t> mCalibMeas_per_peer[NUM_CHANNELS_TO_CAPTURE];			// -- a list of raw RSS measurements for each of three channels for each peer (i.e., neighboring OS).
		int mTotalMeasCntThreshold[NUM_CHANNELS_TO_CAPTURE];
		int mCalibFailCnt[NUM_CHANNELS_TO_CAPTURE];
		int mCalibMeasCollectionProgress[NUM_CHANNELS_TO_CAPTURE];		// -- Unit: [%], range: [0%, 100%]

		double mCalib_a[NUM_CHANNELS_TO_CAPTURE], mCalib_a_best[NUM_CHANNELS_TO_CAPTURE];	// -- Parameters for RSS-to-distance conversion by dist = exp(calib_a*rss + calib_b). Distance unit is centimeter
		double mCalib_b[NUM_CHANNELS_TO_CAPTURE], mCalib_b_best[NUM_CHANNELS_TO_CAPTURE];	// -- Parameters for RSS-to-distance conversion by dist = exp(calib_a*rss + calib_b). Distance unit is centimeter
		list<KalmanFilter_4S> 	mKalmanCalibRSS[NUM_CHANNELS_TO_CAPTURE];		// -- To estimate the true RSS from OminiSensrs. Each channel has its own list of Kalman filter

	void initNode(string DevName, string IPaddr, string NIC,  string SN);	// -- Set the IP address and NIC name and serial number
	void setSensorPos(int x, int y, int z);			// -- Set the world coordinate of the Wi-Fi sensor

//	float convertRSStoDistance(int rss);	// -- Convert a RSS measurement to a world distance [Unit: cm]
//	int convertDistToRSS(float dist);		// -- Convert a distance to an estimated RSS value [Unit: cm]

//	float convertRSStoDist_log_Android(int rss, int rssCalibOffset = 0);
//	int convertDist_logToRSS_Android(float dist, int rssCalibOffset = 0);
//
//	float convertRSStoDist_log_iOS(int rss, int rssCalibOffset = 0);
//	int convertDist_logToRSS_iOS(float dist, int rssCalibOffset = 0);
//
//	float convertRSStoDist_log_All(int rss, int rssCalibOffset = 0);
//	int convertDist_logToRSS_All(float dist, int rssCalibOffset = 0);

	float convertRSStoDist_log(int rss, int ind_channel, int rssCalibOffset = 0, bool isOmniSensr = false);
	int convertDist_logToRSS(float dist, int ind_channel, int rssCalibOffset = 0, bool isOmniSensr = false);

	bool ConnectAndGetStream(string subTopic, string deviceID_suffix);

	void AddKalmanCalibRSS(KalmanFilter_4S kalman, int ind_ch) {	mKalmanCalibRSS[ind_ch].push_back(kalman); }
	KalmanFilter_4S *findKalmanCalibRSS(int srcSensorPos_x, int srcSensorPos_y, int ind_ch);
	calibMeasPerPeer_t *findPeerBuffer(string peerSN, int ind_channel);
	bool AddToPerDistanceBinBuffer(calibMeas_t &m, int ind_channel, int maxSize);
	void AddToPerPeerBuffer(calibMeas_t &m, int ind_channel, int maxSize);

	void SendWiFiCalibMsg(unsigned int mode);

private:

	bool mIsIPinitialized;			// -- Flag to show if the IP of this AP is set or not

	string mSubTopic_remoteData;//, mSubTopic_remoteControl;	// -- Data topic for the remote OmniSensr
	string mTrackerID_suffix;
	MosqMQTT_fwd *mMQTT_forwarder;		// -- Client to local broker

	static void *StreamProcessingThread(void *apData);
	void StreamProcessing_Loop();
};

typedef std::shared_ptr<WiFiSensor_short> WiFiSensor_short_Ptr;



#endif /* MonitoringAP_short_HPP_ */

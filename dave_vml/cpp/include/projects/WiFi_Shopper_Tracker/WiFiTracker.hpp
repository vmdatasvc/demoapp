/*
 * WiFiTracker.hpp
 *
 *  Created on: Jan 24, 2017
 *      Author: jwallace
 */

#ifndef WIFITRACKER_HPP_
#define WIFITRACKER_HPP_

#include "core/OmniSensrApp.hpp"

#include "RemoteSniffer.hpp"

#include "modules/iot/MQTTClient.hpp"
#include "modules/wifi/TrackerPacket.hpp"

#include <string>
#include <set>
#include <deque>
#include <map>

#include <boost/array.hpp>
#include <boost/thread.hpp>
#include <boost/thread/condition.hpp>

#include "TrackPoint.hpp"
//#include "Localizer.hpp"

// forward declare the localizer; it needs to include this header
template <class T>
class Localizer;

class WiFiTracker : public OmniSensrApp {
private:

	enum WiFiMode : uint8_t {
		Tracking,    // Tracking is actively tracking shoppers (drop any calibration packets)
		Calibration, // calibration is dropping all non-calibration packets
		Verification // verification is actively tracking shoppers AND calibration packets
	};

	// ================= convenience typedefs =====================
	typedef std::multiset<TrackerPacket> track_packet_buff_t;
	typedef std::set<TrackPoint*> track_point_buff_t;

public:
	typedef std::map<std::string, boost::array<float, 7> > calib_param_t;

public:
	WiFiTracker(const std::string& configfn);
	~WiFiTracker();

private:
	// No copying or assignment!
	WiFiTracker(const WiFiTracker&);
	WiFiTracker& operator=(const WiFiTracker&);

protected:

	virtual void start();

	virtual unsigned int initConfig(ParseConfigTxt& parser);

	virtual void saveState(std::ostream& os) const;

	virtual void processCommand(const std::string& msg);

private:

	// go into calibration mode
	bool calibrate();

	// calibrate a single OmniSensr
	boost::array<float, 7> calibrateSensor(const std::string& name, const track_packet_buff_t& buf) const;

	// returns true if (and only if) there is sufficient data in the device buffer
	// to perform a calibration.
	bool calibrationDataAvailable(const std::string& name, const std::set<std::string>& senders) const;

	// track the shoppers
	void trackShoppers();

	// take the measurements in the device buffer and make them into tracks
	// NOTE: this is ONLY to be used for "typical" devices - not the Omnisensr calibration packets
	void localizeShopper(track_packet_buff_t& packet_buff, track_point_buff_t& track_buff);

	void localizeSensor(track_packet_buff_t& packet_buff, track_point_buff_t& track_buff);

	// combine all of the measurements from beginning up to(but not including) end into a single tracked point
	TrackPoint* localize(const track_packet_buff_t::const_iterator& beg, const track_packet_buff_t::const_iterator& end);

	// Given a track point buffer for a single device, construct all of the tracks
	void summarizeDeviceTrack(const std::string& fn, track_point_buff_t& track_buff);

	// Given a track point buffer for a single STATIONARY device, construct all of the tracks
	void summarizeSensorTrack(const std::string& fn, track_point_buff_t& track_buff);

	// Generate a simplified track given a set of track points
	// (i.e., if we have a bunch of them in the same spot, just send a "start" and "exit" point
	void generateTrack(const std::string& name, const track_point_buff_t::iterator& beg, const track_point_buff_t::iterator& end, bool compress=true);

	// Adds a single track point to the "A4" string
	void addTrackPoint(std::ostream& os, const TrackPoint& p, const boost::posix_time::ptime& start) const;

	// sends a report of the given track
	void sendRealtimeReport(const std::string& name, const TrackPoint& t, unsigned int seq_num);

	bool readAPList(const std::string& fn);

	bool readCalibration(const std::string& fn);
	bool saveCalibration(const std::string& fn) const;

	bool readList(const std::string& fn, std::set<std::string>& output_set);
	bool saveList(const std::string& fn, const std::set<std::string>& input_set) const;

	// callback to process a packet sent by the aggregators
	void processPacket(const struct mosquitto_message* msg);
	// add a packet to the input buffers, return false if we dropped the packet
	bool addPacket(const TrackerPacket& tp);

	void readOnlyCallback(const std::string& var, const std::string& msg);

	// ================= Runtime Variables ========================
	std::set<std::string> known_service_agents;
	std::set<std::string> known_APs;
	std::set<std::string> devices_of_interest;
	// must lock our devices
	mutable boost::mutex set_lock;

	// list of all remote monitors
	std::deque<RemoteSniffer*> remote_monitors;
	// find remote monitor by S/N
	std::map<std::string, RemoteSniffer*> remote_sn_map;
	// find remote monitor by name
	std::map<std::string, RemoteSniffer*> remote_name_map;

	// The current time (UTC) as defined by the incoming packets
	boost::posix_time::ptime curr_time;

	// The timestamp of the most recent mainloop
	boost::posix_time::ptime process_time;

	boost::posix_time::ptime curr_realtime;
	boost::posix_time::ptime last_heartbeat;

	// calibration parameters.  A map of OmniSensr name to the 6 found parameters:
	// The model for each channel is RSS = a_0 * log(dist) + a_1.  The parameters are:
	// 0 - a_0 for Channel 1
	// 1 - a_1 for Channel 1
	// 2 - a_0 for Channel 6
	// 3 - a_1 for Channel 6
	// 4 - a_0 for Channel 11
	// 5 - a_1 for Channel 11
	calib_param_t calib_params;

	// a per-device buffer of all tracking packets received, ordered by timestamp
	std::map<std::string, track_packet_buff_t> device_buffer;
	boost::mutex buffer_lock;

	// a per-device buffer of tracks
	// NOTE: it is IMPOSSIBLE for two TrackPoints to have identical timestamps
	// for a single device, so we can use a set instead of a multiset here
	// (We still want a sorted container).
	std::map<std::string, track_point_buff_t> device_tracks;

	// a per-device buffer of OmniSensr tracks
	// only used in calibration verification mode
	std::map<std::string, track_point_buff_t> sensor_tracks;

	boost::mutex track_lock;

	// mutex and condition for waking the mainloop
	boost::mutex main_lock;
	boost::condition main_cond;

	// current mode of wifi tracking
	WiFiMode curr_mode;

	// should we continue processing
	bool killed;

	// ================= Configuration Variables ==================

	// ============= calibration parameters =================
	// maximum distance between sender/receiver to consider data "useful"
	float calib_max_distance;
	// should we calibrate each channel independently?
	bool calib_indep_channel;
	// minimum number of packets (per channel) between sender/receiver to use for calibration
	unsigned int calib_min_packets;
	// maximum number of packets (per channel) between sender/receiver to use for calibration
	unsigned int calib_max_packets;
	// should we weight the regression such that each sender/receiver pair counts
	// equally, regardless of the number of calibration packets
	bool calib_equal_ch_wt;
	bool calib_equal_sensr_wt;
	// maximum amount of time we should take to calibrate (seconds)
	unsigned int calib_max_time;
	// the height (in ft.) of the plane of the devices
	float device_plane;

	// ============= Tracking Parameters ====================
	// Set to true to only track the known devices (useful for debugging)
	bool track_known_devices;
	// Set to true to track stationary APs (useful for constant verification)
	bool track_ap;
	// Set to true to track "employees" (i.e., those devices that exist for an extended period of time)
	bool track_service_agent;
	// Set to true to track devices that are not behaving properly (i.e., spoofing their MAC)
	bool track_null_dev;
	// Time (ms) from first packet to use in generating a localization point
	unsigned int packet_window;
	// minimum number of packets to use to generate a track point for a stationary object
	unsigned int min_stationary_packets;
	// minimum timespan (ms) to use to generate a track point for a stationary object
	unsigned int min_stationary_timespan;

	// ============== Localization Parameters ================
	// minimum number of sensrs used in trilateration
	unsigned int min_trilateration_sensrs;
	// maximum number of sensrs used in trilateration
	unsigned int max_trilateration_sensrs;
	// maximum distance between an omnisensr and a device in trilateration
	float max_trilateration_distance;

	// Localizer
	Localizer<track_packet_buff_t::const_iterator>* loc_impl;
	// make the localizer a friend, so you can see in here
	friend class Localizer<track_packet_buff_t::const_iterator>;


	// ============= Track Summarization Parameters ===========
	// minimum time to generate a track (seconds)
	unsigned int min_track_time;
	// maximum time of a shopper track (seconds)
	unsigned int max_track_time;
	// minimum number of points for a valid trajectory
	unsigned int min_trajectory_points;
	// maximum radius of to collapse points at the "same point"
	float max_dwell_radius;
	// maximum time between pings of a device
	// (i.e., generate a separate track if there's a period of silence at least this long)
	unsigned int max_device_time;

	//============== Misc. Parameters =========================
	// set to true when replaying wifi packets
	bool replay_wifi;
	// set to true to get realtime location reporting
	bool realtime_reporting;
	// set to true to make the realtime reporting go out via the cloud
	bool public_realtime;

	// File of known static APs
	std::string known_AP_fn;
	// File of known service agents
	std::string known_agent_fn;
	// File of known devices of interest
	std::string known_dev_fn;
	// File containing pre-defined calibration parameters
	std::string calibration_params_fn;
	// File containing camera details (name, s/n, (x,y,z) position)
	std::string cam_details_fn;

	// An MQTT receiver used to get packet data from the aggregators
	MQTTReceiver data_recv;

	// maximum number of seconds between heartbeats
	unsigned int heartbeat_interval;


};

#endif /* INCLUDE_PROJECTS_WIFI_SHOPPER_TRACKER_WIFITRACKER_HPP_ */

/*
 * PacketProcessing_at_Sniffer.hpp
 *
 *  Created on: Feb 26, 2016
 *      Author: paulshin
 */

#ifndef WIFIPACKETPROCESSOR_HPP_
#define WIFIPACKETPROCESSOR_HPP_


#include <list>
#include <iostream>
#include <memory>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>


using namespace cv;
using namespace std;

#define APP_CONFIG_FILE				"config/WiFi_PacketProcessor/WiFi_PacketProcessor_config.txt"
#define IOT_GATEWAY_CONFIG_FILE		"config/IoT_Gateway/IoT_Gateway_config.txt"

#define	MAX_MEASUREMENT_BUFFER_PER_AP	10000			// -- # of tcpdump_t buffered at an AP
#define	MAX_MEASUREMENT_BUFFER_PER_DEV	1000			// -- # of tcpdump_t buffered at a Dev
#define	MAX_FOUND_DEVICES		9999

const int MAX_BUFFER = 1024;		// -- Max length of a line from tcpdump
const int MAX_TOKENS_PER_LINE = 500;	// -- Max # of tokens in a line of tcpdump input
const char* const DELIMITER = " ";

#define TCPDUMP_COMMAND_1_Custom 		"tcpdump -B 4096 -U -nevv -tttt -y ieee802_11_radio -s 64 -i "		// -- TODO: For Ubuntu-based custom sensor, smaller than 58 cannot retrieve all the necessary information Note: PacketStorage_t should be configured to have larger buffers than the length of a packet captured.
#define TCPDUMP_COMMAND_2 		" \"link[0] != 0x80\""		// -- TODO: larger than 52 fails with 'Bus error'. Don't know why. Note: PacketStorage_t should be configured to have larger buffers than the length of a packet captured.


class RemoteDevice_short {

public:
	RemoteDevice_short()  {
		Initialize();
	};

	virtual ~RemoteDevice_short() {
	}

	int mDeviceType;				// -- AP or Station
	int mChannel;				// -- If this device is an AP, then it will operate at a quite static channel. This will be the actual channel (not channel index)
	int mProtocolType;				// -- 802.11 protocol type among b/g/n
	double mDeviceInitialTime;		// -- The initial time that this device appeared for the first time
	double mSiteInitialTime;		// -- The initial time that this monitoring started
	double mLastlySeenTime;

	//bool mTobeRemoved;


	list<tcpdump_t>	mMeasBuffer_Dev;	// -- Temporary storage of measurements to be processed after a collection process
	list<tcpdump_t>	mMeasBuffer_Dev_prev;	// -- Temporary storage of measurements to be processed after a collection process
	list<tcpdump_t> 	mSummarizedMeas_Dev;

	unsigned int getUnprocessedMeasSize() { return (uint)mMeasBuffer_Dev.size(); };

	void Initialize() {
		//mTobeRemoved = false;

		mChannel = 0;	// -- Since the AP's channel is not confirmed yet, it is set to be 0 as an initial value. If this device is AP, then the channel will be confirmed later.
		mProtocolType = -1;
		mDeviceType = -1;

		mMeasBuffer_Dev.clear();
		mMeasBuffer_Dev_prev.clear();
		mSummarizedMeas_Dev.clear();

		mDeviceInitialTime = gCurrentTime();
		mSiteInitialTime = gCurrentTime();

	}

	void ClearMeasBuf() { mMeasBuffer_Dev.clear(); }
	unsigned int GetMeasBufSize() {	return (uint)mMeasBuffer_Dev.size(); }
	list<tcpdump_t> *AtMeasBuf() { return &mMeasBuffer_Dev; }
	bool PutToMeasBuf(tcpdump_t meas) {

		// -- The oldest measurement will be at front of the queue
		if(mMeasBuffer_Dev.size() < MAX_MEASUREMENT_BUFFER_PER_DEV) {
			mMeasBuffer_Dev.push_back(meas);
			return true;
		}
		return false;
	}

	string GetDevAddr() { return mNodeAddr; }
	void SetDevAddr(string newAddr) { mNodeAddr = newAddr; }


protected:

	string 		mNodeAddr;

//	unsigned int mTimediff_bin_width_accumulated[gNo_timediff_bin_width];			// -- Accumulated count in each time interval bin

};


#endif /* WIFIPACKETPROCESSOR_HPP_ */

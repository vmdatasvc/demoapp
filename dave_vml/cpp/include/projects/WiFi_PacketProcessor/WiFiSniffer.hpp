/*
 * PacketProcessing_at_Sniffer.hpp
 *
 *  Created on: Feb 26, 2016
 *      Author: paulshin
 */

#ifndef WIFISNIFFER_HPP_
#define WIFISNIFFER_HPP_

#include "core/OmniSensrApp.hpp"

#include "modules/iot/IoT_Common.hpp"

#include "modules/wifi/SnifferPacket.hpp"

#include <string>

#include <map>
#include <deque>

#include <boost/thread.hpp>
#include <boost/thread/condition.hpp>
// TODO: timer triggers
//#include <boost/asio.hpp>

class WiFiSniffer : public OmniSensrApp {

public:

	class Device{
	public:
		Device(const std::string& id, unsigned char tp) : _id(id), _type(tp), _first_seen(boost::posix_time::microsec_clock::universal_time()) { update(); }
		~Device(){}

		void update() { _last_seen = boost::posix_time::microsec_clock::universal_time(); }

		unsigned char getType() const {return _type;}
		const std::string& getID() const {return _id;};
		const boost::posix_time::ptime& lastSeen() const {return _last_seen;}

		// order by MAC address
		bool operator<(const Device& o) const{
			return _id < o.getID();
		}

	private:
		const std::string _id;
		const unsigned char _type;
		const boost::posix_time::ptime _first_seen;
		boost::posix_time::ptime _last_seen;
	};

public:
	WiFiSniffer(const std::string& configfn);
	~WiFiSniffer();

private:
	WiFiSniffer(const WiFiSniffer&);
	WiFiSniffer& operator=(const WiFiSniffer&);

protected:

	virtual void start();

	virtual unsigned int initConfig(ParseConfigTxt& parser);

	virtual void saveState(std::ostream& os) const;

	virtual void processCommand(const std::string& msg);

private:

	// parse a packet from libpcap, and put it into the buffer and notify the main thread
	void parsePacket(const uint8_t* buf, size_t len);

	void packData(std::ostream& os, const SnifferPacket& pack) const;

	// change the WiFi mode (to be used with delta callback)
	bool setWiFiMode(bool setInjecting);
	bool getWiFiMode() const { return is_injecting; }

	void updateDevice(const SnifferPacket& pack);
	// remove any device that is "too old"
	void cleanDevices();
	// remove ALL devices
	void removeAllDevices();

	// perform per-device measurement summary
	void summarizeAndSend(const std::string& dev, const boost::posix_time::ptime& curr_time);

	template <class Archive>
	void summarizeData(Archive& os, const std::deque<SnifferPacket>& buffer);

	// no-op delta callback
	void unsettableDeltaCallback(const std::string& var, const std::string& msg);

	// run a packet injection thread
	void runInjection();

	// run a packet sniffing thread, adding to the input buffers
	void runSniffing_libpcap();

	void runSniffing(){runSniffing_libpcap();}

	bool readNicData();

	static void pcap_callback(u_char *arg, const struct pcap_pkthdr*, const u_char*);

	// =============== Runtime Variables =============================

	// True iff running calibration injection
	bool is_injecting;

	// thread running injection
	boost::thread* injection_thread;
	boost::mutex injector_lock;

	// thread for monitoring
	boost::thread* monitor_thread;

	// mutex to lock access to the device structures
	boost::mutex device_lock;

	// mapping of all devices being tracked by ID
	std::map<std::string, Device> device_map;

	// mapping of device MAC to a buffer of measurements made
	std::map<std::string, std::deque<SnifferPacket> > device_meas_buffer;

	// mutex to lock the device measurement buffer
	boost::mutex buffer_lock;

	// mutex to lock/wake the main thread when data is ready to process
	boost::mutex main_lock;
	boost::condition main_cond;

	// FUTURE: async device timers
	//std::map<Device*, boost::asio::deadline_timer> device_timer;

	// ================ Configuration Variables =====================

	// NIC name to use for listenting
	std::string nic;

	// Time until we send a summarized packet (ms)
	unsigned int device_silence_interval;

	// maximum number of seconds allowed between Device measurements (s)
	unsigned int max_device_time;

	// Use only non-beacon packets
	bool use_nonbeacon;
	// Use only broadcast packets
	bool use_broadcast;
	// Use only probe request packets
	bool use_probe;
	// Summarize the packets received over the window
	bool meas_summarization;

	// Comma-separated nics for injection
	std::string inj_nic_str;

	// Filename (absolute path) to read injection and listening NIC string from
	std::string nic_fn;


	// vector of nics for injection
	std::deque<std::string> inj_nic_list;

	// time between injected packets (ms)
	unsigned int packet_injection_interval;
	// max backoff interval (Unif. random between [0,param]) (ms);
	unsigned int backoff_interval;

	// # of seconds to send heartbeats
	unsigned int heartbeat_interval;
	// # of seconds to clean up devices
	unsigned int cleanup_interval;
	// # of ms to wait for new data before processing
	unsigned int processing_interval;

};

template <class Cont>
void WiFiSniffer::summarizeData(Cont& sp_cont, const std::deque<SnifferPacket>& buffer){
	if(!meas_summarization){
		for(auto it=buffer.begin(); it != buffer.end(); it++){
			sp_cont.insert(sp_cont.end(), *it);
		}
	} else {
		// send one packet per channel with the average RSS
		std::map<unsigned char, std::pair<float, unsigned int>> avg_rss;
		for(auto it=buffer.begin(); it != buffer.end(); it++){
			auto avg_it = avg_rss.find(it->getChannel());
			if (avg_it == avg_rss.end()){
				avg_rss[it->getChannel()] = std::make_pair<float, unsigned int>(it->getRSS(), 1);
			} else {
				avg_it->second.first += it->getRSS();
				++(avg_it->second.second);
			}
		}

		auto it_b = buffer.begin();

		for(auto a_it = avg_rss.begin(); a_it != avg_rss.end(); a_it++){
			int16_t rss_avg = (a_it->second.second / a_it->second.first);
			SnifferPacket avg_pack(it_b->getType(), a_it->first, it_b->getMAC_str(), rss_avg);
			sp_cont.insert(sp_cont.end(), avg_pack);
		}
	}
}

#endif

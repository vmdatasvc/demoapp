/*
 * OmniSensr_AWS_IoT_Connector.hpp
 *
 *  Created on: Jan 18, 2017
 *      Author: Jimmy Tseng
 */

#ifndef SRC_PROJECTS_OmniSensr_AWS_IoT_Connector_OmniSensr_AWS_IoT_Connector_HPP_
#define SRC_PROJECTS_OmniSensr_AWS_IoT_Connector_OmniSensr_AWS_IoT_Connector_HPP_

#include "core/OmniSensrApp.hpp"
#include <string>
#include <vector>
#include <boost/thread.hpp>
#include <boost/thread/condition.hpp>
#include "json.hpp"
#include "modules/utility/redis_wrapper.hpp"
#include <boost/shared_ptr.hpp>
#include <mosquittopp.h>
#include <set>
#include <queue>

class OmniSensr_AWS_IoT_Connector : public OmniSensrApp {

	// -- MQTT client for connecting to AWS
	class AWS_MQTT : public mosqpp::mosquittopp {
	public:
		AWS_MQTT(std::string thingname, std::string host, int port, std::string rootCA, std::string cert, std::string privateKey, OmniSensr_AWS_IoT_Connector& conn_in);
		~AWS_MQTT();
		// callbacks inherited from mosquittopp
		void on_connect(int rc);
		void on_disconnect(int rc);
		void on_publish(int mid);
		void on_message(const struct mosquitto_message *message);
		void on_subscribe(int mid, int qos_count, const int *granted_qos);
		void on_error();

		// setup and connect client
		void initial();
		void restart();
		int pub(std::string topic, std::string payload, int qos);
		int sub(std::string topic, int qos);
		int shadow_update(std::string payload);
		void shadow_get();

		bool isConnected() {return connected;};
		bool isReady() {return connected && (totalSub == subCount);};
		bool hasDeltaArrived() {return deltaArrived;};
		nlohmann::json getDeltaMsg() {return deltaMsg;};
		void setDeltaFlag() {deltaArrived = false;};
		void setControlFlag() {controlReceived = false;};
		bool isControlReceived() {return controlReceived;};
		nlohmann::json getControlMsg() {return controlMsg;};
		nlohmann::json getControlTopic() {return controlTopic;};
	private:
		int pmid = 0;
		int latestPmid = -1;
		bool connected = false;
		std::string thingshadow;
		std::set<int> ack_track;
		std::string pRootCA;
		std::string pCert;
		std::string pPrivateKey;
		std::string pHost;
		int pPort;
		int totalSub = 0;
		int subCount = 0;
		nlohmann::json deltaMsg;
		bool deltaArrived = false;
		bool controlReceived = false;
		std::string controlMsg;
		std::string controlTopic;
		OmniSensr_AWS_IoT_Connector* conn;
	};

	public:

	OmniSensr_AWS_IoT_Connector(const std::string& configPath);
	virtual ~OmniSensr_AWS_IoT_Connector();
	// load testbed list
	void loadTestbedList(std::string filepath);
	protected:

	// main processing loop code goes here
	virtual void start();

	// initialize the configuration of your app
	virtual unsigned int initConfig(ParseConfigTxt& parser);

	// save the configuration status
	virtual void saveState(std::ostream& os) const;

	// process a command
	virtual void processCommand(const std::string& cmd);

	private:

	// configuration parameters
	double shadow_periodic_report_interval;
	float minimum_publish_interval;
	float mqtt_command_timeout;
	float tls_handshake_timeout;
	double full_report_interval;
	double refresh_desired_interval;

	std::string aws_iot_mqtt_host_testbed;
	std::string aws_iot_mqtt_host_production;
	std::string aws_iot_root_ca_filename;
	std::string aws_iot_certificate_filename;
	std::string aws_iot_private_key_filename;
	std::string aws_iot_cert_directory;

	bool aws_iot_reset_thingshadow_on_start;

	// variables to report to AWS
	bool realTimeReport;
	double heartbeat_interval;

	bool insync;

	// flag to determine if we should exit the main event loop
	bool killed;

	AWS_MQTT* awsMQTT;

	// sender and receiver to subscribe and publish to additional topics
	MQTTSender mqttSender;
	MQTTReceiver mqttRecv;

	// sample of a complex data structure.
	std::vector<std::string> control_msg_list;
	// Note the mutex, necessary to synchronize access to control_msg_list
	boost::mutex msg_lock;

	// Thread signalling mechanism; allows control channel or delta callback to wake up the main event loop
	boost::condition main_cond;
	boost::mutex main_lock;

	boost::mutex data_lock;

	// callbacks to process the control message
	void controlInt_callback(const std::string& param);

	// boost pointer for redis wrapper class
	boost::shared_ptr<redis_wrapper> rwrapper;

	// separating string for storing data channel messages in database
	std::string separate = "###separate###";

	double reportScale = 1.0;

	bool sendLastUpdate = true;
	bool checkDatabase = true;
	bool sendLocalDelta = false;

	nlohmann::json accUpdate;
	nlohmann::json localDelta;
	nlohmann::json accData;

	void updateAppState();

	// callback for local topic "shadow/update"
	void localShadowCallback(const mosquitto_message *msg);
	// callback for local topic "data/#"
	void localDataCallback(const mosquitto_message *msg);
	// callback for local topic "state/#" // -- Jimmy subscription for old apps
	void localStateCallback(const mosquitto_message *msg);
	void parseOldMsg(std::string msg, std::string key, std::string val);

	// limit the number of digits in floating point numbers
	double trimDigits(float input);

	std::queue<nlohmann::json> data_queue;

	bool isTestbed = false;
};

#endif /* SRC_PROJECTS_OMNISENSR_AWS_IOT_CONNECTOR_OMNISENSR2_AWS_IOT_CONNECTOR2_HPP_ */

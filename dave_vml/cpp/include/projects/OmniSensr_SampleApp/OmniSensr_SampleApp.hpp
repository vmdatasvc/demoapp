/*
 * OmniSensr_SampleApp.hpp
 *
 *  Created on: Mar 15, 2016
 *      Author: paulshin
 */

#ifndef OMNISENSR_SAMPLE_HPP_
#define OMNISENSR_SAMPLE_HPP_

#include "core/OmniSensrApp.hpp"

#include <string>
#include <vector>

#include <boost/thread.hpp>
#include <boost/thread/condition.hpp>

class OmniSensr_SampleApp : public OmniSensrApp {

public:

	OmniSensr_SampleApp(const std::string& configPath);
	virtual ~OmniSensr_SampleApp();

protected:

	// main processing loop code goes here
	virtual void start();

	// initialize the configuration of your app
	virtual unsigned int initConfig(ParseConfigTxt& parser);

	// save the configuration status
	virtual void saveState(std::ostream& os) const;

	// process a command
	virtual void processCommand(const std::string& cmd);

private:

	// some sample configuration parameters
	float priv_float;
	int priv_int;
	std::string priv_str;

	// configuration parameters exposed
	float pub_float;
	int pub_int;
	std::string pub_str;

	// configuration parameter exposed, but MUST be set from control channel
	int control_int;

	// we'll modify this from the control channel, but not see in the ThingShadow
	int sleep_interval;

	// flag to determine if we should exit the main event loop
	bool killed;

	// sample of a complex data structure.
	std::vector<std::string> control_msg_list;
	// Note the mutex, necessary to synchronize access to control_msg_list
	boost::mutex msg_lock;

	// Thread signalling mechanism; allows control channel or delta callback to wake up the main event loop
	boost::condition main_cond;
	boost::mutex main_lock;

	// callbacks to process the delta message for the controlInt parameter
	void controlInt_callback(const std::string& param);

};


#endif /* OMNISENSR_SAMPLE_HPP_ */

/*
 * OmniSensr_IR_Controller.hpp
 *
 *  Created on: Apr 5, 2017
 *      Author: ctseng
 */

#ifndef INCLUDE_PROJECTS_OMNISENSR_IR_CONTROLLER_OMNISENSR_IR_CONTROLLER_HPP_
#define INCLUDE_PROJECTS_OMNISENSR_IR_CONTROLLER_OMNISENSR_IR_CONTROLLER_HPP_

#include "core/OmniSensrApp.hpp"

#include <string>
#include <vector>

#include <boost/thread.hpp>
#include <boost/thread/condition.hpp>

class OmniSensr_IR_Controller : public OmniSensrApp {

public:

	OmniSensr_IR_Controller(const std::string& configPath);
	virtual ~OmniSensr_IR_Controller();

protected:

	// main processing loop code goes here
	virtual void start();

	// initialize the configuration of your app
	virtual unsigned int initConfig(ParseConfigTxt& parser);

	// save the configuration status
	virtual void saveState(std::ostream& os) const;

	// process a command
	virtual void processCommand(const std::string& cmd);

private:
	int heartbeat_interval;

	unsigned int led_red_pin[3];
	unsigned int led_green_pin[3];

	int pi;

	std::string devIP;

	// flag to determine if we should exit the main event loop
	bool killed;

	// sample of a complex data structure.
	std::vector<std::string> control_msg_list;
	// Note the mutex, necessary to synchronize access to control_msg_list
	boost::mutex msg_lock;

	// Thread signalling mechanism; allows control channel or delta callback to wake up the main event loop
	boost::condition main_cond;
	boost::mutex main_lock;
	long long current_msec();
	void setLED(int num, std::string color);
	void blinkLED(int count, std::string color);
	void getKeyPress();
	boost::thread* keyPressThread;
};




#endif /* INCLUDE_PROJECTS_OMNISENSR_IR_CONTROLLER_OMNISENSR_IR_CONTROLLER_HPP_ */

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
//Declare sentry ifndef to stop reinclude when header is reused
#ifndef IntervaledTask_HPP
#define IntervaledTask_HPP

//any includes required for declarations
//In this case since we have only a basic declaration
// includes are required to define the shared_ptr declared below
#include <boost/shared_ptr.hpp>
#include "stdint.h"
#include "modules/iot/MQTTClient.hpp"

using namespace std;


//if using namespace declare here
//namespace healthapp 
//{
//Class declaration
class IntervaledTask
{
public:
//protected stuff
	IntervaledTask(){
		mqttSender.start();
	}
	~IntervaledTask(){
		mqttSender.stop();
	}
	void updateFromShadow(int newInterval);
	void setInterval(int newInterval);
	virtual void DoTask(){};
	int getTaskInterval();
	int getRestartCount();
	int getInitInterval();
	void resetCount();
private:
	//Stuff private to this class
	//Members vars go here
	//VM coding standard uses a lower case "m", capital next
	// and "camelCase"
	// letter to indicate a member variable
	// mMyMemberVar
protected:
	int mInitialInterval;
	int mTaskInterval;
	int restartCount;
	int numToStop;
	MQTTSender mqttSender;
	//protected stuff
};

typedef boost::shared_ptr<IntervaledTask> IntervaledTaskPtr;


// close namespace (if any)
//};


//close sentry ifndef
#endif

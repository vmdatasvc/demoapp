/*
 * OmniSensr_Health_App.hpp
 *
 *  Created on: Jan 31, 2017
 *      Author:
 */

#ifndef OmniSensr_Health_App_HPP_
#define OmniSensr_Health_App_HPP_

#include "core/OmniSensrApp.hpp"

#include <string>
#include <vector>
#include <set>

#include <boost/thread.hpp>
#include <boost/thread/condition.hpp>
#include <CountDownTimer.hpp>


class OmniSensr_Health_App : public OmniSensrApp {

public:

	OmniSensr_Health_App(const std::string& configPath);
	virtual ~OmniSensr_Health_App();

protected:

	// main processing loop code goes here
	virtual void start();

	// initialize the configuration of your app
	virtual unsigned int initConfig(ParseConfigTxt& parser);

	// save the configuration status
	virtual void saveState(std::ostream& os) const;

	// process a command
	virtual void processCommand(const std::string& cmd);

private:
	int OmniSensrVer;
	int heartbeat_timeout;
	std::string snapShotUrl;
	std::string devSubtopic;
	int watchdog_pin;
	double watchdog_interval;
	pair<int, int> ledset_pin[3];

	// -- Device Shadow Parameters
	std::string mIoT_OSVer;
	std::string mIoT_QAVer;
	bool mIoT_QA_OK;
	bool mIoT_WiFi_Role;
	bool mIoT_Face_Role;
	bool mIoT_Visn_Role;
	bool mIoT_Traq_Role;
	double mIoT_State_Report_Interval;
	double mIoT_Heartbeat_Interval;
	int mIoT_Cpu_Collect_Interval;
	int mIoT_Cpu_Report_Interval;
	int mIoT_Temp_Collect_Interval;
	int mIoT_Temp_Report_Interval;
	int mIoT_App_Cpu_Collect_Interval;
	int mIoT_App_Mem_Collect_Interval;
	int mIoT_App_Cpu_Report_Interval;
	int mIoT_App_Mem_Report_Interval;
	double mIoT_Cpu_Threshold;
	double mIoT_Temp_Threshold;

	double mIoT_Cpu_Load;
	double mIoT_Cpu_Temp;

	double mInitialTime;
	double mData_Buffer_Interval;
	std::string mSoftware_ver;

	std::string &ltrim(std::string &s);
	std::string &rtrim(std::string &s);
	std::string &trim(std::string &s);

	void loadQAState();
	void saveQAState(bool pass_fail, std::string os_ver);

	// Routine to read /usr/local/www/version.txt
	std::string getOSVer();

	bool getServiceRunState(std::string serviceName);

	void updateQAVer(const std::string& param);
	void updateOSVer(const std::string& param);
	void startStopServiceByName(std::string serviceName, std::string value);
	void startStopServiceByAppName(std::string serviceName, std::string value);
	void getSnapshot(const std::string& param);


	std::set<std::string> ignoreVariables;
	// Note the mutex, necessary to synchronize access to control_msg_list
	boost::mutex msg_lock;

	// Thread signalling mechanism; allows control channel or delta callback to wake up the main event loop
	boost::condition main_cond;
	boost::mutex main_lock;

	// Countdown timers for apps
	CountDownTimerPtr wifiTimer;
	CountDownTimerPtr demoTimer;
	CountDownTimerPtr visnTimer;
	CountDownTimerPtr traqTimer;
	CountDownTimerPtr connTimer;

	CountDownTimerPtr cpuCollectTimer;
	CountDownTimerPtr tempCollectTimer;

	CountDownTimerPtr AppCPUTimer;
	CountDownTimerPtr AppMEMTimer;

	boost::mutex cpuStat_lock;
	boost::mutex tempStat_lock;
	std::vector<int> cpuStat;
	std::vector<float> tempStat;
	std::vector<float> wifiCPUStat;
	std::vector<float> visnCPUStat;
	std::vector<float> demoCPUStat;
	std::vector<float> traqCPUStat;
	std::vector<float> wifiMEMStat;
	std::vector<float> visnMEMStat;
	std::vector<float> demoMEMStat;
	std::vector<float> traqMEMStat;

	void Service_callback(const std::string& servicename, const std::string& param);
	void restartServiceByAppName(const std::string serviceName);
	void localHeartbeat_callback(const struct mosquitto_message *message);
	void localAppCrash_callback(const struct mosquitto_message *message);
	void localMetric_callback(const struct mosquitto_message *message);

	void updateQAThreadFunc(const std::string& param);
	void Role_ThreadFunc(const std::string& param, const std::string& app);

	void CpuCollectInterval_callback(const std::string& param);
	void TempCollectInterval_callback(const std::string& param);

	void AppCollectInterval_callback(const std::string& param, const std::string& type);
	void pokeWatchDog();

	void setLEDColor(int setnum, std::string color);
	void rebootDevice();

	void updateAppState();
};


#endif /* OmniSensr_Health_App_HPP_ */

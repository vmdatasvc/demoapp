/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef IntervaledTaskFactory_HPP
#define IntervaledTaskFactory_HPP

#include <boost/shared_ptr.hpp>
#include <IntervaledTask.hpp>
#include <string>

class IntervaledTaskFactory
{
public:
	static IntervaledTaskPtr make_task(std::string choose, int numRestart = 10);
};

typedef boost::shared_ptr<IntervaledTask> IntervaledTaskPtr;

#endif

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
//Declare sentry ifndef to stop reinclude when header is reused
//#include <iostream>
//#include <boost/asio.hpp>
//#include <boost/bind.hpp>

//#include <boost/shared_ptr.hpp>
//#include <boost/thread/thread.hpp>

#ifndef CountDownTimer_HPP
#define CountDownTimer_HPP
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/timer/timer.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/function.hpp>
#include <IntervaledTask.hpp>
//namespace healthapp 
//{

class CountDownTimer
{
public:
    CountDownTimer(int timerInterval, IntervaledTaskPtr workerObject = NULL);
    ~CountDownTimer();
    void TimerReset(int newInterval);
    // Updated on June 26, to get task interval
	int getInterval();
private:
	//Pointer to the object whose DoTask() we will call upon timer expires
	IntervaledTaskPtr mExpCallback;
	boost::function<void (const boost::system::error_code& error)> handle;
protected:
    void RunIOService();
    void OnTimerExpire(const boost::system::error_code& error);
	bool mQuit;
//	int mInterval;
	boost::mutex mTimerMutex;
	boost::asio::io_service         mLocService;
    boost::asio::io_service::work   mWork;
    boost::thread                   mTimerThread;
    boost::asio::deadline_timer     mTimer;
};

typedef boost::shared_ptr<CountDownTimer> CountDownTimerPtr;

//close namespace
//}
#endif

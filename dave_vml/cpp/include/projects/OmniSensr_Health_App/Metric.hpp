/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
#ifndef Metric_HPP
#define Metric_HPP
#include <CountDownTimer.hpp>
#include <IntervaledTaskFactory.hpp>
#include <string>

class Metric
{
public:
	std::string mCurrentMetricSettings;
	std::string mMetricName;
	std::string *mMNamePtr = &mMetricName;
	std::string *mMCurSetPtr = &mCurrentMetricSettings;

	std::vector<std::string> strToArray(std::string s);
	Metric(std::string str);
	~Metric();
//	TODO:create UpdateInterval()
	void UpdateInterval(std::string str);
protected:
//	IntervaledTaskFactory factory;
//	IntervaledTaskPtr CollectTask;
//	IntervaledTaskPtr ReportTask;
	CountDownTimerPtr CollectTimer;
	CountDownTimerPtr ReportTimer;
};

typedef boost::shared_ptr<Metric> MetricPtr;
#endif

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "legacy/vision_blob/BlobSizeEstimate.hpp"
#include "legacy/low_level/XmlParser.hpp"
#include <stack>

namespace ait 
{

BlobSizeEstimate::BlobSizeEstimate()
{
}

BlobSizeEstimate::~BlobSizeEstimate()
{
}

// DATA



//
// OPERATIONS
//

void 
BlobSizeEstimate::initializeParameters(int numBlockRows, int numBlockColumns, 
                                       double distributionThreshold, double heightFactor,
                                       int height, int width)
{
  mNumBlockRows = numBlockRows;
  mNumBlockColumns = numBlockColumns;
  mDistributionThreshold = distributionThreshold;
  mHeightFactor = heightFactor;
  mHeight = height;
  mWidth = width;

  mWidthHistogram.resize( mWidth, mHeight * mNumBlockColumns);
  mWidthHistogram.setAll(0);
  mBlobWidth.resize( mNumBlockRows, mNumBlockColumns );
  mBlobHeight.resize( mNumBlockRows, mNumBlockColumns );

  mBlobWidthXY.resize( mWidth,mHeight );
  mBlobHeightXY.resize( mWidth,mHeight );
}


void 
BlobSizeEstimate::learnObjectSizeXY( const Image8 & img ) 
{
  int blockWidth = mWidth / mNumBlockColumns;
  int blockHeight = mHeight / mNumBlockRows;
  int blockColumn;
  int x,y;
  int segLength, prev, curr;

  // update the distribution of object widths
  for (y = 0; y < mHeight; y++) {
    segLength = 0;
    prev = 0;
    for (x = 0; x < mWidth; x++) {
      curr = img(x,y);
      if (curr)
        segLength++;
      if (prev && !curr) {
        blockColumn = (x - segLength / 2) / blockWidth;
        mWidthHistogram(segLength, blockColumn * mHeight + y)++;
        segLength = 0;
      }
      prev = curr;
    }
  }
}


void
BlobSizeEstimate::computeBlockEstimates() 
{
  int brow, bcol;
  int blockHeight = mHeight / mNumBlockRows;
  int blockWidth  = mWidth / mNumBlockColumns;
  int blockRadius = 1;
  int r, c, x, y;
  int widthEst, heightEst;
  Image<double> histogram(mWidth,1);
  // Image<double> histogram2(1,ht);
  double numSeg, sumSeg;

  // for each block compute the object size
  for (brow = 0; brow < mNumBlockRows; brow++) {
    for (bcol = 0; bcol < mNumBlockColumns; bcol++) {
      histogram.setAll(0.0);
      // histogram2.setAll(0.0);

      for (r = brow - blockRadius; r <= brow + blockRadius; r++) {
        for (c = bcol - blockRadius; c <= bcol + blockRadius; c++) {
          if (r >= 0 && r < mNumBlockRows && c >= 0 && c < mNumBlockColumns) {
            // accumulate width histogram
            for (y = r * blockHeight; y < (r+1) * blockHeight; y++)
            for (x = 0; x < mWidth; x++)
              histogram(x,0) += mWidthHistogram( x, c * mHeight + y );

          }
        }
      }

      // compute the width estimate
      numSeg = 0;
      for (x = 0; x < mWidth; x++)
        numSeg += histogram(x,0);
      sumSeg = 0;
      for (x = 0; x < mWidth && sumSeg < mDistributionThreshold * numSeg; x++)
        sumSeg += histogram(x,0);
      widthEst = x;

      heightEst = widthEst * mHeightFactor;  //  height is a fixed factor of width

      mBlobWidth(brow,bcol) = widthEst;
      mBlobHeight(brow,bcol) = heightEst;
    }
  }

  computeInterpolatedImage();
}

void
BlobSizeEstimate::computeInterpolatedImage()
{
  int x, y, m, n, sum;
  int brow, bcol;
  int blockHeight = mHeight / mNumBlockRows;
  int blockWidth  = mWidth / mNumBlockColumns;
  // compute the interpolated XY sizes given the block size estimates

  // replicate the block estimate on each pixel of the block
  for (brow = 0; brow < mNumBlockRows; brow++) 
  {
    for (bcol = 0; bcol < mNumBlockColumns; bcol++) 
    {
      for (x = bcol * blockWidth; x < (bcol + 1) * blockWidth; x++)
      {
        for (y = brow * blockHeight; y < (brow + 1) * blockHeight; y++)
        {
          mBlobWidthXY(x,y) = mBlobWidth(brow,bcol);
          mBlobHeightXY(x,y) = mBlobHeight(brow,bcol);
        }
      }
    }
  }

  Image<float> widthXY(mBlobWidthXY);  
  //TODO: ADDED
  Image<float> heightXY(mBlobHeightXY);
  
  /*

  //ORIGINAL CODE, ONLY INTERPOLATED ESTIMATED WIDTH
  // use a mean filter for interpolation -- this is slow, but i think it's ok since it will be run just once
  int xrad = blockWidth / 2;  
  int yrad = blockHeight / 2;
  int blockArea = blockWidth * blockHeight;

  for (x = 0; x < mWidth-blockWidth; x++)
  {
    for (y = 0; y < mHeight-blockHeight; y++)  {
      sum = 0;
      for (m = 0; m < blockWidth; m++)
        for (n = 0; n < blockHeight; n++)
          sum += widthXY(x+m,y+n);
      mBlobWidthXY(x + xrad, y + yrad) = sum / blockArea;
      mBlobHeightXY(x + xrad, y + yrad) = (int) (mHeightFactor * sum / blockArea);
    }
  }
  */

  // use a mean filter for interpolation -- this is slow, but i think it's ok since it will be run just once
  int xrad = blockWidth / 2;  
  int yrad = blockHeight / 2;
  float blockArea = blockWidth * blockHeight;

  float widthSum = 0;
  float heightSum = 0;
  sum = 0;

  for (x = 0; x < mWidth-blockWidth; x++)
  {
    for (y = 0; y < mHeight-blockHeight; y++)  {
      widthSum = 0;
      heightSum = 0;
      for (m = 0; m < blockWidth; m++)
        for (n = 0; n < blockHeight; n++)
        {
          widthSum += widthXY(x+m,y+n);
          heightSum += heightXY(x+m,y+n);
        }
      mBlobWidthXY(x + xrad, y + yrad) = widthSum / blockArea;
      mBlobHeightXY(x + xrad, y + yrad) = heightSum / blockArea;
    }
  }
}

void
BlobSizeEstimate::visualize(Image32& img)
{
  int brow, bcol;
  int blockHeight = mHeight / mNumBlockRows;
  int blockWidth  = mWidth / mNumBlockColumns;
  int widthEst, heightEst;

  // for each block compute the object size
  for (brow = 0; brow < mNumBlockRows; brow++) {
    for (bcol = 0; bcol < mNumBlockColumns; bcol++) {
      widthEst = mBlobWidth(brow,bcol);
      heightEst = mBlobHeight(brow,bcol);

      int color;
      switch (brow % 2) {  // alternate printing orange and green boxes
        case 0: color = PV_RGB(255,128,0);  break;
        case 1: color = PV_RGB(0,255,0);  break;  
      }
      img.rectangle(
        bcol*blockWidth,
        brow*blockHeight,  
        bcol*blockWidth + widthEst,
        brow*blockHeight + heightEst, 
        color);

      img.rectangle( 
        bcol*blockWidth + 1, 
        brow*blockHeight + 1,  
        bcol*blockWidth + widthEst + 1,
        brow*blockHeight + heightEst + 1, 
        color);
    }
  }
}

void
BlobSizeEstimate::save(  const std::string & filename  )
{
  int r,c;
  // write to XML
  std::ofstream fileStream(filename.c_str());

  // Print comments and settings open tag.
  fileStream << "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n";
  fileStream << "<blob_size_function>\n";
  fileStream << "<frame_size>" << mWidth << "," << mHeight << "</frame_size>\n";
  fileStream << "<block_size>" << mWidth/mNumBlockColumns << "," << mHeight/mNumBlockRows << "</block_size>\n";
  for (r = 0; r < mNumBlockRows; r++) {
    fileStream << "<row>\n";
    for (c = 0; c < mNumBlockColumns; c++) {
      fileStream << "<blob_size>" << mBlobWidth(r,c) << "," << mBlobHeight(r,c) << "</blob_size>\n";
    }
    fileStream << "</row>\n";
  }
  fileStream << "</blob_size_function>\n";
  fileStream.close();  // Close output file.
}

/**
 * XML parsing.
 */
class BlobFileHandler : public Handler
{
public:
  BlobFileHandler() {};

  virtual void start (const std::string& ns, const std::string& tag, const char **attr)
  {
    mTagStack.push(tag);
  }

  virtual void cdata (const XML_Char *s, int len)
  {
    if (!mTagStack.empty())
    {
      if (mTagStack.top() == "blob_size")
      {
        if (idx >= mSizes.size())
        {
          PvUtil::exitError("Too Many points. File does not correspond to frame and block size");
        }
        sscanf(s,"%f,%f",&mSizes(idx).x,&mSizes(idx).y);
        idx++;
      }
      else if (mTagStack.top() == "frame_size")
      {
        sscanf(s,"%i,%i",&mFrameSize.x,&mFrameSize.y);
      }
      else if (mTagStack.top() == "block_size")
      {
        Vector2i blockSize;
        sscanf(s,"%i,%i",&blockSize.x,&blockSize.y);
        mSizes.resize(mFrameSize.y/blockSize.y,mFrameSize.x/blockSize.x);
        idx = 0;
      }
    }
  }

  virtual void end (const std::string& ns, const std::string& tag)
  {
    mTagStack.pop();
  }

  Vector2i mFrameSize;
  std::stack<std::string> mTagStack;
  Matrix<Vector2f> mSizes;
  int idx;

  Vector2f parseCoord(const std::string& in);
};

void
BlobSizeEstimate::load(const std::string& filename)
{
  BlobFileHandler hand;

  XmlParser parser;
  parser.registerNamespaceHandler("", &hand); 

  if (!PvUtil::fileExists(filename.c_str()))
  {
    PvUtil::exitError("File not found: %s",filename.c_str());
  }

  parser.parseFile(filename);

  mNumBlockRows = hand.mSizes.rows();
  mNumBlockColumns = hand.mSizes.cols();
  mWidth = hand.mFrameSize.x;
  mHeight = hand.mFrameSize.y;

  mBlobWidth.resize( mNumBlockRows, mNumBlockColumns );
  mBlobHeight.resize( mNumBlockRows, mNumBlockColumns );

  mBlobWidthXY.resize( mWidth,mHeight );
  mBlobHeightXY.resize( mWidth,mHeight );

  mHeightFactor = hand.mSizes(0,0).y/hand.mSizes(0,0).x;

  int i,j;
  for (i = 0; i < mBlobWidth.rows(); i++)
  {
    for (j = 0; j < mBlobWidth.cols(); j++)
    {
      mBlobWidth(i,j) = hand.mSizes(i,j).x;
      mBlobHeight(i,j) = hand.mSizes(i,j).y;
    }
  }

  computeInterpolatedImage();
}

void
BlobSizeEstimate::scale(const Vector2f& scaleFactor)
{
  mBlobWidthXY.rescale(
    round(mBlobWidthXY.width()*scaleFactor.x),
    round(mBlobWidthXY.height()*scaleFactor.y));
  mBlobWidthXY.mult(scaleFactor.x);

  mBlobHeightXY.rescale(
    round(mBlobHeightXY.width()*scaleFactor.x),
    round(mBlobHeightXY.height()*scaleFactor.y));
  mBlobHeightXY.mult(scaleFactor.y);
}


void
BlobSizeEstimate::crop(const Rectanglei& roi)
{
  //This raises an exception when the blobwidth/height matrix is smaller than the roi matrix

  //PVASSERT()



  mBlobWidthXY.crop(roi.x0,roi.y0,roi.width(),roi.height());
  mBlobHeightXY.crop(roi.x0,roi.y0,roi.width(),roi.height());
}


//
// ACCESS
//

//
// INQUIRY
//

}; // namespace ait


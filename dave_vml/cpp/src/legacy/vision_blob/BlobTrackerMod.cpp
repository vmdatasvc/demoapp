/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#pragma warning(disable:4503)
#endif

#include "legacy/vision_blob/TrajectoryEvents.hpp"
#include "legacy/vision_blob/BlobTrackerMod.hpp"
#include "legacy/vision_blob/MultiFrameTrack.hpp"
#include "legacy/vision/ImageAcquireMod.hpp"
#include "legacy/vision_blob/TrajectoryEvents.hpp"
#include "legacy/vision_blob/BlobSizeEstimate.hpp"
//#include <legacy/pv/PvCanvasImage.hpp>
#include <boost/lexical_cast.hpp>
#include "legacy/vision_blob/ForegroundSegmentMod.hpp"
#include <legacy/vision_tools/ColorConvert.hpp>
#include <legacy/vision_blob/Trajectory.hpp>

#include <legacy/types/strutil.hpp>
#include <legacy/types/DateTime.hpp>

#include <legacy/vision_tools/PolygonGetMask.hpp>

namespace ait 
{

namespace vision
{


BlobTrackerMod::BlobTrackerMod()
{
  //TODO: TEMP, READ FROM XML FILE
  mIsMotionEnabled = false;
  mSaveCsvFileEvents = false;
}

BlobTrackerMod::BlobTrackerMod(const std::string& moduleName)
{
  //TODO: TEMP, READ FROM XML FILE
  mIsMotionEnabled = false;
  mSaveCsvFileEvents = false;
}

BlobTrackerMod::~BlobTrackerMod()
{
}

void 
BlobTrackerMod::init()
{
  VisionMod::init();

  mUseAlternateTracker = false;

//  mpVmsPacket.reset(new VmsPacket(this));

  Image32Ptr pFrame = getImageAcquireModule()->getCurrentFramePtr();
  // TODO: get image frame here. or initialize with width and height.
  mFrameWidth = pFrame->width();
  mFrameHeight = pFrame->height(); 

//  mEventChannels = mSettings.getIntList("eventChannelId", "", false);
  
//  mBenchmark.init(mSettings.getPath(),"Blob Tracking");

  // TODO: replace the following parameters with init
  mIsTopViewCamera = mSettings.getBool("isTopViewCamera [bool]",0);

  mVerbose = mSettings.getBool("Visualization/printMessages [bool]",0);

  mPaintVisualization = mSettings.getBool("Visualization/enabled [bool]",1);

  mSendPreprocessTrajectories = mSettings.getBool("sendPreprocessTrajectories [bool]",0);

  //Alter the visualization frame size if necessary
  if (mPaintVisualization)
    getImageAcquireModule()->setVisualizationFrameSize(Vector2i(640,480));

  mMaxSnakeSize = mSettings.getInt("numFrames",10);
  mMinTrackCreationDistance = mSettings.getFloat("minTrackCreationDistance",1.0);

  mpNewTrackScanImage.reset(new Image8());

  mNewTrackId = 1;

  mMaxConcurrentTracks = mSettings.getInt("maxConcurrentTracks",10);

  mVisualizeSnakeTail = mSettings.getBool("Visualization/showFrameAtTail [bool]",0);

  if (mVisualizeSnakeTail)
  {
    mLastColorFrames.setCapacity(mMaxSnakeSize);
  }

  mPostProcessLowerInterval = mSettings.getFloat("minPostProcessInterval",60.0);
  mPostProcessUpperInterval = mSettings.getFloat("maxPostProcessInterval", 600.0);
  
  mNextPostProcessTime = getCurrentTime() + mPostProcessLowerInterval;
  mNextForcePostProcessTime = getCurrentTime() + mPostProcessUpperInterval;

  //Initialize the map of event polygons
  std::list<SimplePolygon> polyList = convertList(mSettings.getString("eventsPolygon",""));
  std::list<SimplePolygon>::const_iterator iPoly;
  
  std::list<int>::const_iterator iEvtChan = mEventChannels.begin();
  for (iPoly = polyList.begin(); iPoly != polyList.end(); iPoly++)
  {    
    mEventsPolygon[*iEvtChan] = *iPoly;
    iEvtChan++;
  }

  //Tell the vms packet that each event channel will await completion notification
//  for (iEvtChan = mEventChannels.begin(); iEvtChan != mEventChannels.end(); iEvtChan++)
//    mpVmsPacket->enableCompletionNotification(*iEvtChan);
   

  if (!mEventsPolygon.empty() && mEventChannels.size() != mEventsPolygon.size())
    PvUtil::exitError("ERROR: BlobTrackerMod: Different number of event channel IDs and polygons");

  mSavePreTrajectories = mSettings.getBool("savePreTrajectories [bool]", false);
  mSaveCsvFileEvents = mSettings.getBool("saveCsvFileEvents [bool]", false);

  mTrajectoryOutputFolder = mSettings.getString("trajectoryOutputFolder", "");

  if (mSavePreTrajectories || mSaveCsvFileEvents)  
    if (!PvUtil::fileExists(mTrajectoryOutputFolder.c_str()))
      PvUtil::exitError("BlobTrackerMod: ERROR: 'trajectoryOutputFolder' does not exist.");
  


  /*mMaxPreTrajectories = mSettings.getInt("maxPreTrajectories", 500);*/

  if (!mpForegroundSegmentMod->getBlobSizeEstimate().get())
  {
    PvUtil::exitError("The blob size estimator must be enabled in the ForegroundSegmentMod.");
  }

  //SavedTrajectories.clear();

  mTrajEvents.init(mpForegroundSegmentMod->getBlobSizeEstimate());    
  mTrajEvents.setLineThreshold(
    mSettings.getFloat("TrajectoryEvents/LineThreshold", 0.0));
  mTrajEvents.setPolyThreshold(
    mSettings.getFloat("TrajectoryEvents/PolyThreshold", 0.0));
  mTrajEvents.setCardTimeThreshold(
    mSettings.getFloat("TrajectoryEvents/CardTimeThreshold", 2.0)
    * getFramesPerSecond());
  

  //guaranteed to be unique by outside process that inits config files
  mVideoSourceId = atoi(Settings::replaceVars("<VideoSourceId>",mSettings.getPath()).c_str());
  PVASSERT(mVideoSourceId != -1);

  mSendTrajectoriesAsEventAttribute = mSettings.getBool("sendTrajectory [bool]",false);

  //Resize the motion module to have the same size as the foreground segmentation size

  if (mIsMotionEnabled)
  {  
    Rectanglei roi = mpForegroundSegmentMod->getRoiRectangle();
    Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor(); 

    mMaxNoMotionTime = mSettings.getFloat("maxNoMotionTime", 60.0);

    mpMotionMod->setScaleAndRoi(scaleFactor, roi);    
  }


  ///
  //initialize the start polygon list
  std::list<SimplePolygon> startTrackPolyList;
  startTrackPolyList = convertList(mSettings.getString("startTrackPolygon","(0,0,320,0,320,240,0,240,0,0)"));
  
  for (iPoly = startTrackPolyList.begin(); iPoly != startTrackPolyList.end(); iPoly++)
  {
    SimplePolygon startTrackPoly;
    startTrackPoly = *iPoly;

    Polygon p;

    //Remove last redundant entry, polygon class does not use it
    startTrackPoly.pop_back();
    p.setVertices(startTrackPoly);

    //Add polygon object to start track list
    mStartTrackPolyList.push_back(p);
  }  

  //initialize the kill track polygon
  std::list<SimplePolygon> killTrackPolyList;
  killTrackPolyList = convertList(mSettings.getString("killTrackPolygon", ""));

  for (iPoly = killTrackPolyList.begin(); iPoly != killTrackPolyList.end(); iPoly++)
  {
    SimplePolygon killTrackPoly;
    killTrackPoly = *iPoly;

    Polygon p;

    //Remove last redundant entry, polygon class does not use it
    killTrackPoly.pop_back();
    p.setVertices(killTrackPoly);  

    mKillTrackPolyList.push_back(p);
  }
  

  //get ROI in pixel coords from FSM
  Rectanglei intRoi = mpForegroundSegmentMod->getRoiRectangle();
  Rectanglef roi((float)intRoi.x0, 
    (float)intRoi.y0, 
    (float)intRoi.x1, 
    (float)intRoi.y1);


  //generate the masks  
  Image8 tmp;

  mStartTrackMask.resize(round(roi.width()), round(roi.height()));
  mStartTrackMask.setAll(0);
  for (iPoly = startTrackPolyList.begin(); iPoly != startTrackPolyList.end(); iPoly++)
  {
    getMask(*iPoly, roi,tmp);
    for (unsigned int i = 0; i < mStartTrackMask.size(); i++)
      mStartTrackMask(i) |= tmp(i);
  }

  mKillTrackMask.resize(round(roi.width()), round(roi.height()));
  mKillTrackMask.setAll(0);
  for (iPoly = killTrackPolyList.begin(); iPoly != killTrackPolyList.end(); iPoly++)
  {
    getMask(*iPoly, roi,tmp);
    for (unsigned int i = 0; i < mKillTrackMask.size(); i++)
      mKillTrackMask(i) |= tmp(i);
  }

  //rescale the mask by the appropriate scaling factor
  Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();

  mStartTrackMask.rescale(
    roi.width()*scaleFactor.x,
    roi.height()*scaleFactor.y);

  mKillTrackMask.rescale(
    roi.width()*scaleFactor.x,
    roi.height()*scaleFactor.y); 

  mRepelTracks = mSettings.getBool("repelTracks", false);

  assert(mpForegroundSegmentMod->getBlobSizeEstimate().get());

}

void
BlobTrackerMod::finish()
{
  /*
  if (mSavePreTrajectories)
  {
    Trajectory::saveCSV("preTrajectories.csv",mSavedTrajectories);
  }
  */

  postProcessTracks(true);
//  mpVmsPacket->finish();
//  RealTimeMod::finish();
}

//
// OPERATIONS
//

void
BlobTrackerMod::cleanForegroundSegmentation(Image8& img, const Vector2f& pt)
{
  assert(mpForegroundSegmentMod->getBlobSizeEstimate().get());
  BlobSizeEstimate& blobSizeEstimate = *(mpForegroundSegmentMod->getBlobSizeEstimate());
  float sizeX,sizeY;
  blobSizeEstimate.getEstimatedSize(pt.x,pt.y,sizeX,sizeY);

  // Expand considerably to avoid another track being created in this same
  // frame for the same blob.
  
  const float expandBy = 1.2f; //ORIGINAL 1.2f
  img.ellipseFill(
    round(pt.x),
    round(pt.y),
    sizeX*expandBy/2,
    sizeY*expandBy/2,
    0);    
}

void
BlobTrackerMod::scanForNewTracks()
{  
  assert(mpForegroundSegmentMod->getBlobSizeEstimate().get());
  PVASSERT(mStartTrackMask.width() == mpForegroundSegmentMod->getFilteredForeground()->width());
  PVASSERT(mStartTrackMask.height() == mpForegroundSegmentMod->getFilteredForeground()->height());

  unsigned int x,y;

  // Make a copy since the original will be modified.  
  (*mpNewTrackScanImage) = *mpForegroundSegmentMod->getFilteredForeground();
  Image8& img = *mpNewTrackScanImage;

  //hack below
  Image8 rawForeground = *mpForegroundSegmentMod->getForeground();
  //end hack

  Tracks::iterator iTrack;

  for (iTrack = mTracks.begin(); iTrack != mTracks.end(); iTrack++)
  {
    (*iTrack)->cleanForegroundSegmentation(rawForeground);
  }


  mpForegroundSegmentMod->filterByBlobSize(rawForeground, img); 

  

  std::list<Vector2f> newTrackLocations;


  if (mIsMotionEnabled)
  {
    PVASSERT(mpMotionMod->getMotionHistory()->width() == mpForegroundSegmentMod->getFilteredForeground()->width());
    PVASSERT(mpMotionMod->getMotionHistory()->height() == mpForegroundSegmentMod->getFilteredForeground()->height());    
  }
  
  //scan entire image for new tracks    
  for (y = 0; y < img.height(); y++)
  {
    for (x = 0; x < img.width(); x++)
    {
      const Uint8 creationForegroundThreshold = 64; //WAS 64; // WAS 160;
      if (img(x,y) > creationForegroundThreshold && (mTracks.size()+newTrackLocations.size()) < mMaxConcurrentTracks
        && mStartTrackMask(x,y) == 255)
      {

        if (mIsMotionEnabled)
        {
          if ((*mpMotionMod->getMotionHistory())(x,y) > mMaxNoMotionTime)
          {
            //PVMSG("TRACK NOT CREATED DUE TO NO MOTION\n");
            continue;
          }
        }

        Vector2f newTrackLocation(x,y);
        //cleanForegroundSegmentation(img,newTrackLocation);
        newTrackLocations.push_back(newTrackLocation);

        //hack, refilter input image after each new location is chosen
        cleanForegroundSegmentation(rawForeground, newTrackLocation);

        mpForegroundSegmentMod->filterByBlobSize(rawForeground, img);
        
        //
      }
    }
  }  

  // Find the closest track for each new track, and don't create it
  // if the track is too close. Instead, increment the cardinality counter
  // for the closest track.
  std::list<Vector2f>::iterator iLocation;
  for (iLocation = newTrackLocations.begin(); iLocation != newTrackLocations.end(); )
  {
    float minDist = img.width();
    Tracks::iterator iClosestTrack = mTracks.end();
    for (iTrack = mTracks.begin(); iTrack != mTracks.end(); iTrack++)
    {
      const Vector2f& p1 = (*iTrack)->getPos();
      const Vector2f& p2 = (*iLocation);
      // Normalize by blob size.
      const float x = (p1.x-p2.x)/(*iTrack)->getSize().x;
      const float y = (p1.y-p2.y)/(*iTrack)->getSize().y;
      float d = sqrt(x*x+y*y);
      if (d < minDist)
      {
        iClosestTrack = iTrack;
        minDist = d;
      }
    }    
    if (iClosestTrack != mTracks.end() && minDist < mMinTrackCreationDistance)
    {
      // Increment cardinality of snake head.
      //int idx = (*iClosestTrack)->getCurrentSnake().size()-1;
      //(*iClosestTrack)->setSnakeNodeCardinality(idx,(*iClosestTrack)->getSnakeNodeCardinality(idx)+1);
      iLocation = newTrackLocations.erase(iLocation);
    }
    else
    {
      iLocation++;
    }
  }

  for (iLocation = newTrackLocations.begin(); iLocation != newTrackLocations.end(); iLocation++)
  {
    MultiFrameTrackPtr pNewTrack(new MultiFrameTrack());
    pNewTrack->init(
      mNewTrackId++,
      mSettings.getPath(),
      Vector2i(iLocation->x,iLocation->y),
      mpForegroundSegmentMod->getBlobSizeEstimate(),
      getCurrentTime(),
      getFramesPerSecond(),
      mUseAlternateTracker);      

    if (mVerbose)
    {
      PVMSG("### Created Track %d at (%d,%d) [frame: %d]\n",pNewTrack->getId(),iLocation->x,iLocation->y,getCurrentFrameNumber());
    }
    mTracks.push_back(pNewTrack);    
  }  
}

void
BlobTrackerMod::checkOverlappingTracks()
{  
  assert(mpForegroundSegmentMod->getBlobSizeEstimate().get());
  BlobSizeEstimate& blobSizeEstimate = *(mpForegroundSegmentMod->getBlobSizeEstimate());

  // This parameter can be changed it should not be too big.
  const int numOverlapFrames = 3;  
  
  PVASSERT(mMaxSnakeSize >= numOverlapFrames);
 
  Tracks::iterator iTrack;
  for (iTrack = mTracks.begin(); iTrack != mTracks.end(); iTrack++)
  {
    if ((*iTrack)->getAge() < numOverlapFrames || !(*iTrack)->isAlive()) continue;
    Tracks::iterator iTrack2 = iTrack;
    for (iTrack2++; iTrack2 != mTracks.end(); iTrack2++)
    {
      if ((*iTrack2)->getAge() < numOverlapFrames || !(*iTrack2)->isAlive()) continue;
      const MultiFrameTrack::Snake& snake1 = (*iTrack)->getCurrentSnake();
      const MultiFrameTrack::Snake& snake2 = (*iTrack2)->getCurrentSnake();
      float totalDistance = 0;
      for (int frame = 0; frame < numOverlapFrames; frame++)
      {
        const Vector2f p1 = snake1[snake1.size()-frame-1];
        const Vector2f p2 = snake2[snake2.size()-frame-1];
        float dist = sqrt(dist2(p1,p2));
        // Get the blob size at the average point.
        float x = (p1.x+p2.x)/2;
        float y = (p1.y+p2.y)/2;
        float sizeX, sizeY;
        blobSizeEstimate.getEstimatedSize(x,y,sizeX,sizeY);
        // Normalize distance by blob size
        totalDistance += dist/sizeX;
      }
      // The normalized distance has the blob width as the
      // unit.
      const float overlappingDistanceThreshold = 0.5f; // ORIGINALLY 1.0
      totalDistance /= numOverlapFrames;
      if (totalDistance < overlappingDistanceThreshold)
      {
        if (mVerbose)
        {
          PVMSG("### Tracks Overlap (%d,%d) [frame: %d]\n",(*iTrack)->getId(),(*iTrack2)->getId(),getCurrentFrameNumber());
        }
        
        // Kill the youngest track.
        if ((*iTrack2)->getAge() < (*iTrack)->getAge())
        {
          (*iTrack2)->kill();
        }
        else
        {
          (*iTrack)->kill();
        }
      }
    }
  }
}

void 
BlobTrackerMod::process()
{
//  mBenchmark.beginSample();

  assert(mpForegroundSegmentMod->getBlobSizeEstimate().get());

  if (mRepelTracks)
  {
    calculateTrackForce();
  }

  killTracksInPoly();
  
  // Update all tracks.
  Tracks::iterator iTrack;
  for (iTrack = mTracks.begin(); iTrack != mTracks.end(); iTrack++)
  {
    if (mIsMotionEnabled)
    {
       (*iTrack)->useMotionHistory(mpMotionMod->getMotionHistory(), mMaxNoMotionTime);
    }

    (*iTrack)->update(mpForegroundSegmentMod->getFilteredForeground(), getCurrentTime());    
  }
  
  checkOverlappingTracks();

  scanForNewTracks();
  
  // Delete tracks that are not alive
  for (iTrack = mTracks.begin(); iTrack != mTracks.end();)
  {
    if (!(*iTrack)->isAlive())
    {
      if (mVerbose)
      {
        PVMSG("### Removing Track %d [frame: %d]\n",(*iTrack)->getId(),getCurrentFrameNumber());
      }
      TrajectoryPtr pTrajectory = (*iTrack)->getTrajectory();
      const int minTrajectoryLength = 5;
      //const int maxPreTrajectories = mMaxPreTrajectories;
      if (pTrajectory->getNodes().size() >= minTrajectoryLength) /*&& 
        mPreTrajectories.size() < maxPreTrajectories)*/
      {
        // Rescale the points in the trajectory so they match
        // the original image.
        const Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();
        
        const Rectanglei roi = mpForegroundSegmentMod->getRoiRectangle();

        const float scaleX = 1.0f / scaleFactor.x;
        const float scaleY = 1.0f / scaleFactor.y;

        Trajectory::Nodes::iterator iNode;
        Trajectory::Nodes& nodes = pTrajectory->getNodes();
        for (iNode = nodes.begin(); iNode != nodes.end(); ++iNode)
        {
          Trajectory::Node& node = **iNode;
          node.boundingBox.set(
            roi.x0 + node.boundingBox.x0*scaleX,
            roi.y0 + node.boundingBox.y0*scaleY,
            roi.x0 + node.boundingBox.x1*scaleX,
            roi.y0 + node.boundingBox.y1*scaleY);
          node.location.set(
            (node.boundingBox.x0+node.boundingBox.x1)/2,
            mIsTopViewCamera ? (node.boundingBox.y0+node.boundingBox.y1)/2 : node.boundingBox.y1);
        }
        mPreTrajectories.push_back(pTrajectory);
        
        if (mSendPreprocessTrajectories)
        {
//          sendVmsEvent(pTrajectory, mEventChannels.front());
        }        
      }
      iTrack = mTracks.erase(iTrack);
    }
    else
    {
      iTrack++;
    }
  }

  postProcessTracks(false);

//  mBenchmark.endSample();

  if (mVisualizeSnakeTail)
  {
    // Very expensive!!! For visualization only...
    mLastColorFrames.push_back(Image32Ptr(new Image32(getCurrentFrame())));
  }
}

void
BlobTrackerMod::postProcessTracks(bool force)
{
  assert(mpForegroundSegmentMod->getBlobSizeEstimate().get());
  double currentTime = getCurrentTime();
/*
  PVMSG("CURRENT TIME: %f\n", currentTime);
  PVMSG("FORCE TIME:   %f\n", mNextForcePostProcessTime);
  PVMSG("POSTPROC TIME:%f\n", mNextPostProcessTime);
  */
  //PVMSG("INTERVAL: %f %f\n", mPostProcessLowerInterval, mPostProcessUpperInterval);

  if (force == false)
    if (currentTime >= mNextForcePostProcessTime)
    {
      force = true;          
    }

  if (!force && currentTime < mNextPostProcessTime)
  {
    // Don't process, we have not reached the minimum time yet    
    return;
  }


  if (force == false && currentTime >= mNextPostProcessTime)
  {
    //perform post processing if there are no tracks in the scene
    if (mTracks.size() == 0)
    {
      force = true;            
    }
  }

  if (!force)
    return;  
  
  mNextForcePostProcessTime = currentTime + mPostProcessUpperInterval;
  mNextPostProcessTime = currentTime + mPostProcessLowerInterval;

  std::list<TrajectoryPtr> postTrajectories;  

  postTrajectories = mPreTrajectories;

  //TODO: handle this part better, maybe reconstruct combined trajectories later

  //if any tracks are still alive, clip and process the first half of their trajectories  
  std::list<MultiFrameTrackPtr>::iterator iTrack;
  for (iTrack = mTracks.begin(); iTrack != mTracks.end(); iTrack++)
  {
    TrajectoryPtr pFirstPart = (*iTrack)->getTrajectory();
    TrajectoryPtr pLastPart = pFirstPart->breakUp(getCurrentTime(), mNewTrackId++);

    // Rescale the points in the trajectory so they match
    // the original image.    
    const Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();        
    const Rectanglei roi = mpForegroundSegmentMod->getRoiRectangle();
    const float scaleX = 1.0f / scaleFactor.x;
    const float scaleY = 1.0f / scaleFactor.y;

    Trajectory::Nodes::iterator iNode;
    Trajectory::Nodes& nodes = pFirstPart->getNodes();
    for (iNode = nodes.begin(); iNode != nodes.end(); ++iNode)
    {
      Trajectory::Node& node = **iNode;
      node.boundingBox.set(
        roi.x0 + node.boundingBox.x0*scaleX,
        roi.y0 + node.boundingBox.y0*scaleY,
        roi.x0 + node.boundingBox.x1*scaleX,
        roi.y0 + node.boundingBox.y1*scaleY);
      node.location.set(
        (node.boundingBox.x0+node.boundingBox.x1)/2,
        mIsTopViewCamera ? (node.boundingBox.y0+node.boundingBox.y1)/2 : node.boundingBox.y1);
     }     

    (*iTrack)->setTrajectory(pLastPart);      
    postTrajectories.push_back(pFirstPart);   
  }
  
  mPreTrajectories.clear();
    
  if (mSavePreTrajectories && postTrajectories.size() != 0)
  {
    //TEMP
    //TODO: REMOVE
    //mMultiPersonHandler.processTrajectories(postTrajectories);

    //
    DateTime dt(getCurrentTime());    

    std::string filename = "Traj_VidSrcId" + aitSprintf("%d", mVideoSourceId);
    filename += "_" + dt.format("%Y%m%d_%H%M%S.csv");
    std::string loc = ait::combine_path(mTrajectoryOutputFolder, filename);
    
    //Compress trajectories to save space
    std::list<TrajectoryPtr> compactedTraj;
    std::list<TrajectoryPtr>::iterator iTraj;

    double compressDist = 
        mpForegroundSegmentMod->getBlobSizeEstimate()->getCenterWidth()/3.0;

    for (iTraj = postTrajectories.begin(); iTraj != postTrajectories.end(); iTraj++)
    {      

      TrajectoryPtr pTraj = (*iTraj)->compress(compressDist,5.0);      

      if (pTraj->nodeCount() > 1)
        compactedTraj.push_back(pTraj);
    }   
   
    if (compactedTraj.size() > 0)
      Trajectory::saveXmlCsv(loc, compactedTraj);
      //Trajectory::saveCSV(loc, compactedTraj);
  } 
    
  //perform postprocessing
  //mMultiPersonHandler.processTrajectories(postTrajectories);

  if (mSendTrajectoriesAsEventAttribute)
  {
    //Compress trajectories to save space
    std::list<TrajectoryPtr> compactedTraj;
    std::list<TrajectoryPtr>::iterator iTraj;

    double compressDist = 
        mpForegroundSegmentMod->getBlobSizeEstimate()->getCenterWidth()/3.0;

    for (iTraj = postTrajectories.begin(); iTraj != postTrajectories.end(); iTraj++)
    {      
      TrajectoryPtr pTraj = (*iTraj)->compress(compressDist,5.0);

      if (pTraj->nodeCount() > 1)
        compactedTraj.push_back(pTraj);
    }

    for (iTraj = compactedTraj.begin(); iTraj != compactedTraj.end(); ++iTraj)
    {
//      sendVmsEvent(*iTraj,*mEventChannels.begin());
//      BlobTrackerMsgPtr pMsg(new BlobTrackerMsg(getVisualSensorId(),(*iTraj)->getStartTime()));
//      pMsg->setFlag(BlobTrackerMsg::POST_PROCESSED_TRAJECTORY,true);
//      pMsg->setTrajectory(*iTraj);
//      sendMessage(pMsg);
    }
  }
  
//  generateEvents(postTrajectories);

  //send notification that all prior events can be processed
//  std::list<int>::const_iterator iEvtChan;
//  for (iEvtChan = mEventChannels.begin(); iEvtChan != mEventChannels.end(); iEvtChan++)
//    mpVmsPacket->notifyProcessCompletion(currentTime, *iEvtChan);
}

//void
//BlobTrackerMod::generateEvents(const std::list<TrajectoryPtr>& trajectories)
//{
//  assert(mpForegroundSegmentMod->getBlobSizeEstimate().get());
//
//  if (mEventsPolygon.empty()) return;
//
//  std::list<int>::const_iterator iEvtChan;
//
//  for (iEvtChan = mEventChannels.begin(); iEvtChan != mEventChannels.end(); iEvtChan++)
//  {
//    std::list<TrajectoryPtr>::const_iterator iTrajectory;
//    for (iTrajectory = trajectories.begin(); iTrajectory != trajectories.end(); ++iTrajectory)
//    {
//      std::list<PolygonEventPtr> events;
//      mTrajEvents.extractEvents(**iTrajectory, mEventsPolygon[*iEvtChan], events);
//      std::list<PolygonEventPtr>::iterator iEvent;
//      for (iEvent = events.begin(); iEvent != events.end(); ++iEvent)
//      {
//        sendVmsEvent(*iEvent, *iEvtChan);
//      }
//    }
//  }
//}

//void
//BlobTrackerMod::sendVmsEvent(TrajectoryPtr pTrajectory, int eventChannel)
//{
//  assert(mpForegroundSegmentMod->getBlobSizeEstimate().get());
//
//  double duration = pTrajectory->getEndTime()- pTrajectory->getStartTime();
//  //Todo: minimum duration of 1.0 for now;
//  duration = std::max(duration, 1.0);
//
//  double compressDist =
//        mpForegroundSegmentMod->getBlobSizeEstimate()->getCenterWidth()/3.0;
//  TrajectoryPtr pCompressed = pTrajectory->compress(compressDist,5.0);
//
//  mpVmsPacket->beginEventTime(pTrajectory->getStartTime(),duration, eventChannel);
//
//  if (mSendTrajectoriesAsEventAttribute)
//  {
//    mpVmsPacket->addLongStringAttribute("4",pTrajectory->toXmlString());
//  }
//  else
//  {
//    mpVmsPacket->addAttribute("10",boost::lexical_cast<std::string>(pTrajectory->getId()));
//    mpVmsPacket->addString("<extended>" + pTrajectory->getXmlBlobRectangles() + "</extended>");
//  }
//  mpVmsPacket->endEvent();
//}

//void
//BlobTrackerMod::sendVmsEvent(PolygonEventPtr pEvent, int eventChannel)
//{
//  assert(mpForegroundSegmentMod->getBlobSizeEstimate().get());
//  //TODO: HANDLE DURATION = 0 DUE TO LOSS OF PRECISION, ETC
//  //PVMSG("START FRAME: %f, END FRAME: %f\n", pEvent->beginFrame, pEvent->endFrame);
//
//  double duration = pEvent->endFrame - pEvent->beginFrame;
//  //Todo: minimum duration of 1.0 for now;
//  duration = std::max(duration, 1.0);
//
//  assert(duration > 0);
//
//  if (!mSendPreprocessTrajectories)
//  {
//
//    mpVmsPacket->beginEventTime(pEvent->beginFrame,duration, eventChannel);
//
//    if (pEvent->enterSide >= 0)
//    {
//      mpVmsPacket->addAttribute("1",boost::lexical_cast<std::string>(pEvent->enterSide));
//    }
//    if (pEvent->exitSide >= 0)
//    {
//      mpVmsPacket->addAttribute("2",boost::lexical_cast<std::string>(pEvent->exitSide));
//    }
//
//    mpVmsPacket->endEvent();
//  }
//
//  if (mSaveCsvFileEvents)
//  {
//    // This only works for real time events:
//    double start = pEvent->beginFrame;
//    double end = pEvent->endFrame;
//
//    DateTime startDt(start);
//
//    std::string date = startDt.format("%Y%m%d");
//    std::string fname = aitSprintf("blob_events_ech%i_%s.csv",eventChannel,date.c_str());
//    fname = ait::combine_path(mTrajectoryOutputFolder,fname);
//
//    bool fileExists = PvUtil::fileExists(fname.c_str());
//    FILE *fp = fopen(fname.c_str(),"a");
//    if (fp)
//    {
//      if (!fileExists)
//      {
//        fprintf(fp,"Start,Duration,Enter Side,Exit Side\n");
//      }
//
//      fprintf(fp,"%s,%.03f,%d,%d\n",
//        startDt.format(DateTime::vmsFormatStr).c_str(),
//        duration,
//        pEvent->enterSide,
//        pEvent->exitSide);
//
//      fclose(fp);
//    }
//
//  }
//
//}

void 
BlobTrackerMod::killTracksInPoly()
{  
  assert(mpForegroundSegmentMod->getBlobSizeEstimate().get());

  const Image8& img = mKillTrackMask;

  Tracks::iterator iTrack;  

  for (iTrack = mTracks.begin(); iTrack != mTracks.end(); )
  {
    //Don't remove tracks with less than 2 nodes in their trajectories
    if ((*iTrack)->getTrajectory()->nodeCount() < 2)
    {
      iTrack++;
      continue;
    }

    Vector2i pos = (*iTrack)->getImagePos(img.width(),img.height());
    if (img(pos.x, pos.y) == 255)
    {
      (*iTrack)->kill();
      
      //convert trajectory to image coordinates
      TrajectoryPtr pTraj = (*iTrack)->getTrajectory();
      convertToImageCoord(pTraj);
      mPreTrajectories.push_back(pTraj);    
      
      //erase the track, it is no longer needed
      iTrack = mTracks.erase(iTrack);
    }
    else
    {
      iTrack++;
    }
  }
}

void
BlobTrackerMod::convertToImageCoord(TrajectoryPtr pTraj)
{
  assert(mpForegroundSegmentMod->getBlobSizeEstimate().get());

  // Rescale the points in the trajectory so they match
  // the original image.
  const Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();
        
  const Rectanglei roi = mpForegroundSegmentMod->getRoiRectangle();

  const float scaleX = 1.0f / scaleFactor.x;
  const float scaleY = 1.0f / scaleFactor.y;

  Trajectory::Nodes::iterator iNode;
  Trajectory::Nodes& nodes = pTraj->getNodes();
  for (iNode = nodes.begin(); iNode != nodes.end(); ++iNode)
  {
    Trajectory::Node& node = **iNode;
    node.boundingBox.set(
      roi.x0 + node.boundingBox.x0*scaleX,
      roi.y0 + node.boundingBox.y0*scaleY,
      roi.x0 + node.boundingBox.x1*scaleX,
      roi.y0 + node.boundingBox.y1*scaleY);
    node.location.set(
      (node.boundingBox.x0+node.boundingBox.x1)/2,
      mIsTopViewCamera ? (node.boundingBox.y0+node.boundingBox.y1)/2 : node.boundingBox.y1);
  }     
       
}

//void
//BlobTrackerMod::processMessages()
//{
//  VisionMsgPtr pMsg;
//
//  while (mpInputMsgQueue->get(pMsg))
//  {
//    BlobTrackerMsg* pMsg0 = dynamic_cast<BlobTrackerMsg*>(pMsg.get());
//    if (pMsg0 != NULL)
//    {
//      processMsg(pMsg0);
//      continue;
//    }
//    PvUtil::exitError("Message type not supported by this module [%s]",getFullName().c_str());
//  }
//}
//
//void
//BlobTrackerMod::processMsg(BlobTrackerMsg *pMsg)
//{
//}

static void drawText(Image32& img0,
                     int x,
                     int y,
                     const std::string& txt,
                     bool transparent = false)
{
#ifdef WIN32
  HFONT hfnt, hOldFont;
  HDC hdc = CreateCompatibleDC(NULL);

  hfnt = (HFONT)GetStockObject(ANSI_FIXED_FONT); 
  if (hOldFont = (HFONT)SelectObject(hdc, hfnt)) 
  {
    SIZE size;
    GetTextExtentPoint(hdc,txt.c_str(),txt.length(),&size);
    SelectObject(hdc, hOldFont); 
    DeleteDC(hdc);
    
    Uint32 magicColor = PV_RGB(1,1,1);
    PvCanvasImage img(size.cx+2,size.cy+2);
    img.setAll(magicColor);
    hdc = img.dc();
    if (hOldFont = (HFONT)SelectObject(hdc, GetStockObject(ANSI_FIXED_FONT))) 
    {
      POINT pos = { 1 , 1 };
      SetBkMode(hdc,TRANSPARENT);
      if (transparent)
      {
        SetTextColor(hdc,RGB(0,0,0));
        for (int x0 = -1; x0 <= 1; x0++)
        {
          for (int y0 = -1; y0 <= 1; y0++)
          {
            TextOut(hdc, pos.x+x0, pos.y+y0, txt.c_str(), txt.length()); 
          }
        }
      }
      SetTextColor(hdc,RGB(255,255,255));
      TextOut(hdc, pos.x, pos.y, txt.c_str(), txt.length()); 
      SelectObject(hdc, hOldFont); 

      // If transparent, must paste with key color...
      img0.paste(img,x-img.width()/2,y-img.height()/2);
    }
  } 

  GdiFlush();
#endif  
}

//Handle repulsive track forces to deter tracks from jumping blobs
Vector2f 
BlobTrackerMod::pointRepulsion(const Vector2f& p1, const Vector2f& p2)
{
  assert(mpForegroundSegmentMod->getBlobSizeEstimate().get());

  float w = 0.0;
  float h = 0.0;
  mpForegroundSegmentMod->getBlobSizeEstimate()->getEstimatedSize(p1.x,p1.y,w,h);

  //float d = sqrt(dist2(p1,p2));

  Vector2f vec;

  vec.x = p1.x - p2.x;
  vec.y = p1.y - p2.y;

  //divide by blob width,height
  vec.x /= w;
  vec.y /= h;

  //Special case, return 0.0 and let caller deal with it
  if (vec.x == 0.0 && vec.y == 0.0)
  {
    return vec;
  }

  float d = sqrt(vec.x*vec.x + vec.y*vec.y);

  double maxDist = 2.0;

  //return no force if points are > then specified distance apart
  if (d >= maxDist)
  {
    return Vector2f(0.0,0.0);
  }

  PVASSERT(d != 0.0);

  vec.normalize();

  //scale force vector based on distance
  //vec.mult(0.25/d);  //nonlinear

  //linear
  float scale = 1.0 - d/maxDist;
  scale = std::min(scale,1.0f);
  scale = std::max(scale,0.0f);

  vec.mult(scale);
  
  return vec;
}

Vector2f 
BlobTrackerMod::calculateTrackRepulsion(MultiFrameTrackPtr pTrack)
{
  assert(mpForegroundSegmentMod->getBlobSizeEstimate().get());

  Vector2f vec(0.0,0.0);

  Vector2f pos = pTrack->getPos();

  
  Tracks::iterator iTrack;

  for (iTrack = mTracks.begin(); iTrack != mTracks.end(); iTrack++)
  {
    if (*iTrack != pTrack)
    {
      Vector2f f = pointRepulsion(pos, (*iTrack)->getPos());     
      
      vec.x += f.x;
      vec.y += f.y;
    }
  }
  
  return vec;
}

void 
BlobTrackerMod::calculateTrackForce()
{
  assert(mpForegroundSegmentMod->getBlobSizeEstimate().get());

  Tracks::iterator iTrack;

  for (iTrack = mTracks.begin(); iTrack != mTracks.end(); iTrack++)
  {
    Vector2f vec = calculateTrackRepulsion(*iTrack);

    //Vector2f vec;    
    
    //vec.x += -0.0;
    //vec.y += 0.0;
    
    /*
    float val = 1.0f;

    vec.x = std::min(vec.x, val);
    vec.x = std::max(vec.x, -val);
    
    vec.y = std::min(vec.y, val);
    vec.y = std::max(vec.y, -val);   
    */      

    //(*iTrack)->setForceVector(Vector2f(0.0,0.0));    
    
    (*iTrack)->setForceVector(vec);



    //TODO: HACK, MOVE TO OWN FUNCTION
    Vector2f dir = (*iTrack)->getTrajectory()->forwardVector(3.0);

    //dir.div(3.0);

    if (dir.x != 0.0 || dir.y != 0.0)
    {
      //dir.normalize();

      dir.div(50.0);      
    }

    //NOTE: ENABLE THIS
    //(*iTrack)->setDirectionVector(dir);    

  }
}

void
BlobTrackerMod::visualize(Image32& img)
{
  assert(mpForegroundSegmentMod->getBlobSizeEstimate().get());

  if (!mPaintVisualization)
  {
    return;
  }


  const Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();
  const Rectanglei roi = mpForegroundSegmentMod->getRoiRectangle();

  if (mVisualizeSnakeTail)
  {
    img.paste(*(mLastColorFrames[0]),0,0);
  }

  PVASSERT(scaleFactor.x != 0.0);
  PVASSERT(scaleFactor.y != 0.0);

  const float scale_x = 1.0f / scaleFactor.x;
  const float scale_y = 1.0f / scaleFactor.y;

#define TRANSFORM_X(x) roi.x0+(int)((x)*scale_x)
#define TRANSFORM_Y(y) roi.y0+(int)((y)*scale_y)

  if (mpForegroundSegmentMod->getFilteredForeground().get())
  {
    const Image8& gimg = *(mpForegroundSegmentMod->getFilteredForeground());

    Image32 foreground(gimg.width(),gimg.height());
    ColorConvert::convert(gimg,PvImageProc::GRAY_SPACE,
      foreground,PvImageProc::RGB_SPACE);

    std::list<TrajectoryPtr>::iterator iT;
    for (iT = mPreTrajectories.begin(); iT != mPreTrajectories.end(); ++iT)
    {
      float width1 = mpForegroundSegmentMod->getBlobSizeEstimate()->getBlobWidth((*iT)->getStartPos());
      float width2 = mpForegroundSegmentMod->getBlobSizeEstimate()->getBlobWidth((*iT)->getEndPos());

      float avgWidth = (width1+width2)/2.0;

      //TODO: TEMP CODE TO CLEAN UP VISUALIZATION
      /*
      if ((*iT)->getAge() < 2.0)
        continue;

      if ((*iT)->length() < avgWidth * .25)
        continue;
        */


      Trajectory::Nodes& nodes = (*iT)->getNodes();
      Trajectory::Nodes::iterator iNode, iPrevNode = nodes.end();
      for (iNode = nodes.begin(); iNode != nodes.end(); iNode++)
      {
        if (iPrevNode != nodes.end())
        {

          img.line(
            round((*iPrevNode)->location.x),
            round((*iPrevNode)->location.y),
            round((*iNode)->location.x),
            round((*iNode)->location.y),
            PV_RGB(100,100,0));
        }
        iPrevNode = iNode;
      }

    }

    Tracks::iterator iTrack;
    for (iTrack = mTracks.begin(); iTrack != mTracks.end(); iTrack++)
    {
      (*iTrack)->visualize(foreground);

      // Don't visualize spurious tracks...
      if ((*iTrack)->getAge() > mMaxSnakeSize/*TODO: RESTORE mMaxSnakeSize/2*/)
      {
        std::vector<Vector2f>& snake = (*iTrack)->getCurrentSnake();

        if (!mVisualizeSnakeTail)
        {

          for (int i = 1; i < snake.size(); i++)
          {
            img.line(
              TRANSFORM_X(snake[i-1].x),TRANSFORM_Y(snake[i-1].y),
              TRANSFORM_X(snake[i].x),TRANSFORM_Y(snake[i].y),
              PV_RGB(255,255,255));
          }

        }
        if (snake.size() > 1)
        {
          int idx = mVisualizeSnakeTail ? 0 : snake.size()-1;
          float w,h;
          mpForegroundSegmentMod->getBlobSizeEstimate()->getEstimatedSize(snake[idx].x,snake[idx].y,w,h);
          img.ellipse(TRANSFORM_X(snake[idx].x),TRANSFORM_Y(snake[idx].y),w/2*scale_x,h/2*scale_y,PV_RGB(0,0,255));

          if (!mVisualizeSnakeTail)
          {
            img.ellipse(TRANSFORM_X(snake[idx].x),TRANSFORM_Y(snake[idx].y),w*mMinTrackCreationDistance*scale_x,h*mMinTrackCreationDistance*scale_y,PV_RGB(0,255,255));
          }
          Trajectory::Nodes& nodes = (*iTrack)->getTrajectory()->getNodes();
          if (!nodes.empty())
          {
            /* Draw cardinality text
            drawText(img,TRANSFORM_X(snake[idx].x),TRANSFORM_Y(snake[idx].y)-h/2*scale_y,
              aitSprintf("%.01f",nodes.back()->cardinality));
              */
            /* Draw id text
            drawText(img,TRANSFORM_X(snake[idx].x),TRANSFORM_Y(snake[idx].y)-h/2*scale_y,
              aitSprintf("%d", (*iTrack)->getId()));
              */
          }
        }
        if (!mVisualizeSnakeTail)
        {

          Trajectory::Nodes& nodes = (*iTrack)->getTrajectory()->getNodes();
          Trajectory::Nodes::iterator iNode, iPrevNode = nodes.end();
          for (iNode = nodes.begin(); iNode != nodes.end(); iNode++)
          {
            if (iPrevNode != nodes.end())
            {

              img.line(
                TRANSFORM_X((int)(*iPrevNode)->location.x),
                TRANSFORM_Y((int)(*iPrevNode)->location.y),
                TRANSFORM_X((int)(*iNode)->location.x),
                TRANSFORM_Y((int)(*iNode)->location.y),
                PV_RGB(255,0,0));

              img.line(
                TRANSFORM_X((int)(*iPrevNode)->location.x) + 1,
                TRANSFORM_Y((int)(*iPrevNode)->location.y),
                TRANSFORM_X((int)(*iNode)->location.x) + 1,
                TRANSFORM_Y((int)(*iNode)->location.y),
                PV_RGB(255,0,0));

              img.line(
                TRANSFORM_X((int)(*iPrevNode)->location.x) - 1,
                TRANSFORM_Y((int)(*iPrevNode)->location.y),
                TRANSFORM_X((int)(*iNode)->location.x) - 1,
                TRANSFORM_Y((int)(*iNode)->location.y),
                PV_RGB(255,0,0));

              img.line(
                TRANSFORM_X((int)(*iPrevNode)->location.x),
                TRANSFORM_Y((int)(*iPrevNode)->location.y)+1,
                TRANSFORM_X((int)(*iNode)->location.x),
                TRANSFORM_Y((int)(*iNode)->location.y)+1 ,
                PV_RGB(255,0,0));

              img.line(
                TRANSFORM_X((int)(*iPrevNode)->location.x),
                TRANSFORM_Y((int)(*iPrevNode)->location.y)-1,
                TRANSFORM_X((int)(*iNode)->location.x),
                TRANSFORM_Y((int)(*iNode)->location.y)-1,
                PV_RGB(255,0,0));

            }
            iPrevNode = iNode;
          }
        }
      }
    }
    foreground.rescale(gimg.width()*scale_x,gimg.height()*scale_y);
    img.paste(foreground,roi.x0,img.height()/2+roi.y0);

    {
      Image32 img1;
      ColorConvert::convert(*mpNewTrackScanImage,PvImageProc::GRAY_SPACE,
        img1,PvImageProc::RGB_SPACE);
      img1.rescale(img1.width()*scale_x,img1.height()*scale_y);
      img.paste(img1,mFrameWidth+roi.x0,mFrameHeight+roi.y0);
    }

    if (!mEventsPolygon.empty())
    {
      std::list<int>::const_iterator iEvtChan;
      for (iEvtChan = mEventChannels.begin(); iEvtChan != mEventChannels.end(); iEvtChan++)
      {
        int i;
        for (i = 1; i < mEventsPolygon[*iEvtChan].size(); i++)
        {
          img.line(
            mEventsPolygon[*iEvtChan][i-1].x,mEventsPolygon[*iEvtChan][i-1].y,
            mEventsPolygon[*iEvtChan][i].x,mEventsPolygon[*iEvtChan][i].y,
            PV_RGB(255,255,128));
        }
      }
    }
    img.rectangle(roi,PV_RGB(0,0,0));
  }

  if (mIsMotionEnabled)
  {
    Vector2f scaleFactor = mpMotionMod->getScaleFactor();
    Rectanglei roi = mpMotionMod->getRoiRect();

    Image32 lastMotionImage(mpMotionMod->getMotionHistory()->width(), mpMotionMod->getMotionHistory()->height());
    for (int y = 0; y < mpMotionMod->getMotionHistory()->height(); y++)
      for (int x = 0; x < mpMotionMod->getMotionHistory()->width(); x++)
      {
        float pct = std::min((float)(*mpMotionMod->getMotionHistory())(x,y)/(float)mMaxNoMotionTime, 1.0f);
        lastMotionImage(x,y) = PV_RGB((int)(pct*255),0,0);
      }
      lastMotionImage.rescale((int)(mpMotionMod->getMotionHistory()->width()/scaleFactor.x), (int)(mpMotionMod->getMotionHistory()->height()/scaleFactor.y));

    img.paste(lastMotionImage, mFrameWidth + roi.x0, roi.y0);
  }

  //Draw kill and create track polygons
  std::list<Polygon>::const_iterator iPoly;
  for (iPoly = mStartTrackPolyList.begin(); iPoly != mStartTrackPolyList.end(); iPoly++)
  {
    iPoly->draw(img, PV_RGB(255,255,0));
  }

  for (iPoly = mKillTrackPolyList.begin(); iPoly != mKillTrackPolyList.end(); iPoly++)
  {
    iPoly->draw(img, PV_RGB(255,0,0));
  }
}

//
// ACCESS
//

//
// INQUIRY
//

}; // namespace vision

}; // namespace ait


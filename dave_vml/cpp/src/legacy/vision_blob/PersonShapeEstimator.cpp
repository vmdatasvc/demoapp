/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/


#include "PersonShapeEstimator.hpp"


#include <legacy/types/AitBaseTypes.hpp>
#include <legacy/types/strutil.hpp>
#include <legacy/types/String.hpp>
#include <legacy/low_level/Settings.hpp>
#include <legacy/vision_tools/ColorConvert.hpp>
#include <legacy/types/Base64.hpp>
#include <legacy/low_level/XmlParser.hpp>
#include <stack>

#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/convenience.hpp>

#include <boost/lexical_cast.hpp>


namespace ait
{

namespace vision
{

  
PersonShapeEstimator::PersonShapeEstimator()
{
  mIsInitialized = false;
}

void
PersonShapeEstimator::getRenderedScene(Image32& img)
{
  glFlush();

  img.resize(mRenderImageWidth,mRenderImageHeight);

  /*
  const Uint8* val = (Uint8*)mpImageBuffer;

  for (int y = 0; y < mRenderImageHeight; y++)
  {
    for (int x = 0; x < mRenderImageWidth; x++)
    {
      int index = 4*(y*mRenderImageWidth + x);

      int b = val[index];
      int g = val[index + 1];
      int r = val[index + 2];
      img(x,y) = PV_RGB(r,g,b);
    }
  }
  */

  img.setInternalFormat(Image32::PV_BGRA); 

  assert(mpImageBuffer);
  
  const Uint32* val = (Uint32*)mpImageBuffer;

  for (int y = 0; y < mRenderImageHeight; y++)
  {
    for (int x = 0; x < mRenderImageWidth; x++)
    {
      int index = (y*mRenderImageWidth + x);

      //int b = val[index];
      //int g = val[index + 1];
      //int r = val[index + 2];


      img(x,y) = val[index];      
    }
  }
}

void
PersonShapeEstimator::getRenderedScene(Image8& img)
{
  glFlush();

  img.resize(mRenderImageWidth,mRenderImageHeight);  
    

  assert(mpImageBuffer);
  const Uint8* val = (Uint8*)mpImageBuffer;

  for (int y = 0; y < mRenderImageHeight; y++)
  {
    for (int x = 0; x < mRenderImageWidth; x++)
    {
      int index = (y*mRenderImageWidth + x);
      
      img(x,y) = val[index];
    }
  }
}

void
PersonShapeEstimator::drawPersonWorld(float x, float y, bool useColors = false)
{
  
  GLUquadricObj* pQuadric = gluNewQuadric();

  assert(pQuadric);
  
  glTranslatef(x,y,0.0);  

  float r,g,b;

  if (useColors)
  {
    r = rand()/(float)RAND_MAX;
    g = rand()/(float)RAND_MAX;
    b = rand()/(float)RAND_MAX;
  }
  else
  {
    r = 1.0;
    g = 1.0;
    b = 1.0;
  }  

  glColor3f(r,g,b);
  gluCylinder(pQuadric, mPersonRadius, mPersonRadius, mPersonHeight, 20, 20);

  if (useColors) //make the top of the person red to visualization purposes
  {  
    glColor3f(1.0,0,0);
  }

  //BUGFIX: Draw the bottom of the cylinder, sometimes the top cylinder
  // is clipped by the near clipping plane  
  gluDisk(pQuadric, 0.0, mPersonRadius, 20, 20);

  //Translate and draw top of cylinder
  glTranslatef(0,0,mPersonHeight);  
  gluDisk(pQuadric, 0.0, mPersonRadius, 20, 20);
  glTranslatef(-x,-y,-mPersonHeight);  
  gluDeleteQuadric(pQuadric);
}

unsigned int
PersonShapeEstimator::getUndistortedPixel(int x, int y, 
                                          const Image32& worldImg) const
{

  //PVMSG("x,y = %d,%d\n",x,y);
  
  float xNorm = (x - mImageWidth/2)/(mLargeAxis/2.0);
  float yNorm = (y - mImageHeight/2)/(mLargeAxis/2.0);

  //PVMSG("xNorm,yNorm = %f,%f\n",xNorm,yNorm);

  float distortedR = sqrt(xNorm*xNorm + yNorm*yNorm);
  float radians = atan2(yNorm,xNorm);

  //PVMSG("distortedR,radians = %f,%f\n",distortedR,radians);

  float undistortedR = mPolynomial.evaluate(distortedR);

  //PVMSG("undistortedR = %f\n",undistortedR);

  float newX = undistortedR*cos(radians)*(mLargeAxis/2) + worldImg.width()/2;
  float newY = undistortedR*sin(radians)*(mLargeAxis/2) + worldImg.height()/2;

  //PVMSG("newX,newY = %f,%f\n",newX,newY);

  //return blue if world image has no pixels
  if (int(newX) < 0 || int(newX) > mRenderImageWidth -2 ||
      int(newY) < 0 || int(newY) > mRenderImageHeight - 2)
  {
    return PV_RGB(0,0,255);    
  }
  else
  {

    float dx = newX - int(newX);
    float dy = newY - int(newY);

    assert(int(newX) >= 0 && int(newX) < worldImg.width());
    assert(int(newX)+1 >= 1 && int(newX) < worldImg.width());

    assert(int(newY) >= 0 && int(newY) < worldImg.height());
    assert(int(newY)+1 >= 1 && int(newY) < worldImg.height());

    unsigned int p00 = worldImg(int(newX),int(newY));
    unsigned int p01 = worldImg(int(newX)+1,int(newY));
    unsigned int p10 = worldImg(int(newX),int(newY)+1);
    unsigned int p11 = worldImg(int(newX)+1,int(newY)+1);

    int r = int((1.0-dx)*(1.0-dy)*RED(p00) + (dx)*(1.0-dy)*RED(p01) + 
      (1.0-dx)*(dy)*RED(p10) + (dx)*(dy)*RED(p11));
    int g = int((1.0-dx)*(1.0-dy)*GREEN(p00) + (dx)*(1.0-dy)*GREEN(p01) + 
      (1.0-dx)*(dy)*GREEN(p10) + (dx)*(dy)*GREEN(p11));
    int b = int((1.0-dx)*(1.0-dy)*BLUE(p00) + (dx)*(1.0-dy)*BLUE(p01) + 
      (1.0-dx)*(dy)*BLUE(p10) + (dx)*(dy)*BLUE(p11));    
    
    return PV_RGB(r,g,b);
  }
}

unsigned int
PersonShapeEstimator::getUndistortedPixel(int x, int y, 
                                          const Image8& worldImg) const
{
  
  float xNorm = (x - mImageWidth/2)/(mLargeAxis/2.0);
  float yNorm = (y - mImageHeight/2)/(mLargeAxis/2.0);

  float distortedR = sqrt(xNorm*xNorm + yNorm*yNorm);
  float radians = atan2(yNorm,xNorm);

  float undistortedR = mPolynomial.evaluate(distortedR);

  float newX = undistortedR*cos(radians)*(mLargeAxis/2) + worldImg.width()/2;
  float newY = undistortedR*sin(radians)*(mLargeAxis/2) + worldImg.height()/2; 

  //return blue if world image has no pixels
  if (int(newX) < 0 || int(newX) > mRenderImageWidth -2 ||
      int(newY) < 0 || int(newY) > mRenderImageHeight - 2)
  {
    return 0; // return black if there is no source material to draw from    
  }
  else
  {
    assert(int(newX) >= 0 && int(newX) < worldImg.width());
    assert(int(newY) >= 0 && int(newY) < worldImg.height());
    

    /*
    float dx = newX - int(newX);
    float dy = newY - int(newY);

    unsigned int p00 = worldImg(int(newX),int(newY));
    unsigned int p01 = worldImg(int(newX)+1,int(newY));
    unsigned int p10 = worldImg(int(newX),int(newY)+1);
    unsigned int p11 = worldImg(int(newX)+1,int(newY)+1);

    int r = int((1.0-dx)*(1.0-dy)*RED(p00) + (dx)*(1.0-dy)*RED(p01) + 
    (1.0-dx)*(dy)*RED(p10) + (dx)*(dy)*RED(p11));
    int g = int((1.0-dx)*(1.0-dy)*GREEN(p00) + (dx)*(1.0-dy)*GREEN(p01) + 
    (1.0-dx)*(dy)*GREEN(p10) + (dx)*(dy)*GREEN(p11));
    int b = int((1.0-dx)*(1.0-dy)*BLUE(p00) + (dx)*(1.0-dy)*BLUE(p01) + 
    (1.0-dx)*(dy)*BLUE(p10) + (dx)*(dy)*BLUE(p11));    
    
    return PV_RGB(r,g,b);
    */
    return worldImg(int(newX),int(newY));
    //return worldImg(round(newX),round(newY));
  }
}

void 
PersonShapeEstimator::applyCameraDistortion(Image32& img, 
                                            const Image32& worldImg) const
{
  img.resize(mImageWidth,mImageHeight);

  for (int y = 0; y < img.height(); y++)
  {
    for (int x = 0; x < img.width(); x++)
    {
      img(x,y) = getUndistortedPixel(x,y,worldImg);
    }
  } 
}

//fast version
void 
PersonShapeEstimator::applyCameraDistortion(Image8& img, 
                                            const Image8& worldImg) const
{
  img.resize(mImageWidth,mImageHeight);

  for (int y = 0; y < img.height(); y++)
  {
    for (int x = 0; x < img.width(); x++)
    {
      img(x,y) = getUndistortedPixel(x,y,worldImg);
    }
  }  
}


void 
PersonShapeEstimator::applyCameraDistortion(Image8& img, 
                                            const Image8& worldImg, 
                                            Rectanglei rect) const
{
  img.resize(rect.width(),rect.height());

  for (int y = 0; y < img.height(); y++)
  {
    for (int x = 0; x < img.width(); x++)
    {
      img(x,y) = getUndistortedPixel(x + rect.x0,y + rect.y0,worldImg);
    }
  }  
}

void 
PersonShapeEstimator::applyCameraDistortion(Image32& img, 
                                            const Image32& worldImg, 
                                            Rectanglei rect) const
{
  img.resize(rect.width(),rect.height());

  for (int y = 0; y < img.height(); y++)
  {
    for (int x = 0; x < img.width(); x++)
    {
      img(x,y) = getUndistortedPixel(x + rect.x0,y + rect.y0,worldImg);
    }
  }  
}



Vector2f 
PersonShapeEstimator::getWorldCoords(int x, int y) const
{

  //Undistort pixels first
  double xNorm = (x - mImageWidth/2) / (mLargeAxis/2.0);
  double yNorm = (y - mImageHeight/2) / (mLargeAxis/2.0);

  double radians = atan2(yNorm,xNorm);
  double rDistorted = sqrt(xNorm*xNorm + yNorm*yNorm);

  double rUndistorted = mPolynomial.evaluate(rDistorted);

  double newX = rUndistorted*cos(radians) * (mLargeAxis/2.0);
  double newY = rUndistorted*sin(radians) * (mLargeAxis/2.0);


  //Now that we have the undistorted pixel coordinates, we use gluUnproject
  // at the near and far clipping planes to create a ray from this screen
  // position that passes through our openGL scene. Our 'floor' plane is simply
  // the X-Y plane, so where the ray intersects this plane will give us our
  // world coordinates.

  //Extract the modelview/projection matrices for gluUnproject
  GLint viewport[4];
  GLdouble mvMatrix[16];
  GLdouble projMatrix[16]; 

  glGetIntegerv(GL_VIEWPORT,viewport);
  glGetDoublev(GL_MODELVIEW_MATRIX,mvMatrix);
  glGetDoublev(GL_PROJECTION_MATRIX,projMatrix); 

  GLdouble p1_x,p1_y,p1_z;
  GLdouble p2_x,p2_y,p2_z;

  //Calculate a ray that passes through our scene
  gluUnProject(viewport[2]/2 + newX,viewport[3]/2 + newY,0.0,mvMatrix,projMatrix,viewport,&p1_x,&p1_y,&p1_z);
  gluUnProject(viewport[2]/2 + newX,viewport[3]/2 + newY,1.0,mvMatrix,projMatrix,viewport,&p2_x,&p2_y,&p2_z);

  //Case when there is no intersection
  //TODO: raise an exception here
  assert(p2_z-p1_z != 0.0);

  double t = -p1_z/(p2_z-p1_z);

  double worldX = p1_x + (p2_x-p1_x)*t;
  double worldY = p1_y + (p2_y-p1_y)*t;

  
  //Convert undistorted pixels into world coordinates
  //double worldX = newX / mPixelsPerCoord;
  //double worldY = newY / mPixelsPerCoord;
  
  return Vector2f(worldX,worldY);
}

Vector2f 
PersonShapeEstimator::safeGetWorldCoords(int x, int y) const
{

  //Undistort pixels first
  double xNorm = (x - mImageWidth/2) / (mLargeAxis/2.0);
  double yNorm = (y - mImageHeight/2) / (mLargeAxis/2.0);

  double radians = atan2(yNorm,xNorm);
  double rDistorted = sqrt(xNorm*xNorm + yNorm*yNorm);

  double rUndistorted = mPolynomial.evaluate(rDistorted);

  double newX = rUndistorted*cos(radians) * (mLargeAxis/2.0);
  double newY = rUndistorted*sin(radians) * (mLargeAxis/2.0);


  //Now that we have the undistorted pixel coordinates, we use gluUnproject
  // at the near and far clipping planes to create a ray from this screen
  // position that passes through our openGL scene. Our 'floor' plane is simply
  // the X-Y plane, so where the ray intersects this plane will give us our
  // world coordinates.

  //Extract the modelview/projection matrices for gluUnproject
  GLint viewport[4];
  GLdouble mvMatrix[16];
  GLdouble projMatrix[16]; 

  glGetIntegerv(GL_VIEWPORT,viewport);
  glGetDoublev(GL_MODELVIEW_MATRIX,mvMatrix);
  glGetDoublev(GL_PROJECTION_MATRIX,projMatrix); 

  GLdouble p1_x,p1_y,p1_z;
  GLdouble p2_x,p2_y,p2_z;

  //Calculate a ray that passes through our scene
  gluUnProject(viewport[2]/2 + newX,viewport[3]/2 + newY,0.0,mvMatrix,projMatrix,viewport,&p1_x,&p1_y,&p1_z);
  gluUnProject(viewport[2]/2 + newX,viewport[3]/2 + newY,1.0,mvMatrix,projMatrix,viewport,&p2_x,&p2_y,&p2_z);

  //Case when there is no intersection
  //TODO: raise an exception here
  if(p2_z-p1_z != 0)
  {
    assert(p2_z-p1_z != 0.0);

  double t = -p1_z/(p2_z-p1_z);

  double worldX = p1_x + (p2_x-p1_x)*t;
  double worldY = p1_y + (p2_y-p1_y)*t;


  //Convert undistorted pixels into world coordinates
  //double worldX = newX / mPixelsPerCoord;
  //double worldY = newY / mPixelsPerCoord;

  return Vector2f(worldX,worldY);
  }
  else
    return Vector2f(123456.0,123456.0);
}

void
PersonShapeEstimator::initGl()
{
  glClearDepth(1.0);
  glDepthFunc(GL_LESS);
  glEnable(GL_DEPTH_TEST); 
  glClearColor(0.0,0.0,0.0,0.0);

  //Setup camera location/perspective settings

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  
  const double pi = 3.14159265;

  float scaleFactor = mRenderImageHeight/(double)mImageHeight;
  PVMSG("scaleFactor: %f\n", scaleFactor);

  //must divide by 2.0 because we are only using 1/2 of the ccd in 
  // this direction
  double thetaRadians = atan(mCameraCcdHeight/mCameraFocalLength/2.0); 

  double tanTheta = tan(thetaRadians);

  double phiRadians = 
    atan((scaleFactor-1.0)*tanTheta/(scaleFactor*tanTheta*tanTheta + 1.0));
  
  double initialFovy = 180.0/pi*thetaRadians * 2.0;
  double finalFovy = 180.0/pi*(thetaRadians + phiRadians) * 2.0;

  PVMSG("Initial Fovy: %f\n", initialFovy); 
  PVMSG("Final Fovy: %f\n", finalFovy);   
  
  gluPerspective(finalFovy,1.0 * 
    (float)mRenderImageWidth/(float)mRenderImageHeight,0.01,1000);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glRotatef(mCameraRotX,1.0,0.0,0.0);

  gluLookAt(0.0,0.0,mCameraHeight,0.0,0.0,0.0,0.0,1.0,0.0);

  
  //TODO: remove THIS SECTION?
  //Now calculate scaling between pixels and world coords
  /*
  
  GLint viewport[4];
  GLdouble mvMatrix[16];
  GLdouble projMatrix[16]; 

  glGetIntegerv(GL_VIEWPORT,viewport);
  glGetDoublev(GL_MODELVIEW_MATRIX,mvMatrix);
  glGetDoublev(GL_PROJECTION_MATRIX,projMatrix); 
  
  GLdouble wx,wy,wz;

  gluProject(1.0,0.0,0.0,mvMatrix,projMatrix,viewport,&wx,&wy,&wz);
  
  mPixelsPerCoord = wx - mRenderImageWidth/2.0;

  PVMSG("PixelsPerCoord: %f\n", mPixelsPerCoord);
  */
  
}

void 
PersonShapeEstimator::createRenderingSurface(bool useColor)
{  
  //destroy rendering surface if it already exists
  destroyRenderingSurface();

  //If we are using color images for visualization, use more bits
  int bits = (useColor) ? 32 : 8;  

  //get windows device context
  mHdc = CreateCompatibleDC(0);
  if (!mHdc)
  {
    PvUtil::exitError("PersonShapeEstimator: CreateCompatibleDC() FAILED!\n");
  }  

  //Initialize bitmap parameters
  BITMAPINFO bmi = {
    {sizeof(BITMAPINFOHEADER),
      mRenderImageWidth,mRenderImageHeight,1,bits,BI_RGB,0,0,0,0,0},{0}};

  mHbm = CreateDIBSection(mHdc, 
                          &bmi, 
                          DIB_RGB_COLORS,
                          (void **)&mpImageBuffer,
                          0, 
                          0);

  if (!mHbm)
  {
    PvUtil::exitError("PersonShapeEstimator: CreateDIBSection FAILED!\n");
  }

  mSelectResult = SelectObject(mHdc, mHbm);
  if (!mSelectResult)
    PVMSG("PersonShapeEstimator: SelectObject FAILED!\n");  

  //Choose pixel format for OpenGL
  static	PIXELFORMATDESCRIPTOR pfd=
	{
		sizeof(PIXELFORMATDESCRIPTOR),
		1,          								// Version Number
		PFD_DRAW_TO_BITMAP |				// Drawing to a bitmap, not a window
		PFD_SUPPORT_OPENGL,					// Support OpenGL	
		PFD_TYPE_RGBA,							// RGBA Pixel Format    
		bits,			        					// Bitdepth
		0, 0, 0, 0, 0, 0,						// Color Bits Ignored
		0,							          	// No Alpha Buffer
		0,							          	// Shift Bit Ignored
		0,								          // No Accumulation Buffer
		0, 0, 0, 0,					    		// Accumulation Bits Ignored
		32,							          	// 16Bit Z-Buffer (Depth Buffer)
		0,							          	// No Stencil Buffer
		0,							           	// No Auxiliary Buffer
		PFD_MAIN_PLANE,							// Main Drawing Layer
		0,								          // Reserved
		0, 0, 0							       	// Layer Masks Ignored
	};
  
  int pfid = ChoosePixelFormat(mHdc, &pfd); 
  if (!pfid)
  {
    PvUtil::exitError("PersonShapeEstimator: ChoosePixelFormat FAILED!\n");    
  }
    
  if(!SetPixelFormat(mHdc,pfid,&pfd))
  {
    PvUtil::exitError("PersonShapeEstimator: SetPixelFormat FAILED!\n");	  
  }

  //setup opengl rendering context
  mHglrc = wglCreateContext(mHdc);
  if (!mHglrc)
  {
    PvUtil::exitError("PersonShapeEstimator: wglCreateContext FAILED!\n");    
  }

  wglMakeCurrent(mHdc, mHglrc);

}

void
PersonShapeEstimator::visualizeWorld(Image32& img)
{

  createRenderingSurface(true);
  initGl();
    
  //Now draw stuff
  GdiFlush();
  
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  for (float y = -6.0; y <= 6.0; y += 1.0)
  {
  
    for (float x = -6.0; x <= 6.0; x += 1.0)
    {
      drawPersonWorld(x,y,true);
    }
  }
  
  ///glFlush(); // remember to flush GL output! 
  getRenderedScene(img);

  //Destroy rendering surface
  destroyRenderingSurface();
}


void 
PersonShapeEstimator::visualizeFootLocations(Image32& img, const std::vector<Vector2i>& footLoc)
{
  //Initialize bitmap to draw to and openGl 
  createRenderingSurface(true);
  initGl();
  
  //Now draw stuff
  GdiFlush();
  
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


  //

  std::vector<Vector2i>::const_iterator iCoord;
  for (iCoord = footLoc.begin(); iCoord != footLoc.end(); iCoord++)
  {
    Vector2f worldPos = getWorldCoords(iCoord->x,iCoord->y);

    drawPersonWorld(worldPos.x,worldPos.y,true);
  } 

  getRenderedScene(img);

  //Destroy rendering surface
  destroyRenderingSurface();
}

void
PersonShapeEstimator::drawPersonCamera(int x, int y, Image32& img)
{
  GdiFlush();
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  Vector2f worldPos = getWorldCoords(x,y);
  
  drawPersonWorld(worldPos.x,worldPos.y);

  ///glFlush();

  Image32 worldImg;
  getRenderedScene(worldImg);

  applyCameraDistortion(img,worldImg);
}

void
PersonShapeEstimator::drawPersonCamera(int x, int y, Image8& img)
{
  GdiFlush();
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  Vector2f worldPos = getWorldCoords(x,y);
  
  drawPersonWorld(worldPos.x,worldPos.y); 

  ///glFlush();

  Image8 worldImg;
  getRenderedScene(worldImg);  

  applyCameraDistortion(img,worldImg);  
}

void
PersonShapeEstimator::generateMask(int x, int y)
{
  assert(x >= 0 && x < mMaskImage.width());
  assert(y >= 0 && y < mMaskImage.height());

  Image8 img;

  //TODO: doc this rect
  Rectanglei rect;

  if (mCropMasks)
  {
    drawPersonCamera(x,y,img); //masks are cropped

    rect.set(0,0,0,0);
  }
  else
  {    
    //////////////////////////////////////////////////////////////////////////
    // new method 

    rect = getDistortedBoundingBox(x,y);

    GdiFlush();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    Vector2f worldPos = getWorldCoords(x,y);  
    drawPersonWorld(worldPos.x,worldPos.y);

    Image8 worldImg;
    getRenderedScene(worldImg);

    applyCameraDistortion(img,worldImg, rect);  
  
    //////////////////////////////////////////////////////////////////////////
  }
  


  int count = 0;

  //scan through image, find boundaries for mask
  Rectanglei bbox;

  bbox.set(0,0,0,0);

  bool foundYet = false;

  for (int y0 = 0; y0 < img.height(); y0++)
  {
    for (int x0 = 0; x0 < img.width(); x0++)
    {
      if (img(x0,y0) != 0)
      {
        count++;
        
        if (!foundYet)
        {
          bbox.set(x0,y0,x0,y0);
          foundYet = true;
        }
        else
        {
          bbox.x0 = std::min(bbox.x0,x0);
          bbox.x1 = std::max(bbox.x1,x0);
          bbox.y0 = std::min(bbox.y0,y0);
          bbox.y1 = std::max(bbox.y1,y0);
        }        
      }
    }
  }    

  //bbox.x1 = std::min((int)img.width() - 1, bbox.x1 + 1);
  //bbox.y1 = std::min((int)img.height() - 1, bbox.y1 + 1);
  

  bbox.x1 += 1;
  bbox.y1 += 1;
  
  assert(x >= 0 && x < mMaskImage.width());
  assert(y >= 0 && y < mMaskImage.height());

  mask& m = mMaskImage(x,y);
  
  m.personPixels = count;
  if (count > 0)
  {
    Image8Ptr pMaskImg(new Image8(bbox.width(),bbox.height()));
    pMaskImg->crop(img,bbox.x0,bbox.y0,bbox.width(),bbox.height());
    m.pMaskImage = pMaskImg;

    assert(mMaskImage(x,y).pMaskImage.get());
  }

  
  //Adjust bounding box rects
  bbox.x0 += rect.x0;
  bbox.y0 += rect.y0;
  bbox.x1 += rect.x0;
  bbox.y1 += rect.y0;


  //Set the mask rectangle
  m.maskRect.set(bbox.x0,bbox.y0,bbox.x1,bbox.y1);

  assert(mMaskImage(x,y).personPixels >= 0);  
}


//TODO: put function somewhere appropriate
void
PersonShapeEstimator::flipHoriz(const mask& a, mask& b)
{

  if (a.personPixels == 0)
  {
    b.personPixels = 0;
    return;
  }

  //PVMSG("%d\n", a.personPixels);

  assert(a.pMaskImage.get());
  assert(b.pMaskImage.get());
  
  //flip image
  const Image8& aImg = *a.pMaskImage;
  Image8& bImg = *b.pMaskImage;

  bImg.resize(aImg.width(),aImg.height());

  const int width = bImg.width();
  const int height = bImg.height();

  for (int y = 0; y < height; y++)
  {
    for (int x = 0; x < width; x++)
    {

      assert(x >= 0 && x < bImg.width());
      assert(y >= 0 && y < bImg.height());

      assert(width - x - 1 >= 0 && width - x - 1 < aImg.width());
      assert(y >= 0 && y < aImg.height());

      bImg(x,y) = aImg(width - x - 1,y);
    }
  }

  //they share same # of person pixels
  b.personPixels = a.personPixels;
  
  //flip rect
  b.maskRect.x0 = mImageWidth - a.maskRect.x1;
  b.maskRect.x1 = mImageWidth - a.maskRect.x0;
  b.maskRect.y0 = a.maskRect.y0;
  b.maskRect.y1 = a.maskRect.y1;

}

void
PersonShapeEstimator::flipVert(const mask& a, mask& b)
{
  if (a.personPixels == 0)
  {
    b.personPixels = 0;
    b.maskRect = a.maskRect;
    b.pMaskImage.reset();
    return;
  }

  //flip image
  assert(a.pMaskImage.get());
  assert(b.pMaskImage.get());

  const Image8& aImg = *a.pMaskImage;
  Image8& bImg = *b.pMaskImage;

  bImg.resize(aImg.width(),aImg.height());

  const int width = bImg.width();
  const int height = bImg.height();

  for (int y = 0; y < height; y++)
  {
    for (int x = 0; x < width; x++)
    {
      assert(x >= 0 && x < bImg.width());
      assert(y >= 0 && y < bImg.height());

      assert(x >= 0 && x < aImg.width());
      assert(height - 1 - y >= 0 && height - 1 - y < aImg.height());

      bImg(x,y) = aImg(x,height - 1 - y);
    }
  }

  //they share same # of person pixels
  b.personPixels = a.personPixels;
  
  //flip rect
  b.maskRect.x0 = a.maskRect.x0;
  b.maskRect.x1 = a.maskRect.x1;
  b.maskRect.y0 = mImageHeight - a.maskRect.y1;
  b.maskRect.y1 = mImageHeight - a.maskRect.y0;

}




//////////////

/**
 * XML parsing.
 */
class XmlMaskHandler : public Handler
{
public:
  XmlMaskHandler(PersonShapeEstimator& pse) : mPSE(pse) {};

  virtual void start (const std::string& ns, const std::string& tag, const char **attr)
  {
    if (tag == "mask")
    {
      for (int i = 0; attr[i] != 0; i+=2)
      {
        if (!strcmp(attr[i],"x"))
        {
          mX = boost::lexical_cast<int>(attr[i+1]);
        } 
        else if (!strcmp(attr[i],"y"))
        {
          mY = boost::lexical_cast<int>(attr[i+1]);
        }
        else if (!strcmp(attr[i],"rx0"))
        {
          mMaskRect.x0 = boost::lexical_cast<int>(attr[i+1]);
        }
        else if (!strcmp(attr[i],"ry0"))
        {
          mMaskRect.y0 = boost::lexical_cast<int>(attr[i+1]);
        }
        else if (!strcmp(attr[i],"rx1"))
        {
          mMaskRect.x1 = boost::lexical_cast<int>(attr[i+1]);
        }
        else if (!strcmp(attr[i],"ry1"))
        {
          mMaskRect.y1 = boost::lexical_cast<int>(attr[i+1]);
        }
        else if (!strcmp(attr[i],"personPixels"))
        {
          mPersonPixels = boost::lexical_cast<int>(attr[i+1]);
        }
      }
    }

    mTagStack.push(tag);
  }

  virtual void cdata (const XML_Char *s, int len)
  {
    if (!mTagStack.empty())
    {
      if (mTagStack.top() == "mask")
      {
        std::vector<Uint8> out;
        Base64::decode(s,len,out);
        Image8Ptr pImg(new Image8(mMaskRect.width(),mMaskRect.height()));
        assert(out.size() == mMaskRect.width()*mMaskRect.height());
        for (int i = 0; i < out.size(); i++)
        {
          (*pImg)(i) = out[i];
        }
        mPSE.setMask(mX,mY,pImg,mMaskRect,mPersonPixels);
      }
      if (mTagStack.top() == "world_coordinates")
      {
        std::vector<std::string> wcs = tokenize(std::string(s,len)," \r\n");
        for (int i = 0; i < wcs.size(); i++)
        {
          mWorldCoords.push_back(boost::lexical_cast<float>(wcs[i]));
        }
      }
    }
  }

  virtual void end (const std::string& ns, const std::string& tag)
  {
    if (mTagStack.top() == "world_coordinates")
    {
      int i = 0;
      for(int x=0;x<mPSE.getWorldCoordsVector().width();x++)
      {
        for(int y=0;y<mPSE.getWorldCoordsVector().height();y++)
        {
          mPSE.setWorldCoords(x,y,mWorldCoords[i],mWorldCoords[i+1]);
          i+=2;
        }
      }
    }
    mTagStack.pop();
  }

  PersonShapeEstimator& mPSE;
  std::stack<std::string> mTagStack;
  int mX,mY;
  Rectanglei mMaskRect;
  int mPersonPixels;
  std::vector<float> mWorldCoords;
};



void
PersonShapeEstimator::loadXmlMasks(const std::string& path)
{
  XmlMaskHandler hand(*this);
  XmlParser parser;
  parser.registerNamespaceHandler("", &hand);

  std::string filename = combine_path(path,"masks.xml");

  if (!PvUtil::fileExists(filename.c_str()))
  {
    PvUtil::exitError("File not found: %s",filename.c_str());
  }

  mRealWorldCoords.resize(mImageWidth,mImageHeight);

  parser.parseFile(filename);

}

void 
PersonShapeEstimator::loadMasks(const std::string& path)
{ 

  //TODO: MORE ROBUST ERROR HANDLING (WHAT IF IMAGE SIZE DOESNT MATCH MASKS 
  // IN FOLDER)
  // THE SAVE MASK FUNCTION WILL HAVE TO SAVE THIS INFORMATION AS WELL

  //reset mask images
  for (int y = 0; y < mImageHeight; y++)
  {
    for (int x = 0; x < mImageWidth; x++)
    {
      assert(x >= 0 && x < mMaskImage.width());
      assert(y >= 0 && y < mMaskImage.height());

      mMaskImage(x,y).personPixels = 0;
      mMaskImage(x,y).pMaskImage.reset();
    }
  }

  if (PvUtil::fileExists(combine_path(path,"masks.xml").c_str()))
  {
    loadXmlMasks(path);
  }
  else
  {

    std::string filename = combine_path(path,"masks.txt");

    if (!PvUtil::fileExists(filename.c_str()))
    {
      PvUtil::exitError("loadMasks folder/masks.txt file does not exist\n");
    }

    //scan through files in the .txt file
    std::ifstream inFile(filename.c_str());

    //Scan header information and make sure that everything matches


    std::string imageWidth;
    std::string imageHeight;
    std::string personRadius;
    std::string personHeight;
    std::string cameraHeight;
    std::string cameraXRot;
    std::string cameraCcdWidth;
    std::string cameraCcdHeight;
    std::string cameraFocalLength;
    std::string distortString;

    //int renderImageWidth;
    //int renderImageHeight;  

    inFile >> imageWidth;
    inFile >> imageHeight;
    inFile >> personRadius;
    inFile >> personHeight;
    inFile >> cameraHeight;
    inFile >> cameraXRot;
    inFile >> cameraFocalLength;
    inFile >> cameraCcdWidth;
    inFile >> cameraCcdHeight;
    inFile >> distortString;

    //inFile >> renderImageWidth;
    //inFile >> renderImageHeight;
    //TODO: RENDER IMAGE WIDTH AND RENDER IMAGE HEIGHT

    //TODO: THIS COMPARISON IS VERY SIMPLE AND NOT THE BEST WAY TO DO THINGS
    // I.E. ".002" != ".0020" (FIX THIS LATER) 

    /*
    outFile << mSettings.getString("arrayWidth") << std::endl;
    outFile << mSettings.getString("arrayHeight") << std::endl;  
    outFile << mSettings.getString("Camera/height") << std::endl;
    outFile << mSettings.getString("Camera/ccdWidth") << std::endl;
    outFile << mSettings.getString("Camera/ccdHeight") << std::endl;
    outFile << mSettings.getString("Camera/focalLength") << std::endl;
    outFile << mSettings.getString("distortParamters") << std::endl;
    */

    if (imageWidth != mSettings.getString("arrayWidth") || 
      imageHeight != mSettings.getString("arrayHeight") ||      
      personRadius != mSettings.getString("personRadius") ||      
      personHeight != mSettings.getString("personHeight") ||      
      cameraHeight != mSettings.getString("Camera/height") ||
      cameraXRot != mSettings.getString("Camera/rotX") ||
      cameraCcdWidth != mSettings.getString("Camera/ccdWidth") ||
      cameraCcdHeight != mSettings.getString("Camera/ccdHeight") ||
      cameraFocalLength != mSettings.getString("Camera/focalLength") ||
      distortString != mSettings.getString("Camera/distortParameters")) 
      //TODO: RENDER IMAGE WIDTH AND HEIGHT AS WELL
    {
      PvUtil::exitError("PersonShapeEstimator: Error loading mask file. \
                        Parameters do not match.");
    }

    //Debug
    /*std::ofstream temp("C:/temp.txt");
    for(int x=0;x<mImageWidth;x++)
    {
    for(int y=0;y<mImageHeight;y++)
    {
    temp<<mRealWorldCoords(x,y).x<<" "<<mRealWorldCoords(x,y).y<<std::endl;
    }
    }*/


    //Modification for real world coords done


    std::string imgFilename;
    while (true)
    {    

      std::getline(inFile, imgFilename);

      //PVMSG("imgFilename: '%s'\n", imgFilename.c_str());

      if (inFile.eof())
      {
        break;
      }

      if (imgFilename == "")
      {      
        continue;
      }

      std::string fname = get_file_name_without_extension(imgFilename);
      std::vector<std::string> tokens = tokenize(fname,"_");

      assert(tokens.size() == 8);

      std::string maskName = tokens[0];
      int x = boost::lexical_cast<int>(tokens[1]);
      int y = boost::lexical_cast<int>(tokens[2]);

      Rectanglei bbox;
      bbox.x0 = boost::lexical_cast<int>(tokens[3]);
      bbox.y0 = boost::lexical_cast<int>(tokens[4]);
      bbox.x1 = boost::lexical_cast<int>(tokens[5]);
      bbox.y1 = boost::lexical_cast<int>(tokens[6]);   

      int personPixels = boost::lexical_cast<int>(tokens[7]);    

      Image8Ptr pMask(new Image8(1,1));
      pMask->load(imgFilename.c_str());    


      //TEMP
      /*
      if (x == 2 && y == 1)
      {
      PVMSG("LOADING DEBUG\n");

      PVMSG("x,y: %d %d\n",x,y);
      PVMSG("rect: %d %d %d %d\n",bbox.x0,bbox.y0,bbox.x1,bbox.y1);
      PVMSG("img: %d %d\n",pMask->width(), pMask->height());
      PVMSG("personPixels: %d\n",personPixels);

      }
      */
      //END TEMP


      if (maskName == "mask")
      {    
        mMaskImage(x,y).pMaskImage = pMask;
        mMaskImage(x,y).maskRect = bbox;
        mMaskImage(x,y).personPixels = personPixels;
      }
      /*
      else if (maskName == "overlapMask")
      {
      mOverlapMaskImage(x,y).pMaskImage = pMask;
      mOverlapMaskImage(x,y).maskRect = bbox;
      mOverlapMaskImage(x,y).personPixels = personPixels;
      }
      */
      else
      {
        assert(false); // this should not happen
      }


      //PVMSG("%s\n", imgFilename.c_str());
    }

    inFile.close();

    //Modification to write the real world coords
    std::string worldCoordsFile = combine_path(path,"WorldCoords.txt");
    std::ifstream worldCoords(worldCoordsFile.c_str()); 
    mRealWorldCoords.resize(mImageWidth,mImageHeight);
    for(int x=0;x<mImageWidth;x++)
    {
      for(int y=0;y<mImageHeight;y++)
      {
        worldCoords>>mRealWorldCoords(x,y).x>>mRealWorldCoords(x,y).y;
      }
    }
  }

  //Now generate outlines for all of the masks
  generateMaskOutline();

  //Only mirror the quadrants if the camera is looking staight down
  if (mCameraRotX == 0.0)
  {
    mirrorQuadrants();
  }
}

void
PersonShapeEstimator::generateMaskOutline()
{

  mMaskOutlineImage.resize(mMaskImage.width(),mMaskImage.height());
  //reset mask images
  for (int y = 0; y < mImageHeight; y++)
  {
    for (int x = 0; x < mImageWidth; x++)
    {
      mMaskOutlineImage(x,y).personPixels = 0;
      mMaskOutlineImage(x,y).pMaskImage.reset();
    }
  }

  for(int x=0;x<mMaskImage.width();x++)
  {
    for(int y=0;y<mMaskImage.height();y++)
    {
      if(mMaskImage(x,y).pMaskImage.get())
      {
        mask tempMask;      
        Image8 temp = *(mMaskImage(x,y).pMaskImage);
        Image8Ptr temp2(new Image8(temp.width(),temp.height()));
        //temp2->resize(temp.width(),temp.height());
        Rectanglei tempRect = mMaskImage(x,y).maskRect;
        int personPixels = 0;
        for(int a=0;a<temp.width();a++)
        {
          for(int b=0;b<temp.height();b++)
          {
            (*temp2)(a,b)=0;
            if(temp(a,b)>0)
            {
              if(a==0 || b==0 || a==(temp.width()-1) || b==(temp.height()-1))
              {
                (*temp2)(a,b)=255;
                personPixels++;
              }
              else
              {
                int totalNeighborhood = temp(a-1,b-1) + temp(a-1,b) + temp(a-1,b+1) + temp(a,b+1) + temp(a+1,b+1) + temp(a+1,b) + temp(a+1,b-1) + temp(a,b-1);
                if(totalNeighborhood<255*8)
                {
                  (*temp2)(a,b)=255;
                  personPixels++;
                }
              }
            }
          }
        }
        //mMaskOutlineImage(x,y).pMaskImage->resize(temp2->width(),temp2->height());
        (mMaskOutlineImage(x,y).pMaskImage) = temp2;
        mMaskOutlineImage(x,y).maskRect = tempRect;
        mMaskOutlineImage(x,y).personPixels = personPixels;
      }
    }
  }

  
  

}

void 
PersonShapeEstimator::saveMasks(const std::string path)
{

  //TODO: make this a public function that handles everything relating to masks
  //Initialize rendering surface and openGL
  /*
  createRenderingSurface(false);    
  initGl();    
  createMasks();
  saveMasks("c:/ai/bin/vms/masks/cc_16ft");
  */

  bool xmlOutput = mSettings.getBool("useXmlOutput");

  std::string masksFilename = combine_path(path, "masks.txt");
  std::string xmlMasksFilename = combine_path(path, "masks.xml");
  std::string lockFilename = combine_path(path, "masks.inprogress");

  boost::filesystem::path lockPath(lockFilename.c_str(), boost::filesystem::native);

  //Check to see if 'masks.inprogress' exists. If it does, that means an earlier attempt
  // to save masks was aborted. In this case, we remove all .pgm files and masks.txt
  if (boost::filesystem::exists(lockPath))
  {
    boost::filesystem::path maskFilePath(masksFilename.c_str(), boost::filesystem::native);
    if (boost::filesystem::exists(maskFilePath))
    {
      PVMSG("Mask generation previously interrupted. Recovering...\n");
      boost::filesystem::remove(maskFilePath);
    }
    //TODO: remove previously generated .PGM files?
    // or allow them to (possibly) be overwritten?

  } 

  boost::filesystem::path folderPath(path.c_str(), boost::filesystem::native);
  if (!boost::filesystem::exists(folderPath))
  { 
    //create the path if it does not exist
    PVMSG("Save masks path does not exist, creating...\n");
    boost::filesystem::create_directories(folderPath);
  }

  //Create a temporary file during the creation process. If the process is interrupted
  // we can detect this file and take the appropriate action.  
  std::ofstream lockFile(lockFilename.c_str());
  lockFile.close();

  if (!xmlOutput)
  {
    //Modification to write the real world coords
    std::string realWorldCoordsFile = combine_path(path, "WorldCoords.txt");
    std::ofstream worldCoords(realWorldCoordsFile.c_str()); 
    for(int x=0;x<mRealWorldCoords.width();x++)
    {
      for(int y=0;y<mRealWorldCoords.height();y++)
      {
        worldCoords<<mRealWorldCoords(x,y).x<<" "<<mRealWorldCoords(x,y).y<<std::endl;
      }
    }
  }

  std::ofstream outFile;

  if (xmlOutput)
  {
    outFile.open(xmlMasksFilename.c_str());
  }
  else
  {
    outFile.open(masksFilename.c_str());
  }


  //Write header information
  /*
  
  int mImageWidth;
  int mImageHeight;
  int mLargeAxis;

  float mCameraHeight;
  float mPersonHeight;
  float mPersonRadius;

  float mCameraCcdWidth;
  float mCameraCcdHeight;
  float mCameraFocalLength;

  Polynomial mPolynomial;

  int mRenderImageWidth;
  int mRenderImageHeight;
  */

  if (!xmlOutput)
  {
    outFile << mSettings.getString("arrayWidth") << std::endl;
    outFile << mSettings.getString("arrayHeight") << std::endl;  
    outFile << mSettings.getString("personRadius") << std::endl;  
    outFile << mSettings.getString("personHeight") << std::endl;  
    outFile << mSettings.getString("Camera/height") << std::endl;
    outFile << mSettings.getString("Camera/rotX") << std::endl;
    outFile << mSettings.getString("Camera/focalLength") << std::endl;
    outFile << mSettings.getString("Camera/ccdWidth") << std::endl;
    outFile << mSettings.getString("Camera/ccdHeight") << std::endl;  

    outFile << mSettings.getString("Camera/distortParameters") 
      << std::endl;
  }
  else
  {
    outFile << "<?xml version='1.0'?>" << std::endl;
    outFile << "<mask_file version='1.0'>" << std::endl;
    outFile << "<arrayWidth>" << mSettings.getString("arrayWidth") << "</arrayWidth>" << std::endl;
    outFile << "<arrayHeight>" << mSettings.getString("arrayHeight") << "</arrayHeight>" << std::endl;  
    outFile << "<personRadius>" << mSettings.getString("personRadius") << "</personRadius>" << std::endl;  
    outFile << "<personHeight>" << mSettings.getString("personHeight") << "</personHeight>" << std::endl;
    outFile << "<camera>" << std::endl;
    outFile << "<height>" << mSettings.getString("Camera/height") << "</height>" << std::endl;
    outFile << "<rotX>" << mSettings.getString("Camera/rotX") << "</rotX>" << std::endl;
    outFile << "<focalLength>" << mSettings.getString("Camera/focalLength") << "</focalLength>" << std::endl;
    outFile << "<ccdWidth>" << mSettings.getString("Camera/ccdWidth") << "</ccdWidth>" << std::endl;
    outFile << "<ccdHeight>" << mSettings.getString("Camera/ccdHeight") << "</ccdHeight>" << std::endl;  
    outFile << "<distortParameters>" << mSettings.getString("Camera/distortParameters") << "</distortParameters>" << std::endl;  
    outFile << "</camera>" << std::endl;
  }

  //TODO: RENDER IMAGE WIDTH AND RENDER IMAGE HEIGHT

  unsigned int yEnd = mImageHeight;
  unsigned int xEnd = mImageWidth;

  //If the camera is looking straight down, we only need to save the 
  // upper left quadrant, and then we can take advantage of the symmetry
  // and mirror the quadrant to the others to speed things up.  
  if (mCameraRotX == 0.0)
  {
    xEnd = mImageWidth/2;
    yEnd = mImageHeight/2;
  }  

  for (int y = 0; y < yEnd; y++)
  {
    for (int x = 0; x < xEnd; x++)
    {
      if (mMaskImage(x,y).personPixels > 0)
      {
        if (!xmlOutput)
        {
          std::string filename = aitSprintf("mask_%d_%d_%d_%d_%d_%d_%d.pgm",
            x,
            y,
            mMaskImage(x,y).maskRect.x0,
            mMaskImage(x,y).maskRect.y0,
            mMaskImage(x,y).maskRect.x1,
            mMaskImage(x,y).maskRect.y1,
            mMaskImage(x,y).personPixels);                

          mMaskImage(x,y).pMaskImage->save(combine_path(path,filename).c_str());
          outFile << combine_path(path,filename) << std::endl;
        }
        else
        {
          outFile << "<mask x='" << x << "' y = '" << y << "' ";
          outFile << "rx0='" << mMaskImage(x,y).maskRect.x0 << "' ";
          outFile << "ry0='" << mMaskImage(x,y).maskRect.y0 << "' ";
          outFile << "rx1='" << mMaskImage(x,y).maskRect.x1 << "' ";
          outFile << "ry1='" << mMaskImage(x,y).maskRect.y1 << "' ";
          outFile << "personPixels='" << mMaskImage(x,y).personPixels << "'>";

          Image8 maskImage = *(mMaskImage(x,y).pMaskImage);
          outFile << Base64::encode(maskImage.pointer(),maskImage.width()*maskImage.height());

          outFile << "</mask>" << std::endl;
        }

        //Save and print out overlapped mask
        /*
        filename = aitSprintf("overlapMask_%d_%d_%d_%d_%d_%d_%d.pgm",
          x,
          y,
          mOverlapMaskImage(x,y).maskRect.x0,
          mOverlapMaskImage(x,y).maskRect.y0,
          mOverlapMaskImage(x,y).maskRect.x1,
          mOverlapMaskImage(x,y).maskRect.y1,
          mOverlapMaskImage(x,y).personPixels);                

        mOverlapMaskImage(x,y).pMaskImage->save(combine_path(
          path,filename).c_str());
        outFile << combine_path(path,filename) << std::endl;
        */
      }

      
    }
  }

  if (xmlOutput)
  {
    outFile << "<world_coordinates>" << std::endl;
    for(int x=0;x<mRealWorldCoords.width();x++)
    {
      for(int y=0;y<mRealWorldCoords.height();y++)
      {
        outFile<<mRealWorldCoords(x,y).x<<" "<<mRealWorldCoords(x,y).y<<std::endl;
      }
    }
    outFile << "</world_coordinates>" << std::endl;
    outFile << "</mask_file>" << std::endl;
  }

  outFile.close();

  //Now that all of the masks have been properly saved, remove our temporary
  // 'inprogress' file  
  boost::filesystem::remove(lockPath);
}

void 
PersonShapeEstimator::mirrorQuadrants()
{
  //Now flip these masks and use them in the other three quadrants
  //PVMSG("Filling top right quadrants\n");
  //fill in top right quadrant

  for (int y = 0; y < mImageHeight/2; y++)
  {
    for (int x = mImageWidth/2; x < mImageWidth; x++)
    {
      //create image, and flip masks
      
      mMaskImage(x,y).pMaskImage.reset(new Image8(1,1)); 
      flipHoriz(mMaskImage(mImageWidth-1-x,y),mMaskImage(x,y));

      //Flip mask outlines too
      if (mMaskOutlineImage.width() > 1)
      {
        mMaskOutlineImage(x,y).pMaskImage.reset(new Image8(1,1)); 
        flipHoriz(mMaskOutlineImage(mImageWidth-1-x,y),mMaskOutlineImage(x,y));
      }     
    }
  }

  //flip about y axis
  //PVMSG("Filling bottom half quadrant\n");
  for (int y = mImageHeight/2; y < mImageHeight; y++)
  {
    for (int x = 0; x < mImageWidth; x++)
    {
      //create image, and flip masks
      mMaskImage(x,y).pMaskImage.reset(new Image8(1,1));      
      flipVert(mMaskImage(x,mImageHeight-1-y),mMaskImage(x,y));

      //Flip mask outlines too
      if (mMaskOutlineImage.width() > 1)
      {
        mMaskOutlineImage(x,y).pMaskImage.reset(new Image8(1,1));      
        flipVert(mMaskOutlineImage(x,mImageHeight-1-y),mMaskOutlineImage(x,y));      
      }
    }
  }
  

  PVMSG("Done with other quadrants\n"); 
}

void
PersonShapeEstimator::createMasks()
{
  // Loop through 1st quadrant pixels
  //  -Calculate world coords for each pixel
  //  -draw 'person' at that position
  //  -distort image and generate mask  
  
  assert(mImageWidth % 2 == 0);
  assert(mImageHeight % 2 == 0);  

  unsigned int yEnd = mImageHeight;
  unsigned int xEnd = mImageWidth;

  //If the camera is looking straight down, we only need to generate the 
  // upper left quadrant, and then we can take advantage of the symmetry
  // and mirror the quadrant to the others to speed things up.  
  if (mCameraRotX == 0.0)
  {
    xEnd = mImageWidth/2;
    yEnd = mImageHeight/2;
  }

  //Generate masks
  for (int y = 0; y < yEnd; y++)
  {
    PVMSG("%d\n", y);
    for (int x = 0; x < xEnd; x++)
    {
      generateMask(x,y);  
      //PVMSG("count: %d\n", mMaskImage(x,y).personPixels);
    }
  }

  //Now that masks have been generated, generate overlap masks
  /*
  for (int y = 0; y < mImageHeight/2; y++)
  {
    PVMSG("%d\n", y);
    for (int x = 0; x < mImageWidth/2; x++)
    {
      generateOverlapMask(x,y);
    }
  }
  */
  
  //If the camera is looking straight down, mirror the quadrants in memory
  if (mCameraRotX == 0.0)
  {
    mirrorQuadrants();
  }
}

Rectanglei 
PersonShapeEstimator::getDistortedBoundingBox(int x, int y) const
{

  //TODO: OpenGL must be initialized and initGL must be called for this to work
  // because we use gluUnproject

  //Get the position in world coordinates
  Vector2f worldPos = getWorldCoords(x,y);

  //Generate 8 points representing the the bounding box for the person
  // in world coords
  std::vector<Vector3f> worldPoints;
  worldPoints.push_back(Vector3f(worldPos.x + mPersonRadius, 
                                  worldPos.y + mPersonRadius, 0.0));
  worldPoints.push_back(Vector3f(worldPos.x + mPersonRadius, 
                                  worldPos.y - mPersonRadius, 0.0));
  worldPoints.push_back(Vector3f(worldPos.x - mPersonRadius, 
                                  worldPos.y + mPersonRadius, 0.0));
  worldPoints.push_back(Vector3f(worldPos.x - mPersonRadius, 
                                  worldPos.y - mPersonRadius, 0.0));
  worldPoints.push_back(Vector3f(worldPos.x + mPersonRadius, 
                                  worldPos.y + mPersonRadius, mPersonHeight));
  worldPoints.push_back(Vector3f(worldPos.x + mPersonRadius, 
                                  worldPos.y - mPersonRadius, mPersonHeight));
  worldPoints.push_back(Vector3f(worldPos.x - mPersonRadius, 
                                  worldPos.y + mPersonRadius, mPersonHeight));
  worldPoints.push_back(Vector3f(worldPos.x - mPersonRadius, 
                                  worldPos.y - mPersonRadius, mPersonHeight));

  //Extract the modelview/projection matrices
  GLint viewport[4];
  GLdouble mvMatrix[16];
  GLdouble projMatrix[16]; 

  glGetIntegerv(GL_VIEWPORT,viewport);
  glGetDoublev(GL_MODELVIEW_MATRIX,mvMatrix);
  glGetDoublev(GL_PROJECTION_MATRIX,projMatrix); 

  //Now convert world coordinates to our undistorted image coordinates
  std::vector<Vector2f> undistortedPoints;

  std::vector<Vector3f>::const_iterator iPoint;
  for (iPoint = worldPoints.begin(); iPoint != worldPoints.end(); iPoint++)
  {
    GLdouble wx,wy,wz;   
    gluProject(iPoint->x,
               iPoint->y,
               iPoint->z,
               mvMatrix,
               projMatrix,
               viewport,
               &wx,
               &wy,
               &wz);

    undistortedPoints.push_back(Vector2f(wx,wy));    
  }


  //Now normalize each of these undistorted coordinate coordinates
  std::vector<Vector2f> distortedPoints;
  for (iPoint = undistortedPoints.begin(); 
       iPoint != undistortedPoints.end(); iPoint++)
  {
    double x = iPoint->x;
    double y = iPoint->y;

    double xNorm = (x - mRenderImageWidth/2)/(mLargeAxis/2.0);
    double yNorm = (y - mRenderImageHeight/2)/(mLargeAxis/2.0);
    
    //first convert x,y to rUndistorted
    double rUndistorted = sqrt(xNorm*xNorm + yNorm*yNorm);
    double radians = atan2(yNorm,xNorm);        
    
    //Find the root of this polynomial, which is r in the distorted image
    double rDistorted = mPolynomial.solve(rUndistorted);

    //now convert back into image coordinates
    double newX = rDistorted*cos(radians)*(mLargeAxis/2) + mImageWidth/2;
    double newY = rDistorted*sin(radians)*(mLargeAxis/2) + mImageHeight/2;    

    distortedPoints.push_back(Vector2f(newX, newY));    
  }    

  //Now create a rectangle which contains all of these points
  Rectanglei bbox;
  bbox.set((int)distortedPoints[0].x,
           (int)distortedPoints[0].y,
           (int)distortedPoints[0].x,
           (int)distortedPoints[0].y);

  for (iPoint = distortedPoints.begin(); iPoint != distortedPoints.end(); iPoint++)
  {
    int x = (int)iPoint->x;
    int y = (int)iPoint->y;

    bbox.set(
      std::min(x,bbox.x0),
      std::min(y,bbox.y0),
      std::max(x,bbox.x1),
      std::max(y,bbox.y1));    
  }

  bbox.x1 += 1;
  bbox.y1 += 1;

  return bbox;  
}

void
PersonShapeEstimator::init(const std::string& settingsPath)
{  

  mSettings.changePath(settingsPath);   

  mVerbose = mSettings.getBool("verbose [bool]", false);

  mCameraHeight = mSettings.getDouble("Camera/height");
  mCameraCcdWidth = mSettings.getDouble("Camera/ccdWidth");
  mCameraCcdHeight = mSettings.getDouble("Camera/ccdHeight");
  mCameraFocalLength = mSettings.getDouble("Camera/focalLength");

  assert(mCameraHeight > 0.0 && mCameraCcdWidth > 0.0 || 
    mCameraCcdHeight > 0.0 && mCameraFocalLength > 0.0);

  mPersonRadius = mSettings.getDouble("personRadius");
  mPersonHeight = mSettings.getDouble("personHeight");

  assert(mPersonRadius > 0.0 && mPersonHeight > 0.0);

  //Size of image rendering surface
  mRenderImageWidth = 1280;
  mRenderImageHeight = 960;

  
  //mImageWidth = 80;
  //mImageHeight = 60;

  mImageWidth = mSettings.getInt("arrayWidth");
  mImageHeight = mSettings.getInt("arrayHeight");


  mAutoGenerateFolder = mSettings.getString("autoGenerateFolder", "ERROR");
  mAutoGenerateFolder = normalize_path(mAutoGenerateFolder);
  mAutoGenerateMasks = mSettings.getBool("autoGenerateMasks", false);

  assert(mImageWidth > 0 && mImageHeight > 0);    

  mCameraRotX = mSettings.getDouble("Camera/rotX",0.0);

  //mPixelsPerCoord = 0.0;

  mLargeAxis = std::max(mImageHeight, mImageWidth);    

  std::list<double> coeff = 
    mSettings.getDoubleList("Camera/distortParameters","ERROR");
  
  //BUGFIX: Delete all leading zero's. The Polynomial::getMonic() will fail
  // if the leading coefficient is zero.
  std::list<double>::iterator iCoeff;
  for (iCoeff = coeff.begin(); iCoeff != coeff.end();)
  {
    if (*iCoeff == 0.0)
    {
      iCoeff = coeff.erase(iCoeff);
    }
    else
    {
      break;
    }
  }

  mPolynomial.init(coeff);

  mMaskImage.resize(mImageWidth,mImageHeight);

  //If requested, load mask images that have already been generated  
  std::string loadMaskFolder = mSettings.getString("loadMasks","");
  if (loadMaskFolder != "")
  {    
    loadMasks(loadMaskFolder);

    //Done to use safeGetWorldCoords
    createRenderingSurface(false);
    initGl();
  }
  else if (mAutoGenerateMasks)
  {    

    //Generate unique folder name based on parameters
    std::string loadMaskFolder = 
      combine_path(mAutoGenerateFolder, getFolderFromParameters());    

    //We must normalize the path or else boost::filesystem::exists crashes
    loadMaskFolder = normalize_path(loadMaskFolder);  

#if defined(_MSC_VER)

#if(_MSC_VER > 1200) //MSVC 6.0 WORKAROUND   

    //Must manually create a path with the 'native'
    // option or get boost runtime error
    boost::filesystem::path maskPath(loadMaskFolder.c_str(),
                                     boost::filesystem::native);

    std::string lockFilename = combine_path(loadMaskFolder, "masks.inprogress");
    boost::filesystem::path lockPath(lockFilename.c_str(), boost::filesystem::native);  

    //If the mask folder doesn't exist, or if it exists but the the
    // 'inprogress' files remains, then save a new set of masks
    if (boost::filesystem::exists(maskPath) && !boost::filesystem::exists(lockPath))
    {    
      PVMSG("PersonShape masks already exist, loading...: %s\n", 
        loadMaskFolder.c_str());
      //load the masks
      loadMasks(loadMaskFolder);

      //Done to use safeGetWorldCoords
      createRenderingSurface(false);
      initGl();
    }
    else
    {
      PVMSG("PersonShape masks not found, creating...: %s\n", 
        loadMaskFolder.c_str());      
      //run genMask program here

      //std::string cmdLine = "maskgen.exe " + 
      std::string cmdLine = mSettings.getString("arrayWidth");
      cmdLine += " " + mSettings.getString("arrayHeight");
      cmdLine += " " + mSettings.getString("personRadius");
      cmdLine += " " + mSettings.getString("personHeight");
      cmdLine += " " + mSettings.getString("Camera/height");
      cmdLine += " " + mSettings.getString("Camera/rotX");
      cmdLine += " " + mSettings.getString("Camera/focalLength");
      cmdLine += " " + mSettings.getString("Camera/ccdWidth");
      cmdLine += " " + mSettings.getString("Camera/ccdHeight");
      cmdLine += " " + mSettings.getString("Camera/distortParameters");
      cmdLine += " --xml 1 --create \"" + loadMaskFolder + "\"";    

      PVMSG("maskgen parameters: %s\n", cmdLine.c_str());

      SHELLEXECUTEINFO info;

      info.cbSize = sizeof(info);
      info.fMask = SEE_MASK_NOCLOSEPROCESS; //we want to get return val
      info.hwnd = NULL;
      info.lpVerb = "open";
      info.lpFile = "maskgen.exe";
      info.lpParameters = cmdLine.c_str();
      info.lpDirectory = NULL;
      if (mVerbose)
      {      
        info.nShow = SW_SHOW; //show console window
      }
      else
      {
        info.nShow = SW_HIDE; //hide console window
      }
      info.hInstApp = NULL;
      info.lpIDList = NULL;
      info.lpClass = NULL;
      info.hkeyClass = NULL;
      info.dwHotKey = NULL;
      //info.DUMMYUNIONNAME.hIcon = NULL;
      //info.DUMMYUNIONNAME.hMonitor = NULL;
      info.hProcess = NULL;

      if (!ShellExecuteEx(&info))
      {
        PvUtil::exitError("PersonShape: Could not execute maskgen.exe");
      }

      WaitForSingleObject(info.hProcess, INFINITE);
      DWORD exitCode = 0;
      if (GetExitCodeProcess(info.hProcess, &exitCode) == 0)
      {
        PvUtil::exitError("PersonShape: GetExitCodeProcess() failed");
      }

      // release process
      CloseHandle(info.hProcess); 

      if (exitCode != 0)
      {
        PvUtil::exitError(aitSprintf("maskgen.exe failed: %d", exitCode).c_str());
      }

      loadMasks(loadMaskFolder);

      //Done to use safeGetWorldCoords
      createRenderingSurface(false);
      initGl();
    }  

#endif //MSVC 6.0 WORKAROUND
#endif //MSVC 6.0 WORKAROUND
  }  
 
  //TODO: make a changeable XML setting
  mCropMasks = false; 

  //TODO: NEED A SEPARATE BOOLEAN TO SEE IF MASKS ARE LOADED? 
  mIsInitialized = true;    
}

void 
PersonShapeEstimator::cleanForegroundSegmentation(int x, int y, 
                                                  Image8& img, 
                                                  Vector2i offset = Vector2i(0,0))
{
  //BELOW BLOCK IS A HACK, FIGURE OUT WHY IT FAILS LATER ON
  {
    if (x + offset.x < 0)
    {
      PVMSG("ERROR: cleanForegroundSegmentation UNDER: x: %d\n", x);
      x = -offset.x;      
    }
    if (x + offset.x >= mMaskImage.width())
    {
      PVMSG("ERROR: cleanForegroundSegmentation OVER: x: %d\n", x);
      x = mMaskImage.width() - offset.x - 1;     
    }
    if (y + offset.y < 0)
    {
      PVMSG("ERROR: cleanForegroundSegmentation UNDER: y: %d\n", y);
      y = -offset.y;      
    }
    if (y + offset.y >= mMaskImage.height())
    {
      PVMSG("ERROR: cleanForegroundSegmentation OVER: y: %d\n", y);
      y = mMaskImage.height() - offset.y - 1;
    }
  }  

  assert(x + offset.x >=0 && x + offset.x < mMaskImage.width());
  assert(y + offset.y >=0 && y + offset.y < mMaskImage.height());

  int personPixels = mMaskImage(x + offset.x,y + offset.y).personPixels;

  if (personPixels == 1)
  {
    //just set pixel to black
    img(x,y) = 0;
  }
  else if (personPixels > 1)
  {

    assert(mMaskImage(x + offset.x,y + offset.y).pMaskImage.get());

    const Image8& maskImg = 
      *(mMaskImage(x + offset.x,y + offset.y).pMaskImage);
    const Rectanglei& rect = mMaskImage(x + offset.x,y + offset.y).maskRect;

    /*
    for (int b = rect.y0 - offset.y; b < rect.y1 - offset.y; b++)
    {
      for (int a = rect.x0 - offset.x; a < rect.x1 - offset.x; a++)
      {
        //TODO: following checks unnecessary with proper cropping
        if (!(a >= 0 && a < srcImg.width()))
        {
          continue;
        }
        if (!(b >= 0 && b < srcImg.height()))
        {
          continue;
        }           
        
        if (srcImg(a,b) == 255 &&
          mask(a-rect.x0 + offset.x,b-rect.y0 + offset.y) == 255)
        {
          count++;                       
        }
      }
    }
    */

    
    for (int b = rect.y0 - offset.y; b < rect.y1 - offset.y; b++)
    {
      for (int a = rect.x0 - offset.x; a < rect.x1 - offset.x; a++)
      {   
        if (!(a >= 0 && a < img.width()))
        {
          continue;
        }
        if (!(b >= 0 && b < img.height()))
        {
          continue;
        }

        //int imageWidth = img.width();
        //int imageHeight = img.height();

        assert(a - rect.x0 + offset.x >= 0);
        assert(a - rect.x0 + offset.x < maskImg.width());
        assert(b - rect.y0 + offset.y >= 0);
        assert(b - rect.y0 + offset.y < maskImg.height());

        if (maskImg(a - rect.x0 + offset.x, b - rect.y0 + offset.y) == 255)
        {
          assert(a >= 0);
          assert(a < img.width());
          assert(b >= 0);
          assert(b < img.height());

          img(a,b) = 0;

        }      
      }
    }
  }  
}

void 
PersonShapeEstimator::drawPersonPixels(int x, int y, int color, 
                                                  Image32& img, 
                                                  Vector2i offset = Vector2i(0,0))
{
  //BELOW BLOCK IS A HACK, FIGURE OUT WHY IT FAILS LATER ON
  {
    if (x + offset.x < 0)
    {
      PVMSG("ERROR: cleanForegroundSegmentation UNDER: x: %d\n", x);
      x = -offset.x;      
    }
    if (x + offset.x >= mMaskImage.width())
    {
      PVMSG("ERROR: cleanForegroundSegmentation OVER: x: %d\n", x);
      x = mMaskImage.width() - offset.x - 1;     
    }
    if (y + offset.y < 0)
    {
      PVMSG("ERROR: cleanForegroundSegmentation UNDER: y: %d\n", y);
      y = -offset.y;      
    }
    if (y + offset.y >= mMaskImage.height())
    {
      PVMSG("ERROR: cleanForegroundSegmentation OVER: y: %d\n", y);
      y = mMaskImage.height() - offset.y - 1;
    }
  }  

  assert(x + offset.x >=0 && x + offset.x < mMaskImage.width());
  assert(y + offset.y >=0 && y + offset.y < mMaskImage.height());

  int personPixels = mMaskImage(x + offset.x,y + offset.y).personPixels;

  if (personPixels == 1)
  {
    //just set pixel to black
    img(x,y) = color;
  }
  else if (personPixels > 1)
  {

    assert(mMaskImage(x + offset.x,y + offset.y).pMaskImage.get());

    const Image8& maskImg = 
      *(mMaskImage(x + offset.x,y + offset.y).pMaskImage);
    const Rectanglei& rect = mMaskImage(x + offset.x,y + offset.y).maskRect;

    /*
    for (int b = rect.y0 - offset.y; b < rect.y1 - offset.y; b++)
    {
      for (int a = rect.x0 - offset.x; a < rect.x1 - offset.x; a++)
      {
        //TODO: following checks unnecessary with proper cropping
        if (!(a >= 0 && a < srcImg.width()))
        {
          continue;
        }
        if (!(b >= 0 && b < srcImg.height()))
        {
          continue;
        }           
        
        if (srcImg(a,b) == 255 &&
          mask(a-rect.x0 + offset.x,b-rect.y0 + offset.y) == 255)
        {
          count++;                       
        }
      }
    }
    */

    
    for (int b = rect.y0 - offset.y; b < rect.y1 - offset.y; b++)
    {
      for (int a = rect.x0 - offset.x; a < rect.x1 - offset.x; a++)
      {   
        if (!(a >= 0 && a < img.width()))
        {
          continue;
        }
        if (!(b >= 0 && b < img.height()))
        {
          continue;
        }

        //int imageWidth = img.width();
        //int imageHeight = img.height();

        assert(a - rect.x0 + offset.x >= 0);
        assert(a - rect.x0 + offset.x < maskImg.width());
        assert(b - rect.y0 + offset.y >= 0);
        assert(b - rect.y0 + offset.y < maskImg.height());

        if (maskImg(a - rect.x0 + offset.x, b - rect.y0 + offset.y) == 255)
        {
          assert(a >= 0);
          assert(a < img.width());
          assert(b >= 0);
          assert(b < img.height());

          img(a,b) = color;

        }      
      }
    }
  }  
}




void 
PersonShapeEstimator::saveMasksToFolder(const std::string& path)
{
  //Create mask code
  createRenderingSurface(false);
  initGl(); 

  createMasks();

  //Modified to get the real world coords
  mRealWorldCoords.resize(mImageWidth,mImageHeight);
  for(int x=0;x<mImageWidth;x++)
    for(int y=0;y<mImageHeight;y++)
      mRealWorldCoords(x,y) = safeGetWorldCoords(x,y);

  /*
  //TEMPORARY DEBUGGING CODE, CLEAN UP WHEN FINISHED DEBUGGING
  const int xPos = 240;
  const int yPos = 150;

  generateMask(xPos,yPos);

  Image32 tmp;
  Image32 img(320,240);
  img.setAll(0);

  Image8Ptr pGray = mMaskImage(xPos,yPos).pMaskImage;
  Rectanglei rect = mMaskImage(xPos,yPos).maskRect;
  ColorConvert::convert(*pGray,PvImageProc::GRAY_SPACE,tmp,PvImageProc::RGB_SPACE);

  img.paste(tmp,rect.x0,rect.y0);
  img.rectangle(rect.x0,rect.y0,rect.x1,rect.y1,PV_RGB(255,0,0));

  
  img.save(aitSprintf("e:/work/tmp/z/%d_%d.png",xPos,yPos).c_str());
  */

  saveMasks(path); 
}

void 
PersonShapeEstimator::filterImage(Image8& img, 
                                  const Image8& srcImg, 
                                  Vector2i offset = Vector2i(0,0))
{  

  //TODO: checking rectangle bounds slows things down, 
  // do this someplace else in the future

  img.resize(srcImg.width(),srcImg.height());

  img.setAll(0);



  for (int y = 0; y < img.height(); y++)
  {
    for (int x = 0; x < img.width(); x++)
    {
      //TODO: make this separate function

      const int xOff = x + offset.x;
      const int yOff = y + offset.y;
      //TODO: assert that offset is not out of bounds

      assert(xOff >= 0);
      assert(yOff >= 0);
      assert(xOff < mMaskImage.width());
      assert(yOff < mMaskImage.height());

      assert(x >= 0 && x < srcImg.width());
      assert(y >= 0 && y < srcImg.height());

      //if we have a mask for this position
      if(srcImg(x,y) != 0)
      {
        if (mMaskImage(xOff,yOff).personPixels > 1)
        {
          assert(mMaskImage(xOff,yOff).pMaskImage.get());

          const Image8& maskImg = *mMaskImage(xOff,yOff).pMaskImage;
          const Rectanglei& rect = mMaskImage(xOff,yOff).maskRect;
          const int personPixels = mMaskImage(xOff,yOff).personPixels;


          int count = 0;

          int aMask(0),bMask(0);

          int bMin, bMax, aMin, aMax;

          bMin = rect.y0 - offset.y;
          bMax = rect.y1 - offset.y;

          aMin = rect.x0 - offset.x;
          aMax = rect.x1 - offset.x;

          if(bMin < 0)
          {
            bMask = -bMin;
            bMin = 0;
          }

          if(bMax > srcImg.height())
            bMax = srcImg.height();

          if(aMin < 0)
          {
            aMask = -aMin;
            aMin = 0;
          }

          if(aMax > srcImg.width())
            aMax = srcImg.width();

          int aMaskPerm = aMask;

          for (int b = bMin; b < bMax; b++)
          {
            for (int a = aMin; a < aMax; a++)
            {
              count += (maskImg(aMask,bMask) & srcImg(a,b));
              /*
              if (maskImg(aMask,bMask) == 255)
                count+=srcImg(a,b);
                */
              aMask++;
            }
            aMask = aMaskPerm;
            bMask++;
          } 

          const int val = round(count/(float)personPixels);
          assert(val >= 0);
          assert(val <= 255);

          img(x,y) = val;
        }
        else if (mMaskImage(xOff,yOff).personPixels == 1)
        {        
          img(x,y) = srcImg(x,y);
        }


      }
    }

  }  


}

void 
PersonShapeEstimator::filterImageOutline(Image8& img, 
                                         const Image8& srcImg, 
                                         const Image8& fullSegMask,
                                         Vector2i offset = Vector2i(0,0))
{  

  //TODO: checking rectangle bounds slows things down, 
  // do this someplace else in the future

  img.resize(srcImg.width(),srcImg.height());

  img.setAll(0);

  for (int y = 0; y < img.height(); y++)
  {
    for (int x = 0; x < img.width(); x++)
    {
      //TODO: make this separate function

      const int xOff = x + offset.x;
      const int yOff = y + offset.y;
      //TODO: assert that offset is not out of bounds

      assert(xOff >= 0);
      assert(yOff >= 0);
      assert(xOff < mMaskOutlineImage.width());
      assert(yOff < mMaskOutlineImage.height());

      assert(x >= 0 && x < srcImg.width());
      assert(y >= 0 && y < srcImg.height());

      //if we have a mask for this position

      if(fullSegMask(x,y) != 0)
      {
        if(mMaskOutlineImage(xOff,yOff).personPixels > 1)
        {

          assert(mMaskOutlineImage(xOff,yOff).pMaskImage.get());

          const Image8& maskImg = *mMaskOutlineImage(xOff,yOff).pMaskImage;
          const Rectanglei& rect = mMaskOutlineImage(xOff,yOff).maskRect;
          const int personPixels = mMaskOutlineImage(xOff,yOff).personPixels;

          int count = 0;

          int aMask(0),bMask(0);

          int bMin, bMax, aMin, aMax;

          bMin = rect.y0 - offset.y;
          bMax = rect.y1 - offset.y;

          aMin = rect.x0 - offset.x;
          aMax = rect.x1 - offset.x;

          if(bMin < 1)
          {
            bMask = -bMin + 1;
            bMin = 1;
          }

          if(bMax > (srcImg.height()-1))
            bMax = (srcImg.height()-1);

          if(aMin < 1)
          {
            aMask = -aMin + 1;
            aMin = 1;
          }

          if(aMax > (srcImg.width()-1))
            aMax = (srcImg.width()-1);

          int aMaskPerm = aMask;

          for (int b = bMin; b < bMax; b++)
          {
            for (int a = aMin; a < aMax; a++)
            {
              if(maskImg(aMask,bMask) != 0)
              {
                int neighborCount = srcImg(a-1,b-1) + srcImg(a-1,b) + srcImg(a-1,b+1) + srcImg(a,b+1) + srcImg(a+1,b+1) + srcImg(a+1,b) + srcImg(a+1,b-1) + srcImg(a,b-1) + srcImg(a,b);
                if(neighborCount>0)//srcImg(a,b) > 0 )
                  count+=255;
              }
              aMask++;
            }
            aMask = aMaskPerm;
            bMask++;
          }

          const int val = round(count/(float)personPixels);
          assert(val >= 0);
          assert(val <= 255);

          img(x,y) = val;

        }
        else if (mMaskOutlineImage(xOff,yOff).personPixels == 1)
        {        
          img(x,y) = srcImg(x,y);
        }

      }


    }
  }  


}

void 
PersonShapeEstimator::filterImageOutlineLowRes(Image8& img, 
                                         const Image8& srcImg, 
                                         const Image8& fullSegMask,
                                         Vector2i offset = Vector2i(0,0))
{  

  //TODO: checking rectangle bounds slows things down, 
  // do this someplace else in the future

  img.resize(srcImg.width(),srcImg.height());

  img.setAll(0);



  for (int y = 0; y < img.height(); y++)
  {
    for (int x = 0; x < img.width(); x++)
    {
      //TODO: make this separate function

      const int xOff = x + offset.x;
      const int yOff = y + offset.y;
      //TODO: assert that offset is not out of bounds

      assert(xOff >= 0);
      assert(yOff >= 0);
      assert(xOff < mMaskOutlineImage.width());
      assert(yOff < mMaskOutlineImage.height());

      assert(x >= 0 && x < srcImg.width());
      assert(y >= 0 && y < srcImg.height());

      //if we have a mask for this position
      if (mMaskOutlineImage(xOff,yOff).personPixels > 1 && fullSegMask(x,y) != 0)
      {
        assert(mMaskOutlineImage(xOff,yOff).pMaskImage.get());

        const Image8& maskImg = *mMaskOutlineImage(xOff,yOff).pMaskImage;
        const Rectanglei& rect = mMaskOutlineImage(xOff,yOff).maskRect;
        const int personPixels = mMaskOutlineImage(xOff,yOff).personPixels;

        //TEMP FOR DEBUGGING
        /*
        PVMSG("srcImg(x,y): %d\n", srcImg(x,y));
        PVMSG("personPixels: %d\n", personPixels);
        PVMSG("rect: %d %d %d %d\n", rect.x0, rect.y0, rect.x1, rect.y1);
        PVMSG("mask: %d %d\n", mask.width(), mask.height());

        Image32 tmpMask;
        ColorConvert::convert(mask,PvImageProc::GRAY_SPACE, tmpMask, 
        PvImageProc::RGB_SPACE);
        tmpMask.save("e:/work/tmp/mask.png");
        */

        // END TEMP


        int count = 0;


        for (int b = rect.y0 - offset.y; b < rect.y1 - offset.y; b++)
        {
          for (int a = rect.x0 - offset.x; a < rect.x1 - offset.x; a++)
          {
            //TODO: following checks unnecessary with proper cropping
            if (!(a >= 0 && a < srcImg.width()))
            {
              continue;
            }
            if (!(b >= 0 && b < srcImg.height()))
            {
              continue;
            }           

            assert(a >= 0 && a < srcImg.width());
            assert(b >= 0 && b < srcImg.height());

            assert(a-rect.x0+offset.x >= 0 
              && a-rect.x0+offset.x < maskImg.width());
            assert(b-rect.y0+offset.y >= 0 
              && b-rect.y0+offset.y < maskImg.height());

            /*if (srcImg(a,b) > 0 
            && maskImg(a-rect.x0 + offset.x,b-rect.y0 + offset.y) == 255)
            {
            count+=srcImg(a,b);                       
            }*/
            if(a>-1 && a<(srcImg.width()) && b>-1 && b<(srcImg.height()))
            {
              if(maskImg(a-rect.x0 + offset.x,b-rect.y0 + offset.y) > 0 && srcImg(a,b) > 0)
              {
                count+=255;
              }
            }
          }
        }             


        const int val = round(count/(float)personPixels);
        assert(val >= 0);
        assert(val <= 255);

        img(x,y) = val;
        //img(x,y) = 255;


      }
      else if (mMaskOutlineImage(xOff,yOff).personPixels == 1)
      {        
        img(x,y) = srcImg(x,y);
      }
      else
      {
        img(x,y) = 0;
      }
    }
  }  
}
//void 
//PersonShapeEstimator::filterImageOutline(Image8& img, 
//                                         const Image8& srcImg, 
//                                         Vector2i offset = Vector2i(0,0))
//{  
//
//  //TODO: checking rectangle bounds slows things down, 
//  // do this someplace else in the future
//
//  img.resize(srcImg.width(),srcImg.height());
//
//  img.setAll(0);
//
//
//
//  for (int y = 0; y < img.height(); y++)
//  {
//    for (int x = 0; x < img.width(); x++)
//    {
//      //TODO: make this separate function
//
//      const int xOff = x + offset.x;
//      const int yOff = y + offset.y;
//      //TODO: assert that offset is not out of bounds
//
//      assert(xOff >= 0);
//      assert(yOff >= 0);
//      assert(xOff < mMaskOutlineImage.width());
//      assert(yOff < mMaskOutlineImage.height());
//
//      assert(x >= 0 && x < srcImg.width());
//      assert(y >= 0 && y < srcImg.height());
//
//      //if we have a mask for this position
//      if (mMaskOutlineImage(xOff,yOff).personPixels > 1 && srcImg(x,y) != 0)
//      {
//        assert(mMaskOutlineImage(xOff,yOff).pMaskImage.get());
//
//        const Image8& maskImg = *mMaskOutlineImage(xOff,yOff).pMaskImage;
//        const Rectanglei& rect = mMaskOutlineImage(xOff,yOff).maskRect;
//        const int personPixels = mMaskOutlineImage(xOff,yOff).personPixels;
//
//        //TEMP FOR DEBUGGING
//        /*
//        PVMSG("srcImg(x,y): %d\n", srcImg(x,y));
//        PVMSG("personPixels: %d\n", personPixels);
//        PVMSG("rect: %d %d %d %d\n", rect.x0, rect.y0, rect.x1, rect.y1);
//        PVMSG("mask: %d %d\n", mask.width(), mask.height());
//
//        Image32 tmpMask;
//        ColorConvert::convert(mask,PvImageProc::GRAY_SPACE, tmpMask, 
//        PvImageProc::RGB_SPACE);
//        tmpMask.save("e:/work/tmp/mask.png");
//        */
//
//        // END TEMP
//
//
//        int count = 0;
//
//
//        for (int b = rect.y0 - offset.y; b < rect.y1 - offset.y; b++)
//        {
//          for (int a = rect.x0 - offset.x; a < rect.x1 - offset.x; a++)
//          {
//            //TODO: following checks unnecessary with proper cropping
//            if (!(a >= 0 && a < srcImg.width()))
//            {
//              continue;
//            }
//            if (!(b >= 0 && b < srcImg.height()))
//            {
//              continue;
//            }           
//
//            assert(a >= 0 && a < srcImg.width());
//            assert(b >= 0 && b < srcImg.height());
//
//            assert(a-rect.x0+offset.x >= 0 
//              && a-rect.x0+offset.x < maskImg.width());
//            assert(b-rect.y0+offset.y >= 0 
//              && b-rect.y0+offset.y < maskImg.height());
//
//            /*if (srcImg(a,b) > 0 
//              && maskImg(a-rect.x0 + offset.x,b-rect.y0 + offset.y) == 255)
//            {
//              count+=srcImg(a,b);                       
//            }*/
//            if(a>0 && a<(srcImg.width()-1) && b>0 && b<(srcImg.height()-1))
//            {
//              if(maskImg(a-rect.x0 + offset.x,b-rect.y0 + offset.y) > 0)
//              {
//                int neighborCount = srcImg(a-1,b-1) + srcImg(a-1,b) + srcImg(a-1,b+1) + srcImg(a,b+1) + srcImg(a+1,b+1) + srcImg(a+1,b) + srcImg(a+1,b-1) + srcImg(a,b-1) + srcImg(a,b);
//                if(neighborCount>0)
//                  count+=255;
//              }
//            }
//          }
//        }             
//
//
//        const int val = round(count/(float)personPixels);
//        assert(val >= 0);
//        assert(val <= 255);
//
//        img(x,y) = val;
//        //img(x,y) = 255;
//
//
//      }
//      else if (mMaskOutlineImage(xOff,yOff).personPixels == 1)
//      {        
//        img(x,y) = srcImg(x,y);
//      }
//      else
//      {
//        img(x,y) = 0;
//      }
//    }
//  }  
//}


//TODO:
void 
PersonShapeEstimator::destroyRenderingSurface()
{
  //windows specific code

  if (mHglrc)
  {
    wglDeleteContext(mHglrc); // Delete RC
    //PVMSG("wglDeleteContext(mHglrc)\n");
  }  

  if (mHdc)
  {
    SelectObject(mHdc, mSelectResult); // Remove bitmap from DC
    //PVMSG("SelectObject(mHdc,mSelectResults)\n");
  }

  if (mHbm)
  {
    DeleteObject(mHbm);  // Delete bitmap
    //PVMSG("DeleteObject(mHbm)\n");
  }

  if (mHdc)
  {
    DeleteDC(mHdc);  // Delete DC  
    //PVMSG("DeleteDC(mHdc)\n");
  }

  mHglrc = NULL;
  mHdc = NULL;
  mHbm = NULL;  
}

std::string 
PersonShapeEstimator::getFolderFromParameters()
{
  std::vector<std::string> parameters;
  parameters.push_back(mSettings.getString("arrayWidth"));
  parameters.push_back(mSettings.getString("arrayHeight"));

  parameters.push_back(mSettings.getString("personRadius"));
  parameters.push_back(mSettings.getString("personHeight"));

  parameters.push_back(mSettings.getString("Camera/height"));
  parameters.push_back(mSettings.getString("Camera/rotX"));
  parameters.push_back(mSettings.getString("Camera/focalLength"));
  parameters.push_back(mSettings.getString("Camera/ccdWidth"));
  parameters.push_back(mSettings.getString("Camera/ccdHeight"));

  parameters.push_back(
    mSettings.getString("Camera/distortParameters"));

  std::string result = join(parameters, "_");

  return result;
}

}; // namespace vision

}; // namespace ait

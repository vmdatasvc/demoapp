/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#pragma warning(disable:4503)
#endif

//#include <cv.h>

#include <legacy/pv/ait/Image.hpp>
#include "legacy/vision_tools/ColorConvert.hpp"
//#include <image_proc.hpp>
#include <legacy/pv/ait/Rectangle.hpp>


#include <legacy/vision_tools/PolygonGetMask.hpp>
#include <legacy/vision/ImageAcquireMod.hpp>

#include <legacy/vision_blob/ForegroundSegmentMod.hpp>

#include <fstream>

#include "legacy/vision_blob/MotionDetectionMod.hpp"

namespace ait 
{

namespace vision
{

MotionDetectionMod::MotionDetectionMod()
: VisionMod(getModuleStaticName())
{
}

MotionDetectionMod::~MotionDetectionMod()
{
}

//void
//MotionDetectionMod::initExecutionGraph(std::list<VisionModPtr>& srcModules)
//{
//  RealTimeMod::initExecutionGraph(srcModules);
//
//  // Add any source module to the given list here.
//  //getModule("ForegroundSegmentMod",mpForegroundSegmentMod);
//  //srcModules.push_back(mpForegroundSegmentMod);
//}
void MotionDetectionMod::initExecution()
{
	VisionMod::initExecution();
}

void 
MotionDetectionMod::init()
{
//  RealTimeMod:init();
  VisionMod::init();


  //TODO: MAKE THIS SETTING CONFIGURABLE
  mSaveEvents = false;  

  mEventChannels = mSettings.getIntList("eventChannelId", "", false);

  mPrintMotionLevels = mSettings.getBool("printMotionLevels [bool]", false);

  mMotionSensitivity = mSettings.getFloat("motionSensitivity", 5.0);

  mExtensionTime = mSettings.getFloat("extensionTime [sec]", 5.0);
 
  /*
  if (mEventChannels.size() == 0)
  {
    PvUtil::exitError("Error: 'MotionDetectionMod/eventChannelId' - missing event channel setting\n");
  }
  */

  Vector2f scaleFactor = mSettings.getCoord2f("scaleFactor", Vector2f(1.0,1.0));
    
  Image32Ptr pFrame = getImageAcquireModule()->getCurrentFramePtr();

  
  getImageAcquireModule()->setVisualizationFrameSize(Vector2i(pFrame->width()*2,pFrame->height()*2));


  mWidth = pFrame->width();
  mHeight = pFrame->height();

  mRoiRect.set(0,0,mWidth,mHeight);



  //steal roi from foreground seg mod
  /*
  ForegroundSegmentMod* pForegroundSegmentMod = (ForegroundSegmentMod*)getParentModule();  
  mRoiRect = pForegroundSegmentMod->getRoiRectangle();
  */


  //GRAB ROI FROM FSM settings
  
  SimplePolygon roiPoly;

  Settings fsmSettings("VisionMods/" + getExecutionPathName() + "/ForegroundSegmentMod");

  /*
  PVMSG("PATH: %s\n", mSettings.getPath().c_str());
  PVMSG("PATH: %s\n", fsmSettings.getPath().c_str());
  PVMSG("POLY: %s\n", fsmSettings.getString("roiPolygon","NOTHING").c_str());  
  */

  convert(fsmSettings.getString("roiPolygon",""), roiPoly);

  PVASSERT(roiPoly.size() > 0);
  mRoiRect.set(roiPoly[0].x, roiPoly[0].y, roiPoly[0].x, roiPoly[0].y);
  
  SimplePolygon::iterator iCoord;

  for (iCoord = roiPoly.begin(); iCoord != roiPoly.end(); iCoord++)
  {
    mRoiRect.set(
      std::min(mRoiRect.x0, (int)iCoord->x),
      std::min(mRoiRect.y0, (int)iCoord->y),
      std::max(mRoiRect.x1, (int)iCoord->x),
      std::max(mRoiRect.y1, (int)iCoord->y));    
  }
  



  mpFilteredMotion.reset(new Image8());

  mpLastMotion.reset(new Image<double>());

  setScaleAndRoi(scaleFactor, mRoiRect);  

  mMaxHistoryTime = 60.0;

  /*
  int w = round(mWidth * mScaleFactor.x);
  int h = round(mHeight * mScaleFactor.y);
  
  mPreviousFrame.resize(w,h);
  mDifferenceFrame.resize(w,h);
  mPreviousDiff.resize(w,h);
  mAccumulationFrame.resize(w,h);
  //mMotionMask.resize(w,h);
  

  mLastMotion.resize(w,h);
  mLastMotion.setAll(0.0);

  mLastMotionImage.resize(w,h);
  mLastMotionImage.setAll(0);  
  

  mAccumulationFrame.setAll(0);
  //mMotionMask.setAll(0);
  */


  std::list<SimplePolygon> polyList = convertList(mSettings.getString("eventsPolygon",""));    
  std::list<SimplePolygon>::const_iterator iPoly;
  std::list<int>::const_iterator iEvtChan = mEventChannels.begin();

  Image8 currentMask;

  //currentMask.resize(mWidth,mHeight);

  for (iPoly = polyList.begin(); iPoly != polyList.end(); iPoly++, iEvtChan++)  
  {
    if ((*iPoly).size() < 3)
      PvUtil::exitError("Error: 'MotionDetectionMod/eventsPolygon' - invalid number of points\n");
    mEventPolygon[*iEvtChan] = *iPoly;      

    Rectanglef polyRect(iPoly->front().x, iPoly->front().y, iPoly->front().x, iPoly->front().y);

    SimplePolygon::const_iterator iCoord;
    for (iCoord = iPoly->begin(); iCoord != iPoly->end(); iCoord++)
    { 
      polyRect.set(
        std::min(polyRect.x0,iCoord->x),
        std::min(polyRect.y0,iCoord->y),
        std::max(polyRect.x1,iCoord->x),
        std::max(polyRect.y1,iCoord->y));      
    }    
    
    currentMask.resize(polyRect.width(), polyRect.height());
    currentMask.setAll(0);

    getMask(*iPoly, polyRect, currentMask);   

    //currentMask.save("csa_test.pgm");


    //currentMask.rescale(mWidth * mScaleFactor.x, mHeight * mScaleFactor.y);
    
    polyRect.x0 *= mScaleFactor.x;
    polyRect.x1 *= mScaleFactor.x;;
    polyRect.y0 *= mScaleFactor.y;
    polyRect.y1 *= mScaleFactor.y;

    currentMask.rescale((int)polyRect.width(), (int)polyRect.height());

    //currentMask.save("mask.pgm");

    mEventMask[*iEvtChan] = currentMask;
    mEventRect[*iEvtChan] = polyRect;  

    //PVMSG("RECT %d: %d %d %d %d\n", *iEvtChan, (int)polyRect.x0, (int)polyRect.y0, (int)polyRect.x1, (int)polyRect.y1);

    //PVMSG("RECT: %d %d\n", (int)polyRect.width(), (int)polyRect.height());
    //PVMSG("MASK: %d %d\n", currentMask.width(), currentMask.height());
    

    //PVMSG("POLY %d: (%d %d %d %d %d %d %d %d)\n", *iEvtChan
    
  }
  
    
  float sensitivity = mSettings.getFloat("sensitivity", 0.95f);

  mDiffThreshold = (1.0 - sensitivity) * 255;
  
  mFirstFrame = true;    


  if (mSaveEvents)
  {
    mOutFile.open("motionevents.txt");
  }

  /*
  mpPrevImg = cvCreateMat(h,w, CV_8UC1);
  mpCurrImg = cvCreateMat(h,w, CV_8UC1);  
  mpVelX = cvCreateMat(h,w, CV_32FC1);
  mpVelY = cvCreateMat(h,w, CV_32FC1);
  */

  mAccumulationFrame.setAll(0);
  mpFilteredMotion->setAll(0);
  mPreviousFrame.setAll(0);
  mDifferenceFrame.setAll(0);  
}

void
MotionDetectionMod::finish()
{  

  if (mSaveEvents)
    mOutFile.close();

//  mVmsPacket.finish();
//  RealTimeMod::finish();
  VisionMod::finish();
}

//
// OPERATIONS
//

 
Image8Ptr 
MotionDetectionMod::getRoiFrame(Image32Ptr pImg)
{
  Image32Ptr pResized;

  if (mRoiRect == Rectanglei(0,0,pImg->width(),pImg->height()))  
    pResized = pImg;  
  else
  {
    pResized.reset(new Image32(mRoiRect.width(),mRoiRect.height()));
    pResized->crop(*pImg,mRoiRect.x0,mRoiRect.y0,
      mRoiRect.width(),mRoiRect.height());
  }

  Image8Ptr pGrayFrame(new Image8(pResized->width(),pResized->height()));
  ColorConvert::convert(*pResized,PvImageProc::RGB_SPACE,*pGrayFrame,PvImageProc::GRAY_SPACE);

  pGrayFrame->rescale(
    (int)(mRoiRect.width()*mScaleFactor.x),
    (int)(mRoiRect.height()*mScaleFactor.y),
    RESCALE_SKIP); 

  return pGrayFrame;
}

void 
MotionDetectionMod::process()
{   
  Image32Ptr pFrame = getImageAcquireModule()->getCurrentFramePtr();

  Image8Ptr pGrayFrame = getRoiFrame(pFrame);  

  if (mFirstFrame)
  {
    mFirstFrame = false;
    mPreviousFrame = *pGrayFrame;
    return;
  }  
  
  //removes some of the noise
  
  //image_proc::smoothImage(grayFrame, image_proc::SMOOTH_MEDIAN, 3);    
  
  //image_proc::smoothImage(grayFrame, image_proc::SMOOTH_MEAN, 3);
 

  std::list<Image8>::const_iterator iImg;

  for (int y = 0; y < pGrayFrame->height(); y++)
    for (int x = 0; x < pGrayFrame->width(); x++)
    {
      const float accWeight = 0.5;         
     
      mAccumulationFrame(x,y) = (1.0 - accWeight) * mAccumulationFrame(x,y) + 
        accWeight * abs((*pGrayFrame)(x,y) - mPreviousFrame(x,y));        
      
      if (mAccumulationFrame(x,y) > mDiffThreshold ) //TODO: MAKE THIS SETTING CONFIGURABLE
        mDifferenceFrame(x,y) = 255;
      else
        mDifferenceFrame(x,y) = std::max(0,mDifferenceFrame(x,y) - 20);//20); //TODO: MAKE DROPOFF CONFIGURABLE      

      (*mpFilteredMotion)(x,y) = mDifferenceFrame(x,y) & mPreviousDiff(x,y);
    } 

  mPreviousFrame = *pGrayFrame;
  mPreviousDiff = mDifferenceFrame; 

  

  //Update the history of motion
  //TODO: FRAME RATE IS NOT CONSTANT, INCREMENT SHOULD NOT BE CONSTANT

  for (int y = 0; y < mpFilteredMotion->height(); y++)
    for (int x = 0; x < mpFilteredMotion->width(); x++)
    {
      if ((*mpFilteredMotion)(x,y) > 0)
        (*mpLastMotion)(x,y) = 0.0;
      else
        (*mpLastMotion)(x,y) += 1.0/getFramesPerSecond();     
    }
  //

  

  //Now filter the mFinalDiff
  //TODO: FILTER BY BLOB SIZE LATER
  
  Image8 temp = *mpFilteredMotion;  
  mpFilteredMotion->setAll(0);

  Rectanglei box;
  int boxSize = 5;
  float boxArea = (float)(boxSize*boxSize);  
  
  for (int y = 0; y < mpFilteredMotion->height(); y++)
    for (int x = 0; x < mpFilteredMotion->width(); x++)
    {

      if (temp(x,y) == 0)
      {
        (*mpFilteredMotion)(x,y) = 0;
        continue;
      }

      box.x0 = std::max(0, round(x - boxSize/2.0));
      box.x1 = std::min((int)mpFilteredMotion->width() - 1, round(x + boxSize/2.0));
      box.y0 = std::max(0, round(y - boxSize/2.0));
      box.y1 = std::min((int)mpFilteredMotion->height() - 1, round(y + boxSize/2.0));

      PVASSERT(box.width() * box.height() != 0);

      int count = 0;

      for (int by = box.y0; by < box.y1; by++)
        for (int bx = box.x0; bx < box.x1; bx++)
        {
          count += temp(bx,by);
        }
      
      //TODO: DON'T DIVIDE BY BOX AREA, DIVIDE BY NO. OF WHITE MASK PIXELS INSTEAD
      float weight = std::min(255.0f, (float)count/boxArea);
      weight /= 255.0f;

      //TODO: PUT THRESHOLD IN SETTINGS     
      
      if (weight > 0.1)
        (*mpFilteredMotion)(x,y) = round(weight * 255);
    }
    
  


  //Now check for events
    
  std::list<int>::const_iterator iEvtChan;
  for (iEvtChan = mEventChannels.begin(); iEvtChan != mEventChannels.end(); iEvtChan++)
  {
    
    float count = 0.0;    

    int yStart = (int)mEventRect[*iEvtChan].y0;
    int yEnd = (int)mEventRect[*iEvtChan].y0 + mEventMask[*iEvtChan].height();
    int xStart = (int)mEventRect[*iEvtChan].x0;
    int xEnd = (int)mEventRect[*iEvtChan].x0 + mEventMask[*iEvtChan].width();

    const Image8* pMask = &mEventMask[*iEvtChan];
        
    
    for (int y = yStart; y < yEnd; y++)
      for (int x = xStart; x < xEnd; x++)
      {        
        //PVMSG("RECT: %d %d\n", (int)mEventRect[*iEvtChan].width(), (int)mEventRect[*iEvtChan].height());
        //PVMSG("MASK: %d %d\n", mEventMask[*iEvtChan].width(), mEventMask[*iEvtChan].height());
        //PVMSG("CHECK %d %d    %d %d\n", x - (int)mEventRect[*iEvtChan].x0, y - (int)mEventRect[*iEvtChan].y0, mEventMask[*iEvtChan].width(), mEventMask[*iEvtChan].height());        
        
        if ((*pMask)(x - xStart, y - yStart) == 255)
        {
          count += (float)(*mpFilteredMotion)(x,y);          
        }       
        
      }    

    

    //PVASSERT(mEventRect[*iEvtChan].width() * mEventRect[*iEvtChan].height() != 0);

    
    //count /= (float)(mEventRect[*iEvtChan].width() * mEventRect[*iEvtChan].height());
    
    count /= 255.0f;

    //PVMSG("COUNT %d: %f\n", *iEvtChan, count);      
    //TODO: MAKE THRESHOLD A CONFIGURABLE SETTING

    if (count > 0.0f && mPrintMotionLevels)
      PVMSG("MOTION: %d %f / %f\n", *iEvtChan, count, mMotionSensitivity);

    //TODO: TEMP
    mMotionScore = count;
    //END TEMP
    //
    /*
    std::ofstream outfile("C:/tmp/motion.txt", std::ios::app);
    outfile << aitSprintf("%.6f", count) << std::endl; 
    outfile.close();
    */
    //

    if (count > mMotionSensitivity)//5.0f)
    {      
      if (mEvent[*iEvtChan].isActive)
        mEvent[*iEvtChan].endTime = getCurrentFrameNumber();
      else
      {
        mEvent[*iEvtChan].startTime = getCurrentFrameNumber();
        mEvent[*iEvtChan].endTime = getCurrentFrameNumber();
        mEvent[*iEvtChan].isActive = true;
      }      
    }
    else
    {
      if (mEvent[*iEvtChan].isActive)
      {
        //TODO: MAKE INTERVAL A CONFIGURABLE SETTING
        if (getCurrentFrameNumber() - mEvent[*iEvtChan].endTime < (int)(mExtensionTime)*getFramesPerSecond())
        {
          //currently do nothing
        }
        else
        {
          //minimum duration of 1
          //TODO: mVmsPacket.beginEvent(mEvent[*iEvtChan].startTime, std::max(mEvent[*iEvtChan].endTime - mEvent[*iEvtChan].startTime, (Int64)1), *iEvtChan);
//          mVmsPacket.endEvent();
          mEvent[*iEvtChan].startTime = -1;
          mEvent[*iEvtChan].endTime = -1;
          mEvent[*iEvtChan].isActive = false;
        }
      }
    }
    
  }  

  
}

float
MotionDetectionMod::estimateNoise()
{
  /*
  std::list<Image8>::const_iterator iDiff;

  for (int y = 0; y < mDifferenceFrame.height(); y++)
    for (int x = 0; x < mDifferenceFrame.width(); x++)
    {
      
      for (iDiff = mDiffHistory.begin(); iDiff != mDiffHistory.end(); iDiff++)
      {
      }

    }
  */
  return 0.0;
}

void
MotionDetectionMod::setScaleAndRoi(Vector2f scaleFactor, Rectanglei roi)
{

  //Avoid computation if scales are identical
  //if (scaleFactor == mScaleFactor)
  //  return;

  //TODO: WE WILL HAVE TO RESCALE ALL EVENTS AS WELL

  mScaleFactor = scaleFactor;
  int w = (int)(mRoiRect.width() * mScaleFactor.x);
  int h = (int)(mRoiRect.height() * mScaleFactor.y);

  mpFilteredMotion->resize(w,h);
  
  mPreviousFrame.resize(w,h);
  mDifferenceFrame.resize(w,h);
  mPreviousDiff.resize(w,h);
  mAccumulationFrame.resize(w,h);
 
  mpLastMotion->resize(w,h);
  mpLastMotion->setAll(0.0);

  
  mDifferenceFrame.setAll(0.0);
  mPreviousDiff.setAll(0.0);
  mpFilteredMotion->setAll(0.0);
  mLastMotionImage.setAll(0);  
  mAccumulationFrame.setAll(0);
  mPreviousFrame.setAll(0);
  
}

void 
MotionDetectionMod::visualize(Image32& img)
{   

  //TODO: TEMP  
  ColorConvert::convert(*mpFilteredMotion,PvImageProc::GRAY_SPACE,mVisDiff,PvImageProc::RGB_SPACE); 
  mVisDiff.rescale((int)(mVisDiff.width()/mScaleFactor.x), (int)(mVisDiff.height()/mScaleFactor.y));

  std::list<int>::const_iterator iEvtChan;

  for (iEvtChan = mEventChannels.begin(); iEvtChan != mEventChannels.end(); iEvtChan++)
  {    

    //
    //img.rectangle(mEventRect[*iEvtChan].x0,mEventRect[*iEvtChan].y0,mEventRect[*iEvtChan].x1,mEventRect[*iEvtChan].y1, PV_RGB(255,255,0) );
    //Image32 tempMask;
    //ColorConvert::convert(mEventMask[*iEvtChan], PvImageProc::GRAY_SPACE, tempMask, PvImageProc::RGB_SPACE);

    //img.paste(tempMask, mEventRect[*iEvtChan].x0,mEventRect[*iEvtChan].y0);
    //   

    for (int i = 1; i < mEventPolygon[*iEvtChan].size(); i++)
    {
      unsigned int col = 0;
      if (mEvent[*iEvtChan].isActive)
          col = PV_RGB(0,255,0);
        else
          col = PV_RGB(255,0,0);

      img.line(
        mEventPolygon[*iEvtChan][i-1].x,mEventPolygon[*iEvtChan][i-1].y,
        mEventPolygon[*iEvtChan][i].x,mEventPolygon[*iEvtChan][i].y,
        col);
      /*
      //TODO: TEMP DRAW LINES THICKER
      img.line(
        mEventPolygon[*iEvtChan][i-1].x+1,mEventPolygon[*iEvtChan][i-1].y,
        mEventPolygon[*iEvtChan][i].x+1,mEventPolygon[*iEvtChan][i].y,
        col);
      img.line(
        mEventPolygon[*iEvtChan][i-1].x-1,mEventPolygon[*iEvtChan][i-1].y,
        mEventPolygon[*iEvtChan][i].x-1,mEventPolygon[*iEvtChan][i].y,
        col);
      img.line(
        mEventPolygon[*iEvtChan][i-1].x,mEventPolygon[*iEvtChan][i-1].y+1,
        mEventPolygon[*iEvtChan][i].x,mEventPolygon[*iEvtChan][i].y+1,
        col);
      img.line(
        mEventPolygon[*iEvtChan][i-1].x,mEventPolygon[*iEvtChan][i-1].y-1,
        mEventPolygon[*iEvtChan][i].x,mEventPolygon[*iEvtChan][i].y-1,
        col);
      //END TEMP
      */

      //TODO: TEMP: DRAW SAME LINE IN RIGHT IMAGE
      mVisDiff.line(
        mEventPolygon[*iEvtChan][i-1].x - mRoiRect.x0,
        mEventPolygon[*iEvtChan][i-1].y - mRoiRect.y0,
        mEventPolygon[*iEvtChan][i].x - mRoiRect.x0,
        mEventPolygon[*iEvtChan][i].y - mRoiRect.y0,
        col);     

      /*
      //TODO: TEMP DRAW LINES THICKER
      mVisDiff.line(
        mEventPolygon[*iEvtChan][i-1].x+1,mEventPolygon[*iEvtChan][i-1].y,
        mEventPolygon[*iEvtChan][i].x+1,mEventPolygon[*iEvtChan][i].y,
        col);   
      mVisDiff.line(
        mEventPolygon[*iEvtChan][i-1].x-1,mEventPolygon[*iEvtChan][i-1].y,
        mEventPolygon[*iEvtChan][i].x-1,mEventPolygon[*iEvtChan][i].y,
        col);   
      mVisDiff.line(
        mEventPolygon[*iEvtChan][i-1].x,mEventPolygon[*iEvtChan][i-1].y+1,
        mEventPolygon[*iEvtChan][i].x,mEventPolygon[*iEvtChan][i].y+1,
        col);   
      mVisDiff.line(
        mEventPolygon[*iEvtChan][i-1].x,mEventPolygon[*iEvtChan][i-1].y-1,
        mEventPolygon[*iEvtChan][i].x,mEventPolygon[*iEvtChan][i].y-1,
        col);   
      //END TEMP
      */
    }
  }    
  

  
  mLastMotionImage.resize(mpLastMotion->width(), mpLastMotion->height());
  
  for (int y = 0; y < mpLastMotion->height(); y++)
    for (int x = 0; x < mpLastMotion->width(); x++)
    {
      float pct = std::min((float)(*mpLastMotion)(x,y)/60.0f, 1.0f);
      mLastMotionImage(x,y) = PV_RGB((int)(pct*255),0,0);      
    }
  mLastMotionImage.rescale((int)(mpLastMotion->width()/mScaleFactor.x), (int)(mpLastMotion->height()/mScaleFactor.y));

  //img.paste(mLastMotionImage, mRoiRect.x0, mHeight + mRoiRect.y0);
  

  //PVMSG("LM: %d %d\n", mLastMotion.width(), mLastMotion.height());
  //PVMSG("LMI: %d %d\n", mLastMotionImage.width(), mLastMotionImage.height());  

  img.paste(mVisDiff,mWidth + mRoiRect.x0, mRoiRect.y0);

  /*
  Image32 tempMask;
  ColorConvert::convert(mEventMask[0], PvImageProc::GRAY_SPACE, tempMask, PvImageProc::RGB_SPACE);
  img.paste(tempMask, mEventRect[0].x0, mEventRect[0].y0);

  img.rectangle(mEventRect[0].x0, mEventRect[0].y0, mEventRect[0].x1, mEventRect[0].y1, PV_RGB(255,255,0));
  */ 
}


//
// ACCESS
//

//
// INQUIRY
//

}; // namespace vision

}; // namespace ait


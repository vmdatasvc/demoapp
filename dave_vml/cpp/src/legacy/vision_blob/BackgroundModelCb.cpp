/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#pragma warning(disable:4503)
#endif

#include <ColorConvert.hpp>

#include <BackgroundModelCb.hpp>

//#include <iostream>

namespace ait 
{

namespace vision
{

/*
void printCodebook(const std::list<BackgroundModelCb::Codeword>& cwList, double time)
{
  std::list<BackgroundModelCb::Codeword>::const_iterator iCw;

  int count = 1;

  std::cout << aitSprintf("TIME: %.2f", time) << std::endl;

  for (iCw = cwList.begin(); iCw != cwList.end(); iCw++)
  {
    double r,g,b;
    r = iCw->getRed();
    g = iCw->getGreen();
    b = iCw->getBlue();

    std::cout << aitSprintf("Codeword %d of %d",count,cwList.size()) << std::endl;
    std::cout << aitSprintf("\tRGB: %.2f,%.2f,%.2f", r,g,b) << std::endl; 
    std::cout << aitSprintf("\tbrightness: [%.2f,%.2f]", iCw->getMinBrightness(),iCw->getMaxBrightness()) << std::endl; 
    std::cout << aitSprintf("\tfreq: %d", iCw->getFrequency()) << std::endl; 
    std::cout << aitSprintf("\tnrl: %.2f", iCw->getNegRunLength()) << std::endl; 
    std::cout << aitSprintf("\taccess: %.2f -> %.2f", iCw->getFirstAccessTime(), iCw->getLastAccessTime()) << std::endl; 
    count++;
  }
}
*/


BackgroundModelCb::Codeword::Codeword(unsigned int r, unsigned int g, 
                                      unsigned int b, double brightness, 
                                      double startTime, double currentTime)
{ 

  assert(brightness >= 0.0 && brightness <= 442.0);
  assert(startTime >= 0.0);
  assert(currentTime >= startTime);

  assert(r >= 0 && r <= 255);
  assert(g >= 0 && g <= 255);
  assert(b >= 0 && b <= 255);  

  mRed = (double)r;
  mGreen = (double)g;
  mBlue = (double)b;

  mMinBrightness = brightness;
  mMaxBrightness = brightness;

  mFrequency = 1;

  mMaxNegRunLength = currentTime - startTime;
  mFirstAccessTime = currentTime;
  mLastAccessTime = currentTime;    
}

void 
BackgroundModelCb::Codeword::wrapNegRunLength(double startTime, double endTime)
{
  assert(startTime <= endTime);

  mMaxNegRunLength = std::max(mMaxNegRunLength, endTime - mLastAccessTime + mFirstAccessTime - startTime);
}

void
BackgroundModelCb::Codeword::update(unsigned int r, unsigned int g, 
                                    unsigned int b, double brightness, 
                                    double time)
{

  assert(r >= 0 && r <= 255);
  assert(g >= 0 && g <= 255);
  assert(b >= 0 && b <= 255);
  assert(brightness >= 0.0 && brightness <= 442);

  mRed = (mFrequency*mRed + r)/(mFrequency + 1);
  mGreen = (mFrequency*mGreen + g)/(mFrequency + 1);
  mBlue = (mFrequency*mBlue + b)/(mFrequency + 1);


  mMinBrightness = std::min(mMinBrightness, brightness);
  mMaxBrightness = std::max(mMaxBrightness, brightness);
  mFrequency += 1;

  mMaxNegRunLength = std::max(mMaxNegRunLength, time - mLastAccessTime);

  mLastAccessTime = time;

}

BackgroundModelCb::BackgroundModelCb(const std::string& settingsPath)
{
  mSettings.changePath(settingsPath);

  mLearningStatus = NOT_STARTED;
  mStartLearnTime = 0.0;
  mLearnedOnce = false;

  //Codebook comparison parameters  
  mColorThresh = mSettings.getDouble("colorThresh");
  mAlphaScale = mSettings.getDouble("alphaScale");
  mBetaScale = mSettings.getDouble("betaScale");  

  //Learning parameters
  mStartLearnDuration = mSettings.getDouble("startLearnDuration");
  mCacheRemoveThresh = mSettings.getDouble("cacheRemoveTime");
  mCachePromoteThresh = mSettings.getDouble("cachePromoteTime");
  mModelRemoveThresh = mSettings.getDouble("modelRemoveTime");
  mStartLearnMaxNrlFactor = mSettings.getDouble("startLearnMaxNrlFactor");

  assert(mColorThresh >= 0.0);
  assert(mAlphaScale >= 0.0 && mAlphaScale < 1.0);
  assert(mBetaScale > 1.0);
  assert(mStartLearnDuration > 0.0);
  assert(mCacheRemoveThresh > 0.0);
  assert(mCachePromoteThresh > 0.0);
  assert(mModelRemoveThresh > 0.0);
  assert(mStartLearnMaxNrlFactor >= 0 && mStartLearnMaxNrlFactor <= 1.0);
}

void 
BackgroundModelCb::learn(const Image32& img, const Image8& mask, double time)
{

  //Check to see if we learned once, so that the image sizes are set
  if (!mLearnedOnce)
  {
    mCodebook.resize(img.width(),img.height());
    mCache.resize(img.width(), img.height());
    mLearnedOnce = true;
  }
  else
  {
    assert(img.width() == mCodebook.width());
    assert(img.height() == mCodebook.height());
    assert(mask.width() == mCodebook.width());
    assert(mask.height() == mCodebook.height());
  }

  // TEMPORARY TESTING
  /*
  printCodebook(mCodebook(61,53),time);

  unsigned int r = RED(img(61,53));
  unsigned int g = GREEN(img(61,53));
  unsigned int b = BLUE(img(61,53));
  double brightness = sqrt((double)(r*r + g*g + b*b));   

  std::cout << aitSprintf("PIXEL VAL: %d,%d,%d,%.2f",r,g,b,brightness) << std::endl;
  */
  // END TEMPRARY TESTING


  if (mLearningStatus == NOT_STARTED)
  {
    PVMSG("STARTED LEARNING, DURATION: %f\n", mStartLearnDuration);

    //Perform codebook generation and initialize start time
    mStartLearnTime = time;
    constructInitialCodebook(img,mask,time);   
    mLearningStatus = IN_PROGRESS;
  }
  else if (mLearningStatus == IN_PROGRESS)
  {
    //Continue w/ codebook construction
    constructInitialCodebook(img,mask,time);
    if (time > mStartLearnTime + mStartLearnDuration)
    {
      mLearningStatus = FINISHED;
      PVMSG("LEARNING FINISHED\n");
      finishedInitialCodebook(img,mask,time);      
    }
  }
  else if (mLearningStatus == FINISHED)
  {
    //learn via layered method
    //layeredLearning(img,mask,time);
  }
}

void 
BackgroundModelCb::layeredLearning(const Image32& img, const Image8& mask, double currentTime)
{
  assert(mLearningStatus == FINISHED); 

  assert(img.width() == mCodebook.width());
  assert(img.height() == mCodebook.height());
  assert(mask.width() == mCodebook.width());
  assert(mask.height() == mCodebook.height());

  for (int y = 0; y < img.height(); y++)
  {
    for (int x = 0; x < img.width(); x++)
    {
      // Only perform operation on pixels that are not masked
      if (mask(x,y) != 255)
        continue;
      else
      {
        std::list<Codeword>& cwMain = mCodebook(x,y);
        std::list<Codeword>& cwCache = mCache(x,y);
                
        unsigned int r = RED(img(x,y));
        unsigned int g = GREEN(img(x,y));
        unsigned int b = BLUE(img(x,y));
        double brightness = sqrt((double)(r*r + g*g + b*b));

        //Check for codeword match in main model
        bool updatedMain = matchAndUpdate(r,g,b,brightness,cwMain,currentTime);        

        //no match was found in the main model, so check the cache
        if (!updatedMain)
        {
          bool updatedCache = matchAndUpdate(r,g,b,brightness,mCache(x,y),currentTime);          
          //if we did not find a match and update in the cache, create a new codeword
          if (!updatedCache)
          {
            Codeword newCw(r,g,b,brightness,currentTime,currentTime);
            mCache(x,y).push_back(newCw);
            //PVMSG("New CACHE codeword created, total: %d\n", mCache(x,y).size());
          }          
        }        
      }
    }
  }
}

bool 
BackgroundModelCb::matchAndUpdate(unsigned int r, unsigned int g, unsigned int b, double brightness, 
                                  std::list<Codeword>& cwList, double currentTime)
{
  assert(r >=0 && r <= 255);
  assert(g >=0 && g <= 255);
  assert(b >=0 && b <= 255);
  assert(brightness >= 0.0 && brightness <= 442);


  bool found = false;

  //find if existing codeword matches, update if found
   std::list<Codeword>::iterator iCw;     

   for (iCw = cwList.begin(); iCw != cwList.end(); iCw++)
   {          
     if (matchColor(r,g,b,*iCw) && matchBrightness(brightness, *iCw))
     {
       iCw->update(r,g,b,brightness,currentTime);
       found = true;
       break;
     }           
   }

   return found;
}

void 
BackgroundModelCb::finishedInitialCodebook(const Image32& img, const Image8& mask, double currentTime)
{
  assert(mLearningStatus == FINISHED);
  assert(img.width() == mCodebook.width());
  assert(img.height() == mCodebook.height());
  assert(mask.width() == mCodebook.width());
  assert(mask.height() == mCodebook.height());

  for (int y = 0; y < img.height(); y++)
  {
    for (int x = 0; x < img.width(); x++)
    {

      if (mask(x,y) != 255)
        continue;
      else
      {
        //ORIGINAL CODE BELOW
        std::list<Codeword>& cwList = mCodebook(x,y);

        std::list<Codeword>::iterator iCw;

        for (iCw = cwList.begin(); iCw != cwList.end();)
        {
          iCw->wrapNegRunLength(mStartLearnTime, currentTime);

          if (iCw->getNegRunLength() > mStartLearnMaxNrlFactor * mStartLearnDuration)
          {
//            PVMSG("ERASING CODEWORD(%d,%d): %d remaining, last seen %f\n", x,y,cwList.size()-1, iCw->getLastAccessTime());
            iCw = cwList.erase(iCw);
          }
          else
          {
            iCw++;
          }
        }
        
        //TEMPORARY HACK, REMOVE ALL BUT THE TOP CODEWORD

        double minVal = 99999;
        bool firstLoop = true;
        Codeword bestCw;

        for (iCw = cwList.begin(); iCw != cwList.end(); iCw++)
        {
          double val = iCw->getNegRunLength();
          if (firstLoop)
          {
            minVal = val;
            bestCw = *iCw;
            firstLoop = false;
          }
          else
          {
            if (val < minVal)
            {
              minVal = val;
              bestCw = *iCw;
            }
          }
        }

        if (cwList.size() > 1)
        {
          cwList.clear();
          cwList.push_back(bestCw);
        }
        
      }
    }
  }
}

void
BackgroundModelCb::constructInitialCodebook(const Image32& img, const Image8& mask, double time)
{
  assert(mLearningStatus != FINISHED);

  assert(img.width() == mCodebook.width());
  assert(img.height() == mCodebook.height());
  assert(mask.width() == mCodebook.width());
  assert(mask.height() == mCodebook.height());

  for (int y = 0; y < img.height(); y++)
  {
    for (int x = 0; x < img.width(); x++)
    {
      // Only perform operation on pixels that are not masked
      if (mask(x,y) != 255)
        continue;
      else
      {

        unsigned int r = RED(img(x,y));
        unsigned int g = GREEN(img(x,y));
        unsigned int b = BLUE(img(x,y));
        double brightness = sqrt((double)(r*r + g*g + b*b));

        bool updated = matchAndUpdate(r,g,b,brightness,mCodebook(x,y),time);
           
        //if a matching codeword was not found and updated...
        if (!updated)
        {
          //create new codeword
          Codeword newCw(r,g,b,brightness,mStartLearnTime,time);
          mCodebook(x,y).push_back(newCw);          
        }       
      }
    }
  }
}

bool 
BackgroundModelCb::matchColor(unsigned int r, unsigned int g, unsigned int b, const Codeword& cw) const
{
  assert(r >=0 && r <= 255);
  assert(g >=0 && g <= 255);
  assert(b >=0 && b <= 255);  

  double meanRed = cw.getRed();
  double meanGreen = cw.getGreen();
  double meanBlue = cw.getBlue();
  
  double numerator = pow( meanRed*r + meanGreen*g + meanBlue*b,2);
  double denominator = (meanRed*meanRed + meanGreen*meanGreen + meanBlue*meanBlue);

  //If the denominator is 0.0, set it to 1
  if (denominator == 0.0)
    denominator = 1.0;
  assert(denominator != 0.0);

  double x_tSquared = r*r + g*g + b*b;

  double pSquared = numerator/denominator;

  double colorDist = 0.0;

  if (x_tSquared - pSquared >= 0.0)
  {
    colorDist = sqrt(x_tSquared - pSquared);   
  }     

  if (colorDist <= mColorThresh)
  {
    return true;
  }
  else
  {    
    return false;
  }
}
 
bool 
BackgroundModelCb::matchBrightness(double brightness, const Codeword& cw) const
{
  assert(brightness >= 0.0 && brightness <= 442);

  double minBrightness = cw.getMinBrightness();
  double maxBrightness = cw.getMaxBrightness();

  double iLow = mAlphaScale * maxBrightness;
  double iHigh = std::min(mBetaScale*maxBrightness, minBrightness/mAlphaScale);

  if (brightness >= iLow && brightness <= iHigh)
    return true;
  else
    return false;  
}

void 
BackgroundModelCb::foregroundSegment(const Image32& img, const Image8& mask,
                         Image8& outputImage, double time) const
{

  assert(img.width() == mCodebook.width());
  assert(img.height() == mCodebook.height());
  assert(mask.width() == mCodebook.width());
  assert(mask.height() == mCodebook.height());

  // If we are still learning the initial background, return a black image
  if (mLearningStatus != FINISHED)
  {
    outputImage.setAll(0);
  }
  else
  {
    segmentCodebook(mCodebook, img, mask, outputImage);
  }

}

void 
BackgroundModelCb::update(const Image32& img, const Image8& mask, double time)
{
  assert(img.width() == mCodebook.width());
  assert(img.height() == mCodebook.height());
  assert(mask.width() == mCodebook.width());
  assert(mask.height() == mCodebook.height());

  if (mLearningStatus != FINISHED)
  {
    return;
  }   
  
  for (int y = 0; y < img.height(); y++)
  {
    for (int x = 0; x < img.width(); x++)
    {

      if (mask(x,y) != 255)
        continue;
      else
      {
        std::list<Codeword>& mainList = mCodebook(x,y);
        std::list<Codeword>& cacheList = mCache(x,y);
        std::list<Codeword>::iterator iCw;

        //If cache codewords have not been seen recently, remove them
        for (iCw = cacheList.begin(); iCw != cacheList.end(); )
        {
          if (iCw->getNegRunLength() > mCacheRemoveThresh)
            iCw = cacheList.erase(iCw);
          else
            iCw++;
        }

        //Promote cache codewords if they have been in the cache for a long time
        for (iCw = cacheList.begin(); iCw != cacheList.end(); )
        {
          double age = time - iCw->getFirstAccessTime();

          if (age > mCachePromoteThresh)
          {
            mainList.push_back(*iCw);
            iCw = cacheList.erase(iCw);
          }
          else
            iCw++;
        }

        //delete codewords from the main model if they havent been accessed for a long time
        for (iCw = mainList.begin(); iCw != mainList.end(); )
        {
          double lastAccessDiff = time - iCw->getLastAccessTime();

          if (lastAccessDiff > mModelRemoveThresh)
          { 
//            PVMSG("ERASING CODEWORD(%d,%d): %d remaining, last seen %f\n", x,y,mainList.size()-1, iCw->getLastAccessTime());
            iCw = mainList.erase(iCw);            
          }
          else
            iCw++;
        }
      }
    }
  }  
}

void 
BackgroundModelCb::segmentCodebook(const Image<std::list<Codeword>  >& wordImg, const Image32& img, 
                                   const Image8& mask, Image8& outputImg) const
{
  assert(img.width() == mCodebook.width());
  assert(img.height() == mCodebook.height());
  assert(mask.width() == mCodebook.width());
  assert(mask.height() == mCodebook.height());

  outputImg.resize(img.width(),img.height());

  for (int y = 0; y < wordImg.height(); y++)
  {
    for (int x = 0; x < wordImg.width(); x++)
    {      

      if (mask(x,y) != 255)
      {
        outputImg(x,y)=0;
        continue;
      }


      std::list<Codeword>& cwList = wordImg(x,y);

      bool found = false;
      unsigned int r = RED(img(x,y));
      unsigned int g = GREEN(img(x,y));
      unsigned int b = BLUE(img(x,y));
      double brightness = sqrt((double)(r*r + g*g + b*b));

      //find if existing codeword matches
      std::list<Codeword>::iterator iCw;     

      for (iCw = cwList.begin(); iCw != cwList.end(); iCw++)
      {
        if (matchColor(r,g,b,*iCw) && matchBrightness(brightness, *iCw))
        {
          found = true;
          break;
        }
      }      

      if (found)
      {
        outputImg(x,y) = 0;
      }
      else
      {
        outputImg(x,y) = 255;
      }      
    }
  }
}


void
BackgroundModelCb::visualize(const Image32& img, const Image8& mask,
                             Image32& outputImg) const
{

  outputImg.resize(img.width()*2,img.height()*2);
  outputImg.setAll(0);
  outputImg.paste(img,0,0);

  if (mLearningStatus == FINISHED)
  {
    //Draw cache segmentation image  
    Image32 tmp;

    Image8 modelImg;
    segmentCodebook(mCodebook, img, mask, modelImg);
    ColorConvert::convert(modelImg, PvImageProc::GRAY_SPACE, tmp, PvImageProc::RGB_SPACE);
    outputImg.paste(tmp,img.width(),img.height());  

    Image8 cacheImg;
    segmentCodebook(mCache, img, mask, cacheImg);
    ColorConvert::convert(cacheImg, PvImageProc::GRAY_SPACE, tmp, PvImageProc::RGB_SPACE);

    outputImg.paste(tmp,img.width(),0);
  }

  

  
}

} //namespace vision
} //namespace ait
/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include <fstream>
#include "legacy/vision_blob/Trajectory.hpp"
#include <legacy/types/strutil.hpp>
#include <boost/lexical_cast.hpp>
#include <legacy/types/DateTime.hpp>
#include <legacy/low_level/XmlParser.hpp>
#include <stack>

namespace ait 
{

namespace vision
{

Trajectory::Trajectory(int id) : mId(id)
{
}

Trajectory::Trajectory(const Trajectory& from)
: mId(from.mId),
  mNodes(from.mNodes)
{
}

Trajectory::~Trajectory()
{
}

TrajectoryPtr
Trajectory::clone()
{
  TrajectoryPtr pTraj(new Trajectory(mId));

  Nodes::const_iterator iNode;
  for (iNode = mNodes.begin(); iNode != mNodes.end(); iNode++)
  {
    double time = (*iNode)->time;
    Vector3f loc = (*iNode)->location;
    Rectanglef bbox = (*iNode)->boundingBox;    
    NodePtr pNode(new Node(time,loc,bbox));

    pTraj->mNodes.push_back(pNode);
  }

  return pTraj;  
}

//
// OPERATORS
//

Trajectory& 
Trajectory::operator=(const Trajectory& from)
{
  if (this != &from)
  {
    // Assign data members
    mId = from.mId;
    mNodes = from.mNodes;
  }
  return *this;
}

bool
Trajectory::operator==(const Trajectory& from) const
{
  // Compare members
  return (mId == from.mId) && std::equal(mNodes.begin(),mNodes.end(),from.mNodes.begin());
}

bool
Trajectory::operator!=(const Trajectory& from) const
{
  return !(*this == from);
}

//
// OPERATIONS
//

void 
Trajectory::merge(const Trajectory& target)
{
  assert(isValid());
  assert(target.isValid());

  Nodes::const_iterator iNode;  

  //PVMSG("%d merging with %d\n", this->getId(), target.getId());

  float startTime = lastNode()->time;
  //iterate through target nodes
  for (iNode = target.mNodes.begin(); iNode != target.mNodes.end(); iNode++)
  {
    //if the nodes are after startTime, add them to back ofto this->mNodes
    if ((*iNode)->time > startTime)
    {
      NodePtr pNode(new Node(**iNode)); 
      mNodes.push_back(pNode);

      (*iNode)->cardinality -= 1.0;
    }
  }

  /*
  if (!isValid())
  {
    for (iNode = mNodes.begin(); iNode != mNodes.end(); iNode++)
    {
      PVMSG("THIS TIME: %f\n", (*iNode)->time);
    }
    for (iNode = target.mNodes.begin(); iNode != target.mNodes.end(); iNode++)
    {
      PVMSG("TARGET TIME: %f\n", (*iNode)->time);
    }
  }*/
  assert(isValid());
}

void 
Trajectory::split(const Trajectory& target)
{
  assert(isValid());
  assert(target.isValid());

  
  //PVMSG("%d split from %d\n", this->getId(), target.getId());

  float startTime = firstNode()->time;
  
  /*
   *	The following code is temporarily disabled. Splits often cause
   * trajectories to briefly jump in a direction opposite of travel.   
   */
  /*
  Nodes::const_reverse_iterator iNode;
  // iterate through target nodes in reverse
  for (iNode = target.mNodes.rbegin();iNode != target.mNodes.rend(); iNode++)
  {
    // if time < startTime, then add to the front of this->mNodes
    if ((*iNode)->time < startTime)
    {
      NodePtr pNode(new Node(**iNode));       
      mNodes.push_front(pNode);
      (*iNode)->cardinality -= 1.0;
    }
  }*/

  Nodes::const_iterator closestNode;
  float closestDist = 0.0;
  bool firstRun = true;


  Nodes::const_iterator iNode;
  for (iNode = target.mNodes.begin(); iNode != target.mNodes.end(); iNode++)
  {
    if ((*iNode)->time > startTime)
      break;

    float dist = dist2((*iNode)->location, firstNode()->location);

    if (firstRun)
    {
      closestDist = dist;
      closestNode = iNode;
      firstRun = false;
    }
    else
    {
      if (dist <= closestDist)
      {
        closestDist = dist;
        closestNode = iNode;
      }
    }    
  }    
  
  Nodes::const_reverse_iterator irNode;
  // iterate through target nodes in reverse
  for (irNode = Nodes::const_reverse_iterator(closestNode);irNode != target.mNodes.rend(); irNode++)
  {
    // if time < startTime, then add to the front of this->mNodes
    //if ((*irNode)->time <= (*closestNode)->time)
    //{
      NodePtr pNode(new Node(**irNode));
      mNodes.push_front(pNode);
      (*irNode)->cardinality -= 1.0;
    //}
  } 

  assert(isValid());
}

void 
Trajectory::saveCSV(
  const std::string& fileName, 
  std::list<TrajectoryPtr>& trajectories)
{
  std::ofstream fout(fileName.c_str());

  fout << "TrajectoryId,Time,X,Y" << std::endl;

  std::list<TrajectoryPtr>::iterator iTrajectory;  

  for (iTrajectory = trajectories.begin();
    iTrajectory != trajectories.end();
    iTrajectory++)
  {
    Nodes::iterator iNode;
    Nodes& nodes = (*iTrajectory)->getNodes();
    for (iNode = nodes.begin(); iNode != nodes.end(); iNode++)
    {
      fout << (*iTrajectory)->getId() << "," <<  
        aitSprintf("%.6f",(*iNode)->time) << "," <<
        (*iNode)->location.x << "," <<
        (*iNode)->location.y << std::endl;//"," <<
        //(*iNode)->location.z << std::endl;
    }
  }
  fout.close();
}

void
Trajectory::saveXmlCsv(const std::string& filename, const std::list<TrajectoryPtr>& trajList)
{

  double timeOffset = PvUtil::systemTime() - PvUtil::time();

  std::ofstream outFile(filename.c_str());

  outFile << "Start Time,Duration,Trajectory" << std::endl;

  std::list<TrajectoryPtr>::const_iterator iTraj;
  for (iTraj = trajList.begin(); iTraj != trajList.end(); iTraj++)
  {
    DateTime startTime((*iTraj)->getStartTime() + timeOffset);
    double dur = (*iTraj)->getAge();
    std::string xmlString = (*iTraj)->toXmlString();

    std::string startTimeString = startTime.toString();
    
    outFile << startTimeString << "," << aitSprintf("%.3f",dur) << ",\"" << xmlString << "\"" << std::endl; 
  }

  outFile.close();
}

std::string 
Trajectory::toXmlString(unsigned int decimals)
{
  std::string out = "<trajectory id='" + aitSprintf("%i",getId()) + "' start_time='" +
    DateTime(getStartTime()).toString() + "'>";
  Nodes::iterator iNode;
  Nodes& nodes = getNodes();
  const double startTime = getStartTime();
  bool first = true;
  for (iNode = nodes.begin(); iNode != nodes.end(); iNode++)
  {
    assert((*iNode).get());
    if (!first) out += ";";
    out += aitSprintf("%.3f,%.*f,%.*f",
      (*iNode)->time-startTime,
      decimals,
      (*iNode)->location.x,
      decimals,
      (*iNode)->location.y);
    first = false;
  }
  out += "</trajectory>";
  return out;
}

class TrajectoryHandler : public Handler
{
public:
  TrajectoryHandler() 
  {
    mpTrajectory.reset(new Trajectory(-1));
  };

  virtual void start (const std::string& ns, const std::string& tag, const char **attr)
  {
    mTagStack.push(tag);

    if (tag == "trajectory")
    {
      for (int i = 0; attr[i] != NULL; i+=2)
      {
        if (!strcmp(attr[i],"id"))
        {
          mpTrajectory->setId(atoi(attr[i+1]));
        }
        if (!strcmp(attr[i],"start_time"))
        {
          if (mStartTime == 0)
          {
            mStartTime = DateTime::fromString(attr[i+1]).toSeconds();
          }
        }
      }
    }
  }

  virtual void cdata (const XML_Char *s, int len)
  {
    std::vector<double> numbers;
    if (!mTagStack.empty())
    {
      if (mTagStack.top() == "trajectory")
      {
        int start = 0;
        for (int i = 0; i < len;)
        {
          start = i;
          while (s[i] != ';' && s[i] != ',' && i < len) i++;
          numbers.push_back(atof(s+start));
          i++;
          if (numbers.size() == 3)
          {
            mpTrajectory->getNodes().push_back(Trajectory::NodePtr(new Trajectory::Node(
              mStartTime + numbers[0],
              Vector3f((float)numbers[1],(float)numbers[2],0),Rectanglef())));
            numbers.clear();
          }
        }
      }
    }
  }

  virtual void end (const std::string& ns, const std::string& tag)
  {
    mTagStack.pop();
  }

  TrajectoryPtr mpTrajectory;
  double mStartTime;
  std::stack<std::string> mTagStack;
};

TrajectoryPtr
Trajectory::fromXmlString(std::string& xml, double startTime)
{
  TrajectoryHandler hand;
  hand.mStartTime = startTime;
  XmlParser parser;
  parser.registerNamespaceHandler("", &hand);
  parser.parse(xml);
  return hand.mpTrajectory;
}

std::list<TrajectoryPtr>
Trajectory::loadCSV(const std::string& fileName)
{
  std::ifstream fin(fileName.c_str());

  std::string line;

  // Skip first line.
  std::getline(fin,line);

  std::list<TrajectoryPtr> trajectories;

  while (!fin.eof())
  {
    std::getline(fin,line);

    if (line.empty()) break;

    if (line == "TrajectoryId,Time,X,Y") //skip this line
      continue;

    std::vector<std::string> tokens = tokenize(line,", \n");

    int id = boost::lexical_cast<int>(tokens[0]);

    if (trajectories.empty() || trajectories.back()->getId() != id)
    {
      trajectories.push_back(TrajectoryPtr(new Trajectory(id)));
    }

    trajectories.back()->getNodes().push_back(NodePtr(new Node(
      boost::lexical_cast<double>(tokens[1]),
      Vector3f(
        boost::lexical_cast<float>(tokens[2]),
        boost::lexical_cast<float>(tokens[3]),
        0.0//boost::lexical_cast<float>(tokens[4])
        ),Rectanglef())));
  }

  return trajectories;
}

std::string 
Trajectory::getXmlBlobRectangles()
{
  std::string str("<blob_rects>");

  Nodes::iterator iNode;
  Nodes& nodes = getNodes();
  for (iNode = nodes.begin(); iNode != nodes.end(); iNode++)
  {
    if (iNode != nodes.begin())
    {
      str += ",";
    }
    const Node& node = *(*iNode);
    str += 
      boost::lexical_cast<std::string>(node.boundingBox.x0) + "," +
      boost::lexical_cast<std::string>(node.boundingBox.y0) + "," +
      boost::lexical_cast<std::string>(node.boundingBox.x1) + "," +
      boost::lexical_cast<std::string>(node.boundingBox.y1);
  }
  str += "</blob_rects>";

  str += "<frames>";
  for (iNode = nodes.begin(); iNode != nodes.end(); iNode++)
  {
    if (iNode != nodes.begin())
    {
      str += ",";
    }
    const Node& node = *(*iNode);
    str += boost::lexical_cast<std::string>(node.time);
  }
  str += "</frames>";

  return str;
}

void 
Trajectory::draw(Image32& img, unsigned int color, double time) const
{
  //Iterate through the nodes, drawing lines between them
  Nodes::const_iterator iNode = mNodes.begin();
  Nodes::const_iterator iNextNode = mNodes.begin()++;

  while (iNode != mNodes.end()) 
  {
    if (time >= 0.0)
      if ((*iNode)->time > time)
        break;
      
    img.line((*iNode)->location.x, (*iNode)->location.y,
      (*iNextNode)->location.x, (*iNextNode)->location.y, color);
    //make line thicker so it doesn't get 'compressed away' for demos
    
    img.line((*iNode)->location.x+1, (*iNode)->location.y,
      (*iNextNode)->location.x+1, (*iNextNode)->location.y, color);
    img.line((*iNode)->location.x-1, (*iNode)->location.y,
      (*iNextNode)->location.x-1, (*iNextNode)->location.y, color);
    img.line((*iNode)->location.x, (*iNode)->location.y-1,
      (*iNextNode)->location.x, (*iNextNode)->location.y-1, color);
    img.line((*iNode)->location.x, (*iNode)->location.y+1,
      (*iNextNode)->location.x, (*iNextNode)->location.y+1, color);
      

      
    iNextNode = iNode++;
  }
}

TrajectoryPtr 
Trajectory::compress(double distInterval, double timeInterval)
{
  //the new trajectory
  TrajectoryPtr pTraj(new Trajectory(mId));

  if (mNodes.size() < 3)
  {
    std::copy(mNodes.begin(), mNodes.end(), std::inserter(pTraj->mNodes, pTraj->mNodes.begin()));
  }
  else
  {            
    //add the first node
    PVASSERT(pTraj->mNodes.size() == 0);      
    NodePtr pNode(new Node(0.0,Vector2f(),Rectanglef()));
    *pNode = *mNodes.front();
    pTraj->mNodes.push_back(pNode);

    //loop through remaining nodes      
    Nodes::iterator iRef = mNodes.begin();      
    Nodes::iterator iCurr = mNodes.begin();
    iCurr++;

    for (; iCurr != mNodes.end(); iCurr++)
    {        

      double distance = sqrt(dist2((*iCurr)->location, (*iRef)->location));
      double timeDiff = (*iCurr)->time - (*iRef)->time;

      if (timeDiff >= timeInterval || distance >= distInterval)
      {
        NodePtr pNode(new Node(0.0,Vector2f(),Rectanglef()));
        *pNode = **iCurr;
        pTraj->mNodes.push_back(pNode);

        iRef = iCurr;
      }                
    }

    //add the last node (if not already added)
    PVASSERT(pTraj->mNodes.size() > 0);
    pNode.reset(new Node(0.0,Vector2f(),Rectanglef()));
    *pNode = *mNodes.back();

    if (pNode->time > pTraj->lastNode()->time)
      pTraj->mNodes.push_back(pNode);
      
    //PVMSG("SIZE: %d  %d\n", pTraj->mNodes.size(), mNodes.size());           

  }

  PVASSERT(pTraj->isValid());
  return pTraj;
}  

void 
Trajectory::compact(float threshhold)
{
   // Do nothing if there is only one node
   if (mNodes.size() == 1) return;

   //PVMSG("Compacting %d \n", this->getId());

   // We start off by not forming any groups
   bool grouping = false;

   Nodes::iterator iStart;
   Nodes::iterator iEnd;
    
   Vector2f refPos = mNodes.front()->location;
   Vector2f curPos;

   Nodes::iterator iNode = mNodes.begin()++;
   Nodes::iterator iLast = mNodes.begin();

   // iterate through nodes
   while (iNode != mNodes.end())
   {
     curPos = (*iNode)->location;

     float dist = sqrt(dist2(curPos, refPos));      

     if (grouping == true) //grouping nodes
     {
       if (dist > threshhold)
       {
         iEnd = iLast;
         grouping = false;          
         compactNodes(iStart, iEnd);
       }
     }
     else //not grouping nodes
     {
       if (dist <= threshhold)
       {
         iStart = iLast;
         grouping = true;
       }
       else
         refPos = curPos;
     }

     iLast = iNode++;
   }

   if (grouping == true)
     compactNodes(iStart, --(mNodes.end())); //7/7/04   

   assert(isValid());
}

void 
Trajectory::combine(
  const Trajectory& a, 
  const Trajectory& b, 
  Trajectory& result)
{
  assert(a.isValid());
  assert(b.isValid());

  //one trajectory must follow the other in time
  assert(a.firstNode()->time > b.lastNode()->time || b.firstNode()->time > a.lastNode()->time);

  result.setId(std::min(a.getId(), b.getId()));  
  result.mNodes.clear();

  if (a.firstNode()->time > b.lastNode()->time)
  {      
    std::copy(b.mNodes.begin(), b.mNodes.end(), 
      std::back_inserter(result.mNodes));
    std::copy(a.mNodes.begin(), a.mNodes.end(), 
      std::back_inserter(result.mNodes));
  }
  else if (b.firstNode()->time > a.lastNode()->time)
  {
    std::copy(a.mNodes.begin(), a.mNodes.end(), std::back_inserter(result.mNodes));
    std::copy(b.mNodes.begin(), b.mNodes.end(), std::back_inserter(result.mNodes));      
  }
  
  assert(result.isValid());
}

void 
Trajectory::compactNodes(Nodes::iterator start, Nodes::iterator end)
{
  Nodes temp;

  // might not be necessary
  if (start == end)
    return;
  //
    
  Nodes::iterator i = start;
  float xSum = 0.0;
  float ySum = 0.0;
  int numNodes = 0;
  float cardinalitySum = 0.0;

  //calculate average position
  while(true)
  {
    numNodes++;
    xSum += (*i)->location.x;
    ySum += (*i)->location.y;

    cardinalitySum += (*i)->cardinality;
    //std::cout << i->location << std::endl;      

    if (i == end) break;
    i++;
  }
    
  float xAvg = xSum / (float)numNodes;
  float yAvg = ySum / (float)numNodes;
  float cardinalityAvg = cardinalitySum / (float)numNodes;

  (*end)->location = Vector2f(xAvg, yAvg);
  (*end)->time = (*start)->time;
  (*end)->cardinality = cardinalityAvg;

  //erase the nodes, but keep the new averaged one
  mNodes.erase(start,end);  

  assert(isValid());
}

void 
Trajectory::getNodesTimeSlice(
  const double startTime, 
  const double endTime, 
  Trajectory::Nodes& nodelist) const
{ 

  nodelist.clear();

  Trajectory::Nodes::const_iterator iNode;
  for (iNode = mNodes.begin(); iNode != mNodes.end(); iNode++)
  {
    if ((*iNode)->time <= endTime && (*iNode)->time >= startTime)
    {
      NodePtr pNode(new Node(**iNode));

      nodelist.push_back(pNode);    
    }
  }  
}



Vector2f
Trajectory::forwardVectorAtTime(float timeThresh, float time) const
{
  NodePtr startNode = nodeClosestTime(time - timeThresh / 2.0);
  NodePtr endNode = nodeClosestTime(time + timeThresh / 2.0);

  Vector2f dir;
  dir.x = endNode->location.x - startNode->location.x;
  dir.y = endNode->location.y - startNode->location.y;

  return dir;  
}

int 
Trajectory::cardinalityAtTime(float time, float timeThresh) const
{

  Nodes::const_iterator iNode;
  bool timeFound = false;

  for (iNode = mNodes.begin(); iNode != mNodes.end(); iNode++)
  {
    if ((*iNode)->time >= time)
    {
      timeFound = true;
      break;
    }
  }

  float cardTotal = 0.0;

  Nodes::const_iterator iForward = iNode;  
  int cardCount = 0;

  for (; iForward != mNodes.end(); iForward++)
  {
    cardTotal += (*iForward)->cardinality;
    cardCount++;

    if ((*iForward)->time > (time + timeThresh))
      break;    
  }

  Nodes::const_reverse_iterator iReverse(iNode);

  for (; iReverse != mNodes.rend(); iReverse++)
  {
    cardTotal += (*iReverse)->cardinality;
    cardCount++;

    if ((*iReverse)->time < (time - timeThresh))
      break;
  }

  if (timeFound)
    return round(cardTotal / (float)cardCount);
  else
    return 0;  
}

void
Trajectory::extrapolateForward(const Vector2f& location, float thresh)
{     
  Vector2f dir;

  dir.x = location.x - lastNode()->location.x;
  dir.y = location.y - lastNode()->location.y;
  dir.normalize();

  float totalDist = sqrt(dist2(location, lastNode()->location));

  float speed = forwardSpeed(thresh); 
  
  double timeDiff = 2.0 / speed;  

  assert(timeDiff > 0.0);

  float distTraveled = 0.0;
  
  double currentTime = lastNode()->time; 

  while (distTraveled < totalDist)
  {
    distTraveled += 2.0;
    currentTime += timeDiff;

    Vector2f nodeLoc = dir;
    nodeLoc.mult(distTraveled);
    nodeLoc.x += lastNode()->location.x;
    nodeLoc.y += lastNode()->location.y;    

    //TODO: Size of bounding box fixed
    Rectanglef boundBox;
    boundBox.x0 = nodeLoc.x - 1.0;
    boundBox.x1 = nodeLoc.x + 1.0;
    boundBox.y0 = nodeLoc.y;
    boundBox.y1 = nodeLoc.y + 2.0;    
    
    //TODO: Doesn't account for top view

    NodePtr pNode(new Node(currentTime, nodeLoc, boundBox));
    mNodes.push_back(pNode);
  }

  /*
  if (!isValid())
  {
    PVMSG("TIME DIFF: %f\n", timeDiff);
    Nodes::const_iterator iNode;
    for (iNode = mNodes.begin(); iNode != mNodes.end(); iNode++)
    {
      PVMSG("Time: %f\n", (*iNode)->time);
    }
  }
  */

  assert(isValid());

}

void
Trajectory::extrapolateReverse(const Vector2f& location, float thresh)
{
  
  Vector2f dir;

  dir.x = location.x - firstNode()->location.x;
  dir.y = location.y - firstNode()->location.y;
  dir.normalize();

  float totalDist = sqrt(dist2(location, firstNode()->location));

  float speed = reverseSpeed(thresh);
  
  double timeDiff = 2.0 / speed;

  float distTraveled = 0.0;
  
  double currentTime = firstNode()->time; 

  while (distTraveled < totalDist)
  {
    distTraveled += 2.0;
    currentTime -= timeDiff;

    // Events with negative time components are not permitted
    
    if (currentTime < 0.0)
      break;

    Vector2f nodeLoc = dir;
    nodeLoc.mult(distTraveled);
    nodeLoc.x += firstNode()->location.x;
    nodeLoc.y += firstNode()->location.y;

    //TODO: Size of bounding box fixed
    Rectanglef boundBox;
    boundBox.x0 = nodeLoc.x - 1.0;
    boundBox.x1 = nodeLoc.x + 1.0;
    boundBox.y0 = nodeLoc.y;
    boundBox.y1 = nodeLoc.y + 2.0;    
    
    //TODO: top view

    NodePtr pNode(new Node(currentTime, nodeLoc, boundBox));
    mNodes.push_front(pNode);
  }

  /*
  if (!isValid())
  {
    PVMSG("TIME DIFF: %f\n", timeDiff);
    Nodes::const_iterator iNode;
    for (iNode = mNodes.begin(); iNode != mNodes.end(); iNode++)
    {
      PVMSG("Time: %f\n", (*iNode)->time);
    }
  }
  */

  assert(isValid());
}

Trajectory::NodePtr
Trajectory::nodeClosestTime(double time) const
{
  Nodes::const_iterator iNode;  
  
  for (iNode = mNodes.begin(); iNode != mNodes.end(); iNode++)
  {
    if ((*iNode)->time >= time)    
      return *iNode;
  }  
  return lastNode();
}


/*
 *	Note: should have used reverse iterators for opposite directions.
 */


Vector2f 
Trajectory::reverseDirection(float thresh) const
{
  Nodes::const_iterator iNode;
  for (iNode = mNodes.begin()++; iNode != mNodes.end(); iNode++)
  {
    if (sqrt(dist2((*iNode)->location, firstNode()->location)) > thresh)
      break;
  }

  if (iNode == mNodes.end())
    iNode--;

  Vector3f dir;
  dir.x = firstNode()->location.x - (*iNode)->location.x;
  dir.y = firstNode()->location.y - (*iNode)->location.y;

  if (dir.norm() != 0.0)
    dir.normalize(); 
  
  return dir;  
}


Vector2f 
Trajectory::forwardDirection(float thresh) const
{
  
  Nodes::const_reverse_iterator iNode;
  for (iNode = mNodes.rbegin()++; iNode != mNodes.rend(); iNode++)
  {
    if (sqrt(dist2((*iNode)->location, lastNode()->location)) > thresh)
      break;
  }

  if (iNode == mNodes.rend())
    iNode--;

  Vector3f dir;
  dir.x = lastNode()->location.x - (*iNode)->location.x;
  dir.y = lastNode()->location.y - (*iNode)->location.y;
  
  if (dir.norm() != 0.0)
    dir.normalize();
  return dir;
}

Vector2f 
Trajectory::forwardVector(float timeThresh) const
{  
  //If there are not enough nodes to determine direction, return 0.0,0.0
  if (mNodes.size() <= 1)
    return Vector2f(0.0,0.0);

  Nodes::const_reverse_iterator iNode;
  /* Travel from the head of the trajectory towards the tail, breaking
   * when the time threshold has been passed 
   */
  for (iNode = mNodes.rbegin(); iNode != mNodes.rend(); iNode++)
  {
    if (lastNode()->time - (*iNode)->time > timeThresh)
      break;
  }

  if (iNode == mNodes.rend())
    iNode--;

  Vector2f dir;
  dir.x = lastNode()->location.x - (*iNode)->location.x;
  dir.y = lastNode()->location.y - (*iNode)->location.y;

  return dir;

}

Vector2f 
Trajectory::reverseVector(float timeThresh) const
{
  //If there are not enough nodes to determine direction, return 0.0,0.0
  if (mNodes.size() <= 1)
    return Vector2f(0.0,0.0);
  
  Nodes::const_iterator iNode;
  /* Travel from the tail of the trajectory towards the head, breaking
   * when the time threshold has been passed 
   */
  for (iNode = mNodes.begin(); iNode != mNodes.end(); iNode++)
  {
    if ((*iNode)->time - firstNode()->time > timeThresh)
      break;
  }

  if (iNode == mNodes.end())
    iNode--;

  Vector2f dir;
  dir.x = firstNode()->location.x - (*iNode)->location.x;
  dir.y = firstNode()->location.y - (*iNode)->location.y;

  return dir;
}

float
Trajectory::length() const
{
  float length = 0.0;

  Nodes::const_iterator iNode;
  Nodes::const_iterator iNextNode;
  for (iNode = mNodes.begin(), iNextNode = ++mNodes.begin(); 
    iNextNode != mNodes.end(); iNode++, iNextNode++)
  {    
    length += sqrt(dist2((*iNode)->location, (*iNextNode)->location));    
  }
  return length;
}

float 
Trajectory::lengthFromRear(float thresh) const
{ 
  float length = 0.0;

  Nodes::const_iterator iNode;
  Nodes::const_iterator iNextNode;
  for (iNode = mNodes.begin(), iNextNode = ++mNodes.begin(); 
    iNextNode != mNodes.end(); iNode++, iNextNode++)
  {    
    length += sqrt(dist2((*iNode)->location, (*iNextNode)->location));

    if (sqrt(dist2(firstNode()->location, (*iNode)->location)) > thresh)
      break;
  }
  assert(length != 0.0);
  return length;
}

float 
Trajectory::lengthFromFront(float thresh) const
{
  float length = 0.0;

  Nodes::const_reverse_iterator iNode;
  Nodes::const_reverse_iterator iNextNode;
  for (iNode = mNodes.rbegin(), iNextNode = ++mNodes.rbegin(); 
    iNextNode != mNodes.rend(); iNode++, iNextNode++)
  {    
    length += sqrt(dist2((*iNode)->location, (*iNextNode)->location));   

    if (sqrt(dist2(lastNode()->location, (*iNode)->location)) > thresh)
      break;
  }

  assert(length != 0.0);
  return length;
}

float 
Trajectory::timeFromRear(float thresh) const
{  
  Nodes::const_iterator iNode;  
  for (iNode = mNodes.begin(); iNode != mNodes.end(); iNode++)
  {    
    if (sqrt(dist2(firstNode()->location, (*iNode)->location)) > thresh)
      break;
  }

  if (iNode == mNodes.end())
    iNode--;

  float timeDiff = (*iNode)->time - firstNode()->time; 
  assert(timeDiff != 0.0);
  return timeDiff;
}

float 
Trajectory::timeFromFront(float thresh) const
{
  Nodes::const_reverse_iterator iNode;
  for (iNode = mNodes.rbegin(); iNode != mNodes.rend(); iNode++)
  {    
    if (sqrt(dist2(lastNode()->location, (*iNode)->location)) > thresh)
      break;
  }

  if (iNode == mNodes.rend())
    iNode--;
    
  float timeDiff = lastNode()->time - (*iNode)->time;
  assert(timeDiff != 0.0);
  return timeDiff;
}


bool 
Trajectory::getLocationFromTime(double time, Nodes::const_iterator& iNode, Vector3f& location) const
{
  if (iNode == mNodes.end())
  {
    if (mNodes.empty()) return false;
    iNode = mNodes.begin();
  }

  if (time > getEndTime())
  {
    return false;
  }

  while (iNode != mNodes.end())
  {
    const Node& curNode = (**iNode);
    double nodeTime = curNode.time;

    if (time < nodeTime)
    {
      return false;
    }
    else if (nodeTime == time)
    {
      location = curNode.location;
      return true;
    }

    Nodes::const_iterator iNextNode = iNode;
    ++iNextNode;
    if (iNextNode == mNodes.end())
    {
      return false;
    }
    const Node& nextNode = (**iNextNode);

    if (nextNode.time == curNode.time) return false;

    if (time <= nextNode.time)
    {
      // Perform linear interpolation
      float t = (float)(time-curNode.time)/(nextNode.time-curNode.time);
      const Vector3f& p0 = curNode.location;
      const Vector3f& p1 = nextNode.location;
      location.set(p0.x+t*(p1.x-p0.x),p0.y+t*(p1.y-p0.y),p0.z+t*(p1.z-p0.z));
      return true;
    }
    iNode = iNextNode;
  }

  return false;
}


//
// INQUIRY
//

bool 
Trajectory::isValid() const
{
  double lastTime = 0.0;
  bool first = true;

  //PVMSG("Trajectory\n");
  Nodes::const_iterator i;
  for (i = mNodes.begin(); i != mNodes.end(); i++)
  {
    //PVMSG("TIME: %f\n", (*i)->time);
    if (first)     
      first = false;    
    else    
      if ((*i)->time < 0 || (*i)->time <= lastTime) return false;     
    
    lastTime = (*i)->time;
  }
  return true;
}

}; // namespace vision
}; // namespace ait


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "legacy/vision_blob/TrajectoryEvents.hpp"



#include <legacy/math/LineSegment.hpp>
#include <legacy/math/Polygon.hpp>
#include <legacy/pv/ait/Vector3.hpp>

namespace ait 
{

namespace vision
{

TrajectoryEvents::TrajectoryEvents()
: mIsInitialized(false), mIsLine(false),
  mMaxEventsPerTrajectory(0)
{
  mLineThreshold = 0.0;
  mPolyThreshold = 0.0;
  mCardTimeThreshold = 1.0;
}

TrajectoryEvents::~TrajectoryEvents()
{
}

void 
TrajectoryEvents::init(BlobSizeEstimatePtr pBlobEst)
{

  // Initialize this object
  mIsInitialized = true;

  assert(pBlobEst.get());

  mpBlobSizeEstimate = pBlobEst;  

}

//
// OPERATIONS
//



void TrajectoryEvents::extractEventsLine(
  const Trajectory& trajectory,
  std::list<PolygonEventPtr>& events)
{
  PVASSERT(mIsInitialized);
  
  //TODO: What if trajectory ends before out of proximity zone

  const Trajectory::Nodes nodes = trajectory.getNodes();
  Trajectory::Nodes::const_iterator iNode;
  Trajectory::Nodes::const_iterator iNextNode;

  LineSegment ls;

  bool nearEventGenerator = false;
  Trajectory::Nodes::const_iterator iLastIntersect;

  float blobScale = (mpBlobSizeEstimate->getBlobWidth(ls(0)) + 
    mpBlobSizeEstimate->getBlobWidth(ls(1))) / 2.0;  

  Vector2f intersectPoint;
  int initialSide = 0;
  int side = 0;

  int numEvents = 0;

  //iterate through the path, checking for intersections
  for (iNode = nodes.begin(), iNextNode = ++nodes.begin(); 
    iNextNode != nodes.end(); iNode++, iNextNode++)
  {      
    ls(0) = (*iNode)->location;
    ls(1) = (*iNextNode)->location;
    
    if (mLineSeg.intersects(ls, intersectPoint))
    {
      iLastIntersect = iNode;
      if (!nearEventGenerator) 
      {
        nearEventGenerator = true;
        mLineSeg.distanceToPoint(ls(0), initialSide);
      }
    }
    else // no intersection occurred
    {
      if (nearEventGenerator)
      {
        
        float dist = mLineSeg.distanceToPoint(ls(0), side);

        // make sure the point is not on the line of the segment
        if (side != mLineSeg.POINT_BACKWARD_EXTENSION &&
            side != mLineSeg.POINT_FORWARD_EXTENSION &&
            side != mLineSeg.POINT_ON_SEGMENT)
        {
          if (dist > (mLineThreshold * blobScale) && side != initialSide)
          {
            nearEventGenerator = false;
            // make an event here

            int enterSide = 0;
            int exitSide = 0;
            double beginFrame = (*iLastIntersect)->time;
            iLastIntersect++;
            double endFrame = (*iLastIntersect)->time;            

            if (initialSide == mLineSeg.POINT_LEFT)
            {
              enterSide = 0;
              exitSide = 1;
            }
            else
            {
              enterSide = 1;
              exitSide = 0;
            }                  
            assert(trajectory.isValid());
            //PVMSG("BEGIN: %d END: %d\n", beginFrame, endFrame);
            createEvent(
              events,
              enterSide,
              exitSide,
              beginFrame,
              endFrame,
              trajectory);

            numEvents++;
            if (mMaxEventsPerTrajectory > 0 && numEvents >= mMaxEventsPerTrajectory)
            {
              return;
            }
          }
        }
      }
    }    
  }  
}

void TrajectoryEvents::createEvent(
    std::list<PolygonEventPtr>& events,
    int enterSide, 
    int exitSide, 
    double beginFrame,
    double endFrame, 
    const Trajectory& traj)
{   
  PVASSERT(mIsInitialized);

  assert(endFrame - beginFrame >= 0);
  
  //TODO: TEMP CHANGES FOR TESTING BELOW

  for  (int i = 0; i < 1/*std::max(traj.cardinalityAtTime(beginFrame, mCardTimeThreshold), 1)*/; i++)
  {

    PolygonEventPtr currentEvent(new PolygonEvent());  
    currentEvent->enterSide = enterSide;
    currentEvent->exitSide = exitSide;
    currentEvent->beginFrame = beginFrame;
    currentEvent->endFrame = endFrame;

    Trajectory::Nodes tempNodes = currentEvent->trajectory.getNodes();  

    traj.getNodesTimeSlice(
      currentEvent->beginFrame, 
      currentEvent->endFrame, 
      tempNodes);    

    currentEvent->trajectory.getNodes() = tempNodes;
    currentEvent->trajectory.setId(traj.getId());  
    events.push_back(currentEvent);
  }
}

void TrajectoryEvents::extractEventsPolygon(
  const Trajectory& trajectory,
  std::list<PolygonEventPtr>& events)
{
  PVASSERT(mIsInitialized);
  //TODO: CURRENTLY ASSUMES SMALL STEPS, CHANGE THIS LATER, FIX MULTIPLE CROSSES IN ONE STEP CODE
  
  //TODO: MAKE BASED ON BLOB SIZE   
  
  int numEvents = 0;  

  PolygonEventPtr currentEvent;
  const Trajectory::Nodes nodes = trajectory.getNodes();
  std::vector<int> edges;  

  int enterSide = 0;
  int exitSide = 0;
  double beginFrame = 0.0;
  double endFrame = 0.0; 

  bool currentlyInside = mPoly.isPointInside(trajectory.firstNode()->location);
  bool startsInside = currentlyInside;

  bool lastIntersectInside = false;

  float blobScale = 0.0;  

  for (int edge = 0; edge < mPoly.numEdges(); edge++)  
    blobScale += mpBlobSizeEstimate->getBlobWidth(mPoly.getEdge(edge)(0));        
  blobScale /= (float)mPoly.numEdges();  

  
  // MAKE SURE NOT NEAR AN EDGE  
  
  Trajectory::Nodes::const_iterator iNode;
  Trajectory::Nodes::const_iterator iNextNode;
  Trajectory::Nodes::const_iterator iLastIntersect;

  LineSegment ls;

  bool nearEdge = false;

  if (currentlyInside) //start recording data
  {
    enterSide = -1;
    beginFrame = trajectory.firstNode()->time;
  }
  
  //iterate through the path, checking for intersections
  for (iNode = nodes.begin(), iNextNode = ++nodes.begin(); 
    iNextNode != nodes.end(); iNode++, iNextNode++)
  {
    edges.clear();
    ls(0) = (*iNode)->location;
    ls(1) = (*iNextNode)->location;
    edges = mPoly.edgesCrossed(ls);

    currentlyInside = mPoly.isPointInside(ls(0));

    if (edges.size() == 0) //no edges were crossed
    {
      if (nearEdge)
      {
        float dist = mPoly.distanceNearestEdge(ls(0)); 
          
        if (dist > (mPolyThreshold * blobScale) && currentlyInside != lastIntersectInside)
        {
          nearEdge = false;
            
          if (!currentlyInside) //we are outside of the polygon now, end event
          {
            endFrame = (*iNode)->time;
            createEvent(
              events,
              enterSide, 
              exitSide, 
              beginFrame, 
              endFrame, 
              trajectory);
            
            numEvents++;
            if (mMaxEventsPerTrajectory > 0 && numEvents >= mMaxEventsPerTrajectory)
            {
              return;
            }

            startsInside = false;            
          }
        }
      }        
    }
    else if (edges.size() == 1) // one edge crossed
    {
      iLastIntersect = iNode;

      if (!nearEdge)
      {           
        lastIntersectInside = currentlyInside;
        nearEdge = true;
      }

      if (currentlyInside) // traj intersecting from inside headed out        
      {
        endFrame = (*iNextNode)->time;
        exitSide = edges.front();        
      }
      else // traj intersecting from outside headed in        
      {
        beginFrame = (*iNode)->time;
        enterSide = edges.front();        
      }
    }
    else //multiple edges crossed
    {
      iLastIntersect = iNode;

      if (!nearEdge)
      {
        lastIntersectInside = currentlyInside;
        nearEdge = true;
      }

      if (edges.size() % 2 == 0)
      {
        if (currentlyInside)
        {
          //do nothing
        }
        else // not inside polygon
        {
          enterSide = edges.front();
          exitSide = edges.back();
          beginFrame = (*iNode)->time;
          endFrame = (*iNextNode)->time;
        }
      }
      else // crosses an odd number of edges
      {
        if (currentlyInside)        
        {
          exitSide = edges.back();
          endFrame = (*iNextNode)->time;
        }        
        else
        {
          enterSide = edges.front();
          beginFrame = (*iNode)->time;
        }
      }      
    }    
  }

  //check for case when traj ends inside of polygon
  
  //
  if ((startsInside && currentlyInside) || (startsInside && nearEdge))
  {
    exitSide= -1;    
    endFrame = trajectory.lastNode()->time;    
    createEvent(
      events,
      enterSide, 
      exitSide, 
      beginFrame, 
      endFrame, 
      trajectory);
              
    numEvents++;
    if (mMaxEventsPerTrajectory > 0 && numEvents >= mMaxEventsPerTrajectory)
    {
      return;
    }
  }
  else if (!startsInside && currentlyInside && !nearEdge)
  {
    exitSide = -1;    
    endFrame = trajectory.lastNode()->time;

    createEvent(
      events,
      enterSide, 
      exitSide, 
      beginFrame, 
      endFrame, 
      trajectory);              

    numEvents++;
    if (mMaxEventsPerTrajectory > 0 && numEvents >= mMaxEventsPerTrajectory)
    {
      return;
    }
  }
}

void TrajectoryEvents::extractEvents(
  const Trajectory& trajectory, 
  const std::vector<Vector2f>& eventPoly,
  std::list<PolygonEventPtr>& events)
{
  PVASSERT(mIsInitialized);
  
  if (eventPoly.size() == 0) //no polygon information specified
    return;
  // we are dealing with a line
  else if (eventPoly.size() == 2) 
  {
    mIsLine = true;
    mLineSeg(0) = eventPoly[0];
    mLineSeg(1) = eventPoly[1];      
  }
  // we are dealing with a polygon
  else
  {
    mIsLine = false;
    assert(eventPoly.size() > 2);
    mPoly.setVertices(eventPoly);
  }

  if (mIsLine)
    extractEventsLine(trajectory, events);
  else
    extractEventsPolygon(trajectory, events);

}

//
// ACCESS
//

//
// INQUIRY
//

}; // namespace vision
}; // namespace ait



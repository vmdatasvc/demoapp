
#include "legacy/vision_blob/CartDetector.hpp"

namespace ait 
{

namespace vision
{
  
CartDetector::CartDetector():mDetectThres((float)1.01)
  {
    mCartShapes.clear();
    mOrientations.clear();
  }

CartDetector::~CartDetector()
{
}

//
// OPERATIONS
//



void 
CartDetector::constructForegroundList(Image8 &foregroundImage, int foregroundJump)
{
  mForegroundList.clear();

  int imageHeight = foregroundImage.height();
  int imageWidth = foregroundImage.width();
  for(int y = 0; y < foregroundImage.height(); y+=foregroundJump)
  {
    for(int x = 0; x < foregroundImage.width(); x+=foregroundJump)
    {
      // Foreground pixels
      if(foregroundImage(x,y) == 255)
      {
        int regionId, yRegionId, xRegionId;
        if( y <= imageHeight/3)//99 )
          yRegionId = 0;
        else if (y <= imageHeight*2/3)//y >= 100 && y <= 139)
          yRegionId = 1;
        else if (y <= imageHeight)//>= 140)
          yRegionId = 2;

        if( x <= imageWidth/5)//59 )
          xRegionId = 0;
        else if (x <= imageWidth*2/5)//>= 60 && x <= 129)
          xRegionId = 1;
        else if (x <= imageWidth*3/5)//>= 130 && x <= 189)
          xRegionId = 2;
        else if (x <= imageWidth*4/5)//>= 190 && x <= 259)
          xRegionId = 3;
        else if (x <= imageWidth)//>= 260 && x <= 319)
          xRegionId = 4;

        regionId = xRegionId +yRegionId*5;

        mForegroundList.push_back(Vector3f((float)x, (float)y, regionId));
      }
    }
  }
  //  PVMSG("Number of Edge Pixels = %d\n",mForegroundList.size());
}

void 
CartDetector::buildCartModel(void)
{
  int numberOfRegions = mCartShapes.size();
  int numberOfOrientations = mOrientations.size();

  mCartModel.resize(numberOfRegions,numberOfOrientations);
  for (int numRegions = 0; numRegions < mCartShapes.size(); numRegions ++)
  {
    for (int numOrientations = 0; numOrientations < mOrientations.size(); numOrientations++)
    {
      CartEdgeModelPtr pNewCartEdgeModel(new CartEdgeModel());
      pNewCartEdgeModel->createEdgeModel(*(mCartShapes[numRegions]),mOrientations[numOrientations],mInMargin,mOutMargin);
	  mCartModel(numRegions,numOrientations) = pNewCartEdgeModel;
    }
  }

}

void CartDetector::detectCarts(std::vector< Polygon > &carts, Image8 edgeImage, Image32 &scoreImage, int maskJump)
{

  // The foreground pixels are the candidate centroid of the cart
  mCandidates.clear();

  for(int n = 0; n < mForegroundList.size(); n++)
  {
    int candX = mForegroundList[n].x;
    int candY = mForegroundList[n].y;

    //	  printf("At point (%d, %d)\n",candX,candY);

    float maxScore = 0;
    int maxOrien;
	int regionId = mForegroundList[n].z;

    // Apply 0, 45, 90, 135 degrees cart orientations
    //for(int orien = 0; orien < mCartMask.size(); orien ++)
    for(int orien = 0; orien < mCartModel.cols(); orien ++)
    {
	  CartEdgeModelPtr cart = mCartModel(regionId,orien);
      float score = mCartModel(regionId,orien)->generateMatchScore(edgeImage,mForegroundList[n],mDetectThres);

      if(score > maxScore)
      {
        maxScore = score;
        maxOrien = orien;
      }

    }

    //	  PVMSG("score = %f\n",maxScore);
    unsigned char scoreUC = (int)(256.0*(maxScore/2));
    scoreImage(candX, candY) = PV_RGB(scoreUC, scoreUC, scoreUC);

    // Threshold the scores to collect the candidates
    if(maxScore > mDetectThres)
    {
      CartCand cand;
      cand.mOrientation = maxOrien;
      std::vector<Vector2f> overtices;
      std::vector<Vector2f> vertices;
      overtices.clear();
      vertices.clear();
      (mCartModel(regionId,maxOrien)->mPolygonPtrs[0])->getVertices(overtices);
      (mCartModel(regionId,maxOrien)->mPolygonPtrs[5])->getVertices(vertices);

      for(int i = 0; i < vertices.size(); i ++)
      {
        overtices[i].x += (float)candX;
        overtices[i].y += (float)candY;
        vertices[i].x += (float)candX;
        vertices[i].y += (float)candY;
      }

      cand.mCentroid.x = (float)candX;
      cand.mCentroid.y = (float)candY;

      cand.mOuterPolygon.setVertices(overtices);
      cand.mCartPolygon.setVertices(vertices);
      cand.mScore = maxScore;
      mCandidates.push_back(cand);

      //for(int i = 0; i < vertices.size(); i++)
      // PVMSG("%f %f\n",vertices[i].x,vertices[i].y);
      //		  cand.mPolygon.draw(maskImage32, PV_RGB(255,255,0));
    }
  }



  removeOverlap(mCandidates);

  //  PVMSG("numCand after remove_overlap = %d\n",mCandidates.size());

  for(int i = 0; i < mCandidates.size(); i++)
  {
    PVMSG("%f \n",mCandidates[i].mScore);
  }

  // Store the detected carts as a vector of polygons
  for(int i = 0; i < mCandidates.size(); i ++)
  {
    carts.push_back(mCandidates[i].mCartPolygon);
  }
}


void 
CartDetector::removeOverlap(std::vector< CartCand > &candidates)
{
  CartCand temp;

  // Sort the candidates according to the scores
  std::vector< CartCand >::iterator kCand, jCand;
  for(kCand = candidates.begin(); kCand < candidates.end(); kCand++)
  {
    for(jCand = kCand + 1; jCand < candidates.end(); jCand++)
    {
      if ((*kCand).mScore < (*jCand).mScore)
      {
        temp = (*kCand);
        (*kCand) = (*jCand);
        (*jCand) = temp;
      }
    }
  }

  // Remove overlap

  std::vector< CartCand >::iterator iCand;
  for(kCand = candidates.begin(); kCand < candidates.end(); kCand++)
  {
    for(jCand = kCand + 1; jCand < candidates.end();)
    {
      int overlap = 0;

      Vector2f kPosTL((*kCand).mOuterPolygon.getBoundingBox(0).x,(*kCand).mOuterPolygon.getBoundingBox(0).y);
      Vector2f kPosBR((*kCand).mOuterPolygon.getBoundingBox(1).x,(*kCand).mOuterPolygon.getBoundingBox(1).y);
      Vector2f jCentroid((*jCand).mCentroid.x, (*jCand).mCentroid.y);

      // Check whether the j-th candidate centroid is inside the k-th candidate (outmost) polygon
      if(kPosTL.x <= jCentroid.x && jCentroid.x <= kPosBR.x && kPosBR.y <= jCentroid.y && jCentroid.y <= kPosTL.y)
      {
        // Removes the overlapping j-th candidate
        if(jCand != candidates.end())
          jCand = candidates.erase(jCand);
      }
      else
      {
        jCand ++;
      }

    }

  }


}   //end of removeOverlap(std::vector< CartCand > &candidates)


void 
CartDetector::initializeSettings(std::list<ait::SimplePolygon>& cartShapePolyList, std::list<ait::SimplePolygon>& cartRegionList,
                                 std::list<double>& cartOrientationList, float detectThres, float innerThres, float outerThres, 
								 float cornerThres, float inMargin, float outMargin, ait::Vector2f scaleFactor)
{
  mCartShapes.clear();
  for (std::list<SimplePolygon>::const_iterator iPoly = cartShapePolyList.begin(); iPoly != cartShapePolyList.end(); iPoly++)
  {
    SimplePolygon cartShapePoly;
	SimplePolygon scaledCartShapePoly;// = new SimplePolygon();
    cartShapePoly = *iPoly;

    PolygonPtr p(new Polygon());

    //Remove last redundant entry, polygon class does not use it
    cartShapePoly.pop_back();
	for(std::vector<ait::Vector3<float> >::const_iterator iPoint = cartShapePoly.begin(); iPoint != cartShapePoly.end(); iPoint++)
	{
		Vector3<float> polyPoint;
		polyPoint = *iPoint;
		polyPoint.x *= scaleFactor.x;
		polyPoint.y *= scaleFactor.y;
		//polyPoint.mult(scaleFactor);
		scaledCartShapePoly.push_back(polyPoint);
	}
    p->setVertices(scaledCartShapePoly);
    
    mCartShapes.push_back(p);
  }

int cartshapecount;
cartshapecount = 25;

/*  for (std::list<SimplePolygon>::const_iterator iPoly = cartRegionList.begin(); iPoly != cartRegionList.end(); iPoly++)
  {
    SimplePolygon cartRegionPoly;
    cartRegionPoly = *iPoly;

    PolygonPtr p(new Polygon());

    //Remove last redundant entry, polygon class does not use it
    cartRegionPoly.pop_back();
    p->setVertices(cartRegionPoly);  

    mCartRegionPtrs.push_back(p);
  }*/

  for (std::list<double>::const_iterator iOrien = cartOrientationList.begin(); iOrien != cartOrientationList.end(); iOrien++)
  {
    double orien;
    orien = *iOrien;
    mOrientations.push_back(orien);
  }

  mDetectThres = detectThres;
  mOuterThres = outerThres;
  mInnerThres = innerThres;
  mCornerThres = cornerThres;
  mInMargin = inMargin;
  mOutMargin = outMargin;
  

}

void
CartDetector::removeCarts(Image8& img, Image8& cartImg, std::vector<ait::Polygon>& detectedCartPolygons)
{

  //Dilate the convex images
  Image8 temp(img.width(),img.height());
  temp.setAll(0);



  //Now remove the carts from the given image

  for (int cartCount = 0; cartCount < detectedCartPolygons.size(); cartCount++)
  {
    
    //BUGFIX: DO NOT USE POLYGON::FILL due to memory leak, use getmask() instead
    detectedCartPolygons[cartCount].fill(temp, 255);
    detectedCartPolygons[cartCount].draw(temp, 255);
  }
  
  for(int a=0;a<temp.size();a++)
    if(temp(a)>0)
      img(a)=0;

  cartImg = temp;
}
//
// ACCESS
//

//
// INQUIRY
//

}; // namespace vision

}; // namespace ait


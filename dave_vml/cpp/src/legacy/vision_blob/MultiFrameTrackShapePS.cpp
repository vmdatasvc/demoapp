/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "legacy/vision_blob/MultiFrameTrackShapePS.hpp"

#include <legacy/low_level/Settings.hpp>
#include <legacy/vision_tools/ColorConvert.hpp>

namespace ait 
{

namespace vision
{


#define VALUE_X(i) (pValues[2*(i)])
#define VALUE_Y(i) (pValues[2*(i)+1])


void 
MultiFrameTrackShapePS::init(int id,
                      const std::string& settingsPath, 
                      const Vector2i& initPos,                      
                      double startTime,
                      double fps,
                      PersonShapeEstimatorPtr pPersonShapeEstimator,
                      Vector2i personShapeOffset,
                      int personDensityValue)
{
  
  MultiFrameTrackShape::init(id, settingsPath, initPos, startTime, fps, pPersonShapeEstimator, personShapeOffset);
  
  assert(mpPersonShapeEstimator.get());
  PersonShapeEstimator& personShape = *mpPersonShapeEstimator;

  Vector2i pos = initPos;
  //Rectanglei shapeRect = 
  //  personShape.getMaskRect(mSnake[0].x + mPersonShapeOffset.x,mSnake[0].y + mPersonShapeOffset.y);
  Rectanglei shapeRect = 
    personShape.getMaskRect(initPos.x + mPersonShapeOffset.x,initPos.y + mPersonShapeOffset.y);

  int sizeX = (float)shapeRect.width();
  int sizeY = (float)shapeRect.height();

  //PERSON SHAPE CHANGE END    


  const float x = pos.x;//mSnake[0].x;
  const float y = pos.y;//mSnake[0].y;

  setPersonPosition(Vector2f(x,y));


  Trajectory::NodePtr pNode(Trajectory::NodePtr(new Trajectory::Node(
    startTime, //replaces commented out next line
    //mStartTime+ mAge-mSnake.size(), //REPLACED WITH ACTUAL TIME INFORMATION
    Vector2f(x,y),
    Rectanglef(x-sizeX/2,y-sizeY/2,x+sizeX/2,y+sizeY/2))));


  assert(pNode);

  //DISABLE NEXT LINE FOR RELEASE MODE CRASH DEBUG
  //calculateNodeCardinality(*pNode,*((*mpForegroundHistory)[mpForegroundHistory->size()-mSnake.size()]));

  assert(mpTrajectory.get());
  mpTrajectory->getNodes().push_back(pNode);

  noUseTrackAge = 0;
  mCurrentUsesPersonPosition = true;
  mvPersonProbability.push_back(personDensityValue);
  mFreqHPP = 1;
  mRunLengthHPP = 1;
  mProbationaryTrack = true;

  mUsePredictedTrajectory = false;
}

void rotatePoint(Vector2f& rotationPoint, Vector2f centerPoint, float radianAngle)
{
  //First normalize the point wrt the center point
  rotationPoint.x = rotationPoint.x-centerPoint.x;
  rotationPoint.y = rotationPoint.y-centerPoint.y;

  Vector2f outputPt;

  outputPt.x = cos(radianAngle)*rotationPoint.x - sin(radianAngle)*rotationPoint.y;
  outputPt.y = sin(radianAngle)*rotationPoint.x + cos(radianAngle)*rotationPoint.y;

  rotationPoint.x = outputPt.x + centerPoint.x;
  rotationPoint.y = outputPt.y + centerPoint.y;
}

void
MultiFrameTrackShapePS::updateSearchRectangle()
{
  Vector2f predPos = getPredictedPos();
  Vector2f currentPos = getPos();

  Vector2f changePos;
  changePos.x = predPos.x - currentPos.x;
  changePos.y = predPos.y - currentPos.y;

  //Get the angle
  float angle = atan2(-changePos.y,changePos.x);
  //Convert angle to degrees
  //angle = angle*180.0/3.14159;

  //Get the length of the rectangle
  float modXY = sqrt(float(changePos.x*changePos.x+changePos.y*changePos.y));

  //PVMSG("\nmod = %f, angle = %f, change=%f,%f",modXY,angle,changePos.x,changePos.y);

  if(modXY<2)
  {
    if(changePos.x>0)
      angle=0.0f;
    else
      angle = 3.14159;
  }

  //modXY = modXY > 5.0? modXY : 5.0f;

  


  //Arbitrarily try out 3 times the modXY in front and 1 time behind centred at getPos()
  //width of the rectangle is fixed at 10 pixels

  //First draw a rectangle aligned with the x-y directions pointing in positive x axis direction
  Rectanglef tempRect;

  if(modXY<1)
  {
  tempRect.x0 = currentPos.x - 10;
  tempRect.y0 = currentPos.y - 10;
  tempRect.x1 = currentPos.x + 10;
  tempRect.y1 = currentPos.y + 10;
  }
  else
  {
    if(angle==0)
    {
      tempRect.x0 = currentPos.x - 5;
      tempRect.y0 = currentPos.y - 10;
      tempRect.x1 = currentPos.x + 15;
      tempRect.y1 = currentPos.y + 10;

    }
    else
    {
      tempRect.x0 = currentPos.x - 15;
      tempRect.y0 = currentPos.y - 10;
      tempRect.x1 = currentPos.x + 5;
      tempRect.y1 = currentPos.y + 10;
    }
  }

  //Now rotate all four points using the direction of motion angle with the getPos() as center
  Vector2f leftTop,rightTop,leftBottom,rightBottom;
  leftTop.set(tempRect.x0,tempRect.y0);
  rightTop.set(tempRect.x1,tempRect.y0);
  leftBottom.set(tempRect.x0,tempRect.y1);
  rightBottom.set(tempRect.x1,tempRect.y1);
  //rotatePoint(leftTop,currentPos,angle);
  //rotatePoint(rightTop,currentPos,angle);
  //rotatePoint(leftBottom,currentPos,angle);
  //rotatePoint(rightBottom,currentPos,angle);

  std::vector<Vector2f> tempVect;
  tempVect.push_back(leftTop);
  tempVect.push_back(rightTop);
  tempVect.push_back(rightBottom);
  tempVect.push_back(leftBottom);

  Polygon tempPolygon(tempVect);
  mSearchRectangle = tempPolygon;
}



void MultiFrameTrackShapePS::update(Image8Ptr pForeground, double currentTime)
{
  Vector2f& pos=getPersonPosition();
 // assert(mpForegroundHistory.get());
 // mpForegroundHistory->push_back(pForeground);  

 // const int dimension = mpForegroundHistory->capacity();
 // const int snakeSize = mpForegroundHistory->size();
 // const int maxSnakeSize = mCondensation.getDimension()/2;

 // if (mSnake.size() == maxSnakeSize)
 // {
 //   mCircularBufferFilled = true;
 // }  

 //  if (mCircularBufferFilled)
	//  {
 //       {
 //         // Shift the nodes of the 'snake'
	//	  for (int i = 1; i < mSnake.size(); i++)
 //         {
 //          mSnake[i-1] = mSnake[i];
 //         
 //         }
 //       }

 //       mSnake[mSnake.size()-1]=pos;
	//  }
 // else
 // {
	//  mSnake.push_back(pos);
 // }
 // 
 // for (int j = 0; j < mCondensation.getNumParticles(); j++)
 // {
 //   float *pValues = mCondensation.getParticle(j);
	//mCondensation.setParticleWeight(j,float(float((*pForeground)(pos.x,pos.y))/255.0f));
 //   
 //   assert(pValues);

 //   for (int i = 0; i < mSnake.size(); i++)
 //   {
 //     VALUE_X(i) = mSnake[i].x;
 //     VALUE_Y(i) = mSnake[i].y;
 //   }
 //   
 // }
 //  
    
    float sizeX = 0.0,sizeY = 0.0;

    //mpBlobSizeEstimate->getEstimatedSize(mSnake[0].x,mSnake[0].y,sizeX,sizeY);

    //PERSON SHAPE CHANGE BEGIN
    
    assert(mpPersonShapeEstimator.get());
    PersonShapeEstimator& personShape = *mpPersonShapeEstimator;

    //Rectanglei shapeRect = 
    //  personShape.getMaskRect(mSnake[0].x + mPersonShapeOffset.x,mSnake[0].y + mPersonShapeOffset.y);
    Rectanglei shapeRect = 
        personShape.getMaskRect(pos.x + mPersonShapeOffset.x,pos.y + mPersonShapeOffset.y);

    sizeX = (float)shapeRect.width();
    sizeY = (float)shapeRect.height();

    //PERSON SHAPE CHANGE END    

    
    const float x = pos.x;//mSnake[0].x;
    const float y = pos.y;//mSnake[0].y;

    
    Trajectory::NodePtr pNode(Trajectory::NodePtr(new Trajectory::Node(
      currentTime, //replaces commented out next line
      //mStartTime+ mAge-mSnake.size(), //REPLACED WITH ACTUAL TIME INFORMATION
      Vector2f(x,y),
      Rectanglef(x-sizeX/2,y-sizeY/2,x+sizeX/2,y+sizeY/2))));
    

    assert(pNode);

    //DISABLE NEXT LINE FOR RELEASE MODE CRASH DEBUG
    //calculateNodeCardinality(*pNode,*((*mpForegroundHistory)[mpForegroundHistory->size()-mSnake.size()]));
    
    assert(mpTrajectory.get());
    mpTrajectory->getNodes().push_back(pNode);
  
    mAge++;

    mvPersonProbability.push_back(mPersonDensityValue);
   

    if(mPersonProbabilityStatus)
    {
      mFreqHPP++;
      mRunLengthHPP++;
    }
    else
    {
      mRunLengthHPP = 0;
    }
    mPersonProbabilityStatus = false;

    mIsAssigned = false;


  
    //TO DO: Update the status of the track (probationary/confirmed) based on how it was initialized
    if(!mGoodTrackRegionStatus)
    {
      if((mFreqHPP>3 && mRunLengthHPP>3) || mAge > 5)
        mProbationaryTrack = false;
    }
    else
    {
      if((mFreqHPP>7 && mRunLengthHPP>7) || mAge > 15)
        mProbationaryTrack = false;

    }
    //updatePastPredictedPositions(mUsePredictedTrajectory);
    updateSearchRectangle();
}

void
MultiFrameTrackShapePS::updateCartBoundingBox()
{
  if(mCartBoundingBox.size()<10)
    mCartBoundingBox.push_back(mCartPolygon);
  else
  {
    std::vector<Polygon>::iterator iT=mCartBoundingBox.begin();
    iT = mCartBoundingBox.erase(iT);
    mCartBoundingBox.push_back(mCartPolygon);

  }
}

void
MultiFrameTrackShapePS::updatePastPredictedPositions(bool val)
{
  {    
    if(val==true && mUsePredictedTrajectory==false)
    {
      mUsePredictedTrajectory = val;
      mPastPredictedTrajectory = getPredictedPos();
      mPastPredictedMotion.x = getPredictedPos().x-getPos().x;
      mPastPredictedMotion.y = getPredictedPos().y-getPos().y;
    }
    else
    {
      if(val==true && mUsePredictedTrajectory==true)
      {
        mPastPredictedTrajectory.x = mPastPredictedTrajectory.x + mPastPredictedMotion.x;
        mPastPredictedTrajectory.y = mPastPredictedTrajectory.y + mPastPredictedMotion.y;

      }
      else
      {
        if(val==false && mUsePredictedTrajectory==true)
        {
          mUsePredictedTrajectory = val;
          mPastPredictedTrajectory = getPredictedPos();
          mPastPredictedMotion.x = getPredictedPos().x-getPos().x;
          mPastPredictedMotion.y = getPredictedPos().y-getPos().y;
        }
        else
        {
          mPastPredictedTrajectory = getPredictedPos();
          mPastPredictedMotion.x = getPredictedPos().x-getPos().x;
          mPastPredictedMotion.y = getPredictedPos().y-getPos().y;
        }
      }
    }
}
}


Vector2f
MultiFrameTrackShapePS::getPredictedPos()
{
  Trajectory::Nodes& nodes = getTrajectory()->getNodes();
  Trajectory::Nodes::iterator iNode = nodes.end();
  iNode--;
  Vector2f currPos = (*iNode)->location;

  int totalTrackSize = nodes.size();
  int temp = totalTrackSize;

  temp--;
  if(temp==0)
    return currPos;
  else
    iNode--;
  float xAvg = 0.0f;
  float yAvg = 0.0f;
  int totalPts = 0;

  Vector2f lastPos = (*iNode)->location;
  int count = 0;
  while(count<10)
  {
    lastPos = (*iNode)->location;
    //xAvg+=(currPos.x - lastPos.x);
    //yAvg+=(currPos.y - lastPos.y);
    totalPts+=1;
    //currPos = lastPos;
    temp--;
    if(temp==0)
      break;
    else
      iNode--;
    count++;
  }
  xAvg = currPos.x - lastPos.x;
  yAvg = currPos.y - lastPos.y;

 


  return Vector2f((xAvg/float(totalPts)+getPos().x),(yAvg/float(totalPts)+getPos().y));

  //if(mAge < 11)
  //{
  //  return getPos();
  //}
  //else
  //{
  //  //  return getPos();


  //  Trajectory::Nodes& nodes = getTrajectory()->getNodes();
  //  Trajectory::Nodes::iterator iNode = nodes.end();
  //  iNode--;

  //  float xAvg = 0.0f;
  //  float yAvg = 0.0f;
  //  int totalPts = 0;

  //  Vector2f currPos = (*iNode)->location;
  //  iNode--;

  //  if(nodes.size()>10)
  //  {
  //    for(int i=0;i<10;i++)
  //    {
  //      Vector2f lastPos = (*iNode)->location;
  //      xAvg+=(currPos.x - lastPos.x);
  //      yAvg+=(currPos.y - lastPos.y);
  //      totalPts+=1;
  //      currPos = lastPos;
  //      iNode--;
  //    }
  //  }
  //  else
  //  {
  //    for(;iNode!=nodes.begin();iNode--)
  //    {
  //      Vector2f lastPos = (*iNode)->location;
  //      xAvg+=(currPos.x - lastPos.x);
  //      yAvg+=(currPos.y - lastPos.y);
  //      totalPts+=1;
  //      currPos = lastPos;
  //    }
  //    Vector2f lastPos = (*iNode)->location;
  //    xAvg+=(currPos.x - lastPos.x);
  //    yAvg+=(currPos.y - lastPos.y);
  //    totalPts+=1;
  //  }

  //  return Vector2f(int(xAvg/float(totalPts)+getPos().x),int(yAvg/float(totalPts)+getPos().y));
  //}
}

void 
MultiFrameTrackShapePS::visualize(Image32& img)
{  
  
  //int i = 0;
  //int pi = 0;

  //float maxParticleWeight = 0;
  //int maxParticleIndex = 0;

  //for (pi = 0; pi < mCondensation.getNumParticles(); pi++)
  //{
  //  if (maxParticleWeight < mCondensation.getParticleWeight(pi))
  //  {
  //    maxParticleWeight = mCondensation.getParticleWeight(pi);
  //    maxParticleIndex = pi;
  //  }
  //}

  //for (pi = 0; pi < mCondensation.getNumParticles(); pi++)
  //{
  //  for (i = 1; i < mSnake.size(); i++)
  //  {
  //    float *pValues = mCondensation.getParticle(pi);
  //    img.line(
  //      VALUE_X(i-1),VALUE_Y(i-1),
  //      VALUE_X(i),VALUE_Y(i),
  //      PV_RGB((int)(255*mCondensation.getParticleWeight(pi)/maxParticleWeight),0,0));
  //  }
  //}

  //for (i = 1; i < mSnake.size(); i++)
  //{
  //  img.line(
  //    mSnake[i-1](0),mSnake[i-1](1),
  //    mSnake[i](0),mSnake[i](1),
  //    PV_RGB(255,255,0));
  //}

  //
  //if (mSnake.size() > 1)
  //{
  //  int idx = mSnake.size()-1;
  //  //float w,h;
  //  /*
  //  mpBlobSizeEstimate->getEstimatedSize(mSnake[idx].x,mSnake[idx].y,w,h);
  //  img.ellipse(mSnake[idx].x,mSnake[idx].y,w/2,h/2,PV_RGB(0,0,255));
  //  */

  //  //draw rect
  //  assert(mpPersonShapeEstimator.get());
  //  Rectanglei rect = mpPersonShapeEstimator->getMaskRect(mSnake[idx].x + mPersonShapeOffset.x,
  //                                                     mSnake[idx].y + mPersonShapeOffset.y);

  //  //now offset rect to count for translation of roi
  //  rect.x0 -= mPersonShapeOffset.x;
  //  rect.x1 -= mPersonShapeOffset.x;
  //  rect.y0 -= mPersonShapeOffset.y;
  //  rect.y1 -= mPersonShapeOffset.y;

  //  img.line(rect.x0, rect.y0, rect.x1, rect.y0, PV_RGB(0,0,255));
  //  img.line(rect.x0, rect.y1, rect.x1, rect.y1, PV_RGB(0,0,255));
  //  img.line(rect.x0, rect.y0, rect.x0, rect.y1, PV_RGB(0,0,255));
  //  img.line(rect.x1, rect.y0, rect.x1, rect.y1, PV_RGB(0,0,255));

  //  /*if(mCurrentUsesPersonPosition==true)
  //    img.ellipseFill(int(mSnake[idx].x),int(mSnake[idx].y),2,2,PV_RGB(255,0,0));
  //  else
  //    img.ellipseFill(int(mSnake[idx].x),int(mSnake[idx].y),2,2,PV_RGB(0,0,255));*/

  //  
  //}

 

}
  


//
// ACCESS
//

//
// INQUIRY
//

}; // namespace vision

}; // namespace ait


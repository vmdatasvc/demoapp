

#include <ait/Image.hpp>

#include <algorithm>
#include <iostream>
#include <list>
#include <vector>
#include <stack>

#include "legacy/vision_blob/TrackManager.hpp"

#include <legacy/pv/PvUtil.hpp>

#include <legacy/math/LineSegment.hpp>
#include <legacy/pv/ait/Vector3.hpp>

//
//#include <VideoWriterAVI.hpp>
//#include <VideoReaderAVI.hpp>

//#include <PvCanvasImage.hpp>

#include <legacy/low_level/XmlParser.hpp>

#include <boost/lexical_cast.hpp>
#include <boost/shared_ptr.hpp>

#include <string>
//


/*
 *	This structure encapsulates a trajectory with a score. It is used
 * to select the bets candidates for splits, merges, etc.
 */
struct Ranking
{
  ait::vision::TrajectoryPtr pTraj;
  float score;
};

bool cmpRank(const Ranking& a, const Ranking& b)
{
  return a.score < b.score;
}

namespace ait
{

namespace vision
{
  

void 
TrackManager::processCompact(std::list<TrajectoryPtr>& trajList)
{  
  std::list<TrajectoryPtr>::iterator iTraj;

  float compactThresh = getCompactThreshold();
  for (iTraj = trajList.begin(); iTraj != trajList.end(); )
  {
    (*iTraj)->compact(compactThresh);

    if ((*iTraj)->nodeCount() == 1)
    {      
      //PVMSG("\n\nERASING TRACK COMPACT: %f\n", compactThresh);
      iTraj = trajList.erase(iTraj);      
    }
    else
    {
      iTraj++;
    }
  }  
}

std::list<TrajectoryPtr>::iterator getTraj(int id, std::list<TrajectoryPtr>& trajList)
{
  std::list<TrajectoryPtr>::iterator iTraj;
  for (iTraj = trajList.begin(); iTraj != trajList.end(); iTraj++)
  {
    if ((*iTraj)->getId() == id)     
      break;    
  }
  return iTraj;
}


/*
TrackManager::ResultsPtr TrackManager::connectBroken(const TrackManager::SettingsPtr s)
{
  ResultsPtr pResults(new Results());
  
  std::list<TrajectoryPtr>::const_iterator iSource;
  std::list<TrajectoryPtr>::const_iterator iTarget;

  Vector2f sourceForwardDirection;
  Vector2f targetReverseDirection;

  Vector2f sourceEndPos;
  Vector2f targetStartPos;

  float targetStartTime;
  float sourceEndTime;

  Vector2f sourcePredictedPos;

  float distToExtension = 0.0;

 
  for (iSource = mTrajList.begin(); iSource != mTrajList.end(); iSource++)
  {
    std::vector<Ranking> candidates;
    candidates.clear();
    
    sourceForwardDirection = (*iSource)->forwardDirection(max(s->nodesConsidered(), 1));

    if (sourceForwardDirection == Vector2f(0.0,0.0))
      sourceForwardDirection = (*iSource)->forwardDirection(max(s->nodesConsidered() + 1, 1));
    if (sourceForwardDirection == Vector2f(0.0,0.0))
      sourceForwardDirection = (*iSource)->forwardDirection(max(s->nodesConsidered() - 1, 1));

    sourceEndPos = (*iSource)->lastNode()->location;

    for (iTarget = mTrajList.begin(); iTarget != mTrajList.end(); iTarget++)
    {
      if (iTarget == iSource)
          continue;

      //target trajectory must begin after the first     
      //if ((*iTarget)->firstNode()->time > (*iSource)->lastNode()->time ) 
      //{
        targetReverseDirection = (*iTarget)->reverseDirection(max(s->nodesConsidered(), 1));

        //
        //cout << sourceForwardDirection << " -> " << targetReverseDirection << endl;
        //

        float direction = 0.5 * (sourceForwardDirection.dot_prod(targetReverseDirection) + 1.0 );
        
        targetStartPos = (*iTarget)->firstNode()->location;

        //TODO: change 1000.0 part, use bounding box instead
        Vector2f segmentEnd;
        segmentEnd.x = sourceEndPos.x + sourceForwardDirection.x * 1000.0;
        segmentEnd.y = sourceEndPos.y + sourceForwardDirection.y * 1000.0;

        //cout << "Segment start: " << sourceEndPos << endl;
        //cout << "Segment end: " << segmentEnd << endl;

        LineSegment extension(sourceEndPos, segmentEnd);
        int pos = 0;
        distToExtension = extension.distanceToPoint(targetStartPos, pos);

        targetStartTime = (*iTarget)->firstNode()->time;
        sourceEndTime = (*iSource)->lastNode()->time;
        
        sourcePredictedPos.x = sourceEndPos.x + sourceForwardDirection.x * (targetStartTime - sourceEndTime);
        sourcePredictedPos.y = sourceEndPos.y + sourceForwardDirection.y * (targetStartTime - sourceEndTime);

        float predictedDiff = sqrt(dist2(sourcePredictedPos, targetStartPos));
        
        Ranking r;
        r.pTraj = *iTarget;
        r.score = 0.0;
        r.score = s->extensionProxWeight() * (1.0 / (distToExtension + 1.0));
        r.score += s->directionWeight() * direction;
        r.score += s->predictedPosWeight() * (1.0 / (predictedDiff + 1.0));
        r.score += s->timeBetweenWeight() * (1.0 / (1.0 + fabs((*iTarget)->firstNode()->time - (*iSource)->lastNode()->time)));        

        float distBetween = sqrt(dist2( (*iSource)->lastNode()->location, (*iTarget)->firstNode()->location));
        float timeDiff = (*iTarget)->firstNode()->time - (*iSource)->lastNode()->time;

        float tempSpeed = distBetween/timeDiff;
        
        float leaveSpeed = (*iSource)->forwardSpeed(s->nodesConsidered());

        r.score += s->speedWeight() * 1.0 / ( 1.0 + fabs(tempSpeed - leaveSpeed));

        if (r.score >= 20.0)         
          candidates.push_back(r);
      //}     
    }
    
    
    std::vector<Ranking>::iterator iCandidate;
    for (iCandidate = candidates.begin(); iCandidate != candidates.end(); iCandidate++)
    {
      cout << (*iCandidate).pTraj->getId() << ": " << (*iCandidate).score << endl;
    }
    


    if (candidates.size() > 0)
      std::sort(candidates.begin(), candidates.end(), cmpRank);

    if (candidates.size() > 0)
    {      
      Results::TuplePtr pTuple(new Results::Tuple);
      
      pTuple->from = (*iSource)->getId();
      pTuple->to = (candidates.back().pTraj->getId());     
      pResults->addConnect(pTuple); 
    }
  }

   //cout << "Time culled: " << timeCulled << endl;
 
  //temp
    
  //end temp  

  return pResults;
}
*/

void
TrackManager::processExtrapolate(const std::list<TrajectoryPtr>& trajList)
{   
  std::list<TrajectoryPtr>::const_iterator iTraj;

  std::vector<LineSegment> roiLines;
  roiLines.push_back(
    LineSegment(Vector2f(mRoi.x0, mRoi.y0), Vector2f(mRoi.x1, mRoi.y0)));
  roiLines.push_back(
    LineSegment(Vector2f(mRoi.x1, mRoi.y0), Vector2f(mRoi.x1, mRoi.y1)));
  roiLines.push_back(
    LineSegment(Vector2f(mRoi.x1, mRoi.y1), Vector2f(mRoi.x0, mRoi.y1)));
  roiLines.push_back(
    LineSegment(Vector2f(mRoi.x0, mRoi.y1), Vector2f(mRoi.x0, mRoi.y0)));

  float lineLength = mRoi.width()*mRoi.width() + mRoi.height()*mRoi.height();

  for (iTraj = trajList.begin(); iTraj != trajList.end(); iTraj++)  
  {
    LineSegment ls;
    Vector2f intersectPoint;
    bool intersectsRoiEdge = false;
    Vector2f dir;
    std::vector<LineSegment>::const_iterator iSide;
    float dist = 0.0;

    float blobScale = 0.0;    
    
    //Forward Direction    
    blobScale = mpBlobSizeEstimate->getBlobWidth(ls(0));

    dir = (*iTraj)->forwardDirection(blobScale);  
    
    if (dir.x != 0.0 || dir.y != 0.0) // only extrapolate if direction not a zero vector
    {
      ls(0) = (*iTraj)->lastNode()->location;
      ls(1).x = ls(0).x + dir.x * lineLength;
      ls(1).y = ls(0).y + dir.y * lineLength;    
    
      float forwardDistMax = blobScale * mMaxExtrapolateDistance;
    
      for (iSide = roiLines.begin(); iSide != roiLines.end(); iSide++)
        if (ls.intersects((*iSide), intersectPoint))
        {
          intersectsRoiEdge = true;
          break;
        }    
      if (intersectsRoiEdge)
      {    
        dist = sqrt(dist2((*iTraj)->lastNode()->location, intersectPoint));

        //PVMSG("INTERSECT POINT: %f %f\n", intersectPoint(0), intersectPoint(1));
        if (dist < forwardDistMax)
          (*iTraj)->extrapolateForward(intersectPoint, blobScale);    //TODO: change blob scale  
      }
    }    
      
    intersectsRoiEdge = false;
    //Reverse Direction
    blobScale = mpBlobSizeEstimate->getBlobWidth(ls(0));

    dir = (*iTraj)->reverseDirection(blobScale); 
    
    if (dir.x != 0.0 || dir.y != 0.0) //only extrapolate if dir is not a zero vector
    {    
      ls(0) = (*iTraj)->firstNode()->location;
      ls(1).x = ls(0).x + dir.x * lineLength;
      ls(1).y = ls(0).y + dir.y * lineLength;
    
      float reverseDistMax = blobScale * mMaxExtrapolateDistance;
    
      for (iSide = roiLines.begin(); iSide != roiLines.end(); iSide++)
        if (ls.intersects((*iSide), intersectPoint))
        {        
          intersectsRoiEdge = true;
          break;
        }    
      if (intersectsRoiEdge)
      {  
      
        dist = sqrt(dist2(ls(0), intersectPoint));
        //if (intersectPoint(0) < 0.0) intersectPoint(0) = 0.0;
        //if (intersectPoint(1) < 0.0) intersectPoint(1) = 0.0;

        if (dist < reverseDistMax)
          (*iTraj)->extrapolateReverse(intersectPoint, blobScale);
      }  
    }    
    
  }    
  
}

void 
TrackManager::process(std::list<TrajectoryPtr>& trajList)
{  
  //TODO: connect broken tracks 
  
  //TODO: TEMP DISABLED
  
  //processSpurious(trajList);
  /*
  processCompact(trajList);
  processSplit(trajList);
  processMerge(trajList);  
  */

  //TODO: FIX EXTRAPOLATION
  //processExtrapolate(trajList);
  
}

struct SettingsRank
{
  TrackManager::SettingsPtr pSettings;
  int score;
};


bool cmpSettingsRank(const SettingsRank& a, const SettingsRank& b)
{
  return a.score < b.score;
}


void
TrackManager::processSpurious(std::list<TrajectoryPtr>& trajList)
{  
  std::list<TrajectoryPtr>::iterator iTraj;

  for (iTraj = trajList.begin(); iTraj != trajList.end(); )
  {
    if ((*iTraj)->getEndTime() - (*iTraj)->getStartTime() <= getFps()*mSpuriousThreshold)
    {
      iTraj = trajList.erase(iTraj);
    }
    else
      iTraj++;
  }  
}


void 
TrackManager::processMerge(const std::list<TrajectoryPtr>& trajList)
{    
  //ResultsPtr pResults(new Results());

  std::list<TrajectoryPtr>::const_reverse_iterator iSource;
  std::list<TrajectoryPtr>::const_reverse_iterator iTarget;

  Vector2f sourceEndPos;
  float sourceEndTime = 0.0;
  Vector2f targetClosePos;
  
  for (iSource = trajList.rbegin(); iSource != trajList.rend(); iSource++)
  {
    std::vector<Ranking> candidates;
    candidates.clear();
    
    //set source stuff
    sourceEndPos = (*iSource)->lastNode()->location;
    sourceEndTime = (*iSource)->lastNode()->time;

    //sourceForwardDirection = (*iSource)->forwardDirection(s->nodesConsidered());    

    for (iTarget = trajList.rbegin(); iTarget != trajList.rend(); iTarget++)
    {
      if (iTarget == iSource)
          continue;

      //cull out directories that have no chance

      
      if ( sourceEndTime <= (*iTarget)->firstNode()->time 
        || sourceEndTime >= (*iTarget)->lastNode()->time)
        continue;

      float blobScale = mpBlobSizeEstimate->getBlobWidth(sourceEndPos);     

      //Calculate direction vectors
      Vector2f sourceDir = (*iSource)->forwardVector(2.0 * getFps());    
      Vector2f targetDir = (*iTarget)->forwardVectorAtTime(2.0 * getFps(), sourceEndTime);

      //if either of the vectors is a zero vector, do not split/mere
      if (sourceDir == Vector2f(0.0,0.0) || targetDir == Vector2f(0.0,0.0))
        continue;

      //if the vectors are not pointing in a similar direction, do not split/merge
      sourceDir.normalize();
      targetDir.normalize();      
      if (sourceDir.dot_prod(targetDir) < 0.0)
        continue;

      targetClosePos = (*iTarget)->nodeClosestTime(sourceEndTime)->location;
      float dist = sqrt(dist2 (targetClosePos, sourceEndPos));      
      float score = blobScale * getSplitMergeDistanceWeight() - dist;   
      if (score >= 0.0)
      {        
        Ranking r;
        r.pTraj = *iTarget;
        r.score = score;
        candidates.push_back(r);
      }
    }
    
    if (candidates.size() > 0)
    {
      TrajectoryPtr bestCandidate = candidates.begin()->pTraj;
      float bestScore = candidates.begin()->score;

      std::vector<Ranking>::const_iterator iCand;
      for (iCand = candidates.begin(); iCand != candidates.end(); iCand++)
      {
        if ((*iCand).score > bestScore)
        {
          bestScore = (*iCand).score;
          bestCandidate = (*iCand).pTraj;
        }
      }      
      //PVMSG("Merging: %f\n", bestScore);
      (*iSource)->merge(*bestCandidate);
    }   
  }      
}

void 
TrackManager::processSplit(const std::list<TrajectoryPtr>& trajList)
{   
  std::list<TrajectoryPtr>::const_iterator iSource;
  std::list<TrajectoryPtr>::const_iterator iTarget;

  Vector2f sourceStartPos;
  float sourceStartTime = 0.0;
  Vector2f targetClosePos;  
  
 
  for (iSource = trajList.begin(); iSource != trajList.end(); iSource++)
  {
    std::vector<Ranking> candidates;
    candidates.clear();
    
    //set source stuff
    sourceStartPos = (*iSource)->firstNode()->location;
    sourceStartTime = (*iSource)->firstNode()->time;

    for (iTarget = trajList.begin(); iTarget != trajList.end(); iTarget++)
    {
      if (iTarget == iSource)
          continue;

      //cull out directories that have no chance
      
      if ( sourceStartTime <= (*iTarget)->firstNode()->time
        || sourceStartTime >= (*iTarget)->lastNode()->time)
        continue;

      float blobScale = mpBlobSizeEstimate->getBlobWidth(sourceStartPos);

      //Calculate direction vectors
      Vector2f sourceDir = (*iSource)->reverseVector(2.0 * getFps());
      sourceDir.mult(-1.0);
      Vector2f targetDir = (*iTarget)->forwardVectorAtTime(2.0 * getFps(), sourceStartTime);

      //if either of the vectors is a zero vector, do not split/mere
      if (sourceDir == Vector2f(0.0,0.0) || targetDir == Vector2f(0.0,0.0))
        continue;

      //if the vectors are not pointing in a similar direction, do not split/merge
      sourceDir.normalize();
      targetDir.normalize();      
      if (sourceDir.dot_prod(targetDir) < 0.0)
        continue;


      //calculate a distance score
      targetClosePos = (*iTarget)->nodeClosestTime(sourceStartTime)->location;
      float dist = sqrt(dist2(targetClosePos, sourceStartPos));
      
      
      float score = blobScale * getSplitMergeDistanceWeight() - dist;
      
      if (score >= 0.0)
      {
        Ranking r;
        r.pTraj = *iTarget;
        r.score = score;        
        candidates.push_back(r);
      }      
    }
    
    if (candidates.size() > 0)
    {
      TrajectoryPtr bestCandidate = candidates.begin()->pTraj;
      float bestScore = candidates.begin()->score;

      std::vector<Ranking>::const_iterator iCand;
      for (iCand = candidates.begin(); iCand != candidates.end(); iCand++)
      {
        if ((*iCand).score > bestScore)
        {
          bestScore = (*iCand).score;
          bestCandidate = (*iCand).pTraj;
        }
      }
      //PVMSG("SPLITTING: %f\n", bestScore);
      (*iSource)->split(*bestCandidate);
    }
  }       
}


} // namespace vision
} // namespace ait

/*
int main()
{   
  //makeTrajectoryVideo("bestbuy.csv", "c:/VmsVideos/20031128_200001.avi", "bestbuytraj.avi");  
 
  std::list<TrajectoryPtr> trajList = Trajectory::loadCSV("bestbuy.csv");

  //cout << "Size: " << trajList.size() << endl;
  
  TrackManager tm(trajList); 

  TrackManager::SettingsPtr s(new TrackManager::Settings());
  s->splitMergeDistanceWeight = 50000.0;  
  s->compactThreshold = 1.0;


  tm.useSettings(s);


  //tm.compact(1.0);
  tm.process();

  
  //s->randomizeAll();

  TrackManager::ResultsPtr res;
  //TrackManager::Results correctResults = TrackManager::Results::readFromFile("results.xml"); 
  
 
  
  //res = tm.merge(s);

  cout << "----------RESULTS----------" << endl;
  res->print();

  */
   
  /*
  cout << "------------------------" << endl;
  cout << "Comparison: " << res->compare(correctResults) << endl;
  */


  
  
  /*
  srand((unsigned int)PvUtil::time());

  const int individuals = 100;
  const int randomized = 90;
  const int maxGenerations = 100;

  const float mutationChance = 1.0F;

  const float compactThreshold = 5.0;

  TrackManager tm(Trajectory::loadCSV("a.csv"));
  TrackManager::Results correctResults = TrackManager::Results::readFromFile("results.xml");
  tm.compact(compactThreshold);

  vector<SettingsRank> org;

  //initialize the settings
  int i = 0;

  for (i = 0; i < individuals; i++)
  {
    TrackManager::SettingsPtr s(new TrackManager::Settings());
    SettingsRank setRank;
    s->randomizeAll();

    setRank.pSettings = s;
    setRank.score = 0;
    org.push_back(setRank);
    //s->print();
    //cout << "---" << endl;
  }

  TrackManager::ResultsPtr res;

  for (int genNum = 0; genNum < maxGenerations; genNum++)
  {
    //cout << "Generating scores" << endl;
    //generate scores
    for (int orgNum = 0; orgNum < org.size(); orgNum++)
    {
      //cout << "Generating results" << endl;
      res = tm.connectBroken(org[orgNum].pSettings);
      //cout << "Results Generatred" << endl;
      org[orgNum].score = res->compare(correctResults);
      //cout << "Compared results" << endl;
    }

    //cout << "Sorting by score" << endl;

    //sort by score, low scores = good here
    std::sort(org.begin(), org.end(), cmpSettingsRank);

    for (orgNum = 0; orgNum < randomized; orgNum++)
    {
      if (randomRange(0,1) < mutationChance)
      {        
        org[orgNum].pSettings->mutate();  
      }
    }

    for (orgNum = individuals - randomized; orgNum < org.size(); orgNum++)
    {
      org[orgNum].pSettings->randomizeAll();
    }

    std::cout << "Top Score: " << org[0].score << endl;
  }

  */  
  
//	return 0;
//}

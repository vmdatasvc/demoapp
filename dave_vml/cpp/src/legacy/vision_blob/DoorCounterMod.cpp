/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#pragma warning(disable:4503)
#endif


#include "legacy/vision_blob/DoorCounterMod.hpp"

//#include "TrajectoryEvents.hpp"
#include "legacy/vision_blob/BlobTrackerMod.hpp"
#include "legacy/vision_blob/MultiFrameTrack.hpp"
//#include "ImageAcquireMod.hpp"
//#include "TrajectoryEvents.hpp"
#include "legacy/vision_blob/BlobSizeEstimate.hpp"
//#include <legacy/pv/PvCanvasImage.hpp>
#include <boost/lexical_cast.hpp>
#include "legacy/vision_blob/ForegroundSegmentMod.hpp"
#include "legacy/vision_tools/ColorConvert.hpp"
#include "legacy/vision_blob/Trajectory.hpp"
#include "legacy/vision_blob/SimplePolygon.hpp"

#include "legacy/vision_tools/PolygonGetMask.hpp"

#include "legacy/types/strutil.hpp"
#include <legacy/types/DateTime.hpp>

#include "legacy/math/Polygon.hpp"

namespace ait 
{

namespace vision
{

int test_int_code(int t)
{
	return t;
}
#if 0
/////////////// TEMPORARY HACK FOR DEMO CODE, SUBCLASS MODULE LATER WHEN YOU HAVE TIME
  static void drawText(Image32& img0,
                     int x,
                     int y,
                     const std::string& txt,
                     bool transparent = false)
{
#ifdef WIN32
  HFONT hfnt, hOldFont;
  HDC hdc = CreateCompatibleDC(NULL);

  hfnt = (HFONT)GetStockObject(ANSI_FIXED_FONT); 
  if (hOldFont = (HFONT)SelectObject(hdc, hfnt)) 
  {
    SIZE size;
    GetTextExtentPoint(hdc,txt.c_str(),txt.length(),&size);
    SelectObject(hdc, hOldFont); 
    DeleteDC(hdc);
    
    Uint32 magicColor = PV_RGB(1,1,1);
    PvCanvasImage img(size.cx+2,size.cy+2);
    img.setAll(magicColor);
    hdc = img.dc();
    if (hOldFont = (HFONT)SelectObject(hdc, GetStockObject(ANSI_FIXED_FONT))) 
    {
      POINT pos = { 1 , 1 };
      SetBkMode(hdc,TRANSPARENT);
      if (transparent)
      {
        SetTextColor(hdc,RGB(0,0,0));
        for (int x0 = -1; x0 <= 1; x0++)
        {
          for (int y0 = -1; y0 <= 1; y0++)
          {
            TextOut(hdc, pos.x+x0, pos.y+y0, txt.c_str(), txt.length()); 
          }
        }
      }
      SetTextColor(hdc,RGB(255,255,255));
      TextOut(hdc, pos.x, pos.y, txt.c_str(), txt.length()); 
      SelectObject(hdc, hOldFont); 

      // If transparent, must paste with key color...
      img0.paste(img,x-img.width()/2,y-img.height()/2);
    }
  } 

  GdiFlush();
#endif  
}
#endif

DoorCounterMod::DoorCounterMod()
: BlobTrackerMod(getModuleStaticName())
{  
  
}

DoorCounterMod::~DoorCounterMod()
{
}

void 
DoorCounterMod::init(Settings &set)
{
  BlobTrackerMod::init();

  mSettings = set;

  mSettings.changePath("DoorCounterMod");

  mEnterDirection = mSettings.getCoord2f("enterDirection",Vector2f(-1.0,-1.0));
//  mEnterDirection = Vector2f(-1.f,-1.f);
  mEnterDirection.normalize();

  //Extract door location information from settings
  std::string doorString = mSettings.getString("doorLocation","-1,-1,-1,-1");
//  std::string doorString = "-1,-1,-1,-1";
  SimplePolygon doorPoints;
  convert(doorString,doorPoints);

  if (doorPoints.size() != 2)
  {
      // throw std error
      throw std::runtime_error("DoorCounterMod: doorLocation.size() != 2");
    //PvUtil::exitError("DoorCounterMod: doorLocation.size() != 2");
  }

  mDoorLocation(0) = doorPoints[0];
  mDoorLocation(1) = doorPoints[1];
  
  // hard code parameters for a moment
  // parameters are in vision_modules/settings.xml
  // if not there, use the default values marked here

  mMinTrackDisplacement = mSettings.getFloat("minTrackDisplacement", 0.5f);
//  mMinTrackDisplacement = 0.5f;

  mMaxStartDistance = mSettings.getFloat("maxStartDistance",1.0f);
//  mMaxStartDistance = 1.0f;

  mMaxEndDistance = mSettings.getFloat("maxEndDistance",1.0f);
//  mMaxEndDistance = 1.f;


  float angle = mSettings.getFloat("maxDirectionAngle", 60.0f);
//  float angle = 78.f; // from vision_modules/settings.xml

  const double pi = 3.14159265;
  
  mMaxDirectionDotProduct = cos(angle * (pi/180.0));

  //Load full size BlobSizeEstimator for classification of tracks
  // TODO: find out the BlobSizeEstimate directory. load it manually
//  Settings fsmSettings("VisionMods/" + getExecutionPathName() + "/ForegroundSegmentMod");
  mpUnscaledBlobSizeEstimate.reset(new BlobSizeEstimate());
//  mpUnscaledBlobSizeEstimate->load(fsmSettings.getString("BlobSizeEstimator/fileName",""));
  Settings fsmSettings;
  fsmSettings.changePath("BlobSizeEstimator");
  mpUnscaledBlobSizeEstimate->load(fsmSettings.getString("fileName"));

  //Determine if we want additional debug information
  mAdditionalDebugInformation = mSettings.getBool("additionalDebugInformation [bool]", false);
//  mAdditionalDebugInformation = false;

  //Determine how measurements wrt. the door will be made
  mTreatDoorAsPoint = mSettings.getBool("treatDoorAsPoint [bool]", true);
//  mTreatDoorAsPoint = true;
}

void
DoorCounterMod::finish()
{
  BlobTrackerMod::finish();
}


void
DoorCounterMod::cleanForegroundSegmentation(Image8& img, const Vector2f& pt)
{
  BlobSizeEstimate& blobSizeEstimate = *(mpForegroundSegmentMod->getBlobSizeEstimate());
  float sizeX,sizeY;
  blobSizeEstimate.getEstimatedSize(pt.x,pt.y,sizeX,sizeY);

  // Expand considerably to avoid another track being created in this same
  // frame for the same blob.
  
  const float expandBy = 1.2f; //ORIGINAL 1.2f
  img.ellipseFill(
    round(pt.x),
    round(pt.y),
    sizeX*expandBy/2,
    sizeY*expandBy/2,
    0);    
}

void
DoorCounterMod::checkOverlappingTracks()
{
  
  BlobSizeEstimate& blobSizeEstimate = *(mpForegroundSegmentMod->getBlobSizeEstimate());

  // This parameter can be changed it should not be too big.
  const int numOverlapFrames = 3;  
  
  PVASSERT(mMaxSnakeSize >= numOverlapFrames);
 
  Tracks::iterator iTrack;
  for (iTrack = mTracks.begin(); iTrack != mTracks.end(); iTrack++)
  {
    if ((*iTrack)->getAge() < numOverlapFrames || !(*iTrack)->isAlive()) continue;
    Tracks::iterator iTrack2 = iTrack;
    for (iTrack2++; iTrack2 != mTracks.end(); iTrack2++)
    {
      if ((*iTrack2)->getAge() < numOverlapFrames || !(*iTrack2)->isAlive()) continue;
      const MultiFrameTrack::Snake& snake1 = (*iTrack)->getCurrentSnake();
      const MultiFrameTrack::Snake& snake2 = (*iTrack2)->getCurrentSnake();
      float totalDistance = 0;
      for (int frame = 0; frame < numOverlapFrames; frame++)
      {
        const Vector2f p1 = snake1[snake1.size()-frame-1];
        const Vector2f p2 = snake2[snake2.size()-frame-1];
        float dist = sqrt(dist2(p1,p2));
        // Get the blob size at the average point.
        float x = (p1.x+p2.x)/2;
        float y = (p1.y+p2.y)/2;
        float sizeX, sizeY;
        blobSizeEstimate.getEstimatedSize(x,y,sizeX,sizeY);
        // Normalize distance by blob size
        totalDistance += dist/sizeX;
      }
      // The normalized distance has the blob width as the
      // unit.
      const float overlappingDistanceThreshold = 0.5f;//0.5f; // ORIGINALLY 1.0
      totalDistance /= numOverlapFrames;
      if (totalDistance < overlappingDistanceThreshold)
      {
        if (mVerbose)
        {
          PVMSG("### Tracks Overlap (%d,%d) [frame: %d]\n",(*iTrack)->getId(),(*iTrack2)->getId(),-1);
        }
        
        // Kill the youngest track.
        /*
        if ((*iTrack2)->getAge() < (*iTrack)->getAge())
        {
          (*iTrack2)->kill();
        }
        else
        {
          (*iTrack)->kill();
        }
        */
      }
    }
  }
  
}

bool 
DoorCounterMod::isEnterTrajectory(const TrajData& td) const
{
  bool correctDirection = false;
  bool minDisp = false;
  float closeToDoor = false;

  Vector2f dir = td.getDirection();
  
  if (dir.x != 0.0 || dir.y != 0.0)
  {     
    correctDirection = td.getDirWithDoorDotProduct() > mMaxDirectionDotProduct;

    //If total displacement (normalized by blob size) is > than specified amt
    if (td.getDisplacement() > mMinTrackDisplacement)
      minDisp = true;

    if (td.getStartDispFromDoor() < mMaxStartDistance)
      closeToDoor = true;    
  }

  return minDisp && correctDirection && closeToDoor;  
}

bool 
DoorCounterMod::isExitTrajectory(const TrajData& td) const
{

  bool correctDirection = false;
  bool minDisp = false;
  float closeToDoor = false;

  Vector2f dir = td.getDirection();

  if (dir.x != 0.0 || dir.y != 0.0)
  {     
    correctDirection = td.getDirWithDoorDotProduct() < -mMaxDirectionDotProduct;

    //If total displacement (normalized by blob size) is > than specified amt
    if (td.getDisplacement() > mMinTrackDisplacement)
      minDisp = true;

    if (td.getEndDispFromDoor() < mMaxEndDistance)
      closeToDoor = true;    
  }

  return minDisp && correctDirection && closeToDoor;    
}


Vector2f 
DoorCounterMod::pointRepulsion(const Vector2f& p1, const Vector2f& p2)
{

  float w = 0.0;
  float h = 0.0;
  mpForegroundSegmentMod->getBlobSizeEstimate()->getEstimatedSize(p1.x,p1.y,w,h);

  //float d = sqrt(dist2(p1,p2));

  Vector2f vec;

  vec.x = p1.x - p2.x;
  vec.y = p1.y - p2.y;

  //divide by blob width,height
  vec.x /= w;
  vec.y /= h;

  //Special case, return 0.0 and let caller deal with it
  if (vec.x == 0.0 && vec.y == 0.0)
  {
    return vec;
  }

  float d = sqrt(vec.x*vec.x + vec.y*vec.y);

  //return no force if points are > then specified distance apart
  if (d >= 2.0)
  {
    return Vector2f(0.0,0.0);
  }

  PVASSERT(d != 0.0);

  vec.normalize();

  //scale force vector based on distance

  vec.mult(1.0/d);
  
  return vec;
}

Vector2f 
DoorCounterMod::calculateTrackRepulsion(MultiFrameTrackPtr pTrack)
{

  Vector2f vec(0.0,0.0);

  Vector2f pos = pTrack->getPos();

  
  Tracks::iterator iTrack;

  for (iTrack = mTracks.begin(); iTrack != mTracks.end(); iTrack++)
  {
    if (*iTrack != pTrack)
    {
      Vector2f f = pointRepulsion(pos, (*iTrack)->getPos());
      
      vec.x += f.x;
      vec.y += f.y;
    }
  }
  
  return vec;
}

void 
DoorCounterMod::calculateTrackForce()
{
  Tracks::iterator iTrack;

  for (iTrack = mTracks.begin(); iTrack != mTracks.end(); iTrack++)
  {
    Vector2f vec = calculateTrackRepulsion(*iTrack);

    //Vector2f vec;    
    
    //vec.x += -0.0;
    //vec.y += 0.0;
    
    float val = 0.6f;

    vec.x = std::min(vec.x, val);
    vec.x = std::max(vec.x, -val);
    
    vec.y = std::min(vec.y, val);
    vec.y = std::max(vec.y, -val);   
    

    (*iTrack)->setForceVector(vec);  
  }
}

void 
DoorCounterMod::generateExitEvent(const TrajData& td)
{
  const TrajectoryPtr pTraj = td.getTrajectory();
  
  double endTime = pTraj->getEndTime();
  double duration = 1.0;

//  mpVmsPacket->beginEventTime(endTime,duration,mEventChannels.front());
//  mpVmsPacket->addAttribute("1","1");

//  if (mAdditionalDebugInformation)
//  {
//    appendDebugInformation(td);
//  }
//  mpVmsPacket->endEvent();

  mExitTrajectories.push_back(pTraj);
}

void 
DoorCounterMod::generateEnterEvent(const TrajData& td)
{
  TrajectoryPtr pTraj = td.getTrajectory();

  double startTime = pTraj->getStartTime();
  double duration = 1.0;

//  mpVmsPacket->beginEventTime(startTime,duration,mEventChannels.front());
//  mpVmsPacket->addAttribute("1","0");\
//
//  if (mAdditionalDebugInformation)
//  {
//    appendDebugInformation(td);
//  }
//  mpVmsPacket->endEvent();

  mEnterTrajectories.push_back(pTraj);
}

void 
DoorCounterMod::generateUnknownEvent(const TrajData& td)
{
  TrajectoryPtr pTraj = td.getTrajectory();
  
  double startTime = pTraj->getStartTime();
  double duration = 1.0;

//  mpVmsPacket->beginEventTime(startTime,duration,mEventChannels.front());
//  mpVmsPacket->addAttribute("1","2");
//
//  if (mAdditionalDebugInformation)
//  {
//    appendDebugInformation(td);
//  }
//  mpVmsPacket->endEvent();

  mUnknownTrajectories.push_back(pTraj);  
}

void DoorCounterMod::appendDebugInformation(const TrajData& td)
{
//  //2 displacement
//  std::string disp = boost::lexical_cast<std::string>(td.getDisplacement());
//  mpVmsPacket->addAttribute("2", disp);
//
//  //3 StartDispFrom Door
//  std::string startDispFromDoor = boost::lexical_cast<std::string>(td.getStartDispFromDoor());
//  mpVmsPacket->addAttribute("3", startDispFromDoor);
//
//  //4 EndDispFrom Door
//  std::string endDispFromDoor = boost::lexical_cast<std::string>(td.getEndDispFromDoor());
//  mpVmsPacket->addAttribute("4", endDispFromDoor);
//
//  //5 DirDotProduct
//  std::string dirDotProd = boost::lexical_cast<std::string>(td.getDirWithDoorDotProduct());
//  mpVmsPacket->addAttribute("5", dirDotProd);
//
//  //6 xDir
//  Vector2f dir = td.getDirection();
//  std::string xDir = boost::lexical_cast<std::string>(dir.x);
//  mpVmsPacket->addAttribute("6", xDir);
//
//  //7 yDir
//  std::string yDir = boost::lexical_cast<std::string>(dir.y);
//  mpVmsPacket->addAttribute("7", yDir);
//
//  //8 trajectory string
//  std::string trajString = td.getTrajectory()->toXmlString();
//  mpVmsPacket->addLongStringAttribute("8", trajString);
}

void 
DoorCounterMod::compressTraj(std::list<TrajectoryPtr>& trajList)
{
  //Compress trajectories to save space
  std::list<TrajectoryPtr> compactedTraj;  
  
  std::list<TrajectoryPtr>::iterator iTraj;
  
  //Because the tracks are in image coordinates, use the unscaled
  // blob size estimator
  double compressDist = 
        mpUnscaledBlobSizeEstimate->getCenterWidth()/3.0;

  for (iTraj = trajList.begin(); iTraj != trajList.end(); iTraj++)
  {      
    TrajectoryPtr pTraj = (*iTraj)->compress(compressDist,5.0);      

    if (pTraj->nodeCount() > 1)
      compactedTraj.push_back(pTraj);
  }   

  trajList.clear();

  trajList = compactedTraj;
} 

void 
DoorCounterMod::postProcessTracks(bool force)
{

  double currentTime = getCurrentTime();
//	double currentTime = mpImageAcquireMod->getCurrentFrameTime();


  if (force || currentTime >= mNextForcePostProcessTime)
  {
    
    DateTime dt(currentTime);
    std::string datestring = dt.format("%Y%m%d_%H%M%S");

    //save enter trajectories
    if (mEnterTrajectories.size() > 0)
    {

      if (mSavePreTrajectories)
      {      
        std::string filename = "TrajEnter_" + datestring + ".csv";
        std::string loc = ait::combine_path(mTrajectoryOutputFolder, filename);

        //compressTraj(mEnterTrajectories);
        if (mEnterTrajectories.size() > 0)
          Trajectory::saveXmlCsv(loc, mEnterTrajectories);
      }

      mEnterTrajectories.clear();
    }    

    //save exit trajectories
    if (mExitTrajectories.size() > 0)
    {
      if (mSavePreTrajectories)
      {      
        std::string filename = "TrajExit_" + datestring + ".csv";
        std::string loc = ait::combine_path(mTrajectoryOutputFolder, filename);

        //compressTraj(mExitTrajectories);
        if (mExitTrajectories.size() > 0)
          Trajectory::saveXmlCsv(loc, mExitTrajectories);
      }

      mExitTrajectories.clear();
    }    

    //save unknown trajectories
    if (mUnknownTrajectories.size() > 0)
    {
      if (mSavePreTrajectories)
      {        
        std::string filename = "TrajUnknown_" + datestring + ".csv";
        std::string loc = ait::combine_path(mTrajectoryOutputFolder, filename);

        //compressTraj(mUnknownTrajectories);
        if (mUnknownTrajectories.size() > 0)
          Trajectory::saveXmlCsv(loc, mUnknownTrajectories);
      }

      mUnknownTrajectories.clear();
    }
    //
    mNextForcePostProcessTime = currentTime + mPostProcessUpperInterval;
  }   
}

void 
DoorCounterMod::process()
{  
  //calculate repulsion force for all tracks
  calculateTrackForce();
  
  //compress trajectories for transport/storage
  compressTraj(mPreTrajectories);
    
  //Check for trajectories that have yet to be classified
  std::list<TrajectoryPtr>::iterator iTraj;
  for (iTraj = mPreTrajectories.begin(); iTraj != mPreTrajectories.end(); iTraj++)
  {
    //make sure the trajectory has at least 2 nodes
    if ((*iTraj)->nodeCount() < 2)
      continue;    

    
    assert(mpUnscaledBlobSizeEstimate.get());
    TrajData td(mDoorLocation, mTreatDoorAsPoint, mEnterDirection, mpUnscaledBlobSizeEstimate);    
    td.extractData(*iTraj);    

    if (isEnterTrajectory(td))
    {
      generateEnterEvent(td);
    }    
    else if (isExitTrajectory(td))
    {
      generateExitEvent(td);
    }
    else //falls into 'other' group
    {
      generateUnknownEvent(td);
    }        
  }
  mPreTrajectories.clear();  
  
  BlobTrackerMod::process();
}

void
DoorCounterMod::visualize(Image32& img)
{
  BlobTrackerMod::visualize(img);
}


//
// ACCESS
//

//
// INQUIRY
//

}; // namespace vision

}; // namespace ait


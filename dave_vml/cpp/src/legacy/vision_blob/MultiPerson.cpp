/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif


#include <boost/lexical_cast.hpp>

#include <fstream>

#include <legacy/types/strutil.hpp>

#include <legacy/low_level/Settings.hpp>

#include "legacy/vision_blob/MultiPerson.hpp"


#ifdef WIN32
#include <windows.h>
#include <shellapi.h>
#endif

namespace ait 
{

namespace vision
{

MultiPerson::MultiPerson()
: mIsInitialized(false)
{
  mPersonCount = 0;
}

MultiPerson::~MultiPerson()
{
  //resetAll();
}

void 
MultiPerson::init(const std::string& settingsPath, BlobSizeEstimatePtr pBlobSize, int width, int height)
{
  // Initialize this object.
  mIsInitialized = true;

  mpBlobSizeEstimate = pBlobSize;

  mSceneWidth = width;
  mSceneHeight = height;

  Settings xmlSettings(settingsPath);  

  //Initialize all relevant settings
  mOutputFolder = xmlSettings.getString("outputFolder", ""); 

  //The following settings are used for debugging purposes:
  
  //save final output to finalTrajectories.csv in the output folder
  mSaveFinalTrajs = xmlSettings.getBool("saveFinalTrajectories [bool]", false);

  //save images of all input trajectories as input.png in the output folder
  mSaveInputImages = xmlSettings.getBool("saveInputImages [bool]", false);

  //Save a series of snapshots in time of the trajectories as they are being processed
  mSaveTrajImages = xmlSettings.getBool("saveTrajectoryImages [bool]", false);

  //Save a series of graphs generated via graphviz representing each step in time
  // This is very slow. Processing large numbers of trajectories at once may hang the computer
  mSaveGraphImages = xmlSettings.getBool("saveGraphImages [bool]", false);

  //Save an image of the culled trajectories (culled.png) in the output folder
  mSaveCulledTrajImages = xmlSettings.getBool("saveCulledImage [bool]", false);

  //Save an image of the good trajectories (good.png) in the output folder
  mSaveGoodTrajImages = xmlSettings.getBool("saveGoodImage [bool]", false);

  //Save an image for each person path (person<xxx>.png) in the output folder
  mSavePersonPaths = xmlSettings.getBool("savePersonPathImages [bool]", false);


  if (mSaveFinalTrajs || mSaveInputImages || mSaveTrajImages || 
    mSaveGraphImages || mSaveCulledTrajImages || mSaveGoodTrajImages ||
    mSavePersonPaths)
  {     
    if (!PvUtil::fileExists(mOutputFolder.c_str()))  
      PvUtil::exitError("MultiPerson/outputFolder path: '%s' does not exist.\n", mOutputFolder.c_str());
  }


  mVerbose = xmlSettings.getBool("verbose [bool]", false);

  mMinTrajAge = 1.0;
  
  //factor of avg blob size of start and finish points for each trajectory.
  // Shorter lengths will be culled.
  mMinTrajLength = xmlSettings.getFloat("minLengthFactor", 2.0f);

  //The maximum distance considered for splits and merges
  mMaxSplitDistance = xmlSettings.getFloat("maxSplitDistance", 1.0);
  mMaxMergeDistance = xmlSettings.getFloat("maxMergeDistance", 1.0);

  //Define some colors for the visualizations
  mColor.push_back(PV_RGB(255,0,0));
  mColor.push_back(PV_RGB(204,0,0));
  mColor.push_back(PV_RGB(153,0,0));
  mColor.push_back(PV_RGB(102,0,0));

  mColor.push_back(PV_RGB(0,255,0));
  mColor.push_back(PV_RGB(0,204,0));
  mColor.push_back(PV_RGB(0,153,0));
  mColor.push_back(PV_RGB(0,102,0));

  mColor.push_back(PV_RGB(0,0,255));
  mColor.push_back(PV_RGB(0,0,204));
  mColor.push_back(PV_RGB(0,0,153));
  mColor.push_back(PV_RGB(0,0,102));

  mColor.push_back(PV_RGB(255,255,0));
  mColor.push_back(PV_RGB(204,204,0));
  mColor.push_back(PV_RGB(153,153,0));
  mColor.push_back(PV_RGB(102,102,0));

  mColor.push_back(PV_RGB(255,0,255));
  mColor.push_back(PV_RGB(204,0,204));
  mColor.push_back(PV_RGB(153,0,153));
  mColor.push_back(PV_RGB(102,0,102));

  mColor.push_back(PV_RGB(0,255,255));
  mColor.push_back(PV_RGB(0,204,204));
  mColor.push_back(PV_RGB(0,153,153));
  mColor.push_back(PV_RGB(0,102,102));  

}

//
// OPERATIONS
//

std::vector<TrajectoryPtr> 
MultiPerson::removeSpuriousTrajectories(const std::list<TrajectoryPtr>& trajList)
{

  std::vector<TrajectoryPtr> goodTrajectories;  
  std::vector<TrajectoryPtr> culledTrajectories;  

  std::list<TrajectoryPtr>::const_iterator iTrajList;
  
  //save an image of the input trajectories before culling
  if (mSaveInputImages)
  {
    Image32 img(mSceneWidth,mSceneHeight);
    img.setAll(PV_RGB(255,255,255));
    for (iTrajList = trajList.begin(); iTrajList != trajList.end(); iTrajList++)
      (*iTrajList)->draw(img, mColor[(*iTrajList)->getId() % mColor.size()]);
    std::string outputFile = ait::combine_path(mOutputFolder, "input.png");
    img.save(outputFile.c_str());            
  }
  
  //parse through all trajectories, cull any that do not meet the requirements
  for (iTrajList = trajList.begin(); iTrajList != trajList.end(); iTrajList++)
  {

    float width1 = mpBlobSizeEstimate->getBlobWidth((*iTrajList)->getStartPos());
    float width2 = mpBlobSizeEstimate->getBlobWidth((*iTrajList)->getEndPos());   
    float avgWidth = (width1+width2)/2.0;

    bool tooShort = ((*iTrajList)->length() < avgWidth * mMinTrajLength);

    if ((*iTrajList)->getAge() >= mMinTrajAge && !tooShort)    
      goodTrajectories.push_back((*iTrajList));
    else
      culledTrajectories.push_back((*iTrajList));    
  }

  if (mVerbose)
    PVMSG("MultiPerson: %d/%d trajectories culled\n", culledTrajectories.size(), trajList.size());

  //Draw good trajectories to image for debugging
  if (mSaveGoodTrajImages)
  {    
    Image32 img(mSceneWidth, mSceneHeight);    
    img.setAll(PV_RGB(255,255,255));
    std::vector<TrajectoryPtr>::const_iterator iTraj;
    for (iTraj = goodTrajectories.begin(); iTraj != goodTrajectories.end(); iTraj++)
      (*iTraj)->draw(img, mColor[(*iTraj)->getId() % mColor.size()]);
    std::string outputFile = ait::combine_path(mOutputFolder, "goodTraj.png");
    img.save(outputFile.c_str());          
  }

  //Draw culled trajectories to image for debugging
  if (mSaveCulledTrajImages)
  {    
    Image32 img(mSceneWidth, mSceneHeight);    
    img.setAll(PV_RGB(255,255,255));
    std::vector<TrajectoryPtr>::const_iterator iTraj;
    for (iTraj = culledTrajectories.begin(); iTraj != culledTrajectories.end(); iTraj++)
      (*iTraj)->draw(img, mColor[(*iTraj)->getId() % mColor.size()]);
    std::string outputFile = ait::combine_path(mOutputFolder, "culledTraj.png");
    img.save(outputFile.c_str());          
  }



  return goodTrajectories;  
}

void
MultiPerson::processTrajectories(std::list<TrajectoryPtr>& trajList)
{
  PVASSERT(mIsInitialized);

  resetAll();

  //TODO: FIGURE OUT STL BUG, USE LISTS INSTEAD OF VECTORS FOR SPEED      
  //TODO: HANDLE SIMULTANEOUS MERGES/SPLITS TO SAME NODE:
  //TODO:   --try peturbation or allow nodes to be many to many  

  //cull spurious tracks
  std::vector<TrajectoryPtr> goodTrajectories = removeSpuriousTrajectories(trajList);

  
  //calculate the highest previous trajectory id
  int newTrajId = 0;
  std::vector<TrajectoryPtr>::const_iterator iTraj;
  for (iTraj = goodTrajectories.begin(); iTraj != goodTrajectories.end(); iTraj++)
    newTrajId = std::max(newTrajId, (*iTraj)->getId());
  newTrajId++;

  //if there are no good trajectories, do nothing  
  if (goodTrajectories.size() == 0)
  {
    trajList.clear();
    if (mVerbose)
      PVMSG("MultiPerson: Nothing to process\n");
    return;
  }
    
  
  //generate set of all interesting times
  std::set<double> timeSet;

  for (iTraj = goodTrajectories.begin(); iTraj != goodTrajectories.end(); iTraj++)
  {
    timeSet.insert((*iTraj)->getStartTime());
    timeSet.insert((*iTraj)->getEndTime());
  }

  std::set<double>::const_iterator iTime;

  int count = 0;
  
  while(timeSet.size() > 0)
  {     
    //update how many times we have been through this loop
    count++;

    double time = (*timeSet.begin());
    timeSet.erase(time);

    if (mVerbose)
      PVMSG("MultiPerson: Processing time: %f\n", time);

    //generate a set of trajectories alive at this time
    std::vector<TrajectoryPtr> currentlyExist;
    for (iTraj = goodTrajectories.begin(); iTraj != goodTrajectories.end(); iTraj++)
    {
      if ((*iTraj)->getStartTime() <= time && (*iTraj)->getEndTime() >= time)
        currentlyExist.push_back((*iTraj));
    }

    PVASSERT(currentlyExist.size() > 0);
    
    //check for splits/merges
    std::set<TrajectoryPtr> possibleSplits;
    std::set<TrajectoryPtr> possibleMerges;
    std::set<TrajectoryPtr> remaining;    

    for (iTraj = currentlyExist.begin(); iTraj != currentlyExist.end(); iTraj++)
    {
      if ((*iTraj)->getStartTime() == time)
        possibleSplits.insert(*iTraj);
      else if ((*iTraj)->getEndTime() == time)
        possibleMerges.insert(*iTraj);
      else
        remaining.insert(*iTraj);
    }
        
    std::set<TrajectoryPtr>::iterator iRemain;
    std::set<TrajectoryPtr>::iterator iCand;
    
    std::map<TrajectoryPtr, std::set<TrajectoryPtr> > splitEvent;

    //check for splits
    for (iRemain = remaining.begin(); iRemain != remaining.end(); iRemain++)
    {
      Vector3f pos = (*iRemain)->nodeClosestTime(time)->location;
      
      for (iCand = possibleSplits.begin(); iCand != possibleSplits.end(); )
      {
        double dist = sqrt(dist2(pos, (*iCand)->getStartPos()));
        
        float width1 = mpBlobSizeEstimate->getBlobWidth(pos);
        float width2 = mpBlobSizeEstimate->getBlobWidth((*iCand)->getStartPos());
        float avgWidth = (width1+width2)/2.0;        

        if (dist <= avgWidth*mMaxSplitDistance)
        {          
          splitEvent[*iRemain].insert(*iCand);
          iCand = possibleSplits.erase(iCand);         
        }
        else
          iCand++;                
      }     
    }
    
    //send the split events
    std::map<TrajectoryPtr, std::set<TrajectoryPtr> >::iterator iSplit;
    for (iSplit = splitEvent.begin(); iSplit != splitEvent.end(); iSplit++)
    {
      
      TrajectoryPtr pT1 = iSplit->first;      
      remaining.erase(pT1);      
      
      TrajectoryPtr pT2 = pT1->breakUp(time, newTrajId++);
      goodTrajectories.push_back(pT2);      
      iSplit->second.insert(pT2);

      if (mVerbose)
      {
        PVMSG("MultiPerson: Splitting T%d into:", pT1->getId());
      
        for (iCand = iSplit->second.begin(); iCand != iSplit->second.end(); iCand++)
          PVMSG(" T%d", (*iCand)->getId());
        PVMSG("\n");
      }
      
      splitTrack(pT1, iSplit->second);
    }

   

    std::map<TrajectoryPtr, std::set<TrajectoryPtr> > mergeEvent;
    //check for merges
    for (iRemain = remaining.begin(); iRemain != remaining.end(); iRemain++)
    {      
      Vector3f pos = (*iRemain)->nodeClosestTime(time)->location;
      
      for (iCand = possibleMerges.begin(); iCand != possibleMerges.end(); )
      {
        double dist = sqrt(dist2(pos, (*iCand)->getEndPos()));        

        float width1 = mpBlobSizeEstimate->getBlobWidth(pos);
        float width2 = mpBlobSizeEstimate->getBlobWidth((*iCand)->getEndPos());
        float avgWidth = (width1+width2)/2.0;

        if (dist <= mMaxMergeDistance * avgWidth)
        {           
          mergeEvent[*iRemain].insert(*iCand);
          iCand = possibleMerges.erase(iCand);          
        }
        else
          iCand++;
      }
    }    
    
    //send the merge events
    std::map<TrajectoryPtr, std::set<TrajectoryPtr> >::iterator iMerge;
    for (iMerge = mergeEvent.begin(); iMerge != mergeEvent.end(); iMerge++)
    {      
      TrajectoryPtr pT1 = iMerge->first;      
      remaining.erase(pT1);      
      
      TrajectoryPtr pT2 = pT1->breakUp(time, newTrajId++);
      goodTrajectories.push_back(pT2);   
      //
      std::set<TrajectoryPtr> trajSet = iMerge->second;
      trajSet.insert(pT1);
      //    

      if (mVerbose)
      {
        PVMSG("MultiPerson: Merging");    
        for (iCand = trajSet.begin(); iCand != trajSet.end(); iCand++)
          PVMSG(" T%d", (*iCand)->getId());
        PVMSG(" into: T%d\n", pT2->getId());
      }
      
      mergeTrack(trajSet, pT2);
    }    

    //Now begin or end any trajectories that are not splits or merges
    for (iRemain = possibleSplits.begin(); iRemain != possibleSplits.end(); iRemain++)  
    {
      if (mVerbose)
        PVMSG("MultiPerson: Begin Track: T%d\n", (*iRemain)->getId());
      
      beginTrack(*iRemain);
    }

    for (iRemain = possibleMerges.begin(); iRemain != possibleMerges.end(); iRemain++)
    {
      if (mVerbose)
        PVMSG("MultiPerson: End Track Final: T%d\n", (*iRemain)->getId());  

      endTrack(*iRemain, true);
    }

    //Save the current state to an image if desired
    if (mSaveTrajImages)
    {
      Image32 img(mSceneWidth, mSceneHeight);
      img.setAll(PV_RGB(255,255,255));
      
      for (iTraj = goodTrajectories.begin(); iTraj != goodTrajectories.end(); iTraj++)      
        (*iTraj)->draw(img, mColor[(*iTraj)->getId() % mColor.size()], time);

      std::string filename = aitSprintf("traj%03d.png", count);
      std::string loc = ait::combine_path(mOutputFolder, filename);      
      img.save(loc.c_str());
    }

    //Save the current internal graph state if desired
    if (mSaveGraphImages)
    {
      std::string filename = aitSprintf("graph%03d.png", count);
      std::string loc = ait::combine_path(mOutputFolder, filename);
      saveGraph(loc);
    }
  }    

  //convert trajlist to the final result
  trajList.clear(); 

  trajList = getProcessedPaths();

  if (mSaveFinalTrajs)
  {
    std::string filename = aitSprintf("finalTrajectories.csv", count);
    std::string loc = ait::combine_path(mOutputFolder, filename);
    Trajectory::saveCSV(loc, trajList);
  }

  resetAll();
}

void
MultiPerson::beginTrack(TrajectoryPtr pTraj)
{  
  PVASSERT(mIsInitialized);

  //Assert that this node should not already be in the trajectory table
  PVASSERT(mTrajTable.find(pTraj->getId()) == mTrajTable.end());

  NodePtr n(new Node(pTraj));
  mTrajTable[n->getId()] = n;
}

void
MultiPerson::endTrack(TrajectoryPtr pTraj, bool final)
{
  PVASSERT(mIsInitialized);

  //assert that this traj is already in the table
  PVASSERT(mTrajTable.find(pTraj->getId()) != mTrajTable.end());

  NodePtr node = mTrajTable[pTraj->getId()];

  //if id not in decision table
  if (mDecisionTable.find(pTraj->getId()) == mDecisionTable.end())
  {
    //if there are one or no parents (not a merge node)
    if (node->getParentList().size() < 2)
    {
      int personId = addPerson();
      node->addPersonId(personId);
      mPersonTable[personId].insert(node->getId());
    }
    else //we are dealing with a merge node because of multiple parents (do nothing)
    {       
    }
  }
  else
  {    
    //update the decision table

    PVASSERT(mDecisionTable.find(node->getId()) != mDecisionTable.end());

    DecisionPtr d1 = mDecisionTable[node->getId()];
    d1->setFinished(node->getId());

    bool madeDecision = true;

    while (mDecisionQueue.size() > 0)
    {      
      if (mDecisionQueue.front()->allFinished())
      {
        makeDecision(mDecisionQueue.front()->getTrajList());
        mDecisionQueue.pop_front();
      }
      else
        break;
    }
  }

  //did the track die a final death?
  if (final == true)
  {         
    if (mDecisionTable.find(node->getId()) != mDecisionTable.end()) //if trajId in decisionTable        
    {      
      mFinishedTable.push_back(node->getId());        
    }
    else
    {      
      std::set<int>::const_iterator iPerson;
      for (iPerson = node->getPersonSet().begin(); iPerson != node->getPersonSet().end(); iPerson++)
        processPerson(*iPerson);
    }
  }  
}

void 
MultiPerson::mergeTrack(const std::set<TrajectoryPtr>& pTrajList, TrajectoryPtr pT1)
{
  PVASSERT(mIsInitialized);
  
  std::set<TrajectoryPtr>::const_iterator iTraj;
  for (iTraj = pTrajList.begin(); iTraj != pTrajList.end(); iTraj++)      
    endTrack(*iTraj);  
  
  beginTrack(pT1);
  
  std::list<Node*> mergeNodes;

  for (iTraj = pTrajList.begin(); iTraj != pTrajList.end(); iTraj++)
  {
    PVASSERT(mTrajTable.find((*iTraj)->getId()) != mTrajTable.end());
    mergeNodes.push_back(mTrajTable[(*iTraj)->getId()].get());
  }  

  

  PVASSERT(mTrajTable.find(pT1->getId()) != mTrajTable.end());
  NodePtr node = mTrajTable[pT1->getId()];    
  
  std::list<Node*>::const_iterator iNode;
  for (iNode = mergeNodes.begin(); iNode != mergeNodes.end(); iNode++)  
    (*iNode)->addChild(node.get());
  
    
  std::set<int> childrenPersons;
  std::set<int>::const_iterator iPerson;

  //Add source nodes' persons to the destination node
  for (iNode = mergeNodes.begin(); iNode != mergeNodes.end(); iNode++)
  {
    for (iPerson = (*iNode)->getPersonSet().begin(); iPerson != (*iNode)->getPersonSet().end(); iPerson++)    
      childrenPersons.insert(*iPerson);    
  }

  //Update relevant entries in the person table
  for (iPerson = childrenPersons.begin(); iPerson != childrenPersons.end(); iPerson++)
  {
    node->addPersonId(*iPerson);    
    PVASSERT(mPersonTable.find(*iPerson) != mPersonTable.end());
    mPersonTable[*iPerson].insert(node->getId());
  } 
  //update the count
  node->setCount(std::max((int)node->getCount(), (int)node->getPersonSet().size())); 
  
}

void
MultiPerson::splitTrack(TrajectoryPtr pT1, const std::set<TrajectoryPtr>& trajList)
{
  PVASSERT(mIsInitialized);

  endTrack(pT1);
  std::set<TrajectoryPtr>::const_iterator iTraj;  
  for (iTraj = trajList.begin(); iTraj != trajList.end(); iTraj++)  
    beginTrack(*iTraj);


  //make sure id already exists in table
  PVASSERT(mTrajTable.find(pT1->getId()) != mTrajTable.end());

  NodePtr node = mTrajTable[pT1->getId()];

  if (node->getCount() < 2)
  {
    int personId = addPerson();
    addPersonBack(personId, node.get());
  } 

  DecisionPtr d1(new Decision());

  //Add this decision to the decision table
  for (iTraj = trajList.begin(); iTraj != trajList.end(); iTraj++)
  {
    PVASSERT(mTrajTable.find((*iTraj)->getId()) != mTrajTable.end());
    node->addChild(mTrajTable[(*iTraj)->getId()].get());

    d1->addId((*iTraj)->getId());
    mDecisionTable[(*iTraj)->getId()] = d1;
  }

  mDecisionQueue.push_back(d1);  
}


void 
MultiPerson::addPersonBack(int personId, Node* node)
{
  PVASSERT(mIsInitialized);

  node->addPersonId(personId);
  node->setCount(node->getCount() + 1);

  PVASSERT(mPersonTable.find(personId) != mPersonTable.end());
  mPersonTable[personId].insert(node->getId());

  if (node->parentCount() == 0) // no parents
    return;
  else if (node->parentCount() == 1)
    addPersonBack(personId, node->getParentList().front());
  else if (node->parentCount() > 1)
  {

    double bestScore = 0.0;
    Node* bestParent = node->getParentList().front();

    std::vector<Node*>::const_iterator iNode;
    std::set<int>::const_iterator iPerson;

    for (iNode = node->getParentList().begin(); iNode != node->getParentList().end(); iNode++)
      for (iPerson = (*iNode)->getPersonSet().begin(); iPerson != (*iNode)->getPersonSet().end(); iPerson++)
      {
        double score = evaluationFunc(node, *iPerson);
        if (score >= bestScore)
        {
          bestScore = score;
          bestParent = *iNode;
        }
      }
    addPersonBack(personId, bestParent);

  }
}

void
MultiPerson::makeDecision(const std::vector<int>& trajIdList)
{
  PVASSERT(mIsInitialized);
  //assert that t1 and t2 are already in the trajectory table

  std::vector<int>::const_iterator iId;
  for (iId = trajIdList.begin(); iId != trajIdList.end(); iId++)
    PVASSERT(mTrajTable.find(*iId) != mTrajTable.end());
  
  
  std::set<int> parentPersons;
  std::set<int> childPersons;

  std::vector<Node*> nodes;

  for (iId = trajIdList.begin(); iId != trajIdList.end(); iId++)
  {
    NodePtr n = mTrajTable[*iId];
    PVASSERT (n->parentCount() == 1);
    nodes.push_back(n.get());
  }


  //generate set of parents persons
  Node* parent = nodes[0]->getParentList()[0];
  std::copy(parent->getPersonSet().begin(), parent->getPersonSet().end(), 
    std::inserter(parentPersons, parentPersons.begin()));

  int parentCount = parent->getCount();
  int childrenCount = 0;

  //generate set of all child persons
  std::vector<Node*>::const_iterator iNode;

  for (iNode = nodes.begin(); iNode != nodes.end(); iNode++)
  {
    std::copy((*iNode)->getPersonSet().begin(), (*iNode)->getPersonSet().end(),
      std::inserter(childPersons,childPersons.begin()));
    
    childrenCount += (*iNode)->getCount();
  }

  PVASSERT(parentCount >= childrenCount);

  //create a set of people who need assigned (difference between two sets)
  std::set<int> needAssigned;
  
  std::set_difference(
    parentPersons.begin(), parentPersons.end(),
    childPersons.begin(), childPersons.end(),
    std::inserter(needAssigned, needAssigned.begin()));
  

  //for each person, find the best node

  std::set<int>::iterator iPerson;
  for (iPerson = needAssigned.begin(); iPerson != needAssigned.end();)
  {    
    Node* bestNode = nodes.front();
    double bestScore = 0.0;

    for (iNode = nodes.begin(); iNode != nodes.end(); iNode++)
    {
      double score = evaluationFunc(*iNode, *iPerson);
      if (score > bestScore)
      {
        bestScore = score;
        bestNode = *iNode;
      }
    }    

    addPersonForward(*iPerson, bestNode);
    needAssigned.erase(iPerson++);
  }

  //check each node to see if its count matches the number of people it has
  
  for (iNode = nodes.begin(); iNode != nodes.end(); iNode++)
  {
    int personDiff = (*iNode)->getCount() - (*iNode)->getPersonSet().size();

    for (int x = 0; x < personDiff; x++)
    {
      int personId = addPerson();
      addPersonForward(personId, *iNode);
    }
  }

 
  //there should be no persons left unassigned
  PVASSERT(needAssigned.size() == 0);  

  std::vector<int>::iterator iFound;
  
  //check to see which persons can be processed because relevant trajectories
  // are finished
  for (iNode = nodes.begin(); iNode != nodes.end(); iNode++)
  {    
    iFound = std::find(mFinishedTable.begin(), mFinishedTable.end(), (*iNode)->getId());
    if (iFound != mFinishedTable.end()) //id in finishedTable
    {      

      std::set<int>::const_iterator iPerson;
      for (iPerson = (*iNode)->getPersonSet().begin(); iPerson != (*iNode)->getPersonSet().end(); iPerson++)
        processPerson(*iPerson);

      mFinishedTable.erase(iFound);
    }
  }

  
  //Remove decisions from the decision table
  std::map<int, DecisionPtr>::iterator iDecision;  
  for (iNode = nodes.begin(); iNode != nodes.end(); iNode++)
  {
    iDecision = mDecisionTable.find((*iNode)->getId());
    mDecisionTable.erase(iDecision);
  }
}

void
MultiPerson::processPerson(int personId)
{
  PVASSERT(mIsInitialized);
  std::set<int> path;

  //make sure that this person already exists in the table
  PVASSERT(mPersonTable.find(personId) != mPersonTable.end());

  path = mPersonTable[personId];   
  mPersonTable.erase(mPersonTable.find(personId));

  std::set<int>::const_iterator iLoc;

  if (mVerbose)
  {
    PVMSG("MultiPerson: Processed Person %d Path: ", personId);      
    for (iLoc = path.begin(); iLoc != path.end(); iLoc++)
      PVMSG("%d ", *iLoc);
    PVMSG("\n");
  }


  //now generate one trajectory by following all of the paths
  
  //TODO: NEED GLOBAL ID, PERHAPS LET THIS BE FIGURED OUT BY CLASS CALLER

  TrajectoryPtr pTraj(new Trajectory(personId));

  double lastTime = -1.0;

  for (iLoc = path.begin(); iLoc != path.end(); iLoc++)
  {
    PVASSERT(mTrajTable.find(*iLoc) != mTrajTable.end());
    TrajectoryPtr pOther = mTrajTable[*iLoc]->getTraj();


    Trajectory::Nodes::iterator iNodeStart;

    for (iNodeStart = pOther->getNodes().begin(); iNodeStart != pOther->getNodes().end(); iNodeStart++)
    {
      if ((*iNodeStart)->time > lastTime)
      {
        lastTime = (*iNodeStart)->time;
        break;
      }     
    }

    if (pOther->lastNode()->time > lastTime)
      lastTime = pOther->lastNode()->time;

    std::copy(iNodeStart, pOther->getNodes().end(), std::inserter(pTraj->getNodes(), pTraj->getNodes().end()));
  }
  
  if (!pTraj->isValid())
  {
    double lastTime = -1.0;
    Trajectory::Nodes::iterator iNode;
    for (iNode = pTraj->getNodes().begin(); iNode != pTraj->getNodes().end(); iNode++)
    {
      if ((*iNode)->time == lastTime)
        PVMSG("ID: %d   TIME: %f\n", pTraj->getId(), (*iNode)->time);
      lastTime = (*iNode)->time;
    }
  }

  PVASSERT(pTraj->isValid());
  mProcessedTraj.push_back(pTraj);

  //Save the individual paths to image files if requested
  if (mSavePersonPaths)
  {
    Image32 img(mSceneWidth, mSceneHeight);
    img.setAll(PV_RGB(255,255,255));

    pTraj->draw(img, mColor[pTraj->getId() % mColor.size()]);

    std::string filename = ait::aitSprintf("person%03d.png", pTraj->getId());
    std::string loc = ait::combine_path(mOutputFolder, filename);
    img.save(loc.c_str());
  }

}

int
MultiPerson::addPerson()
{
  PVASSERT(mIsInitialized);

  mPersonCount++;

  std::set<int> l;
  mPersonTable[mPersonCount] = l;

  return mPersonCount;
}

void
MultiPerson::addPersonForward(int personId, Node* node)
{
  PVASSERT(mIsInitialized);

  node->addPersonId(personId);
  node->setCount(std::max( (int)node->getPersonSet().size(), (int)node->getCount()));

  PVASSERT(mPersonTable.find(personId) != mPersonTable.end());

  mPersonTable[personId].insert(node->getId());

  if (node->childrenCount() == 0) //no children
    return;
  else if (node->childrenCount() == 1)
    addPersonForward(personId, node->getChildrenList().front());
  else
    return;


}

double
MultiPerson::evaluationFunc(Node* node, int personId)
{
  PVASSERT(mIsInitialized);

  PVASSERT(mPersonTable.find(personId) != mPersonTable.end());

  //TODO: get 'closest' trajectory to one being considered

  /*
  int latestTraj = mPersonTable[personId].back();
  PVASSERT(mTrajTable.find(latestTraj) != mTrajTable.end());  
  TrajectoryPtr pTraj1 = node->getTrajectory();
  TrajectoryPtr pTraj2 = mTrajTable[latestTraj]->getTrajectory();
  PVASSERT(pTraj1 != pTraj2);  
  Vector2f vec1 = pTraj1->
  */
  //TODO: temporary function here
  return (double)rand()/(double)(RAND_MAX + 1);
}


void
MultiPerson::printDebug()
{
  PVASSERT(mIsInitialized);
  
  PVMSG("%d NODES:\n--------------------\n", mTrajTable.size());

  std::map<int,NodePtr>::const_iterator iTrajEntry;
  for (iTrajEntry = mTrajTable.begin(); iTrajEntry != mTrajTable.end(); iTrajEntry++)
    iTrajEntry->second->print();

  PVMSG("PERSONS:");  
  std::map<int, std::set<int> >::const_iterator iPersonEntry;

  for (iPersonEntry = mPersonTable.begin(); iPersonEntry != mPersonTable.end(); iPersonEntry++)
  {
    std::set<int>::const_iterator iTraj;
    PVMSG(" %d:[", iPersonEntry->first);

    for (iTraj = iPersonEntry->second.begin(); iTraj != iPersonEntry->second.end(); iTraj++)
    {
      PVMSG("T%d ", *iTraj);
    }


    PVMSG("]");
  }
  PVMSG("\n");

  PVMSG("DECISION TABLE: ");
  std::map<int,DecisionPtr>::const_iterator iDecisionEntry;
  for (iDecisionEntry = mDecisionTable.begin(); iDecisionEntry != mDecisionTable.end(); iDecisionEntry++)
  {
    PVMSG("%d ", iDecisionEntry->first);
  }
  PVMSG("\n");

  PVMSG("DECISION QUEUE:\n");
  std::list<DecisionPtr>::const_iterator iDecision;
  for (iDecision = mDecisionQueue.begin(); iDecision != mDecisionQueue.end(); iDecision++)
  {    
    (*iDecision)->print();    
  }
  PVMSG("\n");

  PVMSG("FINISHED TABLE: ");
  std::vector<int>::const_iterator iFinished;
  for (iFinished = mFinishedTable.begin(); iFinished != mFinishedTable.end(); iFinished++)
  {
    PVMSG("%d ", *iFinished);
  }

  PVMSG("\n");
}

void
MultiPerson::resetAll()
{
  PVASSERT(mIsInitialized);

  mPersonCount = 0;
  mPersonTable.clear(); //.erase(mPersonTable.begin(), mPersonTable.end());

  std::map<int, NodePtr>::iterator iTrajEntry;
  for (iTrajEntry = mTrajTable.begin(); iTrajEntry != mTrajTable.end(); iTrajEntry++)  
    iTrajEntry->second->unlink();

  mTrajTable.clear();
  mDecisionTable.clear();
  mDecisionQueue.clear();
  mProcessedTraj.clear();
}

void
MultiPerson::saveGraph(std::string filename)
{
  //Saves the current internal state to an image file
  //Graphviz is required for this operation, and the appropriate
  // directory must be added to the PATH environment variable

  PVASSERT(mIsInitialized);
  std::string graphString = "Digraph {";


  std::map<int, NodePtr>::const_iterator iTrajEntry;


  for (iTrajEntry = mTrajTable.begin(); iTrajEntry != mTrajTable.end(); iTrajEntry++)
  {
    NodePtr pCurrentNode = iTrajEntry->second;

    graphString += "\"" + pCurrentNode->getString() + "\" ";

    std::vector<Node*>::const_iterator iNode;
    for (iNode = pCurrentNode->getChildrenList().begin(); 
      iNode != pCurrentNode->getChildrenList().end(); iNode++)
    {
        graphString += "\"" + pCurrentNode->getString() + "\""         
          + "->" + "\"" + (*iNode)->getString() + "\" ";
    }
  }  

  graphString += "}";

  
  

  std::ofstream textFile((filename + ".txt").c_str());
  textFile << graphString;
  textFile.close();

#ifdef WIN32
  std::string params = "/Q /C dot -Tpng <" + filename + ".txt>" + filename;
  ShellExecute(NULL,"open","cmd",params.c_str(),NULL,0);
#endif 

}

//
// ACCESS
//

//
// INQUIRY
//

}; // namespace vision

}; // namespace ait


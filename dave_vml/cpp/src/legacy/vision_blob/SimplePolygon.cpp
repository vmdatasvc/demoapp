/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include "legacy/vision_blob/SimplePolygon.hpp"
#include <legacy/types/strutil.hpp>

#include <boost/tokenizer.hpp>

namespace ait 
{

void convert(const std::string& str, SimplePolygon& p)
{
  std::string vstr = str;
    // Remove parenthesis
  if (vstr.length() > 2 && vstr[0] == '(')
  {
    vstr = vstr.substr(1,vstr.length()-2);
  }

  std::vector<std::string> stringPoints = split(vstr,",");

  if (stringPoints.size() % 2 != 0)
  {
    PvUtil::exitError("Invalid Polygon!");
  }

  p.clear();
  for (int i = 0; i < stringPoints.size(); i+=2)
  {
    //PVMSG("(%s,%s)\n",stringPoints[i].c_str(),stringPoints[i+1].c_str());
    Vector2f point;
    sscanf(stringPoints[i].c_str(),"%f",&point.x);
    sscanf(stringPoints[i+1].c_str(),"%f",&point.y);
    // Lexical cast didn't work?!?!
    /*
    (
      boost::lexical_cast<float>(stringPoints[i]),
      boost::lexical_cast<float>(stringPoints[i+1])
      );
      */
    //PVMSG("(%f,%f)\n",point.x,point.y);
    p.push_back(point);
  }
}

std::list<SimplePolygon> 
convertList(const std::string& str)
{
  std::list<SimplePolygon> result;

  boost::char_separator<char> sep(":");
  boost::tokenizer<boost::char_separator<char> > polyToken(str, sep);

  boost::tokenizer<boost::char_separator<char> >::const_iterator iToken;
  
  for (iToken = polyToken.begin(); iToken != polyToken.end(); iToken++)
  {
    SimplePolygon p;
    convert(*iToken, p);
    result.push_back(p);
  }

  return result;
}

}; // namespace ait


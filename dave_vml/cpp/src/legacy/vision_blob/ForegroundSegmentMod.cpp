/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#pragma warning(disable:4503)
#endif

#include <boost/lexical_cast.hpp>

#include "legacy/vision_blob/ForegroundSegmentMod.hpp"
#include "legacy/vision_blob/BlobSizeEstimate.hpp"
#include "legacy/vision_tools/ColorConvert.hpp"
#include "legacy/vision/ImageAcquireMod.hpp"
//#include "VmsPacket.hpp"
#include <legacy/vision_tools/BackgroundLearner.hpp>
//#include <VideoReaderAVI.hpp>
#include <legacy/media/VideoReaderAVIOpenCV.hpp>
//#include <image_proc.hpp>
#include <legacy/vision_tools/PolygonGetMask.hpp>
//#include <ControlMod.hpp>
#include <legacy/low_level/Settings.hpp>
#include <legacy/types/DateTime.hpp>
#include <legacy/types/strutil.hpp>
#include <fstream>

#include <legacy/vision_tools/PvImageProc.hpp>

namespace ait
{

namespace vision
{

ForegroundSegmentMod::ForegroundSegmentMod()
: VisionMod(getModuleStaticName()),
  mRoiScaleFactor(1,1)
{  
}

ForegroundSegmentMod::ForegroundSegmentMod(const std::string& moduleName) 
: VisionMod(moduleName),
  mRoiScaleFactor(1,1)
{  
}

ForegroundSegmentMod::~ForegroundSegmentMod()
{
}

//void
//ForegroundSegmentMod::initExecutionGraph(std::list<VisionModPtr>& srcModules)
//{
//  RealTimeMod::initExecutionGraph(srcModules);
  
//  mMotionHack = false;

//  if (mMotionHack)
//  {
//    getModule("MotionDetectionMod", mpMotionMod);
//    srcModules.push_back(mpMotionMod);
//  }
  
//  // Add any source module to the given list here.
//  // VisionModPtr pMod;
//  // getModule("SampleMod",pMod);
//  // srcModules.push_back(pMod);
//}

void ForegroundSegmentMod::initExecution()
{
	VisionMod::initExecution();

	if (mMotionHack)
	{
		getModule("MotionDetectionMod",mpMotionMod);
	}
}

void 
ForegroundSegmentMod::init()
{
//  RealTimeMod::init();
  mSettings.changePath("ForegroundSegmentMod");

  mMotionHack = false;

  //TODO: TEMP
  mFirstPass = true;
  //END TEMP
  
  //AUTOEXPOSURE TEMP CHECK
  mCheckForExposure = mSettings.getBool("exposureCheck", false);

  mLastMeanIntensity = 0.0;
  mLastExposureCheck = 0.0;


  mMinNoMotionTime = mSettings.getFloat("minNoMotionTime", 60.0f);

  mExposureLogFilename = mSettings.getString("exposureLog", "");
  //END TEMP  

  mBackgroundLoadLoc = mSettings.getString("loadBackgroundFromFile", "");
  mBackgroundSaveLoc = mSettings.getString("saveBackgroundToFile", "");

//  mpVmsPacket.reset(new VmsPacket(this));

  mLearnBackgroundContinuously = mSettings.getBool("BackgroundLearner/realTime [bool]", 0);

  mEventChannels = mSettings.getIntList("eventChannelId", "", false);

  Image32Ptr pFrame;
  // TODO: feed image here
  pFrame = getImageAcquireModule()->getCurrentFramePtr();
  
  assert(pFrame.get());

  mFrameWidth = pFrame->width();
  mFrameHeight = pFrame->height();

  mVerbose = mSettings.getBool("Visualization/printMessages [bool]",0);
  mPaintVisualization = mSettings.getBool("Visualization/enabled [bool]",1);

  mCurrentProgressUnits = 0;

  mTotalProgressUnits = -1;

//  mBenchmark.init(mSettings.getPath(),"Segment Foreground");

  mUseBlobSize = mSettings.getBool("BlobSizeEstimator/enabled [bool]",false);

  bool doLearnBlobSize = mSettings.getBool("BlobSizeEstimator/learn [bool]",false);

  doLearnBlobSize = doLearnBlobSize && mUseBlobSize;  
  
  mpRoiMask.reset();

  // TODO: check frame size here
  Vector2i frameSize = getImageAcquireModule()->getFrameSize();


  mRoiScaleFactor = mSettings.getCoord2f("roiScaleFactor",Vector2f(1,1));



  mUseMotion = mSettings.getBool("useMotion [bool]",false);
  mNumMotionFrames = mSettings.getInt("numMotionFrames",3);
  mLastFrames.setCapacity(mNumMotionFrames);

  if (mLearnBackgroundContinuously)
  {
    //Grab the relevant settings
    std::list<double> learnTimes =
      mSettings.getDoubleList("BackgroundLearner/modelLearnTime [sec]","10");
    
    std::list<double> sampleIntervals =
      mSettings.getDoubleList("BackgroundLearner/frameUpdateTime [sec]", ".5");
    
    PVASSERT(learnTimes.size() == sampleIntervals.size());
    
    std::list<int> numFrames;
    std::list<int> updateInterval;

    std::list<double>::iterator iTime;
    std::list<double>::const_iterator iInterval = sampleIntervals.begin();

    //Convert the given ForegroundSegmentMod settings from seconds into frames
    for (iTime = learnTimes.begin(); iTime != learnTimes.end(); iTime++)
    {     
      updateInterval.push_back(std::max(round(getImageAcquireModule()->getFramesPerSecond()* (*iInterval)), 1));
      numFrames.push_back(std::max(round((*iTime) * getImageAcquireModule()->getFramesPerSecond())/updateInterval.back(),1));
      iInterval++;
    }
    
    //Set the frame-based settings for the background learner
    mSettings.setIntList("BackgroundLearner/numberOfBackgroundFrames", numFrames);
    mSettings.setIntList("BackgroundLearner/modelUpdateInterval", updateInterval);
    PVMSG("Learning background continously...\n");   
    
    mpBackgroundLearner.reset(new BackgroundLearner());    
    // TODO: modify background learner not to use mSettings
    mpBackgroundLearner->init(mSettings.getPath()+"/BackgroundLearner");        
  }   

  if (doLearnBlobSize)
  {
    // To learn the blob size we take the full image regardless of other
    // parameters.
    mRoiScaleFactor = Vector2f(1,1);    
    mRegionOfInterest.set(0,0,frameSize.x,frameSize.y);
  }
  else
  {
    Rectanglef roif = mSettings.getRectf("roiRectangle",Rectanglef());
//    Rectanglef roif = Rectanglef(0,0,320,240);
    mRegionOfInterest.set((int)roif.x0,(int)roif.y0,(int)roif.x1,(int)roif.y1);
    
    // YR: roiPolygon is not set by default. Disabling the following code
#if 0
    // Get the ROI polygon
    //Grab the list of roi polygons, and parse them into the mRoiPolygon map
    std::list<SimplePolygon> polyList = convertList(mSettings.getString("roiPolygon",""));
    std::list<SimplePolygon>::const_iterator iPoly;

    std::list<int>::const_iterator iEvtChan = mEventChannels.begin();
    for (iPoly = polyList.begin(); iPoly != polyList.end(); iPoly++)
    {
      mRoiPolygon[*iEvtChan] = *iPoly;
      iEvtChan++;
    }

    if (!mRoiPolygon.empty())
    {
      // Calculate new region of interest based on the polygon.
      Rectanglef newRoi(
        mRoiPolygon[mEventChannels.front()][0].x,mRoiPolygon[mEventChannels.front()][0].y,
        mRoiPolygon[mEventChannels.front()][0].x,mRoiPolygon[mEventChannels.front()][0].y);      

      for (iEvtChan = mEventChannels.begin(); iEvtChan != mEventChannels.end(); iEvtChan++)
      {
        int i;
        for (i = 0; i < mRoiPolygon[*iEvtChan].size(); i++)
        {
          newRoi.set(
            std::min(newRoi.x0,mRoiPolygon[*iEvtChan][i].x),
            std::min(newRoi.y0,mRoiPolygon[*iEvtChan][i].y),
            std::max(newRoi.x1,mRoiPolygon[*iEvtChan][i].x),
            std::max(newRoi.y1,mRoiPolygon[*iEvtChan][i].y));
        }
      }

      // We would want to adjust the with so it has the
      // right byte padding for better performance.
      mRegionOfInterest.set(
        (int)newRoi.x0,(int)newRoi.y0,
        (int)newRoi.x1,(int)newRoi.y1);
      const Rectanglei bounds(0,0,frameSize.x,frameSize.y);
      mRegionOfInterest.intersect(bounds);

      // Get the mask.
      mpRoiMask.reset(new Image8((float)mRegionOfInterest.width(),(float)mRegionOfInterest.height()));
      mpRoiMask->setAll(0);

      Image8 currentMask(mRegionOfInterest.width(), mRegionOfInterest.height());

      for (iEvtChan = mEventChannels.begin(); iEvtChan != mEventChannels.end(); iEvtChan++)
      {
        std::vector<Vector2f> vertices;
        for (int i = 0; i < mRoiPolygon[*iEvtChan].size(); i++)
        {
          vertices.push_back(Vector2f(mRoiPolygon[*iEvtChan][i].x-mRegionOfInterest.x0,
            mRoiPolygon[*iEvtChan][i].y-mRegionOfInterest.y0));
        }
        getMask(vertices,Rectanglef(0.0,0.0,mpRoiMask->width(),mpRoiMask->height()),currentMask);

        for (int y = 0; y < mpRoiMask->height(); y++)
        {
          for (int x = 0; x < mpRoiMask->width(); x++)
          {
            (*mpRoiMask)(x,y) = (*mpRoiMask)(x,y) | currentMask(x,y);
          }
        }
      }
    }
    else
    {
      // It will just use the ROI
    }
#endif

    // YR: mUseBlobSIze is false by default. disabling the following code
#if 0
    if (mUseBlobSize)
    {     

      // Load Blob size estimate.
      mpBlobSizeEstimate.reset(new BlobSizeEstimate());
      // TODO: replace blob size estimator loading with hardcoded
      mpBlobSizeEstimate->load(mSettings.getString("BlobSizeEstimator/fileName",""));
      mpBlobSizeEstimate->crop(mRegionOfInterest);     


      //TODO: Temporary hack for hallmark
      if (mUseMotion)
      {
        std::string filename = mSettings.getString("BlobSizeEstimator/fileName","");
        //strip off '.xml' extension
        filename.resize(filename.length() - 4);
        //add a motion extension
        filename += "_motion.xml";                

        if (PvUtil::fileExists(filename.c_str()))
        {
          PVMSG("%s exists, using alternate blob size file.\n", filename.c_str());
          mpBlobSizeEstimate.reset(new BlobSizeEstimate());
          mpBlobSizeEstimate->load(filename);
          mpBlobSizeEstimate->crop(mRegionOfInterest);   
        }
      }
      //END Temporary hack for hallmark



      // Estimate rescale factor based on blob size for the new ROI.
      estimateScaleFactor();      

      // Scale accoding to scale factor.
      mpBlobSizeEstimate->scale(mRoiScaleFactor);
    }
#endif

    if (mpRoiMask.get())
    {
      mpRoiMask->rescale(
        (int)(mRegionOfInterest.width()*mRoiScaleFactor.x),
        (int)(mRegionOfInterest.height()*mRoiScaleFactor.y),
        RESCALE_SKIP);
    }
  }    

  // YR: mPaintVisualization is false by default. disabling the following code
#if 0
  if (mPaintVisualization)
  {
    if (mUseBlobSize)
    {
      getImageAcquireModule()->setVisualizationFrameSize(Vector2i(mFrameWidth*2,mFrameHeight*2));
    }
    else
    {
      getImageAcquireModule()->setVisualizationFrameSize(Vector2i(mFrameWidth*2,mFrameHeight));
    }  
  }
#endif

  // YR: learnBackgroundcontinuously is false by default.
  // Learn the background from the video if necessary
  if (!mLearnBackgroundContinuously && mBackgroundLoadLoc == "")
    learnBackgroundFromVideo();

  // YR: learnBackgroundContinuously is true by default.
#if 0
  //Learn the blob size and quit if we are not processing a live feed
  if (doLearnBlobSize && !mLearnBackgroundContinuously)
  {     
    learnBlobSize();

    // Send a finalize message to the control module.
    ControlModPtr pMod;
    getModule("ControlMod",pMod);
    vision::ControlMsgPtr pMsg(new vision::ControlMsg());
    pMsg->setFlags(vision::ControlMsg::EXIT_APPLICATION);
    pMod->putMsg(pMsg);

    // A more friendly alternative would be to disable this module.
    //pMsg->setEnableModule(this.getExecutionPathName(),this.getModuleName(),false);
  } 
#endif
  //Grab the settings relevant to checkBackground from the settings file

//  mEnableRelearnBackground = mSettings.getBool("relearnEnabled [bool]", false);
  mEnableRelearnBackground = false;
//  mRelearnCheckInterval = mSettings.getFloat("relearnCheckInterval [sec]", 5.0);
  mRelearnCheckInterval = 5.f;
//  mRelearnInertTime = mSettings.getFloat("relearnInertTime [sec]", 300.0);
  mRelearnInertTime = 300.f;
//  mRelearnBlobThresh = mSettings.getFloat("relearnBlobThresh", 1.5);
  mRelearnBlobThresh = 1.5f;
  mLastRelearnCheck = 0; 

#if 0
  if (mBackgroundLoadLoc != "")
  {    
    PVMSG("Loading background model from file: %s\n", mBackgroundLoadLoc.c_str());
    mpBackgroundLearner->loadModel(mBackgroundLoadLoc);
  }  
#endif

//  mpVmsPacket->allowPause();

  //TODO: 


  mSaveSegmentation = mSettings.getBool("saveSegmentation [bool]", false);
  mSaveSegOutputFolder = mSettings.getString("saveSegmentationFolder","");
//  mSaveSegmentation = false;
//  mSaveSegOutputFolder = "";

  mSaveSegInterval = mSettings.getFloat("saveSegmentationInteveral",1.0);
  mSaveSegFileLengthInterval = mSettings.getFloat("saveSegmentionPartitionTime", 1800.0);
//  mSaveSegInterval = 1.f;
//  mSaveSegFileLengthInterval = 1800.f;
  mNextSaveSegTime = getCurrentTime() + mSaveSegInterval;
  mNextBreakFileTime = getCurrentTime() + mSaveSegFileLengthInterval;
  
//  if (mSaveSegmentation)
//    if (!PvUtil::fileExists(mSaveSegOutputFolder.c_str()))
//      PvUtil::exitError("ForegroundSegmentMod: ERROR: The path '%s' does not exist.\n", mSaveSegOutputFolder.c_str());

  
//  mVideoSourceId = atoi(Settings::replaceVars("<VideoSourceId>",mSettings.getPath()).c_str());
//  PVASSERT(mVideoSourceId != -1);

  //Motion hack code
  if (mMotionHack)
  {   

    Rectanglei roi = getRoiRectangle();
    Vector2f scaleFactor = getRoiScaleFactor(); 

    //PVMSG("SCALE FACTOR: %f %f\n", scaleFactor.x, scaleFactor.y);    
    mpMotionMod->setScaleAndRoi(scaleFactor, roi);
  }  
}

void
ForegroundSegmentMod::finish()
{  
  mSegOutputFile.close();

  if (mBackgroundSaveLoc != "")
  {
    PVMSG("Saving background model to file: %s\n", mBackgroundSaveLoc.c_str());
    mpBackgroundLearner->saveModel(mBackgroundSaveLoc);
  }  

//  if (mpVmsPacket.get())
//  {
//    mpVmsPacket->finish();
//  }

//  RealTimeMod::finish();
}

void
ForegroundSegmentMod::estimateScaleFactor()
{   
  Vector2f avgBlobSize;

  float x,y;

  for (y = 0; y < mRegionOfInterest.height(); y++)
  {
    for (x = 0; x < mRegionOfInterest.width(); x++)
    {
      float w,h;
      mpBlobSizeEstimate->getEstimatedSize(x,y,w,h);
      avgBlobSize.x += w;
      avgBlobSize.y += h;
    }
  }

  avgBlobSize.div(mRegionOfInterest.width()*mRegionOfInterest.height());  

  // We want the average blob to be of a specific size in pixels.
  Vector2f desiredBlobSize = mSettings.getCoord2f("BlobSizeEstimator/desiredBlobSize",Vector2f(5,5));
  
  mRoiScaleFactor.x = desiredBlobSize.x/avgBlobSize.x;
  mRoiScaleFactor.y = desiredBlobSize.y/avgBlobSize.y;      
}

std::string
ForegroundSegmentMod::getVideoFileName()
{  
  // Retrieve the video name from the settings.
  std::string deviceID = getImageAcquireModule()->getSettings().getString("grabberDeviceId","");

  std::string filename = "";

  //check for reference device
  int delim = deviceID.find(':');
  if (delim != -1)
  {
    //std::string 
    std::string deviceClass = deviceID.substr(delim+1,deviceID.length() - delim - 2);
    
    if (deviceClass != "REF")
      PvUtil::exitError("Can't learn background from device '%s'", deviceClass.c_str());
    else
    {
      Settings grabberSettings("VisionMods/"+deviceID.substr(0,delim));      

      std::string grabberDevice = grabberSettings.getString("ImageAcquireMod/grabberDeviceId", "");      
      Settings deviceSettings("ImageAcquireControl/Devices/"+grabberDevice);
      filename = deviceSettings.getString("file1","adsf");
    }
  }
  else
  {
    if (deviceID.substr(0,3) != "AVI")
    {
      PvUtil::exitError("Can't learn background from a video file!");
    }
    Settings deviceSettings("ImageAcquireControl/Devices/"+deviceID);
    filename = deviceSettings.getString("file1","");  
  }
  return filename;  
}

void
ForegroundSegmentMod::learnBackgroundFromVideo()
{  
  VideoReaderAVIOpenCV vreader;

  Uint w,h;
  vreader.init(w,h,getVideoFileName().c_str());

  int startFrame = getCurrentFrameNumber();

  // Write settings for the learner.
  //double learnTime = mSettings.getDouble("BackgroundLearner/modelLearnTime [sec]",5*60);

  std::list<double> learnTime = 
    mSettings.getDoubleList("BackgroundLearner/modelLearnTime [sec]","300");

  std::list<double> sampleIntervals = 
    mSettings.getDoubleList("BackgroundLearner/frameUpdateTime [sec]","1");

  int totalFrames = round(learnTime.back() * getFramesPerSecond());

  if (totalFrames > vreader.getNumFrames())
    totalFrames = vreader.getNumFrames();

  int frameUpdateInterval = std::max(round(sampleIntervals.back() * getFramesPerSecond()),1); 

  if (mUseMotion)
  {
    frameUpdateInterval = std::max(frameUpdateInterval,mNumMotionFrames);
  }

  int numFrames = totalFrames/frameUpdateInterval;

  PVASSERT(totalFrames > 0);
  PVASSERT(numFrames > 0);  

  int endFrame = startFrame + totalFrames;
  if (endFrame > vreader.getNumFrames())
  {
    endFrame = vreader.getNumFrames();
    totalFrames = endFrame-startFrame;
  }  

  mSettings.setInt("BackgroundLearner/numberOfBackgroundFrames",numFrames);
  mSettings.setInt("BackgroundLearner/modelUpdateInterval",1); 

  mpBackgroundLearner.reset(new BackgroundLearner());
  mpBackgroundLearner->init(mSettings.getPath()+"/BackgroundLearner");
  
  PVMSG("Learning background from video (%0.02lf minutes)...\n",totalFrames/getFramesPerSecond()/60);

  if (mTotalProgressUnits < 0)
  {
    mTotalProgressUnits = /*4*learnNumFrames+*/ vreader.getNumFrames();
  }

  Image32Ptr pFrame(new Image32(w,h));
  for (int frameNum = startFrame; frameNum <= endFrame; frameNum+= frameUpdateInterval)
  {
    vreader.setCurrentFrame(frameNum);
    vreader.readFrame(*pFrame);

    Image8Ptr pGrayFrame = getRoiFrame(pFrame);
    if (mUseMotion)
    {
      mLastFrames.push_back(pGrayFrame);
      for (int i = 1; i < mNumMotionFrames; i++)
      {
        Image32Ptr pNextFrame(new Image32(w,h));
        vreader.setCurrentFrame(frameNum+i);
        vreader.readFrame(*pNextFrame);
        Image8Ptr pNextGray = getRoiFrame(pNextFrame);
        mLastFrames.push_back(pNextGray);
      }

      pGrayFrame = getDiffImage(mLastFrames);
    }

    mpBackgroundLearner->process(pGrayFrame,true);
    if (mpBackgroundLearner->isBackgroundLearned())
    {
      if (mVerbose)
      {
        PVMSG("Finished learning...\n");
      }
      break;
    }

    //mCurrentProgressUnits += 4;
//    mpVmsPacket->updateProgress(1/mTotalProgressUnits);

    if (mVerbose)
    {
      PVMSG("updating frame: %i of %i\n",frameNum-startFrame,endFrame-startFrame);
    }
  }

  mLastRelearnCheck = 0;
  mRelearnBackground.zero();

  vreader.finish();  
  
}

//
// OPERATIONS
//

void
ForegroundSegmentMod::learnBlobSize()
{  
  VideoReaderAVIOpenCV vreader;

  Uint w,h;
  vreader.init(w,h,getVideoFileName().c_str());

  int startFrame = mSettings.getInt("BlobSizeEstimator/startFrame",0);
  int endFrame = mSettings.getInt("BlobSizeEstimator/endFrame",0);
  double interval = mSettings.getDouble("BlobSizeEstimator/sampleInterval [sec]",0.5);
  double fps = vreader.getFrameRate();
  const int maxFrame = vreader.getNumFrames()-10;
  if (endFrame == 0)
  {
    endFrame = maxFrame;
  }
  else
  {
    endFrame = std::min(endFrame,maxFrame);
  }
  int numFrames = endFrame-startFrame;

  int updateInterval = std::max(1,round(fps*interval));

  mTotalProgressUnits = numFrames/updateInterval;

  PVMSG("Learning blob size from video (%0.02lf minutes)...\n",(numFrames/fps)/60);

  if (mVerbose)
  {
    PVMSG("### Learning Blob Size...\n");
  }

  Image32Ptr pFrame(new Image32(w,h));

  mpBlobSizeEstimate.reset(new BlobSizeEstimate());
  mpBlobSizeEstimate->initializeParameters(
    mSettings.getInt("BlobSizeEstimator/numBlockRows",8),
    mSettings.getInt("BlobSizeEstimator/numBlockColumns",8),
    mSettings.getDouble("BlobSizeEstimator/sizeDistributionThreshold",0.75),
    mSettings.getFloat("BlobSizeEstimator/blobHeightFactor",2),
    (int)(mRegionOfInterest.height()*mRoiScaleFactor.y),
    (int)(mRegionOfInterest.width()*mRoiScaleFactor.x));

  Image8 filteredImage(mRegionOfInterest.width(),mRegionOfInterest.height());
  for (int frameNum = startFrame; frameNum < endFrame; frameNum+= updateInterval)
  {
    vreader.setCurrentFrame(frameNum);
    vreader.readFrame(*pFrame);

    mpBackgroundLearner->process(getRoiFrame(pFrame),false);
    mpBlobSizeEstimate->learnObjectSizeXY(*mpBackgroundLearner->getForegroundImage());

    mCurrentProgressUnits++;
//    mpVmsPacket->updateProgress(mCurrentProgressUnits/mTotalProgressUnits);

    if (mVerbose)
    {
      PVMSG("Blob Size: updating frame: %i of %i\n",frameNum-startFrame,endFrame-startFrame);
    }
  }

  mpBlobSizeEstimate->computeBlockEstimates();

  // Save visualization
  std::string fname = mSettings.getString("BlobSizeEstimator/visualizationFileName","");
  if (!fname.empty())
  {
    mpBlobSizeEstimate->visualize(*pFrame);
    pFrame->save(fname.c_str());
  }

  mpBlobSizeEstimate->save(mSettings.getString("BlobSizeEstimator/fileName",""));

  vreader.finish();
}

/*
Image32Ptr
ForegroundSegmentMod::getColorRoiFrame(Image32Ptr pImg)
{
  
  Image32Ptr pResized;

  //pImg->save("sample.png");

  if (mRegionOfInterest == Rectanglei(0,0,pImg->width(),pImg->height()))
  {
    //pResized = pImg;
    pResized.reset(new Image32());
    *pResized = *pImg;
    
  }
  else
  {
    pResized.reset(new Image32(mRegionOfInterest.width(),mRegionOfInterest.height()));
    pResized->crop(*pImg,mRegionOfInterest.x0,mRegionOfInterest.y0,
      mRegionOfInterest.width(),mRegionOfInterest.height());
  }

  //Image8Ptr pGrayFrame(new Image8(pResized->width(),pResized->height()));
  //ColorConvert::convert(*pResized,PvImageProc::RGB_SPACE,*pGrayFrame,PvImageProc::GRAY_SPACE);

  pResized->rescale(
    (int)(mRegionOfInterest.width()*mRoiScaleFactor.x),
    (int)(mRegionOfInterest.height()*mRoiScaleFactor.y),
    RESCALE_SKIP);

  // Apply polygon mask
  if (mpRoiMask.get())
  {
    Image8& mask = *mpRoiMask;
    Image32& dst = *pResized;
    int x,y;
    for (y = 0; y < mask.height(); y++)
    {
      for (x = 0; x < mask.width(); x++)
      {
        if (mask(x,y) == 255)
        {
        }
        else
        {
          dst(x,y) = PV_RGB(0,0,0); //mask(x,y) & dst(x,y);
        }
        
      }
    }
  }

  return pResized;   
}
*/

Image8Ptr
ForegroundSegmentMod::getRoiFrame(Image32Ptr pImg)
{
  
  Image32Ptr pResized;

  //pImg->save("sample.png");

  if (mRegionOfInterest == Rectanglei(0,0,pImg->width(),pImg->height()))
  {
    pResized = pImg;
  }
  else
  {
    pResized.reset(new Image32(mRegionOfInterest.width(),mRegionOfInterest.height()));
    pResized->crop(*pImg,mRegionOfInterest.x0,mRegionOfInterest.y0,
      mRegionOfInterest.width(),mRegionOfInterest.height());
  }

  Image8Ptr pGrayFrame(new Image8(pResized->width(),pResized->height()));
  ColorConvert::convert(*pResized,PvImageProc::RGB_SPACE,*pGrayFrame,PvImageProc::GRAY_SPACE);

  pGrayFrame->rescale(
    (int)(mRegionOfInterest.width()*mRoiScaleFactor.x),
    (int)(mRegionOfInterest.height()*mRoiScaleFactor.y),
    RESCALE_SKIP); 
  
  // Apply polygon mask
  if (mpRoiMask.get())
  {
    Image8& mask = *mpRoiMask;
    Image8& dst = *pGrayFrame;
    int x,y;
    for (y = 0; y < mask.height(); y++)
    {
      for (x = 0; x < mask.width(); x++)
      {
        dst(x,y) = mask(x,y) & dst(x,y);
      }
    }
  }

  return pGrayFrame;
   
}

void
ForegroundSegmentMod::filterByBlobSize(const Image8& foreground, Image8& output)
{  
  
  BlobSizeEstimate& blobSizeEstimate = *mpBlobSizeEstimate;
  output.resize(foreground.width(),foreground.height());
  for (int y = 0; y < foreground.height(); y++)
  {
    for (int x = 0; x < foreground.width(); x++)
    {
      // For faster processing, don't filter pixels that
      // do not have foreground.
      if (foreground(x,y) == 0)
      {
        output(x,y) = 0;
        continue;
      }
      float sizeX,sizeY;
      blobSizeEstimate.getEstimatedSize(x,y,sizeX,sizeY);
      double numForegroundPixels = 0;

      const Rectanglei blobRect(
        std::max(0,round(x-sizeX/2)),
        std::max(0,round(y-sizeY/2)),
        std::min(round(x+sizeX/2),(int)foreground.width()),
        std::min(round(y+sizeY/2),(int)foreground.height())
        );

      for (int iy = blobRect.y0; iy < blobRect.y1; iy++)
      {
        for (int ix = blobRect.x0; ix < blobRect.x1; ix++)
        {
          numForegroundPixels += foreground(ix,iy);
        }
      }

      float frameWeight = (float)std::min(255.0,numForegroundPixels/(sizeX*sizeY));
      const float minPercentOfBlobCovered = 0.0f; //TODO: ORIGNALLY 0.3f;
      output(x,y) = frameWeight > minPercentOfBlobCovered*255 ? (Uint8)(round(frameWeight)) : 0;
    }
  }  
}

void 
ForegroundSegmentMod::checkBackground()
{ 

  mLastRelearnCheck++;
  float count = 0.0; 

  float fps = getImageAcquireModule()->getFramesPerSecond();
  assert(fps != 0.0);  
  
  
  //PVMSG("%f >= %f\n", mLastRelearnCheck / fps, mRelearnCheckInterval);

  if (mLastRelearnCheck / fps >= mRelearnCheckInterval)
  { 
    mLastRelearnCheck = 0;

    if (mRelearnBackground.cols() == 1 && mRelearnBackground.rows() == 1)
    {
      mRelearnBackground.resize(mpFilteredForeground->width(), mpFilteredForeground->height());     
      mRelearnBackground.zero();
    }       

    for (int n = 0; n < mpFilteredForeground->height(); n++)
    {
      for (int m = 0; m < mpFilteredForeground->width(); m++)
      {
        
        if ((*mpFilteredForeground)(m,n) == 0)
        {
          mRelearnBackground(m,n) = 0;
        }
        else
        {          
          mRelearnBackground(m,n) += round(mRelearnCheckInterval * fps);

          if (mRelearnBackground(m,n) >= mRelearnInertTime * fps)
          {
            float blobWidth = 0.0;
            float blobHeight = 0.0;
            getBlobSizeEstimate()->getEstimatedSize(m,n, blobWidth, blobHeight);
            
            assert(blobWidth*blobHeight != 0.0);
            count += 1.0/(blobWidth * blobHeight);
          }          
        }
      }
    }          
   
    if (count >= mRelearnBlobThresh)
    {
      PVMSG("\nRelearning background: %f\n\n", count);
      if (mLearnBackgroundContinuously)
      {        
        mpBackgroundLearner->resetValid();
        mpBackgroundLearner->resetIntervals();        
        mLastRelearnCheck = 0;
      }
      else
        learnBackgroundFromVideo();
    }    
  }// of if(mLastRelearnCheck/fps >= mRelearnCheckInterval)
}

void
ForegroundSegmentMod::process()
{   
//  mBenchmark.beginSample();

  Image32Ptr pFrame = getImageAcquireModule()->getCurrentFramePtr();
  
  Image8Ptr pGrayFrame = getRoiFrame(pFrame);

  /*
  Image32Ptr pColorFrame = getColorRoiFrame(pFrame);

  if (mCombinedSeg.width() != pGrayFrame->width() || mCombinedSeg.height() != pGrayFrame->height())
  {
    mCombinedSeg.resize(pGrayFrame->width(),pGrayFrame->height());
  }
  */
  
  //HACK  

  
  
  
  //TEMPORARY CHECK FOR AUTOEXPOSURE, Todo: move to a separate function, clean up this section
  if (mCheckForExposure)
  {  
    if (mLastMeanIntensity == 0.0f)
    {
      mLastMeanIntensity = pGrayFrame->mean();
      //FIX THIS BUG
      mLastMeanSeg = 0;//HACK //mpBackgroundLearner->getForegroundImage()->mean();      
      mLastExposureCheck = getCurrentTime();
    }
    else
    {
      double time = getCurrentTime();

      if (time > mLastExposureCheck + 1.0)
      {
        mLastExposureCheck = time;

        double meanSeg = mpBackgroundLearner->getForegroundImage()->mean(); // meanSeg is the mean of foreground of learned background
        double meanIntensity = pGrayFrame->mean();

        //Print out average intensity value
        //PVMSG("MEAN INTENSITY: %f\tMEAN SEGMENTATION: %f\n", meanIntensity, meanSeg);        

        //150 based on experimentation on 4 short clips
        if (meanSeg > 150.0)	// if mean of segmentation is larger than 150
        {
          mpBackgroundLearner->resetIntervals();	// reset background?
          mpBackgroundLearner->resetValid();
          //mpBackgroundLearner->reset();

          if (mMotionHack)
          {          
            mMotionTime.setAll(mMinNoMotionTime * 2.0);

            //hack change added
            
            //if (!mUseMotion)
            //  (*mpLastSafeImage) = (*pColorFrame);
              
            (*mpLastSafeImage) = (*pGrayFrame);
          }

          //write fake event to logfile for later checking
          //PVMSG("WRITING TO FILE!!! - %s\n", mExposureLogFilename.c_str());
          std::ofstream logFile(mExposureLogFilename.c_str(), std::ios::app);                   

          logFile << DateTime(time).toString();
          logFile << ",1.0";
          logFile << "," << mLastMeanIntensity;
          logFile << "," << meanIntensity;
          logFile << "," << mLastMeanSeg;
          logFile << "," << meanSeg << std::endl;
          logFile.close();
        }

        mLastMeanIntensity = meanIntensity;	// mLastMeanIntensity is doing nothing!
        mLastMeanSeg = meanSeg;				// mLastMeanSeg is doing nothing!
      } // of if(time>mLastExposureCheck+1.0)
    } // of else(mLastMeanIntensity == 0.0f)
  } // of if (mCheckForExposure)
  
  //END TEMP

  

  if (mUseMotion)
  {
    mLastFrames.push_back(pGrayFrame);
    Image8Ptr pDiffImage(new Image8(pGrayFrame->width(),pGrayFrame->height()));
    if (mLastFrames.size() < mNumMotionFrames)
    {
      pDiffImage->zero();
    }
    else
    {
      pDiffImage = getDiffImage(mLastFrames);
    }
    pGrayFrame = pDiffImage;
  }  

  
  
  if (mMotionHack)
  { 
    Image8Ptr pMotion = mpMotionMod->getFilteredMotion();    

    if (mFirstPass)
    {
      mFirstPass = false;
      //mpLastSafeImage.reset(new Image32(pColorFrame->width(), pColorFrame->height()));
      mpLastSafeImage.reset(new Image8(pGrayFrame->width(), pGrayFrame->height()));
      mDilatedMotion.resize(pMotion->width(), pMotion->height());
      mDilatedMotion.setAll(0);
      mMotionTime.resize(pMotion->width(), pMotion->height());
      mMotionTime.setAll(0.0);

      //(*mpLastSafeImage) = *pColorFrame;
      (*mpLastSafeImage) = *pGrayFrame;
    }
    
    mDilatedMotion.setAll(0);

    for (int y = 2; y < pMotion->height()-2; y++)
    {
      for (int x = 2; x < pMotion->width()-2; x++)
      {
        if ( 
          (*pMotion)(x-2,y-2) != 0 || 
          (*pMotion)(x-1,y-2) != 0 || 
          (*pMotion)(x,y-2) != 0 || 
          (*pMotion)(x+1,y-2) != 0 ||
          (*pMotion)(x+2,y-2) != 0 || 
          (*pMotion)(x-2,y-1) != 0 || 
          (*pMotion)(x-1,y-1) != 0 || 
          (*pMotion)(x,y-1) != 0 ||
          (*pMotion)(x+1,y-1) != 0 || 
          (*pMotion)(x+2,y-1) != 0 || 
          (*pMotion)(x-2,y) != 0 || 
          (*pMotion)(x-1,y) != 0 ||
          (*pMotion)(x,y) != 0 || 
          (*pMotion)(x+1,y) != 0 || 
          (*pMotion)(x+2,y) != 0 || 
          (*pMotion)(x-2,y+1) != 0 ||
          (*pMotion)(x-1,y+1) != 0 || 
          (*pMotion)(x,y+1) != 0 || 
          (*pMotion)(x+1,y+1) != 0 || 
          (*pMotion)(x+2,y+1) != 0 ||
          (*pMotion)(x-2,y+2) != 0 || 
          (*pMotion)(x-1,y+2) != 0 || 
          (*pMotion)(x,y+2) != 0 || 
          (*pMotion)(x+1,y+2) != 0 ||
          (*pMotion)(x+2,y+2) != 0        
          )
          mDilatedMotion(x,y) = 255;
      }
    }

    float avgFrameDur = 1.0 / getImageAcquireModule()->getFramesPerSecond();    

    for (int y = 0; y < mpLastSafeImage->height(); y++)
    {
      for (int x = 0; x < mpLastSafeImage->width(); x++)
      {
        //if (mDilatedMotion(x,y) == 0)
        //  (*mpLastSafeImage)(x,y) = (*pGrayFrame)(x,y);

        if (mDilatedMotion(x,y) != 0.0)
          mMotionTime(x,y) = 0.0;
        else
          mMotionTime(x,y) += avgFrameDur;

        //INPUT TIME FOR PIXELS TO BE CONSIDERED HERE
        if (mMotionTime(x,y) >= mMinNoMotionTime)
          (*mpLastSafeImage)(x,y) = (*pGrayFrame)(x,y);          
      }
    }   

    mpBackgroundLearner->process(mpLastSafeImage,true);
    mpBackgroundLearner->process(pGrayFrame,false);
    //mpBackgroundLearner->process(pColorFrame,false);
  } // of if (mMotionHack)
  else
  {
    //ORIGINAL CODE
    if (mUseMotion)
    { 
      if (mLearnBackgroundContinuously)
        mpBackgroundLearner->process(pGrayFrame,true);
      else
        mpBackgroundLearner->process(pGrayFrame,false); //do not update background model
    }
    else    
    {   
      
      //if (mLearnBackgroundContinuously)
      //  mpBackgroundLearner->process(pColorFrame,true);
      //else
      //  mpBackgroundLearner->process(pColorFrame,false); //do not update background model
        
      if (mLearnBackgroundContinuously)
        mpBackgroundLearner->process(pGrayFrame,true);
      else
        mpBackgroundLearner->process(pGrayFrame,false); //do not update background model
       
    }
  } // of else (mMotionHack)
  
  if (mUseBlobSize)
  {
    if (!mpFilteredForeground.get())
    {
      mpFilteredForeground.reset(new Image8(pGrayFrame->width(),pGrayFrame->height()));
    }

    filterByBlobSize(*mpBackgroundLearner->getForegroundImage(),*mpFilteredForeground);	// mpFilteredForeground is the output
    
     
//    if (!mUseMotion)
//    {
//      Image8& seg = *mpBackgroundLearner->getForegroundImage();
//      Image8& normSeg = *mpBackgroundLearner->getForegroundNormalizedImage();
//
//      for (int y = 0; y < mCombinedSeg.height(); y++)
//        for (int x = 0; x < mCombinedSeg.width(); x++)
//        {
//          mCombinedSeg(x,y) = seg(x,y) & normSeg(x,y);
//        }
//    }
//    else
//    {
//      mCombinedSeg = *mpBackgroundLearner->getForegroundImage();
//    }
//    filterByBlobSize(mCombinedSeg,*mpFilteredForeground);
    
    
    
    //Check to determine if the background needs to be relearned (possibly restart settings)
    if (mEnableRelearnBackground)
      checkBackground();    
  }  

  mCurrentProgressUnits += 1;
//  mpVmsPacket->updateProgress(mCurrentProgressUnits/mTotalProgressUnits);
  
  if (mSaveSegmentation)
    saveSegmentationOutput();  


/*
  const Image8& seg = *mpBackgroundLearner->getForegroundImage();
  int foregroundPixels = 0;

  for (int y = 0; y < seg.height(); y++)
  {
    for (int x = 0; x < seg.width(); x++)
    {
      if (seg(x,y) == 255)
        foregroundPixels++;
    }
  }
  
  PVMSG("Foreground Pct: %f\n", (float)foregroundPixels/mUnmaskedRoiPixels);
  */ 

//  mBenchmark.endSample();

}

void 
ForegroundSegmentMod::visualize(Image32& img)
{  
  if (!mPaintVisualization)
  {
    return;
  }

  // Clear areas outside the image.
  img.rectFill(mFrameWidth,0,mFrameWidth*2,mFrameHeight,0);
  
  const Vector2f scaleFactor = getRoiScaleFactor();  
  
  const Rectanglei roi = getRoiRectangle();

  const float scale_x = 1.0f / scaleFactor.x;
  const float scale_y = 1.0f / scaleFactor.y;

  {
    Image32 img1;
    ColorConvert::convert(*mpBackgroundLearner->getForegroundImage(),PvImageProc::GRAY_SPACE,
      img1,PvImageProc::RGB_SPACE);

    /*
    ColorConvert::convert(mCombinedSeg,PvImageProc::GRAY_SPACE,
      img1,PvImageProc::RGB_SPACE);
      */


    img1.rescale(img1.width()*scale_x,img1.height()*scale_y);    
    img.paste(img1,mFrameWidth+roi.x0,roi.y0);
  }
  
  if (mUseBlobSize)
  {
    img.rectFill(0,mFrameHeight,mFrameWidth*2,mFrameHeight*2,0);
    Image32 img1;    
    
    ColorConvert::convert(*getFilteredForeground(),PvImageProc::GRAY_SPACE,
      img1,PvImageProc::RGB_SPACE);
    img1.rescale(img1.width()*scale_x,img1.height()*scale_y);
    img.paste(img1,roi.x0,mFrameHeight+roi.y0);
  }

  
  std::list<int>::const_iterator iEvtChan;
  for (iEvtChan = mEventChannels.begin(); iEvtChan != mEventChannels.end(); iEvtChan++)
  {
    int i;
    for (i = 1; i < mRoiPolygon[*iEvtChan].size(); i++)
    {
      img.line(
        mRoiPolygon[*iEvtChan][i-1].x,mRoiPolygon[*iEvtChan][i-1].y,
        mRoiPolygon[*iEvtChan][i].x,mRoiPolygon[*iEvtChan][i].y,
        PV_RGB(0,255,128));        
    }
  }   

  //TODO: TEMP MOTION HACK  
/*
  if (mMotionHack)
  { 
    
    Image32 safeImg;

    ColorConvert::convert(*mpLastSafeImage,PvImageProc::GRAY_SPACE,
        safeImg,PvImageProc::RGB_SPACE);

    safeImg.rescale(320,240, RESCALE_LINEAR);

    img.paste(safeImg,0,240);


    Image32 motionImg;
    ColorConvert::convert(mDilatedMotion,PvImageProc::GRAY_SPACE,
        motionImg,PvImageProc::RGB_SPACE);

    motionImg.rescale(320,240, RESCALE_LINEAR);
    img.paste(motionImg,320,240);
       
  }
  */

   //PVMSG("ROI SCALE FACTOR: %f %f\n", mRoiScaleFactor.x, mRoiScaleFactor.y);
/*
  if (mUseNormalizedG)
  {
    Image32 colImg;
    ColorConvert::convert(mNormalizedG, PvImageProc::GRAY_SPACE, colImg, PvImageProc::RGB_SPACE);
    colImg.rescale(320,240,RESCALE_CUBIC);
    img.paste(colImg, 320,240);
  }
  */
}

void
ForegroundSegmentMod::saveSegmentationOutput()
{
  const float scaleX = 0.4f;
  const float scaleY = 0.4f;


  if (getCurrentTime() < mNextSaveSegTime)
    return;

  mNextSaveSegTime = getCurrentTime() + mSaveSegInterval;

  //

  if (getCurrentTime() >= mNextBreakFileTime)
  {   
    mSegOutputFile.close();
    mNextBreakFileTime = getCurrentTime() + mSaveSegFileLengthInterval;
    
  }

  if (!mSegOutputFile.is_open())
  {    
    DateTime dt;
    std::string filename = "Seg_VidSrcId" + aitSprintf("%d", mVideoSourceId);
    filename += "_" + dt.format("%Y%m%d_%H%M%S.txt");
    std::string loc = ait::combine_path(mSaveSegOutputFolder, filename);    

    mSegOutputFile.clear();
    mSegOutputFile.open(loc.c_str());
  }     

  PVASSERT(mSegOutputFile.is_open());


  Image8 img = *getForeground();

  int newHeight = std::max((int)(img.height() * scaleY), 1);
  int newWidth = std::max((int)(img.width() * scaleX), 1);

  img.rescale(newWidth, newHeight);

  if (img.maximum() == 0)
    return;
  
  
  mSegOutputFile << aitSprintf("%.6f", getCurrentTime()) << "," << newWidth << "," << newHeight << std::endl;
  
  for (int y = 0; y < newHeight; y++)
  {
    for (int x = 0; x < newWidth; x++)
    {
      mSegOutputFile << (int)img(x,y); 


      if (x < newWidth - 1)
        mSegOutputFile << ",";
    }

    mSegOutputFile << std::endl;
  }  
}

//
// ACCESS
//

Image8Ptr
ForegroundSegmentMod::getForeground()
{  
  return mpBackgroundLearner->getForegroundImage();  
}

Image8Ptr
ForegroundSegmentMod::getFilteredForeground()
{  
  return mpFilteredForeground;
}

ImageAcquireModPtr ForegroundSegmentMod::getImageAcquireModule()
{
  return mpImageAcquireMod;
}

MotionDetectionModPtr ForegroundSegmentMod::getMotionDetectionModule()
{
  return mpMotionMod;
}

//
// INQUIRY
//

}; // namespace vision

}; // namespace ait


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#pragma warning(disable:4503)
#endif

#include "legacy/vision_blob/PersonShapeTrackerMod.hpp"

#include <legacy/vision_tools/PolygonGetMask.hpp>

#include <legacy/vision_tools/ColorConvert.hpp>

#include <legacy/types/DateTime.hpp>

#include <legacy/types/strutil.hpp>
#include <boost/lexical_cast.hpp>

//#include <ImageAcquireMod.hpp>

#include <legacy/vision_tools/HistogramBackgroundModel.hpp>

#include <cv.h>
#include <cxcore.h>
//#include <ipp.h>
#include <sstream>
//#include <legacy/PvCanvasImage.hpp>


namespace ait 
{

namespace vision
{

PersonShapeTrackerMod::PersonShapeTrackerMod()
  : RealTimeMod(getModuleStaticName())
{  
}

void 
  PersonShapeTrackerMod::initExecutionGraph(std::list<VisionModPtr>& srcModules)
{
  RealTimeMod::initExecutionGraph(srcModules);  

  getModule("ForegroundSegmentModShape",mpForegroundSegmentMod);  
  srcModules.push_back(mpForegroundSegmentMod);    

  //getModule("ForegroundSegmentModEdge",mpEdgeMod);
  //srcModules.push_back(mpEdgeMod);
}

PersonShapeTrackerMod::~PersonShapeTrackerMod()
{
}

void 
  PersonShapeTrackerMod::init()
{  
  RealTimeMod::init();   

  mpVmsPacket.reset(new VmsPacket(this));

  Image32Ptr pFrame = getImageAcquireModule()->getCurrentFramePtr();

  assert(pFrame.get());
  mFrameWidth = pFrame->width();
  mFrameHeight = pFrame->height(); 

  mEventChannels = mSettings.getIntList("eventChannelId", "", false);  

  mBenchmark.init(mSettings.getPath(),"Blob Tracking");

  mIsTopViewCamera = mSettings.getBool("isTopViewCamera [bool]",0);

  mVerbose = mSettings.getBool("Visualization/printMessages [bool]",0);

  mPaintVisualization = mSettings.getBool("Visualization/enabled [bool]",1);

  mSendExtendedAttributes = mSettings.getBool("sendExtendedAttributes [bool]",0);

  mSendExtendedEventData = mSettings.getBool("sendExtendedEventData [bool]", 0);

  mSendPreprocessTrajectories = mSettings.getBool("sendPreprocessTrajectories [bool]",0);

  std::string drawMode = mSettings.getString("Visualization/drawTracksMode","ERROR");

  if (drawMode == "all")
  {
    mDrawTracksMode = ALL;
  }
  else if (drawMode == "alive")
  {
    mDrawTracksMode = ALIVE;
  }
  else if (drawMode == "none")
  {
    mDrawTracksMode = NONE;
  }
  else
  {
    PvUtil::exitError("Incorrect or missing setting for Visualization/drawTracksMode");
  }

  //Alter the visualization frame size if necessary
  if (mPaintVisualization)
    getImageAcquireModule()->setVisualizationFrameSize(Vector2i(640,480));

  mMaxSnakeSize = mSettings.getInt("numFrames",10);
  mMinTrackCreationDistance = mSettings.getFloat("minTrackCreationDistance",1.0);

  mpNewTrackScanImage.reset(new Image8());

  mNewTrackId = 1;

  mMaxConcurrentTracks = mSettings.getInt("maxConcurrentTracks",10);

  mVisualizeSnakeTail = mSettings.getBool("Visualization/showFrameAtTail [bool]",0);

  if (mVisualizeSnakeTail)
  {
    mLastColorFrames.setCapacity(mMaxSnakeSize);
  }

  mPostProcessLowerInterval = mSettings.getFloat("minPostProcessInterval",60.0);
  mPostProcessUpperInterval = mSettings.getFloat("maxPostProcessInterval", 600.0);

  mNextPostProcessTime = getCurrentTime() + mPostProcessLowerInterval;
  mNextForcePostProcessTime = getCurrentTime() + mPostProcessUpperInterval;

  //Initialize the map of event polygons
  std::list<SimplePolygon> polyList = convertList(mSettings.getString("eventsPolygon",""));
  std::list<SimplePolygon>::const_iterator iPoly;

  std::list<int>::const_iterator iEvtChan = mEventChannels.begin();
  for (iPoly = polyList.begin(); iPoly != polyList.end(); iPoly++)
  {    
    mEventsPolygon[*iEvtChan] = *iPoly;
    iEvtChan++;
  }

  //Tell the vms packet that each event channel will await completion notification
  for (iEvtChan = mEventChannels.begin(); iEvtChan != mEventChannels.end(); iEvtChan++)
    mpVmsPacket->enableCompletionNotification(*iEvtChan);

  if (!mEventsPolygon.empty() && mEventChannels.size() != mEventsPolygon.size())
    PvUtil::exitError("ERROR: BlobTrackerMod: Different number of event channel IDs and polygons");

  mSavePreTrajectories = mSettings.getBool("savePreTrajectories [bool]", false);
  mSaveCsvFileEvents = mSettings.getBool("saveCsvFileEvents [bool]", false);

  mTrajectoryOutputFolder = mSettings.getString("trajectoryOutputFolder", "");

  if (mSavePreTrajectories || mSaveCsvFileEvents)  
    if (!PvUtil::fileExists(mTrajectoryOutputFolder.c_str()))
      PvUtil::exitError("BlobTrackerMod: ERROR: 'trajectoryOutputFolder' does not exist.");


  //guaranteed to be unique by outside process that inits config files  
  mVideoSourceId = atoi(Settings::replaceVars("<VideoSourceId>",mSettings.getPath()).c_str());
  PVASSERT(mVideoSourceId != -1);

  mSendTrajectoriesAsEventAttribute = mSettings.getBool("sendTrajectory [bool]",false);

  //Resize the motion module to have the same size as the foreground segmentation size

  /*
  //TODO: MAKE SURE THAT THIS HACK WORKS WHEN UNCOMMENTING CODE
  if (mIsMotionEnabled)
  {  
  Rectanglei roi = mpForegroundSegmentMod->getRoiRectangle();
  Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor(); 

  mMaxNoMotionTime = mSettings.getFloat("maxNoMotionTime", 60.0);

  mpMotionMod->setScaleAndRoi(scaleFactor, roi);    
  }
  */


  //initialize the start polygon list
  std::list<SimplePolygon> startTrackPolyList;
  startTrackPolyList = convertList(mSettings.getString("startTrackPolygon","(0,0,320,0,320,240,0,240,0,0)"));

  for (iPoly = startTrackPolyList.begin(); iPoly != startTrackPolyList.end(); iPoly++)
  {
    SimplePolygon startTrackPoly;
    startTrackPoly = *iPoly;

    Polygon p;

    //Remove last redundant entry, polygon class does not use it
    startTrackPoly.pop_back();
    p.setVertices(startTrackPoly);

    //Add polygon object to start track list
    mStartTrackPolyList.push_back(p);
  }  

  //initialize the kill track polygon
  std::list<SimplePolygon> killTrackPolyList;
  killTrackPolyList = convertList(mSettings.getString("killTrackPolygon", ""));

  for (iPoly = killTrackPolyList.begin(); iPoly != killTrackPolyList.end(); iPoly++)
  {
    SimplePolygon killTrackPoly;
    killTrackPoly = *iPoly;

    Polygon p;

    //Remove last redundant entry, polygon class does not use it
    killTrackPoly.pop_back();
    p.setVertices(killTrackPoly);  

    mKillTrackPolyList.push_back(p);
  }

  //Addition for person detection region

  //initialize the person track polygon
  std::list<SimplePolygon> personTrackPolyList;
  personTrackPolyList = convertList(mSettings.getString("personTrackPolygon", ""));

  for (iPoly = personTrackPolyList.begin(); iPoly != personTrackPolyList.end(); iPoly++)
  {
    SimplePolygon personTrackPoly;
    personTrackPoly = *iPoly;

    Polygon p;

    //Remove last redundant entry, polygon class does not use it
    personTrackPoly.pop_back();
    p.setVertices(personTrackPoly);  

    mPersonTrackRegionList.push_back(p);
  }

  //initialize the good track region polygon
  std::list<SimplePolygon> goodTrackPolyList;
  goodTrackPolyList = convertList(mSettings.getString("goodTrackRegionPolygon", ""));


  //get ROI in pixel coords from FSM 
  Rectanglei intRoi = mpForegroundSegmentMod->getRoiRectangle();
  Rectanglef roi((float)intRoi.x0, 
    (float)intRoi.y0, 
    (float)intRoi.x1, 
    (float)intRoi.y1);


  //generate the masks  
  Image8 tmp;

  mStartTrackMask.resize(round(roi.width()), round(roi.height()));
  mStartTrackMask.setAll(0);
  for (iPoly = startTrackPolyList.begin(); iPoly != startTrackPolyList.end(); iPoly++)
  {
    getMask(*iPoly, roi,tmp);
    for (int i = 0; i < mStartTrackMask.size(); i++)
      mStartTrackMask(i) |= tmp(i);
  }

  mKillTrackMask.resize(round(roi.width()), round(roi.height()));
  mKillTrackMask.setAll(0);
  for (iPoly = killTrackPolyList.begin(); iPoly != killTrackPolyList.end(); iPoly++)
  {
    getMask(*iPoly, roi,tmp);
    for (int i = 0; i < mKillTrackMask.size(); i++)
      mKillTrackMask(i) |= tmp(i);
  }

  //Added for person region

  mPersonTrackMask.resize(round(roi.width()), round(roi.height()));
  mPersonTrackMask.setAll(0);
  for (iPoly = personTrackPolyList.begin(); iPoly != personTrackPolyList.end(); iPoly++)
  {
    getMask(*iPoly, roi,tmp);
    for (int i = 0; i < mPersonTrackMask.size(); i++)
      mPersonTrackMask(i) |= tmp(i);
  }

  //Added for good track region polygon
  mGoodTrackRegionMask.resize(round(roi.width()), round(roi.height()));
  mGoodTrackRegionMask.setAll(0);
  for (iPoly = goodTrackPolyList.begin(); iPoly != goodTrackPolyList.end(); iPoly++)
  {
    getMask(*iPoly, roi,tmp);
    for (int i = 0; i < mGoodTrackRegionMask.size(); i++)
      mGoodTrackRegionMask(i) |= tmp(i);
  }


  //rescale the mask by the appropriate scaling factor
  Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();

  mStartTrackMask.rescale(
    roi.width()*scaleFactor.x,
    roi.height()*scaleFactor.y);

  mKillTrackMask.rescale(
    roi.width()*scaleFactor.x,
    roi.height()*scaleFactor.y);

  //Added for person region
  mPersonTrackMask.rescale(
    roi.width()*scaleFactor.x,
    roi.height()*scaleFactor.y); 

  //Added for good track region polygon
  mGoodTrackRegionMask.rescale(
    roi.width()*scaleFactor.x,
    roi.height()*scaleFactor.y); 


  mRepelTracks = mSettings.getBool("repelTracks", false);
  mThresholdList=mSettings.getIntList("thresholdList","242,229,204,178,153,140",true);




  //Make sure the threshold list is sorted in the right way
  //TO DO

  //INITIALIZE PERSON SHAPE ESTIMATOR  

  mpFilteredForeground.reset(new Image8(1,1));

  mpPersonShapeEstimator.reset(new PersonShapeEstimator());
  mpPersonShapeEstimator->init(mSettings.getPath() + "/PersonShapeEstimator");    

  //Used to measure raw segmentation statistics in polygon region
  mMeasureRawSegmentation = mSettings.getBool("measureRawSegmentation [bool]", false);
  mMeasureRawSegRate = mSettings.getFloat("measureRawSegmentationSampleRate [samples per second]", 1.0f);
  mMeasureRawSegFilePath = mSettings.getString("measureSegFilePath", "C:/");

  std::list<SimplePolygon> rawSegPolyList;
  rawSegPolyList = convertList(mSettings.getString("segmentationMeasurePolygon","(0,0,320,0,320,240,0,240,0,0)"));

  //mMeasureSegMask.resize(round(roi.width()), round(roi.height()));
  //mMeasureSegMask.setAll(0);
  for (iPoly = rawSegPolyList.begin(); iPoly != rawSegPolyList.end(); iPoly++)
  {
    getMask(*iPoly, roi,tmp);
    mMeasureSegMask.push_back(tmp);
    tmp.setAll(0);
  }

  for(int r=0;r<mMeasureSegMask.size();r++)
  {
    mMeasureSegMask[r].rescale(
      roi.width()*scaleFactor.x,
      roi.height()*scaleFactor.y);
    mSegmentationScaling.push_back(1);
  }
  //Used for debugging
  /* char fileName[256];
  for(int r=0;r<mMeasureSegMask.size();r++)
  {
  sprintf(fileName,"C:/temp%d.pgm",r+1);
  mMeasureSegMask[r].save(fileName);
  }*/

  for(int s=0;s<mMeasureSegMask.size();s++)
  {
    for(int r=0;r<mMeasureSegMask[s].size();r++)
      if(mMeasureSegMask[s](r)>0)
        mSegmentationScaling[s]++;
  }

  //for(int r=0;r<mMeasureSegMask.size();r++)
  // PVMSG("%d %d\n",r,mSegmentationScaling[r]);

  mCurrentFrameTime=0.0f;

  //Added for new tracking algorithm
  //Code for checking if two persons have overlapping person shapes
  mPersonShapeDistanceThresh = mSettings.getFloat("PersonShapeDistanceThreshold",1.0);

  Rectanglei roiRect = mpForegroundSegmentMod->getRoiRectangle();
  Vector2i offset;
  offset.x = std::min(roiRect.x0,roiRect.x1) * scaleFactor.x;
  offset.y = std::min(roiRect.y0,roiRect.y1) * scaleFactor.y;  
  Image32 tempImage(roi.width()*scaleFactor.x,roi.height()*scaleFactor.y);
  tempImage.setAll(PV_RGB(0,0,0));

  Rectanglei tempRect;

  bool flagUsePt = false;
  realWorldCoords.resize(roi.width()*scaleFactor.x,roi.height()*scaleFactor.y);
  realWorldCoords = mpPersonShapeEstimator->getWorldCoordsVector();

  mPersonLowerThreshold = mSettings.getInt("LowerScanningThreshold",51);
  mPersonTrackMaxDistance = mSettings.getFloat("PersonTrackMaxDistance",2.0);
  noUseTrackPeriod = mSettings.getInt("PersonNoUseTrackAge",0);

  mFilterSegmentationFlag = mSettings.getBool("OrderedSegmentationFilter [bool]",true);
  mFilterSegmentationOrder = mSettings.getInt("SegmentationFilteringOrder", 3);  

  //Create image in memory so pointer never return null
  mpRawSegmentationImage.reset(new Image8(1,1));

  //We have to update the XML settings for the edge-based
  //background model to convert

  std::string fsmPath = "VisionMods/" + 
    getImageAcquireModule()->getExecutionPathName() + 
    "/ForegroundSegmentModShape/EdgeBackgroundLearner";

  Settings set;
  set.changePath(fsmPath);

  //Grab the relevant settings
  std::list<double> learnTimes = 
    set.getDoubleList("modelLearnTime [sec]","10");

  std::list<double> sampleIntervals =
    set.getDoubleList("frameUpdateTime [sec]", ".5");  

  PVASSERT(learnTimes.size() == sampleIntervals.size());

  std::list<int> numFrames;
  std::list<int> updateInterval;

  std::list<double>::iterator iTime;
  std::list<double>::const_iterator iInterval = sampleIntervals.begin();

  //Convert the given ForegroundSegmentMod settings from seconds into frames
  for (iTime = learnTimes.begin(); iTime != learnTimes.end(); iTime++)
  {     
    updateInterval.push_back(std::max(round(getFramesPerSecond()* (*iInterval)), 1));
    numFrames.push_back(std::max(round((*iTime) * getFramesPerSecond())/updateInterval.back(),1));
    iInterval++;
  }

  //Set the frame-based settings for the background learner
  set.setIntList("numberOfBackgroundFrames", numFrames);
  set.setIntList("modelUpdateInterval", updateInterval);
  PVMSG("Learning background continously...\n");

  //Initial the background learner we are using on edges
  mpEdgeBackgroundLearner.reset(new BackgroundLearner());

  //We want to use the same settings as the FSM for learning except for forceOneCluster
  mpEdgeBackgroundLearner->init(fsmPath);

  mpEdgeImage.reset(new Image8(1,1));
  mpEdgeLearnedImage.reset(new Image8(1,1));
  mpAdjustedEdgeImage.reset(new Image8(1,1));
  mpCombinedEdgeAndSegImage.reset(new Image8(1,1));
  mpFilteredEdgeImage.reset(new Image8(1,1));

  Vector3i selectColor = mSettings.getCoord3i("cartColor",Vector3i(133,43,64));
  Vector3i selectColor2 = mSettings.getCoord3i("cartColor2",Vector3i(0,0,0));
  mSelectedColor = PV_RGB(selectColor.x,selectColor.y,selectColor.z);
  mSelectedColor2 = PV_RGB(selectColor2.x,selectColor2.y,selectColor2.z);
  mSelectedColorDistH = mSettings.getInt("cartDistH",8);
  mSelectedColorDistS = mSettings.getInt("cartDistS",255);
  mSelectedColorDistVAndS = mSettings.getInt("cartDistSAndV",229);
  mSelectedColorDistV = mSettings.getInt("cartDistV",102);
 
  mEdgeDetectThreshold = mSettings.getInt("edgeDetectThreshold",120);
  mMinCartComponentSize = mSettings.getInt("minCartComponentSize",50);
  mMaxCartComponentSize = mSettings.getInt("maxCartComponentSize",200);

  //PVMSG("Debug %d %d %d",RED(mSelectedColor),GREEN(mSelectedColor),BLUE(mSelectedColor));

  //Added by Rahul
  //Weight used for determining combination of edge and plain segmented image
  mEdgeWeight = mSettings.getFloat("ImageCombinationEdgeWeight",0.5f);
  mRegSegPersonDetectionLowerThresh = mSettings.getInt("RegularSegPersonDetectionLowerThresh",79);  
  mEdgeSegPersonDetectionLowerThresh = mSettings.getInt("EdgeSegPersonDetectionLowerThresh",79);
  //Done

  mMaxPersonTrackDistance = mSettings.getFloat("maxPersonTrackDistance",10.0f);

  mDebug = mSettings.getBool("debugFlag [bool]",false);

  std::string tempFile;
  if(mDebug)
    tempFile = mSettings.getString("debugFile","C:/debug.txt");

  mDebugFile.open(tempFile.c_str());

  mFrameNum = 0;

  mMaxCartTrackDistance = mSettings.getFloat("maxCartTrackDistance",10.0f);

  mOccludedPersonThresh = mSettings.getInt("occludedPersonThresh",65);

  mOcclusionRegions=mSettings.getIntList("occlusionRegions","120,160,200,90,120,150",true);

  mDetectCarts = mSettings.getBool("DetectCarts [bool]",false);

  mpDilatedForegroundSegmentationImage.reset(new Image8(mpRawSegmentationImage->width(),mpRawSegmentationImage->height()));
  
  mpDilatedForegroundSegmentationEdgeImage.reset(new Image8(mpRawSegmentationImage->width(),mpRawSegmentationImage->height()));

  mDetectCartsUsingEdges = mSettings.getBool("DetectCartsUsingEdges [bool]",false);
  
  mUseEdgePersonDetection = mSettings.getBool("UseEdgePersonDetection [bool]",false);
  
  if(mDetectCartsUsingEdges)
  {
    //Initialize the cart shape Polygons from the vision settings
    std::list<SimplePolygon> cartShapePolyList;
    cartShapePolyList = convertList(mSettings.getString("cartShapes", "(-18,-11,18,-9,18,9,-18,11,-18,-11):(-20,-13,20,-9,20,9,-20,13,-20,-13):(-19,-13,19,-11,19,11,-19,13,-19,-13):(-20,-13,20,-9,20,9,-20,13,-20,-13):(-18,-11,18,-9,18,9,-18,11,-18,-11):(-18,-11,18,-9,18,9,-18,11,-18,-11):(-20,-12,20,-10,20,10,-20,12,-20,-12):(-19,-12,19,-10,19,10,-19,12,-19,-12):(-20,-12,20,-10,20,10,-20,12,-20,-12):(-18,-11,18,-9,18,9,-18,11,-18,-11):(-18,-11,18,-9,18,9,-18,11,-18,-11):(-20,-13,20,-9,20,9,-20,13,-20,-13):(-19,-13,19,-11,19,11,-19,13,-19,-13):(-20,-13,20,-9,20,9,-20,13,-20,-13):(-18,-11,18,-9,18,9,-18,11,-18,-11)"));
   //cartShapePolyList = convertList(mSettings.getString("cartShapes", "(-16,-10,16,-8,16,8,-16,10,-16,-10):(-18,-12,18,-8,18,8,-18,12,-18,-12):(-17,-12,17,-10,17,10,-17,12,-17,-12):(-18,-12,18,-8,18,8,-18,12,-18,-12):(-16,-10,16,-8,16,8,-16,10,-16,-10):(-16,-10,16,-8,16,8,-16,10,-16,-10):(-18,-11,18,-9,18,9,-18,11,-18,-11):(-17,-11,17,-9,17,9,-17,11,-17,-11):(-18,-11,18,-9,18,9,-18,11,-18,-11):(-16,-10,16,-8,16,8,-16,10,-16,-10):(-16,-10,16,-8,16,8,-16,10,-16,-10):(-18,-12,18,-8,18,8,-18,12,-18,-12):(-17,-12,17,-10,17,10,-17,12,-17,-12):(-18,-12,18,-8,18,8,-18,12,-18,-12):(-16,-10,16,-8,16,8,-16,10,-16,-10)"));
    //cartShapePolyList = convertList(mSettings.getString("cartShapes", "(-15,-10,15,-8,15,8,-15,10,-15,-10):(-17,-12,17,-8,17,8,-17,12,-17,-12):(-16,-12,16,-10,16,10,-16,12,-16,-12):(-17,-12,17,-8,17,8,-17,12,-17,-12):(-15,-10,15,-8,15,8,-15,10,-15,-10):(-15,-10,15,-8,15,8,-15,10,-15,-10):(-17,-11,17,-9,17,9,-17,11,-17,-11):(-16,-11,16,-9,16,9,-16,11,-16,-11):(-17,-11,17,-9,17,9,-17,11,-17,-11):(-15,-10,15,-8,15,8,-15,10,-15,-10):(-15,-10,15,-8,15,8,-15,10,-15,-10):(-17,-12,17,-8,17,8,-17,12,-17,-12):(-16,-12,16,-10,16,10,-16,12,-16,-12):(-17,-12,17,-8,17,8,-17,12,-17,-12):(-15,-10,15,-8,15,8,-15,10,-15,-10)"));
    //Initialize the regions for different cart shapes from the vision settings
    std::list<SimplePolygon> cartRegionList;
    cartRegionList = convertList(mSettings.getString("cartRegions", "(0,0,30,0,30,50,0,50,0,0):(30,0,65,0,65,50,30,50,30,0):(65,0,95,0,95,50,65,50,65,0):(95,0,130,0,130,50,95,50,95,0):(130,0,160,0,160,50,130,50,130,0):(0,50,30,50,30,70,0,70,0,50):(30,50,65,50,65,70,30,70,30,50):(65,50,95,50,95,70,65,70,65,50):(95,50,130,50,130,70,95,70,95,50):(130,50,160,50,160,70,130,70,130,50):(0,70,30,70,30,120,0,120,0,70):(30,70,65,70,65,120,30,120,30,70):(65,70,95,70,95,120,65,120,65,70):(95,70,130,70,130,120,95,120,95,70):(130,70,160,70,160,120,130,120,130,70)"));
    
    //Initialize cart orientations 
    std::list<double> cartOrientationList;
    //cartOrientationList = mSettings.getDoubleList("cartOrientations", "0,1.57,3.14,4.71", true);
    cartOrientationList = mSettings.getDoubleList("cartOrientations", "0,3.14", true);
    
    double cartDetectorThres, cartDetectorOuterThres, cartDetectorInnerThres, cartDetectorCornerThres, cartDetectorInMargin, cartDetectorOutMargin;
    
    cartDetectorThres = mSettings.getDouble("cartDetectorhres",0.9,true);
    cartDetectorOuterThres = mSettings.getDouble("cartDetectorOuterThres",0.9,true);
    cartDetectorInnerThres = mSettings.getDouble("cartDetectorInnerThres",0.9,true);
    cartDetectorCornerThres = mSettings.getDouble("cartDetectorCornerThres",0.9,true);
    cartDetectorInMargin = mSettings.getDouble("cartDetectorInMargin",3,true);
    cartDetectorOutMargin = mSettings.getDouble("cartDetectorOutMargin",3,true);

    mCartDetector.initializeSettings(cartShapePolyList, cartRegionList, cartOrientationList, cartDetectorThres, cartDetectorInnerThres, 
                                     cartDetectorOuterThres, cartDetectorCornerThres, cartDetectorInMargin, cartDetectorOutMargin, mpForegroundSegmentMod->getRoiScaleFactor());

    mCartDetector.buildCartModel();
  }

   mCartScoreImage.resize(320,240);

  /*for (iPoly = cartShapePolyList.begin(); iPoly != cartShapePolyList.end(); iPoly++)
  {
    SimplePolygon cartShapePoly;
    cartShapePoly = *iPoly;

    Polygon p;

    //Remove last redundant entry, polygon class does not use it
    cartShapePoly.pop_back();
    p.setVertices(cartShapePoly);  

    mCartDetector.mCartShapes.push_back(p);
  }*/
  


  
  /*for (std::list<double>::const_iterator iOrien = cartOrientationList.begin(); iOrien != cartOrientationList.end(); iOrien++)
  {
    double orien;
    orien = *iOrien;
	mCartDetector.mOrientations.push_back(orien);
  }*/ 

  //Initialize Edge Cart Detector thresholds



}

/*
void getForegroundEdges(Image8& backgroundEdgeImg, Image8& foregroundEdgeImg, Image8& foregroundSegmentationImg)
{
for(int i=0;i<backgroundEdgeImg.size();i++)
{
if((foregroundEdgeImg(i)>0 && backgroundEdgeImg(i)==0) || (foregroundEdgeImg(i)>0 && foregroundSegmentationImg(i)>0))
foregroundEdgeImg(i) = 255;
}
}
*/

void 
  PersonShapeTrackerMod::makeAdjustedEdgeImage(const Image8& edgeBackgroundImg, 
  const Image8& edgeCurrentFrame,
  const Image8& foregroundSegImg,
  Image8& outputImg) const
{

  assert(edgeBackgroundImg.width() == edgeCurrentFrame.width());
  assert(edgeBackgroundImg.height() == edgeCurrentFrame.height());
  assert(edgeBackgroundImg.width() == foregroundSegImg.width());
  assert(edgeBackgroundImg.height() == foregroundSegImg.height());

  outputImg.resize(edgeBackgroundImg.width(), edgeBackgroundImg.height());

  int yMax = edgeBackgroundImg.height()-1;
  int xMax = edgeBackgroundImg.width()-1;
  for(int y = 1; y < yMax ; y++)
  {
	  for(int x = 1; x < xMax; x++)
	  {
		  outputImg(x,y) =0;
      
      if(edgeCurrentFrame(x,y) == 255) //Process further only if current pixel is edge pixel
      {
        
        if(edgeBackgroundImg(x,y) == 0) //Determines if it is a foreground edge
          outputImg(x,y) = 255;
        else
        {
          for(int dy = -1; dy <= 1; dy ++) //Determines if it is background edge but part of foreground segmentation
          {
            for(int dx = -1; dx <= 1; dx ++)
            {
              if(foregroundSegImg(x+dx, y+dy) == 255)
                  outputImg(x,y) = 255;
            }
          }

        }
      
      }

		  
	  }
  }

}

void 
  PersonShapeTrackerMod::makeAdjustedForegroundSegmentationEdgeImage(const Image8& dilatedForegroundSegmentationEdgeImg, 
  const Image8& adjustedEdgeImg,
  Image8& outputImg) const
{

  assert(dilatedForegroundSegmentationEdgeImg.width() == adjustedEdgeImg.width());
  assert(dilatedForegroundSegmentationEdgeImg.height() == adjustedEdgeImg.height());

  outputImg.resize(adjustedEdgeImg.width(), adjustedEdgeImg.height());

  for(int y = 0; y < adjustedEdgeImg.height(); y++)
  {
	  for(int x = 0; x < adjustedEdgeImg.width(); x++)
	  {
		  if(adjustedEdgeImg(x,y) == 255 || dilatedForegroundSegmentationEdgeImg(x,y)== 255)
			  outputImg(x,y) = 255;
	  }

  }
}

void
  PersonShapeTrackerMod::convertToImageCoord(TrajectoryPtr pTraj)
{
  // Rescale the points in the trajectory so they match
  // the original image.
  const Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();

  const Rectanglei roi = mpForegroundSegmentMod->getRoiRectangle();

  const float scaleX = 1.0f / scaleFactor.x;
  const float scaleY = 1.0f / scaleFactor.y;

  Trajectory::Nodes::iterator iNode;
  Trajectory::Nodes& nodes = pTraj->getNodes();
  for (iNode = nodes.begin(); iNode != nodes.end(); ++iNode)
  {
    Trajectory::Node& node = **iNode;
    node.boundingBox.set(
      roi.x0 + node.boundingBox.x0*scaleX,
      roi.y0 + node.boundingBox.y0*scaleY,
      roi.x0 + node.boundingBox.x1*scaleX,
      roi.y0 + node.boundingBox.y1*scaleY);
    node.location.set(
      (node.boundingBox.x0+node.boundingBox.x1)/2,
      mIsTopViewCamera ? (node.boundingBox.y0+node.boundingBox.y1)/2 : node.boundingBox.y1);
  }     
}

void 
  PersonShapeTrackerMod::killTracksInPoly()
{ 
  const Image8& img = mKillTrackMask;

  TracksShapePS::iterator iTrack;  

  for (iTrack = mTracks.begin(); iTrack != mTracks.end(); )
  {
    //Don't remove tracks with less than 2 nodes in their trajectories
    if ((*iTrack)->getTrajectory()->nodeCount() < 2)
    {
      iTrack++;
      continue;
    } 

    Vector2f pos = (*iTrack)->getPos();

    //if (img((int)pos.x, (int)pos.y) == 255) //kill tracks inside kill polygons

    if (img((int)pos.x, (int)pos.y) == 0) //kill tracks outside of polygons
    {
      (*iTrack)->kill();

      //convert trajectory to image coordinates
      TrajectoryPtr pTraj = (*iTrack)->getTrajectory();
      convertToImageCoord(pTraj);

      //TODO: make sure that this is working
      mPreTrajectories.push_back(pTraj);

      /*
      if (isExitTrajectory(pTraj))
      {
      //generate an exit event
      generateExitEvent(pTraj);
      }
      else
      {
      generateUnknownEvent(pTraj);
      }
      */

      //erase the track, it is no longer needed
      iTrack = mTracks.erase(iTrack);

    }
    else
    {
      iTrack++;
    }
  }
}

void
  PersonShapeTrackerMod::checkOverlappingTracks()
{  

  //TODO: this is a very trivial way of killing tracks, improve upon this
  Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();
  Vector2i offset; 
  Rectanglei roiRect = mpForegroundSegmentMod->getRoiRectangle();
  offset.x = std::min(roiRect.x0,roiRect.x1) * scaleFactor.x;
  offset.y = std::min(roiRect.y0,roiRect.y1) * scaleFactor.y;  

  TracksShapePS::iterator iTrack;
  for (iTrack = mTracks.begin(); iTrack != mTracks.end(); iTrack++)
  {

    TracksShapePS::iterator iOtherTrack;

    for (iOtherTrack = mTracks.begin(); iOtherTrack != mTracks.end(); iOtherTrack++)
    {
      if (iOtherTrack == iTrack)
      {
        continue;
      }


      Vector2f pos1 = (*iTrack)->getPos();
      Vector2f pos2 = (*iOtherTrack)->getPos();

      float dist = sqrt(dist2(pos1,pos2));

      Rectanglei rect1 = mpPersonShapeEstimator->getMaskRect(pos1.x + offset.x,pos1.y + offset.y);
      Rectanglei rect2 = mpPersonShapeEstimator->getMaskRect(pos2.x + offset.x,pos2.y + offset.y);

      int small1 = std::min(rect1.width(),rect1.height());
      int small2 = std::min(rect2.width(),rect2.height());

      const float overlapFactor = 0.5;
      float avgSmall = std::max((small1+small2)/2.0 * overlapFactor,1.0);

      if (dist < avgSmall) //kill younger track
      {
        //PVMSG("KILLING OVERLAPPING TRACK\n");
        if ((*iTrack)->getAge() < (*iOtherTrack)->getAge())
        {
          (*iTrack)->kill();
        }
        else
        {
          (*iOtherTrack)->kill();
        }
      }
    }
  }
}

void 
  PersonShapeTrackerMod::calculateTrackForce()
{

  TracksShapePS::iterator iTrack;

  for (iTrack = mTracks.begin(); iTrack != mTracks.end(); iTrack++)
  {   

    Vector2f vec = calculateTrackRepulsion(*iTrack);   

    //Vector2f vec;    

    //vec.x += -0.0;
    //vec.y += 0.0;

    /*
    float val = 1.0f;

    vec.x = std::min(vec.x, val);
    vec.x = std::max(vec.x, -val);

    vec.y = std::min(vec.y, val);
    vec.y = std::max(vec.y, -val);   
    */      

    //(*iTrack)->setForceVector(Vector2f(0.0,0.0));

    (*iTrack)->setForceVector(vec);    


    //TODO: HACK, MOVE TO OWN FUNCTION

    Vector2f dir = (*iTrack)->getTrajectory()->forwardVector(2.0);

    //PERSONSHAPE BEGIN
    Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();
    Vector2i offset; 
    Rectanglei roiRect = mpForegroundSegmentMod->getRoiRectangle();
    offset.x = std::min(roiRect.x0,roiRect.x1) * scaleFactor.x;
    offset.y = std::min(roiRect.y0,roiRect.y1) * scaleFactor.y;  
    //PERSONSHAPE END

    Vector2f p1 = (*iTrack)->getTrajectory()->getEndPos();

    Rectanglei shapeRect = mpPersonShapeEstimator->getMaskRect(p1.x+offset.x,p1.y+offset.y);

    float w = (float)shapeRect.width();
    float h = (float)shapeRect.height();    

    //dir.div(3.0);

    if (dir.x != 0.0 || dir.y != 0.0)
    {
      dir.x /= w;
      dir.y /= h;

      //dir.x = std::min(dir.x, w);
      //dir.y = std::min(dir.y, h);

      float cap = 0.75f;

      dir.x = std::min(dir.x, cap);
      dir.y = std::min(dir.y, cap);
      dir.x = std::max(dir.x, -cap);
      dir.y = std::max(dir.y, -cap);


    }

    //NOTE: ENABLE THIS
    (*iTrack)->setDirectionVector(dir);        
  }
}

Vector2f 
  PersonShapeTrackerMod::pointRepulsion(const Vector2f& p1, const Vector2f& p2)
{
  //PERSONSHAPE BEGIN
  Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();
  Vector2i offset; 
  Rectanglei roiRect = mpForegroundSegmentMod->getRoiRectangle();
  offset.x = std::min(roiRect.x0,roiRect.x1) * scaleFactor.x;
  offset.y = std::min(roiRect.y0,roiRect.y1) * scaleFactor.y;  
  //PERSONSHAPE END

  float w = 0.0;
  float h = 0.0;
  //mpForegroundSegmentMod->getBlobSizeEstimate()->getEstimatedSize(p1.x,p1.y,w,h);

  Rectanglei shapeRect = mpPersonShapeEstimator->getMaskRect(p1.x+offset.x,p1.y+offset.y);

  w = (float)shapeRect.width();
  h = (float)shapeRect.height();

  //float d = sqrt(dist2(p1,p2));

  Vector2f vec;

  vec.x = p1.x - p2.x;
  vec.y = p1.y - p2.y;

  //divide by blob width,height
  vec.x /= w;
  vec.y /= h;  

  //Special case, return 0.0 and let caller deal with it
  if (vec.x == 0.0 && vec.y == 0.0)
  {
    return vec;
  }

  float d = sqrt(vec.x*vec.x + vec.y*vec.y);  

  double maxDist = 1.0;

  //return no force if points are > then specified distance apart
  if (d >= maxDist)
  {
    return Vector2f(0.0,0.0);
  }

  PVASSERT(d != 0.0);

  vec.normalize();

  //scale force vector based on distance
  //vec.mult(0.25/d);  //nonlinear

  //linear
  float scale = 1.0 - d/maxDist;
  scale = std::min(scale,1.0f);
  scale = std::max(scale,0.0f);

  vec.mult(scale);

  return vec;
}


Vector2f 
  PersonShapeTrackerMod::calculateTrackRepulsion(MultiFrameTrackShapePSPtr pTrack)
{
  Vector2f vec(0.0,0.0);

  Vector2f pos = pTrack->getPos();


  TracksShapePS::iterator iTrack;

  for (iTrack = mTracks.begin(); iTrack != mTracks.end(); iTrack++)
  {
    if (*iTrack != pTrack)
    {
      Vector2f f = pointRepulsion(pos, (*iTrack)->getPos());     

      vec.x += f.x;
      vec.y += f.y;
    }
  }  
  return vec;
}

float 
  PersonShapeTrackerMod::euclideanDist(Vector2f& temp1, Vector2f& temp2)
{
  return sqrt(fabs(temp1.x-temp2.x)*fabs(temp1.x-temp2.x)+fabs(temp1.y-temp2.y)*fabs(temp1.y-temp2.y));
}

bool
  PersonShapeTrackerMod::checkOverlappingPersonShapes(Vector2f& pos1, Vector2f pos2)
{  

  //Using outline points
  Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();
  Rectanglei roiRect = mpForegroundSegmentMod->getRoiRectangle();
  Vector2i offset;
  offset.x = std::min(roiRect.x0,roiRect.x1) * scaleFactor.x;
  offset.y = std::min(roiRect.y0,roiRect.y1) * scaleFactor.y; 

  //123456.0 is used in the world coordinate space saved by MaskMaker for those points having intersect at infinity
  if((realWorldCoords(pos1.x,pos1.y).x==123456.0 && realWorldCoords(pos1.x,pos1.y).y==123456.0) || (realWorldCoords(pos2.x,pos2.y).x==123456.0 && realWorldCoords(pos2.x,pos2.y).y==123456.0) || (realWorldCoords(pos1.x,pos1.y).x-realWorldCoords(pos2.x,pos2.y).x)*(realWorldCoords(pos1.x,pos1.y).x-realWorldCoords(pos2.x,pos2.y).x)+(realWorldCoords(pos1.x,pos1.y).y-realWorldCoords(pos2.x,pos2.y).y)*(realWorldCoords(pos1.x,pos1.y).y-realWorldCoords(pos2.x,pos2.y).y)<mPersonShapeDistanceThresh)
    return true;
  else
    return false;
}

float
  PersonShapeTrackerMod::getRealWorldDistance(Vector2i& pos1, Vector2i& pos2)
{
  if(realWorldCoords(pos1.x,pos1.y).x==123456.0 || realWorldCoords(pos1.x,pos1.y).y==123456.0 || realWorldCoords(pos2.x,pos2.y).x==123456.0 || realWorldCoords(pos2.x,pos2.y).y==123456.0)
    return 10000000.0f;
  else
    return (realWorldCoords(pos1.x,pos1.y).x-realWorldCoords(pos2.x,pos2.y).x)*(realWorldCoords(pos1.x,pos1.y).x-realWorldCoords(pos2.x,pos2.y).x)+(realWorldCoords(pos1.x,pos1.y).y-realWorldCoords(pos2.x,pos2.y).y)*(realWorldCoords(pos1.x,pos1.y).y-realWorldCoords(pos2.x,pos2.y).y);
}

void 
  PersonShapeTrackerMod::scaleImage(Image8& inputImage, float max, float min)
{
  for(int x=0;x<inputImage.width();x++)
    for(int y=0;y<inputImage.height();y++)
    {
      inputImage(x,y)=inputImage(x,y)*(min+(max-min)*y/(inputImage.height()-1));
    }
}

void 
PersonShapeTrackerMod::filterSegmentation(Image8& image, int order)
{
  Image8 transImage;
  transImage.resize(image.width(),image.height());
  transImage.setAll(0);
  for(int x=1;x<image.width()-1;x++)
    for(int y=1;y<image.height()-1;y++)
    {
      if(image(x,y)>0)
      {
        int sum=0;
        if(image(x-1,y-1)>0)
          sum++;
        if(image(x,y-1)>0)
          sum++;
        if(image(x+1,y-1)>0)
          sum++;
        if(image(x-1,y)>0)
          sum++;
        if(image(x+1,y)>0)
          sum++;
        if(image(x+1,y+1)>0)
          sum++;
        if(image(x,y+1)>0)
          sum++;
        if(image(x-1,y+1)>0)
          sum++;
        if(sum>order)
          transImage(x,y)=255;
        else
          transImage(x,y)=0;
      }
    }
    //transImage.setAll(255);
    image=transImage;
}

void 
PersonShapeTrackerMod::filterSegmentationWholeImg(Image8& image, int order)
{
  Image8 transImage;
  transImage.resize(image.width(),image.height());
  transImage.setAll(0);
  for(int x=1;x<image.width()-1;x++)
    for(int y=1;y<image.height()-1;y++)
    {
     // if(image(x,y)>0)
      {
        int sum=0;
        if(image(x-1,y-1)>0)
          sum++;
        if(image(x,y-1)>0)
          sum++;
        if(image(x+1,y-1)>0)
          sum++;
        if(image(x-1,y)>0)
          sum++;
        if(image(x+1,y)>0)
          sum++;
        if(image(x+1,y+1)>0)
          sum++;
        if(image(x,y+1)>0)
          sum++;
        if(image(x-1,y+1)>0)
          sum++;
        if(sum>order)
          transImage(x,y)=255;
        else
          transImage(x,y)=0;
      }
    }
    //transImage.setAll(255);
    image=transImage;
}


void 
  PersonShapeTrackerMod::measureRawSegmentationHack()
{
  std::string fileName=mMeasureRawSegFilePath;
  DateTime tmp1(getCurrentTime());
  std::string day,month;
  month=tmp1.format("%b");
  day=tmp1.format("%d");
  fileName+=month;
  fileName+=day;
  fileName+=".csv";
  double tempTime=getCurrentTime();
  if(tempTime>mCurrentFrameTime+float(1.0f/float(mMeasureRawSegRate)))
  {
    std::ofstream tempFile(fileName.c_str(),std::ios_base::app|std::ios_base::out);

    Image8 foreground = *mpForegroundSegmentMod->getForeground();
    std::vector<int> count;
    for(int s=0;s<mMeasureSegMask.size();s++)
      count.push_back(1);

    Image8 tmp(foreground.width(),foreground.height());
    for(int r=0;r<mMeasureSegMask.size();r++)
    {
      tmp.setAll(0);
      for(int s=0;s<foreground.size();s++)
        if(mMeasureSegMask[r](s)!=0 && foreground(s)!=0)
          tmp(s)=1;
      count[r]=int(tmp.mean()*tmp.size());
    }

    DateTime tmp1(tempTime);
    tempFile<<tmp1.toString()<<" ";
    for(int r=0;r<mMeasureSegMask.size();r++)
    {
      tempFile<<100*float(count[r])/float(mSegmentationScaling[r])<<" ";
    }
    tempFile<<std::endl;
    tempFile.close();
    mCurrentFrameTime=tempTime;
  }
}

void 
  PersonShapeTrackerMod::makeSpiralLookup(unsigned int width, unsigned int height, Image<unsigned int>& table) const
{

  Vector2i curPos;
  Vector2i center;
  Vector2i curDir(1,0);
  unsigned int moves = 1;
  unsigned int tablePos = 0;

  table.resize(width,height);
  table.setAll(0);

  //Find the center block
  if (width % 2 == 0)
    center.set(width/2 - 1, height/2 - 1);
  else
    center.set(width/2,height/2);

  //calculate the maximum distance to each corner 
  //(to be used later when translating center)

  float maxDist = sqrt((float)dist2(center, Vector2i(0,0)));
  maxDist = std::max(maxDist, sqrt((float)dist2(center, Vector2i(width-1,0))));
  maxDist = std::max(maxDist, sqrt((float)dist2(center, Vector2i(0,height-1))));
  maxDist = std::max(maxDist, sqrt((float)dist2(center, Vector2i(width-1,height-1))));

  //Set the current position to be the center
  curPos = center;

  //First entry in table
  table(tablePos) = curPos.y*width + curPos.x;
  tablePos++;

  while(true)
  {
    for (int i = 0; i < moves; i++)
    {
      curPos.x += curDir.x;
      curPos.y += curDir.y;

      if (curPos.x >= 0 && curPos.x < width && curPos.y >= 0 && curPos.y < height)
      {
        table(tablePos) = curPos.y*width + curPos.x;
        tablePos++;
      }
    }

    //check distance, if spiral completely covers array, break out of loop
    float d = sqrt((float)dist2(center, curPos));
    if (d > maxDist + 1.0)
      break;

    //change direction of movement and take action as appropriate
    if (curDir == Vector2i(1,0))
    {
      curDir = Vector2i(0,1);
    }
    else if (curDir == Vector2i(0,1))
    {
      curDir = Vector2i(-1,0);
      moves++;
    }
    else if (curDir == Vector2i(-1,0))
    {
      curDir = Vector2i(0,-1);
    }
    else if (curDir == Vector2i(0,-1))
    {
      curDir = Vector2i(1,0);
      moves++;
    }
    else
    {
      assert(false); //should never reach here
    }
  }
}

//Function returns true if given coordinate has highest pixel value in eight neighbor else false
bool 
  PersonShapeTrackerMod::isMaxEightNeighbor(int x, int y, Image8& img) const
{
  //Check the point location in image
  if(x>0 && x<(img.width()-1) && y>0 && y<(img.height()-1))
  {
    if(img(x,y)>=img(x-1,y-1) && img(x,y)>=img(x-1,y) 
      && img(x,y)>=img(x-1,y+1) && img(x,y)>=img(x,y+1)
      && img(x,y)>=img(x+1,y+1) && img(x,y)>=img(x+1,y) 
      && img(x,y)>=img(x+1,y-1) && img(x,y)>=img(x,y-1))
      return true;
    else
      return false;
  }
  else
  {
    return false;
  }
}

bool
PersonShapeTrackerMod::isNearCart(int x, int y, Image8& img)
{
  if(!mDetectCarts)
    return false;
  int leftMin = x-1>=0? x-1 : 0;
  int rightMax = x+1<img.width()?x+1:img.width()-1;
  int topMin = y-1>=0? y-1 : 0;
  int bottomMax = y+1 < img.height()?y+1:img.height()-1;

  for (int a=leftMin;a<rightMax+1;a++)
    for(int b=topMin;b<bottomMax+1;b++)
      {
        if(img(a,b)>0)
          return true;
    }

    return false;
}

void
PersonShapeTrackerMod::removeBelowCartRegions(Image8& img)
{
  if(mDetectCarts)
  {
  for(int x=0;x<img.width();x++)
    for(int y=0;y<img.height();y++)
    {
      if(isBelowCart(x,y))
        img(x,y)=0;
    }
  }
}


void 
  PersonShapeTrackerMod::process()
{  
  mBenchmark.beginSample();

  // Send progress to vms app.
  ImageAcquireModPtr pIAM = getImageAcquireModule();
  if (!pIAM->isLiveVideoSource())
  {
    mpVmsPacket->updateProgress((double)pIAM->getCurrentFrameNumber()/pIAM->getTotalNumberOfFrames());
  }

  if(mMeasureRawSegmentation) //MEASURE SEGMENTATION HACK
  {
    measureRawSegmentationHack();
  }
  else //NORMAL PROCESSING
  {   

    mFrameNum++;
    if(mDebug)
    {
      mDebugFile<<"Frame number "<<mFrameNum<<std::endl;
      mDebugFile<<"\nTrack positions:\n";
      for(TracksShapePS::iterator iTrack=mTracks.begin();iTrack!=mTracks.end();iTrack++)
        mDebugFile<<(*iTrack)->getPos().x<<" "<<(*iTrack)->getPos().y<<std::endl;
    }

    // BEGIN NEW CODE SECTION
    Image32Ptr pRgbFrame = getImageAcquireModule()->getCurrentFramePtr();
    assert(pRgbFrame.get());



	//Perform edge detection on original image
    Image8Ptr pGrayFrame = mpForegroundSegmentMod->getRoiFrame(pRgbFrame);
    assert(pGrayFrame.get());
    
	//First pass, create the spiral lookup table
    if (mSpiralScanLookup.size() == 1)
    {
      makeSpiralLookup(pGrayFrame->width(),pGrayFrame->height(), mSpiralScanLookup);
    } 
	detectEdges(*pGrayFrame, *mpEdgeImage, mEdgeDetectThreshold);   

    //Send the edge image to our edge background learner
    assert(mpEdgeBackgroundLearner->isInitialized());
    mpEdgeBackgroundLearner->process(mpEdgeImage, true);   

    if (mpEdgeBackgroundLearner->getValidModel().get())
    {
      if (mpEdgeBackgroundLearner->isModelUpdated())
      {
        PVMSG("Edge model updated...\n");
        //Must dynamic cast to directly access the model
        HistogramBackgroundModelPtr pModel = boost::dynamic_pointer_cast<HistogramBackgroundModel>(mpEdgeBackgroundLearner->getValidModel());
        pModel->drawMaxThresh(*mpEdgeLearnedImage);      
      }

    }
    else
    {
      //mpEdgeLearnedImage->resize(pGrayFrame->width(), pGrayFrame->height());
      //mpEdgeLearnedImage->setAll(0);

      *mpEdgeLearnedImage = *mpEdgeImage;
    }

    //Copy FSM image here for potential processing
    *mpRawSegmentationImage = *mpForegroundSegmentMod->getForeground(); 

    //Clean out the segmentation lying outside the personTrackRegion
    for(int a=0;a<mPersonTrackMask.size();a++)
      if(mPersonTrackMask(a)==0)
      {
        (*mpRawSegmentationImage)(a)=0;
        (*mpEdgeLearnedImage)(a)=0;
        (*mpEdgeImage)(a) = 0;
      }
     
      
      
    
      
      
      //Create the AdjustedEdgeImage for use in Edge Cart Detection and in Person Detection
	makeAdjustedEdgeImage(*mpEdgeLearnedImage, *mpEdgeImage, *mpRawSegmentationImage, *mpAdjustedEdgeImage);
	
  
  //For Test Purposes
	/*mTestAdjustedEdgeImage.resize(mpAdjustedEdgeImage->width(),mpAdjustedEdgeImage->height());
  mTestEdgeImage.resize(mpEdgeImage->width(),mpEdgeImage->height());
 
  for (int imageLoopX = 0; imageLoopX < mTestAdjustedEdgeImage.width(); imageLoopX++)
  {
    for (int imageLoopY = 0; imageLoopY < mTestAdjustedEdgeImage.height(); imageLoopY++)
    {
      mTestAdjustedEdgeImage(imageLoopX,imageLoopY) = (*mpAdjustedEdgeImage)(imageLoopX,imageLoopY);
    }
  }
  //For Test Purposes
  for (int imageLoopX = 0; imageLoopX < mTestEdgeImage.width(); imageLoopX++)
  {
    for (int imageLoopY = 0; imageLoopY < mTestEdgeImage.height(); imageLoopY++)
    {
      mTestEdgeImage(imageLoopX,imageLoopY) = (*mpEdgeImage)(imageLoopX,imageLoopY);
    }
  }*/
  
  //Cart initializations
    Image8 cartImage(mpRawSegmentationImage->width(),mpRawSegmentationImage->height());
    
    
	//Color Based Cart detection and clean up
    
	if(mDetectCarts)
    {
		//Remove carts from the raw segmentation and the edge image
		mCartPolygons.clear();

		//Old cart detector -- need to revive some functions
		
		detectCarts();
   

		//Code for tracking carts
		mvCartPositions.clear();
    
		getCartPositions();

		if(0)
		{
			//Cart assignment
			trackCarts();
    
			//Update cart tracks
			updateCartTracks();

			//Kill cart tracks outside polygon (killTrackRegion)
			killCartTracksOutsidePoly();

			//Create new tracks for carts
			createTracksForCarts();

			//Delete dead cart tracks
			deleteDeadCartTracks();

			//Get the occluding rectangles for all the carts
			mOccludingCartRectangles.clear();
			getOccludingCartRectanglesSloped();
			getBelowCartRegions();
		}

		// //Done code for tracking carts
    
    
		removeCarts(*mpRawSegmentationImage,cartImage);

		removeBelowCartRegions(*mpRawSegmentationImage);
    }
	
	//Foreground Segmentation Edge Based Cart Detection
	
  if(mDetectCartsUsingEdges)
  {
    *mpDilatedForegroundSegmentationImage = *mpForegroundSegmentMod->getForeground(); 

    filterSegmentationWholeImg(*mpDilatedForegroundSegmentationImage,2);

    detectEdges(*mpDilatedForegroundSegmentationImage, *mpDilatedForegroundSegmentationEdgeImage, mEdgeDetectThreshold);

    makeAdjustedForegroundSegmentationEdgeImage(*mpDilatedForegroundSegmentationEdgeImage,*mpAdjustedEdgeImage,*mpDilatedForegroundSegmentationEdgeImage);

    mCartPolygons.clear();

    mCartDetector.constructForegroundList(*mpDilatedForegroundSegmentationImage, 2);

    mCartDetector.detectCarts(mCartPolygons, *mpDilatedForegroundSegmentationEdgeImage, mCartScoreImage, 2);

    mCartDetector.removeCarts(*mpRawSegmentationImage,cartImage,mCartPolygons);
  }


//removeCarts(*mpRawSegmentationImage,cartImage);


    

    //Done cleaning out the segmentation

    //DON'T REMOVE CARTS FROM THE EDGE IMAGE
    //removeCarts(*mpEdgeImage);

    




    //PVMSG("Debug %d %d %d",RED(mSelectedColor),GREEN(mSelectedColor),BLUE(mSelectedColor));
    //First, filter the image based on the person size
    Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();   
    Vector2i offset;
    Rectanglei roiRect = mpForegroundSegmentMod->getRoiRectangle();
    offset.x = std::min(roiRect.x0,roiRect.x1) * scaleFactor.x;
    offset.y = std::min(roiRect.y0,roiRect.y1) * scaleFactor.y;    

    //END NEW CODE SECTION         

    //Filter the raw segmentation w/ carts removed
    Image8 rawForeground = *mpRawSegmentationImage;
    if(mFilterSegmentationFlag)
      filterSegmentation(rawForeground,mFilterSegmentationOrder);

    

    mpPersonShapeEstimator->filterImage(*mpFilteredForeground, rawForeground, offset);

    //if (mUseEdgePersonDetection)
    //{
      //filter the edge image
      mpPersonShapeEstimator->filterImageOutline(*mpFilteredEdgeImage, *mpAdjustedEdgeImage, rawForeground, offset);
      //Combine the edge and segmentation images
      combineEdgesAndSeg(*mpFilteredEdgeImage, *mpFilteredForeground, *mpCombinedEdgeAndSegImage, mEdgeWeight);
   // }
    //else
      //mpCombinedEdgeAndSegImage = mpFilteredForeground;
    

    //New code added by Rahul
    std::vector<int> personProbDensity,personProbDensityLt;


    //PERSON DETECTION AND TRACKING STARTS HERE

    //Clear all the person positions
    mvPersonPositions.clear();
    mvPersonPositionsLowThresh.clear();

    Image8 personDetectSegImage(roiRect.width()*scaleFactor.x,roiRect.height()*scaleFactor.y),filterImg(roiRect.width()*scaleFactor.x,roiRect.height()*scaleFactor.y);
    Image8 personDetectEdgeImage(roiRect.width()*scaleFactor.x,roiRect.height()*scaleFactor.y);
    personDetectSegImage=rawForeground;
    personDetectEdgeImage=*mpAdjustedEdgeImage;
    filterImg=*mpCombinedEdgeAndSegImage;


    

    //*********Latest
    Image8 segFiltered;
    Image8 edgeFiltered;

    bool flagRectSafe = true;
    Rectanglei trackRect;

    std::list<int>::iterator threshIter;
    for(threshIter=mThresholdList.begin();threshIter!=mThresholdList.end();threshIter++)
    {
      for (int i = 0; i < mSpiralScanLookup.size(); i++)
      {
        const unsigned int a = i % personDetectSegImage.width();
        const unsigned int b = i / personDetectSegImage.width();        

        if(personDetectSegImage(a,b)>mRegSegPersonDetectionLowerThresh && 
          personDetectEdgeImage(a,b)>mEdgeSegPersonDetectionLowerThresh && 
          (isBelowCart(a,b) ? filterImg(a,b)>((*threshIter)+20) : filterImg(a,b)>(*threshIter)) && 
          mPersonTrackMask(a,b)!=0 && 
          isMaxEightNeighbor(a,b,filterImg) && !isNearCart(a,b,cartImage))
        {
          flagRectSafe = true;
          for(int s=0;s<mvPersonPositions.size();s++)
          {
            //trackRect = personRects[s];
            //if this point is inside the bounding box, erase it from the new track list
            if (checkOverlappingPersonShapes(Vector2f(a,b),mvPersonPositions[s].personPosition))
            {
              flagRectSafe = false;
              break;
            } 

          }
          if(flagRectSafe)
          {
            //New code added by Rahul
            personStructure temp;
            temp.isAssigned = false;
            temp.personPosition.set(a,b);
            temp.personProbability = filterImg(a,b);
            mvPersonPositions.push_back(temp);
            //Done

            //Now refilter the images
            mpPersonShapeEstimator->cleanForegroundSegmentation(a,b,personDetectSegImage,Vector2i(0,0));            
            mpPersonShapeEstimator->cleanForegroundSegmentation(a,b,personDetectEdgeImage,Vector2i(0,0));                        

            mpPersonShapeEstimator->filterImage(segFiltered,personDetectSegImage,Vector2i(0,0));                        
            mpPersonShapeEstimator->filterImageOutline(edgeFiltered,
              personDetectEdgeImage,
              personDetectSegImage,
              Vector2i(0,0));

            //combine filtered image into whole image again
            combineEdgesAndSeg(edgeFiltered, segFiltered, filterImg,mEdgeWeight);            
          }
        }        
      }      
    }
    //********Latest
    if (mRepelTracks)
    {    
      calculateTrackForce();
    }

    killTracksInPoly();


    //Now scan for persons at lower threshold

    //**********Latest
    mpPersonShapeEstimator->filterImage(segFiltered,personDetectSegImage,Vector2i(0,0));
    mpPersonShapeEstimator->filterImageOutline(edgeFiltered,personDetectEdgeImage,personDetectSegImage,Vector2i(0,0));

    //combine filtered image into whole image again
    combineEdgesAndSeg(edgeFiltered, segFiltered, filterImg, mEdgeWeight);

    for (int i = 0; i < mSpiralScanLookup.size(); i++)
    {
      const unsigned int a = i % personDetectSegImage.width();
      const unsigned int b = i / personDetectSegImage.width();

      if((isBelowCart(a,b) ? filterImg(a,b)>(mPersonLowerThreshold+20) : filterImg(a,b)>mPersonLowerThreshold) && mPersonTrackMask(a,b)!=0 && isMaxEightNeighbor(a,b,filterImg) && !isNearCart(a,b,cartImage))
      {
        flagRectSafe = true;
        for(int s=0;s<mvPersonPositionsLowThresh.size();s++)
        {
          //if this point is inside the bounding box, erase it from the new track list
          if (checkOverlappingPersonShapes(Vector2f(a,b),mvPersonPositionsLowThresh[s].personPosition))
          {
            flagRectSafe = false;
            break;
          } 
        }
        if(flagRectSafe==true)
          for(int s=0;s<mvPersonPositions.size();s++)
          {
            //if this point is inside the bounding box, erase it from the new track list
            if (checkOverlappingPersonShapes(Vector2f(a,b),mvPersonPositions[s].personPosition))
            {
              flagRectSafe = false;
              break;
            } 
          }
          if(flagRectSafe)
          {     
            //New code added by Rahul
            personStructure temp;
            temp.isAssigned = false;
            temp.personPosition.set(a,b);
            temp.personProbability = filterImg(a,b);
            mvPersonPositionsLowThresh.push_back(temp);
            //Done

            mpPersonShapeEstimator->cleanForegroundSegmentation(a,b,personDetectSegImage,Vector2i(0,0));
            mpPersonShapeEstimator->cleanForegroundSegmentation(a,b,personDetectEdgeImage,Vector2i(0,0));            

            mpPersonShapeEstimator->filterImage(segFiltered,personDetectSegImage,Vector2i(0,0));
            mpPersonShapeEstimator->filterImageOutline(edgeFiltered,
              personDetectEdgeImage,
              personDetectSegImage,
              Vector2i(0,0));

            //combine filtered image into whole image again
            combineEdgesAndSeg(edgeFiltered, segFiltered, filterImg,mEdgeWeight);
            //ltPersonPositions.push_back(Vector3f(float(a),float(b),-1.0f));
          }
      }        
    } 

    //For occluded persons
    if(mDetectCarts)
    {
    mvOccludedPersonPositions.clear();

    for (int i = 0; i < mSpiralScanLookup.size(); i++)
    {
      const unsigned int a = i % personDetectSegImage.width();
      const unsigned int b = i / personDetectSegImage.width();
      bool flagInsideOccludeRectangle = false;
      for(int r=0;r<mOccludingCartRectangles.size();r++)
      {
        const Vector3i temp(a,b,0);
        if(mOccludingCartRectangles[r].inside(temp))
        {
          flagInsideOccludeRectangle = true;
          break;
        }
      }
      if(filterImg(a,b)>(mOccludedPersonThresh) && mPersonTrackMask(a,b)!=0 && isMaxEightNeighbor(a,b,filterImg) && !isNearCart(a,b,cartImage) && flagInsideOccludeRectangle)
      {
        flagRectSafe = true;
        for(int s=0;s<mvOccludedPersonPositions.size();s++)
        {
          //if this point is inside the bounding box, erase it from the new track list
          if (checkOverlappingPersonShapes(Vector2f(a,b),mvOccludedPersonPositions[s].personPosition))
          {
            flagRectSafe = false;
            break;
          } 
        }
        for(int s=0;s<mvPersonPositionsLowThresh.size();s++)
        {
          //if this point is inside the bounding box, erase it from the new track list
          if (checkOverlappingPersonShapes(Vector2f(a,b),mvPersonPositionsLowThresh[s].personPosition))
          {
            flagRectSafe = false;
            break;
          } 
        }
        if(flagRectSafe==true)
          for(int s=0;s<mvPersonPositions.size();s++)
          {
            //if this point is inside the bounding box, erase it from the new track list
            if (checkOverlappingPersonShapes(Vector2f(a,b),mvPersonPositions[s].personPosition))
            {
              flagRectSafe = false;
              break;
            } 
          }
          if(flagRectSafe)
          {     
            //New code added by Rahul
            personStructure temp;
            temp.isAssigned = false;
            temp.personPosition.set(a,b);
            temp.personProbability = filterImg(a,b);
            mvOccludedPersonPositions.push_back(temp);
            //Done

            mpPersonShapeEstimator->cleanForegroundSegmentation(a,b,personDetectSegImage,Vector2i(0,0));
            mpPersonShapeEstimator->cleanForegroundSegmentation(a,b,personDetectEdgeImage,Vector2i(0,0));            

            mpPersonShapeEstimator->filterImage(segFiltered,personDetectSegImage,Vector2i(0,0));
            mpPersonShapeEstimator->filterImageOutline(edgeFiltered,
              personDetectEdgeImage,
              personDetectSegImage,
              Vector2i(0,0));

            //combine filtered image into whole image again
            combineEdgesAndSeg(edgeFiltered, segFiltered, filterImg,mEdgeWeight);
            //ltPersonPositions.push_back(Vector3f(float(a),float(b),-1.0f));
          }
      }        
    } 

    }
    

    ////combine filtered image into whole image again
    //combineEdgesAndSeg(edgeFiltered, segFiltered, filterImg, mEdgeWeight);




    if(mDebug)
    {
      mDebugFile<<"\nPerson positions (HPP):\n";
      for(int a=0;a<mvPersonPositions.size();a++)
        mDebugFile<<mvPersonPositions[a].personPosition.x<<" "<<mvPersonPositions[a].personPosition.y<<std::endl;
      mDebugFile<<"\nPerson positions (LPP):\n";
      for(int a=0;a<mvPersonPositionsLowThresh.size();a++)
        mDebugFile<<mvPersonPositionsLowThresh[a].personPosition.x<<" "<<mvPersonPositionsLowThresh[a].personPosition.y<<std::endl;

    }

    
    //*********Latest

    //Scanned at lower threshold
    personTrackAssignmentPredicted(filterImg);
    
    updateTracks();

    createNewTracks();

    deleteDeadTracks();
    
    postProcessTracks(false);    

  }

  mBenchmark.endSample();
}

bool
PersonShapeTrackerMod::isBelowCart(int x, int y)
{
  if(!mDetectCarts)
    return false;
  for(int i=0;i<mBelowCartRegions.size();i++)
  {
    if(mBelowCartRegions[i].inside(Vector3i(x,y,0)))
      return true;
  }
  return false;
}

bool
PersonShapeTrackerMod::isInOccludedRegion(int x, int y)
{
  if(!mDetectCarts)
    return false;
  for(int i=0;i<mOccludingCartRectangles.size();i++)
  {
    if(mOccludingCartRectangles[i].inside(Vector3i(x,y,0)))
      return true;
  }
  return false;
}

void
PersonShapeTrackerMod::getMaxValueOccludingRegion(Image8& img)
{
  maxOccludingValues.clear();
  for(int a=0;a<mOccludingCartRectangles.size();a++)
  {
    int maxVal=-1;
    Vector2i pt(0,0);
    for(int x=mOccludingCartRectangles[a].x0;x<mOccludingCartRectangles[a].x1;x++)
      for(int y=mOccludingCartRectangles[a].y0;y<mOccludingCartRectangles[a].y1;y++)
      {
        if(img(x,y)>maxVal)
        {
          pt.x = x;
          pt.y = y;
          maxVal = img(x,y);
        }
      }
    maxOccludingValues.push_back(maxVal);
  }
}

void fixRect(Rectanglef& inRect)
{
  if(inRect.x0>inRect.x1)
  {
    float temp = inRect.x0;
    inRect.x0 = inRect.x1;
    inRect.x1 = temp;
  }
  if(inRect.y0>inRect.y1)
  {
    float temp = inRect.y0;
    inRect.y0 = inRect.y1;
    inRect.y1 = temp;
  }
}

void
PersonShapeTrackerMod::getBelowCartRegions()
{
  mBelowCartRegions.clear();
  std::vector<int> occRegions;
  for(std::list<int>::iterator temp=mOcclusionRegions.begin();temp!=mOcclusionRegions.end();temp++)
  {
    occRegions.push_back(*temp);
  }
  for(TracksShapePS::iterator iT=mCartTracks.begin();iT!=mCartTracks.end();iT++)
  {
    Polygon cartPolygon;
    (*iT)->getCartPolygon(cartPolygon);
    Rectanglef rectBB=cartPolygon.getBoundingBox();
    fixRect(rectBB);

    float percentExtra = 0.0f;
    int pixelHeight = 5;


    //SM int x1 = (int)rectBB.x0,y1 = (int)rectBB.y0,x2 = (int)rectBB.x1,y2 = (int)rectBB.y0,x3 = (int)rectBB.x1,y3 = (int)rectBB.y1,x4 = (int)rectBB.x0,y4 = (int)rectBB.y1;

    int x1 = (int)rectBB.x0; int y1 = (int)rectBB.y0;int x2 = (int)rectBB.x1;int y2 = (int)rectBB.y0;int x3 = (int)rectBB.x1;int y3 = (int)rectBB.y1;int x4 = (int)rectBB.x0;int y4 = (int)rectBB.y1;

    int xCart = (*iT)->getPos().x;
    int yCart = (*iT)->getPos().y;

    int xCenter = mPersonTrackMask.width()/2;
    int yCenter = mPersonTrackMask.height()/2;

    //Get grid position of cart
    int xGridVal =0;
    int yGridVal =0;

    int x_scale = 320/mPersonTrackMask.width();
    int y_scale = 240/mPersonTrackMask.height();

    for(int a=0;a<occRegions.size()/2;a++)
      if(xCart>occRegions[a]/x_scale)
        xGridVal++;
    for(int a=occRegions.size()/2;a<occRegions.size();a++)
      if(yCart>occRegions[a]/y_scale)
        yGridVal++;

    if(xGridVal>=0 && xGridVal<=3 && yGridVal<2)
    {
      //Add the region at bottom of cart
      Rectanglei rect;
      int yBottom = y3 + pixelHeight < 2*yCenter ? y3 + pixelHeight : 2*yCenter-1;

      //Increase by 15% the width
      int xLeft = x1 - int(float(x2-x1)*percentExtra) >= 0 ? x1 - int(float(x2-x1)*percentExtra) : 0;
      int xRight = x2 + int(float(x2-x1)*percentExtra) < 2*xCenter ? x2 + int(float(x2-x1)*percentExtra) : 2*xCenter-1;

      rect.set(xLeft,y4,xRight,yBottom);
      mBelowCartRegions.push_back(rect); 
    }

    if(xGridVal>=0 && xGridVal<=3 && yGridVal>1)
    {
      //Add rectangle at top of cart
      Rectanglei rect;
      int yTop = y1 - pixelHeight >= 0 ? y1 - pixelHeight : 0;
      //Increase by 15% the width
      int xLeft = x1 - int(float(x2-x1)*percentExtra) >= 0 ? x1 - int(float(x2-x1)*percentExtra) : 0;
      int xRight = x2 + int(float(x2-x1)*percentExtra) < 2*xCenter ? x2 + int(float(x2-x1)*percentExtra) : 2*xCenter-1;

      rect.set(xLeft,yTop,xRight,y1);
      mBelowCartRegions.push_back(rect);
    }
  }

}

void
PersonShapeTrackerMod::getOccludingCartRectanglesSloped()
{
  mOccludingCartRectangles.clear();
  
  std::vector<int> occRegions;
  for(std::list<int>::iterator temp=mOcclusionRegions.begin();temp!=mOcclusionRegions.end();temp++)
  {
    occRegions.push_back(*temp);
  }
  for(TracksShapePS::iterator iT=mCartTracks.begin();iT!=mCartTracks.end();iT++)
  {
    //if((*iT)->mOcclusionStatus == true)
    { Polygon cartPolygon;
    (*iT)->getCartPolygon(cartPolygon);
    Rectanglef rectBB=cartPolygon.getBoundingBox();
    fixRect(rectBB);

    int occDepth = 10;
    float extraPercent=0.50f;
    
    //SM int x1 = (int)rectBB.x0,y1 = (int)rectBB.y0,x2 = (int)rectBB.x1,y2 = (int)rectBB.y0,x3 = (int)rectBB.x1,y3 = (int)rectBB.y1,x4 = (int)rectBB.x0,y4 = (int)rectBB.y1;

    int x1 = (int)rectBB.x0; int y1 = (int)rectBB.y0;int x2 = (int)rectBB.x1;int y2 = (int)rectBB.y0;int x3 = (int)rectBB.x1;int y3 = (int)rectBB.y1;int x4 = (int)rectBB.x0;int y4 = (int)rectBB.y1;
    
    int xCart = (*iT)->getPos().x;
    int yCart = (*iT)->getPos().y;

    int xCenter = mPersonTrackMask.width()/2;
    int yCenter = mPersonTrackMask.height()/2;

    //Get grid position of cart
    int xGridVal =0;
    int yGridVal =0;

    int x_scale = 320/mPersonTrackMask.width();
    int y_scale = 240/mPersonTrackMask.height();

    for(int a=0;a<occRegions.size()/2;a++)
      if(xCart>occRegions[a]/x_scale)
        xGridVal++;
    for(int a=occRegions.size()/2;a<occRegions.size();a++)
      if(yCart>occRegions[a]/y_scale)
        yGridVal++;

    //PVMSG("%d %d\n",xGridVal,yGridVal);

    //Assume a 4*4 grid

    if(xGridVal > 0 && xGridVal < 3 && yGridVal > 0 && yGridVal < 3)
    {
      //Do nothing
    }

    if(yGridVal==0)
    {
      
      //Add rectangles to the top side
      Rectanglei occludingRect;
      int yTop = y1 - occDepth >= 0 ? y1 - occDepth : 0;
      //Increase by 15% the width
      int xLeft = x1 - int(float(x2-x1)*extraPercent) >= 0 ? x1 - int(float(x2-x1)*extraPercent) : 0;
      int xRight = x2 + int(float(x2-x1)*extraPercent) < 2*xCenter ? x2 + int(float(x2-x1)*extraPercent) : 2*xCenter-1;

      occludingRect.set(xLeft,yTop,xRight,y1);
      mOccludingCartRectangles.push_back(occludingRect);
    }

    if(yGridVal==3)
    {
      //Add rectangles to the bottom side
      Rectanglei occludingRect;
      int yBottom = y3 + occDepth < 2*yCenter ? y3 + occDepth : 2*yCenter-1;
      
      //Increase by 15% the width
      int xLeft = x1 - int(float(x2-x1)*extraPercent) >= 0 ? x1 - int(float(x2-x1)*extraPercent) : 0;
      int xRight = x2 + int(float(x2-x1)*extraPercent) < 2*xCenter ? x2 + int(float(x2-x1)*extraPercent) : 2*xCenter-1;

      occludingRect.set(xLeft,y4,xRight,yBottom);
      mOccludingCartRectangles.push_back(occludingRect);      
    }
    if(xGridVal==0)
    {
      //Add rectangles to the left side
      Rectanglei occludingRect;
      // SM int xLeft = x1 - (x2-x1) >= 0 ? x1 - (x2-x1) : 0;
      int xLeft = x1 - occDepth >= 0 ? x1 - occDepth : 0;
      //Increase by 15% the height
      int yTop = y1 - int(float(y3-y1)*extraPercent) >= 0 ? y1 - int(float(y3-y1)*extraPercent) : 0;
      int yBottom = y3 + int(float(y3-y1)*extraPercent) < 2*yCenter ? y3 + int(float(y3-y1)*extraPercent) : 2*yCenter-1;

      occludingRect.set(xLeft,yTop,x1,yBottom);
      mOccludingCartRectangles.push_back(occludingRect);

    }
    if(xGridVal==3)
    {
      //Add rectangles to the right side
      Rectanglei occludingRect;
      //SM int xRight = x1 + (x2-x1) < xCenter*2 ? x1 + (x2-x1) : 2*xCenter-1;
      int xRight = x3 + occDepth < xCenter*2 ? x3 + occDepth : 2*xCenter-1;

      //Increase by 15% the height
      int yTop = y1 - int(float(y3-y1)*extraPercent) >= 0 ? y1 - int(float(y3-y1)*extraPercent) : 0;
      int yBottom = y3 + int(float(y3-y1)*extraPercent) < 2*yCenter ? y3 + int(float(y3-y1)*extraPercent) : 2*yCenter-1;

      occludingRect.set(x2,yTop,xRight,yBottom);
      mOccludingCartRectangles.push_back(occludingRect);
    }
    
    }
  }
}

void
PersonShapeTrackerMod::getOccludingCartRectangles()
{
  mOccludingCartRectangles.clear();
  
  for(TracksShapePS::iterator iT=mCartTracks.begin();iT!=mCartTracks.end();iT++)
  {
    Polygon cartPolygon;
    (*iT)->getCartPolygon(cartPolygon);
    Rectanglef rectBB=cartPolygon.getBoundingBox();
    fixRect(rectBB);
    int x1 = (int)rectBB.x0,y1 = (int)rectBB.y0,x2 = (int)rectBB.x1,y2 = (int)rectBB.y0,x3 = (int)rectBB.x1,y3 = (int)rectBB.y1,x4 = (int)rectBB.x0,y4 = (int)rectBB.y1;
    //PVMSG("\nALL %d %d %d %d %d %d %d %d\n",x1,y1,x2,y2,x3,y3,x4,y4);
    //Figure the further line from the center of image
    int xCenter = mPersonTrackMask.width()/2;
    int yCenter = mPersonTrackMask.height()/2;

    int xCart = (*iT)->getPos().x;
    int yCart = (*iT)->getPos().y;

    Rectanglei occludingRect;
    bool flagDone = false;
    //Determine whether the line joining center of image and center of cart cuts any line or not
    float ua,ub;

    float a1,a2,a3,a4,b1,b2,b3,b4;
    a1 = xCart;
    b1 = yCart;
    a2 = xCenter;
    b2 = yCenter;
    //Line 1 Top of rect

    if(flagDone==false)
    {

    
    a3 = x1;
    b3 = y1;
    a4 = x2;
    b4 = y2;
    
    ua = ((a4-a3)*(b1-b3) - (b4-b3)*(a1-a3))/((b4-b3)*(a2-a1) - (a4-a3)*(b2-b1));
    ub = ((a2-a1)*(b1-b3) - (b2-b1)*(a1-a3))/((b4-b3)*(a2-a1) - (a4-a3)*(b2-b1));

    if(ua<=1 && ua>=0 && ub<=1 && ub>=0)
    {
      int lowY = y3 + (y3-y2) < yCenter*2 ? y3 + (y3-y2): yCenter*2-1;
      occludingRect.set(x4,y4,x3,lowY);
      //PVMSG("Line 1 %d %d %d %d",x4,y4,x3,lowY);
      flagDone = true;
    }
    }

    //Line 2 Top of rect

    if(flagDone==false)
    {

    
    a3 = x2;
    b3 = y2;
    a4 = x3;
    b4 = y3;

    ua = ((a4-a3)*(b1-b3) - (b4-b3)*(a1-a3))/((b4-b3)*(a2-a1) - (a4-a3)*(b2-b1));
    ub = ((a2-a1)*(b1-b3) - (b2-b1)*(a1-a3))/((b4-b3)*(a2-a1) - (a4-a3)*(b2-b1));

    if(ua<=1 && ua>=0 && ub<=1 && ub>=0)
    {
      int lowX = x1 - (x3-x1) >= 0 ? x1 - (x3-x1): 0;
      occludingRect.set(lowX,y2,x4,y3);
      //PVMSG("Line 2 %d %d %d %d",lowX,y2,x4,y3);
      flagDone = true;
    }
    }
    //Line 3 Top of rect

    if(flagDone==false)
    {

    
    a3 = x3;
    b3 = y3;
    a4 = x4;
    b4 = y4;

    ua = ((a4-a3)*(b1-b3) - (b4-b3)*(a1-a3))/((b4-b3)*(a2-a1) - (a4-a3)*(b2-b1));
    ub = ((a2-a1)*(b1-b3) - (b2-b1)*(a1-a3))/((b4-b3)*(a2-a1) - (a4-a3)*(b2-b1));

    if(ua<=1 && ua>=0 && ub<=1 && ub>=0)
    {
      int highY = y1 - (y3-y1) >= 0 ? y1 - (y3-y1): 0;
      occludingRect.set(x1,highY,x2,y2);
      //PVMSG("Line 3 %d %d %d %d",x1,highY,x2,y2);
      flagDone = true;
    }
    }

    //Line 4 Top of rect

    if(flagDone==false)
    {
      a3 = x4;
    b3 = y4;
    a4 = x1;
    b4 = y1;

    ua = ((a4-a3)*(b1-b3) - (b4-b3)*(a1-a3))/((b4-b3)*(a2-a1) - (a4-a3)*(b2-b1));
    ub = ((a2-a1)*(b1-b3) - (b2-b1)*(a1-a3))/((b4-b3)*(a2-a1) - (a4-a3)*(b2-b1));

    if(ua<=1 && ua>=0 && ub<=1 && ub>=0)
    {
      int highX = x2 + (x2-x1) > xCenter*2 ? x2 + (x2-x1): xCenter*2-1;
      occludingRect.set(x2,y2,highX,y3);
      //PVMSG("Line 4 %d %d %d %d",x2,y2,highX,y3);
      flagDone = true;
    }
    }
    mOccludingCartRectangles.push_back(occludingRect);
  }
}


void
PersonShapeTrackerMod::createTracksForCarts()
{
  for(int s=0;s<mvCartPositions.size();s++)
  {
    Rectanglef rectBB = mCartPolygons[s].getBoundingBox();
    fixRect(rectBB);
    //if(mvCartPositions[s].isAssigned == false && mKillTrackMask((int)mvCartPositions[s].personPosition.x,(int)mvCartPositions[s].personPosition.y)>0)
    if(mvCartPositions[s].isAssigned == false &&
      (mKillTrackMask(rectBB.x0,rectBB.y0)>0
      || mKillTrackMask(rectBB.x1,rectBB.y0)>0
      || mKillTrackMask(rectBB.x1,rectBB.y1)>0
      || mKillTrackMask(rectBB.x0,rectBB.y1)>0))
    {
      createNewCartTracks(mvCartPositions[s].personPosition.x,mvCartPositions[s].personPosition.y,mCartPolygons[s]);
    }
  }

}

void
PersonShapeTrackerMod::createNewCartTracks(int x, int y, Polygon& poly)
{
  Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();
  Vector2i offset;
  Rectanglei roiRect = mpForegroundSegmentMod->getRoiRectangle();
  offset.x = std::min(roiRect.x0,roiRect.x1) * scaleFactor.x;
  offset.y = std::min(roiRect.y0,roiRect.y1) * scaleFactor.y;

  MultiFrameTrackShapePSPtr pNewTrack(new MultiFrameTrackShapePS());

  pNewTrack->init(
    mNewTrackId++,
    mSettings.getPath(),
    Vector2i(x,y),      
    getCurrentTime(),
    getFramesPerSecond(),
    mpPersonShapeEstimator,
    offset,100); 

  //Set the type of track depending on where it was initialized
  /*if(mGoodTrackRegionMask(x,y)>0)
    pNewTrack->setTrackInitType(true);
  else*/
    pNewTrack->setTrackInitType(false);
    pNewTrack->setCartPolygon(poly);
   // pNewTrack->setTrackStatus(false);
  //pNewTrack->setGoodTrackRegion(mGoodTrackRegionMask);

  if (mVerbose)
  {
    //    PVMSG("### Created Track %d at (%d,%d) [frame: %d]\n",pNewTrack->getId(),iLocation->x,iLocation->y,getCurrentFrameNumber());
  }
  mCartTracks.push_back(pNewTrack);  

}

void
PersonShapeTrackerMod::getCartPositions()
{
  mvCartPositions.clear();
  for(int i=0;i<mCartPolygons.size();i++)
  {
    personStructure temp;
    Rectanglef polygonBB = mCartPolygons[i].getBoundingBox();
    temp.personPosition = polygonBB.getCenter();
    //PVMSG("x=%f y=%f\n",temp.personPosition.x,temp.personPosition.y);
    temp.personProbability = 255;
    temp.isAssigned = false;
    mvCartPositions.push_back(temp);

  }
}

void adaptToBounds(Rectanglef& tempRect,int width, int height)
{
  if(tempRect.x0<0)
    tempRect.x0=0;
  if(tempRect.x0>=width)
    tempRect.x0=width-1;

  if(tempRect.x1>=width)
    tempRect.x1=width-1;
  if(tempRect.x1<0)
    tempRect.x1=0;

  if(tempRect.y0<0)
    tempRect.y0=0;
  if(tempRect.y0>=height)
    tempRect.y0=height-1;

  if(tempRect.y1>=height)
    tempRect.y1=height-1;
  if(tempRect.y1<0)
    tempRect.y1=0;

}

void 
PersonShapeTrackerMod::killCartTracksOutsidePoly()
{
  for(TracksShapePS::iterator iT=mCartTracks.begin(); iT!=mCartTracks.end();iT++)
  {
    Polygon tempPoly;
    (*iT)->getCartPolygon(tempPoly);
    Rectanglef tempRect = tempPoly.getBoundingBox();
    fixRect(tempRect);

    //Make sure tempRect lies inside bounds
    adaptToBounds(tempRect,mPersonTrackMask.width(),mPersonTrackMask.height());
    //if(mPersonTrackMask((*iT)->getPos().x,(*iT)->getPos().y) == 0)
    if(mPersonTrackMask(int(tempRect.x0),int(tempRect.y0)) == 0
      && mPersonTrackMask(int(tempRect.x0),int(tempRect.y1)) == 0
      && mPersonTrackMask(int(tempRect.x1),int(tempRect.y0)) == 0
      && mPersonTrackMask(int(tempRect.x1),int(tempRect.y1)) == 0)
      (*iT)->kill();
  }
}

void
PersonShapeTrackerMod::deleteDeadCartTracks()
{
  for(TracksShapePS::iterator iT = mCartTracks.begin();iT!=mCartTracks.end();)
  {
    if(!(*iT)->isAlive())
      iT = mCartTracks.erase(iT);
    else
      iT++;
  }
}
void
PersonShapeTrackerMod::trackCarts()
{
  TracksShapePS::iterator iTrack;
   {


    //Now handle those confirmed tracks that are still unassigned
    //First create a large list with all the unassigned tracks and persons (including low threshold) arranged by distance
    //Create a single vector containing all the unassigned tracks
    std::vector< tableAssignment > unAssignedTrackTable;
    int trackNum=0;
    for(iTrack=mCartTracks.begin();iTrack!=mCartTracks.end();iTrack++,trackNum++)
    {
      if(!(*iTrack)->getAssignmentVar())
      {

        for(int a=0;a<mvCartPositions.size();a++)
        {
          if(mvCartPositions[a].isAssigned == false)
          {
            tableAssignment temp;
            temp.personNum = a;
            temp.trackNum = trackNum;
            temp.dist = sqrt(dist2((*iTrack)->getPos(),mvCartPositions[a].personPosition));
            temp.HPP = true;
            //PVMSG("Distance = %f\n",temp.dist);
            if(temp.dist<mMaxCartTrackDistance)
              unAssignedTrackTable.push_back(temp);
          }
        }
 
        }
      }
    
    rowSort(unAssignedTrackTable);

    //Now go down the unAssignedTrackTable and assign the unassigned tracks
    for(int a=0;a<unAssignedTrackTable.size();a++)
    {
      iTrack = mCartTracks.begin();
      for(int i=0;i<unAssignedTrackTable[a].trackNum;i++,iTrack++);
      if((*iTrack)->getAssignmentVar()==false && unAssignedTrackTable[a].dist<mMaxCartTrackDistance)
      {

        
        
          if(mvCartPositions[unAssignedTrackTable[a].personNum].isAssigned == false)
          {
            iTrack = mCartTracks.begin();
            for(int i=0;i<unAssignedTrackTable[a].trackNum;i++,iTrack++);

            //PVMSG("\nAssigned track %d at (%f,%f) to person %d at (%f,%f)\n",comparisonTable[a][0].trackNum,(*iTrack)->getPos().x,(*iTrack)->getPos().y,comparisonTable[a][0].personNum,mvPersonPositions[comparisonTable[a][0].personNum].personPosition.x,mvPersonPositions[comparisonTable[a][0].personNum].personPosition.y);
            (*iTrack)->setAssignmentVar(true);
            mvCartPositions[unAssignedTrackTable[a].personNum].isAssigned = true;
            (*iTrack)->setPersonPosition(mvCartPositions[unAssignedTrackTable[a].personNum].personPosition);
            //(*iTrack)->setPersonDensityValue(mvCartPositions[comparisonTable[a][0].personNum].personProbability);
            //Use something to indicate that a HPP was used
            (*iTrack)->setPersonProbabilityStatus(true);
            (*iTrack)->noUseTrackAge = 0;
            (*iTrack)->setCartPolygon(mCartPolygons[unAssignedTrackTable[a].personNum]);
          }
       
        
      }
    }
  }

  //Keep the carts wherever they are in case they are not assigned to any existing cart
  for(iTrack = mCartTracks.begin();iTrack != mCartTracks.end();iTrack++)
  {
    //PVMSG("Check2 x=%f,y=%f,assign=%d,ID=%d\n",(*iTrack)->getPersonPosition().x,(*iTrack)->getPersonPosition().y,(*iTrack)->getAssignmentVar(),(*iTrack)->getId());
    if(!(*iTrack)->getAssignmentVar())
    {

      (*iTrack)->setAssignmentVar(true);
      //(*iTrack)->setPersonPosition((*iTrack)->getPos());
      (*iTrack)->setPersonPosition((*iTrack)->getPredictedPos());
      Polygon cartPoly;
      (*iTrack)->getCartPolygon(cartPoly);
      Vector2f motionPos;
      motionPos.x = (*iTrack)->getPredictedPos().x - (*iTrack)->getPos().x;
      motionPos.y = (*iTrack)->getPredictedPos().y - (*iTrack)->getPos().y;
      cartPoly.translate(Vector2f(int(motionPos.x),int(motionPos.y)));
      (*iTrack)->setCartPolygon(cartPoly);
      (*iTrack)->noUseTrackAge++;
      if((*iTrack)->noUseTrackAge >  15 || (*iTrack)->getTrackStatus()==true)
        (*iTrack)->kill();

    }
  }
}

void
  PersonShapeTrackerMod::personTrackAssignmentPredicted(Image8& filterImg)
{
  bool isDone = false;
  bool useLowPP = false;
  TracksShapePS::iterator iTrack;
  int tracksAssigned = 0;
  int tracksKilled = 0;

  //First do the assignment for confirmed tracks
  {


    //First set up the table
    std::vector< std::vector< tableAssignment > > comparisonTable;

    //For each unassigned confirmed track make a column entry for the table
    int trackNum = 0;
    for(iTrack = mTracks.begin();iTrack != mTracks.end();iTrack++,trackNum++)
    {
      std::vector< tableAssignment > trackRow;
      if((*iTrack)->getTrackStatus() == false)
      {

        for(int a=0;a<mvPersonPositions.size();a++)
        {
          if(mvPersonPositions[a].isAssigned == false)
          {
            tableAssignment temp;
            temp.personNum = a;
            temp.trackNum = trackNum;
            temp.dist = sqrt(dist2((*iTrack)->getPredictedPos(),mvPersonPositions[a].personPosition));
            /*Polygon tempPolygon;
            (*iTrack)->getSearchRectangle(tempPolygon);*/
            temp.HPP = true;
            if(temp.dist<mMaxPersonTrackDistance)
            //if(tempPolygon.isPointInside(mvPersonPositions[a].personPosition))
              trackRow.push_back(temp);
          }
        }
   
        rowSort(trackRow);
   
        comparisonTable.push_back(trackRow);
      }
    }

    //Table is made

    //Now check for non-conflict track person assignment
    for(int a=0;a<comparisonTable.size();a++)
    {
      bool isConflicting = false;
      for(int b=0;b<comparisonTable.size();b++)
      {
        if(a!=b && comparisonTable[a].size()>0 && comparisonTable[b].size()>0)
        {
          if(comparisonTable[a][0].personNum == comparisonTable[b][0].personNum && comparisonTable[a][0].HPP == comparisonTable[b][0].HPP)
          {
            isConflicting = true;
            break;
          }
        }
      }
      if(comparisonTable[a].size()>0)
        if(!isConflicting && comparisonTable[a][0].dist<mMaxPersonTrackDistance)
        {
          //Do the track person assignment
          //Get the appropriate track
          iTrack = mTracks.begin();
          for(int i=0;i<comparisonTable[a][0].trackNum;i++,iTrack++);

          //PVMSG("\nAssigned track %d at (%f,%f) to person %d at (%f,%f)\n",comparisonTable[a][0].trackNum,(*iTrack)->getPos().x,(*iTrack)->getPos().y,comparisonTable[a][0].personNum,mvPersonPositions[comparisonTable[a][0].personNum].personPosition.x,mvPersonPositions[comparisonTable[a][0].personNum].personPosition.y);
          (*iTrack)->setAssignmentVar(true);
          mvPersonPositions[comparisonTable[a][0].personNum].isAssigned = true;
          (*iTrack)->setPersonPosition(mvPersonPositions[comparisonTable[a][0].personNum].personPosition);
          (*iTrack)->setPersonDensityValue(mvPersonPositions[comparisonTable[a][0].personNum].personProbability);
          tracksAssigned++;
          //Use something to indicate that a HPP was used
          (*iTrack)->setPersonProbabilityStatus(true);
          (*iTrack)->noUseTrackAge = 0;
        }

    }
  }

  //Now do it with the lower threshold
  {


    //First set up the table
    std::vector< std::vector< tableAssignment > > comparisonTable;

    //For each unassigned confirmed track make a column entry for the table
    int trackNum = 0;
    for(iTrack = mTracks.begin();iTrack != mTracks.end();iTrack++,trackNum++)
    {
      std::vector< tableAssignment > trackRow;
      if((*iTrack)->getTrackStatus() == false && !(*iTrack)->getAssignmentVar())
      {

        for(int a=0;a<mvPersonPositions.size();a++)
        {
          if(mvPersonPositions[a].isAssigned == false)
          {
            tableAssignment temp;
            temp.personNum = a;
            temp.trackNum = trackNum;
            temp.dist = sqrt(dist2((*iTrack)->getPredictedPos(),mvPersonPositions[a].personPosition));
            temp.HPP = true;
            /*Polygon tempPolygon;
            (*iTrack)->getSearchRectangle(tempPolygon);*/
            if(temp.dist<mMaxPersonTrackDistance)
            //if(tempPolygon.isPointInside(mvPersonPositions[a].personPosition))
              trackRow.push_back(temp);
          }
        }

        for(int a=0;a<mvPersonPositionsLowThresh.size();a++)
        {
          if(mvPersonPositionsLowThresh[a].isAssigned == false)
          {
            tableAssignment temp;
            temp.personNum = a;
            temp.trackNum = trackNum;
            temp.dist = sqrt(dist2((*iTrack)->getPredictedPos(),mvPersonPositionsLowThresh[a].personPosition));
            temp.HPP = false;
            /*Polygon tempPolygon;
            (*iTrack)->getSearchRectangle(tempPolygon);*/
            if(temp.dist<mMaxPersonTrackDistance)
            //if(tempPolygon.isPointInside(mvPersonPositionsLowThresh[a].personPosition))
              trackRow.push_back(temp);
          }
        }
        //Sort the trackRow
        rowSort(trackRow);
        comparisonTable.push_back(trackRow);
      }
    }
    //Table is made
    //Now check for non-conflict track person assignment
    for(int a=0;a<comparisonTable.size();a++)
    {
      bool isConflicting = false;
      for(int b=0;b<comparisonTable.size();b++)
      {
        if(a!=b && comparisonTable[a].size()>0 && comparisonTable[b].size()>0)
        {
          if(comparisonTable[a][0].personNum == comparisonTable[b][0].personNum  && comparisonTable[a][0].HPP == comparisonTable[b][0].HPP)
          {
            isConflicting = true;
            break;
          }
        }
      }
      if(comparisonTable[a].size()>0)
        if(!isConflicting && comparisonTable[a][0].dist<mMaxPersonTrackDistance)
        {
          //Do the track person assignment
          //Get the appropriate track
          iTrack = mTracks.begin();
          for(int i=0;i<comparisonTable[a][0].trackNum;i++,iTrack++);

          (*iTrack)->setAssignmentVar(true);
          if(comparisonTable[a][0].HPP == true)
          {
            //PVMSG("\nAssigned track %d at (%f,%f) to person %d at (%f,%f)\n",comparisonTable[a][0].trackNum,(*iTrack)->getPos().x,(*iTrack)->getPos().y,comparisonTable[a][0].personNum,mvPersonPositions[comparisonTable[a][0].personNum].personPosition.x,mvPersonPositions[comparisonTable[a][0].personNum].personPosition.y);
            mvPersonPositions[comparisonTable[a][0].personNum].isAssigned = true;
            (*iTrack)->setPersonPosition(mvPersonPositions[comparisonTable[a][0].personNum].personPosition);
            (*iTrack)->setAssignmentVar(true);
            (*iTrack)->setPersonDensityValue(mvPersonPositions[comparisonTable[a][0].personNum].personProbability);
            tracksAssigned++;
            //Use something to indicate that a HPP was used
            (*iTrack)->setPersonProbabilityStatus(true);
            (*iTrack)->noUseTrackAge = 0;
          }
          else
          {
            //PVMSG("\nAssigned track %d at (%f,%f) to person %d at (%f,%f)\n",comparisonTable[a][0].trackNum,(*iTrack)->getPos().x,(*iTrack)->getPos().y,comparisonTable[a][0].personNum,mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition.x,mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition.y);
            mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].isAssigned = true;
            (*iTrack)->setPersonPosition(mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition);
            (*iTrack)->setAssignmentVar(true);
            (*iTrack)->setPersonDensityValue(mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personProbability);
            tracksAssigned++;
            //Use something to indicate that a LPP was used
            (*iTrack)->setPersonProbabilityStatus(false);
            (*iTrack)->noUseTrackAge = 0;
          }
        }
    }
  }
 
//For occluded persons
  {


    //First set up the table
    std::vector< std::vector< tableAssignment > > comparisonTable;

    //For each unassigned confirmed track make a column entry for the table
    int trackNum = 0;
    for(iTrack = mTracks.begin();iTrack != mTracks.end();iTrack++,trackNum++)
    {
      std::vector< tableAssignment > trackRow;
      if((*iTrack)->getTrackStatus() == false && !(*iTrack)->getAssignmentVar())
      {

        for(int a=0;a<mvPersonPositions.size();a++)
        {
          if(mvPersonPositions[a].isAssigned == false)
          {
            tableAssignment temp;
            temp.personNum = a;
            temp.trackNum = trackNum;
            temp.dist = sqrt(dist2((*iTrack)->getPredictedPos(),mvPersonPositions[a].personPosition));
            temp.HPP = true;
            temp.occluded = false;
            /*Polygon tempPolygon;
            (*iTrack)->getSearchRectangle(tempPolygon);*/
            if(temp.dist<mMaxPersonTrackDistance)
            //if(tempPolygon.isPointInside(mvPersonPositions[a].personPosition))
              trackRow.push_back(temp);
          }
        }

        for(int a=0;a<mvPersonPositionsLowThresh.size();a++)
        {
          if(mvPersonPositionsLowThresh[a].isAssigned == false)
          {
            tableAssignment temp;
            temp.personNum = a;
            temp.trackNum = trackNum;
            temp.dist = sqrt(dist2((*iTrack)->getPredictedPos(),mvPersonPositionsLowThresh[a].personPosition));
            temp.HPP = false;
            temp.occluded = false;
            /*Polygon tempPolygon;
            (*iTrack)->getSearchRectangle(tempPolygon);*/
            if(temp.dist<mMaxPersonTrackDistance)
            //if(tempPolygon.isPointInside(mvPersonPositionsLowThresh[a].personPosition))
              trackRow.push_back(temp);
          }
        }

        for(int a=0;a<mvOccludedPersonPositions.size();a++)
        {
          if(mvOccludedPersonPositions[a].isAssigned == false)
          {
            tableAssignment temp;
            temp.personNum = a;
            temp.trackNum = trackNum;
            temp.dist = sqrt(dist2((*iTrack)->getPredictedPos(),mvOccludedPersonPositions[a].personPosition));
            temp.HPP = false;
            temp.occluded = true;
            /*Polygon tempPolygon;
            (*iTrack)->getSearchRectangle(tempPolygon);*/
            if(temp.dist<mMaxPersonTrackDistance)
            //if(tempPolygon.isPointInside(mvOccludedPersonPositions[a].personPosition))
              trackRow.push_back(temp);
          }
        }
        //Sort the trackRow
        rowSort(trackRow);
        comparisonTable.push_back(trackRow);
      }
    }
    
    //Table is made
    //Now check for non-conflict track person assignment
    for(int a=0;a<comparisonTable.size();a++)
    {
      bool isConflicting = false;
      for(int b=0;b<comparisonTable.size();b++)
      {
        if(a!=b && comparisonTable[a].size()>0 && comparisonTable[b].size()>0)
        {
          if(comparisonTable[a][0].personNum == comparisonTable[b][0].personNum  && comparisonTable[a][0].HPP == comparisonTable[b][0].HPP && comparisonTable[a][0].occluded == comparisonTable[b][0].occluded)
          {
            isConflicting = true;
            break;
          }
        }
      }
      if(comparisonTable[a].size()>0)
        if(!isConflicting && comparisonTable[a][0].dist<mMaxPersonTrackDistance)
        {
          //Do the track person assignment
          //Get the appropriate track
          iTrack = mTracks.begin();
          for(int i=0;i<comparisonTable[a][0].trackNum;i++,iTrack++);

          (*iTrack)->setAssignmentVar(true);
          if(comparisonTable[a][0].HPP == true)
          {
            //PVMSG("\nAssigned track %d at (%f,%f) to person %d at (%f,%f)\n",comparisonTable[a][0].trackNum,(*iTrack)->getPos().x,(*iTrack)->getPos().y,comparisonTable[a][0].personNum,mvPersonPositions[comparisonTable[a][0].personNum].personPosition.x,mvPersonPositions[comparisonTable[a][0].personNum].personPosition.y);
            mvPersonPositions[comparisonTable[a][0].personNum].isAssigned = true;
            (*iTrack)->setPersonPosition(mvPersonPositions[comparisonTable[a][0].personNum].personPosition);
            (*iTrack)->setAssignmentVar(true);
            (*iTrack)->setPersonDensityValue(mvPersonPositions[comparisonTable[a][0].personNum].personProbability);
            tracksAssigned++;
            //Use something to indicate that a HPP was used
            (*iTrack)->setPersonProbabilityStatus(true);
            (*iTrack)->noUseTrackAge = 0;
          }
          else
          {
            if(comparisonTable[a][0].occluded == true)
            {
              //PVMSG("\nAssigned track %d at (%f,%f) to person %d at (%f,%f)\n",comparisonTable[a][0].trackNum,(*iTrack)->getPos().x,(*iTrack)->getPos().y,comparisonTable[a][0].personNum,mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition.x,mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition.y);
              mvOccludedPersonPositions[comparisonTable[a][0].personNum].isAssigned = true;
              (*iTrack)->setPersonPosition(mvOccludedPersonPositions[comparisonTable[a][0].personNum].personPosition);
              (*iTrack)->setAssignmentVar(true);
              (*iTrack)->setPersonDensityValue(mvOccludedPersonPositions[comparisonTable[a][0].personNum].personProbability);
              tracksAssigned++;
              //Use something to indicate that a LPP was used
              (*iTrack)->setPersonProbabilityStatus(false);
              (*iTrack)->noUseTrackAge = 0;
            }
            else
            {

            
            //PVMSG("\nAssigned track %d at (%f,%f) to person %d at (%f,%f)\n",comparisonTable[a][0].trackNum,(*iTrack)->getPos().x,(*iTrack)->getPos().y,comparisonTable[a][0].personNum,mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition.x,mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition.y);
            mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].isAssigned = true;
            (*iTrack)->setPersonPosition(mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition);
            (*iTrack)->setAssignmentVar(true);
            (*iTrack)->setPersonDensityValue(mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personProbability);
            tracksAssigned++;
            //Use something to indicate that a LPP was used
            (*iTrack)->setPersonProbabilityStatus(false);
            (*iTrack)->noUseTrackAge = 0;
            }
          }
        }
    }
  }
 
  {


    //Now handle those confirmed tracks that are still unassigned
    //First create a large list with all the unassigned tracks and persons (including low threshold) arranged by distance
    //Create a single vector containing all the unassigned tracks
    std::vector< tableAssignment > unAssignedTrackTable;
    int trackNum=0;
    for(iTrack=mTracks.begin();iTrack!=mTracks.end();iTrack++,trackNum++)
    {
      if(!(*iTrack)->getAssignmentVar() && !(*iTrack)->getTrackStatus())
      {

        for(int a=0;a<mvPersonPositions.size();a++)
        {
          if(mvPersonPositions[a].isAssigned == false)
          {
            tableAssignment temp;
            temp.personNum = a;
            temp.trackNum = trackNum;
            temp.dist = sqrt(dist2((*iTrack)->getPredictedPos(),mvPersonPositions[a].personPosition));
            temp.HPP = true;
            /*Polygon tempPolygon;
            (*iTrack)->getSearchRectangle(tempPolygon);
            if(tempPolygon.isPointInside(mvPersonPositions[a].personPosition))*/
            if(temp.dist<mMaxPersonTrackDistance)
              unAssignedTrackTable.push_back(temp);
          }
        }

        for(int a=0;a<mvPersonPositionsLowThresh.size();a++)
        {
          if(mvPersonPositionsLowThresh[a].isAssigned == false)
          {
            tableAssignment temp;
            temp.personNum = a;
            temp.trackNum = trackNum;
            temp.dist = sqrt(dist2((*iTrack)->getPredictedPos(),mvPersonPositionsLowThresh[a].personPosition));
            temp.HPP = false;
            /*Polygon tempPolygon;
            (*iTrack)->getSearchRectangle(tempPolygon);*/
            if(temp.dist<mMaxPersonTrackDistance)
            //if(tempPolygon.isPointInside(mvPersonPositionsLowThresh[a].personPosition))
              unAssignedTrackTable.push_back(temp);
          }
        }
      }
    }
    rowSort(unAssignedTrackTable);

    //Now go down the unAssignedTrackTable and assign the unassigned tracks
    for(int a=0;a<unAssignedTrackTable.size();a++)
    {
      iTrack = mTracks.begin();
      for(int i=0;i<unAssignedTrackTable[a].trackNum;i++,iTrack++);
      if((*iTrack)->getAssignmentVar()==false && unAssignedTrackTable[a].dist<mMaxPersonTrackDistance)
      {

        if(unAssignedTrackTable[a].HPP == true)
        {
          if(mvPersonPositions[unAssignedTrackTable[a].personNum].isAssigned == false)
          {
            mvPersonPositions[unAssignedTrackTable[a].personNum].isAssigned = true;
            (*iTrack)->setPersonPosition(mvPersonPositions[unAssignedTrackTable[a].personNum].personPosition);
            (*iTrack)->setAssignmentVar(true);
            (*iTrack)->setPersonDensityValue(mvPersonPositions[unAssignedTrackTable[a].personNum].personProbability);
            tracksAssigned++;
            //Use something to indicate that a HPP was used
            (*iTrack)->setPersonProbabilityStatus(true);
            (*iTrack)->noUseTrackAge = 0;
          }
        }
        else
        {
          if(mvPersonPositionsLowThresh[unAssignedTrackTable[a].personNum].isAssigned == false)
          {
            mvPersonPositionsLowThresh[unAssignedTrackTable[a].personNum].isAssigned = true;
            (*iTrack)->setPersonPosition(mvPersonPositionsLowThresh[unAssignedTrackTable[a].personNum].personPosition);
            (*iTrack)->setAssignmentVar(true);
            (*iTrack)->setPersonDensityValue(mvPersonPositionsLowThresh[unAssignedTrackTable[a].personNum].personProbability);
            tracksAssigned++;
            //Use something to indicate that a LPP was used
            (*iTrack)->setPersonProbabilityStatus(false);
            (*iTrack)->noUseTrackAge = 0;
          }
        }
      }
    }
  }
  /*PVMSG("Stage 4 Track positions\n");
  for(iTrack=mTracks.begin();iTrack!=mTracks.end();iTrack++)
  PVMSG("Pos = %f %f, status = %d \n",(*iTrack)->getPos().x,(*iTrack)->getPos().y,(*iTrack)->getAssignmentVar());*/

  //Now do the assignment for probationary tracks

  {


    //First set up the table
    std::vector< std::vector< tableAssignment > > comparisonTable;

    //For each unassigned confirmed track make a column entry for the table
    int trackNum = 0;
    for(iTrack = mTracks.begin();iTrack != mTracks.end();iTrack++,trackNum++)
    {
      std::vector< tableAssignment > trackRow;
      if((*iTrack)->getTrackStatus() == true)
      {

        for(int a=0;a<mvPersonPositions.size();a++)
        {
          if(mvPersonPositions[a].isAssigned == false)
          {
            tableAssignment temp;
            temp.personNum = a;
            temp.trackNum = trackNum;
            temp.dist = sqrt(dist2((*iTrack)->getPredictedPos(),mvPersonPositions[a].personPosition));
            temp.HPP = true;
           /* Polygon tempPolygon;
            (*iTrack)->getSearchRectangle(tempPolygon);*/
            if(temp.dist<mMaxPersonTrackDistance)
            //if(tempPolygon.isPointInside(mvPersonPositions[a].personPosition))
              trackRow.push_back(temp);
          }
        }
        //Sort the trackRow
        /* PVMSG("\nBEFORE\n");
        for(int r=0;r<trackRow.size();r++)
        PVMSG("%f ",trackRow[r].dist);*/
        rowSort(trackRow);
        /* PVMSG("\nAFTER\n");
        for(int r=0;r<trackRow.size();r++)
        PVMSG("%f ",trackRow[r].dist);*/
        comparisonTable.push_back(trackRow);
      }
    }

    //Table is made

    //PVMSG("First pass output\n");
    ////Print out the best for all the tracks
    //for(int r=0;r<comparisonTable.size();r++)
    //  if(comparisonTable[r].size()>0)
    //    PVMSG("Track # %d, person number %d, person type %d, distance %f\n",comparisonTable[r][0].trackNum,comparisonTable[r][0].personNum,comparisonTable[r][0].HPP,comparisonTable[r][0].dist);
    //Now check for non-conflict track person assignment
    for(int a=0;a<comparisonTable.size();a++)
    {
      bool isConflicting = false;
      for(int b=0;b<comparisonTable.size();b++)
      {
        if(a!=b && comparisonTable[a].size()>0 && comparisonTable[b].size()>0)
        {
          if(comparisonTable[a][0].personNum == comparisonTable[b][0].personNum && comparisonTable[a][0].HPP == comparisonTable[b][0].HPP)
          {
            isConflicting = true;
            break;
          }
        }
      }
      if(comparisonTable[a].size()>0)
        if(!isConflicting && comparisonTable[a][0].dist<mMaxPersonTrackDistance)
        {
          //Do the track person assignment
          //Get the appropriate track
          iTrack = mTracks.begin();
          for(int i=0;i<comparisonTable[a][0].trackNum;i++,iTrack++);

          //PVMSG("\nAssigned track %d at (%f,%f) to person %d at (%f,%f)\n",comparisonTable[a][0].trackNum,(*iTrack)->getPos().x,(*iTrack)->getPos().y,comparisonTable[a][0].personNum,mvPersonPositions[comparisonTable[a][0].personNum].personPosition.x,mvPersonPositions[comparisonTable[a][0].personNum].personPosition.y);
          (*iTrack)->setAssignmentVar(true);
          mvPersonPositions[comparisonTable[a][0].personNum].isAssigned = true;
          (*iTrack)->setPersonPosition(mvPersonPositions[comparisonTable[a][0].personNum].personPosition);
          (*iTrack)->setPersonDensityValue(mvPersonPositions[comparisonTable[a][0].personNum].personProbability);
          tracksAssigned++;
          //Use something to indicate that a HPP was used
          (*iTrack)->setPersonProbabilityStatus(true);
          (*iTrack)->noUseTrackAge = 0;
        }

    }
  }

  //Now do it with the lower threshold
  /*PVMSG("Stage 2 Track positions\n");
  for(iTrack=mTracks.begin();iTrack!=mTracks.end();iTrack++)
  PVMSG("Pos = %f %f, status = %d \n",(*iTrack)->getPos().x,(*iTrack)->getPos().y,(*iTrack)->getAssignmentVar());*/

  {


    //First set up the table
    std::vector< std::vector< tableAssignment > > comparisonTable;

    //For each unassigned confirmed track make a column entry for the table
    int trackNum = 0;
    for(iTrack = mTracks.begin();iTrack != mTracks.end();iTrack++,trackNum++)
    {
      std::vector< tableAssignment > trackRow;
      if((*iTrack)->getTrackStatus() == true && !(*iTrack)->getAssignmentVar())
      {

        for(int a=0;a<mvPersonPositions.size();a++)
        {
          if(mvPersonPositions[a].isAssigned == false)
          {
            tableAssignment temp;
            temp.personNum = a;
            temp.trackNum = trackNum;
            temp.dist = sqrt(dist2((*iTrack)->getPredictedPos(),mvPersonPositions[a].personPosition));
            temp.HPP = true;
            /*Polygon tempPolygon;
            (*iTrack)->getSearchRectangle(tempPolygon);*/
            if(temp.dist<mMaxPersonTrackDistance)
            //if(tempPolygon.isPointInside(mvPersonPositions[a].personPosition))
              trackRow.push_back(temp);
          }
        }

        for(int a=0;a<mvPersonPositionsLowThresh.size();a++)
        {
          if(mvPersonPositionsLowThresh[a].isAssigned == false)
          {
            tableAssignment temp;
            temp.personNum = a;
            temp.trackNum = trackNum;
            temp.dist = sqrt(dist2((*iTrack)->getPredictedPos(),mvPersonPositionsLowThresh[a].personPosition));
            temp.HPP = false;
            /*Polygon tempPolygon;
            (*iTrack)->getSearchRectangle(tempPolygon);*/
            if(temp.dist<mMaxPersonTrackDistance)
            //if(tempPolygon.isPointInside(mvPersonPositionsLowThresh[a].personPosition))
              trackRow.push_back(temp);
          }
        }
        //Sort the trackRow
        rowSort(trackRow);
        comparisonTable.push_back(trackRow);
      }
    }

    //PVMSG("Second pass output\n");
    ////Print out the best for all the tracks
    //for(int r=0;r<comparisonTable.size();r++)
    //  if(comparisonTable[r].size()>0)
    //    PVMSG("Track # %d, person number %d, person type %d, distance %f\n",comparisonTable[r][0].trackNum,comparisonTable[r][0].personNum,comparisonTable[r][0].HPP,comparisonTable[r][0].dist);
    //Table is made
    //Now check for non-conflict track person assignment
    for(int a=0;a<comparisonTable.size();a++)
    {
      bool isConflicting = false;
      for(int b=0;b<comparisonTable.size();b++)
      {
        if(a!=b && comparisonTable[a].size()>0 && comparisonTable[b].size()>0)
        {
          if(comparisonTable[a][0].personNum == comparisonTable[b][0].personNum  && comparisonTable[a][0].HPP == comparisonTable[b][0].HPP)
          {
            isConflicting = true;
            break;
          }
        }
      }
      if(comparisonTable[a].size()>0)
        if(!isConflicting && comparisonTable[a][0].dist<mMaxPersonTrackDistance)
        {
          //Do the track person assignment
          //Get the appropriate track
          iTrack = mTracks.begin();
          for(int i=0;i<comparisonTable[a][0].trackNum;i++,iTrack++);

          (*iTrack)->setAssignmentVar(true);
          if(comparisonTable[a][0].HPP == true)
          {
            //PVMSG("\nAssigned track %d at (%f,%f) to person %d at (%f,%f)\n",comparisonTable[a][0].trackNum,(*iTrack)->getPos().x,(*iTrack)->getPos().y,comparisonTable[a][0].personNum,mvPersonPositions[comparisonTable[a][0].personNum].personPosition.x,mvPersonPositions[comparisonTable[a][0].personNum].personPosition.y);
            mvPersonPositions[comparisonTable[a][0].personNum].isAssigned = true;
            (*iTrack)->setPersonPosition(mvPersonPositions[comparisonTable[a][0].personNum].personPosition);
            (*iTrack)->setAssignmentVar(true);
            (*iTrack)->setPersonDensityValue(mvPersonPositions[comparisonTable[a][0].personNum].personProbability);
            tracksAssigned++;
            //Use something to indicate that a HPP was used
            (*iTrack)->setPersonProbabilityStatus(true);
            (*iTrack)->noUseTrackAge = 0;
          }
          else
          {
            //PVMSG("\nAssigned track %d at (%f,%f) to person %d at (%f,%f)\n",comparisonTable[a][0].trackNum,(*iTrack)->getPos().x,(*iTrack)->getPos().y,comparisonTable[a][0].personNum,mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition.x,mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition.y);
            mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].isAssigned = true;
            (*iTrack)->setPersonPosition(mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition);
            (*iTrack)->setAssignmentVar(true);
            (*iTrack)->setPersonDensityValue(mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personProbability);
            tracksAssigned++;
            //Use something to indicate that a LPP was used
            (*iTrack)->setPersonProbabilityStatus(false);
            (*iTrack)->noUseTrackAge = 0;
          }
        }
    }
  }


  //For occluded persons
  {


    //First set up the table
    std::vector< std::vector< tableAssignment > > comparisonTable;

    //For each unassigned confirmed track make a column entry for the table
    int trackNum = 0;
    for(iTrack = mTracks.begin();iTrack != mTracks.end();iTrack++,trackNum++)
    {
      std::vector< tableAssignment > trackRow;
      if((*iTrack)->getTrackStatus() == true && !(*iTrack)->getAssignmentVar())
      {

        for(int a=0;a<mvPersonPositions.size();a++)
        {
          if(mvPersonPositions[a].isAssigned == false)
          {
            tableAssignment temp;
            temp.personNum = a;
            temp.trackNum = trackNum;
            temp.dist = sqrt(dist2((*iTrack)->getPredictedPos(),mvPersonPositions[a].personPosition));
            temp.HPP = true;
            temp.occluded = false;
            /*Polygon tempPolygon;
            (*iTrack)->getSearchRectangle(tempPolygon);*/
            if(temp.dist<mMaxPersonTrackDistance)
            //if(tempPolygon.isPointInside(mvPersonPositions[a].personPosition))
              trackRow.push_back(temp);
          }
        }

        for(int a=0;a<mvPersonPositionsLowThresh.size();a++)
        {
          if(mvPersonPositionsLowThresh[a].isAssigned == false)
          {
            tableAssignment temp;
            temp.personNum = a;
            temp.trackNum = trackNum;
            temp.dist = sqrt(dist2((*iTrack)->getPredictedPos(),mvPersonPositionsLowThresh[a].personPosition));
            temp.HPP = false;
            temp.occluded = false;
            /*Polygon tempPolygon;
            (*iTrack)->getSearchRectangle(tempPolygon);*/
            if(temp.dist<mMaxPersonTrackDistance)
            //if(tempPolygon.isPointInside(mvPersonPositionsLowThresh[a].personPosition))
              trackRow.push_back(temp);
          }
        }

        for(int a=0;a<mvOccludedPersonPositions.size();a++)
        {
          if(mvOccludedPersonPositions[a].isAssigned == false)
          {
            tableAssignment temp;
            temp.personNum = a;
            temp.trackNum = trackNum;
            temp.dist = sqrt(dist2((*iTrack)->getPredictedPos(),mvOccludedPersonPositions[a].personPosition));
            temp.HPP = false;
            temp.occluded = true;
            /*Polygon tempPolygon;
            (*iTrack)->getSearchRectangle(tempPolygon);*/
            if(temp.dist<mMaxPersonTrackDistance)
            //if(tempPolygon.isPointInside(mvOccludedPersonPositions[a].personPosition))
              trackRow.push_back(temp);
          }
        }
        //Sort the trackRow
        rowSort(trackRow);
        comparisonTable.push_back(trackRow);
      }
    }

    //PVMSG("Second pass output\n");
    ////Print out the best for all the tracks
    //for(int r=0;r<comparisonTable.size();r++)
    //  if(comparisonTable[r].size()>0)
    //    PVMSG("Track # %d, person number %d, person type %d, distance %f\n",comparisonTable[r][0].trackNum,comparisonTable[r][0].personNum,comparisonTable[r][0].HPP,comparisonTable[r][0].dist);
    //Table is made
    //Now check for non-conflict track person assignment
    for(int a=0;a<comparisonTable.size();a++)
    {
      bool isConflicting = false;
      for(int b=0;b<comparisonTable.size();b++)
      {
        if(a!=b && comparisonTable[a].size()>0 && comparisonTable[b].size()>0)
        {
          if(comparisonTable[a][0].personNum == comparisonTable[b][0].personNum  && comparisonTable[a][0].HPP == comparisonTable[b][0].HPP && comparisonTable[a][0].occluded == comparisonTable[b][0].occluded)
          {
            isConflicting = true;
            break;
          }
        }
      }
      if(comparisonTable[a].size()>0)
        if(!isConflicting && comparisonTable[a][0].dist<mMaxPersonTrackDistance)
        {
          //Do the track person assignment
          //Get the appropriate track
          iTrack = mTracks.begin();
          for(int i=0;i<comparisonTable[a][0].trackNum;i++,iTrack++);

          (*iTrack)->setAssignmentVar(true);
          if(comparisonTable[a][0].HPP == true)
          {
            //PVMSG("\nAssigned track %d at (%f,%f) to person %d at (%f,%f)\n",comparisonTable[a][0].trackNum,(*iTrack)->getPos().x,(*iTrack)->getPos().y,comparisonTable[a][0].personNum,mvPersonPositions[comparisonTable[a][0].personNum].personPosition.x,mvPersonPositions[comparisonTable[a][0].personNum].personPosition.y);
            mvPersonPositions[comparisonTable[a][0].personNum].isAssigned = true;
            (*iTrack)->setPersonPosition(mvPersonPositions[comparisonTable[a][0].personNum].personPosition);
            (*iTrack)->setAssignmentVar(true);
            (*iTrack)->setPersonDensityValue(mvPersonPositions[comparisonTable[a][0].personNum].personProbability);
            tracksAssigned++;
            //Use something to indicate that a HPP was used
            (*iTrack)->setPersonProbabilityStatus(true);
            (*iTrack)->noUseTrackAge = 0;
          }
          else
          {
            if(comparisonTable[a][0].occluded == true)
            {
              //PVMSG("\nAssigned track %d at (%f,%f) to person %d at (%f,%f)\n",comparisonTable[a][0].trackNum,(*iTrack)->getPos().x,(*iTrack)->getPos().y,comparisonTable[a][0].personNum,mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition.x,mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition.y);
              mvOccludedPersonPositions[comparisonTable[a][0].personNum].isAssigned = true;
              (*iTrack)->setPersonPosition(mvOccludedPersonPositions[comparisonTable[a][0].personNum].personPosition);
              (*iTrack)->setAssignmentVar(true);
              (*iTrack)->setPersonDensityValue(mvOccludedPersonPositions[comparisonTable[a][0].personNum].personProbability);
              tracksAssigned++;
              //Use something to indicate that a LPP was used
              (*iTrack)->setPersonProbabilityStatus(false);
              (*iTrack)->noUseTrackAge = 0;
            }
            else
            {


              //PVMSG("\nAssigned track %d at (%f,%f) to person %d at (%f,%f)\n",comparisonTable[a][0].trackNum,(*iTrack)->getPos().x,(*iTrack)->getPos().y,comparisonTable[a][0].personNum,mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition.x,mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition.y);
              mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].isAssigned = true;
              (*iTrack)->setPersonPosition(mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition);
              (*iTrack)->setAssignmentVar(true);
              (*iTrack)->setPersonDensityValue(mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personProbability);
              tracksAssigned++;
              //Use something to indicate that a LPP was used
              (*iTrack)->setPersonProbabilityStatus(false);
              (*iTrack)->noUseTrackAge = 0;
            }
          }
        }
    }
  }



  /*PVMSG("Stage 3 Track positions\n");
  for(iTrack=mTracks.begin();iTrack!=mTracks.end();iTrack++)
  PVMSG("Pos = %f %f, status = %d \n",(*iTrack)->getPos().x,(*iTrack)->getPos().y,(*iTrack)->getAssignmentVar());*/



  {


    //Now handle those confirmed tracks that are still unassigned
    //First create a large list with all the unassigned tracks and persons (including low threshold) arranged by distance
    //Create a single vector containing all the unassigned tracks
    std::vector< tableAssignment > unAssignedTrackTable;
    int trackNum=0;
    for(iTrack=mTracks.begin();iTrack!=mTracks.end();iTrack++,trackNum++)
    {
      if(!(*iTrack)->getAssignmentVar() && (*iTrack)->getTrackStatus())
      {

        for(int a=0;a<mvPersonPositions.size();a++)
        {
          if(mvPersonPositions[a].isAssigned == false)
          {
            tableAssignment temp;
            temp.personNum = a;
            temp.trackNum = trackNum;
            temp.dist = sqrt(dist2((*iTrack)->getPredictedPos(),mvPersonPositions[a].personPosition));
            temp.HPP = true;
            /*Polygon tempPolygon;
            (*iTrack)->getSearchRectangle(tempPolygon);*/
            if(temp.dist<mMaxPersonTrackDistance)
            //if(tempPolygon.isPointInside(mvPersonPositions[a].personPosition))
              unAssignedTrackTable.push_back(temp);
          }
        }

        for(int a=0;a<mvPersonPositionsLowThresh.size();a++)
        {
          if(mvPersonPositionsLowThresh[a].isAssigned == false)
          {
            tableAssignment temp;
            temp.personNum = a;
            temp.trackNum = trackNum;
            temp.dist = sqrt(dist2((*iTrack)->getPredictedPos(),mvPersonPositionsLowThresh[a].personPosition));
            temp.HPP = false;
            //Polygon tempPolygon;
            //(*iTrack)->getSearchRectangle(tempPolygon);
            if(temp.dist<mMaxPersonTrackDistance)
            //if(tempPolygon.isPointInside(mvPersonPositionsLowThresh[a].personPosition))
              unAssignedTrackTable.push_back(temp);
          }
        }
      }
    }
    rowSort(unAssignedTrackTable);

    //Now go down the unAssignedTrackTable and assign the unassigned tracks
    for(int a=0;a<unAssignedTrackTable.size();a++)
    {
      iTrack = mTracks.begin();
      for(int i=0;i<unAssignedTrackTable[a].trackNum;i++,iTrack++);
      if((*iTrack)->getAssignmentVar()==false && unAssignedTrackTable[a].dist<mMaxPersonTrackDistance)
      {

        if(unAssignedTrackTable[a].HPP == true)
        {
          if(mvPersonPositions[unAssignedTrackTable[a].personNum].isAssigned == false)
          {
            mvPersonPositions[unAssignedTrackTable[a].personNum].isAssigned = true;
            (*iTrack)->setPersonPosition(mvPersonPositions[unAssignedTrackTable[a].personNum].personPosition);
            (*iTrack)->setAssignmentVar(true);
            (*iTrack)->setPersonDensityValue(mvPersonPositions[unAssignedTrackTable[a].personNum].personProbability);
            tracksAssigned++;
            //Use something to indicate that a HPP was used
            (*iTrack)->setPersonProbabilityStatus(true);
            (*iTrack)->noUseTrackAge = 0;
          }
        }
        else
        {
          if(mvPersonPositionsLowThresh[unAssignedTrackTable[a].personNum].isAssigned == false)
          {
            mvPersonPositionsLowThresh[unAssignedTrackTable[a].personNum].isAssigned = true;
            (*iTrack)->setPersonPosition(mvPersonPositionsLowThresh[unAssignedTrackTable[a].personNum].personPosition);
            (*iTrack)->setAssignmentVar(true);
            (*iTrack)->setPersonDensityValue(mvPersonPositionsLowThresh[unAssignedTrackTable[a].personNum].personProbability);
            tracksAssigned++;
            //Use something to indicate that a LPP was used
            (*iTrack)->setPersonProbabilityStatus(false);
            (*iTrack)->noUseTrackAge = 0;
          }
        }
      }
    }
  }

  //Now for all the tracks check to see if there is any segmentation nearby and assign it to the highest filtered value nearby
  //Do not assign if the highest value is 0
  for(iTrack=mTracks.begin();iTrack!=mTracks.end();iTrack++)
  {
    if(!(*iTrack)->getAssignmentVar() && !(*iTrack)->getTrackStatus())
    {
      //Get the predicted track position
      Trajectory::Nodes& nodes = (*iTrack)->getTrajectory()->getNodes();
      
      int xPos = (int)(*iTrack)->getPredictedPos().x;
      int yPos = (int)(*iTrack)->getPredictedPos().y;
      if(nodes.size()>1)
      {
        Trajectory::Nodes::iterator iPrevNode = nodes.end();
      //Trajectory::Nodes::iterator iNode = iPrevNode--;

      iPrevNode--;
      xPos= (int)(*iPrevNode)->location.x;
      yPos = (int)(*iPrevNode)->location.y;
      iPrevNode--;
      xPos = 2*xPos - (int)(*iPrevNode)->location.x;
      yPos = 2*yPos - (int)(*iPrevNode)->location.y;

      //PVMSG("x=%d,y=%d\n",xPos,yPos);
      }
            

      //int xPos = (int)(*iTrack)->getPos().x;
      //int yPos = (int)(*iTrack)->getPos().y;
      int leftLimit = xPos-2 >=0 ? xPos-2 : 0;
      int rightLimit = xPos+2 < filterImg.width() ? xPos+2 : filterImg.width()-1;
      int topLimit = yPos-2 >=0 ? yPos-2 : 0;
      int bottomLimit = yPos+2 < filterImg.height() ? yPos+2 : filterImg.height()-1;

      int maxVal = -1;
      int maxX = xPos,maxY = yPos;

      for(int a=leftLimit;a<rightLimit+1;a++)
      {
        for(int b=topLimit;b<bottomLimit+1;b++)
        {
          if(filterImg(a,b)>maxVal)
          {
            maxX = a;
            maxY = b;
            maxVal = filterImg(a,b);
          }
        }
      }

      if(maxVal>0)
      {

        (*iTrack)->setPersonPosition(Vector2f(maxX,maxY));
        (*iTrack)->setAssignmentVar(true);
        (*iTrack)->setPersonDensityValue(maxVal);
        tracksAssigned++;
        //Use something to indicate that a LPP was used
        (*iTrack)->setPersonProbabilityStatus(false);
        (*iTrack)->noUseTrackAge = 0;
      }

    }
  }
  
  
    for(iTrack=mTracks.begin();iTrack!=mTracks.end();iTrack++)
    {
      if(!(*iTrack)->getAssignmentVar())
      {
        /*if((*iTrack)->getTrackStatus())
          (*iTrack)->kill();
        else*/
        {
          (*iTrack)->noUseTrackAge++;
          if((*iTrack)->noUseTrackAge > 5)
            (*iTrack)->kill();
        }
        //tracksKilled++;

      }
    }
    //PVMSG("Tracks killed = %d\n",tracksKilled);
  
  }

  void
    PersonShapeTrackerMod::personTrackAssignment(Image8& filterImg)
  {
    bool isDone = false;
    bool useLowPP = false;
    TracksShapePS::iterator iTrack;
    int tracksAssigned = 0;
    int tracksKilled = 0;

    {


      //First set up the table
      std::vector< std::vector< tableAssignment > > comparisonTable;

      //For each unassigned confirmed track make a column entry for the table
      int trackNum = 0;
      for(iTrack = mTracks.begin();iTrack != mTracks.end();iTrack++,trackNum++)
      {
        std::vector< tableAssignment > trackRow;
        if((*iTrack)->getTrackStatus() == false)
        {

          for(int a=0;a<mvPersonPositions.size();a++)
          {
            if(mvPersonPositions[a].isAssigned == false)
            {
              tableAssignment temp;
              temp.personNum = a;
              temp.trackNum = trackNum;
              temp.dist = sqrt(dist2((*iTrack)->getPos(),mvPersonPositions[a].personPosition));
              temp.HPP = true;
              if(temp.dist<mMaxPersonTrackDistance)
                trackRow.push_back(temp);
            }
          }
         
          rowSort(trackRow);
         
          comparisonTable.push_back(trackRow);
        }
      }

      //Table is made

      //Now check for non-conflict track person assignment
      for(int a=0;a<comparisonTable.size();a++)
      {
        bool isConflicting = false;
        for(int b=0;b<comparisonTable.size();b++)
        {
          if(a!=b && comparisonTable[a].size()>0 && comparisonTable[b].size()>0)
          {
            if(comparisonTable[a][0].personNum == comparisonTable[b][0].personNum && comparisonTable[a][0].HPP == comparisonTable[b][0].HPP)
            {
              isConflicting = true;
              break;
            }
          }
        }
        if(comparisonTable[a].size()>0)
          if(!isConflicting && comparisonTable[a][0].dist<mMaxPersonTrackDistance)
          {
            //Do the track person assignment
            //Get the appropriate track
            iTrack = mTracks.begin();
            for(int i=0;i<comparisonTable[a][0].trackNum;i++,iTrack++);

            //PVMSG("\nAssigned track %d at (%f,%f) to person %d at (%f,%f)\n",comparisonTable[a][0].trackNum,(*iTrack)->getPos().x,(*iTrack)->getPos().y,comparisonTable[a][0].personNum,mvPersonPositions[comparisonTable[a][0].personNum].personPosition.x,mvPersonPositions[comparisonTable[a][0].personNum].personPosition.y);
            (*iTrack)->setAssignmentVar(true);
            mvPersonPositions[comparisonTable[a][0].personNum].isAssigned = true;
            (*iTrack)->setPersonPosition(mvPersonPositions[comparisonTable[a][0].personNum].personPosition);
            (*iTrack)->setPersonDensityValue(mvPersonPositions[comparisonTable[a][0].personNum].personProbability);
            tracksAssigned++;
            //Use something to indicate that a HPP was used
            (*iTrack)->setPersonProbabilityStatus(true);
            (*iTrack)->noUseTrackAge = 0;
          }

      }
    }

    //Now do it with the lower threshold
   {


      //First set up the table
      std::vector< std::vector< tableAssignment > > comparisonTable;

      //For each unassigned confirmed track make a column entry for the table
      int trackNum = 0;
      for(iTrack = mTracks.begin();iTrack != mTracks.end();iTrack++,trackNum++)
      {
        std::vector< tableAssignment > trackRow;
        if((*iTrack)->getTrackStatus() == false && !(*iTrack)->getAssignmentVar())
        {

          for(int a=0;a<mvPersonPositions.size();a++)
          {
            if(mvPersonPositions[a].isAssigned == false)
            {
              tableAssignment temp;
              temp.personNum = a;
              temp.trackNum = trackNum;
              temp.dist = sqrt(dist2((*iTrack)->getPos(),mvPersonPositions[a].personPosition));
              temp.HPP = true;
              if(temp.dist<mMaxPersonTrackDistance)
                trackRow.push_back(temp);
            }
          }

          for(int a=0;a<mvPersonPositionsLowThresh.size();a++)
          {
            if(mvPersonPositionsLowThresh[a].isAssigned == false)
            {
              tableAssignment temp;
              temp.personNum = a;
              temp.trackNum = trackNum;
              temp.dist = sqrt(dist2((*iTrack)->getPos(),mvPersonPositionsLowThresh[a].personPosition));
              temp.HPP = false;
              if(temp.dist<mMaxPersonTrackDistance)
                trackRow.push_back(temp);
            }
          }
          //Sort the trackRow
          rowSort(trackRow);
          comparisonTable.push_back(trackRow);
        }
      }

    
      //Table is made
      //Now check for non-conflict track person assignment
      for(int a=0;a<comparisonTable.size();a++)
      {
        bool isConflicting = false;
        for(int b=0;b<comparisonTable.size();b++)
        {
          if(a!=b && comparisonTable[a].size()>0 && comparisonTable[b].size()>0)
          {
            if(comparisonTable[a][0].personNum == comparisonTable[b][0].personNum  && comparisonTable[a][0].HPP == comparisonTable[b][0].HPP)
            {
              isConflicting = true;
              break;
            }
          }
        }
        if(comparisonTable[a].size()>0)
          if(!isConflicting && comparisonTable[a][0].dist<mMaxPersonTrackDistance)
          {
            //Do the track person assignment
            //Get the appropriate track
            iTrack = mTracks.begin();
            for(int i=0;i<comparisonTable[a][0].trackNum;i++,iTrack++);

            (*iTrack)->setAssignmentVar(true);
            if(comparisonTable[a][0].HPP == true)
            {
              //PVMSG("\nAssigned track %d at (%f,%f) to person %d at (%f,%f)\n",comparisonTable[a][0].trackNum,(*iTrack)->getPos().x,(*iTrack)->getPos().y,comparisonTable[a][0].personNum,mvPersonPositions[comparisonTable[a][0].personNum].personPosition.x,mvPersonPositions[comparisonTable[a][0].personNum].personPosition.y);
              mvPersonPositions[comparisonTable[a][0].personNum].isAssigned = true;
              (*iTrack)->setPersonPosition(mvPersonPositions[comparisonTable[a][0].personNum].personPosition);
              (*iTrack)->setAssignmentVar(true);
              (*iTrack)->setPersonDensityValue(mvPersonPositions[comparisonTable[a][0].personNum].personProbability);
              tracksAssigned++;
              //Use something to indicate that a HPP was used
              (*iTrack)->setPersonProbabilityStatus(true);
              (*iTrack)->noUseTrackAge = 0;
            }
            else
            {
              //PVMSG("\nAssigned track %d at (%f,%f) to person %d at (%f,%f)\n",comparisonTable[a][0].trackNum,(*iTrack)->getPos().x,(*iTrack)->getPos().y,comparisonTable[a][0].personNum,mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition.x,mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition.y);
              mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].isAssigned = true;
              (*iTrack)->setPersonPosition(mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition);
              (*iTrack)->setAssignmentVar(true);
              (*iTrack)->setPersonDensityValue(mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personProbability);
              tracksAssigned++;
              //Use something to indicate that a LPP was used
              (*iTrack)->setPersonProbabilityStatus(false);
              (*iTrack)->noUseTrackAge = 0;
            }
          }
      }
    }
    //For occluded persons
    {


      //First set up the table
      std::vector< std::vector< tableAssignment > > comparisonTable;

      //For each unassigned confirmed track make a column entry for the table
      int trackNum = 0;
      for(iTrack = mTracks.begin();iTrack != mTracks.end();iTrack++,trackNum++)
      {
        std::vector< tableAssignment > trackRow;
        if((*iTrack)->getTrackStatus() == false && !(*iTrack)->getAssignmentVar())
        {

          for(int a=0;a<mvPersonPositions.size();a++)
          {
            if(mvPersonPositions[a].isAssigned == false)
            {
              tableAssignment temp;
              temp.personNum = a;
              temp.trackNum = trackNum;
              temp.dist = sqrt(dist2((*iTrack)->getPos(),mvPersonPositions[a].personPosition));
              temp.HPP = true;
              temp.occluded = false;
              if(temp.dist<mMaxPersonTrackDistance)
                trackRow.push_back(temp);
            }
          }

          for(int a=0;a<mvPersonPositionsLowThresh.size();a++)
          {
            if(mvPersonPositionsLowThresh[a].isAssigned == false)
            {
              tableAssignment temp;
              temp.personNum = a;
              temp.trackNum = trackNum;
              temp.dist = sqrt(dist2((*iTrack)->getPos(),mvPersonPositionsLowThresh[a].personPosition));
              temp.HPP = false;
              temp.occluded = false;
              if(temp.dist<mMaxPersonTrackDistance)
                trackRow.push_back(temp);
            }
          }

          for(int a=0;a<mvOccludedPersonPositions.size();a++)
          {
            if(mvOccludedPersonPositions[a].isAssigned == false)
            {
              tableAssignment temp;
              temp.personNum = a;
              temp.trackNum = trackNum;
              temp.dist = sqrt(dist2((*iTrack)->getPos(),mvOccludedPersonPositions[a].personPosition));
              temp.HPP = false;
              temp.occluded = true;
              if(temp.dist<mMaxPersonTrackDistance)
                trackRow.push_back(temp);
            }
          }
          //Sort the trackRow
          rowSort(trackRow);
          comparisonTable.push_back(trackRow);
        }
      }

      //Table is made
      //Now check for non-conflict track person assignment
      for(int a=0;a<comparisonTable.size();a++)
      {
        bool isConflicting = false;
        for(int b=0;b<comparisonTable.size();b++)
        {
          if(a!=b && comparisonTable[a].size()>0 && comparisonTable[b].size()>0)
          {
            if(comparisonTable[a][0].personNum == comparisonTable[b][0].personNum  && comparisonTable[a][0].HPP == comparisonTable[b][0].HPP && comparisonTable[a][0].occluded == comparisonTable[b][0].occluded)
            {
              isConflicting = true;
              break;
            }
          }
        }
        if(comparisonTable[a].size()>0)
          if(!isConflicting && comparisonTable[a][0].dist<mMaxPersonTrackDistance)
          {
            //Do the track person assignment
            //Get the appropriate track
            iTrack = mTracks.begin();
            for(int i=0;i<comparisonTable[a][0].trackNum;i++,iTrack++);

            (*iTrack)->setAssignmentVar(true);
            if(comparisonTable[a][0].HPP == true)
            {
              //PVMSG("\nAssigned track %d at (%f,%f) to person %d at (%f,%f)\n",comparisonTable[a][0].trackNum,(*iTrack)->getPos().x,(*iTrack)->getPos().y,comparisonTable[a][0].personNum,mvPersonPositions[comparisonTable[a][0].personNum].personPosition.x,mvPersonPositions[comparisonTable[a][0].personNum].personPosition.y);
              mvPersonPositions[comparisonTable[a][0].personNum].isAssigned = true;
              (*iTrack)->setPersonPosition(mvPersonPositions[comparisonTable[a][0].personNum].personPosition);
              (*iTrack)->setAssignmentVar(true);
              (*iTrack)->setPersonDensityValue(mvPersonPositions[comparisonTable[a][0].personNum].personProbability);
              tracksAssigned++;
              //Use something to indicate that a HPP was used
              (*iTrack)->setPersonProbabilityStatus(true);
              (*iTrack)->noUseTrackAge = 0;
            }
            else
            {
              if(comparisonTable[a][0].occluded == true)
              {
                //PVMSG("\nAssigned track %d at (%f,%f) to person %d at (%f,%f)\n",comparisonTable[a][0].trackNum,(*iTrack)->getPos().x,(*iTrack)->getPos().y,comparisonTable[a][0].personNum,mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition.x,mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition.y);
                mvOccludedPersonPositions[comparisonTable[a][0].personNum].isAssigned = true;
                (*iTrack)->setPersonPosition(mvOccludedPersonPositions[comparisonTable[a][0].personNum].personPosition);
                (*iTrack)->setAssignmentVar(true);
                (*iTrack)->setPersonDensityValue(mvOccludedPersonPositions[comparisonTable[a][0].personNum].personProbability);
                tracksAssigned++;
                //Use something to indicate that a LPP was used
                (*iTrack)->setPersonProbabilityStatus(false);
                (*iTrack)->noUseTrackAge = 0;
              }
              else
              {


                //PVMSG("\nAssigned track %d at (%f,%f) to person %d at (%f,%f)\n",comparisonTable[a][0].trackNum,(*iTrack)->getPos().x,(*iTrack)->getPos().y,comparisonTable[a][0].personNum,mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition.x,mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition.y);
                mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].isAssigned = true;
                (*iTrack)->setPersonPosition(mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition);
                (*iTrack)->setAssignmentVar(true);
                (*iTrack)->setPersonDensityValue(mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personProbability);
                tracksAssigned++;
                //Use something to indicate that a LPP was used
                (*iTrack)->setPersonProbabilityStatus(false);
                (*iTrack)->noUseTrackAge = 0;
              }
            }
          }
      }
    }
    

    {


      //Now handle those confirmed tracks that are still unassigned
      //First create a large list with all the unassigned tracks and persons (including low threshold) arranged by distance
      //Create a single vector containing all the unassigned tracks
      std::vector< tableAssignment > unAssignedTrackTable;
      int trackNum=0;
      for(iTrack=mTracks.begin();iTrack!=mTracks.end();iTrack++,trackNum++)
      {
        if(!(*iTrack)->getAssignmentVar() && !(*iTrack)->getTrackStatus())
        {

          for(int a=0;a<mvPersonPositions.size();a++)
          {
            if(mvPersonPositions[a].isAssigned == false)
            {
              tableAssignment temp;
              temp.personNum = a;
              temp.trackNum = trackNum;
              temp.dist = sqrt(dist2((*iTrack)->getPos(),mvPersonPositions[a].personPosition));
              temp.HPP = true;
              if(temp.dist<mMaxPersonTrackDistance)
                unAssignedTrackTable.push_back(temp);
            }
          }

          for(int a=0;a<mvPersonPositionsLowThresh.size();a++)
          {
            if(mvPersonPositionsLowThresh[a].isAssigned == false)
            {
              tableAssignment temp;
              temp.personNum = a;
              temp.trackNum = trackNum;
              temp.dist = sqrt(dist2((*iTrack)->getPos(),mvPersonPositionsLowThresh[a].personPosition));
              temp.HPP = false;
              if(temp.dist<mMaxPersonTrackDistance)
                unAssignedTrackTable.push_back(temp);
            }
          }
        }
      }
      rowSort(unAssignedTrackTable);

      //Now go down the unAssignedTrackTable and assign the unassigned tracks
      for(int a=0;a<unAssignedTrackTable.size();a++)
      {
        iTrack = mTracks.begin();
        for(int i=0;i<unAssignedTrackTable[a].trackNum;i++,iTrack++);
        if((*iTrack)->getAssignmentVar()==false && unAssignedTrackTable[a].dist<mMaxPersonTrackDistance)
        {

          if(unAssignedTrackTable[a].HPP == true)
          {
            if(mvPersonPositions[unAssignedTrackTable[a].personNum].isAssigned == false)
            {
              mvPersonPositions[unAssignedTrackTable[a].personNum].isAssigned = true;
              (*iTrack)->setPersonPosition(mvPersonPositions[unAssignedTrackTable[a].personNum].personPosition);
              (*iTrack)->setAssignmentVar(true);
              (*iTrack)->setPersonDensityValue(mvPersonPositions[unAssignedTrackTable[a].personNum].personProbability);
              tracksAssigned++;
              //Use something to indicate that a HPP was used
              (*iTrack)->setPersonProbabilityStatus(true);
              (*iTrack)->noUseTrackAge = 0;
            }
          }
          else
          {
            if(mvPersonPositionsLowThresh[unAssignedTrackTable[a].personNum].isAssigned == false)
            {
              mvPersonPositionsLowThresh[unAssignedTrackTable[a].personNum].isAssigned = true;
              (*iTrack)->setPersonPosition(mvPersonPositionsLowThresh[unAssignedTrackTable[a].personNum].personPosition);
              (*iTrack)->setAssignmentVar(true);
              (*iTrack)->setPersonDensityValue(mvPersonPositionsLowThresh[unAssignedTrackTable[a].personNum].personProbability);
              tracksAssigned++;
              //Use something to indicate that a LPP was used
              (*iTrack)->setPersonProbabilityStatus(false);
              (*iTrack)->noUseTrackAge = 0;
            }
          }
        }
      }
    }
  

    //Now do the assignment for probationary tracks

    {


      //First set up the table
      std::vector< std::vector< tableAssignment > > comparisonTable;

      //For each unassigned confirmed track make a column entry for the table
      int trackNum = 0;
      for(iTrack = mTracks.begin();iTrack != mTracks.end();iTrack++,trackNum++)
      {
        std::vector< tableAssignment > trackRow;
        if((*iTrack)->getTrackStatus() == true)
        {

          for(int a=0;a<mvPersonPositions.size();a++)
          {
            if(mvPersonPositions[a].isAssigned == false)
            {
              tableAssignment temp;
              temp.personNum = a;
              temp.trackNum = trackNum;
              temp.dist = sqrt(dist2((*iTrack)->getPos(),mvPersonPositions[a].personPosition));
              temp.HPP = true;
              if(temp.dist<mMaxPersonTrackDistance)
                trackRow.push_back(temp);
            }
          }
       
          rowSort(trackRow);
       
          comparisonTable.push_back(trackRow);
        }
      }

      //Table is made

      
      //Now check for non-conflict track person assignment
      for(int a=0;a<comparisonTable.size();a++)
      {
        bool isConflicting = false;
        for(int b=0;b<comparisonTable.size();b++)
        {
          if(a!=b && comparisonTable[a].size()>0 && comparisonTable[b].size()>0)
          {
            if(comparisonTable[a][0].personNum == comparisonTable[b][0].personNum && comparisonTable[a][0].HPP == comparisonTable[b][0].HPP)
            {
              isConflicting = true;
              break;
            }
          }
        }
        if(comparisonTable[a].size()>0)
          if(!isConflicting && comparisonTable[a][0].dist<mMaxPersonTrackDistance)
          {
            //Do the track person assignment
            //Get the appropriate track
            iTrack = mTracks.begin();
            for(int i=0;i<comparisonTable[a][0].trackNum;i++,iTrack++);

            //PVMSG("\nAssigned track %d at (%f,%f) to person %d at (%f,%f)\n",comparisonTable[a][0].trackNum,(*iTrack)->getPos().x,(*iTrack)->getPos().y,comparisonTable[a][0].personNum,mvPersonPositions[comparisonTable[a][0].personNum].personPosition.x,mvPersonPositions[comparisonTable[a][0].personNum].personPosition.y);
            (*iTrack)->setAssignmentVar(true);
            mvPersonPositions[comparisonTable[a][0].personNum].isAssigned = true;
            (*iTrack)->setPersonPosition(mvPersonPositions[comparisonTable[a][0].personNum].personPosition);
            (*iTrack)->setPersonDensityValue(mvPersonPositions[comparisonTable[a][0].personNum].personProbability);
            tracksAssigned++;
            //Use something to indicate that a HPP was used
            (*iTrack)->setPersonProbabilityStatus(true);
            (*iTrack)->noUseTrackAge = 0;
          }

      }
    }

    //Now do it with the lower threshold
    
    {


      //First set up the table
      std::vector< std::vector< tableAssignment > > comparisonTable;

      //For each unassigned confirmed track make a column entry for the table
      int trackNum = 0;
      for(iTrack = mTracks.begin();iTrack != mTracks.end();iTrack++,trackNum++)
      {
        std::vector< tableAssignment > trackRow;
        if((*iTrack)->getTrackStatus() == true && !(*iTrack)->getAssignmentVar())
        {

          for(int a=0;a<mvPersonPositions.size();a++)
          {
            if(mvPersonPositions[a].isAssigned == false)
            {
              tableAssignment temp;
              temp.personNum = a;
              temp.trackNum = trackNum;
              temp.dist = sqrt(dist2((*iTrack)->getPos(),mvPersonPositions[a].personPosition));
              temp.HPP = true;
              if(temp.dist<mMaxPersonTrackDistance)
                trackRow.push_back(temp);
            }
          }

          for(int a=0;a<mvPersonPositionsLowThresh.size();a++)
          {
            if(mvPersonPositionsLowThresh[a].isAssigned == false)
            {
              tableAssignment temp;
              temp.personNum = a;
              temp.trackNum = trackNum;
              temp.dist = sqrt(dist2((*iTrack)->getPos(),mvPersonPositionsLowThresh[a].personPosition));
              temp.HPP = false;
              if(temp.dist<mMaxPersonTrackDistance)
                trackRow.push_back(temp);
            }
          }
          //Sort the trackRow
          rowSort(trackRow);
          comparisonTable.push_back(trackRow);
        }
      }
     
      //Table is made
      //Now check for non-conflict track person assignment
      for(int a=0;a<comparisonTable.size();a++)
      {
        bool isConflicting = false;
        for(int b=0;b<comparisonTable.size();b++)
        {
          if(a!=b && comparisonTable[a].size()>0 && comparisonTable[b].size()>0)
          {
            if(comparisonTable[a][0].personNum == comparisonTable[b][0].personNum  && comparisonTable[a][0].HPP == comparisonTable[b][0].HPP)
            {
              isConflicting = true;
              break;
            }
          }
        }
        if(comparisonTable[a].size()>0)
          if(!isConflicting && comparisonTable[a][0].dist<mMaxPersonTrackDistance)
          {
            //Do the track person assignment
            //Get the appropriate track
            iTrack = mTracks.begin();
            for(int i=0;i<comparisonTable[a][0].trackNum;i++,iTrack++);

            (*iTrack)->setAssignmentVar(true);
            if(comparisonTable[a][0].HPP == true)
            {
              //PVMSG("\nAssigned track %d at (%f,%f) to person %d at (%f,%f)\n",comparisonTable[a][0].trackNum,(*iTrack)->getPos().x,(*iTrack)->getPos().y,comparisonTable[a][0].personNum,mvPersonPositions[comparisonTable[a][0].personNum].personPosition.x,mvPersonPositions[comparisonTable[a][0].personNum].personPosition.y);
              mvPersonPositions[comparisonTable[a][0].personNum].isAssigned = true;
              (*iTrack)->setPersonPosition(mvPersonPositions[comparisonTable[a][0].personNum].personPosition);
              (*iTrack)->setAssignmentVar(true);
              (*iTrack)->setPersonDensityValue(mvPersonPositions[comparisonTable[a][0].personNum].personProbability);
              tracksAssigned++;
              //Use something to indicate that a HPP was used
              (*iTrack)->setPersonProbabilityStatus(true);
              (*iTrack)->noUseTrackAge = 0;
            }
            else
            {
              //PVMSG("\nAssigned track %d at (%f,%f) to person %d at (%f,%f)\n",comparisonTable[a][0].trackNum,(*iTrack)->getPos().x,(*iTrack)->getPos().y,comparisonTable[a][0].personNum,mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition.x,mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition.y);
              mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].isAssigned = true;
              (*iTrack)->setPersonPosition(mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition);
              (*iTrack)->setAssignmentVar(true);
              (*iTrack)->setPersonDensityValue(mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personProbability);
              tracksAssigned++;
              //Use something to indicate that a LPP was used
              (*iTrack)->setPersonProbabilityStatus(false);
              (*iTrack)->noUseTrackAge = 0;
            }
          }
      }
    }


    //For occluded persons
    {


      //First set up the table
      std::vector< std::vector< tableAssignment > > comparisonTable;

      //For each unassigned confirmed track make a column entry for the table
      int trackNum = 0;
      for(iTrack = mTracks.begin();iTrack != mTracks.end();iTrack++,trackNum++)
      {
        std::vector< tableAssignment > trackRow;
        if((*iTrack)->getTrackStatus() == true && !(*iTrack)->getAssignmentVar())
        {

          for(int a=0;a<mvPersonPositions.size();a++)
          {
            if(mvPersonPositions[a].isAssigned == false)
            {
              tableAssignment temp;
              temp.personNum = a;
              temp.trackNum = trackNum;
              temp.dist = sqrt(dist2((*iTrack)->getPos(),mvPersonPositions[a].personPosition));
              temp.HPP = true;
              temp.occluded = false;
              if(temp.dist<mMaxPersonTrackDistance)
                trackRow.push_back(temp);
            }
          }

          for(int a=0;a<mvPersonPositionsLowThresh.size();a++)
          {
            if(mvPersonPositionsLowThresh[a].isAssigned == false)
            {
              tableAssignment temp;
              temp.personNum = a;
              temp.trackNum = trackNum;
              temp.dist = sqrt(dist2((*iTrack)->getPos(),mvPersonPositionsLowThresh[a].personPosition));
              temp.HPP = false;
              temp.occluded = false;
              if(temp.dist<mMaxPersonTrackDistance)
                trackRow.push_back(temp);
            }
          }

          for(int a=0;a<mvOccludedPersonPositions.size();a++)
          {
            if(mvOccludedPersonPositions[a].isAssigned == false)
            {
              tableAssignment temp;
              temp.personNum = a;
              temp.trackNum = trackNum;
              temp.dist = sqrt(dist2((*iTrack)->getPos(),mvOccludedPersonPositions[a].personPosition));
              temp.HPP = false;
              temp.occluded = true;
              if(temp.dist<mMaxPersonTrackDistance)
                trackRow.push_back(temp);
            }
          }
          //Sort the trackRow
          rowSort(trackRow);
          comparisonTable.push_back(trackRow);
        }
      }

   
      //Table is made
      //Now check for non-conflict track person assignment
      for(int a=0;a<comparisonTable.size();a++)
      {
        bool isConflicting = false;
        for(int b=0;b<comparisonTable.size();b++)
        {
          if(a!=b && comparisonTable[a].size()>0 && comparisonTable[b].size()>0)
          {
            if(comparisonTable[a][0].personNum == comparisonTable[b][0].personNum  && comparisonTable[a][0].HPP == comparisonTable[b][0].HPP && comparisonTable[a][0].occluded == comparisonTable[b][0].occluded)
            {
              isConflicting = true;
              break;
            }
          }
        }
        if(comparisonTable[a].size()>0)
          if(!isConflicting && comparisonTable[a][0].dist<mMaxPersonTrackDistance)
          {
            //Do the track person assignment
            //Get the appropriate track
            iTrack = mTracks.begin();
            for(int i=0;i<comparisonTable[a][0].trackNum;i++,iTrack++);

            (*iTrack)->setAssignmentVar(true);
            if(comparisonTable[a][0].HPP == true)
            {
              //PVMSG("\nAssigned track %d at (%f,%f) to person %d at (%f,%f)\n",comparisonTable[a][0].trackNum,(*iTrack)->getPos().x,(*iTrack)->getPos().y,comparisonTable[a][0].personNum,mvPersonPositions[comparisonTable[a][0].personNum].personPosition.x,mvPersonPositions[comparisonTable[a][0].personNum].personPosition.y);
              mvPersonPositions[comparisonTable[a][0].personNum].isAssigned = true;
              (*iTrack)->setPersonPosition(mvPersonPositions[comparisonTable[a][0].personNum].personPosition);
              (*iTrack)->setAssignmentVar(true);
              (*iTrack)->setPersonDensityValue(mvPersonPositions[comparisonTable[a][0].personNum].personProbability);
              tracksAssigned++;
              //Use something to indicate that a HPP was used
              (*iTrack)->setPersonProbabilityStatus(true);
              (*iTrack)->noUseTrackAge = 0;
            }
            else
            {
              if(comparisonTable[a][0].occluded == true)
              {
                //PVMSG("\nAssigned track %d at (%f,%f) to person %d at (%f,%f)\n",comparisonTable[a][0].trackNum,(*iTrack)->getPos().x,(*iTrack)->getPos().y,comparisonTable[a][0].personNum,mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition.x,mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition.y);
                mvOccludedPersonPositions[comparisonTable[a][0].personNum].isAssigned = true;
                (*iTrack)->setPersonPosition(mvOccludedPersonPositions[comparisonTable[a][0].personNum].personPosition);
                (*iTrack)->setAssignmentVar(true);
                (*iTrack)->setPersonDensityValue(mvOccludedPersonPositions[comparisonTable[a][0].personNum].personProbability);
                tracksAssigned++;
                //Use something to indicate that a LPP was used
                (*iTrack)->setPersonProbabilityStatus(false);
                (*iTrack)->noUseTrackAge = 0;
              }
              else
              {


                //PVMSG("\nAssigned track %d at (%f,%f) to person %d at (%f,%f)\n",comparisonTable[a][0].trackNum,(*iTrack)->getPos().x,(*iTrack)->getPos().y,comparisonTable[a][0].personNum,mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition.x,mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition.y);
                mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].isAssigned = true;
                (*iTrack)->setPersonPosition(mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personPosition);
                (*iTrack)->setAssignmentVar(true);
                (*iTrack)->setPersonDensityValue(mvPersonPositionsLowThresh[comparisonTable[a][0].personNum].personProbability);
                tracksAssigned++;
                //Use something to indicate that a LPP was used
                (*iTrack)->setPersonProbabilityStatus(false);
                (*iTrack)->noUseTrackAge = 0;
              }
            }
          }
      }
    }

    {


      //Now handle those confirmed tracks that are still unassigned
      //First create a large list with all the unassigned tracks and persons (including low threshold) arranged by distance
      //Create a single vector containing all the unassigned tracks
      std::vector< tableAssignment > unAssignedTrackTable;
      int trackNum=0;
      for(iTrack=mTracks.begin();iTrack!=mTracks.end();iTrack++,trackNum++)
      {
        if(!(*iTrack)->getAssignmentVar() && (*iTrack)->getTrackStatus())
        {

          for(int a=0;a<mvPersonPositions.size();a++)
          {
            if(mvPersonPositions[a].isAssigned == false)
            {
              tableAssignment temp;
              temp.personNum = a;
              temp.trackNum = trackNum;
              temp.dist = sqrt(dist2((*iTrack)->getPos(),mvPersonPositions[a].personPosition));
              temp.HPP = true;
              if(temp.dist<mMaxPersonTrackDistance)
                unAssignedTrackTable.push_back(temp);
            }
          }

          for(int a=0;a<mvPersonPositionsLowThresh.size();a++)
          {
            if(mvPersonPositionsLowThresh[a].isAssigned == false)
            {
              tableAssignment temp;
              temp.personNum = a;
              temp.trackNum = trackNum;
              temp.dist = sqrt(dist2((*iTrack)->getPos(),mvPersonPositionsLowThresh[a].personPosition));
              temp.HPP = false;
              if(temp.dist<mMaxPersonTrackDistance)
                unAssignedTrackTable.push_back(temp);
            }
          }
        }
      }
      rowSort(unAssignedTrackTable);

      //Now go down the unAssignedTrackTable and assign the unassigned tracks
      for(int a=0;a<unAssignedTrackTable.size();a++)
      {
        iTrack = mTracks.begin();
        for(int i=0;i<unAssignedTrackTable[a].trackNum;i++,iTrack++);
        if((*iTrack)->getAssignmentVar()==false && unAssignedTrackTable[a].dist<mMaxPersonTrackDistance)
        {

          if(unAssignedTrackTable[a].HPP == true)
          {
            if(mvPersonPositions[unAssignedTrackTable[a].personNum].isAssigned == false)
            {
              mvPersonPositions[unAssignedTrackTable[a].personNum].isAssigned = true;
              (*iTrack)->setPersonPosition(mvPersonPositions[unAssignedTrackTable[a].personNum].personPosition);
              (*iTrack)->setAssignmentVar(true);
              (*iTrack)->setPersonDensityValue(mvPersonPositions[unAssignedTrackTable[a].personNum].personProbability);
              tracksAssigned++;
              //Use something to indicate that a HPP was used
              (*iTrack)->setPersonProbabilityStatus(true);
              (*iTrack)->noUseTrackAge = 0;
            }
          }
          else
          {
            if(mvPersonPositionsLowThresh[unAssignedTrackTable[a].personNum].isAssigned == false)
            {
              mvPersonPositionsLowThresh[unAssignedTrackTable[a].personNum].isAssigned = true;
              (*iTrack)->setPersonPosition(mvPersonPositionsLowThresh[unAssignedTrackTable[a].personNum].personPosition);
              (*iTrack)->setAssignmentVar(true);
              (*iTrack)->setPersonDensityValue(mvPersonPositionsLowThresh[unAssignedTrackTable[a].personNum].personProbability);
              tracksAssigned++;
              //Use something to indicate that a LPP was used
              (*iTrack)->setPersonProbabilityStatus(false);
              (*iTrack)->noUseTrackAge = 0;
            }
          }
        }
      }
    }

    //Now for all the tracks check to see if there is any segmentation nearby and assign it to the highest filtered value nearby
    //Do not assign if the highest value is 0
    for(iTrack=mTracks.begin();iTrack!=mTracks.end();iTrack++)
    {
      if(!(*iTrack)->getAssignmentVar() && !(*iTrack)->getTrackStatus())
      {
        //Get the predicted track position
        Trajectory::Nodes& nodes = (*iTrack)->getTrajectory()->getNodes();

        int xPos = (int)(*iTrack)->getPos().x;
        int yPos = (int)(*iTrack)->getPos().y;
        if(nodes.size()>1)
        {
          Trajectory::Nodes::iterator iPrevNode = nodes.end();
          //Trajectory::Nodes::iterator iNode = iPrevNode--;

          iPrevNode--;
          xPos= (int)(*iPrevNode)->location.x;
          yPos = (int)(*iPrevNode)->location.y;
          iPrevNode--;
          xPos = 2*xPos - (int)(*iPrevNode)->location.x;
          yPos = 2*yPos - (int)(*iPrevNode)->location.y;

          //PVMSG("x=%d,y=%d\n",xPos,yPos);
        }


        //int xPos = (int)(*iTrack)->getPos().x;
        //int yPos = (int)(*iTrack)->getPos().y;
        int leftLimit = xPos-2 >=0 ? xPos-2 : 0;
        int rightLimit = xPos+2 < filterImg.width() ? xPos+2 : filterImg.width()-1;
        int topLimit = yPos-2 >=0 ? yPos-2 : 0;
        int bottomLimit = yPos+2 < filterImg.height() ? yPos+2 : filterImg.height()-1;

        int maxVal = -1;
        int maxX = xPos,maxY = yPos;

        for(int a=leftLimit;a<rightLimit+1;a++)
        {
          for(int b=topLimit;b<bottomLimit+1;b++)
          {
            if(filterImg(a,b)>maxVal)
            {
              maxX = a;
              maxY = b;
              maxVal = filterImg(a,b);
            }
          }
        }

        if(maxVal>0)
        {

          (*iTrack)->setPersonPosition(Vector2f(maxX,maxY));
          (*iTrack)->setAssignmentVar(true);
          (*iTrack)->setPersonDensityValue(maxVal);
          tracksAssigned++;
          //Use something to indicate that a LPP was used
          (*iTrack)->setPersonProbabilityStatus(false);
          (*iTrack)->noUseTrackAge = 0;
        }

      }
    }


    for(iTrack=mTracks.begin();iTrack!=mTracks.end();iTrack++)
    {
      if(!(*iTrack)->getAssignmentVar())
      {
        /*if((*iTrack)->getTrackStatus())
        (*iTrack)->kill();
        else*/
        {
          (*iTrack)->noUseTrackAge++;
          if((*iTrack)->noUseTrackAge > 5)
            (*iTrack)->kill();
        }
        //tracksKilled++;

      }
    }
    //PVMSG("Tracks killed = %d\n",tracksKilled);

  }


void
  PersonShapeTrackerMod::rowSort(std::vector<tableAssignment>& row)
{
  if(row.size()>1)
  {
    for(int i=0;i<row.size();i++)
    {
      for(int j=i+1;j<row.size();j++)
      {
        tableAssignment temp1=row[i];
        tableAssignment temp2=row[j];
        if(temp1.dist>temp2.dist)
        {
          row[i] = temp2;
          row[j] = temp1;
        }
      }
    }
  }
}

void
PersonShapeTrackerMod::updateCartTracks()
{
  TracksShapePS::iterator iTrack;
  for(iTrack=mCartTracks.begin();iTrack!=mCartTracks.end();iTrack++)
  {
    //PVMSG("Check x=%f y=%f\n",(*iTrack)->getPersonPosition().x,(*iTrack)->getPersonPosition().y);
    (*iTrack)->update(mpFilteredForeground,getCurrentTime());
  }
}

void
  PersonShapeTrackerMod::updateTracks()
{
  TracksShapePS::iterator iTrack;
  for(iTrack=mTracks.begin();iTrack!=mTracks.end();iTrack++)
    (*iTrack)->update(mpFilteredForeground,getCurrentTime());
}


void
  PersonShapeTrackerMod::createNewTracks()
{
  for(int s=0;s<mvPersonPositions.size();s++)
  {
    if(mvPersonPositions[s].isAssigned == false && mStartTrackMask((int)mvPersonPositions[s].personPosition.x,(int)mvPersonPositions[s].personPosition.y)>0)
    {
      createNewTracks(mvPersonPositions[s].personPosition.x,mvPersonPositions[s].personPosition.y);
    }
  }

  //Added to create tracks at occluded person positions
  for(int s=0;s<mvOccludedPersonPositions.size();s++)
  {
    if(mvOccludedPersonPositions[s].isAssigned == false && mStartTrackMask((int)mvOccludedPersonPositions[s].personPosition.x,(int)mvOccludedPersonPositions[s].personPosition.y)>0 && mvOccludedPersonPositions[s].personProbability > 90)
    {
      createNewTracks(mvOccludedPersonPositions[s].personPosition.x,mvOccludedPersonPositions[s].personPosition.y);
    }
  }

  //Added to create tracks at LPP inside occluded regions
  for(int s=0;s<mvPersonPositionsLowThresh.size();s++)
  {
    bool flagInside = false;
    for(int a=0;a<mOccludingCartRectangles.size();a++)
    {
      if(mOccludingCartRectangles[a].inside(Vector3i(mvPersonPositionsLowThresh[s].personPosition.x,mvPersonPositionsLowThresh[s].personPosition.x,0)))
      {
        flagInside = true;
        break;
      }
    }
    if(mvPersonPositionsLowThresh[s].isAssigned == false && mStartTrackMask((int)mvPersonPositionsLowThresh[s].personPosition.x,(int)mvPersonPositionsLowThresh[s].personPosition.y)>0 && flagInside)
    {
      createNewTracks(mvPersonPositionsLowThresh[s].personPosition.x,mvPersonPositionsLowThresh[s].personPosition.y);
    }
  }

}

void 
  PersonShapeTrackerMod::deleteDeadTracks()
{
  // Delete tracks that are not alive
  for (TracksShapePS::iterator iTrack = mTracks.begin(); iTrack != mTracks.end();)
  {
    /*if((*iTrack)->noUseTrackAge > noUseTrackPeriod)
    (*iTrack)->kill();*/
    if (!(*iTrack)->isAlive())
    {
      if (mVerbose)
      {
        PVMSG("### Removing Track %d [frame: %d]\n",(*iTrack)->getId(),getCurrentFrameNumber());
      }
      TrajectoryPtr pTrajectory = (*iTrack)->getTrajectory();
      const int minTrajectoryLength = 1;
      //PVMSG("%d\n",pTrajectory->getNodes().size());
      //const int maxPreTrajectories = mMaxPreTrajectories;
      if (pTrajectory->getNodes().size() >= minTrajectoryLength) 
      {
        // Rescale the points in the trajectory so they match
        // the original image.
        const Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();

        const Rectanglei roi = mpForegroundSegmentMod->getRoiRectangle();

        const float scaleX = 1.0f / scaleFactor.x;
        const float scaleY = 1.0f / scaleFactor.y;

        Trajectory::Nodes::iterator iNode;
        Trajectory::Nodes& nodes = pTrajectory->getNodes();
        for (iNode = nodes.begin(); iNode != nodes.end(); ++iNode)
        {
          Trajectory::Node& node = **iNode;
          node.boundingBox.set(
            roi.x0 + node.boundingBox.x0*scaleX,
            roi.y0 + node.boundingBox.y0*scaleY,
            roi.x0 + node.boundingBox.x1*scaleX,
            roi.y0 + node.boundingBox.y1*scaleY);
          node.location.set(
            (node.boundingBox.x0+node.boundingBox.x1)/2,
            mIsTopViewCamera ? (node.boundingBox.y0+node.boundingBox.y1)/2 : node.boundingBox.y1);
        }
        mPreTrajectories.push_back(pTrajectory);

        if (mSendExtendedAttributes && mSendPreprocessTrajectories)
        {
          sendVmsEvent(pTrajectory, mEventChannels.front());
        }        
      }
      iTrack = mTracks.erase(iTrack);
    }
    else
    {
      iTrack++;
    }
  }
}

void 
  PersonShapeTrackerMod::createNewTracks(int x, int y)
{

  Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();
  Vector2i offset;
  Rectanglei roiRect = mpForegroundSegmentMod->getRoiRectangle();
  offset.x = std::min(roiRect.x0,roiRect.x1) * scaleFactor.x;
  offset.y = std::min(roiRect.y0,roiRect.y1) * scaleFactor.y;

  MultiFrameTrackShapePSPtr pNewTrack(new MultiFrameTrackShapePS());

  pNewTrack->init(
    mNewTrackId++,
    mSettings.getPath(),
    Vector2i(x,y),      
    getCurrentTime(),
    getFramesPerSecond(),
    mpPersonShapeEstimator,
    offset,100); 

  //Set the type of track depending on where it was initialized
  if(mGoodTrackRegionMask(x,y)>0)
    pNewTrack->setTrackInitType(true);
  else
    pNewTrack->setTrackInitType(false);
  //pNewTrack->setGoodTrackRegion(mGoodTrackRegionMask);

  if (mVerbose)
  {
    //    PVMSG("### Created Track %d at (%d,%d) [frame: %d]\n",pNewTrack->getId(),iLocation->x,iLocation->y,getCurrentFrameNumber());
  }
  mTracks.push_back(pNewTrack);  
}

void
  PersonShapeTrackerMod::scanForNewTracks()
{  

  PVASSERT(mStartTrackMask.width() ==  mpFilteredForeground->width());
  PVASSERT(mStartTrackMask.height() == mpFilteredForeground->height()); 


  // Make a copy since the original will be modified.  
  (*mpNewTrackScanImage) = *mpFilteredForeground;
  Image8& img = *mpNewTrackScanImage;

  Image8 rawForeground = *mpRawSegmentationImage;

  Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();
  Vector2i offset;
  Rectanglei roiRect = mpForegroundSegmentMod->getRoiRectangle();
  offset.x = std::min(roiRect.x0,roiRect.x1) * scaleFactor.x;
  offset.y = std::min(roiRect.y0,roiRect.y1) * scaleFactor.y;


  TracksShapePS::iterator iTrack;
  for (iTrack = mTracks.begin(); iTrack != mTracks.end(); iTrack++)
  {
    //(*iTrack)->cleanForegroundSegmentation(rawForeground);
    Vector2f pos =  (*iTrack)->getPos();
    mpPersonShapeEstimator->cleanForegroundSegmentation(round(pos.x),round(pos.y),rawForeground,offset);
  }

  //Now filter the image
  mpPersonShapeEstimator->filterImage(*mpNewTrackScanImage, rawForeground, offset);



  //iteratively scan through the image, creating tracks at higher threshold positions first
  std::vector<int> creationThresholds;
  std::list<int>::const_iterator iCreationThreshold;

  std::list<Vector2f> newTrackLocations;
  std::list<Vector2f>::iterator iLocation;

  /*
  //TODO: MAKE SURE BELOW HACK WORKS BEFORE UNCOMMENTING CODE
  if (mIsMotionEnabled)
  {
  PVASSERT(mpMotionMod->getMotionHistory()->width() == mpForegroundSegmentMod->getFilteredForeground()->width());
  PVASSERT(mpMotionMod->getMotionHistory()->height() == mpForegroundSegmentMod->getFilteredForeground()->height());    
  }
  */

  //<<<<<<< .mine
  for (iCreationThreshold = mThresholdList.begin(); iCreationThreshold != mThresholdList.end(); iCreationThreshold++)
  {  
    //scan entire image for new tracks    
    for (int y = 0; y < img.height(); y++)
    {
      for (int x = 0; x < img.width(); x++)
      {

        const Uint8 creationForegroundThreshold = *iCreationThreshold; //178; //WAS 178;         

        if (img(x,y) > creationForegroundThreshold && (mTracks.size()+newTrackLocations.size()) < mMaxConcurrentTracks
          && mStartTrackMask(x,y) == 255)
        {

          //PVMSG("TRACK CREATE ATTEMPT: %d %d\n", *iCreationThreshold, img(x,y));

          /*
          //TODO: MAKE SURE HACK WORKS BEFORE UNCOMMENTING BELOW CODE
          if (mIsMotionEnabled)
          {
          if ((*mpMotionMod->getMotionHistory())(x,y) > mMaxNoMotionTime)
          {
          //PVMSG("TRACK NOT CREATED DUE TO NO MOTION\n");
          continue;
          }
          }
          */


          Vector2f newTrackLocation(x,y);        
          newTrackLocations.push_back(newTrackLocation);        

          //hack, refilter input image after each new location is chosen        
          mpPersonShapeEstimator->cleanForegroundSegmentation(x,y,rawForeground,offset);
          mpPersonShapeEstimator->filterImage(*mpNewTrackScanImage, rawForeground, offset);        

          //
        }
      }
    }  



    //DONT CREATE TRACKS IF THEY ARE IN BOUNDING BOX OF ANOTHER TRACK    
    for (iLocation = newTrackLocations.begin(); iLocation != newTrackLocations.end(); )
    {

      bool keepNewTrack = true;

      //float minDist = img.width();
      TracksShapePS::iterator iClosestTrack = mTracks.end();
      for (iTrack = mTracks.begin(); iTrack != mTracks.end(); iTrack++)
      {
        const Vector2f& p1 = (*iTrack)->getPos();
        const Vector2f& p2 = (*iLocation);

        //convert location to integer version
        Vector2i newPos;
        newPos.x = int(p2.x);
        newPos.y = int(p2.y);

        //get mask rectangle from track
        Rectanglei trackRect = mpPersonShapeEstimator->getMaskRect(p1.x + offset.x, p1.y + offset.y);

        //modify rect to account for roi translation
        trackRect.x0 -= offset.x;
        trackRect.x1 -= offset.x;
        trackRect.y0 -= offset.y;
        trackRect.y1 -= offset.y;


        //if this point is inside the bounding box, erase it from the new track list
        if (trackRect.inside(newPos))
        {
          keepNewTrack = false;
          break;
        } 

        //TEST FOR INTERSECTION OF BOTH RECTS
        Rectanglei locRect = mpPersonShapeEstimator->getMaskRect(p2.x + offset.x, p2.y + offset.y);
        locRect.x0 -= offset.x;
        locRect.x1 -= offset.x;
        locRect.y0 -= offset.y;
        locRect.y1 -= offset.y;

        if (locRect.intersect(trackRect))
        {
          //PVMSG("TRACK NOT CREATED BECAUSE OF BBOX INTESECTION\n");
          keepNewTrack = false;
          break;
        }


        //END TEST

      }   

      if (keepNewTrack)
      {
        iLocation++;
      }
      else
      {
        iLocation = newTrackLocations.erase(iLocation);
      }   
    }  
  }

  //ACTUALLY CREATE TRACKS NOW  
  for (iLocation = newTrackLocations.begin(); iLocation != newTrackLocations.end(); iLocation++)
  {
    MultiFrameTrackShapePSPtr pNewTrack(new MultiFrameTrackShapePS());


    pNewTrack->init(
      mNewTrackId++,
      mSettings.getPath(),
      Vector2i(iLocation->x,iLocation->y),      
      getCurrentTime(),
      getFramesPerSecond(),
      mpPersonShapeEstimator,
      offset,100);      

    if (mVerbose)
    {
      PVMSG("### Created Track %d at (%d,%d) [frame: %d]\n",pNewTrack->getId(),iLocation->x,iLocation->y,getCurrentFrameNumber());
    }
    mTracks.push_back(pNewTrack);    
  }    
}


void
  PersonShapeTrackerMod::postProcessTracks(bool force)
{
  double currentTime = getCurrentTime();

  if (force == false)
    if (currentTime >= mNextForcePostProcessTime)
    {
      force = true;          
    }

    if (!force && currentTime < mNextPostProcessTime)
    {
      // Don't process, we have not reached the minimum time yet    
      return;
    }


    if (force == false && currentTime >= mNextPostProcessTime)
    {
      //perform post processing if there are no tracks in the scene
      if (mTracks.size() == 0)
      {
        force = true;            
      }
    }

    if (!force)
      return;  

    mNextForcePostProcessTime = currentTime + mPostProcessUpperInterval;
    mNextPostProcessTime = currentTime + mPostProcessLowerInterval;

    std::list<TrajectoryPtr> postTrajectories;  

    postTrajectories = mPreTrajectories;

    //TODO: handle this part better, maybe reconstruct combined trajectories later
    const Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();        

    //if any tracks are still alive, clip and process the first half of their trajectories  
    //std::list<MultiFrameTrackShapePSPtr>::iterator iTrack;
    TracksShapePS::iterator iTrack;
    for (iTrack = mTracks.begin(); iTrack != mTracks.end(); iTrack++)
    {
      TrajectoryPtr pFirstPart = (*iTrack)->getTrajectory();
      TrajectoryPtr pLastPart = pFirstPart->breakUp(getCurrentTime(), mNewTrackId++);

      // Rescale the points in the trajectory so they match
      // the original image.    
      //const Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();        
      const Rectanglei roi = mpForegroundSegmentMod->getRoiRectangle();
      const float scaleX = 1.0f / scaleFactor.x;
      const float scaleY = 1.0f / scaleFactor.y;

      Trajectory::Nodes::iterator iNode;
      Trajectory::Nodes& nodes = pFirstPart->getNodes();
      for (iNode = nodes.begin(); iNode != nodes.end(); ++iNode)
      {
        Trajectory::Node& node = **iNode;
        node.boundingBox.set(
          roi.x0 + node.boundingBox.x0*scaleX,
          roi.y0 + node.boundingBox.y0*scaleY,
          roi.x0 + node.boundingBox.x1*scaleX,
          roi.y0 + node.boundingBox.y1*scaleY);
        node.location.set(
          (node.boundingBox.x0+node.boundingBox.x1)/2,
          mIsTopViewCamera ? (node.boundingBox.y0+node.boundingBox.y1)/2 : node.boundingBox.y1);
      }     

      (*iTrack)->setTrajectory(pLastPart);      
      postTrajectories.push_back(pFirstPart);   
    }

    mPreTrajectories.clear();

    if (mSavePreTrajectories && postTrajectories.size() != 0)
    {    
      //
      DateTime dt(getCurrentTime());

      std::string filename = "Traj_VidSrcId" + aitSprintf("%d", mVideoSourceId);
      filename += "_" + dt.format("%Y%m%d_%H%M%S.csv");
      std::string loc = ait::combine_path(mTrajectoryOutputFolder, filename);

      //Compress trajectories to save space
      std::list<TrajectoryPtr> compactedTraj;
      std::list<TrajectoryPtr>::iterator iTraj;

      /*
      double compressDist = 
      mpForegroundSegmentMod->getBlobSizeEstimate()->getCenterWidth()/3.0;
      */
      //PERSON SHAPE BEGIN    
      Vector2i frameSize = getImageAcquireModule()->getFrameSize();
      Rectanglei personSize = mpPersonShapeEstimator->getMaskRect(frameSize.x/2,frameSize.y/2);
      double compressDist = personSize.width()/scaleFactor.x/3.0;;
      //PERSON SHAPE END


      for (iTraj = postTrajectories.begin(); iTraj != postTrajectories.end(); iTraj++)
      {      

        TrajectoryPtr pTraj = (*iTraj)->compress(compressDist,5.0);      

        if (pTraj->nodeCount() > 1)
          compactedTraj.push_back(pTraj);
      }   

      if (compactedTraj.size() > 0)
        Trajectory::saveXmlCsv(loc, compactedTraj);
      //Trajectory::saveCSV(loc, compactedTraj);
    } 

    //perform postprocessing
    //mMultiPersonHandler.processTrajectories(postTrajectories);

    if (mSendTrajectoriesAsEventAttribute)
    {
      //Compress trajectories to save space
      std::list<TrajectoryPtr> compactedTraj;
      std::list<TrajectoryPtr>::iterator iTraj;


      //double compressDist = 
      //    mpForegroundSegmentMod->getBlobSizeEstimate()->getCenterWidth()/3.0;
      //PERSON SHAPE BEGIN
      Vector2i frameSize = getImageAcquireModule()->getFrameSize();
      Rectanglei personSize = mpPersonShapeEstimator->getMaskRect(frameSize.x/2,frameSize.y/2);
      double compressDist = personSize.width()/scaleFactor.x/3.0;
      //PERSON SHAPE END

      for (iTraj = postTrajectories.begin(); iTraj != postTrajectories.end(); iTraj++)
      {      
        TrajectoryPtr pTraj = (*iTraj)->compress(compressDist,5.0);

        if (pTraj->nodeCount() > 1)
          compactedTraj.push_back(pTraj);
      }

      for (iTraj = compactedTraj.begin(); iTraj != compactedTraj.end(); ++iTraj)
      {
        sendVmsEvent(*iTraj,*mEventChannels.begin());
        BlobTrackerMsgShapePtr pMsg(new BlobTrackerMsgShape(getVisualSensorId(),(*iTraj)->getStartTime()));
        pMsg->setFlag(BlobTrackerMsgShape::POST_PROCESSED_TRAJECTORY,true);
        pMsg->setTrajectory(*iTraj);
        sendMessage(pMsg);
      }
    }

    //DO NOT CALL THIS
    //generateEvents(postTrajectories);

    //send notification that all prior events can be processed
    std::list<int>::const_iterator iEvtChan;
    for (iEvtChan = mEventChannels.begin(); iEvtChan != mEventChannels.end(); iEvtChan++)
      mpVmsPacket->notifyProcessCompletion(currentTime, *iEvtChan);
}


void
  PersonShapeTrackerMod::generateEvents(const std::list<TrajectoryPtr>& trajectories)
{
  assert(false);

  /*
  assert(mpForegroundSegmentMod->getBlobSizeEstimate().get());

  if (mEventsPolygon.empty()) return;

  std::list<int>::const_iterator iEvtChan;

  for (iEvtChan = mEventChannels.begin(); iEvtChan != mEventChannels.end(); iEvtChan++)
  {
  std::list<TrajectoryPtr>::const_iterator iTrajectory;
  for (iTrajectory = trajectories.begin(); iTrajectory != trajectories.end(); ++iTrajectory)
  {
  std::list<PolygonEventPtr> events;    
  mTrajEvents.extractEvents(**iTrajectory, mEventsPolygon[*iEvtChan], events);
  std::list<PolygonEventPtr>::iterator iEvent;
  for (iEvent = events.begin(); iEvent != events.end(); ++iEvent)
  {
  sendVmsEvent(*iEvent, *iEvtChan);
  }
  }
  }
  */
}

void
  PersonShapeTrackerMod::sendVmsEvent(TrajectoryPtr pTrajectory, int eventChannel)
{

  //assert(mpForegroundSegmentMod->getBlobSizeEstimate().get());
  assert(mpPersonShapeEstimator.get());

  double duration = pTrajectory->getEndTime()- pTrajectory->getStartTime();
  //Todo: minimum duration of 1.0 for now;
  duration = std::max(duration, 1.0);

  //double compressDist = 
  //      mpForegroundSegmentMod->getBlobSizeEstimate()->getCenterWidth()/3.0;


  Vector2i frameSize = getImageAcquireModule()->getFrameSize();
  Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();
  Rectanglei personSize = mpPersonShapeEstimator->getMaskRect(frameSize.x/2,frameSize.y/2);
  double compressDist = personSize.width()/scaleFactor.x/3.0;;

  TrajectoryPtr pCompressed = pTrajectory->compress(compressDist,5.0);

  mpVmsPacket->beginEventTime(pTrajectory->getStartTime(),duration, eventChannel);

  if (mSendTrajectoriesAsEventAttribute)
  {
    mpVmsPacket->addLongStringAttribute("4",pTrajectory->toXmlString());
  }
  else
  {
    mpVmsPacket->addAttribute("10",boost::lexical_cast<std::string>(pTrajectory->getId()));
    mpVmsPacket->addString("<extended>" + pTrajectory->getXmlBlobRectangles() + "</extended>");
  }
  mpVmsPacket->endEvent();

}

void
  PersonShapeTrackerMod::processMessages()
{
  VisionMsgPtr pMsg;

  while (mpInputMsgQueue->get(pMsg))
  {
    BlobTrackerMsgShape* pMsg0 = dynamic_cast<BlobTrackerMsgShape*>(pMsg.get());
    if (pMsg0 != NULL)
    {
      processMsg(pMsg0);
      continue;
    }
    PvUtil::exitError("Message type not supported by this module [%s]",getFullName().c_str());
  }
}

void 
  PersonShapeTrackerMod::processMsg(BlobTrackerMsgShape *pMsg)
{
}

void
  PersonShapeTrackerMod::finish()
{ 

  postProcessTracks(true);

  //mpVmsPacket->finish();
  RealTimeMod::finish();
}


unsigned int getV(unsigned int r, unsigned int g, unsigned int b)
{
  unsigned int maxVal = std::max(r,g);
  maxVal = std::max(maxVal,b);

  return maxVal;
}

unsigned int getH(unsigned int r, unsigned int g, unsigned int b)
{
  unsigned int maxVal = std::max(r,g);
  maxVal = std::max(maxVal,b);

  unsigned int minVal = std::min(r,g);
  minVal = std::min(minVal,b); 

  int denom = maxVal - minVal;

  unsigned int finalVal = 0;

  if (maxVal == minVal)
  {
    //undefined, set to complementary color for now as hack
    finalVal = 165;
    //assert(false);
  }
  else if (maxVal == r && g >= b)
  {
    int num = g-b;
    float fraction = (float)num/denom;
    finalVal = (unsigned int)(60 * fraction);
  }
  else if (maxVal == r && g < b)
  {
    int num = g-b;    
    float fraction = (float)num/denom;
    finalVal = (unsigned int)(60 * fraction + 360);
  }
  else if (maxVal == g)
  {
    int num = b-r;
    float fraction = (float)num/denom;
    finalVal = (unsigned int)(60 * fraction + 120);
  }
  else if (maxVal == b)
  {
    int num = r-g;
    float fraction = (float)num/denom;
    finalVal = (unsigned int)(60 * fraction + 240);
  }

  assert(finalVal >= 0);
  assert(finalVal <= 360);
  return finalVal;
}

unsigned int getS(unsigned int r, unsigned int g, unsigned int b)
{
  unsigned int maxVal = std::max(r,g);
  maxVal = std::max(maxVal,b);

  unsigned int minVal = std::min(r,g);
  minVal = std::min(minVal,b); 

  float finalVal = 0.0;

  if (maxVal == 0)
    finalVal = 0.0;
  else
    finalVal = 1.0 - (float)minVal/(float)maxVal;

  assert((unsigned int)(finalVal*255.0) >=0);
  assert((unsigned int)(finalVal*255.0) <= 255);

  return (unsigned int)(finalVal*255.0);

}

//unsigned int getV(unsigned int r, unsigned int g, unsigned int b)
//{
//  assert(false);
//  return 0;
//}

void
PersonShapeTrackerMod::drawBisector(Image8& img, Vector2f pos1, Vector2f pos2)
{
  float x1=pos1.x,y1=pos1.y,x2=pos2.x,y2=pos2.y;
  
  //Get the centre point (new centre of coordinate system)
  float xCenter = (x1+x2)/2.0f;
  float yCenter = (y1+y2)/2.0f;

  //Updated x and y coordinates
  x1 -= xCenter;
  x2 -= xCenter;
  y1 -= yCenter;
  y2 -= yCenter;


  //Now do the rotation and scaling
  float scaleFactor = 10.0f;
  if(sqrt(dist2(pos1,pos2))<15)
    scaleFactor = 10.0f;

  //Matrix is [0 -1;1 0] for 90 CC rotation
  float x1_found,x2_found,y1_found,y2_found;

  x1_found = -1.0f*y1*scaleFactor;
  y1_found = x1*scaleFactor;
  x2_found = -1.0f*y2*scaleFactor;
  y2_found = x2*scaleFactor;

  //Coordinates in appropriate space
  x1_found+=xCenter;
  x2_found+=xCenter;
  y1_found+=yCenter;
  y2_found+=yCenter;

  //Now draw the line

  img.line(x1_found,y1_found,x2_found,y2_found,PV_RGB(0,0,0));
  mNoCartRegions.push_back(Rectanglei(x1_found,y1_found,x2_found,y2_found));



}

void
  PersonShapeTrackerMod::detectCarts()
{
  Image32Ptr pCurrentFrame = getImageAcquireModule()->getCurrentFramePtr();
  Image32Ptr pRoiImg = mpForegroundSegmentMod->getColorRoiFrame(pCurrentFrame);

  //Init mpColorSegmentedImage if not already available
  if (!mpColorSegmentedImage.get())
  {   
    mpColorSegmentedImage.reset(new Image8(pRoiImg->width(),pRoiImg->height()));
    mpColorAndForegroundImage.reset(new Image8(pRoiImg->width(),pRoiImg->height()));
    mpFilteredComponentImage.reset(new Image8(pRoiImg->width(),pRoiImg->height()));
  }

  mCartPolygons.clear();

  Image8& foregroundImg = *mpRawSegmentationImage;

  assert(mpColorSegmentedImage->size() == pRoiImg->size());

  const Image32& roiImg = *pRoiImg;

  Image8& colorImg = *mpColorSegmentedImage;

  const unsigned int sR = RED(mSelectedColor);
  const unsigned int sG = GREEN(mSelectedColor);
  const unsigned int sB = BLUE(mSelectedColor);

  const unsigned int sH = getH(sR,sG,sB);
  const unsigned int sS = getS(sR,sG,sB);
  const unsigned int sV = getV(sR,sG,sB);

  const unsigned int sR2 = RED(mSelectedColor2);
  const unsigned int sG2 = GREEN(mSelectedColor2);
  const unsigned int sB2 = BLUE(mSelectedColor2);

  const unsigned int sH2 = getH(sR2,sG2,sB2);
  const unsigned int sS2 = getS(sR2,sG2,sB2);
  const unsigned int sV2 = getV(sR2,sG2,sB2);

  for (int i = 0; i < roiImg.size(); i++)
  {
    unsigned int r = RED(roiImg(i));
    unsigned int g = GREEN(roiImg(i));
    unsigned int b = BLUE(roiImg(i));

    unsigned int h = getH(r,g,b);
    unsigned int s = getS(r,g,b);
    unsigned int v = getV(r,g,b);

    //H is a ring, so we must wrap around at 360 degrees
    unsigned int dist = abs((int)(sH-h));
	unsigned int dist2 = abs((int) (sS-s));
	unsigned int dist3 = abs((int) (sV-v));
	//PVMSG("dist: %d\tdist2: %d\tdist3: %d\n", dist, dist2, dist3);
    if (sH > h)
      dist = std::min(dist, 360-sH+h);
    else
      dist = std::min(dist, 360-h+sH);

    if (dist <= mSelectedColorDistH && dist2<=mSelectedColorDistS && dist3<=mSelectedColorDistV && (s+v)>mSelectedColorDistVAndS)
    {
      colorImg(i) = 255;      
    }
	else
	{
		/*if(r+g+b>0)
		{
			int a = s+v;
			std::stringstream ss;
			ss << "red: " << r << "\t\tgreen: " << g << "\t\tblues: " << b <<
				"\nhue: " << h << "\t\tsatur: " << s << "\t\tvalue: " << v <<
				"\ndist: " << dist << "\t\tdist2: " << dist2 << "\t\tdist3: " << dist3 << "\t\tdist4: " << (s+v) <<
				"\ndist: " << mSelectedColorDistH << "\t\tdist2: " << mSelectedColorDistS << "\t\tdist3: " << mSelectedColorDistV << "\t\tdist4: " << mSelectedColorDistVAndS;
			MessageBox(NULL, ss.str().c_str(), "vals", MB_OK);
		}*/
      colorImg(i) = 0;
	}
  }

  if(mSelectedColor2 != 0)
  {
	  for (int i = 0; i < roiImg.size(); i++)
	  {
		unsigned int r = RED(roiImg(i));
		unsigned int g = GREEN(roiImg(i));
		unsigned int b = BLUE(roiImg(i));

		unsigned int h = getH(r,g,b);
		unsigned int s = getS(r,g,b);
		unsigned int v = getV(r,g,b);

		//H is a ring, so we must wrap around at 360 degrees
		unsigned int dist = abs((int)(sH2-h));
		unsigned int dist2 = abs((int) (sS2-s));
		unsigned int dist3 = abs((int) (sV2-v));
		//PVMSG("dist: %d\tdist2: %d\tdist3: %d\n", dist, dist2, dist3);
		if (sH2 > h)
		  dist = std::min(dist, 360-sH2+h);
		else
		  dist = std::min(dist, 360-h+sH2);

		if (dist <= mSelectedColorDistH && dist2<=mSelectedColorDistS && dist3<=mSelectedColorDistV && (s+v)>mSelectedColorDistVAndS)
		{
		  colorImg(i) = 255;      
		}
	  }
  }
  

  //Now AND the foreground segmentation w/ the color segmentation
  Image8& colorAndForegroundImg = *mpColorAndForegroundImage;
  for (int i = 0; i < colorAndForegroundImg.size(); i++)
  {    
    colorAndForegroundImg(i) = colorImg(i);// & foregroundImg(i);
  }


  //Ordered filtering to complete the image
  Image8 tempImg(colorAndForegroundImg.width(),colorAndForegroundImg.height());
  tempImg = colorAndForegroundImg;
  filterSegmentationWholeImg(colorAndForegroundImg,2);
  for(int a=0;a<colorAndForegroundImg.size();a++)
    colorAndForegroundImg(a) = tempImg(a) | colorAndForegroundImg(a);
  filterSegmentation(colorAndForegroundImg,5);

  //Code to prevent multiple carts from merging into one another
  //Draw bounding box for the cart and change the size based on motion
  ////std::vector<Rectanglei> noCartRegions;
  ////mNoCartRegions.clear();
  ////std::vector<int> doneTracks;

  ////for(TracksShapePS::iterator iT = mCartTracks.begin();iT!=mCartTracks.end();iT++)
  ////{
  ////  for(TracksShapePS::iterator iT2 = mCartTracks.begin();iT2!=mCartTracks.end();iT2++)
  ////  {
  ////    if(iT==iT2)
  ////      continue;
  ////    Vector2f firstPos,secondPos;
  ////    firstPos = (*iT)->getPredictedPos();
  ////    secondPos = (*iT2)->getPredictedPos();

  ////    if(sqrt(dist2(firstPos,secondPos))<30.0f)
  ////    {

  ////      std::vector<Polygon> tempPoly;
  ////      tempPoly = (*iT)->getCartPolygonVector();
  ////      Rectanglef tempRect,tempRectAvg(0,0,0,0);
  ////      float avgWidth=0.0f,avgHeight=0.0f;
  ////      float totalPoly = 0.0f;
  ////      for(int a=0;a<tempPoly.size();a++)
  ////      {
  ////        tempRect = tempPoly[a].getBoundingBox();
  ////        fixRect(tempRect);
  ////        avgHeight+=tempRect.height();
  ////        avgWidth+=tempRect.width();
  ////        totalPoly+=1;
  ////      }

  ////      avgWidth/=totalPoly;
  ////      avgHeight/=totalPoly;
  ////      tempRectAvg.set(firstPos.x-avgWidth/2.0f,firstPos.y-avgHeight/2.0f,firstPos.x+avgWidth/2.0f,firstPos.y+avgHeight/2.0f);
  ////      

  ////      std::vector<Polygon> tempPoly2;
  ////      tempPoly2 = (*iT2)->getCartPolygonVector();
  ////      Rectanglef tempRect2,tempRectAvg2(0,0,0,0);
  ////      float avgWidth2=0.0f,avgHeight2=0.0f;
  ////      float totalPoly2 = 0.0f;
  ////      for(int a=0;a<tempPoly2.size();a++)
  ////      {
  ////        tempRect2 = tempPoly2[a].getBoundingBox();
  ////        fixRect(tempRect2);
  ////        avgHeight2+=tempRect2.height();
  ////        avgWidth2+=tempRect2.width();
  ////        totalPoly2+=1;
  ////      }

  ////      avgWidth2/=totalPoly2;
  ////      avgHeight2/=totalPoly2;
  ////      tempRectAvg2.set(secondPos.x-avgWidth2/2.0f,secondPos.y-avgHeight2/2.0f,secondPos.x+avgWidth2/2.0f,secondPos.y+avgHeight2/2.0f);
  ////      
  ////      //First draw at appropriate place for first cart
  ////      if(firstPos.y<secondPos.y)
  ////      {
  ////        //Draw on bottom of *iT

  ////        mNoCartRegions.push_back(Rectanglei(tempRectAvg.x0,tempRectAvg.y1,tempRectAvg.x1,tempRectAvg.y1));
  ////      }
  ////      else
  ////      {
  ////        //Draw on top of *iT
  ////        
  ////        mNoCartRegions.push_back(Rectanglei(tempRectAvg.x0,tempRectAvg.y0,tempRectAvg.x1,tempRectAvg.y0));
  ////      }

  ////      if(secondPos.y<firstPos.y)
  ////      {
  ////        //Draw on bottom of *iT
  ////        
  ////        mNoCartRegions.push_back(Rectanglei(tempRectAvg2.x0,tempRectAvg2.y1,tempRectAvg2.x1,tempRectAvg2.y1));
  ////      }
  ////      else
  ////      {
  ////        //Draw on top of *iT
  ////        mNoCartRegions.push_back(Rectanglei(tempRectAvg2.x0,tempRectAvg2.y0,tempRectAvg2.x1,tempRectAvg2.y0));
  ////      }

  ////    }

  ////  }
  ////}

  ////for(TracksShapePS::iterator iT = mCartTracks.begin();iT!=mCartTracks.end();iT++)
  ////{
  ////  for(TracksShapePS::iterator iT2 = mCartTracks.begin();iT2!=mCartTracks.end();iT2++)
  ////  {
  ////    if(iT==iT2)
  ////      continue;
  ////    Vector2f firstPos,secondPos;
  ////    firstPos = (*iT)->getPredictedPos();
  ////    secondPos = (*iT2)->getPredictedPos();

  ////    if(sqrt(dist2(firstPos,secondPos))<30.0f)
  ////    {
  ////      //First draw at appropriate place for first cart
  ////      if(firstPos.y<secondPos.y)
  ////      {
  ////        //Draw on bottom of *iT
  ////        Polygon tempPoly;
  ////        (*iT)->getCartPolygon(tempPoly);
  ////        Rectanglef tempRect;
  ////        tempRect = tempPoly.getBoundingBox();
  ////        fixRect(tempRect);
  ////        mNoCartRegions.push_back(Rectanglei(tempRect.x0,tempRect.y1,tempRect.x1,tempRect.y1));
  ////      }
  ////      else
  ////      {
  ////        //Draw on top of *iT
  ////        Polygon tempPoly;
  ////        (*iT)->getCartPolygon(tempPoly);
  ////        Rectanglef tempRect;
  ////        tempRect = tempPoly.getBoundingBox();
  ////        fixRect(tempRect);
  ////        mNoCartRegions.push_back(Rectanglei(tempRect.x0,tempRect.y0,tempRect.x1,tempRect.y0));
  ////      }

  ////      if(secondPos.y<firstPos.y)
  ////      {
  ////        //Draw on bottom of *iT
  ////        Polygon tempPoly;
  ////        (*iT2)->getCartPolygon(tempPoly);
  ////        Rectanglef tempRect;
  ////        tempRect = tempPoly.getBoundingBox();
  ////        fixRect(tempRect);
  ////        mNoCartRegions.push_back(Rectanglei(tempRect.x0,tempRect.y1,tempRect.x1,tempRect.y1));
  ////      }
  ////      else
  ////      {
  ////        //Draw on top of *iT
  ////        Polygon tempPoly;
  ////        (*iT2)->getCartPolygon(tempPoly);
  ////        Rectanglef tempRect;
  ////        tempRect = tempPoly.getBoundingBox();
  ////        fixRect(tempRect);
  ////        mNoCartRegions.push_back(Rectanglei(tempRect.x0,tempRect.y0,tempRect.x1,tempRect.y0));
  ////      }

  ////    }

  ////  }
  ////}

  ///*for(int i=0;i<mNoCartRegions.size();i++)
  //  colorAndForegroundImg.line(mNoCartRegions[i].x0,mNoCartRegions[i].y0,mNoCartRegions[i].x1,mNoCartRegions[i].y1,PV_RGB(0,0,0));*/


  ////Code for resetting the track status and updating the past predicted positions
  ///*for(TracksShapePS::iterator iT = mCartTracks.begin();iT!=mCartTracks.end();iT++)
  //{
  //  bool flagClose;
  //  for(TracksShapePS::iterator iT2 = mCartTracks.begin();iT2!=mCartTracks.end();iT2++)
  //  {
  //    if(iT==iT2)
  //      continue;
  //    else
  //    {
  //      Vector2f pos1,pos2,tempPos;
  //      (*iT)->getPastPredictedPositions(pos1,tempPos);
  //      (*iT2)->getPastPredictedPositions(pos2,tempPos);
  //      PVMSG("\n%f %f\n",pos1.x,pos1.y);
  //      if(sqrt(dist2(pos1,pos2))<30)
  //        flagClose = true;
  //    }
  //  }
  //  if(flagClose)
  //    (*iT)->setPastPredictedTrajectoryStatus(true);
  //  else
  //    (*iT)->setPastPredictedTrajectoryStatus(false);
  //}*/

  ////for(TracksShapePS::iterator iT = mCartTracks.begin();iT!=mCartTracks.end();iT++)
  ////{

  ////  bool flagNear=false;
  ////  for(TracksShapePS::iterator iT2 = mCartTracks.begin();iT2!=mCartTracks.end();iT2++)
  ////  {
  //////(*iT)->setPastPredictedTrajectoryStatus(false);
  //////(*iT2)->setPastPredictedTrajectoryStatus(false);

  ////    if(iT==iT2)
  ////      continue;
  ////    Vector2f firstPos,secondPos,tempPos;
  ////(*iT)->getPastPredictedPositions(firstPos,tempPos);
  ////(*iT2)->getPastPredictedPositions(secondPos,tempPos);
  ////if(sqrt(dist2(firstPos,secondPos))<30.0f)
  ////     flagNear=true;
  ////  }
  ////  if(flagNear)
  ////    (*iT)->setPastPredictedTrajectoryStatus(false);
  ////  else
  ////    (*iT)->setPastPredictedTrajectoryStatus(false);


  ////}
  ////for(TracksShapePS::iterator iT = mCartTracks.begin();iT!=mCartTracks.end();iT++)
  ////{
  ////  bool flagNear = false;
  ////  bool flagBelow = false;
  ////  bool flagLeft = false;
  ////  Trajectory::Nodes& nodes = (*iT)->getTrajectory()->getNodes();
  ////  Trajectory::Nodes::iterator iNode = nodes.end();
  ////  Vector2f firstPos,secondPos,tempPos;
  ////  /*Vector2f firstPos1,firstPos2,secondPos1,secondPos2;
  ////  
  ////  iNode--;
  ////  firstPos1 = (*iNode)->location;

  ////  if(nodes.size()>10)
  ////  {
  ////    for(int a=0;a<10;a++)
  ////      iNode--;
  ////  }
  ////  else
  ////  {
  ////    iNode = nodes.begin();
  ////  }
  ////  firstPos2 = (*iNode)->location;*/

  ////  
  ////  for(TracksShapePS::iterator iT2 = mCartTracks.begin();iT2!=mCartTracks.end();iT2++)
  ////  {
  ////    /*Trajectory::Nodes& nodes2 = (*iT2)->getTrajectory()->getNodes();
  ////    Trajectory::Nodes::iterator iNode2 = nodes2.end();
  ////    iNode2--;
  ////    secondPos1 = (*iNode2)->location;
  ////    if(nodes2.size()>10)
  ////    {
  ////      for(int a=0;a<10;a++)
  ////        iNode2--;
  ////    }
  ////    else
  ////    {
  ////      iNode2 = nodes2.begin();
  ////    }
  ////    secondPos2 = (*iNode2)->location;*/


  ////    bool flagDone = false;
  ////    for(int a=0;a<doneTracks.size();a++)
  ////      if((*iT)->getAge()==doneTracks[a] || (*iT2)->getAge()==doneTracks[a])
  ////        flagDone = true;
  ////    if(iT==iT2 || flagDone==true)
  ////      continue;
  ////    //firstPos = (*iT)->getPos();
  ////    //secondPos = (*iT2)->getPos();
  ////    /*(*iT)->getPastPredictedPositions(firstPos,tempPos);
  ////    (*iT2)->getPastPredictedPositions(secondPos,tempPos);*/
  ////    //Check
  ////    //(*iT)->setPastPredictedTrajectoryStatus(false);
  ////    //(*iT2)->setPastPredictedTrajectoryStatus(false);

  ////    (*iT)->getPastPredictedPositions(firstPos,tempPos);
  ////    (*iT2)->getPastPredictedPositions(secondPos,tempPos);


  ////    
  ////    //firstPos=(*iT)->getPredictedPos();
  ////    //secondPos=(*iT2)->getPredictedPos();
  ////    
  ////    float distTracks = sqrt(dist2(firstPos,secondPos)); 
  ////    if(distTracks<30.0f)
  ////    {
  ////      //doneTracks.push_back((*iT)->getAge());
  ////      //doneTracks.push_back((*iT2)->getAge());

  ////      flagNear = true;
  ////      //drawBisector(colorAndForegroundImg,firstPos,secondPos);
  ////      //if(firstPos.y<secondPos.y)
  ////      //  flagBelow = true;
  ////      //if(firstPos.x<secondPos.x)
  ////      //  flagLeft = true;

  ////      break;
  ////    }
  ////    
  ////  }
  ////  if(flagNear)
  ////  {


  ////    drawBisector(colorAndForegroundImg,firstPos,secondPos);

  ////    //Polygon tempPoly;
  ////    //(*iT)->getCartPolygon(tempPoly);

  ////    //Rectanglef tempR = tempPoly.getBoundingBox();
  ////    //fixRect(tempR);

  ////    //Trajectory::Nodes& nodes = (*iT)->getTrajectory()->getNodes();
  ////    //Trajectory::Nodes::iterator iNode = nodes.end();
  ////    //iNode--;
  ////    //Vector2f currPos = (*iNode)->location;

  ////    //int tempCount = nodes.size();
  ////    //if(nodes.size()>10)
  ////    //{
  ////    //  for(int a=0;a<10;a++)
  ////    //    iNode--;
  ////    //}
  ////    //else
  ////    //{
  ////    //  iNode = nodes.begin();
  ////    //}

  ////    //Vector2f pastPos = (*iNode)->location;

  ////    //float xChange = currPos.x-pastPos.x;
  ////    //float yChange = currPos.y-pastPos.y;

  ////    //if(fabs(xChange)>0 || fabs(yChange)>0)
  ////    //{
  ////    //  if(fabs(xChange)>fabs(yChange))
  ////    //  {
  ////    //    //Draw horizontal lines
  ////    //    int xLeft = tempR.x0 - 6 >= 0 ?tempR.x0 - 6:0;
  ////    //    int xRight = tempR.x1 + 6 < tempImg.width() ?tempR.x1 + 6:tempImg.width()-1;
  ////    //    int yTop = tempR.y0;
  ////    //    int yBottom = tempR.y1;
  ////    //    //int yTop = tempR.y0 - 6 >=0? tempR.y0 - 6:0;
  ////    //    //int yBottom = tempR.y1 + 6<tempImg.height()?tempR.y1 + 6:tempImg.height()-1;
  ////    //    colorAndForegroundImg.line(xLeft,yTop,xRight,yTop,PV_RGB(0,0,0));
  ////    //    colorAndForegroundImg.line(xLeft,yBottom,xRight,yBottom,PV_RGB(0,0,0));
  ////    //    if(!flagBelow)
  ////    //    mNoCartRegions.push_back(Rectanglei(xLeft,yTop,xRight,yTop));
  ////    //    else
  ////    //    mNoCartRegions.push_back(Rectanglei(xLeft,yBottom,xRight,yBottom));
  ////    //  }
  ////    //  else
  ////    //  {
  ////    //    //Draw vertical lines
  ////    //    int xLeft = tempR.x0;
  ////    //    int xRight = tempR.x1;
  ////    //    //int xLeft = tempR.x0 - 6 >= 0 ?tempR.x0 - 6:0;
  ////    //    //int xRight = tempR.x1 + 6 < tempImg.width() ?tempR.x1 + 6:tempImg.width()-1;
  ////    //    int yTop = tempR.y0 - 6 >=0? tempR.y0 - 6:0;
  ////    //    int yBottom = tempR.y1 + 6<tempImg.height()?tempR.y1 + 6:tempImg.height()-1;
  ////    //    colorAndForegroundImg.line(xLeft,yTop,xLeft,yBottom,PV_RGB(0,0,0));
  ////    //    colorAndForegroundImg.line(xRight,yTop,xRight,yBottom,PV_RGB(0,0,0));
  ////    //    if(flagLeft)
  ////    //    mNoCartRegions.push_back(Rectanglei(xLeft,yTop,xLeft,yBottom));
  ////    //    else
  ////    //    mNoCartRegions.push_back(Rectanglei(xRight,yTop,xRight,yBottom));
  ////    //  }
  ////    //}



  ////    //Rectanglei tempRect;
  ////    //if((*iT)->getAge()<3)
  ////    //{
  ////    //  Polygon tempPoly;
  ////    //  (*iT)->getCartPolygon(tempPoly);

  ////    //  Rectanglef tempR = tempPoly.getBoundingBox();
  ////    //  fixRect(tempR);

  ////    //  int xLeftTop = tempR.x0 - 5 >=0 ? tempR.x0 - 5 : 0;
  ////    //  int yLeftTop = tempR.y0 - 5 >=0 ? tempR.y0 - 5 : 0;
  ////    //  int xRightBottom = tempR.x1 + 5 < tempImg.width() ? tempR.x1 + 5 : tempR.width()-1;
  ////    //  int yRightBottom = tempR.y1 + 5< tempImg.height() ? tempR.y1 + 5 : tempR.height()-1;

  ////    //  tempRect.set(xLeftTop,yLeftTop,xRightBottom,yRightBottom);
  ////    //  noCartRegions.push_back(tempRect);
  ////    //}
  ////    //else
  ////    //{
  ////    //  Polygon tempPoly;
  ////    //  (*iT)->getCartPolygon(tempPoly);

  ////    //  Rectanglef tempR = tempPoly.getBoundingBox();
  ////    //  fixRect(tempR);

  ////    //  int xLeftTop = tempR.x0 - 5 >=0 ? tempR.x0 - 5 : 0;
  ////    //  int yLeftTop = tempR.y0 - 5 >=0 ? tempR.y0 - 5 : 0;
  ////    //  int xRightBottom = tempR.x1 + 5 < tempImg.width() ? tempR.x1 + 5 : tempR.width()-1;
  ////    //  int yRightBottom = tempR.y1 + 5< tempImg.height() ? tempR.y1 + 5 : tempR.height()-1;

  ////    //  Trajectory::Nodes& nodes = (*iT)->getTrajectory()->getNodes();
  ////    //  Trajectory::Nodes::iterator iNode = nodes.end();
  ////    //  iNode--;

  ////    //  Vector2f currPos = (*iNode)->location;
  ////    //  iNode--;
  ////    //  Vector2f pastPos = (*iNode)->location;

  ////    //  int xChange = currPos.x - pastPos.x;
  ////    //  int yChange = currPos.y - pastPos.y;

  ////    //  if(xChange>0 || yChange>0)
  ////    //  {
  ////    //    //Draw lines
  ////    //    if(xChange>yChange)
  ////    //    {
  ////    //      //Draw parallel horizontal lines
  ////    //      int xLeft = tempR.x0 - 6 >= 0 ?tempR.x0 - 6:0;
  ////    //      int xRight = tempR.x0 + 6 < tempImg.width() ?tempR.x0 + 6:tempImg.width()-1;
  ////    //      colorAndForegroundImg.line(xLeft,tempR.y0,xRight,tempR.y0,PV_RGB(0,0,0));
  ////    //      colorAndForegroundImg.line(xLeft,tempR.y1,xRight,tempR.y1,PV_RGB(0,0,0));
  ////    //      mNoCartRegions.push_back(Rectanglei(xLeft,tempR.y0,xRight,tempR.y0));
  ////    //      mNoCartRegions.push_back(Rectanglei(xLeft,tempR.y1,xRight,tempR.y1));

  ////    //    }
  ////    //    else
  ////    //    {
  ////    //      //Draw parallel vertical lines
  ////    //      int yTop = tempR.y0 - 6 >=0? tempR.y0 - 6:0;
  ////    //      int yBottom = tempR.y1 + 6<tempImg.height()?tempR.y1 + 6:tempImg.height()-1;
  ////    //      colorAndForegroundImg.line(tempR.x0,yTop,tempR.x0,yBottom,PV_RGB(0,0,0));
  ////    //      colorAndForegroundImg.line(tempR.x1,yTop,tempR.x1,yBottom,PV_RGB(0,0,0));
  ////    //      mNoCartRegions.push_back(Rectanglei(tempR.x0,yTop,tempR.x0,yBottom));
  ////    //      mNoCartRegions.push_back(Rectanglei(tempR.x1,yTop,tempR.x1,yBottom));
  ////    //    }
  ////    //  }

  ////    //Draw rectangles
  ////    /*if(xChange>0)
  ////    {
  ////    int xLeftTop = tempR.x0 - 2 >=0 ? tempR.x0 - 2 : 0;
  ////    int xRightBottom = tempR.x1 + 2 + xChange < tempImg.width() ? tempR.x1 + 2 + xChange : tempR.width()-1;
  ////    }
  ////    else
  ////    { if(xChange<0)
  ////    {
  ////    int xLeftTop = tempR.x0 - 2 + xChange >=0 ? tempR.x0 - 2 + xChange : 0;
  ////    int xRightBottom = tempR.x1 + 2  < tempImg.width() ? tempR.x1 + 2 : tempR.width()-1;
  ////    }
  ////    else
  ////    {
  ////    int xLeftTop = tempR.x0 - 2 >=0 ? tempR.x0 - 2 : 0;
  ////    int xRightBottom = tempR.x1 + 2  < tempImg.width() ? tempR.x1 + 2 : tempR.width()-1;
  ////    }

  ////    }

  ////    if(yChange>0)
  ////    {

  ////    int yLeftTop = tempR.y0 - 2 >=0 ? tempR.y0 - 2 : 0;
  ////    int yRightBottom = tempR.y1 + 2 + yChange< tempImg.height() ? tempR.y1 + 2 + yChange : tempR.height()-1;

  ////    }
  ////    else
  ////    {
  ////    if(yChange<0)
  ////    {
  ////    int yLeftTop = tempR.y0 - 2 + yChange >=0 ? tempR.y0 - 2 + yChange : 0;
  ////    int yRightBottom = tempR.y1 + 2< tempImg.height() ? tempR.y1 + 2 : tempR.height()-1;
  ////    }
  ////    else
  ////    {
  ////    int yLeftTop = tempR.y0 - 2 >=0 ? tempR.y0 - 2 + yChange : 0;
  ////    int yRightBottom = tempR.y1 + 2< tempImg.height() ? tempR.y1 + 2 : tempR.height()-1;


  ////    }
  ////    }
  ////    tempRect.set(xLeftTop,yLeftTop,xRightBottom,yRightBottom);
  ////    noCartRegions.push_back(tempRect);*/
  ////    //}
  ////  }

  ////}

  ///*for(int i=0;i<noCartRegions.size();i++)
  //  colorAndForegroundImg.rectangle(noCartRegions[i].x0,noCartRegions[i].y0,noCartRegions[i].x1,noCartRegions[i].y1,PV_RGB(0,0,0));*/


  ////Dilate the color segmented image before doing convex shape
  ////Now perform dilation and erosion on the ANDed output (using ipp)  
  ///*
  //const unsigned int dilatePasses = 1;

  //IppiSize ippRoi = {colorAndForegroundImg.width() -2, colorAndForegroundImg.height() - 2};

  ////Dilation
  //for (int i = 0; i < dilatePasses; i++)
  //{    
  //int status = ippiDilate3x3_8u_C1IR(colorAndForegroundImg.pointer() + colorAndForegroundImg.width() + 1,
  //colorAndForegroundImg.width(), 
  //ippRoi);
  //assert(status == ippStsNoErr);  
  //}

  ////Erosion
  //for (int i = 0; i < dilatePasses; i++)
  //{    
  //int status = ippiErode3x3_8u_C1IR(colorAndForegroundImg.pointer() + colorAndForegroundImg.width() + 1,
  //colorAndForegroundImg.width(), 
  //ippRoi);
  //assert(status == ippStsNoErr);
  //}
  //*/


  //Now that the image is segmented, calculate the connected components
  mConnectedComp.label(colorAndForegroundImg);

  Image8& filteredComponentImg = *mpFilteredComponentImage;   

  for (int i = 0; i < filteredComponentImg.size(); i++)
  {    
    if (mConnectedComp(i) != 0 && (mConnectedComp.comp(mConnectedComp(i)).size >= mMinCartComponentSize || (mConnectedComp.comp(mConnectedComp(i)).ymax - mConnectedComp.comp(mConnectedComp(i)).ymin)*(mConnectedComp.comp(mConnectedComp(i)).xmax - mConnectedComp.comp(mConnectedComp(i)).xmin) >= 100))    
      filteredComponentImg(i) = 255;
    else
    {
      filteredComponentImg(i) = 0;

      //Set color to 0 so not considered for convex hull below
      mConnectedComp.comp(mConnectedComp(i)).color = 0;
    }

  }

 /* for(TracksShapePS::iterator iT=mCartTracks.begin();iT!=mCartTracks.end();iT++)
  {
    (*iT)->mOcclusionStatus = true;
  }*/

  //Now computer the convex hull of these points  std::vector<Polygon> polyList;

  //Create memory storage and the openCV matrices using the storage
  for (int i = 0; i < mConnectedComp.numberOfComps(); i++)
  {

    const PvComp& comp = mConnectedComp.comp(i);

    //ignore black components
    if (comp.color == 0)
      continue;

    //ignore components w/ size < 3
    if (comp.size < 3)
      continue;
    
    //New code by Satish to detect shelves
    if(comp.size <= mMinCartComponentSize || comp.size >= mMaxCartComponentSize)
      continue;
    

    CvPoint* pPoints = new CvPoint[comp.size];
    int* pHull = new int[comp.size];



    //scan through the bounding box for each component, and add its pixels to 
    //the point list

    unsigned int curPos = 0;
    for (int y = comp.ymin; y <= comp.ymax; y++)    
    {
      for (int x = comp.xmin; x <= comp.xmax; x++)      
      {
        if (mConnectedComp(x,y) == i)
        {          
          pPoints[curPos].x = x;
          pPoints[curPos].y = y;
          //PVMSG("%d point found: %d %d\n", i, pPoints[curPos].x, pPoints[curPos].y);
          curPos++;
        }
      }
    }
    assert(curPos == comp.size);

    //now create the matrix objects
    CvMat pointMatrix = cvMat(1, comp.size, CV_32SC2, pPoints);
    CvMat hullMatrix = cvMat(1, comp.size, CV_32SC1, pHull);

    cvConvexHull2(&pointMatrix, &hullMatrix, CV_CLOCKWISE, 0);

    //Now extract the hull polygons from the data, and convert them into a form we can use   
    std::vector<Vector2f> polyPoints;

    int hullCount = hullMatrix.cols;
    for (int a = 0; a < hullCount; a++)
    {
      CvPoint pt = pPoints[pHull[a]];
      //PVMSG("%d %d\n", pt.x, pt.y);

      polyPoints.push_back(Vector2f(pt.x,pt.y));
    }

    //Replace very large components with the predicted cart positions from the cart track
    if(comp.size > mMaxCartComponentSize)
    {
      Polygon tempPolygon(polyPoints);
      for(TracksShapePS::iterator iT=mCartTracks.begin();iT!=mCartTracks.end();iT++)
      {
        Vector2f tempPos = (*iT)->getPredictedPos();
        if (tempPolygon.isPointInside(tempPos))
        {
          //(*iT)->mOcclusionStatus = false;
          Polygon cartPoly;
          (*iT)->getCartPolygon(cartPoly);
          Vector2f motionPos;
          motionPos.x = (*iT)->getPredictedPos().x - (*iT)->getPos().x;
          motionPos.y = (*iT)->getPredictedPos().y - (*iT)->getPos().y;
          cartPoly.translate(Vector2f(int(motionPos.x),int(motionPos.y)));
          mCartPolygons.push_back(cartPoly);
        }
      }
      continue;
    }
    

    mCartPolygons.push_back(Polygon(polyPoints));

    delete[] pPoints;
    delete[] pHull;
  } 
}

void
  PersonShapeTrackerMod::removeCarts(Image8& img, Image8& cartImg)
{

  //Dilate the convex images
  Image8 temp(img.width(),img.height());
  temp.setAll(0);
  
  for (int cartCount = 0; cartCount < mCartPolygons.size(); cartCount++)
  {

    //BUGFIX: DO NOT USE POLYGON::FILL due to memory leak, use getmask() instead
    mCartPolygons[cartCount].fill(temp, 255);
    mCartPolygons[cartCount].draw(temp, 255);
  }


  //Now remove the carts from the given image
/*
  Polygon iPoly;
  for (TracksShapePS::iterator iT = mCartTracks.begin(); iT != mCartTracks.end(); iT++)
  {
    (*iT)->getCartPolygon(iPoly);
    
    iPoly.fill(temp, 255);
    iPoly.draw(temp, 255);

   
  }  
  
  //Added for dilation of the convex hull
  //Dilate temp
  const unsigned int dilatePasses = 1;

  IppiSize ippRoi = {temp.width() -2, temp.height() - 2};

  //Dilation
  for (int i = 0; i < dilatePasses; i++)
  {    
    int status = ippiDilate3x3_8u_C1IR(temp.pointer() + temp.width() + 1,
      temp.width(), 
      ippRoi);
    assert(status == ippStsNoErr);  
  }
*/
  for(int a=0;a<temp.size();a++)
    if(temp(a)>0)
      img(a)=0;
  //Completed dilation
  cartImg = temp;

}

void 
  PersonShapeTrackerMod::removeCartsDebugVisualization(Image32& img) const
{

  //Do nothing if the following images have not been initialized
  if (!mpColorSegmentedImage.get() || !mpColorAndForegroundImage.get() || !mpFilteredComponentImage.get())
    return;

  const Rectanglei roi = mpForegroundSegmentMod->getRoiRectangle(); 
  Vector2i frameSize = getImageAcquireModule()->getFrameSize();

  //TODO: WE ARE COMPLETELY IGNORING ROI CASES THAT DO NOT REST ON ORIGIN
  assert(roi.x0 == 0);
  assert(roi.y0 == 0);

  //Color segmented code
  Image32 tmp;
  ColorConvert::convert(*mpColorSegmentedImage, PvImageProc::GRAY_SPACE, tmp, PvImageProc::RGB_SPACE);
  tmp.rescale(roi.width(),roi.height());
  img.paste(tmp,frameSize.x,0);

  //Now paste ANDed color and segmentation image in the lower left
  ColorConvert::convert(*mpColorAndForegroundImage, PvImageProc::GRAY_SPACE, tmp, PvImageProc::RGB_SPACE);    
  //Image32Ptr pCurrentFrame = getImageAcquireModule()->getCurrentFramePtr();
  //Image32Ptr pRoiImg = mpForegroundSegmentMod->getColorRoiFrame(pCurrentFrame);
  tmp.rescale(roi.width(), roi.height());
  //tmp.paste(*pRoiImg,0,0);
  img.paste(tmp,0,frameSize.y);

  //Now paste filtered components in the lower right
  ColorConvert::convert(*mpFilteredComponentImage, PvImageProc::GRAY_SPACE, tmp, PvImageProc::RGB_SPACE);  

  //draw convex hull polygons on segmentation
  for (int i = 0; i < mCartPolygons.size(); i++)
  {
    mCartPolygons[i].draw(tmp, PV_RGB(255,255,0));      
  }

  tmp.rescale(roi.width(), roi.height());
  img.paste(tmp,frameSize.x,frameSize.y);

  drawCartsOutline(img,PV_RGB(255,255,0));

}

void
  PersonShapeTrackerMod::drawCartsOutline(Image32& img, unsigned int color = PV_RGB(255,255,0)) const
{
  const Rectanglei roi = mpForegroundSegmentMod->getRoiRectangle();
  const Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();

  assert(scaleFactor.x != 0.0);
  assert(scaleFactor.y != 0.0);

  const float scale_x = 1.0f / scaleFactor.x;
  const float scale_y = 1.0f / scaleFactor.y;

  //draw convex hull polygons on top left image after scaling them
  for (int i = 0; i < mCartPolygons.size(); i++)
  {
    std::vector<Vector2f> polyPoints;

    for (int edge = 0; edge < mCartPolygons[i].numEdges(); edge++)
    {
      LineSegment ls = mCartPolygons[i].getEdge(edge);
      polyPoints.push_back(Vector2f(roi.x0+(int)(ls(0).x*scale_x), roi.y0+(int)(ls(0).y*scale_y)));
      polyPoints.push_back(Vector2f(roi.x0+(int)(ls(1).x*scale_x), roi.y0+(int)(ls(1).y*scale_y)));        
    }

    Polygon p(polyPoints);
    p.draw(img,color);      
  }
}

void 
  PersonShapeTrackerMod::detectEdges(const Image8& img, Image8& edgeImg, unsigned int thresh) const
{
  assert(img.width() > 0);
  assert(img.height() > 0);

  edgeImg.resize(img.width(),img.height());  

  Image<short int> imageDx(img.width(),img.height()),imageDy(img.width(),img.height());
  IppiSize roiSize = {img.width(),img.height()};
  IppiMaskSize maskSize = ippMskSize3x3;

  int bufSize;
  Ipp8u *horizBuf=0, *vertBuf=0;
  ippiFilterSobelHorizGetBufferSize_8u16s_C1R(roiSize,ippMskSize3x3,&bufSize);
  horizBuf = new Ipp8u[bufSize];
  ippiFilterSobelVertGetBufferSize_8u16s_C1R(roiSize,ippMskSize3x3,&bufSize);
  vertBuf = new Ipp8u[bufSize];

  ippiFilterSobelHorizBorder_8u16s_C1R(img.pointer(),img.width(),imageDx.pointer(),imageDx.width()*sizeof(short),
    roiSize,ippMskSize3x3,ippBorderConst,0,horizBuf);
  ippiFilterSobelVertBorder_8u16s_C1R(img.pointer(),img.width(),imageDy.pointer(),imageDy.width()*sizeof(short),
    roiSize,ippMskSize3x3,ippBorderConst,0,vertBuf);

  edgeImg.setAll(0);
  for(int x=0;x<edgeImg.size();x++)
  {
    int a=imageDx(x);
    int b=imageDy(x);
	float edgeMagnitude = sqrt((float)a*a + (float)b*b);
    //std::cout<<a<<" "<<b<<std::endl;
//    if(abs(a)>thresh || abs(b)>thresh)
    if(edgeMagnitude>thresh)
      edgeImg(x)=255;    
  }

  delete[] vertBuf;
  delete[] horizBuf;
}

void 
  PersonShapeTrackerMod::combineEdgesAndSeg(const Image8& filteredEdgeImg,
  const Image8& filteredSegImg, 
  Image8& outputImg, 
  float edgeWeight) const
{
  assert(filteredEdgeImg.width() == filteredSegImg.width());
  assert(filteredEdgeImg.height() == filteredSegImg.height());

  outputImg.resize(filteredEdgeImg.width(),filteredEdgeImg.height());
  outputImg.setAll(0);
  float segWeight = 1-edgeWeight;
  for(int i=0;i<filteredEdgeImg.size();i++)
  {
    if(filteredEdgeImg(i)>0 ) // use only edgImg as it willbe a similar image as only segImg
      outputImg(i) = filteredEdgeImg(i)*edgeWeight + filteredSegImg(i)*segWeight;
  }

}

void 
  PersonShapeTrackerMod::overlayPersonMasks(Image32& img) const
{
  const Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();
  const Rectanglei roi = mpForegroundSegmentMod->getRoiRectangle();  

  PVASSERT(scaleFactor.x != 0.0);
  PVASSERT(scaleFactor.y != 0.0);

  const float scale_x = 1.0f / scaleFactor.x;
  const float scale_y = 1.0f / scaleFactor.y;  

  Vector2i offset(roi.x0,roi.y0);

  TracksShapePS::const_iterator iTrack;
  for (iTrack = mTracks.begin(); iTrack != mTracks.end(); iTrack++)
  {      
    int idCurr=(*iTrack)->getId();
    Vector2i pos;
    pos.x = (int)(*iTrack)->getPos().x;
    pos.y = (int)(*iTrack)->getPos().y;

    unsigned int color=PV_RGB(25*(idCurr%10 + 1),80*(idCurr%3 + 1),60*(idCurr%4 + 1));

    Image32 overlayImg(roi.width()*scaleFactor.x,roi.height()*scaleFactor.y);
    overlayImg.setAll(0);

    mpPersonShapeEstimator->drawPersonPixels(pos.x, pos.y,color,overlayImg,offset);
    overlayImg.rescale(roi.width(),roi.height());

    //Now iterate through the image, and blend pixels w/ img
    for (int y = 0; y < overlayImg.height(); y++)
    {
      for (int x = 0; x < overlayImg.width(); x++)
      {
        if (overlayImg(x,y) == 0)
          continue;
        else
        {
          //blend the colors 50%
          const double blendFactor = 0.5;

          unsigned int r1 = RED(img(x,y));
          unsigned int g1 = GREEN(img(x,y));
          unsigned int b1 = BLUE(img(x,y));

          unsigned int r2 = RED(overlayImg(x,y));
          unsigned int g2 = GREEN(overlayImg(x,y));
          unsigned int b2 = BLUE(overlayImg(x,y));

          unsigned int newR = (int)((1.0 - blendFactor)*r2 + (blendFactor)*r1);
          unsigned int newG = (int)((1.0 - blendFactor)*g2 + (blendFactor)*g1);
          unsigned int newB = (int)((1.0 - blendFactor)*b2 + (blendFactor)*b1);

          assert(newR >= 0 && newR <= 255);
          assert(newG >= 0 && newG <= 255);
          assert(newB >= 0 && newB <= 255);

          img(x,y) = PV_RGB(newR,newG,newB);
        }
      }
    }
  }
}

static void drawText(Image32& img0,
                     int x,
                     int y,
                     const std::string& txt,
                     bool transparent = false)
{
#ifdef WIN32
  HFONT hfnt, hOldFont;
  HDC hdc = CreateCompatibleDC(NULL);

  hfnt = (HFONT)GetStockObject(ANSI_FIXED_FONT); 
  if (hOldFont = (HFONT)SelectObject(hdc, hfnt)) 
  {
    SIZE size;
    GetTextExtentPoint(hdc,txt.c_str(),txt.length(),&size);
    SelectObject(hdc, hOldFont); 
    DeleteDC(hdc);

    Uint32 magicColor = PV_RGB(1,1,1);
    PvCanvasImage img(size.cx+2,size.cy+2);
    img.setAll(magicColor);
    hdc = img.dc();
    if (hOldFont = (HFONT)SelectObject(hdc, GetStockObject(ANSI_FIXED_FONT))) 
    {
      POINT pos = { 1 , 1 };
      SetBkMode(hdc,TRANSPARENT);
      if (transparent)
      {
        SetTextColor(hdc,RGB(0,0,0));
        for (int x0 = -1; x0 <= 1; x0++)
        {
          for (int y0 = -1; y0 <= 1; y0++)
          {
            TextOut(hdc, pos.x+x0, pos.y+y0, txt.c_str(), txt.length()); 
          }
        }
      }
      SetTextColor(hdc,RGB(255,255,255));
      TextOut(hdc, pos.x, pos.y, txt.c_str(), txt.length()); 
      SelectObject(hdc, hOldFont); 

      // If transparent, must paste with key color...
      img0.paste(img,x-img.width()/2,y-img.height()/2);
    }
  } 

  GdiFlush();
#endif  
}

void 
  PersonShapeTrackerMod::visualize(Image32& img)
{
  //removeCartsDebugVisualization(img);

  const Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();
  const Rectanglei roi = mpForegroundSegmentMod->getRoiRectangle();

  //NOTE: IN ITS CURRENT FORM, THIS FUNCTION WILL NOT WORK UNLESS THE
  // ORIGIN OF THE ROI MATCHES THE ORIGIN OF THE IMAGE i.e (0,0)
  assert(roi.x0 == 0);
  assert(roi.y0 == 0);

  if (!mPaintVisualization)
  {
    return;
  }  

  if (mVisualizeSnakeTail)
  {
    img.paste(*(mLastColorFrames[0]),0,0);   
  }   

  PVASSERT(scaleFactor.x != 0.0);
  PVASSERT(scaleFactor.y != 0.0);

  const float scale_x = 1.0f / scaleFactor.x;
  const float scale_y = 1.0f / scaleFactor.y;

#define TRANSFORM_X(x) roi.x0+(int)((x)*scale_x)
#define TRANSFORM_Y(y) roi.y0+(int)((y)*scale_y)


  Vector2i frameSize = getImageAcquireModule()->getFrameSize();  

  //Paste 'Raw' (or filtered) foreground in the top right of the visualization
  Image8 rawForeground = *mpForegroundSegmentMod->getForeground();
  if(mFilterSegmentationFlag)
    filterSegmentation(rawForeground,mFilterSegmentationOrder);
  Image32 rawImage;
  rawForeground.rescale(roi.width(),roi.height());
  ColorConvert::convert(rawForeground,PvImageProc::GRAY_SPACE, rawImage, PvImageProc::RGB_SPACE);
  drawCartsOutline(rawImage, PV_RGB(255,0,0));
  img.paste(rawImage,frameSize.x,0);    

  //removeCartsDebugVisualization(img);

  //Now paste the raw segmentation w/o carts in the bottom right
  Image32 tmp;
//  ColorConvert::convert(*mpRawSegmentationImage,PvImageProc::GRAY_SPACE, tmp, PvImageProc::RGB_SPACE);
  Image8 tmp8(*mpAdjustedEdgeImage);
  tmp8.rescale(roi.width(),roi.height());
  ColorConvert::convert(tmp8,PvImageProc::GRAY_SPACE, tmp, PvImageProc::RGB_SPACE);
//  ColorConvert::convert(*mpEdgeImage,PvImageProc::GRAY_SPACE, tmp, PvImageProc::RGB_SPACE)
  drawCartsOutline(tmp, PV_RGB(255,0,0));
  img.paste(tmp,frameSize.x,frameSize.y); 

  //draw 'dead' trajectories that have not yet been cleaned up
  if (mDrawTracksMode == ALL)
  {
    std::list<TrajectoryPtr>::iterator iT;

    for (iT = mPreTrajectories.begin(); iT != mPreTrajectories.end(); iT++)
    {     
      Trajectory::Nodes& nodes = (*iT)->getNodes();
      Trajectory::Nodes::iterator iNode, iPrevNode = nodes.end();
      for (iNode = nodes.begin(); iNode != nodes.end(); iNode++)
      {
        if (iPrevNode != nodes.end())
        {           
          img.line(
            round((*iPrevNode)->location.x),
            round((*iPrevNode)->location.y),
            round((*iNode)->location.x),
            round((*iNode)->location.y),
            PV_RGB(100,100,0));
        }
        iPrevNode = iNode;
      }
    }
  }

  //Visualize the combined segmentation on the lower left
  Image32 filteredForeground;
  ColorConvert::convert(*mpCombinedEdgeAndSegImage,PvImageProc::GRAY_SPACE, filteredForeground, PvImageProc::RGB_SPACE);  

  TracksShapePS::iterator iTrack;
  for (iTrack = mTracks.begin(); iTrack != mTracks.end(); iTrack++)
  {      
    (*iTrack)->visualize(filteredForeground);

    // Don't visualize spurious tracks...
    if ((*iTrack)->getAge() > 0)//mMaxSnakeSize)
    {
      std::vector<Vector2f>& snake = (*iTrack)->getCurrentSnake();

      if (!mVisualizeSnakeTail)
      {

        /*for (int i = 1; i < snake.size(); i++)
        {
        img.line(
        TRANSFORM_X(snake[i-1].x),TRANSFORM_Y(snake[i-1].y),
        TRANSFORM_X(snake[i].x),TRANSFORM_Y(snake[i].y),
        PV_RGB(255,255,255));           
        }    */     

      }
      if (snake.size() > 1)
      {
        int idx = mVisualizeSnakeTail ? 0 : snake.size()-1;
        //float w,h;
        //mpForegroundSegmentMod->getBlobSizeEstimate()->getEstimatedSize(snake[idx].x,snake[idx].y,w,h);          
        //img.ellipse(TRANSFORM_X(snake[idx].x),TRANSFORM_Y(snake[idx].y),w/2*scale_x,h/2*scale_y,PV_RGB(0,0,255));                   

        if (!mVisualizeSnakeTail)
        {
          //img.ellipse(TRANSFORM_X(snake[idx].x),TRANSFORM_Y(snake[idx].y),w*mMinTrackCreationDistance*scale_x,h*mMinTrackCreationDistance*scale_y,PV_RGB(0,255,255));                     
        }
        Trajectory::Nodes& nodes = (*iTrack)->getTrajectory()->getNodes();
        if (!nodes.empty())
        {

        }
      }
      if (!mVisualizeSnakeTail)
      {

        Rectanglei tempRect;
        TracksShapePS::iterator it;
        for(it=mTracks.begin();it!=mTracks.end();it++)
        {
          int idCurr=(*it)->getId();
          tempRect=mpPersonShapeEstimator->getMaskRect((*it)->getPos().x,(*it)->getPos().y);
          int color=PV_RGB(25*(idCurr%10 + 1),80*(idCurr%3 + 1),60*(idCurr%4 + 1));

          //TODO: FIX THIS, IT ASSUMES ROI ORIGIN MATCHES IMAGE ORIGIN
          //img.rectangle(tempRect.x0*4,tempRect.y0*4,tempRect.x1*4,tempRect.y1*4,color);                    
          //img.rectangle(tempRect.x0*scale_x,tempRect.y0*scale_y,tempRect.x1*scale_x,tempRect.y1*scale_y,color);


          if (mDrawTracksMode != NONE)
          {   

            Trajectory::Nodes& nodes = (*it)->getTrajectory()->getNodes();
            Trajectory::Nodes::iterator iNode, iPrevNode = nodes.end();

            for (iNode = nodes.begin(); iNode != nodes.end(); iNode++)
            {           
              if (iPrevNode != nodes.end())
              { 

                img.line(
                  TRANSFORM_X((int)(*iPrevNode)->location.x),
                  TRANSFORM_Y((int)(*iPrevNode)->location.y),
                  TRANSFORM_X((int)(*iNode)->location.x),
                  TRANSFORM_Y((int)(*iNode)->location.y),
                  color);  

                img.line(
                  TRANSFORM_X((int)(*iPrevNode)->location.x) + 1,
                  TRANSFORM_Y((int)(*iPrevNode)->location.y),
                  TRANSFORM_X((int)(*iNode)->location.x) + 1,
                  TRANSFORM_Y((int)(*iNode)->location.y),
                  color);   

                img.line(
                  TRANSFORM_X((int)(*iPrevNode)->location.x) - 1,
                  TRANSFORM_Y((int)(*iPrevNode)->location.y),
                  TRANSFORM_X((int)(*iNode)->location.x) - 1,
                  TRANSFORM_Y((int)(*iNode)->location.y),
                  color);    

                img.line(
                  TRANSFORM_X((int)(*iPrevNode)->location.x),
                  TRANSFORM_Y((int)(*iPrevNode)->location.y)+1,
                  TRANSFORM_X((int)(*iNode)->location.x),
                  TRANSFORM_Y((int)(*iNode)->location.y)+1,
                  color);

                img.line(
                  TRANSFORM_X((int)(*iPrevNode)->location.x),
                  TRANSFORM_Y((int)(*iPrevNode)->location.y)-1,
                  TRANSFORM_X((int)(*iNode)->location.x),
                  TRANSFORM_Y((int)(*iNode)->location.y)-1,
                  color);            

              }
              iPrevNode = iNode;
            }        
          }
        }                
      }
    }
  }      

  //Visualize cart positions
  for(TracksShapePS::iterator iT=mCartTracks.begin();iT!=mCartTracks.end();iT++)
  {
    filteredForeground.ellipseFill((*iT)->getPos().x,(*iT)->getPos().y,2,2,PV_RGB(0,255,0));
  }

  //Visualize region below carts
  /*for(int i=0;i<mBelowCartRegions.size();i++)
  {
    filteredForeground.rectFill(mBelowCartRegions[i].x0,mBelowCartRegions[i].y0,mBelowCartRegions[i].x1,mBelowCartRegions[i].y1,PV_RGB(0,255,0));
  }*/
  //Visualize occluding regions of carts
  //for(int i=0;i<mOccludingCartRectangles.size();i++)
  //{
    //PVMSG("%d %d %d %d\n",mOccludingCartRectangles[i].x0,mOccludingCartRectangles[i].y0,mOccludingCartRectangles[i].x1,mOccludingCartRectangles[i].y1);
    /*std::string tempStr;
    std::ostringstream temp;
    temp<<maxOccludingValues[i];
    tempStr = temp.str();*/
    
    //filteredForeground.rectFill(mOccludingCartRectangles[i].x0,mOccludingCartRectangles[i].y0,mOccludingCartRectangles[i].x1,mOccludingCartRectangles[i].y1,PV_RGB(255,0,0));
    //drawText(filteredForeground,mOccludingCartRectangles[i].getCenter().x,mOccludingCartRectangles[i].getCenter().y,tempStr,false);
    //filteredForeground.ellipseFill((*iT)->getPos().x,(*iT)->getPos().y,2,2,PV_RGB(0,255,0));
  //}

  /*{
    int color = PV_RGB(0,255,0);
    for(TracksShapePS::iterator iT = mCartTracks.begin();iT!=mCartTracks.end();iT++)
    {
    
    Trajectory::Nodes& nodes = (*iT)->getTrajectory()->getNodes();
    Trajectory::Nodes::iterator iNode, iPrevNode = nodes.end();

    for (iNode = nodes.begin(); iNode != nodes.end(); iNode++)
    {           
      if (iPrevNode != nodes.end())
      { 

        filteredForeground.line(
          ((int)(*iPrevNode)->location.x),
          ((int)(*iPrevNode)->location.y),
          ((int)(*iNode)->location.x),
          ((int)(*iNode)->location.y),
          PV_RGB(0,255,0));  

        filteredForeground.line(
          ((int)(*iPrevNode)->location.x) + 1,
          ((int)(*iPrevNode)->location.y),
          ((int)(*iNode)->location.x) + 1,
          ((int)(*iNode)->location.y),
          PV_RGB(0,255,0));   

        filteredForeground.line(
          ((int)(*iPrevNode)->location.x) - 1,
          ((int)(*iPrevNode)->location.y),
          ((int)(*iNode)->location.x) - 1,
          ((int)(*iNode)->location.y),
          PV_RGB(0,255,0));    

        filteredForeground.line(
          ((int)(*iPrevNode)->location.x),
          ((int)(*iPrevNode)->location.y)+1,
          ((int)(*iNode)->location.x),
          ((int)(*iNode)->location.y)+1,
          PV_RGB(0,255,0));

        filteredForeground.line(
          ((int)(*iPrevNode)->location.x),
          ((int)(*iPrevNode)->location.y)-1,
          ((int)(*iNode)->location.x),
          ((int)(*iNode)->location.y)-1,
          PV_RGB(0,255,0));            
  }
    }
    }
  }*/
  //Paste the person positions
  //TODO: FIX BUG THAT ASSUMES ROI ORIGIN MATCHES PERSON ORIGIN
  /* // I DO NOT KNOW WHAT THIS VIS REPRESENTS, COMMENTING OUT FOR NOW
  for(int s=0;s<mPersonPositions.size();s++)
  {
  if(mPersonPositions[s].z==1.0f)
  filteredForeground.ellipseFill(int(mPersonPositions[s].x*scale_x),int(mPersonPositions[s].y*scale_y),5,5,PV_RGB(255,0,255));
  //filteredVis.ellipseFill(int(mPersonPositions[s].x*4),int(mPersonPositions[s].y*4),5,5,PV_RGB(255,0,255));
  else
  filteredForeground.ellipseFill(int(mPersonPositions[s].x*scale_x),int(mPersonPositions[s].y*scale_y),5,5,PV_RGB(255,255,255));
  //filteredVis.ellipseFill(int(mPersonPositions[s].x*4),int(mPersonPositions[s].y*4),5,5,PV_RGB(255,255,255));
  }
  */

  //Added by Rahul
  //Visualize track threshold level
  //int colorArray[10] = {PV_RGB(128,0,0),PV_RGB(255,0,0),PV_RGB(0,128,0),PV_RGB(0,255,0),PV_RGB(0,0,128),PV_RGB(0,0,255),PV_RGB(255,255,0),PV_RGB(255,0,255),PV_RGB(0,255,255),PV_RGB(255,255,255)};
  //for(TracksShapePS::iterator it=mTracks.begin();it!=mTracks.end();it++)
  //{
  //  //There is one case where it will fail if the value lies between 0 and 25
  //  filteredForeground.ellipseFill((*it)->getPos().x,(*it)->getPos().y,2,2,colorArray[(*it)->getPersonDensityValue()/25 - 1]);
  //}
  //Done

  //Visualize person positions
  for(int a=0;a<mvPersonPositions.size();a++)
    filteredForeground.ellipseFill(mvPersonPositions[a].personPosition.x,mvPersonPositions[a].personPosition.y,2,2,PV_RGB(255,0,0));

  for(int a=0;a<mvPersonPositionsLowThresh.size();a++)
    filteredForeground.ellipseFill(mvPersonPositionsLowThresh[a].personPosition.x,mvPersonPositionsLowThresh[a].personPosition.y,2,2,PV_RGB(0,0,255));

  for(int a=0;a<mvOccludedPersonPositions.size();a++)
    filteredForeground.ellipseFill(mvOccludedPersonPositions[a].personPosition.x,mvOccludedPersonPositions[a].personPosition.y,2,2,PV_RGB(255,255,0));

  for(int a=0;a<mNoCartRegions.size();a++)
    filteredForeground.line(mNoCartRegions[a].x0,mNoCartRegions[a].y0,mNoCartRegions[a].x1,mNoCartRegions[a].y1,PV_RGB(255,0,255));



  filteredForeground.rescale(roi.width(),roi.height());
  img.paste(filteredForeground,0,frameSize.y);


  //Draw kill and create track polygons
  std::list<Polygon>::const_iterator iPoly;
  for (iPoly = mStartTrackPolyList.begin(); iPoly != mStartTrackPolyList.end(); iPoly++)
  {
    //iPoly->draw(img, PV_RGB(255,255,0));
    iPoly->draw(img, PV_RGB(0,255,0));
  }

  /*for (iPoly = mKillTrackPolyList.begin(); iPoly != mKillTrackPolyList.end(); iPoly++)
  {
    iPoly->draw(img, PV_RGB(255,0,0));
  }    

  for (iPoly = mPersonTrackRegionList.begin(); iPoly != mPersonTrackRegionList.end(); iPoly++)
  {
    iPoly->draw(img, PV_RGB(0,0,255));
  }  */

  //Draw carts if available
//  drawCartsOutline(img, PV_RGB(255,255,0));  

  overlayPersonMasks(img);
   
  /*Image32 tmpForegroundEdge;
  ColorConvert::convert(*mpDilatedForegroundSegmentationEdgeImage,PvImageProc::GRAY_SPACE, tmpForegroundEdge, PvImageProc::RGB_SPACE);
  //ColorConvert::convert(mTestAdjustedEdgeImage,PvImageProc::GRAY_SPACE, tmpForegroundEdge, PvImageProc::RGB_SPACE);
  //ColorConvert::convert(mTestEdgeImage,PvImageProc::GRAY_SPACE, tmpForegroundEdge, PvImageProc::RGB_SPACE);
  tmpForegroundEdge.rescale(frameSize.x,frameSize.y);
  img.paste(tmpForegroundEdge,frameSize.x,frameSize.y);*/

}



}; // namespace vision

}; // namespace ait


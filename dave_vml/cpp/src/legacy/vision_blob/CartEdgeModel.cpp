
#include "legacy/vision_blob/CartEdgeModel.hpp"

namespace ait 
{

namespace vision
{

CartEdgeModel::CartEdgeModel()
{
}

CartEdgeModel::~CartEdgeModel()
{
}

//
// OPERATIONS
//

void
CartEdgeModel::createEdgeModel(ait::Polygon& cartPolygon, float orientation, int inMargin, int outMargin)
{
  //Parsing any Polygon to get the four vertices in TL, TR, BL, BR order

  int topLeftX, topLeftY, topRightX, topRightY, bottomLeftX, bottomLeftY, bottomRightX, bottomRightY;

  topLeftX=topLeftY=topRightX=topRightY=bottomLeftX=bottomLeftY=bottomRightX=bottomRightY=0;

  std::vector< Vector2f > cartVertices;

  cartPolygon.getVertices(cartVertices);

  for (int vertexCount = 0; vertexCount < cartVertices.size(); vertexCount++)
  {
    if(cartVertices[vertexCount].x < topLeftX && cartVertices[vertexCount].y < topLeftY)
    {
      topLeftX = cartVertices[vertexCount].x;
      topLeftY = cartVertices[vertexCount].y;
    }

    if(cartVertices[vertexCount].x < bottomLeftX && cartVertices[vertexCount].y > bottomLeftY)
    {
      bottomLeftX = cartVertices[vertexCount].x;
      bottomLeftY = cartVertices[vertexCount].y;
    }

    if(cartVertices[vertexCount].x > topRightX && cartVertices[vertexCount].y < topRightY)
    {
      topRightX = cartVertices[vertexCount].x;
      topRightY = cartVertices[vertexCount].y;
    }

    if(cartVertices[vertexCount].x > bottomRightX && cartVertices[vertexCount].y > bottomRightY)
    {
      bottomRightX = cartVertices[vertexCount].x;
      bottomRightY = cartVertices[vertexCount].y;
    }
  }


  PolygonPtr Outer0(new Polygon()), Left0(new Polygon()), Right0(new Polygon()), Top0(new Polygon()), Bottom0(new Polygon()), 
             Outer1(new Polygon()), Left1(new Polygon()), Right1(new Polygon()), Top1(new Polygon()), Bottom1(new Polygon()), 
             TLCorner(new Polygon()), TRCorner(new Polygon()), BLCorner(new Polygon()), BRCorner(new Polygon());

  //Creating the 12 Feature Polygons based on the cart polygon and in & out margins


  // Outside Boundary
  std::vector<Vector2f> vOuter0;
  vOuter0.push_back(Vector2f(topLeftX-outMargin, topLeftY-outMargin));
  vOuter0.push_back(Vector2f(topRightX+outMargin, topRightY-outMargin));
  vOuter0.push_back(Vector2f(bottomRightX+outMargin, bottomRightY+outMargin));
  vOuter0.push_back(Vector2f(bottomLeftX-outMargin, bottomLeftY+outMargin));
  Outer0->setVertices(vOuter0);

  // Left outside strip
  std::vector<Vector2f> vLeft0;
  vLeft0.push_back(Vector2f(topLeftX-outMargin,topLeftY-outMargin));
  vLeft0.push_back(Vector2f(topLeftX,topLeftY-outMargin));
  vLeft0.push_back(Vector2f(bottomLeftX,bottomLeftY+outMargin));
  vLeft0.push_back(Vector2f(bottomLeftX-outMargin,bottomLeftY+outMargin));
  Left0->setVertices(vLeft0);

  // Right outside strip
  std::vector<Vector2f> vRight0;
  vRight0.push_back(Vector2f(topRightX,topRightY-outMargin));
  vRight0.push_back(Vector2f(topRightX+outMargin,topRightY-outMargin));
  vRight0.push_back(Vector2f(bottomRightX+outMargin,bottomRightY+outMargin));
  vRight0.push_back(Vector2f(bottomRightX,bottomRightY+outMargin));
  Right0->setVertices(vRight0);

  // Top outside strip
  std::vector<Vector2f> vTop0;
  vTop0.push_back(Vector2f(topLeftX-outMargin,topLeftY-outMargin));
  vTop0.push_back(Vector2f(topRightX+outMargin,topRightY-outMargin));
  vTop0.push_back(Vector2f(topRightX+outMargin,topRightY));
  vTop0.push_back(Vector2f(topLeftX-outMargin,topLeftY));
  Top0->setVertices(vTop0);

  // Bottom outside strip
  std::vector<Vector2f> vBottom0;
  vBottom0.push_back(Vector2f(bottomLeftX-outMargin,bottomLeftY));
  vBottom0.push_back(Vector2f(bottomRightX+outMargin,bottomRightY));
  vBottom0.push_back(Vector2f(bottomRightX+outMargin,bottomRightY+outMargin));
  vBottom0.push_back(Vector2f(bottomLeftX-outMargin,bottomLeftY+outMargin));
  Bottom0->setVertices(vBottom0);

  // Outside Boundary
  std::vector<Vector2f> vOuter1;
  vOuter1.push_back(Vector2f(topLeftX, topLeftY));
  vOuter1.push_back(Vector2f(topRightX, topRightY));
  vOuter1.push_back(Vector2f(bottomRightX, bottomRightY));
  vOuter1.push_back(Vector2f(bottomLeftX, bottomLeftY));
  Outer1->setVertices(vOuter1);

  // Left outside strip
  std::vector<Vector2f> vLeft1;
  vLeft1.push_back(Vector2f(topLeftX,topLeftY));
  vLeft1.push_back(Vector2f(topLeftX+inMargin,topLeftY));
  vLeft1.push_back(Vector2f(bottomLeftX+inMargin,bottomLeftY));
  vLeft1.push_back(Vector2f(bottomLeftX,bottomLeftY));
  Left1->setVertices(vLeft1);

  // Right outside strip
  std::vector<Vector2f> vRight1;
  vRight1.push_back(Vector2f(topRightX-inMargin,topRightY));
  vRight1.push_back(Vector2f(topRightX,topRightY));
  vRight1.push_back(Vector2f(bottomRightX,bottomRightY));
  vRight1.push_back(Vector2f(bottomRightX-inMargin,bottomRightY));
  Right1->setVertices(vRight1);

  // Top outside strip
  std::vector<Vector2f> vTop1;
  vTop1.push_back(Vector2f(topLeftX,topLeftY));
  vTop1.push_back(Vector2f(topRightX,topRightY));
  vTop1.push_back(Vector2f(topRightX,topRightY+inMargin));
  vTop1.push_back(Vector2f(topLeftX,topLeftY+inMargin));
  Top1->setVertices(vTop1);

  // Bottom outside strip
  std::vector<Vector2f> vBottom1;
  vBottom1.push_back(Vector2f(bottomLeftX,bottomLeftY-inMargin));
  vBottom1.push_back(Vector2f(bottomRightX,bottomRightY-inMargin));
  vBottom1.push_back(Vector2f(bottomRightX,bottomRightY));
  vBottom1.push_back(Vector2f(bottomLeftX,bottomLeftY));
  Bottom1->setVertices(vBottom1);

  //Top Left Corner
  std::vector<Vector2f> vTLCorner;
  vTLCorner.push_back(Vector2f(topLeftX,topLeftY));
  vTLCorner.push_back(Vector2f(topLeftX+inMargin,topLeftY));
  vTLCorner.push_back(Vector2f(topLeftX+inMargin,topLeftY+inMargin));
  vTLCorner.push_back(Vector2f(topLeftX,topLeftY+inMargin));
  TLCorner->setVertices(vTLCorner);

  //Top Right Corner
  std::vector<Vector2f> vTRCorner;
  vTRCorner.push_back(Vector2f(topRightX,topRightY));
  vTRCorner.push_back(Vector2f(topRightX+inMargin,topRightY));
  vTRCorner.push_back(Vector2f(topRightX+inMargin,topRightY+inMargin));
  vTRCorner.push_back(Vector2f(topRightX,topRightY+inMargin));
  TRCorner->setVertices(vTRCorner);

  //Bottom Left Corner
  std::vector<Vector2f> vBLCorner;
  vBLCorner.push_back(Vector2f(bottomLeftX,bottomLeftY));
  vBLCorner.push_back(Vector2f(bottomLeftX+inMargin,bottomLeftY));
  vBLCorner.push_back(Vector2f(bottomLeftX+inMargin,bottomLeftY-inMargin));
  vBLCorner.push_back(Vector2f(bottomLeftX,bottomLeftY-inMargin));
  BLCorner->setVertices(vBLCorner);

  //Bottom Right Corner
  std::vector<Vector2f> vBRCorner;
  vBRCorner.push_back(Vector2f(bottomRightX,bottomRightY));
  vBRCorner.push_back(Vector2f(bottomRightX-inMargin,bottomRightY));
  vBRCorner.push_back(Vector2f(bottomRightX-inMargin,bottomRightY-inMargin));
  vBRCorner.push_back(Vector2f(bottomRightX,bottomRightY-inMargin));
  BRCorner->setVertices(vBRCorner);


  // Vector of all Feature Polygons and FeatureIds

  mPolygonPtrs.push_back(Outer0);
  mFeatureId.push_back(0);
  mPolygonPtrs.push_back(Left0);
  mFeatureId.push_back(1);
  mPolygonPtrs.push_back(Right0);
  mFeatureId.push_back(2);
  mPolygonPtrs.push_back(Top0);
  mFeatureId.push_back(3);
  mPolygonPtrs.push_back(Bottom0);
  mFeatureId.push_back(4);
  mPolygonPtrs.push_back(Outer1);
  mFeatureId.push_back(5);
  mPolygonPtrs.push_back(Left1);
  mFeatureId.push_back(6);
  mPolygonPtrs.push_back(Right1);
  mFeatureId.push_back(7);
  mPolygonPtrs.push_back(Top1);
  mFeatureId.push_back(8);
  mPolygonPtrs.push_back(Bottom1);
  mFeatureId.push_back(9);
  mPolygonPtrs.push_back(TLCorner);
  mFeatureId.push_back(10);
  mPolygonPtrs.push_back(TRCorner);
  mFeatureId.push_back(11);
  mPolygonPtrs.push_back(BLCorner);
  mFeatureId.push_back(12);
  mPolygonPtrs.push_back(BRCorner);
  mFeatureId.push_back(13);

  // Rotate all polygons (background polygon, outer polygon, and inner polygon) according to the given angle
  for(int n = 0; n < mPolygonPtrs.size(); n++)
  {
    (mPolygonPtrs[n])->rotateAndRound(orientation, Vector2f(0.0, 0.0));    
  }

  

  mMask.clear();

  int 	numLeft0 = 0, numRight0 = 0, numTop0 = 0, numBottom0 = 0, numLeft1 = 0, numRight1 = 0, numTop1 = 0, numBottom1 = 0;

  Vector2f bottomLeft = mPolygonPtrs[0]->getBoundingBox(0); 
  Vector2f topRight = mPolygonPtrs[0]->getBoundingBox(1);

  for(int y = topRight.y; y < bottomLeft.y; y++)
  {
    for(int x = bottomLeft.x; x < topRight.x; x++)
    {
      int featureVal = 0;
      for(int n = 0; n < mPolygonPtrs.size(); n++)
      {
        // Fill in the filter values from outside to inside 
        //(but needs to be pushed_back() according to the order at the initialization)
        if(mFeatureId[n] != 0 && mFeatureId[n] != 5 && mPolygonPtrs[n]->isPointInside(Vector2f((float)x, (float)y)))
        {
          //					printf("(%d %d)\n",x,y);
          featureVal = mFeatureId[n];

          if(featureVal==1)
            numLeft0 ++;
          else if(featureVal==2)
            numRight0 ++;
          else if(featureVal==3)
            numTop0 ++;
          else if(featureVal==4)
            numBottom0 ++;
          else if(featureVal==6)
            numLeft1 ++;
          else if(featureVal==7)
            numRight1 ++;
          else if(featureVal==8)
            numTop1 ++;
          else if(featureVal==9)
            numBottom1 ++;

        }
      }
      //mMask.push_back(Vector3i(x, y, featureVal));
      if(featureVal > 0)
        mMask.push_back(Vector3i(x, y, featureVal));
    }
  }

  mFeatureScale.push_back(numLeft0);
  mFeatureScale.push_back(numRight0);
  mFeatureScale.push_back(numTop0);
  mFeatureScale.push_back(numBottom0);
  mFeatureScale.push_back((bottomLeftY-topLeftY-2*inMargin));
  mFeatureScale.push_back((bottomRightY-topRightY-2*inMargin));
  mFeatureScale.push_back((topRightX-topLeftX-2*inMargin));
  mFeatureScale.push_back((bottomRightX-bottomLeftX-2*inMargin));
  mFeatureScale.push_back(inMargin);
  mFeatureScale.push_back(inMargin);
  mFeatureScale.push_back(inMargin);
  mFeatureScale.push_back(inMargin);

}

float
CartEdgeModel::generateMatchScore(ait::Image8 & edgeImage, ait::Vector2f & cartCandidate, int DetectThres)
{
  int candX = cartCandidate.x;
  int candY = cartCandidate.y;

  int Left0Count = 0;
  int Right0Count = 0;
  int Top0Count = 0;
  int Bottom0Count = 0;
  int Left1Count = 0;
  int Right1Count = 0;
  int Top1Count = 0;
  int Bottom1Count = 0;
  int TLCornerCount = 0;
  int TRCornerCount = 0;
  int BLCornerCount = 0;
  int BRCornerCount = 0;

  for(int i = 0; i < mMask.size(); i ++)
  {
    int probX = candX + mMask[i].x;
    int probY = candY + mMask[i].y;

    if(0 <= probX && probX < edgeImage.width() && 0 <= probY && probY < edgeImage.height())
      //if(edgeImage(probX, probY) == 255 && mMask[i].z != 0)
      if(edgeImage(probX, probY) == 255)
      {
        if(mMask[i].z == 1)
        {
          Left0Count ++;
          //					  scoreImage(probX, probY) = PV_RGB(255, 0, 0);
        }
        else if(mMask[i].z == 2)
        {
          Right0Count ++;
          //					  scoreImage(probX, probY) = PV_RGB(0, 255, 0);
        }
        else if(mMask[i].z == 3)
        {
          Top0Count ++;
          //					  scoreImage(probX, probY) = PV_RGB(0, 0, 255);
        }
        else if(mMask[i].z == 4)
        {
          Bottom0Count ++;
          //					  scoreImage(probX, probY) = PV_RGB(255, 255, 0);
        }
        else if(mMask[i].z == 6)
        {
          Left1Count ++;
          //					  scoreImage(probX, probY) = PV_RGB(127, 0, 0);
        }
        else if(mMask[i].z == 7)
        {
          Right1Count ++;
          //					  scoreImage(probX, probY) = PV_RGB(0, 127, 0);
        }
        else if(mMask[i].z == 8)
        {
          Top1Count ++;
          //					  scoreImage(probX, probY) = PV_RGB(0, 0, 127);
        }
        else if(mMask[i].z == 9)
        {
          Bottom1Count ++;
          //					  scoreImage(probX, probY) = PV_RGB(127, 127, 0);
        }
        else if(mMask[i].z == 10)
        {
          TLCornerCount ++;
        }
        else if(mMask[i].z == 11)
        {
          TRCornerCount ++;
        }
        else if(mMask[i].z == 12)
        {
          BLCornerCount ++;
        }
        else if(mMask[i].z == 13)
        {
          BRCornerCount ++;
        }
      }
  }

  float Left0Score, Right0Score, Top0Score, Bottom0Score;
  float Left1Score, Right1Score, Top1Score, Bottom1Score;
  float TLCScore, TRCScore, BLCScore, BRCScore;

  Left0Score = 1.0 - float(Left0Count)/mFeatureScale[0];
  Right0Score = 1.0 - float(Right0Count)/mFeatureScale[1];
  Top0Score = 1.0 - float(Top0Count)/mFeatureScale[2];
  Bottom0Score = 1.0 - float(Bottom0Count)/mFeatureScale[3];



  Left1Score = float(Left1Count)/mFeatureScale[4];
  Right1Score = float(Right1Count)/mFeatureScale[5];
  Top1Score = float(Top1Count)/mFeatureScale[6];
  Bottom1Score = float(Bottom1Count)/mFeatureScale[7];

  Left1Score = std::min<float> (Left1Score,1);
  Right1Score = std::min<float> (Right1Score,1);
  Top1Score = std::min<float> (Top1Score,1);
  Bottom1Score = std::min<float> (Bottom1Score,1);

  TLCScore = std::min<float> (float(TLCornerCount)/2,1.5);
  TRCScore = std::min<float> (float(TRCornerCount)/2,1.5);
  BLCScore = std::min<float> (float(BLCornerCount)/2,1.5);
  BRCScore = std::min<float> (float(BRCornerCount)/2,1.5);


  float CornerScore, Score1, Score0;

  CornerScore = (TLCScore + TRCScore + BLCScore + BRCScore)/4;
  CornerScore = std::min<float> (CornerScore,1);

  Score1 = (Left1Score)*.25+(Top1Score)*.25+(Bottom1Score)*.25+(Right1Score)*.25;
  Score0 = (Left0Score)*.15+(Top0Score)*.25+(Bottom0Score)*.25+(Right0Score)*.35;


  if (Score1 > DetectThres && Score0 > DetectThres && CornerScore > .6)
    mFinalScore = (Score1+Score0)/2; //*3/8+CornerScore/4;
  else
    mFinalScore = 0;

  return(mFinalScore);
}

//
// ACCESS
//

//
// INQUIRY
//

}; // namespace vision

}; // namespace ait


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "legacy/vision_blob/MultiFrameTrack.hpp"
#include <legacy/pv/ait/Image.hpp>
#include <legacy/pv/PvRandom.hpp>
#include <legacy/low_level/Settings.hpp>
#include <legacy/vision_tools/ColorConvert.hpp>
#include <utility>
#include "legacy/vision_blob/BlobSizeEstimate.hpp"

namespace ait 
{

namespace vision
{


#define VALUE_X(i) (pValues[2*(i)])
#define VALUE_Y(i) (pValues[2*(i)+1])

void 
MultiFrameTrack::ThisCondensation::init(int numParticles, int dimension, MultiFrameTrack *pMFT)
{
  Condensation<float>::setParams(numParticles,dimension);
  mpMultiFrameTrack = pMFT;
}

void
MultiFrameTrack::ThisCondensation::predict()
{
  assert(mpMultiFrameTrack);
  mpMultiFrameTrack->predict();
}


MultiFrameTrack::MultiFrameTrack()
: mIsInitialized(false), mIsAlive(false), mId(-1)
{
}

MultiFrameTrack::~MultiFrameTrack()
{
}

void
MultiFrameTrack::predict()
{

  const int snakeSize = mpForegroundHistory->size();

  // Do nothing if there is less than two nodes in the snake.
  if (snakeSize < 2)
  {
    return;
  }

  const int maxSnakeSize = mCondensation.getDimension()/2;
  assert(mpBlobSizeEstimate.get());
  BlobSizeEstimate& blobSizeEstimate = *mpBlobSizeEstimate;
  PvRandom rand;
  PvRandom uniformRand;

  const float minFactor = 0.02f;
  const float maxFactor = 0.1f;
  const float deltaFactor = maxSnakeSize > 1? (maxFactor-minFactor)/(maxSnakeSize-1) : 0;

  int i,j;

  // Angles and magnitudes for all difference vectors of consecutive nodes
  // in the snake. The angles are stored in unitary vectors.
  std::vector<float> magnitudes(snakeSize-1);
  std::vector<float> cosThetas(snakeSize-1);
  std::vector<float> sinThetas(snakeSize-1);  

  for (j = 0; j < mCondensation.getNumParticles(); j++)
  {
    float *pValues = mCondensation.getParticle(j);

    // If the snake is as long as the buffer size, let's shift the values.
    if (mCircularBufferFilled)
    {
      // Shift the nodes of the 'snake'
      for (i = 1; i < snakeSize; i++)
      {
        VALUE_X(i-1) = VALUE_X(i);
        VALUE_Y(i-1) = VALUE_Y(i);
      }
    }

    // Calculate the current angles and vectors.
    for (i = 0; i < snakeSize-2; i++)
    {
      const float dx = VALUE_X(i+1) - VALUE_X(i);
      const float dy = VALUE_Y(i+1) - VALUE_Y(i);
      const float mag = sqrt(dx*dx+dy*dy);
      // Handle the case of zero magnitude
      magnitudes[i] = mag;
      if (mag == 0)
      {
        cosThetas[i] = 1;
        sinThetas[i] = 0;
      }
      else
      {
        cosThetas[i] = dx/mag;
        sinThetas[i] = dy/mag;
      }
    }

    if (snakeSize >= 3)
    {
      // Set the magnitude and angle of the new head equal to
      // the magnitude and angle of the previous segment vector.
      // Note that the head of the snake is at the end of the
      // vector.
      magnitudes[snakeSize-2] = magnitudes[snakeSize-3];
      cosThetas[snakeSize-2] = cosThetas[snakeSize-3];
      sinThetas[snakeSize-2] = sinThetas[snakeSize-3];
    }
    else
    {
      // This case occurs when the snake size is 2.
      // Initialize the first segment with half the blob size magnitude 
      // and a random angle.
      float sizeX,sizeY;
      blobSizeEstimate.getEstimatedSize(VALUE_X(0),VALUE_Y(0),sizeX,sizeY);
      magnitudes[0] = sizeX*mMaxBlobSpeedPerFrame*0.3;
      float theta = uniformRand.uniform0to1()*(2*M_PI);
      cosThetas[0] = cos(theta);
      sinThetas[0] = sin(theta);
      // Also perturb the first element so it can find the best
      // origin.
      VALUE_X(0) = VALUE_X(0) + rand.gaussRand()*sizeX*mMaxBlobSpeedPerFrame;
      VALUE_Y(1) = VALUE_Y(1) + rand.gaussRand()*sizeY*mMaxBlobSpeedPerFrame;
    }

    /* TODO: TEMP DISABLE
    for (i = 0; i < snakeSize-1; i++)
    {
      // Each node will be perturbed by modifying the magnitude
      // and the angle by gaussian noise. The noise will propagate
      // along the nodes of the snake because each new value is based
      // on the previous values.
      const float magnitudeStdDev = 0.2f;
      float newMag = magnitudes[i]*fabs(1+rand.gaussRand()*magnitudeStdDev);

      // Threshold the magnitude based on the blob size (width only).
      float sizeX,sizeY;
      blobSizeEstimate.getEstimatedSize(VALUE_X(i),VALUE_Y(i),sizeX,sizeY);
      const float minMagnitudeFactor = 0.01f;
      const float maxMagnitudeFactor = mMaxBlobSpeedPerFrame;
      newMag = std::min(std::max(sizeX*minMagnitudeFactor,newMag),sizeX*maxMagnitudeFactor);

      const float deltaThetaDegrees = 30.0f;//TODO: RESTORE 10.0f;

      const float deltaTheta = (float)M_PI*deltaThetaDegrees*rand.gaussRand()/180.0f;
      const float cosDeltaTheta = cos(deltaTheta);
      const float sinDeltaTheta = sin(deltaTheta);

      const float newCosTheta = cosThetas[i]*cosDeltaTheta - sinThetas[i]*sinDeltaTheta;
      const float newSinTheta = sinThetas[i]*cosDeltaTheta + cosThetas[i]*sinDeltaTheta;

      VALUE_X(i+1) = VALUE_X(i) + newMag*newCosTheta;
      VALUE_Y(i+1) = VALUE_Y(i) + newMag*newSinTheta;
    }
    */

    //try peturbing only the last node of the snake
    const float magnitudeStdDev = 0.2f;

    float newMag = magnitudes[i]*fabs(1+rand.gaussRand()*magnitudeStdDev);

    // Threshold the magnitude based on the blob size (width only).
    float sizeX,sizeY;
    blobSizeEstimate.getEstimatedSize(VALUE_X(i),VALUE_Y(i),sizeX,sizeY);
    const float minMagnitudeFactor = 0.01f;
    const float maxMagnitudeFactor = mMaxBlobSpeedPerFrame;
    newMag = std::min(std::max(sizeX*minMagnitudeFactor,newMag),sizeX*maxMagnitudeFactor);

    const float deltaThetaDegrees = 180.0f;//TODO: ORIGINAL VALUE 10.0f;    

    const float deltaTheta = (float)M_PI*deltaThetaDegrees*rand.gaussRand()/180.0f;
    const float cosDeltaTheta = cos(deltaTheta);
    const float sinDeltaTheta = sin(deltaTheta);

    const float newCosTheta = cosThetas[i]*cosDeltaTheta - sinThetas[i]*sinDeltaTheta;
    const float newSinTheta = sinThetas[i]*cosDeltaTheta + cosThetas[i]*sinDeltaTheta;

    VALUE_X(snakeSize - 1) = VALUE_X(snakeSize - 2) + newMag*newCosTheta;
    VALUE_Y(snakeSize - 1) = VALUE_Y(snakeSize - 2) + newMag*newSinTheta;
  }

  if (snakeSize == maxSnakeSize)
  {
    mCircularBufferFilled = true;
  }
}

  
void 
MultiFrameTrack::init(int id,
                      const std::string& settingsPath, 
                      const Vector2i& initPos,
                      BlobSizeEstimatePtr pBSE,
                      double startTime,
                      double fps,
                      bool useAlternateTracker)
{
  //HACK
  mUseAlternateTracker = useAlternateTracker;

  //END HACK
  

  // Initialize this object.

  Settings set(settingsPath);

  int numFrames = set.getInt("numFrames",10);
  mpForegroundHistory.reset(new ForegroundHistory(numFrames));

  mCondensation.init(set.getInt("numParticles",200),numFrames*2,this);  

  assert(pBSE.get());
  mpBlobSizeEstimate = pBSE;

  mSnake.clear();
  mSnake.push_back(Vector2f(initPos.x,initPos.y));
  mCircularBufferFilled = false;

  mIsInitialized = true;
  mIsAlive = true;
  mAge = 0;

  mId = id;

  //mStartTime = startTime;

  PVASSERT(fps != 0.0);
  mMaxBlobSpeedPerFrame = set.getFloat("maxBlobSpeedPerSecond",7.5f) / fps;

  mBoundingBoxNonZeroesRatio = set.getFloat("boundingBoxNonZeroesRatio",0.8f);

  mpTrajectory.reset(new Trajectory(id));

  //
  mForceVector.x = 0.0;
  mForceVector.y = 0.0;
  
  mMaxNoMotionTime = 0.0;
  mIsMotionEnabled = false;
}

//
// OPERATIONS
//

float
MultiFrameTrack::evaluateOptimizationFunction(float* pValues, int numParticle, int numPhase)
{

  const int snakeSize = mpForegroundHistory->size();

  const Vector2f pos1 = getPos();

  Vector2f pos2;
  pos2.x = VALUE_X(snakeSize - 1);
  pos2.y = VALUE_Y(snakeSize - 1);

  if (mIsMotionEnabled)
  {
    int x = pos2.x;
    int y = pos2.y;

    assert(mpLastMotion.get());
    x = std::min((int)(mpLastMotion->width()-1), x);
    x = std::max(0, x);
    y = std::min((int)(mpLastMotion->height()-1), y);
    y = std::max(0, y);

    if ((*mpLastMotion)(x, y) > mMaxNoMotionTime)
      return 0.0;
  }


  if (mUseAlternateTracker) //use experimental tracker
  {    
    float foregroundMatch = evaluateForegroundMatching(pValues);
    
    foregroundMatch = std::min(foregroundMatch, 1.0f); //ORIG 0.2f);   
  
    float prefLocScore = 0.0;
  
    
    float w = 0.0;
    float h = 0.0;
    assert(mpBlobSizeEstimate.get());
    mpBlobSizeEstimate->getEstimatedSize(pos1.x,pos1.y,w,h);

    //Pos based on preferred location

    Vector2f prefPos;

    float dirWeight = 0.9f;

    prefPos.x = pos1.x + ((dirWeight)*mDirectionVector.x 
      + (1.0 - dirWeight)*mForceVector.x) * w;

    prefPos.y = pos1.y + ((dirWeight)*mDirectionVector.y 
      + (1.0 - dirWeight)*mForceVector.y) * h;

    
    float d = sqrt(dist2(prefPos,pos2));

    //define max distance from newPref that will receive a bonus
    float maxDist = w*2.0; //blob widths

    //calc score based on proximity to preferred pos
    prefLocScore = -d/maxDist + 1.0;

    //prefLocScore = 1.0/(1.0 + d);

    prefLocScore = std::min(prefLocScore,1.0f);
    prefLocScore = std::max(prefLocScore,0.0f);

    float foregroundBias = 0.1f;// WAS 0.1f;

    float weight = 0.0;

    if (foregroundMatch > 0.0)
    {
      weight = foregroundBias * foregroundMatch + (1.0-foregroundBias)*prefLocScore;
    }      
    
    return weight;
  }
  else //use original doorcounter tracker
  {
    float d1 = sqrt(dist2(pos1,pos2)); 

    float distWeight = d1/1000;

    float foregroundMatch = evaluateForegroundMatching(pValues);
    //const float pathCoherence = evaluatePathCoherence(pValues);
    //const float pathOverlap = evaluatePathOverlap(pValues);
    //const float foregroundBias = 0.5f;
  
    foregroundMatch = std::min(foregroundMatch, 0.2f); //ORIG 0.2f);
  
    //penlalize track for moving when it is already 'satisfied' with current segmentation
    //weight = std::max(weight - distWeight, 0.0f);    
    
    float prefLocScore = 0.0;
  
    if (mForceVector.x != 0.0 || mForceVector.y != 0.0)
    {    
      float w = 0.0;
      float h = 0.0;
      assert(mpBlobSizeEstimate.get());
      mpBlobSizeEstimate->getEstimatedSize(pos1.x,pos1.y,w,h);

      //Pos based on preferred location

      Vector2f prefPos;
      prefPos.x = pos1.x + mForceVector.x * w;
      prefPos.y = pos1.y + mForceVector.y * h;

      float d = sqrt(dist2(prefPos,pos2));

      //define max distance from newPref that will receive a bonus
      float maxDist = w*1.0; //blob widths    

      //calc score based on proximity to preferred pos
      prefLocScore = -d/maxDist + 1.0;

      //prefLocScore = 1.0/(1.0 + d);

      prefLocScore = std::min(prefLocScore,1.0f);
      prefLocScore = std::max(prefLocScore,0.0f);

    }  

    float foregroundBias = .5;

    float weight = 0.0;

    if (foregroundMatch > 0.0)
    {
      weight = foregroundBias * foregroundMatch + (1.0-foregroundBias)*prefLocScore;
    }
      
    //ORIGINAL WEIGHT FUNCTION
    //float weight = foregroundMatch*((foregroundBias)*foregroundMatch + (1.0f-foregroundBias)*(pathCoherence*pathOverlap));
  
    //saveParticleImage(pValues,weight);
    ///*if (foregroundMatch > 0)*/ saveParticleTrajectoryImage(pValues,weight);

    return weight;
  }  
}

/*
float
MultiFrameTrack::evaluatePathOverlap(float *pValues)
{ 
  // no tracks have been processed yet
  if (mpProcessedTracks.size() == 0)
  {     
    return 1.0;
  }
  else
  {
    
    const int numFrames = mpForegroundHistory->size();

    float product = 1.0; //the multiplication of the other scores

    Tracks::iterator iTrack;
    for (iTrack = mpProcessedTracks.begin(); iTrack != mpProcessedTracks.end(); iTrack++)
    {
      //find overlap with tracks already processed
      for (int i = 0; i < std::min(mSnake.size(), (*iTrack)->mSnake.size()); i++)
      {
        
        //this tracks data        
        float x1 = mSnake[i].x;  
        float y1 = mSnake[i].y;        

        float w1 = 0.0;
        float h1 = 0.0;
        mpBlobSizeEstimate->getEstimatedSize(x1,y1,w1,h1);

        
        Rectanglef r1;
        r1.x0 = x1 - w1/2.0;
        r1.x1 = x1 + w1/2.0; 
        r1.y0 = y1 - h1/2.0;
        r1.y1 = y1 + h1/2.0;
        
        
        //the other tracks data
        float x2 = (*iTrack)->mSnake[i].x;
        float y2 = (*iTrack)->mSnake[i].y;
        float w2 = 0.0;
        float h2 = 0.0;
        mpBlobSizeEstimate->getEstimatedSize(x2,y2,w2,h2);
        
        Rectanglef r2;
        r2.x0 = x2 - w2/2.0;
        r2.x1 = x2 + w2/2.0;
        r2.y0 = y2 - h2/2.0;
        r2.y1 = y2 + h2/2.0;
        
        Rectanglef intersectRect;
        
        if (intersectRect.intersect(r1,r2))
        {           
          assert(r1.width()*r1.height() > 0.0);
          product *= (1.0 - (intersectRect.width()*intersectRect.height()) / (r1.width()*r1.height()));
        }        
      }
    }
    //saveOverlapImage(mpProcessedTracks, product);

    return product;
  }  
}
*/

float
MultiFrameTrack::evaluateForegroundMatching(float* pValues)
{
  float foregroundWeight = 0.0f;

  assert(mpForegroundHistory.get());
  const int numFrames = mpForegroundHistory->size();  

  int i;
  // Calculate how much it matches the template.
  for (i = 0; i < numFrames; i++)
  {
    assert((*mpForegroundHistory)[i].get());
    const Image8& foreground = *((*mpForegroundHistory)[i]);

    const float x = VALUE_X(i);
    const float y = VALUE_Y(i);
    
    if (x < 0 || x > foreground.width()-0.5 ||
      y < 0 || y > foreground.height()-0.5)
    {
      return 0;
    }   


    float width, height;

    assert(mpBlobSizeEstimate);
    mpBlobSizeEstimate->getEstimatedSize(x,y,width,height);

    // TODO: TEMP CODE FOR EXPERIMENT
    width /= 2.0f;
    height /= 2.0f;

    width = std::min(width, 2.0f);
    height = std::min(height, 2.0f);
    //    

    if (mUseAlternateTracker)
    {
      foregroundWeight += (float)foreground(x,y)/255.0f;
    }
    else
    {
      Rectanglei box;    

      box.set(
        std::max(round(x - width/2.0), 0),
        std::max(round(y - height/2.0), 0),
        std::min(round(x + width/2.0), (int)foreground.width()),
        std::min(round(y + width/2.0), (int)foreground.height())
        );

      int intensityCount = 0;
      for (int by = box.y0; by < box.y1; by++)
        for (int bx = box.x0; bx < box.x1; bx++)
          intensityCount += foreground(bx,by);

      int area = box.width() * box.height();
      PVASSERT(area != 0); 
    
      foregroundWeight += ((float)intensityCount/(float)area)/255.0f;    
    }    
  } 

  return foregroundWeight/numFrames;
}

float
MultiFrameTrack::evaluatePathCoherence(float* pValues)
{
  assert(mpForegroundHistory.get());
  const int numFrames = mpForegroundHistory->size();

  // Now calculate the path coherence
  float coherence = 1;

	// weights for angle and deviation
	const float angleWeight = 0.5f;
	const float speedWeight = 1.0f - angleWeight;

  if (numFrames < 3)
  {
    coherence = 1;
  }
  else
  {
    for (int i = 1; i < numFrames-1; i++)
    {
		  const float diffx1 = VALUE_X(i) - VALUE_X(i-1);
		  const float diffy1 = VALUE_Y(i) - VALUE_Y(i-1);
		  const float mag1 = sqrt(diffx1*diffx1 + diffy1*diffy1);
		  
		  const float diffx2 = VALUE_X(i+1) - VALUE_X(i);
		  const float diffy2 = VALUE_Y(i+1) - VALUE_Y(i);
		  const float mag2 = sqrt(diffx2*diffx2 + diffy2*diffy2);

      // Calculate the cosine of the angle between the two vectors, 
      // which will be between -1 and 1. then add 1 and divide by
      // two to normalize.
      const float angle = ((diffx1*diffx2 + diffy1*diffy2)/(mag1*mag2)+1)/2;

      // The following expression will equal to 1 when the two magnitudes
      // are equal. Then the value reduces.
      const float speed = 2*sqrt(mag1*mag2)/(mag1+mag2);

      // The total coherence for these two consecutive vectors is
      // weighted.
      const float vectorCoherence = angleWeight*angle + speedWeight*speed;

      // Since we want all the segments to be coherent, we must
      // multiply the values.
      coherence *= vectorCoherence;
    }
  }  
  return coherence;
}

Vector2f
MultiFrameTrack::getSnakeNodeBlobSize(int idx)
{
  PVASSERT(idx < mSnake.size());
  // Make a weighted average of each location
  // of the given node per particle particles.

  int i;
  assert(mpBlobSizeEstimate.get());
  BlobSizeEstimate& blobSizeEstimate = *mpBlobSizeEstimate;
  Rectanglef blobExtent(0,0,0,0);

  float w,h;
  for (i = 0; i < mCondensation.getNumParticles(); i++)
  {
    const float *pValues = mCondensation.getParticle(i);
    const float weight = mCondensation.getParticleWeight(i);
    const float x = VALUE_X(idx);
    const float y = VALUE_Y(idx);
    blobSizeEstimate.getEstimatedSize(x,y,w,h);
    blobExtent.set(
      blobExtent.x0 + (x-w/2)*weight,
      blobExtent.y0 + (y-h/2)*weight,
      blobExtent.x1 + (x+w/2)*weight,
      blobExtent.y1 + (y+h/2)*weight
      );
  }

  // We know the weights are already normalized to add up to one.
  return Vector2f(
    blobExtent.width(),
    blobExtent.height());
}

void
MultiFrameTrack::cleanForegroundSegmentation(Image8& img)
{
  PVASSERT(!mSnake.empty());
  const Vector2f pt(mSnake.back());
  assert(mpBlobSizeEstimate.get());
  BlobSizeEstimate& blobSizeEstimate = *mpBlobSizeEstimate;

  // Get the size of the blob at the head of the snake.
  Vector2f size = getSnakeNodeBlobSize(mSnake.size()-1);

  const float expandBy = 1.2f;// TODO: RESTORE 1.2f;
  size.mult(expandBy);

  img.ellipseFill(
    round(pt.x),
    round(pt.y),
    size.x/2,
    size.y/2,
    0);
    
}

Vector2f 
MultiFrameTrack::getSize()
{
  assert(mpBlobSizeEstimate.get());
  BlobSizeEstimate& blobSizeEstimate = *mpBlobSizeEstimate;
  float w,h;
  blobSizeEstimate.getEstimatedSize(getPos().x,getPos().y,w,h);
  return Vector2f(w,h);
}

Vector2i
MultiFrameTrack::getImagePos(int imgWidth, int imgHeight)
{
  Vector2f pos = getPos();
  return Vector2i(
    std::max(0, std::min(imgWidth-1, ait::round(pos.x))),
    std::max(0, std::min(imgHeight-1, ait::round(pos.y))));
}


void
MultiFrameTrack::update(Image8Ptr pForeground, double currentTime)
{
  assert(mpForegroundHistory.get());
  assert(pForeground.get());

  mpForegroundHistory->push_back(pForeground);  

  const int dimension = mpForegroundHistory->capacity();

  // Build particle vector as required by the signature.
  Matrix<float> values(2*dimension);
  values.setAll(0);
  float *pValues = values.pointer();

  int i;
  for (i = 0; i < mSnake.size(); i++)
  {
    VALUE_X(i) = mSnake[i].x;
    VALUE_Y(i) = mSnake[i].y;
  }

  mCondensation.step(values,evalFunctionStub,this);

  // Rebuild the snake.
  mSnake.clear();
  for (i = 0; i < mpForegroundHistory->size(); i++)
  {
    mSnake.push_back(Vector2f(VALUE_X(i),VALUE_Y(i)));
  }

  const bool useMaxParticle = false;

  if (useMaxParticle)
  {
    // Get the maximum particle.
    float maxParticleWeight = 0;
    int maxParticleIndex = 0;

    for (int pi = 0; pi < mCondensation.getNumParticles(); pi++)
    {
      if (maxParticleWeight < mCondensation.getParticleWeight(pi))
      {
        maxParticleWeight = mCondensation.getParticleWeight(pi);
        maxParticleIndex = pi;
      }
    }

    pValues = mCondensation.getParticle(maxParticleIndex);
    for (i = 0; i < mSnake.size(); i++)
    {
      mSnake[i].set(VALUE_X(i),VALUE_Y(i));
    }
  }

  // Update the trajectory with the tail of the snake:
  if (mCircularBufferFilled)
  {    
    float sizeX,sizeY;
    assert(mpBlobSizeEstimate.get());
    mpBlobSizeEstimate->getEstimatedSize(mSnake[0].x,mSnake[0].y,sizeX,sizeY);
    const float x = mSnake[0].x;
    const float y = mSnake[0].y;
    Trajectory::NodePtr pNode(Trajectory::NodePtr(new Trajectory::Node(
      currentTime, //replaces commented out next line
      //mStartTime+ mAge-mSnake.size(), //REPLACED WITH ACTUAL TIME INFORMATION
      Vector2f(x,y),
      Rectanglef(x-sizeX/2,y-sizeY/2,x+sizeX/2,y+sizeY/2))));
    calculateNodeCardinality(*pNode,*((*mpForegroundHistory)[mpForegroundHistory->size()-mSnake.size()]));
    mpTrajectory->getNodes().push_back(pNode);
  }
  // Check if this track is done
  float minSnakeSize = 3;
  if (mSnake.size() >= minSnakeSize && mCondensation.areAllWeightsZero())
  {
    kill();
  }
  else
  {
    mAge++;
  }
}

void
MultiFrameTrack::kill()
{
  mIsAlive = false;  
}

void
MultiFrameTrack::calculateNodeCardinality(Trajectory::Node& node, const Image8& foreground)
{
  // The method is to measure the number of nonzeroes in the filtered
  // foreground image in a square that is twice the area of the blob size,
  // Then the expected number of nonzeroes for a single blob is equal to
  // half of the pixels (we are scanning twice the area).

  const Rectanglef& bbox = node.boundingBox;
  const Rectanglei blobRect(
    std::max(0,round(bbox.x0-bbox.width()*0.25f)),
    std::max(0,round(bbox.y0-bbox.height()*0.25f)),
    std::min(round(bbox.x1+bbox.width()*0.25f),(int)foreground.width()),
    std::min(round(bbox.y1+bbox.height()*0.25f),(int)foreground.height())
    );

  float numForegroundPixels = 0;
  for (int iy = blobRect.y0; iy < blobRect.y1; iy++)
  {
    for (int ix = blobRect.x0; ix < blobRect.x1; ix++)
    {
      numForegroundPixels += foreground(ix,iy) > 0 ? 1 : 0;
    }
  }

  const float expectedBlobRatio = 0.5f*mBoundingBoxNonZeroesRatio;
  const float ratio = numForegroundPixels/(blobRect.width()*blobRect.height());

  node.cardinality = std::max(1.0f,ratio/expectedBlobRatio);
}

void
MultiFrameTrack::saveParticleImage(float *pValues,float weight)
{
  if (mCircularBufferFilled) 
  {
    std::string fname = aitSprintf("test/weight_%0.02f.png",weight);
    if (!PvUtil::fileExists(fname.c_str()))
    {
      float scale = 1;
      const Rectanglef bound(0,0,(*mpForegroundHistory)[0]->width(),(*mpForegroundHistory)[0]->height());
      Image32 img(bound.width()*mSnake.size()*scale,bound.height()*scale);
      for (int j = 0; j < mSnake.size(); j+=2)
      {
        const Image8& foreground = *((*mpForegroundHistory)[j]);

        Image32 fimg(foreground.width(),foreground.height());
        ColorConvert::convert(foreground,PvImageProc::GRAY_SPACE,
          fimg,PvImageProc::RGB_SPACE);

        img.paste(fimg,fimg.width()*j,0);

        for (int i = 0; i < mSnake.size(); i++)
        {
          Vector2f X0(VALUE_X(i-1),VALUE_Y(i-1));
          Vector2f X1(VALUE_X(i),VALUE_Y(i));
          X0.mult(scale);
          X1.mult(scale);
          X0.add(Vector2f(fimg.width()*j,0));
          X1.add(Vector2f(fimg.width()*j,0));
          img.line(X0.x,X0.y,X1.x,X1.y,PV_RGB(255,0,0));
        }
        Vector2f X0(VALUE_X(j-1),VALUE_Y(j-1));
        X0.mult(scale);
        X0.add(Vector2f(fimg.width()*j,0));
        float w,h;
        assert(mpBlobSizeEstimate.get());
        mpBlobSizeEstimate->getEstimatedSize(VALUE_X(j),VALUE_Y(j),w,h);
        img.ellipse(X0.x,X0.y,w/2*scale,h/2*scale,PV_RGB(0,0,255));
      }
      img.save(fname.c_str());
    }
  }

}

void
MultiFrameTrack::saveParticleTrajectoryImage(float *pValues,float weight)
{
  // Visualize the particle and save it to a file.
  if (mCircularBufferFilled) 
  {
    std::string fname = aitSprintf("test/weight_%0.02f.png",weight);
    if (!PvUtil::fileExists(fname.c_str()))
    {
      float scale = 4;
      const Rectanglef bound(0,0,(*mpForegroundHistory)[0]->width(),(*mpForegroundHistory)[0]->height());
      Image32 img(bound.width()*scale,bound.height()*scale);
      img.setAll(0);
      for (int i = 1; i < mSnake.size(); i++)
      {
        Vector2f X0(VALUE_X(i-1),VALUE_Y(i-1));
        Vector2f X1(VALUE_X(i),VALUE_Y(i));
        X0.mult(scale);
        X1.mult(scale);
        img.line(X0.x,X0.y,X1.x,X1.y,PV_RGB(255,0,0));
        img.circle(X0.x,X0.y,2,PV_RGB(0,100+155*(i-1)/mSnake.size(),0));
        img.circle(X1.x,X1.y,2,PV_RGB(0,100+155*i/mSnake.size(),0));
      }
      img.save(fname.c_str());
    }
  }
}

void 
MultiFrameTrack::visualize(Image32& img)
{
  int i;
  int pi;

  float maxParticleWeight = 0;
  int maxParticleIndex = 0;

  for (pi = 0; pi < mCondensation.getNumParticles(); pi++)
  {
    if (maxParticleWeight < mCondensation.getParticleWeight(pi))
    {
      maxParticleWeight = mCondensation.getParticleWeight(pi);
      maxParticleIndex = pi;
    }
  }

  for (pi = 0; pi < mCondensation.getNumParticles(); pi++)
  {
    for (i = 1; i < mSnake.size(); i++)
    {
      float *pValues = mCondensation.getParticle(pi);
      img.line(
        VALUE_X(i-1),VALUE_Y(i-1),
        VALUE_X(i),VALUE_Y(i),
        PV_RGB((int)(255*mCondensation.getParticleWeight(pi)/maxParticleWeight),0,0));
    }
  }

  for (i = 1; i < mSnake.size(); i++)
  {
    img.line(
      mSnake[i-1](0),mSnake[i-1](1),
      mSnake[i](0),mSnake[i](1),
      PV_RGB(255,255,0));
  }

  if (mSnake.size() > 1)
  {
    int idx = mSnake.size()-1;
    float w,h;
    assert(mpBlobSizeEstimate.get());
    mpBlobSizeEstimate->getEstimatedSize(mSnake[idx].x,mSnake[idx].y,w,h);
    img.ellipse(mSnake[idx].x,mSnake[idx].y,w/2,h/2,PV_RGB(0,0,255));
  }

  //Draw the force vector as a green line
  int idx = mSnake.size()-1;
  Vector2f vec = mForceVector;
  float w = 0.0;
  float h = 0.0;

  float xPos = mSnake[idx].x;
  float yPos = mSnake[idx].y;

  assert(mpBlobSizeEstimate.get());
  mpBlobSizeEstimate->getEstimatedSize(xPos,yPos,w,h);
  vec.x *= w;
  vec.y *= h;

  img.line(xPos, yPos, xPos + vec.x, yPos + vec.y, PV_RGB(0,255,0));
  

  vec = mDirectionVector;
  vec.x *= w;
  vec.y *= h; 

  //Draw the direction vector as a yellow line  
  img.line(xPos,yPos, xPos + vec.x, yPos + vec.y, PV_RGB(255,255,0));
  

}

//
// ACCESS
//

//
// INQUIRY
//

}; // namespace vision

}; // namespace ait


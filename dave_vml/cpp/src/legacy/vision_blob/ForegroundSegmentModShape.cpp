/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#pragma warning(disable:4503)
#endif

#include <legacy/vision_tools/PolygonGetMask.hpp>

#include "legacy/vision_blob/ForegroundSegmentModShape.hpp"

#include <legacy/vision_blob/ForegroundSegmentMod.hpp>

#include <legacy/vision_tools/BackgroundLearner.hpp>
#include <legacy/vision/ImageAcquireMod.hpp>
#include <legacy/types/DateTime.hpp>
#include <legacy/vision_tools/ColorConvert.hpp>
//#include <ippcv.h>	// no IPP
//#include <ipp.h>
#include<legacy/math/Polygon.hpp>




namespace ait
{

namespace vision
{

ForegroundSegmentModShape::ForegroundSegmentModShape() : ForegroundSegmentMod(getModuleStaticName())
{  
}

void
ForegroundSegmentModShape::init()
{    
  //initialize old FSM stuff to be safe
  ForegroundSegmentMod::init();


  //BUGFIX: Check to see if the BlobSizeEstimator is enabled. If it is,
  // throw an error and quit
  if (mSettings.getString("BlobSizeEstimator/enabled [bool]", "") == "1")
  {
    PvUtil::exitError("ERROR: BlobSizeEstimator erroneously enabled");
  }


  assert(mSettings.getString("equalizeInput [bool]", "ERROR") != "ERROR");
  mEqualizeGrayImage = mSettings.getBool("equalizeInput [bool]", false);

  //Flag to use edges for segmentation
  //mUseEdge = mSettings.getBool("UseEdge [bool]", false);

  Vector2i frameSize = getImageAcquireModule()->getFrameSize();  

  //Get scale factor
  Vector2f scaleFactor = mSettings.getCoord2f("roiScaleFactor",Vector2f(1.0,1.0));
  const int subSampledWidth = frameSize.x * scaleFactor.x;
  const int subSampledHeight = frameSize.y * scaleFactor.y;

  //Now recompute the ROI mask because the size has changed
  Image8 currentMask(mRegionOfInterest.width(), mRegionOfInterest.height());

  mpRoiMask.reset(new Image8(mRegionOfInterest.width(), mRegionOfInterest.height()));

  std::list<int>::const_iterator iEvtChan;
  for (iEvtChan = mEventChannels.begin(); iEvtChan != mEventChannels.end(); iEvtChan++)
  {
    std::vector<Vector2f> vertices;
    for (int i = 0; i < mRoiPolygon[*iEvtChan].size(); i++)
    {
      vertices.push_back(Vector2f(mRoiPolygon[*iEvtChan][i].x-mRegionOfInterest.x0,
                         mRoiPolygon[*iEvtChan][i].y-mRegionOfInterest.y0));
    }

    //mpRoiMask.reset(new Image8(mRegionOfInterest.width(), mRegionOfInterest.height()));
    mpRoiMask->setAll(0);


    getMask(vertices,Rectanglef(0.0,0.0,mpRoiMask->width(),mpRoiMask->height()),currentMask);

    for (int y = 0; y < mpRoiMask->height(); y++)
    {
      for (int x = 0; x < mpRoiMask->width(); x++)
      {
        (*mpRoiMask)(x,y) = (*mpRoiMask)(x,y) | currentMask(x,y);
      }
    }
  }

  //Scale image according to scale factor
  mpRoiMask->rescale(scaleFactor.x*mRegionOfInterest.width(),scaleFactor.y*mRegionOfInterest.height());  

  //PVMSG("mpRoiMask after: %d %d\n", mpRoiMask->width(), mpRoiMask->height());
  //TODO: MOTION HACK NEEDS TO BE RESIZED HERE

  //mpRoiMask->save("e:/work/tmp/roimask.pgm");

  //CODEBOOK hack goes here
  mUseCodebook = mSettings.getBool("useCodebook",false);
  if(mUseCodebook)
    mUseEqualizeCodebook = mSettings.getBool("EqualizeCodebook",false);
  mCodebookLearnInterval = mSettings.getDouble("codebookLearnInterval",5.0);

  mCodebookStartupList = mSettings.getDoubleList("codebookStartup","60.0,300.0,1800.0");

  if (mUseCodebook)
  {    
    BackgroundModelCbPtr pTmp;

    std::list<double>::iterator iVal;

    for (iVal = mCodebookStartupList.begin(); iVal != mCodebookStartupList.end(); iVal++)
    {
      pTmp.reset(new BackgroundModelCb(mSettings.getPath() + "/BackgroundModelCb"));
      pTmp->setStartLearnDuration(*iVal);
      mLearningCbList.push_back(pTmp);
      mAvgIntensity.push_back(Vector2f(0.0,0.0));
    }

    //Add the default learning interval to the end of the learning list
    pTmp.reset(new BackgroundModelCb(mSettings.getPath() + "/BackgroundModelCb"));
    mLearningCbList.push_back(pTmp);

    mAvgIntensity.push_back(Vector2f(0.0,0.0));

    mFirstCodebookPass = true;
  }

  //Code for motion hack learning

  //Hack for using motion based learning mechanism for learning background
  mUseMotionHackLearning=mSettings.getBool("useMotionHackLearning",false);
  if (mUseMotionHackLearning)
  {
    mPixelThreshold = mSettings.getInt("pixelThreshold",10);
    mFullAreaThreshold = mSettings.getFloat("fullAreaThreshold",0.25);
    mLearningTemporalThreshold = mSettings.getInt("learningTemporalThreshold",75);
    mPastImage.resize(round(subSampledWidth), round(subSampledHeight));
    mPastImage.setAll(0);
    Settings btmSettings("VisionMods/" + getExecutionPathName() + "/PersonShapeTrackerMod");
    Image8 tmp;
    std::list<SimplePolygon>::const_iterator iPoly;
    std::list<Polygon> mKillTrackPolyList;
    std::list<SimplePolygon> killTrackPolyList;
    killTrackPolyList = convertList(btmSettings.getString("startTrackPolygon", ""));

    for (iPoly = killTrackPolyList.begin(); iPoly != killTrackPolyList.end(); iPoly++)
    {
      SimplePolygon killTrackPoly;
      killTrackPoly = *iPoly;

      Polygon p;

      //Remove last redundant entry, polygon class does not use it
      killTrackPoly.pop_back();
      p.setVertices(killTrackPoly);  
      mKillTrackPolyList.push_back(p);
    }

    Rectanglef roi;
    roi.set(0,0,frameSize.x,frameSize.y);
//    PVMSG("%d %d",subSampledWidth,subSampledHeight);
    
    mROIMask.resize(frameSize.x,frameSize.y);
    mROIMask.setAll(0);
    for (iPoly = killTrackPolyList.begin(); iPoly != killTrackPolyList.end(); iPoly++)
    {
      getMask(*iPoly, roi, tmp);
      //tmp.save("C:/temp2.pgm");
      for (int i = 0; i < mROIMask.size(); i++)
        mROIMask(i) |= tmp(i);
    }

    mROIMask.rescale(round(subSampledWidth), round(subSampledHeight),RESCALE_SKIP);
    //mROIMask.save("C:/temp.pgm");
  }

}

Image32Ptr
ForegroundSegmentModShape::getColorRoiFrame(Image32Ptr pImg)
{  
  Image32Ptr pResized;  

  if (mRegionOfInterest == Rectanglei(0,0,pImg->width(),pImg->height()))
  {    
    pResized.reset(new Image32());
    *pResized = *pImg;
  }
  else
  {
    //PVMSG("HERE\n");
    pResized.reset(new Image32(mRegionOfInterest.width(),mRegionOfInterest.height()));
    pResized->crop(*pImg,mRegionOfInterest.x0,mRegionOfInterest.y0,
      mRegionOfInterest.width(),mRegionOfInterest.height());
  }

  //PVMSG("%d %d\n", (int)(mRegionOfInterest.width()*mRoiScaleFactor.x),(int)(mRegionOfInterest.height()*mRoiScaleFactor.y));  

  //Image8Ptr pGrayFrame(new Image8(pResized->width(),pResized->height()));
  //ColorConvert::convert(*pResized,PvImageProc::RGB_SPACE,*pGrayFrame,PvImageProc::GRAY_SPACE);

  pResized->rescale(
    (int)(mRegionOfInterest.width()*mRoiScaleFactor.x),
    (int)(mRegionOfInterest.height()*mRoiScaleFactor.y),
    RESCALE_SKIP); 

  // Apply polygon mask
  if (mpRoiMask.get())
  {
    Image8& mask = *mpRoiMask;
    Image32& dst = *pResized;
    int x,y;
    for (y = 0; y < mask.height(); y++)
    {
      for (x = 0; x < mask.width(); x++)
      {
        if (mask(x,y) == 255)
        {
        }
        else
        {
          dst(x,y) = PV_RGB(0,0,0); //mask(x,y) & dst(x,y);
        }
      }
    }
  }

  return pResized;

}

void 
ForegroundSegmentModShape::process()
{
  //PVMSG("Shape process\n");

//  mBenchmark.beginSample();

  Image32Ptr pFrame = getImageAcquireModule()->getCurrentFramePtr();

  Image8 tempImage;

  if (mUseCodebook)
  {
    Image32Ptr pRgbFrame = getColorRoiFrame(pFrame);
    if(mUseEqualizeCodebook)
    {
      tempImage.resize(pRgbFrame->width(),pRgbFrame->height());
      ColorConvert::convert(*pRgbFrame,PvImageProc::RGB_SPACE,tempImage,PvImageProc::GRAY_SPACE);
      ColorConvert::convert(tempImage,PvImageProc::GRAY_SPACE,*pRgbFrame,PvImageProc::RGB_SPACE);
    }


    //Get the avg. intensity of the image
    //float currAverage=0.0;
    //for(int x=0;x<pRgbFrame->size();x++)
    //  currAverage+=sqrt(float(RED((*pRgbFrame)(x))*RED((*pRgbFrame)(x))+GREEN((*pRgbFrame)(x))*GREEN((*pRgbFrame)(x))+BLUE((*pRgbFrame)(x))*BLUE((*pRgbFrame)(x))));
    //currAverage/=float(pRgbFrame->size());

    double currentTime = getCurrentTime();

    if (mFirstCodebookPass)
    {
      mpForeground.reset(new Image8(pRgbFrame->width(), pRgbFrame->height()));      
      mpForeground->setAll(0);

      mWhiteMask.resize(pRgbFrame->width(), pRgbFrame->height());
      mWhiteMask.setAll(255);

      //HACK, SET TIME TO ZERO TO FORCE LEARN ON FIRST FRAME
      mCodebookLastLearnTime = currentTime;

      //mLastModelSwapTime = currentTime;
      mFirstCodebookPass = false;


      //WE WANT TO LEARN ON THE FIRST FRAME
      std::list<BackgroundModelCbPtr>::iterator iBm;

      for (iBm = mLearningCbList.begin(); iBm != mLearningCbList.end(); iBm++)
      {
        (*iBm)->learn(*pRgbFrame, mWhiteMask,currentTime);
      }

       /* std::list<Vector2f>::iterator iAv;
      for (iAv = mAvgIntensity.begin(); iAv != mAvgIntensity.end(); iAv++)
      {
        (*iAv).x = (currAverage + (*iAv).x*(*iAv).y)/((*iAv).y + 1);
        (*iAv).y = (*iAv).y + 1.0f;
      }*/
    }

    //check the top of the learning cb stack, if it is finished learning, swap models and remove from the list

    assert(mLearningCbList.size() > 0);

    if (mLearningCbList.front()->isFinishedLearning())
    {
      mpActiveCb = mLearningCbList.front();
      mLearningCbList.pop_front();

      //mModelIntensity = mAvgIntensity.front().x;
      //mAvgIntensity.pop_front();

      //Add another learning interval on the back if there are none left
      if (mLearningCbList.empty())
      {
        BackgroundModelCbPtr pTmp(new BackgroundModelCb(mSettings.getPath() + "/BackgroundModelCb"));
        mLearningCbList.push_back(pTmp);
       // mAvgIntensity.push_back(Vector2f(0.0,0.0));
      }      
      PVMSG("SWAPPING MODELS, %d STILL LEARNING\n", mLearningCbList.size());
    }    

    if (mpActiveCb.get())
    {
      Image32& currentImage = *pRgbFrame;
      //Translate the image
      /*for(int x=0;x<pRgbFrame->size();x++)
      {
        int r = int(RED(currentImage(x)) - currAverage + mModelIntensity);
        int g = int(GREEN(currentImage(x)) - currAverage + mModelIntensity);
        int b = int(BLUE(currentImage(x)) - currAverage + mModelIntensity);

        r = std::min(r,255);
        g = std::min(g,255);
        b = std::min(b,255);

        r = std::max(r,0);
        g = std::max(g,0);
        b = std::max(b,0);

        currentImage(x) = PV_RGB(r,g,b);
      }*/

      mpActiveCb->foregroundSegment(*pRgbFrame, mWhiteMask, *mpForeground, currentTime);
    }

    if (currentTime > mCodebookLastLearnTime + mCodebookLearnInterval)
    {
      std::list<BackgroundModelCbPtr>::iterator iBm;

      for (iBm = mLearningCbList.begin(); iBm != mLearningCbList.end(); iBm++)
      {
        (*iBm)->learn(*pRgbFrame, mWhiteMask,currentTime);
      }

      /*std::list<Vector2f>::iterator iAv;
      for (iAv = mAvgIntensity.begin(); iAv != mAvgIntensity.end(); iAv++)
      {
        (*iAv).x = (currAverage + (*iAv).x*(*iAv).y)/((*iAv).y + 1);
        (*iAv).y = (*iAv).y + 1.0f;
      }*/

      mCodebookLastLearnTime = currentTime;
    }

    //

  }
  else
  {  
    /*if(mUseEdge)
    {
      IppiSize srcSize = { pFrame->width(),pFrame->height() };
      IppiMaskSize maskSize;
      maskSize=ippMskSize3x3;
      IppiBorderType border = ippBorderConst;
      Ipp8u *tempBuf,*horizBuf,*vertBuf;
      int sizeBuf;
      ippiCannyGetSize(srcSize,&sizeBuf);
      tempBuf = new Ipp8u[sizeBuf];

      Image<short int> dxImg(pFrame->width(),pFrame->height()),dyImg(pFrame->width(),pFrame->height());
      Image8 grayOut(pFrame->width(),pFrame->height()),grayImg1(pFrame->width(),pFrame->height());
      grayOut.setAll(0);
      dxImg.setAll(0);
      dyImg.setAll(0);
      ColorConvert::convert(*pFrame,PvImageProc::RGB_SPACE,grayImg1,PvImageProc::GRAY_SPACE);
      ippiFilterSobelHorizGetBufferSize_8u16s_C1R(srcSize,maskSize,&sizeBuf);
      horizBuf = new Ipp8u[sizeBuf];
      ippiFilterSobelHorizBorder_8u16s_C1R(grayImg1.pointer(),grayImg1.width(),dxImg.pointer(),dxImg.width()*sizeof(short),srcSize,maskSize,border,0,horizBuf);
      ippiFilterSobelVertGetBufferSize_8u16s_C1R(srcSize,maskSize,&sizeBuf);
      vertBuf = new Ipp8u[sizeBuf];
      ippiFilterSobelVertBorder_8u16s_C1R(grayImg1.pointer(),grayImg1.width(),dyImg.pointer(),dyImg.width()*sizeof(short),srcSize,maskSize,border,0,vertBuf);

      ippiCanny_16s8u_C1R(dxImg.pointer(),dxImg.width()*sizeof(short),dyImg.pointer(),dyImg.width()*sizeof(short),grayOut.pointer(),grayOut.width(),srcSize,30.0f,60.0f,tempBuf);
      Image8Ptr pGrayFrame = getRoiFrame(pFrame); 
      if (!mpForeground.get())
      {
        mpForeground.reset(new Image8(pGrayFrame->width(), pGrayFrame->height())); 
      }
      else
      {
        mpForeground->rescale(grayOut,pGrayFrame->width(),pGrayFrame->height());
      }
      delete []tempBuf;

    }
    else*/
    {
      const bool useGray = true;    
      
      Image8Ptr pGrayFrame;
      Image32Ptr pRgbFrame;

      if (useGray)
        pGrayFrame = getRoiFrame(pFrame);  
      else
        pRgbFrame = getColorRoiFrame(pFrame);

      if (!mpForeground.get())
      {
        if (useGray)
          mpForeground.reset(new Image8(pGrayFrame->width(), pGrayFrame->height()));      
        else
          mpForeground.reset(new Image8(pRgbFrame->width(), pRgbFrame->height()));

        mpForeground->setAll(0);
      }

      if (useGray)
      {
        assert(pGrayFrame.get());    
        //equalize image if requested
        if (mEqualizeGrayImage)
        {
          //PVMSG("Equalizing...\n");
          PvImageProc::EqualizeImage(*pGrayFrame);
        }
      }
      else
        assert(pRgbFrame.get());

      //TEMPORARY CHECK FOR AUTOEXPOSURE, Todo: move to a separate function, clean up this section

      if (mCheckForExposure)
      {  
        if (mLastMeanIntensity == 0.0f)
        {
          mLastMeanIntensity = 1.0;//pGrayFrame->mean();


          //FIX THIS BUG
          mLastMeanSeg = 0;//HACK //mpBackgroundLearner->getForegroundImage()->mean();      
          mLastExposureCheck = getCurrentTime();
        }
        else
        {
          double time = getCurrentTime();

          if (time > mLastExposureCheck + 1.0)
          {
            mLastExposureCheck = time;

            assert(mpBackgroundLearner.get());
            assert(mpBackgroundLearner->getForegroundImage().get());
            double meanSeg = mpBackgroundLearner->getForegroundImage()->mean();
            double meanIntensity = 1.0; //pGrayFrame->mean();

            //Print out average intensity value
            //PVMSG("MEAN INTENSITY: %f\tMEAN SEGMENTATION: %f\n", meanIntensity, meanSeg);        

            //150 based on experimentation on 4 short clips
            if (meanSeg > 150.0)
            {
              mpBackgroundLearner->resetIntervals();
              mpBackgroundLearner->resetValid();
              //mpBackgroundLearner->reset();

              if (mMotionHack)
              { 
                assert(false); //MAKE SURE MOTION HACK WORKS BEFORE UNCOMMENTING BELOW CODE

                //            mMotionTime.setAll(mMinNoMotionTime * 2.0);
                //
                //            //hack change added
                //            
                //            //if (!mUseMotion)
                //            //  (*mpLastSafeImage) = (*pColorFrame);
                //              
                //            (*mpLastSafeImage) = (*pGrayFrame);

             }

             //write fake event to logfile for later checking
             //PVMSG("WRITING TO FILE!!! - %s\n", mExposureLogFilename.c_str());          
             std::ofstream logFile(mExposureLogFilename.c_str(), std::ios::app);                   

             logFile << DateTime(time).toString();
             logFile << ",1.0";
             logFile << "," << mLastMeanIntensity;
             logFile << "," << meanIntensity;
             logFile << "," << mLastMeanSeg;
             logFile << "," << meanSeg << std::endl;
             logFile.close();

           }

           mLastMeanIntensity = meanIntensity;
           mLastMeanSeg = meanSeg;
         }        
       }
     }  
     //END TEMP


      if (mUseMotion)
      {
        mLastFrames.push_back(pGrayFrame);
        Image8Ptr pDiffImage(new Image8(pGrayFrame->width(),pGrayFrame->height()));
        if (mLastFrames.size() < mNumMotionFrames)
        {
          pDiffImage->zero();
        }
        else
        {
          pDiffImage = getDiffImage(mLastFrames);
        }
        pGrayFrame = pDiffImage;    
      }  

      if (mMotionHack)
      { 
        assert(false); //MAKE SURE MOTION HACK WORKS BEFORE UNCOMMENTING BELOW CODE   
      }
      else
      {
        //ORIGINAL CODE
        if (mUseMotion)
        { 
          //assert(false); //MAKE SURE USE MOTION WORKS BEFORE UNCOMMENTING BELOW CODE            
          if (mLearnBackgroundContinuously)        
            mpBackgroundLearner->process(pGrayFrame,true);      
          else
            mpBackgroundLearner->process(pGrayFrame,false); //do not update background model

          mpForeground = mpBackgroundLearner->getForegroundImage();
        }
        else    
        {       

          if (mLearnBackgroundContinuously)
          {
            //Code added for motion hack for learning the background used at service counters
            if(mUseMotionHackLearning)
        {
          bool learnBackground;
          int totalPixels=0,motionPixels=0;
          for(int s=0;s<mPastImage.size();s++)
          {
            if(mROIMask(s)>0)
            {
              totalPixels++;
              //PVMSG("HEre\n");
              if ((int((mPastImage(s))-int((*pGrayFrame)(s)))>(mPixelThreshold)) || ((mPastImage(s)-(*pGrayFrame)(s))<(-1*mPixelThreshold)))
              {
                motionPixels++;
              }
            }
          }
          /*if(float(motionPixels)/float(totalPixels)>0)
          PVMSG("%f\n",float(motionPixels)/float(totalPixels));*/
          if (float(motionPixels)/float(totalPixels)>mFullAreaThreshold)
          {
            //PVMSG("RESETTING\n");
            mMotionLearningCounter=0;
          }
          else
          {
            mMotionLearningCounter++;
          }
          if(mMotionLearningCounter>mLearningTemporalThreshold)
          {
            //PVMSG("RELEARNING\n");
            learnBackground=true;
          }
          else
          {

            learnBackground=false;
          }
          if (useGray)
            mpBackgroundLearner->process(pGrayFrame,learnBackground);
          else
            mpBackgroundLearner->process(pRgbFrame,learnBackground);
          mPastImage = *pGrayFrame;
        }
        else
        {
          if (useGray)
            mpBackgroundLearner->process(pGrayFrame,true);
          else
            mpBackgroundLearner->process(pRgbFrame,true);
        }
          }
          else
          {
            if (useGray)
              mpBackgroundLearner->process(pGrayFrame,false); //do not update background model
            else
              mpBackgroundLearner->process(pRgbFrame,false); //do not update background model
          }

          if (useGray)
          {
            mpForeground = mpBackgroundLearner->getForegroundImage();
          }
          else
          {
            //AND NORMALIZED AND STD FOREGROUND TO REMOVE SHADOWS
            const Image8& bg1 = *mpBackgroundLearner->getForegroundImage();
            const Image8& bg2 = *mpBackgroundLearner->getForegroundNormalizedImage();
            Image8& bgFinal = *mpForeground;

            for (int i = 0; i < bg1.size(); i++)
            {
              bgFinal(i) = bg1(i) & bg2(i);
            }
          }       
        }
      }  
    }

  }

  mCurrentProgressUnits += 1;
//  mpVmsPacket->updateProgress(mCurrentProgressUnits/mTotalProgressUnits);
//  mBenchmark.endSample();
}

void 
ForegroundSegmentModShape::visualize(Image32& img)
{
  if (!mPaintVisualization)
  {
    return;
  }

  // Clear areas outside the image.
  img.rectFill(mFrameWidth,0,mFrameWidth*2,mFrameHeight,0);

  const Vector2f scaleFactor = getRoiScaleFactor();  

  const Rectanglei roi = getRoiRectangle();

  const float scale_x = 1.0f / scaleFactor.x;
  const float scale_y = 1.0f / scaleFactor.y;

  {
    Image32 img1;
    //ColorConvert::convert(*mpBackgroundLearner->getForegroundImage(),PvImageProc::GRAY_SPACE,
    //  img1,PvImageProc::RGB_SPACE);

    ColorConvert::convert(*mpForeground,PvImageProc::GRAY_SPACE,
      img1,PvImageProc::RGB_SPACE);

    img1.rescale(img1.width()*scale_x,img1.height()*scale_y);        

    img.paste(img1,mFrameWidth+roi.x0,roi.y0);
  }

  //DRAW ROI
  std::list<int>::const_iterator iEvtChan;
  for (iEvtChan = mEventChannels.begin(); iEvtChan != mEventChannels.end(); iEvtChan++)
  {
    int i;
    for (i = 1; i < mRoiPolygon[*iEvtChan].size(); i++)
    {
      img.line(
        mRoiPolygon[*iEvtChan][i-1].x,mRoiPolygon[*iEvtChan][i-1].y,
        mRoiPolygon[*iEvtChan][i].x,mRoiPolygon[*iEvtChan][i].y,
        PV_RGB(0,255,128));        
    }
  }   
}

  //
  // INQUIRY
  //

}; // namespace vision

}; // namespace ait


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#pragma warning(disable:4503)
#endif

#include "legacy/vision_blob/BlobTrackerModShape.hpp"

#include <legacy/vision_tools/PolygonGetMask.hpp>

#include <legacy/vision_tools/ColorConvert.hpp>

#include <legacy/types/DateTime.hpp>

#include <legacy/types/strutil.hpp>
#include <boost/lexical_cast.hpp>

#include <legacy/vision/ImageAcquireMod.hpp>

namespace ait 
{

namespace vision
{

BlobTrackerModShape::BlobTrackerModShape()
: RealTimeMod(getModuleStaticName())
{  
}

void 
BlobTrackerModShape::initExecutionGraph(std::list<VisionModPtr>& srcModules)
{
  RealTimeMod::initExecutionGraph(srcModules);  
  
  getModule("ForegroundSegmentModShape",mpForegroundSegmentMod);
  srcModules.push_back(mpForegroundSegmentMod);    
}

BlobTrackerModShape::~BlobTrackerModShape()
{
}

void 
BlobTrackerModShape::init()
{  
  
  RealTimeMod::init();   
  

  mpVmsPacket.reset(new VmsPacket(this));

  Image32Ptr pFrame = getImageAcquireModule()->getCurrentFramePtr();

  assert(pFrame.get());
  mFrameWidth = pFrame->width();
  mFrameHeight = pFrame->height(); 

  mEventChannels = mSettings.getIntList("eventChannelId", "", false);  
  
  mBenchmark.init(mSettings.getPath(),"Blob Tracking");

  mIsTopViewCamera = mSettings.getBool("isTopViewCamera [bool]",0);

  mVerbose = mSettings.getBool("Visualization/printMessages [bool]",0);

  mPaintVisualization = mSettings.getBool("Visualization/enabled [bool]",1);

  mSendExtendedAttributes = mSettings.getBool("sendExtendedAttributes [bool]",0);

  mSendExtendedEventData = mSettings.getBool("sendExtendedEventData [bool]", 0);

  mSendPreprocessTrajectories = mSettings.getBool("sendPreprocessTrajectories [bool]",0);

  

  //Alter the visualization frame size if necessary
  if (mPaintVisualization)
    getImageAcquireModule()->setVisualizationFrameSize(Vector2i(640,480));

  mMaxSnakeSize = mSettings.getInt("numFrames",10);
  mMinTrackCreationDistance = mSettings.getFloat("minTrackCreationDistance",1.0);

  mpNewTrackScanImage.reset(new Image8());

  mNewTrackId = 1;

  mMaxConcurrentTracks = mSettings.getInt("maxConcurrentTracks",10);

  mVisualizeSnakeTail = mSettings.getBool("Visualization/showFrameAtTail [bool]",0);

  if (mVisualizeSnakeTail)
  {
    mLastColorFrames.setCapacity(mMaxSnakeSize);
  }

  mPostProcessLowerInterval = mSettings.getFloat("minPostProcessInterval",60.0);
  mPostProcessUpperInterval = mSettings.getFloat("maxPostProcessInterval", 600.0);
  
  mNextPostProcessTime = getCurrentTime() + mPostProcessLowerInterval;
  mNextForcePostProcessTime = getCurrentTime() + mPostProcessUpperInterval;

  //Initialize the map of event polygons
  std::list<SimplePolygon> polyList = convertList(mSettings.getString("eventsPolygon",""));
  std::list<SimplePolygon>::const_iterator iPoly;
  
  std::list<int>::const_iterator iEvtChan = mEventChannels.begin();
  for (iPoly = polyList.begin(); iPoly != polyList.end(); iPoly++)
  {    
    mEventsPolygon[*iEvtChan] = *iPoly;
    iEvtChan++;
  }

  //Tell the vms packet that each event channel will await completion notification
  for (iEvtChan = mEventChannels.begin(); iEvtChan != mEventChannels.end(); iEvtChan++)
    mpVmsPacket->enableCompletionNotification(*iEvtChan);

  
   

  if (!mEventsPolygon.empty() && mEventChannels.size() != mEventsPolygon.size())
    PvUtil::exitError("ERROR: BlobTrackerMod: Different number of event channel IDs and polygons");

  mSavePreTrajectories = mSettings.getBool("savePreTrajectories [bool]", false);
  mSaveCsvFileEvents = mSettings.getBool("saveCsvFileEvents [bool]", false);

  mTrajectoryOutputFolder = mSettings.getString("trajectoryOutputFolder", "");

  if (mSavePreTrajectories || mSaveCsvFileEvents)  
    if (!PvUtil::fileExists(mTrajectoryOutputFolder.c_str()))
      PvUtil::exitError("BlobTrackerMod: ERROR: 'trajectoryOutputFolder' does not exist.");
  


  //mMaxPreTrajectories = mSettings.getInt("maxPreTrajectories", 500);

  /*
  //DISABLE THIS, WE ARE NO LONGER USING THE BLOB SIZE ESTIMATOR
  if (!mpForegroundSegmentMod->getBlobSizeEstimate().get())
  {
    PvUtil::exitError("The blob size estimator must be enabled in the ForegroundSegmentMod.");
  }*/

  //SavedTrajectories.clear();

  //assert(mpForegroundSegmentMod->getBlobSizeEstimate().get());
    /*
  mTrajEvents.init(mpForegroundSegmentMod->getBlobSizeEstimate());    
  mTrajEvents.setLineThreshold(
    mSettings.getFloat("TrajectoryEvents/LineThreshold", 0.0));
  mTrajEvents.setPolyThreshold(
    mSettings.getFloat("TrajectoryEvents/PolyThreshold", 0.0));
  mTrajEvents.setCardTimeThreshold(
    mSettings.getFloat("TrajectoryEvents/CardTimeThreshold", 2.0)
    * getFramesPerSecond());
    */  
    
  //guaranteed to be unique by outside process that inits config files  
  mVideoSourceId = atoi(Settings::replaceVars("<VideoSourceId>",mSettings.getPath()).c_str());
  PVASSERT(mVideoSourceId != -1);

  mSendTrajectoriesAsEventAttribute = mSettings.getBool("sendTrajectory [bool]",false);

  //Resize the motion module to have the same size as the foreground segmentation size

  /*
  //TODO: MAKE SURE THAT THIS HACK WORKS WHEN UNCOMMENTING CODE
  if (mIsMotionEnabled)
  {  
    Rectanglei roi = mpForegroundSegmentMod->getRoiRectangle();
    Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor(); 

    mMaxNoMotionTime = mSettings.getFloat("maxNoMotionTime", 60.0);

    mpMotionMod->setScaleAndRoi(scaleFactor, roi);    
  }
  */


  ///
  //initialize the start polygon list
  std::list<SimplePolygon> startTrackPolyList;
  startTrackPolyList = convertList(mSettings.getString("startTrackPolygon","(0,0,320,0,320,240,0,240,0,0)"));
  
  for (iPoly = startTrackPolyList.begin(); iPoly != startTrackPolyList.end(); iPoly++)
  {
    SimplePolygon startTrackPoly;
    startTrackPoly = *iPoly;

    Polygon p;

    //Remove last redundant entry, polygon class does not use it
    startTrackPoly.pop_back();
    p.setVertices(startTrackPoly);

    //Add polygon object to start track list
    mStartTrackPolyList.push_back(p);
  }  

  //initialize the kill track polygon
  std::list<SimplePolygon> killTrackPolyList;
  killTrackPolyList = convertList(mSettings.getString("killTrackPolygon", ""));

  for (iPoly = killTrackPolyList.begin(); iPoly != killTrackPolyList.end(); iPoly++)
  {
    SimplePolygon killTrackPoly;
    killTrackPoly = *iPoly;

    Polygon p;

    //Remove last redundant entry, polygon class does not use it
    killTrackPoly.pop_back();
    p.setVertices(killTrackPoly);  

    mKillTrackPolyList.push_back(p);
  }
  

  //get ROI in pixel coords from FSM 
  Rectanglei intRoi = mpForegroundSegmentMod->getRoiRectangle();
  Rectanglef roi((float)intRoi.x0, 
    (float)intRoi.y0, 
    (float)intRoi.x1, 
    (float)intRoi.y1);


  //generate the masks  
  Image8 tmp;

  mStartTrackMask.resize(round(roi.width()), round(roi.height()));
  mStartTrackMask.setAll(0);
  for (iPoly = startTrackPolyList.begin(); iPoly != startTrackPolyList.end(); iPoly++)
  {
    getMask(*iPoly, roi,tmp);
    for (int i = 0; i < mStartTrackMask.size(); i++)
      mStartTrackMask(i) |= tmp(i);
  }

  mKillTrackMask.resize(round(roi.width()), round(roi.height()));
  mKillTrackMask.setAll(0);
  for (iPoly = killTrackPolyList.begin(); iPoly != killTrackPolyList.end(); iPoly++)
  {
    getMask(*iPoly, roi,tmp);
    for (int i = 0; i < mKillTrackMask.size(); i++)
      mKillTrackMask(i) |= tmp(i);
  }

  //rescale the mask by the appropriate scaling factor
  Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();

  mStartTrackMask.rescale(
    roi.width()*scaleFactor.x,
    roi.height()*scaleFactor.y);

  mKillTrackMask.rescale(
    roi.width()*scaleFactor.x,
    roi.height()*scaleFactor.y); 

  mRepelTracks = mSettings.getBool("repelTracks", false);

  //Read in the track creation thresholds
  mTrackCreationThresholds = mSettings.getIntList("trackCreationThresholds", "242,229,204,178");      

  //INITIALIZE PERSON SHAPE ESTIMATOR  

  mpFilteredForeground.reset(new Image8(1,1));

  mpPersonShapeEstimator.reset(new PersonShapeEstimator());
  mpPersonShapeEstimator->init(mSettings.getPath() + "/PersonShapeEstimator");  

  //TEMP for debugging  
  /*
  Image32 fImg;
  fImg.load("e:/work/tmp/testfilter.png");
  Image8 tempImg;
  ColorConvert::convert(fImg, PvImageProc::RGB_SPACE, tempImg, PvImageProc::GRAY_SPACE);

  Image8 outImg;

  mpPersonShapeEstimator->filterImage(outImg, tempImg, Vector2i(10,7));

  Image32 colOutImg;
  ColorConvert::convert(outImg, PvImageProc::GRAY_SPACE, colOutImg, PvImageProc::RGB_SPACE);
  colOutImg.save("e:/work/tmp/outImg.png");

  PvUtil::exitError("foo");
  */  
}

void
BlobTrackerModShape::convertToImageCoord(TrajectoryPtr pTraj)
{
  // Rescale the points in the trajectory so they match
  // the original image.
  const Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();
        
  const Rectanglei roi = mpForegroundSegmentMod->getRoiRectangle();

  const float scaleX = 1.0f / scaleFactor.x;
  const float scaleY = 1.0f / scaleFactor.y;

  Trajectory::Nodes::iterator iNode;
  Trajectory::Nodes& nodes = pTraj->getNodes();
  for (iNode = nodes.begin(); iNode != nodes.end(); ++iNode)
  {
    Trajectory::Node& node = **iNode;
    node.boundingBox.set(
      roi.x0 + node.boundingBox.x0*scaleX,
      roi.y0 + node.boundingBox.y0*scaleY,
      roi.x0 + node.boundingBox.x1*scaleX,
      roi.y0 + node.boundingBox.y1*scaleY);
    node.location.set(
      (node.boundingBox.x0+node.boundingBox.x1)/2,
      mIsTopViewCamera ? (node.boundingBox.y0+node.boundingBox.y1)/2 : node.boundingBox.y1);
  }     
       
}

void 
BlobTrackerModShape::killTracksInPoly()
{ 
  const Image8& img = mKillTrackMask;

  TracksShape::iterator iTrack;  

  for (iTrack = mTracks.begin(); iTrack != mTracks.end(); )
  {
    //Don't remove tracks with less than 2 nodes in their trajectories
    if ((*iTrack)->getTrajectory()->nodeCount() < 2)
    {
      iTrack++;
      continue;
	  } 

    Vector2f pos = (*iTrack)->getPos();

    //if (img((int)pos.x, (int)pos.y) == 255) //kill tracks inside kill polygons

    if (img((int)pos.x, (int)pos.y) == 0) //kill tracks outside of polygons
    {
      (*iTrack)->kill();
      
      //convert trajectory to image coordinates
      TrajectoryPtr pTraj = (*iTrack)->getTrajectory();
      convertToImageCoord(pTraj);

      //TODO: make sure that this is working
      mPreTrajectories.push_back(pTraj);

      /*
      if (isExitTrajectory(pTraj))
      {
        //generate an exit event
        generateExitEvent(pTraj);
      }
      else
      {
        generateUnknownEvent(pTraj);
      }
      */
      
      //erase the track, it is no longer needed
      iTrack = mTracks.erase(iTrack);

    }
    else
    {
      iTrack++;
    }
  }
}

void
BlobTrackerModShape::checkOverlappingTracks()
{  

  //TODO: this is a very trivial way of killing tracks, improve upon this
  Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();
  Vector2i offset; 
  Rectanglei roiRect = mpForegroundSegmentMod->getRoiRectangle();
  offset.x = std::min(roiRect.x0,roiRect.x1) * scaleFactor.x;
  offset.y = std::min(roiRect.y0,roiRect.y1) * scaleFactor.y;  

  TracksShape::iterator iTrack;
  for (iTrack = mTracks.begin(); iTrack != mTracks.end(); iTrack++)
  {
    
    TracksShape::iterator iOtherTrack;

    for (iOtherTrack = mTracks.begin(); iOtherTrack != mTracks.end(); iOtherTrack++)
    {
      if (iOtherTrack == iTrack)
      {
        continue;
      }

      
      Vector2f pos1 = (*iTrack)->getPos();
      Vector2f pos2 = (*iOtherTrack)->getPos();

      float dist = sqrt(dist2(pos1,pos2));

      Rectanglei rect1 = mpPersonShapeEstimator->getMaskRect(pos1.x + offset.x,pos1.y + offset.y);
      Rectanglei rect2 = mpPersonShapeEstimator->getMaskRect(pos2.x + offset.x,pos2.y + offset.y);

      int small1 = std::min(rect1.width(),rect1.height());
      int small2 = std::min(rect2.width(),rect2.height());

      const float overlapFactor = 0.5;
      float avgSmall = std::max((small1+small2)/2.0 * overlapFactor,1.0);

      if (dist < avgSmall) //kill younger track
      {
        //PVMSG("KILLING OVERLAPPING TRACK\n");
        if ((*iTrack)->getAge() < (*iOtherTrack)->getAge())
        {
          (*iTrack)->kill();
        }
        else
        {
          (*iOtherTrack)->kill();
        }
      }
    }
  }
}

void 
BlobTrackerModShape::calculateTrackForce()
{

  TracksShape::iterator iTrack;

  for (iTrack = mTracks.begin(); iTrack != mTracks.end(); iTrack++)
  {   

    Vector2f vec = calculateTrackRepulsion(*iTrack);   

    //Vector2f vec;    
    
    //vec.x += -0.0;
    //vec.y += 0.0;
    
    /*
    float val = 1.0f;

    vec.x = std::min(vec.x, val);
    vec.x = std::max(vec.x, -val);
    
    vec.y = std::min(vec.y, val);
    vec.y = std::max(vec.y, -val);   
    */      

    //(*iTrack)->setForceVector(Vector2f(0.0,0.0));
    
    (*iTrack)->setForceVector(vec);    


    //TODO: HACK, MOVE TO OWN FUNCTION
    
    Vector2f dir = (*iTrack)->getTrajectory()->forwardVector(2.0);

    //PERSONSHAPE BEGIN
    Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();
    Vector2i offset; 
    Rectanglei roiRect = mpForegroundSegmentMod->getRoiRectangle();
    offset.x = std::min(roiRect.x0,roiRect.x1) * scaleFactor.x;
    offset.y = std::min(roiRect.y0,roiRect.y1) * scaleFactor.y;  
    //PERSONSHAPE END

    Vector2f p1 = (*iTrack)->getTrajectory()->getEndPos();
    
    Rectanglei shapeRect = mpPersonShapeEstimator->getMaskRect(p1.x+offset.x,p1.y+offset.y);

    float w = (float)shapeRect.width();
    float h = (float)shapeRect.height();    

    //dir.div(3.0);

    if (dir.x != 0.0 || dir.y != 0.0)
    {
      dir.x /= w;
      dir.y /= h;
      
      //dir.x = std::min(dir.x, w);
      //dir.y = std::min(dir.y, h);

      float cap = 0.75f;

      dir.x = std::min(dir.x, cap);
      dir.y = std::min(dir.y, cap);
      dir.x = std::max(dir.x, -cap);
      dir.y = std::max(dir.y, -cap);
      
      
    }

    //NOTE: ENABLE THIS
    (*iTrack)->setDirectionVector(dir);        
  }
}

Vector2f 
BlobTrackerModShape::pointRepulsion(const Vector2f& p1, const Vector2f& p2)
{
  //PERSONSHAPE BEGIN
  Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();
  Vector2i offset; 
  Rectanglei roiRect = mpForegroundSegmentMod->getRoiRectangle();
  offset.x = std::min(roiRect.x0,roiRect.x1) * scaleFactor.x;
  offset.y = std::min(roiRect.y0,roiRect.y1) * scaleFactor.y;  
  //PERSONSHAPE END

  float w = 0.0;
  float h = 0.0;
  //mpForegroundSegmentMod->getBlobSizeEstimate()->getEstimatedSize(p1.x,p1.y,w,h);

  Rectanglei shapeRect = mpPersonShapeEstimator->getMaskRect(p1.x+offset.x,p1.y+offset.y);

  w = (float)shapeRect.width();
  h = (float)shapeRect.height();

  //float d = sqrt(dist2(p1,p2));

  Vector2f vec;

  vec.x = p1.x - p2.x;
  vec.y = p1.y - p2.y;

  //divide by blob width,height
  vec.x /= w;
  vec.y /= h;  

  //Special case, return 0.0 and let caller deal with it
  if (vec.x == 0.0 && vec.y == 0.0)
  {
    return vec;
  }

  float d = sqrt(vec.x*vec.x + vec.y*vec.y);  

  double maxDist = 1.0;

  //return no force if points are > then specified distance apart
  if (d >= maxDist)
  {
    return Vector2f(0.0,0.0);
  }

  PVASSERT(d != 0.0);

  vec.normalize();

  //scale force vector based on distance
  //vec.mult(0.25/d);  //nonlinear

  //linear
  float scale = 1.0 - d/maxDist;
  scale = std::min(scale,1.0f);
  scale = std::max(scale,0.0f);

  vec.mult(scale);
  
  return vec;
}


Vector2f 
BlobTrackerModShape::calculateTrackRepulsion(MultiFrameTrackShapePtr pTrack)
{
  Vector2f vec(0.0,0.0);

  Vector2f pos = pTrack->getPos();

  
  TracksShape::iterator iTrack;

  for (iTrack = mTracks.begin(); iTrack != mTracks.end(); iTrack++)
  {
    if (*iTrack != pTrack)
    {
      Vector2f f = pointRepulsion(pos, (*iTrack)->getPos());     
      
      vec.x += f.x;
      vec.y += f.y;
    }
  }  
  return vec;
}


void 
BlobTrackerModShape::process()
{  
  mBenchmark.beginSample();

  /*
  if (getCurrentTime() > 1278056142.06668)
  {
    PVMSG("TESTING\n");
  }
  */

  //First, filter the image based on the person size

  Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();

  Vector2i offset; 
  Rectanglei roiRect = mpForegroundSegmentMod->getRoiRectangle();
  offset.x = std::min(roiRect.x0,roiRect.x1) * scaleFactor.x;
  offset.y = std::min(roiRect.y0,roiRect.y1) * scaleFactor.y;  

  //create temporary image for filtering based on roi

  //PVMSG("%d %d\n", offset.x, offset.y);

  mpPersonShapeEstimator->filterImage(*mpFilteredForeground, *mpForegroundSegmentMod->getForeground(), offset);      

  
  if (mRepelTracks)
  {    
    calculateTrackForce();
  }
  

  killTracksInPoly();


  
  // Update all tracks.
  TracksShape::iterator iTrack;
  for (iTrack = mTracks.begin(); iTrack != mTracks.end(); iTrack++)
  {
    /*
    //TODO: MAKE SURE HACK WORKS WHEN UNCOMMENTING BELOW CODE
    if (mIsMotionEnabled)
    {
       (*iTrack)->useMotionHistory(mpMotionMod->getMotionHistory(), mMaxNoMotionTime);
    }
    */

    (*iTrack)->update(mpFilteredForeground, getCurrentTime());    
  }

  
  //TODO: THIS SHOULD BE ENABLED
  checkOverlappingTracks();

  

  scanForNewTracks();
  
  
  
  
  // Delete tracks that are not alive
  for (iTrack = mTracks.begin(); iTrack != mTracks.end();)
  {
    if (!(*iTrack)->isAlive())
    {
      if (mVerbose)
      {
        PVMSG("### Removing Track %d [frame: %d]\n",(*iTrack)->getId(),getCurrentFrameNumber());
      }
      TrajectoryPtr pTrajectory = (*iTrack)->getTrajectory();
      const int minTrajectoryLength = 5;
      //const int maxPreTrajectories = mMaxPreTrajectories;
      if (pTrajectory->getNodes().size() >= minTrajectoryLength) 
      {
        // Rescale the points in the trajectory so they match
        // the original image.
        const Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();
        
        const Rectanglei roi = mpForegroundSegmentMod->getRoiRectangle();

        const float scaleX = 1.0f / scaleFactor.x;
        const float scaleY = 1.0f / scaleFactor.y;

        Trajectory::Nodes::iterator iNode;
        Trajectory::Nodes& nodes = pTrajectory->getNodes();
        for (iNode = nodes.begin(); iNode != nodes.end(); ++iNode)
        {
          Trajectory::Node& node = **iNode;
          node.boundingBox.set(
            roi.x0 + node.boundingBox.x0*scaleX,
            roi.y0 + node.boundingBox.y0*scaleY,
            roi.x0 + node.boundingBox.x1*scaleX,
            roi.y0 + node.boundingBox.y1*scaleY);
          node.location.set(
            (node.boundingBox.x0+node.boundingBox.x1)/2,
            mIsTopViewCamera ? (node.boundingBox.y0+node.boundingBox.y1)/2 : node.boundingBox.y1);
        }
        mPreTrajectories.push_back(pTrajectory);
        
        if (mSendExtendedAttributes && mSendPreprocessTrajectories)
        {
          sendVmsEvent(pTrajectory, mEventChannels.front());
        }        
      }
      iTrack = mTracks.erase(iTrack);
    }
    else
    {
      iTrack++;
    }
  }
   
  
  postProcessTracks(false);

  mBenchmark.endSample();

  
  if (mVisualizeSnakeTail)
  {
    // Very expensive!!! For visualization only...
    mLastColorFrames.push_back(Image32Ptr(new Image32(getCurrentFrame())));
  } 
  
  
}

void
BlobTrackerModShape::scanForNewTracks()
{  

  PVASSERT(mStartTrackMask.width() ==  mpFilteredForeground->width());
  PVASSERT(mStartTrackMask.height() == mpFilteredForeground->height()); 
  

  // Make a copy since the original will be modified.  
  (*mpNewTrackScanImage) = *mpFilteredForeground;
  Image8& img = *mpNewTrackScanImage;
  
  Image8 rawForeground = *mpForegroundSegmentMod->getForeground();
  
 
  Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();
  Vector2i offset;
  Rectanglei roiRect = mpForegroundSegmentMod->getRoiRectangle();
  offset.x = std::min(roiRect.x0,roiRect.x1) * scaleFactor.x;
  offset.y = std::min(roiRect.y0,roiRect.y1) * scaleFactor.y;


  TracksShape::iterator iTrack;
  for (iTrack = mTracks.begin(); iTrack != mTracks.end(); iTrack++)
  {
    //(*iTrack)->cleanForegroundSegmentation(rawForeground);
    Vector2f pos =  (*iTrack)->getPos();
    mpPersonShapeEstimator->cleanForegroundSegmentation(round(pos.x),round(pos.y),rawForeground,offset);
  }

  //Now filter the image
  mpPersonShapeEstimator->filterImage(*mpNewTrackScanImage, rawForeground, offset);



  //iteratively scan through the image, creating tracks according to mTrackCreationThresholds
  std::list<int>::const_iterator iCreationThreshold; 
  
  std::list<Vector2f> newTrackLocations;

  std::list<Vector2f>::iterator iLocation;

  /*
  //TODO: MAKE SURE BELOW HACK WORKS BEFORE UNCOMMENTING CODE
  if (mIsMotionEnabled)
  {
    PVASSERT(mpMotionMod->getMotionHistory()->width() == mpForegroundSegmentMod->getFilteredForeground()->width());
    PVASSERT(mpMotionMod->getMotionHistory()->height() == mpForegroundSegmentMod->getFilteredForeground()->height());    
  }
  */

  for (iCreationThreshold = mTrackCreationThresholds.begin(); 
       iCreationThreshold != mTrackCreationThresholds.end(); 
       iCreationThreshold++)
  {  
    //scan entire image for new tracks    
    for (int y = 0; y < img.height(); y++)
    {
      for (int x = 0; x < img.width(); x++)
      {
      
        const Uint8 creationForegroundThreshold = *iCreationThreshold; //178; //WAS 178;         

        if (img(x,y) > creationForegroundThreshold && (mTracks.size()+newTrackLocations.size()) < mMaxConcurrentTracks
          && mStartTrackMask(x,y) == 255)
        {

          //PVMSG("TRACK CREATE ATTEMPT: %d %d\n", *iCreationThreshold, img(x,y));

          /*
          //TODO: MAKE SURE HACK WORKS BEFORE UNCOMMENTING BELOW CODE
          if (mIsMotionEnabled)
          {
            if ((*mpMotionMod->getMotionHistory())(x,y) > mMaxNoMotionTime)
            {
              //PVMSG("TRACK NOT CREATED DUE TO NO MOTION\n");
              continue;
            }
          }
          */

        
          Vector2f newTrackLocation(x,y);        
          newTrackLocations.push_back(newTrackLocation);        

          //hack, refilter input image after each new location is chosen        
          mpPersonShapeEstimator->cleanForegroundSegmentation(x,y,rawForeground,offset);
          mpPersonShapeEstimator->filterImage(*mpNewTrackScanImage, rawForeground, offset);        
        
          //
        }
      }
    }  

  

    //DONT CREATE TRACKS IF THEY ARE IN BOUNDING BOX OF ANOTHER TRACK    
    for (iLocation = newTrackLocations.begin(); iLocation != newTrackLocations.end(); )
    {

      bool keepNewTrack = true;
    
      //float minDist = img.width();
      TracksShape::iterator iClosestTrack = mTracks.end();
      for (iTrack = mTracks.begin(); iTrack != mTracks.end(); iTrack++)
      {
        const Vector2f& p1 = (*iTrack)->getPos();
        const Vector2f& p2 = (*iLocation);

        //convert location to integer version
        Vector2i newPos;
        newPos.x = int(p2.x);
        newPos.y = int(p2.y);

        //get mask rectangle from track
        Rectanglei trackRect = mpPersonShapeEstimator->getMaskRect(p1.x + offset.x, p1.y + offset.y);

        //modify rect to account for roi translation
        trackRect.x0 -= offset.x;
        trackRect.x1 -= offset.x;
        trackRect.y0 -= offset.y;
        trackRect.y1 -= offset.y;

      
        //if this point is inside the bounding box, erase it from the new track list
        if (trackRect.inside(newPos))
        {
          keepNewTrack = false;
          break;
        } 
      
        //TEST FOR INTERSECTION OF BOTH RECTS
        Rectanglei locRect = mpPersonShapeEstimator->getMaskRect(p2.x + offset.x, p2.y + offset.y);
        locRect.x0 -= offset.x;
        locRect.x1 -= offset.x;
        locRect.y0 -= offset.y;
        locRect.y1 -= offset.y;

        if (locRect.intersect(trackRect))
        {
          //PVMSG("TRACK NOT CREATED BECAUSE OF BBOX INTESECTION\n");
          keepNewTrack = false;
          break;
        }


        //END TEST

      }   

      if (keepNewTrack)
      {
        iLocation++;
      }
      else
      {
        iLocation = newTrackLocations.erase(iLocation);
      }   
    }  
  }

  //ACTUALLY CREATE TRACKS NOW  
  for (iLocation = newTrackLocations.begin(); iLocation != newTrackLocations.end(); iLocation++)
  {
    MultiFrameTrackShapePtr pNewTrack(new MultiFrameTrackShape());

    
    pNewTrack->init(
      mNewTrackId++,
      mSettings.getPath(),
      Vector2i(iLocation->x,iLocation->y),      
      getCurrentTime(),
      getFramesPerSecond(),
      mpPersonShapeEstimator,
      offset);      

    if (mVerbose)
    {
      PVMSG("### Created Track %d at (%d,%d) [frame: %d]\n",pNewTrack->getId(),iLocation->x,iLocation->y,getCurrentFrameNumber());
    }
    mTracks.push_back(pNewTrack);    
  }    
}


void
BlobTrackerModShape::postProcessTracks(bool force)
{
  double currentTime = getCurrentTime();

  if (force == false)
    if (currentTime >= mNextForcePostProcessTime)
    {
      force = true;          
    }

  if (!force && currentTime < mNextPostProcessTime)
  {
    // Don't process, we have not reached the minimum time yet    
    return;
  }


  if (force == false && currentTime >= mNextPostProcessTime)
  {
    //perform post processing if there are no tracks in the scene
    if (mTracks.size() == 0)
    {
      force = true;            
    }
  }

  if (!force)
    return;  
  
  mNextForcePostProcessTime = currentTime + mPostProcessUpperInterval;
  mNextPostProcessTime = currentTime + mPostProcessLowerInterval;

  std::list<TrajectoryPtr> postTrajectories;  

  postTrajectories = mPreTrajectories;

  //TODO: handle this part better, maybe reconstruct combined trajectories later
  const Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();        

  //if any tracks are still alive, clip and process the first half of their trajectories  
  //std::list<MultiFrameTrackShapePtr>::iterator iTrack;
  TracksShape::iterator iTrack;
  for (iTrack = mTracks.begin(); iTrack != mTracks.end(); iTrack++)
  {
    TrajectoryPtr pFirstPart = (*iTrack)->getTrajectory();
    TrajectoryPtr pLastPart = pFirstPart->breakUp(getCurrentTime(), mNewTrackId++);

    // Rescale the points in the trajectory so they match
    // the original image.    
    //const Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();        
    const Rectanglei roi = mpForegroundSegmentMod->getRoiRectangle();
    const float scaleX = 1.0f / scaleFactor.x;
    const float scaleY = 1.0f / scaleFactor.y;

    Trajectory::Nodes::iterator iNode;
    Trajectory::Nodes& nodes = pFirstPart->getNodes();
    for (iNode = nodes.begin(); iNode != nodes.end(); ++iNode)
    {
      Trajectory::Node& node = **iNode;
      node.boundingBox.set(
        roi.x0 + node.boundingBox.x0*scaleX,
        roi.y0 + node.boundingBox.y0*scaleY,
        roi.x0 + node.boundingBox.x1*scaleX,
        roi.y0 + node.boundingBox.y1*scaleY);
      node.location.set(
        (node.boundingBox.x0+node.boundingBox.x1)/2,
        mIsTopViewCamera ? (node.boundingBox.y0+node.boundingBox.y1)/2 : node.boundingBox.y1);
     }     

    (*iTrack)->setTrajectory(pLastPart);      
    postTrajectories.push_back(pFirstPart);   
  }
  
  mPreTrajectories.clear();
    
  if (mSavePreTrajectories && postTrajectories.size() != 0)
  {    
    //
    DateTime dt(getCurrentTime());

    std::string filename = "Traj_VidSrcId" + aitSprintf("%d", mVideoSourceId);
    filename += "_" + dt.format("%Y%m%d_%H%M%S.csv");
    std::string loc = ait::combine_path(mTrajectoryOutputFolder, filename);
    
    //Compress trajectories to save space
    std::list<TrajectoryPtr> compactedTraj;
    std::list<TrajectoryPtr>::iterator iTraj;

    /*
    double compressDist = 
        mpForegroundSegmentMod->getBlobSizeEstimate()->getCenterWidth()/3.0;
        */
    //PERSON SHAPE BEGIN    
    Vector2i frameSize = getImageAcquireModule()->getFrameSize();
    Rectanglei personSize = mpPersonShapeEstimator->getMaskRect(frameSize.x/2,frameSize.y/2);
    double compressDist = personSize.width()/scaleFactor.x/3.0;;
    //PERSON SHAPE END


    for (iTraj = postTrajectories.begin(); iTraj != postTrajectories.end(); iTraj++)
    {      

      TrajectoryPtr pTraj = (*iTraj)->compress(compressDist,5.0);      

      if (pTraj->nodeCount() > 1)
        compactedTraj.push_back(pTraj);
    }   
   
    if (compactedTraj.size() > 0)
      Trajectory::saveXmlCsv(loc, compactedTraj);
      //Trajectory::saveCSV(loc, compactedTraj);
  } 
    
  //perform postprocessing
  //mMultiPersonHandler.processTrajectories(postTrajectories);

  if (mSendTrajectoriesAsEventAttribute)
  {
    //Compress trajectories to save space
    std::list<TrajectoryPtr> compactedTraj;
    std::list<TrajectoryPtr>::iterator iTraj;

    
    //double compressDist = 
    //    mpForegroundSegmentMod->getBlobSizeEstimate()->getCenterWidth()/3.0;
    //PERSON SHAPE BEGIN
    Vector2i frameSize = getImageAcquireModule()->getFrameSize();
    Rectanglei personSize = mpPersonShapeEstimator->getMaskRect(frameSize.x/2,frameSize.y/2);
    double compressDist = personSize.width()/scaleFactor.x/3.0;
    //PERSON SHAPE END

    for (iTraj = postTrajectories.begin(); iTraj != postTrajectories.end(); iTraj++)
    {      
      TrajectoryPtr pTraj = (*iTraj)->compress(compressDist,5.0);

      if (pTraj->nodeCount() > 1)
        compactedTraj.push_back(pTraj);
    }

    for (iTraj = compactedTraj.begin(); iTraj != compactedTraj.end(); ++iTraj)
    {
      sendVmsEvent(*iTraj,*mEventChannels.begin());
      BlobTrackerMsgShapePtr pMsg(new BlobTrackerMsgShape(getVisualSensorId(),(*iTraj)->getStartTime()));
      pMsg->setFlag(BlobTrackerMsgShape::POST_PROCESSED_TRAJECTORY,true);
      pMsg->setTrajectory(*iTraj);
      sendMessage(pMsg);
    }
  }
  
  //DO NOT CALL THIS
  //generateEvents(postTrajectories);

  //send notification that all prior events can be processed
  std::list<int>::const_iterator iEvtChan;
  for (iEvtChan = mEventChannels.begin(); iEvtChan != mEventChannels.end(); iEvtChan++)
    mpVmsPacket->notifyProcessCompletion(currentTime, *iEvtChan);
}


void
BlobTrackerModShape::generateEvents(const std::list<TrajectoryPtr>& trajectories)
{
  assert(false);

  /*
  assert(mpForegroundSegmentMod->getBlobSizeEstimate().get());

  if (mEventsPolygon.empty()) return;

  std::list<int>::const_iterator iEvtChan;

  for (iEvtChan = mEventChannels.begin(); iEvtChan != mEventChannels.end(); iEvtChan++)
  {
    std::list<TrajectoryPtr>::const_iterator iTrajectory;
    for (iTrajectory = trajectories.begin(); iTrajectory != trajectories.end(); ++iTrajectory)
    {
      std::list<PolygonEventPtr> events;    
      mTrajEvents.extractEvents(**iTrajectory, mEventsPolygon[*iEvtChan], events);
      std::list<PolygonEventPtr>::iterator iEvent;
      for (iEvent = events.begin(); iEvent != events.end(); ++iEvent)
      {
        sendVmsEvent(*iEvent, *iEvtChan);
      }
    }
  }
  */
}

void
BlobTrackerModShape::sendVmsEvent(TrajectoryPtr pTrajectory, int eventChannel)
{
  
  //assert(mpForegroundSegmentMod->getBlobSizeEstimate().get());
  assert(mpPersonShapeEstimator.get());

  double duration = pTrajectory->getEndTime()- pTrajectory->getStartTime();
  //Todo: minimum duration of 1.0 for now;
  duration = std::max(duration, 1.0);
  
  //double compressDist = 
  //      mpForegroundSegmentMod->getBlobSizeEstimate()->getCenterWidth()/3.0;

  
  Vector2i frameSize = getImageAcquireModule()->getFrameSize();
  Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();
  Rectanglei personSize = mpPersonShapeEstimator->getMaskRect(frameSize.x/2,frameSize.y/2);
  double compressDist = personSize.width()/scaleFactor.x/3.0;;

  TrajectoryPtr pCompressed = pTrajectory->compress(compressDist,5.0);

  mpVmsPacket->beginEventTime(pTrajectory->getStartTime(),duration, eventChannel);

  if (mSendTrajectoriesAsEventAttribute)
  {
    mpVmsPacket->addLongStringAttribute("4",pTrajectory->toXmlString());
  }
  else
  {
    mpVmsPacket->addAttribute("10",boost::lexical_cast<std::string>(pTrajectory->getId()));
    mpVmsPacket->addString("<extended>" + pTrajectory->getXmlBlobRectangles() + "</extended>");
  }
  mpVmsPacket->endEvent();
  
}

void
BlobTrackerModShape::processMessages()
{
  VisionMsgPtr pMsg;

  while (mpInputMsgQueue->get(pMsg))
  {
    BlobTrackerMsgShape* pMsg0 = dynamic_cast<BlobTrackerMsgShape*>(pMsg.get());
    if (pMsg0 != NULL)
    {
      processMsg(pMsg0);
      continue;
    }
    PvUtil::exitError("Message type not supported by this module [%s]",getFullName().c_str());
  }
}

void 
BlobTrackerModShape::processMsg(BlobTrackerMsgShape *pMsg)
{
}

void
BlobTrackerModShape::finish()
{ 

  postProcessTracks(true);
  
  //mpVmsPacket->finish();
  RealTimeMod::finish();
}



void 
BlobTrackerModShape::visualize(Image32& img)
{
  if (!mPaintVisualization)
  {
    return;
  }

  const Vector2f scaleFactor = mpForegroundSegmentMod->getRoiScaleFactor();  
  const Rectanglei roi = mpForegroundSegmentMod->getRoiRectangle(); 
  
  if (mVisualizeSnakeTail)
  {
    img.paste(*(mLastColorFrames[0]),0,0);   
  }   

  PVASSERT(scaleFactor.x != 0.0);
  PVASSERT(scaleFactor.y != 0.0);

  const float scale_x = 1.0f / scaleFactor.x;
  const float scale_y = 1.0f / scaleFactor.y;

#define TRANSFORM_X(x) roi.x0+(int)((x)*scale_x)
#define TRANSFORM_Y(y) roi.y0+(int)((y)*scale_y)


  Image32 filteredForeground;
  ColorConvert::convert(*mpFilteredForeground,PvImageProc::GRAY_SPACE, filteredForeground, PvImageProc::RGB_SPACE);

  

  Vector2i frameSize = getImageAcquireModule()->getFrameSize();


  //draw trajectories
  std::list<TrajectoryPtr>::iterator iT;
  for (iT = mPreTrajectories.begin(); iT != mPreTrajectories.end(); iT++)
  {     
    Trajectory::Nodes& nodes = (*iT)->getNodes();
    Trajectory::Nodes::iterator iNode, iPrevNode = nodes.end();
    for (iNode = nodes.begin(); iNode != nodes.end(); iNode++)
    {
      if (iPrevNode != nodes.end())
      {           
        img.line(
          round((*iPrevNode)->location.x),
          round((*iPrevNode)->location.y),
          round((*iNode)->location.x),
          round((*iNode)->location.y),
          PV_RGB(100,100,0));
      }
      iPrevNode = iNode;
    }
  }

    
  TracksShape::iterator iTrack;
  for (iTrack = mTracks.begin(); iTrack != mTracks.end(); iTrack++)
  {      
    (*iTrack)->visualize(filteredForeground);      

    // Don't visualize spurious tracks...
    if ((*iTrack)->getAge() > 0)//mMaxSnakeSize)
    {
      std::vector<Vector2f>& snake = (*iTrack)->getCurrentSnake();

      if (!mVisualizeSnakeTail)
      {
          
        for (int i = 1; i < snake.size(); i++)
        {
          img.line(
            TRANSFORM_X(snake[i-1].x),TRANSFORM_Y(snake[i-1].y),
            TRANSFORM_X(snake[i].x),TRANSFORM_Y(snake[i].y),
            PV_RGB(255,255,255));           
        }         

      }
      if (snake.size() > 1)
      {
        int idx = mVisualizeSnakeTail ? 0 : snake.size()-1;
        //float w,h;
        //mpForegroundSegmentMod->getBlobSizeEstimate()->getEstimatedSize(snake[idx].x,snake[idx].y,w,h);          
        //img.ellipse(TRANSFORM_X(snake[idx].x),TRANSFORM_Y(snake[idx].y),w/2*scale_x,h/2*scale_y,PV_RGB(0,0,255));                   
          
        if (!mVisualizeSnakeTail)
        {
          //img.ellipse(TRANSFORM_X(snake[idx].x),TRANSFORM_Y(snake[idx].y),w*mMinTrackCreationDistance*scale_x,h*mMinTrackCreationDistance*scale_y,PV_RGB(0,255,255));                     
        }
        Trajectory::Nodes& nodes = (*iTrack)->getTrajectory()->getNodes();
        if (!nodes.empty())
        {
          
        }
      }
      if (!mVisualizeSnakeTail)
      {
         
        Trajectory::Nodes& nodes = (*iTrack)->getTrajectory()->getNodes();
        Trajectory::Nodes::iterator iNode, iPrevNode = nodes.end();
        for (iNode = nodes.begin(); iNode != nodes.end(); iNode++)
        {           
          if (iPrevNode != nodes.end())
          { 
            
            img.line(
              TRANSFORM_X((int)(*iPrevNode)->location.x),
              TRANSFORM_Y((int)(*iPrevNode)->location.y),
              TRANSFORM_X((int)(*iNode)->location.x),
              TRANSFORM_Y((int)(*iNode)->location.y),
              PV_RGB(255,0,0));  
              
            img.line(
              TRANSFORM_X((int)(*iPrevNode)->location.x) + 1,
              TRANSFORM_Y((int)(*iPrevNode)->location.y),
              TRANSFORM_X((int)(*iNode)->location.x) + 1,
              TRANSFORM_Y((int)(*iNode)->location.y),
              PV_RGB(255,0,0));   
              
            img.line(
              TRANSFORM_X((int)(*iPrevNode)->location.x) - 1,
              TRANSFORM_Y((int)(*iPrevNode)->location.y),
              TRANSFORM_X((int)(*iNode)->location.x) - 1,
              TRANSFORM_Y((int)(*iNode)->location.y),
              PV_RGB(255,0,0));    

            img.line(
              TRANSFORM_X((int)(*iPrevNode)->location.x),
              TRANSFORM_Y((int)(*iPrevNode)->location.y)+1,
              TRANSFORM_X((int)(*iNode)->location.x),
              TRANSFORM_Y((int)(*iNode)->location.y)+1 ,
              PV_RGB(255,0,0));

            img.line(
              TRANSFORM_X((int)(*iPrevNode)->location.x),
              TRANSFORM_Y((int)(*iPrevNode)->location.y)-1,
              TRANSFORM_X((int)(*iNode)->location.x),
              TRANSFORM_Y((int)(*iNode)->location.y)-1,
              PV_RGB(255,0,0));            
              
          }
          iPrevNode = iNode;
        }          
      }
    }
  }    
  

  //img.paste
  Image32 newTrackVis(frameSize.x,frameSize.y);
  newTrackVis.setAll(0);
  Image32 newTrackScanImage;  
  ColorConvert::convert(*mpNewTrackScanImage, PvImageProc::GRAY_SPACE, newTrackScanImage, PvImageProc::RGB_SPACE);  
  newTrackScanImage.rescale(newTrackScanImage.width() * scale_x, newTrackScanImage.height() * scale_y, RESCALE_SKIP);
  newTrackVis.paste(newTrackScanImage, roi.x0, roi.y0);
   
  Image32 filteredVis(frameSize.x,frameSize.y);
  filteredVis.setAll(0);
  filteredForeground.rescale(filteredForeground.width() * scale_x, filteredForeground.height() * scale_y, RESCALE_SKIP);

  filteredVis.paste(filteredForeground, roi.x0, roi.y0);

  img.paste(filteredVis,0,frameSize.y);
  img.paste(newTrackVis,frameSize.x,frameSize.y);


  //
  //Draw kill and create track polygons
  std::list<Polygon>::const_iterator iPoly;
  for (iPoly = mStartTrackPolyList.begin(); iPoly != mStartTrackPolyList.end(); iPoly++)
  {
    iPoly->draw(img, PV_RGB(255,255,0));
  }

  for (iPoly = mKillTrackPolyList.begin(); iPoly != mKillTrackPolyList.end(); iPoly++)
  {
    iPoly->draw(img, PV_RGB(255,0,0));
  }    
}
  
  

}; // namespace vision

}; // namespace ait


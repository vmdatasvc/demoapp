/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "legacy/vision_blob/MultiFrameTrackShape.hpp"

#include <legacy/low_level/Settings.hpp>
#include <legacy/vision_tools/ColorConvert.hpp>

namespace ait 
{

namespace vision
{


#define VALUE_X(i) (pValues[2*(i)])
#define VALUE_Y(i) (pValues[2*(i)+1])


void 
MultiFrameTrackShape::ThisCondensation::init(int numParticles, int dimension, MultiFrameTrackShape *pMFT)
{
  assert(pMFT);
  Condensation<float>::setParams(numParticles,dimension);
  mpMultiFrameTrack = pMFT;
}

void
MultiFrameTrackShape::ThisCondensation::predict()
{
  mpMultiFrameTrack->predict();
}

MultiFrameTrackShape::MultiFrameTrackShape()
: mIsInitialized(false), mIsAlive(false), mId(-1)
{
}

MultiFrameTrackShape::~MultiFrameTrackShape()
{
}


void 
MultiFrameTrackShape::init(int id,
                      const std::string& settingsPath, 
                      const Vector2i& initPos,                      
                      double startTime,
                      double fps,
                      PersonShapeEstimatorPtr pPersonShapeEstimator,
                      Vector2i personShapeOffset)
{
  
  assert(pPersonShapeEstimator.get());

  // Initialize this object.
  Settings set(settingsPath);

  int numFrames = set.getInt("numFrames",10);
  mpForegroundHistory.reset(new ForegroundHistory(numFrames));

  mPersonShapeOffset = personShapeOffset;
  mpPersonShapeEstimator = pPersonShapeEstimator;

  mCondensation.init(set.getInt("numParticles",200),numFrames*2,this);

  mTrackKillThreshold = set.getInt("trackKillThreshold",76);

  mSnake.clear();
  mSnake.push_back(Vector2f(initPos.x,initPos.y));
  mCircularBufferFilled = false;

  mIsInitialized = true;
  mIsAlive = true;
  mAge = 0;

  mId = id;

  //mStartTime = startTime;

  PVASSERT(fps != 0.0);
  mMaxBlobSpeedPerFrame = set.getFloat("maxBlobSpeedPerSecond",7.5f) / fps;  

  mpTrajectory.reset(new Trajectory(id));

  //
  mForceVector.x = 0.0;
  mForceVector.y = 0.0; 

}


void
MultiFrameTrackShape::predict()
{  
  assert(mpForegroundHistory.get());
  const int snakeSize = mpForegroundHistory->size();

  

  // Do nothing if there is less than two nodes in the snake.
  if (snakeSize < 2)
  {
    return;
  }

  const int maxSnakeSize = mCondensation.getDimension()/2;
  
  //PERSON SHAPE BEGIN
  assert(mpPersonShapeEstimator.get());
  PersonShapeEstimator& personShape = *mpPersonShapeEstimator;
  // PERSON SHAPE END
  //BlobSizeEstimate& blobSizeEstimate = *mpBlobSizeEstimate;


  PvRandom rand;
  PvRandom uniformRand;

  const float minFactor = 0.02f;
  const float maxFactor = 0.1f;
  const float deltaFactor = maxSnakeSize > 1? (maxFactor-minFactor)/(maxSnakeSize-1) : 0;

  int i = 0,j = 0;

  // Angles and magnitudes for all difference vectors of consecutive nodes
  // in the snake. The angles are stored in unitary vectors.
  std::vector<float> magnitudes(snakeSize-1);
  std::vector<float> cosThetas(snakeSize-1);
  std::vector<float> sinThetas(snakeSize-1);  

  

  for (j = 0; j < mCondensation.getNumParticles(); j++)
  {
    float *pValues = mCondensation.getParticle(j);
    
    assert(pValues);

    // If the snake is as long as the buffer size, let's shift the values.
    if (mCircularBufferFilled)
    {
      // Shift the nodes of the 'snake'
      for (i = 1; i < snakeSize; i++)
      {
        VALUE_X(i-1) = VALUE_X(i);
        VALUE_Y(i-1) = VALUE_Y(i);
      }
    }

    

    // Calculate the current angles and vectors.
    for (i = 0; i < snakeSize-2; i++)
    {
      const float dx = VALUE_X(i+1) - VALUE_X(i);
      const float dy = VALUE_Y(i+1) - VALUE_Y(i);
      const float mag = sqrt(dx*dx+dy*dy);
      // Handle the case of zero magnitude
      magnitudes[i] = mag;
      if (mag == 0)
      {
        cosThetas[i] = 1;
        sinThetas[i] = 0;
      }
      else
      {
        cosThetas[i] = dx/mag;
        sinThetas[i] = dy/mag;
      }
    }

    if (snakeSize >= 3)
    {
      // Set the magnitude and angle of the new head equal to
      // the magnitude and angle of the previous segment vector.
      // Note that the head of the snake is at the end of the
      // vector.
      magnitudes[snakeSize-2] = magnitudes[snakeSize-3];
      cosThetas[snakeSize-2] = cosThetas[snakeSize-3];
      sinThetas[snakeSize-2] = sinThetas[snakeSize-3];
    }    
    else
    {
      // This case occurs when the snake size is 2.
      // Initialize the first segment with half the blob size magnitude 
      // and a random angle.
      float sizeX = 0.0,sizeY = 0.0;

      //blobSizeEstimate.getEstimatedSize(VALUE_X(0),VALUE_Y(0),sizeX,sizeY);

      //PERSONSHAPE CHANGE BEGIN
      
      Rectanglei shapeRect = 
        personShape.getMaskRect(VALUE_X(0) + mPersonShapeOffset.x,VALUE_Y(0) + mPersonShapeOffset.y);

      sizeX = (float)shapeRect.width();
      sizeY = (float)shapeRect.height();            
      //PERSONSHAPE CHANGE END


      magnitudes[0] = sizeX*mMaxBlobSpeedPerFrame*0.3;
      float theta = uniformRand.uniform0to1()*(2*M_PI);
      cosThetas[0] = cos(theta);
      sinThetas[0] = sin(theta);
      // Also perturb the first element so it can find the best
      // origin.
      VALUE_X(0) = VALUE_X(0) + rand.gaussRand()*sizeX*mMaxBlobSpeedPerFrame;
      VALUE_Y(1) = VALUE_Y(1) + rand.gaussRand()*sizeY*mMaxBlobSpeedPerFrame;
    } 

    
    //try peturbing only the last node of the snake
    const float magnitudeStdDev = 0.2f;

    float newMag = magnitudes[i]*fabs(1+rand.gaussRand()*magnitudeStdDev);

    // Threshold the magnitude based on the blob size (width only).
    float sizeX = 0.0,sizeY = 0.0;
    //blobSizeEstimate.getEstimatedSize(VALUE_X(i),VALUE_Y(i),sizeX,sizeY);
    
    //PERSONSHAPE CHANGE BEGIN
    
    Rectanglei shapeRect = 
      personShape.getMaskRect(VALUE_X(i) + mPersonShapeOffset.x,VALUE_Y(i) + mPersonShapeOffset.y);

    sizeX = (float)shapeRect.width();
    sizeY = (float)shapeRect.height();         
    //PERSONSHAPE CHANGE END    

    
    const float minMagnitudeFactor = .05;//WAS 0.01f;


    const float maxMagnitudeFactor = mMaxBlobSpeedPerFrame;
    newMag = std::min(std::max(sizeX*minMagnitudeFactor,newMag),sizeX*maxMagnitudeFactor);

    const float deltaThetaDegrees = 180.0f;//TODO: ORIGINAL VALUE 10.0f;    

    const float deltaTheta = (float)M_PI*deltaThetaDegrees*rand.gaussRand()/180.0f;
    const float cosDeltaTheta = cos(deltaTheta);
    const float sinDeltaTheta = sin(deltaTheta);

    const float newCosTheta = cosThetas[i]*cosDeltaTheta - sinThetas[i]*sinDeltaTheta;
    const float newSinTheta = sinThetas[i]*cosDeltaTheta + cosThetas[i]*sinDeltaTheta;

    VALUE_X(snakeSize - 1) = VALUE_X(snakeSize - 2) + newMag*newCosTheta;
    VALUE_Y(snakeSize - 1) = VALUE_Y(snakeSize - 2) + newMag*newSinTheta;  
  }
  
  if (snakeSize == maxSnakeSize)
  {
    mCircularBufferFilled = true;
  }  
  
}

float
MultiFrameTrackShape::evaluateOptimizationFunction(float* pValues, int numParticle, int numPhase)
{
  assert(mpForegroundHistory.get());
  const int snakeSize = mpForegroundHistory->size();

  float foregroundMatch = evaluateForegroundMatching(pValues);  

  float weight = 0.0;

  float prefLocScore = 0.0;

  if (mForceVector.x != 0.0 || mForceVector.y != 0.0)
  {    

      const Vector2f pos1 = getPos();

      Vector2f pos2;
      pos2.x = VALUE_X(snakeSize - 1);
      pos2.y = VALUE_Y(snakeSize - 1);
      
      //mpBlobSizeEstimate->getEstimatedSize(pos1.x,pos1.y,w,h);
      //PERSON SHAPE CHANGE BEGIN    
      assert(mpPersonShapeEstimator.get());
      PersonShapeEstimator& personShape = *mpPersonShapeEstimator;
      Rectanglei shapeRect = 
        personShape.getMaskRect(pos1.x + mPersonShapeOffset.x, pos1.y + mPersonShapeOffset.y);

      float w = (float)shapeRect.width();
      float h = (float)shapeRect.height();    

     
      //PERSON SHAPE CHANGE END

      //Pos based on preferred location

      Vector2f prefPos;
      float dirWeight = 0.5;

      prefPos.x = pos1.x + ((dirWeight)*mDirectionVector.x 
        + (1.0 - dirWeight)*mForceVector.x) * w;

      prefPos.y = pos1.y + ((dirWeight)*mDirectionVector.y 
        + (1.0 - dirWeight)*mForceVector.y) * h;

      float d = sqrt(dist2(prefPos,pos2));

      //define max distance from newPref that will receive a bonus
      float maxDist = w*1.0; //blob widths    

      //calc score based on proximity to preferred pos
      prefLocScore = -d/maxDist + 1.0;

      //prefLocScore = 1.0/(1.0 + d);

      prefLocScore = std::min(prefLocScore,1.0f);
      prefLocScore = std::max(prefLocScore,0.0f);

   }  

 
  
    //ORIGINAL WEIGHT FUNCTION
    //float weight = foregroundMatch*((foregroundBias)*foregroundMatch + (1.0f-foregroundBias)*(pathCoherence*pathOverlap));  

  //cube for faster convergence
  foregroundMatch = foregroundMatch*foregroundMatch*foregroundMatch*foregroundMatch;

  if (foregroundMatch > 0.0)
  {
    float foregroundBias = 0.5;
    weight = foregroundMatch * foregroundBias + prefLocScore*(1.0f - foregroundBias);
  }  
    
  return weight;  
}

float
MultiFrameTrackShape::evaluateForegroundMatching(float* pValues)
{  
  
  float foregroundWeight = 0.0f;
  assert(mpForegroundHistory.get());
  const int numFrames = mpForegroundHistory->size();

  int i = 0;
  // Calculate how much it matches the template.
  for (i = 0; i < numFrames; i++)
  {
    assert(mpForegroundHistory.get());
    assert((*mpForegroundHistory)[i].get());
    const Image8& foreground = *((*mpForegroundHistory)[i]);

    const float x = VALUE_X(i);
    const float y = VALUE_Y(i);
    
    if (x < 0 || x > foreground.width()-0.5 ||
      y < 0 || y > foreground.height()-0.5)
    {
      return 0;
    }   


    //float width, height;
    //mpBlobSizeEstimate->getEstimatedSize(x,y,width,height);
    // TODO: TEMP CODE FOR EXPERIMENT
    //width /= 2.0f;
    //height /= 2.0f;
    //width = std::min(width, 2.0f);
    //height = std::min(height, 2.0f);
    //    

    if (foreground(x,y) > mTrackKillThreshold) //ignore foreground pixels less than 30%
    {
      foregroundWeight += (float)foreground(x,y)/255.0f;        
    }    
  } 

  return foregroundWeight/numFrames;
  
}


Vector2f 
MultiFrameTrackShape::getSize()
{
  assert(false); //assert statement for testing, remove if needed
  PVMSG("MultiFrameTrackShape::getSize()\n");


//  assert(mpPersonShapeEstimator.get());
//  PersonShapeEstimator& personShape = *mpPersonShapeEstimator;
//  Rectanglei shapeRect = 
//    personShape.getMaskRect(getPos().x + mPersonShapeOffset.x,getPos().y + mPersonShapeOffset.y);
//  
//  return Vector2f(shapeRect.width(),shapeRect.height());

  return Vector2f(5,5);
 
}

void
MultiFrameTrackShape::update(Image8Ptr pForeground, double currentTime)
{
  assert(mpForegroundHistory.get());
  mpForegroundHistory->push_back(pForeground);  

  const int dimension = mpForegroundHistory->capacity();

  // Build particle vector as required by the signature.
  Matrix<float> values(2*dimension);
  values.setAll(0);
  float *pValues = values.pointer();

  int i = 0;
  for (i = 0; i < mSnake.size(); i++)
  {
    VALUE_X(i) = mSnake[i].x;
    VALUE_Y(i) = mSnake[i].y;
  } 


  //std::ofstream outFile("e:/work/tmp/times.txt");
  //outFile << aitSprintf("%.15f") << std::endl;
  //outFile.close(); 


  
  
  mCondensation.step(values,evalFunctionStub,this); 

  
   

  // Rebuild the snake.
  mSnake.clear();
  for (i = 0; i < mpForegroundHistory->size(); i++)
  {
    mSnake.push_back(Vector2f(VALUE_X(i),VALUE_Y(i)));
  }

  const bool useMaxParticle = false;

  

  if (useMaxParticle)
  {
    // Get the maximum particle.
    float maxParticleWeight = 0;
    int maxParticleIndex = 0;

    for (int pi = 0; pi < mCondensation.getNumParticles(); pi++)
    {
      if (maxParticleWeight < mCondensation.getParticleWeight(pi))
      {
        maxParticleWeight = mCondensation.getParticleWeight(pi);
        maxParticleIndex = pi;
      }
    }

    pValues = mCondensation.getParticle(maxParticleIndex);
    for (i = 0; i < mSnake.size(); i++)
    {
      mSnake[i].set(VALUE_X(i),VALUE_Y(i));
    }
  }

    
  // Update the trajectory with the tail of the snake:
  if (mCircularBufferFilled)
  {    

    
    float sizeX = 0.0,sizeY = 0.0;

    //mpBlobSizeEstimate->getEstimatedSize(mSnake[0].x,mSnake[0].y,sizeX,sizeY);

    //PERSON SHAPE CHANGE BEGIN
    
    assert(mpPersonShapeEstimator.get());
    PersonShapeEstimator& personShape = *mpPersonShapeEstimator;

    Rectanglei shapeRect = 
      personShape.getMaskRect(mSnake[0].x + mPersonShapeOffset.x,mSnake[0].y + mPersonShapeOffset.y);

    sizeX = (float)shapeRect.width();
    sizeY = (float)shapeRect.height();

    //PERSON SHAPE CHANGE END    

    
    const float x = mSnake[0].x;
    const float y = mSnake[0].y;

    
    Trajectory::NodePtr pNode(Trajectory::NodePtr(new Trajectory::Node(
      currentTime, //replaces commented out next line
      //mStartTime+ mAge-mSnake.size(), //REPLACED WITH ACTUAL TIME INFORMATION
      Vector2f(x,y),
      Rectanglef(x-sizeX/2,y-sizeY/2,x+sizeX/2,y+sizeY/2))));
    

    assert(pNode);

    //DISABLE NEXT LINE FOR RELEASE MODE CRASH DEBUG
    //calculateNodeCardinality(*pNode,*((*mpForegroundHistory)[mpForegroundHistory->size()-mSnake.size()]));
    
    assert(mpTrajectory.get());
    mpTrajectory->getNodes().push_back(pNode);
    
    
  }
  // Check if this track is done
  float minSnakeSize = 3;
  if (mSnake.size() >= minSnakeSize && mCondensation.areAllWeightsZero())
  {
    kill();
  }
  else
  {
    mAge++;
  }    

  
}

void
MultiFrameTrackShape::kill()
{
  mIsAlive = false;  
}

void
MultiFrameTrackShape::saveParticleImage(float *pValues,float weight)
{
  assert(false); //temp check, remove when needed

  if (mCircularBufferFilled) 
  {
    std::string fname = aitSprintf("test/weight_%0.02f.png",weight);
    if (!PvUtil::fileExists(fname.c_str()))
    {
      float scale = 1;
      const Rectanglef bound(0,0,(*mpForegroundHistory)[0]->width(),(*mpForegroundHistory)[0]->height());
      Image32 img(bound.width()*mSnake.size()*scale,bound.height()*scale);
      for (int j = 0; j < mSnake.size(); j+=2)
      {
        const Image8& foreground = *((*mpForegroundHistory)[j]);

        Image32 fimg(foreground.width(),foreground.height());
        ColorConvert::convert(foreground,PvImageProc::GRAY_SPACE,
          fimg,PvImageProc::RGB_SPACE);

        img.paste(fimg,fimg.width()*j,0);

        for (int i = 0; i < mSnake.size(); i++)
        {
          Vector2f X0(VALUE_X(i-1),VALUE_Y(i-1));
          Vector2f X1(VALUE_X(i),VALUE_Y(i));
          X0.mult(scale);
          X1.mult(scale);
          X0.add(Vector2f(fimg.width()*j,0));
          X1.add(Vector2f(fimg.width()*j,0));
          img.line(X0.x,X0.y,X1.x,X1.y,PV_RGB(255,0,0));
        }
        Vector2f X0(VALUE_X(j-1),VALUE_Y(j-1));
        X0.mult(scale);
        X0.add(Vector2f(fimg.width()*j,0));
        float w,h;
        //assert(mpBlobSizeEstimate.get());
        //mpBlobSizeEstimate->getEstimatedSize(VALUE_X(j),VALUE_Y(j),w,h);

        //PERSON SHAPE BEGIN
        assert(mpPersonShapeEstimator.get());
        Rectanglei shapeRect = 
          mpPersonShapeEstimator->getMaskRect(VALUE_X(j) + mPersonShapeOffset.x,VALUE_Y(j) + mPersonShapeOffset.y);
        w = shapeRect.width();
        h = shapeRect.height();
        //PERSON SHAPE END




        img.ellipse(X0.x,X0.y,w/2*scale,h/2*scale,PV_RGB(0,0,255));
      }
      img.save(fname.c_str());
    }
  }

}

void
MultiFrameTrackShape::saveParticleTrajectoryImage(float *pValues,float weight)
{
  // Visualize the particle and save it to a file.
  if (mCircularBufferFilled) 
  {
    std::string fname = aitSprintf("test/weight_%0.02f.png",weight);
    if (!PvUtil::fileExists(fname.c_str()))
    {
      float scale = 4;
      const Rectanglef bound(0,0,(*mpForegroundHistory)[0]->width(),(*mpForegroundHistory)[0]->height());
      Image32 img(bound.width()*scale,bound.height()*scale);
      img.setAll(0);
      for (int i = 1; i < mSnake.size(); i++)
      {
        Vector2f X0(VALUE_X(i-1),VALUE_Y(i-1));
        Vector2f X1(VALUE_X(i),VALUE_Y(i));
        X0.mult(scale);
        X1.mult(scale);
        img.line(X0.x,X0.y,X1.x,X1.y,PV_RGB(255,0,0));
        img.circle(X0.x,X0.y,2,PV_RGB(0,100+155*(i-1)/mSnake.size(),0));
        img.circle(X1.x,X1.y,2,PV_RGB(0,100+155*i/mSnake.size(),0));
      }
      img.save(fname.c_str());
    }
  }
}

void 
MultiFrameTrackShape::visualize(Image32& img)
{  
  
  int i = 0;
  int pi = 0;

  float maxParticleWeight = 0;
  int maxParticleIndex = 0;

  for (pi = 0; pi < mCondensation.getNumParticles(); pi++)
  {
    if (maxParticleWeight < mCondensation.getParticleWeight(pi))
    {
      maxParticleWeight = mCondensation.getParticleWeight(pi);
      maxParticleIndex = pi;
    }
  }

  for (pi = 0; pi < mCondensation.getNumParticles(); pi++)
  {
    for (i = 1; i < mSnake.size(); i++)
    {
      float *pValues = mCondensation.getParticle(pi);
      img.line(
        VALUE_X(i-1),VALUE_Y(i-1),
        VALUE_X(i),VALUE_Y(i),
        PV_RGB((int)(255*mCondensation.getParticleWeight(pi)/maxParticleWeight),0,0));
    }
  }

  for (i = 1; i < mSnake.size(); i++)
  {
    img.line(
      mSnake[i-1](0),mSnake[i-1](1),
      mSnake[i](0),mSnake[i](1),
      PV_RGB(255,255,0));
  }

  
  if (mSnake.size() > 1)
  {
    int idx = mSnake.size()-1;
    //float w,h;
    /*
    mpBlobSizeEstimate->getEstimatedSize(mSnake[idx].x,mSnake[idx].y,w,h);
    img.ellipse(mSnake[idx].x,mSnake[idx].y,w/2,h/2,PV_RGB(0,0,255));
    */

    //draw rect
    assert(mpPersonShapeEstimator.get());
    Rectanglei rect = mpPersonShapeEstimator->getMaskRect(mSnake[idx].x + mPersonShapeOffset.x,
                                                       mSnake[idx].y + mPersonShapeOffset.y);

    //now offset rect to count for translation of roi
    rect.x0 -= mPersonShapeOffset.x;
    rect.x1 -= mPersonShapeOffset.x;
    rect.y0 -= mPersonShapeOffset.y;
    rect.y1 -= mPersonShapeOffset.y;

    img.line(rect.x0, rect.y0, rect.x1, rect.y0, PV_RGB(0,0,255));
    img.line(rect.x0, rect.y1, rect.x1, rect.y1, PV_RGB(0,0,255));
    img.line(rect.x0, rect.y0, rect.x0, rect.y1, PV_RGB(0,0,255));
    img.line(rect.x1, rect.y0, rect.x1, rect.y1, PV_RGB(0,0,255));

    
  }

  //Draw the force vector as a green line
  int idx = mSnake.size()-1;
  Vector2f vec = mForceVector;

  float w = 0.0;
  float h = 0.0;

  float xPos = mSnake[idx].x;
  float yPos = mSnake[idx].y;

  Rectanglei rect = mpPersonShapeEstimator->getMaskRect(xPos + mPersonShapeOffset.x,yPos + mPersonShapeOffset.y);

  w = (float)rect.width();
  h = (float)rect.height();

  vec.x *= w;
  vec.y *= h;

  img.line(xPos, yPos, xPos + vec.x, yPos + vec.y, PV_RGB(0,255,0));  

  
  //Draw direction/momentum vector
  vec = mDirectionVector;
  vec.x *= w;
  vec.y *= h; 

  //Draw the direction vector as a yellow line  
  img.line(xPos,yPos, xPos + vec.x, yPos + vec.y, PV_RGB(255,255,0));    

}
  


//
// ACCESS
//

//
// INQUIRY
//

}; // namespace vision

}; // namespace ait


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include <legacy/math/Polygon.hpp>

#include <legacy/math/LineSegment.hpp>

namespace ait 
{

bool
sortEdgeDist(const EdgeDist& a, const EdgeDist& b)
{
  return (a.distance < b.distance);
}

//constructor
Polygon::Polygon(const std::vector<Vector2f>& verts)
{ 
    setVertices(verts);
}

const Polygon&
Polygon::operator = (const Polygon& rhs)
{
  mTopLeft = rhs.mTopLeft;
  mBottomRight = rhs.mBottomRight;

  mSides.clear();
  for(int i=0;i<rhs.mSides.size();i++)
  {
    mSides.push_back(rhs.mSides[i]);
  }

  return *this;
}
void
Polygon::translate(Vector2f deltaMovement)
{
  for(int i=0;i<mSides.size();i++)
  {
    Vector2f tempPos;
    tempPos.x = mSides[i](0).x + deltaMovement.x;
    tempPos.y = mSides[i](0).y + deltaMovement.y;
    
    mSides[i](0) = tempPos;

    tempPos.x = mSides[i](1).x + deltaMovement.x;
    tempPos.y = mSides[i](1).y + deltaMovement.y;

    mSides[i](1) = tempPos;
  }

  mTopLeft.x = mTopLeft.x + deltaMovement.x;
  mTopLeft.y = mTopLeft.y + deltaMovement.y;
  mBottomRight.x = mBottomRight.x + deltaMovement.x;
  mBottomRight.y = mBottomRight.y + deltaMovement.y;
}

void
Polygon::rotate(float radian, Vector2f rotCenter)
{
	float cost = cos(radian);
	float sint = sin(radian);
	
	for(int i=0;i<mSides.size();i++)
	{
		Vector2f tempPos;
	 
		tempPos.x = cost*(mSides[i](0).x - rotCenter.x) - sint*(mSides[i](0).y - rotCenter.y) + rotCenter.x;
		tempPos.y = sint*(mSides[i](0).x - rotCenter.x) + cost*(mSides[i](0).y - rotCenter.y) + rotCenter.y;
		mSides[i](0) = tempPos;

		tempPos.x = cost*(mSides[i](1).x - rotCenter.x) - sint*(mSides[i](1).y - rotCenter.y) + rotCenter.x;
		tempPos.y = sint*(mSides[i](1).x - rotCenter.x) + cost*(mSides[i](1).y - rotCenter.y) + rotCenter.y;
		mSides[i](1) = tempPos;
		
		mTopLeft(0) = std::min(mTopLeft(0), mSides[i](0).x);
		mTopLeft(1) = std::max(mTopLeft(1), mSides[i](0).y);
		mBottomRight(0) = std::max(mBottomRight(0), mSides[i](0).x);
		mBottomRight(1) = std::min(mBottomRight(1), mSides[i](0).y);      
	}
  mTopLeft(0) = mSides[0](0).x;
  mTopLeft(1) = mSides[0](0).y;
  mBottomRight(0) = mSides[0](0).x;
  mBottomRight(1) = mSides[0](0).y;

  for(int i=0; i<mSides.size();i++)
  {
    mTopLeft(0) = std::min(mTopLeft(0), mSides[i](0).x);
    mTopLeft(1) = std::max(mTopLeft(1), mSides[i](0).y);
    mBottomRight(0) = std::max(mBottomRight(0), mSides[i](0).x);
    mBottomRight(1) = std::min(mBottomRight(1), mSides[i](0).y);  
  }


}

void
Polygon::rotateAndRound(float radian, Vector2f rotCenter)
{
  float cost = cos(radian);
  float sint = sin(radian);
  
  for(int i=0;i<mSides.size();i++)
  {
    Vector2f tempPos;

    tempPos.x = round(cost*(mSides[i](0).x - rotCenter.x) - sint*(mSides[i](0).y - rotCenter.y) + rotCenter.x);
    tempPos.y = round(sint*(mSides[i](0).x - rotCenter.x) + cost*(mSides[i](0).y - rotCenter.y) + rotCenter.y);
    mSides[i](0) = tempPos;

    tempPos.x = round(cost*(mSides[i](1).x - rotCenter.x) - sint*(mSides[i](1).y - rotCenter.y) + rotCenter.x);
    tempPos.y = round(sint*(mSides[i](1).x - rotCenter.x) + cost*(mSides[i](1).y - rotCenter.y) + rotCenter.y);
    mSides[i](1) = tempPos;
    
  }

  mTopLeft(0) = mSides[0](0).x;
  mTopLeft(1) = mSides[0](0).y;
  mBottomRight(0) = mSides[0](0).x;
  mBottomRight(1) = mSides[0](0).y;

  for(int i=0; i<mSides.size();i++)
  {
    mTopLeft(0) = std::min(mTopLeft(0), mSides[i](0).x);
    mTopLeft(1) = std::max(mTopLeft(1), mSides[i](0).y);
    mBottomRight(0) = std::max(mBottomRight(0), mSides[i](0).x);
    mBottomRight(1) = std::min(mBottomRight(1), mSides[i](0).y);  
  }
 
}

void 
Polygon::setVertices(const std::vector<Vector2f>& verts)
{
    mSides.clear();
    assert(verts.size() > 2);

    mTopLeft = verts[0];
    mBottomRight = verts[0];

    //create and store the segments from the vertices
    for (int i = 0, j = 1; i < verts.size(); i++)
    {
      if (i == verts.size() - 1)
        j = 0;
      else j = i + 1;      
      
      //naming conventions assumes origin bottom left
      mTopLeft(0) = std::min(mTopLeft(0), verts[i](0));
      mTopLeft(1) = std::max(mTopLeft(1), verts[i](1));
      mBottomRight(0) = std::max(mBottomRight(0), verts[i](0));
      mBottomRight(1) = std::min(mBottomRight(1), verts[i](1));      

      mSides.push_back(LineSegment(verts[i], verts[j]));      
    }
}

void 
Polygon::getVertices(std::vector<Vector2f>& verts)
{
	for (int i = 0; i < mSides.size(); i++)
    {
		verts.push_back(mSides[i](0));  
    }
}

void 
Polygon::draw(Image32& img, unsigned int color) const
{
  for (int i = 0; i < mSides.size(); i++)
  {
    mSides[i].draw(img, color);
  }  
}

//NOTE: below function GrayImage copy of the above
//TODO: refactor this properly when time available
void 
Polygon::draw(Image8& img, unsigned int color) const
{
  for (int i = 0; i < mSides.size(); i++)
  {
    mSides[i].draw(img, color);
  }  
}

std::vector<int> 
Polygon::edgesCrossed(const LineSegment& ls) const
{ 

  //TODO: try to speed this up, cull segments which cannot intersect
  // maybe just improve on the intersect function    

  assert(mSides.size() > 2);
  Vector2f intersectPoint;
  std::vector<EdgeDist> data;

  for (int iSide = 0; iSide < mSides.size(); iSide++)
  {

    if (ls.intersects(mSides[iSide], intersectPoint))
    {
      EdgeDist currentEdge;
      //don't need to use expensive sqrt, since distance only used for
      // comparison
      currentEdge.distance = 
        (intersectPoint(0) - ls(0)(0)) * (intersectPoint(0) - ls(0)(0)) + 
        (intersectPoint(1) - ls(0)(1)) * (intersectPoint(0) - ls(0)(1));
      currentEdge.edgeNum = iSide;
      data.push_back(currentEdge);
    }
  }

  // now sort the edges by their distance
  std::sort(data.begin(), data.end(), sortEdgeDist);

  std::vector<int> edges;

  //now just push the edges into a vector and return it
  for (int iter = 0; iter < data.size(); iter++)    
    edges.push_back(data[iter].edgeNum);

  // return the iterator
  return edges;    
    
}

bool 
Polygon::intersects(const LineSegment& ls, Vector2f& intersectPoint) const
{
  assert(mSides.size() > 2);

  bool intersects = false;

  Vector2f tempIntersectPoint;

  float shortestDist = 0.0;

  bool ranOnce = false;

  
  
  for (int iSide = 0; iSide < mSides.size(); iSide++)
  {

    if (ls.intersects(mSides[iSide], tempIntersectPoint))
    {      
      intersects = true;

      if (!ranOnce)
      {
        shortestDist = dist2(ls(0), tempIntersectPoint);
        intersectPoint = tempIntersectPoint;
      }
      else
      {
        if (dist2(ls(0), tempIntersectPoint) < shortestDist)
        {
          shortestDist = dist2(ls(0), tempIntersectPoint);
          intersectPoint = tempIntersectPoint;
        }
      }      
    }
  } 
  
  return intersects;    
}

float
Polygon::distanceNearestEdge(const Vector2f& point) const
{
  std::vector<LineSegment>::const_iterator iEdge;

  bool ranOnce = false;
  float closestDist = 0.0;

  int relativePos = 0;

  for (iEdge = mSides.begin(); iEdge != mSides.end(); iEdge++)
  {
    if (!ranOnce)
    {
      closestDist = iEdge->distanceToPoint(point, relativePos);
      ranOnce = true;
    }
    else
    {
      closestDist = std::min(iEdge->distanceToPoint(point, relativePos), closestDist);
    }
  }

  return closestDist;
}

  
//
// INQUIRY
//
 
bool
Polygon::isPointInside(const Vector2f& point) const
{
  assert(mSides.size() > 2);
  /*
   *	Creates a horizontal segment from point to the right
   *  of the internal bounding box. Counts number of intersections
   *  to determine whether or not the point is inside or outside of
   *  the polygon.
   */
  
  //Is the point inside the polygon's bounding box

  //NOTE: uses peturbed intersection function

  //NOTE: speed could be increased by not constructing an empty Vector2f
  // each iteration

  //Also, with a large number of edges, we would be better off by culling
  // the edges which cannot possibly intersect  

  if (point(0) > mTopLeft(0) && point(0) < mBottomRight(0)
    && point(1) < mTopLeft(1) && point(1) > mBottomRight(1) )
  {
    LineSegment ls(point, Vector2f(mBottomRight(0) + 1.0, point(1)));
    bool odd = false;    
    std::vector<LineSegment>::const_iterator iSeg;
    // Scan through all edges, counting number of intersections
    for (iSeg = mSides.begin(); iSeg != mSides.end(); iSeg++)
    {
      Vector2f temp; //this variable is not used
      if (ls.intersects(*iSeg, temp))        
        odd = !odd;
      
    }

    if (odd)
      return true;
    else
      return false;
  }
  else // point not inside bounding box
    return false;   
    
}



/* ============================================================
   scanFill
*/

void
Polygon::fill(Image32& img, unsigned int color) const
{
  //std::vector<Vector2i> pts;

  ////collect all vertexes
  //for (int i = 0; i < mSides.size(); i++)
  //{
  //  Vector2f t(mSides[i](0));
  //  pts.push_back(Vector2i(t.x, t.y));
  //}

  //fill(img, color, pts);
  float minLeft = mTopLeft.x<mBottomRight.x ? mTopLeft.x : mBottomRight.x;
  float maxRight = mTopLeft.x<mBottomRight.x ? mBottomRight.x : mTopLeft.x;
  float minTop = mTopLeft.y<mBottomRight.y ? mTopLeft.y : mBottomRight.y;
  float maxBottom = mTopLeft.y<mBottomRight.y ? mBottomRight.y : mTopLeft.y;
  for(int x=int(minLeft);x<int(maxRight);x++)
    for(int y=int(minTop);y<int(maxBottom);y++)
    {
      if(x>=0 && x<img.width() && y>=0 && y<img.height())
      if(isPointInside(Vector2f(x,y)))
        img(x,y)=color;
    }
}

//NOTE: below function GrayImage copy of the above
//TODO: refactor this properly when time available
void
Polygon::fill(Image8& img, unsigned int color) const
{
  //std::vector<Vector2i> pts;

  ////collect all vertexes
  //for (int i = 0; i < mSides.size(); i++)
  //{
  //  Vector2f t(mSides[i](0));
  //  pts.push_back(Vector2i(t.x, t.y));
  //}

  //fill(img, color, pts);

  float minLeft = mTopLeft.x<mBottomRight.x ? mTopLeft.x : mBottomRight.x;
  float maxRight = mTopLeft.x<mBottomRight.x ? mBottomRight.x : mTopLeft.x;
  float minTop = mTopLeft.y<mBottomRight.y ? mTopLeft.y : mBottomRight.y;
  float maxBottom = mTopLeft.y<mBottomRight.y ? mBottomRight.y : mTopLeft.y;
  for(int x=int(minLeft);x<int(maxRight);x++)
    for(int y=int(minTop);y<int(maxBottom);y++)
    {
      if(x>=0 && x<img.width() && y>=0 && y<img.height())
      if(isPointInside(Vector2f(x,y)))
        img(x,y)=color;
    }
}

void
Polygon::process(void (*funk)(int, int, void*), void* data)
{
  std::vector<Vector2i> pts;

  //collect all vertexes
  for (int i = 0; i < mSides.size(); i++)
  {
    Vector2f t(mSides[i](0));
    pts.push_back(Vector2i(t.x, t.y));
  }

  process(pts, funk, data);
}

void 
//Polygon::scanFill (Image32& img, unsigned int color, int cnt, dcPt * pts)
Polygon::fill (Image32& img, unsigned int color, std::vector<Vector2i>& pts) const
{  
  /*int w = img.width();
  int h = img.height();*/

  Edge ** edges = new Edge*[(int)mTopLeft.y+2]; //origin is assumed to be in bottom left corner!
  Edge * active;
  int i, scan;

  int cnt = mSides.size();  

  active = (Edge *) malloc (sizeof (Edge));
  for (i = 0; i < mTopLeft.y+1; i++) {
    edges[i] = (Edge *) malloc (sizeof (Edge));
    edges[i]->next = NULL;
  }
  active->next = NULL;
  buildEdgeList (pts, edges);
  for (scan = 0; scan < mTopLeft.y+1; scan++) {
    buildActiveList (scan, active, edges);
    if (active->next) {
      fillScan (img, color, scan, active);
      updateActiveList (scan, active);
      resortActiveList (active);
    }
  }
  delete [] edges;
}

//NOTE: below function GrayImage copy of the above
//TODO: refactor this properly when time available
void 
//Polygon::scanFill (Image32& img, unsigned int color, int cnt, dcPt * pts)
Polygon::fill (Image8& img, unsigned int color, std::vector<Vector2i>& pts) const
{  
  /*int w = img.width();
  int h = img.height();*/

  Edge ** edges = new Edge*[(int)mTopLeft.y+2]; //origin is assumed to be in bottom left corner!
  Edge * active;
  int i, scan;

  int cnt = mSides.size();  

  active = (Edge *) malloc (sizeof (Edge));
  for (i = 0; i < mTopLeft.y+1; i++) {
    edges[i] = (Edge *) malloc (sizeof (Edge));
    edges[i]->next = NULL;
  }
  active->next = NULL;
  buildEdgeList (pts, edges);
  for (scan = 0; scan < mTopLeft.y+1; scan++) {
    buildActiveList (scan, active, edges);
    if (active->next) {
      fillScan (img, color, scan, active);
      updateActiveList (scan, active);
      resortActiveList (active);
    }
  }
  delete [] edges;
}

void 
Polygon::process(std::vector<Vector2i>& pts,void (*funk)(int, int, void*), void* data)
{  
  //int w = img.width();
  //int h = img.height();

  Edge ** edges = new Edge*[(int)mTopLeft.y+2];
  Edge * active;
  int i, scan;

  int cnt = mSides.size();  

  active = (Edge *) malloc (sizeof (Edge));
  for (i = 0; i < mTopLeft.y+1; i++) {
    edges[i] = (Edge *) malloc (sizeof (Edge));
    edges[i]->next = NULL;
  }
  active->next = NULL;
  buildEdgeList (pts, edges);
  for (scan = 0; scan < mTopLeft.y+1; scan++) {
    buildActiveList (scan, active, edges);
    if (active->next) {
      //fillScan (img, color, scan, active);
      processScan(scan, active, funk, data);
      updateActiveList (scan, active);
      resortActiveList (active);
    }
  }
  delete [] edges;
}

/* Inserts edge into list in order of increasing xIntersect field. */
void 
Polygon::insertEdge (Edge * list, Edge * edge) const
{
  Edge * q = list;
  Edge * p = q->next;
  while (p != NULL) {
    if (edge->xIntersect < p->xIntersect)
      p = NULL;
    else {
      q = p;
      p = p->next;
    }
  }
  edge->next = q->next;
  q->next = edge;
}

/* Given an index, return the y-coordinate of next nonhorizontal line */
int 
Polygon::yNext (int k, std::vector<Vector2i>& pts) const
{
  int j;

  int cnt = pts.size();

  if ((k+1) > (cnt-1))
    j = 0;
  else
    j = k + 1;
  while (pts[k].y == pts[j].y)
    if ((j+1) > (cnt-1))
      j = 0;
    else
      j++;
  return (pts[j].y);
}

void 
Polygon::makeEdgeRec(Vector2i& lower, Vector2i& upper, int yComp, Edge * edge, Edge * edges[]) const
{
  edge->dxPerScan =
    (float) (upper.x - lower.x) / (upper.y - lower.y);
  edge->xIntersect = lower.x;
  if (upper.y < yComp)
    edge->yUpper = upper.y - 1;
  else
    edge->yUpper = upper.y;
  insertEdge (edges[lower.y], edge);
}

void 
Polygon::buildEdgeList (std::vector<Vector2i>& pts, Edge * edges[]) const
{
  Edge * edge;
  Vector2i v1, v2;
  int i;
  
  int cnt = pts.size();

  int yPrev = pts[cnt - 2].y;

  v1.x = pts[cnt-1].x; v1.y = pts[cnt-1].y;
  for (i = 0; i < cnt; i++) {
    v2 = pts[i];
    if (v1.y != v2.y) {  /* a nonhorizontal line */
      edge = (Edge *) malloc (sizeof (Edge));
      if (v1.y < v2.y)
	makeEdgeRec (v1, v2, yNext (i, pts), edge, edges);
      else
	makeEdgeRec (v2, v1, yPrev, edge, edges);
    }
    yPrev = v1.y;
    v1 = v2;
  }
}

void 
Polygon::buildActiveList (int scan, Edge * active, Edge * edges[]) const
{
  Edge * p, * q;
  p = edges[scan]->next;
  while (p) {
    q = p->next;
    insertEdge (active, p);
    p = q;
  }
}

void 
Polygon::fillScan (Image32& img, unsigned int color, int scan, Edge * active) const
{
  Edge * p1, * p2;
  int i;

  unsigned int** pImg = img.pointerpointer();

  p1 = active->next;
  while (p1) {
    p2 = p1->next;
    for (i = p1->xIntersect; i < p2->xIntersect; i++)
    {
      //setPixel ((int) i, scan);
      //img.safe_set((int) i, scan, color);
      pImg[scan][(int) i] = color;
      
      //unsigned int oldC = pImg[scan][(int) i];
      //pImg[scan][(int) i] = PV_RGB((int)(0.5f*(RED(oldC)) + 0.5f*(RED(color))), (int)(0.5f*(GREEN(oldC)) + 0.5f*GREEN(color)), (int)(0.5f*BLUE(oldC) + 0.5f*BLUE(color)));
    }

    /* Advance to next pair of intersections */
    p1 = p2->next;
  }
}

//NOTE: below function GrayImage copy of the above
//TODO: refactor this properly when time available
void 
Polygon::fillScan (Image8& img, unsigned int color, int scan, Edge * active) const
{
  Edge * p1, * p2;
  int i;

  unsigned char** pImg = img.pointerpointer();

  p1 = active->next;
  while (p1) {
    p2 = p1->next;
    for (i = p1->xIntersect; i < p2->xIntersect; i++)
    {
      //setPixel ((int) i, scan);
      //img.safe_set((int) i, scan, color);
      pImg[scan][(int) i] = color;
      
      //unsigned int oldC = pImg[scan][(int) i];
      //pImg[scan][(int) i] = PV_RGB((int)(0.5f*(RED(oldC)) + 0.5f*(RED(color))), (int)(0.5f*(GREEN(oldC)) + 0.5f*GREEN(color)), (int)(0.5f*BLUE(oldC) + 0.5f*BLUE(color)));
    }

    /* Advance to next pair of intersections */
    p1 = p2->next;
  }
}

void 
Polygon::processScan(int scan, Edge * active, void (*funk)(int, int, void*), void* data)
{
  Edge * p1, * p2;
  int i;

  p1 = active->next;
  while (p1) {
    p2 = p1->next;
    for (i = p1->xIntersect; i < p2->xIntersect; i++)
    {
      funk(i, scan, data);
    }

    /* Advance to next pair of intersections */
    p1 = p2->next;
  }
}

void 
Polygon::deleteAfter (Edge * q) const
{
  Edge * p = q->next;
  q->next = p->next;
  free (p);
}

void 
Polygon::updateActiveList (int scan, Edge * active) const
{
  Edge * p = active->next;
  Edge * q = active;
  while (p) {
    if (scan >= p->yUpper) {
      p = p->next;
      deleteAfter (q);
    }
    else {
      p->xIntersect = p->xIntersect + p->dxPerScan;
      q = p;
      p = p->next;
    }
  }
}

void 
Polygon::resortActiveList (Edge * active) const
{
  Edge * p = active->next;
  Edge * q;
  active->next = NULL;
  while (p) {
    q = p->next;
    insertEdge (active, p);
    p = q;
  }
}

}; // namespace ait




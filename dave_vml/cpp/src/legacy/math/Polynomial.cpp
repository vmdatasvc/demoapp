/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include <legacy/math/Polynomial.hpp>

//STL includes
#include <numeric>

#include <cmath>

//our includes
#include <legacy/pv/PvUtil.hpp> //used to fix for loop scope problem

namespace ait
{

//LIFECYCLE

Polynomial::Polynomial(const std::vector<double>& coeff)
{
  assert(coeff.size() > 0);
  mCoefficients = coeff;    
}

//OPERATIONS

void 
Polynomial::init(const std::vector<double>& coeff)
{
  assert(coeff.size() > 0);  
  mCoefficients = coeff;
}

void 
Polynomial::init(const std::list<double>& coeff)
{
  assert(coeff.size() > 0);

  mCoefficients.clear();

  std::list<double>::const_iterator iCoeff;
  for (iCoeff = coeff.begin(); iCoeff != coeff.end(); iCoeff++)
  {
    mCoefficients.push_back(*iCoeff);
  }  
}

double 
Polynomial::evaluate(double val) const
{ 
  double sum = 0.0;    

  for (int x = 0; x < mCoefficients.size(); x++)
  {      
    //sum += mCoefficients[x]*pow(val, mCoefficients.size() - x - 1);      
    sum += mCoefficients[x]*pow(val, (int)(mCoefficients.size() - x - 1));
  }
  return sum;
} 
  

//horrible hack due to MSVC STL problems

Polynomial::Complex
Polynomial::evaluate(const Polynomial::Complex& val) const
{
  Polynomial::Complex sum(0.0,0.0);    

  for (int x = 0; x < mCoefficients.size(); x++)
  {      
    sum += Polynomial::Complex::pow(val, mCoefficients.size() - x - 1) * mCoefficients[x];      
  }
  return sum;
}


//ACCESS

Polynomial 
Polynomial::getMonic() const
{
  std::vector<double> coeff = mCoefficients;

  double divisor = coeff[0];
  
  assert(divisor != 0.0);

  for (int i = 0; i < coeff.size(); i++)
  {
    coeff[i] /= divisor;
  }    

  return Polynomial(coeff);
}


std::vector<double> 
Polynomial::getRealRoots() const
{
  std::vector<double> realRoots;

  std::vector<Polynomial::Complex> roots = getRoots();

  //for (int i = 0; i < roots.size(); i++)
  //  PVMSG("ROOT %d: %f %fi\n", i, roots[i].realPart(), roots[i].imagPart());


  //hackish way of doing things, may work for now

  for (int i = 0; i < roots.size(); i++)
  {
    if (fabs(roots[i].imagPart()) < 0.00000000001)
      realRoots.push_back(roots[i].realPart());
  }

  return realRoots;
}



std::vector<Polynomial::Complex>
Polynomial::getRoots() const
{
  std::vector<Polynomial::Complex> values;
  std::vector<Polynomial::Complex> oldValues;

  //Uses Durand-Kerner method

  //PVMSG("Original polynomial:\n");
  //for (int i = 0; i < mCoefficients.size(); i++)
  //  PVMSG("\t%d: %f\n", i, mCoefficients[i]);
    
  Polynomial monicPoly = getMonic();

  values.resize(mCoefficients.size() - 1);
  oldValues.resize(mCoefficients.size() - 1);

  //PVMSG("Durand-Kerner Start\n");
  //PVMSG("0\t");

  //Assign starting values
  for (int i = 0; i < mCoefficients.size() - 1; i++)
  {
    values[i] = Polynomial::Complex::pow(Polynomial::Complex(0.4,0.9), i);        
    //PVMSG("%f %fi\t", values[i].realPart(),values[i].imagPart());
  }

  //PVMSG("\n");

  //int tempCount = 1;
    
  while (true)
  {
    oldValues = values;      

    //PVMSG("%d\t",tempCount++);

    for (int x = 0; x < mCoefficients.size() - 1; x++)
    {
      Polynomial::Complex num = monicPoly.evaluate(values[x]);

      //calculate denominator
      std::vector<Polynomial::Complex> denomList;
        
      for (int y = 0; y < mCoefficients.size() - 1; y++)
      {
        if (y != x)
          denomList.push_back(values[x] - values[y]);
      }
      Polynomial::Complex den = std::accumulate(
        denomList.begin(),
        denomList.end(),
        Polynomial::Complex(1.0,0.0),
        std::multiplies<Polynomial::Complex>());
        
      values[x] = values[x] - num/den;       
      //PVMSG("%f %fi\t", values[x].realPart(),values[x].imagPart());
    }
    //PVMSG("\n");

    //Break out of the loop when the accuracy is high enough
    bool breakOut = true;

    for (int x = 0; x < mCoefficients.size() - 1; x++)
    {
        
      if (Polynomial::Complex::abs(values[x] - oldValues[x]) > 0.00000000001)
        breakOut = false;        
    }

    if (breakOut)
    {
      break;
    }
  }

  return values;
}


double
Polynomial::solve(double val) const
{
  Polynomial p = *this;

  int last = p.mCoefficients.size() - 1;
  p.mCoefficients[last] -= val;
  
  std::vector<double> roots = p.getRealRoots();


  //for (int i = 0; i < roots.size(); i++)
//    PVMSG("ROOT %d: %f\n", i, roots[i]);

  //TODO: handle case with 0 or multiple solutions
  assert(roots.size() == 1);

  return roots.front();
}

}; // namespace ait



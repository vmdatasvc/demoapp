/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "legacy/math/Homography2D.hpp"

//#include <PvUtil.hpp>


namespace ait 
{

static const int WEIGHT1 = 1;
static const int WEIGHT2 = 1;

Homography2D::Homography2D()
{
}

Homography2D::Homography2D(const Homography2D& from)
{
  mHomography = from.mHomography;
}

Homography2D::~Homography2D()
{
}

//
// OPERATORS
//

Homography2D& 
Homography2D::operator=(const Homography2D& from)
{
  if (this != &from)
  {
    // Assign data members
    mHomography = from.mHomography;
  }
  return *this;
}

bool
Homography2D::operator==(const Homography2D& from) const
{
  // Compare members
  //PvUtil::exitError("Comparison not implemented.");
  return mHomography == from.mHomography;
  return false;
}

bool
Homography2D::operator!=(const Homography2D& from) const
{
  return !(*this == from);
}

//
// OPERATIONS
//

void 
Homography2D::convert(Matrix<float>& matrix, int nRows, int nCols)
{
  if (nRows*nCols != matrix.cols()*matrix.rows())
    PVMSG("Convert parameters are wrong.\n");
  
  Matrix<float> temp(nRows, nCols);
  
  for (int x=0; x<nRows; ++x)
  {
    for (int y=0; y<nCols; ++y)
    {
      temp(x,y) = matrix(x*nCols+y,0);
    }
  }
  
  matrix.resize(nRows,nCols);
  
  for (int x=0; x<nRows; ++x)
  {
    for (int y=0; y<nCols; ++y)
    {
      matrix(x,y) = temp(x,y);
    }
  }
}



void 
Homography2D::normalization2D(std::vector<Vector3f>& points, std::vector<Vector3f>& normPoints, Matrix<float>& translationMatrix)
{
  normPoints.erase(normPoints.begin(), normPoints.end());
  
  iteratorPvCoord3f iterCPC;
  
  float average1_X=0;
  float average1_Y=0;
  
  for (iterCPC=points.begin(); iterCPC!=points.end(); ++iterCPC)
  {
    average1_X += (*iterCPC)(0);
    average1_Y += (*iterCPC)(1);
  }
  average1_X /= points.size();
  average1_Y /= points.size();
  
  float temp=0;
  
  for (iterCPC=points.begin(); iterCPC!=points.end(); ++iterCPC)
  {
    temp += sqrt( pow((*iterCPC)(0) - average1_X, 2) + pow((*iterCPC)(1) - average1_Y, 2));
  }
  temp = temp/points.size(); 
  
  translationMatrix(0,0) = 1/temp;
  translationMatrix(0,1) = 0;
  translationMatrix(0,2) = -average1_X/temp;
  translationMatrix(1,0) = 0;
  translationMatrix(1,1) = 1/temp;
  translationMatrix(1,2) = -average1_Y/temp;
  translationMatrix(2,0) = 0;
  translationMatrix(2,1) = 0;
  translationMatrix(2,2) = 1;
  
	Vector3f cp; 
  float val1, val2, val3;
  //const float f = sqrt(2.0f);
  
  for (iterCPC=points.begin(); iterCPC!=points.end(); ++iterCPC)
  {
    val1 = (*iterCPC)(0)*translationMatrix(0,0) + (*iterCPC)(1)*translationMatrix(0,1) + translationMatrix(0,2);
    val2 = (*iterCPC)(0)*translationMatrix(1,0) + (*iterCPC)(1)*translationMatrix(1,1) + translationMatrix(1,2);
    val3 = (*iterCPC)(0)*translationMatrix(2,0) + (*iterCPC)(1)*translationMatrix(2,1) + translationMatrix(2,2);
    val1/=val3;
    val2/=val3;
		cp.set(val1,val2);
    
    //normPoints.push_back(*cp); 
		normPoints.push_back(cp); 
  }
}

bool 
Homography2D::DLT(Matrix<float>& matrix, Matrix<float>& transformationMatrix)
{
  const int m = matrix.rows();
  const int n = matrix.cols();
  Matrix<float> S(m,1);
  Matrix<float> V(n,n);  

  /*matrix.printf("%.4f ");
  PVMSG("  =  \n");*/

  int fail = matrix.svd(S, V);
  if (fail)
     return false;  

  singularSorting(matrix.pointerpointer(),S.pointer(),V.pointerpointer(), matrix.rows(), matrix.cols());

 /* matrix.printf("%.4f ");
  PVMSG("  *  \n");
  S.printf("%.4f ");
  PVMSG("  *  \n");
  V.printf("%.4f ");
  PVMSG("  *  \n");*/

  transformationMatrix.resize(matrix.cols(),1);
  
  float* pTrans = transformationMatrix.pointer();
  float* pV = V.pointer();
  int w = matrix.cols();  
  for (int i=0, j=w-2; i<matrix.cols(); ++i, j+=w) 
    pTrans[i] = pV[j];   
 
  return true;
}


bool 
Homography2D::compute(std::vector<Vector3f>& points_ref, std::vector<Vector3f>& points_proj)
{ 
  std::vector<Vector3f> normPoints_ref;
  std::vector<Vector3f> normPoints_proj;

	Matrix<float> normMatrix_ref(3,3);
	Matrix<float> normMatrix_proj(3,3); 
	Matrix<float> homographyCap(9,1); 

	normalization2D(points_ref,  normPoints_ref, normMatrix_ref);
	normalization2D(points_proj, normPoints_proj, normMatrix_proj);
 
  mHomography.resize(3,3);
  
  Matrix<float> popMat;
  
  populate(popMat, normPoints_ref, normPoints_proj);

	if(DLT(popMat, homographyCap))
  {
		convert(homographyCap, 3, 3);

    // Multiplication with Homography matrices   
		normMatrix_ref.inv();    
		homographyCap.mult(normMatrix_proj);
		normMatrix_ref.mult(homographyCap);

    for(int i=0; i<3; i++)
    {
      for(int j=0; j<3; j++)
      {
				mHomography(i,j) = normMatrix_ref(i,j);
      }
    }
  }
  else
    return false;

  return true;
}

void 
Homography2D::populate(Matrix<float>& matrix, std::vector<Vector3f>& imagePoints, std::vector<Vector3f>& groundPoints)
{
  int n = 2*imagePoints.size();
  matrix.resize(n+1, 9);
  //matrix.setAll(0);
  
  iteratorPvCoord3f iter1;
  iteratorPvCoord3f iter2;  
  
  iter1 = groundPoints.begin();
  iter2 = imagePoints.begin();
  
  int count=0;
  for ( ; iter1 != groundPoints.end(); ++iter1, ++iter2, ++count)
  {
    matrix(2*count,0) = 0.0f;
    matrix(2*count,1) = 0.0f;
    matrix(2*count,2) = 0.0f;
    matrix(2*count,3) = -(*iter1)(0);
    matrix(2*count,4) = -(*iter1)(1);   
    matrix(2*count,5) = -1;
    matrix(2*count,6) =  (*iter2)(1) * (*iter1)(0);
    matrix(2*count,7) =  (*iter2)(1) * (*iter1)(1);   
    matrix(2*count,8) = (*iter2)(1);

    matrix(2*count+1,0) =   (*iter1)(0);
    matrix(2*count+1,1) =   (*iter1)(1);   
    matrix(2*count+1,2) =   1;
    matrix(2*count+1,3) = 0.0f;
    matrix(2*count+1,4) = 0.0f;
    matrix(2*count+1,5) = 0.0f;
    matrix(2*count+1,6) =  -(*iter2)(0) * (*iter1)(0);
    matrix(2*count+1,7) =  -(*iter2)(0) * (*iter1)(1);   
    matrix(2*count+1,8) = -(*iter2)(0);    
  }  

  
  for(int i=0;i<9;i++)
    matrix(n,i) = 0;
}


void 
Homography2D::singularSorting(float** u, float* s, float** v, int m, int n)
{
  float temp;
  int index = (m<n)?  m: n;
  for (int i=0; i<index; ++i)
  {
    for (int j=i+1; j<index; ++j)
    {
      if (s[i] < s[j])
      {
        temp = s[i];
        s[i] = s[j];
        s[j] = temp;

        for(int k=0; k<m; k++)
        {
          temp = u[k][i];
          u[k][i] = u[k][j];
          u[k][j] = temp;
        }
        
        for(int k=0; k<n; k++)
        {
          temp = v[k][i];
          u[k][i] = v[k][j];
          v[k][j] = temp;
        }
      }
    }
  }
}
 

void 
Homography2D::transform(const Vector3f& ptIn, Vector3f& ptOut)
{
  float** pH = mHomography.pointerpointer();

  ptOut.x = pH[0][0]*ptIn.x + pH[0][1]*ptIn.y + pH[0][2];
  ptOut.y = pH[1][0]*ptIn.x + pH[1][1]*ptIn.y + pH[1][2];
  ptOut.z = pH[2][0]*ptIn.x + pH[2][1]*ptIn.y + pH[2][2];
  ptOut.x /= ptOut.z;
  ptOut.y /= ptOut.z;
}


void 
Homography2D::transform(Vector3f& ptInOut)
{
	float ptIn_x = ptInOut.x;
	float ptIn_y = ptInOut.y;
	float** pH = mHomography.pointerpointer();

	ptInOut.x = pH[0][0]*ptIn_x + pH[0][1]*ptIn_y + pH[0][2];
	ptInOut.y = pH[1][0]*ptIn_x + pH[1][1]*ptIn_y + pH[1][2];
	ptInOut.z = pH[2][0]*ptIn_x + pH[2][1]*ptIn_y + pH[2][2];
	ptInOut.x /= ptInOut.z;
	ptInOut.y /= ptInOut.z;
}

void
Homography2D::invert()
{
	mHomography.inv();
}
//
// ACCESS
//

//
// INQUIRY
//

}; // namespace ait



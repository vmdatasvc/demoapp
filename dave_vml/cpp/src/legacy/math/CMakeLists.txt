project (legacy_mathLib)
set (LIB_NAME legacy_math)

set (LIB_SRC
	Homography2D.cpp
	LineSegment.cpp
	Polygon.cpp
	Polynomial.cpp
	PvKalman.cpp
	)

vml_add_library (${LIB_NAME}
	STATIC
#	${GEN_SRC}
	${LIB_SRC}
	${PROJECT_HEADER_FILES}
	)

target_link_libraries (${LIB_NAME} core)


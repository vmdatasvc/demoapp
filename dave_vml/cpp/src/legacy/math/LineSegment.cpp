/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include <legacy/math/LineSegment.hpp>


namespace ait
{ 

float LineSegment::distanceToPoint(
  const Vector2f& point, 
  int& relativePosition) const
{    
  if (mStartPoint == mEndPoint)
    return sqrt( dist2(point, mStartPoint) );
   
  //The following algorithm found on the comp.graphics.algorithms FAQ
  //Variable names matched to those in the algorithm

  float Ax = mStartPoint(0);
  float Ay = mStartPoint(1);
  float Bx = mEndPoint(0);
  float By = mEndPoint(1);

  float Cx = point(0);
  float Cy = point(1);

  float lengthSquared =  (Bx-Ax)*(Bx-Ax) + (By-Ay)*(By-Ay);

  float rNumerator = (Cx-Ax)*(Bx-Ax) + (Cy-Ay)*(By-Ay);
  float r = rNumerator/lengthSquared;

  float sNumerator = (Ay - Cy) * (Bx - Ax) - (Ax-Cx) * (By - Ay);    
  float s = sNumerator/lengthSquared;

  float distance = fabs(s) * sqrt(lengthSquared);    
    
    
  if (s < 0)
    relativePosition = LineSegment::POINT_LEFT;
  else if (s > 0)
    relativePosition = LineSegment::POINT_RIGHT;
  else if (r < 0)
  {
    relativePosition = LineSegment::POINT_BACKWARD_EXTENSION;
    distance = sqrt( (Ax-Cx)*(Ax-Cx) + (Ay-Cy)*(Ay-Cy) );
  }
  else if (r > 1)
  {
    relativePosition = LineSegment::POINT_FORWARD_EXTENSION;
    distance = sqrt( (Bx-Cx)*(Bx-Cx) + (By-Cy)*(By-Cy) );
  }    
  else
    relativePosition = LineSegment::POINT_ON_SEGMENT;    
    
       
  return distance;
} 
  
Vector2f 
LineSegment::closestPoint(const Vector2f& point)
{
  if (mStartPoint == mEndPoint)
    return mStartPoint;
   
  //The following algorithm found on the comp.graphics.algorithms FAQ
  //Variable names matched to those in the algorithm

  int relativePosition = 0;

  float Ax = mStartPoint(0);
  float Ay = mStartPoint(1);
  float Bx = mEndPoint(0);
  float By = mEndPoint(1);

  float Cx = point(0);
  float Cy = point(1);

  float lengthSquared =  (Bx-Ax)*(Bx-Ax) + (By-Ay)*(By-Ay);

  float rNumerator = (Cx-Ax)*(Bx-Ax) + (Cy-Ay)*(By-Ay);
  float r = rNumerator/lengthSquared;

  float sNumerator = (Ay - Cy) * (Bx - Ax) - (Ax-Cx) * (By - Ay);    
  float s = sNumerator/lengthSquared;

  //float distance = fabs(s) * sqrt(lengthSquared);        

  Vector2f closest;
  closest.x = Ax + r * (Bx - Ax);
  closest.y = Ay + r * (By - Ay);
    
  if (s < 0)
    relativePosition = LineSegment::POINT_LEFT;
  else if (s > 0)
    relativePosition = LineSegment::POINT_RIGHT;
  else if (r < 0)
  {
    relativePosition = LineSegment::POINT_BACKWARD_EXTENSION;
    //distance = sqrt( (Ax-Cx)*(Ax-Cx) + (Ay-Cy)*(Ay-Cy) );
    closest = mStartPoint;
  }
  else if (r > 1)
  {
    relativePosition = LineSegment::POINT_FORWARD_EXTENSION;
    //distance = sqrt( (Bx-Cx)*(Bx-Cx) + (By-Cy)*(By-Cy) );
    closest = mEndPoint;
  }    
  else
    relativePosition = LineSegment::POINT_ON_SEGMENT;        
       
  return closest;
}
 

bool 
LineSegment::intersects(
  const LineSegment& other, 
  Vector2f& intersectPoint) const
{

  //make sure both segments have nonzero length
  if (mStartPoint == mEndPoint || other(0) == other(1))
    return false;    

  //check to see if one, both, or none of the other segments vertices lie to the 'left'
  // of the current linesegment. The idea is that the current linesegment can be used to split 
  // the coordinate space in half. If the other linesegment does not cross the median at all, it cannot
  // possibly intersect this line segment.

  int startPointPos = 0;
  int endPointPos = 0;
  distanceToPoint(other(0), startPointPos);
  distanceToPoint(other(1), endPointPos);

  //both points are to the left
  if (startPointPos == LineSegment::POINT_LEFT && endPointPos == LineSegment::POINT_LEFT)     
    return false;    
  //only one point is to the left
  else if (startPointPos == LineSegment::POINT_LEFT || endPointPos == LineSegment::POINT_LEFT)
  {      
    //check for intersection
    float Ax = mStartPoint(0);
    float Ay = mStartPoint(1);
    float Bx = mEndPoint(0);
    float By = mEndPoint(1);
    float Cx = other(0)(0);
    float Cy = other(0)(1);
    float Dx = other(1)(0);
    float Dy = other(1)(1);

    float rNumerator = (Ay-Cy)*(Dx-Cx)-(Ax-Cx)*(Dy-Cy);
    float sNumerator = (Ay-Cy)*(Bx-Ax)-(Ax-Cx)*(By-Ay);
    float denominator = (Bx-Ax)*(Dy-Cy)-(By-Ay)*(Dx-Cx);


    if (denominator == 0.0) // parallel segments
      return false;

    //should never happen, collinear case
    assert( !(denominator == 0 && rNumerator == 0) ); 
    assert(denominator != 0.0);

    float r = rNumerator/denominator;
    float s = sNumerator/denominator;

    //intersection occurs
    if (r >= 0.0 && r <= 1.0 && s >= 0.0 && s <= 1.0)
    {        
      intersectPoint(0) = Ax + r*(Bx-Ax);
      intersectPoint(1) = Ay + r*(By-Ay);
      return true;
    }
    //no intersection occurs
    else
    {
      return false;
    }

  }
  // no points are to the left, no intersection possible
  else
  {      
    return false;
  }
} 

Vector2f
LineSegment::midpoint() const
{
  Vector2f result;
  result.x = (mStartPoint.x + mEndPoint.x)/2.0;
  result.y = (mStartPoint.y + mEndPoint.y)/2.0;
  return result;
}

void 
LineSegment::draw(Image32& img, unsigned int color) const
{
  img.line( (int)(mStartPoint(0) +.5), (int)(mStartPoint(1)+.5), 
    (int)(mEndPoint(0)+.5), (int)(mEndPoint(1)+.5), color);
}

void 
LineSegment::draw(Image8& img, unsigned int color) const
{
  img.line( (int)(mStartPoint(0) +.5), (int)(mStartPoint(1)+.5), 
    (int)(mEndPoint(0)+.5), (int)(mEndPoint(1)+.5), color);
}

}; // namespace ait

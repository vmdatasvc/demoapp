/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include "legacy/math/PvKalman.hpp"

namespace ait
{

/************************************************************************
 ** 
 **  PvKalman
 **
 ************************************************************************/

/*********************************************************************
 * CONSTRUCTOR
 * 
 * Constructs a kalman filter with state space dimension n 
 * and observation space dimension m.
 *
 * <input> 
 * 
 * unsigned int n: state space dimension
 * unsigned int m: observation space dimension
 *
 * <output>
 *
 * void
 *
 * <error handling>
 * 
 * TODO
 *
 *********************************************************************/
PvKalman::PvKalman(unsigned int n, unsigned int m)
{
  setSize(n,m);
}


void PvKalman::setSize(unsigned int n, unsigned int m)
{
  I.resize(n,n);
  K.resize(n,m);
  H.resize(m,n);
  A.resize(n,n);
  x1.resize(n,1);
  x2.resize(n,1);
  R.resize(m,m);
  Q.resize(n,n);
  P1.resize(n,n);
  P2.resize(n,n);

  I.id();
}  

/*********************************************************************
 * init()
 * 
 * Initialize the various matrices of the Kalman filter.
 *
 * <input> 
 * 
 * Matrix<float> &x0,&P0,&A0,&H0,&Q0,&R0 : matrices
 *
 * <output>
 *
 * void
 *
 * <error handling>
 * 
 * TODO
 *
 *********************************************************************/
void PvKalman::init(const Matrix<float> &x0,const Matrix<float> &P0,const Matrix<float> &A0,
	    const Matrix<float> &H0,const Matrix<float> &Q0,const Matrix<float> &R0)
{
  int nKal=A0.rows(),mKal=H0.rows();
  
  setSize(nKal,mKal);

  // check that the initialization matrices have the correct size
  if(x0.cols()!=x1.cols() || x0.rows()!=x1.rows())
    PvUtil::exitError("PvKalman::init(): x0 has wrong dimensions");
  if(P0.cols()!=P1.cols() || P0.rows()!=P1.rows())
    PvUtil::exitError("PvKalman::init(): P0 has wrong dimensions");
  if(A0.cols()!=A.cols() || A0.rows()!=A.rows())
    PvUtil::exitError("PvKalman::init(): A0 has wrong dimensions");
  if(H0.cols()!=H.cols() || H0.rows()!=H.rows())
    PvUtil::exitError("PvKalman::init(): H0 has wrong dimensions");
  if(Q0.cols()!=Q.cols() || Q0.rows()!=Q.rows())
    PvUtil::exitError("PvKalman::init(): Q0 has wrong dimensions");
  if(R0.cols()!=R.cols() || R0.rows()!=R.rows())
    PvUtil::exitError("PvKalman::init(): R0 has wrong dimensions");

  x2=x1=x0;
  P1=P0;
  A=A0;
  A_T=A;A_T.transpose();
  H=H0;
  H_T=H;H_T.transpose();
  Q=Q0;
  R=R0;

  if(0)
  {
  PVMSG("Stochastic Equation x=A*x+N(0,Q)\n");
  PVMSG("A=\n");
  A_T.printf("%7.2f ");
  PVMSG("Q=\n");
  Q.printf("%7.2f ");

  PVMSG("Measurement Equation z=H*x+N(0,R)\n");
  PVMSG("H=\n");
  H.printf("%7.2f ");
  PVMSG("R=\n");
  R.printf("%7.2f ");

  PVMSG("-------------------\n");
  PVMSG("P1=\n");
  P1.printf("%7.2f ");
  }
  calcGain();
}

/*********************************************************************
 * calcGain()
 * 
 * Iteratively calculate the Kalman gain.
 *
 * <input> 
 * 
 * void
 *
 * <output>
 *
 * void
 *
 * <error handling>
 * 
 * TODO
 *
 *********************************************************************/
void PvKalman::calcGain(void)
{
  unsigned int i;
  const unsigned int nIter=50;

  for(i=0;i<nIter;i++)
  {
    // calculate K
     t0.mult(H,P1);
    t1.mult(t0,H_T);
    t1.add(R);
    t2.invFast(t1,tinv1,tinv2);
    t3.mult(H_T,t2);
    K.mult(P1,t3);

    // calculate a posteriori P (correction)
    t4.mult(K,H);
    t5.sub(I,t4);
    P2.mult(t5,P1);
    P1=P2;    

    // calculate next a priori P (prediction)
    t4.mult(A,P1);
    t5.mult(t4,A_T);
    P1.add(t5,Q);

    //PVMSG("P1=\n");
    //P1.printf("%g ");
    //PVMSG("K=\n");
    //K.printf("%g ");
  }

  //PVMSG("K=\n");
  //K.printf("%g ");
}

/*********************************************************************
 * predictNext()
 * 
 * Performs Kalman predication + update step.
 *
 * <input> 
 * 
 * Matrix<float> &z
 *
 * <output>
 *
 * void
 *
 * <error handling>
 * 
 * TODO
 *
 *********************************************************************/
void PvKalman::predictNext(Matrix<float> &z)
{
  t0.mult(H,x1);
  t1.sub(z,t0);
  t2.mult(K,t1);
  x2.add(x1,t2);
  x1.mult(A,x2);
  z.mult(H,x1);
}

/*********************************************************************
 * predict()
 * 
 * Given an a priori (before an observation becomes known)
 * prediction of the state x-(k), give back an estimate of the
 * observation z-(k).
 *
 * <input> 
 * 
 * void
 *
 * <output>
 *
 * Matrix<float> &z : observation vector
 *
 * <error handling>
 * 
 *********************************************************************/
void PvKalman::predict(Matrix<float> &z)
{
  x1.mult(A,x2);//predict new state
  z.mult(H,x1); //convert prediction to measurement space
}

/*********************************************************************
 * update()
 * 
 * Given an actual measurement z(k), update the predicted
 * state to an a posteriori state x(k+1) and update the measurement
 * to reflect the actual observation.
 *
 * <input> 
 * 
 * Matrix<float> &z : observation vector z(k)
 *
 * <output>
 *
 * Matrix<float> &z : corrected observation vector z(k)
 *
 * <error handling>
 * 
 *********************************************************************/
void PvKalman::update(Matrix<float> &z)
{  
  t0.mult(H,x1);//convert predicted state to measurement space
  t1.sub(z,t0); //difference to prediction (error)
  t2.mult(K,t1);//weight difference
  x2.add(x1,t2);//add weighted difference to predcited state (update current state)
  z.mult(H,x2); //convert updated state to measurement space
  //x1.mult(A,x2);//predict new state
}


PvKalman& PvKalman::operator= (const PvKalman& kalman)
{
  if(this!=&kalman)
  {
    I=kalman.I;
    K=kalman.K;	    // Kalman gain
    H=kalman.H;	    // measurement matrix
    H_T=kalman.H_T;	    //  - " - transpose
    A=kalman.A;	    // propagation matrix
    A_T=kalman.A_T;	    //  - " - transpose
    x1=kalman.x1;
    x2=kalman.x2;    // states
    R=kalman.R;        // measurement noise
    Q=kalman.Q;        // process noise
    P1=kalman.P1;	    // a priori estimate
    P2=kalman.P2;	    // a posteriori estimate
    var=kalman.var;	    // variance of measurement errors
    
    t0=kalman.t0;
    t1=kalman.t1;
    t2=kalman.t2;
  }
  
  return *this;
}

void PvKalman::setQ(const Matrix<float> &Q0)
{
  Q = Q0;

  // calculate K
  t0.mult(H,P1);
  t1.mult(t0,H_T);
  t1.add(R);
  t2.invFast(t1,tinv1,tinv2);
  t3.mult(H_T,t2);
  K.mult(P1,t3);

  // calculate a posteriori P (correction)
  t4.mult(K,H);
  t5.sub(I,t4);
  P2.mult(t5,P1);
  P1=P2;    

  // calculate next a priori P (prediction)
  t4.mult(A,P1);
  t5.mult(t4,A_T);
  P1.add(t5,Q);
}

void PvKalman::setR(const Matrix<float> &R0)
{
  R = R0;

  // calculate K
  t0.mult(H,P1);
  t1.mult(t0,H_T);
  t1.add(R);
  t2.invFast(t1,tinv1,tinv2);
  t3.mult(H_T,t2);
  K.mult(P1,t3);

  // calculate a posteriori P (correction)
  t4.mult(K,H);
  t5.sub(I,t4);
  P2.mult(t5,P1);
  P1=P2;    

  // calculate next a priori P (prediction)
  t4.mult(A,P1);
  t5.mult(t4,A_T);
  P1.add(t5,Q);
}


void PvKalman::setState(const Matrix<float> &S)
{
  x2=S;
}

} // namespace ait

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#pragma warning(disable:4786)

#include "legacy/media/ImageAcquireProperty.hpp"
#include "legacy/low_level/Settings.hpp"

using namespace std;

namespace ait
{

const char *ImageAcquireProperty::propertyNames[] =
{
  "Invalid Property Code",
  "Brightness",
  "Contrast",
  "Hue",
  "Saturation",
  "Sharpness",
  "Gamma",
  "Color Enable",
  "Whitebalance",
  "Backlight Compensation",
  "Gain",
  "Fps",
  "Source Bit Count",
  "Source Width",
  "Source Height",
  "Pan",
  "Tilt",
  "Roll",
  "Zoom",
  "Exposure",
  "Iris",
  "Focus",
  "Window Pos X",
  "Window Pos Y",
};

ImageAcquireProperty::ImageAcquireProperty()
{
  propertyCode = INVALID_CODE;
  rangeMin = 0;
  rangeMax = 0;
  rangeStep = 0;
  value = 0;
  defaultValue = 0;
  automatic = false;
  available = false;
  interactive = false;
}

ImageAcquireProperty::ImageAcquireProperty(ImageAcquireProperty& c)
{
  // A simple copy should do.
  *this = c;
}

void ImageAcquireProperty::getRange(float& min, float& max, float& step)
{
  min = rangeMin;
  max = rangeMax;
  step = rangeStep;
}

void ImageAcquireProperty::setRange(float min, float max, float step)
{
  rangeMin = min;
  rangeMax = max;
  rangeStep = step;
}

void ImageAcquireProperty::writeToSettings(ait::Settings &set)
{
  set.setInt(getName()+"/available [bool]",(int)available);
  if (!available) return;
  set.setDouble(getName()+"/rangeMin [float]",rangeMin);
  set.setDouble(getName()+"/rangeMax [float]",rangeMax);
  set.setDouble(getName()+"/rangeStep [float]",rangeStep);
  set.setDouble(getName()+"/value [float]",value);
  set.setDouble(getName()+"/defaultValue [float]",defaultValue);
  set.setInt(getName()+"/auto [bool]",(int)automatic);
  set.setInt(getName()+"/interactive [bool]",(int)interactive);
  set.setInt(getName()+"/propertyCode [int]",(int)propertyCode);
}

void ImageAcquireProperty::readFromSettings(ait::Settings &reg, Code p)
{
  PVASSERT(p >= 0 && p < getTotalProperties());

  propertyCode = p;
  //rangeMin = reg.getDouble(getName()+"/rangeMin [float]",rangeMin);
  //rangeMax = reg.getDouble(getName()+"/rangeMax [float]",rangeMax);
  //rangeStep = reg.getDouble(getName()+"/rangeStep [float]",rangeStep);
  value = reg.getDouble(getName()+"/value [float]",value);
  //defaultValue = reg.getDouble(getName()+"/defaultValue [float]",defaultValue);
  automatic = reg.getInt(getName()+"/auto [bool]",(int)automatic) == 1;
  //available = reg.getInt(getName()+"/available [bool]",(int)available) == 1;
  //interactive = reg.getInt(getName()+"/interactive [bool]",(int)interactive) == 1;

  setToDevice();
}

ImageAcquirePropertyBag::~ImageAcquirePropertyBag()
{
  saveSettings();
  props.clear();
  proto.reset();
}

void ImageAcquirePropertyBag::saveSettings()
{
  map<ImageAcquireProperty::Code, ImageAcquirePropertyPtr>::iterator i;
  for (i = props.begin(); i != props.end(); i++)
  {
    // The second condition checks if the registry key has been created before (for another camera maybe)
    if ((*i).second->getAvailable() || (settings.getInt((*i).second->getName()+"/available [bool]",(int)-1,false) != -1))
    {
      (*i).second->writeToSettings(settings);
    }
  }
}

void ImageAcquirePropertyBag::loadSettings()
{
  map<ImageAcquireProperty::Code, ImageAcquirePropertyPtr>::iterator i;
  for (i = props.begin(); i != props.end(); i++)
  {
    if ((*i).second->getAvailable())
    {
      (*i).second->readFromSettings(settings,(*i).first);
    }
  }
}

ImageAcquirePropertyPtr ImageAcquirePropertyBag::get(ImageAcquireProperty::Code p)
{
  map<ImageAcquireProperty::Code, ImageAcquirePropertyPtr>::iterator i;

  i = props.find(p);
  if (i == props.end())
  {
    ImageAcquirePropertyPtr newProp(proto->clone());
    // Change the code from INVALID_CODE
    newProp->setCode(p);
    // Get the values from the device
    newProp->getFromDevice();
    // Get the default values from the registry (if no values are created, it is going to save the values
    // that we just got).
    if (newProp->getAvailable())
    {
      newProp->readFromSettings(settings,p);
    }
    props[p] = newProp;
    return newProp;
  }
  return (*i).second;
}

} // namespace ait

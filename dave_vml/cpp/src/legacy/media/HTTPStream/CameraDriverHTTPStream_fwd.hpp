/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef CameraDriverHTTPStream_fwd_HPP
#define CameraDriverHTTPStream_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class CameraDriverHTTPStream;
class HTTPStreamDecoderException;
typedef boost::shared_ptr<CameraDriverHTTPStream> CameraDriverHTTPStreamPtr;

}; // namespace ait

#endif // CameraDriverHTTPStream_fwd_HPP


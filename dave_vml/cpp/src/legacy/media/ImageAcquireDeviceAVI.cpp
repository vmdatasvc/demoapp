/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#pragma warning(disable:4786)

#include "legacy/media/ImageAcquireDeviceAVI.hpp"
#include <legacy/low_level/XmlParser.hpp>
#include <assert.h>
#include <stack>
//#include <VideoReader.hpp>
#include <legacy/types/DateTime.hpp>

namespace ait
{

class AVIProperty : public ImageAcquireProperty
{
public:

  AVIProperty(ImageAcquireDeviceAVI *pDevice0) : pDevice(pDevice0) {};
  virtual ~AVIProperty() {}

  virtual ImageAcquireProperty *clone() { return new AVIProperty(*this); }

  void setToDevice() { pDevice->setAVIProperty(*this,true); };
  void getFromDevice() { pDevice->setAVIProperty(*this,false); };

protected:

  ImageAcquireDeviceAVI *pDevice;
};

ImageAcquireDeviceAVI::ImageAcquireDeviceAVI()
{
}


ImageAcquireDeviceAVI::~ImageAcquireDeviceAVI()
{
	unInitialize();
};


//
// Video list internal classes
//

/*
 *	Video clip is wrapper for the start and end frames in the XML file
 */
class ImageAcquireDeviceAVI::VideoClip
{
public:
  VideoClip() : mStartFrame(0),mEndFrame(0) {}
  long getStartFrame() const {return mStartFrame;}
  long getEndFrame() const {return mEndFrame;}

  void setStartFrame(long start) {mStartFrame = start;}
  void setEndFrame(long end) {mEndFrame = end;}

  //for testing
  void print()
  {
    std::cout << mStartFrame << " - " << mEndFrame << std::endl;
  }
  
protected:
  long mStartFrame;
  long mEndFrame;
};

/*
 *	VideoFile is a wrapper for the file objects in the XML file.
 *  These objects contain info on video clips
 */
class ImageAcquireDeviceAVI::VideoFile
{
public:
  VideoFile() : mStartTime(0) {}

  // Operations
  void setFilename(std::string filename) {mFilename = filename;}
  void addClip(VideoClipPtr p) {mClips.push_back(p);}

  // Accessors
  VideoClipPtr getClip(int index) const {return mClips[index];}
  int getClipCount() const {return mClips.size();}
  std::string getFilename() const {return Settings::replaceVars(mFilename);}
  void setStartTime(double t) { mStartTime = t; }
  double getStartTime() const { return mStartTime; }

  //for testing
  void print()
  {
    std::cout << "Filename: '" << mFilename << "'" << std::endl;
    VideoClips::iterator i;
    for (i = mClips.begin(); i != mClips.end(); i++)
      (*i)->print();
  }

protected:
  //vector containing points to video clips
  VideoClips mClips; 
  std::string mFilename;
  double mStartTime;
  
};

/*
 *	ConfigHandler handles the XML parsing.
 */
class ImageAcquireDeviceAVI::ConfigHandler : public Handler
{
public:
  ConfigHandler();
  virtual void start (const std::string& ns, const std::string& tag, const char **attr);
  virtual void cdata (const XML_Char *s, int len);  
  virtual void end (const std::string& ns, const std::string& tag);

  VideoFiles& getVideoFiles() { return mVideoFiles; }
 
protected:
  VideoFiles mVideoFiles;
  std::stack<std::string> mTagStack;

  VideoFilePtr mCurrentVideoFile;
  VideoClipPtr mCurrentVideoClip;
};

ImageAcquireDeviceAVI::ConfigHandler::ConfigHandler()
{
  //do nothing here, initializer lists have taken care of everything
}

void 
ImageAcquireDeviceAVI::ConfigHandler::start (const std::string& ns, const std::string& tag, const char **attr)
{
  if (tag == "video_file")
  {
    mCurrentVideoFile.reset(new VideoFile());
    mVideoFiles.push_back(mCurrentVideoFile);
  }

  if (tag == "segment")
  {
    mCurrentVideoClip.reset(new VideoClip());
    mCurrentVideoFile->addClip(mCurrentVideoClip);
  }

  mTagStack.push(tag);
}

void 
ImageAcquireDeviceAVI::ConfigHandler::cdata (const XML_Char *s, int len)
{
  std::string data(s,len);
  if (!mTagStack.empty())
  {
    std::string tag = mTagStack.top();

    if (tag == "file_name")
    {
      mCurrentVideoFile->setFilename(data);
    }
    else if (tag == "start_time")
    {
      DateTime dt = DateTime::fromString(data);
      mCurrentVideoFile->setStartTime(dt.toSeconds());
    }
    else if (tag == "start_frame")
    {
      mCurrentVideoClip->setStartFrame(atol(data.c_str()));
    }
    else if (tag == "end_frame")
    {
      mCurrentVideoClip->setEndFrame(atol(data.c_str()));
    }
  }
}

void 
ImageAcquireDeviceAVI::ConfigHandler::end (const  std::string& ns, const std::string& tag)
{
  mTagStack.pop();
}

bool ImageAcquireDeviceAVI::initialize(int numPrevBuffers, int numConsecutiveFrameGrabs)
{
	assert(numPrevBuffers >= 0);

	if (isInitialized)
		unInitialize();

	//IR-Flashing
	mNumConsecutiveFrameGrabs = numConsecutiveFrameGrabs;	

  lastPaintTime = PvUtil::time();
  paintDelay = 0; //delay paints in secs.

	//Initialize the display window.
	displayFrameFlag = false;

	//Initialize base parameters.
	numPreviousBuffers = numPrevBuffers;

	//Initialize the resolution.
	frameWidth = 0;
	frameHeight = 0;

  // Add default property bag
  pParams = new ImageAcquirePropertyBag(deviceInfo->getRegPath(), 
  ImageAcquirePropertyPtr(new AVIProperty(this)));

  Settings set(deviceInfo->getRegPath());
  mRunAtFrameRate = set.getBool("runAtFrameRate [bool]",true);
  mSkipFrameInterval = set.getInt("skipFrameInterval",1);

  std::string fileList = set.getString("videoSegments","-");

  if (fileList == "-")
  {
    VideoFilePtr pVideoFile(new VideoFile());
    pVideoFile->setFilename(deviceInfo->getFile1());
    mVideoFiles.push_back(pVideoFile);
  }
  else
  {
    loadVideoFiles(fileList);
  }

  mFps = pParams->get(ImageAcquireProperty::FPS)->getValuef();
	//IR-Flashing
	mFps/=mNumConsecutiveFrameGrabs;

  mTotalFrames = 0;
  int numCustomStartTime = 0;

  VideoFiles::iterator iVideo;

  for (iVideo = mVideoFiles.begin(); iVideo != mVideoFiles.end(); ++iVideo)
  {
    VideoFile& vf = **iVideo;
    if (vf.getStartTime() != 0)
    {
      numCustomStartTime++;
    }
  }

  if (numCustomStartTime > 0 && mVideoFiles.size() != numCustomStartTime)
  {
    PvUtil::exitError("Custom start time must be defined for all videos");
  }

  for (iVideo = mVideoFiles.begin(); iVideo != mVideoFiles.end();)
  {
    VideoFile& vf = **iVideo;
    mpReader.reset(VideoReader::create());
    try
    {
      mpReader->open(vf.getFilename());
    }
    catch (std::exception)
    {
      iVideo = mVideoFiles.erase(iVideo);
      mpReader.reset();
      continue;
    }

    Vector2i bufferSize = mpReader->getBufferSize();
    if (frameWidth == 0)
    {
      frameWidth = bufferSize(0);
      frameHeight = bufferSize(1);
    }
    else
    {
      if (frameWidth != bufferSize(0) ||
        frameHeight != bufferSize(1))
      {
        PvUtil::exitError("All videos in [%s] must have the same frame size",fileList.c_str());
      }
    }

    if (vf.getClipCount() == 0)
    {
      VideoClipPtr pClip(new VideoClip());
      vf.addClip(pClip);
    }

    int i;
    for (i = 0; i < vf.getClipCount(); i++)
    {
      VideoClip& clip = *vf.getClip(i);
      if (clip.getStartFrame() > clip.getEndFrame() && clip.getEndFrame() > 0)
      {
        PvUtil::exitError("Start frame is greater than end frame for a clip in file [%s]",fileList.c_str());
      }
      if (clip.getEndFrame() == 0 || clip.getEndFrame() > mpReader->getNumberOfFrames())
      {
        clip.setEndFrame(mpReader->getNumberOfFrames());
      }
      if (clip.getStartFrame() < clip.getEndFrame())
      {
        mTotalFrames += clip.getEndFrame()-clip.getStartFrame();
      }
    }

    ++iVideo;
  }

  if (mVideoFiles.size() == 0)
  {
    frameWidth = pParams->get(ImageAcquireProperty::SOURCEWIDTH)->getValuef();
    frameHeight = pParams->get(ImageAcquireProperty::SOURCEHEIGHT)->getValuef();
  }
  else
  {
    // Load the first video
    mpReader.reset(VideoReader::create());
    mpReader->open(mVideoFiles[0]->getFilename());
  }

  mCurrentVideoFile = 0;
  mCurrentVideoClip = 0;
  mCurrentFrameInClip = -1;

	//Initialize buffers.

  initBuffers(frameWidth,frameHeight,numPrevBuffers);
  for (int i = 0; i < mFrames.capacity(); i++)
  {
    mFrames[i]->pRGBImage->setInternalFormat(Image32::PV_BGRA);
  }

	frameCounter = 0;

	isInitialized = true;

	//Fill buffers.
  nextFrameTime=0;

  fillBuffers();

	return true;
};

void ImageAcquireDeviceAVI::loadVideoFiles(std::string fname)
{
  ConfigHandler config;
  XmlParser parser;
  
  parser.registerNamespaceHandler("", &config); 
  parser.parseFile(fname);

  mVideoFiles = config.getVideoFiles();
}

bool ImageAcquireDeviceAVI::unInitialize()
{
	isInitialized = false;

  mFrames.clear();
  if (pParams)
  {
    delete pParams;
    pParams = NULL;
  }

  mpReader.reset();
  mVideoFiles.clear();

	return true;
};

int ImageAcquireDeviceAVI::grabFrame()
{
  assert(isInitialized);
  
  bool isLastFrame = false;

  if (mVideoFiles.size() == 0)
  {
    return true;
  }

  //Grab into the last position.
  //Get avi here....

  VideoFilePtr pVf;// = mVideoFiles[mCurrentVideoFile];
  VideoClipPtr pVc;// = pVf->getClip(mCurrentVideoClip);

	for(int i=0; i<mNumConsecutiveFrameGrabs; i++)
	{
		pVf = mVideoFiles[mCurrentVideoFile];
		pVc = pVf->getClip(mCurrentVideoClip);

		if (mCurrentFrameInClip < 0)
		{
			mCurrentFrameInClip = pVc->getStartFrame();
		}
		else
		{
			//ensure that "mNumConsecutiveFrameGrabs" consecutive frames are loaded
			if(i==0)
				mCurrentFrameInClip += mSkipFrameInterval;
			else
				mCurrentFrameInClip++;
		}

		while (pVc->getEndFrame() != 0 && mCurrentFrameInClip >= pVc->getEndFrame())
		{
			// Move to the next clip.
			if (mCurrentVideoClip < pVf->getClipCount()-1)
			{
				mCurrentVideoClip++;
			}
			else
			{
				// The current file will change.
				if (mCurrentVideoFile < mVideoFiles.size()-1)
				{
					mCurrentVideoFile++;
				}
				else
				{
					// We are past the end, must loop to the first file.
					mCurrentVideoFile = 0;
				}
				mCurrentVideoClip = 0;

				pVf = mVideoFiles[mCurrentVideoFile];

				// Load new file.
				if (mVideoFiles.size() > 1)
				{
					mpReader.reset(VideoReader::create());
					mpReader->open(pVf->getFilename());
				}

			}

			// At this point the video clip has changed.
			pVc = pVf->getClip(mCurrentVideoClip);
			mCurrentFrameInClip = pVc->getStartFrame();
		}

		mpReader->setCurrentFrameNumber(mCurrentFrameInClip);
		//mpReader->readFrame(*(mFrames.front()->pRGBImage));
    MediaFrameInfoPtr pFrame = mFrames[mFrames.size()-(mNumConsecutiveFrameGrabs-i)];
		mpReader->readFrame(*(pFrame->pRGBImage));
    if (pVf->getStartTime() > 0)
    {
      double fps = mpReader->getFramesPerSecond();
      pFrame->beginTime = pVf->getStartTime() + ((double)mCurrentFrameInClip)/fps;
      pFrame->endTime = pFrame->beginTime + 1.0/fps;
    }
    mpReader->nextFrame();
		
		if (mCurrentVideoFile == mVideoFiles.size()-1 &&
			mCurrentVideoClip == pVf->getClipCount()-1 &&
      pVc->getEndFrame() > 0 &&
			mCurrentFrameInClip + mSkipFrameInterval >= pVc->getEndFrame())
		{
			isLastFrame = true;
		}
  }

	//Can advance here since frame is copied and the bytes were swapped.
	nextFrame(); //automatically advanced "mNumConsecutiveFrameGrabs" frames

	PVTIME now=PvUtil::time();

	if(now < nextFrameTime)
	{
		//PVMSG("Sleeping %g ms!\n",(0.033-(now-lastFrameTime))*1000.0);
		if (mRunAtFrameRate)
		{
			PvUtil::sleep(nextFrameTime-now);
		}
	}

	nextFrameTime += 1.0/mFps;

	// If we are too far, just reset the next frame time.
	if (now > nextFrameTime + 3.0/mFps)
	{
		nextFrameTime = now;
	}

  return isLastFrame;
};


bool ImageAcquireDeviceAVI::isFirstFrame(){

	return false;


};

void ImageAcquireDeviceAVI::gotoFirstFrame()
{
};

void ImageAcquireDeviceAVI::setAVIProperty(ImageAcquireProperty& param, bool set)
{
  long refType = -1;
  long controlType = -1;

  switch (param.getCode())
  {
  case ImageAcquireProperty::BRIGHTNESS:
  case ImageAcquireProperty::CONTRAST:
  case ImageAcquireProperty::HUE:
  case ImageAcquireProperty::SATURATION:
  case ImageAcquireProperty::GAIN:
  case ImageAcquireProperty::WHITEBALANCE:
  case ImageAcquireProperty::BACKLIGHTCOMPENSATION:
  case ImageAcquireProperty::SHARPNESS:
  case ImageAcquireProperty::GAMMA:
  case ImageAcquireProperty::COLORENABLE:
  case ImageAcquireProperty::PAN:
  case ImageAcquireProperty::TILT:
  case ImageAcquireProperty::ROLL:
  case ImageAcquireProperty::ZOOM:
  case ImageAcquireProperty::EXPOSURE:
  case ImageAcquireProperty::IRIS:
  case ImageAcquireProperty::FOCUS:
    break;
  case ImageAcquireProperty::SOURCEWIDTH:
  case ImageAcquireProperty::SOURCEHEIGHT:
    if (!set)
    {
      param.setAvailable(true);
      param.setAuto(false);
      param.setInteractive(false);
      if (param.getCode() == ImageAcquireProperty::SOURCEWIDTH)
      {
        param.setValuef(frameWidth,false);
        param.setDefault(frameWidth);
      }
      else
      {
        param.setValuef(frameHeight,false);
        param.setDefault(frameHeight);
      }
    }
    return;
  case ImageAcquireProperty::FPS:
    if (!set)
    {
      param.setAvailable(true);
      param.setAuto(false);
      param.setInteractive(false);
      param.setValuef(mFps,false);
      param.setDefault(30);
    }
    else
    {
      mFps = param.getValuef(false);
    }
    return;
  }

  param.setAvailable(false);
}

int
ImageAcquireDeviceAVI::getTotalNumberOfFrames()
{
  return mTotalFrames/mSkipFrameInterval;
}

} // namespace ait

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include <ImageAcquireDeviceDLL.hpp>

#include <windows.h>

#include <boost/lexical_cast.hpp>

namespace ait
{

ImageAcquireDeviceDLL::ImageAcquireDeviceDLL()
{

}

ImageAcquireDeviceDLL::~ImageAcquireDeviceDLL()
{
  unInitialize();
}


bool
ImageAcquireDeviceDLL::initialize(int numPrevBuffers, int numConsecutiveFrameGrabs)
{
  //register necessary DLL functions
  registerDLLFunctions();

  assert(numPrevBuffers >= 0);

  if (isInitialized)
  {
    unInitialize();
  }

  lastPaintTime = PvUtil::time();
  paintDelay = 0; // paint delay in secs

  //Initialize the display window
  displayFrameFlag = false;

  //Initialize base paramters
  numPreviousBuffers = numPrevBuffers;

  //initialize camera
  mCameraId = deviceInfo->getdeviceIndex() + 1;
  mpCameraDriver = CamDrvCreate(); 

  //Now, grab all requested settings
  Settings s(deviceInfo->getRegPath());

  std::string sourceWidth = "320"; //s.getString("Source Width/value [float]", "ERROR");
  std::string sourceHeight = "240"; //s.getString("Source Height/value [float]", "ERROR");
  std::string fps = s.getString("Fps/value [float]", "ERROR");

  mFps = boost::lexical_cast<float>(fps);

  //Initialize the resolution
  frameWidth = boost::lexical_cast<int>(sourceWidth); 
  frameHeight = boost::lexical_cast<int>(sourceHeight); 

  pParams = new ImageAcquirePropertyBag(deviceInfo->getRegPath());
  pParams->get(ImageAcquireProperty::SOURCEWIDTH)->setValuef(frameWidth);
  pParams->get(ImageAcquireProperty::SOURCEHEIGHT)->setValuef(frameHeight);
  pParams->get(ImageAcquireProperty::FPS)->setValuef(mFps);

  //After the desired settings are determined, initialize driver
  CamDrvInit(mpCameraDriver, mCameraId, this);

  //Attempt to change the DLL settings with our desired settings
  //TODO: write a structed way to attempt to transfer ALL of the settings
  //TODO: check return values, and exit on failure

  CamDrvSetParam(mpCameraDriver, mCameraId, "SourceWidth", sourceWidth.c_str());
  CamDrvSetParam(mpCameraDriver, mCameraId, "SourceHeight", sourceHeight.c_str());
  CamDrvSetParam(mpCameraDriver, mCameraId, "FramesPerSecond", fps.c_str());    

  frameCounter = 0;

  //init circular buffers
  initBuffers(frameWidth,frameHeight,numPrevBuffers);

  isInitialized = true;    

  return true;
}

bool
ImageAcquireDeviceDLL::unInitialize()
{
  
  if (mhLib)
  {
    //Destroy Camera reference
    CamDrvDispose(mpCameraDriver);


    //Release DLL
    FreeLibrary((HINSTANCE)mhLib);
    mhLib = NULL;
  }
  return true;
}

int
ImageAcquireDeviceDLL::grabFrame()
{
  assert(isInitialized);

  boost::mutex::scoped_lock lk(mFrameReadyMutex);

  long delay = (long)(2000.0/mFps);

  //wait for frame to become available
  while (mpNextFrame.get())
  {
    if (!mIsFrameReady.timed_wait(lk,delay))
    {
      // The timeout expired. We'll send a blue frame. If the callback has
      // not finished, it will realize that mpNextFrame is no longer available
      // and just drop it.
      if (mpNextFrame.get())
      {
        mpNextFrame->pRGBImage->setAll(PV_RGB(0,0,255));
        mpNextFrame.reset();
      }
      else
      {
        //PVMSG("Signaled!\n");
      }
    }
  }

  //rotate circular buffer
  nextFrame();

  // Prepare frame for next iteration so it can be filled asynchronously.
  // The next frame will be written to the back of the buffer
  {
    boost::mutex::scoped_lock lk(mFrameReadyMutex);
    mpNextFrame = mFrames.back();
  }

  return 0;
}

int
ImageAcquireDeviceDLL::getTotalNumberOfFrames()
{
  //TODO: return type depends on DLL type
  return 0;
}

void
ImageAcquireDeviceDLL::registerDLLFunctions()
{
  //dynamically load the DLL functions from the driver, quit on failure

  //TEST CODE HERE
  mhLib = LoadLibrary("E:/ai/build/msvc70/lib/CamDrvGeoVision/debug/CamDrvGeoVision.dll");
  if (mhLib == NULL)
  {
    PvUtil::exitError("Failed to load camera driver DLL");
  }

  CamDrvCreate = NULL;
  CamDrvDispose = NULL;
  CamDrvGetParam = NULL;
  CamDrvSetParam = NULL;
  CamDrvInit = NULL;

  CamDrvCreate = (CamDrvCreateType)GetProcAddress((HMODULE)mhLib, "CamDrvCreate");  
  CamDrvDispose = (CamDrvDisposeType)GetProcAddress((HMODULE)mhLib, "CamDrvDispose");
  CamDrvGetParam = (CamDrvGetParamType)GetProcAddress((HMODULE)mhLib, "CamDrvGetParam");
  CamDrvSetParam = (CamDrvSetParamType)GetProcAddress((HMODULE)mhLib, "CamDrvSetParam");
  CamDrvInit = (CamDrvInitType)GetProcAddress((HMODULE)mhLib, "CamDrvInit");


  
  if (CamDrvCreate == NULL || 
      CamDrvDispose == NULL ||
      CamDrvGetParam == NULL ||
      CamDrvSetParam == NULL ||
      CamDrvInit == NULL)
  {
    PvUtil::exitError("Failed to import camera driver DLL functions");
  }


}

} // namespace ait
/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include <windows.h>
#include <windowsx.h>
#include <memory.h>
#include <mmsystem.h>
#include <vfw.h>
#include "VideoWriterVFW.hpp"
#include <Settings.hpp>


static void aviPrintError(unsigned int error)
{
  switch(error)
  {
  case AVIERR_UNSUPPORTED: PVMSG("AVIERR_UNSUPPORTED");break;
  case AVIERR_BADFORMAT: PVMSG("AVIERR_BADFORMAT");break;
  case AVIERR_MEMORY: PVMSG("AVIERR_MEMORY");break;
  case AVIERR_INTERNAL: PVMSG("AVIERR_INTERNAL");break;
  case AVIERR_BADPARAM: PVMSG("AVIERR_BADPARAM");break;
  case AVIERR_BADSIZE: PVMSG("AVIERR_BADSIZE");break;
  case AVIERR_BADHANDLE: PVMSG("AVIERR_BADHANDLE");break;
  case AVIERR_FILEREAD: PVMSG("AVIERR_FILEREAD");break;
  case AVIERR_FILEWRITE: PVMSG("AVIERR_FILEWRITE");break;
  case AVIERR_FILEOPEN: PVMSG("AVIERR_FILEOPEN");break;
  case AVIERR_COMPRESSOR: PVMSG("AVIERR_COMPRESSOR");break;
  case AVIERR_NOCOMPRESSOR: PVMSG("AVIERR_NOCOMPRESSOR");break;
  case AVIERR_READONLY: PVMSG("AVIERR_READONLY");break;
  case AVIERR_NODATA: PVMSG("AVIERR_NODATA");break;
  case AVIERR_BUFFERTOOSMALL: PVMSG("AVIERR_BUFFERTOOSMALL");break;
  case AVIERR_CANTCOMPRESS: PVMSG("AVIERR_CANTCOMPRESS");break;
  case AVIERR_USERABORT: PVMSG("AVIERR_USERABORT");break;
  case AVIERR_ERROR: PVMSG("AVIERR_ERROR");break;
  default : PVMSG("AVIERROR: %x",error);break;
  }
}

/*
** MakeDib(hbitmap)
**
** Take the given bitmap and transform it into a DIB with parameters:
**
** BitsPerPixel:    8
** Colors:          palette
**
*/
static HANDLE  MakeDib( HBITMAP hbitmap, UINT bits )
{
	HANDLE              hdib ;
	HDC                 hdc ;
	BITMAP              bitmap ;
	UINT                wLineLen ;
	DWORD               dwSize ;
	DWORD               wColSize ;
	LPBITMAPINFOHEADER  lpbi ;
	LPBYTE              lpBits ;
	
	GetObject(hbitmap,sizeof(BITMAP),&bitmap) ;

	//
	// DWORD align the width of the DIB
	// Figure out the size of the colour table
	// Calculate the size of the DIB
	//
	wLineLen = (bitmap.bmWidth*bits+31)/32 * 4;
	wColSize = sizeof(RGBQUAD)*((bits <= 8) ? 1<<bits : 0);
	dwSize = sizeof(BITMAPINFOHEADER) + wColSize +
		(DWORD)(UINT)wLineLen*(DWORD)(UINT)bitmap.bmHeight;

	//
	// Allocate room for a DIB and set the LPBI fields
	//
	hdib = GlobalAlloc(GHND,dwSize);
	if (!hdib)
		return hdib ;

	lpbi = (LPBITMAPINFOHEADER)GlobalLock(hdib) ;

	lpbi->biSize = sizeof(BITMAPINFOHEADER) ;
	lpbi->biWidth = bitmap.bmWidth ;
	lpbi->biHeight = bitmap.bmHeight ;
	lpbi->biPlanes = 1 ;
	lpbi->biBitCount = (WORD) bits ;
	lpbi->biCompression = BI_RGB ;
	lpbi->biSizeImage = dwSize - sizeof(BITMAPINFOHEADER) - wColSize ;
	lpbi->biXPelsPerMeter = 0 ;
	lpbi->biYPelsPerMeter = 0 ;
	lpbi->biClrUsed = (bits <= 8) ? 1<<bits : 0;
	lpbi->biClrImportant = 0 ;

	//
	// Get the bits from the bitmap and stuff them after the LPBI
	//
	lpBits = (LPBYTE)(lpbi+1)+wColSize ;

	hdc = CreateCompatibleDC(NULL) ;

	GetDIBits(hdc,hbitmap,0,bitmap.bmHeight,lpBits,(LPBITMAPINFO)lpbi, DIB_RGB_COLORS);

	// Fix this if GetDIBits messed it up....
	lpbi->biClrUsed = (bits <= 8) ? 1<<bits : 0;

	DeleteDC(hdc) ;
	GlobalUnlock(hdib);

	return hdib ;
}

namespace ait 
{

VideoWriterVFW::VideoWriterVFW()
{
	AVIFileInit();

	pfile = NULL;
	ps = NULL;
  psCompressed = NULL;
  psText = NULL;

  frameCount = 0;
  firstFrame = true;
}

VideoWriterVFW::~VideoWriterVFW()
{
  close();

  AVIFileExit();
}

void VideoWriterVFW::init(int w, int h, const char *name)
{
  HINSTANCE hInstance = (HINSTANCE)GetModuleHandle(NULL);
 
  std::string settingsFile = "c:\\ai\\common\\data\\win32avi.dat";//Settings::replaceVars("<CommonFilesPath>\\data\\win32avi.dat");

  HRESULT hr;

  FILE *fp;
  
	hr = AVIFileOpen(&pfile,		    // returned file pointer
		       name,		            // file name
		       OF_WRITE | OF_CREATE,	    // mode to open file with
		       NULL);			    // use handler determined
						    // from file extension....
	if (hr != AVIERR_OK)
  {
    PvUtil::exitError("VideoWriterAVI: Error in AVIFileOpen('%s').",name);
		goto error;
  }

	// Fill in the header for the video stream....

	AVISTREAMINFO strhdr;
	_fmemset(&strhdr, 0, sizeof(strhdr));
	strhdr.fccType                = streamtypeVIDEO;// stream type
	strhdr.fccHandler             = 0;
	strhdr.dwScale                = 1000;
	strhdr.dwRate                 = (DWORD)(getFramesPerSecond()*1000);
	strhdr.dwSuggestedBufferSize  = w*h*4;
	SetRect(&strhdr.rcFrame, 0, 0,		    // rectangle for stream
        w,
	      h);

	// And create the stream;
	hr = AVIFileCreateStream(pfile,		    // file pointer
			         &ps,		    // returned stream pointer
			         &strhdr);	    // stream header
	if (hr != AVIERR_OK) {
    PvUtil::exitError("VideoWriterAVI: Error in AVIFileCreateStream.\n");
		goto error;
	}

 	AVICOMPRESSOPTIONS opts;
	_fmemset(&opts, 0, sizeof(opts));

  LPAVICOMPRESSOPTIONS aopts[1];
  aopts[0]=&opts;

  fp = fopen(settingsFile.c_str(),"rb");

  if (!fp)
  {
    AVISaveOptions(/*hwnd*/NULL, ICMF_CHOOSE_KEYFRAME|ICMF_CHOOSE_DATARATE, 1, &ps, aopts);
    
    FILE *fp = fopen(settingsFile.c_str(),"wb");

    PVASSERT(fp);

    fwrite(&opts,sizeof(opts),1,fp);

    if (opts.lpFormat)
    {
      fwrite(opts.lpFormat,opts.cbFormat,1,fp);
    }

    if (opts.lpParms)
    {
      fwrite(opts.lpParms,opts.cbParms,1,fp);
    }

    fclose(fp);
  }
  else
  {
    fread(&opts,sizeof(opts),1,fp);
    
    if (opts.cbFormat)
    {
      opts.lpFormat = new char [opts.cbFormat];
      fread(opts.lpFormat,opts.cbFormat,1,fp);
    }

    if (opts.cbParms)
    {
      opts.lpParms = new char [opts.cbParms];;
      fread(opts.lpParms,opts.cbParms,1,fp);
    }

    fclose(fp);
  }

  // Open a temp. stream of compressed data
	hr = AVIMakeCompressedStream(&psCompressed, ps, &opts, NULL);
	if (hr != AVIERR_OK) 
  {
    switch(hr)
    {
    case AVIERR_NOCOMPRESSOR: PvUtil::exitError("A suitable compressor cannot be found.");break;
    case AVIERR_MEMORY: PvUtil::exitError("There is not enough memory to complete the operation.");break;
    case AVIERR_UNSUPPORTED: PvUtil::exitError("Compression is not supported for this type of data. This error might be returned if you try to compress data that is not audio or video.");break;
    }
		goto error;
	}

  return;

error:

  close();

  PvUtil::exitError("VideoWriteAVI: Error!");
}

void 
VideoWriterVFW::open(const std::string& fname)
{
  mFileName = Settings::replaceVars(fname);
  // Try to create the file so there is a message if there is a problem.
  FILE *fp = fopen(fname.c_str(),"wb");
  if (!fp || fclose(fp) != 0)
  {
    throw std::exception(("Unable to create: " + fname).c_str());
  }

  if (!hasValidCodec())
  {
    loadCodecSettings("<CommonFilesPath>/data/codecs/default.codec",true);
  }
}

void 
VideoWriterVFW::writeFrame(const Image32& img)
{
  if (!pfile)
  {
    init(img.width(),img.height(),mFileName.c_str());
  }

  int i;

	LPBITMAPINFOHEADER alpbi[1];
  HDC         hdc ;
	HDC         hdcMem ;

	HRESULT hr;
  unsigned int w=img.width(),h=img.height();
  unsigned char *data;
  unsigned char *imgData=(unsigned char *)img.pointer();
  BITMAPINFO bmi;

  memset(&bmi,0,sizeof(bmi));

  bmi.bmiHeader.biSize = sizeof(bmi.bmiHeader);
  bmi.bmiHeader.biWidth = w;
  bmi.bmiHeader.biHeight = h;
  bmi.bmiHeader.biPlanes = 1;
  bmi.bmiHeader.biBitCount = 32;
  bmi.bmiHeader.biCompression = BI_RGB;

	hdc = GetDC(NULL) ;

  HBITMAP hbm = CreateDIBSection(
    hdc,                     // handle to DC
    &bmi,                    // bitmap data
    DIB_RGB_COLORS,          // data type indicator
    (void **)&data,          // bit values
    NULL,                    // handle to file mapping object
    0                        // offset to bitmap bit values
  );

  hdcMem = CreateCompatibleDC(NULL);

  SelectBitmap(hdcMem, hbm);

  GdiFlush();

  int x,y;

  i=0;
  //for(y=0;y<h;y++)
  for(y=h-1;y>=0;y--)
  {
    for(x=0;x<w;x++)
    {
      data[i++]=imgData[(y*w+x)*4];
      data[i++]=imgData[(y*w+x)*4+1];
      data[i++]=imgData[(y*w+x)*4+2];
      data[i++]=imgData[(y*w+x)*4+3];
    }
  }

  HANDLE hdib=MakeDib(hbm, 32);

  alpbi[0] = (LPBITMAPINFOHEADER)GlobalLock(hdib);

  //alpbi[0]=(LPBITMAPINFOHEADER)GlobalLock(hbm);


  if(firstFrame)
  {
    firstFrame=false;
	hr = AVIStreamSetFormat(psCompressed, 0,
			       &bmi.bmiHeader,	    // stream format
			       bmi.bmiHeader.biSize +   // format size
			       bmi.bmiHeader.biClrUsed * sizeof(RGBQUAD));
	if (hr != AVIERR_OK) {
	goto error;
	}
  }

	hr = AVIStreamWrite(psCompressed,	// stream pointer
			(frameCount++),				// time of this frame
			1,				// number to write
			(LPBYTE) alpbi[0] +		// pointer to data
				alpbi[0]->biSize +
				alpbi[0]->biClrUsed * sizeof(RGBQUAD),
				alpbi[0]->biSizeImage,	// size of this frame
			AVIIF_KEYFRAME,			 // flags....
			NULL,
			NULL);
		if (hr != AVIERR_OK)
    {
      aviPrintError(hr);
      PvUtil::exitError("\nVideoWriterAVI: saveFrame failed on AVIStreamWrite().\n");
    }

  GlobalFreePtr(alpbi[0]);
  //GlobalUnlock(hdib);
  //DeleteObject(hdib);
	DeleteObject(hbm) ;
	DeleteObject(hdcMem) ;
	ReleaseDC(NULL,hdc) ;

  return;

error:

  PVMSG("AVI Error: ");
  aviPrintError(hr);
  PVMSG("\n");

  return;
}

void 
VideoWriterVFW::skipFrame()
{
  frameCount++;
}

void 
VideoWriterVFW::close()
{
	if (ps)
  {
		AVIStreamClose(ps);
    ps = NULL;
  }

	if (psCompressed)
  {
		AVIStreamClose(psCompressed);
    psCompressed = NULL;
  }

	if (psText)
  {
		AVIStreamClose(psText);
    psText = NULL;
  }

	if (pfile)
  {
		AVIFileClose(pfile);
    pfile = NULL;
  }
}

}; // namespace ait


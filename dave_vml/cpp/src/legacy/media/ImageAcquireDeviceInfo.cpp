/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#pragma warning(disable:4786)

#include "ImageAcquireDeviceInfo.hpp"
#include <assert.h>
#include "PvUtil.hpp"

namespace ait
{

/*********************************************************************
 * CONSTRUCTOR
 *********************************************************************/
ImageAcquireDeviceInfo::ImageAcquireDeviceInfo(std::string deviceID, std::string regPath_i)
{
  assert(!regPath_i.empty());
  assert(deviceID.size()>0);
  
  deviceDriverID = deviceID;
  
  //Regpath for this device.
  regPath = regPath_i;
  
  isInitialized = false;
  
  initialize(false);
  
};

/*********************************************************************
 * DESTRUCTOR
 *********************************************************************/
ImageAcquireDeviceInfo::~ImageAcquireDeviceInfo(void)
{
  unInitialize(false);
};

//Initialize this class
void ImageAcquireDeviceInfo::initialize(bool cascade){
  
  if (isInitialized)
    unInitialize(false);
  
  //Init vars.
  deviceName    = "";
  deviceVersion = "";
  file1         = "";
  
  //Setup and Read the registry.
  registry = new ait::Settings(regPath);
  
  deviceName    = registry->getString("deviceName", "NONE");
  deviceVersion = registry->getString("deviceVersion", "NONE");

  // Initialize the device index with the device name.
  std::string path = registry->getPath();
  deviceIndex   = registry->getInt("deviceIndex", atoi(path.substr(path.length()-1).c_str()));

  file1 = registry->getString("file1", "<CommonFilesPath>/avi/seq_001.avi");
  
  Vector2f windowPosf;
  windowPosf    = registry->getCoord2f("windowPos",Vector2f(0,0));
  windowPos.set((int)windowPosf(0),(int)windowPosf(1));
  
  Vector2f windowSizef;
  windowSizef    = registry->getCoord2f("windowSize",Vector2f(0,0));
  windowSize.set((int)windowSizef(0),(int)windowSizef(1));

  mWindowHasBorder = registry->getBool("windowHasBorder",true);

#ifdef WIN32
  mHwndParent = (ait::Uint32)registry->getInt("parentHwnd",0);
#endif

  isInitialized = true;
  
};

void ImageAcquireDeviceInfo::saveSettings()
{
  if (registry)
  {
    registry->setCoord2f("windowPos",Vector2f(windowPos(0),windowPos(1)));
    //registry->setCoord2f("windowSize",Vector2f(windowSize(0),windowSize(1)));
  }
}

//Un - Init this class.
void ImageAcquireDeviceInfo::unInitialize(bool cascade)
{
  //Delete registry.
  if (registry != NULL){
    delete registry;
    registry = NULL;
  }
  
  isInitialized = false; 
};

const std::string& ImageAcquireDeviceInfo::getdeviceDriverID(){
  
  assert(isInitialized);
  
  return deviceDriverID;
  
};

const std::string& ImageAcquireDeviceInfo::getdeviceName(){
  
  assert(isInitialized);
  
  return deviceName;
  
};

const std::string& ImageAcquireDeviceInfo::getdeviceVersion(){
  
  assert(isInitialized);
  
  return deviceVersion;

};

const std::string& ImageAcquireDeviceInfo::getFile1(){
  
  assert(isInitialized);

  return file1;

};

} // namespace ait

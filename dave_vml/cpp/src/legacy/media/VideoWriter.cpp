/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "VideoWriter.hpp"
#include "directshow/VideoWriterDS.hpp"
//#include "VideoWriterFfmpeg.hpp"

namespace ait 
{

VideoWriter::VideoWriter()
: mFramesPerSecond(15)
{
}

VideoWriterPtr 
VideoWriter::create()
{
#ifdef WIN32
  return VideoWriterPtr(new VideoWriterDS());
  //return VideoWriterPtr(new VideoWriterFfmpeg());
#endif
}

VideoWriter::~VideoWriter()
{
}

//
// OPERATIONS
//

//
// ACCESS
//

//
// INQUIRY
//

}; // namespace ait


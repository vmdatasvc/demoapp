/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef CameraDriverVMSense_fwd_HPP
#define CameraDriverVMSense_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class CameraDriverVMSense;
class VMSenseDecoderException;
typedef boost::shared_ptr<CameraDriverVMSense> CameraDriverVMSensePtr;

}; // namespace ait

#endif // CameraDriverVMSense_fwd_HPP


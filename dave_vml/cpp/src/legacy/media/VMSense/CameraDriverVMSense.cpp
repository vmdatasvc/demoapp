/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "CameraDriverVMSense.hpp"

#include <boost/lexical_cast.hpp>
#include <HttpSimpleClient.hpp>

extern "C"
{
#undef FAR
#include <jpeglib.h>
#include <jinclude.h>
#include <jerror.h>
}

typedef struct {
  struct jpeg_source_mgr pub;	/* public fields */
  unsigned char* p2;		/* Next Pointer */
  int p2size;
} my_source_mgr;

typedef my_source_mgr * my_src_ptr;

/*
 * Initialize source --- called by jpeg_read_header
 * before any data is actually read.
 */
METHODDEF(void)
mem_init_source (j_decompress_ptr cinfo)
{
  // Nothing required.
}


METHODDEF(boolean)
mem_fill_input_buffer (j_decompress_ptr cinfo)
{
  my_src_ptr src = (my_src_ptr)cinfo->src;

  src->pub.next_input_byte = src->p2;
  src->pub.bytes_in_buffer = src->p2size;

  return TRUE;
}

METHODDEF(void)
mem_skip_input_data (j_decompress_ptr cinfo, long num_bytes)
{
  my_src_ptr src = (my_src_ptr) cinfo->src;

  /* Just a dumb implementation for now.  Could use fseek() except
   * it doesn't work on pipes.  Not clear that being smart is worth
   * any trouble anyway --- large skips are infrequent.
   */
  if (num_bytes > 0) {
    while (num_bytes > (long) src->pub.bytes_in_buffer) {
      num_bytes -= (long) src->pub.bytes_in_buffer;
      (void) mem_fill_input_buffer(cinfo);
      /* note we assume that fill_input_buffer will never return FALSE,
       * so suspension need not be handled.
       */
    }
    src->pub.next_input_byte += (size_t) num_bytes;
    src->pub.bytes_in_buffer -= (size_t) num_bytes;
  }
}

METHODDEF(void)
mem_term_source (j_decompress_ptr cinfo)
{
  /* no work necessary here */
}

METHODDEF(void)
mem_error_exit (j_common_ptr cinfo)
{
  char buffer[JMSG_LENGTH_MAX];
  /* Create the message */
  (*cinfo->err->format_message) (cinfo, buffer);

  /* Let the memory manager delete any temp files before we die */
  jpeg_destroy(cinfo);

  throw ait::VMSenseDecoderException(buffer);
}


namespace ait 
{

CameraDriverVMSense::CameraDriverVMSense(const std::string& cameraProtocol) : 
  mpEvents(0),
  mpCaptureThread(0)
{
  mHeaderBufferSize = 1024;
  mpHeaderBuffer = new unsigned char[mHeaderBufferSize];
  mHeaderDataSize = 0;
  mpFrameBuffer = 0;
  mpNextByte = 0;
  mFrameBufferSize = 0;
  mFrameDataSize = 0;
  mExitThread = false;
}

CameraDriverVMSense::~CameraDriverVMSense()
{
  finish();
  delete [] mpHeaderBuffer;
  if (mpFrameBuffer) delete [] mpFrameBuffer;
}

/**
   * Set the FPS. Note that some drivers might modify the
   * requested FPS because of device limitations.
   */
double 
CameraDriverVMSense::getFramesPerSecond()
{
return boost::lexical_cast<double>(getParam("Fps/value [float]"));
  //return paramSet.getDouble("Fps/value [float]",15);
}

void
CameraDriverVMSense::init(int numCameras)
{
  initConnection();

  mTimeBetweenFrames = 1.0/getFramesPerSecond();

  if (mpCaptureThread != 0)
  {
    throw std::exception("Can't be initialized twice");
  }

  mpThreadHolder = new ThreadHolder(*this);
  mpCaptureThread = new boost::thread(*mpThreadHolder);
}

void
CameraDriverVMSense::finish()
{
  if (mpCaptureThread)
  {
    mExitThread = true;
    mpCaptureThread->join();
    delete mpThreadHolder;
    delete mpCaptureThread;
    mpCaptureThread = 0;
  }
}

void 
CameraDriverVMSense::initConnection()
{
  mpHttpClient.reset(new HttpSimpleClient());

  std::string url = "http://";
  url += getParam("IpAddress") + ":8080/settings.cgi";

  url += "?res=" + getParam("SourceWidth") + "x" + getParam("SourceHeight") +
    "&fps=" + getParam("FramesPerSecond")+ "&endit=1";

  mpHttpClient->setUrl(url);
  mpHttpClient->sendRequest();

  Sleep(1000);
  url = "http://";
  url += getParam("IpAddress") + ":8080/?action=stream&ignored.mjpg";

  mpHttpClient->setUrl(url);
  
  mpHttpClient->sendRequest();


}

//
// OPERATIONS
//

void 
CameraDriverVMSense::threadLoop()
{
  Image32 img;

  try
  {
    while (!mExitThread)
    {
      getFrame(img);
      mpEvents->onVideoFrame(0,mFrameTime,mFrameTime+mTimeBetweenFrames,
        (unsigned char *)img.pointer(),
        4*img.size());
    }
    
    mpHttpClient.reset();
  }
  catch (std::exception ex)
  {
    PVMSG("VMSense Decoder Exception: %s\n",ex.what());
    // Don't crash. It will just cause the main thread to time-out and
    // reset this driver.
  }
}


void 
CameraDriverVMSense::decodeFrame(unsigned char *pFrameData, int dataSize, 
  unsigned char *pFrameData2, int data2Size,
  Image32& img)
{
  struct jpeg_decompress_struct cinfo;
	struct jpeg_error_mgr jerr;

  unsigned char *data;

  cinfo.err = jpeg_std_error(&jerr);
  cinfo.err->error_exit = mem_error_exit;
	jpeg_create_decompress(&cinfo);

  my_source_mgr src;

  src.pub.init_source = mem_init_source;
  src.pub.fill_input_buffer = mem_fill_input_buffer;
  src.pub.skip_input_data = mem_skip_input_data;
  src.pub.resync_to_restart = jpeg_resync_to_restart; /* use default method */
  src.pub.term_source = mem_term_source;
  src.pub.bytes_in_buffer = dataSize;
  src.pub.next_input_byte = pFrameData;
  src.p2 = pFrameData2;
  src.p2size = data2Size;

  cinfo.src = (jpeg_source_mgr *)&src;
  
  jpeg_read_header(&cinfo, TRUE);

  // This can degrade quality but speeds up about 30% the performance.  
  cinfo.do_fancy_upsampling = false;

  jpeg_start_decompress(&cinfo);

	int w = cinfo.output_width;
	int h = cinfo.output_height;

  img.resize(w,h);

  if(cinfo.output_components!=4)
  {
    throw VMSenseDecoderException("The input must be a color JPEG");
  }

  data=(unsigned char *)img.pointer();

  JSAMPLE **pp = (JSAMPLE **)img.pointerpointer();

  JSAMPLE **p0 = pp;

  int rowsLeft = img.height();

  while (rowsLeft > 0)
  {
    int rowsParsed = jpeg_read_scanlines(&cinfo, p0, rowsLeft);
    rowsLeft -= rowsParsed;
    p0 += rowsParsed;
  }

  jpeg_finish_decompress(&cinfo);

  jpeg_destroy_decompress(&cinfo);

}

int 
CameraDriverVMSense::getLine(char *lineBuffer, int maxLength)
{
  int i = 0;
  unsigned char lastChar = 0;
  for (i = 0; i < maxLength; i++)
  {
    unsigned char ch = getChar();
    lineBuffer[i] = ch;
    if (ch == 0x0A && lastChar == 0x0D)
    {
      lineBuffer[i-1] = 0;
      return i-1;
    }
    lastChar = ch;
  }
  return maxLength;
}

void 
CameraDriverVMSense::skipToMarker(const char *marker, int markerLength)
{
  int matchCount = 0;
  while (matchCount < markerLength)
  {
    unsigned char ch = getChar();
    if (ch == marker[matchCount])
    {
      matchCount++;
    }
    else
    {
      matchCount = 0;
    }
  }
}

void 
CameraDriverVMSense::readFrameBuffer(int dataSize)
{
  // We need to read whatever is left from the image that is not
  // in the header buffer.
  int bytesToRead = dataSize - (mHeaderDataSize - (mpNextByte - mpHeaderBuffer));
  if (bytesToRead < 0)
  {
    mFrameDataSize = 0;
  }
  else
  {
    mFrameDataSize = bytesToRead;
    if (mFrameBufferSize < mFrameDataSize)
    {
      // We need to allocate a bigger buffer
      if (mpFrameBuffer) delete [] mpFrameBuffer;
      mpFrameBuffer = new unsigned char[mFrameDataSize];
    }
    // Now let's read our data
    mpHttpClient->readData(mpFrameBuffer,mFrameDataSize);
  }
}

void 
CameraDriverVMSense::getFrame(Image32& img)
{
  // Try to read header

  bool gotFrame = false;
  
  char line[50];

    char marker[] = {'-','-','b','o','u','n','d','a','r','y','d','o','n','o','t','c','r','o','s','s',0x0D,0x0A}; 
  do
  {
    skipToMarker(marker,sizeof(marker));

    mFrameTime = PvUtil::time();

    int frameDataSize = 0;
    bool skipToNextMarker = false;

    while (!skipToNextMarker)
    {
      int len = getLine(line,sizeof(line));
      if (len >= 50)
      {
        skipToNextMarker = true;
        break;
      }
      if (len == 0)
      {
        // Ready to read the data!
        if (frameDataSize == 0)
        {
          // Didn't get data size.
          skipToNextMarker = true;
          break;
        }
        readFrameBuffer(frameDataSize);
        unsigned char * pStartImageData = mpNextByte;
        long imageDataFromHeaderSize = frameDataSize-mFrameDataSize;
        mpNextByte += imageDataFromHeaderSize;
        try
        {
          decodeFrame(pStartImageData,imageDataFromHeaderSize,
            mpFrameBuffer,mFrameDataSize,img);
        }
        catch (...)
        {
          // Error decoding, try next frame.
          skipToNextMarker = true;
          break;
        }
        gotFrame = true;
        skipToNextMarker = true;
        break;
      }
      if (len >= 16 && strncmp(line,"Content-Length: ",16) == 0)
      {
        frameDataSize = atoi(line+16);
        if (frameDataSize > 4*1024*1024)
        {
          // Don't read frames that are 4MB or larger.
          frameDataSize = 0;
          skipToNextMarker = true;
          break;
        }
      }
    }
  } while (!gotFrame);

}

//
// ACCESS
//

//
// INQUIRY
//

}; // namespace ait


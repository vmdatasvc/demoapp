/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "CameraDriver.hpp"
#include <HTTPStream/CameraDriverHTTPStream.hpp>
#include <RTSPStream/CameraDriverRTSPStream.hpp>
#include <boost/lexical_cast.hpp>
#include <File.hpp>
#include <strutil.hpp>
#include <Base64.hpp>
#include <String.hpp>

namespace ait 
{

CameraDriver::CameraDriver(const std::string& appPath){
}

CameraDriver::~CameraDriver()
{
}

CameraDriver *
CameraDriver::create(const std::string& appPath)
{
Settings locPSet(appPath);
std::string cameraTransport=locPSet.getString("Camera.Transport","");
if(strstr(cameraTransport.c_str(),"rtsp"))
	return new CameraDriverRTSPStream(locPSet);
else return new CameraDriverHTTPStream(locPSet);
 
  throw std::exception(ait::aitSprintf("Unable to create driver. Using Transport:%s\n",cameraTransport).c_str());
}

//
// OPERATIONS
//

//
// ACCESS
//
/*
void 
CameraDriver::setSourceSize(const Vector2i& size)
{
  setParam("SourceWidth",boost::lexical_cast<std::string>(size.x));
  setParam("SourceHeight",boost::lexical_cast<std::string>(size.y));
}

const Vector2i
CameraDriver::getSourceSize()
{
  Vector2i size;
  size.x = boost::lexical_cast<int>(getParam("SourceWidth"));
  size.y = boost::lexical_cast<int>(getParam("SourceHeight"));
  return size;
}

void 
CameraDriver::setFramesPerSecond(double fps)
{
  setParam("FramesPerSecond",boost::lexical_cast<std::string>(fps));
}

double 
CameraDriver::getFramesPerSecond()
{
  return boost::lexical_cast<double>(getParam("FramesPerSecond"));
}
*/
std::string
CameraDriver::getParam(const std::string& fname,const std::string& paramName)
{
  std::string stringData;
  File::readTextFileToMemory(fname,stringData);
  
  using namespace std;

  string linebuffer;

  int linenumber;
  int curCharIndex = 0;

  for (linenumber = 1;; linenumber++)
  {
    if (curCharIndex >= stringData.size())
    {
      break;
    }
    int endLineIndex = curCharIndex;
    while (endLineIndex < stringData.size() && stringData[endLineIndex] != '\n')
    {
      endLineIndex++;
    }
    linebuffer = stringData.substr(curCharIndex,endLineIndex-curCharIndex+1);
    curCharIndex = endLineIndex+1;

    linebuffer = ait::strip(linebuffer);

    // Skip empty lines and comments.
    if (linebuffer.size() == 0 || linebuffer[0] == '#') continue;

    int i = linebuffer.find('=');
    if (i <= 0)
    {
      throw std::exception(ait::aitSprintf("CameraDriver::loadParams() Syntax error in line %d\n",linenumber).c_str());
    }
    string name = ait::strip(linebuffer.substr(0,i));
    string val = ait::strip(linebuffer.substr(i+1));

    if (name == paramName)
    {
      return val;
    }
  }

  return "";
}

void 
CameraDriver::loadParams(const std::string& fname)
{
  std::string stringData;
  File::readTextFileToMemory(fname,stringData);
  
  using namespace std;

  string linebuffer;

  int linenumber;
  int curCharIndex = 0;

  for (linenumber = 1;; linenumber++)
  {
    if (curCharIndex >= stringData.size())
    {
      break;
    }
    int endLineIndex = curCharIndex;
    while (endLineIndex < stringData.size() && stringData[endLineIndex] != '\n')
    {
      endLineIndex++;
    }
    linebuffer = stringData.substr(curCharIndex,endLineIndex-curCharIndex+1);
    curCharIndex = endLineIndex+1;

    linebuffer = ait::strip(linebuffer);

    // Skip empty lines and comments.
    if (linebuffer.size() == 0 || linebuffer[0] == '#') continue;

    int i = linebuffer.find('=');
    if (i <= 0)
    {
      throw std::exception(ait::aitSprintf("CameraDriver::loadParams() Syntax error in line %d\n",linenumber).c_str());
    }
    string name = ait::strip(linebuffer.substr(0,i));
    string val = ait::strip(linebuffer.substr(i+1));

    if (name == "UserPassword")
    {
      std::string decoded; 
      Base64::decode(val,decoded);
      setParam(name,decoded);
    }
    else
    {
      setParam(name,val);
    }
  }
}



//
// INQUIRY
//

}; // namespace ait


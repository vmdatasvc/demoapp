/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef CameraDriverAxisEventsShared_HPP
#define CameraDriverAxisEventsShared_HPP

#ifndef SHAREDAPI
#ifdef VideoMining_CameraDriver_Axis_EXPORTS // <-- This is defined by CMake.
  #define SHAREDAPI __declspec(dllexport)
#else
  #define SHAREDAPI __declspec(dllimport)
#endif
#endif

namespace ait 
{


/**
 * This class is not yet documented.
 */
class SHAREDAPI CameraDriverAxisEventsShared
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  CameraDriverAxisEventsShared();

  /**
   * Destructor
   */
  virtual ~CameraDriverAxisEventsShared();


  //
  // OPERATIONS
  //

public:

  virtual void onVideoFrame(int cameraId, double startTimeStamp, double endTimeStamp,
    unsigned char *data, int dataSize);

};


}; // namespace ait

#endif // CameraDriverAxisEventsShared_HPP


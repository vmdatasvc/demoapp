/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "CameraDriverAxis.hpp"

namespace ait 
{

CameraDriverAxis::CameraDriverAxis(const std::string& cameraProtocol) : 
  mpImp(new CameraDriverAxisShared()),
  mpEvents(0)
{
}

CameraDriverAxis::~CameraDriverAxis()
{
  delete mpImp; 
  if (mpEvents) delete mpEvents;
}

//
// OPERATIONS
//

//
// ACCESS
//

//
// INQUIRY
//

}; // namespace ait


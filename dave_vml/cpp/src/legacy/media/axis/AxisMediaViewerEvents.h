// Contains the event handlers that may be sent by the COM objects.

#pragma once

#include "CameraDriverAxisEventsShared.hpp"

// CAxisMediaViewerEvents command target

class CAxisMediaViewerEvents : public CCmdTarget
{
	DECLARE_DYNAMIC(CAxisMediaViewerEvents)

public:
	CAxisMediaViewerEvents();
	virtual ~CAxisMediaViewerEvents();

	virtual void OnFinalRelease();

  void setEvents(ait::CameraDriverAxisEventsShared* pEvents) { mpEvents = pEvents; }

protected:
  ait::CameraDriverAxisEventsShared* mpEvents;

protected:
	DECLARE_MESSAGE_MAP()
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()

	void OnDecodedImage(UINT64 dw64StartTime, SHORT nColorSpace, VARIANT &SampleArray);
};

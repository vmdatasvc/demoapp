// Callback.cpp : implementation file
//

#include "stdafx.h"
#include "AxisMediaParserEvents.h"
#include "AxisMediaParser.h"

// CAxisMediaParserEvents

IMPLEMENT_DYNAMIC(CAxisMediaParserEvents, CCmdTarget)


CAxisMediaParserEvents::CAxisMediaParserEvents()
{
	EnableAutomation();
}

CAxisMediaParserEvents::~CAxisMediaParserEvents()
{
}


void CAxisMediaParserEvents::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CCmdTarget::OnFinalRelease();
}


BEGIN_MESSAGE_MAP(CAxisMediaParserEvents, CCmdTarget)
END_MESSAGE_MAP()


BEGIN_DISPATCH_MAP(CAxisMediaParserEvents, CCmdTarget)
	DISP_FUNCTION_ID(CAxisMediaParserEvents, "OnError", AMP_OnError, OnError, VT_EMPTY, VTS_I4)
	DISP_FUNCTION_ID(CAxisMediaParserEvents, "OnTriggerData", AMP_OnTriggerData, OnTriggerData, VT_EMPTY, VTS_UI4 VTS_UI8 VTS_UI4 VTS_I2 VTS_UI4 VTS_I2 VTS_BOOL VTS_BSTR)
	DISP_FUNCTION_ID(CAxisMediaParserEvents, "OnVideoSample", AMP_OnVideoSample, OnVideoSample, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_UI8 VTS_UI8 VTS_VARIANT)
	DISP_FUNCTION_ID(CAxisMediaParserEvents, "OnAudioSample", AMP_OnAudioSample, OnAudioSample, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_UI8 VTS_UI8 VTS_VARIANT)
END_DISPATCH_MAP()

// Note: we add support for IID_IEvents to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .IDL file.

// {5A52D7D1-9763-43BF-999E-01935B1E537D}
static const IID IID_IEvents =
{ 0x5A52D7D1, 0x9763, 0x43BF, { 0x99, 0x9E, 0x1, 0x93, 0x5B, 0x1E, 0x53, 0x7D } };

BEGIN_INTERFACE_MAP(CAxisMediaParserEvents, CCmdTarget)
	INTERFACE_PART(CAxisMediaParserEvents, IID_IEvents, Dispatch)
	INTERFACE_PART(CAxisMediaParserEvents, __uuidof(IAxisMediaParserEvents), Dispatch)
END_INTERFACE_MAP()


// CAxisMediaParserEvents message handlers

void CAxisMediaParserEvents::OnError(long ErrorCode)
{
	//printf("CAxisMediaParserEvents::OnError() called with error 0x%X\n", ErrorCode);
}

void CAxisMediaParserEvents::OnTriggerData(LONG lCookieID, UINT64 dw64StartTime, DWORD dwUserTime, SHORT UserTimeFract, DWORD dwUnitTime, SHORT UnitTimeFract, BOOL bUnitTimeInvalid, LPCTSTR lpszTriggerData)
{
	//printf("CAxisMediaParserEvents::OnTriggerData()\n");
}

//#include <PvUtil.hpp>
void CAxisMediaParserEvents::OnVideoSample(LONG lCookieID, LONG lSampleType, LONG lSampleFlags, UINT64 dw64StartTime, UINT64 dw64StopTime, VARIANT &SampleArray)
{
  //static UINT64 lastFrame = 0;
  //int diff = (int)(dw64StartTime-lastFrame);
  //PVMSG("%i,%i,%i,%i\n",lSampleType,lSampleFlags,(int)dw64StartTime,diff);
  //lastFrame = dw64StartTime;
  m_pAxisMediaViewer->RenderVideoSample(lSampleFlags,dw64StartTime,dw64StopTime,SampleArray);
}

void CAxisMediaParserEvents::OnAudioSample(LONG lCookieID, LONG lSampleType, LONG lSampleFlags, UINT64 dw64StartTime, UINT64 dw64StopTime, VARIANT &SampleArray)
{
}

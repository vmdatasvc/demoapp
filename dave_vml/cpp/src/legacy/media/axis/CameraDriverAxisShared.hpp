/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef CameraDriverAxisShared_HPP
#define CameraDriverAxisShared_HPP

#ifndef SHAREDAPI
#ifdef VideoMining_CameraDriver_Axis_EXPORTS // <-- This is defined by CMake.
  #define SHAREDAPI __declspec(dllexport)
#else
  #define SHAREDAPI __declspec(dllimport)
#endif
#endif

#include "CameraDriverAxisEventsShared.hpp"

namespace ait 
{

/**
 * DLL interface for the Axis camera driver.
 * This class uses only built-in C++ types.
 */
class SHAREDAPI CameraDriverAxisShared
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  CameraDriverAxisShared();

  /**
   * Destructor
   */
  virtual ~CameraDriverAxisShared();


  //
  // OPERATIONS
  //

public:

  /**
   * Initialize the camera driver. Some parameters need to be set before this is called.
   */
  virtual void init();

  /**
   * Stop the camera driver. Do not call any function after this.
   */
  virtual void finish();

  /**
   * Set a parameter. Use cameraId = -1 to set a global parameter.
   */
  virtual void setParam(int cameraId, const char* name, const char *value);

  /**
   * Get the value of a parameter. Use cameraId = -1 for global parameters.
   */
  virtual void getParam(int cameraId, const char* name, char* value, int valueMaxLength);

  /**
   * Set several parameters at the same time. Use the following syntax:
   * param1=value1;param2=value2
   * You can also define using quotes: (Not implemented)
   * param1="value1";param2="value2"
   */
  virtual void setParams(int cameraId, const char* params);

  /**
   * This event handler will contain the member to call the events and return,
   * among other things, the frame data.
   */
  virtual void registerEventHandler(CameraDriverAxisEventsShared *pEvents);

protected:

  class Imp;
  Imp *mpImp;


};

class SHAREDAPI CameraDriverAxisSharedException
{
public:
  CameraDriverAxisSharedException(const char *errorMessage);
  CameraDriverAxisSharedException(const CameraDriverAxisSharedException& e);
  ~CameraDriverAxisSharedException();
  const char *getErrorMessage();

private:
  class Imp;
  Imp *mpImp;
  const char *mErrorMessage;

};


}; // namespace ait

#endif // CameraDriverAxisShared_HPP


// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

//#import "C:\\Program Files\\Axis Communications\\AXIS Media Parser SDK 2.10\\bin\\AxisMediaParser.dll" no_namespace
//#import "libid:951BE58F-1EF5-4ECF-9556-4A350EADE8A8" no_namespace
#import "..\\..\\..\\..\\common\\bin\\win32\\axis\\AxisMediaParser.dll" no_namespace

// CAxisMediaParser wrapper class

class CAxisMediaParser : public COleDispatchDriver
{
public:
  CAxisMediaParser(){} // Calls COleDispatchDriver default constructor
  CAxisMediaParser(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  CAxisMediaParser(const CAxisMediaParser& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

  // Attributes
public:

  // Operations
public:


  // IAxisMediaParser methods
public:
  void Connect(long * pCookieID, long * pNumberOfStreams, VARIANT * pMediaType)
  {
    static BYTE parms[] = VTS_PI4 VTS_PI4 VTS_PVARIANT ;
    InvokeHelper(0x1, DISPATCH_METHOD, VT_EMPTY, NULL, parms, pCookieID, pNumberOfStreams, pMediaType);
  }
  void GetVideoSize(long * pWidth, long * pHeight)
  {
    static BYTE parms[] = VTS_PI4 VTS_PI4 ;
    InvokeHelper(0x2, DISPATCH_METHOD, VT_EMPTY, NULL, parms, pWidth, pHeight);
  }
  void Start()
  {
    InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
  }
  void Stop()
  {
    InvokeHelper(0x4, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
  }
  void GetVideoCodec(long * pCodec)
  {
    static BYTE parms[] = VTS_PI4 ;
    InvokeHelper(0xa, DISPATCH_METHOD, VT_EMPTY, NULL, parms, pCodec);
  }
  void GetAudioCodec(long * pCodec)
  {
    static BYTE parms[] = VTS_PI4 ;
    InvokeHelper(0xb, DISPATCH_METHOD, VT_EMPTY, NULL, parms, pCodec);
  }

  // IAxisMediaParser properties
public:
  CString GetMediaURL()
  {
    CString result;
    GetProperty(0x5, VT_BSTR, (void*)&result);
    return result;
  }
  void SetMediaURL(CString propVal)
  {
    SetProperty(0x5, VT_BSTR, propVal);
  }
  CString GetMediaUsername()
  {
    CString result;
    GetProperty(0x6, VT_BSTR, (void*)&result);
    return result;
  }
  void SetMediaUsername(CString propVal)
  {
    SetProperty(0x6, VT_BSTR, propVal);
  }
  CString GetMediaPassword()
  {
    CString result;
    GetProperty(0x7, VT_BSTR, (void*)&result);
    return result;
  }
  void SetMediaPassword(CString propVal)
  {
    SetProperty(0x7, VT_BSTR, propVal);
  }
  long GetNetworkTimeout()
  {
    long result;
    GetProperty(0x8, VT_I4, (void*)&result);
    return result;
  }
  void SetNetworkTimeout(long propVal)
  {
    SetProperty(0x8, VT_I4, propVal);
  }
  long GetStatus()
  {
    long result;
    GetProperty(0x9, VT_I4, (void*)&result);
    return result;
  }
  void SetStatus(long propVal)
  {
    SetProperty(0x9, VT_I4, propVal);
  }

};

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef CameraDriverMjpeg_HPP
#define CameraDriverMjpeg_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <boost/thread.hpp>
#include <map>

// AIT INCLUDES
//

// LOCAL INCLUDES
//
#include <CameraDriver.hpp>
#include <ICameraDriverEvents.hpp>
#include <HttpSimpleClient.hpp>

// FORWARD REFERENCES
//

#include "CameraDriverMjpeg_fwd.hpp"

namespace ait 
{

class MjpegDecoderException : public std::exception
{
  public:
    MjpegDecoderException() : mWhat("MJpeg Decoder Exception") {}
    MjpegDecoderException(const std::string msg) : mWhat(msg) {}
  private:
    std::string mWhat;
    virtual const char* what() { return mWhat.c_str(); }
};

/**
 * Bridge implementation for Axis camera.
 */
class CameraDriverMjpeg : public CameraDriver
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  CameraDriverMjpeg(const std::string& cameraProtocol);

  /**
   * Destructor
   */
  virtual ~CameraDriverMjpeg();


  //
  // OPERATIONS
  //

public:

  /**
   * Start the main loop.
   */
  virtual void init(int numCameras);

  /**
   * Stop the main loop.
   */
  virtual void finish();

  /**
   * Set a specific parameter. See base class.
   */
  virtual void setParam(int cameraId, const std::string& name, const std::string& value)
  {
    mParams[name] = value;
  }

  /**
   * Get a specific parameter. See base class.
   */
  virtual const std::string getParam(int cameraId, const std::string& name)
  {
    if (mParams.find(name) == mParams.end()) return "";
    return mParams[name];
  }

  virtual const std::string getParam(const std::string& name)
  {
    return getParam(-1,name);
  }

  /**
   * Register the event handler to receive frames.
   */
  virtual void registerEventHandler(ICameraDriverEvents *pEvents)
  {
    if (mpEvents)
    {
      throw new std::exception("Event handler already registered");
    }
    mpEvents = pEvents;
  }

public:

  /**
   * This loop will read frames and call the callback on each new frame found.
   */
  void threadLoop();

protected:
	double getFramesPerSecond();

    /**
   * The thread class for boost has to be copyable, so this will
   * be the container.
   */
  class ThreadHolder
  {
  public:
    ThreadHolder(CameraDriverMjpeg& t) : mThread(t) {}
    CameraDriverMjpeg& mThread;
    void operator()() { mThread.threadLoop(); }
  };

  ThreadHolder *mpThreadHolder;

  void operator()()
  {
    threadLoop();
  }

  /**
   * Get one character from the Http stream. It throws an exception
   * if the end of the stream is found.
   */
  unsigned char getChar()
  {
    if (mpNextByte == 0 || mpNextByte == mpHeaderBuffer + mHeaderDataSize)
    {
      // Refill the buffer
      mHeaderDataSize = mpHttpClient->readData(mpHeaderBuffer,mHeaderBufferSize);
      mpNextByte = mpHeaderBuffer;
    }

    if (mpNextByte == mpHeaderBuffer + mHeaderDataSize)
    {
      // It's the end of the stream. This is usually unexpected, so we throw an exception.
      throw MjpegDecoderException("End of http stream found");
    }

    return *mpNextByte++;
  }

  /**
   * Start the connection, send the http request. Will throw on connection errors.
   */
  void initConnection();

  /**
   * Decode a frame given its bytes. It is in two parts, typically the beginning
   * of the data will come from the buffer used to scan the header and the rest
   * will come from the frame data buffer.
   */
  void decodeFrame(unsigned char *pFrameData, int dataSize, 
    unsigned char *pFrameData2, int data2Size,
    Image32& img);

  /**
   * Get one line. Typically when reading the HTTP header.
   * Returns the number of bytes in the line.
   */
  int getLine(char *lineBuffer, int maxLength);

  /**
   * Consume the stream until the given sequence of characters is found.
   * The next read will start just after the end of the given marker.
   */
  void skipToMarker(const char *marker, int markerLength);

  /**
   * Read memory on the frame buffer data of the given size. It will
   * see how much data is already on the header buffer and read the rest
   * in the frame buffer. If necessary, it will allocate a bigger buffer.
   */
  void readFrameBuffer(int dataSize);

  /**
   * Read the next frame and decode it into the given img.
   */
  void getFrame(Image32& img);

  //
  // ATTRIBUTES
  //

protected:

  unsigned char *mpHeaderBuffer;
  unsigned char *mpFrameBuffer;
  unsigned char *mpNextByte;
  int mHeaderBufferSize;
  int mHeaderDataSize;
  int mFrameBufferSize;
  int mFrameDataSize;

  HttpSimpleClientPtr mpHttpClient;

  ICameraDriverEvents *mpEvents;

  double mFrameTime;
  double mTimeBetweenFrames;

  bool mExitThread;

  std::map<std::string,std::string> mParams;

  boost::thread *mpCaptureThread;

};


}; // namespace ait

#endif // CameraDriverMjpeg_HPP


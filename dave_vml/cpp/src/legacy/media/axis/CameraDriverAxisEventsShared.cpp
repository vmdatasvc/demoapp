/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "CameraDriverAxisEventsShared.hpp"

namespace ait 
{

CameraDriverAxisEventsShared::CameraDriverAxisEventsShared()
{
}

CameraDriverAxisEventsShared::~CameraDriverAxisEventsShared()
{
}

void 
CameraDriverAxisEventsShared::onVideoFrame(int cameraId, double startTimeStamp, double endTimeStamp,
    unsigned char *data, int dataSize)
{
}


//
// OPERATIONS
//

//
// ACCESS
//

//
// INQUIRY
//

}; // namespace ait


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef CameraDriverMjpeg_fwd_HPP
#define CameraDriverMjpeg_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class CameraDriverMjpeg;
class MjpegDecoderException;
typedef boost::shared_ptr<CameraDriverMjpeg> CameraDriverMjpegPtr;

}; // namespace ait

#endif // CameraDriverMjpeg_fwd_HPP


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef CameraDriverAxis_fwd_HPP
#define CameraDriverAxis_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class CameraDriverAxis;
typedef boost::shared_ptr<CameraDriverAxis> CameraDriverAxisPtr;

}; // namespace ait

#endif // CameraDriverAxis_fwd_HPP


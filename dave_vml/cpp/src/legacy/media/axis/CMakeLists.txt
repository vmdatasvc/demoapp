
# Source files for this library

SET(SOURCES
	AxisMediaParserEvents.cpp
	AxisMediaViewerEvents.cpp
	stdafx.cpp
	CameraDriverAxisShared.cpp
	CameraDriverAxisEventsShared.cpp
)

SET(HEADERS
	AxisMediaParser.h
	AxisMediaParserEvents.h
	AxisMediaViewer.h
	AxisMediaViewerEvents.h
	stdafx.h
	CameraDriverAxisShared.hpp
	CameraDriverAxisEventsShared.hpp
)

IF(WIN32)
	SET(SOURCES ${SOURCES} ${HEADERS})
ENDIF(WIN32)

ADD_DEFINITIONS(-D_AFXDLL -D_MBCS)

REMOVE_DEFINITIONS(-D"NOMINMAX")

# The WIN32 flag generates a windows application. 
# Remove it to generate a console app. It is ignored in Unix.

# ADD_EXECUTABLE(axis_driver_test ${SOURCES})
# TARGET_LINK_LIBRARIES(groundtruth_gui ${GRAPHICS_LIBS} ${VISION_VMS_LIBS})
# SET_TARGET_PROPERTIES(groundtruth_gui PROPERTIES LINK_FLAGS ${GRAPHICS_LINK_FLAGS})

ADD_LIBRARY(VideoMining.CameraDriver.Axis SHARED
	${SOURCES}
	DllMain.cpp)

SET_TARGET_PROPERTIES(VideoMining.CameraDriver.Axis PROPERTIES
      LINK_FLAGS "/BASE:0x73FA0000 "
      )

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "stdafx.h"

#include "CameraDriverAxisShared.hpp"

#include "AxisMediaParser.h"
#include "AxisMediaParserEvents.h"
#include "AxisMediaViewer.h"
#include "AxisMediaViewerEvents.h"
#include <afxctl.h>									// Includes AfxConnectionAdvise()
#include <HttpSimpleClient.hpp>
#include <strutil.hpp>
#include <String.hpp>

namespace ait 
{

/**
 * The purpose of this class is to implement all the DLLs internals outside 
 * the .hpp file to avoid clutter and provide a cleaner interface for library
 * users.
 */
class CameraDriverAxisShared::Imp
{
public:

  Imp() : 
      communicationProtocol("axrtsp"),
      bitsPerPixel(32),
      cameraSourceSize(320,240),
      framesPerSecond(15)
      {};

  CAxisMediaParser parser;					// COM class wrapper
  CAxisMediaParserEvents parserEvents;	// Event class (derived from CCmdTarget)
  LONG lConnectionCookie;						// Connection cookie (used by callback functions)
  DWORD dwParserEventCookie;				// Event cookie (used by AfxConnectionUnadvise())

  CAxisMediaViewer viewer;          // The viewer will decode the samples.
  CAxisMediaViewerEvents viewerEvents;		// Event class (derived from CCmdTarget)
  DWORD dwViewerEventCookie;				// Event cookie (used by AfxConnectionUnadvise())

  CameraDriverAxisEventsShared *pEvents;

  CSize cameraSourceSize;
  double framesPerSecond;
  CString communicationProtocol;
  CString ipAddress;
  int bitsPerPixel;
  CString userName;
  CString userPassword;

};

CameraDriverAxisShared::CameraDriverAxisShared()
{
  mpImp = new CameraDriverAxisShared::Imp();
  mpImp->pEvents = 0;

  // Initialize COM for thread
  CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
}

CameraDriverAxisShared::~CameraDriverAxisShared()
{
  finish();

  delete mpImp;

  // Uninitialize COM
  CoUninitialize();
}

void 
CameraDriverAxisShared::init()
{
  LONG lNumberOfStreams;						// Contains the number of media streams for connection
  VARIANT MediaType;								// Receives a SAFEARRAY containing media type information

  if (mpImp->ipAddress == "")
  {
    throw CameraDriverAxisSharedException("parameter IpAddress must be defined.");
  }

  // Set camera parameters
  std::string cmd = aitSprintf("http://%s/axis-cgi/operator/param.cgi?action=update",(const char*)mpImp->ipAddress) + 
    aitSprintf("&Image.I0.Appearance.Resolution=%ix%i",mpImp->cameraSourceSize.cx,mpImp->cameraSourceSize.cy) +
    aitSprintf("&Image.I0.Stream.FPS=%i",(int)mpImp->framesPerSecond);

  try
  {
    HttpSimpleClient hsp(cmd);
    if (mpImp->userName != "")
    {
      hsp.setAuthentication((const char *)mpImp->userName,(const char *)mpImp->userPassword);
    }
    hsp.sendRequest();
    std::string response;
    hsp.readStringData(response);

    if (strip(response) != "OK")
    {
      throw std::exception(response.c_str());
    }
  }
  catch (std::exception ex)
  {
    throw CameraDriverAxisSharedException(ex.what());
  }

  // Create the AXIS Media Parser COM object
  if (!mpImp->parser.CreateDispatch("AxisMediaParserLib.AxisMediaParser"))
  {
    throw CameraDriverAxisSharedException("Failed to create dispatch for AxisMediaViewer");
  }

  // Get a pointer to IUnknown for our event handler class
  IUnknown* pUnkParserEvents = mpImp->parserEvents.GetIDispatch(false);

  // Establish a connection between parser and event handlers
  AfxConnectionAdvise(mpImp->parser.m_lpDispatch, __uuidof(IAxisMediaParserEvents), pUnkParserEvents, false, &mpImp->dwParserEventCookie);

  // Setup our COM object properties
  CString url;
  url.Append(mpImp->communicationProtocol);
  url.Append("://");
  url.Append(mpImp->ipAddress);
  if (mpImp->communicationProtocol == "axmphttp")
  {
    url.Append("/mjpg/1/video.mjpg");
  }
  else
  {
    url.Append("/mpeg4/media.amp/trackID=1");
  }
  mpImp->parser.SetMediaURL(url);
  if (mpImp->userName != "")
  {
    mpImp->parser.SetMediaUsername(mpImp->userName);
    mpImp->parser.SetMediaPassword(mpImp->userPassword);
  }

  // Create the AXIS Media Viewer
  if (!mpImp->viewer.CreateDispatch("AxisMediaViewerLib.AxisMediaViewer"))
  {
    throw CameraDriverAxisSharedException("Failed to create dispatch for AxisMediaViewer");
  }

  // Get a pointer to IUnknown for our event handler class
  IUnknown* pUnkViewerEvents = mpImp->viewerEvents.GetIDispatch(false);

  // Establish a connection between viewer and event handlers
  AfxConnectionAdvise(mpImp->viewer.m_lpDispatch, __uuidof(IAxisMediaViewerEvents), pUnkViewerEvents, false, &mpImp->dwViewerEventCookie);

  // Connect to our Axis camera
  try
  {
    mpImp->parser.Connect(&mpImp->lConnectionCookie, &lNumberOfStreams, &MediaType);

    // Create output file and set file handle to callback class
    mpImp->parserEvents.setMediaViewer(&mpImp->viewer);

    mpImp->viewer.SetVMR9(false);
    mpImp->viewer.SetColorSpace(mpImp->bitsPerPixel == 24 ? AMV_CS_RGB24 : AMV_CS_RGB32);
    mpImp->viewer.SetPlayOptions(AMV_PO_VIDEO);
    
    // Initialize our viewer
    mpImp->viewer.Init(0, MediaType, NULL);

    // Register event handler
    mpImp->viewerEvents.setEvents(mpImp->pEvents);

    // Get the size.
    long w,h;
    mpImp->parser.GetVideoSize(&w,&h);
    mpImp->cameraSourceSize.cx = (int)w;
    mpImp->cameraSourceSize.cy = (int)h;

    mpImp->viewer.Start();
    mpImp->parser.Start();
  }
  catch (COleDispatchException* e)
  {
    CameraDriverAxisSharedException ex(e->m_strDescription);
    e->Delete();
    finish();
    throw ex;
  }
}

void 
CameraDriverAxisShared::finish()
{
  try
  {
    if (mpImp->parser.m_lpDispatch)
    {
      // Stop the media stream and close output file
      mpImp->parser.Stop();

      // Stop
      mpImp->viewer.Stop();
    }
  }
  catch (COleDispatchException* e)
  {
    printf("%s\n", e->m_strDescription);
    e->Delete();
  }

  // Terminate the connection between parser and event handlers
  if (mpImp->parser.m_lpDispatch)
  {
    // Get a pointer to IUnknown for our event handler class
    IUnknown* pUnkParserEvents = mpImp->parserEvents.GetIDispatch(false);
    AfxConnectionUnadvise(mpImp->parser.m_lpDispatch, __uuidof(IAxisMediaParserEvents), pUnkParserEvents, false, mpImp->dwParserEventCookie);
  }

  // Terminate the connection between parser and event handlers
  if (mpImp->viewer.m_lpDispatch)
  {
    // Get a pointer to IUnknown for our event handler class
    IUnknown* pUnkViewerEvents = mpImp->viewerEvents.GetIDispatch(false);

    AfxConnectionUnadvise(mpImp->viewer.m_lpDispatch, __uuidof(IAxisMediaViewerEvents), pUnkViewerEvents, false, mpImp->dwViewerEventCookie);
  }

  // Make sure we release COM object before we uninitialize COM
  // (usually not needed since the destructor will do it for us)
  mpImp->parser.ReleaseDispatch();
  mpImp->viewer.ReleaseDispatch();
}

void 
CameraDriverAxisShared::setParam(int cameraId, const char* name0, const char *value0)
{
  CString name(name0);
  CString value(value0);

  if (name == "NumCameras")
  {
    throw CameraDriverAxisSharedException("Only 1 Camera is supported by this Driver");
  }
  else if (name == "MaxCameras")
  {
    throw CameraDriverAxisSharedException("This value is read-only");
  }
  else if (name == "SourceBitCount")
  {
    if (value != "32" && value != "24")
    {
      throw CameraDriverAxisSharedException("Only 32 and 24 bpp are supported.");
    }
    mpImp->bitsPerPixel = atoi(value);
  }
  else if (name == "SourceWidth")
  {
    mpImp->cameraSourceSize.cx = atoi(value);
  }
  else if (name == "SourceHeight")
  {
    mpImp->cameraSourceSize.cy = atoi(value);
  }
  else if (name == "FramesPerSecond")
  {
    mpImp->framesPerSecond = atof(value);
  }
  else if (name == "Axis.CommunicationProtocol")
  {
    mpImp->communicationProtocol = value;
  }
  else if (name == "IpAddress")
  {
    mpImp->ipAddress = value;
  }
  else if (name == "UserName")
  {
    mpImp->userName = value;
  }
  else if (name == "UserPassword")
  {
    mpImp->userPassword = value;
  }
}

void 
CameraDriverAxisShared::getParam(int cameraId, const char* name0, char* value0, int valueMaxLength)
{
  CString name(name0);
  CString value;

  if (name == "NumCameras")
  {
    value = "1";
  }
  else if (name == "MaxCameras")
  {
    value = "1";
  }
  else if (name == "SourceBitCount")
  {
    value = "32";
  }
  else if (name == "SourceWidth")
  {
    value.Format("%i",mpImp->cameraSourceSize.cx);
  }
  else if (name == "SourceHeight")
  {
    value.Format("%i",mpImp->cameraSourceSize.cy);
  }
  else if (name == "FramesPerSecond")
  {
    value.Format("%i",(int)mpImp->framesPerSecond);
  }
  else if (name == "Axis.CommunicationProtocol")
  {
    value = mpImp->communicationProtocol;
  }
  else if (name == "IpAddress")
  {
    value = mpImp->ipAddress;
  }

  if (value.GetLength() >= valueMaxLength)
  {
    throw new CameraDriverAxisSharedException("Value length is too large");
  }

  ::strcpy(value0,value);
}

void 
CameraDriverAxisShared::setParams(int cameraId, const char* params)
{
  throw new CameraDriverAxisSharedException("Not Implemented");
}

void 
CameraDriverAxisShared::registerEventHandler(CameraDriverAxisEventsShared *pEvents)
{
  mpImp->pEvents = pEvents;
}

class CameraDriverAxisSharedException::Imp
{
public:
  CString what;
};

CameraDriverAxisSharedException::CameraDriverAxisSharedException(const CameraDriverAxisSharedException& ex)
{
  mpImp = new Imp();
  mpImp->what = ex.mpImp->what;
  mErrorMessage = (const char*)mpImp->what;
}

CameraDriverAxisSharedException::CameraDriverAxisSharedException(const char *what)
{
  mpImp = new Imp();
  mpImp->what = what;
  mErrorMessage = (const char*)mpImp->what;
}

CameraDriverAxisSharedException::~CameraDriverAxisSharedException()
{
  delete mpImp;
}

const char *
CameraDriverAxisSharedException::getErrorMessage()
{
  return mpImp->what;
}

}; // namespace ait


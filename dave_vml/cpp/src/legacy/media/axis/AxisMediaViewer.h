// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard


#import "..\\..\\..\\..\\common\\bin\\win32\\axis\\AxisMediaViewer.dll" no_namespace
//#import "libid:F6A698FA-9E65-4757-B01C-F17674D37707" no_namespace
//#import "C:\\Program Files\\Axis Communications\\AXIS Media Parser SDK 2.10\\bin\\AxisMediaViewer.dll" no_namespace
// CAxisMediaViewer wrapper class

#pragma once

class CAxisMediaViewer : public COleDispatchDriver
{
public:
  CAxisMediaViewer(){} // Calls COleDispatchDriver default constructor
  CAxisMediaViewer(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  CAxisMediaViewer(const CAxisMediaViewer& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

  // Attributes
public:

  // Operations
public:


  // IAxisMediaViewer methods
public:
  void AddFilter(LPUNKNOWN pIFilter, LPCTSTR FilterName, long Flags)
  {
    static BYTE parms[] = VTS_UNKNOWN VTS_BSTR VTS_I4 ;
    InvokeHelper(0x1, DISPATCH_METHOD, VT_EMPTY, NULL, parms, pIFilter, FilterName, Flags);
  }
  void Init(long NumberOfStreams, VARIANT& MediaType, __int64 hWnd)
  {
    static BYTE parms[] = VTS_I4 VTS_VARIANT VTS_I8 ;
    InvokeHelper(0x2, DISPATCH_METHOD, VT_EMPTY, NULL, parms, NumberOfStreams, &MediaType, hWnd);
  }
  void Init32(long NumberOfStreams, VARIANT& MediaType, long hWndLow, long hWndHigh)
  {
    static BYTE parms[] = VTS_I4 VTS_VARIANT VTS_I4 VTS_I4 ;
    InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, parms, NumberOfStreams, &MediaType, hWndLow, hWndHigh);
  }
  void Start()
  {
    InvokeHelper(0x4, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
  }
  void Stop()
  {
    InvokeHelper(0x5, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
  }
  void RenderVideoSample(long SampleFlags, unsigned __int64 StartTime, unsigned __int64 StopTime, VARIANT& SampleArray)
  {
    static BYTE parms[] = VTS_I4 VTS_UI8 VTS_UI8 VTS_VARIANT ;
    InvokeHelper(0x6, DISPATCH_METHOD, VT_EMPTY, NULL, parms, SampleFlags, StartTime, StopTime, &SampleArray);
  }
  void RenderVideoSample32(long SampleFlags, long StartTimeLow, long StartTimeHigh, long StopTimeLow, long StopTimeHigh, VARIANT& SampleArray)
  {
    static BYTE parms[] = VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_VARIANT ;
    InvokeHelper(0x7, DISPATCH_METHOD, VT_EMPTY, NULL, parms, SampleFlags, StartTimeLow, StartTimeHigh, StopTimeLow, StopTimeHigh, &SampleArray);
  }
  void RenderAudioSample(long SampleFlags, unsigned __int64 StartTime, unsigned __int64 StopTime, VARIANT& SampleArray)
  {
    static BYTE parms[] = VTS_I4 VTS_UI8 VTS_UI8 VTS_VARIANT ;
    InvokeHelper(0x8, DISPATCH_METHOD, VT_EMPTY, NULL, parms, SampleFlags, StartTime, StopTime, &SampleArray);
  }
  void RenderAudioSample32(long SampleFlags, long StartTimeLow, long StartTimeHigh, long StopTimeLow, long StopTimeHigh, VARIANT& SampleArray)
  {
    static BYTE parms[] = VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_VARIANT ;
    InvokeHelper(0x9, DISPATCH_METHOD, VT_EMPTY, NULL, parms, SampleFlags, StartTimeLow, StartTimeHigh, StopTimeLow, StopTimeHigh, &SampleArray);
  }
  void GetVideoSize(long * pWidth, long * pHeight)
  {
    static BYTE parms[] = VTS_PI4 VTS_PI4 ;
    InvokeHelper(0xa, DISPATCH_METHOD, VT_EMPTY, NULL, parms, pWidth, pHeight);
  }
  void DisplayModeChanged()
  {
    InvokeHelper(0xb, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
  }
  void RepaintVideo()
  {
    InvokeHelper(0xc, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
  }
  void SetVideoPosition(long Left, long Top, long Right, long Bottom)
  {
    static BYTE parms[] = VTS_I4 VTS_I4 VTS_I4 VTS_I4 ;
    InvokeHelper(0xd, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Left, Top, Right, Bottom);
  }
  void GetVideoCodec(long * pCodec)
  {
    static BYTE parms[] = VTS_PI4 ;
    InvokeHelper(0x13, DISPATCH_METHOD, VT_EMPTY, NULL, parms, pCodec);
  }
  void GetAudioCodec(long * pCodec)
  {
    static BYTE parms[] = VTS_PI4 ;
    InvokeHelper(0x14, DISPATCH_METHOD, VT_EMPTY, NULL, parms, pCodec);
  }

  // IAxisMediaViewer properties
public:
  BOOL GetVMR9()
  {
    BOOL result;
    GetProperty(0xe, VT_BOOL, (void*)&result);
    return result;
  }
  void SetVMR9(BOOL propVal)
  {
    SetProperty(0xe, VT_BOOL, propVal);
  }
  short GetColorSpace()
  {
    short result;
    GetProperty(0xf, VT_I2, (void*)&result);
    return result;
  }
  void SetColorSpace(short propVal)
  {
    SetProperty(0xf, VT_I2, propVal);
  }
  long GetPlayOptions()
  {
    long result;
    GetProperty(0x10, VT_I4, (void*)&result);
    return result;
  }
  void SetPlayOptions(long propVal)
  {
    SetProperty(0x10, VT_I4, propVal);
  }
  long GetVolume()
  {
    long result;
    GetProperty(0x11, VT_I4, (void*)&result);
    return result;
  }
  void SetVolume(long propVal)
  {
    SetProperty(0x11, VT_I4, propVal);
  }
  long GetStatus()
  {
    long result;
    GetProperty(0x12, VT_I4, (void*)&result);
    return result;
  }
  void SetStatus(long propVal)
  {
    SetProperty(0x12, VT_I4, propVal);
  }

};

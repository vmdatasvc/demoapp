
#pragma once

#include <afxmt.h>
#include "AxisMediaViewer.h"

// CAxisMediaParserEvents command target

class CAxisMediaParserEvents : public CCmdTarget
{
	DECLARE_DYNAMIC(CAxisMediaParserEvents)

public:
	CAxisMediaParserEvents();
	virtual ~CAxisMediaParserEvents();

	virtual void OnFinalRelease();

  void setMediaViewer(CAxisMediaViewer *pViewer) { m_pAxisMediaViewer = pViewer; }

protected:
	DECLARE_MESSAGE_MAP()
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
	
	CCriticalSection m_Critical;
  CAxisMediaViewer *m_pAxisMediaViewer;
	
	void OnError(long ErrorCode);
	void OnTriggerData(LONG lCookieID, UINT64 dw64StartTime, DWORD dwUserTime, SHORT UserTimeFract, DWORD dwUnitTime, SHORT UnitTimeFract, BOOL bUnitTimeInvalid, LPCTSTR lpszTriggerData);
	void OnVideoSample(LONG lCookieID, LONG lSampleType, LONG lSampleFlags, UINT64 dw64StartTime, UINT64 dw64StopTime, VARIANT &SampleArray);
	void OnAudioSample(LONG lCookieID, LONG lSampleType, LONG lSampleFlags, UINT64 dw64StartTime, UINT64 dw64StopTime, VARIANT &SampleArray);
};

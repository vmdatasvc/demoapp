/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef CameraDriverAxis_HPP
#define CameraDriverAxis_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

// LOCAL INCLUDES
//
#include <CameraDriver.hpp>
#include <ICameraDriverEvents.hpp>
#include "CameraDriverAxisShared.hpp"
#include "CameraDriverAxisEventsShared.hpp"

// FORWARD REFERENCES
//

#include "CameraDriverAxis_fwd.hpp"

namespace ait 
{


/**
 * Bridge implementation for Axis camera.
 */
class CameraDriverAxis : public CameraDriver
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  CameraDriverAxis(const std::string& cameraProtocol);

  /**
   * Destructor
   */
  virtual ~CameraDriverAxis();


  //
  // OPERATIONS
  //

public:

  virtual void init(int numCameras) 
  {
    try
    {
      mpImp->init(); 
    }
    catch (CameraDriverAxisSharedException ex)
    {
      throw std::exception(ex.getErrorMessage());
    }
  }

  virtual void finish() 
  { 
    try
    {
     mpImp->finish(); 
    }
    catch (CameraDriverAxisSharedException ex)
    {
      throw std::exception(ex.getErrorMessage());
    }
  }

  virtual void setParam(int cameraId, const std::string& name, const std::string& value)
  {
    try
    {
      mpImp->setParam(cameraId,name.c_str(),value.c_str());
    }
    catch (CameraDriverAxisSharedException ex)
    {
      throw std::exception(ex.getErrorMessage());
    }
  }

  virtual const std::string getParam(int cameraId, const std::string& name)
  {
    try
    {
      char val[1024];
      mpImp->getParam(cameraId,name.c_str(),val,sizeof(val));
      return std::string(val);
    }
    catch (CameraDriverAxisSharedException ex)
    {
      throw std::exception(ex.getErrorMessage());
    }
  }

  virtual void registerEventHandler(ICameraDriverEvents *pEvents)
  {
    if (mpEvents)
    {
      throw new std::exception("Event handler already registered");
    }
    try
    {
      mpEvents = new Events(pEvents);
      mpImp->registerEventHandler(mpEvents);
    }
    catch (CameraDriverAxisSharedException ex)
    {
      throw std::exception(ex.getErrorMessage());
    }
  }

  //
  // ATTRIBUTES
  //

protected:

  CameraDriverAxisShared *mpImp;
  CameraDriverAxisEventsShared *mpEvents;

protected:

  /**
   * Handler for events from DLL.
   */
  class Events : public CameraDriverAxisEventsShared
  {
  public:
    Events(ICameraDriverEvents *pEvents) : mpEvents(pEvents) {}
    virtual ~Events() {};

    virtual void onVideoFrame(int cameraId, double startTimeStamp, double endTimeStamp,
      unsigned char *data, int dataSize)
    {
      mpEvents->onVideoFrame(cameraId,startTimeStamp,endTimeStamp,data,dataSize);
    }
  private:
    ICameraDriverEvents *mpEvents;
  };


};


}; // namespace ait

#endif // CameraDriverAxis_HPP


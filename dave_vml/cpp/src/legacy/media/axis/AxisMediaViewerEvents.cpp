// Contains the event handlers that may be sent by the COM objects.

// Callback.cpp : implementation file
//

#include "stdafx.h"
#include "AxisMediaViewer.h"
#include "AxisMediaViewerEvents.h"


// CAxisMediaViewerEvents

IMPLEMENT_DYNAMIC(CAxisMediaViewerEvents, CCmdTarget)


CAxisMediaViewerEvents::CAxisMediaViewerEvents()
{
	EnableAutomation();
}

CAxisMediaViewerEvents::~CAxisMediaViewerEvents()
{
}


void CAxisMediaViewerEvents::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CCmdTarget::OnFinalRelease();
}


BEGIN_MESSAGE_MAP(CAxisMediaViewerEvents, CCmdTarget)
END_MESSAGE_MAP()


BEGIN_DISPATCH_MAP(CAxisMediaViewerEvents, CCmdTarget)
	DISP_FUNCTION_ID(CAxisMediaViewerEvents, "OnDecodedImage", AMV_OnDecodedImage, OnDecodedImage, VT_EMPTY, VTS_UI8 VTS_I2 VTS_VARIANT)
END_DISPATCH_MAP()

// Note: we add support for IID_IEvents to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .IDL file.

// {7C883F81-78ED-41BE-976A-72B47C1F6639}
static const IID IID_IEvents =
{ 0x7C883F81, 0x78ED, 0x41BE, { 0x97, 0x6A, 0x72, 0xB4, 0x7C, 0x1F, 0x66, 0x39 } };

BEGIN_INTERFACE_MAP(CAxisMediaViewerEvents, CCmdTarget)
	INTERFACE_PART(CAxisMediaViewerEvents, IID_IEvents, Dispatch)
	INTERFACE_PART(CAxisMediaViewerEvents, __uuidof(IAxisMediaViewerEvents), Dispatch)
END_INTERFACE_MAP()


// CAxisMediaViewerEvents message handlers

//#include <PvUtil.hpp>
void CAxisMediaViewerEvents::OnDecodedImage(UINT64 dw64StartTime, SHORT nColorSpace, VARIANT &SampleArray)
{
	// Only interested in RGB24 and RGB32
	if ((nColorSpace != AMV_CS_RGB24) && (nColorSpace != AMV_CS_RGB32))
		return;

	// Define local variables
	COleSafeArray SafeArray;
	void* pBuffer;
	DWORD dwBufferSize;

	// Attach image buffer to our MFC helper class
	SafeArray.Attach(SampleArray);
	SafeArray.AccessData((void**) &pBuffer);
	dwBufferSize = SafeArray.GetOneDimSize();

  /*
	DWORD dwReturn;
	HANDLE hFile;

  static int i = 0;
  CString str;
  str.Format("image%02i.bmp", i++);
	hFile = CreateFile(str, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, 0, NULL);
	BITMAPFILEHEADER bfh = {
		'M' << 8 | 'B', // "BM"
		(sizeof BITMAPFILEHEADER) + dwBufferSize,
		0,
		0,
		(sizeof BITMAPFILEHEADER) + (sizeof BITMAPINFOHEADER)
	};

	// Write BMP file header and image data to file
	WriteFile(hFile, &bfh, sizeof BITMAPFILEHEADER, &dwReturn, NULL);
	WriteFile(hFile, pBuffer, dwBufferSize, &dwReturn, NULL);
	CloseHandle(hFile);
  */

  //static UINT64 lastFrame = 0;
  //PVMSG("Decode -- > %i\n",(int)dw64StartTime);

  BITMAPINFOHEADER *pBih = (BITMAPINFOHEADER *)pBuffer;
  if (mpEvents)
  {
    mpEvents->onVideoFrame(0,0,0,(unsigned char *)pBuffer + pBih->biSize,(int)(dwBufferSize-pBih->biSize));
  }

	// Release buffer from our MFC helper class
	SafeArray.UnaccessData();
	SampleArray = SafeArray.Detach();
}

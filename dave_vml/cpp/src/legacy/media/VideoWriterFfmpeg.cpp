/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include "VideoWriterFfmpeg.hpp"
#include "Settings.hpp"

namespace ait 
{

class InitClass
{
public:
  InitClass()
  {
    av_register_all();
  }
};

InitClass c;

VideoWriterFfmpeg::VideoWriterFfmpeg() :
  fmt(0),
  oc(0),
  video_st(0),
  video_outbuf(0),
  video_outbuf_size(200000) // This will need to be revised
{
}

VideoWriterFfmpeg::~VideoWriterFfmpeg()
{
  close();
}

void 
VideoWriterFfmpeg::open(const std::string& fname)
{
  av_register_all();

  mFileName = Settings::replaceVars(fname);
  // Try to create the file so there is a message if there is a problem.
  FILE *fp = fopen(fname.c_str(),"wb");
  if (!fp || fclose(fp) != 0)
  {
    throw std::exception(("Unable to create: " + fname).c_str());
  }
}

void
VideoWriterFfmpeg::writeFirstFrame(const Image32& img)
{
  fmt = guess_format(NULL, mFileName.c_str(), NULL);

  if (!fmt) 
  {
    PVMSG("Could not deduce output format from file extension: using MPEG.\n");
    fmt = guess_format("mpeg", NULL, NULL);
  }
  if (!fmt) 
  {
    throw std::exception(("Could not find suitable output format: " + mFileName).c_str());
  }

  //fmt->video_codec = CODEC_ID_XVID;

  /* allocate the output media context */
  oc = av_alloc_format_context();
  if (!oc)
  {
    throw std::exception(("Memory error writing: " + mFileName).c_str());
  }
  oc->oformat = fmt;
  _snprintf(oc->filename, sizeof(oc->filename), "%s", mFileName.c_str());

  /* add the audio and video streams using the default format codecs
  and initialize the codecs */
  video_st = NULL;
  if (fmt->video_codec != CODEC_ID_NONE)
  {
    video_st = add_video_stream(oc, fmt->video_codec,img);
  }

  /* set the output parameters (must be done even if no
  parameters). */
  if (av_set_parameters(oc, NULL) < 0)
  {
    throw std::exception(("Invalid output format parameters: " + mFileName).c_str());
  }

  dump_format(oc, 0, oc->filename, 1);

  /* now that all the parameters are set, we can open the audio and
  video codecs and allocate the necessary encode buffers */
  if (video_st)
  {
    open_video(oc, video_st);
  }

  /* open the output file, if needed */
  if (!(fmt->flags & AVFMT_NOFILE)) 
  {
    if (url_fopen(&oc->pb, mFileName.c_str(), URL_WRONLY) < 0) 
    {
      throw std::exception(("Could not open: " + mFileName).c_str());
    }
  }

  /* write the stream header, if any */
  av_write_header(oc);
}

void 
VideoWriterFfmpeg::writeFrame(const Image32& img)
{
  if (!isCodecInitialized())
  {
    writeFirstFrame(img);
  }

  write_video_frame(oc, video_st, img);
}

void 
VideoWriterFfmpeg::skipFrame()
{
  /*
  if (!isFilterGraphInitialized())
  {
  PvUtil::exitError("You must write at least one frime to skip frames!");
  }

  mpPvImageSourceFilter->writePreviousFrame();
  */
}

void 
VideoWriterFfmpeg::close()
{
  if (oc)
  {
    /* close each codec */
    if (video_st)
    {
      avcodec_close(video_st->codec);
      free(picture->data[0]);
      av_free(picture);
      free(video_outbuf);
    }

    /* write the trailer, if any */
    av_write_trailer(oc);

    /* free the streams */
    for(int i = 0; i < oc->nb_streams; i++)
    {
      av_freep(&oc->streams[i]->codec);
      av_freep(&oc->streams[i]);
    }

    if (!(fmt->flags & AVFMT_NOFILE))
    {
      /* close the output file */
      url_fclose(&oc->pb);
    }

    /* free the stream */
    av_free(oc);
    oc = 0;
  }
}

bool 
VideoWriterFfmpeg::loadCodecSettings(const std::string& fname, bool showDialogIfNotPresent)
{
  return true;
}

void 
VideoWriterFfmpeg::saveCodecSettings(const std::string& fname)
{
}

bool 
VideoWriterFfmpeg::showCodecSettingsDialog(const std::string& fname)
{
  /*
  std::string args = "\"" + fname + "\"";
  if (mOwner != NULL)
  {
  args += "--parent_window " + aitSprintf("%i",(int)mOwner);
  }

  TCHAR exeFileName[MAX_PATH+1];
  TCHAR *p;

  DWORD length = SearchPath(
  NULL,
  "codec_settings.exe",
  NULL,
  MAX_PATH,
  exeFileName,
  &p);

  if (length == 0)
  {
  PvUtil::exitError("Error starting codec_settings.exe");
  return false;
  }

  SHELLEXECUTEINFO sei;
  ZeroMemory(&sei,sizeof(sei));
  sei.cbSize = sizeof(sei);
  sei.lpFile = exeFileName;
  sei.lpParameters = args.c_str();
  sei.lpVerb = "open";
  sei.nShow = SW_SHOWNORMAL;
  sei.hwnd = mOwner;
  sei.fMask = SEE_MASK_NOCLOSEPROCESS;

  if (!ShellExecuteEx(&sei))
  {
  PvUtil::exitError("Error starting codec_settings.exe");
  }
  else
  {
  WaitForSingleObject(sei.hProcess,INFINITE);
  DWORD exitCode;
  if (GetExitCodeProcess(sei.hProcess,&exitCode) == 0)
  {
  CloseHandle(sei.hProcess);
  return false;
  }
  CloseHandle(sei.hProcess);
  return exitCode == 0;
  }
  return false;
  */
  return false;
}

bool 
VideoWriterFfmpeg::hasValidCodec()
{
  return true;
}


AVStream *
VideoWriterFfmpeg::add_video_stream(AVFormatContext *oc, int codec_id, const Image32& img)
{
  AVCodecContext *c;
  AVStream *st;

  st = av_new_stream(oc, 0);
  if (!st) {
    fprintf(stderr, "Could not alloc stream\n");
    exit(1);
  }

  c = st->codec;
  c->codec_id = (CodecID)codec_id;
  c->codec_type = CODEC_TYPE_VIDEO;

  /* put sample parameters */
  c->bit_rate = 200000;
  /* resolution must be a multiple of two */
  c->width = img.width();
  c->height = img.height();
  /* time base: this is the fundamental unit of time (in seconds) in terms
  of which frame timestamps are represented. for fixed-fps content,
  timebase should be 1/framerate and timestamp increments should be
  identically 1. */
  c->time_base.den = ait::round(getFramesPerSecond()); // TODO: Fix for non integer values.
  c->time_base.num = 1;
  c->gop_size = 50; /* key frame rate is 50 */
  c->pix_fmt = PIX_FMT_YUV420P;
  if (c->codec_id == CODEC_ID_MPEG2VIDEO) {
    /* just for testing, we also add B frames */
    c->max_b_frames = 2;
  }
  if (c->codec_id == CODEC_ID_MPEG1VIDEO){
    /* needed to avoid using macroblocks in which some coeffs overflow
    this doesnt happen with normal video, it just happens here as the
    motion of the chroma plane doesnt match the luma plane */
    c->mb_decision=2;
  }
  if (c->codec_id == CODEC_ID_MPEG4) {
    /* set the tag for windows encoders. */
    c->codec_tag = 'x' + ('v'<<8) + ('i'<<16) + ('d'<<24);
  }

  // some formats want stream headers to be seperate
  if(!strcmp(oc->oformat->name, "mp4") || !strcmp(oc->oformat->name, "mov") || !strcmp(oc->oformat->name, "3gp"))
    c->flags |= CODEC_FLAG_GLOBAL_HEADER;

  return st;
}

void
VideoWriterFfmpeg::write_video_frame(AVFormatContext *oc, AVStream *st, const Image32& img)
{
  int out_size, ret;
  AVCodecContext *c;

  c = st->codec;

  // Create temporary picture to convert.

  AVFrame *tmp_picture;

  tmp_picture = avcodec_alloc_frame();
  if (!picture)
  {
    throw std::exception("Error allocating frame");
  }

  avpicture_fill((AVPicture *)tmp_picture, (uint8_t *)img.pointer(),
    PIX_FMT_RGBA32, img.width(), img.height());

  img_convert((AVPicture *)picture, PIX_FMT_YUV420P,
    (AVPicture *)tmp_picture, PIX_FMT_RGBA32,c->width, c->height);

  av_free(tmp_picture);

  static int fcount = 1;
  //fill_yuv_image(picture,fcount++,c->width,c->height);

  /*
  if (c->pix_fmt != PIX_FMT_YUV420P) 
  {
  // as we only generate a YUV420P picture, we must convert it
  // to the codec pixel format if needed
  fill_yuv_image(tmp_picture, frame_count, c->width, c->height);
  img_convert((AVPicture *)picture, c->pix_fmt,
  (AVPicture *)tmp_picture, PIX_FMT_YUV420P,
  c->width, c->height);
  } 
  else 
  {
  fill_yuv_image(picture, frame_count, c->width, c->height);
  }
  */


  if (oc->oformat->flags & AVFMT_RAWPICTURE) 
  {
    /* raw video case. The API will change slightly in the near
    future for that */
    AVPacket pkt;
    av_init_packet(&pkt);

    pkt.flags |= PKT_FLAG_KEY;
    pkt.stream_index= st->index;
    pkt.data= (uint8_t *)picture;
    pkt.size= sizeof(AVPicture);

    ret = av_write_frame(oc, &pkt);
  } 
  else 
  {
    /* encode the image */
    out_size = avcodec_encode_video(c, video_outbuf, video_outbuf_size, picture);
    /* if zero size, it means the image was buffered */
    if (out_size > 0) 
    {
      AVPacket pkt;
      av_init_packet(&pkt);

      pkt.pts= av_rescale_q(c->coded_frame->pts, c->time_base, st->time_base);
      if(c->coded_frame->key_frame)
        pkt.flags |= PKT_FLAG_KEY;
      pkt.stream_index= st->index;
      pkt.data= video_outbuf;
      pkt.size= out_size;

      /* write the compressed frame in the media file */
      ret = av_write_frame(oc, &pkt);
    } 
    else 
    {
      ret = 0;
    }
  }

  if (ret != 0) 
  {
    throw std::exception("Error while writing video frame.");
  }
}


void
VideoWriterFfmpeg::open_video(AVFormatContext *oc, AVStream *st)
{
  AVCodec *codec;
  AVCodecContext *c;

  c = st->codec;

  /* find the video encoder */
  codec = avcodec_find_encoder(c->codec_id);
  if (!codec) 
  {
    throw std::exception("codec not found\n");
  }

  /* open the codec */
  if (avcodec_open(c, codec) < 0) 
  {
    throw std::exception("Could not open codec\n");
  }

  video_outbuf = NULL;
  if (!(oc->oformat->flags & AVFMT_RAWPICTURE))
  {
    /* allocate output buffer */
    /* XXX: API change will be done */
    video_outbuf_size = 200000;
    video_outbuf = (uint8_t *)malloc(video_outbuf_size);
  }

  /* allocate the encoded raw picture */
  picture = alloc_picture(c->pix_fmt, c->width, c->height);
  if (!picture) 
  {
    throw std::exception("Could not allocate picture");
  }
}

AVFrame *
VideoWriterFfmpeg::alloc_picture(int pix_fmt, int width, int height)
{
  AVFrame *picture;
  uint8_t *picture_buf;
  int size;

  picture = avcodec_alloc_frame();
  if (!picture)
  {
    return NULL;
  }

  size = avpicture_get_size(pix_fmt, width, height);
  picture_buf = (uint8_t *)malloc(size);
  if (!picture_buf)
  {
    av_free(picture);
    return NULL;
  }
  avpicture_fill((AVPicture *)picture, picture_buf,
    pix_fmt, width, height);
  return picture;
}

void
VideoWriterFfmpeg::setOwnerWindow(Uint32 id)
{
  //mOwner = (HWND)id;
}

void
VideoWriterFfmpeg::setAverageFrameDuration(double avg)
{
  if (isCodecInitialized())
  {
    PvUtil::exitError("This operation must be done before opening the file.");
  }
  VideoWriter::setAverageFrameDuration(avg);
}

}; // ait
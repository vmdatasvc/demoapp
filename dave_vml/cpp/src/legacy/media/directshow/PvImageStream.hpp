/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef PvImageStream_HPP
#define PvImageStream_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <boost/thread.hpp>

// AIT INCLUDES
//

#include <String.hpp>
#include <ait/Image.hpp>
#include <ait/Vector3.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

#include "PvImageStream_fwd.hpp"
#include "PvImageSourceFilter_fwd.hpp"

namespace ait 
{


/**
 * Stream to write images. This is part of the DirectShow filter. Use through the
 * VideoWriter class.
 */
class PvImageStream : public CSourceStream
{
  
public:
  
  PvImageStream(HRESULT *phr, PvImageSourceFilter *pParent, LPCWSTR pPinName);
  ~PvImageStream();
  
  // plots a ball into the supplied video frame
  HRESULT FillBuffer(IMediaSample *pms);
  
  // Ask for buffers of the size appropriate to the agreed media type
  HRESULT DecideBufferSize(IMemAllocator *pIMemAlloc,
    ALLOCATOR_PROPERTIES *pProperties);
  
  // Set the agreed media type, and set up the necessary ball parameters
  HRESULT SetMediaType(const CMediaType *pMediaType);
  
  // Because we calculate the ball there is no reason why we
  // can't calculate it in any one of a set of formats...
  HRESULT CheckMediaType(const CMediaType *pMediaType);
  HRESULT GetMediaType(int iPosition, CMediaType *pmt);
  
  // Resets the stream time to zero
  HRESULT OnThreadCreate(void);
  
  // Quality control notifications sent to us
  STDMETHODIMP Notify(IBaseFilter * pSender, Quality q);
  
  /**
   * This image will be written by the stream thread in the
   * FillBuffer() callback.
   */
  void write(const Image32& img);

  /**
   * Send the end of file marker to the stream.
   */
  void writeEndOfFile();

  /**
   * Repeat the last frame.
   */
  void writePreviousFrame();

  void setImageSize(const Vector2i& size);
  void setFramesPerSecond(double fps);

private:

  bool isFillBufferDone();
  void notifyImageAvailable(boost::mutex::scoped_lock& lk);


  Vector2i mImageSize;

  Image32 *mpImage;                // Image to write up stream.
  
  /**
   * This condition indicates when the image is available
   * to write it on the stream.
   */
  boost::condition mCondImageAvailableToWrite;

  /**
   * Flag that accompanies the condition variable
   */
  bool mIsImageAvailableToWrite;

  /**
   * This condition indicates when the image has been written.
   */
  boost::condition mCondImageWritten;

  /**
   * Flag that accompanies the condition variable
   */
  bool mIsImageWritten;

  /**
   * This variable indicates if the frame has been updated.
   */
  boost::condition mCondFrameUpdated;

  /**
   * Mutex used to lock the frame contents.
   */
  boost::mutex mMutex;

  /**
   * End of stream flag.
   */
  bool mIsEndOfStream;

  /**
   * Number of frames to repeat.
   */
  int mNumFramesToSkip;

  /**
   * The number of frames per second.
   */
  double mFramesPerSecond;

  /**
   * Number of frames.
   */
  Int64 mNumFrames;

}; // PvImageStream


}; // namespace ait

#endif // PvImageStream_HPP


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#pragma warning(disable:4786)

#ifdef WIN32

#include <winstrutil.hpp>

#include "MediaCapture.hpp"
#include "MediaFilterSync.hpp"

using namespace std;

#ifdef HIGH_PERF_LOG
#include "HighPerfLog.hpp"
extern ait::HighPerfLog gLog;
ait::HighPerfLog gLog;
#endif

namespace ait
{

class MediaCaptureProperty : public ImageAcquireProperty
{
public:

  MediaCaptureProperty(MediaCapture *pMC0) : pMC(pMC0) {};
  virtual ~MediaCaptureProperty() {}

  virtual ImageAcquireProperty *clone() { return new MediaCaptureProperty(*this); }

  void setToDevice();
  void getFromDevice();

protected:
  MediaCapture *pMC;
};

void MediaCaptureProperty::setToDevice()
{
  pMC->setDirectMediaParam(*this); 
}

void MediaCaptureProperty::getFromDevice() 
{ 
  pMC->getDirectMediaParam(*this); 
}


static char *getErrText(HRESULT hr)
{
  char *buf = new char [256];
  char hex[20];
  sprintf(hex,"(0x%8X)",hr);

  AMGetErrorText(hr,buf,256);
  strcat(buf,hex);

  return buf;
}


MediaCapture::MediaCapture()
{
  pVSC = NULL;
  pVPA = NULL;
  pCC = NULL;
  pParams = NULL;
	
	mRenderMethod = BLOCKING_DOUBLE;
}


MediaCapture::~MediaCapture()
{

  SAFE_RELEASE(pVSC);
  SAFE_RELEASE(pVPA);
  SAFE_RELEASE(pCC);
  if (pParams) delete pParams;

  for(int i = 0; i < rgpmVideoMenu.size(); i++) 
  {
    if(rgpmVideoMenu[i])
    {
      SAFE_RELEASE(rgpmVideoMenu[i]);
    }
  }
  rgpmVideoMenu.clear();

}

static CameraControlProperty mapCameraControlProperty(ImageAcquireProperty::Code in)
{
  switch (in)
  {
  case ImageAcquireProperty::PAN: return CameraControl_Pan;
  case ImageAcquireProperty::TILT: return CameraControl_Tilt;
  case ImageAcquireProperty::ROLL: return CameraControl_Roll;
  case ImageAcquireProperty::ZOOM: return CameraControl_Zoom;
  case ImageAcquireProperty::EXPOSURE: return CameraControl_Exposure;
  case ImageAcquireProperty::IRIS: return CameraControl_Iris;
  case ImageAcquireProperty::FOCUS: return CameraControl_Focus;
  }

  PvUtil::exitError("Invalid Property Code!");

  return CameraControl_Pan; // just to avoid warning.
}

static VideoProcAmpProperty mapVideoProcAmpProperty(ImageAcquireProperty::Code in)
{
  switch (in)
  {
  case ImageAcquireProperty::BRIGHTNESS: return VideoProcAmp_Brightness;
  case ImageAcquireProperty::CONTRAST: return VideoProcAmp_Contrast;
  case ImageAcquireProperty::HUE: return VideoProcAmp_Hue;
  case ImageAcquireProperty::SATURATION: return VideoProcAmp_Saturation;
  case ImageAcquireProperty::SHARPNESS: return VideoProcAmp_Sharpness;
  case ImageAcquireProperty::GAMMA: return VideoProcAmp_Gamma;
  case ImageAcquireProperty::COLORENABLE: return VideoProcAmp_ColorEnable;
  case ImageAcquireProperty::WHITEBALANCE: return VideoProcAmp_WhiteBalance;
  case ImageAcquireProperty::BACKLIGHTCOMPENSATION: return VideoProcAmp_BacklightCompensation;
  case ImageAcquireProperty::GAIN: return VideoProcAmp_Gain;
  }

  PvUtil::exitError("Invalid Property Code!");

  return VideoProcAmp_Brightness; // just to avoid warning.
}

void MediaCapture::setDirectMediaParam(ImageAcquireProperty& param)
{
  HRESULT hr;

  float val = param.getValuef(false);
  bool automatic = param.getAuto();

  if (param.getCode() == ImageAcquireProperty::IRIS)
  {
    // This parameter is throwing error codes. Have to check that...
    param.setAvailable(false);
    return;
  }
  else if (param.getCode() >= ImageAcquireProperty::FPS && param.getCode() <= ImageAcquireProperty::SOURCEHEIGHT)
  {
    AM_MEDIA_TYPE *pmt;

    if (!pVSC)
    {
      return;
    }

    hr = pVSC->GetFormat(&pmt);

    if (hr != NOERROR) PvUtil::exitError("Error setting camera parameter %s: %s",param.getName().c_str(),getErrText(hr));

    if(pmt->formattype == FORMAT_VideoInfo) 
    {
      VIDEOINFOHEADER *pvi = (VIDEOINFOHEADER *)pmt->pbFormat;
      switch (param.getCode())
      {
      case ImageAcquireProperty::FPS:
        pvi->AvgTimePerFrame = (LONGLONG)(10000000 / val);
        break;
      case ImageAcquireProperty::SOURCEBITCOUNT:
      case ImageAcquireProperty::SOURCEWIDTH:
      case ImageAcquireProperty::SOURCEHEIGHT:
        // These parameters are set to the device on the initialization only.
        break;
      }
      hr = pVSC->SetFormat(pmt);
      if (hr != NOERROR) PvUtil::exitError("Error setting camera parameter %s: %s",param.getName().c_str(),getErrText(hr));
    }
    DeleteMediaType(pmt);

    return;
  }

  long min,max,steppingDelta,default0,capsFlags;

  if (param.getCode() >= ImageAcquireProperty::BRIGHTNESS && param.getCode() <= ImageAcquireProperty::GAIN)
  {
    if (!pVPA)
    {
      param.setAvailable(false);
      return;
    }

    hr = pVPA->GetRange(mapVideoProcAmpProperty(param.getCode()),&min,&max,&steppingDelta,&default0,&capsFlags);

    if (hr != NOERROR) 
    {
      // The driver might not support querying or changing this property.
      param.setAvailable(false);
      return;
    }

    hr = pVPA->Set(mapVideoProcAmpProperty(param.getCode()),(long)val, 
                   automatic ? VideoProcAmp_Flags_Auto : VideoProcAmp_Flags_Manual);

    if (hr != NOERROR) 
    {
      // The driver might not support querying or changing this property.
      param.setAvailable(false);
      return;
    }
  }
  else if (param.getCode() >= ImageAcquireProperty::PAN && param.getCode() <= ImageAcquireProperty::FOCUS)
  {
    if (!pCC)
    {
      param.setAvailable(false);
      return;
    }
    hr = pCC->GetRange(mapCameraControlProperty(param.getCode()),&min,&max,&steppingDelta,&default0,&capsFlags);

    if (hr != NOERROR) 
    {
      // The driver might not support querying or changing this property.
      param.setAvailable(false);
      return;
    }

    hr = pCC->Set(mapCameraControlProperty(param.getCode()),(long)val, 
                   automatic ? CameraControl_Flags_Auto : CameraControl_Flags_Manual);

    if (hr != NOERROR) 
    {
      // The driver might not support querying or changing this property.
      param.setAvailable(false);
      return;
    }
  }
  else
  {
    param.setAvailable(false);
  }
}

void MediaCapture::getDirectMediaParam(ImageAcquireProperty& param)
{
  HRESULT hr;

  if (param.getCode() == ImageAcquireProperty::IRIS)
  {
    // This parameter is throwing error codes. Have to check that...
    param.setAvailable(false);
    return;
  }
  else if (param.getCode() >= ImageAcquireProperty::FPS && param.getCode() <= ImageAcquireProperty::SOURCEHEIGHT)
  {
    AM_MEDIA_TYPE *pmt;

    if (!pVSC)
    {
      param.setAuto(false);
      param.setAvailable(true);
      param.setInteractive(false);
      return;
    }

    hr = pVSC->GetFormat(&pmt);
    if (hr != NOERROR) PvUtil::exitError("Error getting camera parameter %s: %s",param.getName().c_str(),getErrText(hr));

    float fps;

    if(pmt->formattype == FORMAT_VideoInfo) 
    {
      VIDEOINFOHEADER *pvi = (VIDEOINFOHEADER *)pmt->pbFormat;

      switch (param.getCode())
      {
      case ImageAcquireProperty::FPS:
        fps = (float)pvi->AvgTimePerFrame/10000000;
        fps = 1.0f/fps;
        param.setValuef(fps,false);
        param.setDefault(30);
        param.setRange(0,300,1);
        break;
      case ImageAcquireProperty::SOURCEBITCOUNT:
        param.setValuef(pvi->bmiHeader.biBitCount,false);
        param.setDefault(32);
        param.setRange(8,32,1);
        break;
      case ImageAcquireProperty::SOURCEWIDTH:
        param.setValuef(pvi->bmiHeader.biWidth,false);
        param.setDefault(320);
        param.setRange(160,4000,1);
        break;
      case ImageAcquireProperty::SOURCEHEIGHT:
        param.setValuef(pvi->bmiHeader.biHeight,false);
        param.setDefault(240);
        param.setRange(120,3000,1);
        break;
      }
    }

    DeleteMediaType(pmt);

    param.setAuto(false);
    param.setAvailable(true);
    param.setInteractive(false);

    return;
  }

  long min,max,steppingDelta,default0,capsFlags,value;

  if (param.getCode() >= ImageAcquireProperty::BRIGHTNESS && param.getCode() <= ImageAcquireProperty::GAIN)
  {
    if (!pVPA)
    {
      param.setAvailable(false);
      return;
    }

    hr = pVPA->GetRange(mapVideoProcAmpProperty(param.getCode()),&min,&max,&steppingDelta,&default0,&capsFlags);

    if (hr == 0x80070490) {
      //PVMSG("MediaCapture::Property %s not supported for this camera\n",param.getName().c_str());
      param.setAvailable(false);
      return;
    }

    if (hr != NOERROR) PvUtil::exitError("Error getting camera parameter %s: %s",param.getName().c_str(),getErrText(hr));

    hr = pVPA->Get(mapVideoProcAmpProperty(param.getCode()),&value,&capsFlags);

    if (hr != NOERROR) PvUtil::exitError("Error getting camera parameter %s: %s",param.getName().c_str(),getErrText(hr));
  }
  else if (param.getCode() >= ImageAcquireProperty::PAN && param.getCode() <= ImageAcquireProperty::FOCUS)
  {
    if (!pCC)
    {
      param.setAvailable(false);
      return;
    }
    hr = pCC->GetRange(mapCameraControlProperty(param.getCode()),&min,&max,&steppingDelta,&default0,&capsFlags);

    if (hr == 0x80070490) {
      //PVMSG("MediaCapture::Property %s not supported for this camera\n",param.getName().c_str());
      param.setAvailable(false);
      return;
    }

    if (hr != NOERROR) PvUtil::exitError("Error getting camera parameter %s: %s",param.getName().c_str(),getErrText(hr));

    hr = pCC->Get(mapCameraControlProperty(param.getCode()),&value,&capsFlags);

    if (hr != NOERROR) PvUtil::exitError("Error getting camera parameter %s: %s",param.getName().c_str(),getErrText(hr));
  }
  else
  {
    param.setAvailable(false);
    return;
  }

  param.setAvailable(true);
  param.setRange(min,max,steppingDelta);
  param.setAuto(capsFlags & VideoProcAmp_Flags_Auto);
  param.setDefault(default0);
  param.setValuef(value,false);
  param.setInteractive(true);
}

HRESULT MediaCapture::initCaptureDevice(const string& deviceName, int deviceIndex, const std::string& url)
{
  
  HRESULT hr = S_OK;
  
	initRenderer(mRenderMethod);
  
  mpRenderer->setInternalImageFormat(Image32::PV_BGRA);

  mUseExternalControl = false;

  MediaControllerDX *pMediaController = dynamic_cast<MediaControllerDX *>(mpMediaController.get());

  if (!pMediaController)
  {
    pMediaController = new MediaControllerDX();
    mpMediaController.reset(pMediaController);

    // Get the interface for DirectShow's GraphBuilder
    hr = CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC_SERVER, IID_IGraphBuilder, 
      (void **)&pMediaController->pGB);
    if (FAILED(hr)) PvUtil::exitError("Initializing Capture Device");

    // Get the DirectShow media controller interface.
    hr = pMediaController->pGB->QueryInterface(IID_IMediaControl,  
      (void **)&pMediaController->pMC);
    if (FAILED(hr)) PvUtil::exitError("Initializing Capture Device");

    // Get the interface for DirectShow's CaptureGraphBuilder
    hr = CoCreateInstance(CLSID_CaptureGraphBuilder2, NULL, CLSCTX_INPROC_SERVER, IID_ICaptureGraphBuilder2, 
      (void **)&pMediaController->pCGB);
    if (FAILED(hr)) PvUtil::exitError("Initializing Capture Device");

    // Attach the filter graph to the capture graph
    hr = pMediaController->pCGB->SetFiltergraph(pMediaController->pGB);
    if (FAILED(hr)) PvUtil::exitError("Initializing Capture Device: %s",getErrText(hr));
  }

  IGraphBuilder *pGB = pMediaController->pGB;
  ICaptureGraphBuilder2 *pCGB = pMediaController->pCGB;

  // Get a pointer to the IBaseFilter on the TextureRenderer, add it to graph
  hr = pGB->AddFilter(mpRenderer, L"TEXTURERENDERER");
  if (FAILED(hr)) PvUtil::exitError("Initializing Capture Device");

  // Now initialize the property bag
  pParams = new ImageAcquirePropertyBag(settingsPath,ImageAcquirePropertyPtr(new MediaCaptureProperty(this)));

  if (url != "")
  {
    BSTR fname1 = stringToBstr(url);
    hr = pGB->RenderFile(fname1,NULL);
    SysFreeString(fname1);
    if (FAILED(hr)) PvUtil::exitError("Failed rendering URL: %s",url.c_str());
  }
  else
  {
    // Set source filter
    IBaseFilter *pSrcFilter=NULL;

    std::string uniqueDeviceName;
    hr = findCaptureDevice(&pSrcFilter, uniqueDeviceName, deviceName, deviceIndex);
    if (FAILED(hr)) PvUtil::exitError("Initializing Capture Device\n(Probably it is locked by another application)");

    hr = pGB->AddFilter(pSrcFilter, L"Video Capture");
    if (FAILED(hr)) PvUtil::exitError("Initializing Capture Device: %s",getErrText(hr));

    // Interface for setting the media types.
    hr = pCGB->FindInterface(&PIN_CATEGORY_CAPTURE,
      &MEDIATYPE_Video, pSrcFilter,
      IID_IAMStreamConfig, (void **)&pVSC);
    if (FAILED(hr)) PvUtil::exitError("Initializing Capture Device: %s",getErrText(hr));

    int desiredBPP = (int)pParams->get(ImageAcquireProperty::SOURCEBITCOUNT)->getValuef(false);
    int desiredW = (int)pParams->get(ImageAcquireProperty::SOURCEWIDTH)->getValuef(false);
    int desiredH = (int)pParams->get(ImageAcquireProperty::SOURCEHEIGHT)->getValuef(false);

    // Check all possible formats and try to match the one on the properties.
    {
      int i, numCaps, sizeCap;

      pVSC->GetNumberOfCapabilities(&numCaps,&sizeCap);

      VIDEO_STREAM_CONFIG_CAPS *pSCC = (VIDEO_STREAM_CONFIG_CAPS *)new BYTE [sizeCap];
      AM_MEDIA_TYPE *pmt;

#ifdef DEBUG
      string fileName("DirectShow Device ");
      fileName += aitSprintf("%i",deviceIndex);
      fileName +=" Valid Formats.txt";
      FILE *fp = fopen(fileName.c_str(), "w");
#else
      FILE *fp = NULL;
#endif

      std::string formats = "";

      bool formatSet = false;

      for (i = 0; i < numCaps; i++)
      {
        pVSC->GetStreamCaps(i,&pmt,(BYTE *)pSCC);

        if (pmt->formattype == FORMAT_VideoInfo)
        {
          VIDEOINFOHEADER *pvi = (VIDEOINFOHEADER *)pmt->pbFormat;

          // This only works for 3rd party formats
          //std::string fourCC("xxxx");
          //strncpy(&(fourCC[0]),(char*)&(pmt->subtype),4);

          std::string strfmt = aitSprintf("(%i-%i/%i)x(%i-%i/%i)-%ibpp",
            pSCC->MinOutputSize.cx,pSCC->MaxOutputSize.cx,pSCC->OutputGranularityX,
            pSCC->MinOutputSize.cy,pSCC->MaxOutputSize.cy,pSCC->OutputGranularityY,
            pvi->bmiHeader.biBitCount);

          if (fp)
          {
            fprintf(fp,"%s\n",strfmt.c_str());
          }

          if (formats != "")
          {
            formats += ";";
          }

          formats += strfmt;

          if (desiredBPP == pvi->bmiHeader.biBitCount && 
            desiredW >= pSCC->MinOutputSize.cx && desiredW <= pSCC->MaxOutputSize.cx &&
            (pSCC->OutputGranularityX == 0 || (desiredW-pSCC->MinOutputSize.cx)%pSCC->OutputGranularityX == 0) &&
            desiredH >= pSCC->MinOutputSize.cy && desiredH <= pSCC->MaxOutputSize.cy &&
            (pSCC->OutputGranularityY == 0 || (desiredH-pSCC->MinOutputSize.cy)%pSCC->OutputGranularityY == 0) &&
            !formatSet)
          {
            pvi->bmiHeader.biWidth = desiredW;
            pvi->bmiHeader.biHeight = desiredH;

            // Set the sample size and image size.
            // (Round the image width up to a DWORD boundary.)
            pmt->lSampleSize = pvi->bmiHeader.biSizeImage = 
              ((desiredW + 3) & ~3) * desiredH * (pvi->bmiHeader.biBitCount/8); 

            hr = pVSC->SetFormat(pmt);
            if (hr != NOERROR) 
            {
              if (fp) fclose(fp);
              PvUtil::exitError("Error setting source image format parameters (%s)",getErrText(hr));
            }
            formatSet = true;
          }
        }

        DeleteMediaType(pmt);

      }

      if (fp) fclose(fp);

      Settings set(settingsPath);
      set.setString("videoFormats",formats);

      delete [] pSCC;
    }

    // We must set the FPS before rendering. Just querying the variable will set the default values.
    pParams->get(ImageAcquireProperty::FPS)->getValuef();

    /*
    pParams->get(ImageAcquireProperty::SOURCEBITCOUNT)->getValuef();
    pParams->get(ImageAcquireProperty::SOURCEWIDTH)->getValuef();
    pParams->get(ImageAcquireProperty::SOURCEHEIGHT)->getValuef();
    */

    // Have the graph construct the appropriate graph automatically
    hr = pCGB->RenderStream (	&PIN_CATEGORY_CAPTURE, 
      &MEDIATYPE_Video,
      pSrcFilter, 
      NULL, 
      mpRenderer);
    if (FAILED(hr)) PvUtil::exitError("Initializing Capture Device: %s\n(Probably the camera is locked by another application)",getErrText(hr));

    // Set Camera Parameters
    hr = pCGB->FindInterface(&PIN_CATEGORY_CAPTURE,
      &MEDIATYPE_Video,
      pSrcFilter, IID_IAMVideoProcAmp, (void **)&pVPA);
    if (FAILED(hr))
    {
      //PvUtil::exitError("Initializing Capture Device: %s",getErrText(hr));
      //PVMSG("IAMVideoProcAmp not supported: %s\n",getErrText(hr));
    }

    hr = pCGB->FindInterface(&PIN_CATEGORY_CAPTURE,
      &MEDIATYPE_Video,
      pSrcFilter, IID_IAMCameraControl, (void **)&pCC);
    if (FAILED(hr))
    {
      //PvUtil::exitError("Initializing Capture Device: %s",getErrText(hr));
      //PVMSG("IAMCameraControl not supported: %s\n",getErrText(hr));
    }

    // Release pSrc Filter
    SAFE_RELEASE(pSrcFilter);
  }

  // QueryInterface for DirectShow interfaces
  pGB->QueryInterface(IID_IMediaEventEx,  (void **)&pME);
  pGB->QueryInterface(IID_IMediaSeeking,  (void **)&pMS);
  pGB->QueryInterface(IID_IMediaPosition, (void **)&pMP);
  
  // Query for video interfaces, which may not be relevant for audio files
  pGB->QueryInterface(IID_IBasicVideo, (void **)&pBV);
  
  // Query for audio interfaces, which may not be relevant for video-only files
  pGB->QueryInterface(IID_IBasicAudio, (void **)&pBA);

  // Have the graph signal event via window callbacks
  //hr = pME->SetNotifyWindow((OAHWND)ghApp, WM_GRAPHNOTIFY, 0);
  //if (FAILED(hr)) PvUtil::exitError("Initializing Capture Device: %s",getErrText(hr));
  
  // Run the graph to play the media file
#ifdef HIGH_PERF_LOG
  gLog.write(aitSprintf("Running Graph for %s:%i",deviceName.c_str(),deviceIndex));
#endif

  mpMediaController->stop();
  g_psCurrent=Stopped;
  
  return S_OK;
}

HRESULT MediaCapture::findCaptureDevice(IBaseFilter ** ppSrcFilter, 
                                        string& uniqueDeviceName,
                                        const string& deviceName, 
                                        int deviceIndex)
{
  HRESULT hr;
  IBaseFilter *pSrc = NULL;
  
  //    IMoniker *pMoniker =NULL;
  ULONG cFetched;
  UINT    uIndex = 0;
  
  // Create the system device enumerator
  ICreateDevEnum *pDevEnum =NULL;
  
  hr = CoCreateInstance (CLSID_SystemDeviceEnum, NULL, CLSCTX_INPROC,
    IID_ICreateDevEnum, (void ** ) &pDevEnum);
  if (FAILED(hr))
  {
    return hr;
  }
  
  // Create an enumerator for the video capture devices
  IEnumMoniker *pClassEnum = NULL;
  
  hr = pDevEnum->CreateClassEnumerator (CLSID_VideoInputDeviceCategory, &pClassEnum, 0);
  if (FAILED(hr))
  {
    return hr;
  }
  
  // If there are no enumerators for the requested type, then 
  // CreateClassEnumerator will succeed, but pClassEnum will be NULL.
  if (pClassEnum == NULL)
  {
    return E_FAIL;
  }
  
  
  pClassEnum->Reset();
  
  IMoniker *pM=NULL;
  
  uIndex = 0; // Reset Index to 0
  int iNumVCapDevices=0; // Reset Num of Devices to 0
  
  rgpmVideoMenu.clear();

  *ppSrcFilter = NULL;
  int curDeviceIndex = 0;
  
  while(hr = pClassEnum->Next(1,&pM,&cFetched), hr==S_OK)
  {
    IPropertyBag *pBag;

    VARIANT var;

    hr = pM->BindToStorage(0,0,IID_IPropertyBag, (void **)&pBag);
    if(SUCCEEDED(hr))
    {
      VariantInit(&var);
      hr = pBag->Read(L"FriendlyName",&var, NULL);
      if (hr == NOERROR)
      {
        rgpmVideoMenu.push_back(pM);
        pM->AddRef();
      }
      pBag->Release();

      string buf = ait::bstrToString(var.bstrVal);
    
      if (deviceName == "NONE" || deviceName == buf)
      { 
        if (deviceIndex == curDeviceIndex)
        {
          hr = pM->BindToObject(0,0,IID_IBaseFilter, (void **)&pSrc);
          if(SUCCEEDED(hr))
          {
            *ppSrcFilter = pSrc;
            
            // Get the unique name.
            BSTR bstr;
            hr = pM->GetDisplayName(0,0,&bstr);
            int len = SysStringLen(bstr);
            uniqueDeviceName = ait::bstrToString(bstr);
            CoTaskMemFree(bstr);
          }
        }
        else
        {
          curDeviceIndex++;
        }
      }    
    }
    
    pM->Release();
    uIndex++;

    if (*ppSrcFilter) break;
  }
  
  
  pClassEnum->Release();
  pDevEnum->Release();
  iNumVCapDevices = uIndex;
  
  return hr;
}

} // namespace ait

#endif

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VideoWriterDS_fwd_HPP
#define VideoWriterDS_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class VideoWriterDS;
typedef boost::shared_ptr<VideoWriterDS> VideoWriterDSPtr;

}; // namespace ait

#endif // VideoWriterDS_fwd_HPP


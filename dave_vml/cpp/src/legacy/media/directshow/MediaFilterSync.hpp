/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef MediaFilterSync_HPP
#define MediaFilterSync_HPP

#include <boost/thread/condition.hpp>
#include <boost/thread/thread.hpp>

#include <streams.h>
#include "DXUtil.h"
#include <MediaFrameInfo.hpp>
#include <ait/Image.hpp>
#include <ait/Image_fwd.hpp>
#include <ait/Vector3.hpp>
#include <vector>
#include <Benchmark.hpp>

#include "MediaFilterBase.hpp"

namespace ait
{

/**
 * Base class for the AI renderer filters that transform
 * the samples from DirectShow into PvImages.
 */
class MediaFilterSync : public MediaFilterBase
{
public:

  MediaFilterSync(LPUNKNOWN pUnk,HRESULT *phr);

  virtual ~MediaFilterSync();

  /**
   * Get a new frame. This call is made from the main thread, and takes care of 
   * synchronization.
   */
  virtual bool updateFrame(MediaFrameInfoPtr pFrameInfo);

  /**
   * Set the next frame to be updated.
   */
  void pushReadBuffer(MediaFrameInfoPtr pFrameInfo);

	/**
	* Specifies how many consecutive frames should be grabbed with one call to grabFrame()
	*/
	void setGrabNumberOfConsecutiveFrames(int num) {mGrabNumberOfConsecutiveFrames = num;};

protected:

  /**
   * A sample has been delivered. Copy it to the texture.
   */
  virtual HRESULT DoRenderSample(IMediaSample *pMediaSample);

private:

  /**
   * This variable indicates if the frame has been updated.
   */
  boost::condition mCondFrameUpdated;

  /**
   * Mutex used to lock the frame contents.
   */
  boost::mutex mMutex;


	bool framesReadyForPickup;
	bool emptySlotsReadyForStorage;
	bool buffersInitialized;
	double globalStartTime;


  /**
   * Buffers used by the rendering filter.
   */
  std::vector<MediaFrameInfoPtr> mEmptyBuffers;
	std::vector<MediaFrameInfoPtr> mCurrentBuffers;
	std::vector<MediaFrameInfoPtr> mCurrentBuffersCopy;
	std::vector<MediaFrameInfoPtr> mCurrentBuffersCopy2;

  /**
   * Buffers that are filled.
   */
  std::vector<MediaFrameInfoPtr> mFrames;

	int mGrabNumberOfConsecutiveFrames;

	Benchmark mBenchmark;

};

} // namespace ait

#endif // MediaFilterSync

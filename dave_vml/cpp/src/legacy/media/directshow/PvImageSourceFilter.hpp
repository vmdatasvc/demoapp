/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef PvImageSourceFilter_HPP
#define PvImageSourceFilter_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

#include <String.hpp>
#include <ait/Image.hpp>

// LOCAL INCLUDES
//

// FORWARD REFERENCES
//

#include "PvImageSourceFilter_fwd.hpp"
#include "PvImageStream_fwd.hpp" 

#ifdef _MSC_VER
#pragma warning(disable:4275) // non dll-interface class 'CSource' used as base for dll-interface class
#endif

namespace ait 
{


/**
 * DirectShow Filter used to generate images.
 * Based on the Bouncing Ball example.
 */
class /*__declspec(dllexport)*/ PvImageSourceFilter : public CSource
{
public:

    // Fake out any COM ref counting
  //
  STDMETHODIMP_(ULONG) AddRef() { return 2; }
  STDMETHODIMP_(ULONG) Release() { return 1; }

  // Fake out any COM QI'ing
  //
  STDMETHODIMP QueryInterface(REFIID riid, void ** ppv);


  // The only allowed way to create this filter!
  static CUnknown * WINAPI CreateInstance(LPUNKNOWN lpunk, HRESULT *phr);

  //static PvImageSourceFilter *NoDllCreateInstance(LPUNKNOWN lpunk, HRESULT *phr);

  /**
   * This will write one image into the output stream.
   * This call will be forwarded to the stream, and it is
   * invoked from another thread.
   */
  void write(const Image32& img);

  /**
   * Send the end of file marker to the stream.
   */
  void writeEndOfFile();

  /**
   * Repeat the last frame.
   */
  void writePreviousFrame();

  void setImageSize(const Vector2i& size);
  void setFramesPerSecond(double fps);

private:

  /**
   * Get a pointer to the stream.
   */
  PvImageStream *getStream();

private:

  // Don't use this directly.
  PvImageSourceFilter(LPUNKNOWN lpunk, HRESULT *phr);

};


}; // namespace ait

#endif // PvImageSourceFilter_HPP


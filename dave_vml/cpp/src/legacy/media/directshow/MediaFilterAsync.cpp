/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include "MediaFilterAsync.hpp"

#include <strstream>

#ifdef HIGH_PERF_LOG
#include "HighPerfLog.hpp"
extern ait::HighPerfLog gLog;
#endif

//-----------------------------------------------------------------------------
// Define GUID for this filter
//-----------------------------------------------------------------------------
struct __declspec(uuid("{eb71fdef-fa14-47c1-83db-f3aa0eaae097}")) CLSID_MediaFilterAsync;

namespace ait
{

MediaFilterAsync::MediaFilterAsync( LPUNKNOWN pUnk, HRESULT *phr )
: MediaFilterBase(__uuidof(CLSID_MediaFilterAsync),pUnk,phr),
  mpFrameImage(new Image32())

{
  // Store and AddRef the texture for our use.
  *phr = S_OK;
  mNewFrame = false;
  mMediaFrameInfo.pRGBImage = mpFrameImage;
}

MediaFilterAsync::~MediaFilterAsync()
{
}


HRESULT MediaFilterAsync::DoRenderSample( IMediaSample * pSample )
{
  boost::mutex::scoped_lock lk(mFrameMutex);

  copyFrame(pSample, &mMediaFrameInfo);
  mNewFrame = true;

  return S_OK;
}

bool 
MediaFilterAsync::updateFrame(MediaFrameInfoPtr pFrameInfo)
{
  boost::mutex::scoped_lock lk(mFrameMutex);

  if (mNewFrame)
  {
    // Don't overwrite the pointer
    Image32Ptr pImg = pFrameInfo->pRGBImage;
    // Copy all other fields of the structure.
    *pFrameInfo = mMediaFrameInfo;
    // Copy the image pixels
    if (pImg.get())
    {
      *pImg = *mpFrameImage;
      pFrameInfo->pRGBImage = pImg;
    }
    mNewFrame = false;
    return true;
  }

  return false;
}

} // namespace ait

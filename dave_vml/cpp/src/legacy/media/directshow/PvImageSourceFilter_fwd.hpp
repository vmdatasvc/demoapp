/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef PvImageSourceFilter_fwd_HPP
#define PvImageSourceFilter_fwd_HPP

#include <boost/shared_ptr.hpp>

// {D05C5ABC-355F-47e7-BB03-D3653245B924}
DEFINE_GUID(CLSID_PvImageSourceFilter,
0xd05c5abc, 0x355f, 0x47e7, 0xbb, 0x3, 0xd3, 0x65, 0x32, 0x45, 0xb9, 0x24);

namespace ait
{

class PvImageSourceFilter;
typedef boost::shared_ptr<PvImageSourceFilter> PvImageSourceFilterPtr;

}; // namespace ait

#endif // PvImageSourceFilter_fwd_HPP


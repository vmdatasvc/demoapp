/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include "MediaFilterSync.hpp"

#include <strstream>

#ifdef HIGH_PERF_LOG
#include "HighPerfLog.hpp"
extern ait::HighPerfLog gLog;
#endif

//#define ENABLE_DEBUG_INFO

//-----------------------------------------------------------------------------
// Define GUID for this filter
//-----------------------------------------------------------------------------
struct __declspec(uuid("{e824a06c-0988-4721-807a-96db758dc9cb}")) CLSID_MediaFilterSync;


namespace ait
{

MediaFilterSync::MediaFilterSync( LPUNKNOWN pUnk, HRESULT *phr )
: MediaFilterBase(__uuidof(CLSID_MediaFilterSync),pUnk,phr)
{
  // Store and AddRef the texture for our use.
  *phr = S_OK;

	mGrabNumberOfConsecutiveFrames = 1;
	
	framesReadyForPickup = false;
	emptySlotsReadyForStorage = false;
	buffersInitialized = false;
	globalStartTime = PvUtil::time();

	mBenchmark.init("VisionMods/Camera0/ImageAcquireMod","grabFrame");
}


MediaFilterSync::~MediaFilterSync()
{
}

HRESULT MediaFilterSync::DoRenderSample( IMediaSample * pSample )
{

#ifdef HIGH_PERF_LOG
	REFERENCE_TIME sampleStartTime;
	REFERENCE_TIME sampleEndTime;

	pSample->GetTime(&sampleStartTime, &sampleEndTime);

	std::strstream s;
	s << "Received sample from " << (void*)this << " - " << (float)sampleStartTime/10000 << " to " << (float)sampleEndTime/10000 << std::ends;
	gLog.write(s.str());
#endif

	boost::mutex::scoped_lock lk(mMutex);

	/*
			only put frame into empty slot if numFrames slots 
			are not yet filled and ready for pickup
	*/

	if (emptySlotsReadyForStorage)
	{
		if(!mEmptyBuffers.empty())
		{
		copyFrame(pSample,mEmptyBuffers.begin()->get());
		mCurrentBuffers.push_back(mEmptyBuffers.front());
		mEmptyBuffers.erase(mEmptyBuffers.begin());
		}
		else
		{			
			REFERENCE_TIME sampleStartTime;
			REFERENCE_TIME sampleEndTime;

			pSample->GetTime(&sampleStartTime, &sampleEndTime);
			
			std::strstream s;			
			s << "!Dropped sample from " << (void*)this << " - " << (float)sampleStartTime/10000 << " to " << (float)sampleEndTime/10000 << std::ends;
			PVMSG(s.str());

			//PvUtil::exitError("Could not store frame: no empty slot available!");
		}


		if (mEmptyBuffers.empty()) 
		{
			framesReadyForPickup = true;
			emptySlotsReadyForStorage = false;
			mCondFrameUpdated.notify_one();
		}		
	}
	else
	{
		// Frame lost!		
		if(mCurrentBuffers.size() == mGrabNumberOfConsecutiveFrames)
		{
			//move oldest frame to start of current frame list
			MediaFrameInfoPtr pFrameInfo = mCurrentBuffers.front();
			mCurrentBuffers.erase(mCurrentBuffers.begin());
			mCurrentBuffers.push_back(pFrameInfo);
			
			//copy current frame
			copyFrame(pSample,mCurrentBuffers.back().get());		
		}
	}

	return S_OK;
}

bool 
MediaFilterSync::updateFrame(MediaFrameInfoPtr pFrameInfo)
{
	boost::mutex::scoped_lock lk(mMutex);

	//mBenchmark.beginSample();

	/*
			Only continue if numFrames frame slots are filled with frames and are ready for pickup
			(meaning the Renderer got assigned new empty slots where to put new frames in the meanwhile
			while we pick up one frame at the time from the filled slots).
	*/
	
	if(!framesReadyForPickup || mCurrentBuffersCopy.size()<=0) 
	{
		return false;
	}

	bool exist=false;
	
	MediaFrameInfoPtr pFrameInfo2 = pFrameInfo;
	std::vector<MediaFrameInfoPtr>::iterator itr=mCurrentBuffersCopy2.begin();
	
	for(itr;itr!=mCurrentBuffersCopy2.end();itr++)
	{
		if((*itr).get() == pFrameInfo.get()) 
		{					
			pFrameInfo = mCurrentBuffersCopy.front();
	
			mCurrentBuffersCopy.erase(mCurrentBuffersCopy.begin());//.pop_front();			
			mCurrentBuffersCopy2.erase(itr);

			if(mCurrentBuffersCopy.empty()) framesReadyForPickup=false;

			exist=true;	
			break;
		}
	}

	//mBenchmark.endSample();

	if(!exist)
	{
		char txt[255];
		sprintf(txt, "[%f] Reading inconsistent frame (pFrameInfo = %x)", PvUtil::time()-globalStartTime, pFrameInfo.get());
		PvUtil::exitError(txt);
	}

	return true;	
}

void
MediaFilterSync::pushReadBuffer(MediaFrameInfoPtr pFrameInfo)
{
  boost::mutex::scoped_lock lk(mMutex);	

	/*
			only execute when Renderer has already grabbed numFrames frames
			that can then directly be picked up by updateFrame()
			(otherwise the renderer would start putting frames into the new
			empty frame slots without first filling the first set of numFrames )
	*/	
	while(emptySlotsReadyForStorage && buffersInitialized)
	{
		mCondFrameUpdated.wait(lk);	
	}

	mEmptyBuffers.push_back(pFrameInfo);

	if(mEmptyBuffers.size() == mGrabNumberOfConsecutiveFrames) 
	{
		if(mCurrentBuffers.size() == mGrabNumberOfConsecutiveFrames)
		{
			for(int i=0; i<mGrabNumberOfConsecutiveFrames; i++)
			{
				mCurrentBuffersCopy.push_back(mCurrentBuffers.front());
				mCurrentBuffersCopy2.push_back(mCurrentBuffers.front());
				mCurrentBuffers.erase(mCurrentBuffers.begin());
			}
		}
		emptySlotsReadyForStorage = true;
		buffersInitialized = true;

	}		
}

} // namespace ait

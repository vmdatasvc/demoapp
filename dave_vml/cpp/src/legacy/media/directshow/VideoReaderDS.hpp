/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VideoReaderDS_HPP
#define VideoReaderDS_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <atlbase.h>
#include <ddraw.h>       // DirectDraw interfaces
#include <mmstream.h>    // multimedia stream interfaces
#include <amstream.h>    // DirectShow multimedia stream interfaces
#include <ddstream.h>    // DirectDraw multimedia stream interfaces
#include <vector>


// AIT INCLUDES
//

#include <String.hpp>

// LOCAL INCLUDES
//

#include "VideoReader.hpp"

// FORWARD REFERENCES
//

#include "VideoReaderDS_fwd.hpp"

namespace ait 
{


/**
 * DirectShow implementation of the video reader. Do not instantiate this class, use
 * instead the factory member in the base class. Refer to base class for documentation
 * on inherited members.
 */
class VideoReaderDS : public VideoReader
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  VideoReaderDS();

  /**
   * Destructor
   */
  virtual ~VideoReaderDS();


  //
  // OPERATIONS
  //

public:

  virtual void open(const std::string& fname);
  virtual void close();
  virtual bool readFrame(Image32& img);

private:

  void readSample();
  void copySampleToImage(Image32& img);

  //
  // ACCESS
  //

public:

  virtual Int64 getNumberOfFrames();
  virtual void setCurrentFrameNumber(Int64 frame);
  virtual Int64 getCurrentFrameNumber();
  virtual double getFramesPerSecond();
  virtual Vector2i getBufferSize();
  virtual void nextFrame();

  //
  // INQUIRY
  //

public:


  //
  // ATTRIBUTES
  //

protected:

  /**
   * The multimedia stream.
   */
  CComPtr<IAMMultiMediaStream> mpMMStream;

  /**
   * The video stream.
   */
  CComPtr<IDirectDrawMediaStream> mpPrimaryVideoStream;

  /**
   * The video sample from the video stream.
   */
  CComPtr<IDirectDrawStreamSample> mpStreamSample;

  /**
   * The DirectDraw surface used to render. It will actually point
   * to the Image memory.
   */
  CComPtr<IDirectDrawSurface7> mpSurface;

  /**
   * The current frame number.
   */
  Int64 mFrameNumber;

  /**
   * The last frame number that was read from the stream.
   */
  Int64 mLastFrameNumber;

  /**
   * Save here the frame size.	
   */
  Vector2i mBufferSize;

  /**
   * Frame number of the current sample.
   */
  Int64 mCurrentSampleFrameNumber;

  /**
   * 24-bit RGB sample image data.
   */
  std::vector<Uint8> mSampleImageData;

  /**
   * Number of frames per second on this video.
   */
  double mFramesPerSecond;

};


}; // namespace ait

#endif // VideoReaderDS_HPP


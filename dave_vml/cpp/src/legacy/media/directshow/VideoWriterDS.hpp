/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VideoWriterDS_HPP
#define VideoWriterDS_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>
#include <atlbase.h>
#include <dshow.h>
#include <mtype.h>
#include <wxdebug.h>
#include <reftime.h>
#include <vector>

// AIT INCLUDES
//

#include <String.hpp>


// LOCAL INCLUDES
//

#include "VideoWriter.hpp"

// FORWARD REFERENCES
//

#include "VideoWriterDS_fwd.hpp"
#include "PvImageSourceFilter_fwd.hpp"

namespace ait 
{


/**
 * DirectShow implementation of the video writer.
 * See the base class for documentation.
 */
class VideoWriterDS : public VideoWriter
{
  //
  // LIFETIME
  //

public:

  class SettingsData
  {
  public:
    std::string codecId;
    std::vector<Uint8> data;

    static const std::string magicHeader;

    /**
     * Throws std::exception on reading errors.
     */
    void load(const std::string& fname);

    /**
     * Throws std::exception on writing errors.
     */
    void save(const std::string& fname);

  private:
    /**
     * Close file and throw std::exception on writing errors.
     */
    void write(FILE *fp, const void *data, int dataSize);
  };

  typedef boost::shared_ptr<SettingsData> SettingsDataPtr;

  /**
   * Constructor.
   */
  VideoWriterDS();


  /**
   * Destructor
   */
  virtual ~VideoWriterDS();

  //
  // OPERATIONS
  //

public:

  virtual void open(const std::string& fname);
  virtual void writeFrame(const Image32& img);
  virtual void skipFrame();
  virtual void close();
  virtual bool loadCodecSettings(const std::string& fname, bool showDialogIfNotPresent);
  virtual void saveCodecSettings(const std::string& fname);
  virtual bool showCodecSettingsDialog(const std::string& fname);
  virtual bool hasValidCodec();
  virtual void setOwnerWindow(Uint32 id);
  virtual void setAverageFrameDuration(double avgDuration);

private:

  void initFilterGraph(const Image32 &img);
  void disposeFilterGraph();
  void findCompressor();
  bool isFilterGraphInitialized();
  bool tryRegisterFilters();
  
  //
  // ACCESS
  //

public:

  //
  // INQUIRY
  //

public:

  //
  // ATTRIBUTES
  //

protected:

  /**
   * File name.
   */
  std::string mFileName;

  /**
   * Compressor settings file name.
   */
  std::string mCompressorSettingsFileName;

  /**
   * Our special fiter.
   */
  CComPtr<IBaseFilter> mpSourceFilter;

  // DirectShow interfaces.
  CComPtr<IBaseFilter>   mpCompressor;
  CComPtr<IGraphBuilder> mpGraphBuilder;
  CComPtr<IMediaControl> mpControl;
  CComPtr<IMediaSeeking> mpSeeking;
  CComPtr<IMediaEvent> mpEvent;

  /**
   * The owner (parent) window.
   */
  HWND mOwner;

  /**
   * The Current Codec specifications.
   */
  SettingsDataPtr mpSettingsData;

  /**
   * The source filter (pseudo COM object).
   */
  PvImageSourceFilterPtr mpPvImageSourceFilter;

};


}; // namespace ait

#endif // VideoWriterDS_HPP


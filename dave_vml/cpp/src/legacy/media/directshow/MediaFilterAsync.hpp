/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef MediaFilterAsync_HPP
#define MediaFilterAsync_HPP

#include <boost/thread/condition.hpp>
#include <boost/thread/thread.hpp>

#include <streams.h>
#include "DXUtil.h"
#include <MediaFrameInfo.hpp>
#include <ait/Image.hpp>
#include <ait/Image_fwd.hpp>
#include <ait/Vector3.hpp>

#include "MediaFilterBase.hpp"

namespace ait
{

/**
 * This filter will collect the input sample in an internal buffer,
 * asynchrounously, the application asks for the sample. If the sample has already
 * been copied to the application, it will not update it. This is designed
 * for applications with high frame rates. If the application has a slow frame
 * rate, samples will be dropped.
 */
class MediaFilterAsync : public MediaFilterBase
{
public:

  MediaFilterAsync(LPUNKNOWN pUnk,HRESULT *phr);

  virtual ~MediaFilterAsync();

  /**
   * Get a new frame. This call is made from the main thread, and takes care of 
   * synchronization..
   * @ return true if this is a new frame. If false, there were no new frames in
   * the stream.
   */
  virtual bool updateFrame(MediaFrameInfoPtr pFrameInfo);

  /**
   * Return a pointer to the internal mutex.
   */
  boost::mutex& getMutex() { return mFrameMutex; }

protected:

  /**
   * A sample has been delivered. Copy it to the texture.
   */
  virtual HRESULT DoRenderSample(IMediaSample *pMediaSample);

private:

  /**
   * Mutex used to lock the frame contents.
   */
  boost::mutex mFrameMutex;

  /**
   * Media information buffer.
   */
  MediaFrameInfo mMediaFrameInfo;

  /**
   * Image buffer.
   */
  Image32Ptr mpFrameImage;

  /**
   * Flag that indicates if the buffer has been updated.
   */
  bool mNewFrame;
};

} // namespace ait

#endif // MediaFilterAsync

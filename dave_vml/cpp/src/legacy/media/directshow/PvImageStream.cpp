/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include <streams.h>
#include <olectl.h>
#include <initguid.h>

#include "PvImageSourceFilter.hpp"
#include "PvImageStream.hpp"

namespace ait 
{

PvImageStream::PvImageStream(HRESULT *phr,
                         PvImageSourceFilter *pParent,
                         LPCWSTR pPinName) :
    CSourceStream(NAME("Image Source"),phr, pParent, pPinName),
    mpImage(NULL),
    mIsEndOfStream(false),
    mIsImageAvailableToWrite(false),
    mIsImageWritten(false),
    mImageSize(320,240),
    mNumFramesToSkip(0)
{
  ASSERT(phr);
} // (Constructor)


//
// Destructor
//
PvImageStream::~PvImageStream()
{
} // (Destructor)


//
// FillBuffer
//
// Plots a ball into the supplied video buffer
//
HRESULT PvImageStream::FillBuffer(IMediaSample *pms)
{
  CheckPointer(pms,E_POINTER);
  
  BYTE *pData;
  long lDataLen;

  bool eof = false;
  
  pms->GetPointer(&pData);
  lDataLen = pms->GetSize();

  ZeroMemory(pData, lDataLen);

  {  
    boost::mutex::scoped_lock lk(mMutex);
  
    while (!mIsImageAvailableToWrite)
    {
      /*
      boost::xtime xt;
      boost::xtime_get(&xt, boost::TIME_UTC);
      xt.nsec += 100000; // 100 ms
      mCondImageAvailableToWrite.timed_wait(lk,xt);
      */
      mCondImageAvailableToWrite.timed_wait(lk,100);
    }

    BYTE  *pBmpBuffer;     // Bitmap buffer
    
    // Get the video bitmap buffer
    pms->GetPointer( &pBmpBuffer );
    
    if (mpImage)
    {
      Image32& frame = *mpImage;
      
      int w,h;
      
      w = frame.width();
      h = frame.height();
      
      unsigned char *pFrame = ((unsigned char *)frame.pointer()) + 4*w*(h-1);
      int skip = -2*(4*w);
      
      if (frame.getInternalFormat() == Image32::PV_BGRA)
      {
        for (int y = 0; y < h; y++) 
        {
          for (int x = 0; x < w; x++) 
          {	
            pBmpBuffer[0] = pFrame[0];
            pBmpBuffer[1] = pFrame[1];
            pBmpBuffer[2] = pFrame[2];
            pBmpBuffer += 3;
            pFrame += 4;
          }
          pFrame += skip;
        }
      }
      else
      {
        for (int y = 0; y < h; y++) 
        {
          for (int x = 0; x < w; x++) 
          {	
            pBmpBuffer[2] = pFrame[0];
            pBmpBuffer[1] = pFrame[1];
            pBmpBuffer[0] = pFrame[2];
            pBmpBuffer += 3;
            pFrame += 4;
          }
          pFrame += skip;
        }
      }
    }
    
    if (!mIsEndOfStream)
    {
      mNumFrames += mNumFramesToSkip;

      // Skip frames if necessary
      CRefTime start((REFERENCE_TIME)(((double)mNumFrames/mFramesPerSecond)*10000000.0));

      // The current time is the sample's start
      CRefTime end = start + (REFERENCE_TIME)((1.0/mFramesPerSecond)*10000000.0);
    
      pms->SetTime((REFERENCE_TIME *) &start,(REFERENCE_TIME *) &end);

      mNumFrames++;
    }
    else
    {
      eof = true;
    }

    mpImage = NULL;
    mNumFramesToSkip = 0;
    mIsImageAvailableToWrite = false;

    mIsImageWritten = true;
    mCondImageWritten.notify_one();
  }

  pms->SetSyncPoint(TRUE);

  return eof ? S_FALSE : S_OK; 

} // FillBuffer


//
// Notify
//
// Alter the repeat rate according to quality management messages sent from
// the downstream filter (often the renderer).  Wind it up or down according
// to the flooding level - also skip forward if we are notified of Late-ness
//
STDMETHODIMP PvImageStream::Notify(IBaseFilter * pSender, Quality q)
{
  // Always process all frames...
  
  return NOERROR;
  
} // Notify


//
// GetMediaType
//
// I _prefer_ 5 formats - 8, 16 (*2), 24 or 32 bits per pixel and
// I will suggest these with an image size of 320x240. However
// I can accept any image size which gives me some space to bounce.
//
// A bit of fun:
//      8 bit displays get red balls
//      16 bit displays get blue
//      24 bit see green
//      And 32 bit see yellow
//
// Prefered types should be ordered by quality, zero as highest quality
// Therefore iPosition =
// 0    return a 24bit mediatype
// (iPosition > 0 is invalid)
//
HRESULT PvImageStream::GetMediaType(int iPosition, CMediaType *pmt)
{
  CheckPointer(pmt,E_POINTER);
  
  CAutoLock cAutoLock(m_pFilter->pStateLock());
  if(iPosition < 0)
  {
    return E_INVALIDARG;
  }
  
  // Have we run off the end of types?
  
  if(iPosition > 0)
  {
    return VFW_S_NO_MORE_ITEMS;
  }
  
  VIDEOINFO *pvi = (VIDEOINFO *) pmt->AllocFormatBuffer(sizeof(VIDEOINFO));
  if(NULL == pvi)
    return(E_OUTOFMEMORY);
  
  ZeroMemory(pvi, sizeof(VIDEOINFO));
  
  switch(iPosition)
  {
  case 0:
    {   // Return our 24bit format
      pvi->bmiHeader.biCompression = BI_RGB;
      pvi->bmiHeader.biBitCount    = 24;
      break;
    }
  }
  
  pvi->bmiHeader.biSize       = sizeof(BITMAPINFOHEADER);
  pvi->bmiHeader.biWidth      = mImageSize.x;
  pvi->bmiHeader.biHeight     = mImageSize.y;
  pvi->bmiHeader.biPlanes     = 1;
  pvi->bmiHeader.biSizeImage  = GetBitmapSize(&pvi->bmiHeader);
  pvi->bmiHeader.biClrImportant = 0;
  
  SetRectEmpty(&(pvi->rcSource)); // we want the whole image area rendered.
  SetRectEmpty(&(pvi->rcTarget)); // no particular destination rectangle
  
  pmt->SetType(&MEDIATYPE_Video);
  pmt->SetFormatType(&FORMAT_VideoInfo);
  pmt->SetTemporalCompression(FALSE);
  
  // Work out the GUID for the subtype from the header info.
  const GUID SubTypeGUID = GetBitmapSubtype(&pvi->bmiHeader);
  pmt->SetSubtype(&SubTypeGUID);
  pmt->SetSampleSize(pvi->bmiHeader.biSizeImage);
  
  return NOERROR;
  
} // GetMediaType


//
// CheckMediaType
//
// We will accept 24 bit video formats only, in any
// image size that gives room to bounce.
// Returns E_INVALIDARG if the mediatype is not acceptable
//
HRESULT PvImageStream::CheckMediaType(const CMediaType *pMediaType)
{
  CheckPointer(pMediaType,E_POINTER);
  
  if((*(pMediaType->Type()) != MEDIATYPE_Video) ||   // we only output video
    !(pMediaType->IsFixedSize()))                   // in fixed size samples
  {                                                  
    return E_INVALIDARG;
  }
  
  // Check for the subtypes we support
  const GUID *SubType = pMediaType->Subtype();
  if (SubType == NULL)
    return E_INVALIDARG;
  
  if(*SubType != MEDIASUBTYPE_RGB24)
  {
    return E_INVALIDARG;
  }
  
  // Get the format area of the media type
  VIDEOINFO *pvi = (VIDEOINFO *) pMediaType->Format();  
  if(pvi == NULL)
    return E_INVALIDARG;
  
  // Check if the image width & height have changed
  /*
  if(pvi->bmiHeader.biWidth != m_Ball->GetImageWidth() || 
  abs(pvi->bmiHeader.biHeight) != m_Ball->GetImageHeight())
  {
  // If the image width/height is changed, fail CheckMediaType() to force
  // the renderer to resize the image.
  return E_INVALIDARG;
  }
  */
  
  return S_OK;  // This format is acceptable.
  
} // CheckMediaType


//
// DecideBufferSize
//
// This will always be called after the format has been sucessfully
// negotiated. So we have a look at m_mt to see what size image we agreed.
// Then we can ask for buffers of the correct size to contain them.
//
HRESULT PvImageStream::DecideBufferSize(IMemAllocator *pAlloc,
                                      ALLOCATOR_PROPERTIES *pProperties)
{
  CheckPointer(pAlloc,E_POINTER);
  CheckPointer(pProperties,E_POINTER);
  
  CAutoLock cAutoLock(m_pFilter->pStateLock());
  HRESULT hr = NOERROR;
  
  VIDEOINFO *pvi = (VIDEOINFO *) m_mt.Format();
  pProperties->cBuffers = 1;
  pProperties->cbBuffer = pvi->bmiHeader.biSizeImage;
  
  ASSERT(pProperties->cbBuffer);
  
  // Ask the allocator to reserve us some sample memory, NOTE the function
  // can succeed (that is return NOERROR) but still not have allocated the
  // memory that we requested, so we must check we got whatever we wanted
  
  ALLOCATOR_PROPERTIES Actual;
  hr = pAlloc->SetProperties(pProperties,&Actual);
  if(FAILED(hr))
  {
    return hr;
  }
  
  // Is this allocator unsuitable
  
  if(Actual.cbBuffer < pProperties->cbBuffer)
  {
    return E_FAIL;
  }
  
  // Make sure that we have only 1 buffer (we erase the ball in the
  // old buffer to save having to zero a 200k+ buffer every time
  // we draw a frame)
  
  ASSERT(Actual.cBuffers == 1);
  return NOERROR;
  
} // DecideBufferSize


//
// SetMediaType
//
// Called when a media type is agreed between filters
//
HRESULT PvImageStream::SetMediaType(const CMediaType *pMediaType)
{
  CAutoLock cAutoLock(m_pFilter->pStateLock());
  
  // Pass the call up to my base class
  
  HRESULT hr = CSourceStream::SetMediaType(pMediaType);
  
  if(SUCCEEDED(hr))
  {
    return NOERROR;
  } 
  
  return hr;
  
} // SetMediaType


//
// OnThreadCreate
//
// As we go active reset the stream time to zero
//
HRESULT PvImageStream::OnThreadCreate()
{
  boost::mutex::scoped_lock lk(mMutex);
  mNumFrames = 0;

  return NOERROR;
} // OnThreadCreate

void
PvImageStream::write(const Image32& img)
{
  boost::mutex::scoped_lock lk(mMutex);
  mpImage = const_cast<Image32 *>(&img);
  notifyImageAvailable(lk);
}

void 
PvImageStream::writeEndOfFile()
{
  boost::mutex::scoped_lock lk(mMutex);
  mIsEndOfStream = true;
  notifyImageAvailable(lk);
}

/**
 * Repeat the last frame.
 */
void
PvImageStream::writePreviousFrame()
{
  boost::mutex::scoped_lock lk(mMutex);
  mNumFramesToSkip++;
}

void
PvImageStream::notifyImageAvailable(boost::mutex::scoped_lock& lk)
{
  mIsImageAvailableToWrite = true;
  mCondImageAvailableToWrite.notify_one();

  mIsImageWritten = false;

  while (!mIsImageWritten)
  {
    /*
    boost::xtime xt;
    boost::xtime_get(&xt, boost::TIME_UTC);
    xt.nsec += 100;
    mCondImageWritten.timed_wait(lk,xt);
    */
    mCondImageWritten.timed_wait(lk,100);
    {
      // Pump message queue, some filters depend on this otherwise
      // the graph would lock.
      MSG msg;
      while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
      {
          TranslateMessage(&msg);
          DispatchMessage(&msg);
      }
    }
  }
}

void
PvImageStream::setImageSize(const Vector2i& size)
{
  mImageSize = size;
}

void 
PvImageStream::setFramesPerSecond(double fps)
{
  mFramesPerSecond = fps;
}

bool
PvImageStream::isFillBufferDone()
{
  return (mpImage == NULL || mIsEndOfStream);
}

}; // namespace ait


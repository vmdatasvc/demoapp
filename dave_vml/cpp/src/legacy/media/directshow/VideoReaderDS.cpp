/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "VideoReaderDS.hpp"
#include <winstrutil.hpp>

namespace ait 
{

VideoReaderDS::VideoReaderDS()
: mFrameNumber(-1),
  mLastFrameNumber(-1)
{
  // From the MSDN documentation: calls to CoInitializeEx by the same thread are allowed 
  // as long as they pass the same concurrency flag.
  CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
}

VideoReaderDS::~VideoReaderDS()
{
  close();
  CoUninitialize();
}

//
// OPERATIONS
//

void 
VideoReaderDS::open(const std::string& fname)
{
  HRESULT hr = S_OK;

  // Create the direct draw context used for this surface. We will use the
  // DirectDraw7 interface so we can later change the address of the surface
  // memory to point to our own memory in the Image by using SetSurfaceDesc().

  CComPtr<IDirectDraw7> pDD7;
  hr = DirectDrawCreateEx(NULL,(void **) &pDD7, IID_IDirectDraw7, NULL);

  if (hr != S_OK)
  {
    // For some reason DirectDrawCreateEx succeeds event if CoInitializeEx has not been called.
    PvUtil::exitError("A COM object could not be created.\nA call to CoInitializeEx(NULL, COINIT_APARTMENTTHREADED) must be added to the main file.");
  }

  hr = pDD7->SetCooperativeLevel(GetDesktopWindow(), DDSCL_NORMAL);
  CComQIPtr<IDirectDraw,&IID_IDirectDraw> pDD(pDD7);
  if (hr != S_OK)
  {
    PvUtil::exitError("Error calling SetCooperativeLevel()");
  }

  // Create the MM stream.
  hr = mpMMStream.CoCreateInstance(CLSID_AMMultiMediaStream);

  if (hr == CO_E_NOTINITIALIZED)
  {
    PvUtil::exitError("A COM object could not be created.\nA call to CoInitializeEx(NULL, COINIT_APARTMENTTHREADED) must be added to the main file.");
  }

  mpMMStream->Initialize(STREAMTYPE_READ, AMMSF_NOGRAPHTHREAD, NULL);
  mpMMStream->AddMediaStream(pDD, &MSPID_PrimaryVideo, 0, NULL);
  //pAMStream->AddMediaStream(NULL, &MSPID_PrimaryAudio, AMMSF_ADDDEFAULTRENDERER, NULL);

  BSTR fname1 = stringToBstr(fname);
  hr = mpMMStream->OpenFile(fname1, AMMSF_NOCLOCK);
  SysFreeString(fname1);

  if (hr != S_OK)
  {
    throw std::exception("Error opening file.");
  }

  // Get the media streams.
  CComPtr<IMediaStream> pMS;
  hr = mpMMStream->GetMediaStream(MSPID_PrimaryVideo, &pMS);
  hr = pMS.QueryInterface(&mpPrimaryVideoStream);

  // Create the sample
  DDSURFACEDESC ddsd;
  ddsd.dwSize = sizeof(ddsd);
  hr = mpPrimaryVideoStream->GetFormat(&ddsd, NULL, NULL, NULL);

  RECT rect;
  rect.top = rect.left = 0;
  rect.bottom = ddsd.dwHeight;
  rect.right = ddsd.dwWidth;

  mBufferSize.set((int)ddsd.dwWidth,(int)ddsd.dwHeight);

  // Create the surface we will use.
  {
    DDSURFACEDESC2 ddsd2;
    ZeroMemory(&ddsd2,sizeof(ddsd2));
    ddsd2.dwSize = sizeof(ddsd2);
    ddsd2.dwFlags = DDSD_CAPS|DDSD_HEIGHT|DDSD_WIDTH|DDSD_PIXELFORMAT;
    ddsd2.ddsCaps.dwCaps = DDSCAPS_SYSTEMMEMORY|DDSCAPS_OFFSCREENPLAIN;
    ddsd2.dwWidth = rect.right;
    ddsd2.dwHeight = rect.bottom;
    ddsd2.ddpfPixelFormat.dwSize = sizeof(ddsd2.ddpfPixelFormat);
    ddsd2.ddpfPixelFormat.dwFlags = DDPF_RGB;
    ddsd2.ddpfPixelFormat.dwRGBBitCount = 24;
    ddsd2.ddpfPixelFormat.dwRBitMask = 0x00FF0000;
    ddsd2.ddpfPixelFormat.dwGBitMask = 0x0000FF00;
    ddsd2.ddpfPixelFormat.dwBBitMask = 0x000000FF;
    hr = pDD7->CreateSurface(&ddsd2, &mpSurface, NULL);
    mSampleImageData.resize(rect.right*rect.bottom*3);
  }

  CComQIPtr<IDirectDrawSurface,&IID_IDirectDrawSurface> pSurface(mpSurface);
  hr = mpPrimaryVideoStream->CreateSample(pSurface, &rect, 0, &mpStreamSample);

  mpMMStream->SetState(STREAMSTATE_RUN);

  // Get one sample to get the frame rate.
  hr = mpStreamSample->Update(0, NULL, NULL, NULL);

  STREAM_TIME start,end;
  mpStreamSample->GetSampleTimes(&start,&end,NULL);

  double fps = 10000000.0/((double)(end-start));

  // Round to 3 decimals
  fps = ((double)round(fps*1000.0))/1000.0;

  mFramesPerSecond = fps;

  mLastFrameNumber = -1;
  mFrameNumber = 0;
  mCurrentSampleFrameNumber = -1;
}

void 
VideoReaderDS::close()
{
  mpStreamSample.Release();
  mpSurface.Release();
  mpPrimaryVideoStream.Release();
  mpMMStream.Release();
  mSampleImageData.clear();
}

void
VideoReaderDS::readSample()
{
  HRESULT hr = S_OK;

  const Vector2i& size = getBufferSize();

  DDSURFACEDESC2 ddsd;
  ZeroMemory(&ddsd,sizeof(ddsd));
  ddsd.dwSize = sizeof(ddsd);
  ddsd.dwFlags = DDSD_LPSURFACE;
  ddsd.lpSurface = &mSampleImageData[0];
  hr = mpSurface->SetSurfaceDesc(&ddsd,0);

  hr = mpStreamSample->Update(0, NULL, NULL, NULL);

  STREAM_TIME start,end,current;

  mpStreamSample->GetSampleTimes(&start,&end,&current);

  //PVMSG("mms: %i\n",(int)(end-start));

  mCurrentSampleFrameNumber = round(((double)start)/(10000000.0/mFramesPerSecond));
  //PVMSG("FNUM: %i, REQUESTED: %i\n",(int)mCurrentSampleFrameNumber,mFrameNumber);
}

void
VideoReaderDS::copySampleToImage(Image32& img)
{
  img.resize(mBufferSize.x,mBufferSize.y);

  unsigned char * pBmpBuffer = &mSampleImageData[0];
  int w = img.width(), h = img.height();

  unsigned char *pFrame = ((unsigned char *)img.pointer()); // + 4*w*(h-1);
  int skip = 0; //2*(4*w);

  if (img.getInternalFormat() == Image32::PV_BGRA)
  {
    for (int y = 0; y < h; y++) 
    {
      for (int x = 0; x < w; x++) 
      {	
        pFrame[0] = pBmpBuffer[0];
        pFrame[1] = pBmpBuffer[1];
        pFrame[2] = pBmpBuffer[2];
				pFrame[3] = 0;
        pBmpBuffer += 3;
        pFrame += 4;
      }
      pFrame += skip;
    }
  }
  else
  {
    for (int y = 0; y < h; y++) 
    {
      for (int x = 0; x < w; x++) 
      {	
        pFrame[0] = pBmpBuffer[2];
        pFrame[1] = pBmpBuffer[1];
        pFrame[2] = pBmpBuffer[0];
				pFrame[3] = 0;
        pBmpBuffer += 3;
        pFrame += 4;
      }
      pFrame += skip;
    }
  }
}

bool 
VideoReaderDS::readFrame(Image32& img)
{
  if (isEndOfFile())
  {
    return false;
  }

  HRESULT hr = S_OK;

  // Seek the sample to the desired location
  Int64 diff = mFrameNumber-mLastFrameNumber;
  if (mLastFrameNumber == -1 || diff <= 0 || diff > 10)
  {
    // The seek operation is quite expensive, and it is not very smart.
    // For example if we seek to the next frame, it will reconstruct the 
    // stream from the last key frame, making it a very expensive operation.
    // Therefore we try to move forward by just advancing the stream, unless
    // we have to move backwards. We also don't have access to the key frame
    // information to make better decisions here.
    STREAM_TIME timePerFrame = (STREAM_TIME)((1.0/mFramesPerSecond)*10000000);
    STREAM_TIME time = timePerFrame*mFrameNumber + 1;

    //PVMSG("seek frame: %i\n",(int)time);
    hr = mpMMStream->Seek(time);
    mCurrentSampleFrameNumber = -1;
  }

  // Advance the skip samples.
  Int64 lastSampleNumber = -1;
  while (mCurrentSampleFrameNumber < mFrameNumber)
  {
    readSample();
    if (lastSampleNumber == mCurrentSampleFrameNumber)
    {
      // The DirectShow AVI splitter seems to miss the last few frames.
      break;
    }
    lastSampleNumber = mCurrentSampleFrameNumber;
  }

  if (mCurrentSampleFrameNumber <= mFrameNumber)
  {
    copySampleToImage(img);
  }

  mLastFrameNumber = mFrameNumber;

  return true;
}

//
// ACCESS
//

Int64 
VideoReaderDS::getNumberOfFrames()
{
  STREAM_TIME duration;
  mpMMStream->GetDuration(&duration);
  return round((float)((((double)duration)/10000000)*getFramesPerSecond()));
}

void 
VideoReaderDS::setCurrentFrameNumber(Int64 frame)
{
  if (frame < 0 || frame >= getNumberOfFrames())
  {
    throw std::out_of_range("Frame number is not valid");
  }
  mFrameNumber = frame;
}

void
VideoReaderDS::nextFrame()
{
  if (mFrameNumber >= getNumberOfFrames())
  {
    return;
  }
  mFrameNumber++;
}

Int64 
VideoReaderDS::getCurrentFrameNumber()
{
  return mFrameNumber;
}

double 
VideoReaderDS::getFramesPerSecond()
{
  return mFramesPerSecond;
}

Vector2i
VideoReaderDS::getBufferSize()
{
  return mBufferSize;
}

//
// INQUIRY
//

}; // namespace ait


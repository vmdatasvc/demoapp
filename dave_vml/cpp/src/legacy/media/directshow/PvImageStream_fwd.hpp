/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef PvImageStream_fwd_HPP
#define PvImageStream_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class PvImageStream;
typedef boost::shared_ptr<PvImageStream> PvImageStreamPtr;

}; // namespace ait

#endif // PvImageStream_fwd_HPP


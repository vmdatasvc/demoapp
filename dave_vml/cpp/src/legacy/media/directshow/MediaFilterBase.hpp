/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef MediaFilterBase_HPP
#define MediaFilterBase_HPP

#include <boost/thread/condition.hpp>
#include <boost/thread/thread.hpp>

#include <streams.h>
#include "DXUtil.h"
#include <MediaFrameInfo.hpp>
#include <ait/Image.hpp>
#include <ait/Image_fwd.hpp>
#include <ait/Vector3.hpp>

namespace ait
{

/**
 * Base class for the AI renderer filters that transform
 * the samples from DirectShow into PvImages.
 */
class MediaFilterBase : public CBaseVideoRenderer
{
public:
  MediaFilterBase(REFCLSID RenderClass, LPUNKNOWN pUnk,HRESULT *phr);

  virtual ~MediaFilterBase();

  /**
   * Width and Height of video frames.
   */
  virtual Vector2i getFrameSize() { return mFrameSize; }     

  /**
   * Set the image format type. (e.g: Image32::PV_RGBA)
   */
  virtual void setInternalImageFormat(int format) { mInternalImageFormat = format; }

  /**
   * Get a new frame. This call is made from the main thread, and takes care of 
   * synchronization..
   * @return true if the frame was changed.
   */
  virtual bool updateFrame(MediaFrameInfoPtr pFrameInfo) = 0;

protected:

  /**
   * This function actually copies the data from the sample to
   * the frame information.
   */
  virtual void copyFrame(IMediaSample *pMediaSample, MediaFrameInfo* pMediaFrameInfo);

  /**
   * Format acceptable?
   */
  virtual HRESULT CheckMediaType(const CMediaType *pmt );

  /**
   * Video format notification.
   */
  virtual HRESULT SetMediaType(const CMediaType *pmt );

  /**
   * This function is called by the stream thread.
   */
  virtual HRESULT DoRenderSample(IMediaSample *pMediaSample) = 0;

private:

  /**
   * Size of the frame.
   */
  Vector2i mFrameSize;

  /**
   * Internal image format.
   */
  int mInternalImageFormat;

};

} // namespace ait

#endif //MediaFilterBase

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VideoReaderDS_fwd_HPP
#define VideoReaderDS_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class VideoReaderDS;
typedef boost::shared_ptr<VideoReaderDS> VideoReaderDSPtr;

}; // namespace ait

#endif // VideoReaderDS_fwd_HPP


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include <streams.h>
#include <olectl.h>

#include "VideoWriterDS.hpp"
#include <winstrutil.hpp>
#include <Settings.hpp>

#include <initguid.h>
#include "PvImageSourceFilter.hpp"


static HRESULT GetPin( IBaseFilter * pFilter, PIN_DIRECTION dirrequired, int iNum, IPin **ppPin);
static IPin * GetInPin( IBaseFilter * pFilter, int nPin );
static IPin * GetOutPin( IBaseFilter * pFilter, int nPin );

static void hideXviDStatusWindow()
{
 HKEY	hkey;
 DWORD	dwDisposition;

 DWORD dwType, dwSize;

 /*
 // We will write it anyways to avoid the case of running for the first time.
 if (RegOpenKey(HKEY_CURRENT_USER,TEXT("Software\\GNU\\XviD"),&hkey) != ERROR_SUCCESS)
 {
   // XviD doesn't seem to be installed. Do nothing.
   return;
 }
 RegCloseKey(hkey);
 */

 // Now write the value.

 HRESULT res;
 if((res = RegCreateKeyEx(HKEY_CURRENT_USER, TEXT("Software\\GNU\\XviD"), NULL, NULL, NULL,
      KEY_ALL_ACCESS, NULL, &hkey, &dwDisposition))== 
	ERROR_SUCCESS)
 {
  dwType = REG_DWORD;
  dwSize = sizeof(DWORD);
  DWORD dwValue = 0;
  RegSetValueEx(hkey, TEXT("display_status"), 0, dwType, (PBYTE)&dwValue, dwSize);

  RegCloseKey(hkey);
 }
 else
 {
   char msg[256];
   FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM,NULL,res,0,msg,sizeof(msg),NULL);
 }
}

namespace ait 
{

VideoWriterDS::VideoWriterDS()
: mpSourceFilter(NULL),
  mOwner(NULL)
{
  // From the MSDN documentation: calls to CoInitializeEx by the same thread are allowed 
  // as long as they pass the same concurrency flag.
  CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);

  // XviD display window locks the application.
  hideXviDStatusWindow();
}

VideoWriterDS::~VideoWriterDS()
{
  close();
  CoUninitialize();
}

void 
VideoWriterDS::open(const std::string& fname)
{
  mFileName = Settings::replaceVars(fname);
  // Try to create the file so there is a message if there is a problem.
  FILE *fp = fopen(fname.c_str(),"wb");
  if (!fp || fclose(fp) != 0)
  {
    throw std::exception(("Unable to create: " + fname).c_str());
  }

  if (!hasValidCodec())
  {
    loadCodecSettings("<CommonFilesPath>/data/codecs/default.codec",true);
  }
}

void 
VideoWriterDS::writeFrame(const Image32& img)
{
  if (!isFilterGraphInitialized())
  {
    initFilterGraph(img);
  }

  mpPvImageSourceFilter->write(img);
}

void 
VideoWriterDS::skipFrame()
{
  if (!isFilterGraphInitialized())
  {
    PvUtil::exitError("You must write at least one frime to skip frames!");
  }

  mpPvImageSourceFilter->writePreviousFrame();
}

void 
VideoWriterDS::close()
{
  if (isFilterGraphInitialized())
  {
    // Source filter will forward to the source
    // stream and the source stream class will take care
    // of threading safety.
    mpPvImageSourceFilter->writeEndOfFile();
  }

  disposeFilterGraph();
}

bool 
VideoWriterDS::loadCodecSettings(const std::string& fname, bool showDialogIfNotPresent)
{
  mCompressorSettingsFileName = Settings::replaceVars(fname);

  if (showDialogIfNotPresent && !PvUtil::fileExists(mCompressorSettingsFileName.c_str()))
  {
    if (!showCodecSettingsDialog(mCompressorSettingsFileName))
    {
      return false;
    }
  }

  if (isFilterGraphInitialized())
  {
    PvUtil::exitError("Can't change codec settings after opening file.");
  }
  mpSettingsData.reset(new SettingsData());

  try
  {
    mpSettingsData->load(mCompressorSettingsFileName);
  }
  catch (std::exception e)
  {
    if (!showCodecSettingsDialog(mCompressorSettingsFileName))
    {
      return false;
    }
    try
    {
      mpSettingsData->load(mCompressorSettingsFileName);
    }
    catch (std::exception e)
    {
      PvUtil::exitError("Error reading Codec Settings file: %s",e.what());
    }
  }

  return true;
}

void 
VideoWriterDS::saveCodecSettings(const std::string& fname)
{
  if (!mpSettingsData.get())
  {
    PvUtil::exitError("No settings are loaded!");
  }

  try
  {
    mpSettingsData->save(fname);
  }
  catch (std::exception e)
  {
    PvUtil::exitError("Error saving Codec Settings file: %s",e.what());
  }
}

bool 
VideoWriterDS::showCodecSettingsDialog(const std::string& fname)
{
  std::string args = "\"" + fname + "\"";
  if (mOwner != NULL)
  {
    args += "--parent_window " + aitSprintf("%i",(int)mOwner);
  }

  TCHAR exeFileName[MAX_PATH+1];
  TCHAR *p;

  DWORD length = SearchPath(
    NULL,
    "codec_settings.exe",
    NULL,
    MAX_PATH,
    exeFileName,
    &p);

  if (length == 0)
  {
    PvUtil::exitError("Error starting codec_settings.exe");
    return false;
  }
 
  SHELLEXECUTEINFO sei;
  ZeroMemory(&sei,sizeof(sei));
  sei.cbSize = sizeof(sei);
  sei.lpFile = exeFileName;
  sei.lpParameters = args.c_str();
  sei.lpVerb = "open";
  sei.nShow = SW_SHOWNORMAL;
  sei.hwnd = mOwner;
  sei.fMask = SEE_MASK_NOCLOSEPROCESS;

  if (!ShellExecuteEx(&sei))
  {
    PvUtil::exitError("Error starting codec_settings.exe");
  }
  else
  {
    WaitForSingleObject(sei.hProcess,INFINITE);
    DWORD exitCode;
    if (GetExitCodeProcess(sei.hProcess,&exitCode) == 0)
    {
      CloseHandle(sei.hProcess);
      return false;
    }
    CloseHandle(sei.hProcess);
    return exitCode == 0;
  }
  return false;
}

bool 
VideoWriterDS::hasValidCodec()
{
  return mpSettingsData.get() != 0;
}

bool
VideoWriterDS::isFilterGraphInitialized()
{
  return mpGraphBuilder != 0;
}

bool
VideoWriterDS::tryRegisterFilters()
{
  TCHAR dllFileName[MAX_PATH+1];
  TCHAR *p;

  DWORD length = SearchPath(
    NULL,
    "aidirectshowfilters.exe",
    NULL,
    MAX_PATH,
    dllFileName,
    &p);
 
  if (!length) return 0;

  int returnCode = system((std::string("regsvr32.exe /s \"")+dllFileName+std::string("\"")).c_str());

  return returnCode == 0;
}

void
VideoWriterDS::initFilterGraph(const Image32& img)
{
  disposeFilterGraph();

  HRESULT hr;
  
  mpGraphBuilder.CoCreateInstance(CLSID_FilterGraph);
  if (!mpGraphBuilder)
  {
    PvUtil::exitError("Could not initialize graph.\nMake sure to call CoInitializeEx(NULL, COINIT_APARTMENTTHREADED) in the main file.");
  }

  if (mOwner != NULL)
  {
    // Set the owner window
    CComQIPtr< IVideoWindow > pVideoWindow(mpGraphBuilder);
    pVideoWindow->put_Owner((OAHWND)mOwner);
  }

  // create a CaptureGraphBuilder to help connect filters
  CComPtr< ICaptureGraphBuilder > pBuilder;
  pBuilder.CoCreateInstance(CLSID_CaptureGraphBuilder);
  
  // tell the capture graph builder what graph we're using
  hr = pBuilder->SetFiltergraph(mpGraphBuilder);
  if(FAILED(hr))
  {
    PvUtil::exitError("Could not setup capture graph");
  }
  
  // Create our source filter
  mpPvImageSourceFilter.reset((PvImageSourceFilter *)PvImageSourceFilter::CreateInstance(NULL,&hr));

  hr = mpPvImageSourceFilter->QueryInterface(IID_IBaseFilter,(void**)&mpSourceFilter);
  if(FAILED(hr))
  {
    PvUtil::exitError("Error creating Image source filter.");
  }

  mpPvImageSourceFilter->setImageSize(Vector2i(img.width(),img.height()));
  mpPvImageSourceFilter->setFramesPerSecond(getFramesPerSecond());

  mpGraphBuilder->AddFilter(mpSourceFilter,NULL);
  if(FAILED(hr))
  {
    PvUtil::exitError("Error trying to add the source Image source filter\n");
  }

  findCompressor();
  
  // Have the graph builder automatically create a MUX and FileWriter for us,
  // and insert them into the graph, by just specifying an output filename
  CComPtr< IBaseFilter > pMux;
  CComPtr< IFileSinkFilter > pWriter;
  
  BSTR fname = stringToBstr(mFileName);
  
  hr = pBuilder->SetOutputFileName(
    &MEDIASUBTYPE_Avi,
    fname, //T2W(szOutFilename),
    &pMux,
    &pWriter);
  if(FAILED(hr))
  {
    PvUtil::exitError("Cannot set up MUX and File Writer");
  }

  SysFreeString(fname);
  
  hr = mpGraphBuilder->AddFilter(mpCompressor, L"Compressor");
  if(FAILED(hr))
  {
    PvUtil::exitError(TEXT("Could not add selected compressor to the graph.\r\n\r\n")
      TEXT("The input file format may not be compatible with the selected encoder.\r\n")
      TEXT("Some encoders (like Windows Media encoders or DRM-enabled encoders)\r\n")
      TEXT("cannot be used without appropriate licensing."));
  }
  
  // get a set of pins
  CComPtr<IPin> pSourcePin     = GetOutPin(mpSourceFilter,   0);
  CComPtr<IPin> pCompressorIn  = GetInPin (mpCompressor, 0);
  CComPtr<IPin> pCompressorOut = GetOutPin(mpCompressor, 0);
  CComPtr<IPin> pMuxIn1        = GetInPin (pMux, 0);
  
  // connect the source to the sample grabber
  hr = mpGraphBuilder->Connect(pSourcePin, pCompressorIn);
  if(FAILED(hr))
  {
    PvUtil::exitError(TEXT("Could not connect video to selected compressor.\r\n\r\n")
      TEXT("The input file format may not be compatible with the\r\n")
      TEXT("selected encoder.  Not all encoders are compatible."));
  }
  
  // connect the compressor to the MUX
  hr = mpGraphBuilder->Connect(pCompressorOut, pMuxIn1);
  if(FAILED(hr))
  {
    PvUtil::exitError(TEXT("Could not connect compressor to mux.\r\n\r\n")
      TEXT("The selected compressor may not be compatible with this content.\r\n")
      TEXT("Some encoders (like Windows Media encoders or DRM-enabled encoders)\r\n")
      TEXT("cannot be used without appropriate licensing."));
  }
  
  // get filter graph interface pointers
  CComQIPtr< IMediaFilter,  &IID_IMediaFilter >  pMF(mpGraphBuilder);

  mpGraphBuilder.QueryInterface(&mpControl);
  mpGraphBuilder.QueryInterface(&mpSeeking);
  mpGraphBuilder.QueryInterface(&mpEvent);
  
  // tell the graph to run as fast as possible, and not to skip any frames
  // Setting the sync source to NULL disables the clock.
  hr = pMF->SetSyncSource(NULL);
  if(FAILED(hr))
  {
    PvUtil::exitError(TEXT("Could not SetSyncSource NULL on the graph"));
  }
  
  // start running the graph to begin the compression process
  hr = mpControl->Run();
  if(FAILED(hr))
  {
    PvUtil::exitError(TEXT("Could not run compression graph"));
  }
}

void
VideoWriterDS::disposeFilterGraph()
{
  mpSourceFilter.Release();
  mpControl.Release();
  mpSeeking.Release();
  mpEvent.Release();
  mpCompressor.Release();
  mpGraphBuilder.Release();

  // Now that no one should be referencing the pseudo COM
  // object, we can remove it.
  mpPvImageSourceFilter.reset();
}

void
VideoWriterDS::findCompressor()
{
  HRESULT hr = 0;
  
  // we use the same technique in this routine as the one that
  // adds compressors to the UI list box, except this time we
  // return a pointer to an actual filter
  
  CComPtr< ICreateDevEnum > pCreateDevEnum;
  hr = CoCreateInstance(
    CLSID_SystemDeviceEnum, 
    NULL, 
    CLSCTX_INPROC_SERVER,
    IID_ICreateDevEnum, 
    (void**) &pCreateDevEnum);
  
  if(FAILED(hr))
  {
    PvUtil::exitError(TEXT("Failed to create system enumerator"));
  }
  
  CComPtr< IEnumMoniker > pEm;
  hr = pCreateDevEnum->CreateClassEnumerator(
    CLSID_VideoCompressorCategory,
    &pEm, 
    0);
  
  if(FAILED(hr))
  {
    PvUtil::exitError(TEXT("Failed to create class enumerator"));
    return;
  }
  
  pEm->Reset();
  
  while(1)
  {
    ULONG cFetched = 0;
    CComPtr< IMoniker > pMoniker;
    
    hr = pEm->Next(1, &pMoniker, &cFetched);
    if(!pMoniker)
    {
      break;
    }

    {
      CComPtr<IBindCtx> pBindCtx;
      CreateBindCtx(0,&pBindCtx);
      BSTR bstr;
      pMoniker->GetDisplayName(pBindCtx,NULL,&bstr);
      USES_CONVERSION;
      std::string id = W2T(bstr);
      CComPtr<IMalloc> pMalloc;
      CoGetMalloc(1,&pMalloc);
      pMalloc->Free(bstr);

      if (id == mpSettingsData->codecId)
      {
        hr = pMoniker->BindToObject(0, 0, IID_IBaseFilter, (void**) &mpCompressor);
        if(FAILED(hr))
        {
          PvUtil::exitError(TEXT("Failed to bind to object"));
        }
        return;
      }
    }
  }
}

void
VideoWriterDS::setOwnerWindow(Uint32 id)
{
  if (isFilterGraphInitialized())
  {
    PvUtil::exitError("Failed to set the owner window. The filter graph has already been created.");
  }
  mOwner = (HWND)id;
}

void
VideoWriterDS::setAverageFrameDuration(double avg)
{
  if (isFilterGraphInitialized())
  {
    PvUtil::exitError("This operation must be done before opening the file.");
  }
  VideoWriter::setAverageFrameDuration(avg);
}

const std::string VideoWriterDS::SettingsData::magicHeader = "aicodec_1.0";

void
VideoWriterDS::SettingsData::load(const std::string& fname)
{
  FILE *fp = fopen(fname.c_str(),"rb");
  if (!fp)
  {
    throw std::exception(("File not found: " + fname).c_str());
  }

  std::string header = magicHeader;

  if (fread(&header[0],1,magicHeader.length(),fp) < magicHeader.length())
  {
    fclose(fp);
    throw std::exception("Invalid file format.");
  }

  if (header != magicHeader)
  {
    fclose(fp);
    throw std::exception("Invalid file format.");
  }

  Uint32 size = 0;

  fread(&size,sizeof(Uint32),1,fp);

  codecId.resize(size,'x');
  if (fread(&codecId[0],1,size,fp) != size)
  {
    fclose(fp);
    throw std::exception("Invalid file format.");
  }

  fread(&size,sizeof(Uint32),1,fp);

  data.resize(size);
  if (size > 0)
  {
    if (fread(&data[0],1,size,fp) != size)
    {
      fclose(fp);
      throw std::exception("Invalid file format.");
    }
  }

  fclose(fp);
}

void
VideoWriterDS::SettingsData::save(const std::string& fname)
{
  FILE *fp = fopen(fname.c_str(),"wb");
  if (!fp)
  {
    throw std::exception(("Unable to create file: " + fname).c_str());
  }

  write(fp,magicHeader.c_str(),magicHeader.length());

  Uint32 size = codecId.length();
  write(fp,&size,sizeof(Uint32));
  write(fp,codecId.c_str(),size);

  size = data.size();
  write(fp,&size,sizeof(Uint32));

  if (data.size() > 0)
  {
    write(fp,&data[0],data.size());
  }

  fclose(fp);
}

void
VideoWriterDS::SettingsData::write(FILE *fp, const void *data, int dataSize)
{
  if (dataSize == 0) return;
  if (fwrite(data,1,dataSize,fp) != dataSize)
  {
    fclose(fp);
    throw std::exception("Error writing file");
  }
}

}; // namespace ait


static HRESULT GetPin( IBaseFilter * pFilter, PIN_DIRECTION dirrequired, int iNum, IPin **ppPin)
{
    CComPtr< IEnumPins > pEnum;
    *ppPin = NULL;

    if (!pFilter)
       return E_POINTER;

    HRESULT hr = pFilter->EnumPins(&pEnum);
    if(FAILED(hr)) 
        return hr;

    ULONG ulFound;
    IPin *pPin;
    hr = E_FAIL;

    while(S_OK == pEnum->Next(1, &pPin, &ulFound))
    {
        PIN_DIRECTION pindir = (PIN_DIRECTION)3;

        pPin->QueryDirection(&pindir);
        if(pindir == dirrequired)
        {
            if(iNum == 0)
            {
                *ppPin = pPin;  // Return the pin's interface
                hr = S_OK;      // Found requested pin, so clear error
                break;
            }
            iNum--;
        } 

        pPin->Release();
    } 

    return hr;
}

//
// NOTE: The GetInPin and GetOutPin methods DO NOT increment the reference count
// of the returned pin.  Use CComPtr interface pointers in your code to prevent
// memory leaks caused by reference counting problems.  The SDK samples that use
// these methods all use CComPtr<IPin> interface pointers.
// 
//     For example:  CComPtr<IPin> pPin = GetInPin(pFilter,0);
//
static IPin * GetInPin( IBaseFilter * pFilter, int nPin )
{
    CComPtr<IPin> pComPin;
    GetPin(pFilter, PINDIR_INPUT, nPin, &pComPin);
    return pComPin;
}


static IPin * GetOutPin( IBaseFilter * pFilter, int nPin )
{
    CComPtr<IPin> pComPin;
    GetPin(pFilter, PINDIR_OUTPUT, nPin, &pComPin);
    return pComPin;
}


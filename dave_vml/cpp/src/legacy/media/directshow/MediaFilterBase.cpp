/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include "MediaFilterBase.hpp"

#include <strstream>

#ifdef HIGH_PERF_LOG
#include "HighPerfLog.hpp"
extern ait::HighPerfLog gLog;
#endif

namespace ait
{

MediaFilterBase::MediaFilterBase( REFCLSID RenderClass, LPUNKNOWN pUnk, HRESULT *phr )
  : CBaseVideoRenderer(RenderClass, NAME("AI Image Renderer"), pUnk, phr)
{
  // Store and AddRef the texture for our use.
  *phr = S_OK;
}


MediaFilterBase::~MediaFilterBase()
{
}


HRESULT MediaFilterBase::CheckMediaType(const CMediaType *pmt)
{
  HRESULT   hr = E_FAIL;

  VIDEOINFO *pvi;
    
  // Reject the connection if this is not a video type
  if( *pmt->FormatType() != FORMAT_VideoInfo ) {
    return E_INVALIDARG;
  }
    
  // Only accept RGB24
  pvi = (VIDEOINFO *)pmt->Format();
  if(IsEqualGUID( *pmt->Type(),    MEDIATYPE_Video)  &&
     IsEqualGUID( *pmt->Subtype(), MEDIASUBTYPE_RGB24))
    //IsEqualGUID( *pmt->Subtype(), MEDIASUBTYPE_RGB32)) This has been tested, and it is slower.
    {
      hr = S_OK;
    }
  return hr;
}


HRESULT MediaFilterBase::SetMediaType(const CMediaType *pmt)
{
  //    HRESULT hr;

  // Retrieve the size of this media type
  VIDEOINFO *pviBmp;                      // Bitmap info header
  pviBmp = (VIDEOINFO *)pmt->Format();
  LONG lVidWidth  = pviBmp->bmiHeader.biWidth;
  LONG lVidHeight = abs(pviBmp->bmiHeader.biHeight);
  mFrameSize.set(lVidWidth, lVidHeight);
  return S_OK;
}

void 
MediaFilterBase::copyFrame(IMediaSample *pSample, MediaFrameInfo* pMediaFrameInfo)
{
  BYTE  *pBmpBuffer;     // Bitmap buffer
  REFERENCE_TIME sampleStartTime;
  REFERENCE_TIME sampleEndTime;
  
  // Get the video bitmap buffer
  pSample->GetPointer( &pBmpBuffer );

  pSample->GetTime(&sampleStartTime, &sampleEndTime);

  //pMediaFrameInfo->beginTime = (double)sampleStartTime/10000000;
  //pMediaFrameInfo->endTime = (double)sampleEndTime/10000000;
  pMediaFrameInfo->frameIndex = (int)((float)(sampleStartTime)/(float)(sampleEndTime-sampleStartTime));

  if (pMediaFrameInfo->pRGBImage)
  {
    Image32& frame = *(pMediaFrameInfo->pRGBImage);
    frame.resize(mFrameSize(0),mFrameSize(1));
    frame.setInternalFormat(mInternalImageFormat);

    unsigned char *pFrame = ((unsigned char *)frame.pointer()) + 4*mFrameSize(0)*(mFrameSize(1)-1);
    int skip = -2*(4*mFrameSize(0));

    if (frame.getInternalFormat() == Image32::PV_BGRA)
    {
      for (int y = 0; y < mFrameSize(1); y++) 
      {
        for (int x = 0; x < mFrameSize(0); x++) 
        {	
          pFrame[0] = pBmpBuffer[0];
          pFrame[1] = pBmpBuffer[1];
          pFrame[2] = pBmpBuffer[2];
					pFrame[3] = 0;
          pBmpBuffer += 3;
          pFrame += 4;
        }
        pFrame += skip;
      }
    }
    else
    {
      for (int y = 0; y < mFrameSize(1); y++) 
      {
        for (int x = 0; x < mFrameSize(0); x++) 
        {	
          pFrame[0] = pBmpBuffer[2];
          pFrame[1] = pBmpBuffer[1];
          pFrame[2] = pBmpBuffer[0];
					pFrame[3] = 0;
          pBmpBuffer += 3;
          pFrame += 4;
        }
        pFrame += skip;
      }
    }
  }
}

} // namespace ait
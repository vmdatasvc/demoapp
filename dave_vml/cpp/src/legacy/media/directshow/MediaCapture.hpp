/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef MEDIACAPTURE_HPP
#define MEDIACAPTURE_HPP

#include <Media.hpp>
#include <ImageAcquireProperty.hpp>
#include <vector>

namespace ait
{

class MediaCapture : public Media
{
public:
  MediaCapture();
  virtual ~MediaCapture();

  void setSettingsPath(const std::string& settingsPath0) { settingsPath = settingsPath0; }
	
	void setRenderMethod(Media::BufferType renderMethod = BLOCKING_DOUBLE) {mRenderMethod = renderMethod;}

  HRESULT initCaptureDevice(const std::string& deviceName, int deviceIndex, const std::string& url);

  ImageAcquirePropertyBag *getPropertyBag() { return pParams; }

  void setDirectMediaParam(ImageAcquireProperty& param);
  void getDirectMediaParam(ImageAcquireProperty& param);

private:

  /**
   * Look for the capture device with the identified index and name.
   * @param ppSrcFilter [out] Return a pointer to the filter interface.
   * @param uniqueName [out] Return a unique name that identifies this device.
   * @param deviceName [in] Name of the device, this is the friendly name that appears
   * in the GUI. If this name is "NONE" it will match any device.
   * @param deviceIndex [in] For several devices that match, it will return the device
   * at the position indicated by this index. The first position is 0.
   */
  HRESULT findCaptureDevice(IBaseFilter ** ppSrcFilter, 
                            std::string& uniqueName,
                            const std::string& deviceName, 
                            int deviceIndex);

  std::vector<IMoniker *> rgpmVideoMenu;

  IAMStreamConfig *pVSC;      // for video cap
  IAMVideoProcAmp *pVPA;
  IAMCameraControl *pCC;

  ImageAcquirePropertyBag *pParams;

	Media::BufferType mRenderMethod;

  std::string settingsPath;
};

} // namespace ait


#endif

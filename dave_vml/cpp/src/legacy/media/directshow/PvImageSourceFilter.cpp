/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

//#error "See readme.txt file for instructions on how to compile this filter..."

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif


#include <streams.h>
#include <olectl.h>

#include "PvImageSourceFilter.hpp"

// This must included last so it defines the GUIDs only once.
#include <initguid.h>
#include "PvImageStream.hpp"

namespace ait 
{

// Setup data

CUnknown *
PvImageSourceFilter::CreateInstance(LPUNKNOWN lpunk, HRESULT *phr)
{
    ASSERT(phr);

    CUnknown *punk = new PvImageSourceFilter(lpunk, phr);
    if(punk == NULL)
    {
        if(phr)
            *phr = E_OUTOFMEMORY;
    }
    return punk;

} // CreateInstance

PvImageSourceFilter::PvImageSourceFilter(LPUNKNOWN lpunk, HRESULT *phr) :
    CSource(NAME("Image Source"), lpunk, CLSID_PvImageSourceFilter)
{
  ASSERT(phr);
  CAutoLock cAutoLock(&m_cStateLock);
  
  m_paStreams = (CSourceStream **) new PvImageStream*[1];
  if(m_paStreams == NULL)
  {
    if(phr)
      *phr = E_OUTOFMEMORY;
    
    return;
  }
  
  m_paStreams[0] = new PvImageStream(phr, this, L"Image Stream");
  if(m_paStreams[0] == NULL)
  {
    if(phr)
      *phr = E_OUTOFMEMORY;
    
    return;
  }
  
}

//
// OPERATIONS
//

STDMETHODIMP 
PvImageSourceFilter::QueryInterface(REFIID riid, void ** ppv)
{
    CheckPointer(ppv,E_POINTER);
    
    if( riid == CLSID_PvImageSourceFilter || riid == IID_IUnknown) 
    {
        *ppv = (void *) static_cast< PvImageSourceFilter * > ( this );
        return NOERROR;
    }

    if (riid == IID_IBaseFilter)
    {
        *ppv = (void *) static_cast< IBaseFilter * > ( this );
        return NOERROR;
    }
    
    return E_NOINTERFACE;
}
  
void
PvImageSourceFilter::write(const Image32& img)
{
  getStream()->write(img);
}

void
PvImageSourceFilter::writeEndOfFile()
{
  getStream()->writeEndOfFile();
}

void 
PvImageSourceFilter::writePreviousFrame()
{
  getStream()->writePreviousFrame();
}

void
PvImageSourceFilter::setImageSize(const Vector2i& size)
{
  getStream()->setImageSize(size);
}

void 
PvImageSourceFilter::setFramesPerSecond(double fps)
{
  getStream()->setFramesPerSecond(fps);
}

PvImageStream *
PvImageSourceFilter::getStream()
{
  return (PvImageStream *)m_paStreams[0];
}


//
// ACCESS
//

//
// INQUIRY
//

}; // namespace ait


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifdef USE_WATCHNET

#pragma warning(disable:4786)

#include <afxwin.h>

#include "ImageAcquireDeviceWatchnet.hpp"

#include <watchnet/NetSDK.h>

class CWatchnetWnd : public CWnd
{
public:
  CWatchnetWnd(ait::ImageAcquireDeviceWatchnet *pD, HWND hwnd) : pDevice(pD) { Attach(hwnd); };
  ait::ImageAcquireDeviceWatchnet *pDevice;
};

static void CALLBACK gGeneralProc(LPVOID lpOwner, int nSiteKey, int nState, int nChannel, int nCamera, int nWidth, int nHeight, LPBYTE lpData, DWORD dwLen, LPVOID lpTextHeader, LPBYTE lpTextData)
{
  if(nState >= STATEDEFINE_INFO_CONNECTED)
	{
		// general information
		return;
	}

	if(nState >= STATEDEFINE_SVR_REGISTEROK)
	{
		// ip server information when you use an ip server
		return;
	}

  // Only 1 camera for now.
	if(nChannel >= 2) return;


	((CWatchnetWnd*)lpOwner)->pDevice->callback(nWidth, nHeight, lpData);
}

namespace ait
{

class WatchnetProperty : public ImageAcquireProperty
{
public:

  WatchnetProperty(ImageAcquireDeviceWatchnet *pDevice0) : pDevice(pDevice0) {};
  virtual ~WatchnetProperty() {}

  virtual ImageAcquireProperty *clone() { return new WatchnetProperty(*this); }

  void setToDevice() { pDevice->setWatchnetProperty(*this,true); };
  void getFromDevice() { pDevice->setWatchnetProperty(*this,false); };

protected:

  ImageAcquireDeviceWatchnet *pDevice;
};

ImageAcquireDeviceWatchnet::ImageAcquireDeviceWatchnet() : mpNetSdk(0), mpCWnd(0)
{
}


ImageAcquireDeviceWatchnet::~ImageAcquireDeviceWatchnet()
{
	unInitialize();
};


bool ImageAcquireDeviceWatchnet::initialize(int numPrevBuffers, int numConsecutiveFrameGrabs)
{
	assert(numPrevBuffers >= 0);

	if (isInitialized)
		unInitialize();


  mpNetSdk = new CNetSDK();
  mpCWnd = new CWatchnetWnd(this,hwnd1);
  mpNetSdk->Initialize(mpCWnd);
  mpNetSdk->SetGeneralProc(gGeneralProc);

	//Initialize the display window.
	displayFrameFlag = false;

	//Initialize base parameters.
	numPreviousBuffers = numPrevBuffers;

	//Initialize the number of consecutive frames to grab
	mNumConsecutiveFrameGrabs = numConsecutiveFrameGrabs;

	//Initialize the resolution.
	frameWidth = 0;
	frameHeight = 0;

  // Add default property bag
  pParams = new ImageAcquirePropertyBag(deviceInfo->getRegPath(), 
  ImageAcquirePropertyPtr(new WatchnetProperty(this)));

  Settings set(deviceInfo->getRegPath());

  mSkipFrameInterval = set.getInt("skipFrameInterval",1);

  mFps = pParams->get(ImageAcquireProperty::FPS)->getValuef();

	//Initialize buffers.

  initBuffers(frameWidth,frameHeight,numPrevBuffers);
  for (int i = 0; i < mFrames.capacity(); i++)
  {
    mFrames[i]->pRGBImage->setInternalFormat(Image32::PV_BGRA);
  }

	frameCounter = 0;

	isInitialized = true;

  fillBuffers();

	return true;
};

bool ImageAcquireDeviceWatchnet::unInitialize()
{
	isInitialized = false;

  mFrames.clear();
  if (pParams)
  {
    delete pParams;
    pParams = NULL;
  }

  if (mpNetSdk)
  {
    mpNetSdk->Uninitialize();
    delete mpNetSdk;
    mpNetSdk = 0;
  }
  if (mpCWnd)
  {
    delete mpCWnd;
  }

	return true;
};

void
ImageAcquireDeviceWatchnet::callback(int width, int height, Uint8* pData)
{
  mpFrame.reset(new Image32(width,height));
  Image32& img = *mpFrame;
  int numPixels = width*height;
  for (int i = 0; i < numPixels; i++)
  {
    img(i) = pData[i];
  }
}

int ImageAcquireDeviceWatchnet::grabFrame()
{
  assert(isInitialized);

  while (mpFrame.get() == NULL)
  {
    MSG msg;
    GetMessage(&msg, hwnd1, 0, 0);
  }
  
  // Save image here 
  *(mFrames.front()->pRGBImage) = *mpFrame;

  //Can advance here since frame is copied and the bytes were swapped.
  nextFrame();

  return 0;
};

void ImageAcquireDeviceWatchnet::setWatchnetProperty(ImageAcquireProperty& param, bool set)
{
  long refType = -1;
  long controlType = -1;

  switch (param.getCode())
  {
  case ImageAcquireProperty::BRIGHTNESS:
  case ImageAcquireProperty::CONTRAST:
  case ImageAcquireProperty::HUE:
  case ImageAcquireProperty::SATURATION:
  case ImageAcquireProperty::GAIN:
  case ImageAcquireProperty::WHITEBALANCE:
  case ImageAcquireProperty::BACKLIGHTCOMPENSATION:
  case ImageAcquireProperty::SHARPNESS:
  case ImageAcquireProperty::GAMMA:
  case ImageAcquireProperty::COLORENABLE:
  case ImageAcquireProperty::PAN:
  case ImageAcquireProperty::TILT:
  case ImageAcquireProperty::ROLL:
  case ImageAcquireProperty::ZOOM:
  case ImageAcquireProperty::EXPOSURE:
  case ImageAcquireProperty::IRIS:
  case ImageAcquireProperty::FOCUS:
    break;
  case ImageAcquireProperty::SOURCEWIDTH:
  case ImageAcquireProperty::SOURCEHEIGHT:
    if (!set)
    {
      param.setAvailable(true);
      param.setAuto(false);
      param.setInteractive(false);
      if (param.getCode() == ImageAcquireProperty::SOURCEWIDTH)
      {
        param.setValuef(frameWidth,false);
        param.setDefault(frameWidth);
      }
      else
      {
        param.setValuef(frameHeight,false);
        param.setDefault(frameHeight);
      }
    }
    return;
  case ImageAcquireProperty::FPS:
    if (!set)
    {
      param.setAvailable(true);
      param.setAuto(false);
      param.setInteractive(false);
      param.setValuef(mFps,false);
      param.setDefault(30);
    }
    else
    {
      mFps = param.getValuef(false);
    }
    return;
  }

  param.setAvailable(false);
}

} // namespace ait

#endif // USE_WATCHNET
/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include "VideoReaderIppShared.hpp"

#include "VideoReaderIpp.hpp"

namespace ait 
{

SHAREDAPI VideoReaderIppShared::VideoReaderIppShared()
{
  mpVideoReader = new VideoReaderIpp();
}

SHAREDAPI VideoReaderIppShared::~VideoReaderIppShared()
{
  VideoReaderIpp *pVr = static_cast<VideoReaderIpp*>(mpVideoReader);
  delete pVr;
}

//
// OPERATIONS
//

void 
SHAREDAPI VideoReaderIppShared::open(const char *fname)
{
  VideoReaderIpp *pVr = static_cast<VideoReaderIpp*>(mpVideoReader);
  pVr->open(fname);
}

void 
SHAREDAPI VideoReaderIppShared::close()
{
  VideoReaderIpp *pVr = static_cast<VideoReaderIpp*>(mpVideoReader);
  pVr->close();
}

bool 
SHAREDAPI VideoReaderIppShared::readFrame(unsigned char *pImageData, int pitch, int imageFormat)
{
  VideoReaderIpp *pVr = static_cast<VideoReaderIpp*>(mpVideoReader);
  return pVr->readFrame(pImageData,pitch,imageFormat);
}

void 
SHAREDAPI VideoReaderIppShared::deleteIndex(const char *fname)
{
  VideoReaderIpp *pVr = static_cast<VideoReaderIpp*>(mpVideoReader);
  pVr->deleteIndex(fname);
}

int
SHAREDAPI VideoReaderIppShared::getNumberOfFrames()
{
  VideoReaderIpp *pVr = static_cast<VideoReaderIpp*>(mpVideoReader);
  return (int)pVr->getNumberOfFrames();
}

void
SHAREDAPI VideoReaderIppShared::setCurrentFrameNumber(int frame)
{
  VideoReaderIpp *pVr = static_cast<VideoReaderIpp*>(mpVideoReader);
  pVr->setCurrentFrameNumber(frame);
}

int
SHAREDAPI VideoReaderIppShared::getCurrentFrameNumber()
{
  VideoReaderIpp *pVr = static_cast<VideoReaderIpp*>(mpVideoReader);
  return (int)pVr->getCurrentFrameNumber();
}

double
SHAREDAPI VideoReaderIppShared::getFramesPerSecond()
{
  VideoReaderIpp *pVr = static_cast<VideoReaderIpp*>(mpVideoReader);
  return (double)pVr->getFramesPerSecond();
}

void
SHAREDAPI VideoReaderIppShared::getBufferSize(int& width, int& height)
{
  VideoReaderIpp *pVr = static_cast<VideoReaderIpp*>(mpVideoReader);
  Vector2i bufSize = pVr->getBufferSize();
  width = bufSize.x;
  height = bufSize.y;
}

void
SHAREDAPI VideoReaderIppShared::nextFrame()
{
  VideoReaderIpp *pVr = static_cast<VideoReaderIpp*>(mpVideoReader);
  pVr->nextFrame();
}

int
SHAREDAPI VideoReaderIppShared::getIFrames(int *pIFrames)
{
  VideoReaderIpp *pVr = static_cast<VideoReaderIpp*>(mpVideoReader);
  std::vector<Int64> iFrames;
  pVr->getIFrames(iFrames);
  if (pIFrames)
  {
    for (int i = 0; i < iFrames.size(); i++)
    {
      pIFrames[i] = iFrames[i];
    }
  }
  return iFrames.size();
}

double
SHAREDAPI VideoReaderIppShared::getScanProgress()
{
  VideoReaderIpp *pVr = static_cast<VideoReaderIpp*>(mpVideoReader);
  return pVr->getScanProgress();
}


bool
SHAREDAPI VideoReaderIppShared::isEndOfFile()
{
  VideoReaderIpp *pVr = static_cast<VideoReaderIpp*>(mpVideoReader);
  return pVr->isEndOfFile();
}

void
SHAREDAPI VideoReaderIppShared::abort()
{
  VideoReaderIpp *pVr = static_cast<VideoReaderIpp*>(mpVideoReader);
  return pVr->abort();
}


//
// ACCESS
//

//
// INQUIRY
//

}; // namespace ait


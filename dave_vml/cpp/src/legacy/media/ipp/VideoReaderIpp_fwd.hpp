/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VideoReaderIpp_fwd_HPP
#define VideoReaderIpp_fwd_HPP

#include <boost/shared_ptr.hpp>

namespace ait
{

class VideoReaderIpp;
typedef boost::shared_ptr<VideoReaderIpp> VideoReaderIppPtr;

}; // namespace ait

#endif // VideoReaderIpp_fwd_HPP


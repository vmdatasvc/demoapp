/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VideoReaderIppStub_HPP
#define VideoReaderIppStub_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

#include "VideoReader.hpp"
#include "VideoReaderIppShared.hpp"

// AIT INCLUDES
//

#include <String.hpp>

// FORWARD REFERENCES
//

#include "VideoReaderIppStub_fwd.hpp"

namespace ait 
{


/**
 * Intel IPP implementation of the video reader. Do not instantiate this class, use
 * instead the factory member in the base class. Refer to base class for documentation
 * on inherited members.
 */
class VideoReaderIppStub : public VideoReader
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  VideoReaderIppStub() { mpVideoReader = new VideoReaderIppShared(); }

  /**
   * Destructor
   */
  virtual ~VideoReaderIppStub() { delete mpVideoReader; }


  //
  // OPERATIONS
  //

public:

  virtual void open(const std::string& fname) { mpVideoReader->open(fname.c_str()); }
  virtual void close() { mpVideoReader->close(); }
  virtual bool readFrame(Image32& img);
  virtual void deleteIndex(const std::string& fname) { mpVideoReader->deleteIndex(fname.c_str()); }
  virtual Int64 getNumberOfFrames() { return mpVideoReader->getNumberOfFrames(); }
  virtual void setCurrentFrameNumber(Int64 frame) { mpVideoReader->setCurrentFrameNumber((int)frame); }
  virtual Int64 getCurrentFrameNumber() { return mpVideoReader->getCurrentFrameNumber(); }
  virtual double getFramesPerSecond() { return mpVideoReader->getFramesPerSecond(); }
  virtual Vector2i getBufferSize() { int w,h; mpVideoReader->getBufferSize(w,h); return Vector2i(w,h); }
  virtual void getIFrames(std::vector<Int64>& iFrames);
  virtual bool isEndOfFile() { return mpVideoReader->isEndOfFile(); }
  virtual void nextFrame() { mpVideoReader->nextFrame(); }
  virtual void abort() { mpVideoReader->abort(); }
  virtual double getScanProgress() { return mpVideoReader->getScanProgress(); }

protected:

  VideoReaderIppShared *mpVideoReader;

};


}; // namespace ait

#endif // VideoReaderIppStub_HPP


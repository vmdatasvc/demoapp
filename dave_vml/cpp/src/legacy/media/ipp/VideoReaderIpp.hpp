/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VideoReaderIpp_HPP
#define VideoReaderIpp_HPP

// SYSTEM INCLUDES
//

#include <boost/utility.hpp>

// AIT INCLUDES
//

#include <String.hpp>
#include <ait/Vector3.hpp>

#if defined (WIN32) || defined (WIN64)
#include <conio.h>
#endif // WIN32

#include <stdio.h>
#include <stdarg.h>

// vm_debug will redefine this.
#ifdef assert
#undef assert
#endif

#include <ippi.h>
#include <ippcore.h>
#include <vector>
#include <vm_time.h>
#include <vm_sys_info.h>
#include <umc_sys_info.h>
#include <codec_pipeline.h>
#include <umc_color_space_converter.h>
#include <umc_video_data.h>


// FORWARD REFERENCES
//

#include "VideoReaderIpp_fwd.hpp"

namespace ait 
{


/**
 * Intel IPP implementation of the video reader. Do not instantiate this class, use
 * instead the factory member in the base class. Refer to base class for documentation
 * on inherited members.
 */
class VideoReaderIpp
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  VideoReaderIpp();

  /**
   * Destructor
   */
  virtual ~VideoReaderIpp();


  //
  // OPERATIONS
  //

public:

  virtual void open(const std::string& fname);
  virtual void close();
  bool readFrame(unsigned char *pImageData, int pitch, int imageFormat);
  virtual void deleteIndex(const std::string& fname);

private:

  void copySampleToImage(unsigned char *pImageData, int pitch, int imageFormat);

  //
  // ACCESS
  //

public:

  virtual Int64 getNumberOfFrames();
  virtual void setCurrentFrameNumber(Int64 frame);
  virtual Int64 getCurrentFrameNumber();
  virtual double getFramesPerSecond();
  virtual Vector2i getBufferSize();
  virtual void getIFrames(std::vector<Int64>& iFrames);
  virtual bool isEndOfFile();
  virtual void nextFrame();
  virtual double getScanProgress() { return mProgress; }
  virtual void abort() { mAbortIFrameScan = true; }


protected:

  std::string getIndexFileName(const std::string& videoName);
  void readIFrameIndex(const std::string& fname);
  void writeIFrameIndex(const std::string& videoFname, const std::string& indexFname);
  void open(const std::string& fname, bool loadIFrameIndex, bool scanFrameTypeOnly);

  //
  // INQUIRY
  //

public:


  //
  // ATTRIBUTES
  //

protected:

  class OffsetFrameInfo
  {
  public:
    OffsetFrameInfo(Int64 fnumber, Int64 offset) : frameNumber(fnumber), fileOffset(offset) {};
    Int64 frameNumber;
    Int64 fileOffset;
  };

  /**
   * The current frame number.
   */
  Int64 mFrameNumber;

  /**
   * The last frame number that was read from the stream.
   */
  Int64 mLastFrameNumber;

  /**
   * The total number of frames.
   */
  Int64 mNumberOfFrames;

  /**
   * Save here the frame size.	
   */
  Vector2i mBufferSize;

  /**
   * Frame number of the current sample.
   */
  Int64 mCurrentSampleFrameNumber;

  /**
   * 24-bit RGB sample image data.
   */
  std::vector<Uint8> mSampleImageData;

  /**
   * Array with I-Frame indices.
   */
  std::vector<OffsetFrameInfo> mIFrames;

  /**
   * Number of frames per second on this video.
   */
  double mFramesPerSecond;

  double mFirstFrameStartTime;

  /**
   * An estimate of the progress based on the current file position.
   * This can't be used to estimate the current frame.
   */
  double mProgress;

  bool mIsIFrame;

  bool mIsEndOfFile;

  UMC::LocalReaderContext mReadContext;
  UMC::DataReader *mpDataReader;
  UMC::Splitter *mpSplitter;
  UMC::SplitterInfo mSplitterInfo;
  UMC::ColorSpaceConverter mColorConverter;
  UMC::VideoDecoder *mpVideoDecoder;
  UMC::VideoData mVideoData;
  UMC::MediaDataEx mMediaData;

  UMC::Status decodeFrame(UMC::MediaData* pInData,
                     UMC::VideoData& rOutData);

  /**
   * This function will scan the file for I-Frames.
   */
  void scanIFrames(const std::string& videoFname);

  bool mIsMpegStream;

  int mRepeatFrameCount;

  bool mAbortIFrameScan;

};


}; // namespace ait

#endif // VideoReaderIpp_HPP


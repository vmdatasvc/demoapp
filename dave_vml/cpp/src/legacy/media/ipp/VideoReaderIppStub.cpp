/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "VideoReaderIppStub.hpp"
#include <ImageFormatTypes.hpp>

namespace ait 
{

bool 
VideoReaderIppStub::readFrame(Image32& img)
{
  int w,h;
  mpVideoReader->getBufferSize(w,h);
  img.resize(w,h);
  return mpVideoReader->readFrame((unsigned char*)img.pointer(),img.width(),ImageFormatTypes::RGB32);
}

void
VideoReaderIppStub::getIFrames(std::vector<Int64>& iFrames)
{
  std::vector<int> iFramesI;
  int iFramesSize = mpVideoReader->getIFrames(0);
  iFramesI.resize(iFramesSize);
  mpVideoReader->getIFrames(&(iFramesI[0]));
  iFrames.resize(iFramesSize);
  for (int i = 0; i < iFramesI.size(); i++)
  {
    iFrames[i] = iFramesI[i];
  }
}


}; // namespace ait


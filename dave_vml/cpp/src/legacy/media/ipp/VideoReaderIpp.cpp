/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include "VideoReaderIpp.hpp"

#include <strutil.hpp>

#include <shlobj.h>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/convenience.hpp>
#include <boost/lexical_cast.hpp>

#include <Socket.hpp>

#include <umc_avi_spl_base.h>
#include <umc_avi_index.h>

namespace ait 
{

VideoReaderIpp::VideoReaderIpp()
: mFrameNumber(-1),
  mLastFrameNumber(-1),
  mpDataReader(0),
  mpSplitter(0),
  mpVideoDecoder(0),
  mIsIFrame(false),
  mNumberOfFrames(0),
  mProgress(0),
  mIsEndOfFile(false),
  mIsMpegStream(false),
  mAbortIFrameScan(false)
{
}

VideoReaderIpp::~VideoReaderIpp()
{
  close();
}

//
// OPERATIONS
//

void 
VideoReaderIpp::open(const std::string& fname)
{
  open(fname,true,false);
}

void 
VideoReaderIpp::open(const std::string& fname, bool loadIFrameIndex, bool scanFrameTypeOnly)
{

  std::string ext = ait::tolower(ait::get_file_extension(fname));

  mIsMpegStream = ext == ".mpg" || ext == ".mpeg";

  if (loadIFrameIndex)
  {
    std::string iFramesIndexFname = getIndexFileName(fname);

    boost::filesystem::path path = boost::filesystem::path(iFramesIndexFname, boost::filesystem::native);

    bool mustWrite = mIsMpegStream && !boost::filesystem::exists(path);

    if (!mustWrite && mIsMpegStream)
    {
      try
      {
        readIFrameIndex(iFramesIndexFname);
      }
      catch (...)
      {
        // Remove does not throw if the path doesn't exist.
        boost::filesystem::remove(path);
        mustWrite = true;
      }
    }

    if (mustWrite)
    {
      scanIFrames(fname);
      writeIFrameIndex(fname,iFramesIndexFname);
    }
  }

  vm_string_strcpy(mReadContext.m_szFileName, fname.c_str());

  UMC::Status umcRes = UMC::UMC_OK;
  UMC::MediaDataEx firstFrameV;

  umcRes = CodecPipeline::SelectDataReader(
    mReadContext,
    mpDataReader,
    0);

  if (umcRes != UMC::UMC_OK)
  {
    close();
    throw std::exception(aitSprintf("Error opening file: %s",fname.c_str()).c_str());
  }

  umcRes = CodecPipeline::SelectSplitter(mpDataReader,
    UMC::FLAG_VSPL_VIDEO_HEADER_REQ | 
    UMC::FLAG_VSPL_COMPATIBLE | 
    UMC::VIDEO_SPLITTER |
    (scanFrameTypeOnly ? 0 : UMC::FLAG_VSPL_NO_END_OF_STREAM), // No audio enabled (UMC::AUDIO_SPLITTER)
    mpSplitter,
    firstFrameV,
    0,
    0);

  if (umcRes != UMC::UMC_OK)
  {
    close();
    throw std::exception(aitSprintf("Splitter not found: %s",fname.c_str()).c_str());
  }

  mFirstFrameStartTime = firstFrameV.GetTime();

  if (mFirstFrameStartTime < 0)
  {
    mFirstFrameStartTime = 0;
  }

  umcRes = mpSplitter->GetInfo(&mSplitterInfo);

  if (umcRes != UMC::UMC_OK)
  {
    close();
    throw std::exception(aitSprintf("Splitter info not found: %s",fname.c_str()).c_str());
  }

  if (!(mSplitterInfo.m_splitter_flags & UMC::VIDEO_SPLITTER))
  {
    close();
    throw std::exception(aitSprintf("No video is present: %s",fname.c_str()).c_str());
  }

  if (UMC::UNDEF_VIDEO == mSplitterInfo.m_video_info.stream_type)
  {
    close();
    throw std::exception(aitSprintf("No decoder found for video: %s",fname.c_str()).c_str());
  }

  // Check again if it is mpeg, the first check was made only using the
  // file extension, this is more reliable.
  mIsMpegStream = (mSplitterInfo.m_system_info.stream_type & UMC::MPEGx_SYSTEM_STREAM) != 0;

  umcRes = CodecPipeline::SelectVideoDecoder(mSplitterInfo.m_video_info,
    firstFrameV,
    UMC::RGB24,
    UMC::FLAG_CCNV_CONVERT,
    IPPI_INTER_NN,
    0,
    0,
    UMC::FLAG_VDEC_NO_PREVIEW | 
    UMC::FLAG_VDEC_COMPATIBLE | 
    (scanFrameTypeOnly ? UMC::FLAG_VDEC_SCAN_FRAME_TYPE_ONLY : UMC::FLAG_VDEC_REORDER),
    mColorConverter,
    mpVideoDecoder);

  if (umcRes != UMC::UMC_OK)
  {
    close();
    throw std::exception(aitSprintf("Codec not found: %s",fname.c_str()).c_str());
  }

  if((mSplitterInfo.m_video_info.stream_type & UMC::H264_VIDEO) &&
      (mSplitterInfo.m_system_info.stream_type & UMC::AVI_STREAM))
  {
    mBufferSize.set(mSplitterInfo.m_video_info.clip_info.width,
      mSplitterInfo.m_video_info.clip_info.height);
  }
  else
  {
    UMC::VideoDecoderParams vParams;
    PvUtil::sleep(0.1);
    umcRes = mpVideoDecoder->GetInfo(&vParams);
    if(!(int)mSplitterInfo.m_video_info.framerate)mSplitterInfo.m_video_info.framerate = vParams.info.framerate;
    if(!mSplitterInfo.m_video_info.clip_info.width)mSplitterInfo.m_video_info.clip_info.width = vParams.info.clip_info.width;
    if(!mSplitterInfo.m_video_info.clip_info.height)mSplitterInfo.m_video_info.clip_info.height = vParams.info.clip_info.height;
    mBufferSize.set(vParams.info.clip_info.width, vParams.info.clip_info.height);
  }

  mFramesPerSecond = mSplitterInfo.m_video_info.framerate;

  mVideoData.Init(mSplitterInfo.m_video_info.clip_info.width,mSplitterInfo.m_video_info.clip_info.height,UMC::RGB24);

  mSampleImageData.resize(mSplitterInfo.m_video_info.clip_info.width*mSplitterInfo.m_video_info.clip_info.height*3);
  mVideoData.SetBufferPointer(&mSampleImageData[0],mSampleImageData.size());
  mVideoData.SetPitch(mSplitterInfo.m_video_info.clip_info.width*3);

  if (mNumberOfFrames == 0)
  {
    mNumberOfFrames = (int)(mSplitterInfo.m_video_info.duration*mFramesPerSecond)+1;
  }

  // Try to get the Key frames from AVI files
  if (mSplitterInfo.m_system_info.stream_type & UMC::AVI_STREAM)
  {
    UMC::AVIIndex *pAviIndex = static_cast<UMC::AVIIndex*>(mSplitterInfo.m_system_info.pExtraInfo);

    if (pAviIndex)
    {
      mIFrames.clear();

      Int64 frameNum = 0;

      for (int i = 0; i < pAviIndex->GetIndexTableSize(); i++)
      {
        UMC::AVIIndex::IndTblEntry ite;
        pAviIndex->GetIndexTableEntry(i,ite);
        if (ite.ChunkType == AVI_FOURCC_00DB || ite.ChunkType == AVI_FOURCC_00DC)
        {
          if (ite.ulFlags & 16)
          {
            mIFrames.push_back(OffsetFrameInfo(frameNum,0));
          }
          frameNum++;
        }
      }
    }
  }

  mRepeatFrameCount = 0;

  mProgress = 1;

  //PVMSG("Duration: %.2f [%i:%i] %i\n",mSplitterInfo.m_video_info.duration, ((int)mSplitterInfo.m_video_info.duration)/60, ((int)mSplitterInfo.m_video_info.duration)%60, getNumberOfFrames());
}

void 
VideoReaderIpp::close()
{
  if (mpSplitter)
  {
    mpSplitter->Close();
    delete mpSplitter;
    mpSplitter = 0;
  }

  if (mpDataReader)
  {
    mpDataReader->Close();
    delete mpDataReader;
    mpDataReader = 0;
  }

  if (mpVideoDecoder)
  {
    mpVideoDecoder->Close();
    delete mpVideoDecoder;
    mpVideoDecoder = 0;
  }

  mColorConverter.Close();
  mVideoData.Close();
  mMediaData.Close();

}

void 
VideoReaderIpp::getIFrames(std::vector<Int64>& iFrames)
{
  iFrames.resize(mIFrames.size());
  for (int i = 0; i < mIFrames.size(); i++)
  {
    iFrames[i] = mIFrames[i].frameNumber;
  }
}

void
VideoReaderIpp::copySampleToImage(unsigned char *pImageData, int pitch, int imageFormat)
{
  unsigned char * pBmpBuffer = &mSampleImageData[0];
  int w = mBufferSize.x, h = mBufferSize.y;

  unsigned char *pFrame = pImageData; // + 4*w*(h-1);
  int skip = 0; //2*(4*w);

  if (true)
  {
    for (int y = 0; y < h; y++) 
    {
      for (int x = 0; x < w; x++) 
      {	
        pFrame[0] = pBmpBuffer[0];
        pFrame[1] = pBmpBuffer[1];
        pFrame[2] = pBmpBuffer[2];
				pFrame[3] = 0;
        pBmpBuffer += 3;
        pFrame += 4;
      }
      pFrame += skip;
    }
  }
  else
  {
    for (int y = 0; y < h; y++) 
    {
      for (int x = 0; x < w; x++) 
      {	
        pFrame[0] = pBmpBuffer[2];
        pFrame[1] = pBmpBuffer[1];
        pFrame[2] = pBmpBuffer[0];
				pFrame[3] = 0;
        pBmpBuffer += 3;
        pFrame += 4;
      }
      pFrame += skip;
    }
  }
}

UMC::Status
VideoReaderIpp::decodeFrame(UMC::MediaData* pInData,
                     UMC::VideoData& rOutData)
{
    UMC::Status umcRes = UMC::UMC_OK;

    if (pInData && pInData->GetDataSize() == 1)
    {
      mRepeatFrameCount++;
      return UMC::UMC_NOT_ENOUGH_DATA;
    }

    int sizeBefore = pInData ? pInData->GetDataSize() : 0;

    umcRes = mpVideoDecoder->GetFrame(pInData, &rOutData);

    int sizeAfter = pInData ? pInData->GetDataSize() : 0;

    //PVMSG("Passed to decoder [%i] remain [%i] result: %i\n",sizeBefore,sizeAfter,umcRes);

    if (umcRes == UMC::UMC_OK)
    {
      mIsIFrame = rOutData.m_FrameType == UMC::I_PICTURE;

      double t0,t1;
      rOutData.GetTime(t0,t1);
      t0 -= mFirstFrameStartTime;

      std::string ftype = "";
      switch (mVideoData.m_FrameType)
      {
      case UMC::I_PICTURE:
        ftype = "I";
        break;
      case UMC::P_PICTURE:
        ftype = "P";
        break;
      case UMC::B_PICTURE:
        ftype = "B";
        break;
      }

      /*
      PVMSG(VM_STRING("Decoded [%s]\n"),
        ftype.c_str());
        */
    }
    else
    {
      /*
      if (pInData)
      {
        PVMSG(VM_STRING("Decoder Error: %i, %lf, Size: %i\n"),
          umcRes, pInData->GetTime(), (int)pInData->GetBufferSize());
      }
      else
      {
        PVMSG(VM_STRING("Decoder Error: %i\n"), umcRes);
      }
      */

    }

    return umcRes;
}

bool 
VideoReaderIpp::readFrame(unsigned char *pImageData, int pitch, int imageFormat)
{
  if (isEndOfFile())
  {
    return false;
  }

  if (mFrameNumber < 0)
  {
    mFrameNumber = 0;
  }

  UMC::Status umcRes = UMC::UMC_OK;
  UMC::Status umcSplitRes = UMC::UMC_OK;

  int numFramesToSkip = 0;
  bool lookingForIFrame = false;

  bool mediaDataIsValid = false;

  long distanceFromLastFrame = mFrameNumber - mLastFrameNumber;

  if ((mLastFrameNumber >= 0 && distanceFromLastFrame > 0 && distanceFromLastFrame <= 4) ||
      (mLastFrameNumber < 0 && mFrameNumber==0))
  {
    if (mLastFrameNumber >= 0)
    {
      numFramesToSkip = distanceFromLastFrame-1;
      while (mRepeatFrameCount > 0 && numFramesToSkip > 0)
      {
        numFramesToSkip--;
        mRepeatFrameCount--;
      }
      if (numFramesToSkip == 0 && mRepeatFrameCount > 0)
      {
        mRepeatFrameCount--;
        mLastFrameNumber = mFrameNumber;
        copySampleToImage(pImageData,pitch,imageFormat);
        return true;
      }
    }
  }
  else
  {
    if (mIsMpegStream)
    {
      // Find closest KeyFrame
      int firstIFramesOnSameOffset = 0;

      Int64 offset = 0;

      int i = 0;

      // This can be changed to a binary search
      for(;;)
      {
        if (i == mIFrames.size() || mIFrames[i].frameNumber > mFrameNumber)
        {
          break;
        }
        if (mIFrames[i].fileOffset != offset)
        {
          offset = mIFrames[i].fileOffset;
          firstIFramesOnSameOffset = mIFrames[i].frameNumber;
        }
        i++;
      }

      numFramesToSkip = mFrameNumber-firstIFramesOnSameOffset;

      double pos = (double)offset/mpDataReader->GetSize();

      // Stop the splitter.
      mpSplitter->Stop();

      // Empty pending frames from decoder.
      mpVideoDecoder->Reset();

      // Set the new position.
      mpSplitter->SetPosition(pos);

      // Restart the splitter.
      mpSplitter->Run();

      do {
        umcSplitRes = mpSplitter->CheckNextVideoData(&mMediaData);
        if (umcSplitRes != UMC::UMC_OK)
        {
          //PVMSG("umcSplitRes: %i\n",umcSplitRes);
          vm_time_sleep(0);
        }
      } while (umcSplitRes != UMC::UMC_OK && umcSplitRes != UMC::UMC_NOT_ENOUGH_DATA);

      lookingForIFrame = true;
    }
    else
    {
      // Find closest KeyFrame
      int nearestIFrame = 0;

      Int64 offset = 0;

      int i = 0;

      // This can be changed to a binary search
      for(;;)
      {
        if (i == mIFrames.size() || mIFrames[i].frameNumber > mFrameNumber)
        {
          break;
        }
        nearestIFrame = mIFrames[i].frameNumber;
        i++;
      }

      if (mLastFrameNumber < mFrameNumber &&        // If the last frame is before this frame AND either:
          (nearestIFrame < mLastFrameNumber ||      // The nearest I-Frame is before the last frame OR
           mFrameNumber - mLastFrameNumber < 12 ||  // Theres a frame difference of less than 12 frames OR
           (nearestIFrame > mLastFrameNumber && nearestIFrame - mLastFrameNumber < 12) // The nearest I-Frame is very close to the last frame going forward
           ))
      {
        // We don't need to seek, just keep reading
        numFramesToSkip = mFrameNumber - mLastFrameNumber - 1;
      }
      else
      {

        numFramesToSkip = mFrameNumber-nearestIFrame;

        double pos = (double)nearestIFrame/mNumberOfFrames;
        double targetTime = (double)nearestIFrame/this->getFramesPerSecond();

        if (pos > 0 || mLastFrameNumber >= 0)
        {
          //PVMSG("Set Position: %lf\n",pos);
          mpSplitter->SetPosition(pos);

          // Try to give priority to other threads to get the setPosition message.
          vm_time_sleep(0);

          for (;;)
          {
            umcSplitRes = mpSplitter->GetNextVideoData(&mMediaData);
            //PVMSG("Time: %lf, Target: %lf, SplitRes: %i\n",mMediaData.GetTime(), targetTime, umcSplitRes);
            if (umcSplitRes == UMC::UMC_OK && fabs(mMediaData.GetTime() - targetTime) <  1.0/(getFramesPerSecond()*10) &&
              mMediaData.GetDataPointer() != 0)
            {
              mediaDataIsValid = true;
              break;
            }
            else
            {
              vm_time_sleep(0);
            }
          }

          // Empty pending frames from decoder.
          mpVideoDecoder->Reset();
        }
      }
    }

    //PVMSG("Will decode: %i frames\n",numFramesToSkip);

    if (!mediaDataIsValid)
    {
      mMediaData.Close();
    }

    mRepeatFrameCount = 0;
  }

  int numFramesDecoded = 0;

  bool mIsFrameValid = false;

  // Repeat decode procedure until the decoder will agree to decompress
  // at least one frame.
  for(;;)
  {
    // Get some more data from the the splitter if we've decoded
    // all data from the previous buffer
    if ((4 >= mMediaData.GetDataSize() || umcRes == UMC::UMC_NOT_ENOUGH_DATA)
      && UMC::UMC_OK == umcSplitRes)
    {
      umcSplitRes = umcRes = mpSplitter->GetNextVideoData(&mMediaData);
      //PVMSG("VideoProc: [offset, data size] from splitter is [%X, %d]\n",
      //  (int)mMediaData.GetFilePosition(),mMediaData.GetDataSize());
      if(UMC::UMC_WAIT_FOR_REPOSITION == umcSplitRes)
      {
        umcSplitRes = UMC::UMC_OK;
      }
    }
    else
    {   umcRes = UMC::UMC_OK;   }

    if (umcSplitRes != UMC::UMC_OK)
    {
      //PVMSG("umcSplitRes: %i\n",umcSplitRes);
    }

    vm_debug_trace(0, VM_STRING("Splitter PTS: %lf\n"), mMediaData.GetTime());

    // Ok, here is no more data in the splitter. Let's extract the rest
    // of decoded data from the decoder
    if (UMC::UMC_OK == umcRes && NULL == mMediaData.GetDataPointer())
    {   umcSplitRes = UMC::UMC_END_OF_STREAM;   }

    if (UMC::UMC_END_OF_STREAM == umcSplitRes ||
      UMC::UMC_OPERATION_FAILED == umcSplitRes)
    {   umcRes = UMC::UMC_OK;   }

    if (UMC::UMC_OK == umcRes)
    {

      if (UMC::UMC_END_OF_STREAM != umcSplitRes &&
        UMC::UMC_OPERATION_FAILED != umcSplitRes)
      {   umcRes = decodeFrame(&mMediaData, mVideoData);  }
      else
        // sweap reference frames from the decoder
      {   umcRes = decodeFrame(NULL, mVideoData);  }

      mediaDataIsValid = false;

      if(umcRes != UMC::UMC_OK)
      {
        if (umcRes == UMC::UMC_END_OF_STREAM)
        {
          // We won't be able to find more frames, let's just copy the
          // current sample.
          copySampleToImage(pImageData,pitch,imageFormat);
          break;
        }
        vm_time_sleep(0);
      }
      else
      {
        numFramesDecoded++;
        if (lookingForIFrame && mIsIFrame)
        {
          //PVMSG("I-Frame Found!\n");
          lookingForIFrame = false;
        }

        if (lookingForIFrame)
        {
          umcRes = UMC::UMC_NOT_ENOUGH_DATA;
        }
        else
        {
          mIsFrameValid = true;
          while (mRepeatFrameCount > 0 && numFramesToSkip > 0)
          {
            numFramesToSkip--;
            mRepeatFrameCount--;
          }
          if (numFramesToSkip == 0)
          {
            //PVMSG("Frame %i Found!\n",mFrameNumber);
            copySampleToImage(pImageData,pitch,imageFormat);
            break;
          }
          if (numFramesToSkip > 0)
          {
            numFramesToSkip--;
          }
        }
      }
    }

    vm_debug_trace(0, VM_STRING("Decoder PTS: %lf\n"),
      mVideoData.GetTime());

  }

  mLastFrameNumber = mFrameNumber;

  if ((UMC::UMC_END_OF_STREAM == umcSplitRes || UMC::UMC_OPERATION_FAILED == umcSplitRes) &&
      !mIsFrameValid)
  {
    mIsEndOfFile = true;
    return false;
  }

  //PVMSG("Num frames decoded: %i\n",numFramesDecoded);

  return true;
}

void 
VideoReaderIpp::deleteIndex(const std::string& fname)
{
  std::string indexFname = getIndexFileName(fname);
  boost::filesystem::remove(boost::filesystem::path(indexFname.c_str(), boost::filesystem::native));
}



std::string
VideoReaderIpp::getIndexFileName(const std::string& videoName)
{
  std::string fname = get_file_name(videoName);
  
  WIN32_FILE_ATTRIBUTE_DATA fad;
  GetFileAttributesEx(videoName.c_str(),GetFileExInfoStandard,&fad);

  fname += aitSprintf("-%u-%u.vindex",fad.ftLastWriteTime.dwHighDateTime,fad.ftLastWriteTime.dwLowDateTime);
  
  std::string directory;
  TCHAR szPath[MAX_PATH];
  if(SUCCEEDED(SHGetFolderPath(NULL, 
    CSIDL_LOCAL_APPDATA|CSIDL_FLAG_CREATE, 
    NULL, 
    0, 
    szPath))) 
  {
    directory = combine_path(szPath,"VideoMining/Video Index");
  }

  boost::filesystem::path folderPath(directory.c_str(), boost::filesystem::native);
  if (!boost::filesystem::exists(folderPath))
  {
    boost::filesystem::create_directories(folderPath);
  }

  fname = combine_path(directory,fname);

  return fname;
}

void
VideoReaderIpp::readIFrameIndex(const std::string& fname)
{
  FILE *fp = fopen(fname.c_str(),"r");

  if (!fp)
  {
    throw std::exception((std::string("Could not open file: ") + fname).c_str());
  }

  std::string magicCode = "VM.VINDEX.1.1\n";
  char *fileMagicCode = new char[magicCode.size()+1];

  bool isValid = true;

  if (fread(fileMagicCode,sizeof(char),magicCode.size(),fp) != magicCode.size())
  {
    isValid = false;
  }
  else
  {
    fileMagicCode[magicCode.size()] = '\0';

    if (magicCode != fileMagicCode)
    {
      isValid = false;
    }
  }

  delete [] fileMagicCode;

  if (!isValid)
  {
    fclose(fp);
    throw std::exception((std::string("Invalid File: ") + fname).c_str());
  }

  int numFrames;
  if (fscanf(fp,"%i\n",&numFrames) != 1)
  {
    fclose(fp);
    throw std::exception((std::string("Invalid File: ") + fname).c_str());
  }

  mNumberOfFrames = numFrames;

  for(;;)
  {
    int iFrame;
    Int64 offset;
    int ret = fscanf(fp,"%i,%I64X\n",&iFrame,&offset);
    if (ret != 2)
    {
      break;
    }
    mIFrames.push_back(OffsetFrameInfo(iFrame,offset));
  }

  fclose(fp);

}

void
VideoReaderIpp::writeIFrameIndex(const std::string& videoFname, const std::string& indexFname)
{
  FILE *fp = fopen(indexFname.c_str(),"w");
  if (!fp)
  {
    throw std::exception(("Error writing i-frame index file: " + videoFname).c_str());
  }
  fprintf(fp,"VM.VINDEX.1.1\n");
  fprintf(fp,"%i\n",mNumberOfFrames);
  for (int i = 0; i < mIFrames.size(); i++)
  {
    fprintf(fp,"%i,%I64X\n",(int)mIFrames[i].frameNumber,mIFrames[i].fileOffset);
  }
  fclose(fp);
}

void
VideoReaderIpp::scanIFrames(const std::string& videoFname)
{
  open(videoFname,false,true);

  UMC::Status umcRes = UMC::UMC_OK;

  int numFrames = 0;
  mIFrames.clear();

  // This is used to calculate the actual frame number.
  // MPEG frames are not encoded in order. All 'B' frames
  // after an I-Frame go before the I frame. Therefore we will
  // increment the frame number for each B-Frame we find after
  // an I-Frame.
  bool nextFrameIsIFrame = false;

  // Repeat decode procedure until the decoder will agree to decompress
  // at least one frame
  for (;;)
  {
    if (mAbortIFrameScan)
    {
      close();
      throw std::exception("Operation Aborted");
    }

    // We are scanning linearly and without ordering the frames, so this function should
    // work every time.
    umcRes = mpSplitter->GetNextVideoData(&mMediaData);

    mpSplitter->GetPosition(mProgress);

    if (umcRes != UMC::UMC_OK)
    {
        //PVMSG("mpSplitter->GetNextVideoData() umcRes: %i\n",umcRes);
    }

    if (UMC::UMC_END_OF_STREAM == umcRes ||
        UMC::UMC_OPERATION_FAILED == umcRes)
    {
      break;
    }

    if (UMC::UMC_OK == umcRes)
    {
      umcRes = mpVideoDecoder->GetFrame(&mMediaData, &mVideoData);

      if (umcRes == UMC::UMC_OK)
      {
        mIsIFrame = mVideoData.m_FrameType == UMC::I_PICTURE;
        std::string ftype = "";
        switch (mVideoData.m_FrameType)
        {
        case UMC::I_PICTURE:
          ftype = "I";
          nextFrameIsIFrame = true;
          break;
        case UMC::P_PICTURE:
          ftype = "P";
          nextFrameIsIFrame = false;
          break;
        case UMC::B_PICTURE:
          if (nextFrameIsIFrame)
          {
            // Increment the frame number for the last I-frame.
            mIFrames.back().frameNumber++;
          }
          ftype = "B";
          break;
        }

        if (mIsIFrame)
        {
          mIFrames.push_back(OffsetFrameInfo(numFrames,(Int64)mMediaData.GetFilePosition()));
        }

        vm_debug_trace(-1, VM_STRING("Decoder PTS: %lf [%s]\n"),
          mMediaData.GetTime(), ftype.c_str());

        numFrames++;
      }
      else
      {
        //PVMSG("mpVideoDecoder->GetFrame() umcRes: %i\n",umcRes);
      }

      if(umcRes != UMC::UMC_OK)
      {
        vm_time_sleep(0);
      }
    }
  }

  mNumberOfFrames = numFrames;

  close();

}

//
// ACCESS
//

Int64 
VideoReaderIpp::getNumberOfFrames()
{
  return mNumberOfFrames;
}

void 
VideoReaderIpp::setCurrentFrameNumber(Int64 frame)
{
  if (frame < 0 || frame >= mNumberOfFrames)
  {
    throw std::out_of_range("Frame is out of range");
  }
  mIsEndOfFile = false;
  mFrameNumber = frame;
}

Int64 
VideoReaderIpp::getCurrentFrameNumber()
{
  return mFrameNumber;
}

double 
VideoReaderIpp::getFramesPerSecond()
{
  return mFramesPerSecond;
}

Vector2i
VideoReaderIpp::getBufferSize()
{
  return mBufferSize;
}

bool
VideoReaderIpp::isEndOfFile()
{
  return mIsEndOfFile;
}

void 
VideoReaderIpp::nextFrame()
{
  if (mFrameNumber+1 >= mNumberOfFrames)
  {
    mIsEndOfFile = true;
    mFrameNumber = mNumberOfFrames;
  }
  else
  {
    setCurrentFrameNumber(mFrameNumber+1);
  }
}

//
// INQUIRY
//

}; // namespace ait


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef VideoReaderIppShared_HPP
#define VideoReaderIppShared_HPP

#ifndef SHAREDAPI
#ifdef VideoMining_Codecs_Ipp_EXPORTS // <-- This is defined by CMake.
  #define SHAREDAPI __declspec(dllexport)
#else
  #define SHAREDAPI __declspec(dllimport)
#endif
#endif

namespace ait 
{

/**
 * The purpose of this class is to provide an interface to the VideoReader
 * class using only basic C++ types, so it can be exported in a DLL more easily
 * without cascading dependencies on other types.
 */
class SHAREDAPI VideoReaderIppShared
{
  //
  // LIFETIME
  //
  
public:

  /**
   * Constructor.
   */
  VideoReaderIppShared();

  /**
   * Destructor. The destructor will close the file if it is open.
   */
  virtual ~VideoReaderIppShared();

  //
  // OPERATIONS
  //

public:

  /**
   * Open a file for reading.
   * @remark Throws std::exception if there are problems opening the file.
   */
  void open(const char *fname);

  /**
   * Close the current file.
   */
  void close();

  /**
   * Get the current frame from the file. It will read the frame indicated
   * by getCurrentFrameNumber(). It will not increment automatically the
   * frame number. You can use nextFrame() to do this.
   * @param data [in] Pointer to the start of image data. Planar images are not supported.
   * @param pitch [in] space between lines, use negative numbers for bottom up data.
   * @param imageFormat [in] A value from the ait::ImageFormat enumeration (only RGB32 and 
   * RGB24 are currently supported)
   * @return true if a frame was read false if it is one frame past the last
   * frame, in this case no frame was read.
   */
  bool readFrame(unsigned char *pImageData, int pitch, int imageFormat);

  void deleteIndex(const char *fname);

  /**
   * Call this function *AFTER* readFrame to find out if the current frame is
   * an I-Frame.
   */
  bool isIFrame();

  //
  // ACCESS
  //

public:

  /**
   * Return the number of frames in the video.
   */
  int getNumberOfFrames();

  /**
   * Set the current frame.
   * @throws std::exception if the frame number is invalid.
   */
  void setCurrentFrameNumber(int frame);

  /**
   * Get the current frame number. The first frame is frame 0.
   * getCurrentFrameNumber() == getNumberOfFrames() indicates the
   * end of the video.
   */
  int getCurrentFrameNumber();

  /**
   * Get the number of frames per second on this video.
   */
  double getFramesPerSecond();

  /**
   * Get the size of the video buffer. Only valid after opening the file.
   */
  void getBufferSize(int& width, int& height);

  /**
   * Move to the next frame. If it is in the last frame, it will
   * stay there.
   */
  void nextFrame();

  /**
   * Get the array of I-Frames for the current video. The array is
   * zero based. If NULL is passed, it will return the size of the
   * required array.
   */
  int getIFrames(int *pIFrames);

  /**
   * Get the progress from the I-Frame scan.
   */
  double getScanProgress();

  void abort();

  //
  // INQUIRY
  //

public:

  /**
   * Returns true if the video is over, that is getCurrentFrameNumber()
   * == getNumberOfFrames().
   */
  bool isEndOfFile();

  //
  // ATTRIBUTES
  //

protected:

  void *mpVideoReader;

};


}; // namespace ait

#endif // VideoReaderIppShared_HPP


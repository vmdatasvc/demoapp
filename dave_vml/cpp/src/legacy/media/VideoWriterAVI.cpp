/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
#include "VideoWriterAVI.hpp"
#include <Settings.hpp>

void VideoWriterAVI::init(int w, int h, const char *name, bool flag, std::string encodeFile)
{

  std::string settingsFile = ait::Settings::replaceVars(encodeFile);

  mpVideoWriter = ait::VideoWriter::create();

  mpVideoWriter->setFramesPerSecond(15);

  if (!mpVideoWriter->loadCodecSettings(settingsFile,true))
  {
    PvUtil::exitError("Unable to determine compressor");
  }

  try
  {
    mpVideoWriter->open(name);
  }
  catch (std::exception ex)
  {
    PvUtil::exitError("VideoWriterAVI: Error in AVIFileOpen('%s').",name);
  }
}

void VideoWriterAVI::saveFrame(ait::Image32 &image)
{
  mpVideoWriter->writeFrame(image);
}

void VideoWriterAVI::finish(void)
{
  mpVideoWriter.reset();
}


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#pragma warning(disable:4786)

#include "ImageAcquireDeviceGeoVision.hpp"
#include <assert.h>
#include <dshow.h>

#ifdef HIGH_PERF_LOG
#include "HighPerfLog.hpp"
extern ait::HighPerfLog gLog;
#endif


namespace ait
{

GeoVisionThread::GeoVisionThread() :
  mpIdmipcClient(0),
  mCurrentCameraId(-1),
  mGrabFrameFromMainLoop(true),
  mIsCallbackInvoked(false),
  mpThread(0),
  mpThreadHolder(0)
{
}

GeoVisionThreadPtr GeoVisionThread::mpInstance;

GeoVisionThreadPtr
GeoVisionThread::instance()
{
  if (!mpInstance.get())
  {
    mpInstance.reset(new GeoVisionThread());
  }
  return mpInstance;
}

void 
GeoVisionThread::initDmipc()
{
  //If the idmipc client is not initialized, do so
  if (!mpIdmipcClient)
  {
    // Catch Microsoft Exception (not the same as C++ exceptions)
    // Otherwise the exception wouldn't be handled. This is generated
    // by the compiler in the the delay load DLL code.
    __try
    {
      //create interface to geovision main system
      mpIdmipcClient=(CIDMIPCClient*)(CreateIDMIPC(mIdmipcInitData));
    }
    __except (EXCEPTION_EXECUTE_HANDLER)
    {
      PvUtil::exitError("GeoVision DLL not found (DMIPC.DLL)");
    }

    if (mpIdmipcClient)
    {
      if (mpIdmipcClient->Initialize())
      {
        //register callback functions
        if(!mpIdmipcClient->RegClientCallBack(this))
        {
          PvUtil::exitError("Could not register callback functions");
        }       
      }
      else
      {
        PvUtil::exitError("Initializing Capture Device\n(Could not initialize interface)");
      }
    }
    else
    {
      PvUtil::exitError("Initializing Capture Device\n(Geovision Main System not started?)");
    }
  }

  //check for which image size is available
  switch (mIdmipcInitData.uImageBufSize)
  {
  case DMIPC_CAM_SIZE32x24:
    //frameWidth = 320;
    //frameHeight = 240;
    break;
    /*
    case DMIPC_CAM_SIZE64x24:
    frameWidth = 640;
    frameHeight = 240;
    break;
    case DMIPC_CAM_SIZE64x48:
    frameWidth = 640;
    frameHeight = 480;
    break;
    */
  default:
    PvUtil::exitError("Invalid image size received!\n");
  }

  mCurrentCameraId = -1;
  mGrabFrameFromMainLoop = true;

}

void
GeoVisionThread::finishDmipc()
{
  if (mpIdmipcClient)
  {
    mpIdmipcClient->UnRegCallBack();
    ReleaseIDMIPC(mpIdmipcClient);
    mpIdmipcClient = NULL;
  }
}

void
GeoVisionThread::registerDevice(int camId, ImageAcquireDeviceGeo* pDevice)
{
  //only allow one thread/object to register at a time
  boost::mutex::scoped_lock lk(mMutex);

  assert(mCameraMap.find(camId) == mCameraMap.end());

  GeoVisionCameraInfoPtr pCamInfo(new GeoVisionCameraInfo());

  pCamInfo->cameraId = camId;
  pCamInfo->deviceFps = pDevice->getProperty(ImageAcquireProperty::FPS)->getValuef(false);
  pCamInfo->pDevice = pDevice;
  pCamInfo->lastFrameTime = 0;
  pCamInfo->isLastFrameValid = false;
  pCamInfo->delayMonitor.init(pCamInfo->deviceFps);

  bool isFirstDevice = mCameraMap.empty();

  mCameraMap[camId] = pCamInfo;

  if (isFirstDevice)
  {
    mpThreadHolder = new ThreadHolder(*this);
    mpThread = new boost::thread(*mpThreadHolder);
  }

  // Create thread here if it is the first camera.
  
  //PVMSG("MASTER: Registering camera id: %d\n", camId);

}

void
GeoVisionThread::unregisterDevice(int camId)
{
  bool isLastDevice = false;
  //only allow one thread/object to register at a time
  {
    boost::mutex::scoped_lock lk(mMutex);

    std::map<int, GeoVisionCameraInfoPtr>::iterator iCam = mCameraMap.find(camId);

    assert(iCam != mCameraMap.end());

    // Wait here to make sure that the current camera is not the one that will be removed.

    mCameraMap.erase(iCam);

    isLastDevice = mCameraMap.empty();
  }

  // Remove thread here if there are no more cameras registered.
  if (isLastDevice)
  {
    mpThread->join();
    delete mpThread;
    delete mpThreadHolder;
    mpThreadHolder = 0;
    mpThread = 0;
  }
  
  //PVMSG("MASTER: Unregistering camera id: %d\n", camId);

}

GeoVisionThread::GeoVisionCameraInfoPtr
GeoVisionThread::getNextCamera()
{
  GeoVisionCameraInfoPtr pCamInfo;

  boost::mutex::scoped_lock lk0(mMutex);

  if (mCameraMap.empty())
  {
    mCurrentCameraId = -1;
    return GeoVisionCameraInfoPtr();
  }

  std::map<int, GeoVisionCameraInfoPtr>::iterator iCam = mCameraMap.find(mCurrentCameraId);

  if (iCam == mCameraMap.end())
  {
    iCam = mCameraMap.begin();
  }
  else
  {
    iCam++;
    if (iCam == mCameraMap.end())
    {
      iCam = mCameraMap.begin();
    }
  }

  mCurrentCameraId = iCam->first;
  return iCam->second;
}

GeoVisionThread::GeoVisionCameraInfoPtr
GeoVisionThread::getCameraInfo(int camId)
{
  boost::mutex::scoped_lock lk0(mMutex);

  std::map<int, GeoVisionCameraInfoPtr>::iterator iCam = mCameraMap.find(mCurrentCameraId);

  if (iCam != mCameraMap.end())
  {
    return iCam->second;
  }

  return GeoVisionCameraInfoPtr();
}


void
GeoVisionThread::mainLoop()
{
  initDmipc();

  for (;;)
  {
    if (mGrabFrameFromMainLoop)
    {
      GeoVisionCameraInfoPtr pCamInfo = getNextCamera();
      if (!pCamInfo.get())
      {
        // No cameras are available. We'll exit the loop.
        break;
      }

      mGrabFrameFromMainLoop = false;
      mIsCallbackInvoked = true;
      if (mpIdmipcClient->GrabFrame(pCamInfo->cameraId))
      {
        // GrabFrame was successful, the callback will be called.
      }
      else
      {
        // GrabFrame was not successful. We have to see if the
        // server is not working.
        mGrabFrameFromMainLoop = true;
        mIsCallbackInvoked = false;
        pCamInfo->isLastFrameValid = false;

        // Let's not waste CPU on busy waits. Something is wrong,
        // and we'll give some breath to the CPU and frame-grabber
        // to recover. This might be caused by a very busy CPU, or
        // drivers initializing, server down, etc.
        PvUtil::sleep(0.1);
      }
    }
    else
    {
      // Typically all the processing will be done in the callback function, so
      // all we need to do is sleep and wait for the exit signal, or instructions
      // to grab the frame from here.
      PvUtil::sleep(0.1);
    }
  }

  // This shouldn't be necessary because no callback should be running at
  // this point, but it is added for additional safety.
  PvUtil::sleep(0.3);

  finishDmipc();
}

double 
GeoVisionThread::getNextFrameTime(GeoVisionCameraInfoPtr pCamInfo)
{
  if (pCamInfo->lastFrameTime == 0.0) //Handle initial setting
  {
    return PvUtil::time();
  }
  return pCamInfo->lastFrameTime +  pCamInfo->delayMonitor.getDelay();
}

void
GeoVisionThread::registerFrameTime(GeoVisionCameraInfoPtr pCamInfo, double time)
{
  if (pCamInfo->isLastFrameValid)
  {
    pCamInfo->delayMonitor.update(time-pCamInfo->lastFrameTime);
  }
  pCamInfo->lastFrameTime = time;
}

bool
GeoVisionThread::mustSkipThisFrame(GeoVisionCameraInfoPtr pCamInfo, double currentTime)
{
  if (pCamInfo->lastFrameTime == 0.0)
  {
    return false;
  }
  if (currentTime < pCamInfo->lastFrameTime + pCamInfo->delayMonitor.getDelay())
  {
    return true;
  }
  return false;
}

void __stdcall 
GeoVisionThread::OnRpyVideo(UINT iCam,IPC_RPY_CAM* pRpyCam,LPVOID pData)
{ 

  bool ignoreThisMessage = false;
  bool sendBlueFrame = false;

  switch ((int)pRpyCam->time.wYear)
  {
  case 0:
    //PVMSG("Channel not activated! %d\n", iCam);    
    ignoreThisMessage = true;
    break;  
  case 65534:
    ignoreThisMessage = true;
    //PVMSG("Video data not updated!\n");
    break;
  case 65535:
    //PVMSG("LOST VIDEO FEED: %d\n", iCam);
      
    //When video feed is lost, signal to go to the next frame so all cameras 
    // do not lock. Additionally, we paint the frame blue so that it is 
    // compatible with our other devices
    sendBlueFrame = true;
    break;
  }

  if (!ignoreThisMessage)
  {
    GeoVisionCameraInfoPtr pCamInfo;
    {
      boost::mutex::scoped_lock lk(mMutex);
      pCamInfo = getCameraInfo(iCam);

      if (!pCamInfo.get())
      {
        // This camera is no longer registered. We shouldn't do anything else for now.
      }
      else
      {
        boost::mutex::scoped_lock lk(pCamInfo->pDevice->mFrameReadyMutex);
        MediaFrameInfoPtr pFrameInfo = pCamInfo->pDevice->mpNextFrame;
        if (!pFrameInfo.get())
        {
          // We still don't have a frame to fill for this camera.
          // We'll just drop the current data.
        }
        else
        {
          double currentTime = PvUtil::time();

          bool ignoreThisFrame = mustSkipThisFrame(pCamInfo,currentTime);
          
          if (!ignoreThisFrame)
          {
            //PVMSG("GOT GOOD FRAME: %d\n", iCam);
            registerFrameTime(pCamInfo,currentTime);
            if (sendBlueFrame)
            {
              pFrameInfo->pRGBImage->setAll(PV_RGB(0,0,255));
              pCamInfo->isLastFrameValid = false;
            }
            else
            {
              pCamInfo->isLastFrameValid = true;
              // The frame is available to us, let's fill it with data.
              Image32& img = *pFrameInfo->pRGBImage;
              
              for (int y = 0; y < img.height(); y++)
              {
                Uint8* row = (Uint8*)pData + (y*320*3);
                
                for (int x = 0; x < img.width(); x++)
                {
                  img(x,img.height() - y - 1) = PV_RGB(row[x*3+2],row[x*3+1],row[x*3]);
                }
              } 
            }
            //Signal to waiting threads that a frame is now available.
            pCamInfo->pDevice->mpNextFrame.reset();
            pCamInfo->pDevice->mIsFrameReady.notify_one();
          }
          else
          {
            //PVMSG("FRAME SKIPPED: %d\n", iCam);
          }
        }
      }
    }
  }

  // Now fall GrabFrame for the next message.
  GeoVisionCameraInfoPtr pNewCamInfo = getNextCamera();

  if (!pNewCamInfo.get())
  {
    // No cameras are available. Let's just return without calling GrabFrame.
    mIsCallbackInvoked = false;
    mGrabFrameFromMainLoop = true;
  }
  else
  {
    if (!mpIdmipcClient->GrabFrame(pNewCamInfo->cameraId))
    {
      // The call to grab frame failed. We will delegate this to the main loop.
      mIsCallbackInvoked = false;
      mGrabFrameFromMainLoop = true;
    }
  }
}

void __stdcall 
GeoVisionThread::OnReply(struct DMRequest *,struct DMData *)
{
  //PVMSG("OnReply\n");
}

void __stdcall 
GeoVisionThread::OnRpyAudio(unsigned int,struct IPC_RPY_AUDIO *,void *)
{
  //PVMSG("OnRpyAudio\n");
}

void __stdcall 
GeoVisionThread::OnRpyTrigger(struct DMRequest *,struct DMData *)
{
  //PVMSG("OnRpyTrigger\n");
}


ImageAcquireDeviceGeo::ImageAcquireDeviceGeo()
{
  pParams = NULL;
}


ImageAcquireDeviceGeo::~ImageAcquireDeviceGeo()
{ 
	unInitialize();
};

bool ImageAcquireDeviceGeo::initialize(int numPrevBuffers, int numConsecutiveFrameGrabs)
{
  assert(numPrevBuffers >= 0);

	if (isInitialized)
		unInitialize();  

  lastPaintTime = PvUtil::time();
  paintDelay = 0; // paint delay in secs.

  //Initialize the display window.
  displayFrameFlag = false;

  //Initialize base parameters.
  numPreviousBuffers = numPrevBuffers;

  //Initialize the resolution.
  frameWidth = 320;
  frameHeight = 240;  

  //Extract device FPS, used for controlling maximum framerate
  Settings s(deviceInfo->getRegPath());
  mFps = s.getDouble("Fps/value [float]", 99);

  pParams = new ImageAcquirePropertyBag(deviceInfo->getRegPath());
  pParams->get(ImageAcquireProperty::SOURCEWIDTH)->setValuef(frameWidth);
  pParams->get(ImageAcquireProperty::SOURCEHEIGHT)->setValuef(frameHeight);
  pParams->get(ImageAcquireProperty::FPS)->setValuef(mFps);

  frameCounter = 0;

  //initialize camera
  //Geovision camera indices start from 1, NOT 0
  mCameraId = deviceInfo->getdeviceIndex() + 1;

  GeoVisionThread::instance()->registerDevice(mCameraId,this);

  //init circular buffers
  initBuffers(frameWidth, frameHeight, numPrevBuffers);

  // The first frame will now be ready for asynchronous
  // updates.
  {
    boost::mutex::scoped_lock lk(mFrameReadyMutex);  
    mpNextFrame = mFrames.back();
  }

  isInitialized = true;    
      
	return true;
};

bool 
ImageAcquireDeviceGeo::unInitialize()
{
  if (isInitialized)
  {
    GeoVisionThread::instance()->unregisterDevice(mCameraId);
    delete pParams;
  }

	isInitialized = false;  

  return true;
};

int 
ImageAcquireDeviceGeo::grabFrame()
{
	assert(isInitialized);  

  {
    boost::mutex::scoped_lock lk(mFrameReadyMutex);  

    long delay = (long)(2000.0/mFps);

    //wait for frame to become available
    while (mpNextFrame.get())
    {
      //PVMSG("Will wait up to %d milliseconds\n", (int)delay);
      if (!mIsFrameReady.timed_wait(lk,delay))
      {
        //PVMSG("Timed out!\n");
        // The timeout expired. We'll send a blue frame. If the callback has
        // not finished, it will realize that mpNextFrame is no longer available
        // and just drop it.
        // Note that there is a chance that after
        if (mpNextFrame.get())
        {
          mpNextFrame->pRGBImage->setAll(PV_RGB(0,0,255));
          mpNextFrame.reset();
        }
      }
      else
      {
        //PVMSG("Signaled!\n");
      }
    }
  }

  //PVMSG("Done grabbing frame: %d\n", mCameraId);

  //rotate circular buffer
  nextFrame();  

  // Prepare frame for next iteration so it can be filled asynchronously.
  // the next frame will be written to the back of the buffer
  {
    boost::mutex::scoped_lock lk(mFrameReadyMutex);  
    mpNextFrame = mFrames.back();
  }

	return 0;
};


} // namespace ait






















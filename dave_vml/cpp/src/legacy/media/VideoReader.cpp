/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include "VideoReader.hpp"
#include "directshow/VideoReaderDS.hpp"
#include "ipp/VideoReaderIppStub.hpp"
#include <Settings.hpp>

namespace ait 
{

VideoReader::VideoReader()
{
}

VideoReader* 
VideoReader::create()
{
  std::string mediaEngine = Settings::replaceVars("<MediaEngine>");
  if (mediaEngine == "<MediaEngine>")
  {
    mediaEngine = "IPP";
  }
  return VideoReader::create(mediaEngine);
}

VideoReader*
VideoReader::create(const std::string& mediaEngineId)
{
  if (mediaEngineId == "IPP")
  {
    return new VideoReaderIppStub();
  } 
  else if (mediaEngineId == "DirectShow")
  {
    return new VideoReaderDS();
  }
  else
  {
    throw std::exception("Invalid Media Engine ID specified. Valid values are 'IPP' and 'DirectShow'");
  }
}

VideoReader::~VideoReader()
{
}

//
// OPERATIONS
//

//
// ACCESS
//

bool
VideoReader::isEndOfFile()
{
  return getCurrentFrameNumber() >= getNumberOfFrames();
}


//
// INQUIRY
//

}; // namespace ait


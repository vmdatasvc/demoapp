/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifdef WIN32
#define INITGUID

#include <windows.h>
#include <mmsystem.h>
#include <dsound.h>

#include <PvUtil.hpp>

#include "SoundEngine.hpp"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

namespace ait
{

SoundEngine gSE;

/*********************************************************************
 * CONSTRUCTOR
 *********************************************************************/
SoundEngine::SoundEngine()
{
  pds = NULL;
  pdsbPrimary = NULL;
  pwfxFormat = NULL;
}

/*********************************************************************
 * DESTRUCTOR
 *********************************************************************/
SoundEngine::~SoundEngine()
{
  if( NULL != pdsbPrimary ) {
    pdsbPrimary->Stop();
    pdsbPrimary->Release();
    pdsbPrimary = NULL;
  }
  if( NULL != pds ) {
    pds->Release();
    pds = NULL;
  }
}

/*********************************************************************
 * init()
 * Initializes the sound engine.
 *********************************************************************/
int SoundEngine::init(HWND hwndParent)
{
    HRESULT hr;

    //abort();

  hr=DirectSoundCreate(NULL, &pds, NULL);

  if (FAILED(hr) || (NULL == pds))
  {
    MessageBox( NULL, "Unable to get a DirectSound object",
				"COM Failure", MB_OK | MB_ICONSTOP );
    goto ID_ExitError;
  }

  if(hwndParent==NULL)
  {
    hwndParent = GetForegroundWindow();
    if (hwndParent == NULL)
    {
      hwndParent = GetDesktopWindow();
    }
  }

  hr = pds->SetCooperativeLevel( hwndParent, DSSCL_NORMAL);

  if (FAILED(hr))
    {
      MessageBox( NULL, "Could not SetCooperativeLevel()",
				  "Error", MB_OK | MB_ICONSTOP );
      goto ID_ExitReleaseDS;
    }

  return TRUE;

ID_ExitReleaseDS:
  // The InitPrimarySoundBuffer() call should have cleaned up
  // after itself if it failed

  PVASSERT( NULL == pdsbPrimary );

  if( NULL != pds )
    {
      pds->Release();
      pds = NULL;
    }

ID_ExitError:
  return FALSE;

}

/*********************************************************************
 * initPrimarySoundBuffer()
 * Initializes the Primary Sound Buffer Object
 *********************************************************************/
int SoundEngine::initPrimarySoundBuffer()
{
  HRESULT	    hr;
  DSBUFFERDESC    dsbd;

  ZeroMemory( &dsbd, sizeof(DSBUFFERDESC));
  
  pwfxFormat = (LPWAVEFORMATEX)GlobalAlloc(GPTR, sizeof(WAVEFORMATEX));
  if( NULL == pwfxFormat )
      return FALSE;

  ZeroMemory( pwfxFormat, sizeof(WAVEFORMATEX));

  pwfxFormat->wFormatTag = WAVE_FORMAT_PCM;
  pwfxFormat->nSamplesPerSec = 11025*4;
  pwfxFormat->wBitsPerSample = 16;
  pwfxFormat->nChannels = 2;

  // The nBlockAlign calculation below only works for whole-byte samples
  PVASSERT( pwfxFormat->wBitsPerSample % 8 == 0 );

  pwfxFormat->nBlockAlign = pwfxFormat->nChannels * (pwfxFormat->wBitsPerSample / 8);
  pwfxFormat->nAvgBytesPerSec = pwfxFormat->nBlockAlign * pwfxFormat->nSamplesPerSec;

  dsbd.dwSize = sizeof(DSBUFFERDESC);
  dsbd.dwFlags = DSBCAPS_CTRL3D | DSBCAPS_PRIMARYBUFFER;

  if( FAILED( hr = pds->CreateSoundBuffer( &dsbd, &pdsbPrimary, NULL ))) {
    goto IPSB_ExitError;
  }

  if( FAILED( hr = pdsbPrimary->SetFormat( pwfxFormat ))) {
    // Here we can degrade the format
    goto IPSB_ExitError;
  }

  if( FAILED( hr = pdsbPrimary->Play( 0, 0, DSBPLAY_LOOPING ))) {
    goto IPSB_ExitRelease;
  }

  return TRUE;

IPSB_ExitRelease:
  if( pdsbPrimary ) {
    pdsbPrimary->Stop();
    pdsbPrimary->Release();
    pdsbPrimary = NULL;
  }

IPSB_ExitError:
  return FALSE;

}

} // namespace ait

#endif

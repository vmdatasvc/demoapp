/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include "ActiveCam.hpp"

#ifndef WIN32
#include <sys/poll.h>
#endif



/////////////////////////////////////////////////////////////////
//
// ActiveCam::ActiveCam() 
// default constructor
//
/////////////////////////////////////////////////////////////////
ActiveCam::ActiveCam()
: AC_KEEP_CURRENT(-200),
  standardTimeout(10000),
  STEPSPERPANDEGLEFT((float)8.6),
  STEPSPERPANDEGRIGHT((float)8.61),
  STEPSPERTILTDEGUP((float)11.44),
  STEPSPERTILTDEGDOWN((float)11.4),
  MAX_FOCUS(40959),
  MIN_FOCUS(4096),
  RETRIES(5)
{

  int error;
  
  /* Initialize the connection */
  //Jay
  //ser_fd = new HANDLE;

#ifdef WIN32  
  ser_fd = CreateFile(DEFAULT_SERIAL_CHANNEL,// pointer to name of the file
    GENERIC_READ | GENERIC_WRITE, // access mode
    0,                     // comm devices must be opened w/exclusive-access 
    NULL,                  // no security attrs 
    OPEN_EXISTING,         // comm devices must use OPEN_EXISTING 
    FILE_FLAG_OVERLAPPED,  // overlapped I/O
    NULL);                 // hTemplate must be NULL for comm devices 
  
  
  if((error = Init(ser_fd, DEFAULT_SERIAL_CHANNEL) < 0))
  {
    cameraSetup=false;
    return;
  }
#else
  ser_fd = open(DEFAULT_SERIAL_CHANNEL, O_RDWR|O_NONBLOCK);
  if (ser_fd < 0)
  {
    cameraSetup = false;
    error = errno;
    return;
  }
#endif  
  
  exitWhenError=false;

  cameraSetup=true;
}

////////////////////////////////////////////////////////////////
//
// destructor
//
/////////////////////////////////////////////////////////////////
ActiveCam::~ActiveCam()
{
//	delete ser_fd;
}

////////////////////////////////////////////////////////////////
//
// int	ActiveCam::panTilt(int nextPan, int nextTilt)
// set the pan and tilt by the nextPan and nextTilt
// read the current pan and tilt
// if AC_KEEP_CURRENT then keep the current pan or tilt
//
/////////////////////////////////////////////////////////////////
bool ActiveCam::panTilt(int nextPan, int nextTilt, int speed)
{
  Vector2i result;
  int curPan, curTilt;
  
  // when set only either pan or tilt
  if ( (nextPan == AC_KEEP_CURRENT) || (nextTilt == AC_KEEP_CURRENT) )
  {
    // Read camera position
    result = GetPanTiltValue(ser_fd);     
    
    // get the current value of pan and tilt
    curPan  = result(0);
    curTilt = result(1);
    
    // set cam position only either pan or tilt
    if ( nextPan == AC_KEEP_CURRENT ){
      
      if((SetAbsPanTilt(ser_fd, curPan, nextTilt, speed) < 0)){
        PVMSG("ActiveCam: panTilt(): Couldn't write to the camera\n"); 
        return false;
      }
      else
        return true;
      
    }
    else { // nextTilt == AC_KEEP_CURRENT
      
      if((SetAbsPanTilt(ser_fd, nextPan, curTilt, speed) < 0)){
        PVMSG("ActiveCam: panTilt(): Couldn't write to the camera\n"); 
        return false;
      }
      else
        return true;
      
    }
  }
  
  // when set both pan/tilt
  else {
    
    if((SetAbsPanTilt(ser_fd, nextPan, nextTilt, speed) < 0)){
      PVMSG("ActiveCam: panTilt(): Couldn't write to the camera\n");
      return false;
    }
    else
      return true;
    
  }
  
  return false;
}

/////////////////////////////////////////////////////////////////
// bool panTilt(acConst cmd = AC_ABSOLUTE_POS, int, int)
// pan and tilt operation by the nextPan and nextTilt
// INPUT  :
//  cmd   : commands in enum acConst set
//          AC_UP:
//          AC_DOWN:
//          AC_LEFT:
//          AC_RIGHT:
//          AC_UPLEFT:
//          AC_UPRIGHT:
//          AC_DOWNLEFT:
//          AC_DOWNRIGHT:
//          AC_STOP_PAN_TILT:
//          AC_ABSOLUTE_POS:
//          AC_RELATIVE_POS:
//          AC_HOME:
//          AC_RESET:
//
//   Pan  : -100 ~ 100 (left ~ right : from Camera point of view)
//   Tilt : -25  ~ 25  (down ~ up   )
//   Home Position : (0, 0)
//  AC_KEEP_CURRENT: keep the cur pan/tilt(Used only internally)
// OUTPUT : 
/////////////////////////////////////////////////////////////////
bool ActiveCam::panTilt(acConst cmd, int nextPan, int nextTilt, int speed)
{
  
  switch( cmd ){
  case AC_UP:     case AC_DOWN:    case AC_LEFT:     case AC_RIGHT:
  case AC_UPLEFT: case AC_UPRIGHT: case AC_DOWNLEFT: case AC_DOWNRIGHT: 
  case AC_STOP_PAN_TILT:
    if(SetGeneralPanTilt(ser_fd, cmd) < 0)
    {
      if(exitWhenError) PvUtil::exitError("ActiveCam::panTilt: Error, SetGeneralPanTilt!");
      else PVMSG("ActiveCam::panTilt: Error, SetGeneralPanTilt!");
      return false;
    }
    else
      return true;
    break;
  case AC_ABSOLUTE_POS:
    if(!panTilt(nextPan, nextTilt, speed))
    {
      if(exitWhenError) PvUtil::exitError("ActiveCam::panTilt: Error, panTilt!");
      else PVMSG("ActiveCam::panTilt: Error, panTilt!");
      return false;
    }
    else
      return true;
    break;
  case AC_RELATIVE_POS:
    if(SetRelPanTilt(ser_fd, nextPan, nextTilt, speed) < 0)
    {
      if(exitWhenError) PvUtil::exitError("ActiveCam::panTilt: Error, SetRelPanTilt!");
      else PVMSG("ActiveCam::panTilt: Error, SetRelPanTilt!");
      return false;
    }
    else
      return true;
    break;
  case AC_HOME:
    if(SetHomePosition(ser_fd) < 0)
    {
      if(exitWhenError) PvUtil::exitError("ActiveCam::panTilt: Error, SetHomePosition!");
      else PVMSG("ActiveCam::panTilt: Error, SetHomePosition!");
      return false;
    }
    else
      return true;
    break;
  case AC_RESET:
    if(!reset())
    {
      if(exitWhenError) PvUtil::exitError("ActiveCam::panTilt: Error, reset!");
      else PVMSG("ActiveCam::panTilt: Error, reset!");
      return false;
    }
    else
      return true;
    break;
  default:
    PvUtil::exitError("ActiveCam::panTilt: Error, Unknown Command!");
    break;
  }
  
  return false;
}

/////////////////////////////////////////////////////////////////
// Initializing EVI-D30 
//  INPUT  : NONE
//  OUTPUT : bool val of success
/////////////////////////////////////////////////////////////////
bool ActiveCam::initialize()
{
  if( SetInit(ser_fd) < 0)
  {
    if(exitWhenError) PvUtil::exitError("ActiveCam::initialize: Error, SetInit!");
    else PVMSG("ActiveCam::initialize: Error, SetInit!");
    return false;
  }
  
  if( SetHomePosition(ser_fd) < 0)
  {
    if(exitWhenError) PvUtil::exitError("ActiveCam::initialize: Error, SetHomePosition!");
    else PVMSG("ActiveCam::initialize: Error, SetHomePosition!");
    return false;
  }
  
  return true;
}

/////////////////////////////////////////////////////////////////
// Resetting Pan/Tilt
//  INPUT  : NONE
//  OUTPUT : bool val of success
// IMPORTANT: 
//   the meaning of reset is set to the current position 
//   by EVI_D30 manual, which is to repeat the last pan/tilt
//   This command initialize the camera, meaning that
//   the camera recalibrates itself with respect to pan/tilt
/////////////////////////////////////////////////////////////////
bool ActiveCam::reset()
{
  if( SetInit(ser_fd) < 0)
  {
    if(exitWhenError) PvUtil::exitError("ActiveCam::reset: Error, SetInit!");
    else PVMSG("ActiveCam::reset: Error, SetInit!");
    return false;
  }
  
  return true;
}

/////////////////////////////////////////////////////////////////
// set the zoom 
// 1 ~ 1023
// 1   : widest 
// 1023: narrowest
/////////////////////////////////////////////////////////////////
bool ActiveCam::zoom(acConst cmd, int zVal)
{
  
  switch( cmd ){
  case AC_STOP_ZOOM:
  case AC_TELE_STD:
  case AC_WIDE_STD:
    if(SetZoomStd(ser_fd, cmd) < 0)
    {
      if(exitWhenError) PvUtil::exitError("ActiveCam::zoom: Error, SetZoomStd!");
      else PVMSG("ActiveCam::zoom: Error, SetZoomStd!");
      return false;
    }
    else
    {
      return true;
    }
    break;
  case AC_TELE_VAR:
    if(SetZoomIn(ser_fd, zVal) < 0)
    {
      if(exitWhenError) PvUtil::exitError("ActiveCam::zoom: Error, SetZoomIn!");
      else PVMSG("ActiveCam::zoom: Error, SetZoomIn!");
      return false;
    }
    else
    {
      return true;
    }
    break;
  case AC_WIDE_VAR:
    if(SetZoomOut(ser_fd, zVal) < 0)
    {
      if(exitWhenError) PvUtil::exitError("ActiveCam::zoom: Error, SetZoomOut!");
      else PVMSG("ActiveCam::zoom: Error, SetZoomOut!");
      return false;
    }
    else
    {
      return true;
    }
    break;
  case AC_DIRECT_ZOOM:
    if(SetZoom(ser_fd, zVal) < 0)
    {
      if(exitWhenError) PvUtil::exitError("ActiveCam::zoom: Error, SetZoom!");
      else PVMSG("ActiveCam::zoom: Error, SetZoom!");
      return false;
    }
    else
    {
      return true;
    }
    break;
  default:
    PvUtil::exitError("ActiveCam::zoom: Error, Unknown Command!");
    break;
  }
  return false;
}

bool ActiveCam::zoom(int zVal)
{
  return zoom(AC_DIRECT_ZOOM,zVal);
}

/////////////////////////////////////////////////////////////////
// set the focus 
// 4096 ~ 40959
/////////////////////////////////////////////////////////////////
bool ActiveCam::focus(acConst cmd, int fVal)
{
  
  switch( cmd ){
  case AC_FAR:
  case AC_NEAR:
  case AC_DIRECT_FOCUS:
    if(isAutoFocus())
    {
      if(exitWhenError) PvUtil::exitError("ActiveCam::focus: Error, CHANGE the mode to AC_MANUAL_FOCUS_ON!");
      else PVMSG("ActiveCam::focus: Error, CHANGE the mode to AC_MANUAL_FOCUS_ON!");
      return false;
    }
  default:
    break;
  }
  
  
  switch( cmd ){
  case AC_STOP_FOCUS:     case AC_FAR:              case AC_NEAR:
  case AC_AUTO_FOCUS_ON:  case AC_MANUAL_FOCUS_ON:  case AC_AUTO_MANUAL:
    if(SetFocusCmd(ser_fd, cmd) < 0)
    {
      if(exitWhenError) PvUtil::exitError("ActiveCam::focus: Error, SetFocusCmd!");
      else PVMSG("ActiveCam::focus: Error, SetFocusCmd!");
      return false;
    }
    else
    {
      return true;
    }
    break;
  case AC_DIRECT_FOCUS:
    if(SetFocus(ser_fd, fVal) < 0)
    {
      if(exitWhenError) PvUtil::exitError("ActiveCam::focus: Error, SetFocus!");
      else PVMSG("ActiveCam::focus: Error, SetFocus!");
      return false;
    }
    else
    {
      return true;
    }
    break;
  default:
    PvUtil::exitError("ActiveCam::focus: Error, Unknown Command!");
    break;
  }
  
  return false;
}

// White Balance Setting
bool ActiveCam::whiteBalance(acConst cmd)
{
  switch( cmd ){
  case AC_AUTO_WB: case AC_INDOOR: case AC_OUTDOOR: 
  case AC_ONEPUSH: case AC_ONEPUSH_TRIG:    
    if( SetWB(ser_fd, cmd) < 0)
    {
      if(exitWhenError) PvUtil::exitError("ActiveCam::whiteBalance: Error, SetWB!");
      else PVMSG("ActiveCam::whiteBalance: Error, SetWB!");
      return false;
    }
    return true;
    break;
  default:
    PvUtil::exitError("ActiveCam::whiteBalance: Error, Unknown Command!");
    break;
  }
  return false;
}

// Auto Exposure Setting
bool ActiveCam::autoExposure(acConst cmd)
{
  switch( cmd ){
  case AC_FULL_AUTO: case AC_MANUAL: case AC_SHUTTER_PRIO:
  case AC_IRIS_PRIO: case AC_BRIGHT:
    if( SetAE(ser_fd, cmd) < 0)
    {
      if(exitWhenError) PvUtil::exitError("ActiveCam::autoExposure: Error, SetAE!");
      else PVMSG("ActiveCam::autoExposure: Error, SetAE!");
      return false;
    }
    return true;
    break;
  default:
    PvUtil::exitError("ActiveCam::autoExposure: Error, Unknown Command!");
    break;
  }
  return false;
}

/////////////////////////////////////////////////////////////////
//
// Inquiry functions
//
/////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////
//  bool ActiveCam::isPowerOn()
//  input : NONE
//  output : power status
//    true  : on  => Y0 50 02 FF(subtracted by 2 in GetPowerStatus)
//    false : off => Y0 50 03 FF
/////////////////////////////////////////////////////////////////
bool ActiveCam::isPowerOn()
{
  if(GetPowerStatus(ser_fd) != 2) return false;
  
  return true;
}

Vector2i ActiveCam::getPanTiltPos() 
{  
  return GetPanTiltValue(ser_fd);     
}

int ActiveCam::getZoomPos() 
{
  int zoom_value;
  
  if((zoom_value = GetPosition(ser_fd, CAM_ZOOM_POS)) < 0) exit(zoom_value);
  else return zoom_value+1;
  
}

/////////////////////////////////////////////////////////////////
//  bool ActiveCam::isAutoFocus()
//  INPUT  : NONE
//  OUTPUT : bool 
//           true  : auto focus
//           false : manual
/////////////////////////////////////////////////////////////////
bool ActiveCam::isAutoFocus()
{
  if(GetAutoFocusMode(ser_fd) != 2) return false;
  
  return true;
}

int ActiveCam::getFocusPos() 
{
  int result;
  
  if((result = GetPosition(ser_fd, CAM_FOCUS_POS)) < 0)
  {
    if(exitWhenError) PvUtil::exitError("ActiveCam::getFocusPos: Error, GetPosition!");
    else PVMSG("ActiveCam::getFocusPos: Error, GetPosition!");
  }
  else return result+1;
  
  return -1;
  
}

int ActiveCam::getShutterPos() 
{
  int result;
  
  if((result = GetPosition(ser_fd, CAM_SHUTTER_POS)) < 0)
  {
    if(exitWhenError) PvUtil::exitError("ActiveCam::getShutterPos: Error, GetPosition!");
    else PVMSG("ActiveCam::getShutterPos: Error, GetPosition!");
  }
  else return result+1;
  
  return -1;
  
}

int ActiveCam::getIrisPos()
{
  int result;
  
  if((result = GetPosition(ser_fd, CAM_IRIS_POS)) < 0)
  {
    if(exitWhenError) PvUtil::exitError("ActiveCam::getIrisPos: Error, GetPosition!");
    else PVMSG("ActiveCam::getIrisPos: Error, GetPosition!");
  }
  else return result+1;
  
  return -1;
  
}

int ActiveCam::getGainPos()
{
  int result;
  
  if((result = GetPosition(ser_fd, CAM_GAIN_POS)) < 0)
  {
    if(exitWhenError) PvUtil::exitError("ActiveCam::getGainPos: Error, GetPosition!");
    else PVMSG("ActiveCam::getGainPos: Error, GetPosition!");
  }
  else return result+1;
  
  return -1;
  
}

acConst ActiveCam::getWhiteBalance() 
{
  int result;
  
  result = GetWBMode(ser_fd);
  
  switch( result ){
  case 0:
    return AC_AUTO_WB;
  case 1:
    return AC_INDOOR;
  case 2:
    return AC_OUTDOOR;
  case 3:
    return AC_ONEPUSH;
  default:
    return AC_ERROR;
  }

} 

acConst ActiveCam::getAutoExposure() 
{
  int result;
  
  result = GetAEMode(ser_fd);
  
  switch( result ){
  case 0:
    return AC_FULL_AUTO;
  case 3:
    return AC_MANUAL;
  case 10:
    return AC_SHUTTER_PRIO;
  case 11:
    return AC_IRIS_PRIO;
  case 13:
    return AC_BRIGHT;
  default:
    return AC_ERROR;
  }

} 


////////////////////////////////////////////////////////////////////////////
//
// Low Level Functions
//
////////////////////////////////////////////////////////////////////////////
int ActiveCam::IsAck(visca_command *aC) {
  
  int is_eq;
  char tempStr[9];
  
  sprintf(tempStr, "%x %x %x", aC->pos[0], aC->pos[1], aC->pos[2]);
  is_eq = strcmp(tempStr,"90 41 ff");
  
  return (aC->length == 3 && (is_eq == 0));
}

int ActiveCam::IsCpl(visca_command *aC) {
  
  int is_eq;
  char tempStr[9];
  
  sprintf(tempStr, "%x %x %x", aC->pos[0], aC->pos[1], aC->pos[2]);
  is_eq = strcmp(tempStr,"90 51 ff");
  return (aC->length == 3 && (is_eq == 0));
  
}

void ActiveCam::PrintCmd(visca_command *aC) {
  int i;
  
  for (i=0;i<aC->length;i++) {
    PVMSG("ActiveCam: %x ",aC->pos[i]);
  }
  PVMSG("ActiveCam: \nlength: %d\n",aC->length);
}

////////////////////////////////////////////////////////////////////////////
//
// ReadFromCam(HANDLE aFd, visca_command *aCommand)
// 
// Notice: Datatype is casted for some var
//
////////////////////////////////////////////////////////////////////////////
int ActiveCam::ReadFromCam(HANDLE aFd, visca_command *aCommand) {
  int readTillNow = 0, thisTime = 1, number_of_times = 0;
#ifdef WIN32  
  BOOL bSuccess;
#endif  
  char tempStr[3];
  int is_equal;
  
  /* Read the sequence from aFd */
  
  do {
    /* Read one byte */
#ifdef WIN32    
    bSuccess = ReadFile(aFd,&(aCommand->pos[readTillNow]),1, (unsigned long *)&thisTime, &m_ov);
#else
    thisTime = read(aFd, &(aCommand->pos[readTillNow]), 1);
#endif
    if(thisTime > 0) readTillNow++; /* The number of bytes read */
    number_of_times++;
    if(number_of_times == 100) return -1;
    sprintf(tempStr, "%x",aCommand->pos[readTillNow-1]);
    is_equal = strcmp(tempStr, "ff");
  } while(thisTime > -1 && 
    readTillNow < MAXCOMMANDLENGTH && 
    ((is_equal != 0 ) || (readTillNow == 0)));
  aCommand->length = readTillNow; /* The length of the sequence */
  
  return (thisTime < 0 ? -1 : readTillNow); 
}

int ActiveCam::SendCmd(HANDLE aFd, visca_command *aCommand) {
  visca_command retCommand;
  int retrys = 0, retVal, done=0;
  int number_of_times;
  
  /* Send command and wait for an answer, retransmit if nessesary. */
  
  do {
    number_of_times = 0;
    
    /* Send a command to the camera */   
    
    while(SendToCam(aFd, aCommand) != aCommand->length) {
      if(number_of_times < RETRIES) {
        number_of_times++;
        PVMSG("ActiveCam: SendCmd: Retransmitting\n");
      }
      else {
        if(exitWhenError) PvUtil::exitError("ActiveCam::SendCmd: Error, Giving up on sending data!");
        else PVMSG("ActiveCam::SendCmd: Error, Giving up on sending data!");
        return -1; /* Failed to send the command */
      }
    } /* End while */
    
    
    /* Read from the camera. An acknowledge is expected */
    // IMPORTANT : This blocking is not necessary, so not used
    // the waitForLength() function takes care of the waiting time
    
    if(WaitForLength(aFd, &retCommand, 3, standardTimeout) < 0) {
      if(exitWhenError) PvUtil::exitError("ActiveCam::SendCmd(ACK): Error, Giving up on sending data!");
      else PVMSG("ActiveCam::SendCmd(ACK): Error, Giving up on sending data!");
      PrintCmd(&retCommand);
      return -1;  
    }
    retrys++;
  } while((retVal=IsAck(&retCommand)) == 0 && retrys < RETRIES);
  
  /* Error detection */
  
  if(!retVal) {
    if(exitWhenError) PvUtil::exitError("ActiveCam::SendCmd: Error, answer not acknowledge-signal.!");
    else PVMSG("ActiveCam::SendCmd: Error, answer not acknowledge-signal.!");
    PrintCmd(&retCommand); /* Print out the sequence */
    return(-1);
  }
  
  
  do {
    /* Read from the camera. A complete signal is expected */
    
    if(WaitForLength(aFd, &retCommand, 3, standardTimeout) < 0) {
      if(exitWhenError) PvUtil::exitError("ActiveCam::SendCmd(COMPLETE): Error, Giving up on reading data.!");
      else PVMSG("ActiveCam::SendCmd(COMPLETE): Error, Giving up on reading data.!");
      PrintCmd(&retCommand);
      return -1;  
    }
    retrys++;
  } while((retVal=IsCpl(&retCommand)) == 0 && retrys < RETRIES);
  
  /* Error detection */
  
  if(!retVal) {
    if(exitWhenError) PvUtil::exitError("ActiveCam::SendCmd: Error, answer not complete-signal.!");
    else PVMSG("ActiveCam::SendCmd: Error, answer not complete-signal.!");
    PrintCmd(&retCommand); /* Print out the sequence */
    return(-1);
  }
  
  return (0);
}

////////////////////////////////////////////////////////////////////////////
//
// SendToCam(HANDLE aFd, visca_command *aCommand)
//
// Notice: Datatype is casted for some var
//
////////////////////////////////////////////////////////////////////////////
int ActiveCam::SendToCam(HANDLE aFd,visca_command *aCommand) 
{
  int written = 0, thisTime = 0, length = aCommand->length;
  char* pos;
#ifdef WIN32  
  BOOL bSuccess;
#endif
  
  /* Write the command to aFd */

#ifdef WIN32  
  ZeroMemory(&m_ov, sizeof(m_ov));
#endif
  
  while((written < length) && (thisTime != -1)) {
    pos = (char *)(aCommand->pos+sizeof(/*unsigned*/ char)*written);
#ifdef WIN32    
    bSuccess = WriteFile(aFd, pos, length-written, (unsigned long *)&thisTime, &m_ov );
    if (bSuccess != -1) 
      thisTime++;
#else
    thisTime = write(aFd, pos, length-written);
#endif    
    written += thisTime;   }
  
  return (thisTime < 0 ? -2 : written); 
}

////////////////////////////////////////////////////////////////////////////
// 
// WaitForLength(HANDLE aFd, visca_command *aCommand, int length)
//
// Notice: Datatype is casted for some var
//
////////////////////////////////////////////////////////////////////////////
int ActiveCam::WaitForLength(HANDLE aFd, visca_command *aCommand, int length, unsigned int timeOut) 
{
  int thisTime=0, readTillNow = 0, number_of_times = 0;
  DWORD mask = 0;
#ifdef WIN32  
  BOOL bSuccess;
#else
  int bSuccess;
#endif
  
  DWORD dwRead = 0;
#ifdef WIN32  
  BOOL fWaitingOnRead = FALSE;
#else
  bool fWaitingOnRead = false;
#endif  
  DWORD dwRes;
  

#ifdef WIN32  
  m_ov.hEvent = CreateEvent(NULL,  // pointer to security attributes 
    FALSE, // flag for manual-reset event 
    FALSE, // flag for initial state 
    NULL); // pointer to event-object name 
  
  if ( m_ov.hEvent == INVALID_HANDLE_VALUE )
  {
    // Handle the error.
    PVMSG("ActiveCam: ReadData(): CreateEvent() failed\n");
    return(-1);
  }
#endif  
  
  if (!fWaitingOnRead) {
    
    // Issue read operation.
#ifdef WIN32    
    bSuccess = ReadFile(aFd,&(aCommand->pos[0]), length, (unsigned long *)&thisTime, &m_ov);
#else
    bSuccess = thisTime = read(aFd, &(aCommand->pos[0]), length);
#endif    
    aCommand->length = length;
    
    if ( !bSuccess ) {
#ifdef WIN32      
      if (GetLastError() != ERROR_IO_PENDING)     // read not delayed?
#else
      if (errno == EIO)
#endif
        // Error in communications; report it.
        PVMSG("ActiveCam:  Error in communications: ERROR_IO_PENDING;\n");
      else {
        //PVMSG("ActiveCam: !bSuccess: \n");
#ifdef WIN32	
        fWaitingOnRead = TRUE;
#else
	fWaitingOnRead = true;
#endif
      }
    }
    else {    
      // read completed immediately
      //PVMSG("ActiveCam::WaitForLength: !bSuccess: HandleASuccessfulRead(lpBuf, dwRead)\n");
    }
  }
  
  
  if (fWaitingOnRead) {

#ifdef WIN32    
    dwRes = WaitForSingleObject(m_ov.hEvent, timeOut);
    
    switch(dwRes)
    {
      
      // Read completed.
    case WAIT_OBJECT_0:
      if (!GetOverlappedResult(aFd, &m_ov, &dwRead, FALSE))
        // Error in communications; report it.
        PVMSG("ActiveCam::WaitForLength: Error in communications;\n");
      else
        // Read completed successfully.
        //PVMSG("ActiveCam::WaitForLength: HandleASuccessfulRead(lpBuf, dwRead)\n");
      
      //  Reset flag so that another opertion can be issued.
      fWaitingOnRead = FALSE;
      break;
      
    case WAIT_TIMEOUT:
      // Operation isn't complete yet. fWaitingOnRead flag isn't
      // changed since I'll loop back around, and I don't want
      // to issue another read until the first one finishes.
      PVMSG("ActiveCam::WaitForLength: Timeout\n");
      break;                       
      
    default:
      // Error in the WaitForSingleObject; abort.
      // This indicates a problem with the OVERLAPPED structure's
      // event handle.
      break;
    }
#else
    struct pollfd pfd;

    pfd.fd = aFd;
    pfd.events = POLLIN;

    while(dwRead != length)
    {
      dwRes = poll(&pfd, 1, timeOut);

      if (dwRes == 0)
      {
	// Operation isn't complete yet. fWaitingOnRead flag isn't
	// changed since I'll loop back around, and I don't want
	// to issue another read until the first one finishes.
	PVMSG("ActiveCam::WaitForLength: Timeout\n");
      }
      else if (dwRes == 1)
      {
      }
      else
      {
	// Error in the WaitForSingleObject; abort.
	// This indicates a problem with the OVERLAPPED structure's
	// event handle.
	PVMSG("ActiveCam::WaitForLength: Error\n");
      }
    }
#endif    
  }
  
  return ( length > 0 ? length : -1 );
}

////////////////////////////////////////////////////////////////////////////
//
// Init Functions
//
////////////////////////////////////////////////////////////////////////////
int ActiveCam::Init(HANDLE aFd, LPCSTR lpszPortNum) 
{
  visca_command sndCommand, revCommand;
  int number_of_times=0;
  int attempts=2;
  DWORD err_code;
  
  if ( aFd == INVALID_HANDLE_VALUE ) 
  {
    // handle the error
    PVMSG("ActiveCam: ActiveCam: CreateFile() failed, invalid handle value\n");
    err_code = GetLastError();
    PVMSG("ActiveCam: ActiveCam: Last Error Code: %ld", err_code);
    
    return -3;
  }
  
  
  if(SetupConnection(aFd) < 0) return -3;
  
  do {
    number_of_times++;
    if(AssignAddress(aFd) < 0) return -3;
    sndCommand.pos[0]='\x88'; 
    sndCommand.pos[1]='\x30';
    sndCommand.pos[2]='\x02'; 
    sndCommand.pos[3]='\xff';
    sndCommand.length=4;
    
    if(WaitForLength(aFd, &revCommand, 4, 1000) == 0) return -1;
    
  } while(!isEqual(&sndCommand, &revCommand) && number_of_times < attempts);
  
  // ClearInterface Step
  // It was NOT included in Dan's
  // But I included this segment
  if(number_of_times==attempts)  
    return -3; 
  number_of_times = 0;
  do {
    number_of_times++;
    
    if(ClearInterface(aFd) < 0) return -3;
    
    if(WaitForLength(aFd, &revCommand, 5, standardTimeout) == 0) return -1;
    
    sndCommand.pos[0]='\x88'; sndCommand.pos[1]='\x01';
    sndCommand.pos[2]='\x00'; sndCommand.pos[3]='\x01';
    sndCommand.pos[4]='\xff';
    sndCommand.length=5;
    
  } while(!isEqual(&sndCommand, &revCommand) && number_of_times < attempts);
  
  if(number_of_times==attempts) return -3;
  
  return(0);
}

int ActiveCam::isEqual(visca_command *aC, visca_command *bC) {
  int i;
  int retVal = 1;
  
  if(aC->length == bC->length) { /* The same length ? */
    for(i=0;i<aC->length && retVal;i++) 
       retVal = retVal && (bC->pos[i] == aC->pos[i]); /* The same content ? */ 
    return retVal;
  }
  else return 0;  
}

int
ActiveCam::SetupConnection(HANDLE fd)
{
#ifdef WIN32
  DCB  dcb; // structure that defines the control setting for a serial communications device
  BOOL bSuccess;
  
  bSuccess = GetCommState(fd, &dcb);
  
  if ( ! bSuccess ) 
  {
    // Handle the error.
    PVMSG("ActiveCam: ActiveCam: SetupConnection: GetCommState() failed\n");
    CloseHandle(fd);
    
    return -1;
  }
  
  //
  // Fill in the DCB: baud=9600, 8 data bits, no parity, 1 stop bit.
  // Change here the desired baud rate or other options!
  //
  
  dcb.BaudRate = 9600;
  dcb.ByteSize = 8;
  dcb.Parity   = NOPARITY;
  dcb.StopBits = ONESTOPBIT;
  
  bSuccess = SetCommState(fd, &dcb);
  
  if ( ! bSuccess ) 
  {
    // Handle the error. 
    PVMSG("ActiveCam: ActiveCam: SetupConnection: SetCommState() failed\n");
    CloseHandle(fd);
    
    
    return -1;
  }
#endif  
  return(0);
}

////////////////////////////////////////////////////////////////////////////
//
// Set Functions
//
////////////////////////////////////////////////////////////////////////////
int ActiveCam::AssignAddress(HANDLE aFd) {
  visca_command sndCommand;

  /* Camera and command id */

  sndCommand.pos[0]='\x88'; 
  sndCommand.pos[1]='\x30';
  sndCommand.pos[2]='\x01';
  
  sndCommand.pos[3]='\xff'; /* Stop byte */
  sndCommand.length=4;

  /* Send the command to the camera */
  return(SendToCam(aFd, &sndCommand));
}

int ActiveCam::ClearInterface(HANDLE aFd) {
  visca_command sndCommand;

  /* Camera and command id */

  sndCommand.pos[0]='\x88'; sndCommand.pos[1]='\x01';
  sndCommand.pos[2]='\x00'; sndCommand.pos[3]='\x01';
  
  sndCommand.pos[4]='\xff'; /* Stop byte */
  sndCommand.length=5;

  /* Send the command to the camera */

  return(SendToCam(aFd, &sndCommand));
}

int ActiveCam::ResetCameraPosition(HANDLE aFd, int pos_nr) {
  visca_command sndCommand;

  /* Error handling */

  if(pos_nr > 6 || pos_nr < 1) {
    PVMSG("ActiveCam: ActiveCam: \nOnly 6 camera positions are stored\n");
    PVMSG("ActiveCam: ActiveCam: Therefore the program will not accept position nr. %d\n",pos_nr);
    return(-242); /* Error code */
  }

  /* Camera and command id */

  sndCommand.pos[0]='\x81'; sndCommand.pos[1]='\x01'; 
  sndCommand.pos[2]='\x04'; sndCommand.pos[3]='\x3f';
 
  sndCommand.pos[4]='\x00'; /* Reset argument */
  sndCommand.pos[5]=pos_nr-1; /* Camera number */
  
  sndCommand.pos[6]='\xff'; 
  sndCommand.length=7;

  /* Send the command to the camera */

  return(SendCmd(aFd, &sndCommand));
}

int ActiveCam::SetAbsPanTilt(HANDLE aFd, int pan, int tilt, int speed) {
  visca_command sndCommand;
  long panSteps, tiltSteps;
  float pan_factor,tilt_factor;

  /* Error handling */

  if(pan > 100) {
    PVMSG("ActiveCam: \nSetAbsPanTilt: You have tried to pan %d degrees\n",pan);
    PVMSG("ActiveCam: which is outside <-100 - +100>\n");
    PVMSG("ActiveCam: Therefore pan has been reset to 100\n\n");
    pan = 100;
  }
  
  if(pan < -100) {
    PVMSG("ActiveCam: \nSetAbsPanTilt: You have tried to pan %d degrees\n",pan);
    PVMSG("ActiveCam: which is outside <-100 - +100>\n");
    PVMSG("ActiveCam: Therefore pan has been reset to -100\n\n");
    pan = -100;
  }
   
  if(tilt > 25) {
    PVMSG("ActiveCam: \nSetAbsPanTilt: You have tried to tilt %d degrees\n",tilt);
    PVMSG("ActiveCam: which is outside <-25 - +25>\n");
    PVMSG("ActiveCam: Therefore tilt has been reset to 25\n\n");
    tilt = 25;
  }
  
  if(tilt < -25) {
    PVMSG("ActiveCam: \nSetAbsPanTilt: You have tried to tilt %d degrees\n",tilt);
    PVMSG("ActiveCam: which is outside <-25 - +25>\n");
    PVMSG("ActiveCam: Therefore tilt has been reset to -25\n\n");
    tilt = -25;
  } 
  if(speed > 127) {
    PVMSG("ActiveCam: \nSetAbsPanTilt: You have tried to speed %d \n",speed);
    PVMSG("ActiveCam: which is outside <1 - 127>\n");
    PVMSG("ActiveCam: Therefore tilt has been reset to 127\n\n");
    speed = 127;
  }
  
  if(speed < 1) {
    PVMSG("ActiveCam: \nSetAbsPanTilt: You have tried to speed %d \n",speed);
    PVMSG("ActiveCam: which is outside <1 - 127>\n");
    PVMSG("ActiveCam: Therefore tilt has been reset to 1\n\n");
    speed = 1;
  } 

  /* Converting from degrees to bits */

  if(pan > 0) pan_factor = STEPSPERPANDEGRIGHT;
  else pan_factor = STEPSPERPANDEGLEFT;

  if(tilt < 0) tilt_factor = STEPSPERTILTDEGDOWN;
  else tilt_factor = STEPSPERTILTDEGUP;  
  panSteps =  0xffff & (int) (0x10000 + pan * pan_factor + 0.5);
  tiltSteps = 0xffff & (int) (0x10000 + tilt * tilt_factor + 0.5);

  /* Camera and command id */

  sndCommand.pos[0]='\x81'; sndCommand.pos[1]='\x01'; 
  sndCommand.pos[2]='\x06'; sndCommand.pos[3]='\x02';

  sndCommand.pos[4]='\xff' & speed; /* Pan speed */
  sndCommand.pos[5]='\xff' & speed; /* Tilt speed */

  /* Pan argument */

  sndCommand.pos[6]= '\xf' & (panSteps >> 12); 
  sndCommand.pos[7]= '\xf' & (panSteps >> 8);
  sndCommand.pos[8]= '\xf' & (panSteps >> 4);
  sndCommand.pos[9]= '\xf' &  panSteps;

  /* Tilt argument */

  sndCommand.pos[10]= '\xf' & (tiltSteps >> 12);
  sndCommand.pos[11]= '\xf' & (tiltSteps >> 8);
  sndCommand.pos[12]= '\xf' & (tiltSteps >> 4);
  sndCommand.pos[13]= '\xf' & tiltSteps;

  sndCommand.pos[14]='\xff'; /* Stop byte */
  sndCommand.length=15;

  /* Send the command to the camera */

  return(SendCmd(aFd, &sndCommand));
}

int ActiveCam::SetRelPanTilt(HANDLE aFd, int pan, int tilt, int speed) {
  visca_command sndCommand;
  long panSteps, tiltSteps;
  float pan_factor,tilt_factor;

  /* Error handling */

  if(pan > 100) {
    PVMSG("ActiveCam: \nSetRelPanTilt: You have tried to pan %d degrees\n",pan);
    PVMSG("ActiveCam: which is outside <-100 - +100>\n");
    PVMSG("ActiveCam: Therefore pan has been reset to 100\n\n");
    pan = 100;
  }
  
  if(pan < -100) {
    PVMSG("ActiveCam: \nSetRelPanTilt: You have tried to pan %d degrees\n",pan);
    PVMSG("ActiveCam: which is outside <-100 - +100>\n");
    PVMSG("ActiveCam: Therefore pan has been reset to -100\n\n");
    pan = -100;
  }
   
  if(tilt > 25) {
    PVMSG("ActiveCam: \nSetRelPanTilt: You have tried to tilt %d degrees\n",tilt);
    PVMSG("ActiveCam: which is outside <-25 - +25>\n");
    PVMSG("ActiveCam: Therefore tilt has been reset to 25\n\n");
    tilt = 25;
  }
  
  if(tilt < -25) {
    PVMSG("ActiveCam: \nSetRelPanTilt: You have tried to tilt %d degrees\n",tilt);
    PVMSG("ActiveCam: which is outside <-25 - +25>\n");
    PVMSG("ActiveCam: Therefore tilt has been reset to -25\n\n");
    tilt = -25;
  } 
  if(speed > 127) {
    PVMSG("ActiveCam: \nSetRelPanTilt: You have tried to speed %d \n",speed);
    PVMSG("ActiveCam: which is outside <1 - 127>\n");
    PVMSG("ActiveCam: Therefore tilt has been reset to 127\n\n");
    speed = 127;
  }
  
  if(speed < 1) {
    PVMSG("ActiveCam: \nSetRelPanTilt: You have tried to speed %d \n",speed);
    PVMSG("ActiveCam: which is outside <1 - 127>\n");
    PVMSG("ActiveCam: Therefore tilt has been reset to 1\n\n");
    speed = 1;
  } 

  /* Converting from degrees to bits */

  if(pan > 0) pan_factor = STEPSPERPANDEGRIGHT;
  else pan_factor = STEPSPERPANDEGLEFT;

  if(tilt < 0) tilt_factor = STEPSPERTILTDEGDOWN;
  else tilt_factor = STEPSPERTILTDEGUP;  
  panSteps =  0xffff & (int) (0x10000 + pan * pan_factor + 0.5);
  tiltSteps = 0xffff & (int) (0x10000 + tilt * tilt_factor + 0.5);

  /* Camera and command id */

  sndCommand.pos[0]='\x81'; sndCommand.pos[1]='\x01'; 
  sndCommand.pos[2]='\x06'; sndCommand.pos[3]='\x03';

  sndCommand.pos[4]='\xff' & speed; /* Pan speed */
  sndCommand.pos[5]='\xff' & speed; /* Tilt speed */

  /* Pan argument */

  sndCommand.pos[6]= '\xf' & (panSteps >> 12); 
  sndCommand.pos[7]= '\xf' & (panSteps >> 8);
  sndCommand.pos[8]= '\xf' & (panSteps >> 4);
  sndCommand.pos[9]= '\xf' &  panSteps;

  /* Tilt argument */

  sndCommand.pos[10]= '\xf' & (tiltSteps >> 12);
  sndCommand.pos[11]= '\xf' & (tiltSteps >> 8);
  sndCommand.pos[12]= '\xf' & (tiltSteps >> 4);
  sndCommand.pos[13]= '\xf' & tiltSteps;

  sndCommand.pos[14]='\xff'; /* Stop byte */
  sndCommand.length=15;

  /* Send the command to the camera */

  return(SendCmd(aFd, &sndCommand));
}

int ActiveCam::SetGeneralPanTilt(HANDLE aFd, acConst cmd) {
  visca_command sndCommand;
  
  /* Camera and command id */
  
  sndCommand.pos[0]='\x81'; sndCommand.pos[1]='\x01'; 
  sndCommand.pos[2]='\x06'; sndCommand.pos[3]='\x01';
  
  
  // speed is set to the fastest
  sndCommand.pos[4]='\x18'; sndCommand.pos[5]='\x14';
  
  switch( cmd ){
  case AC_UP:
    sndCommand.pos[6]='\x03'; sndCommand.pos[7]='\x01';
    break;
  case AC_DOWN:
    sndCommand.pos[6]='\x03'; sndCommand.pos[7]='\x02';
    break;
  case AC_LEFT:
    sndCommand.pos[6]='\x01'; sndCommand.pos[7]='\x03';
    break;
  case AC_RIGHT:
    sndCommand.pos[6]='\x02'; sndCommand.pos[7]='\x03';
    break;
  case AC_UPLEFT:
    sndCommand.pos[6]='\x01'; sndCommand.pos[7]='\x01';
    break;
  case AC_UPRIGHT:
    sndCommand.pos[6]='\x02'; sndCommand.pos[7]='\x01';
    break;
  case AC_DOWNLEFT:
    sndCommand.pos[6]='\x01'; sndCommand.pos[7]='\x02';
    break;
  case AC_DOWNRIGHT:
    sndCommand.pos[6]='\x02'; sndCommand.pos[7]='\x02';
    break;
  case AC_STOP_PAN_TILT:
    sndCommand.pos[6]='\x03'; sndCommand.pos[7]='\x03';
    break;
  default:  //case AC_HOME:
    break;
  }
  
  
  sndCommand.pos[8]='\xff'; /* Stop byte */
  sndCommand.length=9;
  
  /* Send the command to the camera */
  
  return(SendCmd(aFd, &sndCommand));
}

int ActiveCam::SetAgain(HANDLE aFd) {
  visca_command sndCommand;

  /* Camera id */

  sndCommand.pos[0]='\x81'; 

  sndCommand.pos[1]='\xff'; /* Stop byte */
  sndCommand.length=2;

  /* Send the command to the camera */

  return(SendCmd(aFd, &sndCommand));
}

int ActiveCam::SetCameraPosition(HANDLE aFd, int pos_nr) {
  visca_command sndCommand;

  /* Error handling */

  if(pos_nr > 6 || pos_nr < 1) {
    PVMSG("ActiveCam::SetCameraPosition \nOnly 6 camera positions can be stored\n");
    PVMSG("ActiveCam::SetCameraPosition The program will not accept position nr. %d\n",pos_nr);
    return(-242); /* Error code */
  }

  /* Camera and command id */

  sndCommand.pos[0]='\x81'; sndCommand.pos[1]='\x01'; 
  sndCommand.pos[2]='\x04'; sndCommand.pos[3]='\x3f';

  sndCommand.pos[4]='\x01'; /* Set argument */

  sndCommand.pos[5]=pos_nr-1; /* Camera number */
  
  sndCommand.pos[6]='\xff'; 
  sndCommand.length=7;

  /* Send the command to the camera */

  return(SendCmd(aFd, &sndCommand));
}

int ActiveCam::SetCameraToPresetPosition(HANDLE aFd, int pos_nr) {
  visca_command sndCommand;

  /* Error handling */

  if(pos_nr > 6 || pos_nr < 1) {
    PVMSG("ActiveCam: \nOnly 6 camera positions are stored\n");
    PVMSG("ActiveCam: Therefore the program will not accept position nr. %d\n",pos_nr);
    return(-242); /* Error code */
  }

  /* Camera and command id */

  sndCommand.pos[0]='\x81'; sndCommand.pos[1]='\x01'; 
  sndCommand.pos[2]='\x04'; sndCommand.pos[3]='\x3f';

  sndCommand.pos[4]='\x02'; /* Recall argument */
  sndCommand.pos[5]=pos_nr-1; /* Camera number */
  
  sndCommand.pos[6]='\xff'; 
  sndCommand.length=7;

  /* Send the command to the camera */

  return(SendCmd(aFd, &sndCommand));
}

int ActiveCam::SetHomePosition(HANDLE aFd) {
  visca_command sndCommand;

  /* Camera and command id */

  sndCommand.pos[0]='\x81'; sndCommand.pos[1]='\x01'; 
  sndCommand.pos[2]='\x06'; sndCommand.pos[3]='\x04';

  sndCommand.pos[4]='\xff'; /* Stop byte */
  sndCommand.length=5;

  /* Send the command to the camera */
  
  return(SendCmd(aFd, &sndCommand));
}

int ActiveCam::SetInit(HANDLE aFd) {
  visca_command sndCommand;

  /* Camera and command id */

  // IMPORTANT !!
  // Pan-tiltDrive Resetting, not for init
  sndCommand.pos[0]='\x81'; sndCommand.pos[1]='\x01'; 
  sndCommand.pos[2]='\x06'; sndCommand.pos[3]='\x05';

  sndCommand.pos[4]='\xff'; /* Stop byte */
  sndCommand.length=5;

  /* Send the command to the camera */
  
  return(SendCmd(aFd, &sndCommand));
}

int ActiveCam::SetKeyLock(HANDLE aFd, int mode) {
  visca_command sndCommand;

  /* Error handling */

  if(mode != 1 && mode != 0) {
    PVMSG("ActiveCam: \nSetKeyLock: %d is not acepted.\n",mode); 
    PVMSG("ActiveCam: Use either 1 (on) or 0 (off)\n\n");
    return -242; /* Error message */
  }

  /* Camera and command id */

  sndCommand.pos[0]='\x81'; sndCommand.pos[1]='\x01'; 
  sndCommand.pos[2]='\x04'; sndCommand.pos[3]='\x17';

  sndCommand.pos[4]=2*mode;

  sndCommand.pos[5]='\xff'; /* Stop byte */
  sndCommand.length=6;

  /* Send the command to the camera */
  
  return(SendCmd(aFd, &sndCommand));
}

int ActiveCam::SetStopPanTilt(HANDLE aFd) {
  visca_command sndCommand;

  /* Camera and command id */

  sndCommand.pos[0]='\x81'; sndCommand.pos[1]='\x01'; 
  sndCommand.pos[2]='\x06'; sndCommand.pos[3]='\x01';

  /* Build in argument */

  sndCommand.pos[4]='\x01'; sndCommand.pos[5]='\x01';
  sndCommand.pos[6]='\x03'; sndCommand.pos[7]='\x03';

  sndCommand.pos[8]='\xff'; /* Stop byte */
  sndCommand.length=9;
  
  /* Send the command to the camera */

  return(SendCmd(aFd, &sndCommand));
}

int ActiveCam::SetZoom(HANDLE aFd, int zoom_pos){
  visca_command sndCommand;  

  /* Error handling */
  if(zoom_pos > 1023){
    PVMSG("ActiveCam::SetZoom:Requested zoom value (%d) is above the max value (1023)\n",zoom_pos);
    PVMSG("ActiveCam::SetZoom:Zoom is set to 1023.\n");
    zoom_pos = 1023;
  }

  if(zoom_pos < 1){
    PVMSG("ActiveCam::SetZoom:Requested zoom value (%d) is below the min value (1)\n",zoom_pos);
    PVMSG("ActiveCam::SetZoom:Zoom is set to 1.\n");
    zoom_pos = 1;
  }
     
  /* Camera and command id */
  sndCommand.pos[0]='\x81'; sndCommand.pos[1]='\x01'; 
  sndCommand.pos[2]='\x04'; sndCommand.pos[3]='\x47';

  /* Zoom argument */
  sndCommand.pos[4]= '\xf' & (zoom_pos >> 12);
  sndCommand.pos[5]= '\xf' & (zoom_pos >> 8);
  sndCommand.pos[6]= '\xf' & (zoom_pos >> 4);
  sndCommand.pos[7]= '\xf' &  zoom_pos;

  sndCommand.pos[8]='\xff'; /* Stop byte */
  sndCommand.length=9;

  /* Send the command to the camera */
  return(SendCmd(aFd, &sndCommand));
}

///////////////////////////////////////////////////////////////////////
// used by int zoomIn(int speed)
///////////////////////////////////////////////////////////////////////
int ActiveCam::SetZoomIn(HANDLE aFd, int speed){
  visca_command sndCommand;  

  /* Error handling */

  if(speed > 7){
    PVMSG("ActiveCam: \nSetZoomIn: The requested zoom value (%d)\n", speed);
    PVMSG("ActiveCam: is above the max value (7)\n");
    PVMSG("ActiveCam: and has therefore been set to 7.\n\n");
    speed = 7;
  }

  if(speed < 2){
    PVMSG("ActiveCam: \nSetZoomIn: The requested zoom value (%d)\n", speed);
    PVMSG("ActiveCam: is below the min value (2)\n");
    PVMSG("ActiveCam: and has therefore been set to 2.\n\n");
    speed = 2;
  }
     
  /* Camera and command id */

  sndCommand.pos[0]='\x81'; sndCommand.pos[1]='\x01'; 
  sndCommand.pos[2]='\x04'; sndCommand.pos[3]='\x07';

  /* Zoom argument */

  //sndCommand.pos[4]='\x27'; // default speed fastest 7
  sndCommand.pos[4]='\x20'|('\xf' & speed);
  sndCommand.pos[5]='\xff'; /* Stop byte */
  sndCommand.length=6;

  /* Send the command to the camera */

  return(SendCmd(aFd, &sndCommand));
}

///////////////////////////////////////////////////////////////////////
// used by int zoomOut(int speed)
///////////////////////////////////////////////////////////////////////
int ActiveCam::SetZoomOut(HANDLE aFd, int speed){
  visca_command sndCommand;  

  /* Error handling */

  if(speed > 7){
    PVMSG("ActiveCam: \nSetZoomOut: The requested zoom value (%d)\n", speed);
    PVMSG("ActiveCam: is above the max value (7)\n");
    PVMSG("ActiveCam: and has therefore been set to 7.\n\n");
    speed = 7;
  }

  if(speed < 2){
    PVMSG("ActiveCam: \nSetZoomOut: The requested zoom value (%d)\n", speed);
    PVMSG("ActiveCam: is below the min value (2)\n");
    PVMSG("ActiveCam: and has therefore been set to 2.\n\n");
    speed = 2;
  }
     
  /* Camera and command id */

  sndCommand.pos[0]='\x81'; sndCommand.pos[1]='\x01'; 
  sndCommand.pos[2]='\x04'; sndCommand.pos[3]='\x07';

  /* Zoom argument */

  //sndCommand.pos[4]='\x37'; // default speed fastest 7
  sndCommand.pos[4]='\x30'|('\xf' & speed);
  sndCommand.pos[5]='\xff'; /* Stop byte */
  sndCommand.length=6;

  /* Send the command to the camera */

  return(SendCmd(aFd, &sndCommand));
}

///////////////////////////////////////////////////////////////////////
// used by int bool ActiveCam::zoom(acConst cmd, int zVal)
///////////////////////////////////////////////////////////////////////
int ActiveCam::SetZoomStd(HANDLE aFd, acConst teleWide){
  visca_command sndCommand;  
  
  
  /* Camera and command id */
  
  sndCommand.pos[0]='\x81'; sndCommand.pos[1]='\x01'; 
  sndCommand.pos[2]='\x04'; sndCommand.pos[3]='\x07';
  
  /* Zoom argument */
  
  switch( teleWide ){
  case AC_STOP_ZOOM:
    sndCommand.pos[4]='\x00';
    break;
  case AC_TELE_STD:
    sndCommand.pos[4]='\x02';
    break;
  default:  //case AC_WIDE_STD:
    sndCommand.pos[4]='\x03';
    break;
  }
  
  sndCommand.pos[5]='\xff'; /* Stop byte */
  sndCommand.length=6;
  
  /* Send the command to the camera */
  
  return(SendCmd(aFd, &sndCommand));
}

int ActiveCam::SetFocus(HANDLE aFd, int fVal){
  visca_command sndCommand;  

  /* Error handling */
  if(fVal > MAX_FOCUS){
    PVMSG("ActiveCam::SetFocus: The requested focus value (%d) is above the max value (MAX_FOCUS)\n",fVal);
    PVMSG("ActiveCam: and has therefore been set to MAX_FOCUS.\n");
    fVal = MAX_FOCUS;
  }

  if(fVal < MIN_FOCUS){
    PVMSG("ActiveCam::SetFocus: The requested focus value (%d) is below the min value (MIN_FOCUS)\n",fVal);
    PVMSG("ActiveCam: and has therefore been set to MIN_FOCUS.\n");
    fVal = MIN_FOCUS;
  }
     
  /* Camera and command id */
  sndCommand.pos[0]='\x81'; sndCommand.pos[1]='\x01'; 
  sndCommand.pos[2]='\x04'; sndCommand.pos[3]='\x48';

  /* Focus argument */
  sndCommand.pos[4]= '\xf' & (fVal >> 12);
  sndCommand.pos[5]= '\xf' & (fVal >> 8);
  sndCommand.pos[6]= '\xf' & (fVal >> 4);
  sndCommand.pos[7]= '\xf' &  fVal;

  sndCommand.pos[8]='\xff'; /* Stop byte */
  sndCommand.length=9;

  /* Send the command to the camera */
  return(SendCmd(aFd, &sndCommand));
}

///////////////////////////////////////////////////////////////////////
// used by 
///////////////////////////////////////////////////////////////////////
int ActiveCam::SetFocusCmd(HANDLE aFd, acConst cmd){
  visca_command sndCommand;  
  
  
  sndCommand.pos[0]='\x81'; sndCommand.pos[1]='\x01'; 
  sndCommand.pos[2]='\x04'; 
  
  
  switch( cmd ){
  case AC_STOP_FOCUS:
    sndCommand.pos[3]='\x08';
    sndCommand.pos[4]='\x00';
    break;
  case AC_FAR:
    sndCommand.pos[3]='\x08';
    sndCommand.pos[4]='\x02';
    break;
  case AC_NEAR:
    sndCommand.pos[3]='\x08';
    sndCommand.pos[4]='\x03';
    break;
  case AC_AUTO_FOCUS_ON:
    sndCommand.pos[3]='\x38';
    sndCommand.pos[4]='\x02';
    break;
  case AC_MANUAL_FOCUS_ON:
    sndCommand.pos[3]='\x38';
    sndCommand.pos[4]='\x03';
    break;
  default: // case AC_AUTO_MANUAL:
    sndCommand.pos[3]='\x38';
    sndCommand.pos[4]='\x10';
    break;	  
  }
  
  sndCommand.pos[5]='\xff'; /* Stop byte */
  sndCommand.length=6;
  
  /* Send the command to the camera */
  
  return(SendCmd(aFd, &sndCommand));
}

///////////////////////////////////////////////////////////////////////
// used by bool ActiveCam::whiteBalance(acConst)
///////////////////////////////////////////////////////////////////////
int ActiveCam::SetWB(HANDLE aFd, acConst cmd){
  visca_command sndCommand;  
  
  
  /* Camera and command id */
  
  sndCommand.pos[0]='\x81'; sndCommand.pos[1]='\x01'; 
  sndCommand.pos[2]='\x04'; 
  
  
  switch( cmd ){
  case AC_ONEPUSH_TRIG:
    sndCommand.pos[3]='\x10';
    break;
  default:
    sndCommand.pos[3]='\x35';
    break;
  }
  
  
  // WB argument
  switch( cmd ){
  case AC_INDOOR:
    sndCommand.pos[4]='\x01';
    break;
  case AC_OUTDOOR:
    sndCommand.pos[4]='\x02';
    break;
  case AC_ONEPUSH:
    sndCommand.pos[4]='\x03';
    break;
  case AC_ONEPUSH_TRIG:
    sndCommand.pos[4]='\x05';
    break;
  default:  //case AC_AUTO_WB:
    sndCommand.pos[4]='\x00';
    break;
  }
  
  sndCommand.pos[5]='\xff'; /* Stop byte */
  sndCommand.length=6;
  
  /* Send the command to the camera */
  
  return(SendCmd(aFd, &sndCommand));
}

///////////////////////////////////////////////////////////////////////
// used by bool ActiveCam::autoExposure(acConst)
///////////////////////////////////////////////////////////////////////
int ActiveCam::SetAE(HANDLE aFd, acConst cmd){
  visca_command sndCommand;  
  
  
  /* Camera and command id */
  
  sndCommand.pos[0]='\x81'; sndCommand.pos[1]='\x01'; 
  sndCommand.pos[2]='\x04'; sndCommand.pos[3]='\x39';
  
  
  // WB argument
  switch( cmd ){
  case AC_MANUAL:
    sndCommand.pos[4]='\x03';
    break;
  case AC_SHUTTER_PRIO:
    sndCommand.pos[4]='\x0A';
    break;
  case AC_IRIS_PRIO:
    sndCommand.pos[4]='\x0B';
    break;
  case AC_BRIGHT:
    sndCommand.pos[4]='\x0D';
    break;
  default:  //case AC_FULL_AUTO:
    sndCommand.pos[4]='\x00';
    break;
  }
  
  sndCommand.pos[5]='\xff'; /* Stop byte */
  sndCommand.length=6;
  
  /* Send the command to the camera */
  
  return(SendCmd(aFd, &sndCommand));
}


////////////////////////////////////////////////////////////////////////////
//
// Get Functions
//
////////////////////////////////////////////////////////////////////////////
ActiveCam::visca_command ActiveCam::GetAgain(HANDLE aFd) {
  visca_command sndCommand;
  visca_command revCommand;
  
  /* Camera id */
  
  sndCommand.pos[0]='\x81'; 
  
  sndCommand.pos[1]='\xff'; /* Stop byte */
  sndCommand.length=2;
  
  /* Send request */
  
  if(SendToCam(aFd,&sndCommand) < 0) {
    revCommand.length = -2;
    return revCommand;
  }
  
  /* Read the answer*/ 
  
  if((WaitForLength(aFd, &revCommand, 7, standardTimeout) < 0) || (revCommand.length != 7)) {
    PVMSG("ActiveCam: \nCouldn't read from the camera\n"); 
    revCommand.length = -1; /* Error message */
    return revCommand;
  }
  
  revCommand.length = -1; /* Error message */
  return revCommand;
  
} 

int ActiveCam::GetKeyLockStatus(HANDLE aFd) {
  visca_command sndCommand,revCommand;
  int nr_of_tries=0;
  
  /* Camera and command id */
  
  sndCommand.pos[0]='\x81'; sndCommand.pos[1]='\x09'; 
  sndCommand.pos[2]='\x04'; sndCommand.pos[3]='\x17';
  
  sndCommand.pos[4]='\xff'; /* Stop byte */
  sndCommand.length=5;
  
  /* Send request */
  
  if(SendToCam(aFd,&sndCommand) < 0) return -2;
  
  /* Read the key lock status */ 
  
  if((WaitForLength(aFd, &revCommand, 4, standardTimeout) < 0) || (revCommand.length != 4)) {
    PVMSG("ActiveCam: \nCouldn't read the status of the power on the camera \n"); 
    return -1; /* Error message */
  }
  else return(!(revCommand.pos[2]-2));
} 

Vector2i ActiveCam::GetPanTiltValue(HANDLE aFd) {
  visca_command sndCommand,revCommand;
  Vector2i result;
  int nr_of_tries=0;
  int pan,tilt,tilt_sign=1,pan_sign=1;
  float pan_factor,tilt_factor;

  /* Camera and command id */

  sndCommand.pos[0]='\x81'; sndCommand.pos[1]='\x09'; 
  sndCommand.pos[2]='\x06'; sndCommand.pos[3]='\x12';

  sndCommand.pos[4]='\xff'; /* Stop byte */
  sndCommand.length=5;

  /* Send request */

  if(SendToCam(aFd,&sndCommand) < 0) {
    result(0) = ERROR_WRITE;
    return result; 
  }

  /* Read the pan/tilt values */ 
   if((WaitForLength(aFd, &revCommand, 11, standardTimeout) < 0) || (revCommand.length != 11)) {
     PVMSG("ActiveCam: \nGetPanTiltValue: Couldn't read the pan/tilt values of the camera\n");
     result(0) = ERROR_READ; /* Error message */ 
     return result;  
  }

  /* Convert from a Hex number to the pan angle */  

  if(revCommand.pos[2] > 0) { /* A negativ pan angle */
    revCommand.pos[3] = 15 - revCommand.pos[3];
    revCommand.pos[4] = 15 - revCommand.pos[4];
    revCommand.pos[5] = 15 - revCommand.pos[5] + 1;
    pan_sign = -1;
    pan_factor = STEPSPERPANDEGLEFT;
  }
  else pan_factor = STEPSPERPANDEGRIGHT;
 
  pan = (int)(pan_sign * ((int)(256*revCommand.pos[3]+
				16*revCommand.pos[4]+
				1 + revCommand.pos[5])/pan_factor));

  //PVMSG("ActiveCam::GetPanTiltValue: Pan: %d\n", pan);

  /* Convert from a Hex number to the tilt angle */  

  if(revCommand.pos[6] > 0) { /* A negativ tilt angle */
    revCommand.pos[7] = 15 - revCommand.pos[7];
    revCommand.pos[8] = 15 - revCommand.pos[8];
    revCommand.pos[9] = 15 - revCommand.pos[9] + 1;
    tilt_sign = -1;
    tilt_factor = STEPSPERTILTDEGDOWN;
  }
  else tilt_factor = STEPSPERTILTDEGUP;

  tilt = (int)(tilt_sign * ((int)(256*revCommand.pos[7]+
				  16*revCommand.pos[8]+
				  1+ revCommand.pos[9])/tilt_factor));

  //PVMSG("ActiveCam::GetPanTiltValue: Tilt: %d\n", tilt);

  result.set(pan,tilt);


  return result; 
}

int ActiveCam::GetPowerStatus(HANDLE aFd) {
  visca_command sndCommand,revCommand;
  int nr_of_tries=0;

  /* Camera and command id */

  sndCommand.pos[0]='\x81'; sndCommand.pos[1]='\x09'; 
  sndCommand.pos[2]='\x04'; sndCommand.pos[3]='\x00';

  sndCommand.pos[4]='\xff'; /* Stop byte */
  sndCommand.length=5;

  /* Send request */
  
  if(SendToCam(aFd,&sndCommand) < 0) {
    return -2;
  }

  /* Read the power status */ 

  if((WaitForLength(aFd, &revCommand, 4, standardTimeout) < 0) || (revCommand.length != 4)) {
    PVMSG("ActiveCam: \nCouldn't read the status of the power on the camera \n"); 
    return -1;  /* Error message */
  }
  else {
    return((int)revCommand.pos[2]);
  }
}

int ActiveCam::GetZoomValue(HANDLE aFd) {
  visca_command sndCommand,revCommand;
  int zoom_value,nr_of_tries=0;

  /* Camera and command id */

  sndCommand.pos[0]='\x81'; sndCommand.pos[1]='\x09'; 
  sndCommand.pos[2]='\x04'; sndCommand.pos[3]='\x47';

  sndCommand.pos[4]='\xff'; /* Stop byte */
  sndCommand.length=5;

  /* Send request */
  
  if(SendToCam(aFd,&sndCommand) < 0) return -2;


  if((WaitForLength(aFd, &revCommand, 7, standardTimeout) < 0) || (revCommand.length != 7)) {
    PVMSG("ActiveCam: \nCouldn't read the zoomvalue of the camera \n"); 
    return -1;  /* Error message */
  }

  /* Convert from a Hex number to the zoom value */

   zoom_value = revCommand.pos[3] * 256 +
                revCommand.pos[4] * 16 +
                revCommand.pos[5];

   return(zoom_value);
}

int ActiveCam::GetPosition(HANDLE aFd, camConst cmd) {
  visca_command sndCommand,revCommand;
  int pos_value,nr_of_tries=0;
  
  /* Camera and command id */
  
  sndCommand.pos[0]='\x81'; sndCommand.pos[1]='\x09'; 
  sndCommand.pos[2]='\x04'; 
  
  switch ( cmd ){
  case CAM_ZOOM_POS:
    sndCommand.pos[3]='\x47';
    break;
  case CAM_FOCUS_POS:
    sndCommand.pos[3]='\x48';
    break;
  case CAM_SHUTTER_POS:
    sndCommand.pos[3]='\x4A';
    break;
  case CAM_IRIS_POS:
    sndCommand.pos[3]='\x4B';
    break;
  case CAM_GAIN_POS:
    sndCommand.pos[3]='\x4C';
    break;
  default:
    return -1;
  }
  
  sndCommand.pos[4]='\xff'; /* Stop byte */
  sndCommand.length=5;
  
  
  /* Send request */
  
  if(SendToCam(aFd,&sndCommand) < 0) return -2;
  
  if((WaitForLength(aFd, &revCommand, 7, standardTimeout) < 0) || (revCommand.length != 7)) {
    PVMSG("ActiveCam: \nCouldn't read the position \n"); 
    return -1;  /* Error message */
  }
  
  pos_value = revCommand.pos[3] * 256 +
    revCommand.pos[4] * 16 +
    revCommand.pos[5];
  
  return(pos_value);
}

int ActiveCam::GetAutoFocusMode(HANDLE aFd) {
  visca_command sndCommand,revCommand;
  int nr_of_tries=0;
  
  /* Camera and command id */
  
  sndCommand.pos[0]='\x81'; sndCommand.pos[1]='\x09'; 
  sndCommand.pos[2]='\x04'; sndCommand.pos[3]='\x38';
  
  sndCommand.pos[4]='\xff'; /* Stop byte */
  sndCommand.length=5;
  
  /* Send request */
  
  if(SendToCam(aFd,&sndCommand) < 0) {
    return -2;
  }
  
  /* Read the power status */ 
  
  if((WaitForLength(aFd, &revCommand, 4, standardTimeout) < 0) || (revCommand.length != 4)) {
    PVMSG("ActiveCam: \nCouldn't read the mode for auto focus of the camera \n"); 
    return -1;  /* Error message */
  }
  else {
    return((int)revCommand.pos[2]);
  }
}

int ActiveCam::GetWBMode(HANDLE aFd) {
  visca_command sndCommand,revCommand;
  int nr_of_tries=0, result;
  //HANDLE aFd = ser_fd;
  
  /* Camera and command id */
  
  sndCommand.pos[0]='\x81'; sndCommand.pos[1]='\x09'; 
  sndCommand.pos[2]='\x04'; sndCommand.pos[3]='\x35';
  
  sndCommand.pos[4]='\xff'; /* Stop byte */
  sndCommand.length=5;
  
  /* Send request */
  
  if(SendToCam(aFd,&sndCommand) < 0) return -2;
  
  
  // Read the WB Mode 
  
  if((WaitForLength(aFd, &revCommand, 4, standardTimeout) < 0) || (revCommand.length != 4)) {
    PVMSG("ActiveCam: \nCouldn't read the status of the WB Mode on the camera \n"); 
    return -1; /* Error message */
  }
  else{
    PVMSG("ActiveCam: inqWBMode(): %d\n",revCommand.pos[2]);
    PVMSG("ActiveCam: inqWBMode(): %d\n",(int)revCommand.pos[2]);
    result = (int)revCommand.pos[2];
    return result;
  }
  
} 

int ActiveCam::GetAEMode(HANDLE aFd) {
  visca_command sndCommand,revCommand;
  int nr_of_tries=0, result;
  
  /* Camera and command id */
  
  sndCommand.pos[0]='\x81'; sndCommand.pos[1]='\x09'; 
  sndCommand.pos[2]='\x04'; sndCommand.pos[3]='\x39';
  
  sndCommand.pos[4]='\xff'; /* Stop byte */
  sndCommand.length=5;
  
  /* Send request */
  
  if(SendToCam(aFd,&sndCommand) < 0) return -2;
  
  
  // Read the WB Mode 
  
  if((WaitForLength(aFd, &revCommand, 4, standardTimeout) < 0) || (revCommand.length != 4)) {
    PVMSG("ActiveCam: \nCouldn't read the status of the WB Mode on the camera \n"); 
    return -1; /* Error message */
  }
  else{
    // debugging purpose
    PVMSG("ActiveCam: GetAEMode(): %d\n",revCommand.pos[2]);
    PVMSG("ActiveCam: GetAEMode(): %d\n",(int)revCommand.pos[2]);
    result = (int)revCommand.pos[2];
    return result;
  }
  
} 

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#pragma warning(disable:4786)

#include "ImageAcquireDeviceBlank.hpp"
#include <assert.h>
#include "PvUtil.hpp"

namespace ait
{

/*********************************************************************
 * CONSTRUCTOR
 *********************************************************************/
ImageAcquireDeviceBlank::ImageAcquireDeviceBlank(void)
{
};

/*********************************************************************
 * DESTRUCTOR
 *********************************************************************/
ImageAcquireDeviceBlank::~ImageAcquireDeviceBlank(void)
{
  
	unInitialize();
};


//Initializes this device.
bool ImageAcquireDeviceBlank::initialize(int numPrevBuffers, int numConsecutiveFrameGrabs)
{
  assert(numPrevBuffers >= 0);

  if (isInitialized)
    unInitialize();

  lastPaintTime = PvUtil::time();
  paintDelay = 0; //delay paints in secs.


  //Initialize the display window.
  displayFrameFlag = false;

  //Initialize base parameters.
  numPreviousBuffers = numPrevBuffers;

  //Initialize the resolution.
  frameWidth = 320;
  frameHeight = 240;

  mFrameImage.resize(frameWidth,frameHeight);

  pParams = new ImageAcquirePropertyBag(deviceInfo->getRegPath());

  pParams->get(ImageAcquireProperty::SOURCEWIDTH)->setValuef(frameWidth);
  pParams->get(ImageAcquireProperty::SOURCEHEIGHT)->setValuef(frameHeight);
  pParams->get(ImageAcquireProperty::FPS)->setValuef(15);

  initBuffers(frameWidth,frameHeight,numPrevBuffers);

  frameCounter = 0;

  //For blank that's it.

  //Everything worked.
  isInitialized = true;

  if (PvUtil::fileExists(deviceInfo->getFile1().c_str()))
  {
    mFrameImage.load(deviceInfo->getFile1().c_str());
  }
  else
  {
    mFrameImage.setAll(0xFF000000);
  }

  //Fill buffers.
  fillBuffers();

  return true;


};

//Uninits this device.
bool ImageAcquireDeviceBlank::unInitialize()
{
	isInitialized = false;

  if (pParams)
  {
    delete pParams;
    pParams = NULL;
  }

	return true;
};

int ImageAcquireDeviceBlank::grabFrame()
{
  assert(isInitialized);

  //Pretend to grab.
  //Grab into the last position.
  //frameArray[frameArray.size()-1]->setAll(frameval);
  (*mFrames.front()->pRGBImage) = mFrameImage;
  nextFrame();
  PvUtil::sleep(1.0/15);

  return 0;
};

bool ImageAcquireDeviceBlank::isFirstFrame()
{
	return false;
};

void ImageAcquireDeviceBlank::gotoFirstFrame()
{
};

} // namespace ait

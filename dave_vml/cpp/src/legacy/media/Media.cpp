/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifdef WIN32

#include <PvUtil.hpp>

#include <dshow.h>
#include <commctrl.h>
#include <commdlg.h>
#include <stdio.h>
//#include <tchar.h>

#include "Media.hpp"
#include "directshow/MediaFilterAsync.hpp"
#include "directshow/MediaFilterSync.hpp"

namespace ait
{

///Defining where the static variable resides
float Media::volumeLookupTable[VOLUME_LOOKUP_TABLE_SIZE];

///Creating the Lookup Table. It gets called only once in the Media::Media
void Media::createVolumeLookupTable()
{
  int i;
  ///Defining a boolean variable. It makes sure that the Volume LookupTable is created only once.
  static bool isVolumeLookupTableCreated = false;
  if (!isVolumeLookupTableCreated)
  {
    for (i=0; i<VOLUME_LOOKUP_TABLE_SIZE; ++i)
      Media::volumeLookupTable[i] = pow((float)i/VOLUME_LOOKUP_TABLE_SIZE,0.33333333f);//;
      
    isVolumeLookupTableCreated = true;
  }
}


Media::Media()
: mUseExternalControl(false)
{
  HRESULT hr = S_OK;
  
  //Creating the VolumeLookupTable
  createVolumeLookupTable();
  
  if(FAILED(CoInitialize(NULL)))
  {
    PVMSG("CoInitialize Failed!\n");
    exit(1);
  }
  
  g_bAudioOnly = FALSE;
  g_lVolume = VOLUME_FULL;
  g_psCurrent = Stopped;
  pME = NULL;
  pBA = NULL;
  pBV = NULL;
  pMS = NULL;
  pMP = NULL;
  mpRenderer = NULL;
  
  loopFlag = true;
  curFrame = 0;

  pEventCallback = NULL;
}

/** DESTRUCTOR
Destructs object.
*/
Media::~Media()
{
  closeInterfaces();
  //CoUninitialize();
}

void Media::setVolume(float volume)
{
  if (volume >= 1)
    volume = (float)0.99;
  float attenuation = (volumeLookupTable[(int)(volume*VOLUME_LOOKUP_TABLE_SIZE)]-1)*10000; //(volume-1)*10000.0;
  pBA->put_Volume(attenuation);
}

/**
Determine whether media has video component or just audio.

  @remark  This function has not yet been tested. It is taken unmodified from the DirectShow example playwnd.cpp.
*/

const Vector2i Media::getSize(void) const
{
  PVASSERT(mpRenderer);
  return mpRenderer->getFrameSize();
}

void Media::setFrameRate(double frameRate)
{
  pMP->put_Rate(frameRate);
}


double Media::getTimeRemaining()
{
  double totalTime;
  double currentTime;
  pMP->get_Duration(&totalTime);
  pMP->get_CurrentPosition(&currentTime);
  return totalTime-currentTime;
}


bool Media::isLastFrame()
{
  REFTIME totalTime;
  REFTIME currentTime;
  if (pMP) 
  {
    pMP->get_Duration(&totalTime);
    pMP->get_CurrentPosition(&currentTime);
    
    if ((double)currentTime >= (double)totalTime) 
    { // loop 1 100 nanosecond increment before video actually ends. future problem?				 
      return true;
    }		
    else
    {
      return false;
    }
  }
  return false;
  
}

bool Media::isLastSoundFrame()
{
  REFTIME totalTime;
  REFTIME currentTime;
  if (pMP) {
    pMP->get_Duration(&totalTime);
    pMP->get_CurrentPosition(&currentTime);
    
    if ((double)(currentTime) >= (double)(totalTime)) {   // loop .8 100 nanosecond increments before video actually ends. future problem?				 
      return true;
    }
  }
  
  return false;
  
}

bool Media::isPlaying()
{
  return (g_psCurrent == Running);
}

void Media::reset(void)
{
  HRESULT hr;
  
  if ((!mpMediaController.get()) || (!pMS))
    return;
  
  // Stop and reset postion to beginning
  if((g_psCurrent == Paused) || (g_psCurrent == Running))
  {
    mpMediaController->stop();
    g_psCurrent = Stopped;
  }
  LONGLONG pos = 0;
  hr = pMS->SetPositions(&pos, AM_SEEKING_AbsolutePositioning ,
    NULL, AM_SEEKING_NoPositioning);
}

void Media::stop(void)
{
  if ((!mpMediaController.get()) || (!pMS))
    return;
  
  // Stop
  if((g_psCurrent == Paused) || (g_psCurrent == Running))
  {
    mpMediaController->stop();
    g_psCurrent = Stopped;
  }
}

void Media::resetSound(void)
{
  HRESULT hr;
  
  if ((!mpMediaController.get()) || (!pMS))
    return;
  
  // Stop and reset postion to beginning
  if((g_psCurrent == Paused) || (g_psCurrent == Running))
  {
    mpMediaController->stop();
    g_psCurrent = Stopped;     
  }
  LONGLONG pos = 0;
  hr = pMS->SetPositions(&pos, AM_SEEKING_AbsolutePositioning ,
    NULL, AM_SEEKING_NoPositioning);
}

/**
Pause video stream on current frame.
*/

void Media::pause(void)
{
  if (!mpMediaController.get())
    return;
  
  mpMediaController->pause();
  g_psCurrent = Paused;
}

/**
After video has run, clean up DirectX objects.  Release all interfaces.

  @remark There is currently a problem with this function not being called properly when the application is exited which causes an assertion failure
*/

void Media::closeInterfaces(void)
{
  LONGLONG pos=0;
  SAFE_RELEASE(pME);
  SAFE_RELEASE(pMS);
  SAFE_RELEASE(pMP);
  SAFE_RELEASE(pBA);
  SAFE_RELEASE(pBV);

  MediaControllerDX *pMCDX = dynamic_cast<MediaControllerDX*>(mpMediaController.get());
  if (pMCDX)
  {
    SAFE_RELEASE(pMCDX->pMC);
    SAFE_RELEASE(pMCDX->pGB);
    SAFE_RELEASE(pMCDX->pCGB);
  }

  SAFE_RELEASE(mpRenderer);
  
  // No current media state
  g_psCurrent = Init;
  
}

void Media::play(void)
{
  // Display the first frame to indicate the reset condition
  //PVMSG("Media::play : Start Playing Media\n");
  mpMediaController->play();
  g_psCurrent = Running; 
}

void Media::load(const std::string& name, BufferType bt)
{
  WCHAR wFile[MAX_PATH];
  
#ifndef UNICODE
  MultiByteToWideChar(CP_ACP, 0, name.c_str(), -1, wFile, MAX_PATH);
#else
  lstrcpy(wFile, name.c_str());
#endif
  
	HRESULT hr = S_OK;
  
  initRenderer(bt);

  mpRenderer->setInternalImageFormat(Image32::PV_RGBA);

  MediaControllerDX *pMediaController = dynamic_cast<MediaControllerDX *>(mpMediaController.get());

  if (!pMediaController)
  {
    pMediaController = new MediaControllerDX();
    mpMediaController.reset(pMediaController);

    // Get the interface for DirectShow's GraphBuilder
    hr = CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC_SERVER, IID_IGraphBuilder, 
      (void **)&pMediaController->pGB);
    if (FAILED(hr)) PvUtil::exitError("Initializing Media");

    // Get the DirectShow media controller interface.
    hr = pMediaController->pGB->QueryInterface(IID_IMediaControl,  
      (void **)&pMediaController->pMC);
    if (FAILED(hr)) PvUtil::exitError("Initializing Media");
  }

  IGraphBuilder  *pGB = pMediaController->pGB;
  
  pGB->AddFilter(mpRenderer, NULL);
  
  // Have the graph construct the appropriate graph automatically
  if (pGB->RenderFile(wFile, NULL))
  {
    PvUtil::exitError("Can't load %s.", (char*)name.c_str());
  }	
  
  // QueryInterface for DirectShow interfaces
  pGB->QueryInterface(IID_IMediaEventEx,  (void **)&pME);
  pGB->QueryInterface(IID_IMediaSeeking,  (void **)&pMS);
  pGB->QueryInterface(IID_IMediaPosition, (void **)&pMP);
  
  // Query for video interfaces, which may not be relevant for audio files
  pGB->QueryInterface(IID_IBasicVideo, (void **)&pBV);
  
  // Query for audio interfaces, which may not be relevant for video-only files
  pGB->QueryInterface(IID_IBasicAudio, (void **)&pBA);
  
  // Have the graph signal event via window callbacks for performance
  //pME->SetNotifyWindow((OAHWND)ghApp, WM_GRAPHNOTIFY, 0);
  
  mpMediaController->stop();
  g_psCurrent=Stopped;
  
  return;
}

void Media::update(double dt)
{
  // Check if the media is finished if required
  if (pEventCallback && g_psCurrent == Running)
  {
    // The following code is from the PlayWnd example in the DirectX SDK.
    LONG evCode, evParam1, evParam2;
    HRESULT hr=S_OK;
    
    while(SUCCEEDED(pME->GetEvent(&evCode, &evParam1, &evParam2, 0)))
    {
      // Spin through the events
      hr = pME->FreeEventParams(evCode, evParam1, evParam2);
      
      if(EC_COMPLETE == evCode)
      {
        if (isLooped())
        {
          LONGLONG pos=0;
        
          // Reset to first frame of movie
          hr = pMS->SetPositions(&pos, AM_SEEKING_AbsolutePositioning ,
            NULL, AM_SEEKING_NoPositioning);
            
          if (FAILED(hr))
          {
            // Some custom filters (like the Windows CE MIDI filter) 
            // may not implement seeking interfaces (IMediaSeeking)
            // to allow seeking to the start.  In that case, just stop 
            // and restart for the same effect.  This should not be
            // necessary in most cases.
            mpMediaController->stop();
            mpMediaController->play();
          }
        }
        else
        {
          // The media is over and we are not looping.
          g_psCurrent = Stopped;
          // Send the callback event.
          pEventCallback->eventCallback(callbackEventType,pCallbackObject,0,NULL);
        }
      }
    }
  }
}

bool Media::getFrame(MediaFrameInfoPtr pMediaFrameInfo)
{
  return mpRenderer->updateFrame(pMediaFrameInfo);
}

bool Media::pushReadBuffer(MediaFrameInfoPtr pNextFrameInfo)
{
  ait::MediaFilterSync *p;
  p = dynamic_cast<ait::MediaFilterSync *>(mpRenderer);
  if (!p) PvUtil::exitError("Invalid initialization. Media must have double buffer enabled.");
  p->pushReadBuffer(pNextFrameInfo);
  return true;
}

void Media::setNotifyEvent(EventCallback *pEC, int eventType0, void *pObject)
{
  pEventCallback = pEC;
  callbackEventType = eventType0;
  pCallbackObject = pObject;
}

bool 
Media::useExternalControl()
{
  return mUseExternalControl;
}

void 
Media::setExternalControl(MediaControllerPtr pController)
{
  if (mpMediaController.get())
  {
    PvUtil::exitError("External control must be set before initializing\n");
  }
  mpMediaController = pController;
  mUseExternalControl = true;
}

void 
Media::initRenderer(BufferType bt)
{
  HRESULT hr = S_OK;

  // Create the Texture Renderer object
  switch(bt)
  {
  case BLOCKING_SINGLE:
  case BLOCKING_DOUBLE:
    mpRenderer = new MediaFilterSync(NULL, &hr);
		dynamic_cast<MediaFilterSync*>(mpRenderer)->setGrabNumberOfConsecutiveFrames(1);
    break;
	case BLOCKING_TWO_CONSECUTIVE_FRAMES:
		mpRenderer = new MediaFilterSync(NULL, &hr);	
		dynamic_cast<MediaFilterSync*>(mpRenderer)->setGrabNumberOfConsecutiveFrames(2);
		break;
  case NON_BLOCKING_SINGLE:
    mpRenderer = new MediaFilterAsync(NULL, &hr);
    break;
  }

  mpRenderer->AddRef();

  mBufferType = bt;

  PVASSERT(SUCCEEDED(hr));
}

boost::mutex&
Media::getMutex()
{
  if (mBufferType != NON_BLOCKING_SINGLE)
  {
    PvUtil::exitError("Mutex is only available for non-blocking media objects.");
  }
  MediaFilterAsync *p = dynamic_cast<MediaFilterAsync*>(mpRenderer);
  if (!p)
  {
    PvUtil::exitError("Invalid filter for non-blocking media.");
  }
  return p->getMutex();
}


} // namespace ait


#endif

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#pragma warning(disable:4786)

#include "ImageAcquireDeviceDirectShow.hpp"
#include <assert.h>
#include <dshow.h>

#ifdef HIGH_PERF_LOG
#include "HighPerfLog.hpp"
extern ait::HighPerfLog gLog;
#endif

namespace ait
{

ImageAcquireDeviceDirectShow::ImageAcquireDeviceDirectShow()
{
}


ImageAcquireDeviceDirectShow::~ImageAcquireDeviceDirectShow()
{
	unInitialize();
};

bool ImageAcquireDeviceDirectShow::initialize(int numPrevBuffers, int numConsecutiveFrameGrabs)
{
	assert(numPrevBuffers >= 0);

	if (isInitialized)
		unInitialize();


  lastPaintTime = PvUtil::time();
  paintDelay = 0; //delay paints in secs.

	//Initialize base parameters.
	numPreviousBuffers = numPrevBuffers;
	lastFrame = -1;

	//Player not started.
	isDirectShowStarted = false;

  captureDevice.setSettingsPath(deviceInfo->getRegPath());
	
	mNumConsecutiveFrameGrabs = numConsecutiveFrameGrabs;
	switch(mNumConsecutiveFrameGrabs)
	{
	case 1:
		captureDevice.setRenderMethod(Media::BLOCKING_DOUBLE);
		break;
	case 2:
		captureDevice.setRenderMethod(Media::BLOCKING_TWO_CONSECUTIVE_FRAMES);
		break;
	default:
		captureDevice.setRenderMethod(Media::BLOCKING_DOUBLE);
		break;
	}

  if (captureDevice.initCaptureDevice(deviceInfo->getdeviceName(),deviceInfo->getdeviceIndex(),"") != S_OK)
  {
    captureDevice.closeInterfaces();
    PvUtil::exitError("Initializing Capture Device");
  }

  pParams = captureDevice.getPropertyBag();

  Vector2i windowSize = captureDevice.getSize();

  isDirectShowStarted = true;
  
  frameWidth=windowSize(0);
  frameHeight=windowSize(1);

  initBuffers(frameWidth,frameHeight,numPrevBuffers);

	frameCounter = 0;

  // Load the test image if it exists.
  mTestMode = PvUtil::fileExists(deviceInfo->getFile1().c_str());

  if (mTestMode)
  {
    mTestFrame.load(deviceInfo->getFile1().c_str());
  }

	isInitialized = true;

  // Set the first num advanced buffers.
	for(int i=0; i<mNumConsecutiveFrameGrabs; i++)
		captureDevice.pushReadBuffer(mFrames[mFrames.size()-(mNumConsecutiveFrameGrabs-i)]);
	
  fillBuffers();

  // Now stop the graph. We want to Synchronize all the cameras with the first
  // grabFrame call for all threads.
  //gLog.write("Stop!");
  captureDevice.stop();
  //gLog.write("Pause done.");

	return true;
};

bool ImageAcquireDeviceDirectShow::unInitialize()
{
	isInitialized = false;

  captureDevice.closeInterfaces();

  return true;
};

int ImageAcquireDeviceDirectShow::grabFrame()
{
	assert(isInitialized);

	// If the filter is not running, set it to run now.
	if (!captureDevice.isPlaying())
	{
		if (mIsSynchronized)
		{
			// This trick is used to maximize synchronization. It will put
			// the filter in play state, so they all load into memory all the
			// values, and then stop, and play again. Experiments with IEEE1394
			// cameras have proven this a successful technique.
			//gLog.write("Start Sleep 500 ms " + deviceInfo->getdeviceDriverID());
			PvUtil::sleep(.250);
			//gLog.write("Start play " + deviceInfo->getdeviceDriverID());
			captureDevice.play();
			//gLog.write("Done play command " + deviceInfo->getdeviceDriverID());
			PvUtil::sleep(.50);
			//gLog.write("Start stop2 " + deviceInfo->getdeviceDriverID());
			captureDevice.stop();
			//gLog.write("Done stop2 " + deviceInfo->getdeviceDriverID());
			PvUtil::sleep(.250);
			//gLog.write("Start play2 " + deviceInfo->getdeviceDriverID());
			captureDevice.play();
			//gLog.write("Done play2 " + deviceInfo->getdeviceDriverID());
			PvUtil::sleep(.1);
		}
		captureDevice.play();
	}

	//gLog.write(ait::String("Start capture %s",deviceInfo->getdeviceDriverID().c_str()));
	// Set Advanced buffer for double-buffering.
	
	for(int i=0; i<mNumConsecutiveFrameGrabs; i++)
		captureDevice.pushReadBuffer(mFrames[mFrames.size()-(mNumConsecutiveFrameGrabs*2-i)]);


	for(int i=0; i<mNumConsecutiveFrameGrabs; i++)
		captureDevice.getFrame(mFrames[mFrames.size()-(mNumConsecutiveFrameGrabs-i)]);


	//gLog.write(ait::String("End capture %s",deviceInfo->getdeviceDriverID().c_str()));

	if (mTestMode)
	{
		*(mFrames.front()->pRGBImage) = mTestFrame;
	}

	nextFrame();	

	return 0;
};


bool ImageAcquireDeviceDirectShow::isFirstFrame()
{
	return false;
};

void ImageAcquireDeviceDirectShow::gotoFirstFrame()
{
};

void 
ImageAcquireDeviceDirectShow::setSyncMaster(ImageAcquireDeviceBasePtr pControllingDevice)
{
  ImageAcquireDeviceDirectShow *pCD = dynamic_cast<ImageAcquireDeviceDirectShow*>(pControllingDevice.get());
  if (!pCD)
  {
    PVMSG("warning: could not synchronize incompatible devices\n");
    mIsSynchronized = false;
  }
  else
  {
    mIsSynchronized = true;
    MediaControllerPtr pMC = pCD->captureDevice.getMediaController();
    if (pMC.get() != captureDevice.getMediaController().get())
    {
      captureDevice.setExternalControl(pMC);
    }
    PVMSG("Devices synchronized!\n");
  }
}

} // namespace ait

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "ICameraDriverEvents.hpp"

namespace ait 
{

ICameraDriverEvents::ICameraDriverEvents()
{
}

ICameraDriverEvents::~ICameraDriverEvents()
{
}

//
// OPERATIONS
//

//
// ACCESS
//

//
// INQUIRY
//

}; // namespace ait


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include "legacy/media/ImageAcquireDeviceBase.hpp"
#include "legacy/low_level/Settings.hpp"

#include <cstring>
using namespace std;

#pragma warning(disable:4786)

namespace ait
{

ImageAcquireDeviceObserverPtr ImageAcquireDeviceBase::mpObserver;

ImageAcquireDeviceBase::ImageAcquireDeviceBase(void) :
 mNumConsecutiveFrameGrabs(1),
 mSaveNextFrame(false)
{
  isInitialized = false;
  displayFrameFlag = false;
  interactive = false;
  pParams = NULL;
	mNumConsecutiveFrameGrabs=1;
#ifdef WIN32
  hwnd1 = NULL;
  hBitmap = NULL;	
  hInstance1 = NULL;
  mDestroyWindowFlag = false;
#endif
}

ImageAcquireDeviceBase::~ImageAcquireDeviceBase(void)
{
#ifdef WIN32
  destroyWindow();
#endif
//  if (deviceInfo)
//  {
//    deviceInfo->saveSettings();
//  }
}

const Image32& ImageAcquireDeviceBase::getReferenceToFrame(int frameIndex)
{
  PVASSERT(isInitialized);
  PVASSERT(frameIndex >= 0);
  PVASSERT(frameIndex < mFrames.size()-2);
  
  //Returns reference to indexed frame.
  return *(mFrames[frameIndex]->pRGBImage);
};

void ImageAcquireDeviceBase::launchInteractive()
{
  // don't launch twice.
  if (interactive) return;

//  if (deviceInfo.get() && pParams)
//  {
//    deviceInfo->saveSettings();
//    pParams->saveSettings();
//  }

#ifdef WIN32  
  string command = "pythonw " + Settings::replaceVars("<CommonFilesPath>") + "/script/CameraParameters.py " + deviceInfo->getdeviceDriverID();

  string params = command.substr(command.find(' ')+1,command.length());
  command = command.substr(0,command.find(' '));

  PVMSG("Launching '%s %s'\n",command.c_str(),params.c_str());
  if (!ShellExecute(NULL, NULL, command.c_str(), params.c_str(), NULL, SW_SHOWNORMAL))
  {
  }
  else
  {
    Settings s(deviceInfo->getRegPath());
    s.deleteValue("interactiveDone");
    string str = deviceInfo->getdeviceDriverID() + " (Updating Params)";
    SetWindowText(hwnd1,str.c_str());
    interactive = true;
  }
#endif
}

void ImageAcquireDeviceBase::setInteractive(bool flag)
{
  interactive = flag;
}

void 
ImageAcquireDeviceBase::setObserver(ImageAcquireDeviceObserverPtr pObserver)
{
  mpObserver = pObserver;
}

void ImageAcquireDeviceBase::processInteractive()
{
#ifdef WIN32
  if (!deviceInfo || !interactive) return;

  // This could be more efficient

  Settings s(deviceInfo->getRegPath());

  int state = s.getInt("interactiveNotification",-1,false);
  if (state >= 0)
  {
    s.deleteValue("interactiveNotification");

    // Update all settings at once...
    for (int i = 0; i < ImageAcquireProperty::LAST_PROPERTY_CODE; i++)
    {
      ImageAcquireProperty::Code pc = (ImageAcquireProperty::Code)(i);
      ImageAcquirePropertyPtr p = pParams->get(pc);
      if (p->getAvailable() && p->getInteractive())
      {
        p->readFromSettings(s,pc);
      }
    }
  }

  if (s.getInt("interactiveDone",0,false) == 1)
  {
    SetWindowText(hwnd1,deviceInfo->getdeviceDriverID().c_str());
    s.deleteValue("interactiveDone");
    interactive = false;
  }
#endif
}

int ImageAcquireDeviceBase::displayFrame(bool dispFrame)
{
#ifdef WIN32  
  PVASSERT(isInitialized);

  if (mDestroyWindowFlag)
  {
    destroyWindow();
    mDestroyWindowFlag = false;
  }

  if (!dispFrame)
  {
    if (displayFrameFlag)
    {
		  ShowWindow(hwnd1,SW_HIDE);
    }
  }
  else
  {
    processInteractive();
    if (!hwnd1)
    {
      string str = deviceInfo->getdeviceDriverID();
      createWindow((char *)str.c_str(), 
        Rectanglei(deviceInfo->getWindowPos()(0), deviceInfo->getWindowPos()(1),
          deviceInfo->getWindowPos()(0)+deviceInfo->getWindowSize()(0),
          deviceInfo->getWindowPos()(1)+deviceInfo->getWindowSize()(1)),
        Vector2i(pVisualizationFrame->width(),
          pVisualizationFrame->height()), 
          deviceInfo->windowHasBorder(),
          (HWND)deviceInfo->getParentHwnd());
    }

    if (!displayFrameFlag)
    {
  		ShowWindow(hwnd1,SW_SHOWNORMAL);
    }
    else
    {
      InvalidateRect(hwnd1,NULL,false);
    }
  }

  displayFrameFlag = dispFrame;
#endif

  return 0;
}

void
ImageAcquireDeviceBase::setVisualizationFrameSize(const Vector2i& size)
{
  if (size == mVisualizationFrameSize)
  {
    return;
  }

#ifdef WIN32
  if (hwnd1)
  {
    mDestroyWindowFlag = true;
    displayFrameFlag = false;
  }
#endif

  mVisualizationFrameSize = size;
}

#ifdef WIN32
void ImageAcquireDeviceBase::createWindow(char* title, const Rectanglei& windowRect, 
                                          const Vector2i& bufferSize, bool hasBorder,
                                          HWND parentHwnd)
{
	PVASSERT(isInitialized);

  WNDCLASS    wc;
  
  if (!hInstance1) {
    hInstance1 = GetModuleHandle(NULL);
    wc.style         = CS_DBLCLKS;
    wc.lpfnWndProc   = (WNDPROC)ImageAcquireDeviceBase::windowProc;
    wc.cbClsExtra    = 0;
    wc.cbWndExtra    = 0;
    wc.hInstance     = hInstance1;
    wc.hIcon         = LoadIcon(NULL, IDI_WINLOGO);
    wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = NULL;
    wc.lpszMenuName  = NULL;
    wc.lpszClassName = "Acquire Preview Class";

    if (!RegisterClass(&wc)) {
      //PVMSG("AcquireDevice Window Class was already registered...\n");
    }
  }

  DWORD dwStyle = (parentHwnd ? WS_CHILD : WS_POPUP) | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
  DWORD dwStyleEx = 0;
#ifndef _DEBUG
  dwStyleEx |= WS_EX_TOPMOST;
#endif
  if (hasBorder) 
  {
    dwStyle |= WS_OVERLAPPED | WS_CAPTION | (parentHwnd ? 0 : WS_SYSMENU)  | WS_SIZEBOX;
    dwStyleEx |= WS_EX_TOOLWINDOW;
  }

  RECT rc;
  rc.top = 0;
  rc.left = 0;

  if (windowRect.width() == 0)
  {
    rc.right = bufferSize(0);
    rc.bottom = bufferSize(1);
  }
  else
  {
    rc.right = windowRect.width();
    rc.bottom = windowRect.height();
  }

  AdjustWindowRectEx(&rc,dwStyle,0,dwStyleEx);

  hwnd1 = CreateWindowEx(dwStyleEx,
                      "Acquire Preview Class", 
                      title, 
                      dwStyle,
                      windowRect.x0, windowRect.y0, 
                      rc.right-rc.left, rc.bottom-rc.top, 
                      parentHwnd, NULL, hInstance1, 
                      this); // This will tell ImThread::WinProcStub about this object

  if (hwnd1 == NULL) {
    MessageBox(NULL, "CreateWindow() failed:  Cannot create a window.",
               "Error", MB_OK);
    return;
  }

  HDC hDC = GetDC(hwnd1);
  BITMAPINFO bi;

  memset(&bi,0,sizeof(bi));

  bi.bmiHeader.biSize = sizeof(bi.bmiHeader);
  bi.bmiHeader.biBitCount = 32;
  bi.bmiHeader.biWidth = bufferSize(0);
  bi.bmiHeader.biHeight = -bufferSize(1);
  bi.bmiHeader.biPlanes = 1;

  hBitmap = CreateDIBSection(hDC,&bi,DIB_RGB_COLORS,(void **)&pBitmapData,NULL,0);
  hBitmapDC = CreateCompatibleDC(hDC);
  SelectObject(hBitmapDC,hBitmap);
  ReleaseDC(hwnd1,hDC);
};

void
ImageAcquireDeviceBase::destroyWindow()
{
  if (hBitmap)
  {
    DeleteObject(hBitmap);
    DeleteDC(hBitmapDC);
    hBitmap = NULL;
  }
  if (hwnd1)
  {
    DestroyWindow(hwnd1);
    hwnd1 = NULL;
  }
}

LRESULT CALLBACK ImageAcquireDeviceBase::windowProc(HWND hwnd,UINT uMsg, WPARAM wParam,LPARAM lParam)
{ 
  ImageAcquireDeviceBase *pThis;

  if (uMsg == WM_CREATE) {
		pThis = (ImageAcquireDeviceBase *)((LPCREATESTRUCT)lParam)->lpCreateParams;
		SetWindowLong(hwnd,GWL_USERDATA, (long)pThis);
  }

  pThis = (ImageAcquireDeviceBase *)GetWindowLong(hwnd,GWL_USERDATA);
  if (pThis) {
    return pThis->windowProc2(hwnd,uMsg,wParam,lParam);
  } else {
    // Some messages are received before WM_CREATE, and must
    // be handled (the user data is not yet initialized)
    return DefWindowProc(hwnd,uMsg,wParam,lParam);
  }
};

LRESULT ImageAcquireDeviceBase::windowProc2(HWND hwnd,UINT uMsg, WPARAM wParam,LPARAM lParam)
{
  PAINTSTRUCT ps;
	
	switch(uMsg) 
  {	
	case WM_PAINT:
		BeginPaint(hwnd, &ps);
    hDC = ps.hdc;
		paint();
    hDC = NULL;
		EndPaint(hwnd, &ps);
		return 0;
		
	case WM_SIZE:
    mWindowSize.set(LOWORD(lParam),HIWORD(lParam));
		return 0;

  case WM_MOVE:
    {
      // We want the window rect. lParam has only the position of the client area.
      // Don't update if the window is not in normal state...
      if (!IsIconic(hwnd) && !IsZoomed(hwnd))
      {
        RECT rect;
        GetWindowRect(hwnd,&rect);
        deviceInfo->setWindowPos(Vector2i(rect.left,rect.top));
      }
    }
    return 0;
		
	case WM_CLOSE:
    if (mpObserver.get())
    {
      mpObserver->close(this);
    }
		return 0;

  case WM_LBUTTONDBLCLK:
    launchInteractive();
    return 0;
  
  case WM_KEYDOWN:
    switch(wParam)
    {
    case 'S':
      mSaveNextFrame=true;
    }
    return 0;

	}
	return DefWindowProc(hwnd, uMsg, wParam, lParam); 
};
#endif
void ImageAcquireDeviceBase::paintVideoFrame(Vector2i pos, Vector2i windowSz, const Image32 &image)
{
  PVASSERT(isInitialized);

  int size = image.width()*image.height();

  unsigned char *pSrc = (unsigned char *)image.pointer();
  unsigned char *pDst = pBitmapData;

  /*
  while (size--)
  {
    pDst[2] = pSrc[0];
    pDst[1] = pSrc[1];
    pDst[0] = pSrc[2];

    pDst += 4;
    pSrc += 4;
  }
  */

  memcpy(pDst,pSrc,size*4);

#ifdef WIN32
  if (windowSz(0) == image.width() && windowSz(1) == image.height())
  {
    BitBlt(hDC,0,0,windowSz(0),windowSz(1),hBitmapDC,0,0,SRCCOPY);
  }
  else
  {
    /*
    BLENDFUNCTION blendFunc;
    memset(&blendFunc,0,sizeof(blendFunc));
    blendFunc.BlendOp = AC_SRC_OVER;
    blendFunc.AlphaFormat = AC_SRC_ALPHA;
    blendFunc.SourceConstantAlpha = 255;
    AlphaBlend(hDC,0,0,windowSz(0),windowSz(1),hBitmapDC,
      0,0,image.width(),image.height(),blendFunc);
    */
    StretchBlt(hDC,0,0,windowSz(0),windowSz(1),hBitmapDC,
      0,0,image.width(),image.height(),SRCCOPY);
  }
#endif
  
  if(mSaveNextFrame)
  {
    Image32 img(image);
    char filename[255];
    sprintf(filename, "frame%4d.png",frameCounter);
    img.savepng(filename);
    mSaveNextFrame=false;
  }
  return;
};

void ImageAcquireDeviceBase::paint(Image32& vidFrame)
{
	PVASSERT(isInitialized);

  paintVideoFrame(Vector2i(0,0), mWindowSize, vidFrame);
};


int ImageAcquireDeviceBase::paint()
{
  PVASSERT(isInitialized);

#ifdef WIN32
  if (mDestroyWindowFlag) return 0;
#endif

  lastPaintTime = PvUtil::time();
	
  if (!pVisualizationFrame.get())
  {
    PvUtil::exitError("Visualization frame is not set!");
  }

  PVASSERT((mVisualizationFrameSize == Vector2i()) || 
    (Vector2i(pVisualizationFrame->width(),
               pVisualizationFrame->height()) == mVisualizationFrameSize));

  paint(*pVisualizationFrame);

  return 0;
};

void
ImageAcquireDeviceBase::initBuffers(int frameWidth, int frameHeight, int numPrevBuffers)
{
  mFrames.setCapacity(numPrevBuffers+1);

  for (int i = 0; i < mFrames.capacity(); i++)
  {
    MediaFrameInfoPtr pInfo(new MediaFrameInfo);
    pInfo->pRGBImage.reset(new Image32(frameWidth,frameHeight));
    pInfo->pRGBImage->setAll(0);
    pInfo->pRGBImage->setInternalFormat(Image32::PV_RGBA);
    mFrames.push_front(pInfo);
  }
}

void
ImageAcquireDeviceBase::fillBuffers()
{
	int i;
	for (i=0;i<mFrames.size()/mNumConsecutiveFrameGrabs;i++)
  {
		grabFrame();
  }
}


void
ImageAcquireDeviceBase::nextFrame()
{
	MediaFrameInfoPtr pFrameInfo;
	
	std::list<MediaFrameInfoPtr> tmpFrames;

	for(int i=0; i<mNumConsecutiveFrameGrabs; i++)
	{
		pFrameInfo = mFrames.back();	
		tmpFrames.push_front(pFrameInfo);
		mFrames.pop_back();
	}

	for(int i=0;i<mNumConsecutiveFrameGrabs;i++)
	{
		mFrames.push_front(tmpFrames.back());
		tmpFrames.pop_back();
	}

	frameCounter+= mNumConsecutiveFrameGrabs;
}

Image32* 
ImageAcquireDeviceBase::getPointerToFrame(int frameIndex)
{

	//Returns reference to indexed frame.
	return getFrameInfo(frameIndex)->pRGBImage.get();
};

MediaFrameInfoPtr
ImageAcquireDeviceBase::getFrameInfo(int frameIndex)
{
	PVASSERT(isInitialized);
	PVASSERT(frameIndex >= 0);
	PVASSERT(frameIndex < mFrames.size());
  return mFrames[frameIndex];
}

} // namespace ait

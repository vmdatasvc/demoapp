/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include "legacy/media/VideoReaderAVIOpenCV.hpp"
#include <legacy/pv/PvUtil.hpp>

// constructor
VideoReaderAVIOpenCV::VideoReaderAVIOpenCV()
: mIsLooping(false)
{
};

// destructor
VideoReaderAVIOpenCV::~VideoReaderAVIOpenCV()
{
	// call to make sure that resources are freed
	finish();
};



bool VideoReaderAVIOpenCV::init(unsigned int &w,unsigned  int &h, const char *name)
{
	mpVideoReader.reset(new cv::VideoCapture);

  // open url
  if (!mpVideoReader->open(name))
  {
	  // error opening
	  return false;
  }

  w = (int)mpVideoReader->get(CV_CAP_PROP_FRAME_WIDTH);
  h = (int)mpVideoReader->get(CV_CAP_PROP_FRAME_HEIGHT);

  return true;
}


bool VideoReaderAVIOpenCV::readFrame(ait::Image32& frame, bool BGRMode)
{

	cv::Mat	cvFrame;

	*mpVideoReader >> cvFrame;

	if (cvFrame.empty())
	{
		// reached end of file
		if (mIsLooping)
		{
			mpVideoReader->set(CV_CAP_PROP_POS_FRAMES,0.0);
			*mpVideoReader >> cvFrame;
			PVASSERT(!cvFrame.empty());
		}
		else {
			return false;
		}
	}

	// convert cvFrame to frame
	cv::Mat_<unsigned int> pImgCVHeader(frame.height(),frame.width(),(unsigned int*)frame.pointer());
	if (BGRMode)
	{
		cv::cvtColor(cvFrame,pImgCVHeader,CV_BGR2BGRA);
	}
	else
	{
		cv::cvtColor(cvFrame,pImgCVHeader,CV_BGR2RGBA);
	}


	frame.setInternalFormat(BGRMode ? ait::Image32::PV_BGRA : ait::Image32::PV_RGBA);
//  mpVideoReader->readFrame(frame);

//	return !mpVideoReader->isEndOfFile();
	return true;
}

int VideoReaderAVIOpenCV::getNumFrames()
{
//  return mpVideoReader->getNumberOfFrames();
	  return ((int)mpVideoReader->get(CV_CAP_PROP_FRAME_COUNT));
}

void VideoReaderAVIOpenCV::finish(void)
{
  mpVideoReader.reset();
}

void VideoReaderAVIOpenCV::setCurrentFrame(long fNum)
{
//  mpVideoReader->setCurrentFrameNumber(fNum);
	mpVideoReader->set(CV_CAP_PROP_POS_FRAMES,double(fNum));
}

void VideoReaderAVIOpenCV::setLoopMode(bool flag)
{ 
  mIsLooping=flag; 
}

bool VideoReaderAVIOpenCV::isFirstFrame(void)
{ 
  return  (getCurrentFrameNumber()==0); 
}

float VideoReaderAVIOpenCV::getFrameRate()
{ 
//  return (float)mpVideoReader->getFramesPerSecond();
  return( (int)mpVideoReader->get(CV_CAP_PROP_FPS));
}

int VideoReaderAVIOpenCV::getCurrentFrameNumber(void)
{ 
//  return (int)mpVideoReader->getCurrentFrameNumber();
	return ((int)mpVideoReader->get(CV_CAP_PROP_POS_FRAMES));
}



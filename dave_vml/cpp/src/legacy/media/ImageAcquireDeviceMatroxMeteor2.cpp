/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#pragma warning(disable:4786)

#include "ImageAcquireDeviceMatroxMeteor2.hpp"
#include <assert.h>

namespace ait
{

class MILProperty : public ImageAcquireProperty
{
public:

  MILProperty(ImageAcquireDeviceMatroxMeteor2 *pDevice0) : pDevice(pDevice0) {};
  virtual ~MILProperty() {}

  virtual ImageAcquireProperty *clone() { return new MILProperty(*this); }

  void setToDevice() { pDevice->setMILProperty(*this,true); };
  void getFromDevice() { pDevice->setMILProperty(*this,false); };

protected:

  ImageAcquireDeviceMatroxMeteor2 *pDevice;
};

/** Constructor
*/
ImageAcquireDeviceMatroxMeteor2::ImageAcquireDeviceMatroxMeteor2(void)
{
  //MilDisplayOn = false;
}

/*********************************************************************
* DESTRUCTOR
*********************************************************************/
ImageAcquireDeviceMatroxMeteor2::~ImageAcquireDeviceMatroxMeteor2(void)
{
  unInitialize();
}

bool ImageAcquireDeviceMatroxMeteor2::initialize(int numPrevBuffers)
{
  assert(numPrevBuffers >= 0);
  
  lastPaintTime = PvUtil::time();
  paintDelay = 0; //delay paints in secs.
  
  if (isInitialized)
    unInitialize();
  
  //Initialize base parameters.
  numPreviousBuffers = numPrevBuffers;
  
  //Initialize the resolution.
  frameWidth = 320;
  frameHeight = 240;
  
  if (MilInitialization() != 0)
    PvUtil::exitError("ImageAcquireDeviceMatroxMeteor2::initialize: error Mil Failed to intialize.");
  
  //Initialize buffers.
  initBuffers(frameWidth,frameHeight,numPrevBuffers);

  // Create the property bag
  pParams = new ImageAcquirePropertyBag(deviceInfo->getRegPath(),new MILProperty(this));

  //init frame ct.
  frameCounter = 0;
  
  //Everything worked.
  isInitialized = true;
  
  mMilImageIndex = 0;
  
  //Fill buffers.
  fillBuffers();

  return true;
  
};

bool ImageAcquireDeviceMatroxMeteor2::unInitialize()
{
  isInitialized = false;
  
  //Halt the digitizer.
  MdigHalt(MilDigitizer);
  checkError();
  
  if (pParams) delete pParams;

  int i;
  //Free Mil frame buffers.
  for (i = 0; i < mFrames.size(); i++)
  {
    MbufFree(MilImage[i]);
    checkError();
  }

  /* Release defaults. */
  MappFreeDefault(MilApplication, MilSystem, MilDisplay, MilDigitizer , M_NULL);
  
  return true;
};

void
ImageAcquireDeviceMatroxMeteor2::initBuffers(int frameWidth, int frameHeight, int numPrevBuffers)
{
  mFrames.setCapacity(numPrevBuffers+1);

  for (int i = 0; i < mFrames.capacity(); i++)
  {
    MediaFrameInfoPtr pInfo(new MediaFrameInfo);
    pInfo->pRGBImage.reset(new Image32());
    unsigned char* frmPtr = (unsigned char *)MbufInquire(MilImage[i],M_HOST_ADDRESS,M_NULL);
    pInfo->pRGBImage->setAll(0);
    pInfo->pRGBImage->mx_fromarray((unsigned int *)frmPtr,frameHeight,frameWidth);
    pInfo->pRGBImage->setInternalFormat(Image32::PV_BGRA);
    mFrames.push_back(pInfo);
  }
}

int ImageAcquireDeviceMatroxMeteor2::grabFrame()
{
  assert(isInitialized);
  
  //ring(ring.predictNextStorePos()).setAll(0xFFFFFFFF);
  
  //Asynchronosously grab into last buffer, so its ready when the next grab occurs.
  //MdigGrab(MilDigitizer, MilImage[ring.predictNextStorePos()]);
  
  nextFrame();

  mMilImageIndex = (mMilImageIndex + 1)%mFrames.size();
  
  MdigGrab(MilDigitizer, MilImage[mMilImageIndex]);
  
  return 0;
  
};

int ImageAcquireDeviceMatroxMeteor2::MilInitialization()
{
  //Allocate Mil Application level.
  MappAlloc(M_DEFAULT,&MilApplication);
  
  //sys
  //MsysAlloc(M_SYSTEM_SETUP,M_DEFAULT,M_NO_DDRAW,&MilSystem);
  MsysAlloc(M_SYSTEM_SETUP,M_DEFAULT,M_DEFAULT,&MilSystem);
  
  //Grabber
  MdigAlloc(MilSystem,M_DEFAULT,"M_NTSC",M_DEFAULT,&MilDigitizer);
  
  //Mil's display.
  MdispAlloc(MilSystem,M_DEFAULT,M_DEF_DISPLAY_FORMAT,M_DEFAULT,&MilDisplay);
  
  //long frameBufAttr=M_IMAGE+M_GRAB+M_PROC+M_BGR32+M_PACKED+M_ON_BOARD*0;
  long frameBufAttr=M_IMAGE+M_GRAB+M_PROC+M_BGR32+M_PACKED;
  
  int i;
  //Create Mil Buffers.
  for(i=0;i<numPreviousBuffers+1;i++)
  {
    MilImage[i];
    MbufAllocColor(MilSystem,  
      MdigInquire(MilDigitizer, M_SIZE_BAND, M_NULL),
      (long)(MdigInquire(MilDigitizer, M_SIZE_X, M_NULL)*0.5),
      (long)(MdigInquire(MilDigitizer, M_SIZE_Y, M_NULL)*0.5),
      8L+M_UNSIGNED, 
      frameBufAttr, &MilImage[i]);
  }

  //Settings
  /* Set gain factor. */
  MdigControl(MilDigitizer, M_GRAB_AUTOMATIC_INPUT_GAIN, M_DISABLE);
  
  /* Set scale. */
  MdigControl(MilDigitizer, M_GRAB_SCALE_X, 0.5);
  MdigControl(MilDigitizer, M_GRAB_SCALE_Y, 0.5);

  //Put the digitizer synchronous mode
  MdigControl(MilDigitizer,M_GRAB_MODE, M_ASYNCHRONOUS);
  
  return 0;
  
};

/*********************************************************************
 * checkError()
 * 
 * Check if an error occurred. If so, report it.
 *
 * <input> 
 * 
 * void
 *
 * <output>
 *
 * void
 *
 *********************************************************************/
void ImageAcquireDeviceMatroxMeteor2::checkError(void)
{
  char msg[M_ERROR_MESSAGE_SIZE];

  while(MappGetError(M_GLOBAL+M_MESSAGE,msg)!=M_NULL_ERROR)
  {
   // PvUtil::error("******************************************\nMilError: %s\n******************************************",msg);
  }
}

bool ImageAcquireDeviceMatroxMeteor2::isFirstFrame(){
  
  //This is a do nothing function for backwards compatability.
  return false;
  
};

void ImageAcquireDeviceMatroxMeteor2::gotoFirstFrame(){
  
  //This is a do nothing function for backwards compatability.
  
};

void ImageAcquireDeviceMatroxMeteor2::setMILProperty(ImageAcquireProperty& param, bool set)
{
  long refType = -1;
  long controlType = -1;

  switch (param.getCode())
  {
  case ImageAcquireProperty::BRIGHTNESS:
    refType = M_BRIGHTNESS_REF;
    break;
  case ImageAcquireProperty::CONTRAST:
    refType = M_CONTRAST_REF;
    break;
  case ImageAcquireProperty::HUE:
    refType = M_HUE_REF;
    break;
  case ImageAcquireProperty::SATURATION:
    refType = M_SATURATION_REF;
    break;
  case ImageAcquireProperty::GAIN:
    controlType = M_GRAB_INPUT_GAIN;
    break;
  case ImageAcquireProperty::FPS:
    if (!set)
    {
      param.setValuef(30,false);
      param.setAvailable(true);
      param.setDefault(30);
      param.setAuto(false);
      param.setInteractive(false);
    }
    return;
  case ImageAcquireProperty::WHITEBALANCE:
  case ImageAcquireProperty::BACKLIGHTCOMPENSATION:
  case ImageAcquireProperty::SHARPNESS:
  case ImageAcquireProperty::GAMMA:
  case ImageAcquireProperty::COLORENABLE:
  case ImageAcquireProperty::PAN:
  case ImageAcquireProperty::TILT:
  case ImageAcquireProperty::ROLL:
  case ImageAcquireProperty::ZOOM:
  case ImageAcquireProperty::EXPOSURE:
  case ImageAcquireProperty::IRIS:
  case ImageAcquireProperty::FOCUS:
    break;
  case ImageAcquireProperty::SOURCEWIDTH:
  case ImageAcquireProperty::SOURCEHEIGHT:
    if (!set)
    {
      param.setAvailable(true);
      param.setAuto(false);
      param.setInteractive(false);
      if (param.getCode() == ImageAcquireProperty::SOURCEWIDTH)
      {
        param.setValuef(frameWidth,false);
        param.setDefault(frameWidth);
      }
      else
      {
        param.setValuef(frameHeight,false);
        param.setDefault(frameHeight);
      }
    }
    return;
  }

  if (refType > 0)
  {
    if (set)
    {
      MdigReference(MilDigitizer, refType, (long)(param.getValuef(false)));
    }
    else
    {
      long value;
      MdigInquire(MilDigitizer, refType, &value);
      param.setValuef((float)value,false);
      param.setRange(0,255,1);
      param.setAvailable(true);
      param.setDefault(128);
      param.setAuto(false);
      param.setInteractive(true);
    }
  }
  else if (controlType > 0)
  {
    if (set)
    {
      MdigControl(MilDigitizer, controlType, (long)(param.getValuef(false)));
    }
    else
    {
      long value;
      MdigInquire(MilDigitizer, controlType, &value);
      param.setValuef((float)value,false);
      param.setRange(0,255,1);
      param.setAvailable(true);
      param.setDefault(0.5f);
      param.setAuto(false);
      param.setInteractive(true);
    }
  }
  else
  {
    param.setAvailable(false);
  }
}

} // namespace ait

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifdef USE_POINTGREY

#pragma warning(disable: 4786)
#pragma comment(lib, "digiclops.lib")
#pragma comment(lib, "BoostThread.lib")

// SYSTEM INCLUDES
//
#include <boost/thread/thread.hpp>

// AIT INCLUDES
//

// LOCAL INCLUDES
//

#include "ImageAcquireDevicePointGrey.hpp"

using namespace std;
using namespace boost;

namespace ait
{
  using namespace boost::detail::thread;

  ///////////////////////////////////////////////////////////////////////
  //
  //  Encapsulated classes
  //
  ///////////////////////////////////////////////////////////////////////
  
  struct ImageAcquireDevicePointGrey::CameraContext
  {
    typedef mutex::scoped_lock scoped_lock;

    //
    // LIFECYCLE
    //
    
    // Constructor
    CameraContext() : mbInitialized(false)
    {
      mpRingBuffers[0] = mpRingBuffers[1] = NULL;

      scoped_lock lk(mDigiclopsMutex);

      digiclopsCreateContext(&mContext);
    }
  
    // Destructor
    ~CameraContext()
    {
      scoped_lock lk(mDigiclopsMutex);

      digiclopsDestroyContext(mContext);
    }
    
    // Default copy constructor is OK
    
    //
    // OPERATORS
    //
    // Default assignment operator is OK
    
    //
    // OPERATIONS
    //

    /*
    Dump state.
    */
    string output()
    {
      int nVersion = digiclopsGetLibraryVersion();
      return aitSprintf("%d",nVersion/100) + "." + aitSprintf("%d",nVersion % 100);
    }

    /**
    Grab an image from the Point Grey camera if one has not already been captured.
    Stores a pointer to the image for the camera for later use.
    */
    Bool grabFrame(Int nCameraDevice)
    {
      scoped_lock lk(mDigiclopsMutex);
      
      Int lr = nCameraDevice & 1;
      if (mlUnusedImages[lr].get() == NULL)
      {
        shared_ptr<Image32> pImgLeft(new Image32(mnActualWidth, mnActualHeight));
        pImgLeft->setInternalFormat(Image32::PV_BGRA);
        shared_ptr<Image32> pImgRight(new Image32(mnActualWidth, mnActualHeight));
        pImgRight->setInternalFormat(Image32::PV_BGRA);
        
        if (DIGICLOPS_ok != digiclopsGrabImage(mContext))
        {
          PVMSG( "Failed to grab an image");
        }
        
        DigiclopsImage di;
        if (DIGICLOPS_ok != digiclopsSetImageBuffer(mContext, RIGHT_IMAGE, 
          reinterpret_cast<unsigned char*>(pImgRight->pointer())))
        {
          throw;
        }
        if (DIGICLOPS_ok != digiclopsSetImageBuffer(mContext, LEFT_IMAGE, 
          reinterpret_cast<unsigned char*>(pImgLeft->pointer())))
        {
          throw;
        }

        if (DIGICLOPS_ok != digiclopsExtractDigiclopsImage(mContext, RIGHT_IMAGE, &di))
        {
          throw;
        }
        if (mnActualHeight != mnDesiredHeight || mnActualWidth != mnDesiredWidth)
        {
          pImgRight->rescale(mnDesiredWidth, mnDesiredHeight);
        }
        
        if (DIGICLOPS_ok != digiclopsExtractDigiclopsImage(mContext, LEFT_IMAGE, &di))
        {
          PVMSG("Failed to extract image");
        }
        if (mnActualHeight != mnDesiredHeight || mnActualWidth != mnDesiredWidth)
        {
          pImgLeft->rescale(mnDesiredWidth, mnDesiredHeight);
        }
        // make a buffer for storing the image from the other camera
        // we store this as a raw pointer, not a shared pointer, since it must
        // eventually be given to mx_fromarray below. The class destructor takes
        // care of deleting any instances of it that are left over.
        mpRingBuffers[lr]->nextSlot() = (lr == 0) ? *pImgLeft : *pImgRight;
        if (mpRingBuffers[!lr] != NULL) 
        {
          mlUnusedImages[!lr] = (lr == 1) ? pImgLeft : pImgRight;
        }
      }
      else 
      {
        mpRingBuffers[lr]->nextSlot() = *mlUnusedImages[lr];
        mlUnusedImages[lr].reset();
      }
      

      mpRingBuffers[lr]->advance();
      return true;
    }
    
    /**
    Opens the Point Grey camera.
    @param index [in] index of the camera
    @return success (camera exists, etc.)
    */
    ait::Bool init(int index, 
      int nFrameWidth, 
      int nFrameHeight, 
      int nFrameBuffers, 
      RingBuffer* pRingBuffer)
    {
      scoped_lock lk(mDigiclopsMutex);

      mpRingBuffers[index & 1] = pRingBuffer;

      mnDesiredWidth = nFrameWidth;
      mnDesiredHeight = nFrameHeight;

      if (!mbInitialized) 
      {
        if (DIGICLOPS_ok != digiclopsInitialize(mContext, index/2))
        {
          return false;
        }
        // Set up to capture the left and right images
        if (DIGICLOPS_ok != digiclopsSetImageTypes(mContext, LEFT_IMAGE | RIGHT_IMAGE))
        {
          return false;
        }
        
        DigiclopsInfo info;
        if (DIGICLOPS_ok != digiclopsGetCameraInformation(mContext, &info))
        {
          return false;
        }
        // translate enumerated type labels into useful numbers
        Int nCcdWidth = 0;
        Int nCcdHeight = 0;
        switch(info.ImageSize)
        {
        case DIGICLOPS_160x120:
          mnActualWidth = 160;
          mnActualHeight = 120;
          break;
        case DIGICLOPS_320x240:
          mnActualWidth = 320;
          mnActualHeight = 240;
          break;
        case DIGICLOPS_640x480:
          mnActualWidth = 640;
          mnActualHeight = 480;
          break;
        case DIGICLOPS_1024x768:
          mnActualWidth = 1024;
          mnActualHeight = 768;
          break;
        }

        if (DIGICLOPS_ok != digiclopsSetImageResolution(mContext, DIGICLOPS_FULL)) 
        {
          return false;
        }
        // Start the camera
        if (DIGICLOPS_ok == digiclopsStart(mContext)) 
        {
          mbInitialized = true;
        }

      }
      
      return mbInitialized;
    }    
    /**
    Closes the Point Grey camera
    */
    void uninit()
    {
      scoped_lock lk(mDigiclopsMutex);
      if (mbInitialized)
      {
        digiclopsStop(mContext);
        mbInitialized = false;
      }
    }
    
    //
    // CameraProperty
    //
    
    class ImageAcquireDevicePointGrey::CameraProperty : public ImageAcquireProperty
    {
    public:
      
      CameraProperty(ImageAcquireDevicePointGrey *pDevice) : mpDevice(pDevice) {};
      virtual ~CameraProperty() {}
      
      virtual ImageAcquireProperty *clone() { return new CameraProperty(*this); }
      
      void setToDevice() 
      { 
        mpDevice->setProperty(*this,true); 
      };
      void getFromDevice() 
      { 
        mpDevice->setProperty(*this,false); 
      };
      
    protected:
      
      ImageAcquireDevicePointGrey *mpDevice;
    };

    /**
    Sets a image capture property (brightness, color balance, etc.)
    
      @param CameraProperty& [in/out] the property to set
      @param bool [in] true == set the property false == update property with current value
    */
    void setProperty(CameraProperty& prop, Bool bSet)
    {
      if (prop.getCode() == ImageAcquireProperty::FPS)
      {
        if (bSet)
        {
          DigiclopsFrameRate frame;
          float v = prop.getValuef(false);
          if (v > 75.0)
          {
            frame = DIGICLOPS_FRAMERATE_100;
          }
          else if (v > 37.5)
          {
            frame = DIGICLOPS_FRAMERATE_050;
          } 
          else if (v > 18.75)
          { 
            frame = DIGICLOPS_FRAMERATE_025;
          }
          else
          {
            frame = DIGICLOPS_FRAMERATE_012;
          }
          if (DIGICLOPS_ok != digiclopsStop(mContext))
          {
            throw;
          }
          if (DIGICLOPS_ok != digiclopsSetFrameRate(mContext, frame))
          {
            throw;
          }
          if (DIGICLOPS_ok != digiclopsStart(mContext))
          {
            throw;
          }
        }
        else
        {
          DigiclopsFrameRate frame;
          if (DIGICLOPS_ok != digiclopsGetFrameRate(mContext, &frame))
          {
            throw;
          }
          
          switch (frame)
          {
          case DIGICLOPS_FRAMERATE_100:
            prop.setValuef(100.0, false);
            break;
          case DIGICLOPS_FRAMERATE_050:
            prop.setValuef(50.0, false);
            break;
          case DIGICLOPS_FRAMERATE_025:
            prop.setValuef(25.0, false);
            break;
          case DIGICLOPS_FRAMERATE_012:
            prop.setValuef(12.5, false);
            break;
          }
          prop.setAvailable(true);
          prop.setRange(0,100,1);
          prop.setDefault(100);
          prop.setAuto(false);
          prop.setInteractive(false);
        }
      }
      else
      {
        DigiclopsCameraProperty digiProp = DIGICLOPS_PROPERTY_TOTAL;
        
        switch (prop.getCode())
        {
          // brightness corresponds more or less to shutter speed, I guess...
        case ImageAcquireProperty::BRIGHTNESS:
          digiProp = DIGICLOPS_SHUTTER;
          break;
          
        case ImageAcquireProperty::GAIN:
          digiProp = DIGICLOPS_GAIN;
          break;

        // this is a pain to deal with because whitebalance is really 2
        // properties
        case ImageAcquireProperty::WHITEBALANCE:
          digiProp = DIGICLOPS_WHITEBALANCE;
          break;
          
          // unavailable properties
        case ImageAcquireProperty::HUE:
        case ImageAcquireProperty::SATURATION:
        case ImageAcquireProperty::CONTRAST:
        case ImageAcquireProperty::BACKLIGHTCOMPENSATION:
        case ImageAcquireProperty::SHARPNESS:
        case ImageAcquireProperty::GAMMA:
        case ImageAcquireProperty::COLORENABLE:
        case ImageAcquireProperty::PAN:
        case ImageAcquireProperty::TILT:
        case ImageAcquireProperty::ROLL:
        case ImageAcquireProperty::ZOOM:
        case ImageAcquireProperty::EXPOSURE:
        case ImageAcquireProperty::IRIS:
        case ImageAcquireProperty::FOCUS:
          break;
        }
        
        if (digiProp != DIGICLOPS_PROPERTY_TOTAL)
        {
          if (bSet)
          {
            long lValueA = long(prop.getValuef(false));
            long lValueB = 0;
            if (digiProp == DIGICLOPS_WHITEBALANCE)
            {
              lValueB = lValueA % 16;
              lValueA /= 16;
            }
            if (DIGICLOPS_ok != digiclopsSetCameraProperty(mContext,
              digiProp,
              lValueA,
              lValueB,
              false))
            {
              throw;
            }
          }
          else
          {
            bool bPresent = false;
            long lMin = 0;
            long lMax = 0;
            long lDefault = 0;
            bool bAuto = false;
            bool bManual = false;
            if (DIGICLOPS_ok != digiclopsGetCameraPropertyRange(mContext,
              digiProp,
              &bPresent,
              &lMin,
              &lMax,
              &lDefault,
              &bAuto,
              &bManual))
            {
              throw;
            }
            long lValueA = 0;
            long lValueB = 0;
            bool bAutoVal = false;
            if (DIGICLOPS_ok != digiclopsGetCameraProperty(mContext,
              digiProp,
              &lValueA,
              &lValueB,
              &bAutoVal))
            {
              throw;
            }
            if (digiProp != DIGICLOPS_WHITEBALANCE) 
            {
              prop.setValuef(float(lValueA),false);
            }
            else
            {
              prop.setValuef(float(lValueA * 16 + lValueB),false);
            }
            prop.setRange(float(lMin), float(lMax), 1);
            prop.setAvailable(bPresent);
            prop.setDefault(float(lDefault));
            prop.setAuto(bAuto);
            //prop.setInteractive(bManual);
            prop.setInteractive(true);
          }
        }
        else
        {
          prop.setAvailable(false);
        }
      }
  }


  //
  // ATTRIBUTES
  //
protected:
  /**
  Whether this class has been initialized.
  */
  Bool mbInitialized;
  /**
  CameraContext from Digiclops library
  */
  DigiclopsContext mContext;
  /**
  Actual height and width of the images being captured
  */
  Int mnActualHeight, mnActualWidth;
  /**
  Desired height and width of the images 
  */
  Int mnDesiredHeight, mnDesiredWidth;
  /**
  Pointers to the two ring buffers for the left and right images
  */
  RingBuffer* mpRingBuffers[2];
  /**
  Pointers to images we have captured, but not put in the ring buffers yet.
  */
  shared_ptr<Image32> mlUnusedImages[2];
  /**  
  For synchronizing between the two instances of ImageAcquireDevicePointGrey
  that access the same physical device.
  */
  static mutex mDigiclopsMutex;

  }; // end of class CameraContext
  
  mutex ImageAcquireDevicePointGrey::CameraContext::mDigiclopsMutex;

  //////////////////////////////////////////////////////////////////////////////////
  //
  // End of encapsulated classes
  //
  ////////////////////////////////////////////////////////////////////////////////
  

  ////////////////////////////////////////////////////////////////////////////////
  //
  // ImageAcquireDevicePointGrey 
  //
  ////////////////////////////////////////////////////////////////////////////////

  //
  // STATIC VARIABLES
  //
  vector<ImageAcquireDevicePointGrey::CameraContextPtr> ImageAcquireDevicePointGrey::mvContext;
  
  //
  // LIFECYCLE
  //
  ImageAcquireDevicePointGrey::ImageAcquireDevicePointGrey()
  {
  }
  
  ImageAcquireDevicePointGrey::~ImageAcquireDevicePointGrey()
  {
    unInitialize();
  }
  
  //
  // OPERATIONS
  //
  bool ImageAcquireDevicePointGrey::initialize(int numPrevBuffers, int numConsecutiveFrameGrabs)
  {
    if (isInitialized) 
    {
      unInitialize();
    }
    
    numPreviousBuffers = numPrevBuffers;
    
    // Create the property bag...
    pParams = new ImageAcquirePropertyBag(deviceInfo->getRegPath(),new CameraProperty(this));

    frameWidth = 320;
    frameHeight = 240;
    
    // Initialize the right Point Grey device
    int nCameraIndex = deviceInfo->getdeviceIndex();
    mnPointGreyDevice = nCameraIndex / 2; // each Point Grey device has 2 cameras
    mnCameraIndex = nCameraIndex & 1;
    while (mvContext.size() <= mnPointGreyDevice)
    {
      mvContext.resize(mvContext.size()+1);
      mvContext[mvContext.size()-1].reset(new CameraContext);
    }
    if (!mvContext[mnPointGreyDevice]->init(nCameraIndex,
      frameWidth, 
      frameHeight, 
      numPreviousBuffers+1,
      &ring))
    {
      return false;
    }
    
    //init frame ct.
    frameCounter = 0;
    
    // Initialize and fill buffers.
    ring.init(frameWidth,frameHeight,numPreviousBuffers + 1);
    Int i=0;
    for (; i<=numPreviousBuffers; i++) 
    {
      ring(i).setInternalFormat(Image32::PV_BGRA);
      ring(i).setAll(0xFFFFFFFF);
      // failure indicated by returning non-zero value, go figure
      if (grabFrame())
      {
        break;
      }
    }
    
    if (i == ring.bufferLen) 
    {
      //Everything worked.
      isInitialized = true;
    }
    return isInitialized;
  }
  
  bool ImageAcquireDevicePointGrey::unInitialize()
  {
    mvContext[mnPointGreyDevice]->uninit();
    isInitialized = false;
    pParams->saveSettings();
    return true;
  }
  
  
  int ImageAcquireDevicePointGrey::grabFrame()
  {
    return mvContext[mnPointGreyDevice]->grabFrame(mnCameraIndex) ? 0 : 4;
  };
  
  void ImageAcquireDevicePointGrey::setProperty(CameraProperty& prop, Bool bSet)
  {
    mvContext[mnPointGreyDevice]->setProperty(prop, bSet);
  }
  
}; // namespace ait

#endif // USE_POINTGREY
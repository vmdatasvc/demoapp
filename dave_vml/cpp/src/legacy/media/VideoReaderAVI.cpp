/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include "VideoReaderAVI.hpp"
#include <PvUtil.hpp>

// constructor
VideoReaderAVI::VideoReaderAVI()
: mIsLooping(false)
{
};

// destructor
VideoReaderAVI::~VideoReaderAVI()
{
	// call to make sure that resources are freed
	finish();
};



bool VideoReaderAVI::init(unsigned int &w,unsigned  int &h, const char *name)
{
  mpVideoReader.reset(ait::VideoReader::create());

  try
  {
    mpVideoReader->open(name);
    const ait::Vector2i size = mpVideoReader->getBufferSize();
    w = size.x;
    h = size.y;
  }
  catch (std::exception e)
  {
    PvUtil::exitError("Error reading file: %s",name);
  }
  return true;
}


bool VideoReaderAVI::readFrame(ait::Image32& frame, bool BGRMode)
{
  if (mpVideoReader->isEndOfFile())
  { 
    if (mIsLooping)
    {
      setCurrentFrame(0);
    }
    else
    {
      return false;
    }
  }

	frame.setInternalFormat(BGRMode ? ait::Image32::PV_BGRA : ait::Image32::PV_RGBA);
  mpVideoReader->readFrame(frame);

	return !mpVideoReader->isEndOfFile();
}

int VideoReaderAVI::getNumFrames() 
{
  return mpVideoReader->getNumberOfFrames();
}

void VideoReaderAVI::finish(void)
{
  mpVideoReader.reset();
}

void VideoReaderAVI::setCurrentFrame(long fNum)
{
  mpVideoReader->setCurrentFrameNumber(fNum);
}

void VideoReaderAVI::setLoopMode(bool flag) 
{ 
  mIsLooping=flag; 
}

bool VideoReaderAVI::isFirstFrame(void) 
{ 
  return  (getCurrentFrameNumber()==0); 
}

float VideoReaderAVI::getFrameRate() 
{ 
  return (float)mpVideoReader->getFramesPerSecond(); 
}

int VideoReaderAVI::getCurrentFrameNumber(void) 
{ 
  return (int)mpVideoReader->getCurrentFrameNumber(); 
}



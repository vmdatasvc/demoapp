


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
#ifndef CameraDriverCisco_HPP
#define CameraDriverCisco_HPP
// SYSTEM INCLUDES
//
#include <boost/utility.hpp>
#include <boost/thread.hpp>
#include <map>

// AIT INCLUDES
//

// LOCAL INCLUDES
//
#include <CameraDriver.hpp>
#include <ICameraDriverEvents.hpp>
#include <HttpSimpleClient.hpp>

// FORWARD REFERENCES
//

#include "CameraDriverCisco_fwd.hpp"



namespace ait 
{


class CiscoDecoderException : public std::exception
{
  public:
    CiscoDecoderException() : mWhat("Cisco Decoder Exception") {}
    CiscoDecoderException(const std::string msg) : mWhat(msg) {}
  private:
    std::string mWhat;
    virtual const char* what() { return mWhat.c_str(); }
};

/**
 * Bridge implementation for Axis camera.
 */
class CameraDriverCisco : public CameraDriver
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  CameraDriverCisco(const std::string& cameraProtocol);

  /**
   * Destructor
   */
  virtual ~CameraDriverCisco();


  //
  // OPERATIONS
  //

public:

  /**
   * Start the main loop.
   */
  virtual void init(int numCameras);

  /**
   * Stop the main loop.
   */
  virtual void finish();

  /**
   * Set a specific parameter. See base class.
   */
  virtual void setParam(int cameraId, const std::string& name, const std::string& value)
  {
    mParams[name] = value;
  }

  /**
   * Get a specific parameter. See base class.
   */
  virtual const std::string getParam(int cameraId, const std::string& name)
  {
    if (mParams.find(name) == mParams.end()) return "";
    return mParams[name];
  }

  virtual const std::string getParam(const std::string& name)
  {
    return getParam(-1,name);
  }

  /**
   * Register the event handler to receive frames.
   */
  virtual void registerEventHandler(ICameraDriverEvents *pEvents);
  
public:
  /**
   * This loop will read frames and call the callback on each new frame found.
   */
  void threadLoop();

    /**
   * The thread class for boost has to be copyable, so this will
   * be the container.
   */

  class ThreadHolder
  {
  public:
    ThreadHolder(CameraDriverCisco& t) : mThread(t) {}
    CameraDriverCisco& mThread;
    void operator()() { mThread.threadLoop(); }
  };

  ThreadHolder *mpThreadHolder;

  void operator()()
  {
    threadLoop();
  }

  /**
   * Start the connection, send the http request. Will throw on connection errors.
   */
  void initConnection();

protected:

	double getFramesPerSecond();

  //
  // ATTRIBUTES
  //

protected:

ICameraDriverEvents *mpEvents;

double mFrameTime;
double mTimeBetweenFrames;

  char mExitThread;
  //char eventLoopWatchVariable;

  std::map<std::string,std::string> mParams;

  boost::thread *mpCaptureThread;
  VideoUsageEnvironment* env;
  friend class VideoUsageEnvironment;
};


}; // namespace ait

#endif // CameraDriverCisco_HPP




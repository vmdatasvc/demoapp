/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef CameraDriverCisco_fwd_HPP
#define CameraDriverCisco_fwd_HPP

#include <boost/shared_ptr.hpp>
// Forward declare - resolved in cpp by includes

namespace ait
{
class VideoUsageEnvironment;

class CameraDriverCisco;
class CiscoDecoderException;
typedef boost::shared_ptr<CameraDriverCisco> CameraDriverCiscoPtr;

}; // namespace ait

#endif // CameraDriverCisco_fwd_HPP


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#pragma warning(disable:4786)

#include "legacy/media/ImageAcquireDeviceOpenCV.hpp"
#include <assert.h>
#include <stack>
#include <legacy/types/DateTime.hpp>

namespace ait
{

class OpenCVProperty : public ImageAcquireProperty
{
public:

  OpenCVProperty(ImageAcquireDeviceOpenCV *pDevice0) : pDevice(pDevice0) {};
  virtual ~OpenCVProperty() {}

  virtual ImageAcquireProperty *clone() { return new OpenCVProperty(*this); }

  void setToDevice() { pDevice->setOpenCVProperty(*this,true); };
  void getFromDevice() { pDevice->setOpenCVProperty(*this,false); };

protected:

  ImageAcquireDeviceOpenCV *pDevice;
};

ImageAcquireDeviceOpenCV::ImageAcquireDeviceOpenCV()
{
}


ImageAcquireDeviceOpenCV::~ImageAcquireDeviceOpenCV()
{
	unInitialize();
};

//void ImageAcquireDeviceOpenCV::setVideoProperty(Settings &set, std::string source)
//{
//	mSettings = set;
//	mSettings.changePath("ImageAcquireDeviceOpenCV");
//
//	mVideoFileName = source;
//}

bool ImageAcquireDeviceOpenCV::initialize(int numPrevBuffers, int numConsecutiveFrameGrabs)
{
	assert(numPrevBuffers >= 0);

  if (isInitialized)
	unInitialize();

	//IR-Flashing
  if (numConsecutiveFrameGrabs > 1)
  {
	  return false; 	// currently not supporting multi frame grabs
  }
  mNumConsecutiveFrameGrabs = numConsecutiveFrameGrabs;

  lastPaintTime = PvUtil::time();
  paintDelay = 0; //delay paints in secs.

	//Initialize the display window.
	displayFrameFlag = false;

	//Initialize base parameters.
	numPreviousBuffers = numPrevBuffers;

	//Initialize the resolution.
	frameWidth = 0;
	frameHeight = 0;

  // Add default property bag
  pParams = new ImageAcquirePropertyBag("ImageAcquireDeviceOpenCV",
  ImageAcquirePropertyPtr(new OpenCVProperty(this)));

  Settings set;
  set.changePath("ImageAcquireDeviceOpenCV");
  mRunAtFrameRate = set.getBool("runAtFrameRate [bool]",true);
  mSkipFrameInterval = set.getInt("skipFrameInterval",1);

//  mFps = pParams->get(ImageAcquireProperty::FPS)->getValuef();
//	//IR-Flashing
//	mFps/=mNumConsecutiveFrameGrabs;

  mTotalFrames = 0;
  int numCustomStartTime = 0;

//  VideoFiles::iterator iVideo;
//
//  for (iVideo = mVideoFiles.begin(); iVideo != mVideoFiles.end(); ++iVideo)
//  {
//    VideoFile& vf = **iVideo;
//    if (vf.getStartTime() != 0)
//    {
//      numCustomStartTime++;
//    }
//  }
//
//  if (numCustomStartTime > 0 && mVideoFiles.size() != numCustomStartTime)
//  {
//    PvUtil::exitError("Custom start time must be defined for all videos");
//  }
//
//  for (iVideo = mVideoFiles.begin(); iVideo != mVideoFiles.end();)
//  {
//    VideoFile& vf = **iVideo;
//    mpReader.reset(VideoReader::create());
//    try
//    {
//      mpReader->open(vf.getFilename());
//    }
//    catch (std::exception)
//    {
//      iVideo = mVideoFiles.erase(iVideo);
//      mpReader.reset();
//      continue;
//    }
//
//    Vector2i bufferSize = mpReader->getBufferSize();
//    if (frameWidth == 0)
//    {
//      frameWidth = bufferSize(0);
//      frameHeight = bufferSize(1);
//    }
//    else
//    {
//      if (frameWidth != bufferSize(0) ||
//        frameHeight != bufferSize(1))
//      {
//        PvUtil::exitError("All videos in [%s] must have the same frame size",fileList.c_str());
//      }
//    }
//
//    if (vf.getClipCount() == 0)
//    {
//      VideoClipPtr pClip(new VideoClip());
//      vf.addClip(pClip);
//    }
//
//    int i;
//    for (i = 0; i < vf.getClipCount(); i++)
//    {
//      VideoClip& clip = *vf.getClip(i);
//      if (clip.getStartFrame() > clip.getEndFrame() && clip.getEndFrame() > 0)
//      {
//        PvUtil::exitError("Start frame is greater than end frame for a clip in file [%s]",fileList.c_str());
//      }
//      if (clip.getEndFrame() == 0 || clip.getEndFrame() > mpReader->getNumberOfFrames())
//      {
//        clip.setEndFrame(mpReader->getNumberOfFrames());
//      }
//      if (clip.getStartFrame() < clip.getEndFrame())
//      {
//        mTotalFrames += clip.getEndFrame()-clip.getStartFrame();
//      }
//    }
//
//    ++iVideo;
//  }
//
//  if (mVideoFiles.size() == 0)
//  {
//    frameWidth = pParams->get(ImageAcquireProperty::SOURCEWIDTH)->getValuef();
//    frameHeight = pParams->get(ImageAcquireProperty::SOURCEHEIGHT)->getValuef();
//  }
//  else
//  {
//    // Load the first video
//    mpReader.reset(VideoReader::create());
//    mpReader->open(mVideoFiles[0]->getFilename());
//  }
//
//  mCurrentVideoFile = 0;
//  mCurrentVideoClip = 0;
//  mCurrentFrameInClip = -1;

  // open url
  mVideoFileName = set.getString("Source","filename.avi");
  mpCapture.reset(new cv::VideoCapture);
  if (!mpCapture->open(mVideoFileName))
  {
	  // error opening
	  return false;
  }

  frameWidth = (int)mpCapture->get(CV_CAP_PROP_FRAME_WIDTH);
  frameHeight = (int)mpCapture->get(CV_CAP_PROP_FRAME_HEIGHT);
  mFps = (float)mpCapture->get(CV_CAP_PROP_FPS);
  mTotalFrames = (int)mpCapture->get(CV_CAP_PROP_FRAME_COUNT);

	//Initialize buffers.

  initBuffers(frameWidth,frameHeight,numPrevBuffers);	// sets mFrames here
  for (int i = 0; i < mFrames.capacity(); i++)
  {
    mFrames[i]->pRGBImage->setInternalFormat(Image32::PV_BGRA);
  }

	frameCounter = 0;

	isInitialized = true;

	//Fill buffers.
  nextFrameTime=0;

  fillBuffers();

	return true;
};

//void ImageAcquireDeviceAVI::loadVideoFiles(std::string fname)
//{
//  ConfigHandler config;
//  XmlParser parser;
//
//  parser.registerNamespaceHandler("", &config);
//  parser.parseFile(fname);
//
//  mVideoFiles = config.getVideoFiles();
//}

bool ImageAcquireDeviceOpenCV::unInitialize()
{
	isInitialized = false;

  mFrames.clear();
//  if (pParams)
//  {
//    delete pParams;
//    pParams = NULL;
//  }

//  mpReader.reset();
//  mVideoFiles.clear();

  mpCapture.reset();

	return true;
};

int ImageAcquireDeviceOpenCV::grabFrame()
{
  assert(isInitialized);
  
  bool isLastFrame = false;

//  if (mVideoFiles.size() == 0)
//  {
//    return true;
//  }

  //Grab into the last position.
  //Get avi here....

//  VideoFilePtr pVf;// = mVideoFiles[mCurrentVideoFile];
//  VideoClipPtr pVc;// = pVf->getClip(mCurrentVideoClip);

//	for(int i=0; i<mNumConsecutiveFrameGrabs; i++)
//	{
//		pVf = mVideoFiles[mCurrentVideoFile];
//		pVc = pVf->getClip(mCurrentVideoClip);
//
//		if (mCurrentFrameInClip < 0)
//		{
//			mCurrentFrameInClip = pVc->getStartFrame();
//		}
//		else
//		{
//			//ensure that "mNumConsecutiveFrameGrabs" consecutive frames are loaded
//			if(i==0)
//				mCurrentFrameInClip += mSkipFrameInterval;
//			else
//				mCurrentFrameInClip++;
//		}
//
//		while (pVc->getEndFrame() != 0 && mCurrentFrameInClip >= pVc->getEndFrame())
//		{
//			// Move to the next clip.
//			if (mCurrentVideoClip < pVf->getClipCount()-1)
//			{
//				mCurrentVideoClip++;
//			}
//			else
//			{
//				// The current file will change.
//				if (mCurrentVideoFile < mVideoFiles.size()-1)
//				{
//					mCurrentVideoFile++;
//				}
//				else
//				{
//					// We are past the end, must loop to the first file.
//					mCurrentVideoFile = 0;
//				}
//				mCurrentVideoClip = 0;
//
//				pVf = mVideoFiles[mCurrentVideoFile];
//
//				// Load new file.
//				if (mVideoFiles.size() > 1)
//				{
//					mpReader.reset(VideoReader::create());
//					mpReader->open(pVf->getFilename());
//				}
//
//			}
//
//			// At this point the video clip has changed.
//			pVc = pVf->getClip(mCurrentVideoClip);
//			mCurrentFrameInClip = pVc->getStartFrame();
//		}
//
//		mpReader->setCurrentFrameNumber(mCurrentFrameInClip);
//		//mpReader->readFrame(*(mFrames.front()->pRGBImage));
//    MediaFrameInfoPtr pFrame = mFrames[mFrames.size()-(mNumConsecutiveFrameGrabs-i)];
//		mpReader->readFrame(*(pFrame->pRGBImage));
//    if (pVf->getStartTime() > 0)
//    {
//      double fps = mpReader->getFramesPerSecond();
//      pFrame->beginTime = pVf->getStartTime() + ((double)mCurrentFrameInClip)/fps;
//      pFrame->endTime = pFrame->beginTime + 1.0/fps;
//    }
//    mpReader->nextFrame();
//
//		if (mCurrentVideoFile == mVideoFiles.size()-1 &&
//			mCurrentVideoClip == pVf->getClipCount()-1 &&
//      pVc->getEndFrame() > 0 &&
//			mCurrentFrameInClip + mSkipFrameInterval >= pVc->getEndFrame())
//		{
//			isLastFrame = true;
//		}
//  }

	nextFrame(); //automatically advanced "mNumConsecutiveFrameGrabs" frames

	// grab image
	Image32Ptr	pImg = mFrames.front()->pRGBImage;
	cv::Mat	cvimg(pImg->height(), pImg->width(), CV_8UC3);
	*mpCapture >> cvimg;

//	cv::imwrite("captured.tif",cvimg);

	if (cvimg.empty())
	{
		isLastFrame = true;
	}
	else {
		// convert cvimg to Image32
		cv::Mat pImgCVHeader(pImg->height(),pImg->width(),CV_8UC4,(void *)pImg->pointer());
		cv::cvtColor(cvimg,pImgCVHeader,CV_RGB2RGBA);

//		pImg->save("saved.png");
	}

	PVTIME now=PvUtil::time();

	if(now < nextFrameTime)
	{
		//PVMSG("Sleeping %g ms!\n",(0.033-(now-lastFrameTime))*1000.0);
		if (mRunAtFrameRate)
		{
			PvUtil::sleep(nextFrameTime-now);
		}
	}

	nextFrameTime += 1.0/mFps;

	// If we are too far, just reset the next frame time.
	if (now > nextFrameTime + 3.0/mFps)
	{
		nextFrameTime = now;
	}

  return isLastFrame;
};


bool ImageAcquireDeviceOpenCV::isFirstFrame(){

	return false;


};

void ImageAcquireDeviceOpenCV::gotoFirstFrame()
{
};

void ImageAcquireDeviceOpenCV::setOpenCVProperty(ImageAcquireProperty& param, bool set)
{
  long refType = -1;
  long controlType = -1;

  switch (param.getCode())
  {
  case ImageAcquireProperty::BRIGHTNESS:
  case ImageAcquireProperty::CONTRAST:
  case ImageAcquireProperty::HUE:
  case ImageAcquireProperty::SATURATION:
  case ImageAcquireProperty::GAIN:
  case ImageAcquireProperty::WHITEBALANCE:
  case ImageAcquireProperty::BACKLIGHTCOMPENSATION:
  case ImageAcquireProperty::SHARPNESS:
  case ImageAcquireProperty::GAMMA:
  case ImageAcquireProperty::COLORENABLE:
  case ImageAcquireProperty::PAN:
  case ImageAcquireProperty::TILT:
  case ImageAcquireProperty::ROLL:
  case ImageAcquireProperty::ZOOM:
  case ImageAcquireProperty::EXPOSURE:
  case ImageAcquireProperty::IRIS:
  case ImageAcquireProperty::FOCUS:
    break;
  case ImageAcquireProperty::SOURCEWIDTH:
  case ImageAcquireProperty::SOURCEHEIGHT:
    if (!set)
    {
      param.setAvailable(true);
      param.setAuto(false);
      param.setInteractive(false);
      if (param.getCode() == ImageAcquireProperty::SOURCEWIDTH)
      {
        param.setValuef(frameWidth,false);
        param.setDefault(frameWidth);
      }
      else
      {
        param.setValuef(frameHeight,false);
        param.setDefault(frameHeight);
      }
    }
    return;
  case ImageAcquireProperty::FPS:
    if (!set)
    {
      param.setAvailable(true);
      param.setAuto(false);
      param.setInteractive(false);
      param.setValuef(mFps,false);
      param.setDefault(30);
    }
    else
    {
      mFps = param.getValuef(false);
    }
    return;
  }

  param.setAvailable(false);
}

int
ImageAcquireDeviceOpenCV::getTotalNumberOfFrames()
{
  return mTotalFrames/mSkipFrameInterval;
}

} // namespace ait

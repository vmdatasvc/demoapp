/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include "ImageAcquireDeviceIp.hpp"
#include <CameraDriver.hpp>
#include <PvCanvasImage.hpp>

namespace ait
{

ImageAcquireDeviceIp::ImageAcquireDeviceIp()
{
}


ImageAcquireDeviceIp::~ImageAcquireDeviceIp()
{
	unInitialize();
};

void
ImageAcquireDeviceIp::tryInitDriver()
{
#ifdef _DEBUG
//=====================================================================================
	// Wait here for the debugger to attach to process

//if(strstr(modelName.c_str(),"VM Sense"))
//{
/*
while(!IsDebuggerPresent())
	{
		Sleep(500);
	}
*/
//}

//=====================================================================================
#endif
  try
  {
    mpCameraDriver.reset(CameraDriver::create(deviceInfo->getRegPath()));

    // Read settings from file
	Settings set(deviceInfo->getRegPath());
    std::string paramsFile = set.getString("paramsFile","");
    if (paramsFile != "" && PvUtil::fileExists(paramsFile.c_str()))
    {
      mpCameraDriver->loadParams(paramsFile);
    }

	mpCameraDriver->registerEventHandler(this);

    mpCameraDriver->init();
  }
  catch (std::exception ex)
  {
    setLastErrorMessage(ex.what());
    mpCameraDriver.reset();
    mNextTryStartTime = PvUtil::time() + 10; // Retry in 10 seconds.
  }
}

bool ImageAcquireDeviceIp::initialize(int numPrevBuffers, int numConsecutiveFrameGrabs)
{
	assert(numPrevBuffers >= 0);

	if (isInitialized)
		unInitialize();

  mNextTryStartTime = 0;
  mLastValidFrameTime = 0;

  tryInitDriver();

  paintDelay = 0; //delay paints in secs.

	//Initialize base parameters.
	numPreviousBuffers = numPrevBuffers;

  Settings set(deviceInfo->getRegPath());
  Vector2i windowSize(
    set.getInt("Source Width/value [float]",320),
    set.getInt("Source Height/value [float]",240));
  mFps = set.getDouble("Fps/value [float]",15);

  pParams = new ImageAcquirePropertyBag(deviceInfo->getRegPath());
  pParams->get(ImageAcquireProperty::SOURCEWIDTH)->setValuef(windowSize.x);
  pParams->get(ImageAcquireProperty::SOURCEHEIGHT)->setValuef(windowSize.y);
  pParams->get(ImageAcquireProperty::FPS)->setValuef(mFps);

  frameWidth=windowSize(0);
  frameHeight=windowSize(1);

  initBuffers(frameWidth,frameHeight,numPrevBuffers);

	frameCounter = 0;

	isInitialized = true;

  // Assume we just had a valid frame.
  mLastValidFrameTime = PvUtil::time();
  mLastFrameTime = mLastValidFrameTime;

  fillBuffers();

  // The first frame will now be ready for asynchronous
  // updates.
  {
    boost::mutex::scoped_lock lk(mFrameReadyMutex);  
    mpNextFrame = mFrames.back();
  }

	return true;
};

bool ImageAcquireDeviceIp::unInitialize()
{
	isInitialized = false;

  mpCameraDriver.reset();

  mFrames.clear();
  if (pParams)
  {
    delete pParams;
    pParams = NULL;
  }

  return true;
};

static void drawText(Image32& img0,
                     int x,
                     int y,
                     const std::string& txt,
                     bool transparent = false)
{
#ifdef WIN32
  HFONT hfnt, hOldFont;
  HDC hdc = CreateCompatibleDC(NULL);

  hfnt = (HFONT)GetStockObject(ANSI_FIXED_FONT); 
  if (hOldFont = (HFONT)SelectObject(hdc, hfnt)) 
  {
    SIZE size;
    GetTextExtentPoint(hdc,txt.c_str(),txt.length(),&size);
    SelectObject(hdc, hOldFont); 
    DeleteDC(hdc);
    
    Uint32 magicColor = PV_RGB(0,0,255);
    PvCanvasImage img(size.cx+2,size.cy+2);
    img.setAll(magicColor);
    hdc = img.dc();
    if (hOldFont = (HFONT)SelectObject(hdc, GetStockObject(ANSI_FIXED_FONT))) 
    {
      POINT pos = { 1 , 1 };
      SetBkMode(hdc,TRANSPARENT);
      if (transparent)
      {
        SetTextColor(hdc,RGB(0,0,0));
        for (int x0 = -1; x0 <= 1; x0++)
        {
          for (int y0 = -1; y0 <= 1; y0++)
          {
            TextOut(hdc, pos.x+x0, pos.y+y0, txt.c_str(), txt.length()); 
          }
        }
      }
      SetTextColor(hdc,RGB(255,255,255));
      TextOut(hdc, pos.x, pos.y, txt.c_str(), txt.length()); 
      SelectObject(hdc, hOldFont); 

      // If transparent, must paste with key color...
      img0.paste(img,x,y);
    }
  } 

  GdiFlush();
#endif  
}

int ImageAcquireDeviceIp::grabFrame()
{
	assert(isInitialized);

  double currentTime = PvUtil::time();

  // If we have been in error state for too long then restart the driver.
  if (currentTime - mLastValidFrameTime > 10)
  {
    mpCameraDriver.reset();
    mNextTryStartTime = currentTime;
  }

  if (!mpCameraDriver.get())
  {
    if (currentTime >= mNextTryStartTime)
    {
      tryInitDriver();
    }
  }

  {
    boost::mutex::scoped_lock lk(mFrameReadyMutex);  

    // Wait up to seconds for a good frame, otherwise send blue frames.
    double delay = 5.0;

    // When the mpNextFrame is reset by the callback, the frame is ready.
    bool gotGoodFrame = !mpNextFrame.get();

    //wait for frame to become available
    while (mpNextFrame.get())
    {
      //PVMSG("Will wait up to %d milliseconds\n", (int)delay);
      if (!mIsFrameReady.timed_wait(lk,(long)(delay*1000)))
      {
        //PVMSG("Timed out!\n");
        // The timeout expired. We'll send a blue frame. If the callback has
        // not finished, it will realize that mpNextFrame is no longer available
        // and just drop it.
        if (mpNextFrame.get())
        {
          mpNextFrame->pRGBImage->setAll(PV_RGB(0,0,255));
          drawText(*(mpNextFrame->pRGBImage),0,0,getLastErrorMessage(),false);
          mpNextFrame.reset();
          gotGoodFrame = false;
        }
      }
      else
      {
        // Event was signaled, then we got a good frame! (and mpNextFrame was reset by the callback)
        gotGoodFrame = true;
      }
    }

    if (gotGoodFrame)
    {
      mLastValidFrameTime = PvUtil::time();
    }

  }

  //PVMSG("Done grabbing frame: %d\n", mCameraId);

  //rotate circular buffer
  nextFrame();  

  // Prepare frame for next iteration so it can be filled asynchronously.
  // the next frame will be written to the back of the buffer
  {
    boost::mutex::scoped_lock lk(mFrameReadyMutex);  
    mpNextFrame = mFrames.back();
  }

  mLastFrameTime = currentTime;

	return 0;
}

bool 
ImageAcquireDeviceIp::isFirstFrame()
{
	return false;
};

void 
ImageAcquireDeviceIp::gotoFirstFrame()
{
};

void
ImageAcquireDeviceIp::setLastErrorMessage(const std::string& str)
{
  // Borrow the frame ready mutex...
  boost::mutex::scoped_lock lk(mFrameReadyMutex);
  mLastErrorMessage = str;
}

std::string 
ImageAcquireDeviceIp::getLastErrorMessage()
{
  // Borrow the frame ready mutex...
  boost::mutex::scoped_lock lk(mFrameReadyMutex);
  std::string str = mLastErrorMessage;
  return str;
}

void 
ImageAcquireDeviceIp::onVideoFrame(int cameraId, double startTimeStamp, double endTimeStamp,
    unsigned char *data, int dataSize)
{
  boost::mutex::scoped_lock lk(mFrameReadyMutex);

  MediaFrameInfoPtr pFrameInfo = mpNextFrame;
  if (!pFrameInfo.get())
  {
    // We still don't have a frame to fill for this camera.
    // We'll just drop the current data.
  }
  else
  {
    double currentTime = PvUtil::time();

    // The frame is available to us, let's fill it with data.
    Image32& frame = *(pFrameInfo->pRGBImage);

    // Check the frame size.
    if ((dataSize != 3*frame.width()*frame.height()) && (dataSize != 4*frame.width()*frame.height()))
    {
      setLastErrorMessage("Incorrect Frame Size");
      return; // Don't signal thread.
    }

    int frameBitsPerPixel = dataSize/frame.size();

    mpNextFrame->pRGBImage->setInternalFormat(Image32::PV_BGRA);

    unsigned char *pBmpBuffer = data;


    if (frameBitsPerPixel == 3)
    {
      unsigned char *pFrame = ((unsigned char *)frame.pointer()) + 4*frame.width()*(frame.height()-1);

      int skip = -2*(4*frame.width());

      for (int y = 0; y < frame.height(); y++) 
      {
        for (int x = 0; x < frame.width(); x++) 
        {
          pFrame[0] = pBmpBuffer[0];
          pFrame[1] = pBmpBuffer[1];
          pFrame[2] = pBmpBuffer[2];
	        pFrame[3] = 0;
          pBmpBuffer += 3;
          pFrame += 4;
        }
        pFrame += skip;
      }
    }
    else
    {
      memcpy(frame.pointer(),pBmpBuffer,4*frame.size()); 
    }
  }

  //Signal to waiting threads that a frame is now available.
  mpNextFrame.reset();
  mIsFrameReady.notify_one();
}

} // namespace ait

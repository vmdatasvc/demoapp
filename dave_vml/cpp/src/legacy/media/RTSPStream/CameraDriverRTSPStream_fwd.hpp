/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef CameraDriverRTSPStream_fwd_HPP
#define CameraDriverRTSPStream_fwd_HPP

#include <boost/shared_ptr.hpp>
// Forward declare - resolved in cpp by includes
class RTSPClient;
class UsageEnvironment;
namespace ait
{
class VideoUsageEnvironment;

class CameraDriverRTSPStream;
class RTSPStreamDecoderException;
typedef boost::shared_ptr<CameraDriverRTSPStream> CameraDriverRTSPStreamPtr;

}; // namespace ait

#endif // CameraDriverRTSPStream_fwd_HPP


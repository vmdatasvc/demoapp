


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
#ifndef CameraDriverRTSPStream_HPP
#define CameraDriverRTSPStream_HPP
// SYSTEM INCLUDES
//
#include <boost/utility.hpp>
#include <boost/thread.hpp>
#include <map>

// AIT INCLUDES
//

// LOCAL INCLUDES
//
#include <CameraDriver.hpp>
#include <ICameraDriverEvents.hpp>
#include <HttpSimpleClient.hpp>

// FORWARD REFERENCES
//

#include "CameraDriverRTSPStream_fwd.hpp"
#include <Base64.hpp>



namespace ait 
{


class RTSPStreamDecoderException : public std::exception
{
  public:
    RTSPStreamDecoderException() : mWhat("RTSPStream Decoder Exception") {}
    RTSPStreamDecoderException(const std::string msg) : mWhat(msg) {}
  private:
    std::string mWhat;
    virtual const char* what() { return mWhat.c_str(); }
};

/**
 * Bridge implementation for Axis camera.
 */
class CameraDriverRTSPStream : public CameraDriver
{
  //
  // LIFETIME
  //

public:

  /**
   * Constructor.
   */
  CameraDriverRTSPStream(const Settings& appSet);

  /**
   * Destructor
   */
  virtual ~CameraDriverRTSPStream();


  //
  // OPERATIONS
  //
private:
Settings paramSet;

public:

  /**
   * Start the main loop.
   */
  virtual void init(int numCameras);

  /**
   * Stop the main loop.
   */
  virtual void finish();

  /**
   * Set a specific parameter. See base class.
   */
  virtual void setParam(int cameraId, const std::string& name, const std::string& value)
  {
    if (name == "Camera.Password")
    {
      std::string decoded; 
      Base64::decode(value,decoded);
      mParams[name] = decoded;
    }
    else
    {
    mParams[name] = value;
    }

  }

  /**
   * Get a specific parameter. See base class.
   */
  virtual const std::string getParam(int cameraId, const std::string& name)
  {
    if (mParams.find(name) == mParams.end())
		setParam(-1,name,paramSet.getString(name,""));

    return mParams[name];
  }

  virtual const std::string getParam(const std::string& name)
  {
    return getParam(-1,name);
  }

  /**
  * Get a specific parameter and resolve any sub-parameter fields contained within
  * enclosed within a <tag>
  */

  virtual const std::string getParsedParam(const std::string& name)
  {
  std::string frmtStr = getParam(name);
  std::string paramName;
  std::string paramVal;
  std::size_t ltPtr = frmtStr.find("<");
  std::size_t gtPtr = frmtStr.find(">");
  while((ltPtr!=std::string::npos)&&(gtPtr!=std::string::npos)){
	paramName=frmtStr.substr(ltPtr+1,gtPtr-ltPtr-1);
	frmtStr.replace(ltPtr,gtPtr-ltPtr+1,getParam(paramName));
	ltPtr = frmtStr.find("<");
	gtPtr = frmtStr.find(">");
	}
  return frmtStr;
  }

 /**
   * Set the FPS. Note that some drivers might modify the
   * requested FPS because of device limitations.
   */
double getFramesPerSecond()
{
  return paramSet.getDouble("Fps/value [float]",15);
}

  /**
   * Register the event handler to receive frames.
   */
  virtual void registerEventHandler(ICameraDriverEvents *pEvents);
  
public:
  /**
   * This loop will read frames and call the callback on each new frame found.
   */
  void threadLoop();

    /**
   * The thread class for boost has to be copyable, so this will
   * be the container.
   */

  class ThreadHolder
  {
  public:
    ThreadHolder(CameraDriverRTSPStream& t) : mThread(t) {}
    CameraDriverRTSPStream& mThread;
    void operator()() { mThread.threadLoop(); }
  };

  ThreadHolder *mpThreadHolder;

  void operator()()
  {
    threadLoop();
  }

  /**
   * Start the connection, send the http request. Will throw on connection errors.
   */
  void initConnection();


protected:

  //
  // ATTRIBUTES
  //

protected:

ICameraDriverEvents *mpEvents;

double mFrameTime;
double mTimeBetweenFrames;

  char mExitThread;
  //char eventLoopWatchVariable;

  std::map<std::string,std::string> mParams;

  boost::thread *mpCaptureThread;
  VideoUsageEnvironment* env;
};


}; // namespace ait

#endif // CameraDriverRTSPStream_HPP




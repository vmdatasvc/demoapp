/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include <PvUtil.hpp>

#include "SoundEngine.hpp"
#include "Sound.hpp"
#include "wave.h"

namespace ait
{

/*********************************************************************
 * CONSTRUCTOR
 * if the name is specified, it loads the .wav file.
 *********************************************************************/
Sound::Sound()
{
  pbData = NULL;
  pwfx = NULL;
  ZeroMemory( szFileName, sizeof(szFileName));
  nFileIndex = 0;
  dwInternalFlags = 0;

  ZeroMemory( &dsbd, sizeof(DSBUFFERDESC));
  pDSB = NULL;

  pDSN = NULL;
  pDirectSound = &gSE;
}

Sound::Sound(char *szName)
{
  pbData = NULL;
  pwfx = NULL;
  ZeroMemory( szFileName, sizeof(szFileName));
  nFileIndex = 0;
  dwInternalFlags = 0;

  ZeroMemory( &dsbd, sizeof(DSBUFFERDESC));
  pDSB = NULL;

  pDSN = NULL;
  pDirectSound = &gSE;

  if (szName) {
    int i;
    for (i = strlen(szName)-1; i > 0; i--) {
      if (i == '/' || i == '\\') {
	      break;
      }
    }

    loadWave(szName,i);
  }
}

/*********************************************************************
 * Copy Constructor
 * It duplicates the sound that is provided as argument. See 
 * duplicate()
 *********************************************************************/
Sound::Sound(const Sound& snd)
{
  pbData = NULL;
  pwfx = NULL;
  ZeroMemory( szFileName, sizeof(szFileName));
  nFileIndex = 0;
  dwInternalFlags = 0;

  ZeroMemory( &dsbd, sizeof(DSBUFFERDESC));
  pDSB = NULL;

  pDSN = NULL;
  pDirectSound = &gSE;

  duplicate(snd);
}


/*********************************************************************
 * DESTRUCTOR
 *********************************************************************/
Sound::~Sound()
{
  if( pDSB != NULL )
  {
    pDSB->Release();
    pDSB = NULL;
  }
  if( pwfx )
  {
    GlobalFree( pwfx );
    pwfx = NULL;
  }
  if( pbData )
  {
    GlobalFree( pbData );
    pbData = NULL;
  }
  if(pDSN)
  {
    pDSN->Release();
    pDSN = NULL;
  }
}


/*********************************************************************
 * duplicate()
 *
 * Create a new sound buffer that shares the memory with the buffer
 * passed as parameter.
 *********************************************************************/
void Sound::duplicate(const Sound& snd)
{
  PVASSERT(snd.pDSB);

  if (FAILED(pDirectSound->pds->DuplicateSoundBuffer(snd.pDSB,&pDSB))) {
    PvUtil::exitError("Failed to duplicate sound buffer");
  }
  pDSB->GetFrequency(&freq);

  DSBCAPS dsbc;
  dsbc.dwSize = sizeof(dsbc);
  if(pDSB->GetCaps( &dsbc ))
  {
    PvUtil::exitError("Failed to get Sound Caps");
  }

  if( dsbc.dwFlags & DSBCAPS_LOCHARDWARE ) {
    dwInternalFlags |= FI_INTERNALF_HARDWARE;
  } else {
    dwInternalFlags &= ~FI_INTERNALF_HARDWARE;
  }

}

/*********************************************************************
 * loadWave()
 *
 * Given a filename and an index to the partial filename (i.e. an index 
 * into the possibly full pathname where the actual filename begins), 
 * this function will do everything needed to load a file into the 
 * Sound structure.
 *********************************************************************/
int Sound::loadWave( LPSTR lpszFile, int nIndx )
{
  setFileName( lpszFile, nIndx );

  // TODO: Need to add in support for ACM filters here

  // TODO: Need to determine what's "too big" to load static and then
  //       setup something for streaming the buffer instead.
  if( WaveLoadFile( szFileName, &cbDataSize, &pwfx, &pbData ) != 0 )
  {
    PvUtil::exitError( "Error reading wave file '%s'.",lpszFile);
	      goto LW_Error;
  }
  
  if( newDirectSoundBuffer() != 0 )
  {
  // There had better be a MainWnd object, or something is really messed
  
  PvUtil::exitError( "Cannot create new DirectSoundBuffer object");
  goto LW_Error;
  }

  dwInternalFlags |= FI_INTERNALF_LOADED;

  // If we haven't failed in loading so far, this point will be a valid
  // pointer to the wave's data.
  PVASSERT( NULL != pbData );

  return 0;

LW_Error:
  return -1;
}

/*********************************************************************
 * play()
 * Plays a buffer or updates playback flags according to some class 
 * state variables like our looping flag.
 *********************************************************************/
void Sound::play( void )
{
  if( pDSB )
  {
    if( isLooped())
      pDSB->Play( 0, 0, DSBPLAY_LOOPING );
    else
      pDSB->Play( 0, 0, 0 );

    dwInternalFlags |= FI_INTERNALF_PLAYING;
  }
}

/*********************************************************************
 * play()
 * Plays a buffer or updates playback flags according to some class 
 * state variables like our looping flag using different frequencies
 *********************************************************************/
void Sound::play(float factor)
{
  pDSB->SetFrequency(DWORD(factor*freq));

  if( pDSB )
  {
    if( isLooped())
      pDSB->Play( 0, 0, DSBPLAY_LOOPING );
    else
      pDSB->Play( 0, 0, 0 );

    dwInternalFlags |= FI_INTERNALF_PLAYING;
  }
}

/*********************************************************************
 * stop()
 * Stop the buffer and reset it's position to the start.
 *********************************************************************/
void Sound::stop( void )
{
  if( pDSB )
  {
    pDSB->Stop();
    pDSB->SetCurrentPosition( 0 );

    // Clear our internal state bit
    dwInternalFlags &= ~FI_INTERNALF_PLAYING;
  }
}

//
// Private members
//

/*********************************************************************
 * setFileName()
 * Saves the filname into the member variable
 *********************************************************************/
void Sound::setFileName( LPTSTR lpsz, int nIndx )
{
  lstrcpy( szFileName, lpsz );
  nFileIndex = nIndx;

  // If this PVASSERT fails, then we were handed a bad index value
  PVASSERT( nFileIndex < lstrlen( szFileName ));
}

/*********************************************************************
 * setFileName()
 * Creates a new directsound buffer
 *********************************************************************/
int Sound::newDirectSoundBuffer()
{
  DSBCAPS         dsbc;
  HRESULT         hr;
  BYTE            *pbWrite1	= NULL;
  BYTE            *pbWrite2    = NULL;
  DWORD           cbLen1;
  DWORD           cbLen2;
  BOOL fNotify = isNotifyOn(); // just to avoid making 2 calls to it.

  /* Set up the direct sound buffer. */
  dsbd.dwSize       = sizeof(DSBUFFERDESC);

  // We already set the flags to zero in the constructor.  Don't do it again
  // or we might wipe out anything a derived class has setup.
  if (fNotify)
  {
    dsbd.dwFlags |= DSBCAPS_STATIC | DSBCAPS_CTRLPOSITIONNOTIFY;
  }
  else
  {
    dsbd.dwFlags |= DSBCAPS_STATIC;
  }

  dsbd.dwFlags |= DSBCAPS_CTRLPAN | DSBCAPS_CTRLVOLUME | DSBCAPS_CTRLFREQUENCY;

  dsbd.dwFlags |= DSBCAPS_GETCURRENTPOSITION2;
  dsbd.dwFlags |= DSBCAPS_GLOBALFOCUS;

  dsbd.dwBufferBytes    = cbDataSize;
  dsbd.lpwfxFormat      = pwfx;

  /* Make sure these are NULL before we start */    
  pDSB = NULL;
  
  if( FAILED( hr = pDirectSound->pds->CreateSoundBuffer( &dsbd, &pDSB, NULL )))
  {
    goto ERROR_IN_ROUTINE;
  }

  // now set up notification pointer.
  if (fNotify) {
    if( FAILED(hr = pDSB->QueryInterface(IID_IDirectSoundNotify, (void**)&pDSN))) {
      PvUtil::exitError("Failed to create notification object!");
      goto ERROR_IN_ROUTINE;
    }
  }

  // Lock memory and copy data
  if( FAILED( hr = pDSB->Lock( 0, cbDataSize, (LPVOID *)&pbWrite1, &cbLen1,
                      (LPVOID *)&pbWrite2, &cbLen2, 0L ))) {
    goto ERROR_IN_ROUTINE;
  }

  PVASSERT( pbWrite1 != NULL );
  PVASSERT( cbLen1 == cbDataSize );

  CopyMemory( pbWrite1, pbData, cbDataSize );

  PVASSERT( 0 == cbLen2 );
  PVASSERT( NULL == pbWrite2 );

  /* Ok, now unlock the buffer, we don't need it anymore. */
  if( FAILED( hr = pDSB->Unlock( pbWrite1, cbDataSize, pbWrite2, 0 ))) {
    goto ERROR_IN_ROUTINE;
  }

  pbWrite1 = NULL;

  if (FAILED(hr = pDSB->SetVolume( MAXVOL_VAL ))) {
    goto ERROR_IN_ROUTINE;
  }

  if( FAILED( hr = pDSB->SetPan( MIDPAN_VAL ))) {
    goto ERROR_IN_ROUTINE;
  }

  pDSB->GetFrequency(&freq);

  dsbc.dwSize = sizeof(dsbc);
  if( hr = pDSB->GetCaps( &dsbc ))
  {
    goto ERROR_IN_ROUTINE;
  }

  if( dsbc.dwFlags & DSBCAPS_LOCHARDWARE ) {
    dwInternalFlags |= FI_INTERNALF_HARDWARE;
  } else {
    dwInternalFlags &= ~FI_INTERNALF_HARDWARE;
  }

  goto DONE_ROUTINE;

ERROR_IN_ROUTINE:
  if( pbWrite1 != NULL )
  {
    hr = pDSB->Unlock( pbWrite1, cbDataSize, pbWrite2, 0 );
    pbWrite1 = NULL;
  }

  if( NULL != pDSB )
  {
    pDSB->Release();
    pDSB = NULL;
  }

DONE_ROUTINE:
  return hr;
}

/*********************************************************************
 * recordWave()
 *
 * Given a filename and an index to the partial filename (i.e. an index 
 * into the possibly full pathname where the actual filename begins), 
 * this function records the sound buffer into the file.
 *********************************************************************/
int Sound::recordWave( LPSTR lpszFile, int nIndx )
{
  setFileName( lpszFile, nIndx );

  // TODO: Need to add in support for ACM filters here

  // TODO: Need to determine what's "too big" to load static and then
  //       setup something for streaming the buffer instead.
  if( WaveSaveFile( szFileName, cbDataSize, cbDataSize/2, pwfx, pbData ) != 0 )
  {
    PvUtil::exitError( "Error recording wave file '%s'.",lpszFile);
	      goto LW_Error;
  }
  
  //if( newDirectSoundBuffer() != 0 )
  //{
  //// There had better be a MainWnd object, or something is really messed
  //
  //PvUtil::exitError( "Cannot create new DirectSoundBuffer object");
  //goto LW_Error;
  //}

  //dwInternalFlags |= FI_INTERNALF_LOADED;

  //// If we haven't failed in loading so far, this point will be a valid
  //// pointer to the wave's data.
  //PVASSERT( NULL != pbData );

  return 0;

LW_Error:
  return -1;
}

} // namespace ait
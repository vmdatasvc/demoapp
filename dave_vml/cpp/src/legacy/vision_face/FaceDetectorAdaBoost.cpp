/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif
#include <vector>
#include "legacy/vision_face/FaceDetectorAdaBoost.hpp"

//#include "Settings.hpp"

namespace ait 
{

namespace vision
{

// object initialized in the constructor
FaceDetectorAdaBoost::FaceDetectorAdaBoost()
: mIsInitialized(true), mpCascade(0),
  mNumNeighbors(2)
{
  // The minimum size for the Ada Boost face detector is 24x24.
  mTrainingMinSize.set(24,24);
}

FaceDetectorAdaBoost::~FaceDetectorAdaBoost()
{
  if (mpCascade)
  {
#ifdef OLDCV
    //cvReleaseHidHaarClassifierCascade( &mpCascade );
	  cvReleaseHaarClassifierCascade( &mpCascade );
#else
    mpCascadd.r
#endif
  }
  mpCascade = 0;
}


void FaceDetectorAdaBoost::init(const std::string modelName)
{
#ifdef OLDCV
//    CvHaarClassifierCascade* cascade = cvLoadHaarClassifierCascade("<default_face_cascade>", cvSize(24,24));
//    /* images are assigned inside cvHaarDetectObject, so pass NULL pointers here */
//    mpCascade = cvCreateHidHaarClassifierCascade( cascade, 0, 0, 0, 1 );
//    /* the original cascade is not needed anymore */
//     cvReleaseHaarClassifierCascade( &cascade );
     std::string modelName1 = Settings::replaceVars(modelName);
     if (!PvUtil::fileExists(modelName1.c_str()))
     {
       PvUtil::exitError("File %s not found!",modelName1.c_str());
     }

     CvHaarClassifierCascade* cascade = (CvHaarClassifierCascade*)cvLoad( modelName1.c_str(), 0, 0, 0 );
     mpCascade = cascade;
#else	// ver 2.4.11


#endif
}


//
// OPERATIONS
//

void 
FaceDetectorAdaBoost::detect(const Image8 &grayImage, bool async)
{
  std::vector<Rectanglei> vecRect;

	// Optimization for reducing the search space given to the face detector.
  Image8 subsampledImage;
  float resampleFactor = 1;

  CvSize minFaceSize = cvSize(0,0);

  if (mResampleForMinSize)
  {
    resampleFactor = resampleForMinSize(grayImage,subsampledImage);
  }
  else
  {
    minFaceSize = cvSize(static_cast<int>(mMinFaceSize(0)*resampleFactor), 
                         static_cast<int>(mMinFaceSize(1)*resampleFactor));
  }
  
  // These are the parameters for cvHaarDetectObjects
  CvSize maxFaceSize = cvSize(static_cast<int>(mMaxFaceSize(0)*resampleFactor), 
                              static_cast<int>(mMaxFaceSize(1)*resampleFactor));

  Image8 const *pImg;

  if (resampleFactor < 1.0f)
  {
    pImg = &subsampledImage;
  }
  else
  {
    pImg = &grayImage;
  }

  unsigned char* pixels = pImg->pointer();
  
  assert(pixels!=NULL);
  
  int w=pImg->width();
  int h=pImg->height();
  
  // This face detector won't detect faces smaller than 20x20
  if (w < 20 || h < 20)
  {
    nFaces = 0;
    return;
  }
  
  // call adaboost function for face detection 
  IplImage* iplImage;
  CvSize size;
  size.width = w;
  size.height = h;
  iplImage =  cvCreateImageHeader( size, IPL_DEPTH_8U , 1);
  cvSetData(iplImage,(char*)pixels,pImg->width());

  CvMemStorage* storage = cvCreateMemStorage(0);
  CvSeq* faces;
  int i, scale = 1;

#ifdef OLDCV
  /* use the fastest variant */
  // Disabled Canny Prunning because it performs invalid memory access when applying
  // the sobel operator inside the OpenCV code.
  faces = cvHaarDetectObjects( iplImage, mpCascade, storage, 1.2, mNumNeighbors, 0/*CV_HAAR_DO_CANNY_PRUNING*/,
    maxFaceSize,minFaceSize);
#else
  faces =  cvHaarDetectObjects( iplImage, mpCascade, storage, 1.2, mNumNeighbors, 0 /*CV_HAAR_DO_CANNY_PRUNING*/,
                                            minFaceSize, maxFaceSize );
#endif
  
  int n = faces->total;
  int count1 = 0;
  int count2 = 0;
  //  iter = vecRect.begin();
  for( i = 0; i < faces->total; i++ )
  {
    /* extract the rectanlges only */
#ifdef OLDCV
    CvRect face_rect = *(CvRect*)cvGetSeqElem( faces, i, 0 );   
#else
    CvRect face_rect = *(CvRect*)cvGetSeqElem( faces, i);   
#endif
    
    vecRect.push_back(Rectangle<int>(face_rect.x*scale, face_rect.y*scale, 
      face_rect.width*scale + face_rect.x*scale,
      face_rect.height*scale + face_rect.y*scale));
    int check = 0;
    
    for(int k = 0; k < count1; k++)
    {
      int  xx0 = std::max(vecRect[i].x0,vecRect[k].x0);
      int  yy0 = std::max(vecRect[i].y0,vecRect[k].y0);
      int  xx1 = std::min(vecRect[i].x1,vecRect[k].x1);
      int  yy1 = std::min(vecRect[i].y1,vecRect[k].y1);
      if(((xx1-xx0) > 0)&&((yy1-yy0) > 0))
      {
        if((xx1-xx0)*(yy1-yy0) > 0.5*(vecRect[k].x1-vecRect[k].x0)*(vecRect[k].y1-vecRect[k].y0))
        {
          //  overlap..
          check++;
        }
      }
    }
    if(!check)
    {
      faceArray[count2].x = face_rect.x*scale;
      faceArray[count2].y = face_rect.y*scale;
      faceArray[count2].w = face_rect.width*scale;
      faceArray[count2].h = face_rect.height*scale;
      count2++;
    }
    count1++;
  }
  
  //   if( small_image != image )
  //       cvReleaseImage( &small_image );
  cvReleaseMemStorage( &storage );
  
  cvReleaseImageHeader(&iplImage);

  nFaces = count2;

  // Now we have to scan the array and adjust the sizes so it fits the original image.
  for (i = 0; i < nFaces; i++)
  {
    faceArray[i].x = (int)(faceArray[i].x/resampleFactor);
    faceArray[i].y = (int)(faceArray[i].y/resampleFactor);
    faceArray[i].w = (int)(faceArray[i].w/resampleFactor);
    faceArray[i].h = (int)(faceArray[i].h/resampleFactor);
  }
}



//
// ACCESS
//

//
// INQUIRY
//


}; // namespace vision

}; // namespace ait


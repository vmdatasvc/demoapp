#include "mem.hpp"
#include "file.hpp"
#include "fdb.hpp"

FDB *fdb_open( const char *name )
{
  FDB *fdb = 0;

//  NEW( fdb )
  fdb = (FDB *)malloc(sizeof(*fdb));
  memset(fdb, 0, sizeof(*fdb));


  OPEN( fdb->fp, name )
  READ( fdb->fp, &fdb->num_imgs )
  READ( fdb->fp, &fdb->face_size )
  READ( fdb->fp, &fdb->img_width )
  READ( fdb->fp, &fdb->img_height )

  goto finish;
  err:
    fdb_close( fdb ); fdb = 0;
  finish:
    ;

  return fdb;
}

int fdb_close( FDB *fdb )
{
  int ret = 0;

  if( fdb )
  {
    CLOSE( fdb->fp )
    DELETE( fdb )
  }

  goto finish;
  err:
    ret = -1;
  finish:
    ;

  return ret;
}

int fdb_fetch( FDB *fdb, int num,
               float *pose, unsigned char *img )
{
  int ret = 0;

  if( fdb == 0 )
  {
    fprintf( stderr, "Trying to call fdb_fetch()"
                     " with null FDB pointer\n" );
    goto err;
  }

  SEEK( fdb->fp, 16 + num * (12 + fdb->img_width * fdb->img_height) )
  READ_ARRAY( fdb->fp, pose, 3 )
  READ_ARRAY( fdb->fp, img, fdb->img_width * fdb->img_height )

  goto finish;
  err:
    ret = -1;
  finish:
    ;

  return ret;
}

int fdb_num_imgs( FDB *fdb )
{
  int ret = 0;

  if( fdb == 0 )
  {
    fprintf( stderr, "Trying to call fdb_num_imgs()"
                     " with null FDB pointer\n" );
    goto err;
  }

  ret = fdb->num_imgs;

  goto finish;
  err:
    ret = -1;
  finish:
    ;

  return ret;
}

int fdb_img_width( FDB *fdb )
{
  int ret = 0;

  if( fdb == 0 )
  {
    fprintf( stderr, "Trying to call fdb_img_width()"
                     " with null FDB pointer\n" );
    goto err;
  }

  ret = fdb->img_width;

  goto finish;
  err:
    ret = -1;
  finish:
    ;

  return ret;
}

int fdb_img_height( FDB *fdb )
{
  int ret = 0;

  if( fdb == 0 )
  {
    fprintf( stderr, "Trying to call fdb_img_height()"
                     " with null FDB pointer\n" );
    goto err;
  }

  ret = fdb->img_height;

  goto finish;
  err:
    ret = -1;
  finish:
    ;

  return ret;
}

int fdb_face_size( FDB *fdb )
{
  int ret = 0;

  if( fdb == 0 )
  {
    fprintf( stderr, "Trying to call fdb_face_size()"
                     " with null FDB pointer\n" );
    goto err;
  }

  ret = fdb->face_size;

  goto finish;
  err:
    ret = -1;
  finish:
    ;

  return ret;
}

#include "legacy/vision_face/SparseGeometricNN.hpp"
#include "legacy/vision_face/YawPitchNN.hpp"
#include "legacy/pv/PvUtil.hpp"
#include "legacy/vision_tools/image_proc.hpp"

const char *help3 = "\
MLPTorch III (c) Trebolloc & Co 2003\n\
\n\
This program will test a single image using the trained face detection NN from the model_file.\n";

using namespace std;
//using namespace Torch;

  // Constructor
void YawPitchNN::init(string modelFile_, int imgWidth_, int imgHeight_)
{
  Allocator *allocator = new Allocator;
 
  get_args();

  modelFile = modelFile_;
	imgWidth = imgWidth_;
	imgHeight = imgHeight_;
//	n_inputs = imgWidth * imgHeight;

  DiskXFile::setLittleEndianMode();

  DiskXFile *model = NULL;

//  mvNorm.reserve(1);
  
  model = new(allocator) DiskXFile(modelFile.c_str(), "r");
  cmd.loadXFile(model);

  Linear *c1 = new(allocator) Linear(n_inputs, n_hu1);
  c1->setROption("weight decay", weight_decay);
  Tanh*c2 = new(allocator) Tanh(n_hu1);
  Linear *c3 = new(allocator) Linear(n_hu1, n_hu2);
  c3->setROption("weight decay", weight_decay);
  Tanh *c4 = new(allocator) Tanh(n_hu2);
  Linear *c5 = new(allocator) Linear(n_hu2, n_hu3);
  c5->setROption("weight decay", weight_decay);
  Tanh *c6 = new(allocator) Tanh(n_hu3);
  Linear *c7 = new(allocator) Linear(n_hu3, n_targets);
  c7->setROption("weight decay", weight_decay);

  mlp.addFCL(c1);    
  mlp.addFCL(c2);
  mlp.addFCL(c3);
  mlp.addFCL(c4);
  mlp.addFCL(c5);
  mlp.addFCL(c6);
  mlp.addFCL(c7);

  mlp.build();
  mlp.setPartialBackprop();

//  MeanVarNorm *mvNorm;
	mvNorm = new(allocator) MeanVarNorm(n_inputs, n_targets);
  mvNorm->loadXFile(model);

  mlp.loadXFile(model);    

  printf("%s loaded.\n",modelFile.c_str());

	lsv=0;

	FILE *fpLSV;
  if(fpLSV=fopen("c:/ai/common/data/facial_geometry_estimator/face_3D_YawPitch/lsv5000.save","r"))
    {
      lsv=lsv_read("c:/ai/common/data/facial_geometry_estimator/face_3D_YawPitch/lsv5000.save");
      fclose(fpLSV);
    }
  else
    {    
			fprintf(stderr, "Need c:/ai/common/data/facial_geometry_estimator/face_3D_YawPitch/lsv5000.save!\n");
		exit(1);
		}
//  if(fpLSV=fopen("c:/Program Files/Advanced Interfaces/VideoMining 2.1/common/data/facial_geometry_estimator/face_3D_YawPitch/lsv5000.save","r"))
//    {
//      lsv=lsv_read("c:/Program Files/Advanced Interfaces/VideoMining 2.1/common/data/facial_geometry_estimator/face_3D_YawPitch/lsv5000.save");
//      fclose(fpLSV);
//    }
//  else
//    {    
//			fprintf(stderr, "Need c:/Program Files/Advanced Interfaces/VideoMining 2.1/common/data/facial_geometry_estimator/face_3D_YawPitch/lsv5000.save!\n");
//		exit(1);
//		}

  if(lsv->num_filters!=n_inputs)
  {
    fprintf(stderr, "The lsv5000 dim: %d and the model dim: %d doesn't match\n", lsv->num_filters, n_inputs);
    exit(1);
  }

  printf("YPNN input image width = %d height = %d, Arclet3D output dim = %d\n",imgWidth,imgHeight,n_inputs);
}

// Torch3 interface to read Torch style trained model file
// It seems that the unnecessary options were added from training; 
// need to remove these within the training code
void YawPitchNN::get_args()
{
  //  char *modelListFile, *modelDir, *imageFile;
  char *valid_file;
  char *file;

  int max_load;
  int max_load_valid;
  real accuracy;
  real learning_rate;
  real decay;
  int max_iter;
  int the_seed;

  char *dir_name;
  char *model_file;
  int k_fold;
  bool binary_mode;
  bool regression_mode;
  int class_against_the_others;

  // Put the help line at the beginning
  cmd.info(help3);

  // Train mode
  cmd.addText("\nArguments:");
  cmd.addSCmdArg("file", &file, "the train file");
  cmd.addICmdArg("n_inputs", &n_inputs, "input dimension of the data", true);
  cmd.addICmdArg("n_targets", &n_targets, "output dim. (regression) or # of classes (classification)", true);

  cmd.addText("\nModel Options:");
  cmd.addICmdOption("-class", &class_against_the_others, -1, "train the given class against the others", true);
  cmd.addICmdOption("-nhu1", &n_hu1, 25, "number of hidden units", true);
  cmd.addICmdOption("-nhu2", &n_hu2, 50, "number of hidden units", true);
  cmd.addICmdOption("-nhu3", &n_hu3, 50, "number of hidden units", true);
  cmd.addBCmdOption("-rm", &regression_mode, false, "regression mode ?", true);

  cmd.addText("\nLearning Options:");
  cmd.addICmdOption("-iter", &max_iter, 25, "max number of iterations");
  cmd.addRCmdOption("-lr", &learning_rate, 0.01, "learning rate");
  cmd.addRCmdOption("-e", &accuracy, 0.00001, "end accuracy");
  cmd.addRCmdOption("-lrd", &decay, 0, "learning rate decay");
  cmd.addICmdOption("-kfold", &k_fold, -1, "number of folds, if you want to do cross-validation");
  cmd.addRCmdOption("-wd", &weight_decay, 0, "weight decay", true);

  cmd.addText("\nMisc Options:");
  cmd.addICmdOption("-seed", &the_seed, -1, "the random seed");
  cmd.addICmdOption("-load", &max_load, -1, "max number of examples to load for train");
  cmd.addICmdOption("-load_valid", &max_load_valid, -1, "max number of examples to load for valid");
  cmd.addSCmdOption("-valid", &valid_file, "", "validation file, if you want it");
  cmd.addSCmdOption("-dir", &dir_name, ".", "directory to save measures");
  cmd.addSCmdOption("-save", &model_file, "", "the model file");
  cmd.addBCmdOption("-bin", &binary_mode, false, "binary mode for files");

  // Test mode
  cmd.addMasterSwitch("--test");
  cmd.addText("\nArguments:");

  cmd.addText("\nMisc Options:");
  cmd.addICmdOption("-load", &max_load, -1, "max number of examples to load for train");
  cmd.addSCmdOption("-dir", &dir_name, ".", "directory to save measures");
  cmd.addBCmdOption("-bin", &binary_mode, false, "binary mode for files");

  // Read the command line
  int argc = 2;
  char **argv;
  argv = (char **)malloc(2*sizeof(char *));
  argv[0] = (char *)malloc(11*sizeof(char));
  argv[1] = (char *)malloc(7*sizeof(char));
  strcpy(argv[0], "executable");
  strcpy(argv[1], "--test");
//  argv[0] = "executable";
//  argv[1] = "--test";
  int mode = cmd.read(argc, argv);
}
//get_args()
//{
//  int max_load, k_fold, the_seed, class_against_the_others;
//  real accuracy, decay;
//  char *dir_name, *valid_file;
//  bool regression_mode;
//
//  // Train mode
//  cmd.addICmdArg("n_inputs", &n_inputs, "input dimension of the data", true);
//  cmd.addICmdArg("n_targets", &n_targets, "output dim. (regression) or # of classes (classification)", true);
//  cmd.addICmdOption("-class", &class_against_the_others, -1, "train the given class against the others", true);
//  cmd.addICmdOption("-nhu1", &n_hu1, 25, "number of hidden units", true);
//  cmd.addICmdOption("-nhu2", &n_hu2, 50, "number of hidden units", true);
//  cmd.addICmdOption("-nhu3", &n_hu3, 50, "number of hidden units", true);
//  cmd.addBCmdOption("-rm", &regression_mode, false, "regression mode ?", true);
//  cmd.addRCmdOption("-e", &accuracy, (float)0.00001, "end accuracy");
//  cmd.addRCmdOption("-lrd", &decay, 0, "learning rate decay");
//  cmd.addICmdOption("-kfold", &k_fold, -1, "number of folds, if you want to do cross-validation");
//  cmd.addRCmdOption("-wd", &weight_decay, 0, "weight decay", true);
//  cmd.addICmdOption("-seed", &the_seed, -1, "the random seed");
//  cmd.addSCmdOption("-valid", &valid_file, "", "validation file, if you want it");
//
//  // Test mode
//  cmd.addMasterSwitch("--test");
//  cmd.addICmdOption("-load", &max_load, -1, "max number of examples to load for train");
//  cmd.addSCmdOption("-dir", &dir_name, ".", "directory to save measures");
//
//  // Read the command line
////  char *argv[2] = {"executable", "--test"};
////  int mode = cmd.read(2, argv);
//  int argc = 2;
//  char **argv;
//  argv = (char **)malloc(2*sizeof(char *));
//  argv[0] = (char *)malloc(11*sizeof(char));
//  argv[1] = (char *)malloc(7*sizeof(char));
//  argv[0] = "executable";
//  argv[1] = "--test";
//  int mode = cmd.read(argc, argv);
//}


void YawPitchNN::estimateYawPitch(Image8 faceImage, Image8& outImage, float X, float Y, float S, float O)
{
  Image8 tempImage(faceImage);

  outImage.centerRotateRescaleShift(tempImage, -O, 12.5/S, X, Y-4.28*(S/12.5), 0, -1-0*4.28, imgWidth, imgHeight);
  Image8 ttImage(outImage);

  PvImageProc imageProc;
//  imageProc.EqualizeImage(outImage);
  ait::image_proc::smoothImage(outImage, ait::image_proc::SMOOTH_GAUSSIAN, 3);

  unsigned char *ucImage;

  ucImage = (unsigned char *) malloc(imgWidth*imgHeight * sizeof(unsigned char));
  int count=0;
  for(int y=0; y<imgHeight; y++)
    for(int x=0; x<imgWidth; x++)
      ucImage[count++] = outImage(x,y);

	float *responseVec;
	responseVec = (float *)malloc(lsv->num_filters*sizeof(float));

  lsv_apply_filters(lsv, responseVec, ucImage);
	 
  Sequence inputs(1, n_inputs);

// Normalize the input to NN
  for(int i=0; i < inputs.frame_size; i++)
	{
      inputs.frames[0][i] = responseVec[i];
//  	  printf("%f:(%f,%f) ",inputs.frames[0][i],mvNorm->inputs_mean[i],mvNorm->inputs_stdv[i]);
      inputs.frames[0][i] = (inputs.frames[0][i] - mvNorm->inputs_mean[i]) / mvNorm->inputs_stdv[i]; 
  } 
   
		// NN forward
 	mlp.forward(&inputs);
    
  float rescale = 1.0/1.0;
  float cc, cs, sc, ss, addYP, subYP;
  cc = mlp.outputs->frames[0][0]*rescale;
  cs = mlp.outputs->frames[0][1]*rescale;
  sc = mlp.outputs->frames[0][2]*rescale;
  ss = mlp.outputs->frames[0][3]*rescale;

  addYP = atan2(sc+cs, cc-ss);
  subYP = atan2(sc-cs, cc+ss);

  estYaw = 0.5*(addYP + subYP);
  estPitch = 0.5*(addYP - subYP);

//  printf("estY = %f, estP = %f\n",estYaw,estPitch);

	free(ucImage);
	free(responseVec);
}

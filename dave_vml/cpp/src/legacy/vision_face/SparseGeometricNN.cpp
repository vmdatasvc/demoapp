#include "legacy/vision_face/SparseGeometricNN.hpp"

const char *help = "\
MLPTorch III (c) Trebolloc & Co 2003\n\
\n\
This program will test a single image using the trained face detection NN from the model_file.\n";

using namespace std;
using namespace Torch;

  // Constructor
//SparseGeometricNN::SparseGeometricNN(string modelListFile_, string modelDir_)
void SparseGeometricNN::init(string modelListFile_, string modelDir_)
{
  get_args();

  modelListFile = modelListFile_;
  modelDir = modelDir_;

  DiskXFile::setLittleEndianMode();
  Allocator *allocator = new Allocator;
  DiskXFile *model = NULL;

//  model = new(allocator) DiskXFile("Train.model", "r");
//  cmd.loadXFile(model);
//  printf("model_file:%s n_inputs=%d n_hu1=%d n_hu2=%d n_hu3=%d n_targets=%d\n",
//  modelListFile.c_str(),n_inputs,n_hu1,n_hu2,n_hu3,n_targets); 

  mvNorm.reserve(200);

  if(fp = fopen(modelListFile.c_str(), "r"))
  {}
  else
  {
    fprintf(stderr,"Model list file %s not found!\n", modelListFile.c_str());
	exit(1);
  }

  int counter = 0;

// Read the NN model files and construct NNs
  while(fscanf(fp, "%f %f %f %f\n",&midEyeX, &midEyeY, &scale, &orien) == 4)
  {
    cout<<counter<<" "<<midEyeX<<" "<<midEyeY<<" "<<scale<<" "<<orien<<endl;
    ModelPerson mP(midEyeX, midEyeY, scale, orien);
    mP.copy(&mPerson[counter]);

    char index[1024];
	sprintf(index,"/Face_%4.2f_%4.2f_%4.2f_%4.2f/Train.model",midEyeX,midEyeY,scale,orien);
    string modelFile(modelDir);
	modelFile.append(index);
	cout << modelFile<<endl;

    model = new(allocator) DiskXFile(modelFile.c_str(), "r");
    cmd.loadXFile(model);

//	numInputTmp[counter] = n_inputs;
    printf("model_file:%s n_inputs=%d n_hu1=%d n_hu2=%d n_hu3=%d n_targets=%d\n",
    modelListFile.c_str(),n_inputs,n_hu1,n_hu2,n_hu3,n_targets); 
 
    Linear *c1 = new(allocator) Linear(n_inputs, n_hu1);
    c1->setROption("weight decay", weight_decay);
    Tanh*c2 = new(allocator) Tanh(n_hu1);
    Linear *c3 = new(allocator) Linear(n_hu1, n_hu2);
    c3->setROption("weight decay", weight_decay);
    Tanh *c4 = new(allocator) Tanh(n_hu2);
    Linear *c5 = new(allocator) Linear(n_hu2, n_hu3);
    c5->setROption("weight decay", weight_decay);
    Tanh *c6 = new(allocator) Tanh(n_hu3);
    Linear *c7 = new(allocator) Linear(n_hu3, n_targets);
    c7->setROption("weight decay", weight_decay);

    mlp[counter].addFCL(c1);    
    mlp[counter].addFCL(c2);
    mlp[counter].addFCL(c3);
    mlp[counter].addFCL(c4);
    mlp[counter].addFCL(c5);
    mlp[counter].addFCL(c6);
    mlp[counter].addFCL(c7);

    mlp[counter].build();
    mlp[counter].setPartialBackprop();

    MeanVarNorm *mvN;
	mvN = new(allocator) MeanVarNorm(n_inputs, n_targets);
    mvN->loadXFile(model);

    mvNorm.push_back(mvN);

    mlp[counter].loadXFile(model);    
	counter++;
  }
  fclose(fp);

  numModel = counter;
  printf("numModel = %d\n",numModel);
}

void SparseGeometricNN::get_args()
{
  //  char *modelListFile, *modelDir, *imageFile;
  char *valid_file;
  char *file;

  int max_load;
  int max_load_valid;
  real accuracy;
  real learning_rate;
  real decay;
  int max_iter;
  int the_seed;

  char *dir_name;
  char *model_file;
  int k_fold;
  bool binary_mode;
  bool regression_mode;
  int class_against_the_others;

  // Put the help line at the beginning
  cmd.info(help);

  // Train mode
  cmd.addText("\nArguments:");
  cmd.addSCmdArg("file", &file, "the train file");
  cmd.addICmdArg("n_inputs", &n_inputs, "input dimension of the data", true);
  cmd.addICmdArg("n_targets", &n_targets, "output dim. (regression) or # of classes (classification)", true);

  cmd.addText("\nModel Options:");
  cmd.addICmdOption("-class", &class_against_the_others, -1, "train the given class against the others", true);
  cmd.addICmdOption("-nhu1", &n_hu1, 25, "number of hidden units", true);
  cmd.addICmdOption("-nhu2", &n_hu2, 50, "number of hidden units", true);
  cmd.addICmdOption("-nhu3", &n_hu3, 50, "number of hidden units", true);
  cmd.addBCmdOption("-rm", &regression_mode, false, "regression mode ?", true);

  cmd.addText("\nLearning Options:");
  cmd.addICmdOption("-iter", &max_iter, 25, "max number of iterations");
  cmd.addRCmdOption("-lr", &learning_rate, 0.01, "learning rate");
  cmd.addRCmdOption("-e", &accuracy, 0.00001, "end accuracy");
  cmd.addRCmdOption("-lrd", &decay, 0, "learning rate decay");
  cmd.addICmdOption("-kfold", &k_fold, -1, "number of folds, if you want to do cross-validation");
  cmd.addRCmdOption("-wd", &weight_decay, 0, "weight decay", true);

  cmd.addText("\nMisc Options:");
  cmd.addICmdOption("-seed", &the_seed, -1, "the random seed");
  cmd.addICmdOption("-load", &max_load, -1, "max number of examples to load for train");
  cmd.addICmdOption("-load_valid", &max_load_valid, -1, "max number of examples to load for valid");
  cmd.addSCmdOption("-valid", &valid_file, "", "validation file, if you want it");
  cmd.addSCmdOption("-dir", &dir_name, ".", "directory to save measures");
  cmd.addSCmdOption("-save", &model_file, "", "the model file");
  cmd.addBCmdOption("-bin", &binary_mode, false, "binary mode for files");

  // Test mode
  cmd.addMasterSwitch("--test");
  cmd.addText("\nArguments:");

  cmd.addText("\nMisc Options:");
  cmd.addICmdOption("-load", &max_load, -1, "max number of examples to load for train");
  cmd.addSCmdOption("-dir", &dir_name, ".", "directory to save measures");
  cmd.addBCmdOption("-bin", &binary_mode, false, "binary mode for files");

  // Read the command line
  int argc = 2;
  char **argv;
  argv = (char **)malloc(2*sizeof(char *));
  argv[0] = (char *)malloc(11*sizeof(char));
  argv[1] = (char *)malloc(7*sizeof(char));
  argv[0] = "executable";
  argv[1] = "--test";
  int mode = cmd.read(argc, argv);
}

void SparseGeometricNN::applyModel(int numEstModel, int wgt1, int wgt2, Image8 faceImage)
{
	Image8 cropImage(faceImage);
  Image8 tempImage(cropImage);
  cropImage.Lequalize(tempImage,12); //12 for 30x30
  unsigned char *inputData;
  float responses[200];
  float min, sum;

      // Model loop
  min = 10.0;
  Sequence inputs(1, n_inputs);

   for(int m = 0; m<numEstModel; m++)
  {
    inputData = (unsigned char *) malloc(n_inputs * sizeof(unsigned char));

	mPerson[m].computeFeatureLocations();

// Crop the face images according the the given feature windows

	mPerson[m].cropPassAllImage(cropImage, inputData);      


// Normalize the input to NN
   for(int i=0; i < inputs.frame_size; i++)
	{
      inputs.frames[0][i] = inputData[i];
//	  printf("%d ",inputData[i]);
	  inputs.frames[0][i] = (inputs.frames[0][i] - mvNorm[m]->inputs_mean[i]) / mvNorm[m]->inputs_stdv[i]; 
	}
		// NN forward
   	mlp[m].forward(&inputs);
	
    responses[m] = mlp[m].outputs->frames[0][0];

	if(responses[m] < min)
	  min = responses[m];

	free(inputData);
  }
  
  sum = 0.0;
  for(int m = 0; m<numEstModel; m++)
  {
    responses[m] = responses[m] - min;
	responses[m] = pow(responses[m], wgt1);

	sum += responses[m];
  }

  estMidEyeX = 0.0;
  estMidEyeY = 0.0;
  estMidEyeMouthDist = 0.0;
  estOrientation = 0.0;

  // Estimate
  for(int m=0; m<numEstModel; m++)
  {
	estMidEyeX += responses[m]*mPerson[m].mMidEyeX;
	estMidEyeY += responses[m]*mPerson[m].mMidEyeY;
	estMidEyeMouthDist += responses[m]*mPerson[m].mMidEyeMouthDist;
	estOrientation += responses[m]*mPerson[m].mOrientation;
	mPerson[m].score = responses[m] / sum;
	
//		printf("%f ",responses[m]);
  }
  
  estMidEyeX /= sum;
  estMidEyeY /= sum;
  estMidEyeMouthDist /= sum;
  estOrientation /= sum;

//  printf("\n Estimator:%f %f %f %f\n",estMidEyeX,estMidEyeY,estMidEyeMouthDist,estOrientation);
  // Construct model using the estimates
//  ModelPerson estModel(estMidEyeX, estMidEyeY, estMidEyeMouthDist, estOrientation);
//  estModel.computeFeatureLocations();
}
void SparseGeometricNN::estimateModel(int numEstModel, int wgt1, int wgt2, Image8 faceImage, float detecX, float detecY, 
									  int detecW, int detecH)
{
  int frameWidth = 60;
  int frameHeight = 60;

  Matrix<float> input(2,1), output(2,1);
  Matrix<float> rotation(2,2), center(2,1), frameCenter(2,1);
  Matrix<float> halfPix(2,1);
  halfPix(0,0) = 0.5; halfPix(1,0) = 0.5;

  int detecEndX = detecX + detecW;
  int detecEndY = detecY + detecH;

  float detecMidFrameX, detecMidFrameY, rot, scale, rotationX, rotationY;
  float faceInFrameX, faceInFrameY;

  detecMidFrameX = 0.5*(detecX+detecEndX);
  detecMidFrameY = 0.5*(detecY+detecEndY);
  rot = 0.0;
  scale = 0.5*frameWidth/detecW;
  rotationX = detecMidFrameX;
  rotationY = detecMidFrameY - 4.28/scale;
  faceInFrameX = frameWidth;
  faceInFrameY = frameHeight;
		  
 	// Normalize the images according to the AdaBoost detection window
  Image8 cropImage;
  cropImage.centerRotateRescaleShift(faceImage, -rot, scale, rotationX, rotationY, 
  						0, 0, faceInFrameX, faceInFrameY);

	applyModel(numEstModel, wgt1, wgt2, cropImage);
}

void SparseGeometricNN::estimateModel(int numEstModel, Image8 faceImage, float detecX, float detecY, 
									  int detecW, int detecH)
{
  estimateModel(numEstModel, 2, 2, faceImage, detecX, detecY, detecW, detecH);	
}
	
void SparseGeometricNN::estimateModel(Image8 faceImage, float detecX, float detecY, 
									  int detecW, int detecH)
{
  estimateModel(numModel, faceImage, detecX, detecY, detecW, detecH);

}

void affine(float *vecX, float *vecY, float scale, Matrix<float> rotMat, Matrix<float> rotCenter, Matrix<float> trans)
{
  Matrix<float> halfPix(2,1), input(2,1);

  halfPix(0,0) = 0.5;
  halfPix(1,0) = 0.5;

  input(0,0) = *vecX; input(1,0) = *vecY;
  input = scale*(rotMat*(input - rotCenter - halfPix)) + trans + halfPix;
  *vecX = input(0,0); *vecY = input(1,0);
}

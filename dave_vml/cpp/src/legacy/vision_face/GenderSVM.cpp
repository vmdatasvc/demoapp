#include "legacy/vision_face/GenderSVM.hpp"

namespace ait
{

  // Initialize
void GenderSVM::init(std::string modelFile_, int imgWidth_, int imgHeight_)
{
  modelFile = modelFile_;
  imgWidth = imgWidth_;
  imgHeight = imgHeight_;

  gSvm.load(modelFile);
}

void GenderSVM::correctLeqImage(Image8 faceImage, Image8& outImage, float X, float Y, float S, float O)
{
  Image8 tempImage(faceImage);

  outImage.centerRotateRescaleShift(tempImage, -O, (float)12.5/S, X, Y-4.28*(S/12.5), 0, (float)-4.28, imgWidth, imgHeight);	
  Image8 ttImage(outImage);
  outImage.Lequalize(ttImage,12); //12 for 30x30
}

float GenderSVM::classify(Image8 faceImage, Image8& outImage, float X, float Y, float S, float O)
{
  if(X==0.0 && Y==0.0 && S==0.0 && O==0.0)
  {}
  else
    correctLeqImage(faceImage, outImage, X, Y, S, O);
  
//  unsigned char *inputData;
  float *inputData;

//  inputData = (unsigned char *) malloc(imgWidth * imgHeight * sizeof(unsigned char));
  inputData = (float *) malloc(imgWidth * imgHeight * sizeof(float));
  int count=0;
  for(int y=0; y<imgHeight; y++)
    for(int x=0; x<imgWidth; x++)
      inputData[count++] = (float)outImage(x,y);

// SVM forward
  float response = gSvm.applySVM(inputData);

  free(inputData);
 	return response;
}

float GenderSVM::classify(Image8 faceImage)
{
  return classify(faceImage, faceImage, 0.0, 0.0, 0.0, 0.0);
}

} // namespace ait

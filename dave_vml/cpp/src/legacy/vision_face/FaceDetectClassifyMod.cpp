/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#pragma warning(disable:4503)
#endif

#include "FaceDetectClassifyMod.hpp"
#include <strutil.hpp>
#include "image_proc.hpp"
#include "PvImageProc.hpp"
#include "IlluminationGradient.hpp"
#include <VmsPacket.hpp>
#include <Socket.hpp>
#include <DateTime.hpp>
#include <ImageAcquireMod.hpp>

namespace ait 
{

namespace vision
{

FaceDetectClassifyMod::FaceDetectClassifyMod()
  : RealTimeMod(getModuleStaticName()),
  mRealTimeEventsPort(0)
{
}

FaceDetectClassifyMod::~FaceDetectClassifyMod()
{
}

void 
  FaceDetectClassifyMod::initExecutionGraph(std::list<VisionModPtr>& srcModules)
{
  RealTimeMod::initExecutionGraph(srcModules);
  
  // Add any source module to the given list here.
  
  //  getModule("ForegroundSegmentMod",mpForegroundSegmentMod);
  //  srcModules.push_back(mpForegroundSegmentMod);
}
  
void 
  FaceDetectClassifyMod::init()
{
  RealTimeMod::init();

  mpVmsPacket.reset(new VmsPacket(this));
  
  counter = 0;
  
  videoWidth = getImageAcquireModule()->getFrameSize().x;
  videoHeight = getImageAcquireModule()->getFrameSize().y;
  double videoFps = (int)getImageAcquireModule()->getFramesPerSecond();
  PVMSG("Input video size: %d %d %.02f\n",videoWidth,videoHeight,videoFps);

  mDoSkinToneDetection = mSettings.getBool("doSkinToneDetection",false);

  mDemoVisualization = mSettings.getBool("demoVisualization",false);
  mVisualizeFacialFeatures = mSettings.getBool("visualizeFacialFeatures",0);

  printf("Visualization options: %d %d\n",mDemoVisualization,mVisualizeFacialFeatures);

  int minFaceSize = mSettings.getInt("minFaceSize",24);
  int maxFaceSize = mSettings.getInt("maxFaceSize",160);

  int minFaceClassificationSize = mSettings.getInt("minFaceClassificationSize",24);

  if (minFaceClassificationSize < minFaceSize)
  {
    minFaceClassificationSize = minFaceSize;
  }

  mMinFaceClassificationSize.set(minFaceClassificationSize,minFaceClassificationSize);
  
  //  doMotionSegment = mSettings.getInt("doMotionSegment",0);
  //  doSkinSegment = mSettings.getInt("doSkinSegment",0);
  
  // Init AdaBoost face detector 
  //lower threshold version
//  mFrontFD.init(Settings::replaceVars("<CommonFilesPath>/data/opencv/haarcascades/haarcascade_frontalface_default_20stages.xml")); 
  //default openCV version
  mpFrontFD.reset(new FaceDetectorAdaBoostForeground());
  mpFrontFD->init(Settings::replaceVars("<CommonFilesPath>/data/opencv/haarcascades/haarcascade_frontalface_default.xml")); 
  
  // Set the Max/Min face sizes: good for CC entrance videos
  mpFrontFD->setMinFaceSize(Vector2i(minFaceSize,minFaceSize));
  mpFrontFD->setMaxFaceSize(Vector2i(maxFaceSize,maxFaceSize));
  
  mVideoFileString=mSettings.getString("videoFileString","input");
  
  //set up the *.csv output path
  string cameraOutputPath;
  cameraOutputPath=mSettings.getString("Installation/CameraOutputPath","");
  mFaceXYWHFile=cameraOutputPath;
  mFaceXYWHFile.append(mVideoFileString);
  mFaceXYWHFile.append(".csv");

  doRecordLabelsToCSV = mSettings.getInt("doRecordLabelsToCSV",0);
  if(doRecordLabelsToCSV)
  {
    fpFaceXYWH = fopen(mFaceXYWHFile.c_str(), "w");
    PVMSG("Output CSV file: %s\n",mFaceXYWHFile.c_str());
  }
 
  std::string a3nnModelList, a3nnModelDir, a3nnLsvFile;
  std::string refineA3nnModelList, refineA3nnModelDir, refineA3nnLsvFile;
  
  a3nnModelList=mSettings.getString("a3nnModelList","ModelList.txt");
  a3nnModelDir=mSettings.getString("a3nnModelDir","../x1_1000dim_sigma1.0");
  a3nnLsvFile=mSettings.getString("a3nnLsvFile","../x1_1000dim_sigma1.0/lsv.save");
  refineA3nnModelList=mSettings.getString("refineA3nnModelList","ModelList.txt");
  refineA3nnModelDir=mSettings.getString("refineA3nnModelDir","../x1_1000dim_sigma1.0");
  refineA3nnLsvFile=mSettings.getString("refineA3nnLsvFile","../x1_1000dim_sigma1.0/lsv.save");
  
  int numA3NNModel_ = mSettings.getInt("numEstA3NNModel",200);
  numA3NNModel = numA3NNModel_;
  int numRefineA3NNModel_ = mSettings.getInt("numEstRefineA3NNModel",0);
  numRefineA3NNModel = numRefineA3NNModel_;
  
  std::string mGSvmModelFile, mAfrSvmModelFile, mCauSvmModelFile, mHisSvmModelFile, mOriSvmModelFile, mAgeSvmModelFile;
  mGSvmModelFile = mSettings.getString("gSvmModelFile","./Train.model");
  mAfrSvmModelFile = mSettings.getString("afrSvmModelFile","./Train.model");
  mCauSvmModelFile = mSettings.getString("cauSvmModelFile","./Train.model");
  mHisSvmModelFile = mSettings.getString("hisSvmModelFile","./Train.model");
  mOriSvmModelFile = mSettings.getString("oriSvmModelFile","./Train.model");
  mAgeSvmModelFile = mSettings.getString("ageSvmModelFile","./Train.model");
  
  // Initializes Arclet3DNN face localization
  mA3NN.init(a3nnModelList, a3nnModelDir, a3nnLsvFile);
  mRefineA3NN.init(refineA3nnModelList,refineA3nnModelDir,refineA3nnLsvFile);

  // Initializes SparseYawPitchNN 3D facial pose
  int imgWidth = mSettings.getInt("imgWidth",60);
  int imgHeight = mSettings.getInt("imgHeight",60);
  
  std::string ypModelList, ypModelDir, ypLsvFile;
  
  ypModelList=mSettings.getString("ypModelList","ypModelList.txt");
  ypModelDir=mSettings.getString("ypModelDir","../x1_1000dim_sigma1.0");
  ypLsvFile=mSettings.getString("ypLsvFile","../x1_1000dim_sigma1.0/lsv5000.save");
  int numYPModel_ = mSettings.getInt("nYPModel",9);
  numYPModel = numYPModel_;
  
  doPose = mSettings.getInt("doPose",0);
  if(doPose)
  {
    mYPNN.init(ypModelList, ypModelDir, ypLsvFile, imgWidth, imgHeight);
  }
  
  // Initializes TorchSVM classifiers
  doGender = mSettings.getInt("doGender",0);
  doEthnicity = mSettings.getInt("doEthnicity",0);
  doAge = mSettings.getInt("doAge",0);
  
//  PVMSG("%d %d %d %d\n",doGender,doEthnicity,doPose,doAge);

  if(doGender)
  {
    mGSvm.init(mGSvmModelFile, 30, 30);
//    mGSvm.init("d:/Workspace/GenderRace/Arclet3DNN/Arclet3DNN_Model100L200_x5_1000dim_sigma1.0_wgt2_clean/30x30-Gender/NN/160-80-40-lr0.0075-e0.001-trainall/Train.model", 30, 30);
  }  
  
  if(doEthnicity)
  {
    mAfrSvm.init(mAfrSvmModelFile, 30, 30);
    mCauSvm.init(mCauSvmModelFile, 30, 30);
    mHisSvm.init(mHisSvmModelFile, 30, 30);
    mOriSvm.init(mOriSvmModelFile, 30, 30);
  }
  
  if(doAge)
  {
    PVMSG("Age file: %s\n",mAgeSvmModelFile.c_str()); 
    mAgeSvm.init(mAgeSvmModelFile, 30, 30);
  }  
  
  // Set the region of interest
  mRoi = mSettings.getRecti("roiRect",Rectanglei(0,0,320,240));

  showSkinToneWindow = mSettings.getInt("showSkinToneWindow",0);
  
  if(showSkinToneWindow)
  {
//    getImageAcquireModule()->setVisualizationFrameSize(Vector2i(videoWidth,2*videoHeight));
    getImageAcquireModule()->setVisualizationFrameSize(Vector2i(mRoi.width(),2*mRoi.height()));
  }
  
  // Initializes the frameSkipFactor to adjust the criteria (used to judge whether the track is true or spurious) for the frame rate
  mFrameRate = (float)getImageAcquireModule()->getFramesPerSecond();
  mFrameSkipFactor = mSettings.getFloat("frameSkipFactor",1.0);
  PVMSG("frameRate = %f frameSkipFactor = %f\n",mFrameRate,mFrameSkipFactor);


  mRealTimeEventsPort = mSettings.getInt("RealTimeEvents/listenPort",0);
      
  std::string biName = mSettings.getString("backgroundImageName","");
  if (biName != "")
  {
    mpBackgroundImage.reset(new Image32());
    mpBackgroundImage->load(biName.c_str());
    getImageAcquireModule()->setVisualizationFrameSize(Vector2i(mpBackgroundImage->width(),mpBackgroundImage->height()));
  }

//  mFaceViewRect = mSettings.getRecti("faceViewRect",Rectanglei(0,0,videoWidth,videoHeight));
  mFaceViewRect = mSettings.getRecti("faceViewRect",Rectanglei(0,0,mRoi.x1-mRoi.x0,mRoi.y1-mRoi.y0));

  mGenderUnknownRange = mSettings.getCoord2f("genderUnknownRange",Vector2f(0,0));

  mFaceMatchThreshold = mSettings.getDouble("mFaceMatchThreshold", 0.75);

  mFaceImagesOutputFolder = mSettings.getString("FaceImages/outputFolder","");
  mFaceImagesSendVmsAttribute = mSettings.getBool("FaceImages/sendVmsAttribute",false);
  mFaceImagesMaxFacesPerHour = mSettings.getInt("FaceImages/maxFacesPerHour",-1);
  mFaceImageQuality = mSettings.getFloat("FaceImages/imageQuality",0.5);

  mpVmsPacket->allowPause();
}

void
  FaceDetectClassifyMod::finish()
{

  // Terminate tracks to send any events at the end of the video.

  mpFrontFD.reset();
  
  if(doRecordLabelsToCSV)
  {
    fclose(fpFaceXYWH);
  }
  
  mpVmsPacket->finish();

  RealTimeMod::finish();
}

//
// OPERATIONS
//


void 
FaceDetectClassifyMod::process()
{
  Image32 frame;
  frame.crop(getCurrentFrame(),mRoi.x0,mRoi.y0,mRoi.width(),mRoi.height());
  
  //Time for this frame
  if(counter==0)
  {
    initTime = getCurrentTime();
  }
  
  counter ++;
  
  time = getCurrentTime() - initTime;
  
//  correctColorShift(&frame);
  mSkinTone = Image8(frame.width(), frame.height());

//  Image8 mSkinTone(frame.width(), frame.height());
  if(mDoSkinToneDetection)
  {
  // Skin tone detector
    mSkinToneDetec.findSkinTone(&frame, &mSkinTone);
  }
  else
  {
    mSkinTone.setAll(255);
  }
  
  Image8 grayFrame;
  
  PvImageConverter::convert(frame,grayFrame);

  // Face detection
  mpFrontFD->detect(grayFrame, mSkinTone);
//  mpFrontFD->detect(grayFrame);

//  PVMSG("AdB: %d\n",mpFrontFD->nrOfFaces());
//  return;
  
  //int iFD;
  faceImage = frame;
  
  mLastFrameFaces.clear();
  int fId = 0;
  mFaces.clear();
  for (int i = 0; i < mpFrontFD->nrOfFaces(); i++)
  {
    Face f = mpFrontFD->face(i);

//    PVMSG("Detected face: %d %d %d %d at %f\n",f.x,f.y,f.w,f.h,time);

    if (f.w >= mMinFaceClassificationSize.x && f.h >= mMinFaceClassificationSize.y)
    {
      FaceDataPtr pFace(new FaceData(fId++,0,time,f.x,f.y,f.w,f.h));
      mFaces.push_back(pFace);
    }
    mLastFrameFaces.push_back(Rectanglei(f.x,f.y,f.x+f.w,f.y+f.h));
  }

  std::vector<FaceDataPtr>::iterator iFace; //, jFace;

  int numFaces = mFaces.size();
    
  // Fill in the table with {track, face} data, and compute the scores
  for (iFace = mFaces.begin(); iFace != mFaces.end(); iFace ++)
  {     
      localizeFace(grayFrame, *iFace);
      classifyFace(grayFrame, *iFace);
  }

  // Generate real time classification events:
  if (mRealTimeEventsPort != 0)
  {
    for (iFace = mFaces.begin(); iFace != mFaces.end(); iFace ++)
    {
        DateTime time(getCurrentTime());
        std::string evt = "<?xml version='1.0'?><event type='person_classified' id='1' timestamp='" + time.toString() + "' ";
        
        if (doGender)
        {
          evt += "gender='" + tolower((*iFace)->mGenderString) + "' ";
        }
        if (doEthnicity)
        {
          evt += "ethnicity='" + tolower((*iFace)->mEthnicityString) + "' ";
        }
        if (doAge)
        {
          evt += "age='" + tolower((*iFace)->mAgeString) + "' ";
        }
        
        evt += "/>";
        
        PVMSG("Sending Real Time Event:\n");
        PVMSG("%s\n",evt.c_str());
        
        Socket s;
        s.open("localhost",(Uint)mRealTimeEventsPort,Socket::WRITE_ONLY|Socket::TCP);
        s.send(evt);
        s.close();
    }
  }

  if (!getImageAcquireModule()->isLiveVideoSource())
  {
    mpVmsPacket->updateProgress((double)getCurrentFrameNumber()/getImageAcquireModule()->getTotalNumberOfFrames());
  }

}


void 
FaceDetectClassifyMod::localizeFace(Image8 grayFrame, FaceDataPtr pFace)
{
  int x = pFace->x;
  int y = pFace->y;
  int w = pFace->w;
  int h = pFace->h;
  
  mA3NN.estimateModel(numA3NNModel, 2, 2, grayFrame, x, y, w, h);

  // Correct the bias introduced by the models
  mA3NN.mEstMidEyeMouthDist -= 0.5f;
  mA3NN.mEstOrientation += 0.024f;
  

//      Image8 a3NNChip(30,30);
  
  mRefineA3NN.estimateRefined(numRefineA3NNModel, grayFrame, pFace->mFaceChip, 
  (2*x+w)/2 + mA3NN.mEstMidEyeX - 30.0,(2*y+h)/2 + mA3NN.mEstMidEyeY - 30.0,
  mA3NN.mEstMidEyeMouthDist*(w/30.0),mA3NN.mEstOrientation);

  // Correct the bias introduced by the models
  mRefineA3NN.mEstMidEyeMouthDist -= 0.5f;
  mRefineA3NN.mEstOrientation += 0.02f;

  (*pFace).ex = mA3NN.mEstMidEyeX - 30.0 + mRefineA3NN.mEstMidEyeX;
  (*pFace).ey = mA3NN.mEstMidEyeY - 30.0 + mRefineA3NN.mEstMidEyeY;
  (*pFace).sz = mA3NN.mEstMidEyeMouthDist + mRefineA3NN.mEstMidEyeMouthDist - 12.5;
  (*pFace).or = mA3NN.mEstOrientation + mRefineA3NN.mEstOrientation;

  IlluminationGradient illGraident;
  Image8 temp(pFace->mFaceChip);
  illGraident.process(pFace->mFaceChip, temp);

  PvImageProc::EqualizeImage(pFace->mFaceChip);
  
//  Image8 temp(pFace->mFaceChip);
//  pFace->mFaceChip.Lequalize(temp, 12);
}

void 
FaceDetectClassifyMod::poseEstimateFace(Image8 grayFrame, FaceDataPtr pFace)
{
  int x = pFace->x;
  int y = pFace->y;
  int w = pFace->w;
  int h = pFace->h;  
  float eX = (2*x + w)/2 + (*pFace).ex - 30.0;
  float eY = (2*y + h)/2 + (*pFace).ey - 30.0;
  float sZ = ((*pFace).sz)*(w/(float)30.0);
  float oR = (*pFace).or;
  
  Image8 ypChip(30,30);
  mYPNN.estimateYawPitch(numYPModel, grayFrame, ypChip, eX, eY, sZ, oR);
  (*pFace).yw = mYPNN.mEstYaw;
  (*pFace).pt = mYPNN.mEstPitch;
  
  float poseWeight = cos(2*(*pFace).yw)*cos(2*(*pFace).pt);
}
  
void 
FaceDetectClassifyMod::classifyFace(Image8 grayFrame, FaceDataPtr pFace)
{
  mpVmsPacket->beginEventTime(initTime+time, 15/mFrameRate);

  Image8 faceChip;
  faceChip(30, 30);

  int x, y, w, h, eX, eY, sZ, oR;

  x = pFace->x;
  y = pFace->y;
  w = pFace->w;
  h = pFace->h;  
  eX = (2*x + w)/2 + (*pFace).ex - 30.0;
  eY = (2*y + h)/2 + (*pFace).ey - 30.0;
  sZ = ((*pFace).sz)*(w/(float)30.0);
  oR = (*pFace).or;


//  if(doPose)
//  {
//    Image8 ypChip(30,30);
//    mYPNN.estimateYawPitch(numYPModel, grayFrame, ypChip, eX, eY, sZ, oR);
//    (*pFace).yw = mYPNN.mEstYaw;
//    (*pFace).pt = mYPNN.mEstPitch;
//  }
//  else
//  {
//    (*pFace).yw = 0.0;
//    (*pFace).pt = 0.0;
//  }
//  
//  float poseWeight = cos(2*(*pFace).yw)*cos(2*(*pFace).pt);
  
  float genderScore;
  if(doGender)
  {
    genderScore = mGSvm.classify(grayFrame, faceChip, eX, eY, sZ, oR);
  }
  else
  {
    genderScore = 0.0;
  }
  
  (*pFace).iGender = 2*(genderScore>0)-1;
  string gender;
  
  if(genderScore < 0.0)
  {
    gender = "Female";
  }
  else
  {
    gender = "Male";
  }
  
  float afrScore, cauScore, hisScore, oriScore;
  
  if(doEthnicity)
  {
    afrScore = mAfrSvm.classify(faceChip);
    cauScore = mCauSvm.classify(faceChip);
    hisScore = mHisSvm.classify(faceChip);
    oriScore = mOriSvm.classify(faceChip);
  }
  else
  {
    afrScore = 0.0;
    cauScore = 0.0;
    hisScore = 0.0;
    oriScore = 0.0;
  }
  
  string ethnicity;
  
  if(afrScore >= cauScore && afrScore >= hisScore && afrScore >= oriScore)
  {
    ethnicity = "African";
    (*pFace).iEthnicity = 0;
  }
  if(cauScore >= afrScore && cauScore >= hisScore && cauScore >= oriScore)
  {
    ethnicity = "Caucasian";
    (*pFace).iEthnicity = 1;
  }
  if(hisScore >= afrScore && hisScore >= cauScore && hisScore >= oriScore)
  {
    ethnicity = "Hispanic";
    (*pFace).iEthnicity = 2;
  }
  if(oriScore >= afrScore && oriScore >= cauScore && oriScore >= hisScore)
  {
    ethnicity = "Oriental";
    (*pFace).iEthnicity = 3;
  }
  
  float ageScore;
  if(doAge)
  {
    ageScore = mAgeSvm.classify(faceChip);
  }
  else
  {
    ageScore = 0.0;
  }

  string age;
  
  if(ageScore < 18)
  {
    age = "Child";
    (*pFace).iAge = 0;
  }
  if(18 <= ageScore && ageScore < 30)
  {
    age = "Young Adult";
    (*pFace).iAge = 1;
  }
  if(30 <= ageScore && ageScore < 55)
  {
    age = "Adult";
    (*pFace).iAge = 2;
  }
  if(55 <= ageScore)
  {
    age = "Senior";
    (*pFace).iAge = 3;
  }

  (*pFace).mGenderScore = genderScore;
  (*pFace).mAfrScore = afrScore;
  (*pFace).mCauScore = cauScore;
  (*pFace).mHisScore = hisScore;
  (*pFace).mOriScore = oriScore;
  (*pFace).mAge = ageScore;
   
    if(doGender || doEthnicity || doAge)
      PVMSG("%f %f %f %f %f %f-> %s %s %s\n",
	    (*pFace).mGenderScore,(*pFace).mAfrScore,(*pFace).mCauScore,(*pFace).mHisScore,(*pFace).mOriScore, 
	    (*pFace).mAge, gender.c_str(),ethnicity.c_str(), age.c_str());

	if(doRecordLabelsToCSV)
    {
      fprintf(fpFaceXYWH,"%f,%f,%f,%f,%f,%f,%f,%s,%s,%s\n",time,
  	    (*pFace).mGenderScore,(*pFace).mAfrScore,(*pFace).mCauScore,(*pFace).mHisScore,(*pFace).mOriScore, 
	    (*pFace).mAge, gender.c_str(),ethnicity.c_str(), age.c_str()); 
    }

  if(doGender)
  {
    std::string attributeValue = "0";
    attributeValue = gender == "Female" ? "2" : "1";
    mpVmsPacket->addAttribute("1", attributeValue);
  }

  if (doEthnicity)
  {
    std::string attributeValue = "0"; // Unknown
    if (ethnicity == "African")
    {
      attributeValue = "1";
    }
    else if (ethnicity == "Caucasian")
    {
      attributeValue = "5";
    }
    else if (ethnicity == "Hispanic")
    {
      attributeValue = "6";
    }
    else if (ethnicity == "Oriental")
    {
      attributeValue = "3";
    }
    mpVmsPacket->addAttribute("3",attributeValue);
  }

  if (doAge)
  {
    std::string attributeValue = "0"; // Unknown
    if (age == "Child")
    {
      attributeValue = "6";
    }
    else if (age == "Young Adult")
    {
      attributeValue = "5";
    }
    else if (age == "Adult")
    {
      attributeValue = "3";
    }
    else if (age == "Senior")
    {
      attributeValue = "1";
    }
    mpVmsPacket->addAttribute("2",attributeValue);      
  }

  mpVmsPacket->endEvent();
}


void 
FaceDetectClassifyMod::visualize(Image32& img)
{
  img.zero();
  if (mpBackgroundImage.get())
  {
    img.paste(*mpBackgroundImage,0,0);
  }

//  Image32 img1(getCurrentFrame());
  Image32 img1;
  img1.crop(getCurrentFrame(),mRoi.x0,mRoi.y0,mRoi.width(),mRoi.height());

  std::vector<unsigned int> color;
  
  color.push_back(PV_RGB(255,0,0));
  color.push_back(PV_RGB(0,255,0));
  color.push_back(PV_RGB(0,0,255));
  
  color.push_back(PV_RGB(128,128,0));
  color.push_back(PV_RGB(128,0,128));
  color.push_back(PV_RGB(0,128,128));
  

  std::vector<FaceDataPtr>::iterator iFace;
  for (iFace = mFaces.begin(); iFace != mFaces.end(); iFace ++)
    {
        (*iFace)->draw(img1, mDemoVisualization, mVisualizeFacialFeatures, doPose);
    }

  img1.rescale(mFaceViewRect.width(), mFaceViewRect.height(), RESCALE_CUBIC);
  img.paste(img1,mFaceViewRect.x0,mFaceViewRect.y0);

//  img.paste(img1,mRoi.x0,mRoi.y0);

  if(showSkinToneWindow)
  {
    Image32 skinRGBTone;
	  PvImageConverter::convert(mSkinTone,skinRGBTone);
    img.paste(skinRGBTone, 0, mRoi.height());

  }
}

//
// ACCESS
//

//
// INQUIRY
//

}; // namespace vision

}; // namespace ait


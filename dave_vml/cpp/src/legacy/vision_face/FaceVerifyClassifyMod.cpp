/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#pragma warning(disable:4503)
#endif

#include "FaceVerifyClassifyMod.hpp"
#include <strutil.hpp>
#include "image_proc.hpp"
#include "PvImageProc.hpp"
#include "IlluminationGradient.hpp"
#include <VmsPacket.hpp>
#include <Socket.hpp>
#include <DateTime.hpp>
#include <ImageAcquireMod.hpp>
#include "face_detector/SkinToneMotionDetector.hpp"

#define RAD2DEG 57.29578

namespace ait 
{

namespace vision
{

FaceVerifyClassifyMod::FaceVerifyClassifyMod()
  : RealTimeMod(getModuleStaticName()),
  mRealTimeEventsPort(0)
{
}

FaceVerifyClassifyMod::~FaceVerifyClassifyMod()
{
}

void 
  FaceVerifyClassifyMod::initExecutionGraph(std::list<VisionModPtr>& srcModules)
{
  RealTimeMod::initExecutionGraph(srcModules);
  
  // Add any source module to the given list here.
  
 /*   getModule("ForegroundSegmentMod",mpForegroundSegmentMod);
    srcModules.push_back(mpForegroundSegmentMod);*/
}
  
void 
  FaceVerifyClassifyMod::init()
{
  RealTimeMod::init();

  mpVmsPacket.reset(new VmsPacket(this));
  
  mTrackId = 0;
  counter = 0;
  
  videoWidth = getImageAcquireModule()->getFrameSize().x;
  videoHeight = getImageAcquireModule()->getFrameSize().y;
  double videoFps = (int)getImageAcquireModule()->getFramesPerSecond();
  PVMSG("Input video size: %d %d %.02f\n",videoWidth,videoHeight,videoFps);

    // Set the region of interest
  mRoi = mSettings.getRecti("roiRect",Rectanglei(0,0,320,240));

  mDoSkinToneDetection = mSettings.getBool("doSkinToneDetection",false);
  mSkinRegionRadius = mSettings.getDouble("skinRegionRadius",1.0);
  mDoSkinToneMotionDetection = mSettings.getBool("doSkinToneMotionDetection",false);

  mUseFaceDetectorCombined = mSettings.getBool("useFaceDetectorCombined",0);
  if(mUseFaceDetectorCombined)
  {
	  // force the skin tone detection off when using combined face detector - we don't need combined detector when the skin tone works
 	  mDoSkinToneDetection = false;  
  }

  int minFaceSize = mSettings.getInt("minFaceSize",24);
  int maxFaceSize = mSettings.getInt("maxFaceSize",160);

  mUpscaleFactor = 1.0;
  if(minFaceSize < 24)
  {
	  mUpscaleFactor = 24.0 / minFaceSize;
	  maxFaceSize = int(maxFaceSize * mUpscaleFactor);
  }
  mSkinToneDownSampleFactor = 4.0/mUpscaleFactor;


  mFaceDetectScaleFactor = mSettings.getDouble("faceDetectScaleFactor",1.2);

  int minFaceClassificationSize = mSettings.getInt("minFaceClassificationSize",24);

  if (minFaceClassificationSize < minFaceSize)
  {
    minFaceClassificationSize = minFaceSize;
  }

  mMinFaceClassificationSize.set(minFaceClassificationSize,minFaceClassificationSize);
  
  doVisualizeHistory = mSettings.getInt("doVisualizeHistory",0);
  mVisualizeFacialFeatures = mSettings.getBool("visualizeFacialFeatures",0);
  mDemoVisualization = mSettings.getBool("demoVisualization",false);

  
  //  doMotionSegment = mSettings.getInt("doMotionSegment",0);
  //  doSkinSegment = mSettings.getInt("doSkinSegment",0);
  
  // Init face detector 

    if(mUseFaceDetectorCombined)
	{
		mpFrontCombinedFD.reset(new FaceDetectorCombined());
		mpFrontCombinedFD->init(Settings::replaceVars("<CommonFilesPath>/data/opencv/haarcascades/haarcascade_frontalface_default.xml")); 
  // Set the Max/Min face sizes: good for CC entrance videos
		mpFrontCombinedFD->setMinFaceSize(Vector2i(minFaceSize,minFaceSize));
		mpFrontCombinedFD->setMaxFaceSize(Vector2i(maxFaceSize,maxFaceSize));
	}
	else
	{
		//default openCV version
		mpFrontFD.reset(new FaceDetectorAdaBoostForeground());
		mpFrontFD->init(Settings::replaceVars("<CommonFilesPath>/data/opencv/haarcascades/haarcascade_frontalface_default.xml"), mFaceDetectScaleFactor); 
 
	  // Set the Max/Min face sizes: good for CC entrance videos
		mpFrontFD->setMinFaceSize(Vector2i(minFaceSize,minFaceSize));
		mpFrontFD->setMaxFaceSize(Vector2i(maxFaceSize,maxFaceSize));
	}

  mVideoFileString=mSettings.getString("videoFileString","input");
  
  doRecordLabelsToCSV = mSettings.getInt("doRecordLabelsToCSV",1);
  
  mCSVOutputPath=mSettings.getString("CSVOutputPath","");

  std::string a3nnModelList, a3nnModelDir, a3nnLsvFile;
  std::string refineA3nnModelList, refineA3nnModelDir, refineA3nnLsvFile;
  
  a3nnModelList=mSettings.getString("a3nnModelList","ModelList.txt");
  a3nnModelDir=mSettings.getString("a3nnModelDir","../x1_1000dim_sigma1.0");
  a3nnLsvFile=mSettings.getString("a3nnLsvFile","../x1_1000dim_sigma1.0/lsv.save");
  refineA3nnModelList=mSettings.getString("refineA3nnModelList","ModelList.txt");
  refineA3nnModelDir=mSettings.getString("refineA3nnModelDir","../x1_1000dim_sigma1.0");
  refineA3nnLsvFile=mSettings.getString("refineA3nnLsvFile","../x1_1000dim_sigma1.0/lsv.save");
  
  int numA3NNModel_ = mSettings.getInt("numEstA3NNModel",200);
  numA3NNModel = numA3NNModel_;
  int numRefineA3NNModel_ = mSettings.getInt("numEstRefineA3NNModel",0);
  numRefineA3NNModel = numRefineA3NNModel_;
  
  std::string mGSvmModelFile, mAfrSvmModelFile, mCauSvmModelFile, mHisSvmModelFile, mOriSvmModelFile, mAgeSvmModelFile;
  mGSvmModelFile = mSettings.getString("gSvmModelFile","./Train.model");
  mAfrSvmModelFile = mSettings.getString("afrSvmModelFile","./Train.model");
  mCauSvmModelFile = mSettings.getString("cauSvmModelFile","./Train.model");
  mHisSvmModelFile = mSettings.getString("hisSvmModelFile","./Train.model");
  mOriSvmModelFile = mSettings.getString("oriSvmModelFile","./Train.model");
  mAgeSvmModelFile = mSettings.getString("ageSvmModelFile","./Train.model");

  std::string mNFSvmModelFile;
  mNFSvmModelFile = mSettings.getString("nonFaceSvmModelFile","./Train.model");

  // Initializes Arclet3DNN face localization
  mA3NN.init(a3nnModelList, a3nnModelDir, a3nnLsvFile);
  mRefineA3NN.init(refineA3nnModelList,refineA3nnModelDir,refineA3nnLsvFile);

  // Initializes SparseYawPitchNN 3D facial pose
  int imgWidth = mSettings.getInt("imgWidth",60);
  int imgHeight = mSettings.getInt("imgHeight",60);
  
  std::string ypModelList, ypModelDir, ypLsvFile;
  
  ypModelList=mSettings.getString("ypModelList","ModelList.txt");
  ypModelDir=mSettings.getString("ypModelDir","../x1_1000dim_sigma1.0");
  ypLsvFile=mSettings.getString("ypLsvFile","../x1_1000dim_sigma1.0/lsv5000.save");
  int numYPModel_ = mSettings.getInt("nYPModel",9);
  numYPModel = numYPModel_;
  
  doPose = mSettings.getInt("doPose",0);
  if(doPose)
  {
    mYPNN.init(ypModelList, ypModelDir, ypLsvFile, imgWidth, imgHeight);
  }
  m3DFrontalYawThres = mSettings.getDouble("3DFrontalYawThres",20.0);
  m3DFrontalPitchThres = mSettings.getDouble("3DFrontalPitchThres",20.0);
  m3DFrontalYawThres = m3DFrontalYawThres/RAD2DEG;
  m3DFrontalPitchThres = m3DFrontalPitchThres/RAD2DEG;
  mMin3DFrontalFaceRatio = mSettings.getDouble("min3DFrontalFaceRatio",0.5);

  // Initializes TorchSVM classifiers
  doGender = mSettings.getInt("doGender",0);
  doAge = mSettings.getInt("doAge",0);
  doEthnicity = mSettings.getInt("doEthnicity",0);
  doNonFaceFilter = mSettings.getInt("doNonFaceFilter",0);
//  PVMSG("%d %d %d %d\n",doGender,doEthnicity,doPose,doAge);

  if(doGender)
  {
    mGSvm.init(mGSvmModelFile, 30, 30);
//    mGSvm.init("d:/Workspace/GenderRace/Arclet3DNN/Arclet3DNN_Model100L200_x5_1000dim_sigma1.0_wgt2_clean/30x30-Gender/NN/160-80-40-lr0.0075-e0.001-trainall/Train.model", 30, 30);
  }  
  
  if(doEthnicity)
  {
    mAfrSvm.init(mAfrSvmModelFile, 30, 30);
    mCauSvm.init(mCauSvmModelFile, 30, 30);
    mHisSvm.init(mHisSvmModelFile, 30, 30);
    mOriSvm.init(mOriSvmModelFile, 30, 30);
  }
  
  if(doAge)
  {
    PVMSG("Age file: %s\n",mAgeSvmModelFile.c_str()); 
    mAgeSvm.init(mAgeSvmModelFile, 30, 30);
  }  
  
  if(doNonFaceFilter)
  {
    PVMSG("NonFaceFilter file: %s\n",mNFSvmModelFile.c_str()); 
    mNonFaceSvm.init(mNFSvmModelFile, 30, 30);
  }


  showSkinToneWindow = mSettings.getInt("showSkinToneWindow",0);
  
  if(showSkinToneWindow)
  {
//    getImageAcquireModule()->setVisualizationFrameSize(Vector2i(videoWidth,2*videoHeight));
    getImageAcquireModule()->setVisualizationFrameSize(Vector2i(mRoi.width(),2*mRoi.height()));
  }
  
  // Initializes the frameSkipFactor to adjust the criteria (used to judge whether the track is true or spurious) for the frame rate
  float frameRate = (float)getImageAcquireModule()->getFramesPerSecond();
  frameSkipFactor = mSettings.getFloat("frameSkipFactor",1.0);
  PVMSG("frameRate = %f frameSkipFactor = %f\n",frameRate,frameSkipFactor);

  mRealTimeEventsPort = mSettings.getInt("RealTimeEvents/listenPort",0);
  mRealTimeMinTrackAge = mSettings.getDouble("RealTimeEvents/minTrackAge [sec]",2);
  mRealTimeMinTrackFaces = mSettings.getDouble("RealTimeEvents/minTrackFaces",1);
  mRealTimeMinTraveledDist =	mSettings.getDouble("RealTimeEvents/minTraveledDist", 10);
  mRealTimeMinTotalDisplacement = mSettings.getDouble("RealTimeEvents/minTotalDisplacement", 20);
  mRealTimeMinTotalSizeVariation = mSettings.getInt("RealTimeEvents/minTotalSizeVariation", 50);
  mRealTimeTrackLatency = mSettings.getDouble("RealTimeEvents/trackLatency", 15);
  mUseNormalizedDistanceValues = mSettings.getBool("RealTimeEvents/useNormalizedDistanceValues",false);


  mRealTimeMinTrackFaces = mRealTimeMinTrackFaces * (frameRate/15.0);
      
  std::string biName = mSettings.getString("backgroundImageName","");
  if (biName != "")
  {
    mpBackgroundImage.reset(new Image32());
    mpBackgroundImage->load(biName.c_str());
    getImageAcquireModule()->setVisualizationFrameSize(Vector2i(mpBackgroundImage->width(),mpBackgroundImage->height()));
  }

//  mFaceViewRect = mSettings.getRecti("faceViewRect",PvRecti(0,0,videoWidth,videoHeight));
  mFaceViewRect = mSettings.getRecti("faceViewRect",Rectanglei(0,0,mRoi.x1-mRoi.x0,mRoi.y1-mRoi.y0));

  mGenderUnknownRange = mSettings.getCoord2f("genderUnknownRange",Vector2f(0,0));

  mFaceMatchThreshold = mSettings.getDouble("faceMatchThreshold", 0.5);
  
  mNonFaceThres = mSettings.getDouble("nonFaceThres", 1.0);
  
  mFaceImagesOutputFolder = mSettings.getString("FaceImages/outputFolder","");
  mFaceImagesSendVmsAttribute = mSettings.getBool("FaceImages/sendVmsAttribute",false);
  mFaceImagesMaxFacesPerHour = mSettings.getInt("FaceImages/maxFacesPerHour",-1);
  mFaceImageQuality = mSettings.getFloat("FaceImages/imageQuality",0.5);

  mDoSegmentViewership = mSettings.getBool("doSegmentViewership",false);
  mViewershipLatency = mSettings.getFloat("viewershipLatency", 5);
  mClassifyAppearanceModelOnly = mSettings.getBool("classifyAppearanceModelOnly",true);
  PVMSG("Viewership & latency %d %f\n",mDoSegmentViewership,mViewershipLatency);

  //mFaceHistory.clear();

  mpVmsPacket->allowPause();
}

void
  FaceVerifyClassifyMod::finish()
{

  // Terminate tracks to send any events at the end of the video.
  std::vector<FaceTrackPtr>::iterator iTrack;
  for (iTrack = mTracks.begin(); iTrack != mTracks.end(); ++iTrack)
  {
    terminateTrack(*iTrack);
  }

  if(mUseFaceDetectorCombined)
  {
	mpFrontCombinedFD.reset();
  }
  else
  {
	mpFrontFD.reset();
  }

  //if(doRecordLabelsToCSV)
  //{
  //  fclose(fpFaceXYWH);
  //}
  
  mpVmsPacket->finish();

  RealTimeMod::finish();
}

//
// OPERATIONS
//


void 
FaceVerifyClassifyMod::process()
{
//  mFrameBinary = *(mpForegroundSegmentMod->getForeground());

  Image32 frame;
  frame.crop(getCurrentFrame(),mRoi.x0,mRoi.y0,mRoi.width(),mRoi.height());
 
  //Time for this frame
  if(counter==0)
  {
    initTime = getCurrentTime();
  }

  counter ++;
  
  time = getCurrentTime() - initTime;
  
//  correctColorShift(&frame);
  mSkinTone = Image8(frame.width(), frame.height());

//  Image8 mSkinTone(frame.width(), frame.height());
  if(mDoSkinToneDetection)
  {
  // Skin tone detector
    mSkinToneDetec.findSkinTone(&frame, &mSkinTone, mSkinRegionRadius, mSkinToneDownSampleFactor);

	if(mDoSkinToneMotionDetection && counter >= 2)
	{
		SkinToneMotionDetector skinToneMotionDetector;
		Image8 tmp(mSkinTone);
		skinToneMotionDetector.detectSkinToneMotion(&frame, &mPrevFrame, &tmp, &mSkinTone);
	}
  }
  else
  {
    mSkinTone.setAll(255);
  }
 
  mPrevFrame = frame;

  Image8 grayFrame;
  
  PvImageConverter::convert(frame,grayFrame);

//  PvImageProc::EqualizeImage(grayFrame);

  if(mUpscaleFactor!=1)
  {
	  Image8 doubleImage, doubleSkinTone;	
	  doubleImage.centerRotateRescale(grayFrame, 0, mUpscaleFactor, grayFrame.width()/2, grayFrame.height()/2, 
		  (int)(mUpscaleFactor*grayFrame.width()), (int)(mUpscaleFactor*grayFrame.height()));
	  doubleSkinTone.centerRotateRescale(mSkinTone, 0, mUpscaleFactor, grayFrame.width()/2, grayFrame.height()/2, 
		  (int)(mUpscaleFactor*grayFrame.width()), (int)(mUpscaleFactor*grayFrame.height()));

    if(mUseFaceDetectorCombined)
	{
		mpFrontCombinedFD->detect(doubleImage);
	}
	else
	{
	  mpFrontFD->detect(doubleImage, doubleSkinTone);
	}
  }
  else
  {
    if(mUseFaceDetectorCombined)
	{
		mpFrontCombinedFD->detect(grayFrame);
	}
	else
	{
	  mpFrontFD->detect(grayFrame, mSkinTone);
	}
 }

//  PVMSG("AdB: %d\n",mpFrontFD->nrOfFaces());
  
  //int iFD;
  faceImage = frame;
  
  std::vector<FaceDataPtr> faces;
  mLastFrameFaces.clear();
  int fId = 0;
  if(mUseFaceDetectorCombined)
  {
	  for (int i = 0; i < mpFrontCombinedFD->nrOfFaces(); i++)
	  {
		  Face f = mpFrontCombinedFD->face(i);
		  if (f.w >= mMinFaceClassificationSize.x && f.h >= mMinFaceClassificationSize.y)
		  {
			  // Adjustment of (x, y, w, h) from NN face detector to AdaBoost face detector
			  int ux = (int)((float)f.x/mUpscaleFactor - 4);
			  int uy = (int)((float)f.y/mUpscaleFactor - 6);
			  int uw = (int)((float)f.w/mUpscaleFactor + 8);
			  int uh = (int)((float)f.h/mUpscaleFactor + 8);
			  if(ux >= 0 && uy >= 0 && ux + uw < grayFrame.width() && uy + uh < grayFrame.height())
			  {
				FaceDataPtr pFace(new FaceData(fId++,0,time,ux,uy,uw,uh));
				localizeFace(grayFrame, pFace);

				if(doPose)
				{
				  poseEstimateFace(grayFrame, pFace);
				  if(pFace->is3DFrontal)
				  {
					  faces.push_back(pFace);
 //  	  				PVMSG("Detected face: %d %d %d %d at %f\n",f.x,f.y,f.w,f.h,time);
				  }
			  }
			  else
				{
				  faces.push_back(pFace);
				}
			  }
		  }
	      mLastFrameFaces.push_back(Rectanglei(f.x,f.y,f.x+f.w,f.y+f.h));
		  //    PVMSG("Detected face: %d %d %d %d at %f\n",f.x,f.y,f.w,f.h,time);
	  }
  }
  else
  {
	  for (int i = 0; i < mpFrontFD->nrOfFaces(); i++)
	  {
		  Face f = mpFrontFD->face(i);
		  if (f.w >= mMinFaceClassificationSize.x && f.h >= mMinFaceClassificationSize.y)
		 {
			  FaceDataPtr pFace(new FaceData(fId++,0,time,(int)((float)f.x/mUpscaleFactor),(int)((float)f.y/mUpscaleFactor),
				  (int)((float)f.w/mUpscaleFactor),(int)((float)f.h/mUpscaleFactor)));

			  localizeFace(grayFrame, pFace);

			  if(doPose)
			  {
				  poseEstimateFace(grayFrame, pFace);

//	  			  PVMSG("Detected face: %d %d %d %d pose %f %f ",f.x,f.y,f.w,f.h,RAD2DEG*(pFace->yw),RAD2DEG*(pFace->pt));
				  if(pFace->is3DFrontal)
				  {
					  faces.push_back(pFace);
//					  PVMSG(" --> Frontal!!");
				  }
			  }
			  else
			  {
				  faces.push_back(pFace);
			  }
//			  PVMSG("\n");
		  }
      mLastFrameFaces.push_back(Rectanglei(f.x,f.y,f.x+f.w,f.y+f.h));
	  //    PVMSG("Detected face: %d %d %d %d at %f\n",f.x,f.y,f.w,f.h,time);
	  }
  }    

  
  std::vector<FaceDataPtr>::iterator iFace; //, jFace;
  std::vector<FaceTrackPtr>::iterator iTrack;

  std::vector<FaceTrackScorePtr> faceTrackScoreTable;

  // Update existing tracks
  if (mTracks.size() > 0 && faces.size() > 0)
  {  
    int numTracks = mTracks.size();
    int numFaces = faces.size();
    
    // Fill in the table with {track, face} data, and compute the scores
    for (iFace = faces.begin(); iFace != faces.end(); iFace ++)
    {     
      //if((*iFace)->isFrontLeftRight == 0)
      //  localizeFace(grayFrame, *iFace);
      for (iTrack = mTracks.begin(); iTrack != mTracks.end(); iTrack++)
      {
        double score = evaluate(*iFace, *iTrack);
        FaceTrackScorePtr fTS(new FaceTrackScore(*iFace, *iTrack, score));
        faceTrackScoreTable.push_back(fTS);
        
//                  PVMSG("Track %d Face %d:%d %d %d %d %d score %f\n",(*iTrack)->getId(),(*iFace)->getId(),(*iFace)->isFrontLeftRight,(*iFace)->x, (*iFace)->y, (*iFace)->w, (*iFace)->h, score);
      }
    }
    
    // Sort the table according to the score
    std::sort(faceTrackScoreTable.begin(), faceTrackScoreTable.end(), cmpScores());
    
    // Process each {track, face} pair starting from the best matching ones 
    for(int i=0; i < numFaces*numTracks; i++)
    {
      FaceDataPtr fDP = (*faceTrackScoreTable[i]).getFacePtr();
      FaceTrackPtr fTP = (*faceTrackScoreTable[i]).getTrackPtr();
      float score = (*faceTrackScoreTable[i]).getScore();
      
      int faceIdx = (*fDP).getId();
      int trackIdx = (*fTP).getId();
      
      
      // Only assign those pairs that has both the track and the face not assigned previously
      // && the score above the threshold
      
      if((*faceTrackScoreTable[i]).isactive() && score >= mFaceMatchThreshold)
      {
        Image8Ptr pFaceImg(new Image8());                      
        pFaceImg->crop(grayFrame, (*fDP).x, (*fDP).y, (*fDP).w, (*fDP).h);             
        
//                PVMSG("Track %d Face %d isFrontLeftRight %d: %f %f %d %d %d %d \n", 
//                trackIdx,faceIdx, (*fDP).isFrontLeftRight, score, time, (*fDP).x, (*fDP).y, (*fDP).w, (*fDP).h);
        
        if((*fDP).isFrontLeftRight==0) 
        {
//          localizeFace(grayFrame, fDP);//, fTP);
          accumulateFaceChip(fDP, fTP);
          if(!mClassifyAppearanceModelOnly)
            classifyFace(grayFrame, fDP, fTP);
          if(0*doPose)
            poseEstimateFace(grayFrame, fDP);
        }
        
        int fIdx = (int)(i/numFaces);
        int tIdx = i%numFaces;
        
        (*fTP).addFace(frame,fDP);
//                  PVMSG("Track %d assigned to %d Face %d: %d %d %d %d\n",(*fTP).getId(),(*fDP).isFrontLeftRight,(*fDP).getId(), (*fDP).x, (*fDP).y, (*fDP).w, (*fDP).h);
        if((*fDP).isFrontLeftRight < 3)
        {
          (*fTP).lastAdBFace = (*fTP).numFaces()-1;
          (*fTP).numAdBFace ++;
        }
        
//                PVMSG("isAdB: %d numFace: %d lastAdBFace: %d lastAdBFaceTime = %f:%f\n",(*fDP).isFrontLeftRight,(*fTP).numFaces(),(*fTP).lastAdBFace,time,(*fTP).lastAdBFaceTime());
        
        // Remove both the track and the face from the table
        for(int tb=0; tb < numTracks*numFaces; tb ++)
        {
          FaceDataPtr tableFace = (*faceTrackScoreTable[tb]).getFacePtr();
          FaceTrackPtr tableTrack = (*faceTrackScoreTable[tb]).getTrackPtr();
          int tableFaceIdx = (*tableFace).getId();
          int tableTrackIdx = (*tableTrack).getId();
          if(tableFaceIdx==faceIdx || tableTrackIdx==trackIdx)   
          {
            (*faceTrackScoreTable[tb]).deactivate();
          }
        }
        
        iFace = std::find(faces.begin(), faces.end(), fDP);
        if(iFace!=faces.end())  
        {
          iFace = faces.erase(iFace);       
        }
      }            
    }
  } 

//Create new tracks
//  std::vector<FaceDataPtr>::iterator iFace;
  for (iFace = faces.begin(); iFace != faces.end(); iFace ++)
  {
    if((*iFace)->isFrontLeftRight == 0)
    {
      
      FaceTrackPtr pTrack(new FaceTrack(mTrackId)); //TODO: PUT SIZE VALUE HERE 
      pTrack->addFace(frame,*iFace); 
      mTracks.push_back(pTrack);
      
      Image8Ptr pFaceImg(new Image8());    
      pFaceImg->crop(grayFrame, (*iFace)->x, (*iFace)->y, (*iFace)->w, (*iFace)->h);   
      
      if((*iFace)->isFrontLeftRight==0)
      {
//        localizeFace(grayFrame, *iFace);
        accumulateFaceChip(*iFace, pTrack);
        if(!mClassifyAppearanceModelOnly)
          classifyFace(grayFrame, *iFace, pTrack);
        if(0*doPose)
          poseEstimateFace(grayFrame, *iFace);
      }
//            PVMSG("******** New Track %d Face %d isFrontLeftRight %d: %f %d %d %d %d\n", 
//                    pTrack->getId(), 0, (*iFace)->isFrontLeftRight, (*iFace)->time, (*iFace)->x, (*iFace)->y, (*iFace)->w, (*iFace)->h);
      mTrackId ++;
    }
  }

  //erase old tracks
  for (iTrack = mTracks.begin(); iTrack != mTracks.end();)
  {
    // If the track doesn't get a new (AdB detected) face for ? secs, delete it.
//    if ((*iTrack)->numAdBFace > 0 && time - (*iTrack)->lastAdBFaceTime() > 5.0 ||
//      time - (*iTrack)->lastFaceTime() > 5.0)
//    PVMSG("Track: %d isAdB: %d numFace: %d numAdBFace: %d lastAdBFace: %d lastAdBFaceTime = %f:%f\n",(*iTrack)->getId(),(*iTrack)->getLatestFace()->isFrontLeftRight,(*iTrack)->numFaces(),(*iTrack)->numAdBFace,(*iTrack)->lastAdBFace,time,(*iTrack)->lastAdBFaceTime());


	if(mClassifyAppearanceModelOnly && mDemoVisualization)// & (*iTrack)->numAdBFace >= 5)
    {
      FaceDataPtr dum = (*iTrack)->getLatestFace();
      classifyFace(grayFrame, dum, (*iTrack));
    }


    if (time - (*iTrack)->lastFaceTime() > mRealTimeTrackLatency)
    {
      terminateTrack(*iTrack, grayFrame);

      iTrack = mTracks.erase(iTrack);

    }
    else
    {
      iTrack++;
    }
  }

  // Generate real time classification events:
  if (mRealTimeEventsPort != 0)
  {
    for (iTrack = mTracks.begin(); iTrack != mTracks.end();++iTrack)
    {
      if (!(*iTrack)->realTimeDataSent && (*iTrack)->lastFaceTime() - (*iTrack)->firstFaceTime() >= mRealTimeMinTrackAge
        && (*iTrack)->numAdBFace >= mRealTimeMinTrackFaces 
        &&  (*iTrack)->getTraveledDist() >= mRealTimeMinTraveledDist 
        && (*iTrack)->getTotalDisplacement(mUseNormalizedDistanceValues) >= mRealTimeMinTotalDisplacement 
        && (*iTrack)->getSizeVariation(mUseNormalizedDistanceValues) >= mRealTimeMinTotalSizeVariation && (*iTrack)->m3DFrontalFaceRatio >= mMin3DFrontalFaceRatio && (*iTrack)->mNonFaceScore < mNonFaceThres)         
      {
        DateTime time(getCurrentTime());
        std::string evt = "<?xml version='1.0'?><event type='person_classified' id='1' timestamp='" + time.toString() + "' ";
        
        if (doGender)
        {
          evt += "gender='" + tolower((*iTrack)->mGenderString) + "' ";
        }
        if (doEthnicity)
        {
          evt += "ethnicity='" + tolower((*iTrack)->mEthnicityString) + "' ";
        }
        if (doAge)
        {
          evt += "age='" + tolower((*iTrack)->mAgeString) + "' ";
        }
        
        evt += "/>";
        
        PVMSG("Sending Real Time Event:\n");
        PVMSG("%s\n",evt.c_str());
        
        Socket s;
        s.open("localhost",(Uint)mRealTimeEventsPort,Socket::WRITE_ONLY|Socket::TCP);
        s.send(evt);
        s.close();
        
        (*iTrack)->realTimeDataSent = true;
      }
    }
  }

  if (!getImageAcquireModule()->isLiveVideoSource())
  {
    mpVmsPacket->updateProgress((double)getCurrentFrameNumber()/getImageAcquireModule()->getTotalNumberOfFrames());
  }

}

void
FaceVerifyClassifyMod::terminateTrack(FaceTrackPtr pTrack, Image8 grayFrame)
{
  float firstFaceTime = pTrack->firstFaceTime();
  float lastFaceTime = pTrack->lastFaceTime();
  float traveledDist = pTrack->getTraveledDist();
  float totalDisplacement = pTrack->getTotalDisplacement(mUseNormalizedDistanceValues);
  int numFaces = pTrack->numAdBFace;
  int sizeVariation = pTrack->getSizeVariation(mUseNormalizedDistanceValues);

  if(doNonFaceFilter)
  {  
	  Image8 faceChip(30, 30);
	  
	  for(int y=0; y<faceChip.height(); y++)
		  for(int x=0; x<faceChip.width();x++)
		  {
			  faceChip(x,y) = (unsigned char)(pTrack->mFaceChip(x,y)/pTrack->numInstances);
//        PVMSG("%f ",(*pFace).mFaceChip(x,y));
		  }
		  pTrack->mNonFaceScore = mNonFaceSvm.classify(faceChip);
  }
  else
  {
	  pTrack->mNonFaceScore = 0.0;
  }

  PVMSG("nonFaceScore = %f\n",pTrack->mNonFaceScore);

  	int num3DFrontalFaces;
	if(doPose)
	{
		num3DFrontalFaces = 0;
		for(int i = 0; i < numFaces; i++)
		{
			FaceDataPtr curFace = pTrack->getFace(i);
			if(curFace->is3DFrontal)
				num3DFrontalFaces ++;
//			PVMSG("%f %d\n",curFace->time, curFace->is3DFrontal);
		}
	}
	else
	{
		num3DFrontalFaces = numFaces;
	}

	pTrack->m3DFrontalFaceRatio = ((float)num3DFrontalFaces)/numFaces;

  float frontalFaceRatio = pTrack->m3DFrontalFaceRatio;
  float nonFaceScore = pTrack->mNonFaceScore;
  
  PVMSG("trackAge = %f traveledDist = %f totalDisplacement = %f numFaces = %d sizeVariation = %d frontalFaceRatio = %f\n",lastFaceTime - firstFaceTime, traveledDist, totalDisplacement, numFaces, sizeVariation, frontalFaceRatio);
  
  // Classify only the tracks that persists and shows certain amount of motion
  if(lastFaceTime - firstFaceTime >= mRealTimeMinTrackAge &&
    numFaces >= mRealTimeMinTrackFaces && traveledDist >= mRealTimeMinTraveledDist && 
	totalDisplacement >= mRealTimeMinTotalDisplacement && sizeVariation >= mRealTimeMinTotalSizeVariation &&  
	frontalFaceRatio>= mMin3DFrontalFaceRatio && nonFaceScore < mNonFaceThres)
  {

    if(mClassifyAppearanceModelOnly)
    {
      FaceDataPtr dum = pTrack->getLatestFace();
      classifyFace(grayFrame, dum, pTrack);
    }

    if(doGender)
    {
      if(pTrack->mGenderScore < 0.0)
      {
        pTrack->mGenderString = "Female";
      }
      else
      {
        pTrack->mGenderString = "Male";
      }
    }
    else
    {
      pTrack->mGenderString = "Unknown";
    }

    if(doEthnicity)
    {
      float afrScore = pTrack->mAfrScore;
      float cauScore = pTrack->mCauScore;
      float hisScore = pTrack->mHisScore;
      float oriScore = pTrack->mOriScore;

      if(afrScore >= cauScore && afrScore >= hisScore && afrScore >= oriScore)
      {
        pTrack->mEthnicityString = "African";
      }
      if(cauScore >= afrScore && cauScore >= hisScore && cauScore >= oriScore)
      {
        pTrack->mEthnicityString = "Caucasian";
      }
      if(hisScore >= afrScore && hisScore >= cauScore && hisScore >= oriScore)
      {
        pTrack->mEthnicityString = "Hispanic";
      }
      if(oriScore >= afrScore && oriScore >= cauScore && oriScore >= hisScore)
      {
        pTrack->mEthnicityString = "Oriental";
      }
    }
    else
      pTrack->mEthnicityString = "Unknown";


    if (doAge)
    {
      float ageScore = pTrack->mAge;

      if(ageScore < 18)
      {
        pTrack->mAgeString = "Child";
      }
      if(18 <= ageScore && ageScore < 30)
      {
        pTrack->mAgeString = "Young Adult";
      }
      if(30 <= ageScore && ageScore < 55)
      {
        pTrack->mAgeString = "Adult";
      }
      if(55 <= ageScore)
      {
        pTrack->mAgeString = "Senior";
      }
    }
    else
      pTrack->mAgeString = "Unknown";

	double timeDiff = std::max(pTrack->lastFaceTime()-pTrack->firstFaceTime(), 0.001);

    mpVmsPacket->beginEventTime(initTime+pTrack->firstFaceTime(),
      timeDiff);
    
    if (doGender)
    {
		pTrack->mGenderAttribute = "0";
      if (pTrack->mGenderScore < mGenderUnknownRange(0) ||
        pTrack->mGenderScore > mGenderUnknownRange(1))
      {
        pTrack->mGenderAttribute = pTrack->mGenderString == "Female" ? "2" : "1";
      }
      mpVmsPacket->addAttribute("1", pTrack->mGenderAttribute);
    }
    
    if (doEthnicity)
    {
      pTrack->mEthnicityAttribute = "0"; // Unknown
      if (pTrack->mEthnicityString == "African")
      {
        pTrack->mEthnicityAttribute = "1";
      }
      else if (pTrack->mEthnicityString == "Caucasian")
      {
        pTrack->mEthnicityAttribute = "5";
      }
      else if (pTrack->mEthnicityString == "Hispanic")
      {
        pTrack->mEthnicityAttribute = "6";
      }
      else if (pTrack->mEthnicityString == "Oriental")
      {
        pTrack->mEthnicityAttribute = "3";
      }
      mpVmsPacket->addAttribute("3",pTrack->mEthnicityAttribute);
    }

    if (doAge)
    {
      pTrack->mAgeAttribute = "0"; // Unknown
      if (pTrack->mAgeString == "Child")
      {
        pTrack->mAgeAttribute = "6";
      }
      else if (pTrack->mAgeString == "Young Adult")
      {
        pTrack->mAgeAttribute = "5";
      }
      else if (pTrack->mAgeString == "Adult")
      {
        pTrack->mAgeAttribute = "3";
      }
      else if (pTrack->mAgeString == "Senior")
      {
        pTrack->mAgeAttribute = "1";
      }
      mpVmsPacket->addAttribute("2",pTrack->mAgeAttribute);      
    }

//    if (mDoSegmentViewership)
//    {
//      pTrack->segmentViewership(mViewershipLatency);
//
//      std::string segmentAttribute = ait::aitSprintf("%f",pTrack->mViewershipTime);
//      mpVmsPacket->addAttribute("7", segmentAttribute);
//    }
    
    if (doNonFaceFilter)
    {
	  std::string segmentAttribute = ait::aitSprintf("%f",pTrack->mNonFaceScore);
      mpVmsPacket->addAttribute("7", segmentAttribute);
    }
    

    std::string imageFname = "";

    if (mFaceImagesOutputFolder != "")
    {
      double start = pTrack->firstFaceTime() + initTime;
      DateTime dt(start);
      imageFname = combine_path(mFaceImagesOutputFolder,"face_" + dt.format(DateTime::fileFormatStr) + "_" + dt.format("%f") + ".jpg");
    }

    bool isTempFile = false;
    if (mFaceImagesSendVmsAttribute && imageFname == "")
    {
      imageFname = File::getTempFileName();
      isTempFile = true;
    }

    if (imageFname != "")
    {
      pTrack->saveFaceImages(imageFname,mFaceImageQuality);
//      pTrack->saveFaceChip(imageFname,mFaceImageQuality);

      if (mFaceImagesSendVmsAttribute)
      {
        std::vector<unsigned char> imageData;
        File::readBinaryFileToMemory(imageFname,imageData);
        mpVmsPacket->addBinaryDataAttribute("5",&(imageData[0]),imageData.size());
      }
    }

    try
    {
      if (isTempFile)
      {
        File::deleteFile(imageFname);
      }
    }
    catch (File::write_error ex) 
    {
      // Just ignore.
    }


    PVMSG("Closing track %d from %f to %f with score %f: based on %d faces of traveled dist %f --> %f %f %f %f %f %f -> %s %s %s\n", 
		pTrack->getId(), pTrack->firstFaceTime(), pTrack->lastFaceTime(), nonFaceScore, numFaces, traveledDist,
      pTrack->mGenderScore,pTrack->mAfrScore,pTrack->mCauScore,pTrack->mHisScore, pTrack->mOriScore, pTrack->mAge,
      pTrack->mGenderString.c_str(),pTrack->mEthnicityString.c_str(), pTrack->mAgeString.c_str());
    
    if(doRecordLabelsToCSV)
    {
    // This only works for real time events:
		double start = pTrack->firstFaceTime() + initTime;
		double end = pTrack->lastFaceTime() + initTime;

	    DateTime startDt(start);
		 std::string date = startDt.format("%Y%m%d_%H0000_000");
	    std::string fname = aitSprintf("Face_events_%s.csv",date.c_str());
		fname = ait::combine_path(mCSVOutputPath,fname);

    bool fileExists = PvUtil::fileExists(fname.c_str());
    FILE *fp = fopen(fname.c_str(),"a");
    if (fp)
	{
      if (!fileExists)
      {
		PVMSG("Creating csv file: %s\n",fname.c_str());
//		fprintf(fp,"Start Time,Duration,Gender,Age Group,Ethnicity,NumFaces,FaceTrack\n");
		fprintf(fp,"Start Time,Duration,GenderScore,Age,AfrScore,CauScore,HisScore,OriScore,NumFaces,FaceTrack\n");
      }

  //    fprintf(fp,"%s,%.03f,%s,%s,%s,%d,\"",
  //      startDt.format(DateTime::vmsFormatStr).c_str(),
		//end - start,
		//pTrack->mGenderAttribute.c_str(),
  //      pTrack->mAgeAttribute.c_str(),
  //      pTrack->mEthnicityAttribute.c_str(),
		//pTrack->numAdBFace);
      fprintf(fp,"%s,%.03f,%f,%f,%f,%f,%f,%f,%d,\"",
        startDt.format(DateTime::vmsFormatStr).c_str(),
		end - start,
		pTrack->mGenderScore,
        pTrack->mAge,
		pTrack->mAfrScore,
		pTrack->mCauScore,
		pTrack->mHisScore,
		pTrack->mOriScore,
		pTrack->numAdBFace);
	  for(int i=0; i<pTrack->numAdBFace; i++)
	  {
		  FaceDataPtr curFace = pTrack->getFace(i);
		  fprintf(fp,"%f,%d,%d,%d,",curFace->time - pTrack->firstFaceTime(), curFace->x,curFace->y,curFace->w);
	  }
	  fprintf(fp,"\"\n");

      fclose(fp);
    }

      //fprintf(fpFaceXYWH,"%f,%f,%f,%d,%d,%f,%f,%f,%f,%f,%f,%f,%f,%s,%s,%s\n",time,
      //  pTrack->firstFaceTime(),pTrack->lastAdBFaceTime(),pTrack->getId(),numFaces, pTrack->mViewershipTime, pTrack->m3DFrontalFaceRatio,
      //  pTrack->mGenderScore,
      //  pTrack->mAfrScore,pTrack->mCauScore,pTrack->mHisScore,pTrack->mOriScore,
      //  pTrack->mAge,
      //  pTrack->mGenderString.c_str(),pTrack->mEthnicityString.c_str(),pTrack->mAgeString.c_str()); 

//	  for(int i=0; i<numFaces; i++)
//	  {
//		FaceDataPtr curFace = pTrack->getFace(i);
//	    Vector2i faceCenter((int)(curFace->x + 0.5*curFace->w), (int)(curFace->y + 0.5*curFace->h));
//		mFaceHistory.push_back(faceCenter);
////        fprintf(fpFaceXYWH,"Face,%d,%d,%d,%d\n",curFace->x,curFace->y,curFace->w,curFace->h);
//	  }

    }
		mpVmsPacket->endEvent();
  }
  else
  {
    PVMSG("Track %d was not valid\n",pTrack->getId());
  }
}

void 
FaceVerifyClassifyMod::localizeFace(Image8 grayFrame, FaceDataPtr pFace)
{
  int x = pFace->x;
  int y = pFace->y;
  int w = pFace->w;
  int h = pFace->h;
  
  mA3NN.estimateModel(numA3NNModel, 2, 2, grayFrame, x, y, w, h);

  // Correct the bias introduced by the models
  mA3NN.mEstMidEyeMouthDist -= 0.5f;
  mA3NN.mEstOrientation += 0.024f;
  

//      Image8 a3NNChip(30,30);
  
  mRefineA3NN.estimateRefined(numRefineA3NNModel, grayFrame, pFace->mFaceChip, 
  (2*x+w)/2 + mA3NN.mEstMidEyeX - 30.0,(2*y+h)/2 + mA3NN.mEstMidEyeY - 30.0,
  mA3NN.mEstMidEyeMouthDist*(w/30.0),mA3NN.mEstOrientation);

  // Correct the bias introduced by the models
  mRefineA3NN.mEstMidEyeMouthDist -= 0.5f;
  mRefineA3NN.mEstOrientation += 0.02f;

  (*pFace).ex = mA3NN.mEstMidEyeX - 30.0 + mRefineA3NN.mEstMidEyeX;
  (*pFace).ey = mA3NN.mEstMidEyeY - 30.0 + mRefineA3NN.mEstMidEyeY;
  (*pFace).sz = mA3NN.mEstMidEyeMouthDist + mRefineA3NN.mEstMidEyeMouthDist - 12.5;
  (*pFace).or = mA3NN.mEstOrientation + mRefineA3NN.mEstOrientation;

  IlluminationGradient illGraident;
  Image8 temp(pFace->mFaceChip);
  illGraident.process(pFace->mFaceChip, temp);

  PvImageProc::EqualizeImage(pFace->mFaceChip);
  
//  Image8 temp(pFace->mFaceChip);
//  pFace->mFaceChip.Lequalize(temp, 12);
}

void 
FaceVerifyClassifyMod::localizeFace(Image8 grayFrame, FaceDataPtr pFace, FaceTrackPtr pTrack)
{
  FaceDataPtr lastFace = (*pTrack).getLatestFace();
  
  int x = (2*pFace->x + lastFace->x)/3;
  int y = (2*pFace->y + lastFace->y)/3;
  int w = (pFace->w + lastFace->w)/2;
  int h = (pFace->h + lastFace->h)/2;
  
  pFace->x = x;
  pFace->y = y;
  pFace->w = w;
  pFace->h = h;
  
  mA3NN.estimateModel(numA3NNModel, 2, 2, grayFrame, x, y, w, h);

//  (*pFace).ex = mA3NN.mEstMidEyeX;
//  (*pFace).ey = mA3NN.mEstMidEyeY;
//  (*pFace).sz = mA3NN.mEstMidEyeMouthDist;
//  (*pFace).or = mA3NN.mEstOrientation;

  Image8 a3NNChip(30,30);
  mRefineA3NN.estimateRefined(50, grayFrame, a3NNChip, 
    (2*x+w)/2 + mA3NN.mEstMidEyeX - 30.0,(2*y+h)/2 + mA3NN.mEstMidEyeY - 30.0,
    mA3NN.mEstMidEyeMouthDist*(w/30.0),mA3NN.mEstOrientation);

  (*pFace).ex = mA3NN.mEstMidEyeX - 30.0 + mRefineA3NN.mEstMidEyeX;
  (*pFace).ey = mA3NN.mEstMidEyeY - 30.0 + mRefineA3NN.mEstMidEyeY;
  (*pFace).sz = mA3NN.mEstMidEyeMouthDist - 12.5 + mRefineA3NN.mEstMidEyeMouthDist;
  (*pFace).or = mA3NN.mEstOrientation + mRefineA3NN.mEstOrientation;
}

void 
FaceVerifyClassifyMod::poseEstimateFace(Image8 grayFrame, FaceDataPtr pFace)
{
  int x = pFace->x;
  int y = pFace->y;
  int w = pFace->w;
  int h = pFace->h;  
  float eX = (2*x + w)/2 + (*pFace).ex - 30.0;
  float eY = (2*y + h)/2 + (*pFace).ey - 30.0;
  float sZ = ((*pFace).sz)*(w/(float)30.0);
  float oR = (*pFace).or;
  
  Image8 ypChip(30,30);
  mYPNN.estimateYawPitch(numYPModel, grayFrame, ypChip, eX, eY, sZ, oR);
  (*pFace).yw = mYPNN.mEstYaw;
  (*pFace).pt = mYPNN.mEstPitch;
  
  float poseWeight = cos(2*(*pFace).yw)*cos(2*(*pFace).pt);

//  PVMSG("%f < %f ? %f < %f ",RAD2DEG*(*pFace).yw,RAD2DEG*m3DFrontalYawThres,RAD2DEG*(*pFace).pt,RAD2DEG*m3DFrontalPitchThres);

  if( fabs((*pFace).yw) < m3DFrontalYawThres && fabs((*pFace).pt) < m3DFrontalPitchThres)
  {
	  (*pFace).is3DFrontal = 1;
  }
  else
  {	
	  (*pFace).is3DFrontal = 0;
  }
}
  
void 
FaceVerifyClassifyMod::classifyFace(Image8 grayFrame, FaceDataPtr pFace, FaceTrackPtr pTrack)
{
//  (*pTrack).numInstances ++;

  Image8 faceChip(30, 30);
  
  if(mClassifyAppearanceModelOnly && (*pTrack).numInstances > 1)
  {
    for(int y=0; y<faceChip.height(); y++)
      for(int x=0; x<faceChip.width();x++)
      {
        faceChip(x,y) = (unsigned char)((*pTrack).mFaceChip(x,y)/(*pTrack).numInstances);
//        PVMSG("%f ",(*pFace).mFaceChip(x,y));
       }

     Image8 ttImage(faceChip);
     faceChip.Lequalize(ttImage,12);
  }
  
  int x, y, w, h, eX, eY, sZ, oR;
  if(!mClassifyAppearanceModelOnly)
  {
    x = pFace->x;
    y = pFace->y;
    w = pFace->w;
    h = pFace->h;  
    eX = (2*x + w)/2 + (*pFace).ex - 30.0;
    eY = (2*y + h)/2 + (*pFace).ey - 30.0;
    sZ = ((*pFace).sz)*(w/(float)30.0);
    oR = (*pFace).or;
  }

//  if(doPose)
//  {
//    Image8 ypChip(30,30);
//    mYPNN.estimateYawPitch(numYPModel, grayFrame, ypChip, eX, eY, sZ, oR);
//    (*pFace).yw = mYPNN.mEstYaw;
//    (*pFace).pt = mYPNN.mEstPitch;
//  }
//  else
//  {
//    (*pFace).yw = 0.0;
//    (*pFace).pt = 0.0;
//  }
//  
//  float poseWeight = cos(2*(*pFace).yw)*cos(2*(*pFace).pt);
  
  float genderScore;
  if(doGender)
  {
    if(mClassifyAppearanceModelOnly)
      genderScore = mGSvm.classify(faceChip);
    else
      genderScore = mGSvm.classify(grayFrame, faceChip, eX, eY, sZ, oR);
  }
  else
  {
    genderScore = 0.0;
  }
  
  (*pFace).iGender = 2*(genderScore>0)-1;
  string gender;
  
  if(genderScore < 0.0)
  {
    gender = "Female";
  }
  else
  {
    gender = "Male";
  }
  
  float afrScore, cauScore, hisScore, oriScore;
  
  if(doEthnicity)
  {
    afrScore = mAfrSvm.classify(faceChip);
    cauScore = mCauSvm.classify(faceChip);
    hisScore = mHisSvm.classify(faceChip);
    oriScore = mOriSvm.classify(faceChip);
  }
  else
  {
    afrScore = 0.0;
    cauScore = 0.0;
    hisScore = 0.0;
    oriScore = 0.0;
  }
  
  string ethnicity;
  
  if(afrScore >= cauScore && afrScore >= hisScore && afrScore >= oriScore)
  {
    ethnicity = "African";
    (*pFace).iEthnicity = 0;
  }
  if(cauScore >= afrScore && cauScore >= hisScore && cauScore >= oriScore)
  {
    ethnicity = "Caucasian";
    (*pFace).iEthnicity = 1;
  }
  if(hisScore >= afrScore && hisScore >= cauScore && hisScore >= oriScore)
  {
    ethnicity = "Hispanic";
    (*pFace).iEthnicity = 2;
  }
  if(oriScore >= afrScore && oriScore >= cauScore && oriScore >= hisScore)
  {
    ethnicity = "Oriental";
    (*pFace).iEthnicity = 3;
  }
  
  float ageScore;
  if(doAge)
  {
    ageScore = mAgeSvm.classify(faceChip);
  }
  else
  {
    ageScore = 0.0;
  }

  string age;
  
  if(ageScore < 18)
  {
    age = "Child";
    (*pFace).iAge = 0;
  }
  if(18 <= ageScore && ageScore < 30)
  {
    age = "Young Adult";
    (*pFace).iAge = 1;
  }
  if(30 <= ageScore && ageScore < 55)
  {
    age = "Adult";
    (*pFace).iAge = 2;
  }
  if(55 <= ageScore)
  {
    age = "Senior";
    (*pFace).iAge = 3;
  }

  if(mClassifyAppearanceModelOnly)
  {
	  (*pTrack).mGenderScore = genderScore;
	  (*pTrack).mAfrScore = afrScore;
	  (*pTrack).mCauScore = cauScore;
	  (*pTrack).mHisScore = hisScore;
	  (*pTrack).mOriScore = oriScore;
	  (*pTrack).mAgeSum = ageScore;
  }
  else
  {
	  (*pFace).mGenderScore = genderScore;
	  (*pFace).mAfrScore = afrScore;
	  (*pFace).mCauScore = cauScore;
	  (*pFace).mHisScore = hisScore;
	  (*pFace).mOriScore = oriScore;
	  (*pFace).mAge = ageScore;
	   
	  (*pTrack).mGenderScore += (*pFace).mGenderScore;
	  (*pTrack).mAfrScore += (*pFace).mAfrScore;
	  (*pTrack).mCauScore += (*pFace).mCauScore;
	  (*pTrack).mHisScore += (*pFace).mHisScore;
	  (*pTrack).mOriScore += (*pFace).mOriScore;
	  (*pTrack).mAgeSum += (*pFace).mAge;
  }

  if(mClassifyAppearanceModelOnly)
  {
    (*pTrack).mAge = (*pTrack).mAgeSum;
  }
  else
  {
    (*pTrack).mAge = (*pTrack).mAgeSum/(*pTrack).numInstances;
  }
    //if((*pFace).isFrontLeftRight==0 || doGender || doEthnicity || doAge)
    //  PVMSG("%f:%f %f:%f %f:%f %f:%f %f:%f %f-> %s %s %f\n",
    //  (*pFace).mGenderScore,(*pTrack).mGenderScore,
    //  (*pFace).mAfrScore,(*pTrack).mAfrScore,
    //  (*pFace).mCauScore,(*pTrack).mCauScore,
    //  (*pFace).mHisScore,(*pTrack).mHisScore,
    //  (*pFace).mOriScore,(*pTrack).mOriScore, (*pFace).mAge,
    //  gender.c_str(),ethnicity.c_str(), (*pTrack).mAge);
}

void 
FaceVerifyClassifyMod::accumulateFaceChip(FaceDataPtr pFace, FaceTrackPtr pTrack)
{
  (*pTrack).numInstances ++;

  for(int y=0; y<(*pFace).mFaceChip.height(); y++)
    for(int x=0; x<(*pFace).mFaceChip.width();x++)
      (*pTrack).mFaceChip(x,y) += (*pFace).mFaceChip(x,y);
      
//      (*pTrack).mFaceChip.add((*pFace).mFaceChip); 
//  (*pTrack).mFaceChip.mult(0.5);
}

double 
FaceVerifyClassifyMod::evaluate(FaceDataPtr pFace, FaceTrackPtr pTrack)
{
  // Positinal constraint
  Vector2f pos1;
  pos1.x = pTrack->getLatestFace()->x;
  pos1.y = pTrack->getLatestFace()->y;
  Double time1;
  time1 = pTrack->getLatestFace()->time;
  
  Vector2f pos2(pFace->x, pFace->y);
  double time2 = pFace->time;
  
  double dist = sqrt(dist2(pos1,pos2));
  dist /= pTrack->getLatestFace()->w;
  
  // Time constraint
  double timeLapse = time2 - time1;
  
  
  // Match between the current detection window size and the predicted window size
  
//  float predSize = pTrack->getPredictedSize(pFace->time);
  float predSize = pTrack->getLatestFace()->w;
  float currSize = pFace->w;
  double sizeMatch = fabs(predSize-currSize);
  
  float templateScore = 0.0;
  if(pFace->isFrontLeftRight == 0)
  {
    for(int y=0; y<pTrack->mFaceChip.height(); y++)
      for(int x=0; x<pTrack->mFaceChip.width();x++)
      {
        float diff = fabs(pTrack->mFaceChip(x,y)/pTrack->numInstances - pFace->mFaceChip(x,y));
        templateScore += diff;
      }
  }

  double appearanceScore, geometryScore, totalScore;
  geometryScore = 0.75/(dist + 0.35*timeLapse + 0.1*sizeMatch + 1);
  appearanceScore = 12500.0/(templateScore*0.64);
  totalScore = geometryScore + appearanceScore;

//  PVMSG("*** templateScore = %f appearanceScore = %f geometryScore = %f totalScore = %f\n",templateScore, appearanceScore, geometryScore, totalScore);

//      PVMSG("dist=%f timeLapse=%f pw=%f w=%f sizeMatch=%f predDist=%f matchScore=%f\n",dist,timeLapse,predSize,currSize,sizeMatch,predDist,matchScore);
  
  return totalScore;
}

void 
FaceVerifyClassifyMod::visualize(Image32& img)
{
  img.zero();
  if (mpBackgroundImage.get())
  {
    img.paste(*mpBackgroundImage,0,0);
  }

//  Image32 img1(getCurrentFrame());
  Image32 img1;
  img1.crop(getCurrentFrame(),mRoi.x0,mRoi.y0,mRoi.width(),mRoi.height());

  //Image8 imgg;
  //PvImageConverter::convert(img1,imgg);
  //PvImageProc::EqualizeImage(imgg);
  //PvImageConverter::convert(imgg, img1);

  std::vector<unsigned int> color;
  
  color.push_back(PV_RGB(255,0,0));
  color.push_back(PV_RGB(0,255,0));
  color.push_back(PV_RGB(0,0,255));
  
  color.push_back(PV_RGB(128,128,0));
  color.push_back(PV_RGB(128,0,128));
  color.push_back(PV_RGB(0,128,128));
  

    std::vector<FaceTrackPtr>::const_iterator iTrack;
    for (iTrack = mTracks.begin(); iTrack != mTracks.end(); iTrack++)
    {
//		if(time - (*iTrack)->lastFaceTime() == 0)
      if ((*iTrack)->lastFaceTime() - (*iTrack)->firstFaceTime() >= mRealTimeMinTrackAge &&
        (*iTrack)->numAdBFace >= mRealTimeMinTrackFaces 
        &&  (*iTrack)->getTraveledDist() >= mRealTimeMinTraveledDist 
        && (*iTrack)->getTotalDisplacement(mUseNormalizedDistanceValues) >= mRealTimeMinTotalDisplacement 
        && (*iTrack)->getSizeVariation(mUseNormalizedDistanceValues) >= mRealTimeMinTotalSizeVariation && (*iTrack)->m3DFrontalFaceRatio >= mMin3DFrontalFaceRatio)
      {
//          PVMSG("%f - %f = %f %d %f %f\n",(*iTrack)->lastFaceTime(),(*iTrack)->firstFaceTime(),(*iTrack)->lastFaceTime()-(*iTrack)->firstFaceTime(), (*iTrack)->numFaces(), 
//                (float)(*iTrack)->getTraveledDist(), (float)(*iTrack)->getTotalDisplacement(mUseNormalizedDistanceValues));
        (*iTrack)->draw(img1, color[(*iTrack)->getId() % color.size()], doVisualizeHistory, mDemoVisualization, mVisualizeFacialFeatures, doPose);
      }
    }

  //for(int i=0; i<mFaceHistory.size(); i++)
  //{
	 // img1.circle(mFaceHistory[i].x, mFaceHistory[i].y, 0.5, PV_RGB(0, 255, 0));
  //}


  img1.rescale(mFaceViewRect.width(), mFaceViewRect.height(), RESCALE_CUBIC);
  img.paste(img1,mFaceViewRect.x0,mFaceViewRect.y0);

//  img.paste(img1,mRoi.x0,mRoi.y0);

  if(showSkinToneWindow)
  {
    Image32 skinRGBTone;
//    PvImageConverter::convert(mSkinTone,skinRGBTone);
//    img.paste(skinRGBTone, 0, mRoi.height());
	//for(int j=0; j<mSkinTone.rows(); j++)
	//	for(int i=0; i<mSkinTone.cols(); i++)
	//	{
	//		if(mSkinTone(i,j) && mFrameBinary(i,j))
	//			mFrameBinary(i,j) = 127;
	//	}

	PvImageConverter::convert(mSkinTone,skinRGBTone);
	img.paste(skinRGBTone, 0, mRoi.height());
  }
}

//
// ACCESS
//

//
// INQUIRY
//

}; // namespace vision

}; // namespace ait


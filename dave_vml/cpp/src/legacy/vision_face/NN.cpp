#include "PvUtil.hpp"
#include "NN.hpp"

void NN::allocateMemory()
{
  // Need to implement the memory allocation of mNodes, mWeights, mInputMeans, and mInputVars

  numHiddenNodes.reserve(numHiddenLayers);
}

bool NN::isBlank(unsigned char ch)
{
  return ch == ' ' || ch == '\n' || ch == '\t' || ch == '\r';
}

float NN::readFloat(std::string& stringData, int& curCharIndex)
{
  int beginIndex = curCharIndex;
  while (!isBlank(stringData[curCharIndex]) && stringData[curCharIndex] != '\0')
  {
    curCharIndex++;
  }

  char lastVal = stringData[curCharIndex];
  stringData[curCharIndex] = '\0';
  // atof calls strlen() internally, if we don't put this
  // end of string character, it will take ages!
  float f = atof(&stringData[beginIndex]); //printf("%s ",stringData[beginIndex]);
  stringData[curCharIndex] = lastVal;

  // Go to next non blank (or eof)
  while (isBlank(stringData[curCharIndex]))
  {
    curCharIndex++;
  }

  return f;
}

void NN::loadFromMemory(std::string& stringData)
{
  using namespace std;

  string linebuffer;

  // Magic header.
  string magic("AI_NN1.1");

  int linenumber;
  int curCharIndex = 0;
  for (linenumber = 1;; linenumber++)
  {
    int endLineIndex = curCharIndex;
    while (stringData[endLineIndex] != '\n' && 
      stringData[endLineIndex] != '\0')
    {
      endLineIndex++;
    }
    linebuffer = stringData.substr(curCharIndex,endLineIndex-curCharIndex+1);
    curCharIndex = endLineIndex+1;

    if (linenumber == 1)
    {
      string str = linebuffer;
      if (str.substr(0,magic.length()) != magic)
      {
        PvUtil::exitError("Invalid NN File format!");
      }
      continue;
    }

    // Skip comments.
    if (linebuffer[0] == '#') continue;
  
    vector<string> tokens = ait::tokenize(linebuffer,"=");

    //printf("line %d: %d tokens, curCharIndex=%d",linenumber,tokens.size(),curCharIndex);
    //for(int i=0; i<tokens.size(); i++)
    //  printf(" token %d=%s ",i,tokens[i].c_str());
    //printf("\n");
    
    // Skip empty lines.
    if (tokens.empty() || tokens.size() == 1) continue;

    if (tokens.size() == 3)
    {
      // Strip spaces
      tokens[0] = ait::strip(tokens[0]);

            // Skip empty lines with only spaces.
      if (tokens[0].empty()) continue;

      // The only valid single token is the "SvmVectors:" tag.
      if (tokens[0] == "NNWeights")
      {
//        printf("%s!!!\n",tokens[0].c_str());
        // Get out of the loop and read the svm vectors.
        break;
      }
      PvUtil::exitError("NN::load() Syntax error in line %d\n",linenumber);
    }
    else if (tokens.size() == 2)
    {
      // Strip spaces
      tokens[0] = ait::strip(tokens[0]);
      tokens[1] = ait::strip(tokens[1]);

      if (tokens[0] == "Title")
      {
        // Ignore for now.
      }
      else if (tokens[0] == "NumHiddenLayers")
      {
        numHiddenLayers = atoi(tokens[1].c_str());
      }
      else if (tokens[0] == "NumInputNodes") 
      {
        numInputNodes = atoi(tokens[1].c_str());
      }
      else if (tokens[0] == "NumOutputNodes")
      {
        numOutputNodes = atoi(tokens[1].c_str());
      }
      else if (tokens[0] == "NumHiddenNodes") 
      {
        vector<string> subtokens = ait::tokenize(tokens[1]," ");

//        for(int i=0; i<subtokens.size(); i++)
//          printf(" %d -> subtoken %d=%s ",subtokens.size(), i,subtokens[i].c_str());
//        printf("\n");
        
        for(int i=0; i<subtokens.size(); i++)
        {
          numHiddenNodes.push_back(atoi(subtokens[i].c_str()));
          //printf("%d\n",numHiddenNodes[i]);
        }
      }
      else if (tokens[0] == "NonlinearType")
      {
        mNonlinearType = atoi(tokens[1].c_str());
      }
      else
      {
        PvUtil::exitError("NN::load() Invalid param in line %d\n",
          linenumber);
      }
    }
    else
    {
      PvUtil::exitError("NN::load() Syntax error in line %d\n",
        linenumber);
    }
  }

  numLayers = numHiddenLayers + 2;
  numNodes.reserve(numLayers);
  numNodes.push_back(numInputNodes);
  for(int l = 1; l < numHiddenLayers + 1; l++)
    {
      numNodes.push_back(numHiddenNodes[l-1]);
    } 
  numNodes.push_back(numOutputNodes);


  //for(int l=0; l < numLayers; l++)
  //  printf("%d --> %d\n",l,numNodes[l]);

  allocateMemory();

  //static int idx = 0; FILE *fp = fopen(ait::aitSprintf("c:\\out%i.txt",idx++).c_str(),"w");

  std::vector< std::vector< double > > longvec;
  std::vector< double > shortvec, shortvec1;
  float read;

  // Read the NN weights from this point.
  for(int l = 1; l < numLayers; l++)
  {
    longvec.clear();
    shortvec1.clear();
    for(int n = 0; n < numNodes[l]; n++)
    {
      shortvec.clear();
  
      for(int m = 0; m < numNodes[l-1]; m++)
	    {
        read = readFloat(stringData,curCharIndex);
        shortvec.push_back(read);
	    }
      longvec.push_back(shortvec);
        
      read = readFloat(stringData,curCharIndex);
      shortvec1.push_back(read);
    }
    mWeights.push_back(longvec);
    mBiases.push_back(shortvec1);
  }

  //printf("mWeights.size = %d\n",mWeights.size());

  for(int j = 0; j < numInputNodes;j++)
  {
    mInputMeans.push_back(readFloat(stringData,curCharIndex));
  }

  for(int j = 0; j < numInputNodes;j++)
  {
    mInputVars.push_back(readFloat(stringData,curCharIndex));
  }

//  for(int l = 1; l < numLayers; l++)
//  {
//    printf("mWeights[%d].size = %d\n",l-1,mWeights[l-1].size());
//    for(int n = 0; n < numNodes[l]; n++)
//    {
//      for(int m = 0; m < numNodes[l-1]; m++)
//	    {
//        printf("%f ",mWeights[l-1][n][m]);
//	    }
//      printf(": %f\n",mBiases[l-1][n]);
//    }
//    printf("\n");
//  }
//
//  for(int j = 0; j < numInputNodes;j++)
//    printf("%f ",mInputMeans[j]);
//  printf("\n");
//
//  for(int j = 0; j < numInputNodes;j++)
//    printf("%f ",mInputVars[j]);
//  printf("\n");
}


void NN::readModel(void)
{
  std::string stringData;
  ait::TextCodec::decodeFromFile(mModelFileName, stringData); //printf("%s\n",mModelFileName.c_str());
  loadFromMemory(stringData);
}

void NN::init(std::string modelFileName_)
{
  mModelFileName = modelFileName_;

  readModel();
}


void NN::normalizeInputs(std::vector< double > input)
{
  std::vector< double > inputRow;

  for(int i=0; i<numInputNodes; i++)
    inputRow.push_back((input[i] - mInputMeans[i])/mInputVars[i]);
//    inputRow.push_back(input[i]);

  mNodes.push_back(inputRow);
}

void NN::feedForward(std::vector< double > input)
{
  mNodes.clear();
  normalizeInputs(input);

  std::vector< double > inputRow;

  for(int l=1; l<numLayers; l++)
  {
    inputRow.clear();
    for(int n=0; n<numNodes[l]; n++)
      {
//        printf("numNodes[%d] = %d, numNodes[%d] = %d\n",l-1,numNodes[l-1],l,numNodes[l]);

        double	sum = 0.0, out;
	      for(int m=0; m<numNodes[l-1]; m++)
	        {
	          sum += mWeights[l-1][n][m] * mNodes[l-1][m];
//              printf("%f = %f * %f\n",mWeights[l-1][n][m] * mNodes[l-1][m],mWeights[l-1][n][m], mNodes[l-1][m]);
	        }
//        printf("\n");
        out = nonlinear(sum + mBiases[l-1][n]);
        inputRow.push_back(out);
      }
    mNodes.push_back(inputRow);
  }
}

double NN::nonlinear(double x)
{
//  return 1./(1.+ exp(-x));
  return tanh(x);
}

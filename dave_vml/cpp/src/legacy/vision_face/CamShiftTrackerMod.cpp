/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#pragma warning(disable:4503)
#endif

#include "CamShiftTrackerMod.hpp"
#include <PvImageConverter.hpp>
#include <strutil.hpp>
#include "image_proc.hpp"
#include "PvImageProc.hpp"

namespace ait 
{

namespace vision
{

CamShiftTrackerMod::CamShiftTrackerMod()
: RealTimeMod(getModuleStaticName())
{
}

CamShiftTrackerMod::~CamShiftTrackerMod()
{
}

void 
CamShiftTrackerMod::initExecutionGraph(std::list<VisionModPtr>& srcModules)
{
  RealTimeMod::initExecutionGraph(srcModules);
  
  // Add any source module to the given list here.
  // VisionModPtr pMod;  
  // getModule("SampleMod",pMod); 
  // srcModules.push_back(pMod);
  getModule("ForegroundSegmentMod",mpForegroundSegmentMod);
  srcModules.push_back(mpForegroundSegmentMod);
}

void 
CamShiftTrackerMod::init()
{
  RealTimeMod::init();

  mTrackId = 0;
  counter = 0;
  // 
  mFrontFD.init(Settings::replaceVars("c:/ai/common/data/opencv/haarcascades/haarcascade_frontalface_default_20stages.xml")); 
//  mFrontFD.setMinFaceSize(mSettings.getCoord2i("minFaceSize",Vector2i(24,24)));
//	mFrontFD.setMaxFaceSize(mSettings.getCoord2i("maxFaceSize",Vector2i(36,36)));
  mFrontFD.setMinFaceSize(mSettings.getCoord2i("minFaceSize",Vector2i(40,40)));
	mFrontFD.setMaxFaceSize(mSettings.getCoord2i("maxFaceSize",Vector2i(120,120)));

//  mLeftFD.init(Settings::replaceVars("c:/ai/common/data/opencv/haarcascades/haarcascade_profileface_24stages.xml")); 
//  mRightFD.init(Settings::replaceVars("c:/ai/common/data/opencv/haarcascades/haarcascade_profileface_flip_24stages.xml")); 
//  
//  mLeftFD.setMinFaceSize(mSettings.getCoord2i("minFaceSize",Vector2i(24,24)));
//	mLeftFD.setMaxFaceSize(mSettings.getCoord2i("maxFaceSize",Vector2i(36,36)));
//
//  mRightFD.setMinFaceSize(mSettings.getCoord2i("minFaceSize",Vector2i(24,24)));
//	mRightFD.setMaxFaceSize(mSettings.getCoord2i("maxFaceSize",Vector2i(36,36)));

  Settings args;
  args.parseXml("./settings.xml");

  videoFileString=args.getString("VideoFileString","input");

  string cameraOutputPath;
  cameraOutputPath=args.getString("Installation/CameraOutputPath","");
  faceXYWHFile=cameraOutputPath;
  faceXYWHFile.append(videoFileString);
  faceXYWHFile.append(".csv");
  fpFaceXYWH = fopen(faceXYWHFile.c_str(), "w");
//  PVMSG("%s\n",faceXYWHFile.c_str());
  
  std::string a3nnModelList, a3nnModelDir, ypModelFile;

  a3nnModelList=args.getString("a3nnModelList","A3NNModelList.txt");
  a3nnModelDir=args.getString("a3nnModelDir","../x1_1000dim_sigma1.0");

  int numEstA3NNModel_ = args.getInt("numEstA3NNModel",200);
  numEstA3NNModel = numEstA3NNModel_;

  ypModelFile=args.getString("ypModelFile","");
  
  std::string gSvmModelFile, afrSvmModelFile, cauSvmModelFile, hisSvmModelFile;
  gSvmModelFile = args.getString("gSvmModelFile","./Train.model");
  afrSvmModelFile = args.getString("afrSvmModelFile","./Train.model");
  cauSvmModelFile = args.getString("cauSvmModelFile","./Train.model");
  hisSvmModelFile = args.getString("hisSvmModelFile","./Train.model");

  a3nn.init(a3nnModelList, a3nnModelDir);
//	ypnn.init(ypModelFile, 60, 60); 

  gSvm.init(gSvmModelFile, 30, 30);
  afrSvm.init(afrSvmModelFile, 30, 30);
  cauSvm.init(cauSvmModelFile, 30, 30);
  hisSvm.init(hisSvmModelFile, 30, 30);

//  cvNamedWindow ("Skin Tone Image", CV_WINDOW_AUTOSIZE );
}

void
CamShiftTrackerMod::finish()
{
  cvDestroyWindow ("Skin Tone Image");
  RealTimeMod::finish();
}

//
// OPERATIONS
//

void 
CamShiftTrackerMod::process()
{
  Image32& frame = getCurrentFrame();
  faceImage = frame;

//  Foreground segmentation
  mFrameBinary.resize(frame.width(),frame.height());
  mFrameBinary = *(mpForegroundSegmentMod->getForeground());

  skinTone = Image8(frame.width(), frame.height());
  skinToneDetec.findSkinTone(&frame, &skinTone);

  for(int y = 0; y < skinTone.height(); y++)
    for(int x = 0; x < skinTone.width(); x++)
      if(mFrameBinary(x,y)>0)
        skinTone(x,y) = skinTone(x,y);
      else
        skinTone(x,y) = 0;
    
//	cvShowImage ("Skin Tone Image", skinToneDetec.skinRegion);

  Image8 grayFrame;

  PvImageConverter imageConverter;
  imageConverter.convert(frame,grayFrame);

//  PvImageProc imageProc;
//  imageProc.EqualizeImage(grayFrame);

//  ait::image_proc::smoothImage(grayFrame, ait::image_proc::SMOOTH_GAUSSIAN, 3);

//  mFrontFD.detect(grayFrame);
  mFrontFD.detect(grayFrame, skinTone);
//  mLeftFD.detect(grayFrame);
//  mRightFD.detect(grayFrame);
 
//  std::cout << "AdB: "<<mFrontFD.nrOfFaces()<<std::endl;
	
  int iFD;
  faceImage = frame;

  CvRect faceRect;
  std::vector<FaceDataPtr> faces;
  int fId = 0;
  for (int i = 0; i < mFrontFD.nrOfFaces(); i++)
  {
    Face f = mFrontFD.face(i);
    FaceDataPtr pFace(new FaceData(fId++,0,time,f.x,f.y,f.w,f.h));
    faces.push_back(pFace);
  }  
//  for (int i = 0; i < mLeftFD.nrOfFaces(); i++)
//  {
//    Face f = mLeftFD.face(i);
//    FaceDataPtr pFace(new FaceData(fId++,1,time,f.x,f.y,f.w,f.h));
//    if(mFrontFD.nrOfFaces()==0)
//      faces.push_back(pFace);
//  }  
//  for (int i = 0; i < mRightFD.nrOfFaces(); i++)
//  {
//    Face f = mRightFD.face(i);
//    FaceDataPtr pFace(new FaceData(fId++,2,time,f.x,f.y,f.w,f.h));
//    if(mFrontFD.nrOfFaces()==0 && mLeftFD.nrOfFaces()==0)
//      faces.push_back(pFace);
//  }  
  
  std::vector<CamShiftTrackerPtr>::iterator iCamShift;
  for (iCamShift = mCamShifts.begin(); iCamShift != mCamShifts.end(); iCamShift++)
  {
    int x = (*iCamShift)->tracRect.x;
    int y = (*iCamShift)->tracRect.y;
    int w = (*iCamShift)->tracRect.width;
    int h = (*iCamShift)->tracRect.height;

    if( x + w < frame.width() && y + h < frame.height())
    {
      FaceDataPtr pFace(new FaceData(fId++,3,time,x,y,w,h));
      faces.push_back(pFace);
    }
  }
  
  //Time for this frame
  if(counter==0)
    initTime = getCurrentTime();

  counter ++;

  time = getCurrentTime() - initTime;

//  getImageAcquireModule()->getCurrentFrameNumber();

  //  PVMSG("Faces in store after detection %d\n",faces.size());

  // The tracks-faces table that keeps track of {track, face} scores and assigments
  std::vector<FaceTrackScorePtr> faceTrackScoreTable;

  // Update existing tracks
  if (mTracks.size() > 0 && faces.size() > 0)
  {  
    int numTracks = mTracks.size();
    int numFaces = faces.size();
 
    std::vector<FaceDataPtr>::iterator iFace;

    // Fill in the table with {track, face} data, and compute the scores
    for (iFace = faces.begin(); iFace != faces.end(); iFace ++)
    {           
      std::vector<FaceTrackPtr>::iterator iTrack;
      for (iTrack = mTracks.begin(); iTrack != mTracks.end(); iTrack++)
      {
        double score = evaluate(*iFace, *iTrack);
        FaceTrackScorePtr fTS(new FaceTrackScore(*iFace, *iTrack, score));
        faceTrackScoreTable.push_back(fTS);

//        PVMSG("Track %d Face %d score %f\n",(*iTrack)->getId(),(*iFace)->getId(),score);
      }
    }

    // Sort the table according to the score
    std::sort(faceTrackScoreTable.begin(), faceTrackScoreTable.end(), cmpScores());

    // Process each {track, face} pair starting from the best matching ones 
    for(int i=0; i < numFaces*numTracks; i++)
    {
      FaceDataPtr fDP = (*faceTrackScoreTable[i]).getFacePtr();
      FaceTrackPtr fTP = (*faceTrackScoreTable[i]).getTrackPtr();
      float score = (*faceTrackScoreTable[i]).getScore();

      int faceIdx = (*fDP).getId();
      int trackIdx = (*fTP).getId();
          
         
      // Only assign those pairs that has both the track and the face not assigned previously
      // && the score above the threshold

      if((*faceTrackScoreTable[i]).isActive() && score >= 0.25)
      {
        Image8Ptr pFaceImg(new Image8());                      
        pFaceImg->crop(grayFrame, (*fDP).x, (*fDP).y, (*fDP).w, (*fDP).h);             
  
//        PVMSG("Track %d Face %d isFrontLeftRight %d: %f %f %d %d %d %d \n", 
//        trackIdx,faceIdx, (*fDP).isFrontLeftRight, score, time, (*fDP).x, (*fDP).y, (*fDP).w, (*fDP).h);

        localizeFace(grayFrame, fDP);
        classifyFace(grayFrame, fDP, fTP);

        int fIdx = (int)(i/numFaces);
        int tIdx = i%numFaces;
        
        (*fTP).addFace(fDP);
//        PVMSG("Track %d assigned to Face %d\n",(*fTP).getId(),(*fDP).getId());
        if((*fDP).isFrontLeftRight < 3)
        {
          (*fTP).lastAdBFace = (*fTP).numFaces();
          (*fTP).numAdBFace ++;
        }
           
//        printf("isAdB: %d numFace: %d lastAdBFace: %d\n",(*fDP).isFrontLeftRight,(*fTP).numFaces(),(*fTP).lastAdBFace);

        // Remove both the track and the face from the table
        for(int tb=0; tb < numTracks*numFaces; tb ++)
        {
          FaceDataPtr tableFace = (*faceTrackScoreTable[tb]).getFacePtr();
          FaceTrackPtr tableTrack = (*faceTrackScoreTable[tb]).getTrackPtr();
          int tableFaceIdx = (*tableFace).getId();
          int tableTrackIdx = (*tableTrack).getId();
          if(tableFaceIdx==faceIdx || tableTrackIdx==trackIdx)   
            (*faceTrackScoreTable[tb]).deactivate();
        }
        
        iFace = std::find(faces.begin(), faces.end(), fDP);
        if(iFace!=faces.end())  
        iFace = faces.erase(iFace);       
       }            
     }
   } 
 
  
  //Create new tracks
  std::vector<FaceDataPtr>::iterator iFace;
  for (iFace = faces.begin(); iFace != faces.end(); iFace ++)
  {
    if((*iFace)->isFrontLeftRight < 3)
    {
    
      FaceTrackPtr pTrack(new FaceTrack(mTrackId));
      pTrack->addFace(*iFace);
      mTracks.push_back(pTrack);   
    
      Image8Ptr pFaceImg(new Image8());    
      pFaceImg->crop(grayFrame, (*iFace)->x, (*iFace)->y, (*iFace)->w, (*iFace)->h);   

      localizeFace(grayFrame, *iFace);
      classifyFace(grayFrame, *iFace, pTrack);

//      PVMSG("New Track %d Face %d isFrontLeftRight %d: %f %d %d %d %d\n", 
//              pTrack->getId(), 0, (*iFace)->isFrontLeftRight, time, (*iFace)->x, (*iFace)->y, (*iFace)->w, (*iFace)->h);

      CamShiftTrackerPtr pCamShift(new CamShiftTracker(mTrackId));
      pCamShift->Calibrate(2);
      mCamShifts.push_back(pCamShift);

      mTrackId ++;
    }
  }

  std::vector<FaceTrackPtr>::iterator iTrack;
  for (iTrack = mTracks.begin(), iCamShift = mCamShifts.begin(); iTrack != mTracks.end(); iTrack++, iCamShift++)
  {
    FaceDataPtr lastFace = (*iTrack)->getLatestFace();
    int x = lastFace->x;
    int y = lastFace->y;
    int w = lastFace->w;
    int h = lastFace->h;
  //    CvRect bodyRect = cvRect(0, 0, 320, 240); // Background
//    CvRect bodyRect = cvRect(x - w/2, y + 1.5*h, 2*w, 2*h); // Upper body
    CvRect bodyRect = cvRect(x+0.1*w, y+0.1*h, 0.8*w, 0.8*h); // Face
    if(bodyRect.x + bodyRect.width < frame.width() && bodyRect.y + bodyRect.height < frame.height())
    {
    
    (*iCamShift)->Track(&frame, bodyRect);
//    printf("CamShifts %d:%d %d %d %d %d --> %d %d %d %d\n",(*iCamShift)->getId(),(*iCamShift)->numTracking,x,y,w,h,(*iCamShift)->tracRect.x, (*iCamShift)->tracRect.y, (*iCamShift)->tracRect.width, (*iCamShift)->tracRect.height);
    }
  }

  //erase old tracks
  for (iTrack = mTracks.begin(), iCamShift = mCamShifts.begin(); iTrack != mTracks.end();)
  {
    if((*iTrack)->genderScore < 0.0)
      (*iTrack)->genderString = "Female";
    else
      (*iTrack)->genderString = "Male";

    float afrScore = (*iTrack)->afrScore;
    float cauScore = (*iTrack)->cauScore;
    float hisScore = (*iTrack)->hisScore;

    if(afrScore >= cauScore && afrScore >= hisScore)
    {
      (*iTrack)->ethnicityString = "African";
    }
    if(cauScore >= afrScore && cauScore >= hisScore)
    {
      (*iTrack)->ethnicityString = "Caucasian";
    }
    if(hisScore >= afrScore && hisScore >= cauScore)
    {
      (*iTrack)->ethnicityString = "Hispanic";
    }
      
    // Kill a dormant track
    if (time - (*iTrack)->lastFaceTime() > 5.0)
    {
      float traveledDist = (*iTrack)->getTraveledDist();
      int numFaces = (*iTrack)->numAdBFace;

      if(traveledDist/numFaces > 0.1 && numFaces >= 2 && (*iTrack)->getTotalDisplacement() > 20.0)
      {
        PVMSG("Closing track %d at %f: based on %d faces of traveled dist %f --> %f %f %f %f -> %s %s\n", 
          (*iTrack)->getId(), time, numFaces, traveledDist/numFaces,
          (*iTrack)->genderScore,(*iTrack)->afrScore,(*iTrack)->cauScore,(*iTrack)->hisScore,
          (*iTrack)->genderString.c_str(),(*iTrack)->ethnicityString.c_str());
      }
      iTrack = mTracks.erase(iTrack);
      iCamShift = mCamShifts.erase(iCamShift);
    }
    else
    {
      iTrack++;
      iCamShift++;
    }
  }  
}

void 
CamShiftTrackerMod::localizeFace(Image8 grayFrame, FaceDataPtr pFace)
{
  int x = pFace->x;
  int y = pFace->y;
  int w = pFace->w;
  int h = pFace->h;
  
  a3nn.estimateModel(numEstA3NNModel, 2, 2, grayFrame, x, y, w, h);

//  float eX = (2*x + w)/2 + a3nn.estMidEyeX - 30.0;
//  float eY = (2*y + h)/2 + a3nn.estMidEyeY - 30.0;
//  float sZ = (a3nn.estMidEyeMouthDist)*(w/(float)gSvm.imgWidth);
//  float oR = a3nn.estOrientation;
//
//  (*pFace).ex = eX;
//  (*pFace).ey = eY;
//  (*pFace).sz = sZ;
//  (*pFace).or = oR;

  (*pFace).ex = a3nn.estMidEyeX;
  (*pFace).ey = a3nn.estMidEyeY;
  (*pFace).sz = a3nn.estMidEyeMouthDist;
  (*pFace).or = a3nn.estOrientation;
}

void 
CamShiftTrackerMod::classifyFace(Image8 grayFrame, FaceDataPtr pFace, FaceTrackPtr pTrack)
{
	Image8 faceChip(30, 30);

  int x = pFace->x;
  int y = pFace->y;
  int w = pFace->w;
  int h = pFace->h;  
  float eX = (2*x + w)/2 + (*pFace).ex - 30.0;
  float eY = (2*y + h)/2 + (*pFace).ey - 30.0;
  float sZ = ((*pFace).sz)*(w/(float)gSvm.imgWidth);
  float oR = (*pFace).or;
  
//  float eX = (*pFace).ex;
//  float eY = (*pFace).ey;
//  float sZ = (*pFace).sz;
//  float oR = (*pFace).or;

//  Image8 ypChip(30,30);
//  ypnn.estimateYawPitch(grayFrame, ypChip, eX, eY, sZ, oR);

  (*pFace).yw = ypnn.estYaw;
  (*pFace).pt = ypnn.estPitch;
      
  float genderScore;
  if((*pFace).isFrontLeftRight==0)
    genderScore = gSvm.classify(grayFrame, faceChip, eX, eY, sZ, oR);
  else
    genderScore = 0.0;
  (*pFace).iGender = 2*(genderScore>0)-1;
  string gender;
      
  if(genderScore < 0.0)
    gender = "Female";
  else
    gender = "Male";

  float afrScore, cauScore, hisScore;

  if(0 && (*pFace).isFrontLeftRight==0)
  {
    afrScore = afrSvm.classify(faceChip);
    cauScore = cauSvm.classify(faceChip);
    hisScore = hisSvm.classify(faceChip);
  }
  else
  {
    afrScore = 0.0;
    cauScore = 0.0;
    hisScore = 0.0;
  }

  string ethnicity;

  if(afrScore >= cauScore && afrScore >= hisScore)
  {
    ethnicity = "African";
    (*pFace).iEthnicity = 0;
  }
  if(cauScore >= afrScore && cauScore >= hisScore)
  {
    ethnicity = "Caucasian";
    (*pFace).iEthnicity = 1;
  }
  if(hisScore >= afrScore && hisScore >= cauScore)
  {
    ethnicity = "Hispanic";
    (*pFace).iEthnicity = 2;
  }
      
  (*pFace).genderScore = genderScore;
  (*pFace).afrScore = afrScore;
  (*pFace).cauScore = cauScore;
  (*pFace).hisScore = hisScore;

  (*pTrack).genderScore += (*pFace).genderScore;
  (*pTrack).afrScore += (*pFace).afrScore;
  (*pTrack).cauScore += (*pFace).cauScore;
  (*pTrack).hisScore += (*pFace).hisScore;

//  PVMSG("%f %f %f %f %f %f -> %f:%s %s\n",
//    (*pFace).ex,(*pFace).ey,(*pFace).sz,(*pFace).or,
//    (*pFace).yw,(*pFace).pt,genderScore,gender.c_str(),ethnicity.c_str());

  PVMSG("%f:%f %f:%f %f:%f %f:%f -> %s %s\n",
    (*pFace).genderScore,(*pTrack).genderScore,
    (*pFace).afrScore,(*pTrack).afrScore,
    (*pFace).cauScore,(*pTrack).cauScore,
    (*pFace).hisScore,(*pTrack).hisScore,
    gender.c_str(),ethnicity.c_str());

//  fprintf(fpFaceXYWH, "%f %f %f %f %f %f -> %f:%s %s\n",
//    a3nn.estMidEyeX,a3nn.estMidEyeY,a3nn.estMidEyeMouthDist,a3nn.estOrientation,
//    ypnn.estYaw,ypnn.estPitch,genderScore,gender.c_str(),ethnicity.c_str());
//  fprintf(fpFaceXYWH, "%f %f %f %f -> %f:%s %s\n",
//    a3nn.estMidEyeX,a3nn.estMidEyeY,a3nn.estMidEyeMouthDist,a3nn.estOrientation,
//    genderScore,gender.c_str(),ethnicity.c_str());

}

double 
CamShiftTrackerMod::evaluate(FaceDataPtr pFace, FaceTrackPtr pTrack)
{
  // Positinal constraint
  Vector2f pos1;
  pos1.x = pTrack->getLatestFace()->x;
  pos1.y = pTrack->getLatestFace()->y;
  Double time1;
  time1 = pTrack->getLatestFace()->time;

  Vector2f pos2(pFace->x, pFace->y);
  double time2 = pFace->time;

  double dist = sqrt(dist2(pos1,pos2));
  dist /= pTrack->getLatestFace()->w;

  // Time constraint
  double timeLapse = time2 - time1;


  // Match between the current detection window size and the predicted window size

  float predSize = pTrack->getPredictedSize(pFace->time);
  float currSize = pFace->w;
  double sizeMatch = fabs(predSize-currSize);
  
  Vector2f predPos = pTrack->getPredictedPos(pFace->time);
  double predDist = sqrt(dist2(predPos,pos2));

  double matchScore = 1.0/(dist + 0.5*timeLapse + 0.025*sizeMatch + 0.05*predDist + 1);

  if(pFace->isFrontLeftRight == 3)
    matchScore *= 0.5;

//  PVMSG("dist=%f timeLapse=%f pw=%f w=%f sizeMatch=%f predDist=%f matchScore=%f\n",dist,timeLapse,predSize,currSize,sizeMatch,predDist,matchScore);

  return matchScore;
}

void 
CamShiftTrackerMod::visualize(Image32& img)
{
// Mask out non-motion and/or non-skin region
//  for(int y = 0; y < skinTone.height(); y++)
//    for(int x = 0; x < skinTone.width(); x++)
//      if(skinTone(x,y)==0)
//        img(x,y) = PV_RGB(0,0,0);

  std::vector<unsigned int> color;

  color.push_back(PV_RGB(255,0,0));
  color.push_back(PV_RGB(0,255,0));
  color.push_back(PV_RGB(0,0,255));

  color.push_back(PV_RGB(128,128,0));
  color.push_back(PV_RGB(128,0,128));
  color.push_back(PV_RGB(0,128,128));

  std::vector<FaceTrackPtr>::const_iterator iTrack;
  std::vector<CamShiftTrackerPtr>::iterator iCamShift;
  for (iTrack = mTracks.begin(), iCamShift = mCamShifts.begin(); iTrack != mTracks.end(); iTrack++, iCamShift++)
  {
// Visualization for a live video
    if((*iTrack)->numFaces() >= 2.0 && 
      (*iTrack)->getTraveledDist() / (*iTrack)->numFaces() > 0.025 &&
      (*iTrack)->getTotalDisplacement() > 5.0)
//    if((*iTrack)->numAdBFace >= 2.0 && 
//      (*iTrack)->getTraveledDist() / (*iTrack)->numFaces() > 0.1 &&
//      (*iTrack)->getTotalDisplacement() > 20.0)
    {
      printf("%d %f %f\n",(*iTrack)->numFaces(), 
        (float)(*iTrack)->getTraveledDist() / (*iTrack)->numFaces(), (float)(*iTrack)->getTotalDisplacement());
      (*iTrack)->draw(img, color[(*iTrack)->getId() % color.size()]);
    }

//      int currentTrack = (*iTrack)->getId();
//      int x, y, w, h;
//      x = (*iCamShift)->tracRect.x;
//      y = (*iCamShift)->tracRect.y;
//      w = (*iCamShift)->tracRect.width;
//      h = (*iCamShift)->tracRect.height;
//
//  //    printf("%d: %d --> %d %d %d %d\n", currentTrack, (*iCamShift)->numTracking,
//  //      (*iCamShift)->tracRect.x, (*iCamShift)->tracRect.y, (*iCamShift)->tracRect.width, (*iCamShift)->tracRect.height);
//
//      if(x+w < img.width() && y+h < img.height())
//        img.rectangle(x, y, x + w, y + h, PV_RGB(255,255,255));
    	
  }
}

//
// ACCESS
//

//
// INQUIRY
//

}; // namespace vision

}; // namespace ait


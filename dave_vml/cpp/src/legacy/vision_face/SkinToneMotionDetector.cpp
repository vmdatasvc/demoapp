/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#pragma warning(disable:4503)
#endif

#include "legacy/vision_tools/image_proc.hpp"
#include <legacy/vision/ImageAcquireMod.hpp>
#include "legacy/vision_face/SkinToneMotionDetector.hpp"

#include "opencv2/opencv.hpp"
//#include "cv.h"
//#include "cv.hpp"
//#include "cvaux.h"

#include "legacy/vision_tools/OpenCvUtils.hpp"
//#include "ipp.h"

namespace ait 
{

namespace vision
{

SkinToneMotionDetector::SkinToneMotionDetector()
{
}

SkinToneMotionDetector::~SkinToneMotionDetector()
{
}

void 
SkinToneMotionDetector::detectSkinToneMotion(Image32* curImage, Image32* prevImage, Image8* skinToneImage, Image8* motionImage)
{
	int width = curImage->width();
	int height = curImage->height();
	
	//ait::image_proc::smoothImage(curImage, ait::image_proc::SMOOTH_GAUSSIAN, 3);
	//ait::image_proc::smoothImage(prevImage, ait::image_proc::SMOOTH_GAUSSIAN, 3);

  unsigned int* pixels = curImage->pointer();
  unsigned int* tpixels = prevImage->pointer();
  
  Image<uchar>& motion = *motionImage;
  motion.setAll(0);

  for(int y = 0; y < height; y++)
  {
	  for(int x = 0; x < width; x++)
	  {
		  if((*skinToneImage)(x,y))
		  {
			  unsigned int pixel = pixels[y*width + x];
			  int cR = (((pixel)) & 0xff);
			  int cG = (((pixel)>>8) & 0xff);
			  int cB = (((pixel)>>16) & 0xff);

			  unsigned int tpixel = tpixels[y*width + x];
			  int tR = (((tpixel)) & 0xff);
			  int tG = (((tpixel)>>8) & 0xff);
			  int tB = (((tpixel)>>16) & 0xff);
			  
			  float tnorm = sqrt((float)((cR-tR)*(cR-tR) + (cG-tG)*(cG-tG) + (cB-tB)*(cB-tB))/3);
			  
			  float thres = (255.0/(*skinToneImage)(x,y));
			  
			  if(tnorm > 0.5*thres)
//			  if(tnorm > 1.0)
			  {
				  motion(x,y) = 255;
			  }
		  }
	  }
  }
  
  //Vector2i morphMask(2,2);
  //erode(motion, morphMask);
  //dilate(motion, morphMask);

  //for(int y = 0; y < height; y++)
	 // for(int x = 0; x < width; x++)
		//  (*motionImage)(x,y) = motion(x,y);

  //IplImage *iplMotionImage;
  //iplMotionImage = ait::vision::OpenCvUtils::convertPvImageToIpl(*motionImage);

  //cvErode(iplMotionImage, iplMotionImage);
  //cvDilate(iplMotionImage, iplMotionImage);

  //ait::vision::OpenCvUtils::copyIplToPvImage(iplMotionImage,*motionImage);
  //cvReleaseImage(&iplMotionImage);
}

}; // namespace vision

}; // namespace ait


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
#include "FacialFeatureDetector.hpp"
#include <PvImageProc.hpp>

namespace ait 
{

namespace vision
{

void FacialFeatureDetector::EyeDetect(Image8 &faceImage)
{
  int width, height;
  unsigned char* test;

  width = faceImage.width();
  height = faceImage.height();

  Image8 tempImage;
  tempImage = faceImage;
  tempImage.rescale(width*5, height*5);
 
  test = tempImage.pointer();
  //test.rescale(width*5, height*5);

  unsigned int      histogram[256];
  float             lookup[256];
  unsigned long int imagearea;
  unsigned long int i;
  
  imagearea = 25*width*height;
  PvImageProc::ComputeHistogram(test, width*5, height*5, histogram, 256);
  PvImageProc::ComputeLookup(histogram, lookup, width, height, 256);
  
  for (i=0;i<imagearea;i++)
    test[i] = (unsigned char)lookup[test[i]];

  //Threshold = 0.6*255..check if this is correct or not.

  float thresh = 0.6*255;

  /*for(i = 0; i < width*5; i++)
    for (int j = 0 ; j < height*5; j++)
    {
      if(test(i,j) < thresh)
        test(i,j) = 255;
      else
        test(i,j) = 0;
    }

  */

}

}
}


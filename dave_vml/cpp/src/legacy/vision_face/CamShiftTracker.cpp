/*
 * Do tracking using opencv builtin camshift color tracking algorithm.
 * This class just wraps up CvCamShiftTracker for easier use,
 * it doesn't subclass or plug into any other opencv stuff.
 * R. Jacob 7/28/2004
 *
 * Usage:
 * 	Instantiate a CamShiftTracker
 *	Call CamShiftTracker->Calibrate(n) to calibrate on next n frames
 * 	Call CamShiftTracker->doImage() to process live image
 * 	Call CamShiftTracker->getLoc() to get the answer, as a CvPoint
 *
 * User commands:
 *	In input window:
 *		Drag mouse rectangle to reset algorithm's window
 *		Use sliders to set algorithm parameters, see below
 *	In backproj window:
 *		Left mouse button => Calibrate now
 *
 * Returns coord of center of object, in pixels,
 * where LOWER left corner = 0,0
 *
 * Based on sys/vision/opencv/Tracker.cc, which was
 * based on filters/Tracker3dFilter/trackers/CamShiftTracker/CamShiftTracker.cpp
 * NB CvCamShiftTracker is defined in cv/include/cv.hpp and ./CvCamShiftTracker.cc (= cv/src/camshift.cpp)
 *
 * For more on setting these parameters, see:
 *	\Program Files\OpenCV\docs\appPage\CamShift\camshift.htm
 *	and \Program Files\OpenCV\docs\papers\camshift.pdf
 * also suggests turn auto white balance off on camera
 *
 * If want to operate one level lower, ie sans CvCamShiftTracker, then see:
 *	cv/src/camshift.cpp defines CvCamShiftTracker
 *	and it calls the lower routines, including cvCalcBackProject
 */

//#include <cvaux.h>
#include "legacy/vision_face/CamShiftTracker.hpp"
//#include "CvCamShiftTracker.hpp"
#include <stdio.h>

/*
 * Kludge so that static callback functions can access our object,
 * assumes that there's only one instance of CamShiftTracker around.
 */
//static CamShiftTracker *this_object;

namespace ait
{
 

CamShiftTracker::CamShiftTracker(int mId_, int width_, int height_, int x, int y, int w, int h, int frontLeftRight_)
: mId(mId_), mFrontLeftRight(frontLeftRight_)
{
  // Initialize our ivars
//  numCalibrate = 10;
  mFirstTime = true;
  mTracCenter = cvPoint (-1, -1);
  numTracking = 0;
  mFaceWindowAdjustment = true;

  // Initialize our mCsTracker
  mCsTracker = new CvCamShiftTracker();
  int dims[] = { 40, 40, 40};
  mCsTracker->set_hist_dims( 3, dims );
  mCsTracker->set_hist_bin_range( 0, 1, 180 );
  mCsTracker->set_threshold( 0 );
//  mCsTracker->set_min_ch_val( 1, 20 );   // S MIN
//  mCsTracker->set_max_ch_val( 1, 255 );  // S MAX
//  mCsTracker->set_min_ch_val( 2, 40 );   // V MIN
//  mCsTracker->set_max_ch_val( 2, 240 );  // V MAX
  mCsTracker->set_min_ch_val( 1, 0 );   // S MIN
  mCsTracker->set_max_ch_val( 1, 255 );  // S MAX
  mCsTracker->set_min_ch_val( 2, 0 );   // V MIN
  mCsTracker->set_max_ch_val( 2, 255 );  // V MAX

  mSize.width = width_;
  mSize.height = height_;
  mIplImage = cvCreateImage( mSize, IPL_DEPTH_8U, 3 );
  mIplMaskImage = cvCreateImage( mSize, IPL_DEPTH_8U, 1 );
  //	cvNamedWindow ("Back projection", CV_WINDOW_AUTOSIZE );
  mTracRect.x = x;
  mTracRect.y = y;
  mTracRect.width = w;
  mTracRect.height = h;
}

CamShiftTracker::CamShiftTracker(int mId_, int width, int height) 
{
  CamShiftTracker(mId_, width, height, 0, 0, 0, 0, 0);
  mFaceWindowAdjustment = false;
}

CamShiftTracker::~CamShiftTracker()
{
    cvReleaseImage( &mIplImage );
    cvReleaseImage( &mIplMaskImage);
    delete mCsTracker;
}

unsigned int CamShiftTracker::getId() {return mId;}

    /*
 * Calls the mCsTracker to do the work,
 * stashes answer in loc
 */

void CamShiftTracker::Track(Image32 *pvImage, CvRect faceRect) 
{
  // Process: perform tracking on the image. The format of the image will match
  // the format passed to SetFormat.
  // (probably call on each frame)


  unsigned int* pixels = pvImage->pointer();
//  unsigned char* pixels = (unsigned char*)pvImage->pointer();
  
  assert(pixels!=NULL);
  
  int w=pvImage->width();
  int h=pvImage->height();

  uchar *ucpixels = 0;
  int step = 0;
  cvGetRawData(mIplImage, &ucpixels, &step, &mSize);
  
  for(int y=0; y<h; y++)
    for(int x=0; x<w; x++)
    {
      unsigned int pixel = pixels[y*w + x];
      ucpixels[3*y*w + 3*x + 0] = (((pixel)) & 0xff);
      ucpixels[3*y*w + 3*x + 1] = (((pixel)>>8) & 0xff);
      ucpixels[3*y*w + 3*x + 2] = (((pixel)>>16) & 0xff);
    }
   
  if (mFirstTime) 
  {
	  imageHeight = pvImage->height();
	  mFirstTime = false;
  }

  if (numCalibrate > 0) 
  {
    CvRect bodyRect;
    if(mFaceWindowAdjustment)
    {    
      bodyRect = cvRect(faceRect.x + 0.1*faceRect.width, faceRect.y + 0.1*faceRect.height, 
      0.8*faceRect.width, 0.8*faceRect.height);
    }
    else
    {
      bodyRect = cvRect(faceRect.x - faceRect.width/2, faceRect.y + 1.5*faceRect.height,
      2*faceRect.width, faceRect.height);
    }
    // bodyRect = faceRect;
    mCsTracker->set_window (bodyRect);
    mCsTracker->update_histogram (mIplImage);
//    printf("numCalibrate = %d\n",numCalibrate);
    numCalibrate--;
    mTracRect = bodyRect;
    mTracCenter = cvPoint(bodyRect.x + bodyRect.width/2, bodyRect.y + bodyRect.height/2);
    return;
  }
  
    // In any case:
  if (! mCsTracker->track_object (mIplImage)) 
  {
	  fprintf (stderr, "track_object returned error\n");
	  mTracCenter = cvPoint (-1, -1);
	  return;
  }

  // Set loc
  mTracRect = mCsTracker->get_window();  // Returns bounding rectangle for the object
  mTracCenter = cvPoint (mTracRect.x + mTracRect.width/2, mTracRect.y + mTracRect.height/2);


	// This one is a passthrough from our mCsTracker,
	// displayed image seems upside down, so we flip it here
//	mIplImage *backprojShow = cvCloneImage (mCsTracker->get_back_project());
//	cvFlip (backprojShow, NULL, 0);
//	cvShowImage ("Back projection", backprojShow);
//	cvReleaseImage (&backprojShow);

  numTracking ++;

//  free(ucpixels);
}

void CamShiftTracker::Track(Image32 *pvImage, CvRect faceRect, Image8 *maskImage) 
{
  // Process: perform tracking on the image. The format of the image will match
  // the format passed to SetFormat.
  // (probably call on each frame)
  
  unsigned int* pixels = pvImage->pointer();
  
  assert(pixels!=NULL);
  
  int w=pvImage->width();
  int h=pvImage->height();

  uchar *ucpixels = 0;
  uchar *ucmpixels = 0;
  int step = 0;
  int step2 = 0;
  cvGetRawData(mIplImage, &ucpixels, &step, &mSize);
  cvGetRawData(mIplMaskImage, &ucmpixels, &step2, &mSize);
  
  for(int y=0; y<h; y++)
    for(int x=0; x<w; x++)
    {
      unsigned int pixel = pixels[y*w + x];
      ucpixels[3*y*w + 3*x + 0] = (((pixel)) & 0xff);
      ucpixels[3*y*w + 3*x + 1] = (((pixel)>>8) & 0xff);
      ucpixels[3*y*w + 3*x + 2] = (((pixel)>>16) & 0xff);
      ucmpixels[y*w + x] = (*maskImage)(x, y);
    }

  if (mFirstTime) 
  {
	  imageHeight = pvImage->height();
	  mFirstTime = false;
  }

  if (numCalibrate > 0) 
  {
//    CvRect bodyRect = cvRect(faceRect.x - faceRect.width/2, faceRect.y + 1.5*faceRect.height,
//      2*faceRect.width, faceRect.height);
    CvRect bodyRect = faceRect;
    mCsTracker->set_window (bodyRect);
    mCsTracker->update_histogram (mIplImage);
//    printf("numCalibrate = %d\n",numCalibrate);
    numCalibrate--;
    mTracRect = bodyRect;
    mTracCenter = cvPoint(bodyRect.x + bodyRect.width/2, bodyRect.y + bodyRect.height/2);
    return;
  }
  
    // In any case:
  if (! mCsTracker->track_object_mask (mIplImage, mIplMaskImage)) 
  {
	  fprintf (stderr, "track_object returned error\n");
	  mTracCenter = cvPoint (-1, -1);
	  return;
  }

  // Set loc
  mTracRect = mCsTracker->get_window();  // Returns bounding rectangle for the object
  mTracCenter = cvPoint (mTracRect.x + mTracRect.width/2, mTracRect.y + mTracRect.height/2);


	// This one is a passthrough from our mCsTracker,
	// displayed image seems upside down, so we flip it here
//	mIplImage *backprojShow = cvCloneImage (mCsTracker->get_back_project());
//	cvFlip (backprojShow, NULL, 0);
//	cvShowImage ("Back projection", backprojShow);
//	cvReleaseImage (&backprojShow);

  numTracking ++;
}

    
/*
 * Return stashed data
 * Center of object, in pixels, where lower left corner = 0,0
 */
CvPoint CamShiftTracker::getLoc () 
{
  return mTracCenter;
}

void CamShiftTracker::Calibrate (int nframes) 
{
  numCalibrate = nframes;
  mCsTracker->reset_histogram ();
}

} // namespace ait

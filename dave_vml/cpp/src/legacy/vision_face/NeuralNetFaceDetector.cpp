/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
//#include <io.h>
#ifndef __INTEL_COMPILER
#pragma warning(disable: 4786)
#endif // __INTEL_COMPILER

#include "legacy/vision_face/NeuralNetFaceDetector.hpp"
#include "legacy/low_level/Settings.hpp"
#include "legacy/vision_tools/PvImageProc.hpp""

#include <numeric>

using namespace std;

namespace ait
{

double NeuralNetFaceDetector::mPyramidScaleX = 1.2;
double NeuralNetFaceDetector::mPyramidScaleY = 1.2;


/*********************************************************************
* CONSTRUCTOR
*********************************************************************/
NeuralNetFaceDetector::NeuralNetFaceDetector(void) :
  verbose(0),
  mLightingCorrectionMatrixInitialized(false),
  mInitialized(false),
  mRealFastList(NULL),
  mSleepMilliseconds(0)
{
    mDataPath = ait::Settings::replaceVars("<CommonFilesPath>/data/nns/data/");
}

bool operator<(const AccumKey& l, const AccumKey& r)
{
  if (l.mX < r.mX) return true;
  if (l.mX > r.mX) return false;
  if (l.mY < r.mY) return true;
  if (l.mY > r.mY) return false;
  if (l.mS < r.mS) return true;
  return false;
}

/*********************************************************************
* DESTRUCTOR
*********************************************************************/
NeuralNetFaceDetector::~NeuralNetFaceDetector(void)
{
  finalize();
}

FastForwardStruct::~FastForwardStruct() 
{
  if (unitList!=NULL) delete[] unitList;
  if (connList!=NULL) delete[] connList;
  if (connections!=NULL) delete[] connections;
  if (unitInfo!=NULL) delete[] unitInfo;
  if (groupInfo!=NULL) delete[] groupInfo;
}

// USED
// Save detection information in a list of detection.  The list is 
// given in the ClientData argument

void NeuralNetFaceDetector::SaveDetections(DetectionList *detectionList, Image8 *image,
                    int x, int y, int width, int height,
                    int level,
                    double scale, double output, int orientation)
{
  detectionList->push_back(Detection(x,y,level,output,orientation));
}

inline float FastForwardUnit::getActivation() const
{
  return activation;
}

inline void FastForwardUnit::setActivation(const float fActivation)
{
  activation=fActivation;
}

// USED
// Search for the eyes in the face.  Inputs are the image pyramid, 
// the face location and scale.  Returns with the two eye positions.
// If the return value is 0, then the face is considered to be too 
// small for the eye detector to be effective.  If the function returns
// 1, then the eye detector has been run.  However, in this case the
// x and y positions being set to -1 indicate that no eye was detected.

int NeuralNetFaceDetector::SearchEyes(int numScales, Image8 *imagePyramid[], Image8 *mask,
                                      int xx, int yy, int s, int basenet,
                                      int *eyex1, int *eyey1, int *eyex2, int *eyey2)
{
  
  // Figure out the scale at which to run the eye detector 
  // (the "7" was determined empirically)
  
  int scale=s-7;
  if (scale<0 && scale>=-3) scale=0;
  if (scale>=0)
  {
    Image8 *image=imagePyramid[scale];
    double x1=0, y1=0, n1=0, x2=0, y2=0, n2=0;
    
    int *tmp=new int[mask->width()*mask->height()];      // Window for eye detector
    
    // Possible upper-left X positions for the left eye
    int startx1=(int)floor(0.5+xx*pow(mPyramidScaleX,s-scale))-mask->width()/2;
    int endx1=(int)floor(0.5+(xx+10)*pow(mPyramidScaleX,s-scale))-mask->width()/2;
    if (startx1<0) startx1=0;
    if (endx1>=image->width()-mask->width()+1) endx1=image->width()-mask->width()+1;
    
    // Possible upper-left X positions for the right eye
    
    int startx2=max(0, int(floor(0.5+(xx+10)*pow(mPyramidScaleX,s-scale))-mask->width()/2));
    int endx2=min((int)(image->width()-mask->width()+1), 
      int(floor(0.5+(xx+20)*pow(mPyramidScaleX,s-scale))-mask->width()/2));
    
    // Possible upper-left Y positions for the eyes
    int starty=max(0, int(floor(0.5+yy*pow(mPyramidScaleY,s-scale))-mask->height()/2));
    int endy=min((int)(image->height()-mask->height()+1), 
		 int(floor(0.5+(yy+10)*pow(mPyramidScaleY,s-scale))-mask->height()));

    int y;
    int x;
    for (y=starty; y<endy; y++) {
      // Look for right eye on this scan line
      for (x=startx2; x<endx2; x++) {
        int ptr=0;
        int hist[256] = {0};

        // Copy the window into tmp (using mirror image), and compute
        // the histogram over the entire window

        int j;
        for (j=0; j<mask->height(); j++) 
        {
          int i=0;
          for (i=mask->width()-1; i>=0; i--) {
            int data=(*image)(i+x,j+y);
            tmp[ptr++]=data;
            hist[data]++;
          }
        }
        // Compute cummulative histogram
        
        int map[256];
        int num=0;
        int i=0;
        for (i=0; i<256; i++) 
        {
          map[i]=num;
          num+=hist[i];
        }
        int tot=num;
        for (i=255; i>=0; i--) {
          map[i]+=num;
          num-=hist[i];
        }
        
        // Apply the histogram equalization, and write window to network inputs
        
        double scaleFactor=1.0/tot;
        FastForwardUnit *unit=&(mRealFastList[basenet]->unitList[1]);
        for (i=0; i<mask->width()*mask->height(); i++)
          (unit++)->setActivation(map[tmp[i]]*scaleFactor-1.0);
        
        // If the network responds positively, add the detection to centroid
        
        double output=FastForwardPass(mRealFastList[basenet]);
        if (output>0) {
          n2+=output;
          x2+=output*(x+mask->width()/2)*pow(mPyramidScaleX,scale);
          y2+=output*(y+mask->height()/2)*pow(mPyramidScaleY,scale);
          
        }
      }
      
      // Look for left eye in this scan line
      
      for (x=startx1; x<endx1; x++) {
        int ptr=0;
        int hist[256] = {0};
        
        // Copy the window into tmp (using mirror image), and compute
        // the histogram over the entire window
        
        for (int j=0; j<mask->height(); j++) 
        {
          int i=0;
          for (i=0; i<mask->width(); i++) 
          {
            int data=(*image)(i+x,j+y);
            tmp[ptr++]=data;
            hist[data]++;
          }
        }
        
        // Compute cummulative histogram
        
        int map[256];
        int num=0;
        int i=0;
        for (i=0; i<256; i++) {
          map[i]=num;
          num+=hist[i];
        }
        int tot=num;
        for (i=255; i>=0; i--) {
          map[i] += num - tot;
          num-=hist[i];
        }
        
        // Apply the histogram equalization, and write window to network inputs
        
        double scaleFactor = 1.0/tot;
        FastForwardUnit *unit = &(mRealFastList[basenet]->unitList[1]);
        for (i=0; i<mask->width()*mask->height(); i++)
        {
          (unit++)->setActivation(map[tmp[i]]*scaleFactor);
        }

        // If the network responds positively, add the detection to centroid
        
        double output=FastForwardPass(mRealFastList[basenet]);
        if (output>0) {
          n1+=output;
          x1+=output*(x+mask->width()/2)*pow(mPyramidScaleX,scale);
          y1+=output*(y+mask->height()/2)*pow(mPyramidScaleY,scale);
        }
      }
    }
    
    // If the left eye was detected at least once, return centroid
    
    if (n1>0) {
      *eyex1=(int)floor(0.5+x1/n1); *eyey1=(int)floor(0.5+y1/n1);
    } else {
      *eyex1=-1; *eyey1=-1;
    }
    
    // If the right eye was detected at least once, return centroid
    
    if (n2>0) {
      *eyex2=(int)floor(0.5+x2/n2); *eyey2=(int)floor(0.5+y2/n2);
    } else {
      *eyex2=-1; *eyey2=-1;
    }
    delete[] tmp;
    return 1;
  } 
  
  // Return fact that face was too small
  *eyex1=-1; *eyey1=-1;
  *eyex2=-1; *eyey2=-1;
  return 0;
}

// USED
// Fill a rectangular region of a byte image with the given value

void NeuralNetFaceDetector::FillRectangle(Image8 *image, int x1, int y1, int x2, int y2, int color)
{
  for (int y=max(0,y1); y<min((int)(image->height()),y2); y++)
  {
    for (int x=max(0,x1); x<min((int)(image->width()),x2); x++)
    {
      (*image)(x,y)=color;
    }
  }
}

// USED
// Resample an image, according to the following rule: if any of the
// pixels in the source image which coorespond to a pixel in the
// destination image are zero, then set the destination pixel to zero.
// Otherwise, set the pixel to 255.  scale<1 makes the image smaller.
// The idea is that 0 means a pixel location should be scanned, while
// 255 means that a face has already been found and need not be checked
// again.  Destination can be the same as the source.
// The scaling factor is determined by SCALE, and the size of the
// destination image is controlled by newWith and newHeight;

void NeuralNetFaceDetector::ResampleByteImage(Image8 *image, Image8 *src,
                                              int newWidth, int newHeight, double scale)
{
  image->resize(newWidth,newHeight);  // Allocate destination
  image->setAll(255);
  
  for (int y=0; y<newHeight; y++)      // Scan over lower resolution image
  {
    int y1=min((int)(src->height()-1), int(0.5+y/scale));
    int y2=min((int)(src->height()-1), int(0.5+(y+1)/scale));
    float fx = 0.5;
    int x;
    for (x=0; x<newWidth; x++) 
    {
      int x1=min((int)(src->width()-1), int(fx));
      fx += 1/scale;
      int x2=min((int)(src->width()-1), int(fx));

      int i, j;
      for (j=y1; j<=y2; j++)      // Scan over corresponding pixels in hi res
      {
        unsigned char *pSrc = &(*src)(x1,j);
        for (i=x1; i<=x2; i++) 
        {
          if (!*pSrc++) {  // If any pixel is zero, make new pixel zero
            (*image)(x,y) = 0;
            goto next;
          }
        }
      }
next:
      ;
    }
  }
}

// USED
// Scale the given byte image down by a factor of (mPyramidScaleX,mPyramidScaleY), putting the result
// in the given destination (which *cannot* be the same as the source).  The
// scaling uses bilinear interpolation, implemented by two steps of linear
// interpolation: first scaling in the X direction, then in the Y direction.

void NeuralNetFaceDetector::Interpolate(Image8 *dest, Image8 *src, double scaleX, double scaleY)
{
  const int width=src->width();
  const int height=src->height();
  const int newWidth= int(floor(width/scaleX));
  const int newHeight= int(floor(height/scaleY));
  
  // First scale in X
  Image8 tmpImg(newWidth, height);
  int x, y;
  for (y=0; y<height; y++) {
    float fPos = 0.0;
    for (x=0; x<newWidth; x++) {
      int pos = min(width-2, int(fPos));
      double frac = fPos-pos;
      tmpImg(x,y) = clamp(int(0.5+(1.0-frac)*(*src)(pos,y)+frac*(*src)(pos+1,y)), 0, 255);
      fPos += scaleX;
    }
  }
  
  // Then scale in Y
  dest->resize(newWidth, newHeight);
  for (x=0; x<newWidth; x++) {
    float fPos = 0.0;
    for (y=0; y<newHeight; y++) {
      int pos = min(height-2, int(fPos));
      double frac = fPos-pos;
      (*dest)(x,y) = clamp(int(0.5+(1.0-frac)*tmpImg(x,pos)+frac*tmpImg(x,pos+1)), 0, 255);
      fPos += scaleY;
    }
  }
}


void NeuralNetFaceDetector::MirrorByteImage(Image8 *image)
{
  for (int y=0; y<image->height(); y++)
  {
    for (int x=0; x<image->width()/2; x++)
    {
      Byte tmp=(*image)(x,y);
      (*image)(x,y)=(*image)(image->width()-x-1,y);
      (*image)(image->width()-x-1,y)=tmp;
    }
  }
}

bool NeuralNetFaceDetector::init()
{
  if(!mInitialized){
    char *networks[]={"umec","face17c","face18c","eye"};
    FastAllocateNetworks(4);
    FastInitNetwork(0,1,networks);      // network 0: ume
    FastInitNetwork(1,1,networks+1);    // network 1: face12 + face13
    FastInitNetwork(2,1,networks+2);    // network 1: face12 + face13
    FastInitNetwork(3,1,networks+3);    // network 2: eye

    string fname = mDataPath + "facemask20x20.pgm";
    if (!mFaceMask.load(fname.c_str())) 
    {
      return false;
    }

    fname = mDataPath + "eyemask25x15.pgm";
    if (!mEyeMask.load(fname.c_str()))
    {
      return false;
    }
    mInitialized = true;
  }
  return true;
}

void NeuralNetFaceDetector::finalize()
{
  if(mInitialized)
  {
    int i = 0;
    for (i=0; i<mRealNumNetworks; i++){ 
      delete mRealFastList[i];
      mRealFastList[i] = NULL;
    }
    delete [] mRealFastList;
    mRealFastList = NULL;
    mInitialized = false;
  }
}

// USED
// Function to count and locate all faces in an image.  Input buffer
// is an array of unsigned char, 1 byte per pixel indicating the
// intensity.  The width and height are the width and height in
// pixels.  The return value is the number of faces found.  If locs is
// NULL, the actual locations are discarded.  Otherwise, if no faces
// were found, *locs is set to NULL.  If faces were found, *locs is
// set to a pointer to an array of struct FaceLocation.  It is the
// responsibility of the caller to free() this array.  The structures
// will also contain the locations of the eyes, if they are found.
// The left and right designations are from the user's point of view.
// The flags left and right are set to 1 if that eye is detected, 0 if
// it is not detected, or -1 if the face is to small to attempt
// detection.

int NeuralNetFaceDetector::Track_FindAllFaces(Image8 &image,
                                              struct FaceEyeLocation **locs, int asynch)
{
  int levels=0;                            // Figure out # levels in pyramid
  const int width = image.width();
  const int height = image.height();

  int w=width;
  int h=height;
  while (w>=mSearchMaskWidth && h>=mSearchMaskHeight) {
    w=(int)(w/mPyramidScaleX);
    h=(int)(h/mPyramidScaleY);
    levels++;
  }

  if (levels == 0)
  {
    return 0;
  }
  
  std::vector<Image8*> imagePyramid(levels);
  imagePyramid[0] = &image;
  int i;
  for (i=1; i<levels; i++)
  {
    if(verbose) PVMSG("level %d/%d\n",i,levels);
    imagePyramid[i] = new Image8;
    const Image8& src = *imagePyramid[i-1];
    Interpolate(imagePyramid[i], imagePyramid[i-1], mPyramidScaleX, mPyramidScaleY);
    //imagePyramid[i]->rescale(src,src.width()/mPyramidScaleX,src.height()/mPyramidScaleY,RESCALE_LINEAR);
  }
  
  Image8 *mainmask=new Image8(width,height);  
  mainmask->setAll(0);                    // Mask, 0=scan, 255=face found
  Image8 *levelmask=new Image8; // Mask for current level
  
  DetectionList alldetections[1];         // Store detections
  for (i=levels-1; i>=0; i--)
  {
    if(verbose) PVMSG("processing level %d/%d\n",i,levels);
    
    if(mSleepMilliseconds) 
    {
      //PVMSG("going to sleep...\n");
      PvUtil::sleep((double)mSleepMilliseconds/1000);
      //PVMSG("I'm back...\n");
    }
    
    // Get search mask at right level
    ResampleByteImage(levelmask, mainmask, imagePyramid[i]->width(), imagePyramid[i]->height(), pow(1.2,-i));
    
    DetectionList detections;             // Find candidates
    SearchFaster(imagePyramid[i], levelmask, i, &detections);
    
    DlIterator detect = detections.begin();
    for (; detect != detections.end(); detect++)
    {
      int newX, newY, newS;
      // Verify candidates
      if (FindNewLocation(levels, &imagePyramid[0], &mFaceMask,
        detect->x, detect->y, detect->s, 6, 6, 1, 2, 1,
        &newX, &newY, &newS))
      {
        // Save verified candidate
        SaveDetections(alldetections, imagePyramid[newS], newX, newY, 20, 20, newS, pow(1.2,newS), 1.0, 0);

        // Put detection in search mask, to save computation
        FillRectangle(mainmask, (int)(pow(mPyramidScaleX,newS)*newX+0.5),
          (int)(pow(mPyramidScaleY,newS)*newY+0.5),
          (int)(pow(mPyramidScaleX,newS)*(newX+20)+0.5),
          (int)(pow(mPyramidScaleY,newS)*(newY+20)+0.5),
          255);
      }
    }
  }
  
  // Remove overlapping detections (a few may slip through)
  DetectionList results;
  FuzzyVote2(width, height, 1, alldetections, &results, 2, 1, 1, 1, 0, &mFaceMask);
  
  int counter=results.size();
  if (locs!=NULL)
  {
    if (counter==0) 
    {
      *locs=NULL; 
    }
    else
    {
      // Make output array
      *locs=(struct FaceEyeLocation*)malloc(sizeof(struct FaceEyeLocation)*counter);
      int i=0;
      DlIterator d = results.begin();
      for (; d!=results.end(); d++)
      {
        // Check for eyes
        int bigenough = SearchEyes(levels, &imagePyramid[0], &mEyeMask,
          d->x, d->y, d->s, 3,
          &((*locs)[i].rightx),
          &((*locs)[i].righty),
          &((*locs)[i].leftx),
          &((*locs)[i].lefty));
        if (bigenough) 
        {
          // Set flags: 1=eye found, 0=not found
          (*locs)[i].left=((*locs)[i].leftx==-1)?0:1;
          (*locs)[i].right=((*locs)[i].rightx==-1)?0:1;
        } 
        else 
        {
          // Set flags: -1=face too small
          (*locs)[i].left=-1;
          (*locs)[i].right=-1;
        }

        // Map face location into original resolution
        // (eye locations are already set appropriately)
        double sX=pow(mPyramidScaleX,d->s);
        double sY=pow(mPyramidScaleY,d->s);
        (*locs)[i].x1=(int)floor(d->x*sX+0.5);
        (*locs)[i].y1=(int)floor(d->y*sY+0.5);
        (*locs)[i].x2=(int)floor((d->x+20)*sX+0.5);
        (*locs)[i].y2=(int)floor((d->y+20)*sY+0.5);
        i++;
      }
    }
  }
  
  // Clean up
  for (i=1; i<levels; i++) {
    delete imagePyramid[i];
    imagePyramid[i] = NULL;
  }
  
  delete mainmask;
  delete levelmask;
  
  if(verbose) PVMSG("### FaceDetectorControl - escaped programers hell!\n");
  
  return counter;
}

// USED
// The lighting correction matrix is used to fit an affine function to 
// the intensity values in a window

void NeuralNetFaceDetector::InitializeLightingCorrectionMatrix()
{
  int ptr=0;
  Mat mat=Zero(3,3);
  int i;
  const int w2 = mFaceMask.width()/2;
  const int h2 = mFaceMask.height()/2;
  for (int j=-h2; j<int(mFaceMask.height())-h2; j++)
  {
    for (i=-w2; i<int(mFaceMask.width())-w2; i++) 
    {
      
      // Only use pixels inside the mask
      
      if (mFaceMask(i+w2, j+h2)) {
        mat(0,0)+=i*i;
        mat(0,1)+=i*j;
        mat(0,2)+=i;
        mat(1,0)+=i*j;
        mat(1,1)+=j*j;
        mat(1,2)+=j;
        mat(2,0)+=i;
        mat(2,1)+=j;
        mat(2,2)+=1;
      }
    }
  }
  LightingCorrectionMatrix=LUInverse(mat);
  mLightingCorrectionMatrixInitialized = true;
}


// USED
// Given a candidate detection location and the image pyramid, search around
// in that candidate to try and find out whether there is really a face there.
// Uses two networks (merged together), both of which have to agree on a
// detection.  If both networks agree on multiple locations, the centroid of
// those locations is computed.

int NeuralNetFaceDetector::FindNewLocation(int numScales, Image8 *imagePyramid[], Image8 *mask,
                                           int x, int y, int s, int dx, int dy, int ds, int step,
                                           int basenet,
                                           int *newX, int *newY, int *news)
{
  if (!mLightingCorrectionMatrixInitialized)
    InitializeLightingCorrectionMatrix();
  
  double totX=0;      // Used to record centroid of face detections
  double totY=0;
  double totS=0;
  double tot=0;
  
  int ptr;
  int hist[512];                  // Histogram
  int map[512];                   // Cumulative histogram
  Vec vec(3);                     // Part of affine fitting
  int *tmp=new int[mask->height()*mask->width()];   // Window
  
  int halfX=mask->width()/2;       
  int halfY=mask->height()/2;
  
  x+=halfX;     // Input location is upper left corner of
  y+=halfY;     // the "centered" face.  This makes location be face center.
  
  FastForwardStruct *net=mRealFastList[basenet];
  FastForwardStruct *net2=mRealFastList[basenet+1];
  
  // Loop over scales
  for (int ss=s-ds; ss<=s+ds; ss++) {
    if (ss<0 || ss>=numScales) continue;
    
    int cx=(int)floor(x*pow(mPyramidScaleX,s-ss)+0.5);  // Map center position to scale
    int cy=(int)floor(y*pow(mPyramidScaleY,s-ss)+0.5);
    
    Image8 *image=imagePyramid[ss];
    for (int yy=cy-dy; yy<=cy+dy; yy+=step)  // Scan over position
      for (int xx=cx-dx; xx<=cx+dx; xx+=step) {
        ptr=0;
        double v0=0, v1=0, v2=0;
        
        // The next two loops copy the window into the tmp variable, and
        // begin computing the affine fit (using only pixels inside the mask)
        // The first version is for windows inside the image, the second 
        // replicates the edge pixels of the image.
        
        if (xx-halfX>=0 && yy-halfY>=0 &&
          xx+halfX<image->width() && yy+halfY<image->height()) 
        {
          int i=0;
          for (int iy=yy-halfY; iy<yy+halfY; iy++, i++) 
          {
            int j=0;
            for (int ix=xx-halfX; ix<xx+halfX; ix++, j++) {
              int val=(*image)(ix,iy);
              tmp[ptr++]=val;
              if ((*mask)(i, j))
              {
                v0+=(ix-xx)*val;
                v1+=(iy-yy)*val;
                v2+=val;
              }
            }
          }
        } else 
        {
          int i=0;
          for (int iy=yy-halfY; iy<yy+halfY; iy++, i++)     
          {
            int j=0;
            for (int ix=xx-halfX; ix<xx+halfX; ix++, j++) {
              int ii=clamp(ix, 0, image->width()-1);
              int jj=clamp(iy, 0, image->height()-1);
              int val=(*image)(ii,jj);
              tmp[ptr++]=val;
              if ((*mask)(i, j))
              {
                v0+=(ix-xx)*val;
                v1+=(iy-yy)*val;
                v2+=val;
              }
            }             
          }
        }
#ifdef DEBUG
        
        if (xx-halfX==12 && yy-halfY==28 && ss==8) {
          for (int i=0; i<400; i++) 
            fprintf(stderr,"in[%d]=%d\n",i,tmp[i]);
        }
#endif
        // Actually compute the parameters of the affine fit.
        
        vec(0)=v0; vec(1)=v1; vec(2)=v2;
        vec=LightingCorrectionMatrix*vec;
        v0=vec(0); v1=vec(1); v2=vec(2);
        
        // Apply the affine correction, and build the histogram based
        // on pixels in the mask
        const int h2 = mask->height()/2;
        const int w2 = mask->width()/2;
	int i;
        for (i=0; i<512; i++) 
        { 
          hist[i]=0;
        }
        ptr=0;
        float fV1 = -h2*v1;
	int j;
        for (j=0; j<2*h2; j++)
        {
          double fV0 = fV1 - w2*v0 + v2 - 256.5;
          for (i=0; i<2*w2; i++) 
          {
            int val=clamp(tmp[ptr]-int(fV0), 0, 511);
            if ((*mask)(i,j)) 
            {
              hist[val]++;
            }
            tmp[ptr++]=val;
            fV0 += v0;
          }
          fV1 += v1;
        }
          
          // Build the cummulative histogram
          
          int *to=map;
          int *from=hist;
          int total=0;
          for (i=0; i<512; i++) {
            int old=total;
            total+=*(from++);
            *(to++)=old+total;
          }
          
#ifdef DEBUG
          fprintf(stderr,"total=%d!!!!\n",total);
#endif
          // Apply histogram equalization and copy the window into the 
          // network inputs
          
          double scaleFactor=1.0/total;
          FastForwardUnit *unit=&(net->unitList[1]);
          int *p=tmp;
          for (i=0; i<mask->height()*mask->width(); i++)
            (unit++)->setActivation(map[*(p++)]*scaleFactor-1.0);
          
          FastForwardPass(net);

          // Check the two outputs (we applied one network, but really
          // it is two merged networks).  If both responded postively,
          // then add the detection on to the centroid of detections.
          
          if (net->unitList[net->firstOutput].getActivation()>0) {
            FastForwardUnit *unit=&(net->unitList[1]);
            FastForwardUnit *unit2=&(net2->unitList[1]);
            for (i=0; i<mask->height()*mask->width(); i++)
              (unit2++)->setActivation((unit++)->getActivation());
            FastForwardPass(net2);
            if (net2->unitList[net2->firstOutput].getActivation()>0) {
              totS+=ss;
              totX+=xx*pow(mPyramidScaleX,ss);
              totY+=yy*pow(mPyramidScaleY,ss);
              tot++;
            }
          }
      }
  }
  delete[] tmp;
  if (tot==0) return 0;
  
  // Compute the centroid, first in scale and then in position at that scale
  
  int newS=clamp(int(floor(totS/tot+0.5)), 0, numScales-1);
  *newX=(int)floor(totX/tot/pow(mPyramidScaleX,newS)+0.5)-halfX;
  *newY=(int)floor(totY/tot/pow(mPyramidScaleY,newS)+0.5)-halfY;
  *news=newS;
  return 1;
}

// USED
// Compute a forward pass through the given neural network, and return 
// the value of the first output unit (which is most useful if the network
// only has a single output).
float NeuralNetFaceDetector::FastForwardPass(FastForwardStruct *net)
{
  FastForwardUnit *unit;
  for (unit=net->unitList+net->firstHidden; unit<net->unitList+net->numUnits; 
    unit++)
  {
    assert(TanhUnitType == unit->type);
    // tanh() activation function, compute weighted sum of inputs
    float tot0 = 0.0;
    float tot1 = 0.0;
    float tot2 = 0.0;
    float tot3 = 0.0;
    FastForwardConnection *conn;
    // unroll the loop for some efficiency
    for (conn=unit->connections; 
         conn<unit->connections+unit->numInputs-3;
         conn+=4) 
    {
      tot0+=conn[0].weight*conn[0].from->getActivation();
      tot1+=conn[1].weight*conn[1].from->getActivation();
      tot2+=conn[2].weight*conn[2].from->getActivation();
      tot3+=conn[3].weight*conn[3].from->getActivation();
    }
    float tot = tot0 + tot1 + tot2 + tot3;
    // finish
    for (;
         conn<unit->connections+unit->numInputs;
         conn++) 
    {
      tot+=conn->weight*conn->from->getActivation();
    }
    // Non-linearity
    unit->setActivation(tanh(tot));
  }
  return net->unitList[net->firstOutput].getActivation();
}

FastForwardStruct *NeuralNetFaceDetector::FastLoad(char *netFile)
{
  char *netList[1];
  netList[0]=netFile;
  FastForwardStruct *net=FastLoadMerge(1,netList);
  FastLoadMergeWeights(net,1,netList);
  return net;
}

// USED
// Given a list network file names, add a ".wet" to the name, and load
// the weights from those files.  This function loads multiple sets of
// weights into a single network, and assuming that the same set of network
// architectures has been loaded by FastLoadMerge.
void NeuralNetFaceDetector::FastLoadMergeWeights(FastForwardStruct *fastnet,
                                                 int numNetworks, char *nets[])
{
  int conn=0;
  for (int net=0; net<numNetworks; net++)
  {
    char name[1024];
    sprintf(name,"%s%s.wet",mDataPath.c_str(),nets[net]);
    FILE *wet=fopen(name,"r");
    if (wet==NULL) continue;
    int weights;
    fscanf(wet,"%*d epochs\n%d weights\n",&weights);
    for (int i=0; i<weights; i++)
      fscanf(wet,"%f\n",&(fastnet->connList[conn++].conn->weight));
    fclose(wet);
  }
}

// USED
// Load one or more networks into a single network.  The input is the number
// of networks to load, and either a list of names of network files (to which
// ".net" will be appended), or a list of FILE pointers from which the networks
// will be read.  The networks are merged such that they share inputs, then
// the hidden units are grouped together, and finally the outputs from each
// network are listed one after another.
FastForwardStruct *NeuralNetFaceDetector::FastLoadMerge(int numNetworks, char *nets[], FILE **netFile)
{
  // If a vector of FILE pointers was not given, use the network names in
  // the nets array to open the files
  int filesGiven=(netFile!=NULL);
  if (!filesGiven) {
    netFile=new FILE*[numNetworks];
    for (int net=0; net<numNetworks; net++) {
      char name[1024];
      sprintf(name,"%s%s.net",mDataPath.c_str(),nets[net]);
      //printf("dir: %s ,fileName: %s\n",getcwd(NULL,1024),name);
      netFile[net]=fopen(name,"r");
      if (netFile[net]==NULL) {
        //fprintf(stderr,"Could not open %s\n",name);
        PvUtil::exitError("Could not open %s\n",name);
      }
      assert(netFile[net]!=NULL);
    }
  }
  // Record some numbers for each network
  int *netInputs=new int[numNetworks];
  int *netHiddens=new int[numNetworks];
  int *netOutputs=new int[numNetworks];
  int *netUnits=new int[numNetworks];
  int *netConns=new int[numNetworks];
  int totalUnits=0, totalInputs=0, totalHiddens=0, totalOutputs=0;
  int totalConns=0;
  
  // Read in the number of units and number of inputs for each network
  // The inputs are merged together, but all other units must be
  // represented separately
  int net;
  for (net=0; net<numNetworks; net++) {
    fscanf(netFile[net],"%d units\n%d inputs\n",
      &netUnits[net],&netInputs[net]);
    totalInputs=netInputs[net];
    totalUnits+=netUnits[net]-netInputs[net];
  }
  
  // Read x,y,group information for each input unit
  int *unitx=new int[totalUnits+totalInputs];
  int *unity=new int[totalUnits+totalInputs];
  int *unitg=new int[totalUnits+totalInputs];
  for (net=0; net<numNetworks; net++) {
    for (int i=0; i<netInputs[net]; i++)
      fscanf(netFile[net],"%*d %d %d %d\n",
      unitg+i,unity+i,unitx+i);
  }
  
  // Read the number of hidden units, the hidden units themselves,
  // and the number of output units
  int ptr=totalInputs;
  for (net=0; net<numNetworks; net++) {
    fscanf(netFile[net],"\n%d hiddens\n",&netHiddens[net]);
    totalHiddens+=netHiddens[net];
    for (int i=0; i<netHiddens[net]; i++) {
      fscanf(netFile[net],"%*d %d %d %d\n",
        unitg+ptr,unity+ptr,unitx+ptr);
      ptr++;
    }
    fscanf(netFile[net],"\n%d outputs\n",&netOutputs[net]);
    totalOutputs+=netOutputs[net];
  }
  
  // Read information on the output units, and the number of connections
  for (net=0; net<numNetworks; net++) {
    for (int i=0; i<netOutputs[net]; i++) {
      fscanf(netFile[net],"%*d %d %d %d\n",
        unitg+ptr,unity+ptr,unitx+ptr);
      ptr++;
    }
    fscanf(netFile[net],"\n%d conns\n",&netConns[net]);
    totalConns+=netConns[net];
  }
  
  // Begin creating the data structure
  FastForwardStruct *fastnet=new FastForwardStruct;
  totalUnits+=totalInputs;
  fastnet->numUnits=totalUnits;
  fastnet->numInputs=totalInputs;
  fastnet->numHiddens=totalHiddens;
  fastnet->numOutputs=totalOutputs;
  fastnet->numGroups=1;
  
  // Figure out how many groups there were
  int i;
  for (i=0; i<fastnet->numUnits; i++)
    if (unitg[i]+1>fastnet->numGroups)
      fastnet->numGroups=unitg[i]+1;
    
    fastnet->unitInfo=new FastUnitInfo[fastnet->numUnits];
    fastnet->groupInfo=new FastGroupInfo[fastnet->numGroups];
    fastnet->patterns=0;
    fastnet->firstInput=1;
    fastnet->firstHidden=totalInputs;
    fastnet->firstOutput=totalInputs+totalHiddens;
    fastnet->unitList=new FastForwardUnit[fastnet->numUnits];
    fastnet->connList=new FastForwardConnectionRef[totalConns];
    fastnet->connections=new FastForwardConnection[totalConns];
    fastnet->numConns=totalConns;
    
    for (i=0; i<fastnet->numGroups; i++) {
      fastnet->groupInfo[i].startx=-1;
      fastnet->groupInfo[i].endx=-1;
      fastnet->groupInfo[i].starty=-1;
      fastnet->groupInfo[i].endy=-1;
      fastnet->groupInfo[i].numUnits=0;
      fastnet->groupInfo[i].firstUnit=-1;
    }
    
    for (i=0; i<fastnet->numUnits; i++) {
      // Initialize each unit
      fastnet->unitList[i].numInputs=0;
      fastnet->unitList[i].numOutputs=0;
      fastnet->unitList[i].setActivation(1.0);
      if (i==0) fastnet->unitList[i].type=BiasUnitType; else
        if (i<totalInputs) fastnet->unitList[i].type=InputUnitType; else
          fastnet->unitList[i].type=TanhUnitType;
        fastnet->unitInfo[i].x=unitx[i];
        fastnet->unitInfo[i].y=unity[i];
        fastnet->unitInfo[i].group=unitg[i];
        // Figure out the bounding dimensions of each group
        if (fastnet->groupInfo[unitg[i]].startx==-1 ||
          fastnet->groupInfo[unitg[i]].startx>unitx[i])
          fastnet->groupInfo[unitg[i]].startx=unitx[i];
        if (fastnet->groupInfo[unitg[i]].starty==-1 ||
          fastnet->groupInfo[unitg[i]].starty>unity[i])
          fastnet->groupInfo[unitg[i]].starty=unity[i];
        if (fastnet->groupInfo[unitg[i]].endx==-1 ||
          fastnet->groupInfo[unitg[i]].endx<unitx[i])
          fastnet->groupInfo[unitg[i]].endx=unitx[i];
        if (fastnet->groupInfo[unitg[i]].endy==-1 ||
          fastnet->groupInfo[unitg[i]].endy<unity[i])
          fastnet->groupInfo[unitg[i]].endy=unity[i];
        fastnet->groupInfo[unitg[i]].numUnits++;
        if (fastnet->groupInfo[unitg[i]].firstUnit==-1)
          fastnet->groupInfo[unitg[i]].firstUnit=i;
    }
    
    int firstHidden=totalInputs;
    int firstOutput=totalInputs+totalHiddens;
    
    if (!filesGiven) {
      // If we are reading from files, then try locating the .type file
      for (int net=0; net<numNetworks; net++) {
        char name[1024];
        sprintf(name,"%s.type",nets[net]);
        FILE *inf=fopen(name,"r");
        if (inf==NULL) continue;
	int i;
        for (i=0; i<netInputs[net]; i++) fscanf(inf,"%*d");
        for (i=0; i<netHiddens[net]; i++)
          fscanf(inf,"%d",&(fastnet->unitList[firstHidden++].type));
        for (i=0; i<netOutputs[net]; i++)
          fscanf(inf,"%d",&(fastnet->unitList[firstOutput++].type));
        fclose(inf);
      }
    }
    
    // Read in the connections
    firstHidden=totalInputs;
    firstOutput=totalInputs+totalHiddens;
    int firstConn=0;
    for (net=0; net<numNetworks; net++) {
      for (i=0; i<netConns[net]; i++) {
        int from, to;
        fscanf(netFile[net],"%d %d -1\n",&from,&to);
        if (from>=netHiddens[net]+netInputs[net])
        {
          from+=firstOutput-netHiddens[net]-netInputs[net]; 
        }
        else
        {
          if (from>=netInputs[net])
          {
            from+=firstHidden-netInputs[net];
          }
        }
        if (to>=netHiddens[net]+netInputs[net]) 
        {
          to+=firstOutput-netHiddens[net]-netInputs[net]; 
        }
        else 
        {
          if (to>=netInputs[net]) 
          {
            to+=firstHidden-netInputs[net];
          }
        }
        fastnet->unitList[from].numOutputs++;
        fastnet->unitList[to].numInputs++;
        fastnet->connList[firstConn].from=from;
        fastnet->connList[firstConn].to=to;
        firstConn++;
      }
      if (!filesGiven) fclose(netFile[net]);
      firstHidden+=netHiddens[net];
      firstOutput+=netOutputs[net];
    }
    
    // Allocate space for each of the connections into a unit
    firstConn=0;
    for (i=0; i<totalUnits; i++) {
      if (fastnet->unitList[i].numInputs==0)
      {
        fastnet->unitList[i].connections=NULL; 
      }
      else 
      {
        fastnet->unitList[i].connections=fastnet->connections+firstConn;
        firstConn+=fastnet->unitList[i].numInputs;
        fastnet->unitList[i].numInputs=0;
      }
    }
    
    // Associate the connections going into each unit with the unit itself
    for (i=0; i<totalConns; i++) {
      FastForwardConnection *pTo =
        fastnet->unitList[fastnet->connList[i].to].connections+
        (fastnet->unitList[fastnet->connList[i].to].numInputs++);
      FastForwardUnit *pFrom = fastnet->unitList+fastnet->connList[i].from;
      pTo->from=pFrom;
      fastnet->connList[i].conn=pTo;
//      pFrom->vTo.push_back(pTo);
      pTo->activation = pFrom->getActivation();
    }
    
    // Clean up
    if (!filesGiven) delete[] netFile;
    delete[] netInputs;
    delete[] netHiddens;
    delete[] netOutputs;
    delete[] netUnits;
    delete[] netConns;
    delete[] unitx;
    delete[] unity;
    delete[] unitg;
    return fastnet;
}

void NeuralNetFaceDetector::FastReadActivations(FastForwardStruct *net, char *filename)
{
  FILE *inf=fopen(filename,"r");
  for (int i=0; i<net->numUnits; i++) {
    float activation;
    fscanf(inf,"i=%*d, t=%*d, a=%g\n", &activation);
    net->unitList[i].setActivation(activation);
  }
  fclose(inf);
}

float *NeuralNetFaceDetector::FastInputWeights(FastForwardStruct *net, int from, int to,
                                               int *width, int *height, int *num)
{
  *width=net->groupInfo[from].endx-net->groupInfo[from].startx+1;
  *height=net->groupInfo[from].endy-net->groupInfo[from].starty+1;
  *num=net->groupInfo[to].numUnits;
  float *buf=(float*)malloc(sizeof(float)*(*width)*(*height)*(*num));
  int i;
  for (i=0; i<(*width)*(*height)*(*num); i++) buf[i]=0;
  for (i=0; i<net->numConns; i++) {
    if (net->unitInfo[net->connList[i].to].group==to &&
      net->unitInfo[net->connList[i].from].group==from) {
      int band=net->connList[i].to-net->groupInfo[to].firstUnit;
      int x=net->unitInfo[net->connList[i].from].x-net->groupInfo[from].startx;
      int y=net->unitInfo[net->connList[i].from].y-net->groupInfo[from].starty;
      //      if (x==0 && y==0) fprintf(stderr,"foo: %d (to=%d,band=%d)\n",i,to,band);
      buf[band*(*width)*(*height)+y*(*width)+x]=
        net->connList[i].conn->weight;
    }
  }
  return buf;
}

// USED
// Get the value of some location in the detection pyramid.  If that location
// has not already been allocated, create it and set it to zero.

int NeuralNetFaceDetector::FuzzyVoteAccumGet(int x, int y, int s)
{
  int nResult = FuzzyVoteAccumCheck(x, y, s);
  if (nResult)
  {
    return nResult;
  } 
  else
  {
    AccumKey key(x,y,s);
    AccumElement *elem = new AccumElement(x,y,s,0);
    pair<AemIterator,bool> pr = mAccumElementMap.insert(AccumElementMap::value_type(key,elem));
    assert(pr.second);
    mBins[0].addLast(elem);
    return pr.first->second->getValue();
  }
}

// USED
// Get the value of some location in the detection pyramid.  If that location
// does not exist, then return zero (but do not allocate that location).

int NeuralNetFaceDetector::FuzzyVoteAccumCheck(int x, int y, int s)
{
  AccumKey key(x,y,s);
  AemIterator it = mAccumElementMap.find(key);
  if (it == mAccumElementMap.end()) {
    return 0;
  }
  else
  {
    return it->second->getValue();
  }
}

// USED
// Set a value in the detection pyramid; if that location does not exist,
// then create it.  Also, add that location to the list of locations with
// that value.  
void NeuralNetFaceDetector::FuzzyVoteAccumZero(int x, int y, int s)
{
  AccumElement* pElem = NULL;
  AccumKey key(x,y,s);
  AemIterator it = mAccumElementMap.find(key);
  if (it == mAccumElementMap.end()) 
  {
    // element not found
    // Don't create a new entry--this is also the value that will be returned
    // if the entry is not there so it is not needed
    return;
  } 
  else
  {
    // remove element from its current bin
    pElem = it->second;
    mBins[pElem->getValue()].unchain(pElem);
    pElem->setValue(0);
  }
  // add element to new bin
  mBins[0].addLast(pElem);
}

// USED
// Set a value in the detection pyramid; if that location does not exist,
// then create it.  Also, add that location to the list of locations with
// that value.  
void NeuralNetFaceDetector::FuzzyVoteAccumIncr(int x, int y, int s)
{
  AccumElement* pElem = NULL;
  AccumKey key(x,y,s);
  AemIterator it = mAccumElementMap.find(key);
  int val;
  if (it == mAccumElementMap.end()) 
  {
    val = 1;
    pElem = new AccumElement(x,y,s,val);
    pair<AemIterator,bool> pr = mAccumElementMap.insert(AccumElementMap::value_type(key,pElem));    
    assert(pr.second);
  } 
  else
  {
    // remove element from its current bin
    pElem = it->second;
    assert(pElem->getValue()>=0 && pElem->getValue()<256);
    mBins[pElem->getValue()].unchain(pElem);
    pElem->setValue(pElem->getValue() + 1);
    val = pElem->getValue();
  }
  // add element to new bin
  mBins[val].addLast(pElem);
}

// USED 
// Given some point in the detection pyramid, locate all 6-connection 
// locations with a value greater than or equal to the specified amount,
// and find their centroid.  The locations in the centroid are set to zero.
// Centroid in scale is computed by averaging the pyramid levels at which
// the faces are detected.
void NeuralNetFaceDetector::FindCentroidAccum(int numImages,
                                              int s, int x, int y,
                                              int minVal, int currVal,
                                              double *totalS, double *totalX, double *totalY,
                                              double *total)
{
  int value=FuzzyVoteAccumCheck(x,y,s);
  if (value>=minVal && value<=currVal)
  {
    FuzzyVoteAccumZero(x,y,s);
    double rvalue=value;
    (*total)+=rvalue;
    (*totalS)+=s*rvalue;
    (*totalX)+=x*pow(mPyramidScaleX,s)*rvalue;
    (*totalY)+=y*pow(mPyramidScaleY,s)*rvalue;
    FindCentroidAccum(numImages,s,x+1,y,minVal,value,
      totalS,totalX,totalY,total);
    FindCentroidAccum(numImages,s,x-1,y,minVal,value,
      totalS,totalX,totalY,total);
    FindCentroidAccum(numImages,s,x,y+1,minVal,value,
      totalS,totalX,totalY,total);
    FindCentroidAccum(numImages,s,x,y-1,minVal,value,
      totalS,totalX,totalY,total);
    FindCentroidAccum(numImages,
      s-1,(int)floor(0.5+x*mPyramidScaleX),(int)floor(0.5+y*mPyramidScaleY),
      minVal,value,
      totalS,totalX,totalY,total);
    FindCentroidAccum(numImages,
      s+1,(int)floor(0.5+x/mPyramidScaleX),(int)floor(0.5+y/mPyramidScaleY),
      minVal,value,
      totalS,totalX,totalY,total);
  }
}


// USED
// Search the given image using the mSearchMaskWidthxmSearchMaskHeight candidate detector.  The levelmask
// is used to indicate which portions of the image to not bother searching,
// because a face has already been found there.  When a face is found, 
// the callback function is called with informatoin about the detection.

void NeuralNetFaceDetector::SearchFaster(Image8 *image, 
                                         Image8 *levelmask, 
                                         int level,
                                         DetectionList *data,
                                         double threshold)
{
  // the search window is divided horizontally and vertically into thirds.
  // the search is centered in the middle of the window. This means the search
  // window size must be a multiple of 3 (actually, 6, since nSm*3 gets divided
  // by 2 below).
  const int nSmw3 = mSearchMaskWidth/3;
  const int nSmh3 = mSearchMaskHeight/3;

  int x, y, i, j;

  if (levelmask!=NULL) 
  {

    // For each nSmw3 x nSmh3 pixel block where the center of a face could be,
    // see if more than 5 pixel locations still need to be scanned.  If
    // so, the block must be scanned.  This is indicated by a flag placed
    // at the upper-left corner of each block
    
    int xp=(image->width()+(nSmw3-1))/nSmw3;
    int yp=(image->height()+(nSmh3-1))/nSmh3;
    int tot = 0;
    int num = 0;
    for (y=0; y<yp; y++)
    {
      for (x=0; x<xp; x++)
      {
        tot=0; 
        num=0;
        for (j=y*nSmh3; j<min((int)(levelmask->height()), (y+1)*nSmh3); j++) 
        {
          for (i=x*nSmw3; i<min((int)(levelmask->width()), (x+1)*nSmw3); i++)
          {
              tot++;
              if (!(*levelmask)(i,j)) num++;
          }
        }
        if (num<=5)
        {
          (*levelmask)(x*nSmw3,y*nSmh3)=255; 
        }
        else
        {
          (*levelmask)(x*nSmw3,y*nSmh3)=0;
        }


        if (mpInclusionIntegralImageMask != NULL)
        {
          // Calculate area rect in levelmask size.
          Rectanglei areaRect(x*nSmw3,
                           y*nSmh3,
                           min((int)(levelmask->width()), (x+1)*nSmw3),
                           min((int)(levelmask->height()), (y+1)*nSmh3));

          Image<unsigned short>& mask = *mpInclusionIntegralImageMask;

          // Now resize the area rect to the inclusion mask size.
          areaRect.set(areaRect.x0*mask.width()/levelmask->width(),
                       areaRect.y0*mask.height()/levelmask->height(),
                       areaRect.x1*mask.width()/levelmask->width(),
                       areaRect.y1*mask.height()/levelmask->height());

          unsigned short area = calcIntegralImageArea(mask,areaRect);

          float areaFraction = ((float)area)/(areaRect.width()*areaRect.height());

          if (areaFraction < mInclusionAreaFactor)
          {
            (*levelmask)(x*nSmw3,y*nSmh3)=255;
          }
        }
      }
    }
  }
  
  int tmp[mSearchMaskWidth * mSearchMaskHeight];              // Used to store the window
  for (y=0; y<image->height(); y+=nSmh3)     // For each block
  {
    for (x=0; x<image->width(); x+=nSmw3)
    {
      if (levelmask!=NULL && (*levelmask)(x,y))
        continue;
      int i, j;
      
      int hist[256] = {0};             // Used to store histogram
      
      // Copy the window from the image into tmp.  The first loop is used
      // when the window is entirely inside the image, the second one is
      // used otherwise.  For pixels outside the image, the pixels at the
      // edge of the image are replicated.  The histogram is updated.
      int *to=tmp;
      if (x>=nSmw3 && y>=nSmh3 && x+nSmw3*2<=image->width() && y+nSmh3*2<=image->height()) 
      {
        for (j=y-nSmh3; j<y+nSmh3*2; j++) 
        {
          unsigned char *pImage = &(*image)(x-nSmw3,j);
          for (i=x-nSmw3; i<x+nSmw3*2; i++) {
            hist[*pImage]++;
            (*(to++))=*pImage++;
          }
        }
      } else {
        for (j=y-nSmh3; j<y+nSmh3*2; j++) 
        {
          for (i=x-nSmw3; i<x+nSmw3*2; i++) 
          {
            int ii=clamp(i, 0, image->width()-1);
            int jj=clamp(j, 0, image->height()-1);
            int val=(*image)(ii,jj);
            hist[val]++;
            (*(to++))=val;
          }
        }
      }
      
      // The loop below is supposed to calculate a cumulative histogram for
      // histogram equalization. What it actually calculates is
      // sum(hist[j], j=0..i-1) + sum(hist[j], j=0..i)
      int tot=0;
      for (i=0; i<256; i++)
      {
        int old=tot;
        tot+=hist[i];
        hist[i]=old+tot - mSearchMaskWidth * mSearchMaskHeight;
      }
      // Apply histogram equalization, write image into network input units
      double scaleFactor=1.0/tot;
      FastForwardUnit *unit=&(mRealFastList[0]->unitList[1]);
      for (i=0; i<mSearchMaskWidth * mSearchMaskHeight; i++) {
        unit[i].setActivation(hist[tmp[i]]*scaleFactor);
      }
      
      // Apply the network
      double output=FastForwardPass(mRealFastList[0]);
      
      // If there is a detection, call the callback function to record
      // or otherwise deal with that detection
      if (output>threshold)
      {
        SaveDetections(data,image,x-nSmw3/2,y-nSmw3/2,nSmw3*2,nSmw3*2,level,pow(1.2,level),output,0);
      }
    }
  }
}

void NeuralNetFaceDetector::FindCentroid(Image8 *images[], 
                                         int numImages,
                                         int s, int x, int y, int val,
                                         double *totalS, double *totalX, double *totalY,
                                         double *total)
{
  if (s>=0 && s<numImages &&
    x>=0 && x<images[s]->width() &&
    y>=0 && y<images[s]->height() &&
    (*images[s])(x,y)>=val)
  {
    (*total)+=(*images[s])(x,y);
    (*totalS)+=s*(*images[s])(x,y);
    (*totalX)+=x*exp(log(mPyramidScaleX)*s)*(*images[s])(x,y);
    (*totalY)+=y*exp(log(mPyramidScaleY)*s)*(*images[s])(x,y);
    (*images[s])(x,y)=0;
    FindCentroid(images,numImages,s,x+1,y,val,
      totalS,totalX,totalY,total);
    FindCentroid(images,numImages,s,x-1,y,val,
      totalS,totalX,totalY,total);
    FindCentroid(images,numImages,s,x,y+1,val,
      totalS,totalX,totalY,total);
    FindCentroid(images,numImages,s,x,y-1,val,
      totalS,totalX,totalY,total);
    FindCentroid(images,numImages,
      s-1,(int)floor(0.5+x*mPyramidScaleX),(int)floor(0.5+y*mPyramidScaleY),val,
      totalS,totalX,totalY,total);
    FindCentroid(images,numImages,
      s+1,(int)floor(0.5+x/mPyramidScaleX),(int)floor(0.5+y/mPyramidScaleY),val,
      totalS,totalX,totalY,total);
  }
}


#ifdef __GNUC__
template class ListClass<AccumElement>;
template class ListNode<AccumElement>;
#endif

// USED 
// Given some point in the detection pyramid, locate all 6-connection 
// locations with a value greater than or equal to the specified amount,
// and find their centroid.  The locations in the centroid are set to zero.
// Centroid in scale is computed by averaging the pyramid levels at which
// the faces are detected.

void NeuralNetFaceDetector::FindCentroidAccum(int numImages, int s, int x, int y, int val,
                                              double *totalS, double *totalX, double *totalY,
                                              double *total)
{
  int value=FuzzyVoteAccumCheck(x,y,s);
  if (value>=val) {
    FuzzyVoteAccumZero(x,y,s);
    (*total)+=value;
    (*totalS)+=s*value;
    (*totalX)+=x*pow(mPyramidScaleX,s)*value;
    (*totalY)+=y*pow(mPyramidScaleY,s)*value;
    FindCentroidAccum(numImages,s,x+1,y,val,
      totalS,totalX,totalY,total);
    FindCentroidAccum(numImages,s,x-1,y,val,
      totalS,totalX,totalY,total);
    FindCentroidAccum(numImages,s,x,y+1,val,
      totalS,totalX,totalY,total);
    FindCentroidAccum(numImages,s,x,y-1,val,
      totalS,totalX,totalY,total);
    FindCentroidAccum(numImages,
      s-1,(int)floor(0.5+x*mPyramidScaleX),(int)floor(0.5+y*mPyramidScaleY),val,
      totalS,totalX,totalY,total);
    FindCentroidAccum(numImages,
      s+1,(int)floor(0.5+x/mPyramidScaleX),(int)floor(0.5+y/mPyramidScaleY),val,
      totalS,totalX,totalY,total);
  }
}

// USED
// Implementation of the arbitration mechanisms described in the paper.
// Inputs are lists of detections, the width and height of the image that
// is being processed, a callback function used to report the arbitration
// results, and a bunch of parameters for the arbitration itself.

void NeuralNetFaceDetector::FuzzyVote2(int width, int height, int numLocFiles,
                                       DetectionList detections[],
                                       DetectionList *results,
                                       int spread, int search, int collapse, int overlap,
                                       int filterodd, Image8 *mask)
{
  // Figure out the number of scales in the pyramid
  
  int temWidth=width, temHeight=height;
  int numScales=0;
  while (temWidth>=mask->width() && temHeight>=mask->height()) {
    temWidth=(int)(floor(temWidth/mPyramidScaleX));
    temHeight=(int)(floor(temHeight/mPyramidScaleY));
    numScales++;
  }
  
  // For each detection list given as input (all detections are treated
  // equally)
  
  for (int num=0; num<numLocFiles; num++) {
    int numFaces=0;
    int done=0;
    
    // For each detection
    
    DlIterator detect = detections[num].begin();
    for (; detect != detections[num].end(); detect++) {
      int x=detect->x, y=detect->y, s=detect->s;
      if (filterodd && ((x&1) || (y&1))) continue;
      int xc=x+mask->width()/2;
      int yc=y+mask->height()/2;
      // Spread out the detection in both scale and location by 
      // "spread" levels or pixels, incrementing the value of each
      // location in the detection pyramid
      for (int si=-spread; si<=spread; si++) {
        if (si+s<0 || si+s>=numScales) continue;
        int xci=(int)floor(0.5+xc*exp(log(mPyramidScaleX)*-si));
        int yci=(int)floor(0.5+yc*exp(log(mPyramidScaleY)*-si));
        int sspread=spread;
        for (int xx=xci-sspread; xx<=xci+sspread; xx++)
          for (int yy=yci-sspread; yy<=yci+sspread; yy++)
            FuzzyVoteAccumIncr(xx,yy,s+si);
      }
      numFaces++;
    }
  }
  
  // Scan through the detection pyramid from highest to lowest value
  
  for (int val=255; val>=search; val--) {
    while (!mBins[val].empty()) {
      // Get the detection
      int x=mBins[val].first->getX();
      int y=mBins[val].first->getY();
      int s=mBins[val].first->getS();
      int cs, cx, cy;
      
      // If we are to collapse nearby overlapping detections, find the
      // centroid of the detections around this location.  Otherwise, just
      // record this location.
      
      if (collapse) {
        double total=0.0;
        double totalS=0, totalX=0, totalY=0;
        FindCentroidAccum(numScales,s,x,y,search,
          &totalS,&totalX,&totalY,&total);
        cs=(int)floor(0.5+totalS/total);
        cx=(int)floor(0.5+totalX/total*exp(log(mPyramidScaleX)*-cs));
        cy=(int)floor(0.5+totalY/total*exp(log(mPyramidScaleY)*-cs));
      } else {
        cs=s;
        cx=x;
        cy=y;
        FuzzyVoteAccumZero(x,y,s);
      }
      
      // If we are to remove overlapping detections, scan through
      // the detection pyramid, removing all possible overlaps
      
      if (overlap) {
        for (int scale=0; scale<numScales; scale++) {
          int xpos=(int)floor(0.5+cx*pow(mPyramidScaleX,cs-scale));
          int ypos=(int)floor(0.5+cy*pow(mPyramidScaleY,cs-scale));
          int sizex=mask->width()/2+
            (int)floor(0.5+mask->width()/2*pow(mPyramidScaleX,cs-scale));
          int sizey=mask->height()/2+
            (int)floor(0.5+mask->height()/2*pow(mPyramidScaleY,cs-scale));
          for (int xx=xpos-sizex; xx<=xpos+sizex; xx++)
            for (int yy=ypos-sizey; yy<=ypos+sizey; yy++)
              FuzzyVoteAccumZero(xx,yy,scale);
        }
      }
      
      // Record (or otherwise deal with) this detection
      SaveDetections(results,NULL,cx-mask->width()/2,cy-mask->height()/2,
        mask->width(),mask->height(),
        cs,pow(1.2,cs),1.0,0);
    }
  }
  // Clean up
  mAccumElementMap.clear();
  int i;
  for (i = 0; i < sizeof(mBins)/sizeof(mBins[0]); i++)
  {
    mBins[i].deleteNodes();
  }
}

// USED
// Allocate an array of pointers to neural networks.  This array is global
// (yuck!).  If an array already exists, it and the networks it points to
// are deleted.

void NeuralNetFaceDetector::FastAllocateNetworks(int num)
{
  if (mRealFastList!=NULL) {
    for (int i=0; i<mRealNumNetworks; i++) delete mRealFastList[i];
    delete[] mRealFastList;
  }
  mRealFastList=new FastForwardStruct*[num];
  for (int i=0; i<num; i++) mRealFastList[i]=NULL;
  mRealNumNetworks=num;
}

// USED
// Load a network, and place it in the array created by FastAllocateNetworks

void NeuralNetFaceDetector::FastInitNetwork(int num, int numNetworks, char *networkList[])
{
  if (mRealFastList[num]!=NULL) delete mRealFastList[num];
  mRealFastList[num]=FastLoadMerge(numNetworks,networkList);
  FastLoadMergeWeights(mRealFastList[num],numNetworks,networkList);
}

} // namespace ait

project (legacy_vision_faceLib)
set (LIB_NAME legacy_vision_face)

set (LIB_SRC
#	Face.cpp
#	FaceDetector.cpp
#	FaceDetectorAdaBoost.cpp
#	FaceDetectorAdaBoostForeground.cpp
#	FaceDetectorCombined.cpp
#	FaceDetectorNN.cpp
	Mat2.cpp
	system2.cpp
#	SkinToneDetector.cpp
#	SkinToneMotionDetector.cpp
#	NeuralNetFaceDetector.cpp
#	GenderSVM.cpp
#	CvCamShiftTracker.cpp
#	CamShiftTracker.cpp
#	FaceDetectClassifyMod.cpp
#	FaceVerifyClassifyMod.cpp
	Arclet3DNN.cpp
	YawPitchNN.cpp
	SparseGeometricNN.cpp
	lssvr.cpp
	)

vml_add_library (${LIB_NAME}
	STATIC
	${LIB_SRC}
	${PROJECT_HEADER_FILES}
	)



#include <stdio.h>
//#include "SparseGeometricNN.hpp"
#include "legacy/vision_face/Arclet3DNN.hpp"
#include "legacy/vision_tools/image_proc.hpp"

const char *help1 = "\
MLPTorch III (c) Trebolloc & Co 2003\n\
\n\
This program will test a single image using the trained face detection NN from the model_file.\n";

using namespace std;
using namespace Torch;

namespace ait
{

  // Constructor
void Arclet3DNN::init(string mModelListFile_, string mModelDir_, string mLsvFile_)
{
  get_args();

  maxNumModel = 200;
  mModelListFile = mModelListFile_;
  mModelDir = mModelDir_;
  mLsvFile = mLsvFile_;

  Allocator *allocator = new Allocator();
  DiskXFile::setLittleEndianMode();
  mMvNorm.reserve(maxNumModel);

  PVMSG("%s %s\n",mModelDir.c_str(),mModelListFile.c_str());

	std::string str = mModelDir + "/" + mModelListFile;
	fp = fopen(str.c_str(), "r");
  if( fp )
  {}
  else
  {
    //fprintf(stderr,"Model list file %s not found!\n", mModelListFile.c_str());
		PvUtil::exitError("Model list file %s not found!\n", str.c_str());
		//exit(1);	
  }

  int counter = 0;

// Read the NN model files and construct NNs
  while(fscanf(fp, "%f %f %f %f\n",&mMidEyeX, &mMidEyeY, &mScale, &mOrien) == 4)
  {
//    cout<<counter<<" "<<mMidEyeX<<" "<<mMidEyeY<<" "<<mScale<<" "<<mOrien<<endl;
    ModelPersonPtr pM3D(new ModelPerson(mMidEyeX, mMidEyeY, mScale, mOrien));
    mModel3D.push_back(pM3D);

    std::string index = ait::aitSprintf("/Face_%4.2f_%4.2f_%4.2f_%4.2f/Train.model",mMidEyeX,mMidEyeY,mScale,mOrien);
    string modelFile(mModelDir + index);

 //	cout << modelFile<<endl;

    DiskXFile model(modelFile.c_str(), "r");
    mCmd.loadXFile(&model);

//  	numInputTmp[counter] = numInputs;
//    printf("model_file:%s numInputs=%d numHiddenUnits1=%d numHiddenUnits2=%d numHiddenUnits3=%d numTargets=%d\n",
//    mModelListFile.c_str(),numInputs,numHiddenUnits1,numHiddenUnits2,numHiddenUnits3,numTargets); 
 
    Linear *c1 = new(allocator) Linear(numInputs, numHiddenUnits1,&mRandom);
    c1->setROption("weight decay", weight_decay);
    Tanh*c2 = new(allocator) Tanh(numHiddenUnits1);
    Linear *c3 = new(allocator) Linear(numHiddenUnits1, numHiddenUnits2,&mRandom);
    c3->setROption("weight decay", weight_decay);
    Tanh *c4 = new(allocator) Tanh(numHiddenUnits2);
    Linear *c5 = new(allocator) Linear(numHiddenUnits2, numHiddenUnits3,&mRandom);
    c5->setROption("weight decay", weight_decay);
    Tanh *c6 = new(allocator) Tanh(numHiddenUnits3);
    Linear *c7 = new(allocator) Linear(numHiddenUnits3, numTargets,&mRandom);
    c7->setROption("weight decay", weight_decay);

    Torch::ConnectedMachine *pCc = new Torch::ConnectedMachine();

    pCc->addFCL(c1);    
    pCc->addFCL(c2);
    pCc->addFCL(c3);
    pCc->addFCL(c4);
    pCc->addFCL(c5);
    pCc->addFCL(c6);
    pCc->addFCL(c7);

    pCc->build();
    pCc->setPartialBackprop();

    MeanVarNorm *mvN;
  	mvN = new MeanVarNorm(numInputs, numTargets);
    mvN->loadXFile(&model);

    mMvNorm.push_back(mvN);

    pCc->loadXFile(&model);

    mMlp.push_back(pCc);

    counter++;
  }
  fclose(fp);

  numModel = counter;
  printf("numModel = %d\n",numModel);

	mLsv=0;

	FILE *fpLSV;
	fpLSV=fopen(mLsvFile.c_str(),"r");
  if(fpLSV)
    {
      mLsv=lsv_read(mLsvFile.c_str());
      fclose(fpLSV);
    }
  else
    {    
			fprintf(stderr, "Need lsv.save!\n");
		exit(1);
		}
//  if(fpLSV=fopen("c:/Program Files/Advanced Interfaces/VideoMining 2.1/common/data/facial_geometry_estimator/face_2D_XYSO/lsv.save","r"))
//    {
//      mLsv=lsv_read("c:/Program Files/Advanced Interfaces/VideoMining 2.1/common/data/facial_geometry_estimator/face_2D_XYSO/lsv.save");
//      fclose(fpLSV);
//    }
//  else
//    {    
//			fprintf(stderr, "Need c:/Program Files/Advanced Interfaces/VideoMining 2.1/common/data/facial_geometry_estimator/face_2D_XYSO/lsv.save!\n");
//		exit(1);
//		}
 fclose(fp);
}

void Arclet3DNN::init(string mModelListFile_, string mModelDir_)
{
  std::string lsvFile("c:/ai/common/data/facial_geometry_estimator/face_2D_XYSO/lsv.save");
  init(mModelListFile_, mModelDir_, lsvFile);
}

// Torch3 interface to read Torch style trained model file
// It seems that the unnecessary options were added from training; 
// need to remove these within the training code
//void Arclet3DNN::get_args()
//{
//  int max_load, k_fold, the_seed, class_against_the_others;
//  real accuracy, decay;
//  char *dir_name, *valid_file;
//  bool regression_mode;
//
//  // Train mode
//  mCmd.addICmdArg("n_inputs", &numInputs, "input dimension of the data", true);
//  mCmd.addICmdArg("n_targets", &numTargets, "output dim. (regression) or # of classes (classification)", true);
//  mCmd.addICmdOption("-class", &class_against_the_others, -1, "train the given class against the others", true);
//  mCmd.addICmdOption("-nhu1", &numHiddenUnits1, 25, "number of hidden units", true);
//  mCmd.addICmdOption("-nhu2", &numHiddenUnits2, 50, "number of hidden units", true);
//  mCmd.addICmdOption("-nhu3", &numHiddenUnits3, 50, "number of hidden units", true);
//  mCmd.addBCmdOption("-rm", &regression_mode, false, "regression mode ?", true);
//  mCmd.addRCmdOption("-e", &accuracy, (float)0.00001, "end accuracy");
//  mCmd.addRCmdOption("-lrd", &decay, 0, "learning rate decay");
//  mCmd.addICmdOption("-kfold", &k_fold, -1, "number of folds, if you want to do cross-validation");
//  mCmd.addRCmdOption("-wd", &weight_decay, 0, "weight decay", true);
//  mCmd.addICmdOption("-seed", &the_seed, -1, "the random seed");
//  mCmd.addSCmdOption("-valid", &valid_file, "", "validation file, if you want it");
//
//  // Test mode
//  mCmd.addMasterSwitch("--test");
//  cmd.addImCmdOption("-load", &max_load, -1, "max number of examples to load for train");
//  cmd.addSmCmdOption("-dir", &dir_name, ".", "directory to save measures");
//
//  // Read the command line
//  int argc = 2;
//  char **argv;
//  argv = (char **)malloc(2*sizeof(char *));
//  argv[0] = (char *)malloc(11*sizeof(char));
//  argv[1] = (char *)malloc(7*sizeof(char));
//  argv[0] = "executable";
//  argv[1] = "--test";
//  int mode = cmd.read(argc, argv);
//  
//}

void Arclet3DNN::get_args()
{
  //  char *mModelListFile, *mModelDir, *imageFile;

  // Put the help line at the beginning
  mCmd.info(help1);

  // Train mode
  mCmd.addText("\nArguments:");
  mCmd.addSCmdArg("file", &mFileParams.file, "the train file");
  mCmd.addICmdArg("n_inputs", &numInputs, "input dimension of the data", true);
  mCmd.addICmdArg("n_targets", &numTargets, "output dim. (regression) or # of classes (classification)", true);

  mCmd.addText("\nModel Options:");
  mCmd.addICmdOption("-class", &mFileParams.class_against_the_others, -1, "train the given class against the others", true);
  mCmd.addICmdOption("-nhu1", &numHiddenUnits1, 25, "number of hidden units", true);
  mCmd.addICmdOption("-nhu2", &numHiddenUnits2, 50, "number of hidden units", true);
  mCmd.addICmdOption("-nhu3", &numHiddenUnits3, 50, "number of hidden units", true);
  mCmd.addBCmdOption("-rm", &mFileParams.regression_mode, false, "regression mode ?", true);

  mCmd.addText("\nLearning Options:");
  mCmd.addICmdOption("-iter", &mFileParams.max_iter, 25, "max number of iterations");
  mCmd.addRCmdOption("-lr", &mFileParams.learning_rate, 0.01f, "learning rate");
  mCmd.addRCmdOption("-e", &mFileParams.accuracy, 0.00001f, "end accuracy");
  mCmd.addRCmdOption("-lrd", &mFileParams.decay, 0.0f, "learning rate decay");
  mCmd.addICmdOption("-kfold", &mFileParams.k_fold, -1, "number of folds, if you want to do cross-validation");
  mCmd.addRCmdOption("-wd", &weight_decay, 0.0f, "weight decay", true);

  mCmd.addText("\nMisc Options:");
  mCmd.addICmdOption("-seed", &mFileParams.the_seed, -1, "the random seed");
  mCmd.addICmdOption("-load", &mFileParams.max_load, -1, "max number of examples to load for train");
  mCmd.addICmdOption("-load_valid", &mFileParams.max_load_valid, -1, "max number of examples to load for valid");
  mCmd.addSCmdOption("-valid", &mFileParams.valid_file, "", "validation file, if you want it");
  mCmd.addSCmdOption("-dir", &mFileParams.dir_name, ".", "directory to save measures");
  mCmd.addSCmdOption("-save", &mFileParams.model_file, "", "the model file");
  mCmd.addBCmdOption("-bin", &mFileParams.binary_mode, false, "binary mode for files");

  // Test mode
  mCmd.addMasterSwitch("--test");
  mCmd.addText("\nArguments:");

  mCmd.addText("\nMisc Options:");
  mCmd.addICmdOption("-load", &mFileParams.max_load, -1, "max number of examples to load for train");
  mCmd.addSCmdOption("-dir", &mFileParams.dir_name, ".", "directory to save measures");
  mCmd.addBCmdOption("-bin", &mFileParams.binary_mode, false, "binary mode for files");

  // Read the command line
  char *argv[2];
  argv[0] = "executable";
  argv[1] = "--test";
  int mode = mCmd.read(2, argv);
}

void Arclet3DNN::applyModel(int numEstModel, int wgt1, int wgt2, Image8 faceImage)
{
  unsigned char *ucImage;
  vector< float > responses;
  responses.reserve(numEstModel);
  float min, sum;

  ucImage = (unsigned char *) malloc(mLsv->img_height * mLsv->img_height * sizeof(unsigned char));
	float *responseVec;
	responseVec = (float *)malloc(mLsv->num_filters*sizeof(float));

	int count=0;
		for(int y=0; y<mLsv->img_height; y++)
			for(int x=0; x<mLsv->img_width; x++)
      {
				ucImage[count++] = faceImage(x,y);
      }

	 lsv_apply_filters(mLsv, responseVec, ucImage);
	 
	 // Model loop
  min = 10.0;
  Sequence inputs(1, numInputs);

  for(int m = 0; m<numEstModel; m++)
  {

// Normalize the input to NN
   for(int i=0; i < inputs.frame_size; i++)
   {
      inputs.frames[0][i] = responseVec[i];
//	  printf("%d ",inputData[i]);
	  inputs.frames[0][i] = (inputs.frames[0][i] - mMvNorm[m]->inputs_mean[i]) / mMvNorm[m]->inputs_stdv[i]; 
//	  inputs.frames[0][i] = 1.0; 
	 }
		// NN forward
   	mMlp[m]->forward(&inputs);
	
    responses.push_back(mMlp[m]->outputs->frames[0][0]);

	if(responses[m] < min)
	  min = responses[m];
  }
	free(ucImage);
	free(responseVec);
  
  for(int m = 0; m<numEstModel; m++)
  {
    responses[m] = responses[m] - min;
	  responses[m] = pow(responses[m], wgt1);
  }
  
  sum = 0.0;
  // Estimate
  mEstMidEyeX = 0.0;
  mEstMidEyeY = 0.0;
  mEstMidEyeMouthDist = 0.0;
  mEstOrientation = 0.0;

  for(int m=0; m<numEstModel; m++)
  {
    {
    	mEstMidEyeX += responses[m]*mModel3D[m]->mMidEyeX;
    	mEstMidEyeY += responses[m]*mModel3D[m]->mMidEyeY;
    	mEstMidEyeMouthDist += responses[m]*mModel3D[m]->mMidEyeMouthDist;
    	mEstOrientation += responses[m]*mModel3D[m]->mOrientation;
    	sum += responses[m];
    }
  //	printf("%f ",responses[m]);
  }

  if(sum)
  {
    mEstMidEyeX /= sum;
    mEstMidEyeY /= sum;
    mEstMidEyeMouthDist /= sum;
    mEstOrientation /= sum;
  }
}


void Arclet3DNN::estimateModel(int numEstModel, int wgt1, int wgt2, Image8 faceImage, float detecX, float detecY, 
									  int detecW, int detecH)
{
  if(numEstModel==0)  
  {
    mEstMidEyeX = 30.0;
    mEstMidEyeY = 30.0;
    mEstMidEyeMouthDist = 12.5;
    mEstOrientation = 0.0;
    return;
  }

  int frameWidth = 60;
  int frameHeight = 60;

  Matrix<float> input(2,1), output(2,1);
  Matrix<float> rotation(2,2), center(2,1), frameCenter(2,1);
  Matrix<float> halfPix(2,1);
  halfPix(0,0) = 0.5; halfPix(1,0) = 0.5;

	int detecEndX = ait::round(detecX + detecW);
	int detecEndY = ait::round(detecY + detecH);

  float detecMidFrameX, detecMidFrameY, rot, scale, rotationX, rotationY;
  int faceInFrameX, faceInFrameY;

  detecMidFrameX = 0.5f*(detecX+detecEndX);
  detecMidFrameY = 0.5f*(detecY+detecEndY);
  rot = 0.0f;
  scale = 0.5f*frameWidth/detecW;
  rotationX = detecMidFrameX;
  rotationY = detecMidFrameY - 4.28f/scale;
  faceInFrameX = frameWidth;
  faceInFrameY = frameHeight;
		  
 	// Normalize the images according to the AdaBoost detection window
  Image8 cropImage;
  cropImage.centerRotateRescaleShift(faceImage, -rot, scale, rotationX, rotationY, 
		0.0f, 0.0f, faceInFrameX, faceInFrameY);

  ait::image_proc::smoothImage(faceImage, ait::image_proc::SMOOTH_GAUSSIAN, 3);
//  ait::image_proc::smoothImage(faceImage, ait::image_proc::SMOOTH_GAUSSIAN, 3);
  applyModel(numEstModel, wgt1, wgt2, cropImage);
 
}

void Arclet3DNN::estimateModel(int numEstModel, Image8 faceImage, float detecX, float detecY, 
									  int detecW, int detecH)
{
  estimateModel(numEstModel, 2, 2, faceImage, detecX, detecY, detecW, detecH);	
}
	
void Arclet3DNN::estimateModel(Image8 faceImage, float detecX, float detecY, 
									  int detecW, int detecH)
{
  estimateModel(numModel, faceImage, detecX, detecY, detecW, detecH);

}

void Arclet3DNN::estimateRefined(int numEstModel, Image8 faceImage, Image8& outImage, float X, float Y, float S, float O)
{

  Image8 tempImage(faceImage);

  outImage.centerRotateRescaleShift(tempImage, -O, 12.5f/S, X, Y-4.28f*(S/12.5f), 0.0f, 0.0f, mLsv->img_width, mLsv->img_height);
//  Image8 ttImage(outImage);
//
//  PvImageProc imageProc;
//  imageProc.EqualizeImage(outImage);
//  ait::image_proc::smoothImage(outImage, ait::image_proc::SMOOTH_GAUSSIAN, 3);

  unsigned char *ucImage; 

  ucImage = (unsigned char *) malloc(mLsv->img_height * mLsv->img_height * sizeof(unsigned char));

  int count=0;
	for(int y=0; y<mLsv->img_height; y++)
		for(int x=0; x<mLsv->img_width; x++)
    {
			ucImage[count++] = outImage(x,y);
    }


  if(numEstModel==0)  
  {
    mEstMidEyeX = 30.0;
    mEstMidEyeY = 30.0;
    mEstMidEyeMouthDist = 12.5;
    mEstOrientation = 0.0;

//    outImage.crop(18,23,24,24);
    outImage.crop(15,19,30,30);

    free(ucImage);
    return;
  }

	float *responseVec;
	responseVec = (float *)malloc(mLsv->num_filters*sizeof(float));

  lsv_apply_filters(mLsv, responseVec, ucImage);
	
  vector< float > responses;
  responses.reserve(numEstModel);
  float min, sum;

	 // Model loop
  min = 10.0;
  for(int m = 0; m<numEstModel; m++)
  {
    Sequence inputs(1, numInputs);

  // Normalize the input to NN
    for(int i=0; i < inputs.frame_size; i++)
	  {
      inputs.frames[0][i] = responseVec[i];
//	  printf("%d ",inputData[i]);
	    inputs.frames[0][i] = (inputs.frames[0][i] - mMvNorm[m]->inputs_mean[i]) / mMvNorm[m]->inputs_stdv[i]; 
	  }
		// NN forward
   	mMlp[m]->forward(&inputs);
	
    responses.push_back(mMlp[m]->outputs->frames[0][0]);

  	if(responses[m] < min)
	    min = responses[m];
  }

  free(ucImage);
	free(responseVec);
  
  for(int m = 0; m<numEstModel; m++)
  {
    responses[m] = responses[m] - min;
//	  responses[m] = pow(responses[m], 1 );
  }
  
  sum = 0.0;
  // Estimate
  mEstMidEyeX = 0.0;
  mEstMidEyeY = 0.0;
  mEstMidEyeMouthDist = 0.0;
  mEstOrientation = 0.0;

  for(int m=0; m<numEstModel; m++)
  {
    mEstMidEyeX += responses[m]*mModel3D[m]->mMidEyeX;
    mEstMidEyeY += responses[m]*mModel3D[m]->mMidEyeY;
    mEstMidEyeMouthDist += responses[m]*mModel3D[m]->mMidEyeMouthDist;
    mEstOrientation += responses[m]*mModel3D[m]->mOrientation;
    sum += responses[m];
  }

  if(sum)
  {
    mEstMidEyeX /= sum;
    mEstMidEyeY /= sum;
    mEstMidEyeMouthDist /= sum; 
    mEstOrientation /= sum; 
  }

  tempImage = outImage;
  outImage.centerRotateRescaleShift(tempImage, -mEstOrientation, 12.5f/mEstMidEyeMouthDist, 
    mEstMidEyeX, mEstMidEyeY-0*4.28f*(mEstMidEyeMouthDist/12.5f), 0.0f, 0.0f, mLsv->img_width, mLsv->img_height);

//  outImage.crop(18,23,24,24);
  outImage.crop(15,19,30,30);
}

} // namespace ait

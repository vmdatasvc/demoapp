//#include "facial_geometry_estimator/SparseGeometricNN.hpp"
#include "face_classifier/GenderTorchNN.hpp"
#include "PvUtil.hpp"
#include "image_proc.hpp"

using namespace std;
using namespace Torch;

namespace ait
{

  // Constructor
void GenderTorchNN::init(string mModelFile_, int imgWidth_, int imgHeight_)
{
  Allocator *allocator = new Allocator;
 
  get_args();

  mModelFile = mModelFile_;
	imgWidth = imgWidth_;
	imgHeight = imgHeight_;
//	numInputs = imgWidth * imgHeight;

  DiskXFile::setLittleEndianMode();

  DiskXFile model(mModelFile.c_str(), "r");

//  mMeanVar.reserve(1);
  
  mCmd.loadXFile(&model);

  Linear *c1 = new(allocator) Linear(numInputs, numHiddenUnits1, &mRandom);
  c1->setROption("weight decay", mWeightdecay);
  Tanh*c2 = new(allocator) Tanh(numHiddenUnits1);
  Linear *c3 = new(allocator) Linear(numHiddenUnits1, numHiddenUnits2, &mRandom);
  c3->setROption("weight decay", mWeightdecay);
  Tanh *c4 = new(allocator) Tanh(numHiddenUnits2);
  Linear *c5 = new(allocator) Linear(numHiddenUnits2, numHiddenUnits3, &mRandom);
  c5->setROption("weight decay", mWeightdecay);
  Tanh *c6 = new(allocator) Tanh(numHiddenUnits3);
  Linear *c7 = new(allocator) Linear(numHiddenUnits3, numTargets, &mRandom);
  c7->setROption("weight decay", mWeightdecay);
  LogSoftMax *c8 = new(allocator) LogSoftMax(2);

  mMlp.addFCL(c1);    
  mMlp.addFCL(c2);
  mMlp.addFCL(c3);
  mMlp.addFCL(c4);
  mMlp.addFCL(c5);
  mMlp.addFCL(c6);
  mMlp.addFCL(c7);
  mMlp.addFCL(c8);

  mMlp.build();
  mMlp.setPartialBackprop();

//  MeanVarNorm *mMeanVar;
	mMeanVar = new(allocator) MeanVarNorm(numInputs, numTargets);
  mMeanVar->loadXFile(&model);

  mMlp.loadXFile(&model);    

  printf("%s loaded.\n",mModelFile.c_str());

}

// Torch3 interface to read Torch style trained model file
// It seems that the unnecessary options were added from training; 
// need to remove these within the training code
void GenderTorchNN::get_args()
{
  //  char *modelListFile, *mModelDir, *imageFile;
  char *valid_file;
  char *file;

  int max_load;
  int max_load_valid;
  real accuracy;
  real learning_rate;
  real decay;
  int max_iter;
  int the_seed;

  char *dir_name;
  char *model_file;
  int k_fold;
  bool binary_mode;
  bool regression_mode;
  int class_against_the_others;

  // Put the help line at the beginning
//  mCmd.info(help1);

  // Train mode
  mCmd.addText("\nArguments:");
  mCmd.addSCmdArg("file", &file, "the train file");
  mCmd.addICmdArg("n_inputs", &numInputs, "input dimension of the data", true);
  mCmd.addICmdArg("n_targets", &numTargets, "output dim. (regression) or # of classes (classification)", true);

  mCmd.addText("\nModel Options:");
  mCmd.addICmdOption("-class", &class_against_the_others, -1, "train the given class against the others", true);
  mCmd.addICmdOption("-nhu1", &numHiddenUnits1, 25, "number of hidden units", true);
  mCmd.addICmdOption("-nhu2", &numHiddenUnits2, 50, "number of hidden units", true);
  mCmd.addICmdOption("-nhu3", &numHiddenUnits3, 50, "number of hidden units", true);
  mCmd.addBCmdOption("-rm", &regression_mode, false, "regression mode ?", true);

  mCmd.addText("\nLearning Options:");
  mCmd.addICmdOption("-iter", &max_iter, 25, "max number of iterations");
  mCmd.addRCmdOption("-lr", &learning_rate, 0.01, "learning rate");
  mCmd.addRCmdOption("-e", &accuracy, 0.00001, "end accuracy");
  mCmd.addRCmdOption("-lrd", &decay, 0, "learning rate decay");
  mCmd.addICmdOption("-kfold", &k_fold, -1, "number of folds, if you want to do cross-validation");
  mCmd.addRCmdOption("-wd", &mWeightdecay, 0, "weight decay", true);

  mCmd.addText("\nMisc Options:");
  mCmd.addICmdOption("-seed", &the_seed, -1, "the random seed");
  mCmd.addICmdOption("-load", &max_load, -1, "max number of examples to load for train");
  mCmd.addICmdOption("-load_valid", &max_load_valid, -1, "max number of examples to load for valid");
  mCmd.addSCmdOption("-valid", &valid_file, "", "validation file, if you want it");
  mCmd.addSCmdOption("-dir", &dir_name, ".", "directory to save measures");
  mCmd.addSCmdOption("-save", &model_file, "", "the model file");
  mCmd.addBCmdOption("-bin", &binary_mode, false, "binary mode for files");

  // Test mode
  mCmd.addMasterSwitch("--test");
  mCmd.addText("\nArguments:");

  mCmd.addText("\nMisc Options:");
  mCmd.addICmdOption("-load", &max_load, -1, "max number of examples to load for train");
  mCmd.addSCmdOption("-dir", &dir_name, ".", "directory to save measures");
  mCmd.addBCmdOption("-bin", &binary_mode, false, "binary mode for files");

  // Read the command line
  int argc = 2;
  char **argv;
  argv = (char **)malloc(2*sizeof(char *));
  argv[0] = (char *)malloc(11*sizeof(char));
  argv[1] = (char *)malloc(7*sizeof(char));
  strcpy(argv[0], "executable");
  strcpy(argv[1], "--test");
//  argv[0] = "executable";
//  argv[1] = "--test";
  int mode = mCmd.read(argc, argv);
}

float GenderTorchNN::classify(Image8 faceImage, Image8& outImage, float X, float Y, float S, float O)
{
  Image8 tempImage(faceImage);

  outImage.centerRotateRescaleShift(tempImage, -O, 12.5/S, X, Y-4.28*(S/12.5), 0, -4.28, imgWidth, imgHeight);
  Image8 ttImage(outImage);
  outImage.Lequalize(ttImage,12); //12 for 30x30

  unsigned char *inputData;

  inputData = (unsigned char *) malloc(imgWidth*imgHeight * sizeof(unsigned char));
  int count=0;
  for(int y=0; y<imgHeight; y++)
    for(int x=0; x<imgWidth; x++)
      inputData[count++] = outImage(x,y);

  Sequence inputs(1, numInputs);

// Normalize the input to NN
  for(int i=0; i < inputs.frame_size; i++)
	{
      inputs.frames[0][i] = (float)inputData[i];
//  	  printf("%f:(%f,%f) ",inputs.frames[0][i],mMeanVar->inputs_mean[i],mMeanVar->inputs_stdv[i]);
      inputs.frames[0][i] = (inputs.frames[0][i] - mMeanVar->inputs_mean[i]) / mMeanVar->inputs_stdv[i]; 
  } 
  free(inputData);
   
		// NN forward
  mMlp.forward(&inputs);
    
  mFemaleScore = mMlp.outputs->frames[0][0];
  mMaleScore = mMlp.outputs->frames[0][1];
  float score = mMaleScore - mFemaleScore;
//  printf("maleScore = %f femaleScore = %f score = %f\n",mMaleScore, mFemaleScore, score);

  return score;
}

} // namespace ait
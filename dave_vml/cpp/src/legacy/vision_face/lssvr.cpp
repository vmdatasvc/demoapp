
#include <math.h>
#include "legacy/vision_face/lssvr.hpp"

#include "legacy/vision_face/lssvr_tools.hpp"

long idum=-1;

static void apply_filters( Lsv *lsv, float *val, unsigned char *img )
{
  LsvFilter *filter;
  int filter_num;
  float v, av;
  unsigned char *img_row;
  float *filter_row;
  int x, y;
  float *sum;
  int col, row;
  int num_features;

  num_features = lsv->num_features;

  sum = (float *)malloc(num_features*sizeof(float));
  int n;
  for(n=0;n<num_features;n++)
    sum[n]=0.0;

  for( filter_num = lsv->num_filters - 1, filter = lsv->filter;
       filter;
       filter_num--, filter = filter->next )
  {
    av = 0;
    for( img_row = img + filter->xy0, filter_row = filter->weight, y = 0;
         y < filter->height;
         img_row += lsv->img_width, filter_row += filter->width, y++ )
      for( x = 0; x < filter->width; x++ )
	{
	  // for 80x80
	  col = filter->xy0%lsv->img_width + x;
	  row = (int)(filter->xy0/lsv->img_width) + y;
//	    if( 24 <= col && col < 104 && 24 <= row && row < 104 )
    av += img_row[ x ];
	}
    av /= (filter->width*filter->height);

    v = 0;
    for( img_row = img + filter->xy0, filter_row = filter->weight, y = 0;
         y < filter->height;
         img_row += lsv->img_width, filter_row += filter->width, y++ )
      for( x = 0; x < filter->width; x++ )
	{
	  // for 80x80
 	  col = filter->xy0%lsv->img_width + x;
 	  row = (int)(filter->xy0/lsv->img_width) + y;
//	    if( 24 <= col && col < 104 && 24 <= row && row < 104 )
    v += filter_row[ x ] * ( img_row[ x ] - av );

	    //	  if(filter_num==0)
//	    	    printf(" %d %d %d img=%d filter = %f av = %f corel=%f\n",filter->xy0,col,row,img[row*lsv->img_width + col],filter_row[ x ],av, v); 
	}
    val[ filter_num ] = (float)fabs(v);

    sum[filter_num%num_features] += (float)fabs(v);

//    printf(" %d %d %d img=%d corel=%f\n",filter->xy0,col,row,img[row*lsv->img_width + col], v); 
   
//    int i;

    //     for(i=0; i<num_features; i++)
    //       {
    // 	printf("%f ",sum[i]);
    //       }
    //     printf("\n");
  }
  
   int i;
   for(i=0; i<num_features; i++)
     {
       sum[i] /= (lsv->num_filters/num_features);
       if( sum[i] == 0.0 )
	 sum[i] = 1.0;
     }
   for( filter_num = 0; filter_num < lsv->num_filters; filter_num++)
     val[filter_num] /= sum[filter_num%10];

   free(sum);
}

static void apply_filters_at( Lsv *lsv, float *val, unsigned char *img, int width, int height, int X, int Y )
{
  LsvFilter *filter;
  int filter_num;
  float v, av;
  unsigned char *img_row;
  float *filter_row;
  int x, y;
    float *sum;
  int col, row;
  int img_XY0;
  int x0, y0;
  int num_features;

  num_features = lsv->num_features;
   sum = (float *)malloc(num_features*sizeof(float));
  int n;
  for(n=0;n<num_features;n++)
    sum[n]=0.0;

  for( filter_num = lsv->num_filters - 1, filter = lsv->filter;
       filter;
       filter_num--, filter = filter->next )
  {
    y0 = (int)(filter->xy0 / lsv->img_width);
    x0 =  filter->xy0 % lsv->img_width;
    
    img_XY0 = (Y + y0) * width + X + x0;
    
//     printf("filter->xy0=%d\n",filter->xy0);
//     printf("X=%d Y=%d width=%d height=%d lsv->img_width=%d x=%d y0=%d img_XY0=%d\n",X,Y,width,height,lsv->img_width,x0,y0,img_XY0);
    
    av = 0;
    for( img_row = img + img_XY0, filter_row = filter->weight, y = 0;
         y < filter->height;
         img_row += width, filter_row += filter->width, y++ )
      for( x = 0; x < filter->width; x++ )
	{
	  // for 80x80
// 	  col = xy0%width + x;
// 	  row = (int)(xy0/width) + y;
// 	  if( 24 <= col && col < 104 && 24 <= row && row < 104 )
	    av += img_row[ x ];
	}
    av /= (filter->width*filter->height);

    v = 0;
    for( img_row = img + img_XY0, filter_row = filter->weight, y = 0;
         y < filter->height;
         img_row += width, filter_row += filter->width, y++ )
      for( x = 0; x < filter->width; x++ )
	{
	  // for 80x80
 	  col = X + x0 + x;
 	  row = Y + y0 + y;
// 	  if( 24 <= col && col < 104 && 24 <= row && row < 104 )
	    v += filter_row[ x ] * ( img_row[ x ] - av );

	    // 	    if(1)//filter_num==0)
	    //  printf(" %d %d %d img=%d filter = %f av = %f corel=%f\n",xy0,col,row,img[row*width + col],filter_row[ x ],av, v); 
	}
    val[ filter_num ] = (float)fabs(v);

    sum[filter_num%num_features] += (float)fabs(v);

    //        printf("%d %d img=%d corel=%f\n",col,row,img[row*width + col], v); 
   
//    int i;

    //     for(i=0; i<num_features; i++)
    //   {
    // 	printf("%f ",sum[i]);
    //  }
    // printf("\n");
  }
  
   int i;
   for(i=0; i<num_features; i++)
     {
       sum[i] /= (lsv->num_filters/num_features);
       if( sum[i] == 0.0 )
	 sum[i] = 1.0;
     }
   for( filter_num = 0; filter_num < lsv->num_filters; filter_num++)
     val[filter_num] /= sum[filter_num%num_features];

   free(sum);
}

static float get_negdist2( float *val0, float *val1, int len )
{
  float negdist2;
  float d;
  int i;

  negdist2 = 0;
  for( i = 0; i < len; i++ )
  {
    d = val0[ i ] - val1[ i ];
    negdist2 -= d * d;
  }

  return negdist2;
}

static int recursive_write_filter( FILE *fp, LsvFilter *filter )
{
  int num_written = 0;

  if( filter == 0 )
    goto cleanup;

  num_written = recursive_write_filter( fp, filter->next );
  if( lsv_err() ) goto err;

  WRITE( fp, &filter->xy0 )
  WRITE( fp, &filter->width )
  WRITE( fp, &filter->height )
  WRITE_ARRAY( fp, filter->weight, filter->width * filter->height )
  num_written++;

  goto cleanup;
  err:
    ;
  cleanup:
    ;

  return num_written;
}

static int recursive_write_vector( FILE *fp, LsvVector *vector, Lsv *lsv )
{
  int num_written = 0;

  if( vector == 0 )
    goto cleanup;

  num_written = recursive_write_vector( fp, vector->next, lsv );
  if( lsv_err() ) goto err;

  WRITE_ARRAY( fp, vector->alpha, lsv->num_outputs )
  WRITE_ARRAY( fp, vector->val, lsv->num_filters )
  num_written++;

  goto cleanup;
  err:
    ;
  cleanup:
    ;

  return num_written;
}

const char *lsv_err( void )
{
  return *g_err ? g_err : 0;
}

void lsv_reset_err( void )
{
  *g_err = 0;
}

Lsv *lsv_new( int img_width, int img_height, int num_outputs, int num_features )
{
  Lsv *lsv = 0;
  int i;

  if( img_width < 1 || img_height < 1 )
  {
    sprintf( g_err, "Illegal image size (%d x %d) in lsv_new()",
                    img_width, img_height );
    goto err;
  }
  if( num_outputs < 1 )
  {
    sprintf( g_err, "Illegal number of outputs (%d) in lsv_new()",
                    num_outputs );
    goto err;
  }

  //  NEW( lsv )
  lsv=(Lsv *)malloc(sizeof(*lsv));
  memset(lsv, 0, sizeof(*lsv));

  lsv->img_width = img_width;
  lsv->img_height = img_height;
  lsv->num_outputs = num_outputs;
  lsv->num_features = num_features;
  lsv->offset = (float *)malloc(num_outputs*sizeof(*lsv->offset));
  memset(lsv->offset, 0, num_outputs*sizeof(*lsv->offset));

  lsv->sigma2 = (float *)malloc(num_outputs*sizeof(*lsv->sigma2));
  memset(lsv->sigma2, 0, num_outputs*sizeof(*lsv->sigma2));
  //  NEW_ARRAY( lsv->offset, num_outputs );
  //  NEW_ARRAY( lsv->sigma2, num_outputs );

  for( i = 0; i < num_outputs; i++ )
    lsv->sigma2[ i ] = 1;

  goto cleanup;
  err:
    lsv_delete( lsv ); lsv = 0;
  cleanup:
    ;

  return lsv;
}

void lsv_delete( Lsv *lsv )
{
  LsvFilter *filter, *next_filter;
  LsvVector *vector, *next_vector;

  if( lsv )
  {
    for( filter = lsv->filter; filter; filter = next_filter )
    {
      next_filter = filter->next;
      DELETE( filter->weight )
      DELETE( filter )
    }

    for( vector = lsv->vector; vector; vector = next_vector )
    {
      next_vector = vector->next;
      DELETE( vector->alpha )
      DELETE( vector->val )
      DELETE( vector )
    }

    DELETE( lsv->offset );
    DELETE( lsv->sigma2 );
    DELETE( lsv )
  }

  goto cleanup;
  err:
    ;
  cleanup:
    ;
}

void lsv_add_filter( Lsv *lsv,
                     int x0, int y0, int width, int height,
                     float *weight )
{
  LsvFilter *filter = 0;

  //printf("inside add_filter: width=%d height=%d x0=%d y0=%d %f\n",width,height,x0,y0,weight[0]);

if( lsv == 0 )
  {
    sprintf( g_err, "Null LSV pointer in lsv_add_filter()" );
    goto err;
  }
  if( 0 > x0 || x0 + width > lsv->img_width ||
      0 > y0 || y0 + height > lsv->img_height )
  {
    sprintf( g_err, "Filter window (<%d, %d> %d x %d) goes off image"
                    " in lsv_add_filter() (image size is %d x %d)",
                    x0, y0, width, height,
                    lsv->img_width, lsv->img_height );
    goto err;
  }
  if( weight == 0 )
  {
    sprintf( g_err, "Null weight pointer in lsv_add_filter()" );
    goto err;
  }
  if( lsv->num_vectors > 0 )
  {
    sprintf( g_err, "Trying to add filters after adding vectors"
                    " (not allowed)" );
    goto err;
  }

  filter = (LsvFilter *)malloc(sizeof(*filter));
  memset(filter, 0, sizeof(*filter));
  filter->weight = (float *)malloc(width * height*sizeof(*filter->weight));
  memset(filter->weight, 0, width * height*sizeof(*filter->weight));

  //NEW( filter )
  //  NEW_ARRAY( filter->weight, width * height )

  filter->xy0 = y0 * lsv->img_width + x0;
  filter->width = width;
  filter->height = height;
  memcpy( filter->weight, weight, width * height * sizeof( *filter->weight ) );

  //  printf("inside add_filter: width=%d height=%d x0=%d y0=%d xy0=%d lsv->num_filters=%d\n",width,height,x0,y0,filter->xy0,lsv->num_filters);


/*   float sum = 0.0; */
/*   int i; */
/*   for( i = 0; i < width * height ; i ++) */
/*     { */
/*       sum += filter->weight[i]; */
/*       printf("sum =%f ",sum); */
/*     } */
/*   sum /= width*height; */

/*   printf("sum=%f\n",sum); */

/*   for( i = 0; i < width * height ; i ++) */
/*     filter->weight[i] -= sum; */


  filter->next = lsv->filter;
  lsv->filter = filter;
  lsv->num_filters++;

//   int i;
//   if(lsv->num_filters==5000)
//   for( i = 0; i < width * height ; i ++)
//     {
// 	printf("%d %f\n",i,filter->weight[i]);
//     }


  goto cleanup;
  err:
    if( filter )
    {
      if( lsv->filter == filter )
        lsv->filter = filter->next;
      DELETE( filter->weight )
      DELETE( filter )
    }
  cleanup:
    ;
}

void lsv_add_vector( Lsv *lsv, int out_num, float alpha, float *val )
{
  float *alpha_array = 0;
  LsvVector *vector;
  int i;

  if( lsv == 0 )
  {
    sprintf( g_err, "Null LSV pointer in lsv_add_vector()" );
    goto err;
  }
  if( 0 > out_num || out_num >= lsv->num_filters )
  {
    sprintf( g_err, "Illegal out_num (%d) in lsv_add_vector()"
                    " (max is %d)",
                    out_num, lsv->num_filters - 1 );
    goto err;
  }
  if( val == 0 )
  {
    sprintf( g_err, "Null val pointer in lsv_add_vector()" );
    goto err;
  }
  if( lsv->num_filters < 1 )
  {
    sprintf( g_err, "Trying to add a vector before adding any filters"
                    " (not allowed)" );
    goto err;
  }

  for( vector = lsv->vector; vector; vector = vector->next )
  {
    for( i = 0; i < lsv->num_filters; i++ )
      if( vector->val[ i ] != val[ i ] )
        break;
    if( i == lsv->num_filters )
      break;
  }

  if( vector )
    vector->alpha[ out_num ] = alpha;
  else
  {
    //    NEW_ARRAY( alpha_array, lsv->num_outputs );
    alpha_array = (float *)malloc(lsv->num_outputs*sizeof(*alpha_array));
    memset(alpha_array, 0, lsv->num_outputs*sizeof(*alpha_array));
    alpha_array[ out_num ] = alpha;
    lsv_add_allout_vector( lsv, alpha_array, val );
    if( lsv_err() ) goto err;
  }

  goto cleanup;
  err:
    ;
  cleanup:
    DELETE( alpha_array );
}

void lsv_add_allout_vector( Lsv *lsv, float *alpha, float *val )
{
  LsvVector *vector = 0;

  if( lsv == 0 )
  {
    sprintf( g_err, "Null LSV pointer in lsv_add_allout_vector()" );
    goto err;
  }
  if( alpha == 0 )
  {
    sprintf( g_err, "Null alpha pointer in lsv_add_allout_vector()" );
    goto err;
  }
  if( val == 0 )
  {
    sprintf( g_err, "Null val pointer in lsv_add_allout_vector()" );
    goto err;
  }
  if( lsv->num_filters < 1 )
  {
    sprintf( g_err, "Trying to add a vector before adding any filters"
                    " (not allowed)" );
    goto err;
  }

  //  NEW( vector )
  //  NEW_ARRAY( vector->alpha, lsv->num_outputs )
  //  NEW_ARRAY( vector->val, lsv->num_filters )

  vector = (LsvVector *)malloc(sizeof(*vector));
  memset(vector, 0, sizeof(*vector));
  vector->alpha = (float *)malloc(lsv->num_outputs*sizeof(*vector->alpha));
  memset(vector->alpha, 0, lsv->num_outputs*sizeof(*vector->alpha));
  vector->val = (float *)malloc(lsv->num_filters*sizeof(*vector->val));
  memset(vector->val, 0, lsv->num_filters*sizeof(*vector->val));

  memcpy( vector->alpha, alpha, lsv->num_outputs * sizeof( *vector->alpha ) );
  memcpy( vector->val, val, lsv->num_filters * sizeof( vector->val ) );

  vector->next = lsv->vector;
  lsv->vector = vector;
  lsv->num_vectors++;

  goto cleanup;
  err:
    if( vector )
    {
      if( lsv->vector == vector )
        lsv->vector = vector->next;
      DELETE( vector->alpha )
      DELETE( vector->val )
      DELETE( vector )
    }
  cleanup:
    ;
}

void lsv_set_sigma( Lsv *lsv, int out_num, float sigma )
{
  if( lsv == 0 )
  {
    sprintf( g_err, "Null LSV pointer in lsv_set_sigma()" );
    goto err;
  }
  if( 0 > out_num || out_num >= lsv->num_filters )
  {
    sprintf( g_err, "Illegal out_num (%d) in lsv_set_sigma()"
                    " (max is %d)",
                    out_num, lsv->num_filters - 1 );
    goto err;
  }

  lsv->sigma2[ out_num ] = sigma * sigma;

  goto cleanup;
  err:
    ;
  cleanup:
    ;
}

void lsv_set_offset( Lsv *lsv, int out_num, float offset )
{
  if( lsv == 0 )
  {
    sprintf( g_err, "Null LSV pointer in lsv_set_sigma()" );
    goto err;
  }
  if( 0 > out_num || out_num >= lsv->num_filters )
  {
    sprintf( g_err, "Illegal out_num (%d) in lsv_set_sigma()"
                    " (max is %d)",
                    out_num, lsv->num_filters - 1 );
    goto err;
  }

  lsv->offset[ out_num ] = offset;

  goto cleanup;
  err:
    ;
  cleanup:
    ;
}

void lsv_apply_filters( Lsv *lsv, float *val, unsigned char *img )
{
  if( lsv == 0 )
  {
    sprintf( g_err, "Null LSV pointer in lsv_apply_filters()" );
    goto err;
  }
  if( val == 0 )
  {
    sprintf( g_err, "Null val pointer in lsv_apply_filters()" );
    goto err;
  }
  if( img == 0 )
  {
    sprintf( g_err, "Null img pointer in lsv_apply_filters()" );
    goto err;
  }

  apply_filters( lsv, val, img );

  goto cleanup;
  err:
    ;
  cleanup:
    ;
}

void lsv_apply_filters_at( Lsv *lsv, float *val, unsigned char *img, int width, int height, int startX, int startY )
{
  if( lsv == 0 )
  {
    sprintf( g_err, "Null LSV pointer in lsv_apply_filters()" );
    goto err;
  }
  if( val == 0 )
  {
    sprintf( g_err, "Null val pointer in lsv_apply_filters()" );
    goto err;
  }
  if( img == 0 )
  {
    sprintf( g_err, "Null img pointer in lsv_apply_filters()" );
    goto err;
  }

  apply_filters_at( lsv, val, img, width, height, startX, startY );

  goto cleanup;
  err:
    ;
  cleanup:
    ;
}

void lsv_apply( Lsv *lsv, float *outputs, unsigned char *img )
{
  float *val = 0;
  LsvVector *vector;
  float d;
  int i;

  if( lsv == 0 )
  {
    sprintf( g_err, "Null LSV pointer in lsv_apply()" );
    goto err;
  }
  if( outputs == 0 )
  {
    sprintf( g_err, "Null outputs pointer in lsv_apply()" );
    goto err;
  }
  if( img == 0 )
  {
    sprintf( g_err, "Null img pointer in lsv_apply()" );
    goto err;
  }

  //  NEW_ARRAY( val, lsv->num_filters )

  val = (float *)malloc(sizeof(*val));
  memset(val, 0, sizeof(*val));
  apply_filters( lsv, val, img );

  memcpy( outputs, lsv->offset, lsv->num_outputs * sizeof( *outputs ) );
  for( vector = lsv->vector; vector; vector = vector->next )
  {
    d = get_negdist2( vector->val, val, lsv->num_filters );
    for( i = 0; i < lsv->num_outputs; i++ )
      outputs[ i ] += vector->alpha[ i ] * exp( d / lsv->sigma2[ i ] );
  }

  goto cleanup;
  err:
    ;
  cleanup:
    DELETE( val )
}

void lsv_write( Lsv *lsv, const char *file )
{
  FILE *fp = 0;
  LsvFilter *filter;
  int size, max_size;
  int num_written;

  if( lsv == 0 )
  {
    sprintf( g_err, "Null LSV pointer in lsv_write()" );
    goto err;
  }
  if( file == 0 )
  {
    sprintf( g_err, "Null file pointer in lsv_write()" );
    goto err;
  }

  max_size = 0;
  for( filter = lsv->filter; filter; filter = filter->next )
  {
    size = filter->width * filter->height;
    if( max_size < size )
      max_size = size;
  }

  //  printf("width=%d height=%d outputs=%d filters=%d vectors=%d max=%d\n",lsv->img_width,lsv->img_height, lsv->num_outputs,lsv->num_filters,lsv->num_vectors,max_size); 

  CREATE( fp, file )
  WRITE( fp, &lsv->img_width )
  WRITE( fp, &lsv->img_height )
  WRITE( fp, &lsv->num_outputs )
  WRITE( fp, &lsv->num_filters )
  WRITE( fp, &lsv->num_features )
  WRITE( fp, &lsv->num_vectors )
  WRITE( fp, &max_size )

  num_written = recursive_write_filter( fp, lsv->filter );
  if( lsv_err() ) goto err;
  if( num_written != lsv->num_filters )
  {
    sprintf( g_err, "Wrote wrong number of filters in lsv_write()"
                    " (should write %d, but wrote %d)",
                    lsv->num_filters, num_written );
    goto err;
  }

  if(lsv->num_filters)
    {
      WRITE_ARRAY( fp, lsv->offset, lsv->num_outputs )
	WRITE_ARRAY( fp, lsv->sigma2, lsv->num_outputs )
	
	num_written = recursive_write_vector( fp, lsv->vector, lsv );
      if( lsv_err() ) goto err;
      if( num_written != lsv->num_vectors )
	{
	  sprintf( g_err, "Wrote wrong number of vectors in lsv_write()"
		   " (should write %d, but wrote %d)",
		   lsv->num_vectors, num_written );
	  goto err;
	}
    }

  goto cleanup;
  err:
    ;
  cleanup:
    CLOSE( fp )
}

Lsv *lsv_read( const char *file )
{
  Lsv *lsv = 0;
  FILE *fp = 0;
  float *alpha = 0;
  float *val = 0;
  float *weight = 0;
  int img_width;
  int img_height;
  int num_outputs;
  int num_filters;
  int num_features;
  int num_vectors;
  int max_size;
  int xy0, x0, y0;
  int width;
  int height;
  int size;
  int i;

  if( file == 0 )
  {
    sprintf( g_err, "Null file pointer in lsv_read()" );
    goto err;
  }

  OPEN( fp, file )
  READ( fp, &img_width )
  READ( fp, &img_height )
  READ( fp, &num_outputs )
  READ( fp, &num_filters )
  READ( fp, &num_features )
  READ( fp, &num_vectors )
  READ( fp, &max_size )

    //printf("img_width=%d img_height=%d num_filters=%d num_features=%d num_vectors=%d max_size=%d\n",img_width,img_height,num_filters,num_features,num_vectors,max_size);
 
  lsv = lsv_new( img_width, img_height, num_outputs, num_features );
  if( lsv_err() )
  {
    sprintf( g_err + strlen( g_err ), " [while reading '%s']", file );
    goto err;
  }
  if( num_filters < 1 )
  {
    sprintf( g_err, "Illegal number of filters (%d) in file '%s'",
                    num_filters, file );
    goto err;
  }
  if( num_vectors < 0 )
  {
    sprintf( g_err, "Illegal number of vectors (%d) in file '%s'",
                    num_vectors, file );
    goto err;
  }
  if( max_size < 1 )
  {
    sprintf( g_err, "Illegal maximum filter size (%d) in file '%s'",
                    max_size, file );
    goto err;
  }

  //  NEW_ARRAY( weight, max_size )
  weight = (float *)malloc(max_size*sizeof(*weight));
  memset(weight, 0, max_size*sizeof(*weight));

  for( i = 0; i < num_filters; i++ )
  {
    READ( fp, &xy0 )
    READ( fp, &width )
    READ( fp, &height )

      //    printf("inside add_filter: xy0=%d width=%d height=%d\n",xy0,width,height);

    size = width * height;
    if( size > max_size )
    {
      sprintf( g_err, "Bad filter size in '%s'"
                      " (filter %d has size %d, but header said max is %d)",
                      file, i, size, max_size );
      goto err;
    }

    READ_ARRAY( fp, weight, size )

    x0 = xy0 % img_width;
    y0 = xy0 / img_width;
    lsv_add_filter( lsv, x0, y0, width, height, weight );
    if( lsv_err() )
      {
      sprintf( g_err + strlen( g_err ), " [while reading '%s']", file );
      goto err;
      }
  }

  if(num_vectors)
    {
      READ_ARRAY( fp, lsv->offset, num_outputs )
	READ_ARRAY( fp, lsv->sigma2, num_outputs )
	
	//  NEW_ARRAY( alpha, num_outputs )
	//	NEW_ARRAY( val, num_filters )

	alpha = (float *)malloc(num_outputs*sizeof(*alpha));
	memset(alpha, 0, num_outputs*sizeof(*alpha));
	val = (float *)malloc(num_filters*sizeof(*val));
	memset(val, 0, num_filters*sizeof(*val));

	for( i = 0; i < num_vectors; i++ )
	  {
	    READ_ARRAY( fp, alpha, num_outputs )
	      READ_ARRAY( fp, val, num_filters )
	      
	      lsv_add_allout_vector( lsv, alpha, val );
	    if( lsv_err() )
	      {
		sprintf( g_err + strlen( g_err ), " [while reading '%s']", file );
		goto err;
	      }
	  }
    }

  goto cleanup;
  err:
    lsv_delete( lsv ); lsv = 0;
  cleanup:
    DELETE( alpha )
    DELETE( val )
    DELETE( weight )
    CLOSE( fp )

  return lsv;
}

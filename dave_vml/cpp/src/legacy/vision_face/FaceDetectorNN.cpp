/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "legacy/vision_face/FaceDetectorNN.hpp"
#include "legacy/vision_face/NeuralNetFaceDetector.hpp"

//#include "detect.h"

//#include "img.hpp"
#include "legacy/vision_tools/PvImageProc.hpp"

#include "legacy/low_level/Settings.hpp"

namespace ait
{

 /*********************************************************************
 * CONSTRUCTOR
*********************************************************************/
FaceDetectorNN::FaceDetectorNN(void) : isInitialized(false), 
									   equalizeImage(false)
{
	initialize();

  // The minimum size for the Neural Network face detector is 30x30.
  mTrainingMinSize.set(30,30);
}

/*********************************************************************
* DESTRUCTOR
*********************************************************************/
FaceDetectorNN::~FaceDetectorNN(void)
{
	unInitialize();
}

/*********************************************************************
* initialize()
*
* Do initialization. Whatever is necessary.
*********************************************************************/
void FaceDetectorNN::initialize(void)
{
	if(isInitialized) unInitialize();
	
  mpFaceDetector.reset(new NeuralNetFaceDetector());
	mpFaceDetector->init();

	isInitialized=true;
}

/*********************************************************************
* unInitialize()
* 
* Do un-initialization. Whatever is necessary.
*********************************************************************/
void FaceDetectorNN::unInitialize(void)
{
	if(!isInitialized) return;
	
	// code here (?)
	mpFaceDetector->finalize();

  mpFaceDetector.reset();
	
	isInitialized=false;
}

/*********************************************************************
* detect()
* 
* <input>  - Gray Level image.
*			  Input image must be at least 20x20 pixels.
* 
* Image8 &image:     image within which to detect faces 
*
* <output>  _ Loads FaceArray in Base class with locations of all
*				Faces found.  Use face(i) to get the ith face.
*
* Information stored in nFaces and faceArray[]. See base class.
*********************************************************************/
void FaceDetectorNN::detect(const Image8 &image,bool asynch)
{
	int w,h,i;
	unsigned char *pixels = NULL;
	
	w=image.width();
	h=image.height();
	
	
	assert(w>0);
	assert(h>0);
	
	// Check dimensions.  Must be at least 20 for both dimensions.
	if (w<20 || h<20)
  {
    nFaces = 0;
    return;
  }
	
	//Face location structure.
  struct NeuralNetFaceDetector::FaceEyeLocation *locArray = NULL;



	//If equalizeImage is true then equalize this image.
	//if (equalizeImage)
		//PvImageProc::EqualizeImage(image);



	//Get out the pointer to data.

	pixels=image.pointer();

	assert(pixels!=NULL);

  // Set the inclusion mask for this face detector.
  mpFaceDetector->setInclusionIntegralImageMask(mpMaskIntegralImage,mMaskAreaFraction);

	// Optimization for reducing the search space given to the face detector.
  Image8 subsampledImage;
  float resampleFactor = resampleForMinSize(image,subsampledImage);

  if (resampleFactor < 1.0f)
  {
    nFaces=mpFaceDetector->Track_FindAllFaces(subsampledImage,&locArray,asynch ? 1 : 0);

    // Now we have to scan the array and adjust the sizes so it fits the original image.
    for (int i = 0; i < nFaces; i++)
    {
      locArray[i].x1 = (int)(locArray[i].x1/resampleFactor);
      locArray[i].y1 = (int)(locArray[i].y1/resampleFactor);
      locArray[i].x2 = (int)(locArray[i].x2/resampleFactor);
      locArray[i].y2 = (int)(locArray[i].y2/resampleFactor);
    }
  }
  else
  {
    // Just operate on the original image.
    nFaces=mpFaceDetector->Track_FindAllFaces(const_cast<Image8&>(image),&locArray,asynch ? 1 : 0);
  }

	assert(nFaces <= FD_MAX_NR_OF_FACES);
	
	unsigned int j=0;

  i=0;
	//For each face found, load up FaceArray.
	while(i<nFaces)
	{
		//Load face array.
		faceArray[j].x = locArray[i].x1;
		faceArray[j].y = locArray[i].y1;
		//faceArray[j].x2 = locArray[i].x2;
		//faceArray[j].y2 = locArray[i].y2;
		faceArray[j].w = locArray[i].x2 - locArray[i].x1+1;
		faceArray[j].h = locArray[i].y2 - locArray[i].y1+1;

    if(locArray[i].x1>0 && locArray[i].y1>0 && 
      locArray[i].x2 - locArray[i].x1+1>0 &&
      locArray[i].y2 - locArray[i].y1+1>0)
    {
      if(faceArray[j].x+faceArray[j].w<image.width() &&
        faceArray[j].y+faceArray[j].h<image.height())
      {
        j++;
      }
      else
      {
        //PVMSG("Error, %d %d %d %d\n",faceArray[j].x,faceArray[j].y,faceArray[j].x+faceArray[j].w,faceArray[j].y+faceArray[j].h);
        //image.save("input.pgm");
      }
    }
		
    i++;
	}
  nFaces=j;

  if (locArray != NULL)
  {
    free((char*)locArray);
  }
};

void 
FaceDetectorNN::setSleepMilliseconds(int t)
{ 
  mpFaceDetector->setSleepMilliseconds(t); 
}

} // namespace ait

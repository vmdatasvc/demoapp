/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "legacy/vision_face/FaceDetectorCombined.hpp"

namespace ait 
{

namespace vision
{

FaceDetectorCombined::FaceDetectorCombined()
: mIsInitialized(false)
{
}

FaceDetectorCombined::~FaceDetectorCombined()
{
}

void 
FaceDetectorCombined::init(std::string adaModelFile)
{
  // Initialize this object.

  mIsInitialized = true;

  mFaceDetectAB.init(adaModelFile);
 
}

//
// OPERATIONS
//

void 
FaceDetectorCombined::detect(const Image8 &grayImage, bool async)
{
  Image8 faceImage;
  int count = 0;
  
  // Set the minimum and maximum face sizes (for AdaBoost only, for now...).
  mFaceDetectAB.setMinFaceSize(mMinFaceSize);
  mFaceDetectAB.setMaxFaceSize(mMaxFaceSize);
  mFaceDetectAB.setResampleForMinSize(mResampleForMinSize);

  mFaceDetectAB.detect(grayImage, true);
	for (int j = 0; j < mFaceDetectAB.nrOfFaces(); j++)
  {			
    faceImage.crop(grayImage, mFaceDetectAB.face(j).x, mFaceDetectAB.face(j).y,
      mFaceDetectAB.face(j).w, mFaceDetectAB.face(j).h);
    
    mFaceDetectNN.detect(faceImage, true);
    
    for(int k = 0; k < mFaceDetectNN.nrOfFaces(); k++)
    {
      Face& f = mFaceDetectNN.face(k);
      faceArray[count].x = f.x + mFaceDetectAB.face(j).x ;
      faceArray[count].y = f.y + mFaceDetectAB.face(j).y ;
      faceArray[count].w = f.w;
      faceArray[count].h = f.h;  
      count++;          
    }
  }
  nFaces = count;
}

//
// ACCESS
//

//
// INQUIRY
//


}; // namespace vision

}; // namespace ait


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "SetsOfGtPersons.hpp"
#include <ait/Image_fwd.hpp>

namespace ait 
{
  
  SetsOfGtPersons::SetsOfGtPersons()
  {
  }
  
  SetsOfGtPersons::~SetsOfGtPersons()
  {
  }
  
  //
  // OPERATIONS
  //



  void 
    SetsOfGtPersons::getPersonInfoFromCsv(char * name, char *path, bool flag)
  {
    std::ifstream fin(name);
    std::string csvLine;
    
    
    
    std::getline(fin,csvLine,'\n');
    
    while(std::getline(fin,csvLine,'\n'))
    {	
      GtPersonPtr pCurrentPerson(new GtPerson);
      
      std::vector<std::string> tokens= ait::split(csvLine,",");
      if(flag)
      {
        pCurrentPerson->mFileName.assign(path);
        pCurrentPerson->mFileName+=tokens[0];
      }
      else
      {
        pCurrentPerson->mFileName = tokens[0];
      }
      
      pCurrentPerson->mFrontalFace = tokens[16];
      pCurrentPerson->mAutomaticDetection = tokens[17];
      pCurrentPerson->mPitch = tokens[18];
      pCurrentPerson->mYaw = tokens[19];
      
      pCurrentPerson->mDemographics.mAge = atoi(tokens[2].c_str());
      pCurrentPerson->mDemographics.mGender = tokens[1];
      pCurrentPerson->mDemographics.mEthnicity = tokens[3];
      
      pCurrentPerson->mManualFaceLocation.mFacePosX = atof(tokens[4].c_str());
      pCurrentPerson->mManualFaceLocation.mFacePosY = atof(tokens[5].c_str());
      pCurrentPerson->mManualFaceLocation.mFaceSizeX = atof(tokens[6].c_str());
      pCurrentPerson->mManualFaceLocation.mFaceSizeY = atof(tokens[7].c_str());
      
      /*
      pCurrentPerson->mAutomaticFaceLocation.mFacePosX = atof(tokens[20].c_str());
      pCurrentPerson->mAutomaticFaceLocation.mFacePosY = atof(tokens[21].c_str());
      pCurrentPerson->mAutomaticFaceLocation.mFaceSizeX = atof(tokens[22].c_str());
      pCurrentPerson->mAutomaticFaceLocation.mFaceSizeY = atof(tokens[23].c_str());
      */    
      pCurrentPerson->mFaceFeatures.mLeftEyeX = atof(tokens[8].c_str());
      pCurrentPerson->mFaceFeatures.mLeftEyeY = atof(tokens[9].c_str());
      pCurrentPerson->mFaceFeatures.mRightEyeX = atof(tokens[10].c_str());
      pCurrentPerson->mFaceFeatures.mRightEyeY = atof(tokens[11].c_str());
      pCurrentPerson->mFaceFeatures.mNoseX = atof(tokens[12].c_str());
      pCurrentPerson->mFaceFeatures.mNoseY = atof(tokens[13].c_str());
      pCurrentPerson->mFaceFeatures.mMouthX = atof(tokens[14].c_str());
      pCurrentPerson->mFaceFeatures.mMouthY = atof(tokens[15].c_str());
      tokens.erase(tokens.begin(),tokens.end());
      
      mListOfGtPersons.push_back(pCurrentPerson);
      
    }
    PVMSG("done...%d\n",mListOfGtPersons.size());
  };
  
  
  
  
  
  
  /*
  *	Loads data into database 
  */
  
  void 
    SetsOfGtPersons::populateDatabase()
  {
    // Log file to check for errors. Has no purpose as of now. Will be refactored.
    
    std::ofstream log;
    log.open("c:/mysql/data/face_image/log.txt");
    
    
    // Create database connection.
    
    static MySqlDatabase database;
    
    for (;;)
    {
      try
      {
//        database.connect("", "root", "","aitdev016");
        database.connect("", "root", "","localhost");
        break;
      }
      catch (std::string str)
      {
        printf("failed connecting to database \n");
      }
    }
    
    
    MySqlRecordSet rs;

    /*
     *Reads in the filename that contains the folder names from which image data has to be transferred
     */

    std::cout<<"Enter filename that has folder locations:\n";
    std::string filename;
    std::cin>>filename;
    std::ifstream foldersInput;
    foldersInput.open(filename.c_str());
    
    
    std::string Line;
    std::string root, folder, folderInfo;
   

    /*
     *	gets a line from the file (each line contains a new folder from which image data ha sto be transferred)
     */
   
    
    while(std::getline(foldersInput,Line,'\n'))
    {	
      
      


      std::vector<std::string> tokens= ait::split(Line,"\t");
      root=tokens[0];
      folder=tokens[1];
      folderInfo=tokens[1]; // folderinfo has junk as of now. should redo that.
      
      std::string temp;
      temp=root+folder+"demographics-gt.csv";
      
      char tempchar[300];
      sprintf(tempchar,temp.c_str());
      
      /*
       *	Gets a list of all faces in that folder from teh csv file
       */
      reInitListOfGtPersons();
      getPersonInfoFromCsv(tempchar,"",TRUE);
      
      
      /*
       *	Checks if the root for  the current location is already entered in the table
       */

      std::string sqlString("SELECT * FROM face_image.root WHERE location ='");
      sqlString+=root;
      sqlString+="'";
      
      database.readRecordSet(sqlString.c_str(),&rs);
      
      int rootID;
      
      if(rs.getNumberOfRecords()==1)
        rootID=rs.getIntField("id");
      else
      {
        database.readRecordSet("select * from face_image.root",&rs);
        rootID= rs.getNumberOfRecords()+1;
        std::string sqlInsert("INSERT INTO face_image.root VALUES(");
        sqlInsert=ait::aitSprintf("%s%d",sqlInsert.c_str(),rootID);
        sqlInsert+=",'";
        sqlInsert+=root;
        sqlInsert+="')";
        database.executeSQL(sqlInsert.c_str());
      }
       
      /*
       *	Checks if the folder has already been entered. If data from folder has been entered it 
       *  quits else it enters data from the folder
       */     
      
      sqlString="SELECT * FROM face_image.folder WHERE location ='";
      sqlString+=folder;
      sqlString+="'&& root_id=";
      sqlString=ait::aitSprintf("%s%d",sqlString.c_str(),rootID);
      
      database.readRecordSet(sqlString.c_str(),&rs);
      
      int folderID;
      if(rs.getNumberOfRecords()==1)
        folderID=rs.getIntField("id");
      else
      {
        database.readRecordSet("select * from face_image.folder",&rs);
        folderID= rs.getNumberOfRecords()+1;
        std::string sqlInsert("INSERT INTO face_image.folder VALUES(");
        sqlInsert=ait::aitSprintf("%s%d",sqlInsert.c_str(),folderID);
        sqlInsert+=",'";
        sqlInsert+=folder;
        sqlInsert+="','";
        sqlInsert+=folderInfo;
        sqlInsert+="',";
        sqlInsert=ait::aitSprintf("%s%d",sqlInsert.c_str(),rootID);
        sqlInsert+=")";
        database.executeSQL(sqlInsert.c_str());
        
        
        for (int i=0; i<mListOfGtPersons.size(); i++)
        {
          //Get ethnicityID
          sqlString = "SELECT * FROM face_image.ethnicity_index WHERE ethnicity ='";
          sqlString+=mListOfGtPersons[i]->mDemographics.mEthnicity;
          sqlString+="'";
          
          database.readRecordSet(sqlString.c_str(),&rs);
          int ethnicityID= rs.getIntField("id");
          
          
          //Create insert face query
          sqlString="";
          sqlString = "INSERT INTO face_image.image_data VALUES (";
          sqlString = ait::aitSprintf("%s%d",sqlString.c_str(),folderID);
          sqlString += ",'";
          sqlString += mListOfGtPersons[i]->mFileName;
          sqlString += "','";
          sqlString += mListOfGtPersons[i]->mDemographics.mGender;
          sqlString += "',";
          sqlString = ait::aitSprintf("%s%d",sqlString.c_str(),mListOfGtPersons[i]->mDemographics.mAge);
          sqlString += ",";
          sqlString = ait::aitSprintf("%s%d",sqlString.c_str(),ethnicityID);
          sqlString += ",";
          sqlString = ait::aitSprintf("%s%d",sqlString.c_str(),int(mListOfGtPersons[i]->mManualFaceLocation.mFacePosX));
          sqlString += ",";
          sqlString = ait::aitSprintf("%s%d",sqlString.c_str(),int(mListOfGtPersons[i]->mManualFaceLocation.mFacePosY));
          sqlString += ",";
          sqlString = ait::aitSprintf("%s%d",sqlString.c_str(),int(mListOfGtPersons[i]->mManualFaceLocation.mFaceSizeX));
          sqlString += ",";
          sqlString = ait::aitSprintf("%s%d",sqlString.c_str(),int(mListOfGtPersons[i]->mManualFaceLocation.mFaceSizeY));
          sqlString += ",";
          sqlString = ait::aitSprintf("%s%d",sqlString.c_str(),int(mListOfGtPersons[i]->mFaceFeatures.mLeftEyeX));
          sqlString += ",";
          sqlString = ait::aitSprintf("%s%d",sqlString.c_str(),int(mListOfGtPersons[i]->mFaceFeatures.mLeftEyeY));
          sqlString += ",";
          sqlString = ait::aitSprintf("%s%d",sqlString.c_str(),int(mListOfGtPersons[i]->mFaceFeatures.mRightEyeX));
          sqlString += ",";
          sqlString = ait::aitSprintf("%s%d",sqlString.c_str(),int(mListOfGtPersons[i]->mFaceFeatures.mRightEyeY));
          sqlString += ",";
          sqlString = ait::aitSprintf("%s%d",sqlString.c_str(),int(mListOfGtPersons[i]->mFaceFeatures.mNoseX));
          sqlString += ",";
          sqlString = ait::aitSprintf("%s%d",sqlString.c_str(),int(mListOfGtPersons[i]->mFaceFeatures.mNoseY));
          sqlString += ",";
          sqlString = ait::aitSprintf("%s%d",sqlString.c_str(),int(mListOfGtPersons[i]->mFaceFeatures.mMouthX));
          sqlString += ",";
          sqlString = ait::aitSprintf("%s%d",sqlString.c_str(),int(mListOfGtPersons[i]->mFaceFeatures.mMouthY));
          sqlString += ",'";
          sqlString += mListOfGtPersons[i]->mFrontalFace;
          sqlString += "','";
          sqlString += mListOfGtPersons[i]->mAutomaticDetection;
          sqlString += "','";
          sqlString += mListOfGtPersons[i]->mPitch;
          sqlString += "','";
          sqlString += mListOfGtPersons[i]->mYaw;
          sqlString += "')";
          log<<sqlString<<std::endl;

          // Enters data for single person into database
          
          database.executeSQL(sqlString.c_str()); 
        } 
      }
      
  }

  reInitListOfGtPersons();

  database.closeConnection();
}





/*
*	Processes a query to image_data 
*/


void 
SetsOfGtPersons::processFaceDatabaseQuery(std::vector<GtPersonPtr> & queiredFaces, std::string query)
{
  
  //Instantiate mysqldatabase object
  
  static MySqlDatabase database;
  
  
  //Create database connection
  
  for (;;)
  {
    try
    {
//      database.connect("", "root", "","aitdev016");
      database.connect("", "root", "","localhost");
      break;
    }
    catch (std::string str)
    {
      printf("failed connecting to database \n");
    }
  }
  
  //MySqlRecordSet object to process queries that return datasets
  
  MySqlRecordSet rs;
  
  database.readRecordSet(query.c_str(),&rs);

  
  while(!rs.endOfRecordSet())
  {
    GtPersonPtr pCurrentPerson(new GtPerson);
    
    pCurrentPerson->mManualFaceLocation.mFacePosX=rs.getIntField("face_pos_x");
    pCurrentPerson->mManualFaceLocation.mFacePosY=rs.getIntField("face_pos_y");
    pCurrentPerson->mManualFaceLocation.mFaceSizeX=rs.getIntField("face_size_x");
    pCurrentPerson->mManualFaceLocation.mFaceSizeY=rs.getIntField("face_size_y");
    pCurrentPerson->mFaceFeatures.mLeftEyeX=rs.getIntField("left_eye_x");
    pCurrentPerson->mFaceFeatures.mLeftEyeY=rs.getIntField("left_eye_y");
    pCurrentPerson->mFaceFeatures.mRightEyeX=rs.getIntField("right_eye_x");
    pCurrentPerson->mFaceFeatures.mRightEyeY=rs.getIntField("right_eye_y");
    pCurrentPerson->mFaceFeatures.mNoseX=rs.getIntField("nose_x");
    pCurrentPerson->mFaceFeatures.mNoseY=rs.getIntField("nose_y");
    pCurrentPerson->mFaceFeatures.mMouthX=rs.getIntField("mouth_x");
    pCurrentPerson->mFaceFeatures.mMouthY=rs.getIntField("mouth_y");
    pCurrentPerson->mFrontalFace=rs.getStringField("frontal_face");
    pCurrentPerson->mAutomaticDetection=rs.getStringField("auto_face_rect");
    pCurrentPerson->mPitch=rs.getStringField("pitch");
    pCurrentPerson->mYaw=rs.getStringField("yaw");
    
    pCurrentPerson->mDemographics.mAge=rs.getIntField("age");
    pCurrentPerson->mDemographics.mGender=rs.getStringField("gender");
    
    MySqlRecordSet ethnicityRs;
    std::string ethnicityQuery("Select ethnicity from face_image.ethnicity_index where id=");
    
    ethnicityQuery = ait::aitSprintf("%s%d",ethnicityQuery.c_str(),rs.getIntField("ethnicity_id"));
    
    database.readRecordSet(ethnicityQuery.c_str(),&ethnicityRs);
    
    pCurrentPerson->mDemographics.mEthnicity=ethnicityRs.getStringField("ethnicity");
    
    MySqlRecordSet folderRs;
    std::string folderQuery("Select * from face_image.folder where id=");
    
    folderQuery = ait::aitSprintf("%s%d",folderQuery.c_str(),rs.getIntField("folder_id"));
    
    database.readRecordSet(folderQuery.c_str(),&folderRs);
    
    std::string folderpath;
    folderpath=folderRs.getStringField("location");
    
    MySqlRecordSet rootRs;
    std::string rootQuery("Select * from face_image.root where id=");
    
    rootQuery = ait::aitSprintf("%s%d",rootQuery.c_str(),folderRs.getIntField("root_id"));
    
    database.readRecordSet(rootQuery.c_str(),&rootRs);
    
    pCurrentPerson->mFileOnlyName = rs.getStringField("file_name");
    pCurrentPerson->mFolderID = rs.getIntField("folder_id");

    std::string totalPath(rootRs.getStringField("location"));
    totalPath=totalPath+folderpath+rs.getStringField("file_name");
    
    pCurrentPerson->mFileName=totalPath;
    queiredFaces.push_back(pCurrentPerson);
    rs.moveToNextRecord();
    
  }
  database.closeConnection();
}





void 
SetsOfGtPersons::getFacePtrvector(GtPersonPtrList listOfpersons, std::vector<Image8Ptr>& faceImageList)
{
  for(int Cnt=0; Cnt<listOfpersons.size(); Cnt++)
  {
    Image8Ptr pCurrentFace(new Image8);
    if(listOfpersons[Cnt]->getGrayFaceImage(*pCurrentFace)==1)
      faceImageList.push_back(pCurrentFace);
  }
}

/*
 *	gets a fraction of the personList accurate to 2 decimal places
 */

void 
SetsOfGtPersons::getSubset(GtPersonPtrList& inputPersonList, GtPersonPtrList& outputPersonList, float fraction)
{
  int NoOfSamples = fraction*inputPersonList.size();
  
  int firstSampleRate=fraction*10+.1;
  float newFraction=fraction*10-firstSampleRate;
  int secondSampleRate=newFraction*10+.1;
  
  
  int secondCnt=0;
  int secondFraction=(inputPersonList.size()*secondSampleRate)/100;
  
  
  for(int Cnt=0; Cnt<inputPersonList.size(); Cnt=Cnt+10)
  {
    int loopCntMax;
    
    if(secondCnt<secondFraction)
      loopCntMax=Cnt+firstSampleRate+1;
    else
      loopCntMax=Cnt+firstSampleRate;
    
    for(int loopCnt=Cnt; loopCnt<loopCntMax; loopCnt++)
    {
      GtPersonPtr currentGtPerson;
      currentGtPerson = inputPersonList[loopCnt];
      outputPersonList.push_back(currentGtPerson);
    }
    secondCnt++;
    
  }
  
  
}


void 
SetsOfGtPersons::getSubset(GtPersonPtrList& inputPersonList, GtPersonPtrList& outputPersonList1, GtPersonPtrList& outputPersonList2, float fraction)
{
  int NoOfSamples = fraction*inputPersonList.size();
  
  int firstSampleRate=fraction*10+.1;
  float newFraction=fraction*10-firstSampleRate;
  int secondSampleRate=newFraction*10+.1;
  
  
  int secondCnt=0;
  int secondFraction=(inputPersonList.size()*secondSampleRate)/100;
  
  
  for(int Cnt=0; Cnt<inputPersonList.size(); Cnt=Cnt+10)
  {
    int loopCntMax;
    
    if(secondCnt<secondFraction)
      loopCntMax=Cnt+firstSampleRate+1;
    else
      loopCntMax=Cnt+firstSampleRate;
    
    for(int loopCnt=Cnt; loopCnt<loopCntMax&&loopCnt<inputPersonList.size(); loopCnt++)
    {
      GtPersonPtr currentGtPerson;
      currentGtPerson = inputPersonList[loopCnt];
      outputPersonList1.push_back(currentGtPerson);
    }
    for(int loopCnt=loopCntMax; loopCnt<Cnt+10&&loopCnt<inputPersonList.size(); loopCnt++)
    {
      GtPersonPtr currentGtPerson;
      currentGtPerson = inputPersonList[loopCnt];
      outputPersonList2.push_back(currentGtPerson);
    }
    secondCnt++;
    
  }
  
  
}

/*
 *	Gets desired # of person accurate to tens place
 */

/*
void 
SetsOfGtPersons::getSubset(GtPersonPtrList& inputPersonList, GtPersonPtrList& outputPersonList, int noOfPersons)
{
  float fraction = noOfPersons/inputPersonList.size();

  GtPersonPtrList& tempInList = 
  getSubset(inputPersonList, outputPersonList,fraction);

}
*/




void 
SetsOfGtPersons::splitList(GtPersonPtrList& inputPersonList, std::vector<GtPersonPtrList>& splitLists)
{
  
}

//
// ACCESS
//

//
// INQUIRY
//

}; // namespace ait


#include "GenderTorchSVM.hpp"

using namespace std;
using namespace Torch;

namespace ait
{

// Initialize
void GenderTorchSVM::init(string mModelFile_, int imgWidth_, int imgHeight_)
{
	get_args();

  Allocator *allocator = new Allocator;
 
  mModelFile = mModelFile_;
	imgWidth = imgWidth_;
	imgHeight = imgHeight_;
	numInputs = imgWidth * imgHeight;

  DiskXFile::setLittleEndianMode();

  Kernel *kernel = NULL;

  DiskXFile model(mModelFile.c_str(), "r");
  mCmd.loadXFile(&model);

  if(mDegree > 0)
    kernel = new(allocator) PolynomialKernel(mDegree, mACst, mBCst);
  else
    kernel = new(allocator) GaussianKernel(1./(mStdv*mStdv));
 
  if(mRegression)
    mSvm = new(allocator) SVMRegression(kernel);
  else
    mSvm = new(allocator) SVMClassification(kernel);

  mSvm->loadXFile(&model);

	PVMSG("SVM model loaded: nSV=%d std=%f\n",mSvm->n_support_vectors, mStdv);

}

// Torch3 interface to read Torch style trained model file
// It seems that the unnecessary options were added from training; 
// need to remove these within the training code
void GenderTorchSVM::get_args()
{
  
  // Train mode
  mCmd.addRCmdOption("-std", &mStdv, 10., "the std parameter in the gaussian kernel [exp(-|x-y|^2/std^2)]", true);
  mCmd.addICmdOption("-degree", &mDegree, -1, "if positive, use a polynomial kernel [(a xy + b)^d] with the specified degree", true);
  mCmd.addRCmdOption("-a", &mACst, 1., "constant a in the polynomial kernel", true);
  mCmd.addRCmdOption("-b", &mBCst, 1., "constant b in the polynomial kernel", true);
  mCmd.addBCmdOption("-rm", &mRegression, false, "regression mode", true);
  mCmd.addICmdOption("-class", &mFileParams.class_against_the_others, -1, "train the given class against the others", true);
  mCmd.addBCmdOption("-norm", &mFileParams.normalize, false, "normalize the data (mean/stdv)?", true);
  mCmd.addRCmdOption("-e", &mFileParams.accuracy, (float)0.01, "end accuracy");
  mCmd.addRCmdOption("-m", &mFileParams.cache_size, 50., "cache size in Mo");
  mCmd.addICmdOption("-h", &mFileParams.iter_shrink, 100, "minimal number of iterations before shrinking");
  mCmd.addICmdOption("-seed", &mFileParams.the_seed, -1, "the random seed");
  mCmd.addICmdOption("-load", &mFileParams.max_load, -1, "max number of examples to load for train");

  // Test mode
  mCmd.addMasterSwitch("--test");
  mCmd.addICmdOption("-load", &mFileParams.max_load, -1, "max number of examples to load for train");

  // Read the command line
  char **argv;
  argv = (char **)malloc(2*sizeof(char *));
  argv[0] = (char *)malloc(11*sizeof(char));
  argv[1] = (char *)malloc(7*sizeof(char));
//  strcpy(argv[0], "executable");
//  strcpy(argv[1], "--test");
  argv[0] = "executable";
  argv[1] = "--test";

  int mode = mCmd.read(2, argv);
}

void GenderTorchSVM::correctLeqImage(Image8 faceImage, Image8& outImage, float X, float Y, float S, float O)
{
  Image8 tempImage(faceImage);

  outImage.centerRotateRescaleShift(tempImage, -O, 12.5/S, X, Y-4.28*(S/12.5), (float)0, (float)-4.28, imgWidth, imgHeight);	
  Image8 ttImage(outImage);
  outImage.Lequalize(ttImage,12); //12 for 30x30
}

float GenderTorchSVM::classify(Image8 faceImage, Image8& outImage, float X, float Y, float S, float O)
{
  if(X==0.0 && Y==0.0 && S==0.0 && O==0.0)
  {}
  else
    correctLeqImage(faceImage, outImage, X, Y, S, O);

  unsigned char *inputData;

  inputData = (unsigned char *) malloc(numInputs * sizeof(unsigned char));
  int count=0;
  for(int y=0; y<imgHeight; y++)
    for(int x=0; x<imgWidth; x++)
      inputData[count++] = outImage(x,y);

//	outImage = cropImage;
  Sequence inputs(1, numInputs);

  for(int i=0; i < inputs.frame_size; i++)
	{
	  inputs.frames[0][i] = (float)inputData[i];
//	  printf("%d:%f ",i,inputs.frames[0][i]);
//	  inputs.frames[0][i] = (inputs.frames[0][i] - mvNorm->inputs_mean[i]) / mvNorm->inputs_stdv[i];  
	 }

	free(inputData);

// SVM forward
  mSvm->forward(&inputs);
	
  float response = mSvm->outputs->frames[0][0];

	return response;
}

float GenderTorchSVM::classify(Image8 faceImage)
{
  return classify(faceImage, faceImage, 0.0, 0.0, 0.0, 0.0);
}

} // namespace ait
/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include "legacy/vision_tools/TransKalman.hpp"

using namespace std;

namespace ait
{

TransKalmanRepository TransKalman::rep;

bool TransKalmanRepository::findAndGet(string fileName,TransKalman &kalman)
{
  bool found=false;

  EnterCriticalSection(&classLock);

  TKRList::iterator it=rep.begin();

  while(it!=rep.end())
  {
    if(fileName==it->fileName)
    {
      *((PvKalman *)&kalman)=it->kalman;
      kalman.transMethod=it->transMethod;
      found=true;
    }
    it++;
  }

  LeaveCriticalSection(&classLock);

  return found;
}

void TransKalmanRepository::store(TransKalman &kalman,string fileName)
{
  TransKalmanRepEntry entry;

  entry.kalman=kalman;
  entry.transMethod=kalman.transMethod;
  entry.fileName=fileName;

  EnterCriticalSection(&classLock);

  rep.push_front(entry);

  LeaveCriticalSection(&classLock);

  PVMSG("Stored kalman filter '%s' in repository.\n",fileName.c_str());
}

/************************************************************************
 ** 
 **  TransKalman
 **
 ************************************************************************/

/*********************************************************************
 * CONSTRUCTOR
 *********************************************************************/
TransKalman::TransKalman(void) : pTrans(NULL), z(2,1),obs(2,1)
{
}

/*********************************************************************
 * DESTRUCTOR
 *********************************************************************/
TransKalman::~TransKalman(void)
{
  if(pTrans) delete pTrans;
}

/*********************************************************************
 * init()
 * 
 * Initialize the various matrices of the Kalman filter.
 *
 * <input> 
 * 
 *
 * <output>
 *
 * void
 *
 * <error handling>
 * 
 * TODO
 *
 *********************************************************************/
void TransKalman::init(const Vector2f &coord,const char *fileName)
{
  bool foundInRep=true;

  if(!rep.findAndGet(fileName,*this))
  {
    foundInRep=false;

    ifstream cin;
    
    cin.open(fileName);
    
    if(!cin)
    {
      PvUtil::exitError("TransKalman::init(%s): ",fileName);
    }
    
    cin >> transMethod;
    cin >> varP0 >> varPx;
    cin >> varROff >> varRDiag;
    
    cin >> A;
    
    cin >> Q;
    
    cin.close();
    
    //A.printf("%g ");
    //Q.printf("%g ");

    init(coord,A,Q,varP0,varPx,varRDiag,varROff,transMethod);
  }
  else
  {
    postInit(coord);
  }

  if(!foundInRep)
  {
    Vector2f coordObs=coord;

    // make shure kalman filter is all set up
    predict(coordObs);
    coordObs=coord;
    update(coordObs);

    // store in repository
    rep.store(*this,fileName);
  }
}

void TransKalman::postInit(const Vector2f &coord)
{
  switch(transMethod)
    {
    case TR_ID: PVMSG("TR_ID\n");pTrans=new IdTransformer<float>(2);break;
    case TR_COMB_4: PVMSG("TR_COMB_4\n");pTrans=new Combine4Transformer<float>(2);break;
    case TR_COMB_3: PVMSG("TR_COMB_3\n");pTrans=new Combine3Transformer<float>(2);break;
    case TR_COMB: PVMSG("TR_COMB\n");pTrans=new CombineTransformer<float>(2);break;
    case TR_NEWTON: PVMSG("TR_NEWTON\n");pTrans=new NewtonTransformer<float>(2);break;
    case TR_NEWTON_4: PVMSG("TR_NEWTON_4\n");pTrans=new Newton4Transformer<float>(2);break;
    case TR_DIFF: PVMSG("TR_DIFF\n");pTrans=new DiffTransformer<float>(2);break;
    case TR_TRAJ: PVMSG("TR_TRAJ\n");pTrans=new TrajectoryTransformer<float>(2);break;
    default: break;
    }

  obs(0)=coord(0);
  obs(1)=coord(1);

  // initialize kalman filter
  int i,nKal=A.rows();
    
  Matrix<float> x(nKal,1);

  // create an array of identical x vectors and transform them, to get an initial
  // observation vector
  Matrix<float> xArray(16,2),xTransformed;

  for(i=0;i<16;i++)
  {
    xArray(i,0)=coord(0);
    xArray(i,1)=coord(1);
  }

  pTrans->transform(xArray,xTransformed);

  if(xTransformed.cols()!=nKal)
  {
    PvUtil::exitError("TransKalman: Error, number of components in transformed coordinate array is not equal to number of components in state vector.");
  }

  while(!pTrans->transformIncremental(obs,x));

  x1=x; // equivalent to state()=x

  z.resize(x.rows()*x.cols(),1);
}

void TransKalman::init(const Vector2f &coord,
            const Matrix<float> A,
            const Matrix<float> B,
            float NvarP0,float NvarPx,float NvarRDiag,float NvarROff,int NtransMethod)
{
  // initialize kalman filter
  int i,j,nKal=A.rows(),mKal=B.cols();
  
  varP0=NvarP0;
  varPx=NvarPx;
  varRDiag=NvarRDiag;
  varROff=NvarROff;
  transMethod=NtransMethod;

  setSize(nKal,mKal);
  
  Matrix<float> x(nKal,1),P(nKal,nKal);
  Matrix<float> H(mKal,nKal),R(mKal,mKal);
  
  for(i=0;i<nKal;i++) x(i)=coord(i%2);
  
  P.id();

  if(0)
  {
    P(0,0)=varP0;
    P(1,1)=varP0;
    for(i=2;i<nKal;i++) P(i,i)=varPx;
  }
  else
  {
    for(i=0;i<nKal;i++) 
    {
      for(j=0;j<nKal;j++)
      {
        if(i==j) P(i,j)=varP0;
        else    P(i,j)=varPx;
      }      
    }
  }

  H.id();  
  
  R.zero();
  
  for(i=0;i<mKal;i++) 
  {
    for(j=0;j<mKal;j++)
    {
      if(i==j) R(i,j)=varRDiag;
      else    R(i,j)=varROff;
    }      
  }

  PvKalman::init(x,P,A,H,B,R);
  
  postInit(coord);
}

/*********************************************************************
 * predictNext()
 * 
 * Performs Kalman prediction + update step.
 *
 * <input> 
 * 
 * void
 *
 * <output>
 *
 * void
 *
 * <error handling>
 * 
 * TODO
 *
 *********************************************************************/
void TransKalman::predictNext(Vector2f &coord)
{
  z(0)=coord(0);
  z(1)=coord(1);

  t0.mult(H,x1);
  t1.sub(z,t0);
  t2.mult(K,t1);
  x2.add(x1,t2);
  x1.mult(A,x2);
  z.mult(H,x1);
  // z.mult(H,x2);

  coord(0)=z(0);
  coord(1)=z(1);
}

/*********************************************************************
 * predict()
 * 
 * Given an a priori (before an observation becomes known)
 * prediction of the state x-(k), give back an estimate of the
 * observation z-(k).
 *
 * <input> 
 * 
 * void
 *
 * <output>
 *
 * Matrix<float> &z : observation vector
 *
 * <error handling>
 * 
 *********************************************************************/
void TransKalman::predict(Vector2f &coord)
{
  z.mult(H,x1);

  coord(0)=z(0);
  coord(1)=z(1);
}

/*********************************************************************
 * update()
 * 
 * Given an actual measurement z(k), update the predicted
 * state to an a posteriori state x(k+1) and update the measurement
 * to reflect the actual observation.
 *
 * <input> 
 * 
 * Matrix<float> &z : observation vector z(k)
 *
 * <output>
 *
 * Matrix<float> &z : corrected observation vector z(k)
 *
 * <error handling>
 * 
 *********************************************************************/
void TransKalman::update(Vector2f &coord)
{
  obs(0)=coord(0);
  obs(1)=coord(1);

  pTrans->transformIncremental(obs,z);

  t0.mult(H,x1);
  t1.sub(z,t0);
  t2.mult(K,t1);
  x2.add(x1,t2);
  z.mult(H,x2);
  x1.mult(A,x2);

  coord(0)=z(0);
  coord(1)=z(1);
}

} // namespace ait

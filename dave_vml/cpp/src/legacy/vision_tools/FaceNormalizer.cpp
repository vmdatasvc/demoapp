/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "vision_tools/FaceNormalizer.hpp"
#include "ipp.h"

// x location of the eye in a 60x60 image fixed at 8
#define LEFT_EYE_RATIO_X_HEAD 23/60

#define LEFT_EYE_RATIO_X_FACE 15/60


// y location of the eye in a 60x60 image fixed at 7
#define LEFT_EYE_RATIO_Y_HEAD 22/60

#define LEFT_EYE_RATIO_Y_FACE 15/60

// horizontal distance between left eye and right eye in a 30x30 image fixed at 14
#define RIGHT_EYE_DIST_RATIO 14/60

namespace ait 
{
  
  FaceNormalizer::FaceNormalizer()
  {
  }
  
  FaceNormalizer::~FaceNormalizer()
  {
  }
  
  //
  // OPERATIONS
  //
  void 
    FaceNormalizer::faceNormalize(const Image8 &headImage, Image8& normalizedImage, Vector2f leftEye, Vector2f RightEye)
  {
    int leftEyeX, leftEyeY, rightEyeDist; 
    
    if(ceil((float)headImage.width() * LEFT_EYE_RATIO_X_HEAD) - headImage.width() * LEFT_EYE_RATIO_X_HEAD > 0.5)
      leftEyeX = headImage.width() * LEFT_EYE_RATIO_X_HEAD;
    else
      leftEyeX = ceil((float)headImage.width() * LEFT_EYE_RATIO_X_HEAD);
    
    if(ceil((float)headImage.height() * LEFT_EYE_RATIO_Y_HEAD) - headImage.height() * LEFT_EYE_RATIO_Y_HEAD > 0.5)
      leftEyeY = headImage.height() * LEFT_EYE_RATIO_Y_HEAD;
    else
      leftEyeY = ceil((float)headImage.height() * LEFT_EYE_RATIO_Y_HEAD);
    
    if(ceil((float)headImage.width() * RIGHT_EYE_DIST_RATIO) - headImage.width() * RIGHT_EYE_DIST_RATIO > 0.5)
      rightEyeDist = headImage.width() * RIGHT_EYE_DIST_RATIO;
    else
      rightEyeDist = ceil((float)headImage.width() * RIGHT_EYE_DIST_RATIO);
    
    IppiSize srcSize = {headImage.width(), headImage.height()};
    
    FacialFeatureDetector::FacialFeatureLocation eyeLocation;
    eyeLocation.leftEye = leftEye;
    eyeLocation.rightEye = RightEye;
    
    IppiRect srcROI ;
    srcROI.x = 0; srcROI.y = 0; srcROI.width = headImage.width(); srcROI.height = headImage.height();
    
    Image8 destImage1, destImage2;
    destImage1.resize(headImage.width(), headImage.height());
    destImage2.resize(headImage.width(), headImage.height());
    
    float theta = atan((eyeLocation.rightEye(1) - eyeLocation.leftEye(1)) / (eyeLocation.rightEye(0) - eyeLocation.leftEye(0)));
    
    float scaling = (rightEyeDist)/sqrt(pow(eyeLocation.leftEye(0) - eyeLocation.rightEye(0), 2)
      + pow(eyeLocation.leftEye(1) - eyeLocation.rightEye(1), 2));

    double coeff[2][3];
    
    //first translate and then rotate and then scale..is this sequence correct?

    coeff[0][0] = 1; coeff[0][1] = 0; coeff[0][2] = (-eyeLocation.leftEye(0) + leftEyeX);
    coeff[1][0] = 0; coeff[1][1] = 1; coeff[1][2] = (-eyeLocation.leftEye(1) + leftEyeY); 
    
    ippiWarpAffine_8u_C1R(headImage.pointer(), srcSize, headImage.width(), srcROI, destImage1.pointer(),
      headImage.width(), srcROI, coeff, IPPI_INTER_LINEAR);
    
    coeff[0][0] = cos(theta); coeff[0][1] = sin(theta); coeff[0][2] = 0;
    coeff[1][0] = -sin(theta); coeff[1][1] = cos(theta); coeff[1][2] = 0;
    
   
    ippiRotateCenter_8u_C1R(destImage1.pointer(), srcSize, headImage.width(),  srcROI, destImage2.pointer(), 
      headImage.width(), srcROI, theta*57.3, leftEyeX, leftEyeY, IPPI_INTER_LINEAR);
      /*
      ippiRotateCenter_8u_C1R(destImage1.pointer(), srcSize, headImage.width(),  srcROI, destImage2.pointer(), 
      headImage.width(), srcROI, -theta*57.3, LEFT_EYE_RATIO_X_HEAD*headImage.width(), LEFT_EYE_RATIO_Y_HEAD*headImage.height(), IPPI_INTER_LINEAR);
    */
    
    //destImage2.rescale((int)(headImage.width()*scaling), (int)(headImage.height()*scaling));
    destImage2.rescale(headImage.width(), headImage.height(), RESCALE_CUBIC);

    normalizedImage.crop(destImage2, (int)((float)LEFT_EYE_RATIO_X_FACE * (float)headImage.width()*scaling), (int)((float)LEFT_EYE_RATIO_Y_FACE * (float)headImage.height()*scaling), (int)(headImage.width()/(2*scaling)), (int)(headImage.height()/(2*scaling)));
    
}


bool
FaceNormalizer::faceNormalize(const Image8 &faceImage, const Image8 &headImage, Image8& normalizedImage)
{
  int leftEyeX, leftEyeY, rightEyeDist; 
  
  if(ceil((float)faceImage.width() * LEFT_EYE_RATIO_X_HEAD) - faceImage.width() * LEFT_EYE_RATIO_X_HEAD > 0.5)
    leftEyeX = faceImage.width() * LEFT_EYE_RATIO_X_HEAD;
  else
    leftEyeX = ceil((float)faceImage.width() * LEFT_EYE_RATIO_X_HEAD);
  
  if(ceil((float)faceImage.height() * LEFT_EYE_RATIO_Y_HEAD) - faceImage.height() * LEFT_EYE_RATIO_Y_HEAD > 0.5)
    leftEyeY = faceImage.height() * LEFT_EYE_RATIO_Y_HEAD;
  else
    leftEyeY = ceil((float)faceImage.height() * LEFT_EYE_RATIO_Y_HEAD);
  
  if(ceil((float)faceImage.width() * RIGHT_EYE_DIST_RATIO) - faceImage.width() * RIGHT_EYE_DIST_RATIO > 0.5)
    rightEyeDist = faceImage.width() * RIGHT_EYE_DIST_RATIO;
  else
    rightEyeDist = ceil((float)faceImage.width() * RIGHT_EYE_DIST_RATIO);
  
  IppiSize srcSize = {headImage.width(), headImage.height()};
  
  FacialFeatureDetector::FacialFeatureLocation eyeLocation;
  
  if(ffd.eyeLocate(faceImage, eyeLocation))
  {
    IppiRect srcROI ;
    srcROI.x = 0; srcROI.y = 0; srcROI.width = headImage.width(); srcROI.height = headImage.height();
    
    Image8 destImage1, destImage2;
    destImage1.resize(headImage.width(), headImage.height());
    destImage2.resize(headImage.width(), headImage.height());
    
    float theta = atan((eyeLocation.rightEye(1) - eyeLocation.leftEye(1)) / (eyeLocation.rightEye(0) - eyeLocation.leftEye(0)));
    
    float scaling = (rightEyeDist)/sqrt(pow(eyeLocation.leftEye(0) - eyeLocation.rightEye(0), 2)
                        + pow(eyeLocation.leftEye(1) - eyeLocation.rightEye(1), 2));

    double coeff[2][3];
    
    //first translate and then rotate..
    
    coeff[0][0] = 1; coeff[0][1] = 0; coeff[0][2] = -eyeLocation.leftEye(0) + leftEyeX;
    coeff[1][0] = 0; coeff[1][1] = 1; coeff[1][2] = -eyeLocation.leftEye(1) + leftEyeY; 
    
    ippiWarpAffine_8u_C1R(headImage.pointer(), srcSize, headImage.width(), srcROI, destImage1.pointer(),
      headImage.width(), srcROI, coeff, IPPI_INTER_LINEAR);
    
    coeff[0][0] = scaling*cos(theta); coeff[0][1] = scaling*sin(theta); coeff[0][2] = 0;
    coeff[1][0] = -scaling * sin(theta); coeff[1][1] = scaling*cos(theta); coeff[1][2] = 0; 
    
    ippiRotateCenter_8u_C1R(destImage1.pointer(), srcSize, headImage.width(),  srcROI, destImage2.pointer(), 
      headImage.width(), srcROI, -theta*57.3, (headImage.width() - faceImage.width())/2 + leftEyeX, 
      (headImage.height() - faceImage.height())/2 + leftEyeY, IPPI_INTER_LINEAR);
    
    normalizedImage.crop(destImage2, (headImage.width() - faceImage.width())/2, (headImage.height() - faceImage.height())/2,
      faceImage.width(), faceImage.height());
 
    return true;
  }
  return false;
}
//
// ACCESS
//

//
// INQUIRY
//

}; // namespace ait


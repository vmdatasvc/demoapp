/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "legacy/vision_tools/FacialFeatureDetector.hpp"

#include <legacy/vision_tools/PvImageProc.hpp>
#include <ipp.h>
#include <ippcv.h>
#include <legacy/vision_tools/PvConCompLab.hpp>
#include <vector>

#define PERCENT 0.2
#define RESCALE_IMAGE_X 150
#define RESCALE_IMAGE_Y 150

namespace ait 
{

FacialFeatureDetector::FacialFeatureDetector()
{
}

FacialFeatureDetector::~FacialFeatureDetector()
{
}

//
// OPERATIONS
//

bool FacialFeatureDetector::eyeLocate(const Image8 &faceImage, FacialFeatureLocation& locations)
{
  int width, height;
  unsigned char* test;

  Image8 tempImage;
  Image8 templateImg;
  templateImg.load("D:\\034\\034\\eye_template.pgm");

  tempImage = faceImage;
  // Rescale to a fixed size here...

  tempImage.rescale(RESCALE_IMAGE_X, RESCALE_IMAGE_Y, RESCALE_LINEAR);
  
  int imagearea = tempImage.width() * tempImage.height();

  locations.leftEye(0) = 0;
  locations.leftEye(1) = 0;
  locations.rightEye(0) = 0;
  locations.rightEye(1) = 0;
  std::vector<IppiConnectedComp> componentVector;

  width = tempImage.width();
  height = tempImage.height();
  float percent = 0.2f;
  Image8 tempImage1;
  static int mcount = 0;
  do
  {
    componentVector.erase(componentVector.begin(), componentVector.end());
    tempImage1 = tempImage;
    // Thresholding 
    PvImageProc::EqualizeImage(tempImage1);
    unsigned int histogram[256];
    
    test = tempImage1.pointer();  
    PvImageProc::ComputeHistogram(test, width, height, histogram, 256);
    
    int pixelSum = 0;
    int i;
    for(i=0; pixelSum < percent*imagearea; i++)
    {
      pixelSum += histogram[i];
    }
    
    int threshold = i-1;
    
    for(i=0; i < tempImage1.width(); i++)
    {
      for(int j=0; j < tempImage1.height(); j++)
      {
        if(tempImage1(i,j) > threshold || templateImg(i,j) == 255) tempImage1(i,j) = 255;
        else tempImage1(i,j) = 0;
      }
    }
    
    
    //Post Processing
    
    IppiPoint anchor = {2,2};
    IppiSize maskSize = {5,5};
    IppiSize dstRoiSize1 = {tempImage.width()-4, tempImage.height()-4};
    
    const unsigned char pMask[] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
    
//    ippiErode_8u_C1IR(tempImage1.pointer()+2*tempImage1.width()+2, tempImage.width(), dstRoiSize1, pMask, maskSize, anchor);
//    ippiDilate_8u_C1IR(tempImage1.pointer()+2*tempImage1.width()+2, tempImage.width(), dstRoiSize1, pMask, maskSize, anchor);
    
    //Connected Components
    
    IppiSize dstRoiSize = {tempImage1.width(), tempImage1.height()};
    int pBufSize;
    IppiPoint seed;
    
    
    
    ippiFloodFillGetSize(dstRoiSize, &pBufSize);
    Ipp8u *pBuffer = new Ipp8u[pBufSize];
    
    for(i=0; i < tempImage.width(); i++)
    {
      for(int j=0; j<tempImage.height()/2; j++)   
      {
        if(tempImage1(i,j) == 0)
        {
          IppiConnectedComp* pRegion = new IppiConnectedComp[1];
          seed.x = i;
          seed.y = j;
          
          ippiFloodFill_4Con_8u_C1IR(tempImage1.pointer(), tempImage1.width(), dstRoiSize, seed, 64, pRegion, pBuffer);
          componentVector.push_back(*pRegion);
          delete[] pRegion;
        }
      }
    }
    
    delete[] pBuffer;
    percent = percent + 0.05f;    

  }while(componentVector.size() < 2);

  char filename2[100];
  sprintf(filename2,"D:\\034\\034\\bigImage_%05d.pgm",mcount);
  tempImage1.save(filename2);
    mcount++;

  std::vector<IppiConnectedComp>::iterator iter;

  for(iter = componentVector.begin(); iter != componentVector.end(); )
  {
//     PVMSG("%d\n", iter->rect.x + iter->rect.width/2);
    if(iter->rect.x + iter->rect.width/2 > RESCALE_IMAGE_X -10 || iter->rect.x + iter->rect.width/2 < 10 )
     // iter->rect.height > 2*iter->rect.width)
      iter = componentVector.erase(iter);
    else
      iter++;
  }

  
  iter = componentVector.begin();

  if(componentVector.size() == 2)
  {
    float temp1x = iter->rect.x + iter->rect.width/2;
    float temp1y = iter->rect.y + iter->rect.height/2;

    iter++;

    float temp2x = iter->rect.x + iter->rect.width/2;
    float temp2y = iter->rect.y + iter->rect.height/2;

    locations.leftEye(0) = std::min(temp1x, temp2x)/5;
    locations.rightEye(0) = std::max(temp1x, temp2x)/5;
    
    locations.leftEye(1) = (locations.leftEye(0) == temp1x)? temp1y/5 : temp2y/5;
    locations.rightEye(1)= (locations.leftEye(0)  == temp1x)? temp2y/5 : temp1y/5;

//    PVMSG("lefteye = %f %f righteye = %f %f\n", locations.leftEye(0), locations.leftEye(1), locations.rightEye(0), locations.rightEye(1));
    
    componentVector.erase(componentVector.begin(), componentVector.end());
    
    return 1;
  }

  componentVector.erase(componentVector.begin(), componentVector.end());

  return 0;
}



//
// ACCESS
//

//
// INQUIRY
//

}; // namespace ait


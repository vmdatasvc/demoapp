/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "legacy/vision_tools/HistogramBackgroundModel.hpp"
#include <legacy/low_level/Settings.hpp>

namespace ait 
{

HistogramBackgroundModel::HistogramBackgroundModel()
: mNumFrames(-1),
  mModelInitialized(false)
{
}

HistogramBackgroundModel::~HistogramBackgroundModel()
{
}

void 
HistogramBackgroundModel::init(const std::string& settingsPath)
{
//  BackgroundModelBase::init(settingsPath);
//
  Settings set(settingsPath);

  mNumHistogramBins = set.getInt("numHistogramBins",128);
  mSaveVisualization = set.getBool("Visualization/saveVisualization [bool]",0);
  mHistogramMargin = set.getInt("histogramMargin [0..255]",10);
  mUseOneCluster = set.getBool("forceOneCluster [bool]",false);  

  mModelInitialized = false;
}

//
// OPERATIONS
//

void
HistogramBackgroundModel::update(const Image8Ptr pInputImage)
{
  Image8& img = *pInputImage;

  if (!isInitialized())
  {
    initialize(pInputImage);
  }

  int x,y;
  const int shift = PvUtil::intLog2(256/mNumHistogramBins);
  for (y = 0; y < img.height(); y++)
  {
    for (x = 0; x < img.width(); x++)
    {
      (getHistogram(x,y)[img(x,y)>>shift])++;
    }
  }

  mNumFrames++;
}

void
HistogramBackgroundModel::foregroundSegment(const Image8Ptr pInputImage, 
                                              Image8Ptr pForegroundImage)
{
  Image8& img = *pInputImage;
  Image8& foreground = *pForegroundImage;

  int x,y;
  for (y = 0; y < img.height(); y++)
  {
    for (x = 0; x < img.width(); x++)
    {
      const HistogramBackgroundModel::Thresholds& T = mThresholds(x,y);
      const Uint8 val = img(x,y);
      foreground(x,y) = (val < T.min0 || val > T.max0) && (val < T.min1 || val > T.max1) ? 255:0;
    }
  }

}

void
HistogramBackgroundModel::initialize(const Image8Ptr pInputImage)
{
  Image8& img = *pInputImage;

  mHistograms.resize(img.width()*img.height(),mNumHistogramBins);
  mHistograms.setAll(0);
  
  mNumFrames = 0;

  mImageSize.set(img.width(),img.height());

  mModelInitialized = true;
}

class Cluster
{
public:
  Cluster(int min0, int max0, float w) : min(min0), max(max0), weight(w) {}
  int min;
  int max;
  float weight;
};

static bool compare(Cluster *x, Cluster *y)
{
  return (y->weight) < (x->weight);
}

void
HistogramBackgroundModel::buildThresholds(Uint16 *histogram, 
                                            Uint8& min0, Uint8& max0, 
                                            Uint8& min1, Uint8& max1)
{
  std::vector<Cluster> clusters;
  clusters.reserve(mNumHistogramBins);

  // This is the distance required to cluster two entries into the
  // same model.
  int margin = std::max(1,round(mNumHistogramBins*mHistogramMargin/255));

  // Initially each non-zero value of the histogram represent a cluster.
  // The we will add the margin for each cluster.
  int i,j;
  for (i = 0; i < mNumHistogramBins; i++)
  {
    if (histogram[i] > 0)
    {
      clusters.push_back(Cluster(i-margin,i+margin,(float)(histogram[i])/mNumFrames));
    }
  }

  // Now work with this array for efficiency
  std::vector<Cluster*> sortedClusters;
  for (i = 0; i < clusters.size(); i++)
  {
    sortedClusters.push_back(&clusters[i]);
  }

  // Sort the clusters by frequency in the histogram. We want
  // higher frequencies first.
  std::sort(sortedClusters.begin(),sortedClusters.end(),compare);

  // Now discard the clusters that represent less than
  // a percentage.
  float discardPercentage = 0.2f;
  float accum = 0;

  // Count backwards and delete the clusters with lower weight.
  for (;;)
  {
    const float w = sortedClusters.back()->weight;
    if (accum + w > discardPercentage)
    {
      break;
    }
    sortedClusters.pop_back();
    accum += w;
  }

  // Now we have eliminated outlayer clusters. Let's join 
  // all overlapping clusters.
  bool overlap = false;
  do 
  {
    overlap = false;
    for (i = 0; i < sortedClusters.size(); i++)
    {
      for (j = i+1; j < sortedClusters.size(); j++)
      {
        const Cluster& c1 = *(sortedClusters[i]);
        const Cluster& c2 = *(sortedClusters[j]);
        // Check if they intersect
        if (std::max(c1.min,c2.min) <=
            std::min(c1.max,c2.max))
        {
          overlap = true;
          // Join the clusters:
          *(sortedClusters[i]) = Cluster(
            std::min(c1.min,c2.min),
            std::max(c1.max,c2.max),
            c1.weight+c2.weight);
          sortedClusters[j] = sortedClusters.back();
          sortedClusters.pop_back();
        }
      }
    }
  } while(overlap);

  // Sort the remaining clusters
  std::sort(sortedClusters.begin(),sortedClusters.end(),compare);

  // Take the top two, or copy the unique cluster to our
  // dual model threshold.

  const int shift = PvUtil::intLog2(256/mNumHistogramBins);

  Cluster *pC0 = sortedClusters[0];
  Cluster *pC1;
  
  if (sortedClusters.size() == 1)
  {
    pC1 = pC0;
  }
  else
  {
    // The weights must be similar to be considered another
    // background.
    const float weightFactor =  sortedClusters[1]->weight/pC0->weight;

    // This is the factor to consider the second weight
    // significant. A factor of 0.5 means that the weight
    // for the second cluster must be at least half of the
    // first cluster.
    const float weightFactorThreshold = 0.3f;
    if ((weightFactor < weightFactorThreshold) || mUseOneCluster)
    {
      pC1 = pC0;
    }
    else
    {
      // Otherwise we have two clusters.
      pC1 = sortedClusters[1];
    }
  }


  // This case will check this pixel changes too much over
  // the whole interaction. If the top two clusters spread
  // over more than 50% of the intensity values, this pixel
  // will be eliminated by making the thresholds occupy the
  // whole range.
  const float maxCombinedIntensityRange = 0.5f;
  float clustersMagnitude = pC0->max-pC0->min;
  if (pC1 != pC0)
  {
    clustersMagnitude += pC1->max-pC1->min;
  }
  if (clustersMagnitude > mNumHistogramBins*maxCombinedIntensityRange)
  {
    min0 = min1 = 0;
    max0 = max1 = 255;
  }
  else
  {
    // Calculate the threshold in the range [0,255]
    min0 = (Uint8)(std::max(pC0->min,0)<<shift);
    max0 = (Uint8)(std::min(pC0->max,mNumHistogramBins-1)<<shift);
    min1 = (Uint8)(std::max(pC1->min,0)<<shift);
    max1 = (Uint8)(std::min(pC1->max,mNumHistogramBins-1)<<shift);
  }
}

void 
HistogramBackgroundModel::drawMaxThresh(Image8& img) const
{
  assert(mThresholds.width() > 0);
  assert(mThresholds.height() > 0);

  img.resize(mThresholds.width(),mThresholds.height());

  for (int i = 0; i < mThresholds.size(); i++)
  {
    img(i) = std::max(mThresholds(i).max0, mThresholds(i).max1);
  }  
}

void
HistogramBackgroundModel::validate()
{
  
  // Use two models for thresholding.
  int w = mImageSize.x, h = mImageSize.y;
  mThresholds.resize(w,h);

  int x,y;

  for (y = 0; y < h; y++)
  {
    for (x = 0; x < w; x++)
    {
      HistogramBackgroundModel::Thresholds& T = mThresholds(x,y);
      buildThresholds(getHistogram(x,y),T.min0,T.max0,T.min1,T.max1);
    }
  }

  if (mSaveVisualization)
  {
    visualize();
  }

  // Reset the histograms. This will save considerable amount of memory.
  mHistograms.resize(1,1);
}

void
HistogramBackgroundModel::visualize()
{
  const int shift = PvUtil::intLog2(256/mNumHistogramBins);

  int x,y;

  Image8 maxImg0(mImageSize.x,mImageSize.y);
  Image8 maxImg1(mImageSize.x,mImageSize.y);

  for (y = 0; y < maxImg0.height(); y++)
  {
    for (x = 0; x < maxImg0.width(); x++)
    {
      HistogramBackgroundModel::Thresholds& T = mThresholds(x,y);
      if (T.min0 == 0 && T.max0 == 255)
      {
        maxImg0(x,y) = 0;
        maxImg1(x,y) = 0;
      }
      else
      {
        maxImg0(x,y) = (Uint8)(((int)T.min0+(int)T.max0)/2);
        maxImg1(x,y) = (Uint8)(((int)T.min1+(int)T.max1)/2);
      }
    }
  }

  Image8 img(maxImg0);
  img.div(2);
  img.rescale(640,480,RESCALE_SKIP);
  
  int dx = img.width()/16;
  int dy = img.height()/12;

  for (y = 0; y < img.height(); y+=dy)
  {
    for (x = 0; x < img.width(); x+=dx)
    {
      int modelX = (x+dx/2)*mImageSize.x/img.width();
      int modelY = (y+dy/2)*mImageSize.y/img.height();
      Uint16 *h = getHistogram(modelX,modelY);
      img(x+dx/2,y+dy/2) = 255;
      Uint16 maxH = 0;
      int i;
      for (i = 0; i < mNumHistogramBins; i++)
      {
        maxH = std::max(h[i],maxH);
      }

      Image8 tmp(mNumHistogramBins*12/10,256);
      tmp.setAll(0);
      tmp.rectangle(0,0,tmp.width(),tmp.height(),50);
      for (i = 0; i < mNumHistogramBins; i++)
      {
        tmp.line(i,255,i,255-255*h[i]/maxH,255);
      }

      HistogramBackgroundModel::Thresholds& T = mThresholds(modelX,modelY);
      tmp.line(T.min0>>shift,0,T.min0>>shift,255,200);
      tmp.line(T.max0>>shift,0,T.max0>>shift,255,200);
      tmp.line(T.min1>>shift,0,T.min1>>shift,255,150);
      tmp.line(T.max1>>shift,0,T.max1>>shift,255,150);

      tmp.save(aitSprintf("test/%03i_%03i.pgm",x+dx/2,y+dy/2+maxImg0.height()).c_str());
      FILE *fp = fopen(aitSprintf("test/%03i_%03i.csv",x+dx/2,y+dy/2+maxImg0.height()).c_str(),"w");
      if (fp)
      {
        fprintf(fp,"vals\n");
        for (i = 0; i < mNumHistogramBins; i++)
        {
          fprintf(fp,"%i\n",(int)h[i]);
        }
        fprintf(fp,"\n");
        fprintf(fp,"min0, %i\n",(int)T.min0);
        fprintf(fp,"max0, %i\n",(int)T.max0);
        fprintf(fp,"min1, %i\n",(int)T.min1);
        fprintf(fp,"max0, %i\n",(int)T.max1);
        fclose(fp);
      }
      tmp.rescale(dx,dy,RESCALE_CUBIC);
      int x1,y1;
      for (y1 = 0; y1 < tmp.height(); y1++)
      {
        for (x1 = 0; x1 < tmp.width(); x1++)
        {
          if (tmp(x1,y1) != 0)
          {
            img(x+x1,y+y1) = tmp(x1,y1);
          }
        }
      }
    }
  }

  Image8 gimg(img.width(),img.height()+maxImg0.height());
  gimg.setAll(0);
  gimg.paste(maxImg0,0,0);
  gimg.paste(maxImg1,320,0);
  gimg.paste(img,0,maxImg0.height());
  static int staticIndex = 0;
  gimg.save(aitSprintf("test/bg_model%02d.pgm",staticIndex++).c_str());
}

void 
HistogramBackgroundModel::saveModel(std::string filename) 
{
  mThresholds.save(filename.c_str(), "%d ");
};

Vector2i 
HistogramBackgroundModel::loadModel(std::string filename) 
{
  mThresholds.load(filename.c_str(), "%d");

  return Vector2i(mThresholds.width(), mThresholds.height());
};

//
// ACCESS
//

//
// INQUIRY
//

}; // namespace ait


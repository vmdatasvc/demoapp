/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "legacy/vision_tools/SvmVotingCascade.hpp"

namespace ait 
{

namespace vision
{

SvmVotingCascade::SvmVotingCascade()
{
}

SvmVotingCascade::~SvmVotingCascade()
{
}

void
SvmVotingCascade::init(CascadeClassifierPtr pCascadeClassifier, int maxVotingSamples)
{
  CascadeClassifier::CascadeMap& ccMap = pCascadeClassifier->getCascadeMap();
  CascadeClassifier::CascadeMap::iterator i;

  for (i = ccMap.begin(); i != ccMap.end(); ++i)
  {
    SvmVotingPtr pSvmVoting(new SvmVoting());
    SvmClassifierPtr pSvm = boost::dynamic_pointer_cast<SvmClassifier>(i->second->pClassifier);
    if (!pSvm.get()) PvUtil::exitError("The SvmVotingCascade has members that are not SvmClassifier");
    pSvmVoting->init(pSvm,maxVotingSamples);
    mCascade[i->first] = pSvmVoting;
  }

  mpCascadeClassifier = pCascadeClassifier;
}

void
SvmVotingCascade::calcResult()
{
  // Calculate the result from the votings.
  CascadeClassifier::CascadeMap& ccMap = mpCascadeClassifier->getCascadeMap();
  
  CascadeClassifier::NodeId curNode = mpCascadeClassifier->getRootId();
  
  for(;;)
  {
    SvmVotingPtr pVoting = mCascade[curNode];
    PVASSERT(pVoting.get());
    Label result = pVoting->getResult();
    if (result == ClassifierLabels::UNKNOWN)
    {
      mResultLabel = result;
      break;
    }
    CascadeClassifier::NodePtr pNode = ccMap[curNode];
    std::map<Label,CascadeClassifier::NodeId>::iterator iNextNode;
    iNextNode = pNode->nextNodeFromLabel.find(result);
    if (iNextNode == pNode->nextNodeFromLabel.end())
    {
      mResultLabel = result;
      break;
    }
    curNode = iNextNode->second;
  }
}

void 
SvmVotingCascade::addSample(const CascadeClassifier::Result& r)
{
  Lock lk(mMutex);

  CascadeClassifier::ResultMap::const_iterator i;

  for (i = r.resultCascade.begin(); i != r.resultCascade.end(); ++i)
  {
    SvmClassifier::ResultPtr pResult = boost::dynamic_pointer_cast<SvmClassifier::Result>(i->second);
    if (!pResult.get()) PvUtil::exitError("The SvmVotingCascade has members that are not SvmClassifier");
    mCascade[i->first]->addSample(*pResult);
  }

  calcResult();
}

void
SvmVotingCascade::merge(const SvmVotingCascade& from)
{
  Lock lk(mMutex);

  PVASSERT(mpCascadeClassifier == from.mpCascadeClassifier);

  Cascade::iterator i;
  Cascade::const_iterator iFrom;

  for (i = mCascade.begin(); i != mCascade.end(); ++i)
  {
    CascadeClassifier::NodeId id = i->first;
    iFrom = from.mCascade.find(id);
    i->second->merge(*(iFrom->second));
  }

  calcResult();
}

Label
SvmVotingCascade::getResult()
{
  Lock lk(mMutex);

  return mResultLabel;
}

//
// OPERATIONS
//

//
// ACCESS
//

//
// INQUIRY
//

}; // namespace vision

}; // namespace ait


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "legacy/vision_tools/CascadeClassifier.hpp"
#include "legacy/vision_tools/SvmClassifier.hpp"

namespace ait 
{

namespace vision
{

CascadeClassifier::CascadeClassifier()
{
}

CascadeClassifier::~CascadeClassifier()
{
}

void 
CascadeClassifier::init()
{
  // Initialize this object.
}

void
CascadeClassifier::initSvmNode(Settings& set, const NodeId& id)
{
  if (id.empty()) return;

  NodeId posNode = set.getString(id+"/positiveChild","");
  NodeId negNode = set.getString(id+"/negativeChild","");
  std::string paramsFile = set.getString(id+"/parametersFile","unspecified_svm_file.txt");

  SvmClassifierPtr pSvm(new SvmClassifier());
  pSvm->init(mpLabels, paramsFile);

  addBinaryClassifier(id,pSvm,posNode,negNode);

  initSvmNode(set,posNode);
  initSvmNode(set,negNode);
}

void 
CascadeClassifier::initSvm(ClassifierLabelsPtr pLabels, Settings& set)
{
  Classifier::init(pLabels);

  initSvmNode(set,set.getString("rootNode","1"));

#ifdef _DEBUG
  // Write the values of the labels in the registry for developers
  // feedback. Include UNKNOWN and UNCLASSIFIED labels.
  for (int label = -1; label <= mpLabels->getNumLabels(); label++)
  {
    set.setInt("LabelValues (debug output)/"+pLabels->getLabelString((Label)label),label);
  }
#endif
}

//
// OPERATIONS
//

void 
CascadeClassifier::classify(Sample& sample, Result& result)
{
  NodeId node = getRootId();

  Classifier::ResultPtr pResult;

  for (;;)
  {
    CascadeMap::iterator i = mCascade.find(node);
    if (i == mCascade.end()) break;

    NodePtr pNode = i->second;
    ClassifierPtr pClassifier = pNode->pClassifier;
    pResult = pClassifier->createResult();

    SampleMap::iterator iSample = sample.sampleCascade.find(node);
    if (iSample == sample.sampleCascade.end())
    {
      iSample = sample.sampleCascade.find(getRootId());
      if (iSample == sample.sampleCascade.end())
      {
        PvUtil::exitError("This sample does not define a root sample for the cascade");
      }
    }
    pClassifier->classify(*(iSample->second),*pResult);
    result.resultCascade[node] = pResult;

    LabelNodeMap::iterator iNextNode;
    iNextNode = pNode->nextNodeFromLabel.find(pResult->label);
    if (iNextNode == pNode->nextNodeFromLabel.end()) break;
    node = iNextNode->second;
  }

  // Get the resulting label from the leaf
  if (pResult.get()) 
  {
    result.label = pResult->label;
  }
}


void 
CascadeClassifier::classify(Classifier::Sample& sample, Classifier::Result& result)
{
  Sample *pSample = dynamic_cast<Sample *>(&sample);
  Result *pResult = dynamic_cast<Result *>(&result);
  if (!pSample || !pResult) PvUtil::exitError("Invalid sample or result type for CascadeClassifier");
  classify(*pSample,*pResult);
}

//
// ACCESS
//

//
// INQUIRY
//

}; // namespace vision

}; // namespace ait


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include "VisionMsg.hpp"
#include "MsgQueue.hpp"

namespace ait 
{

namespace vision
{

MsgQueue::MsgQueue()
: mpNotifyMutex(NULL),
  mpNotifyCond(NULL)
{
}

MsgQueue::~MsgQueue()
{
}

//
// OPERATIONS
//

void 
MsgQueue::put(VisionMsgPtr pMsg)
{
  // This is tricky (and probably can be improved) we must always lock
  // the notification mutex before the local mutex. The calling thread
  // can only do it in that order. Otherwise we would fall in an
  // interlocked deadlock (it happened): ExecutionPath::waitForMessages() is asking
  // if the queue is empty (locking mMutex), while the mpNotifyMutex is locked.
  // If this routine has mMutex locked and tries to lock mpNotificationMutex,
  // it will wait for mpNotifyMutex to be unlocked while ExecutionPath is waiting
  // for mMutex to be unlocked...
  if (mpNotifyMutex)
  {
    Lock lk(*mpNotifyMutex);
    Lock lk2(mMutex);
    mMsgs.push_back(pMsg);
    mpNotifyCond->notify_one();
  }
  else
  {
    Lock lk(mMutex);
    mMsgs.push_back(pMsg);
  }

}

Bool 
MsgQueue::get0(VisionMsgPtr& pMsg)
{
  Lock lk(mMutex);

  Bool wasEmpty = mMsgs.empty();
  if (!wasEmpty)
  {
    pMsg = *mMsgs.begin();
    mMsgs.pop_front();
  }
  else
  {
    pMsg.reset();
  }

  return !wasEmpty;
}

void 
MsgQueue::clear()
{
  Lock lk(mMutex);
  mMsgs.clear();
}

void 
MsgQueue::setNotifyVars(boost::mutex *pMutex, boost::condition *pCond)
{
  mpNotifyMutex = pMutex;
  mpNotifyCond = pCond;
}

//
// ACCESS
//

//
// INQUIRY
//

Bool
MsgQueue::isEmpty()
{
  Lock lk(mMutex);
  return mMsgs.empty();
}

std::string
MsgQueue::output() const
{
  Lock lk(mMutex);
  VisionMessages::const_iterator i;

  std::string out("(size=");
  out += aitSprintf("%d",mMsgs.size());

  out += ")";

  /*
  out += ",mMsgs={";

  for (i = mMsgs.begin(); i != mMsgs.end();)
  {
    out += (*i)->output();
    ++i;
    if (i != mMsgs.end()) out += ";";
  }

  out += "})";
  */

  return out;
}

int 
MsgQueue::size() const
  {
    return mMsgs.size();
  }


}; // namespace vision

}; // namespace ait


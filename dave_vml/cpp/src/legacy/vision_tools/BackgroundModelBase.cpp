/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "legacy/vision_tools/BackgroundModelBase.hpp"
#include <legacy/pv/ait/Image.hpp>

namespace ait 
{

BackgroundModelBase::BackgroundModelBase()
{
}

BackgroundModelBase::~BackgroundModelBase()
{
}

void
BackgroundModelBase::init(const std::string& settingsPath)
{
}

void
BackgroundModelBase::update(const Image8Ptr pInputImage)
{
  PvUtil::exitError("This background model does not support grayscale images");
}

void
BackgroundModelBase::foregroundSegment(const Image8Ptr pInputImage, 
                                              Image8Ptr pForegroundImage)
{
  PvUtil::exitError("This background model does not support grayscale images");
}

void
BackgroundModelBase::update(const Image32Ptr pInputImage)
{
  PvUtil::exitError("This background model does not support color images");
}

void
BackgroundModelBase::foregroundSegment(const Image32Ptr pInputImage, 
                                              Image8Ptr pForegroundImage)
{
  PvUtil::exitError("This background model does not support color images");
}

}; // namespace ait


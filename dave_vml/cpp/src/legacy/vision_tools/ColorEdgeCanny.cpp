/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include <string.h>

#include "legacy/vision_tools/ColorEdgeCanny.hpp"

namespace ait 
{

namespace vision
{

ColorEdgeCanny::ColorEdgeCanny():mIsInitialized(false)
{
}

ColorEdgeCanny::~ColorEdgeCanny()
{
}

void 
ColorEdgeCanny::init(const Settings &set, const int w, const int h)
{
  // Initialize this object.
	numOfCols = w;
	numOfRows = h;

	/* read in all settings from the registry  */
//	mSettings.changePath(settingsPath);
//	readVarsFromRegistry();
	mSettings = set;

	/* set the dimesions of all images */
	targetColSpaceImage.resize(numOfCols, numOfRows);
	gradientImage.resize(numOfCols, numOfRows);
	orientationImage.resize(numOfCols, numOfRows);
	luminanceEdge.resize(numOfCols, numOfRows);
	if(rescale)
	{
		rows = numOfRows/2;
		cols = numOfCols/2;
	}
	else
	{
		rows = numOfRows;
		cols = numOfCols;
	}
	chrominanceEdge.resize(cols, rows);
	gradMag.resize(numOfCols, numOfRows);
	histImage.resize(numOfCols, numOfRows);
	histImage.setAll(0);
	/* setting pointers to respective data */
	pTargetColSpaceImageData = (unsigned char*)targetColSpaceImage.pointer();
	pGradientImageData = (unsigned char*)gradientImage.pointer();
	//pOrientationImageData = (unsigned char*)orientationImage.pointer();
    pOrientationImageData = orientationImage.pointer();
	pLuminanceEdgesImageData = (unsigned char*) luminanceEdge.pointer();
	pChrominanceEdgeImageData = (unsigned char*) chrominanceEdge.pointer();
	
	rInitial = 1;
	cInitial = 1;
	initPos = 4 * (rInitial * numOfCols + cInitial);
	hist.allocate(1, 256);

//	mBenchmark.init(mSettings.getPath(), "Color Edge Detection");
//	mBenchmark.setEnable(enableBenchmark);
//	mBenchmarkLuminanceEdge.init(mSettings.getPath(), "Luminance Edge Detection");
//	mBenchmarkLuminanceEdge.setEnable(enableBenchmark);
//	mBenchmarkChrominanceEdge.init(mSettings.getPath(), "Chrominance Edge Detection");
//	mBenchmarkChrominanceEdge.setEnable(enableBenchmark);
//	mBenchmarkFuseEdges.init(mSettings.getPath(), "Fuse Edges");
//	mBenchmarkFuseEdges.setEnable(enableBenchmark);
//	mBenchmarkPercentile.init(mSettings.getPath(), "Percentile+scaling");
//	mBenchmarkPercentile.setEnable(enableBenchmark);

	offSet = 4 *(cInitial + cInitial);

    readVarsFromRegistry();

  mIsInitialized = true;
}


void
ColorEdgeCanny::process(Image32 &inImage)
{	 
	//readVarsFromRegistry();
//	mBenchmark.beginSample();
	/* convert incoming image to target color space */
	convertToTargetColorSpace(inImage);

	EdgeByGradient();
//	mBenchmark.endSample();
}

void
ColorEdgeCanny::process(Image32 &inImage, Rectangle<int>& rect)
{
  Image32 cropImg;
  cropImg.crop(inImage, rect.x0, rect.y0, rect.width(), rect.height());

  process(cropImg);

  inImage.paste(cropImg, rect.x0, rect.y0);
}

void
ColorEdgeCanny::readVarsFromRegistry()
{
	colorSpace = mSettings.getString("Color Space [YCbCr -or- YUV]","YCbCr");
	mMinSignificantEdgeThreshold = mSettings.getInt("Minimum Sigificant Edge Threshold",35);
	rescale = mSettings.getBool("Rescale [1 -OR- 0]",0);
	chMagBooster = mSettings.getFloat("Chrominance Magnitude Multiplier",3);
	LuMagLimiter = mSettings.getFloat("Luminance Magnitude Limiter",2);
	LuMagLimiter = 1.0f/LuMagLimiter;
	onlyGrayScaleEdgesDesired = mSettings.getBool(
		"OnlyGrayscaleEdges [make sure 'Detection Method' is 'gradient']",false);
	enableBenchmark = mSettings.getBool("Enable Benchmarking [1 -OR- 0]",1);
	percentile = mSettings.getFloat("Percentile",90.0f);
}


//
// OPERATIONS
//
void
ColorEdgeCanny::convertToTargetColorSpace(Image32 &inImage)
{

	if(!strcmp(colorSpace.c_str(),"YUV"))
		ait::ColorConvert::convert(inImage, PvImageProc::RGB_SPACE, targetColSpaceImage, PvImageProc::YUV_SPACE);
	else
		ait::ColorConvert::convert(inImage, PvImageProc::RGB_SPACE, targetColSpaceImage, PvImageProc::YCBCR_SPACE);
	

}


void
ColorEdgeCanny::EdgeByGradient()
{
	gradientImage.setAll(0);
	/* find edges in luminance component */
	EdgesOfLuminanceComponent();
	if(onlyGrayScaleEdgesDesired)
	{
		return;
	}
	/* find edges in color components */
	EdgesOfColorComponents();
	/* fuse edges in luminance and chrominance images */
	if(rescale)
		RescaledFuseEdges();
	else
		FuseEdgeResponses();

}

void
ColorEdgeCanny::EdgesOfLuminanceComponent()
{
//	mBenchmarkLuminanceEdge.beginSample();
	pos = initPos;
	pos1 = pos - 4*numOfCols;
	pos2 = pos + 4*numOfCols;
	pTargetColSpaceImageData = (unsigned char*)targetColSpaceImage.pointer();

    unsigned int posOri = pos/4;
    unsigned int offSetOri = offSet/4;

    /* first initialize border to zero */
    unsigned int lastRow = numOfRows*numOfCols-1;
    for (c = 0; c < rInitial*numOfCols; c++)      
    {
      pOrientationImageData[c]=0;
      ((unsigned int*)(pGradientImageData))[c]=0;
      ((unsigned int*)(pLuminanceEdgesImageData))[c]=0;

      pOrientationImageData[lastRow-c]=0;
      ((unsigned int*)(pGradientImageData))[lastRow-c]=0;
      ((unsigned int*)(pLuminanceEdgesImageData))[lastRow-c]=0;
    }

    for (r = rInitial; r < numOfRows - rInitial; r++)
      for(c = 0; c < cInitial; c++)
      {
        pOrientationImageData[r*numOfCols+c]=0;
        ((unsigned int*)(pGradientImageData))[r*numOfCols+c]=0;
        ((unsigned int*)(pLuminanceEdgesImageData))[r*numOfCols+c]=0;

        pOrientationImageData[(r+1)*numOfCols-c-1]=0;
        ((unsigned int*)(pGradientImageData))[(r+1)*numOfCols-c-1]=0;
        ((unsigned int*)(pLuminanceEdgesImageData))[(r+1)*numOfCols-c-1]=0;
      }
    /* end of initialization */

	for (r = rInitial; r < numOfRows - rInitial; r++)
	{
		for(c = cInitial; c < numOfCols - cInitial; c++)
		{
			hDiff = pTargetColSpaceImageData[pos+4] - pTargetColSpaceImageData[pos-4];
			vDiff = pTargetColSpaceImageData[pos2] - pTargetColSpaceImageData[pos1];
			pos1 += 4; pos2 += 4;
			northEastDiff = pTargetColSpaceImageData[pos1] 
			- pTargetColSpaceImageData[pos2-8];
			southEastDiff = pTargetColSpaceImageData[pos2] 
			- pTargetColSpaceImageData[pos1-8];
			
			fX = hDiff + (southEastDiff + northEastDiff)/2;
			fY = vDiff + (southEastDiff - northEastDiff)/2;

			temp = sqrt((float)(fX*fX + fY*fY));
			//fMag = (temp > 255.0) ? 255 : floor(temp);
            fMag = (temp > 255.0) ? 255 : temp;

			orientationY = atan2((float)(fY), (float)(fX));
			//orientationY = atan2((float)(fY - 128), (float)(fX-128));

			if(orientationY < 0)
				orientationY += (float)PI;
			
            //orientationY = 180.0*(orientationY/PI);

			//pOrientationImageData[pos] = static_cast<Uint8>(orientationY);
            //pOrientationImageData[posOri++] = (unsigned int)orientationY;
            pOrientationImageData[posOri++] = orientationY;
            

            fX = ((fX + 511)/2/2); //range is -511 to 512. adding 511 (range now is 0 to 1023).
			fY = (fY + 511)/2/2; //normalize by 0.249 (255/1024) i.e. approx .25 (/ -or * by 4)

			pLuminanceEdgesImageData[pos++] = static_cast<Uint8>(fX);
			pLuminanceEdgesImageData[pos++] = static_cast<Uint8>(fY);
			pLuminanceEdgesImageData[pos++] = (fMag);
			pLuminanceEdgesImageData[pos++] = fMag/2;

			// populating the gradient image 
			if(onlyGrayScaleEdgesDesired)
			{
				pGradientImageData[pos-4] = static_cast<Uint8>(fX);
				pGradientImageData[pos-3] = static_cast<Uint8>(fY);
				pGradientImageData[pos-2] = (fMag);
				pGradientImageData[pos-1] = fMag/2;
			}
		}
        posOri+=offSetOri;
		pos+=offSet;
		pos1+=offSet;
		pos2+=offSet;
	}
//	mBenchmarkLuminanceEdge.endSample();
}



void
ColorEdgeCanny::EdgesOfColorComponents()
{
//	mBenchmarkChrominanceEdge.beginSample();
	/* rescale the two color component images if desired */
	if(rescale)
	{
		targetColSpaceImage.rescale(cols, rows, RESCALE_CUBIC);
		pTargetColSpaceImageData = (unsigned char*)targetColSpaceImage.pointer();
	}


	pos = initPos + 1;//the '+1' takes it to Cb/Y component
	pos1 = pos - 4 * cols;
	pos2 = pos + 4 * cols;
	try{
	for (r = rInitial; r < rows - rInitial; r++)
	{
		for(c = cInitial; c < cols - cInitial; c++)
		{
			/* Cb/U component */
			hDiff = pTargetColSpaceImageData[pos+4] - pTargetColSpaceImageData[pos-4];
			vDiff = pTargetColSpaceImageData[pos2] - pTargetColSpaceImageData[pos1];
			northEastDiff = pTargetColSpaceImageData[pos1+4] 
			- pTargetColSpaceImageData[pos2-4];
			southEastDiff = pTargetColSpaceImageData[pos2+4] 
			- pTargetColSpaceImageData[pos1-4];
			fXcb = hDiff + (southEastDiff + northEastDiff)/2;
			fYcb = vDiff + (southEastDiff - northEastDiff)/2;
			temp = sqrt((float)(fXcb*fXcb + fYcb*fYcb));
			fMagcb = (temp > 255.0) ? 255 : floor(temp);

			/* moving pointers to Cr byte */
			pos++;
			pos1++;
			pos2++;

			/* Cr/V component */
			hDiff = pTargetColSpaceImageData[pos+4] - pTargetColSpaceImageData[pos-4];
			vDiff = pTargetColSpaceImageData[pos2] - pTargetColSpaceImageData[pos1];
			northEastDiff = pTargetColSpaceImageData[pos1+4] 
			- pTargetColSpaceImageData[pos2-4];
			southEastDiff = pTargetColSpaceImageData[pos2+4] 
			- pTargetColSpaceImageData[pos1-4];
			fXcr = hDiff + (southEastDiff + northEastDiff)/2;
			fYcr = vDiff + (southEastDiff - northEastDiff)/2;
			temp = sqrt((float)(fXcr*fXcr+ fYcr*fYcr));
			fMagcr = (temp > 255.0) ? 255 : floor(temp);

			/* combining the results */
			tempMag = (unsigned int)std::max(fMagcb, fMagcr);//((unsigned int)fMagcb + (unsigned int)fMagcr)/2;//
			fMag = (tempMag > 255) ? 255 : tempMag;
			if((fXcb < 0) && (fXcr < 0)) /* taking the greatest -ve edge */
				fX = std::min(fXcb, fXcr);
			else
				if(abs(fXcb) > abs(fXcr)) /* if cb > cr (cb can be +ve or -ve, again take the stronger edge */
					fX = fXcb;
				else
					fX = fXcr;
			fX = (fX + 511)/4;

			if((fYcb < 0) && (fYcr < 0)) /* taking the greatest -ve edge */
				fY = std::min(fYcb, fYcr);
			else
				if(abs(fYcb) > abs(fYcr)) /* if cb > cr (cb can be +ve or -ve, again take the stronger edge */
					fY = fYcb;
				else
					fY = fYcr;
			fY = (fY + 511)/4;

			pChrominanceEdgeImageData[pos-2] = static_cast<Uint8>(fX);
			pChrominanceEdgeImageData[pos-1] = static_cast<Uint8>(fY);
			pChrominanceEdgeImageData[pos++] = fMag;
			pChrominanceEdgeImageData[pos++] = fMag/2;
			pos++;
			pos1+=3;
			pos2+=3;
		}
		pos+=offSet;//-4;
		pos1+=offSet;//-4;
		pos2+=offSet;//-4;
	}
	}
	catch (...) 
	{
		FILE*fp;
		fp = fopen("EXCEPTIONS.DAT","a");
    fprintf(fp,"row = %d\ncol = %d\n,pos1 = %d\npos = %d\npos2 = %d\n", r, c, pos1, pos, pos2);
		fclose(fp);
	}
//	mBenchmarkChrominanceEdge.endSample();
}

void
ColorEdgeCanny::RescaledFuseEdges()
{
//	mBenchmarkFuseEdges.beginSample();
	maxMag=0; minMag=0;
	pos = initPos;//4 * (rInitial * numOfCols + cInitial);
	int rCh, cCh;

	for (r = rInitial, rCh = r; r < numOfRows - rInitial; r++)
	{
		for(c = cInitial, cCh = c; c < numOfCols - cInitial; c++)
		{
			rCh = (r+1)/2;
			cCh = (c+1)/2;

			/* Magn */
			fMag = 0;
			fMagLu = pLuminanceEdgesImageData[pos + 2];
			fMagCh = RED(chrominanceEdge(cCh,rCh));
			tempMag = (unsigned int)(((float)fMagLu*LuMagLimiter + (float)fMagCh*chMagBooster)/(LuMagLimiter+chMagBooster));
			fMag = (tempMag > 255) ? 255 : tempMag;
			gradMag(c,r) = fMag;
			// to find the max and min magnitude - in order to scale it later
			if(maxMag < fMag)
				maxMag = fMag;
			if(minMag >= fMag)
				minMag = fMag;

			/* X */
			fMagX = 0;
			fMagXY = pLuminanceEdgesImageData[pos];
			fMagXC = BLUE(chrominanceEdge(cCh,rCh));
			unweightedXC = (4 * fMagXC) - 511;
			unweightedXY = (4 * fMagXY) - 511;
			tempMag = (unsigned int)(((float)unweightedXY*LuMagLimiter + (float)unweightedXC*chMagBooster)/(LuMagLimiter+chMagBooster));
			tempMag = (tempMag + 511)/4;
			fMagX = (tempMag > 255) ? 255 : tempMag;

			/* Y */
			fMagY = 0;
			fMagYY = pLuminanceEdgesImageData[pos + 1];
			fMagYC = GREEN(chrominanceEdge(cCh,rCh));
			unweightedYC = (4*fMagYC) - 511;
			unweightedYY = (4*fMagYY) - 511;
			tempMag = (unsigned int)(((float)unweightedYY * LuMagLimiter + (float)unweightedYC * chMagBooster)/(LuMagLimiter+chMagBooster));
			tempMag = (tempMag + 511)/4;
			fMagY = (tempMag > 255) ? 255 : tempMag;


			/* populating the gradient image */
			pGradientImageData[pos++] = fMagX;
			pGradientImageData[pos++] = fMagY;
			pGradientImageData[pos++] = fMag;
			pGradientImageData[pos++] = fMag/2;
		}
		pos+=offSet;
	}

	image_proc::smoothImage(gradientImage, image_proc::SMOOTH_GAUSSIAN, 3);
//	mBenchmarkFuseEdges.endSample();
//	mBenchmarkPercentile.beginSample();
	findPercentile(percentile);
	scaleFusedEdgeResponse();
//	mBenchmarkPercentile.endSample();
}   


void
ColorEdgeCanny::FuseEdgeResponses()
{
//	mBenchmarkFuseEdges.beginSample();
	
	maxMag=0; minMag=0;
	pos = initPos;
	for (r = rInitial; r < numOfRows - rInitial; r++)
	{
		for(c = cInitial; c < numOfCols - cInitial; c++)
		{
			/* Magn */
			fMag = 0;
			fMagLu = pLuminanceEdgesImageData[pos + 2];
			fMagCh = pChrominanceEdgeImageData[pos+2];
			tempMag = (unsigned int)(((float)fMagLu*LuMagLimiter + (float)fMagCh*chMagBooster)/(LuMagLimiter+chMagBooster));
			fMag = (tempMag > 255) ? 255 : tempMag;
			gradMag(c,r) = fMag;
			// to find the max and min magnitude - in order to scale it later
			if(maxMag < fMag)
				maxMag = fMag;
			if(minMag >= fMag)
				minMag = fMag;

			/* X */
			fMagX = 0;
			fMagXY = pLuminanceEdgesImageData[pos];
			fMagXC = pChrominanceEdgeImageData[pos];
			unweightedXC = 4*fMagXC - 511;
			unweightedXY = 4*fMagXY - 511;
			tempMag = (unsigned int)(((float)unweightedXY*LuMagLimiter + (float)unweightedXC*chMagBooster)/(LuMagLimiter+chMagBooster));
			tempMag = (tempMag + 511)/4;
			fMagX = (tempMag > 255) ? 255 : tempMag;

			/* Y */
			fMagY = 0;
			fMagYY = pLuminanceEdgesImageData[pos + 1];
			fMagYC = pChrominanceEdgeImageData[pos+1];
			unweightedYC = 4*fMagYC - 511;
			unweightedYY = 4*fMagYY - 511;
			tempMag = (unsigned int)(((float)unweightedYY * LuMagLimiter + (float)unweightedYC * chMagBooster)/(LuMagLimiter+chMagBooster));
			tempMag = (tempMag + 511)/4;
			fMagY = (tempMag > 255) ? 255 :tempMag;


			/* populating the gradient image */
			pGradientImageData[pos++] = fMagX;
			pGradientImageData[pos++] = fMagY;
			pGradientImageData[pos++] = fMag;
			pGradientImageData[pos++] = fMag/2;
		}
		pos+=offSet;
	}
	image_proc::smoothImage(gradientImage, image_proc::SMOOTH_GAUSSIAN, 3);
//	mBenchmarkFuseEdges.endSample();
//	mBenchmarkPercentile.beginSample();
	findPercentile(percentile);
	scaleFusedEdgeResponse();
//	mBenchmarkPercentile.endSample();
}


void
ColorEdgeCanny::findPercentile(float& val)
{
	float p = 0.0f;
	Uint oldMaxmag = maxMag;
	hist.compute(gradMag, Rectanglei(cInitial, rInitial, numOfCols - cInitial, numOfRows - rInitial));
	Uint numSamples = hist.getNumSamples();
	Uint c, i, sum = 0;
	for(c = 0; c < hist.getNumChannels(); c++)
		for(i = 0; i < hist.getNumBins()(c); i++)
		{
			sum += hist(c,i);
			p = (float)sum*100.0f/(float)numSamples;
			if(p >= val)
			{
				maxMag = i;
				break;
			}
		}
}


void
ColorEdgeCanny::scaleFusedEdgeResponse()
{
	multFactor = 255.0f/(float)(maxMag - minMag);
	pos = initPos+2;
	for (r = rInitial; r < numOfRows - rInitial; r++)
	{
		for(c = cInitial; c < numOfCols - cInitial; c++)
		{
			
			fMag = RED(gradientImage(c,r)) - minMag;
			temp = multFactor * fMag;
			fMag = (temp > 255.0f) ? 255 : (int)(temp);
			pGradientImageData[pos] = fMag;
			gradMag(c,r) = fMag;
			pos+=4;
		}
		pos+=offSet;
	}
}



/*
void
ColorEdgeDetection::EdgeByTensorGradient()
{
	int r, c;
	unsigned char rt, tr, br, lt, tl, bl, u, d;
	float gXR, gYR, gXG, gYG, gXB, gYB;
	float gXX, gYY, gXY;
	float F1, F2;
	int ImageCols = numOfCols;
	int ImageRows = numOfRows;
	//F.resize(ImageCols, ImageRows);
	BasicMatrix<float> Ftemp;
	Ftemp.resize(ImageCols,ImageRows);
	//F.setAll(0);
	float m = 0;
	float th,theta1, theta2;

	for (r = rInitial; r < ImageRows-rInitial; r++)
	{
		for(c = cInitial; c < ImageCols-cInitial; c++)
		{
			u = BLUE(targetColSpaceImage(c,r-1));			d = BLUE(targetColSpaceImage(c,r+1));
			rt = BLUE(targetColSpaceImage(c+1,r));		lt = BLUE(targetColSpaceImage(c-1,r));
			tr = BLUE(targetColSpaceImage(c+1,r-1));	tl = BLUE(targetColSpaceImage(c-1,r-1));
			br = BLUE(targetColSpaceImage(c+1,r+1));	bl = BLUE(targetColSpaceImage(c-1,r+1));
			gXB = (rt - lt) + (float)(br - tl + tr - bl)/2;
			gYB = (d - u) + (float)(br - tl + bl - tr)/2;

			u = GREEN(targetColSpaceImage(c,r-1));			d = GREEN(targetColSpaceImage(c,r+1));
			rt = GREEN(targetColSpaceImage(c+1,r));			lt = GREEN(targetColSpaceImage(c-1,r));
			tr = GREEN(targetColSpaceImage(c+1,r-1));		tl = GREEN(targetColSpaceImage(c-1,r-1));
			br = GREEN(targetColSpaceImage(c+1,r+1));		bl = GREEN(targetColSpaceImage(c-1,r+1));
			gXG = (rt - lt) + (float)(br - tl + tr - bl)/2;
			gYG = (d - u) + (float)(br - tl + bl - tr)/2;

			u = RED(targetColSpaceImage(c,r-1));			d = RED(targetColSpaceImage(c,r+1));
			rt = RED(targetColSpaceImage(c+1,r));			lt= RED(targetColSpaceImage(c-1,r));
			tr = RED(targetColSpaceImage(c+1,r-1));		tl = RED(targetColSpaceImage(c-1,r-1));
			br = RED(targetColSpaceImage(c+1,r+1));		bl = RED(targetColSpaceImage(c-1,r+1));
			gXR = (rt - lt) + (float)(br - tl + tr - bl)/2;
			gYR = (d - u) + (float)(br - tl + bl - tr)/2;

			gXX = gXR * gXR + gXG * gXG + gXB * gXB;
			gYY = gYR * gYR + gYG * gYG + gYB * gYB;
			gXY = gXR * gYR + gXG * gYG + gXB * gYB;

			if(gXX == gYY)
				theta1 = (float)PI_2;
			else
				theta1 = 0.5 * atan(2 * (float)gXY/(float)(gXX - gYY));
			if(theta1 <= 0)
				theta2 = theta1 + PI_2;
			else
				theta2 = theta1 - PI_2;

			F1 = (sqrt(0.5*((gXX+gYY)+(gXX-gYY)*cos(2*theta1)+2*gXY*sin(2*theta1))));
			F2 = (sqrt(0.5*((gXX+gYY)+(gXX-gYY)*cos(2*theta2)+2*gXY*sin(2*theta2))));
			Ftemp(c, r) = std::max(F1,F2);
			if(m < std::max(F1,F2))
				m = Ftemp(c,r);
			if(Ftemp(c,r) == F1)
				th = theta1;
			else
				th = theta2;
			th+=(float)PI_2;
			th*=180/(float)PI;
			pOrientationImageData[4*(r*ImageCols+c)]= floor(th);
		}
	}
	unsigned char temp;
	for(r = rInitial; r < ImageRows - rInitial; r++)
		for(c = cInitial; c < ImageCols - cInitial; c++)
		{
			Ftemp(c,r) = (Ftemp(c,r)/m)*255;
			temp =(Ftemp(c,r) > 255) ? 255 : floor(Ftemp(c,r));
			pGradientImageData[4*(r*ImageCols+c)] = temp;
			pGradientImageData[4*(r*ImageCols+c)+1] = temp;
			pGradientImageData[4*(r*ImageCols+c)+2] = temp;
			pGradientImageData[4*(r*ImageCols+c)+3] = temp;
			//F(c,r) = temp;
		}
		//gradFrame.save("gradFrame.png");
		//F.save("F.pgm");
}*/

//
// ACCESS
//

void
ColorEdgeCanny::clear()
{
}

/**
* Retrieve the gray scale gradient image	
*/
void 
ColorEdgeCanny::GetGradientImage(Image8& outImage)
{ 
	// if flag onlyGrayScaleEdgesDesired is turned ON, then gradientImage contains ONLY luminance information
	// else magnitude of combined luminance AND chrominance is returned.
  outImage.resize(gradientImage.width(), gradientImage.height());

	for(int i = 0; i < numOfRows; i++)
		for(int j = 0; j < numOfCols; j++)
			outImage(j,i) = RED(gradientImage(j,i));
}
/**
* Retrieve ONLY the color gradient image	
*/
void 
ColorEdgeCanny::GetColorGradient(Image8& outImage)
{
	if(onlyGrayScaleEdgesDesired)
	{
		outImage.setAll(0);
		return;
	}
	int rows, cols;
	if(rescale)
	{
		rows = numOfRows/2;
		cols = numOfCols/2;
	}
	else
	{
		rows = numOfRows;
		cols = numOfCols;
	}
	if(outImage.rows() != rows && outImage.cols() != cols)
		outImage.resize(cols, rows);
	for(int i = 0; i < rows; i++)
		for(int j = 0; j < cols; j++)
			outImage(j,i) = RED(chrominanceEdge(j,i));
}



//
// INQUIRY
//

}; // namespace vision

}; // namespace ait


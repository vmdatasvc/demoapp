/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "legacy/vision_tools/MapDifferentialMotion.hpp"

#include "legacy/vision_tools/SmoothBoxTrajectory.hpp"
#include "legacy/vision_tools/RunningAvgTrajectory.hpp"
#include "legacy/vision_tools/SmoothAnisotropic.hpp"
#include "legacy/vision_tools/ZPushFilter.hpp"

namespace ait 
{

namespace vision
{

MapDifferentialMotion::MapDifferentialMotion()
: mIsInitialized(false)
{
}

MapDifferentialMotion::~MapDifferentialMotion()
{
}

void 
MapDifferentialMotion::init()
{
  // Initialize this object.
  mIsInitialized = true;

	mSettings.changePath("MapDifferentialMotion");
	mReadSettingsOnlyOnInit = mSettings.getBool("readSettingsOnlyOnInit",true);

  // Get registry settings
	mSettings.changePath("MapDifferentialMotion/SmoothTrajectory");
	mEnableSmoothing = mSettings.getBool("enabled [bool]",1);

	// Smoothing filters
	if (mEnableSmoothing)
	{
		SmoothTrajectory<float>*p = NULL;

		if (mSettings.getBool("Box/enabled [bool]",false)) 
		{
			float f = mSettings.getFloat("Box/boxSize",0.1f);
			p = new SmoothBoxTrajectory();
			(dynamic_cast<SmoothBoxTrajectory*>(p))->init(mSettings.getInt("Box/historySize",5),Vector3f(f,f,f));
		}
		else if (mSettings.getBool("Anisotropic/enabled [bool]",false)) 
		{
			p = new SmoothAnisotropic();
			(dynamic_cast<SmoothAnisotropic*>(p))->init(mSettings.getInt("Anisotropic/paramK",50),
				mSettings.getFloat("Anisotropic/paramLambda",0.15f));
		}
		else if (mSettings.getBool("ZPushFilter/enabled [bool]",false)) 
		{
			p = new ZPushFilter();
			(dynamic_cast<ZPushFilter*>(p))->init(mSettings.getInt("ZPushFilter/paramK",50),
				mSettings.getFloat("ZPushFilter/paramLambda",0.15f));
		}
		else if (mEnableRunningAvg = mSettings.getBool("Avg/enabled [bool]",true)) 
		{
			p = new RunningAvgTrajectory<float>();
			Vector3f minSDev,maxSDev;
			minSDev = mSettings.getCoord3f("Avg/minSDev",Vector3f(0.015f,0.0f,0.0f));
			maxSDev = mSettings.getCoord3f("Avg/maxSDev",Vector3f(0.035f,0.0f,0.0f));
			Matrix<float> minM(3,3), maxM(3,3);
			minM.zero(); maxM.zero();
			minM(0,0) = minSDev(0); minM(1,1) = minSDev(1); minM(2,2) = minSDev(2);
			maxM(0,0) = maxSDev(0); maxM(1,1) = maxSDev(1); maxM(2,2) = maxSDev(2);
			(dynamic_cast<RunningAvgTrajectory<float>*>(p))->init(mSettings.getInt("Avg/historySize",5),
				minM,maxM);
		}

		if (p)
			mpSmoothTrajectory.reset(p);
		else
			mpSmoothTrajectory.reset();
	}

	mSettings.changePath("MapDifferentialMotion/Sensitivity");
	mEnableWeightedSensitivity = mSettings.getBool("enableWeightedSensitivity [bool]",true);
	mHeadMotionSensitivity = mSettings.getCoord3f("headMotionSensitivity",Vector3f(10.0f,15.0f,0.0f));
	mHeadMotionMinSensitivity = mSettings.getCoord3f("headMotionMinSensitivity",Vector3f(10.0f,15.0f,0.0f));
}

void
MapDifferentialMotion::computeCursorDisp(const Vector3f& disp, Vector3f& cursorDisp)
{
	if (!mReadSettingsOnlyOnInit)
		getSettings();

	// Set the new mouse position
	// Image displacement is flipped with respect to cursor displacement
	mUnFilteredMousePos.x = mUnFilteredMousePos.x + -1.0f*disp.x;
	mUnFilteredMousePos.y = mUnFilteredMousePos.y + disp.y;

	// Clip to viewable area (0..1) in normalized coordinates
	//mUnFilteredMousePos.x = ((mUnFilteredMousePos.x > 0) ? std::min(mUnFilteredMousePos.x,1.0f) : 0.0f);
	//mUnFilteredMousePos.y = ((mUnFilteredMousePos.y > 0) ? std::min(mUnFilteredMousePos.y,1.0f) : 0.0f);

	//PVMSG("%f %f\n",headDisp.x,headDisp.y);
	// Smoothing of Unfiltered positions
	if (mpSmoothTrajectory.get())
	{
		// Get the filtered mouse position
		Matrix<float> mVec;
		mVec.resize(1,3);

		mVec(0) = mUnFilteredMousePos.x;
		mVec(1) = mUnFilteredMousePos.y;
		mVec(2) = mUnFilteredMousePos.z;

		mpSmoothTrajectory->update(mVec);

		mFilteredMousePos.set(mVec(0),mVec(1),mVec(2));
	}
	else
	{
		mFilteredMousePos = mUnFilteredMousePos;
	}

	//pHead->setOrigHeadDisp(headDisp);

	// The head motion sensitivity is calculated from a smooth step function. The default value 
	// of head motion sensitivity is the max head value sensitivity.
	Vector3f finalHeadSensitivity(mHeadMotionSensitivity);
	float meanWt = 0.0f, currentWt = 0.0f;
	if (mEnableWeightedSensitivity && mEnableRunningAvg && mpSmoothTrajectory.get())
	{
		Vector2f range;
		(dynamic_cast<RunningAvgTrajectory<float>*>(mpSmoothTrajectory.get()))->getWeights(&meanWt,&currentWt);				
		(dynamic_cast<RunningAvgTrajectory<float>*>(mpSmoothTrajectory.get()))->getRange(range.x,range.y);				

		//PVMSG("%f | %f %f\n",currentWt,range.x,range.y);

		// Assign a minimum sensitivity for even the smallest motion and zero if there is no motion.
		// This means that if the wt is non-zero, then final_sensitivity = min_sensitivity +
		//	wt*(max_sensitivity - min_sensitivity), otherwise  final_sensitivity = 0. 
		if (currentWt > 0.0f)
		{
			finalHeadSensitivity.sub(mHeadMotionSensitivity,mHeadMotionMinSensitivity);
			finalHeadSensitivity.mult(currentWt);
			finalHeadSensitivity.add(mHeadMotionMinSensitivity);
		}
		else
		{
			finalHeadSensitivity.zero();
		}
	}

	// Compute the "sensitized" displacement
	cursorDisp.set(finalHeadSensitivity.x * (mFilteredMousePos.x - mPrevFilteredMousePos.x),
		finalHeadSensitivity.y * (mFilteredMousePos.y - mPrevFilteredMousePos.y));
	//PVMSG("%f %f | %f %f | %f\n",disp.x,disp.y,cursorDisp.x,cursorDisp.y,currentWt);

	mPrevFilteredMousePos = mFilteredMousePos;
}

//
// OPERATIONS
//

void
MapDifferentialMotion::getSettings()
{
	mSettings.changePath("MapDifferentialMotion/SmoothTrajectory");
	mEnableSmoothing = mSettings.getBool("enabled [bool]",1);
  mEnableRunningAvg = mSettings.getBool("Avg/enabled [bool]",true);

	// Smoothing filters
	if (mEnableSmoothing)
	{
		if (mEnableRunningAvg) 
		{
			Vector3f minSDev,maxSDev;
			minSDev = mSettings.getCoord3f("Avg/minSDev",Vector3f(0.015f,0.0f,0.0f));
			maxSDev = mSettings.getCoord3f("Avg/maxSDev",Vector3f(0.035f,0.0f,0.0f));
			Matrix<float> minM(3,3), maxM(3,3);
			minM.zero(); maxM.zero();
			minM(0,0) = minSDev(0); minM(1,1) = minSDev(1); minM(2,2) = minSDev(2);
			maxM(0,0) = maxSDev(0); maxM(1,1) = maxSDev(1); maxM(2,2) = maxSDev(2);
      if (!mpSmoothTrajectory.get())
      {
        SmoothTrajectory<float>*p = new RunningAvgTrajectory<float>();
        (dynamic_cast<RunningAvgTrajectory<float>*>(p))->init(mSettings.getInt("Avg/historySize",5),
          minM,maxM);
        if (p)
          mpSmoothTrajectory.reset(p);
        else
          mpSmoothTrajectory.reset();
      }
      else
      {
			(dynamic_cast<RunningAvgTrajectory<float>*>(mpSmoothTrajectory.get()))->setRange(minM,maxM);
      }
		}
	}

  // Reset the smoothing filter object
  if (!mEnableSmoothing || !mEnableRunningAvg)
  {
    if (mpSmoothTrajectory.get())
    {
      mpSmoothTrajectory.reset();
		}
	}

	mSettings.changePath("MapDifferentialMotion/Sensitivity");
	mEnableWeightedSensitivity = mSettings.getBool("enableWeightedSensitivity [bool]",true);
	mHeadMotionSensitivity = mSettings.getCoord3f("headMotionSensitivity",Vector3f(10.0f,15.0f,0.0f));
	mHeadMotionMinSensitivity = mSettings.getCoord3f("headMotionMinSensitivity",Vector3f(10.0f,15.0f,0.0f));
}

//
// ACCESS
//

//
// INQUIRY
//

}; // namespace vision

}; // namespace ait


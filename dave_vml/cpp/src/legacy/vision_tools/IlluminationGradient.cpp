/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include <legacy/pv/ait/Matrix.hpp>

#include "legacy/vision_tools/IlluminationGradient.hpp"

// This is implemented at the end of this file.
static int nr_svdcmp(float **a, int m, int n, float w[], float **v);

namespace ait 
{

namespace vision
{

using namespace std;

IlluminationGradient::IlluminationGradient()
{
}

IlluminationGradient::~IlluminationGradient()
{
}

//
// OPERATIONS
//

void 
IlluminationGradient::process(Image8& dstImg, const Image8& srcImg)
{
  // Check if the mask has not been set.
  if (mMask.width() != 1)
  {
    if (mMask.width() != srcImg.width() || mMask.height() != srcImg.height())
    {
      PvUtil::exitError("IlliminationGradient: Mask size doesn't match the image size.");
    }
  }
  else
  {
    mMask.resize(srcImg.width(),srcImg.height());
    mMask.setAll(1);
  }

  Matrix<Float> gradientMultipliers(3,3); 
  Matrix<Float> gradientCoefficients(3,1);
  Matrix<Float> transGradientMultipliers(3,4);
  
  gradientCoefficients.zero();
  gradientMultipliers.zero();

  Uint x,y;
  for(y=0;y<(srcImg.height());y++)
  {
    for(x=0;x<(srcImg.width());x++)
    {
      if (mMask(x,y))
      {
        gradientCoefficients(0,0)=gradientCoefficients(0,0)+x*srcImg(x,y);
        gradientCoefficients(1,0)=gradientCoefficients(1,0)+y*srcImg(x,y);
        gradientCoefficients(2,0)=gradientCoefficients(2,0)+srcImg(x,y);
      
        gradientMultipliers(0,0)=gradientMultipliers(0,0)+x*x;
        gradientMultipliers(0,1)=gradientMultipliers(0,1)+x*y;
        gradientMultipliers(0,2)=gradientMultipliers(0,2)+x;
        gradientMultipliers(1,0)=gradientMultipliers(1,0)+x*y;
        gradientMultipliers(1,1)=gradientMultipliers(1,1)+y*y;
        gradientMultipliers(1,2)=gradientMultipliers(1,2)+y;
        gradientMultipliers(2,0)=gradientMultipliers(2,0)+x;
        gradientMultipliers(2,1)=gradientMultipliers(2,1)+y;
        gradientMultipliers(2,2)=gradientMultipliers(2,2)+1;
      }
    }
  }
  
  // Copy the matrix and add a fourth column.
  Uint i,j;
  for(i=0;i<3;i++)
  {
    for(j=0;j<3;j++)
    {
      transGradientMultipliers(i,j)=gradientMultipliers(i,j);
    }
  }
  transGradientMultipliers(0,3)=(-gradientCoefficients(0,0));
  transGradientMultipliers(1,3)=(-gradientCoefficients(1,0));
  transGradientMultipliers(2,3)=(-gradientCoefficients(2,0));

  // Perform a Singular Value Decomposition using
  // the 'Numerical Recipes in C' function.
  Matrix<Float> s(4,1);
  Matrix<Float> v(4,4);

  {
    int fail = nr_svdcmp(transGradientMultipliers.pointerpointer(), 
                         transGradientMultipliers.rows(),
                         transGradientMultipliers.cols(), 
                         s.pointer(),
                         v.pointerpointer());
    if (fail)
    {
      // SVD failed. It could be a singular matrix, let's just copy the original
      // image.
      dstImg = srcImg;
      return;
    }
  }
  
  gradientCoefficients(0,0)=v(0,3)/v(3,3);
  gradientCoefficients(1,0)=v(1,3)/v(3,3);
  gradientCoefficients(2,0)=v(2,3)/v(3,3);

  // Substract the illumination gradient at each pixel. Also calculate the minimum and maximum
  // value.

  Image<Float> image1(srcImg.width(),srcImg.height());

  Float max=0;
  Float min=255;
  Float val;
  
  for(y=0;y<srcImg.height();y++)
  {
    for(x=0;x<srcImg.width();x++)
    {
      val=(Float)srcImg(x,y)-(gradientCoefficients(0,0)*x+
                              gradientCoefficients(1,0)*y+
                              gradientCoefficients(2,0));
      if(val<min) min=val;
      if(val>max) max=val;

      image1(x,y) = val;
    }
  }

  // Resize the destination image.
  dstImg.resize(srcImg.width(),srcImg.height());

  // This should not happen
  if (min >= max)
  {
    dstImg = srcImg;
    return;
  }
  
  // Some values could be below 0, we are going to rescale the values to the range
  // [0,255].
  for(y=0;y<srcImg.height();y++)
  {
    for(x=0;x<srcImg.width();x++)
    {
      dstImg(x,y)=(Uint)(((image1(x,y)-min)/(max-min))*255);
    }
  }
}

//
// ACCESS
//

void 
IlluminationGradient::setMask(const Image8& mask)
{
  mMask = mask;
}

//
// INQUIRY
//


}; // namespace vision

}; // namespace ait

//
// From Numerical Recipes

#define NR_END 1
#define FREE_ARG char*

static float *vect(long nl, long nh)
/* allocate a float vect with subscript range v[nl..nh] */
{
	float *v;

	v=(float *)malloc((size_t) ((nh-nl+1+NR_END)*sizeof(float)));
	//if (!v) nrerror("allocation failure in vect()");
	return v-nl+NR_END;
}

static void free_vect(float *v, long nl, long nh)
/* free a float vect allocated with vect() */
{
	free((FREE_ARG) (v+nl-NR_END));
}

#define SIGN(a,b) ((b) >= 0.0 ? fabs(a) : -fabs(a))

#define FMAX(a,b) std::max(a,b)

#define IMIN(a,b) std::min(a,b)

inline float sqr(float a) { return a*a; }
#define SQR(a) sqr(a)

static float pythag(float a, float b)
{
	float absa,absb;
	absa=(float)fabs(a);
	absb=(float)fabs(b);
	if (absa > absb) return (float)(absa*sqrt(1.0+SQR(absb/absa)));
	else return (float)((absb == 0.0 ? 0.0 : absb*sqrt(1.0+SQR(absa/absb))));
}

static int nr_svdcmp(float **a, int m, int n, float w[], float **v)
{
	int flag,i,its,j,jj,k,l,nm;
	float anorm,c,f,g,h,s,scale,x,y,z,*rv1;

	rv1=vect(0,n-1);
	g=scale=anorm=0.0;
	for (i=0;i<n;i++) {
		l=i+1;
		rv1[i]=scale*g;
		g=s=scale=0.0;
		if (i < m) {
			for (k=i;k<m;k++) scale += (float)fabs(a[k][i]);
			if (scale) {
				for (k=i;k<m;k++) {
					a[k][i] /= scale;
					s += a[k][i]*a[k][i];
				}
				f=a[i][i];
				g = -(float)(SIGN(sqrt(s),f));
				h=f*g-s;
				a[i][i]=f-g;
				for (j=l;j<n;j++) {
					for (s=0.0,k=i;k<m;k++) s += a[k][i]*a[k][j];
					f=s/h;
					for (k=i;k<m;k++) a[k][j] += f*a[k][i];
				}
				for (k=i;k<m;k++) a[k][i] *= scale;
			}
		}
		w[i]=scale *g;
		g=s=scale=0.0;
		if (i < m && i != (n-1)) {
			for (k=l;k<n;k++) scale += (float)fabs(a[i][k]);
			if (scale) {
				for (k=l;k<n;k++) {
					a[i][k] /= scale;
					s += a[i][k]*a[i][k];
				}
				f=a[i][l];
				g = -(float)(SIGN(sqrt(s),f));
				h=f*g-s;
				a[i][l]=f-g;
				for (k=l;k<n;k++) rv1[k]=a[i][k]/h;
				for (j=l;j<m;j++) {
					for (s=0.0,k=l;k<n;k++) s += a[j][k]*a[i][k];
					for (k=l;k<n;k++) a[j][k] += s*rv1[k];
				}
				for (k=l;k<n;k++) a[i][k] *= scale;
			}
		}
		anorm=(float)(FMAX(anorm,(float)(fabs(w[i])+fabs(rv1[i]))));
	}
	for (i=(n-1);i>=0;i--) 
  {
		if (i < (n-1)) 
    {
			if (g) 
      {
				for (j=l;j<n;j++)
					v[j][i]=(a[i][j]/a[i][l])/g;
				for (j=l;j<n;j++) {
					for (s=0.0,k=l;k<n;k++) s += a[i][k]*v[k][j];
					for (k=l;k<n;k++) v[k][j] += s*v[k][i];
				}
			}
			for (j=l;j<n;j++) v[i][j]=v[j][i]=0.0;
		}
		v[i][i]=1.0;
		g=rv1[i];
		l=i;
	}

	for (i=IMIN(m-1,n-1);i>=0;i--) 
  {
		l=i+1;
		g=w[i];
		for (j=l;j<n;j++) a[i][j]=0.0;
		if (g) 
    {
			g=(float)(1.0/g);
			for (j=l;j<n;j++) 
      {
				for (s=0.0,k=l;k<m;k++) s += a[k][i]*a[k][j];
				f=(s/a[i][i])*g;
				for (k=i;k<m;k++) a[k][j] += f*a[k][i];
			}
			for (j=i;j<m;j++) a[j][i] *= g;
		} 
    else
      for (j=i;j<m;j++) a[j][i]=0.0;
		++a[i][i];
	}

	for (k=n-1;k>=0;k--)
  {
		for (its=0;its<30;its++) 
    {
			flag=1;
			for (l=k;l>=0;l--) {
				nm=l-1;
				if ((float)(fabs(rv1[l])+anorm) == anorm) {
					flag=0;
					break;
				}
				if ((float)(fabs(w[nm])+anorm) == anorm) break;
			}
			if (flag) {
				c=0.0;
				s=1.0;
				for (i=l;i<=k;i++) {
					f=s*rv1[i];
					rv1[i]=c*rv1[i];
					if ((float)(fabs(f)+anorm) == anorm) break;
					g=w[i];
					h=pythag(f,g);
					w[i]=h;
					h=(float)(1.0/h);
					c=g*h;
					s = -f*h;
					for (j=0;j<m;j++)
          {
						y=a[j][nm];
						z=a[j][i];
						a[j][nm]=y*c+z*s;
						a[j][i]=z*c-y*s;
					}
				}
			}
			z=w[k];
			if (l == k) {
				if (z < 0.0) {
					w[k] = -z;
					for (j=0;j<n;j++) v[j][k] = -v[j][k];
				}
				break;
			}
			if (its == 30-1) 
      {
      	free_vect(rv1,0,n-1);
        return -1;
      }
			x=w[l];
			nm=k-1;
			y=w[nm];
			g=rv1[nm];
			h=rv1[k];
			f=(float)(((y-z)*(y+z)+(g-h)*(g+h))/(2.0*h*y));
			g=pythag(f,1.0);
			f=(float)(((x-z)*(x+z)+h*((y/(f+SIGN(g,f)))-h))/x);
			c=s=1.0;
			for (j=l;j<=nm;j++) 
      {
				i=j+1;
				g=rv1[i];
				y=w[i];
				h=s*g;
				g=c*g;
				z=pythag(f,h);
				rv1[j]=z;
				c=f/z;
				s=h/z;
				f=x*c+g*s;
				g = g*c-x*s;
				h=y*s;
				y *= c;
				for (jj=0;jj<n;jj++) {
					x=v[jj][j];
					z=v[jj][i];
					v[jj][j]=x*c+z*s;
					v[jj][i]=z*c-x*s;
				}
				z=pythag(f,h);
				w[j]=z;
				if (z) {
					z=(float)(1.0/z);
					c=f*z;
					s=h*z;
				}
				f=c*g+s*y;
				x=c*y-s*g;
				for (jj=0;jj<m;jj++)
        {
					y=a[jj][j];
					z=a[jj][i];
					a[jj][j]=y*c+z*s;
					a[jj][i]=z*c-y*s;
				}
			}
			rv1[l]=0.0;
			rv1[k]=f;
			w[k]=x;
		}
	}
	free_vect(rv1,0,n-1);

  return 0;
}


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "legacy/vision_tools/MotionDetect.hpp"

namespace ait 
{

MotionDetect::MotionDetect()
{
}

MotionDetect::~MotionDetect()
{
}

//
// OPERATIONS
//

//
// ACCESS
//

//
// INQUIRY
//


}; // namespace ait


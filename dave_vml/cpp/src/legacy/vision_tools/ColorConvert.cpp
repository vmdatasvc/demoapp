/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#pragma warning(disable: 4503)
#endif

//#include <ipp.h>

#include "legacy/vision_tools/ColorConvert.hpp"

namespace ait 
{

void 
ColorConvert::convert(const Image32& in, PvImageProc::colorSpaceConst inColSpace,
                      Image32& out, PvImageProc::colorSpaceConst outColSpace,
                      Rectanglei roi)
{
  if (roi.x1 == 0)
  {
    roi.x1 = in.width();
    roi.y1 = in.height();
  }
  else
  {
    PVASSERT((&in != &out) || (roi == Rectanglei(0,0,in.width(),in.height())));
  }

  out.resize(roi.width(),roi.height());
  
  if (inColSpace == outColSpace)
  {
    out.crop(in,roi.x0,roi.y0,roi.width(),roi.height());
    return;
  }

  switch(inColSpace)
  {
  case PvImageProc::RGB_SPACE:
    switch(outColSpace)
    {
    case PvImageProc::I1I2I3_SPACE:
      ColorConvertBase<colorConverters::RGBtoI1I2I3>::convert3CTo3C(in,out,roi);
      break;
    case PvImageProc::CRCGI_SPACE:
      ColorConvertBase<colorConverters::RGBtoCrCgI>::convert3CTo3C(in,out,roi);
      break;
    case PvImageProc::RGB_SPACE:
      ColorConvertBase<colorConverters::RGBtoRGB>::convert3CTo3C(in,out,roi);
      break;
    case PvImageProc::GRAY_SPACE:
      ColorConvertBase<colorConverters::RGBtoGray>::convert3CTo3C(in,out,roi);
      break;
    case PvImageProc::FAST_GRAY_SPACE:
      ColorConvertBase<colorConverters::RGBtoFastGray>::convert3CTo3C(in,out,roi);
      break;
    case PvImageProc::NORMALIZED0_SPACE:
      ColorConvertBase<colorConverters::RGBtoNormal0>::convert3CTo3C(in,out,roi);
      break;
    case PvImageProc::NORMALIZED1_SPACE:
      ColorConvertBase<colorConverters::RGBtoNormal1>::convert3CTo3C(in,out,roi);
      break;
    case PvImageProc::HSV_SPACE:
    case PvImageProc::XYZ_SPACE:
    case PvImageProc::YUV_SPACE:
    case PvImageProc::YCBCR_SPACE:
    case PvImageProc::LUV_SPACE:
    case PvImageProc::YCC_SPACE:
    case PvImageProc::HLS_SPACE:
    	// Following line was commented out cause IPP was not supported
//      ippConvert(in,inColSpace,out,outColSpace,roi);
    	// TODO: Use openCV color space convert instead
      break;
    default:
      PvUtil::exitError("Color Conversion Unsupported!");
    }
    break;
  default:
    PvUtil::exitError("Color Conversion Unsupported!");
  }
}

#if 0
/* ipp not supported */
void 
ColorConvert::ippConvert(const Image32& in, PvImageProc::colorSpaceConst inColSpace,
                         Image32& out, PvImageProc::colorSpaceConst outColSpace,
                         Rectanglei roi)
{
  PVASSERT(Rectanglei(0,0,in.width(),in.height()).inside(roi));

  IppiSize roiSize = { roi.width(), roi.height() };
  IppStatus s;

  out.resize(roi.width(), roi.height());

  Uint8 *pSrc = (Uint8*)(in.pointer() + roi.x0 + roi.y0*in.width());

  switch(inColSpace)
  {
  case PvImageProc::RGB_SPACE:
    switch(outColSpace)
    {
    case PvImageProc::HSV_SPACE:
      s = ippiRGBToHSV_8u_AC4R(pSrc,in.width()*4,
                               (Uint8*)out.pointer(), out.width()*4, 
                               roiSize);
      PVASSERT(s == ippStsNoErr);
      break;

    case PvImageProc::XYZ_SPACE:
      s = ippiRGBToXYZ_8u_AC4R(pSrc,in.width()*4,
                               (Uint8*)out.pointer(), out.width()*4, 
                               roiSize);
      PVASSERT(s == ippStsNoErr);
      break;

    case PvImageProc::YUV_SPACE:
      s = ippiRGBToYUV_8u_AC4R(pSrc,in.width()*4,
                               (Uint8*)out.pointer(), out.width()*4, 
                               roiSize);
      PVASSERT(s == ippStsNoErr);
      break;

    case PvImageProc::YCBCR_SPACE:
      s = ippiRGBToYCbCr_8u_AC4R(pSrc,in.width()*4,
                                 (Uint8*)out.pointer(), out.width()*4, 
                                 roiSize);
      PVASSERT(s == ippStsNoErr);
      break;

    case PvImageProc::LUV_SPACE:
      s = ippiRGBToLUV_8u_AC4R(pSrc,in.width()*4,
                               (Uint8*)out.pointer(), out.width()*4, 
                               roiSize);
      PVASSERT(s == ippStsNoErr);
      break;

    case PvImageProc::YCC_SPACE:
      s = ippiRGBToYCC_8u_AC4R(pSrc,in.width()*4,
                               (Uint8*)out.pointer(), out.width()*4, 
                               roiSize);
      PVASSERT(s == ippStsNoErr);
      break;

    case PvImageProc::HLS_SPACE:
      s = ippiRGBToHLS_8u_AC4R(pSrc,in.width()*4,
                               (Uint8*)out.pointer(), out.width()*4, 
                               roiSize);
      PVASSERT(s == ippStsNoErr);
      break;

    default:
      PvUtil::exitError("Color Conversion Unsupported!");
    }
    break;
  default:
    PvUtil::exitError("Color Conversion Unsupported!");
  }

}
#endif

void 
ColorConvert::convert(const Image32& in, PvImageProc::colorSpaceConst inColSpace,
                      Image8& out0, Image8& out1, Image8& out2, 
                      PvImageProc::colorSpaceConst outColSpace,
                      Rectanglei roi)
{
  if (roi.x1 == 0)
  {
    roi.x1 = in.width();
    roi.y1 = in.height();
  }
  
  out0.resize(roi.width(),roi.height());
  out1.resize(roi.width(),roi.height());
  out2.resize(roi.width(),roi.height());

  switch(inColSpace)
  {
  case PvImageProc::RGB_SPACE:
    switch(outColSpace)
    {
    case PvImageProc::I1I2I3_SPACE:
      ColorConvertBase<colorConverters::RGBtoI1I2I3>::convert3CTo3P(in,out0,out1,out2,roi);
      break;
    case PvImageProc::CRCGI_SPACE:
      ColorConvertBase<colorConverters::RGBtoCrCgI>::convert3CTo3P(in,out0,out1,out2,roi);
      break;
    case PvImageProc::RGB_SPACE:
      ColorConvertBase<colorConverters::RGBtoRGB>::convert3CTo3P(in,out0,out1,out2,roi);
      break;
    case PvImageProc::GRAY_SPACE:
      ColorConvertBase<colorConverters::RGBtoGray>::convert3CTo3P(in,out0,out1,out2,roi);
      break;
    case PvImageProc::FAST_GRAY_SPACE:
      ColorConvertBase<colorConverters::RGBtoFastGray>::convert3CTo3P(in,out0,out1,out2,roi);
      break;
    case PvImageProc::NORMALIZED0_SPACE:
      ColorConvertBase<colorConverters::RGBtoNormal0>::convert3CTo3P(in,out0,out1,out2,roi);
      break;
    case PvImageProc::NORMALIZED1_SPACE:
      ColorConvertBase<colorConverters::RGBtoNormal1>::convert3CTo3P(in,out0,out1,out2,roi);
      break;
    default:
      PvUtil::exitError("Color Conversion Unsupported!");
    }
    break;
  default:
    PvUtil::exitError("Color Conversion Unsupported!");
  }
}

void 
ColorConvert::convert(const Image32& in, PvImageProc::colorSpaceConst inColSpace,
                      Image8& out, PvImageProc::colorSpaceConst outColSpace,
                      Rectanglei roi)
{
  if (roi.x1 == 0)
  {
    roi.x1 = in.width();
    roi.y1 = in.height();
  }
  
  out.resize(roi.width(),roi.height());

  switch(inColSpace)
  {
  case PvImageProc::RGB_SPACE:
    switch(outColSpace)
    {
    case PvImageProc::GRAY_SPACE:
#ifdef USE_IPP
      {
        IppiSize roiSize = { roi.width(), roi.height() };
        IppStatus s;

        out.resize(roi.width(), roi.height());

        Uint8 *pSrc = (Uint8*)(in.pointer() + roi.x0 + roi.y0*in.width());

        s = ippiRGBToGray_8u_AC4C1R(pSrc,in.width()*4,out.pointer(),out.width(),roiSize);

        PVASSERT(s == ippStsNoErr);
      }
#else
      ColorConvertBase<colorConverters::RGBtoGray>::convert3CTo1P(in,out,roi);
#endif
      break;
    case PvImageProc::FAST_GRAY_SPACE:
      ColorConvertBase<colorConverters::RGBtoFastGray>::convert3CTo1P(in,out,roi);
      break;
    default:
      PvUtil::exitError("Color Conversion Unsupported!");
    }
    break;
  default:
    PvUtil::exitError("Color Conversion Unsupported!");
  }
}

void 
ColorConvert::convert(const Image8& in, PvImageProc::colorSpaceConst inColSpace,
                      Image32& out, PvImageProc::colorSpaceConst outColSpace,
                      Rectanglei roi)
{
  if (roi.x1 == 0)
  {
    roi.x1 = in.width();
    roi.y1 = in.height();
  }
  
  out.resize(roi.width(),roi.height());

  switch(inColSpace)
  {
  case PvImageProc::GRAY_SPACE:
  case PvImageProc::FAST_GRAY_SPACE:
    switch(outColSpace)
    {
    case PvImageProc::RGB_SPACE:
//#ifdef USE_IPP
      {
//        IppiSize roiSize = { roi.width(), roi.height() };

        for (int y = roi.y0; y < roi.y1; y++)
        {
          for (int x = roi.x0; x < roi.x1; x++)
          {
            Uint32 c = in(x,y);
            out(x-roi.x0,y-roi.y0) = PV_RGB(c,c,c);
          }
        }
      }
//#else
//      ColorConvertBase<colorConverters::RGBtoGray>::convert3CTo1P(in,out,roi);
//#endif
      break;
    default:
      PvUtil::exitError("Color Conversion Unsupported!");
    }
    break;
  default:
    PvUtil::exitError("Color Conversion Unsupported!");
  }
}

}; // namespace ait


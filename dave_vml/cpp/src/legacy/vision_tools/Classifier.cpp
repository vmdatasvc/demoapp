/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "legacy/vision_tools/Classifier.hpp"

namespace ait 
{

namespace vision
{

Classifier::Classifier()
: mIsInitialized(false)
{
}

Classifier::~Classifier()
{
}

void 
Classifier::init(ClassifierLabelsPtr pLabels)
{
  // Initialize this object.

  mpLabels = pLabels;

  mIsInitialized = true;
}

//
// OPERATIONS
//

//
// ACCESS
//

//
// INQUIRY
//

}; // namespace vision

}; // namespace ait


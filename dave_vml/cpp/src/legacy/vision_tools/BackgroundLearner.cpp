/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "legacy/vision_tools/BackgroundLearner.hpp"
#include "legacy/vision_tools/HistogramBackgroundModel.hpp"
#include <legacy/low_level/Settings.hpp>

namespace ait 
{

BackgroundLearner::BackgroundLearner()
: mIsInitialized(false)
{
}

BackgroundLearner::~BackgroundLearner()
{
}

void 
BackgroundLearner::init(const std::string& settingsPath)
{
  Settings set(settingsPath);

  mSettingsPath = "BackgroundLearner";
  
//  mInitNumFrames = set.getInt("initNumFrames",30);

  mNumBackgroundFrames = set.getIntList("numberOfBackgroundFrames", "300");
  mModelUpdateInterval = set.getIntList("modelUpdateInterval","10");
//  mNumBackgroundFrames.push_back(300);
//  mModelUpdateInterval.push_back(10);

  PVASSERT(mNumBackgroundFrames.size() > 0);
  PVASSERT(mNumBackgroundFrames.size() == mModelUpdateInterval.size());  

  miNumFrames = mNumBackgroundFrames.begin();
  miUpdateInterval = mModelUpdateInterval.begin();  

  mMedianFilterSize = set.getInt("medianFilterSize",5);
//  mMedianFilterSize = 5;

  /*
  if (mInitNumFrames == 0)
  {
    mInitNumFrames = mNumBackgroundFrames;
  }
  */
  mBackgroundModelClassName = set.getString("backgroundModelClassName","HistogramBackgroundModel");
//  mBackgroundModelClassName = "HistogramBackgroundModel";
  resetValid();
  
  mIsInitialized = true;  
}

BackgroundModelBasePtr 
BackgroundLearner::createBackgroundModel()
{
  BackgroundModelBasePtr pModel;
  if (mBackgroundModelClassName == "HistogramBackgroundModel")
  {
    pModel.reset(new HistogramBackgroundModel());
  }
  else
  {
    PvUtil::exitError("Background model not supported or not defined [%s]",mBackgroundModelClassName.c_str());
  }

  pModel->init(mSettingsPath+"/"+mBackgroundModelClassName); // TODO: init with all params later

  return pModel;
}

BackgroundModelBasePtr 
BackgroundLearner::createBackgroundModel(std::string settingsPathName)
{
  BackgroundModelBasePtr pModel;
  if (mBackgroundModelClassName == "HistogramBackgroundModel")
  {
    pModel.reset(new HistogramBackgroundModel());
  }
  else
  {
    PvUtil::exitError("Background model not supported or not defined [%s]",mBackgroundModelClassName.c_str());
  }

  pModel->init(mSettingsPath+"/"+mBackgroundModelClassName); // TODO: init with all params later

  return pModel;
}

//
// OPERATIONS
//

void 
BackgroundLearner::reset()
{
  mpBuildModel.reset();
  mpBuildNormalizedModel.reset();
  mModelUpdated = false;  
  mModelUpdateCounter = *miUpdateInterval - 1;
}

void
BackgroundLearner::resetValid()
{
  reset();
  mpValidModel.reset();
  mpValidNormalizedModel.reset();
}

void
BackgroundLearner::resetIntervals()
{
  miUpdateInterval = mModelUpdateInterval.begin();
  miNumFrames = mNumBackgroundFrames.begin();
}


void 
BackgroundLearner::process(const Image32Ptr pImg, bool learnNewModel)
{
  //processImplementation(pImg,learnNewModel);
  processImplementationHack(pImg,learnNewModel);
}


void
BackgroundLearner::process(const Image8Ptr pImg, bool learnNewModel)
{
  processImplementation(pImg,learnNewModel);
}

void 
BackgroundLearner::saveModel(std::string filename)
{
  if (mpValidModel.get())
    mpValidModel->saveModel(filename);
}

void 
BackgroundLearner::loadModel(std::string filename)
{
  mpValidModel = createBackgroundModel();

  //mpValidNormalizedModel = createBackgroundModel();

  Vector2i frameSize = mpValidModel->loadModel(filename);  
  mpForegroundImage.reset(new Image8(frameSize.x, frameSize.y));   

  //mpForegroundNormalizedImage.reset(new Image8(frameSize.x, frameSize.y));   
}

//
// ACCESS
//

//
// INQUIRY
//

}; // namespace ait


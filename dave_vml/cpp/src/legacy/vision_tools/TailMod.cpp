/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#pragma warning(disable:4503)
#endif

#include "TailMod.hpp"
#include <limits>

namespace ait 
{

namespace vision
{

TailMod::TailMod()
: VisionMod(getModuleStaticName())
{
}

TailMod::~TailMod()
{
}

void 
TailMod::initExecutionGraph(std::list<VisionModPtr>& srcModules)
{
  VisionMod::initExecutionGraph(srcModules);

  // Note that this priority is not valid for any other module.
  setExecutionPriority(std::numeric_limits<int>::max());

  // The execution graph will add a source dependency with all the
  // real-time modules.
}

//
// OPERATIONS
//

//
// ACCESS
//

//
// INQUIRY
//

}; // namespace vision

}; // namespace ait


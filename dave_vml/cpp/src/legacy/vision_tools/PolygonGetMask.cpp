// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include <list>
#include <algorithm>
#include <vector>

#include <boost/cast.hpp>

#include <legacy/pv/ait/Vector3.hpp>
#include <legacy/pv/ait/Rectangle.hpp>
#include <legacy/pv/ait/Image.hpp>

#include <legacy/vision_tools/PolygonGetMask.hpp>

/*
 *	PolygonEdgeData contains relevant information
 *  about edges, and allows for them to be easily sorted
 *  by getMask()
 *  
 *  
 */
class PolygonEdgeData
{
  //
  // LIFETIME
  //

public:
  // Use compilers default construct

  /*
   * Constructor
   *
   * @param minY The min y value from the edge-defining pair of tuples.	
   * @param maxY The max y value from the edge-defining pair of tuples.
   * @param x The x value associated with minY
   * @param inverseSlope The inverse slope of the line equation
   */
  PolygonEdgeData(
    float minY, 
    float maxY, 
    float x, 
    float inverseSlope);

  //
  // OPERATORS
  //

public:
  /**
   *	The less than operator is defined to allow easy sorting
   *  by STL's sort() function of the global edge list
   */  
  bool operator<(const PolygonEdgeData& otherEdge) const;

  //Used by STL's built in sort function to sort the active edge list
  static bool compareX(
    const PolygonEdgeData& edge1, 
    const PolygonEdgeData& edge2);

  //
  // OPERATIONS
  //

public:
  void setX(float x) {mX = x;};
  

  //
  // ACCESS
  //

public:

  float getMinY() const {return mMinY;};
  float getMaxY() const {return mMaxY;};
  float getX() const {return mX;};
  float getInverseSlope() const {return mInverseSlope;};

  //
  // ATTRIBUTES
  //
   
protected:

  /*
   *	The min y value from the edge-defining pair of tuples.	
   */
  float mMinY;
  /*
   *	maxY The max y value from the edge-defining pair of tuples.
   */
  float mMaxY;
  /*
   *	The x value associated with minY
   */
  float mX;
  /*
   *	The inverse slope of the line equation
   */
  float mInverseSlope;

};

//Constructor
PolygonEdgeData::PolygonEdgeData(
  float minY,
  float maxY, 
  float x, 
  float inverseSlope)
  : mMinY(minY), mMaxY(maxY), mX(x), mInverseSlope(inverseSlope)
{
  //Do nothing here. Initializer list already initialized variables.
}
  
/*
 *	Compares a PolygonEdgeData object. Compares minY values.
 *  When minY values are equivalent, compares, maxY, then X.
 *  Used to sort the global list of edges in the beginning.
 */

bool PolygonEdgeData::operator<(const PolygonEdgeData& otherEdge) const
{
  if (mMinY < otherEdge.getMinY()) //less than
    return true;
  else if (mMinY > otherEdge.getMinY()) //greater than
    return false;
  else //must be equal
  {
    if (mMaxY < otherEdge.getMaxY()) //less than
      return true;
    else if (mMaxY > otherEdge.getMaxY()) //greater than
      return false;
    else //must be equal, check x
    {
      if (mX < otherEdge.getX())
        return true;
      else
        return false;
    }
  }
}

/*
 *	Compares PolygonEdgeData objects solely by their x values.
 *  This function is used by STL's sort to sort the active
 *  edge lists
 */
bool PolygonEdgeData::compareX(const PolygonEdgeData& edge1, const PolygonEdgeData& edge2)
{
  if (edge1.getX() < edge2.getX())
    return true;
  else
    return false;
}


//

namespace ait
{
namespace vision
{

//based on algorithm at http://www.cs.rit.edu/~icss571/filling/how_to.html
void 
getMask(
  const std::vector<Vector2f>& inputPixels,
  const Rectanglef& roi, 
  Image8& img)
{  

  /* Bug fix: this function was initially designed to work with integers, but the requirements
   * were later changed so that it would accept a vector of floats. The floats must be whole
   * numbers, or else the algorithm may not work.
   */

  std::vector<Vector2f> pixels;

  for (int x = 0; x < inputPixels.size(); x++)
  {
    Vector2f val = inputPixels[x];

    int xi = (int)val.x;
    int yi = (int)val.y;

    val.x = (float)xi;
    val.y = (float)yi;
    pixels.push_back(val);
  }

  /**
   * End bug fix
   */



  const int POLY_COLOR = 255; //color that fills the polygon
  const int BGROUND_COLOR = 0; //the background color

  img.resize((int)(roi.width() + 0.5),(int)(roi.height() + 0.5));
  //Set the background to black
  img.rectFill(0,0,img.width(),img.height(), BGROUND_COLOR);

  //No coordinates given
  if (pixels.size() == 0)
  {  
    //do nothing
  } 
  //handle case when there is only one coord (draw point)
  else if (pixels.size() == 1)
  {
    //make sure pixel is in the image
    if (pixels[0].x >=0.0 && pixels[0].x < (float)img.width()
        && pixels[0].y >= 0.0 && pixels[0].y < (float)img.height())
      img((int)(pixels[0].x + .5), (int)(pixels[0].y + .5)) = POLY_COLOR;    
  }
  //handle case when there are only two coords (draw line)
  else if (pixels.size() == 2)    
    img.line((int)(pixels[0].x + .5), (int)(pixels[0].y+.5), (int)(pixels[1].x + .5), (int)(pixels[1].y+.5), POLY_COLOR);  
  else
  {      
    //handle polygon
    assert(pixels.size() > 2); //need at lest 3 pixel coords for a polygon
  
    std::vector<PolygonEdgeData> allEdges;

    const Vector2f *pLast = &pixels.back();
    std::vector<Vector2f>::const_iterator iCoord;
    //Create edges out of pixel vertices
    for (iCoord = pixels.begin(); iCoord != pixels.end(); iCoord++)
    {    
      float minY = std::min(iCoord->y, pLast->y) - std::min(roi.y0, roi.y1);
      float maxY = std::max(iCoord->y, pLast->y) - std::min(roi.y0, roi.y1);
      float xVal = 0.0;
      if (iCoord->y < pLast->y)
        xVal = iCoord->x - std::min(roi.x0, roi.x1);
      else
        xVal = pLast->x - std::min(roi.x0, roi.x1);

      float deltaX = pLast->x - iCoord->x;
      float deltaY = pLast->y - iCoord->y;
  
      if (deltaY != 0.0) // do not add to the edge list if slope is zero         
        allEdges.push_back(PolygonEdgeData(minY, maxY, xVal, 
          deltaX/deltaY));      
    
      pLast = &(*iCoord);
    }
  
    //There must be at least 2 edges
    assert(allEdges.size() > 1);

    //sort edges, uses PolygonEdgeData::operator<   
    std::sort(allEdges.begin(), allEdges.end());

    //Init the active edge table  
    std::vector<PolygonEdgeData> activeEdges;
  
    //init first scanline and activeEdge table
    int scanLine = (int)(allEdges.front().getMinY() + .5);

    //std::list<PolygonEdgeData>::iterator iEdge;
    std::vector<PolygonEdgeData>::iterator iEdge;
  
    for (iEdge = allEdges.begin(); iEdge != allEdges.end();)
    {
      if ((int)(iEdge->getMinY() + .5) == scanLine)
      {      
        activeEdges.push_back(*iEdge);
        iEdge = allEdges.erase(iEdge);     
      }
      else
      {
        ++iEdge;
      }
    }

    int imageHeight = (int)(img.height() + .5);
  
    //for each scanline
    while (activeEdges.size() != 0)
    {    
      //draw the pixels
      //int y = imageHeight - scanLine; //flips y-axis
      int y = scanLine;
    
      std::vector<PolygonEdgeData>::iterator iPair = activeEdges.begin();
      //parse through activeEdges, drawing pixels as needed
    
      for (iPair=activeEdges.begin(); iPair != activeEdges.end(); iPair += 2)
      {
        float nowX = iPair->getX();
        //this should never happen -> odd number of active edges
        assert(iPair+1 != activeEdges.end());        
        float nextX = (iPair+1)->getX();        

        img.hline((int)(nowX+.5), y, (int)(nextX - nowX + 1.5), POLY_COLOR);     
      }    

      //increment scan line
      scanLine++;    
       
      //check to see if an active edge needs removed
      for (iEdge = activeEdges.begin(); iEdge != activeEdges.end(); /*i++*/)
      {
        if ((int)(iEdge->getMaxY()+.5) == scanLine)
        {       
          iEdge = activeEdges.erase(iEdge);
        }
        else
        {
          ++iEdge;
        }
      }

      //update x values
      for (iEdge = activeEdges.begin(); iEdge != activeEdges.end(); iEdge++)
      {
        iEdge->setX(iEdge->getX() + iEdge->getInverseSlope());
      }
    
      //move edges from global edge list to active list when minY == scanLine
      for (iEdge = allEdges.begin(); iEdge != allEdges.end(); /*i++*/)
      {
        if ((int)(iEdge->getMinY() + .5) == scanLine)
        {      
          activeEdges.push_back(*iEdge);
          iEdge = allEdges.erase(iEdge);     
        }
        else
        {
          ++iEdge;
        }
      }
    
      //reorder edges in active edge table according to increasing x values
      std::sort(activeEdges.begin(), activeEdges.end(), PolygonEdgeData::compareX);
    }
  }
}

}; //namespace vision
}; // namespace ait

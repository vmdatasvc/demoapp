/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#pragma warning(disable: 4503)
#endif

#include <PvImageProc.hpp>

#include "VideoWriterMod.hpp"
#include "ImageAcquireMod.hpp"
#include <stdlib.h>
#include <DateTime.hpp>
#include <strutil.hpp>
#include <boost/lexical_cast.hpp>

namespace ait 
{

namespace vision
{

VideoWriterMod::VideoWriterMod()
: RealTimeMod(getModuleStaticName())
{
}

VideoWriterMod::~VideoWriterMod()
{
}

void 
VideoWriterMod::init()
{
  RealTimeMod::init();

  Double temp = mSettings.getDouble("captureRate [in FPS]",0.033);
  mCaptureRate = (Int)(temp/0.033);
  mCaptureRateCounter = 0;

  mFrameNameFormat = mSettings.getString("frameNameFormat","./<CameraID>/Images/<Timestamp>_%.06d.jpg");

  mFrameCounter = 0;
  mMaxFrames = mSettings.getInt("maxFrames [int]",0);

  mSubSampleLevel = mSettings.getInt("subSampleLevel [int]",0);

  mVideoNameFormat = mSettings.getString("videoName","./<CameraID>/Videos/<Timestamp>.avi");

  mCompressionSettingsFname = mSettings.getString("compressionSettingsFileName","");

  mImageName = processFileName(mFrameNameFormat,PvUtil::systemTime());
  if (mVideoNameFormat != "-")
  {
    resetVideo();
  }

  mWriteVisualization = mSettings.getBool("writeVisualization [bool]",0);

  mIsRecording = mSettings.getBool("autoRecord [bool]",1);

  mAddIdTimeStampFlag = mSettings.getBool("setIdTimeStamp [bool]",1);

  mPartitionSize = mSettings.getDouble("partitionSize [sec]",0);

  mExtraVideoWriterFrameOffset = mSettings.getInt("extraVideoWriterFrameOffset [frames]",2);

  mNextDiskFullCheckTime = 0;
  mIsDiskFull = false;

  mMinDiskSpace = mSettings.getDouble("minDiskSpace [MB]",100);

  std::string frames = mSettings.getString("saveFrameList","");
  if (frames != "")
  {
    std::vector<std::string> v = split(frames,",");
    for (int i = 0; i < v.size(); i++)
    {
      mExtractFrameList.push_back(boost::lexical_cast<int>(v[i]));
    }
    std::sort(mExtractFrameList.begin(),mExtractFrameList.end());
  }

  mNextFrameToExtractIndex = 0;

  mDoSnapshot = false;
}

void
VideoWriterMod::finish()
{
  closeVideo();
  RealTimeMod::finish();
}

void 
VideoWriterMod::initExecutionGraph(std::list<VisionModPtr>& srcModules)
{
  RealTimeMod::initExecutionGraph(srcModules);

  setExecutionPriority(9);
}

void
VideoWriterMod::closeVideo()
{
  if (mpVideoWriter.get() != NULL)
  {
    // Try to find the exact timestamp.
    //double preciseStartTime = getCurrentTime() - (mFrameCounter-1+mExtraVideoWriterFrameOffset)/getFramesPerSecond();

    // Close the video.
    mpVideoWriter->close();

    // Now rename the video file:
    //std::string newFname = processFileName(mVideoNameFormat,preciseStartTime);
    //PVMSG("Renaming: [%s] to [%s]\n", mLastVideoFileName.c_str(),newFname.c_str());
    //rename(mLastVideoFileName.c_str(),newFname.c_str());
  }

}

bool
VideoWriterMod::isDiskFull()
{
  if (mLastVideoFileName == "")
  {
    mIsDiskFull = false;
  }
  else
  {
    if (PvUtil::time() >= mNextDiskFullCheckTime)
    {

#ifdef WIN32

      std::vector<std::string> parts = split_path(mLastVideoFileName);

      if (parts.size() == 2)
      {
        std::string dirName = parts[0];
        ULARGE_INTEGER avail, total;
        BOOL res = GetDiskFreeSpaceEx(dirName.c_str(),                 // directory name
          &avail,    // bytes available to caller
          &total,    // bytes on disk
          NULL // free bytes on disk
        );

        if (res == 0)
        {
          PVMSG("Error getting free disk space!\n");
        }
        else
        {
          mIsDiskFull = (__int64)(avail.QuadPart/(1024*1024)) < mMinDiskSpace;
          if (mIsDiskFull)
          {
            PVMSG("Disk is full!\n");
          }
        }
      }
#endif

      // Check again in 10 seconds.
      mNextDiskFullCheckTime = PvUtil::time() + 10;
    }
  }

  return mIsDiskFull;
}

void
VideoWriterMod::resetVideo()
{
  closeVideo();
  mpVideoWriter = VideoWriter::create();

  if (mCompressionSettingsFname != "")
  {
    mpVideoWriter->loadCodecSettings(mCompressionSettingsFname,true);
  }
  mFrameCounter = 0;
  mLastFrameTime = 0;
}

//
// OPERATIONS
//

void
VideoWriterMod::processMessages()
{
  VisionMsgPtr pMsg;

  while (mpInputMsgQueue->get(pMsg))
  {
    VideoWriterMsg* pMsg0 = dynamic_cast<VideoWriterMsg*>(pMsg.get());
    if (pMsg0 != NULL)
    {
      processMsg(*pMsg0);
      continue;
    }
    PvUtil::exitError("Message type not supported by this module [%s]",getFullName().c_str());
  }
}

void 
VideoWriterMod::processMsg(VideoWriterMsg& msg)
{
  if (msg.getFlag(VideoWriterMsg::RECORD))
  {
    mIsRecording = true;
  }
  
  if (msg.getFlag(VideoWriterMsg::PAUSE))
  {
    mIsRecording = false;
  }

  if (msg.getFlag(VideoWriterMsg::STOP))
  {
    if (mpVideoWriter.get())
    {
      resetVideo();
    }
    mIsRecording = false;
  }

  if (msg.getFlag(VideoWriterMsg::SNAPSHOT))
  {
    mDoSnapshot = true;
  }
}

void 
VideoWriterMod::writeFrame(Image32& img)
{
  if (!mIsRecording) return;

  if (isDiskFull())
  {
    if (mFrameCounter > 0)
    {
      resetVideo();
    }
    return;
  }

  // See settings/VideoWritingMod.xml for an explanation.
  // This is a work around a bug in the video writer
  double startTimeOffset = -(double)mExtraVideoWriterFrameOffset/getFramesPerSecond();

  double systemTime = PvUtil::systemTime();
  double currentFrameTime;
  if (getImageAcquireModule()->isLiveVideoSource())
  {
    currentFrameTime = systemTime - (PvUtil::time() - RealTimeMod::getCurrentTime());

    // If the difference in times is more than 30 seconds, the system
    // clock might have been changed. Let's reset the video just in case.
    if (mLastFrameTime > 0 && fabs(currentFrameTime-mLastFrameTime) > 30)
    {
      PVMSG("Time difference since last frame too large. Resetting video.\n");
      resetVideo();
    }
  }
  else
  {
    currentFrameTime = RealTimeMod::getCurrentTime();
  }

  Image32* pSubSampled;

  if (mSubSampleLevel > 0)
  {
    pSubSampled = new Image32();
    PvImageProc::subSample(img,*pSubSampled,mSubSampleLevel);
  }
  else
  {
    pSubSampled = &img;
  }

  if (mpVideoWriter.get())
  {
    if (mFrameCounter > 0 && mPartitionSize > 0 && mNextPartitionTime < currentFrameTime)
    {
      resetVideo();
    }
    if (mFrameCounter == 0)
    {

      try
      {
        // Remove the offset from the timestamp to work around bug.
        double videoStartTime = currentFrameTime+startTimeOffset;
        mLastVideoFileName = processFileName(mVideoNameFormat,videoStartTime);
        if (isDiskFull())
        {
          return;
        }
        mpVideoWriter->open(mLastVideoFileName);
      }
      catch (std::exception ex)
      {
        PvUtil::exitError(ex.what());
      }
      mStartRecordingTime = currentFrameTime;

      if (mPartitionSize > 0)
      {
        if (mPartitionSize <= 60*60*24)
        {
          mNextPartitionTime = DateTime::findNextRoundTime(DateTime(mStartRecordingTime-startTimeOffset),mPartitionSize).toSeconds();

          // The next case will take care of cases like:
          //   If we stopped recording at 8:59:59 we don't want to create a 1 minute video.
          if (mNextPartitionTime - mStartRecordingTime < mPartitionSize*0.1f)
          {
            mNextPartitionTime += mPartitionSize;
          }

          // Now subtract the offset.
          mNextPartitionTime -= startTimeOffset;
        }
        else
        {
          mNextPartitionTime += mPartitionSize;
        }
      }

      double fps = getImageAcquireModule()->getFramesPerSecond();
      mpVideoWriter->setFramesPerSecond(fps);
    }
  }

  if ((mMaxFrames == 0) || (mFrameCounter < mMaxFrames))
  {
    // Check if addTimeStampFlag is set  
    // If set, write timestamp 
    if(mAddIdTimeStampFlag)
    {
      // Do not modify original frame
      // Add timestamp on copy
      PvCanvasImage* pvCurrentVideoFrame = new PvCanvasImage(*pSubSampled);
      if (pSubSampled != &img)
      {
        delete pSubSampled;
      }
      writeIdTimeStamp(*pvCurrentVideoFrame, currentFrameTime);
      pSubSampled = pvCurrentVideoFrame; // point back to pvCurrentVideoFrame
    }

    if (mpVideoWriter.get())
    {
      bool dontWriteThisFrame = false;

      if (mFrameCounter > 0 && getImageAcquireModule()->isLiveVideoSource())
      {
        double fps = getImageAcquireModule()->getFramesPerSecond();
        double timeSinceStart = PvUtil::systemTime() - mStartRecordingTime;

        // The following code will check if there are frames missing.
        // This can happen with High CPU load, where the camera was
        // not able to capture some frames. In this case the previous
        // frame will be repeated on the video file, as many times
        // as required to catch up with the current clock time.

        bool writeSameFrame = false;
        //PVMSG("fps [%f], timeSinceStart [%f], curFrameTime [%f]\n",fps,timeSinceStart,(double)mFrameCounter/fps);
        while ((double)mFrameCounter/fps < timeSinceStart - 1.0/fps)
        {
          //PVMSG("Skipping frame %i\n",mFrameCounter);
          if (mFrameCounter > 0)
          {
            // Skip frame might not be ready for prime-time. The problem
            // Is that encoders sometimes have troubles with this operation,
            // so it will be safer to just write the frame.
            //mpVideoWriter->skipFrame();
            mpVideoWriter->writeFrame(*pSubSampled);
          }
          else
          {
            mpVideoWriter->writeFrame(*pSubSampled);
          }
          ++mFrameCounter;
          writeSameFrame = true;
        }

        // The next scenario (which actually happens!) is that
        // cameras are asked to capture at a given frame rate,
        // but the actual frame rate is higher, therefore generating
        // more frames than requested, which will damage time 
        // synchronization for video files.
        if (!writeSameFrame && (double)mFrameCounter/fps > timeSinceStart + 1.0/fps)
        {
          dontWriteThisFrame = true;
          PVMSG("Will not write this frame %i\n",(int)mFrameCounter);
        }
      }

      if (!dontWriteThisFrame)
      {
        mpVideoWriter->writeFrame(*pSubSampled);
         ++mFrameCounter;

        int frameNumber = mpImageAcquireMod->getCurrentFrameNumber();
        while (mNextFrameToExtractIndex < mExtractFrameList.size() &&
          mExtractFrameList[mNextFrameToExtractIndex] < frameNumber)
        {
          mNextFrameToExtractIndex++;
        }
        if (mNextFrameToExtractIndex < mExtractFrameList.size() &&
          mExtractFrameList[mNextFrameToExtractIndex] == frameNumber)
        {
          std::string fileName(aitSprintf(mImageName.c_str(),frameNumber));
          pSubSampled->save(fileName.c_str());
        }
      }
    }
    else
    {
      if (++mCaptureRateCounter == mCaptureRate)
      {
        std::string fileName(aitSprintf(mImageName.c_str(),mFrameCounter++));
        pSubSampled->save(fileName.c_str());
        mCaptureRateCounter = 0;
      }
    }
  }

  if (pSubSampled != &img)
  {
    delete pSubSampled;
  }

  mLastFrameTime = currentFrameTime;
}

void
VideoWriterMod::writeSnapshot(Image32& img)
{
  if (!mDoSnapshot) return;
  std::string fname = mSettings.getString("snapshotFileName","snap.jpg");
  fname = processFileName(fname,PvUtil::systemTime());

  // Do not modify original frame
  // Add timestamp on copy
  PvCanvasImage currentVideoFrame(img);
  writeIdTimeStamp(currentVideoFrame, PvUtil::systemTime());
  currentVideoFrame.save(fname.c_str());

  mDoSnapshot = false;
}

void 
VideoWriterMod::process()
{
  if (!mWriteVisualization)
  {
    writeFrame(getCurrentFrame());
    writeSnapshot(getCurrentFrame());
  }
}

void 
VideoWriterMod::visualize(Image32& img)
{
  if (mWriteVisualization)
  {
    writeFrame(img);
    writeSnapshot(getCurrentFrame());
  }
}

std::string
VideoWriterMod::processFileName(const std::string& in, double time)
{
  std::string out=in;
  
  int pos = 0;
  std::string cameraID("<CameraID>");
  pos = out.find(cameraID);
  if (pos != std::string::npos)
  {
    out.replace(pos, cameraID.length(), getExecutionPathName());
  }
  else
  {
    out = in;
  }

  pos = 0;
  std::string dateTemplate("<Timestamp>");
  pos = out.find(dateTemplate);
  if (pos != std::string::npos)
  {
    std::string date = DateTime(time).format(DateTime::fileFormatStr+"_%f");
    out.replace(pos,dateTemplate.length(),date);
  }

  return out;
}


static 
std::string lowercase(std::string str)
{
  return tolower(str);
}

void 
VideoWriterMod::writeIdTimeStamp(PvCanvasImage& img, double timestamp)
{
    DateTime now(timestamp);
    std::string stamp = now.format("%Y.%m.%d %H:%M:%S"); //now.format("%Y.%m.%d %H:%M:%S.%f");

#ifdef WIN32
    HFONT hfnt, hOldFont; 
    HDC hdc = img.dc();
 
    SetBkMode(hdc,TRANSPARENT);

    hfnt = (HFONT)GetStockObject(ANSI_FIXED_FONT); 
    if (hOldFont = (HFONT)SelectObject(hdc, hfnt)) 
    {
      SIZE size;
      GetTextExtentPoint(hdc,stamp.c_str(),stamp.length(),&size);
      POINT pos = { img.width()-size.cx-2 , img.height()-size.cy-1 };
      SetTextColor(hdc,RGB(0,0,0));
      for (int x = -1; x <= 1; x++)
      {
        for (int y = -1; y <= 1; y++)
        {
          TextOut(hdc, pos.x+x, pos.y+y, stamp.c_str(), stamp.length()); 
        }
      }
      SetTextColor(hdc,RGB(255,255,255));
      TextOut(hdc, pos.x, pos.y, stamp.c_str(), stamp.length()); 
      SelectObject(hdc, hOldFont); 
    } 

    GdiFlush();
#endif

}




//
// ACCESS
//

//
// INQUIRY
//


}; // namespace vision

}; // namespace ait



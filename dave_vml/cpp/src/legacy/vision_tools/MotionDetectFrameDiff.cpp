/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "legacy/vision_tools/MotionDetectFrameDiff.hpp"

namespace ait 
{

MotionDetectFrameDiff::MotionDetectFrameDiff()
{
}

MotionDetectFrameDiff::~MotionDetectFrameDiff()
{
}

void 
MotionDetectFrameDiff::init(float thres1, 
                            float thres2, 
                            bool calcImage)
{
  mThreshold1 = static_cast<int>(thres1);
  mThreshold2 = thres2;
  mCalcEnergyImage = calcImage;
  reset();
}

//
// OPERATIONS
//

void 
MotionDetectFrameDiff::update(const Image8& inputImage)
{
  int x,y;

  if (mPrevImage.size() == inputImage.size())
  {
    const Image8& prevImg = mPrevImage;
    const Image8& nextImg = inputImage;

    int d;
    int count = 0;

    if (!mCalcEnergyImage)
    {
      for(y=0; y < prevImg.height(); y++)
      {
        for(x=0 ;x < prevImg.width(); x++)  
        {
          d = (int)prevImg(x,y) - (int)nextImg(x,y);
          d = abs(d);
          if (d >= mThreshold1)
          {
            count++;
          }
        }
      }
    }
    else
    {
      mMotionEnergy.resize(inputImage.width(), inputImage.height());
      for(y=0; y < prevImg.height(); y++)
      {
        for(x=0 ;x < prevImg.width(); x++)  
        {
          d = (int)prevImg(x,y) - (int)nextImg(x,y);
          d = abs(d);
          if (d >= mThreshold1)
          {
            count++;
            mMotionEnergy(x,y) = 255; //(Uint8)d;
          }
          else
          {
            mMotionEnergy(x,y) = 0;
          }
        }
      }
    }

    mIsMotionPresent = count > inputImage.size()*mThreshold2;
  }
  else
  {
    if (mCalcEnergyImage)
    {
      mMotionEnergy.resize(inputImage.width(), inputImage.height());
      mMotionEnergy.setAll(0);
    }
    mIsMotionPresent = false;
  }

  mPrevImage = inputImage;
}


void 
MotionDetectFrameDiff::update_fast(const Image8& inputImage)
{
  //int x,y;

  if (mPrevImage.size() == inputImage.size())
  {
    //const Image8& prevImg = mPrevImage;
    //const Image8& nextImg = inputImage;
    unsigned char* prevImgItr = mPrevImage.pointer();
    unsigned char* nextImgItr = inputImage.pointer();
    unsigned char* motionEnergyPtr = mMotionEnergy.pointer();

    int s;
    int d;
    int count = 0;

    s = inputImage.width()*inputImage.height();

    if (!mCalcEnergyImage)
    {
      for(int i=0;i<s;i++)
        {
          d = (int)prevImgItr[i] - (int)nextImgItr[i];
          d = abs(d);
          if (d >= mThreshold1)
          {
            count++;
          }
        }      
    }
    else
    {
      mMotionEnergy.resize(inputImage.width(), inputImage.height());
      for(int i=0;i<s;i++)
        {
          d = (int)prevImgItr[i] - (int)nextImgItr[i];          
          d = abs(d);
          if (d >= mThreshold1)
          {
            count++;
            motionEnergyPtr[i] = 255; //(Uint8)d;
          }
          else
          {
            motionEnergyPtr[i] = 0;
          }
        }      
    }

    mIsMotionPresent = count > inputImage.size()*mThreshold2;
  }
  else
  {
    if (mCalcEnergyImage)
    {
      mMotionEnergy.resize(inputImage.width(), inputImage.height());
      mMotionEnergy.setAll(0);
    }
    mIsMotionPresent = false;
  }

  mPrevImage = inputImage;
}

void 
MotionDetectFrameDiff::reset()
{
  mPrevImage.resize(1,1);
  mIsMotionPresent = false;
  mMotionEnergy.setAll(0);
}

//
// ACCESS
//

const Image8& 
MotionDetectFrameDiff::getMotionEnergy()
{
  if (!mCalcEnergyImage)
  {
    PvUtil::exitError("Energy Image is not enabled!");
  }
  return mMotionEnergy;
}

//
// INQUIRY
//

bool 
MotionDetectFrameDiff::isMoving()
{
  return mIsMotionPresent;
}

}; // namespace ait


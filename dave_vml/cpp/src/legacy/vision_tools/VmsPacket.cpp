/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "VmsPacket.hpp"
#include "RealTimeMod.hpp"
#include "ImageAcquireMod.hpp"
#include <boost/lexical_cast.hpp>
#include <Socket.hpp>
#include <Settings.hpp>
#include <DateTime.hpp>
#include <strutil.hpp>
#include <csv_utils.hpp>
#include <Base64.hpp>

namespace ait 
{

VmsPacket::VmsPacket(ait::vision::RealTimeMod* pMod)
: mLastMessageTimeTamp(0),
  mProgress(0),
  mpRealTimeMod(pMod),
  mIsEventTagOpen(false),
  mForceLiveSource(false)
{
  mForceLiveSource = !pMod->getImageAcquireModule()->getSettings().getBool("useVideoFramesOnVmsPacket [bool]",true);
  mUseTimeStamps = pMod->getImageAcquireModule()->getSettings().getBool("useTimeStampsOnVmsPacket [bool]",false);
  updateSystemTime();
}

VmsPacket::VmsPacket()
: mLastMessageTimeTamp(0),
  mProgress(0),
  mpRealTimeMod(NULL),
  mIsEventTagOpen(false),
  mForceLiveSource(false)
{
  updateSystemTime();
}

VmsPacket::~VmsPacket()
{
}

void
VmsPacket::initConnection(int listenPort)
{
  mXmlMessage += "<connection_info><listen_port>" + 
    boost::lexical_cast<std::string>(listenPort) + 
    "</listen_port></connection_info>";
  sendXml(true);
}

void
VmsPacket::allowPause()
{
  mXmlMessage += "<accept_pause/>";
  sendXml(true);
}

void
VmsPacket::updateSystemTime()
{
  mSystemTimeToTimeOffset = PvUtil::systemTime() - PvUtil::time();
  //PVMSG("mSystemTimeToTimeOffset: %.03f\n",mSystemTimeToTimeOffset);
}

std::string
VmsPacket::getIdAttributes(int eventChannelId)
{
  std::string evtChan, vidSrc;

  Settings set("VmsVision"); 

  if (eventChannelId == -1)
  {
    if (!mpRealTimeMod)
    {
      evtChan = set.getString("eventChannelId", "-1");
    }
    else
    {
      evtChan = Settings::replaceVars("<EventChannelId>",mpRealTimeMod->getSettings().getPath());
    }
  }
  else
  {
    evtChan = boost::lexical_cast<std::string>(eventChannelId);
  }

  if (mpRealTimeMod)
  {
    vidSrc = Settings::replaceVars("<VideoSourceId>",mpRealTimeMod->getSettings().getPath());
  }
  else
  {
    vidSrc = set.getString("videoSourceId","-1");
  }

  return " video_source_id=\"" + vidSrc + "\" event_channel_id=\"" + evtChan + "\"";
}

void 
VmsPacket::beginEvent(Int64 startFrame, Int64 duration, int eventChannelId)
{
  std::string evtChan, vidSrc;

  if (duration <= 0)
  {
    PvUtil::exitError("Events must have duration greater than zero.");
  }

  mXmlMessage += "<events " + getIdAttributes(eventChannelId) + ">";

  mXmlMessage += "<e s=\"" + 
    boost::lexical_cast<std::string>((long double)startFrame) +
    "\" d=\"" +
    boost::lexical_cast<std::string>((long double)duration) +
    "\"";

  mIsEventTagOpen = true;
}

void
VmsPacket::beginEventTime(double startTimestamp, double duration, int eventChannelId)
{
  mEventStartTime = startTimestamp;
  mEventDuration = duration;
  mEventAttributes.clear();

  if (!mUseTimeStamps)
  {
    updateSystemTime();
    Int64 startFrame = timeToAbsoluteFrames(startTimestamp);
    beginEvent(startFrame,
      timeToAbsoluteFrames(startTimestamp+duration)-startFrame, eventChannelId);
  }
  else
  {
    if (duration <= 0)
    {
      PvUtil::exitError("Events must have duration greater than zero.");
    }

    mXmlMessage += "<events " + getIdAttributes(eventChannelId) + ">";

    mXmlMessage += "<e st=\"" + 
      DateTime(startTimestamp).format(DateTime::vmsFormatStr) +
      "\" dt=\"" +
      aitSprintf("%.03lf",duration) +
      "\"";
    mIsEventTagOpen = true;
  }
}

void 
VmsPacket::addAttribute(const std::string& id, const std::string& value)
{
  if (!mIsEventTagOpen)
  {
    PvUtil::exitError("Must write all attributes before extended events");
  }
  mXmlMessage += " a" + id + "=\"" + value + "\"";
  mEventAttributes[id] = value;
}

void 
VmsPacket::addLongStringAttribute(const std::string& id, const std::string& value)
{
  mExtendedAttributes += "<a" + id + ">" + xml_escape(value) + "</a" + id + ">";
  mEventAttributes[id] = value;
}

void 
VmsPacket::addBinaryDataAttribute(const std::string& id, const Uint8 *data, int dataLength)
{
  std::string encoded = Base64::encode(data,dataLength);
  mExtendedAttributes += "<a" + id + ">" + encoded + "</a" + id + ">";
  mEventAttributes[id] = encoded;
}


void 
VmsPacket::enableCompletionNotification(int eventChannelId)
{
  std::string evtChan, vidSrc;
  addString("<enable_completion_notification " + getIdAttributes(eventChannelId) + "/>");
}

void 
VmsPacket::notifyProcessCompletion(double timestamp, int eventChannelId)
{
  updateSystemTime();
  Int64 frame = timeToAbsoluteFrames(timestamp);
  addString("<completion_notification " + getIdAttributes(eventChannelId) + " frame=\"" +
    boost::lexical_cast<std::string>((long double)frame) + "\"/>");
}

Int64 
VmsPacket::timeToAbsoluteFrames(double timestamp)
{
  if (!mpRealTimeMod)
  {
    PvUtil::exitError("Image acquire module was not defined for this Vms packet");
  }
  if (mForceLiveSource || mpRealTimeMod->getImageAcquireModule()->isLiveVideoSource())
  {
    // Calculate frame based on Turing's birthday and 1000 frames
    // per second.
    DateTime dt(1912,6,12);
    double secs = dt.toSeconds();

    // The ImageAcquire module obtains the current time using PvUtil::time().
    // However we must use the system time to get an accurate time stamp for
    // the current event.
    timestamp += mSystemTimeToTimeOffset;

    return (Int64)((timestamp - secs)*1000);
  }
  else
  {
    return (Int64)(mpRealTimeMod->getCurrentFrameNumber() - (mpRealTimeMod->getCurrentTime() - timestamp)*mpRealTimeMod->getFramesPerSecond());
  }
}

void
VmsPacket::addString(const std::string& str)
{
  if (mIsEventTagOpen)
  {
    mXmlMessage += ">";
    mIsEventTagOpen = false;
  }
  mXmlMessage += str;
}


void 
VmsPacket::endEvent()
{
  if (!mExtendedAttributes.empty())
  {
    addString(mExtendedAttributes);
    mExtendedAttributes = "";
  }
  if (mIsEventTagOpen)
  {
    mXmlMessage += "/>";
  }
  else
  {
    mXmlMessage += "</e>";
  }
  mIsEventTagOpen = false;
  mXmlMessage += "</events>";
  sendXml();

  // Save CSV event.
  std::string dirName = mpRealTimeMod->getSettings().getString("csvEventsOutputDirName","");
  if (dirName != "")
  {
    DateTime now;
    std::string fname = dirName + "/events_" + now.format("%Y%m%d") + ".csv";
    try
    {
      bool fileExists = PvUtil::fileExists(fname.c_str());
      std::ofstream fout(fname.c_str(),std::ios::app|std::ios::out);
      std::vector<std::string> rows;
      AttributeMap::iterator i;

      if (!fileExists)
      {
        rows.push_back("Start Time");
        rows.push_back("Duration");
        for (i = mEventAttributes.begin(); i != mEventAttributes.end(); ++i)
        {
          rows.push_back("attribute"+i->first);
        }
        writeCsvLine(fout,rows);
        rows.clear();
      }

      rows.push_back(DateTime(mEventStartTime).toString());
      rows.push_back(aitSprintf("%.3f",mEventDuration));
      for (i = mEventAttributes.begin(); i != mEventAttributes.end(); ++i)
      {
        rows.push_back(i->second);
      }
      writeCsvLine(fout,rows);
      fout.close();
    }
    catch (std::exception ex)
    {
      PVMSG("Error writing event to CVS file: %s\n",ex.what());
    }
  }
}

void 
VmsPacket::updateProgress(double p)
{
  updateSystemTime();
  mProgress = p;
  sendXml();
}

void 
VmsPacket::sendXml(bool forceSend)
{
  if (forceSend || PvUtil::time() - mLastMessageTimeTamp > 1)
  {
    mLastMessageTimeTamp = PvUtil::time();

    Settings set("VmsVision");

    Uint dstPort = set.getInt("dstPort", 3000);
    std::string dstAddr = set.getString("dstIpAddress","localhost");

    std::string cameraId = set.getString("cameraViewId","");

    if (mpRealTimeMod)
    {
      cameraId = Settings::replaceVars("<CameraId>",mpRealTimeMod->getSettings().getPath());
    }

    std::string msg = "<?xml version=\"1.0\"?><vms_packet";
    
    if (cameraId != "")
    {
      msg += " camera_id='" + cameraId + "'";
    }
    msg += ">" + mXmlMessage;

    if (mProgress > 0)
    {
      msg += "<progress>" + boost::lexical_cast<std::string>(mProgress) + "</progress>";
    }
    msg += "</vms_packet>";

    if (!set.getBool("sendVmsEvents [bool]",0))
    {
      if (mXmlMessage != "")
      {
        if (msg.size() < 1024)
        {
          PVMSG("%s\n",msg.c_str());
        }
        else
        {
          PVMSG("Large vms packet: %02f KB\n",(float)msg.size()/1024.0f);
        }
      }
      mXmlMessage = "";
      return;
    }

    Socket s;

    if (s.open(dstAddr,dstPort,Socket::TCP|Socket::WRITE_ONLY))
    {
      s.send(msg);
      //PVMSG("Sent Event to %s:%d...\n",dstAddr.c_str(),dstPort);
      mXmlMessage = "";
    }
    else
    {
      PVMSG("Failed to send to %s:%d...\n",dstAddr.c_str(),dstPort);
    }
  }
}

void
VmsPacket::finish()
{
  updateProgress(1);
  mXmlMessage += "<finished/>";
  sendXml(true);
  mpRealTimeMod = NULL;
}

//
// ACCESS
//

//
// INQUIRY
//

}; // namespace ait


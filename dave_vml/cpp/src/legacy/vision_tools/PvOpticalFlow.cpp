/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
#include "legacy/vision_tools/PvOpticalFlow.hpp"

namespace ait
{

/**********************************************************************************
 * Quantized Optical Flow
 **********************************************************************************/
void PvQuantizedFlow::process(Image32 &input,PvFlowField &output)
{
  int i;
  int x,y;

  unsigned char *img1,*img2;

  static int vx[27]={ -1, 0, 1,-1, 0, 1,-1, 0, 1 }; 
  static int vy[27]={ -1,-1,-1, 0, 0, 0, 1, 1, 1 };

  // check dimensions
  if(w!=input.width() || h!=input.height())
  {
    PvUtil::exitError("opticalFlow: Error, dimensions should be %d x %d\n",w,h);
  }

  output.resize(w,h);

  // frame store
  for(i=3*d-1;i>0;i--) frames[i]=frames[i-1];
  frames[0]=input.pointer();
  nFrames++;

  if(nFrames<3*d)
  {
    if(first)
    {
      PVMSG("optical flow: %d x %d\n",input.width(),input.height());
      for(i=0;i<convNum;i++)
      {
        convImg[i].resize(w,h);
        convFrames[i]=convImg[i].pointer();
      }
      
      output.setAll(flowVector(0,0));

      PvImageProc::sumOperatorAlloc(convImg[0], n, sumOp);

      first=false;

      int j0,j1;

      i=0;j0=9;j1=17;
      while(i<9)
      {
        if(i==4) i++;
        vx[j0  ]=vx[i];
        vy[j0++]=vy[i];
        vx[j1  ]=vx[i];
        vy[j1++]=vy[i];
        i++;
      }

      i=0;j0=9;j1=17;
      while(i<9)
      {
        vx[i]*=2;
        vy[i]*=2;
        if(i!=4)
        {
          vx[j0]*=2;
          vy[j0]*=2;
          vc[j0] =0x8080ff;
          vc[j1] =0x0000ff;
          j0++;
          j1++;
        }
        vc[i   ] =0xffffff;
        if(i==4) vc[i]=0;
        i++;
      }

      for(i=0;i<25;i++)
      {
        PVMSG("%d [%d %d %d]\n",i,vx[i],vy[i],vc[i]);
      }

    }

    return;
  }
  int offset;
  const int off0_R=4*(-1-w)+R_IDX,off0_G=4*(-1-w)+G_IDX,off0_B=4*(-1-w)+B_IDX;
  const int off1_R=4*(  -w)+R_IDX,off1_G=4*(  -w)+G_IDX,off1_B=4*(  -w)+B_IDX;
  const int off2_R=4*( 1-w)+R_IDX,off2_G=4*( 1-w)+G_IDX,off2_B=4*( 1-w)+B_IDX;
  const int off3_R=4*(-1  )+R_IDX,off3_G=4*(-1  )+G_IDX,off3_B=4*(-1  )+B_IDX;
  const int off4_R=         R_IDX,off4_G=         G_IDX,off4_B=         B_IDX;
  const int off5_R=4*( 1  )+R_IDX,off5_G=4*( 1  )+G_IDX,off5_B=4*( 1  )+B_IDX;
  const int off6_R=4*(-1+w)+R_IDX,off6_G=4*(-1+w)+G_IDX,off6_B=4*(-1+w)+B_IDX;
  const int off7_R=4*(   w)+R_IDX,off7_G=4*(   w)+G_IDX,off7_B=4*(   w)+B_IDX;
  const int off8_R=4*( 1+w)+R_IDX,off8_G=4*( 1+w)+G_IDX,off8_B=4*( 1+w)+B_IDX;
  int r,g,b;
  int dr,dg,db;

  for(y=m;y<h-m;y++)
  {
    img1=(unsigned char *)frames[0]+4*(m+y*w);
    for(x=m;x<w-m;x++)
    {
      offset=x+y*w;
      b=int(*(img1++));
      g=int(*(img1++));
      r=int(*img1);
      img1+=2;

      img2=(unsigned char *)frames[1]+4*offset;
      dr=r-img2[off0_R];dg=g-img2[off0_G];db=b-img2[off0_B];
      convFrames[0][offset]=dr*dr+dg*dg+db*db;

      dr=r-img2[off1_R];dg=g-img2[off1_G];db=b-img2[off1_B];
      convFrames[1][offset]=dr*dr+dg*dg+db*db;

      dr=r-img2[off2_R];dg=g-img2[off2_G];db=b-img2[off2_B];
      convFrames[2][offset]=dr*dr+dg*dg+db*db;

      dr=r-img2[off3_R];dg=g-img2[off3_G];db=b-img2[off3_B];
      convFrames[3][offset]=dr*dr+dg*dg+db*db;

      dr=r-img2[off4_R];dg=g-img2[off4_G];db=b-img2[off4_B];
      convFrames[4][offset]=dr*dr+dg*dg+db*db;

      dr=r-img2[off5_R];dg=g-img2[off5_G];db=b-img2[off5_B];
      convFrames[5][offset]=dr*dr+dg*dg+db*db;

      dr=r-img2[off6_R];dg=g-img2[off6_G];db=b-img2[off6_B];
      convFrames[6][offset]=dr*dr+dg*dg+db*db;

      dr=r-img2[off7_R];dg=g-img2[off7_G];db=b-img2[off7_B];
      convFrames[7][offset]=dr*dr+dg*dg+db*db;

      dr=r-img2[off8_R];dg=g-img2[off8_G];db=b-img2[off8_B];
      convFrames[8][offset]=dr*dr+dg*dg+db*db;

      if(d>=3)
      {
      img2=(unsigned char *)frames[2]+4*offset;
      dr=r-img2[off0_R];dg=g-img2[off0_G];db=b-img2[off0_B];
      convFrames[9][offset]=dr*dr+dg*dg+db*db;

      dr=r-img2[off1_R];dg=g-img2[off1_G];db=b-img2[off1_B];
      convFrames[10][offset]=dr*dr+dg*dg+db*db;

      dr=r-img2[off2_R];dg=g-img2[off2_G];db=b-img2[off2_B];
      convFrames[11][offset]=dr*dr+dg*dg+db*db;

      dr=r-img2[off3_R];dg=g-img2[off3_G];db=b-img2[off3_B];
      convFrames[12][offset]=dr*dr+dg*dg+db*db;

      dr=r-img2[off5_R];dg=g-img2[off5_G];db=b-img2[off5_B];
      convFrames[13][offset]=dr*dr+dg*dg+db*db;

      dr=r-img2[off6_R];dg=g-img2[off6_G];db=b-img2[off6_B];
      convFrames[14][offset]=dr*dr+dg*dg+db*db;

      dr=r-img2[off7_R];dg=g-img2[off7_G];db=b-img2[off7_B];
      convFrames[15][offset]=dr*dr+dg*dg+db*db;

      dr=r-img2[off8_R];dg=g-img2[off8_G];db=b-img2[off8_B];
      convFrames[16][offset]=dr*dr+dg*dg+db*db;

      if(d>=4)
      {
        img2=(unsigned char *)frames[3]+4*offset;
        dr=r-img2[off0_R];dg=g-img2[off0_G];db=b-img2[off0_B];
        convFrames[17][offset]=dr*dr+dg*dg+db*db;
        
        dr=r-img2[off1_R];dg=g-img2[off1_G];db=b-img2[off1_B];
        convFrames[18][offset]=dr*dr+dg*dg+db*db;
        
        dr=r-img2[off2_R];dg=g-img2[off2_G];db=b-img2[off2_B];
        convFrames[19][offset]=dr*dr+dg*dg+db*db;
        
        dr=r-img2[off3_R];dg=g-img2[off3_G];db=b-img2[off3_B];
        convFrames[20][offset]=dr*dr+dg*dg+db*db;
               
        dr=r-img2[off5_R];dg=g-img2[off5_G];db=b-img2[off5_B];
        convFrames[21][offset]=dr*dr+dg*dg+db*db;
        
        dr=r-img2[off6_R];dg=g-img2[off6_G];db=b-img2[off6_B];
        convFrames[22][offset]=dr*dr+dg*dg+db*db;
        
        dr=r-img2[off7_R];dg=g-img2[off7_G];db=b-img2[off7_B];
        convFrames[23][offset]=dr*dr+dg*dg+db*db;
        
        dr=r-img2[off8_R];dg=g-img2[off8_G];db=b-img2[off8_B];
        convFrames[24][offset]=dr*dr+dg*dg+db*db;
      }
      }
    }
  }

  for(i=0;i<convNum-d+1;i++)
  {
    PvImageProc::sumOperatorFast(convImg[i],n,sumOp);
  }

  int di,min,minInit=MAX_INT;
  int c;
  int offsetDest,yw;

  for(y=n+m;y<h-(n+m);y++)
  {
    yw=y*w;
    offset=n+m+yw;
    offsetDest=offset+w-2*(n+m);

    while(offset<offsetDest)
    {
      min=minInit;
      i=0;
      while(i<convNum-d+1)
      {
        c=convFrames[i][offset];
        if(c<min) { min=c;di=i; }
        i++;
      }
      output(offset).x=vx[di];
      output(offset).y=vy[di];
      offset++;
    }
  }
}

void PvQuantizedFlow::visualize(Image32 &frame, PvFlowField &flowField)
{
  int x,y,d=frame.width()/flowField.width();
  int level=PvUtil::intLog2(d);

  d=d/2;

  for(y=0;y<h;y++)
  {
    for(x=0;x<w;x++)
    {
      frame.line((x<<level)+d,(y<<level)+d,(x<<level)+flowField(x,y).x+d,(y<<level)+flowField(x,y).y+d,0xffffff);
    }
  }
}

} // namespace ait

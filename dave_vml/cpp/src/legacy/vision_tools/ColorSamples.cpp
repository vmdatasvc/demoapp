/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include <string.h>

#include "legacy/vision_tools/ColorSamples.hpp"
#include "legacy/types/AitBaseTypes.hpp"

using namespace ait;

// create checkered pattern
void ColorSamples::markFreeSampleSlots(void)
{
  int x,y;
  int w=sampleStorage.width();
  int h=sampleStorage.height();

  y=nrOfSamples/w;
  x=nrOfSamples%w;

  while(y<h)
  {
    while(x<w)
    {
      if(((x % 16)<8)==((y % 16)<8))
      {
        sampleStorage(x,y)=0x808080;
      }
      else
      {
        sampleStorage(x,y)=0xffffff;
      }
      x++;
    }
    x=0;
    y++;
  }
}

void ColorSamples::resize(const unsigned int minNewSize)
{
  unsigned int storageSize=sampleStorage.width()*sampleStorage.height();

  // resize really necessary?
  if(minNewSize<=storageSize) return; // no

  unsigned int newSize;
  int sizeLog=1;

  // double size of storage
  newSize=sampleStorage.width()*sampleStorage.height()*2;

  // check if even that is not enough
  if(newSize<minNewSize) newSize=minNewSize;

  // make new size a power of two
  while((1<<sizeLog)<newSize) sizeLog++;

  newSize=(1<<sizeLog);

  // set width and height
  // image will be squared or twice the size in x than in y direction
  unsigned int shift0,shift1;
  unsigned int newWidth,newHeight;

  shift0=sizeLog/2;
  shift1=sizeLog-shift0;

  newWidth=(1<<shift1);
  newHeight=(1<<shift0);

  // allocate temporary sample storage
  unsigned int *tmpStorage=new unsigned int [nrOfSamples];

  // copy sample to temp. storage
  int i;
  for(i=0;i<nrOfSamples;i++) tmpStorage[i]=sampleStorage(i);

  // allocate new sample storage
  sampleStorage.resize(newWidth,newHeight);

  markFreeSampleSlots();

  // copy sample back from temp. storage
  for(i=0;i<nrOfSamples;i++) sampleStorage(i)=tmpStorage[i];

  delete [] tmpStorage;
}

void ColorSamples::addSamples(const Image32 &image)
{
  unsigned int imgSize=image.width()*image.height();

  // make sure size is sufficient
  resize(nrOfSamples+imgSize);

  int i;

  for(i=0;i<imgSize;i++) sampleStorage(nrOfSamples+i)=image(i);

  nrOfSamples+=imgSize;
}

void ColorSamples::addSample(const unsigned int sample)
{
  // make sure size is sufficient
  resize(nrOfSamples+1);

  sampleStorage(nrOfSamples)=sample;

  nrOfSamples++;
}

int compare(const void *ptr1,const void *ptr2)
{
  int a=*((unsigned int *)ptr1);
  int b=*((unsigned int *)ptr2);
  if(a>b)
  {
    return 1;
  }
  else if(a<b) return -1;
  else return 0;
}

// remove outliers
void ColorSamples::removeOutliers(float keepFraction, unsigned int logBinSize)
{
  int i,j,n=size();

  // determine number of pixel to remove
  // the actual number will be less or equal to this fraction
  int fraction=n-int(float(n)*keepFraction);

  if(fraction==0) return;

  int binSize=1<<logBinSize;
  int d=256>>logBinSize;
  int s1=8-logBinSize,s2=2*s1;
  int nrOfBins=d*d*d,nrOfFullBins;
  int *histogram=new int [nrOfBins];
  int *sorted=new int [nrOfBins];

  int bin;

  int r,g,b;

  // clear histogram
  memset(histogram,0,nrOfBins*sizeof(int));

  // create histogram

  for(i=0;i<n;i++)
  {
    const unsigned int pixel = sampleStorage(i);
    r=RED(pixel)>>binSize;
    g=GREEN(pixel)>>binSize;
    b=BLUE(pixel)>>binSize;

    bin=r+(g<<s1)+(b<<s2);

    histogram[bin]++;
  }

  // sort bins according to counts
  nrOfFullBins=0;
  for(i=0;i<nrOfBins;i++)
  {
    if(histogram[i]!=0)
    {
      sorted[nrOfFullBins]=histogram[i];
      nrOfFullBins++;
    }
  }

  qsort(sorted,nrOfFullBins,sizeof(int),compare);

  j=0;
  for(i=0;i<nrOfFullBins;i++)
  {
    j+=sorted[i];
    PVMSG("sorted[%d]=%d sum=%d\n",i,sorted[i],j);
  }

  // find the cut-off count value
  // all colors that fall into a bin that has count value less
  // or equal to this cut-off, will be considered outliers
  i=0;j=0;
  while(j<fraction)
  {
    j+=sorted[i++];
  }

  int cutOff;

  if(i==0) goto done;

  cutOff=sorted[i-1]-1;

  if(cutOff==0) goto done;

  j=0;
  // re-create sampleStorage by removing the outliers
  for(i=0;i<n;i++)
  {
    const unsigned int pixel = sampleStorage(i);
    r=RED(pixel)>>binSize;
    g=GREEN(pixel)>>binSize;
    b=BLUE(pixel)>>binSize;

    bin=r+(g<<s1)+(b<<s2);

    if(histogram[bin]>cutOff)
    {
      sampleStorage(j++)=sampleStorage(i);
    }
  }

  nrOfSamples=j;  

  markFreeSampleSlots();

done:

  delete [] histogram;
  delete [] sorted;

  return;
}

// Create an image of the color distribution and save it to disk
void ColorSamples::visualize(void)
{
  //removeOutliers(0.80f,2);

  Image32 rgb(256*3,256);
 
  rgb.setAll(PV_RGB(0,0,255));

  int r,g,b;
  int i,n=size();

  for(i=0;i<n;i++)
  {
    const unsigned int pixel = sampleStorage(i);
    r=RED(pixel);
    g=GREEN(pixel);
    b=BLUE(pixel);

    rgb(r,g)=sampleStorage(i);
    rgb(r+256,b)=sampleStorage(i);
    rgb(g+512,b)=sampleStorage(i);
  }

  //rgb.savepng("rgb.png");


  //
  // I1I2I3 space
  //
	//	I1 = (r+g+b)/3;              [0,256]
  //  I2 = (r-b)/2+128;            [0,256]
  //	I3 = *((g*2)-(r+b))+512)/4;  [0,256]

  Image32 i123(256*3,256);
 
  i123.setAll(PV_RGB(0,0,255));

  float i1,i2,i3;

  for(i=0;i<n;i++)
  {
    const unsigned int pixel = sampleStorage(i);
    r=RED(pixel);
    g=GREEN(pixel);
    b=BLUE(pixel);

    i1=(r+g+b)/3.0f;
    i2=(r-b)/2.0f+128.0f;
    i3=((g*2.0f-r-b)*0.25f+128.0f);

    i123((Uint)i1,(Uint)i2)=sampleStorage(i);
    i123((Uint)(i1+256),(Uint)i3)=sampleStorage(i);
    i123((Uint)(i2+512),(Uint)i3)=sampleStorage(i);
  }

  //i123.savepng("i123.png");
}

void ColorSamples::setColorSpace(PvImageProc::colorSpaceConst space)
{
  colorSpace=space;
}

void ColorSamples::meanAndVariance(Vector3f &mean, Vector3f &var,PvImageProc::colorSpaceConst space)
{
  Image32 image(1,1);

  image.mx_fromarray(sampleStorage.pointer(),nrOfSamples /* cols */,1 /* rows */);

  if(space==PvImageProc::INVALID_SPACE)
  {
    space=colorSpace;
  }

  PvImageProc::meanAndVariance(space,image,mean,var);
};


//Copy.
ColorSamples& ColorSamples::operator=(const ColorSamples& rightSide){

	//Copy all members.
	if(this != &rightSide)
    {

		nrOfSamples = rightSide.nrOfSamples;
		sampleStorage = rightSide.sampleStorage;
			
	}


	return *this;



};

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "legacy/vision_tools/SvmClassifier.hpp"

namespace ait 
{

namespace vision
{

SvmClassifier::SvmClassifier()
{
}

SvmClassifier::~SvmClassifier()
{
}

void 
SvmClassifier::init(ClassifierLabelsPtr pLabels,
                    const std::string& paramFile,
                    Label positiveClassLabel,
                    Label negativeClassLabel,
                    char kernelType,
                    double kernelParameter,
                    float uncertaintyGap,
                    int vectorSize
                    )
{
  // Create SVM class.
  mpSvm.reset(new SVM<Float>());

  // This will not be necessary with the new file format.
  mpSvm->setXDimension(vectorSize);
  mpSvm->setKernelParams(kernelType,kernelParameter);
  mpSvm->setUncertaintyGap(uncertaintyGap);

  try
  {
    mpSvm->load(paramFile);
  }
  catch (std::exception e)
  {
    PvUtil::exitError("Error reading SVM file: %s: %s",paramFile.c_str(),e.what());
  }

  if (positiveClassLabel == ClassifierLabels::UNKNOWN || 
      negativeClassLabel == ClassifierLabels::UNKNOWN)
  {
    positiveClassLabel = pLabels->add(mpSvm->getPositiveLabel());
    negativeClassLabel = pLabels->add(mpSvm->getNegativeLabel());
  }

  // Init the base classifier.
  BinaryClassifier::init(pLabels,positiveClassLabel,negativeClassLabel);
}

//
// OPERATIONS
//

void 
SvmClassifier::classify(Sample& sample, Result& result)
{
  SVM_CLASS_LABEL classLabel;

  // Perform the SVM classification.
  classLabel = mpSvm->classify(sample.pVector->pointer());

  switch(classLabel)
  {
  case 0:
    result.label = ClassifierLabels::UNKNOWN;
    break;
  case 1:
    result.label = mPositiveLabel;
    break;
  case -1:
    result.label = mNegativeLabel;
    break;
  }

  result.distance = mpSvm->getLastDecisionValue();
}

void 
SvmClassifier::classify(Classifier::Sample& sample, Classifier::Result& result)
{
  Sample *pSample = dynamic_cast<Sample *>(&sample);
  Result *pResult = dynamic_cast<Result *>(&result);
  if (!pSample || !pResult) PvUtil::exitError("Invalid sample or result type for SvmClassifier");
  classify(*pSample,*pResult);
}

//
// ACCESS
//

//
// INQUIRY
//

}; // namespace vision

}; // namespace ait


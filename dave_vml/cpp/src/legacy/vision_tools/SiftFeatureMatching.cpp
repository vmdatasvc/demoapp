/************************************************************************
Demo software: Invariant keypoint matching.
Author: David Lowe

match.c:
This file contains a sample program to read images and keypoints, then
   draw lines connecting matched keypoints.


Modified by: Yan Ke
December 2003
Adds matching of PCA-SIFT local descriptors.
Adds option of matching nearest neighbor with and without looking
at second nearest neighbor.

*************************************************************************/


/*---------------------------- Constants ---------------------------------*/
/* by Yan Ke.
   1 if we check the absolute distance to the nearest neighbor only.
   This uses SIFT_THRESH and PCA_THRESH.

   0 if we check the relative distance between the closest
   and second closest neighbors.
   This uses SIFT_MULT and PCA_MULT.

   In practice, SIFT works better if we check for relative distances.
   (NEAREST_ONLY = 0). PCA-SIFT works better if we check for absolute
   distances (NEAREST_ONLY = 1).

   Further, NEAREST_ONLY = 0 works better if we're matching exactly two images.

   However, NEAREST_ONLY = 0 works only if we're searching for one instance
   of a keypoint.  If we're looking for multiple instances (for example from
   two of the same objects in the scene), then we must use NEAREST_ONLY = 1.
   Similarly, if we're looking for keypoints in a large database and we don't
   don't know if the second closest neighbor is supposed to be from the same
   or a different keypoint, then we must use NEAREST_ONLY = 1.
*/
////#define NEAREST_ONLY 0
////
////#define SIFT_MULT 0.6
////#define PCA_MULT 0.6
////
////#define SIFT_THRESH 90
////#define PCA_THRESH 3000
////
#include "legacy/vision_tools/SiftFeatureMatching.hpp"
#include <map>

namespace ait 
{

namespace vision
{

SiftFeatureMatching::SiftFeatureMatching()
: mIsInitialized(false)
{
}

SiftFeatureMatching::~SiftFeatureMatching()
{
}

/*----------------------------- Routines ----------------------------------*/

/* Top level routine.  Read PGM images and keypoints from files given
   in command line arguments, then call FindMatches.
*/

void
SiftFeatureMatching::init(std::string settingsPath)
{
  //remove file ending
  mSettings.changePath(settingsPath + "/FeatureMatching");
	
	loadTrainingPattern();
  
  mMatchMethodIsNEAREST_ONLY = mSettings.getBool("UseNearestMatchMethod [bool]", true);

  mSIFT_MULT = mSettings.getFloat("MatchTresholdMultiplier(SIFT)", 0.6f);
  mPCA_MULT = mSettings.getFloat("MatchTresholdMultiplier(PCA)", 0.6f);

  mSIFT_THRESH = mSettings.getInt("MatchTreshold(SIFT)", 90);
  mPCA_THRESH = mSettings.getInt("MatchTreshold(PCA)", 3000);

  mDoConsistencyCheck = mSettings.getBool("doConsistencyCheck [bool]", false);

  mIsInitialized=true;
}

void
SiftFeatureMatching::loadTrainingPattern(std::string imageFilename)
{
	if(imageFilename=="")
	{		
		imageFilename = mSettings.getString("referencePatternImage", "<CommonFilesPath>/data/sift_feature_matching/pattern.png");
	}

	mTrainingPatternPvImage.loadpng(imageFilename.c_str());

	mTrainingPatternImageWidth = mTrainingPatternPvImage.width();
	mTrainingPatternImageHeight = mTrainingPatternPvImage.height();

	mTrainingPatternImage = CreateImage(mTrainingPatternImageHeight, mTrainingPatternImageWidth);
	ConvertPvRGBImage(mTrainingPatternPvImage, mTrainingPatternImage);

	WritePGM("TrainingPattern.pgm", mTrainingPatternImage);

	//call sift binary to calculate key feature points and their descriptors
  std::string binary = mSettings.getString("SIFT Binary", "<CommonFilesPath>/bin/win32/SiftWin32.exe");
	std::string params = binary + " <TrainingPattern.pgm >TrainingPattern.keys";
	system(params.c_str());

	//call sift to write out the testimage with keys drawn in it
	//params = binary + " -display <TrainingPattern.pgm > TrainingPattern_keys.pgm";
	//system(params.c_str());

	//mTrainingPatternKeys = loadFeatureKeys("TrainingPattern.keys");
	loadFeatureKeys("TrainingPattern.keys", mTrainingPatternKeys );
}

void
SiftFeatureMatching::loadFeatureKeys(const char* filename, std::list<SiftFeatureMatching::KeypointPtr> &featureList)
{
  ReadKeyFile(filename, featureList);
}

void 
SiftFeatureMatching::findFeatures(Image32& image, std::vector<SIFTkeypoint> &featurePoints, bool drawKeys)
{  
  std::list<SiftFeatureMatching::KeypointPtr>::iterator featurePointMatchItr;    
  int count = 0;  
  int w = image.width();
  int h = image.height();  

  //WritePGM("TestPattern.pgm", image);

  //call sift binary to calculate key feature points and their descriptors
  std::string binary = mSettings.getString("SIFT Binary", "<CommonFilesPath>/bin/win32/SiftWin32.exe");
  std::string params = binary + " <TestPattern.pgm >TestPattern.keys";
  system(params.c_str());

  //call sift to write out the training image with keys drawn in it
  //params = binary + " -display <TestPattern.pgm >TestPattern_keys.pgm";
  //system(params.c_str());

  std::list<SiftFeatureMatching::KeypointPtr> testPatternKeys;
  loadFeatureKeys("TestPattern.keys", testPatternKeys);

  featurePoints.clear();

  SIFTkeypoint keypoint;
  for (featurePointMatchItr=testPatternKeys.begin(); featurePointMatchItr != testPatternKeys.end(); featurePointMatchItr++)
  {     
    float x0,y0;
    x0=(*featurePointMatchItr)->col;
    y0=(*featurePointMatchItr)->row;

    keypoint.id = (*featurePointMatchItr)->id;
    keypoint.matched_id = keypoint.id;
    keypoint.p.x=x0;
    keypoint.p.y=y0;
    keypoint.p.z=0;
    keypoint.p_orig = keypoint.p;
    keypoint.ori = (*featurePointMatchItr)->ori;
    keypoint.scale = (*featurePointMatchItr)->scale;

    featurePoints.push_back(keypoint);

    if(drawKeys)
    {         
      //image.ellipseFill(x0,y0, 2, 2, PV_RGB(255,0,0));        
      image.safe_set((unsigned int)x0,(unsigned int)y0, PV_RGB(255,0,0));
      image.circle((int)x0,(int)y0, 2.0f*keypoint.scale, PV_RGB(255,0,0));        
    }     
  }      
}

void
SiftFeatureMatching::matchFeatures(Image32& image, std::vector<SIFTkeypoint> &matchedFeaturePoints, bool drawKeys)
{
  std::list<SiftFeatureMatching::KeypointPtr> featurePointMatchList;
  std::list<SiftFeatureMatching::KeypointPtr>::iterator featurePointMatchItr;

  matchFeatures(image, mTrainingPatternKeys, featurePointMatchList, drawKeys);

  int w = image.width();
  int h = image.height();  
  int count=0;
  matchedFeaturePoints.clear();
  SIFTkeypoint keypoint;
  for (featurePointMatchItr=featurePointMatchList.begin(); featurePointMatchItr != featurePointMatchList.end(); featurePointMatchItr++)
  {
    if((*featurePointMatchItr)->currentMatchItr != (*featurePointMatchItr)->bestMatchList.end())
    {
      count++;
      keypoint.id = (*featurePointMatchItr)->currentMatchItr->key->id;
      keypoint.p(0) = (*featurePointMatchItr)->col/w;
      keypoint.p(1) = (*featurePointMatchItr)->row/h;
      keypoint.p(2) = 0;
      keypoint.scale = (*featurePointMatchItr)->scale;
      keypoint.ori = (*featurePointMatchItr)->ori;
      matchedFeaturePoints.push_back(keypoint);
    }
  }
}

void
SiftFeatureMatching::matchFeatures(Image32& image1, Image32& image2, std::vector<SIFTkeypoint> &matchedFeaturePoints1, std::vector<SIFTkeypoint> &matchedFeaturePoints2, bool drawKeys)
{
  std::list<SiftFeatureMatching::KeypointPtr> featurePointMatchList1;
  std::list<SiftFeatureMatching::KeypointPtr> featurePointMatchList2;
  std::list<SiftFeatureMatching::KeypointPtr>::iterator featurePointMatchItr1;
  std::list<SiftFeatureMatching::KeypointPtr>::iterator featurePointMatchItr2;  

  int w1 = image1.width();
  int h1 = image1.height();  
  int w2 = image2.width();
  int h2 = image2.height();

  int count=0;

  matchedFeaturePoints1.clear();
  matchedFeaturePoints2.clear();

  SIFTkeypoint keypoint;

  int method = mSettings.getInt("matchMethod [0=Test-ImgL-ImgR, 1=Test-ImgL-Test-ImgR]", 0);

  matchFeatures(image1, mTrainingPatternKeys, featurePointMatchList1, drawKeys);

  if(method==0)
  {
    matchFeatures(image2, featurePointMatchList1, featurePointMatchList2, false);  

    for (featurePointMatchItr2=featurePointMatchList2.begin(); featurePointMatchItr2 != featurePointMatchList2.end(); featurePointMatchItr2++)
    {
      if((*featurePointMatchItr2)->currentMatchItr != (*featurePointMatchItr2)->bestMatchList.end())      
      {
        count++;
        keypoint.id = (*featurePointMatchItr2)->currentMatchItr->key->id;
        keypoint.p(0) = (*featurePointMatchItr2)->currentMatchItr->key->col/w1;
        keypoint.p(1) = (*featurePointMatchItr2)->currentMatchItr->key->row/h1;
        keypoint.p(2) = 0;
        keypoint.p_orig(0) = (*featurePointMatchItr2)->currentMatchItr->key->currentMatchItr->key->col/mTrainingPatternImageWidth;
        //keypoint.p_orig(0) = (*featurePointMatchItr1)->currentMatchItr->key->col/mTrainingPatternImageWidth;
        keypoint.p_orig(1) = (*featurePointMatchItr2)->currentMatchItr->key->currentMatchItr->key->row/mTrainingPatternImageHeight;
        //keypoint.p_orig(1) = (*featurePointMatchItr1)->currentMatchItr->key->row/mTrainingPatternImageHeight;
        keypoint.p_orig(2) = 0;
        keypoint.scale = (*featurePointMatchItr2)->currentMatchItr->key->scale;
        keypoint.ori = (*featurePointMatchItr2)->currentMatchItr->key->ori;
        matchedFeaturePoints1.push_back(keypoint);

        //keypoint.id = (*featurePointMatchItr2)->id;
        keypoint.id = (*featurePointMatchItr2)->currentMatchItr->key->id;
        keypoint.p(0) = (*featurePointMatchItr2)->col/w2;
        keypoint.p(1) = (*featurePointMatchItr2)->row/h2;
        keypoint.p(2) = 0;
        //keypoint.p_orig(0) = (*featurePointMatchItr1)->currentMatchItr->key->col/w1;
        //keypoint.p_orig(1) = (*featurePointMatchItr1)->currentMatchItr->key->row/h1;
        keypoint.p_orig(2) = 0;
        keypoint.scale = (*featurePointMatchItr2)->scale;
        keypoint.ori = (*featurePointMatchItr2)->ori;
        matchedFeaturePoints2.push_back(keypoint);

        if(drawKeys)
        {
          int x0,y0;
          x0=(int)(*featurePointMatchItr2)->col;
          y0=(int)(*featurePointMatchItr2)->row;        
          image2.ellipseFill(x0,y0, 3, 3, PV_RGB(255,0,0));        
        }
      }
    }
  }
  else
  {
    matchFeatures(image2, mTrainingPatternKeys, featurePointMatchList2, drawKeys);  

    
    for (featurePointMatchItr1=featurePointMatchList1.begin(); featurePointMatchItr1 != featurePointMatchList1.end(); featurePointMatchItr1++)
      for (featurePointMatchItr2=featurePointMatchList2.begin(); featurePointMatchItr2 != featurePointMatchList2.end(); featurePointMatchItr2++)
      {     
        if((*featurePointMatchItr2)->currentMatchItr != (*featurePointMatchItr2)->bestMatchList.end() &&
          (*featurePointMatchItr2)->currentMatchItr->key->id == (*featurePointMatchItr1)->currentMatchItr->key->id)
        {
          keypoint.id = (*featurePointMatchItr1)->currentMatchItr->key->id;
          keypoint.p(0) = (*featurePointMatchItr1)->col/w1;
          keypoint.p(1) = (*featurePointMatchItr1)->row/h1;
          keypoint.p(2) = 0;
          keypoint.p_orig(0) = (*featurePointMatchItr1)->currentMatchItr->key->col/mTrainingPatternImageWidth;                    
          keypoint.p_orig(1) = (*featurePointMatchItr1)->currentMatchItr->key->row/mTrainingPatternImageHeight;
          keypoint.p_orig(2) = 0;
          keypoint.scale = (*featurePointMatchItr1)->scale;
          keypoint.ori = (*featurePointMatchItr1)->ori;
          matchedFeaturePoints1.push_back(keypoint);
          
          keypoint.id = (*featurePointMatchItr2)->currentMatchItr->key->id;
          keypoint.p(0) = (*featurePointMatchItr2)->col/w2;
          keypoint.p(1) = (*featurePointMatchItr2)->row/h2;
          keypoint.p(2) = 0;
          keypoint.p_orig(0) = (*featurePointMatchItr2)->currentMatchItr->key->col/mTrainingPatternImageWidth;                    
          keypoint.p_orig(1) = (*featurePointMatchItr2)->currentMatchItr->key->row/mTrainingPatternImageHeight;
          keypoint.p_orig(2) = 0;
          keypoint.scale = (*featurePointMatchItr2)->scale;
          keypoint.ori = (*featurePointMatchItr2)->ori;
          matchedFeaturePoints2.push_back(keypoint);

          if(drawKeys)
          {
            int x0,y0;
            x0=(int)(*featurePointMatchItr2)->col;
            y0=(int)(*featurePointMatchItr2)->row;        
            image2.ellipseFill(x0,y0, 3, 3, PV_RGB(255,0,0));        
          }
          
          count++;    
          continue;
        }
      }
  }

  PVMSG("%d matches imported.\n", count);  

}

void
//SiftFeatureMatching::matchFeatures(Image32& image, std::vector<SIFTkeypoint> &matchedFeaturePoints, bool drawKeys)
SiftFeatureMatching::matchFeatures(Image32& image, std::list<SiftFeatureMatching::KeypointPtr> &trainingPatternKeys, std::list<SiftFeatureMatching::KeypointPtr> &featurePointMatchList, bool drawKeys)
{
  SiftFeatureMatching::KeypointPtr k;  
  //std::list<SiftFeatureMatching::KeypointPtr> featurePointMatchList;
  std::list<SiftFeatureMatching::KeypointPtr>::iterator featurePointMatchItr;
  Image32 imgTest(image);
  //SiftFeatureMatching::Image result;
  int count = 0;

  PVASSERT(mIsInitialized==true);

  int w = image.width();
  int h = image.height();  

  WritePGM("TestPattern.pgm", image);

  //call sift binary to calculate key feature points and their descriptors
  std::string binary = mSettings.getString("SIFT Binary", "<CommonFilesPath>/bin/SiftWin32.exe");
  std::string params = binary + " <TestPattern.pgm >TestPattern.keys";
  system(params.c_str());

  //call sift to write out the training image with keys drawn in it
  //params = binary + " -display <TestPattern.pgm >TestPattern_keys.pgm";
  //system(params.c_str());

//  mTestPatternKeys = loadFeatureKeys("TestPattern.keys");
  std::list<SiftFeatureMatching::KeypointPtr> testPatternKeys;
  loadFeatureKeys("TestPattern.keys", testPatternKeys);
  matchFeatures(testPatternKeys, trainingPatternKeys, featurePointMatchList);

  if(drawKeys)
  {
    image.paste(mTrainingPatternPvImage, w-mTrainingPatternImageWidth-1, h-mTrainingPatternImageHeight-1);

    int count=0;   
    SIFTkeypoint keypoint;
    for (featurePointMatchItr=featurePointMatchList.begin(); featurePointMatchItr != featurePointMatchList.end(); featurePointMatchItr++)
    {
      if((*featurePointMatchItr)->currentMatchItr != (*featurePointMatchItr)->bestMatchList.end())
      {    
        int x0,y0,x1,y1;
        x0=(int)(*featurePointMatchItr)->col;
        y0=(int)(*featurePointMatchItr)->row;
        x1=w-mTrainingPatternImageWidth+(int)(*featurePointMatchItr)->currentMatchItr->key->col;
        y1=h-mTrainingPatternImageHeight+(int)(*featurePointMatchItr)->currentMatchItr->key->row;

        image.ellipseFill(x0,y0, 3, 3, PV_RGB(255,0,0));        
        image.ellipseFill(x1, y1, 3, 3, PV_RGB(0,0,255));
        image.line(x0,y0, x1,y1, PV_RGB(255,255,255));     
      }
    }
      
    //image.savepng("testImage_afterConsistencyCheck.png");
  }
//  PVMSG("%d matches left after consistency check.\n", count);  
}

void
SiftFeatureMatching::matchFeatures(std::list<SiftFeatureMatching::KeypointPtr> &testPatternKeys, std::list<SiftFeatureMatching::KeypointPtr> &trainingPatternKeys, std::list<SiftFeatureMatching::KeypointPtr> &featurePointMatchList)
{
  /* Match the keys in list keys1 to their best matches in keys2.
  */
  SiftFeatureMatching::KeypointPtr match;  
  std::list<SiftFeatureMatching::KeypointPtr>::iterator kItr;
  featurePointMatchList.clear();
  //PVMSG("match #%d training features to #%d test features!\n", mTrainingPatternKeys->id, mTestPatternKeys->id);

  int count=0;

//  for (kItr=trainingPatternKeys.begin(); kItr!=trainingPatternKeys.end(); kItr++)
    for (kItr=testPatternKeys.begin(); kItr!=testPatternKeys.end(); kItr++)
  {
    //match = CheckForMatch((*kItr), testPatternKeys);      
    match = CheckForMatch((*kItr), trainingPatternKeys);      

    if (match.get() != NULL) 
    {
      //PVMSG("->found match with test feature #%d\n", match->id);            
      //PVMSG("  currentMatchItr->key->id = %d, currentMatchItr->dist = %f\n", k->currentMatchItr->key->id, k->currentMatchItr->dist);
      
      featurePointMatchList.push_back((*kItr));

      count++;
    ///*  if(drawKeys)
    //  {
    //    int x0,y0,x1,y1;
    //    x0=match->col;
    //    y0=match->row;
    //    x1=w-mTrainingPatternImageWidth+k->col;
    //    y1=h-mTrainingPatternImageHeight+k->row;

    //    imgTest.ellipseFill(x0,y0, 3, 3, PV_RGB(128,0,0));        
    //    imgTest.ellipseFill(x1, y1, 3, 3, PV_RGB(0,0,128));
    //    imgTest.line(x0,y0, x1,y1, PV_RGB(128,128,128));
    //  }*/
    }
    //else
    //  match->currentMatchItr = match->bestMatchList.end();
  }

  //PVMSG("Found %d matches.\n", count);

  //WritePGM("testImage_beforeConsistencyCheck.pgm", imgTest);
  //imgTest.savepng("testImage_beforeConsistencyCheck.png");

  if(mDoConsistencyCheck) checkMatchConsistency(featurePointMatchList);
}

void
SiftFeatureMatching::checkMatchConsistency(std::list<SiftFeatureMatching::KeypointPtr> &list)
{
  std::map<int, SiftFeatureMatching::KeypointPtr> douplicateMap;
  std::map<int, SiftFeatureMatching::KeypointPtr>::iterator douplicateMapItr;
  typedef std::pair<int, SiftFeatureMatching::KeypointPtr> douplicateMapPair;

  std::list<SiftFeatureMatching::KeypointPtr>::iterator listItr;
  
  int douplicates=1;

  while(douplicates!=0)
  {
    douplicates=0;
    douplicateMap.clear();
    for(listItr=list.begin();listItr!=list.end();)
    {
      bool validMatch = false;

      //check that this key has a match assigned
      if((*listItr)->currentMatchItr != (*listItr)->bestMatchList.end())
      {
        //check that this match has valid match distance
        int distsq1 = (int)(*listItr)->currentMatchItr->dist;
        
          /* check only nearest neighbor */
          if (PCAKEYS) { /* PCA-SIFT KEYS */
            if (distsq1 < mPCA_THRESH) {              
              validMatch=true; }            
          } else { /* SIFT KEYS */        
            if (distsq1 < mSIFT_THRESH) {
              validMatch=true;}
          }
      }

      if(validMatch)
      {
        douplicateMapItr = (douplicateMap.find((*listItr)->currentMatchItr->key->id));
        //PVMSG("check training key #%d with match #%d and dist=%f\n", (*listItr)->id, (*listItr)->currentMatchItr->key->id, (*listItr)->currentMatchItr->dist);
        if(douplicateMapItr!=douplicateMap.end())
        {//another key is already matched with this key
          douplicates++;
          SiftFeatureMatching::KeypointPtr k = douplicateMapItr->second;
          //PVMSG("-> training key #%d already matched to test key #%d (dist=%f)\n", k->id, k->currentMatchItr->key->id, k->currentMatchItr->dist);
          //check if this key matches better than the currently assigned key
          if((*listItr)->currentMatchItr->dist < k->currentMatchItr->dist)
          {//this key does match better, so swap the assigned best match keys
            //PVMSG("   training key #%d and test key #%d do match better: swap keys!\n", (*listItr)->id, (*listItr)->currentMatchItr->key->id);
            douplicateMapItr->second = (*listItr);
            k->currentMatchItr++;
            if(k->currentMatchItr == k->bestMatchList.end())
            {
              //PVMSG("   ~!!!~ training key #%d has no more matches: delete this key!\n", (*listItr)->id);
              //listItr = list.erase(listItr);
              //listItr--;
            }
          }
          else
          {//this key does not match better than the already assigned match key
            //PVMSG("   training key #%d and test key #%d do NOT match better:\n  -> assign next best match #", (*listItr)->id, (*listItr)->currentMatchItr->key->id);
            (*listItr)->currentMatchItr++;          
            if((*listItr)->currentMatchItr == (*listItr)->bestMatchList.end())
            {
              //PVMSG("?\n   ~!!!~ training key #%d has no more matches: delete this key!\n", (*listItr)->id);
              //listItr = list.erase(listItr);
              //listItr--;
            }
            //else
              //PVMSG("%d (dist=%f)\n", (*listItr)->currentMatchItr->key->id, (*listItr)->currentMatchItr->dist);
          }
        }
        else
        {//no other key matches this key yet
          douplicateMap.insert(douplicateMapPair((*listItr)->currentMatchItr->key->id, *listItr));
        }      
      }//if (check for valid match)
      else
      {//this key has no valid "best match" -> delete it from the list
        //PVMSG("training key #%d has no valid match -> delete this key from the feature list!\n", (*listItr)->id);
        (*listItr)->currentMatchItr = (*listItr)->bestMatchList.end();
        listItr = list.erase(listItr);
        continue;
      }
      listItr++;
    }
   

    //PVMSG("douplicates=%d\n", douplicates);
  }
}

/* This searches through the keypoints in klist for the two closest
   matches to key.  If the closest is less than 0.6 times distance to
   second closest, then return the closest match.  Otherwise, return
   NULL.
*/
SiftFeatureMatching::KeypointPtr 
SiftFeatureMatching::CheckForMatch(SiftFeatureMatching::KeypointPtr key, std::list<SiftFeatureMatching::KeypointPtr> &klist)
{
    //int i=0;
    int dsq, distsq1 = 100000000, distsq2 = 100000000;
    SiftFeatureMatching::KeypointPtr minkey;
    std::list<SiftFeatureMatching::KeypointPtr>::iterator kItr;

    //typedef struct Match {Keypoint key; float dist;};    
    //Match match;
    std::list<KeypointSt::match> bestMatchList;

    /* Find the two closest matches, and put their squared distances in
       distsq1 and distsq2.
       */
    //for (k = klist; k.get() != NULL; k = k->next) 
    for (kItr = klist.begin(); kItr!=klist.end(); kItr++) 
    {      
      //i++;
      dsq = Dist(key, (*kItr));
  
      KeypointSt::match m;
      m.key=(*kItr);
      m.dist =(float)dsq;
      
      if (dsq < distsq1) 
      {
        distsq2 = distsq1;
        distsq1 = dsq;
        minkey = (*kItr);        
        //minkey->id=i;                
        bestMatchList.push_front(m);
      } 
      else 
      {
        bestMatchList.push_back(m);
        if (dsq < distsq2) 
        {
          distsq2 = dsq;
        }
      }
    }

    if(minkey.get()==NULL) return minkey;

    //minkey->bestMatchList = bestMatchList;
    key->bestMatchList = bestMatchList;
    key->currentMatchItr = key->bestMatchList.begin();
    
    
    if (mMatchMethodIsNEAREST_ONLY) {
      /* check only nearest neighbor */
      if (PCAKEYS) { /* PCA-SIFT KEYS */
        //PVMSG("PCAKEY[%d,%d]: dist=%d, threshold=%d -> ", key->id, minkey->id, distsq1, mPCA_THRESH);
        if (distsq1 < mPCA_THRESH) {
          //PVMSG("OK!\n");
          return minkey; }
        //PVMSG("nope!\n");
      } else { /* SIFT KEYS */        
        //PVMSG("SIFTKEY[%d,%d]: dist=%d, threshold=%d -> ",key->id, minkey->id, distsq1, mSIFT_THRESH);
        if (distsq1 < mSIFT_THRESH) {
          //PVMSG("OK!\n");
          return minkey;}
        //PVMSG("nope!\n");
      }
    } else {
      /* check second nearest neighbor as well */
      if (PCAKEYS) { /* PCA-SIFT KEYS */
        //PVMSG("PCAKEY[%d,%d]: dist=%d, threshold=%f -> ", key->id, minkey->id, distsq1, mPCA_MULT * distsq2);
        if (distsq1 < mPCA_MULT * distsq2) {
          //PVMSG("OK!\n");
          return minkey;}
        //PVMSG("nope!\n");
      } else { /* SIFT KEYS */
        //PVMSG("SIFTKEY[%d,%d]: dist=%d, threshold=%f -> ", key->id, minkey->id, distsq1, mSIFT_MULT * distsq2);
        if (distsq1 < mSIFT_MULT * distsq2) {
          //PVMSG("OK!\n");
          return minkey;}
        //PVMSG("nope!\n");
      }
    }

    KeypointPtr kptr;
    return kptr;
}


/* Return squared distance between two keypoint descriptors.
*/
int 
SiftFeatureMatching::Dist(SiftFeatureMatching::KeypointPtr k1, SiftFeatureMatching::KeypointPtr k2)
{
    unsigned int i;
    int dif;
    long int distsq = 0;
    /*int *pk1, *pk2;

    pk1 = k1->descrip;
    pk2 = k2->descrip;*/
    
    PVASSERT(k1->descrip.size() == k2->descrip.size());

    std::vector<int>::iterator k1Itr = k1->descrip.begin();
    std::vector<int>::iterator k2Itr = k2->descrip.begin();

    for (i=0; i < k1->descrip.size(); i++) {
      //dif =  *pk1++ - *pk2++;
      dif = *k1Itr++ - *k2Itr++;
      distsq += dif * dif;
    }
   
    return (int) sqrt((long double)distsq);
}



/*----------------- Routines for image creation ------------------------*/

/* Create a new image with uninitialized pixel values.
*/
SiftFeatureMatching::Image 
SiftFeatureMatching::CreateImage(int rows, int cols)
{
    SiftFeatureMatching::Image im;

    im = (SiftFeatureMatching::Image) malloc(sizeof(struct ImageSt));
    im->rows = rows;
    im->cols = cols;
    im->pixels = AllocMatrix(rows, cols);
    im->next = NULL;
    return im;
}


/* Allocate memory for a 2D float matrix of size [row,col].  This returns
     a vector of pointers to the rows of the matrix, so that routines
     can operate on this without knowing the dimensions.
*/
float**
SiftFeatureMatching::AllocMatrix(int rows, int cols)
{
    int i;
    float **m, *v;

    m = (float **) malloc(rows * sizeof(float *));
    v = (float *) malloc(rows * cols * sizeof(float));
    for (i = 0; i < rows; i++) {
	m[i] = v;
	v += cols;
    }
    return (m);
}

void
SiftFeatureMatching::ConvertPvRGBImage(Image32 &imgIn, SiftFeatureMatching::Image imgOut)
{
  PVASSERT(imgIn.rows()==imgOut->rows && imgIn.cols()==imgOut->cols);

  unsigned int *imgInPtr = imgIn.pointer();
  unsigned int val;
  //for(int i=0;i<imgOut->rows*imgOut->cols;i++)    
  for(int i=0;i<imgOut->rows;i++)    
    for(int j=0;j<imgOut->cols;j++)    
    {
      val = imgInPtr[i*imgOut->cols+j];      
      val = (222*RED(val) + 707*GREEN(val) + 71*BLUE(val))/1000;	          
      imgOut->pixels[i][j] = (float)val/255.0f;
    }
}

void
SiftFeatureMatching::ConvertImage(SiftFeatureMatching::Image imgIn, Image32 &imgOut)
{
  PVASSERT(imgIn!=NULL);

  if(imgOut.rows()!=imgIn->rows || imgOut.cols() != imgIn->cols)
    imgOut.resize(imgIn->cols, imgIn->rows);
  
  unsigned int *imgOutPtr = imgOut.pointer();
  unsigned int val, p;
  
  for(unsigned int i=0;i<imgOut.rows();i++)    
    for(unsigned int j=0;j<imgOut.cols();j++)    
    {
     p = (unsigned int)(255.0f*(imgIn->pixels[i][j]));      
     val=p;
     val*=256;val+=p;
     val*=256;val+=p;
     imgOutPtr[i*imgOut.cols()+j] = val;
  }
}

/*---------------------- Read keypoint file ---------------------------*/


/* This reads a keypoint file from a given filename and returns the list
   of keypoints.
*/
void
SiftFeatureMatching::ReadKeyFile(const char *filename, std::list<SiftFeatureMatching::KeypointPtr> &featureList)
{
    FILE *file;

    file = fopen (filename, "rb");
    if (! file)
	PvUtil::exitError("Could not open file: %s", filename);

    ReadKeys(file, featureList);
}


/* Read keypoints from the given file pointer and return the list of
   keypoints.  The file format starts with 2 integers giving the total
   number of keypoints and the size of descriptor vector for each
   keypoint (currently assumed to be 128). Then each keypoint is
   specified by 4 floating point numbers giving subpixel row and
   column location, scale, and orientation (in radians from -PI to
   PI).  Then the descriptor vector for each keypoint is given as a
   list of integers in range [0,255].

   Modified by Yan Ke:  We assume int's instead of unsigned chars now
   for descriptor vector.

*/
void
SiftFeatureMatching::ReadKeys(FILE *fp, std::list<SiftFeatureMatching::KeypointPtr> &klist)
{
    int i, j, num, len, val;
    SiftFeatureMatching::KeypointPtr k, keys;

    klist.clear();

    if (fscanf(fp, "%d %d", &num, &len) != 2)
	PvUtil::exitError("Invalid keypoint file beginning.");

    if (len != 128) {
      //PvUtil::exitError("Keypoint descriptor length invalid (should be 128).");
	PCAKEYS = 1;
    } else
      PCAKEYS = 0;
    
    DLEN = len;

    for (i = 0; i < num; i++) {
      /* Allocate memory for the keypoint. */
      k.reset(new KeypointSt());
      //k->next = keys;
      //keys = k;            
      k->descrip.resize(len);
      k->id = i;
      if (fscanf(fp, "%f %f %f %f", &(k->row), &(k->col), &(k->scale),
        &(k->ori)) != 4)
        PvUtil::exitError("Invalid keypoint file format.");

      for (j = 0; j < len; j++) 
      {
        if (fscanf(fp, "%d", &val) != 1/* || val < 0 || val > 255 */)
          PvUtil::exitError("Invalid keypoint file value.");
        k->descrip[j] = val;
      }
      klist.push_back(k);
    }

    fclose(fp);

    //return keys;
}


/*----------------- Read and write PGM files ------------------------*/


/* This reads a PGM file from a given filename and returns the image.
*/
SiftFeatureMatching::Image 
SiftFeatureMatching::ReadPGM(const char *filename)
{
    FILE *file;

    /* The "b" option is for binary input, which is needed if this is
       compiled under Windows.  It has no effect in Linux.
    */
    file = fopen (filename, "rb");
    if (! file)
	PvUtil::exitError("Could not open file: %s", filename);

    return ReadPGM(file);
}


/* Read a PGM file from the given file pointer and return it as a
   float Image structure with pixels in the range [0,1].  If the file
   contains more than one image, then the images will be returned
   linked by the "next" field of the Image data structure.  
     See "man pgm" for details on PGM file format.  This handles only
   the usual 8-bit "raw" PGM format.  Use xv or the PNM tools (such as
   pnmdepth) to convert from other formats.
*/
SiftFeatureMatching::Image 
SiftFeatureMatching::ReadPGM(FILE *fp)
{
  int char1, char2, width, height, max, c1, c2, c3, r, c;
  SiftFeatureMatching::Image image, nextimage;

  char1 = fgetc(fp);
  char2 = fgetc(fp);
  SkipComments(fp);
  c1 = fscanf(fp, "%d", &width);
  SkipComments(fp);
  c2 = fscanf(fp, "%d", &height);
  SkipComments(fp);
  c3 = fscanf(fp, "%d", &max);

  if (char1 != 'P' || char2 != '5' || c1 != 1 || c2 != 1 || c3 != 1 ||
      max > 255)
    PvUtil::exitError("Input is not a standard raw 8-bit PGM file.\n"
	    "Use xv or pnmdepth to convert file to 8-bit PGM format.\n");

  fgetc(fp);  /* Discard exactly one byte after header. */

  /* Create floating point image with pixels in range [0,1]. */
  image = CreateImage(height, width);
  for (r = 0; r < height; r++)
    for (c = 0; c < width; c++)
      image->pixels[r][c] = ((float) fgetc(fp)) / 255.0f;

  /* Check if there is another image in this file, as the latest PGM
     standard allows for multiple images. */
  SkipComments(fp);
  if (getc(fp) == 'P') {
    ungetc('P', fp);
    nextimage = ReadPGM(fp);
    image->next = nextimage;
  }

  fclose(fp);

  return image;
}


/* PGM files allow a comment starting with '#' to end-of-line.  Skip
   white space including any comments.
*/
void 
SiftFeatureMatching::SkipComments(FILE *fp)
{
    int ch;

    fscanf(fp," ");      /* Skip white space. */
    while ((ch = fgetc(fp)) == '#') {
      while ((ch = fgetc(fp)) != '\n'  &&  ch != EOF)
	;
      fscanf(fp," ");
    }
    ungetc(ch, fp);      /* Replace last character read. */
}


/* Write an image to the file fp in PGM format.
*/
void 
SiftFeatureMatching::WritePGM(FILE *fp, SiftFeatureMatching::Image image)
{
    int r, c, val;

    fprintf(fp, "P5\n%d %d\n255\n", image->cols, image->rows);

    for (r = 0; r < image->rows; r++)
      for (c = 0; c < image->cols; c++) {
	val = (int) (255.0 * image->pixels[r][c]);
    fputc(std::max(0, std::min(255, val)), fp);
      }
}


void
SiftFeatureMatching::WritePGM(const char *filename, SiftFeatureMatching::Image image)
{
    FILE *file;

    /* The "b" option is for binary input, which is needed if this is
       compiled under Windows.  It has no effect in Linux.
    */
    file = fopen (filename, "wb");
    if (! file)
	PvUtil::exitError("Could not open file: %s", filename);

    WritePGM(file, image);

    fclose(file);

    return;
}


void
SiftFeatureMatching::WritePGM(const char *filename, Image32 &image)
{
    FILE *file;

    /* The "b" option is for binary input, which is needed if this is
       compiled under Windows.  It has no effect in Linux.
    */
    file = fopen (filename, "wb");
    if (! file)
	PvUtil::exitError("Could not open file: %s", filename);

    WritePGM(file, image);
    
    fclose(file);

    return;
}

void 
SiftFeatureMatching::WritePGM(FILE *fp, Image32 &image)
{
    int val;

    fprintf(fp, "P5\n%d %d\n255\n", image.cols(), image.rows());

    unsigned int* imgPtr = image.pointer();

    for (unsigned int i = 0; i < image.rows()*image.cols(); i++)      
      {
	    //val = (int) (255.0 * image->pixels[r][c]);
        val = imgPtr[i];
        //Gray scale=Y=(222*Red+707*Green+71*Blue)/1000
        val = (222*RED(val) + 707*GREEN(val) + 71*BLUE(val))/1000;
        fputc(std::max(0, std::min(255, val)), fp);
      }

}

}; // namespace vision

}; // namespace ait



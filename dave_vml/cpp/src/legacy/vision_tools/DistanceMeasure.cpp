/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include "legacy/vision_tools/DistanceMeasure.hpp"
#include <assert.h>
#include "legacy/types/AitBaseTypes.hpp"

using namespace ait;


/*********************************************************************
 ** CONSTRUCTOR
 *********************************************************************/
DistanceMeasure::DistanceMeasure(void) : initializedFlag(false)
{
  lut0=new float [256];
  lut1=new float [256];
  lut2=new float [256];
  
  iLut0=new unsigned int [256];
  iLut1=new unsigned int [256];
  iLut2=new unsigned int [256];


}

/*********************************************************************
 ** DESTRUCTOR
 *********************************************************************/
DistanceMeasure::~DistanceMeasure(void)
{
  delete [] lut0;
  delete [] lut1;
  delete [] lut2;

  delete [] iLut0;
  delete [] iLut1;
  delete [] iLut2;
}


void DistanceMeasure::init(Vector3f &m, Vector3f &v)
{
  if(!isInitialized() || mean(0)!=m(0) || mean(1)!=m(1) || mean(2)!=m(2) ||
     var(0)!=v(0) || var(1)!=v(1) || var(2)!=v(2))
  {
    initializedFlag=true;

    mean=m;
    var=v;

    calculateLut(lut0,mean(0),var(0),1.0f);
    calculateLut(lut1,mean(1),var(1),1.0f);
    calculateLut(lut2,mean(2),var(2),65535.0f);

    calculateIntLut();
  }
}


void DistanceMeasure::calculateLut(float *lut,const float m,const float v,const float c)
{
  int i;
  float d,var=-0.5f/v/v;

  for(i=0;i<256;i++)
  {
    d=m-float(i);
    d=d*d*var;
    lut[i]=exp(d)*c;
  }
}


void DistanceMeasure::calculateIntLut(void)
{
  int i;
  for(i=0;i<256;i++)
  {
    iLut0[i]=(Uint)(lut0[i]*65535.0f);
    iLut1[i]=(Uint)(lut1[i]*65535.0f);
    iLut2[i]=(Uint)(lut2[i]);
  }
}

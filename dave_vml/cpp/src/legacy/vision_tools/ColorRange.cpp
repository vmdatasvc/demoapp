/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
#include "legacy/vision_tools/ColorRange.hpp"

namespace ait
{

void ColorRange::initRanges(void)
{
  int i;

  for(i=0;i<256;i++)
  {
    range(ICE,i)=PV_RGB(0,0,i);
    range(ICE,i+256)=PV_RGB(0,i,255);
    range(ICE,i+512)=PV_RGB(i,255,255);

    range(FIRE,i)=PV_RGB(i,0,0);
    range(FIRE,i+256)=PV_RGB(255,i,0);
    range(FIRE,i+512)=PV_RGB(255,255,i);

    range(JOY,i)=PV_RGB(i,0,0);
    range(JOY,i+256)=PV_RGB(255,0,i);
    range(JOY,i+512)=PV_RGB(255,i,255);
  }
}

} // namespace ait

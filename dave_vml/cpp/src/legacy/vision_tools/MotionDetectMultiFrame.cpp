/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "legacy/vision_tools/MotionDetectMultiFrame.hpp"

namespace ait 
{

MotionDetectMultiFrame::MotionDetectMultiFrame()
{
}

MotionDetectMultiFrame::~MotionDetectMultiFrame()
{
}

//
// OPERATIONS
//

void 
MotionDetectMultiFrame::update(const Image8& inputImage)
{
  Image8Ptr pImg(new Image8(inputImage));
  VarImagePtr pVarImg(new Image<float>(inputImage.width(),inputImage.height()));

  int i;
  
  if (mImageBuffer.size() == 0)
  {
    mSum.resize(inputImage.width(),inputImage.height());
    mSum.setAll(0);
    mVarSum.resize(inputImage.width(),inputImage.height());
    mVarSum.setAll(0);
    mMotionEnergy.resize(inputImage.width(),inputImage.height());
    mMotionEnergy.setAll(0);
  }
  if (mImageBuffer.size() < mNumFrames)
  {
    for (i = 0; i < pImg->size(); i++)
    {
      mSum(i) += inputImage(i);
    }

    if (mImageBuffer.size() == mNumFrames-1)
    {
      std::list<Image8Ptr>::iterator iImg;
      std::list<VarImagePtr>::iterator iVarImg;

      for (iImg = mImageBuffer.begin(), iVarImg = mVarBuffer.begin(); 
           iImg != mImageBuffer.end(); iImg++, iVarImg++)
      {
        for (i = 0; i < pImg->size(); i++)
        {
          const float v = ((float)(**iImg)(i) - ((float)mSum(i))/mNumFrames);
          (**iVarImg)(i) = v*v;
          mVarSum(i) += (**iVarImg)(i);
        }
      }

      // Add this image too
      for (i = 0; i < pImg->size(); i++)
      {
        const float v = ((float)(inputImage)(i) - ((float)mSum(i))/mNumFrames);
        (*pVarImg)(i) = v*v;
        mVarSum(i) += (*pVarImg)(i);
      }
    }
  }
  else
  {
    std::list<Image8Ptr>::iterator iImg = mImageBuffer.begin();
    std::list<VarImagePtr>::iterator iVarImg = mVarBuffer.begin();
  
    mMotionEnergy.setAll(0);

    for(i = 0; i < inputImage.size(); i++)
    {
      // Update accumulators.
      mVarSum(i) -= (**iVarImg)(i);
      mSum(i) -= (Int32)(**iImg)(i); 

      mSum(i) += inputImage(i);
      const float v1 = ((float)inputImage(i) - ((float)mSum(i))/mNumFrames);
      (*pVarImg)(i) = v1*v1;
      mVarSum(i) += (*pVarImg)(i);

      // Calculate the actual motion image.
      const float var2 = mVarSum(i)/mNumFrames;
      const float mean = ((float)mSum(i))/mNumFrames;
      const float diff = (float)inputImage(i) - mean;
      if(var2 > 4)
      {
        const float motion = diff*diff/var2;
        mMotionEnergy(i) = (Uint8)std::min(255.0f,motion*30);
      }
    }
  
    mImageBuffer.pop_front();
    mVarBuffer.pop_front();
  }

  mImageBuffer.push_back(pImg);
  mVarBuffer.push_back(pVarImg);
}

void 
MotionDetectMultiFrame::reset()
{
  mImageBuffer.clear();
}

//
// ACCESS
//

//
// INQUIRY
//

//
// ACCESS
//

//
// INQUIRY
//

}; // namespace ait


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include "legacy/vision_tools/ColorMotion.hpp"
#include <legacy/vision_tools/PvImageProc.hpp>
#include <assert.h>

#include <legacy/types/AitBaseTypes.hpp>

using namespace ait;

void ColorMotion::setParameters(unsigned int colMaskSize, unsigned int motMaskSize, float colorFrac, float motionFrac, float historyWeight0)
{ 
  cMaskSize=colMaskSize; 
  mMaskSize=motMaskSize; 
  pC=colorFrac; 
  pM=motionFrac; 

  // Only two values valid for optimization...
  //PVASSERT(historyWeight0 == 0 || historyWeight0 == 0.5);
  historyWeight = historyWeight0;

  parametersValid=true; 
}

void ColorMotion::segment(Image32 &colImage,Image32 &motImage,Image32 &motImagePrev, unsigned int x, unsigned int y,
                          unsigned int w, unsigned int h, Vector3f &mean, Vector3f &var)
{
  unsigned int iw=colImage.width(),ih=colImage.height();

  xPos=x;yPos=y;
  
  unsigned int imgRatio=iw/motImage.width();

  w=w-w%imgRatio;
  h=h-h%imgRatio;

  PVASSERT(x+w<=iw && y+h<=ih);

  resize(w,h);

  // Set up the history
  if (historyWeight != 0)
  {
    if (history.width() == 1)
    {
      history.resize(w,h);
      history.setAll(0);
    }
    else
    {
      history = (*this);
    }
  }

  dm.init(mean,var);

  cmType *dest=pointer();

  //if(cMaskSize>8) cMaskSize=8;
  //assert(cMaskSize<=8);

  unsigned char *data=(unsigned char *)colImage.pointer()+(x+y*iw)*4;
  unsigned char *dataEnd=data+(h*iw)*4;
  unsigned char *rowEnd;
  int regionWidth=w*4;
  int offset=(iw-w)*4;
  unsigned int rr,gg,bb,ii;

  while(data<dataEnd)
  {
    rowEnd=data+regionWidth;
    while(data<rowEnd)
    {      
      rr=data[R_IDX];
      gg=data[G_IDX];
      bb=data[B_IDX];
      ii=rr+gg+bb+1;
      rr=(rr<<8)/ii;
      gg=(gg<<8)/ii;
      bb=(ii-1)/3;

      *dest=dm.measureInt(rr,gg,bb)*2;
      //if(*dest==0) *dest=0xffff;

      dest++;
      //*(dest++)=dm.measureInt(data[R_IDX],data[G_IDX],data[B_IDX]);
      //*(dest++)=dm.measure(data[R_IDX],data[G_IDX],data[B_IDX]);
      data+=4;
    }
    data+=offset;
  }

  sumOperator(*this,cMaskSize);

  assert(w%imgRatio==0 && h%imgRatio==0);
  assert(fabs(float(imgRatio)-(float)iw/motImage.width())<0.00001);

  iw=motImage.width(),ih=motImage.height();

  int mx=x/imgRatio,my=y/imgRatio,mw=w/imgRatio,mh=h/imgRatio;

  Image<unsigned short> diffImage(mw,mh);

  //assert(mMaskSize<=4);

  unsigned short *diffDest=diffImage.pointer();

  short r,g,b,dr,dg,db,d0;
  unsigned char *dataPrev=(unsigned char *)motImagePrev.pointer()+(mx+my*iw)*4;
  unsigned int line=mw*4;

  data=(unsigned char *)motImage.pointer()+(mx+my*iw)*4;
  dataEnd=data+(mh*iw)*4;
  regionWidth=mw*4;
  offset=(iw-mw)*4;
  
  //Matrix<short> diff(mh,mw);
  //Image32 img0(mw,mh),img1(mw,mh);

  unsigned int  i=0,xp=0,yp=0;
  while(data<dataEnd)
  {
    rowEnd=data+regionWidth;
    while(data<rowEnd)
    {
      r=(short)data[R_IDX];
      g=(short)data[G_IDX];
      b=(short)data[B_IDX];
      dr=r-(short)dataPrev[R_IDX];
      dg=g-(short)dataPrev[G_IDX];
      db=b-(short)dataPrev[B_IDX];
      d0=abs(dr)+abs(dg)+abs(db);
      //img0(i)=*((unsigned int *)(data));
      //img1(i)=*((unsigned int *)(dataPrev));
      //diff(i)=d0;
      *(diffDest++)=d0;
      data+=4;
      dataPrev+=4;
      i++;
      //colImage(xp,yp)=PV_RGB(abs(dr)*4,abs(dg)*4,abs(db)*4);
      xp++;
    }
    yp++;xp=0;
    data+=offset;
    dataPrev+=offset;
  }

  //diffImage.save("diffImage.m","%3d ");
  //((BasicMatrix<unsigned int>)img0).save("img0.png","%xd ");
  //((BasicMatrix<unsigned int>)img1).save("img1.png","%xd ");

  sumOperator(diffImage,mMaskSize);

  const bool integer=false;

  if(!integer)
  {
    float motFactor=65535.0f/(float(motionOneStdDev)*(2.0f*mMaskSize+1.0f)*(2.0f*mMaskSize+1.0f));
    float colFactor=1.0f/((2.0f*cMaskSize+1.0f)*(2.0f*cMaskSize+1.0f));
    
    int shift=PvUtil::intLog2(imgRatio);
    unsigned int xp,yp;
    float p,pc,pm0,pm1,C=pC,M=pM,CM=(1.0f-C-M);
    //float p,pc,pm,C=1.0f,M=1.0f,CM=(1.0f-C-M);
    
    CM=CM*motFactor*colFactor/65535.0f;
    M= M*motFactor;
    C= C*colFactor;
    
    for(yp=0;yp<h;yp+=2)
    {
      for(xp=0;xp<w;xp+=2)
      {
        pm0=diffImage(xp>>shift,yp>>shift);
        pm1=CM*pm0;
        pm0=M*pm0;
        
        pc=(float)mat[yp][xp];
        p=pc*pm1+C*pc+pm0;
        mat[yp][xp]=(unsigned int)(p>65535.0 ? 65535 : p);
        
        pc=(float)mat[yp][xp+1];
        p=pc*pm1+C*pc+pm0;
        mat[yp][xp+1]=(unsigned int)(p>65535.0 ? 65535 : p);
        
        pc=(float)mat[yp+1][xp];
        p=pc*pm1+C*pc+pm0;
        mat[yp+1][xp]=(unsigned int)(p>65535.0 ? 65535 : p);
        
        pc=(float)mat[yp+1][xp+1];
        p=pc*pm1+C*pc+pm0;
        mat[yp+1][xp+1]=(unsigned int)(p>65535.0 ? 65535 : p);
      }
    }
  }
  else
  {
    int shift=PvUtil::intLog2(imgRatio);
    unsigned int xp,yp;
    float C=pC,M=pM,CM=(1.0f-C-M);

    int m,m2;
    int mn=20*(2*mMaskSize+1)*(2*mMaskSize+1);
    int c;
    int cn=((2*cMaskSize+1)*(2*cMaskSize+1)*256)/6;
    int fcm=int(CM*65535.0);
    int fc=int(C*65535.0);
    int fm=int(M*65535.0);
    int p;

    for(yp=0;yp<h;yp+=2)
    {
      for(xp=0;xp<w;xp+=2)
      {
        m=(int)diffImage(xp>>shift,yp>>shift);
        m=(m<<8)/mn;
        if(m>0xffff) m=0xffff;
        m2=(fm*m)>>16;
  
        c=mat[yp][xp];
        c=c/cn;
        if(c>0xffff) c=0xffff;
        p=((fcm*((c*m)>>16))>>16)+((fc*c)>>16)+m2;
        mat[yp][xp]=p;

        c=mat[yp+1][xp];
        c=c/cn;
        if(c>0xffff) c=0xffff;
        p=((fcm*((c*m)>>16))>>16)+((fc*c)>>16)+m2;
        mat[yp+1][xp]=p;

        c=mat[yp][xp+1];
        c=c/cn;
        if(c>0xffff) c=0xffff;
        p=((fcm*((c*m)>>16))>>16)+((fc*c)>>16)+m2;
        mat[yp][xp+1]=p;

        c=mat[yp+1][xp+1];
        c=c/cn;
        if(c>0xffff) c=0xffff;
        p=((fcm*((c*m)>>16))>>16)+((fc*c)>>16)+m2;
        mat[yp+1][xp+1]=p;

        /*
        pm1=CM*pm0;
        pm0=M*pm0;
        
        pc=mat[yp][xp];
        p=pc*pm1+C*pc+pm0;
        mat[yp][xp]=p>65535.0 ? 65535 : p;
        
        pc=mat[yp][xp+1];
        p=pc*pm1+C*pc+pm0;
        mat[yp][xp+1]=p>65535.0 ? 65535 : p;
        
        pc=mat[yp+1][xp];
        p=pc*pm1+C*pc+pm0;
        mat[yp+1][xp]=p>65535.0 ? 65535 : p;
        
        pc=mat[yp+1][xp+1];
        p=pc*pm1+C*pc+pm0;
        mat[yp+1][xp+1]=p>65535.0 ? 65535 : p;
        */
      }
    }
  }

  if (historyWeight != 0)
  {
    // historyWeight must be 0.5
    // Apply history weight
    int i = w*h;
    cmType *prev = history.pointer();
    cmType *current = pointer();

    /*
    while (i-- > 0)
    {
      if (*prev > 1) (*prev) -= 1;
      *current = (*prev) > (*current) ? (*prev) : (*current);
      current++; prev++;
    }
    */

    while (i-- > 0)
    {
      *current = (unsigned int)((cmType)((*prev)*historyWeight) + ((*current)*(1.0f-historyWeight)));
      current++; prev++;
    }

    /*
    while (i-- > 0)
    {
      *current = ((*prev)>>2) + ((*current)>>2);
      current++; prev++;
    }
    */
  }
  
  /*
  Image8 gray(w,h);

  for(int i=0;i<w*h;i++) gray(i)=(*this)(i)>>6;

  gray.save("seg.pgm");
  */

  //save("colorMotion.m","%5d ");
}

void ColorMotion::visualize(Image32 &frame, ColorRange &range)
{
  int xp,yp;
  int c;
  int x0=(int)xOffset(),y0=(int)yOffset();
  int w=width(),h=height();
  
  for(yp=0;yp<h;yp++)
  {
    for(xp=0;xp<w;xp++)
    {
      c=mat[yp][xp]*767/65535;
      //frame(x0+xp,y0+yp)=PvImageProc::sumRGB(frame(x0+xp,y0+yp),range(c>767 ? 767 : c));       
      frame(x0+xp,y0+yp)=range(c>767 ? 767 : c);       
    }
  }      
}

void ColorMotion::refinePosition(Vector2f &pos, unsigned int w, unsigned int h)
{
  unsigned int x0=(int)(pos(0)-w/2-xPos),y0=(int)(pos(1)-h/2-yPos);
  unsigned int x1=x0+w-1,y1=y0+h-1;

  // refine window

  if(w>width() || h>height()) 
    PvUtil::exitError("ColorMotion::refinePosition(): Error, size too large!");

  if(x0<0) { x0=0; x1=x0+w-1;}
  if(y0<0) { y0=0; y1=y0+h-1;}
  if(x1<0 || y1<0) 
    PvUtil::exitError("ColorMotion::refinePosition(): Error, pos out of bounds!");

  if(x1>=width())  { x1=width()-1 ;x0=x1-w+1; }
  if(y1>=height()) { y1=height()-1;y0=y1-h+1; }

  if(x0>=width() || y0>=height()) 
    PvUtil::exitError("ColorMotion::refinePosition(): Error, pos out of bounds!");

  // perform refinement

  unsigned int c,x,y;
  // Changed by ARC: to use the Ait Types
  Int64 cc=0,xp=0,yp=0;
  //unsigned long xp2=0,yp2=0;

  for(y=y0;y<=y1;y++)
  {
    for(x=x0;x<=x1;x++)
    {
      c=mat[y][x];
      cc+=c;
      xp+=x*c;
      yp+=y*c;
      //xp2+=c*x*x;
      //yp2+=c*y*y;
    }
  }

  pos(0)=(float)xp/cc;
  pos(1)=(float)yp/cc;

  pos(0)+=xPos;
  pos(1)+=yPos;
}

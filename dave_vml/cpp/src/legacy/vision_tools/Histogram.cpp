/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#pragma warning(disable: 4503)
#endif

#include "legacy/vision_tools/Histogram.hpp"

namespace ait
{

Histogram::Histogram() 
{ 
}

Histogram& 
Histogram::operator=(const Histogram& from)
{
  if (this != &from)
  {
    mNumBins = from.mNumBins;
    mIsMultidimensional = from.mIsMultidimensional;
    mNumSamples = from.mNumSamples;
    Matrix<Uint>::operator=(from);
  }
  return *this;
}

void 
Histogram::allocate(Uint numChannels, Uint numBins)
{
  resize(numChannels, numBins);
  mNumBins.resize(numChannels);
  mNumBins.setAll(numBins);
  mIsMultidimensional = 0;
}

void 
Histogram::allocate(const Matrix<Uint>& numBins, Bool isMultidimensional)
{
  mNumBins = numBins;
  mIsMultidimensional = isMultidimensional;
  if (mIsMultidimensional)
  {
    Uint numElems = 1, i;
    for (i = 0; i < mNumBins.size(); i++) numElems *= mNumBins(i);
    resize(numElems);
  }
  else
  {
    if (mNumBins.minimum() == mNumBins.maximum())
    {
      resize(numBins.size(),numBins(0));
    }
    else
    {
      Uint numElems = 1, i;
      for (i = 0; i < mNumBins.size(); i++) numElems += mNumBins(i);
      resize(numElems);
    }
  }
}

void 
Histogram::compute(const Image32& img, Rectanglei roi)
{
  PVASSERT(getNumChannels() == 3);
  PVASSERT(Rectanglei(0,0,img.width(),img.height()).inside(roi));

  if (roi.width() == 0)
  {
    roi.x1 = img.width();
    roi.y1 = img.height();
  }

  unsigned int i;
  for (i = 0; i < mNumBins.size(); i++)
  {
    if ((256 % mNumBins(i)) != 0) PvUtil::exitError("Histogram Size must be a divisor of 256 for Image32");
  }

  int numPixelsX = roi.width();
  int numPixelsY = roi.height();
  int lineSkip = img.width()-numPixelsX;

  mNumSamples = numPixelsX*numPixelsY;

  const Uint slotSpan0 = 256/mNumBins(0);
  const Uint slotSpan1 = 256/mNumBins(1);
  const Uint slotSpan2 = 256/mNumBins(2);

  Uint8 *pData = (Uint8 *)img.pointer();
  pData += 4*(roi.y0*img.width()+roi.x0);

  zero();

  if (!mIsMultidimensional)
  {
    Uint *pHisto1 = pointer();
    Uint *pHisto2 = pHisto1+mNumBins(0);
    Uint *pHisto3 = pHisto2+mNumBins(1);

    while (numPixelsY-- > 0)
    {
      while (numPixelsX-- > 0)
      {
        pHisto1[pData[R_IDX]/slotSpan0]++;
        pHisto2[pData[G_IDX]/slotSpan1]++;
        pHisto3[pData[B_IDX]/slotSpan2]++;
        pData += 4;
      }
      numPixelsX = roi.width();
      pData += lineSkip*4;
    }
  }
  else
  {
    Uint *pHisto = pointer();
    const Uint step0 = mNumBins(1)*mNumBins(2);
    const Uint step1 = mNumBins(2);

    while (numPixelsY-- > 0)
    {
      while (numPixelsX-- > 0)
      {
        const Uint idx = (static_cast<Uint>(pData[R_IDX])/slotSpan0)*step0 + 
                         (static_cast<Uint>(pData[G_IDX])/slotSpan1)*step1 +
                         (static_cast<Uint>(pData[B_IDX])/slotSpan2);
        PVASSERT(idx<size());
        pHisto[idx]++;
        pData += 4;
      }
      numPixelsX = roi.width();
      pData += lineSkip*4;
    }
  }
}

void 
Histogram::compute(const Image8& img, Rectanglei roi)
{
  PVASSERT(getNumChannels() == 1);
  PVASSERT(Rectanglei(0,0,img.width(),img.height()).inside(roi));

  if (roi.width() == 0)
  {
    roi.x1 = img.width();
    roi.y1 = img.height();
  }

  unsigned int i;
  for (i = 0; i < mNumBins.size(); i++)
  {
    if ((256 % mNumBins(i)) != 0) PvUtil::exitError("Histogram Size must be a divisor of 256 for Image32");
  }

  int numPixelsX = roi.width();
  int numPixelsY = roi.height();
  int lineSkip = img.width()-numPixelsX;

  mNumSamples = numPixelsX*numPixelsY;

  const Uint slotSpan0 = 256/mNumBins(0);

  unsigned char *pData = (unsigned char *)img.pointer();
  //pData += 4*(roi.y0*img.width()+roi.x0);
  pData += roi.y0*img.width()+roi.x0;

  zero();

  Uint *pHisto1 = pointer();

  while (numPixelsY-- > 0)
  {
    while (numPixelsX-- > 0)
    {
			pHisto1[*pData/slotSpan0]++;
      pData++;
    }
    numPixelsX = roi.width();
    pData += lineSkip;
  }  
}

void 
Histogram::compute(const Image<Uint>& img, Rectanglei roi)
{
  PVASSERT(Rectanglei(0,0,img.width(),img.height()).inside(roi));

  if (roi.width() == 0)
  {
    roi.x1 = img.width();
    roi.y1 = img.height();
  }

  int numPixelsX = roi.width();
  int numPixelsY = roi.height();
  int lineSkip = img.width()-numPixelsX;

  mNumSamples = numPixelsX*numPixelsY;

  Uint *pData = img.pointer();
  pData += (roi.y0*img.width()+roi.x0);

  zero();

  while (numPixelsY-- > 0)
  {
    while (numPixelsX-- > 0)
    {
      ((*this)(*pData++))++;
    }
    numPixelsX = roi.width();
    pData += lineSkip;
  }
}

void 
Histogram::computeIndices(const Image32& img, 
                          Image<Uint>& indexImage, 
                          Rectanglei roi)
{
  PVASSERT(getNumChannels() == 3);
  PVASSERT(Rectanglei(0,0,img.width(),img.height()).inside(roi));

  if (roi.width() == 0)
  {
    roi.x1 = img.width();
    roi.y1 = img.height();
  }

  unsigned int i;
  for (i = 0; i < mNumBins.size(); i++)
  {
    if ((256 % mNumBins(i)) != 0) PvUtil::exitError("Histogram Size must be a divisor of 256 for Image32");
  }

  int numPixelsX = roi.width();
  int numPixelsY = roi.height();
  int lineSkip = img.width()-numPixelsX;

  indexImage.resize(numPixelsX,numPixelsY);

  mNumSamples = numPixelsX*numPixelsY;
  int numPixel = 0;

  const Uint slotSpan0 = 256/mNumBins(0);
  const Uint slotSpan1 = 256/mNumBins(1);
  const Uint slotSpan2 = 256/mNumBins(2);

  Uint8 *pData = (Uint8 *)img.pointer();
  pData += 4*(roi.y0*img.width()+roi.x0);

  if (!mIsMultidimensional)
  {
    PvUtil::exitError("Histogram::computeIndices only works with multidimensional histograms");
  }
  else
  {
    Uint *pHisto = pointer();
    const Uint step0 = mNumBins(1)*mNumBins(2);
    const Uint step1 = mNumBins(2);

    while (numPixelsY-- > 0)
    {
      while (numPixelsX-- > 0)
      {
        const Uint idx = (static_cast<Uint>(pData[R_IDX])/slotSpan0)*step0 + 
                         (static_cast<Uint>(pData[G_IDX])/slotSpan1)*step1 +
                         (static_cast<Uint>(pData[B_IDX])/slotSpan2);
        PVASSERT(idx<size());
        indexImage(numPixel++) = idx;
        pData += 4;
      }
      numPixelsX = roi.width();
      pData += lineSkip*4;
    }
  }
}

void 
Histogram::getMax(Matrix<Uint>& vMax)
{
  vMax.resize(2,3);
  vMax.zero();

  Uint c,i;

  for (c = 0; c < getNumChannels(); c++)
  {
    for (i = 0; i < getNumBins()(c); i++)
    {
      if (vMax(0,c) < (*this)(c,i)) 
      {
        vMax(0,c) = (*this)(c,i);
        vMax(1,c) = i;
      }
    }
  }
}


void 
Histogram::getWeightedCenter(Matrix<Uint>& vMax)
{
  vMax.resize(1,3);
  vMax.zero();

  Uint c,i;
  Uint sum=0;
  Uint idx=0;

  for (c = 0; c < getNumChannels(); c++)
  {
    for (i = 0; i < getNumBins()(c); i++)
    {
      sum+=(*this)(c,i);
      idx+=(*this)(c,i)*i;
    }
    idx/=sum;
    vMax(c)=idx;
  }
}

void 
Histogram::normalize(Uint newNumSamples)
{
  // There might be errors because of roundings.
  Uint remainingSamples = newNumSamples*(mIsMultidimensional?1:getNumChannels());
  unsigned int i;
  Double carry = 0;
  for (i = 0; i < size(); i++)
  {
    
    const Double newValf = static_cast<Double>((*this)(i))*newNumSamples/mNumSamples;
    Uint newVal = static_cast<Uint>(newValf);
    carry += newValf-newVal;
    if (carry > 1) 
    {
      newVal++;
      carry-=1;
    }
    (*this)(i) = newVal;
    remainingSamples -= newVal;
  }

  // There could be one sample missing.
  if (remainingSamples != 0)
  {
    // just increment the sample count in the middle bin.
    ((*this)(size()/2))++;
  }

  mNumSamples = newNumSamples;
}

void 
Histogram::plot(Image32& img, Uint h, Uint stepWidth, Uint maxValue)
{
  PVASSERT(!mIsMultidimensional);
  //PVASSERT(size()/stepWidth < img.width());

  Uint iMax = maxValue ? maxValue : maximum();

  Uint c,i,j,idx = 0;
  Uint x = 0;

  for (c = 0; c < getNumChannels(); c++)
  {
    Uint col = !c ? PV_RGB(255,0,0) : (c == 1 ? PV_RGB(0,255,0) : PV_RGB(0,0,255));
    img.vline(x, 0, h, PV_RGB(255,255,255));
    for (i = 0; i < getNumBins()(c); i++)
    {
      Uint val = (int)(((double)(*this)(idx++))*h/iMax);
      if (val > h) val = h;
      for (j = 0; j < (Uint)stepWidth; j++) img.vline(x++, h-val, val, col);
    }
  }
  img.vline(x, 0, h, PV_RGB(255,255,255));
}

Double 
Histogram::compare(const Histogram& lhs, const Histogram& rhs, CompareMethodLabel method)
{
  PVASSERT(lhs.getNumSamples() == rhs.getNumSamples());
  PVASSERT(lhs.getNumChannels() == rhs.getNumChannels());
  PVASSERT(lhs.getNumBins() == rhs.getNumBins());
  PVASSERT(lhs.isMultidimensional() == rhs.isMultidimensional());

  switch (method)
  {
  case ABSOLUTE_DIFFERENCE:
    {
      Double sum = 0;
      Uint size = rhs.size();
      Uint *p1 = rhs.pointer();
      Uint *p2 = lhs.pointer();
      while (size-- > 0)
      {
        sum += fabs(static_cast<Double>(*p1++) - static_cast<Double>(*p2++));
      }
      return sum/rhs.getNumSamples();
    }
    break;
  case SQUARE_DISTANCE:
    {
      Double sum = 0;
      Uint size = rhs.size();
      Uint *p1 = rhs.pointer();
      Uint *p2 = lhs.pointer();
      while (size-- > 0)
      {
        const Double val = static_cast<Double>(*p1++) - static_cast<Double>(*p2++);
        sum += val*val;
      }
      return sum/rhs.getNumSamples();
    }
    break;
  case INTERSECTION:
    {
      Double sum = 0;
      Uint size = rhs.size();
      Uint *p1 = rhs.pointer();
      Uint *p2 = lhs.pointer();
      while (size-- > 0)
      {
        const Double val = std::min(*p1++,*p2++);
        sum += val;
      }
      return sum/rhs.getNumSamples();
    }
    break;
  case EARTH_MOVER_DISTANCE:
    {
      PvUtil::exitError("EARTH_MOVER_DISTANCE is not implemented");
    }
    break;
  }

  return 0;
}

//
// ACCESS
//

//
// INQUIRY
//


}; // namespace ait


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "legacy/vision_tools/BinaryClassifier.hpp"

namespace ait 
{

namespace vision
{

BinaryClassifier::BinaryClassifier()
{
}

BinaryClassifier::~BinaryClassifier()
{
}

//
// OPERATIONS
//

//
// ACCESS
//

//
// INQUIRY
//

}; // namespace vision

}; // namespace ait


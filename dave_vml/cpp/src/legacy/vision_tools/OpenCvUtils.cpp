/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "OpenCvUtils.hpp"

namespace ait 
{

namespace vision
{

IplImage *
OpenCvUtils::convertPvImageToIpl(const Image32& img)
{
  CvSize size;
  size.width = img.width();
  size.height = img.height();

  IplImage *iplImage = cvCreateImage(size,8,3);
  OpenCvUtils::copyPvImageToIpl(img,iplImage);
  return iplImage;
}

void 
OpenCvUtils::copyPvImageToIpl(const Image32& img, IplImage *pIplImage)
{

  int w = img.width();
  int h = img.height();

  PVASSERT(pIplImage->nChannels == 3);
  PVASSERT(pIplImage->depth == 8);

  unsigned char *pIplData;
  int iplStep;
  cvGetRawData(pIplImage,&pIplData,&iplStep);

  for(int y=0; y<h; y++)
  {
    unsigned char *pSrcData = ((unsigned char *)img.pointer()) + y*w*4;
    unsigned char *pDstData = ((unsigned char *)pIplData) + y*iplStep;
    for(int x=0; x<w; x++)
    {
      pDstData[0] = pSrcData[2];
      pDstData[1] = pSrcData[1];
      pDstData[2] = pSrcData[0];
      pDstData += 3;
      pSrcData += 4;
    }
  }
}

void 
OpenCvUtils::copyIplToPvImage(IplImage *pIplImage, Image32& img)
{
  img.resize(pIplImage->width,pIplImage->height);

  int w = img.width();
  int h = img.height();

  PVASSERT(pIplImage->nChannels == 3);
  PVASSERT(pIplImage->depth == 8);

  unsigned char *pIplData;
  int iplStep;
  cvGetRawData(pIplImage,&pIplData,&iplStep);

  for(int y=0; y<h; y++)
  {
    unsigned char *pDstData = ((unsigned char *)img.pointer()) + y*w*4;
    unsigned char *pSrcData = ((unsigned char *)pIplData) + y*iplStep;
    for(int x=0; x<w; x++)
    {
      pDstData[0] = pSrcData[2];
      pDstData[1] = pSrcData[1];
      pDstData[2] = pSrcData[0];
      pDstData += 4;
      pSrcData += 3;
    }
  }
}

IplImage *
OpenCvUtils::convertPvImageToIpl(const Image8& img)
{
  CvSize size;
  size.width = img.width();
  size.height = img.height();

  IplImage *iplImage = cvCreateImage(size,8,1);
  OpenCvUtils::copyPvImageToIpl(img,iplImage);
  return iplImage;
}

void 
OpenCvUtils::copyPvImageToIpl(const Image8& img, IplImage *pIplImage)
{

  int w = img.width();
  int h = img.height();

  PVASSERT(pIplImage->nChannels == 1);
  PVASSERT(pIplImage->depth == 8);

  unsigned char *pIplData;
  int iplStep;
  cvGetRawData(pIplImage,&pIplData,&iplStep);

  for(int y=0; y<h; y++)
  {
    unsigned char *pSrcData = ((unsigned char *)img.pointer()) + y*w;
    unsigned char *pDstData = ((unsigned char *)pIplData) + y*iplStep;
    for(int x=0; x<w; x++)
    {
      pDstData[x] = pSrcData[x];
    }
  }
}

void 
OpenCvUtils::copyIplToPvImage(IplImage *pIplImage, Image8& img)
{
  img.resize(pIplImage->width,pIplImage->height);

  int w = img.width();
  int h = img.height();

  PVASSERT(pIplImage->nChannels == 1);
  PVASSERT(pIplImage->depth == 8);

  unsigned char *pIplData;
  int iplStep;
  cvGetRawData(pIplImage,&pIplData,&iplStep);

  for(int y=0; y<h; y++)
  {
    unsigned char *pDstData = ((unsigned char *)img.pointer()) + y*w;
    unsigned char *pSrcData = ((unsigned char *)pIplData) + y*iplStep;
    for(int x=0; x<w; x++)
    {
      pDstData[x] = pSrcData[x];
    }
  }
}

}; // namespace vision

}; // namespace ait


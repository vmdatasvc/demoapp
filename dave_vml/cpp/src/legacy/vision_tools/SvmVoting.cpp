/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "legacy/vision_tools/SvmVoting.hpp"

namespace ait 
{

namespace vision
{

SvmVoting::SvmVoting()
: mResultLabel(ClassifierLabels::UNCLASSIFIED)
{
}

SvmVoting::~SvmVoting()
{
}

void
SvmVoting::init(SvmClassifierPtr pSvm, int maxVotingSamples)
{
  mpSvm = pSvm;
  mDistanceStats.init(maxVotingSamples,true);
}

//
// OPERATIONS
//

void
SvmVoting::calcResult()
{
  // Get a robust accumulated value from the SVM
  mMedianDistance = mDistanceStats.getMedian();
  
  if (mpSvm->isUnknown(mMedianDistance))
  {
    mResultLabel = ClassifierLabels::UNKNOWN;
  }
  else if (mMedianDistance < 0)
  {
    mResultLabel = mpSvm->getNegativeLabel();
  }
  else
  {
    mResultLabel = mpSvm->getPositiveLabel();
  }
}

void 
SvmVoting::addSample(const SvmClassifier::Result& r)
{
  mDistanceStats.addElement(r.distance);
  calcResult();
}

Label 
SvmVoting::getResult()
{
  return mResultLabel;
}

void 
SvmVoting::merge(const SvmVoting& from)
{
  PVASSERT(mpSvm == from.mpSvm);

  for (int i = 0; i < from.mDistanceStats.getNumSamples(); i++)
  {
    mDistanceStats.addElement(from.mDistanceStats.getSample(i));
  }

  calcResult();
}

//
// ACCESS
//

//
// INQUIRY
//

}; // namespace vision

}; // namespace ait


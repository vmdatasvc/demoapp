/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#pragma warning(disable:4503)
#endif

#include <legacy/vision_tools/ColorConvert.hpp>
#include <legacy/pv/PvUtil.hpp>
#include <legacy/vision_tools/image_proc.hpp>
//#include <Settings.hpp>

#ifdef ICL_OPTIMIZATION
#include <fvec.h>
#endif


#include "legacy/vision_tools/BackgroundSub.hpp"

namespace ait
{

BackgroundSub::BackgroundSub()
{
}

BackgroundSub::~BackgroundSub()
{
}

void 
BackgroundSub::init(Settings &set)
{
  //PVMSG("BackgroundSub Init\n");
  
  Settings mSettings(set);	// need copy constructor
  
  mInitNumFrames = mSettings.getInt("initNumFrames",30);
  mNumBackgroundFrames = mSettings.getInt("numberOfBackgroundFrames", 120);
  mMagnificationFactor = mSettings.getFloat("foregroundMagnificationFactor", 2.0);
  mModelUpdateInterval = mSettings.getInt("modelUpdateInterval",1);
  mBackgroundLearnThreshold = mSettings.getInt("backgroundLearnThreshold",3);
  mForegroundPixelThreshold = mSettings.getFloat("foregroundPixelThreshold", 0.02f);
  mMinForegroundThreshold = mSettings.getFloat("minForegroundThreshold",32.0f);
  mForegroundPercentage = 0.0;
//	mInitNumFrames = 30;
//	mNumBackgroundFrames = 120;
//	mMagnificationFactor = 2.0;
//	mModelUpdateInterval = 1;
//	mBackgroundLearnThreshold = 3;
//	mForegroundPixelThreshold = 0.02f;
//	mMinForegroundThreshold = 32.f;
//	mForegroundPercentage = 0.f;
  
//  bool resetBackground = mSettings.getBool("resetBackground [bool]",0);
//  mSettings.setBool("resetBackground [bool]",0);
	bool resetBackground = false;
  
  if (resetBackground)
  {
    resetValid();
  }
  else
  {
    reset();
  }
}

void 
BackgroundSub::reset()
{
  mpBuildModel.reset(new BackgroundModel());
  mModelUpdated = false;
  mModelUpdateCounter = 0;
  mIsForegroundPresent = false;
  mForegroundPercentage = 0.0;
}

void 
BackgroundSub::backgroundInitialize(const Image32 &inputImage, BackgroundModelPtr &mpBackgroundModel)
{
  int x,y,w,h;
  
  w = inputImage.width();
  h = inputImage.height();
  
  BackgroundModel& bg = *mpBackgroundModel;

#ifdef ICL_OPTIMIZATION
  if (bg.stats.size() > 1)
  {
    _mm_free(bg.stats.pointer());
  }
  bg.stats.mx_fromarray((PixelStats *)_mm_malloc(w*h*sizeof(PixelStats),16),w,h);
#else
  bg.stats.resize(w,h);
#endif
  
  for (y=0; y<h; y++)
  {
    for(x=0; x<w; x++)
    {
      PixelStats& s = bg.stats(x,y);
      unsigned int px = inputImage(x,y);
      s.ch1Mean = (float)RED(px);
      s.ch1Var = 144;
      
      s.ch2Mean = (float)GREEN(px);
      s.ch2Var = 144;
      
      s.ch3Mean = (float)BLUE(px);
      s.ch3Var = 144;
      
      s.dummy0 = 0;
      s.dummy1 = 1;
    }

  }
  
  bg.numFrames = 1;
}

void 
BackgroundSub::backgroundInitialize(const Image8 &inputImage, BackgroundModelPtr &mpBackgroundModel)
{
  int x,y,w,h;
  
  w = inputImage.width();
  h = inputImage.height();
  
  BackgroundModel& bg = *mpBackgroundModel;

#ifdef ICL_OPTIMIZATION
  if (bg.stats.size() > 1)
  {
    _mm_free(bg.stats.pointer());
  }
  bg.stats.mx_fromarray((PixelStats *)_mm_malloc(w*h*sizeof(PixelStats),16),w,h);
#else
  bg.stats.resize(w,h);
#endif
  
  for (y=0; y<h; y++)
  {
    for(x=0; x<w; x++)
    {
      PixelStats& s = bg.stats(x,y);
      Uint8 px = inputImage(x,y);
      s.ch1Mean = px;
      s.ch1Var = 144;
    }

  }
  
  bg.numFrames = 1;
}

void 
BackgroundSub::backgroundModel(const Image32 &inputImage, BackgroundModelPtr &mpBackgroundModel)
{
  int x,y,w,h;
  int extraPixelCount = 0;
  
  w = inputImage.width();
  h = inputImage.height();
  
  const Image32& frame = inputImage;
  BackgroundModel& bg = *mpBackgroundModel;
  
  const float roh = 0.2f; //higher value of roh makes the Background Model adapt faster.
  
    for(y=0; y<h; y++)
    {
      for(x=0; x<w; x++)
      {
        const Uint32 pixel = frame(x,y);
        PixelStats& s = bg.stats(x,y);
        
        const float r = (float)RED(pixel);
        const float g = (float)GREEN(pixel);
        const float b = (float)BLUE(pixel);
        
        const float mean1 = s.ch1Mean;
        const float var1 = s.ch1Var;
        const float dr = fabs(r - mean1);
        
        const float mean2 = s.ch2Mean;
        const float var2 = s.ch2Var;
        const float dg = fabs(g - mean2);
        
        const float mean3 = s.ch3Mean;
        const float var3 = s.ch3Var;
        const float db = fabs(b - mean3);
        
        if (dr < mBackgroundLearnThreshold * var1 &&
            dg < mBackgroundLearnThreshold * var2 &&
            db < mBackgroundLearnThreshold * var3)
        {
          s.ch1Var = std::max(1.0f,var1 * (1-roh) + roh * dr*dr);
          s.ch1Mean = ((1-roh) * mean1 + roh * r);
          s.ch2Var = std::max(1.0f,var2 * (1-roh) + roh * dg*dg);
          s.ch2Mean = ((1-roh) * mean2 + roh * g);
          s.ch3Var = std::max(1.0f,var3 * (1-roh) + roh * db*db);
          s.ch3Mean = ((1-roh) * mean3 + roh * b);
        }
      }
    }
    
    bg.numFrames++;
  
}

void 
BackgroundSub::backgroundModel(const Image8 &inputImage, BackgroundModelPtr &mpBackgroundModel)
{
  int x,y,w,h;
  int extraPixelCount = 0;
  
  w = inputImage.width();
  h = inputImage.height();
  
  const Image8& frame = inputImage;
  BackgroundModel& bg = *mpBackgroundModel;
  
  const float roh = 0.2f; //higher value of roh makes the Background Model adapt faster.
  
    for(y=0; y<h; y++)
    {
      for(x=0; x<w; x++)
      {
        const Uint8 pixel = frame(x,y);
        PixelStats& s = bg.stats(x,y);
        
        const float p = pixel;
        
        const float mean1 = s.ch1Mean;
        const float var1 = s.ch1Var;
        const float d = fabs(p - mean1);
        
        if (d < mBackgroundLearnThreshold * var1)
        {
          s.ch1Var = std::max(1.0f,var1 * (1-roh) + roh * d*d);
          s.ch1Mean = ((1-roh) * mean1 + roh * p);
        }
      }
    }
    
    bg.numFrames++;
}

void
BackgroundSub::resetValid()
{
  reset();
  mpValidModel.reset();
}

void 
BackgroundSub::foregroundSegment(const Image32 &inputImage, Image8 &foregroundImage)
{
  
  PVASSERT(mpValidModel.get());
  
  unsigned int x,y;
  unsigned int count = 0;
  float foregroundFraction;
  
  const Image32& frame = inputImage;
  
  BackgroundModel& bg = *mpValidModel;
  
  const float t2 = (float)(mBackgroundLearnThreshold*mBackgroundLearnThreshold);
  
#ifdef ICL_OPTIMIZATION
  F32vec4 t2_4(t2,t2,t2,t2);

  // This image will be vectorized by the compiler.
  Image<float> tmp;
  float *tmp1 = (float *)_mm_malloc(frame.width()*frame.height()*sizeof(float),16);
  tmp.mx_fromarray(tmp1,frame.height(),frame.width());
#endif
  
  mForegroundImage.resize(frame.width(), frame.height());
  mForegroundImage.setAll(0);
  
  for(y=0; y<frame.height(); y++)
  {
    for (x=0; x<frame.width(); x++)
    {
      const Uint32 pixel = frame(x,y);
      PixelStats& s = bg.stats(x,y);
#ifndef ICL_OPTIMIZATION
      const float dr = (RED(pixel) - s.ch1Mean);
      const float dg = (GREEN(pixel) - s.ch2Mean);
      const float db = (BLUE(pixel) - s.ch3Mean);
      const float dr2 = dr*dr;
      const float dg2 = dg*dg;
      const float db2 = db*db;
      
      if ((dr2 > t2*s.ch1Var) ||
        (dg2 > t2*s.ch2Var) ||
        (db2 > t2*s.ch3Var))
      {
        // Shriram's changes
        float val = mMagnificationFactor * sqrt((dr2 + dg2 + db2)/3);
        val = std::min(val, 255.0f);
        
        if (val < mMinForegroundThreshold) 
        {
          val = 0;
        }
        
        mForegroundImage(x,y) = static_cast<Uint8>(val);
        //if (mForegroundImage(x,y) < 32) mForegroundImage(x,y) = 0;
      }
#else
      F32vec4 px4(0,(float)BLUE(pixel),(float)GREEN(pixel),(float)RED(pixel));
      F32vec4 *pMean = (F32vec4 *)&s.ch1Mean;
      F32vec4 *pVar = (F32vec4 *)&s.ch1Var;
      F32vec4 d = px4 - *pMean;
      F32vec4 d2 = d*d;
      F32vec4 t4 = t2_4*(*pVar);
      
      F32vec4 add = select_gt(d2,t4,d2,F32vec4(0,0,0,0));
      float f = add_horizontal(add);
      tmp(x,y) = f > 0 ? add_horizontal(d2) : 0;
#endif
    }
  }

#ifdef ICL_OPTIMIZATION
  F32vec4 mag(mMagnificationFactor,mMagnificationFactor,mMagnificationFactor,mMagnificationFactor);
  int s = tmp.size();
  for (int i = 0; i < s; i+=4)
  {
    F32vec4 *pV = (F32vec4 *)&(tmp(i));
    //F32vec4 V(tmp(i+3),tmp(i+2),tmp(i+1),tmp(i));
    F32vec4 val = mag*sqrt((*pV)/F32vec4(3.0f,3.0f,3.0f,3.0f));
    F32vec4 v1 = select_lt(val,
      F32vec4(mMinForegroundThreshold,mMinForegroundThreshold,
              mMinForegroundThreshold,mMinForegroundThreshold),
      F32vec4(0,0,0,0),val);
    F32vec4 v = simd_min(v1,F32vec4(255,255,255,255));
    Uint8 *pImg = &mForegroundImage(i);
    pImg[0] = (Uint8)v[0];
    pImg[1] = (Uint8)v[1];
    pImg[2] = (Uint8)v[2];
    pImg[3] = (Uint8)v[3];
  }

  _mm_free(tmp1);
#endif

  for(y = 0; y < mForegroundImage.height(); y++)
  {
    for(x = 0; x < mForegroundImage.width(); x++)
    {
      if(mForegroundImage(x,y) > 0)
        count++;
    }
  }
  
  foregroundFraction = (float)(count)/(mForegroundImage.width() * mForegroundImage.height());

  //PVMSG("f = %f\n", foregroundFraction);
  mForegroundPercentage = foregroundFraction;
  mIsForegroundPresent = foregroundFraction > mForegroundPixelThreshold;
  
  image_proc::smoothImage(mForegroundImage, image_proc::SMOOTH_GAUSSIAN, 3);
}

void 
BackgroundSub::foregroundSegment(const Image8 &inputImage, Image8 &foregroundImage)
{
  PVASSERT(mpValidModel.get());
  
  unsigned int x,y;
  unsigned int count = 0;
  float foregroundFraction;
  
  const Image8& frame = inputImage;
  
  BackgroundModel& bg = *mpValidModel;
  
  const float t2 = (float)(mBackgroundLearnThreshold*mBackgroundLearnThreshold);
  
  mForegroundImage.resize(frame.width(), frame.height());
  mForegroundImage.setAll(0);
  
  for(y=0; y<frame.height(); y++)
  {
    for (x=0; x<frame.width(); x++)
    {
      const Uint8 pixel = frame(x,y);
      PixelStats& s = bg.stats(x,y);
      const float d = (pixel - s.ch1Mean);
      const float d2 = d*d;
      
      if (d2 > t2*s.ch1Var)
      {
        // Shriram's changes
        float val = mMagnificationFactor * fabs(d);
        val = std::min(val, 255.0f);
        
        if (val < mMinForegroundThreshold) 
        {
          val = 0;
        }
        
        mForegroundImage(x,y) = static_cast<Uint8>(val);
      }
    }
  }

  for(y = 0; y < mForegroundImage.height(); y++)
  {
    for(x = 0; x < mForegroundImage.width(); x++)
    {
      if(mForegroundImage(x,y) > 0)
        count++;
    }
  }
  
  foregroundFraction = (float)(count)/(mForegroundImage.width() * mForegroundImage.height());
  mForegroundPercentage = foregroundFraction;

  //PVMSG("f = %f\n", foregroundFraction);

  mIsForegroundPresent = foregroundFraction > mForegroundPixelThreshold;
  
  image_proc::smoothImage(mForegroundImage, image_proc::SMOOTH_GAUSSIAN, 3);
}

};


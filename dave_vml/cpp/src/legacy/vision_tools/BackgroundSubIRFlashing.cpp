/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "legacy/vision_tools/BackgroundSubIRFlashing.hpp"
#include <legacy/vision_tools/image_proc.hpp>

#include <list>

#define DEBUG_SAVE_FRAMES

namespace ait 
{

namespace vision
{

BackgroundSubIRFlashing::BackgroundSubIRFlashing()
: mIsInitialized(false),
  mIplMask(NULL),
  mIplTmpMask(NULL)
{
}

BackgroundSubIRFlashing::~BackgroundSubIRFlashing()
{
}

void 
BackgroundSubIRFlashing::init(int w, int h)
{
  // Initialize this object.
//  mSettings.changePath(settingsPath + "/InfraredSubtraction/");

//  mEnableRealTimeParamAdjust = mSettings.getBool("enableRealTimeParamAdjust [bool]",0);
	mEnableRealTimeParamAdjust = false;
//  mSaveFrameHistory = mSettings.getBool("saveFrameHistory [bool]",0);
	mSaveFrameHistory = false;
//  mInfraredThreshold = mSettings.getInt("infraredThreshold [int]",0);
	mInfraredThreshold = 0;
//  mInfraredMaskInAlpha = mSettings.getBool("infraredMaskInAlpha [bool]",0);
	mInfraredMaskInAlpha = false;
//  mInfraredDoThreshold = mSettings.getBool("infraredDoThreshold [bool]",0);
	mInfraredDoThreshold = false;
////  mInfraredVisualize = mSettings.getBool("infraredVisualize [bool]",0);
//  mHistogramStep = mSettings.getInt("histogramVisualizationStep [int]",0);
	mHistogramStep = 0;
//  mMeanShiftStartIdx = mSettings.getInt("meanShiftStartIdx [int]",1);
	mMeanShiftStartIdx = 1;
//  mFindThresholdPercentage = mSettings.getFloat("findThresholdPercentage [float]",0.8f);
	mFindThresholdPercentage = 0.8f;
//  mDoDialateAndErode = mSettings.getBool("doDialateAndErode [bool]",1);
	mDoDialateAndErode = true;
//  mMorphOpenKernelSize = mSettings.getInt("morphOpenKernelSize [int]",2);
	mMorphOpenKernelSize = 2;
//  mMorphOpenIterations = mSettings.getInt("morphOpenIterations [int]",1);
	mMorphOpenIterations = 1;
//  mMorphCloseKernelSize = mSettings.getInt("morphCloseKernelSize [int]",3);
	mMorphCloseKernelSize = 3;
//  mMorphCloseIterations = mSettings.getInt("morphCloseIterations [int]",1);
	mMorphCloseIterations = 1;
//  mMorphDilateKernelSize = mSettings.getInt("morphDilateKernelSize [int]",3);
	mMorphDilateKernelSize = 3;
//  mMorphDilateIterations = mSettings.getInt("morphDilateIterations [int]",1);
	mMorphDilateIterations = 1;
//  mDisplayHistogram = mSettings.getBool("displayHistogram [bool]",1);
	mDisplayHistogram = true;
//  mDisplayBlownHighlights = mSettings.getBool("displayBlownHighlights [bool]",0);
	mDisplayBlownHighlights = false;
//  mDisplayHistogramThresholds = mSettings.getBool("displayHistogramThresholds [bool]",0);
	mDisplayHistogramThresholds = false;
//  mBlownHighlightThreshold = mSettings.getInt("blownHighlightThreshold [225-255]",250);
	mBlownHighlightThreshold = 250;
//  mSmoothHistogram = mSettings.getBool("smoothHistogram [bool]",1);
	mSmoothHistogram = true;
//  mSmoothDiffImage = mSettings.getBool("smoothDiffImage [bool]",1);
	mSmoothDiffImage = true;
//  mSmoothHistogramKernelSize = mSettings.getInt("smoothHistogramKernelSize [3,5]",3);
	mSmoothHistogramKernelSize = 3;

  if(mSmoothHistogramKernelSize > 5) mSmoothHistogramKernelSize=5;
  else if(mSmoothHistogramKernelSize != 5) mSmoothHistogramKernelSize=3;

  if(mIplMask)
  {
    //cvReleaseImage(&mIplMask);
    //cvReleaseImage(&mIplTmpMask);
    cvReleaseImageHeader(&mIplMask);
    cvReleaseImageHeader(&mIplTmpMask);
    mIplMask=NULL;
    mIplTmpMask=NULL;

    cvReleaseStructuringElement(&mShapeOpenKernel);
    cvReleaseStructuringElement(&mShapeCloseKernel);
    cvReleaseStructuringElement(&mShapeDilateKernel);
  }

//  const Image32& src = mpDevice->getReferenceToFrame(0);
  mMaskSize.height = h;//src.height();
  mMaskSize.width = w;//src.width();
  mMask.resize(mMaskSize.height,mMaskSize.width);
  mTmpMask.resize(mMaskSize.height,mMaskSize.width);		

  if(mDoDialateAndErode && mIplMask==NULL)
  {							
    mIplMask = cvCreateImageHeader(mMaskSize, IPL_DEPTH_8U,1);
    mIplTmpMask = cvCreateImageHeader(mMaskSize, IPL_DEPTH_8U,1);
    cvSetData(mIplMask, mMask.pointer(), CV_AUTOSTEP);
    cvSetData(mIplTmpMask, mTmpMask.pointer(), CV_AUTOSTEP);

    mShapeOpenKernel = cvCreateStructuringElementEx(mMorphOpenKernelSize,mMorphOpenKernelSize, mMorphOpenKernelSize/2,mMorphOpenKernelSize/2, CV_SHAPE_CROSS, NULL);
    mShapeCloseKernel = cvCreateStructuringElementEx(mMorphCloseKernelSize,mMorphCloseKernelSize, mMorphCloseKernelSize/2,mMorphCloseKernelSize/2, CV_SHAPE_RECT, NULL);
    mShapeDilateKernel = cvCreateStructuringElementEx(mMorphDilateKernelSize,mMorphDilateKernelSize, mMorphDilateKernelSize/2,mMorphDilateKernelSize/2, CV_SHAPE_ELLIPSE, NULL);			

  }

  //initialize histogram (here: 256 bins)
  for(int i = 0; i < 256; i++)
    mHistogram[i] = 0;

  mIsInitialized = true;
}

void 
BackgroundSubIRFlashing::uninitialize()
{
  if(mIplMask)
  {
    //cvReleaseImage(&mIplMask);
    //cvReleaseImage(&mIplTmpMask);
    cvReleaseImageHeader(&mIplMask);
    cvReleaseImageHeader(&mIplTmpMask);
    mIplMask=NULL;
    mIplTmpMask=NULL;

    cvReleaseStructuringElement(&mShapeOpenKernel);
    cvReleaseStructuringElement(&mShapeCloseKernel);
    cvReleaseStructuringElement(&mShapeDilateKernel);
  }
}
//
// OPERATIONS
//

void
BackgroundSubIRFlashing::process(Image32Ptr pImage1, Image32Ptr pImage2, FrameHistory& fh)
{
  /* Process Frames */

  if(mEnableRealTimeParamAdjust)
  {
    mSaveFrameHistory = mSettings.getBool("saveFrameHistory [bool]",0);  
    mDisplayHistogram = mSettings.getBool("displayHistogram [bool]",1);
    mDisplayHistogramThresholds = mSettings.getBool("displayHistogramThresholds [bool]",1);
    mDisplayBlownHighlights = mSettings.getBool("displayBlownHighlights [bool]",0);
    mBlownHighlightThreshold = mSettings.getInt("blownHighlightThreshold [225-255]",250);
  }

  // Determine which is lit frame by 
  // finding average red value in 
  // each frame. The frame with the higher
  // average value is assumed lit
  int meanValueImage1 = getImgRedMean(pImage1);
  int meanValueImage2 = getImgRedMean(pImage2);
  int meanValue = meanValueImage1 - meanValueImage2;
  //unsigned int threshold;

  if ( meanValue > 0)
  {
    //Mean of image 1 is greater than image 2, so image 1 is lit

    if(mDisplayBlownHighlights)
      getBlownHighlights(*pImage2, mBlownHighlights,2);

    mThreshold =	subtractAndThreshold(pImage1,pImage2,fh);
  }
  else
  {
    //Mean of image 2 is greater than image 1, so image 2 is lit
    if(mDisplayBlownHighlights)
      getBlownHighlights(*pImage1, mBlownHighlights, 2);

    mThreshold =	subtractAndThreshold(pImage2,pImage1,fh);
  }
}

void BackgroundSubIRFlashing::visualize(Image32& vf)
{
  if(mDisplayBlownHighlights)
    displayBlownHighlights(vf, mBlownHighlights);

  if(mDisplayHistogram)			
    displayThresholdHistogram(vf);
}

void
BackgroundSubIRFlashing::displayThresholdHistogram(Image32& vf)
  {	
    int w = vf.width();
    int h = vf.height();
    unsigned int** vfPtr = vf.pointerpointer();
    unsigned int mMax = 0;
    unsigned int step = 0;
    int i = 0;

    if(mEnableRealTimeParamAdjust)
      mHistogramStep = mSettings.getInt("histogramVisualizationStep [int]",0);

    step = pow(2.0,(int)mHistogramStep);

    for(i=0; i<256; i++)
      mMax = std::max(mMax, mHistogram[i]);

    //for(int i=0; i<256/step; i++)
    for(i=0; i<std::min(256.0f,(float)w/step); i++)
    {
      int maxRow = h-(int)((((float)mHistogram[i])/mMax)*h);
      for(int k=0;k<step;k++)
        for(int j=h-1; j>=maxRow; j--)
        {
          PVASSERT(i*step+k<w);
          PVASSERT(j>=0 && j<h);
          vfPtr[j][i*step+k] = 0xff;
        }
    }

    if(mDisplayHistogramThresholds)
    {
      for(int k=0;k<step;k++)
        for(int j=0; j<h; j++)
        {
          PVASSERT(j>=0 && j<h);			
          i=mThreshold*step+k; if(i>=0 && i<w) vfPtr[j][i] = 0xff0000;

          i=threshold1*step+k; if(i>=0 && i<w) vfPtr[j][i] = PV_RGB(128,0,0);
          i=threshold2*step+k; if(i>=0 && i<w) vfPtr[j][i] = PV_RGB(255,0,0);
          i=peak1*step+k; if(i>=0 && i<w) vfPtr[j][i] = PV_RGB(255,255,0);
          i=peak2*step+k; if(i>=0 && i<w) vfPtr[j][i] = PV_RGB(255,255,0);
        }
    }
  }


int 
BackgroundSubIRFlashing::getImgRedMean(Image32Ptr& img)
  {
    int redTotal = 0;
    int rows = (*img).rows();
    int cols = (*img).cols();

    for (int i = 0; i < rows; i++)
    {
      for (int j = 0; j < cols; j++)
      {
        redTotal += RED((*img)(j,i));    
      }
    }

    return redTotal/(rows * cols);
  }

unsigned int 
BackgroundSubIRFlashing::subtractAndThreshold(Image32Ptr& pLitImage, Image32Ptr& pUnlitImage, FrameHistory& fh)
  {
    unsigned int threshold;
    char filename[255];
    double t = PvUtil::time();   
//    int w,h;

    if(mEnableRealTimeParamAdjust)
      mInfraredDoThreshold = mSettings.getBool("infraredDoThreshold [bool]",0);

#ifdef DEBUG_SAVE_FRAMES
    if(mSaveFrameHistory)
    {      
      sprintf(filename, "TestImages2\\individualFrames\\LitImage_%f.jpg", t);	
      pLitImage->savejpg(filename);    
      sprintf(filename, "TestImages2\\individualFrames\\UnlitImage_%f.jpg", t);	
      pUnlitImage->savejpg(filename);

      if(mDisplayBlownHighlights)
      {
        Image32 img(*pUnlitImage);
        displayBlownHighlights(img, mBlownHighlights);
        sprintf(filename, "TestImages2\\individualFrames\\UnlitImage_BlownHighlights_%f.jpg", t);	
        img.savejpg(filename);
      }
    }
#endif

    if(mInfraredDoThreshold)
    {
      //if (!mInfraredVisualize)
      //{
      //  mDiffImage.resize(pLitImage->width(), pLitImage->height());
      //  mDiffImage.absSub(*pLitImage, *pUnlitImage);

      //  // If the original lit image should be visualized,
      //  // and not the masked version of the lit image,
      //  // an extra image copy is required to keep
      //  // the original lit image intact
      //  *pUnlitImage = *pLitImage;
      //  // Threshold the image
      //  threshold = thresholdImg(*pLitImage,mDiffImage); //Ingmar: Marc, you never return or use tempImage, is this intended?
      //}
      //else
      {  
        (*pUnlitImage).absSub(*pLitImage, *pUnlitImage);

        // Threshold the image
        threshold = thresholdImg(*pLitImage,*pUnlitImage);
      }

#ifdef DEBUG_SAVE_FRAMES    
      if(mSaveFrameHistory)
      {                
        sprintf(filename, "TestImages2\\individualFrames\\ForegroundImage_%f.jpg", t);	
        pLitImage->savejpg(filename);    
        sprintf(filename, "TestImages2\\individualFrames\\DifferenceImage_%f.jpg", t);	
        pUnlitImage->savejpg(filename);
        Image32 histImg(pLitImage->width(), pLitImage->height());
        histImg.setAll(PV_RGB(255,255,255));
        displayThresholdHistogram(histImg);
        sprintf(filename, "TestImages2\\individualFrames\\HistogramImage_%f.jpg", t);	
        histImg.savejpg(filename);        
      }
#endif
    }


    // Replace pointers in buffer so the lit image is in the front position
    //FrameHistory& fh = *mMultiResFrames[0];
    fh.pop_front();
    fh.pop_front();

    // If the original lit image is to be visualized,
    // this stores a copy of the original lit image.
    // This acts as a placeholder in the circular buffer 
    // if the masked lit image is visualized.
    fh.push_front(pUnlitImage);

    // This places the masked lit image in the front position of the buffer 
    fh.push_front(pLitImage);

    return threshold;
  }


unsigned int
BackgroundSubIRFlashing::thresholdImg(Image32& litImage, Image32& subResult)
  {
    int rows = litImage.rows();
    int cols = litImage.cols();
    int count = rows*cols;
    int i, k;	
    unsigned int t = 0;	
    unsigned int freqTotal = 0;			
    unsigned int m1 = 0, m2 = 0;
    static unsigned int tnew = 70;
    unsigned int tmpHistogram[256];


    if(mEnableRealTimeParamAdjust)
    {
      mSmoothHistogram = mSettings.getBool("smoothHistogram [bool]",1);
      mSmoothDiffImage = mSettings.getBool("smoothDiffImage [bool]",1);
      mSmoothHistogramKernelSize = mSettings.getInt("smoothHistogramKernelSize [3,5]",3);
      if(mSmoothHistogramKernelSize > 5) mSmoothHistogramKernelSize=5;
      else if(mSmoothHistogramKernelSize != 5) mSmoothHistogramKernelSize=3;
    }

    //smooth difference image
    if(mSmoothDiffImage)
    {		
      image_proc::copyRGB2Gray(subResult, mTmpGrayImage1, 1);
      image_proc::smoothImage(mTmpGrayImage1,mTmpGrayImage2,image_proc::SMOOTH_MEAN,mSmoothHistogramKernelSize); 					
    }
    else
      image_proc::copyRGB2Gray(subResult, mTmpGrayImage2, 1);


    uchar* mask = mMask.pointer();
    uchar* temp1 = mTmpMask.pointer();
    uchar* imgPtr = mTmpGrayImage2.pointer();

    //build histogram (here: 256 bins)
    for(i = 0; i < 256; i++)
    {
      mHistogram[i] = 0;
      tmpHistogram[i] = 0;
    }
    for(i=count-1; i--;)
    {
      k = imgPtr[i];
      mHistogram[k]++;
      tmpHistogram[k]++;
    }


    //smooth histogram
    if(mSmoothHistogram)
    {
      int smoothWindowSize = 3; //has to be odd
      std::list<unsigned int> tmp;
      unsigned int tmpSum = 0;

      for(i=0;i<smoothWindowSize/2;i++)
      {
        tmp.push_back(tmpHistogram[i]);
        tmpSum+=tmpHistogram[i];
      }
      for(i=smoothWindowSize/2;i<smoothWindowSize;i++)
      {
        tmp.push_back(tmpHistogram[i]);
        tmpSum+=tmpHistogram[i];
        mHistogram[i]=tmpSum;
      }
      for(i=smoothWindowSize;i<256-smoothWindowSize;i++)
      {
        tmp.push_back(tmpHistogram[i]);
        tmpSum-=tmp.front();
        tmpSum+=tmpHistogram[i];
        mHistogram[i]=tmpSum;
        tmp.pop_front();
      }
      for(i=256-smoothWindowSize;i<256;i++)
      {
        tmpSum-=tmp.front();
        tmpSum+=tmpHistogram[i];
        mHistogram[i]=tmpSum;
        tmp.pop_front();
      }
    }

    //find mean shift
    int startIdx = mMeanShiftStartIdx;
    do
    {
      t = tnew;

      freqTotal = 0;
      m1 = 0;

      for(i = startIdx; i < t; i++)
      {
        m1 += mHistogram[i]*i;
        freqTotal += mHistogram[i];
      }

      if(freqTotal != 0)
      {
        m1 = m1/freqTotal;
      }

      freqTotal = 0;
      m2 = 0;

      for(i = t; i < 256; i++)
      {
        m2 += mHistogram[i]*i;
        freqTotal += mHistogram[i];
      }

      if(freqTotal != 0)
      {
        m2 = m2/freqTotal;
      }

      tnew = ((m1 + m2) / 2);
    } while(tnew != t);


    //find valley between two peaks
    unsigned int maxV = 0;
    unsigned int sumV = 0;
    unsigned int bias = UINT_MAX;
    int valley = 0;

    //first find the true first peak
    float m1_new=m1;
    for(i=(int)m1;i>=0;i--)
      if(mHistogram[i]>mHistogram[(int)m1_new])
        m1_new=i;
    m1=m1_new;

    for(i=(int)m1; i<=tnew; i++)
      bias = std::min(bias, std::max(1u,mHistogram[i]));

    for(i=(int)m1; i<=tnew; i++)
      sumV += std::max(0u, mHistogram[i] - bias);

    if(mEnableRealTimeParamAdjust)
      mFindThresholdPercentage = mSettings.getFloat("findThresholdPercentage [float]",0.8f);

    maxV=sumV*mFindThresholdPercentage;
    sumV = 0;

    unsigned int diff = 0;
    for(i=(int)m1; i<=tnew; i++)
    {
      diff = std::max(0u, mHistogram[i] - bias);
      sumV += diff;			
      if(diff<(1.0f-mFindThresholdPercentage)*sumV) break;
    }

    valley=i;

    threshold1 = (unsigned int)tnew;
    threshold2 = (unsigned int)valley;
    peak1 = (unsigned int) m1;
    peak2 = (unsigned int) m2;

    tnew=valley;

    unsigned int sumForeground = 0;

    //create foreground mask	
    for(i=count-1; i--;)
    {
      if (imgPtr[i] < tnew)
      {
        mask[i] = 0;       
      }
      else
      {
        mask[i] = 1;				
      }			
    }

    if(mEnableRealTimeParamAdjust)
      mDoDialateAndErode = mSettings.getBool("doDialateAndErode [bool]",1);

    if(mDoDialateAndErode)
    {
      if(mEnableRealTimeParamAdjust)
      {
        mMorphOpenKernelSize = mSettings.getInt("morphOpenKernelSize [int]",2);
        mMorphOpenIterations = mSettings.getInt("morphOpenIterations [int]",1);
        mMorphCloseKernelSize = mSettings.getInt("morphCloseKernelSize [int]",3);
        mMorphCloseIterations = mSettings.getInt("morphCloseIterations [int]",1);
        mMorphDilateKernelSize = mSettings.getInt("morphDilateKernelSize [int]",3);
        mMorphDilateIterations = mSettings.getInt("morphDilateIterations [int]",1);
      }

      cvMorphologyEx(mIplMask, mIplTmpMask, NULL, mShapeOpenKernel, CV_MOP_OPEN, mMorphOpenIterations);
      cvMorphologyEx(mIplTmpMask, mIplMask, NULL, mShapeCloseKernel, CV_MOP_CLOSE, mMorphCloseIterations);			
      cvDilate(mIplMask, mIplTmpMask, mShapeDilateKernel, mMorphDilateIterations);
    }
    else
      temp1=mask;

    unsigned int* litPtr = litImage.pointer();
    unsigned int tmp=0;

    if(!mInfraredMaskInAlpha)
    {
      //Apply mask to lit image
      //litImage.elemMult(mask);
      for(i=count-1; i--;)
      {
        if(temp1[i] == 0)
          litPtr[i] = 0;
      }
    } 
    else
    {
      for(i=count-1; i--;)
      {
        //Encode Mask in Alpha Channel of the Lit Image
        if(temp1[i] == 0)
        {
          litPtr[i] = 0;
        }
        else
        {
          litPtr[i] = litPtr[i] | 0xFF000000;						
        }
      }				
    }

    //return mInfraredThreshold + tnew;
    return tnew;
  }


  void
  BackgroundSubIRFlashing::displayBlownHighlights(Image32& vf, Image32& mask)
  {	
    uint* maskPtr = mask.pointer();
    uint* vfPtr = vf.pointer();
    uint color = PV_RGB(255,0,255);

    for(int i=mask.cols()*mask.rows()-1; i--;)
    {
      if(maskPtr[i] && vfPtr[i])
        vfPtr[i] = color;
    }

  }

  uint
  BackgroundSubIRFlashing::getBlownHighlights(const Image32& img, Image32& mask, int checkChannel)
  {
    uint* dataPtr = img.pointer();
    uint* maskPtr;
    uint num = 0;
    uint d=0;

    mask.resize(img.width(),img.height());
    maskPtr = mask.pointer();

    if(checkChannel==-1) //check all channels
    {
      uint threshold = (mBlownHighlightThreshold<<16) + (mBlownHighlightThreshold<<8) + mBlownHighlightThreshold;
      for(int i=img.cols()*img.rows()-1; i--;)
      {
        d=(dataPtr[i] & 0xFFFFFF)>threshold;		
        num+=d;
        maskPtr[i]=d;
      }
    }
    else		
    {
      uint mask=0;
      uint shift=0;
      switch(checkChannel)
      {
      case 0:
        mask=0xFF0000;
        shift=16;
        break;
      case 1:
        mask=0xFF00;
        shift=8;
      case 2:
        mask=0xFF;
        shift=0;
        break;
      }

      for(int i=img.cols()*img.rows()-1; i--;)
      {
        d=((dataPtr[i] & mask)>>shift)>mBlownHighlightThreshold;
        num+=d;
        maskPtr[i]=d;
      }
    }

    /*Image32 tmp = img;
    tmp.savejpg("mBlownHighlights_Source.jpg");
    mask.savejpg("mBlownHighlights_Mask.jpg");*/

    return num;
  }


//
// ACCESS
//

//
// INQUIRY
//

}; // namespace vision

}; // namespace ait


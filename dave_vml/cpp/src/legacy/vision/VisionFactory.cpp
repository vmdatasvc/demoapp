/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#pragma warning(disable: 4503)
#endif

#include "legacy/vision/VisionFactory.hpp"

//
// Include all vision modules in LibVision
//

#include "legacy/vision/VisionMod.hpp"
//#include "ControlMod.hpp"
#include "legacy/vision/ImageAcquireMod.hpp"
//#include "VideoWriterMod.hpp"
//#include "TailMod.hpp"
//#include "ImageAcquireControlMod.hpp"
//#include "ImageAcquireAutoAdjustMod.hpp"

namespace ait 
{

namespace vision
{

// Global instance. This will ensure that the class is deleted at the end of the
// program.
static VisionFactory gInstance;

VisionFactory::VisionFactory()
{
  // Register core modules.

//  AIT_REGISTER_VISION_MODULE(ControlMod);
  AIT_REGISTER_VISION_MODULE(ImageAcquireMod);	// image acquire module is always needed
//  AIT_REGISTER_VISION_MODULE(TailMod);
//  AIT_REGISTER_VISION_MODULE(ImageAcquireControlMod);
//  AIT_REGISTER_VISION_MODULE(ImageAcquireAutoAdjustMod);
//  AIT_REGISTER_VISION_MODULE(VideoWriterMod);

  // INSERT NEW MODULES BEFORE THIS COMMENT
}

VisionFactory::~VisionFactory()
{
  garbageCollect();
}

VisionFactory*
VisionFactory::instance()
{
  return &gInstance;
}

//
// OPERATIONS
//

VisionMod* 
VisionFactory::createModule(const std::string& exePathName, const std::string& moduleName)
{
  VisionMod* pMod;

  ModuleCreatorsMap::iterator i = mModuleCreators.find(moduleName);

  if (i == mModuleCreators.end())
  {
    PvUtil::exitError("Don't know how to create module %s",moduleName.c_str());
  }

  // Call the functor to create a new instance;
  pMod = (*(i->second))();
  
  pMod->setExecutionPath(exePathName);

  return pMod;
}

void 
VisionFactory::getModuleAux(const std::string& exePathName, const std::string& moduleName, VisionModPtr& pMod)
{
  garbageCollect();

  Lock lk(mMutex);

  // Look for the path
  PathsMap::iterator iPath = mPaths.find(exePathName);

  if (iPath == mPaths.end())
  {
    // The path is not in the map. Create it!

    // The next line should work, but it doesn't compile in MSVC++.
    // iPath = mPaths.insert(PathsMap::value_type(exePathName,new ModulesMap));

    // This compiles, but is not the most efficient.
    mPaths[exePathName] = ModulesMapPtr(new ModulesMap);
    iPath = mPaths.find(exePathName);
  }

  ModulesMapPtr pModMap = iPath->second;

  ModulesMap::iterator iMod = pModMap->find(moduleName);
  
  if (iMod == pModMap->end())
  {
    // Now the module doesn't exist, it will have to be created
    pMod.reset(createModule(exePathName,moduleName));

    // And now added to the map
    (*pModMap)[moduleName] = pMod;
  }
  else
  {
    // In this case it was in the map.
    pMod = iMod->second;
  }
}

void 
VisionFactory::registerModule(const std::string& modName, ModuleCreatorPtr pCreatorFunctor)
{
  Lock lk(mMutex);

  mModuleCreators[modName] = pCreatorFunctor;
}

void 
VisionFactory::garbageCollect()
{
  Lock lk(mMutex);
  
  PathsMap::iterator iPath;
  
  for (iPath = mPaths.begin(); iPath != mPaths.end();)
  {
    
    ModulesMapPtr pModMap = iPath->second;
    
    ModulesMap::iterator iMod;
    
    for (iMod = pModMap->begin(); iMod != pModMap->end();)
    {
      if (iMod->second.unique())
      {
        // Added the next 3 lines because g++ compiler would not allow an assignment of the iterator from the result of the erase().
        ModulesMap::iterator tmpMod;
        tmpMod = iMod;
        ++iMod;
        
        (*pModMap).erase(tmpMod);
      }
      else
      {
        ++iMod;
      }
    }
    
    if (pModMap->empty())
    {
      // Added the next 3 lines because g++ compiler would not allow an assignment of the iterator from the result of the erase().
      PathsMap::iterator tmpPath;
      tmpPath = iPath;
      ++iPath;
      
      mPaths.erase(tmpPath);
    }
    else
    {
      ++iPath;
    }
  }
  
}

void 
VisionFactory::parseFullName(const std::string& fullName, std::string& executionPathName, std::string& moduleName)
{
  std::string::size_type pos = fullName.find(':');
  if (pos == std::string::npos)
  {
    executionPathName = "";
    moduleName = fullName;
  }
  else
  {
    executionPathName = fullName.substr(0,pos);
    moduleName = fullName.substr(pos+1);
  }
}

//
// ACCESS
//


}; // namespace vision

}; // namespace ait


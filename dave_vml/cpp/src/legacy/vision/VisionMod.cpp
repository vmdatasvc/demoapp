/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#pragma warning(disable: 4503)
#endif

#ifdef WIN32
#include <windows.h>
#endif

#include "legacy/vision/VisionMod.hpp"
#include "legacy/vision/ImageAcquireMod.hpp"
//#include "MsgQueue.hpp"
//#include "VisionFactory.hpp"

namespace ait 
{

namespace vision
{

VisionMod::VisionMod()
:mName("VisionMod"),
 mPathName(""),
 mIsInitialized(false),
 mVisualizeFlag(false),
 mpParentModule(NULL),
 mExecutionPriority(5)
// mpImageAcquireMod(NULL)
	{

	}

VisionMod::VisionMod(const std::string& moduleName)
: mName(moduleName),
  mPathName(""),
  mIsInitialized(false),
  mVisualizeFlag(false),
  mpParentModule(NULL),
//  mpInputMsgQueue(new MsgQueue()),
  mExecutionPriority(5)
{
  PVMSG("+++ [%s] - Constructor\n",moduleName.c_str());
}

VisionMod::~VisionMod()
{
  PVMSG("--- [%s] - Destructor\n",getModuleName().c_str());
}

void 
VisionMod::init()
{
  mIsInitialized = true;

#ifdef WIN32
  PVMSG("%s::init() in thread %x\n",getModuleName().c_str(),GetCurrentThreadId());
#endif
}

void 
VisionMod::finish()
{
#ifdef WIN32
  PVMSG("%s::finish() in thread %x\n",getModuleName().c_str(),GetCurrentThreadId());
#endif
  mIsInitialized = false;
}

//void
//VisionMod::initExecutionGraph(std::list<VisionModPtr>& srcModules)
//{
//}
void VisionMod::initExecution()
{
	// do nothing? need to set mpImageAcquire?
	getModule("ImageAcquireMod",mpImageAcquireMod);
}

//
// OPERATIONS
//

void 
VisionMod::process()
{
  // Do nothing by default.
}

void 
VisionMod::processMessages()
{
  // Do nothing by default.
}

void 
VisionMod::visualize(Image32& frame)
{
  // Do nothing by default
}

void 
VisionMod::setExecutionPath(const std::string& pathName)
{
  mPathName = pathName;
  mSettings.changePath(std::string("VisionMods/") + mPathName + "/" + mName);
}

//void
//VisionMod::addOutputMsgQueue(MsgQueuePtr pDest)
//{
//  Lock lk(mQueuesMutex);
//  mOutputMsgQueues.push_back(pDest);
//}
//
//void
//VisionMod::putMsg(VisionMsgPtr pMsg)
//{
//  mpInputMsgQueue->put(pMsg);
//}
//
//void
//VisionMod::sendMessage(VisionMsgPtr pMsg)
//{
//  Lock lk(mQueuesMutex);
//  std::list<MsgQueuePtr>::iterator i;
//
//  for (i = mOutputMsgQueues.begin(); i != mOutputMsgQueues.end(); ++i)
//  {
//    (*i)->put(pMsg);
//  }
//}

//
// ACCESS
//

void 
VisionMod::setVisualize(Bool set)
{
  mVisualizeFlag = set;
}

//MsgQueuePtr
//VisionMod::getInputMsgQueue()
//{
////  return mpInputMsgQueue;
//}

void 
VisionMod::setParentModule(VisionModPtr& pParent)
{
  mpParentModule = pParent.get();
}

VisionMod* 
VisionMod::getParentModule()
{
  return mpParentModule;
}

Settings&
VisionMod::getSettings()
{
  return mSettings;
}

//
// INQUIRY
//

Bool
VisionMod::isSubModule()
{
  return mpParentModule != NULL;
}

Bool 
VisionMod::isVisualize()
{
  return mVisualizeFlag;
}

ImageAcquireModPtr
VisionMod::getImageAcquireModule()
{
  return mpImageAcquireMod;
}

const ImageAcquireMod * const
VisionMod::getImageAcquireModule() const
{
  return mpImageAcquireMod.get();
}

Image32&
VisionMod::getCurrentFrame()
{
  return getImageAcquireModule()->getCurrentFrame();
}

Image32&
VisionMod::getVisualizationFrame()
{
  return getImageAcquireModule()->getVisualizationFrame();
}

int
VisionMod::getCurrentFrameNumber()
{
  return getImageAcquireModule()->getCurrentFrameNumber();
}

double VisionMod::getCurrentTime()
{
  return getImageAcquireModule()->getCurrentFrameTime();
}

double VisionMod::testreturn()
{
	return 0.1;
}


} // namespace vision

} // namespace ait


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#pragma warning(disable: 4503)
#endif

#include <string>

//#include <ImageAcquireDeviceBase.hpp>
//#include <BackgroundSubIRFlashing.hpp>
#include <legacy/vision_tools/PvImageProc.hpp>
#include <legacy/types/DateTime.hpp>

#include "legacy/vision/ImageAcquireMod.hpp"

// All possible devices
#ifdef USE_MIL_LIBRARY
#include "ImageAcquireDeviceMatroxMeteor2.hpp"
#endif
//#include "ImageAcquireDeviceBlank.hpp"
#ifdef WIN32
#include "ImageAcquireDeviceAVI.hpp"
#include "ImageAcquireDeviceDirectShow.hpp"
#include "ImageAcquireDeviceIp.hpp"
#ifdef USE_GEOVISION
#include "ImageAcquireDeviceGeoVision.hpp"
#endif
#endif
#ifdef USE_POINTGREY
#include "ImageAcquireDevicePointGrey.hpp"
#endif
#ifdef USE_WATCHNET
#include "ImageAcquireDeviceWatchnet.hpp"
#endif
//#include <ImageAcquireDeviceReference.hpp>
#include "legacy/media/ImageAcquireDeviceOpenCV.hpp"

namespace ait
{

namespace vision
{

ImageAcquireMod::ImageAcquireMod()
: VisionMod(getModuleStaticName()),
	mCurrentFrameIndex(0),
	mCurrentFrameTimeStamp(0),
	mIsRealTimeDevice(true),
	mDeviceFps(0),
	mSkipFrameInterval(1),
  mStartTimestampTime(0.0),
  mUseStartingTimestamp(false)
{
}

ImageAcquireMod::~ImageAcquireMod()
{
}

//void
//ImageAcquireMod::initExecutionGraph(std::list<VisionModPtr>& srcModules)
//{
//  // This call will set a circular dependency.
//  RealTimeMod::initExecutionGraph(srcModules);
//
//  // This module does not have image acquire as a source module. Break
//  // the circular dependency.
//  srcModules.clear();
//
//  // Check if this camera must be synchronized with a master camera.
//  std::string syncMaster = mSettings.getString("syncMaster","",false);
//  if (!syncMaster.empty())
//  {
//    // Add any source module to the given list here.
//    VisionModPtr pMod;
//    getModule(syncMaster + ":ImageAcquireControlMod",pMod);
//    srcModules.push_back(pMod);
//  }
//
//  // Get the frame rate from the registry. The device has not been initialized
//  // so we can't obtain the Fps from that class.
//  std::string deviceName = mSettings.getString("grabberDeviceId","BLANK0");
//
//  if (!mDeviceFps)
//  {
//    Settings s("ImageAcquireControl/Devices");
//    mDeviceFps = s.getDouble(deviceName+"/Fps/value [float]",15);
//
//    //PVMSG("FRAME RATE 1: %f\n",mDeviceFps);
//
//    //multiply by the skipFrameInterval for the correct FPS
//    //mDeviceFps *= (float)s.getInt("skipFrameInterval",1);
//  }
//
//
//  int delim = deviceName.find(':');
//  if (delim != -1)
//  {
//    std::string execPath(deviceName, 0, delim);
//    std::string deviceId(deviceName, delim+1, deviceName.length());
//
//    if (deviceId.substr(0,3) == "REF")
//    {
//
//      //VisionModPtr pMod;
//      //getModule("VisionMods/" + execPath + ":ImageAcquireMod", pMod);
//      ImageAcquireModPtr pMod;
//      getModule(execPath + ":ImageAcquireMod", pMod);
//      srcModules.push_back(pMod);
//
//      //TODO: this code depends on a global setting of VisionMods/CameraX
//      // This should be changed
//
//      Settings s("VisionMods/" + execPath);
//      std::string grabberDevice = s.getString("ImageAcquireMod/grabberDeviceId", "");
//
//      PVASSERT(grabberDevice != "");
//      Settings s1("ImageAcquireControl/Devices");
//      mDeviceFps = s1.getDouble(grabberDevice+"/Fps/value [float]", 99);
//
//    }
//  }
//}

void 
ImageAcquireMod::init()
{
  if (mIsInitialized) return;

//  RealTimeMod::init();

  mSettings.changePath("ImageAcquireMod");

//  mBenchmark.init(mSettings.getPath(),"grabAndSubSample");

  mNumSubScaleLevels = mSettings.getInt("numSubScaleLevels [int]",4);
  mNumHistoryFrames = mSettings.getInt("numHistoryFrames [int]",3);

//  mVisualSensorId.setId(mSettings.getString("visualSensorId","0.0.0"));

  std::string deviceName = mSettings.getString("grabberDeviceId","BLANK0");

  mFirstFrameTimeStamp = 0;

  initDevice(deviceName);

  initRingBuffers();

  mpVisualizationFrame.reset(new Image32);
  mpDevice->setAlternativeVisualizationFrame(mpVisualizationFrame);

  /// Load the default parameters from the registry.
  for (int i = 1; i < ImageAcquireProperty::getTotalProperties(); ++i)
  {
    mpDevice->getProperty(static_cast<ImageAcquireProperty::Code>(i));
  }

  //Determine whether we should use a different time origin if we 
  // are processing videos
  std::string startTimestamp = mSettings.getString("startTimestamp","");

  if (startTimestamp != "")
  {
    mUseStartingTimestamp = true;

    DateTime dt = DateTime::fromString(startTimestamp);    

    mStartTimestampTime = dt.toSeconds();

    mFirstFrameTimeStamp = mStartTimestampTime;
    mCurrentFrameTimeStamp = mStartTimestampTime;

    PVMSG("ImageAcquireMod: Setting internal clock start time to: %s\n", 
      DateTime(mStartTimestampTime).toString().c_str());
  }
  else
    mStartTimestampTime = 0.0;
}

void
ImageAcquireMod::finish()
{    
  PVMSG("ImageAcquireMod::finish()...");

  // Hide the window.
  mpDevice->displayFrame(false);

  mpDevice->unInitialize();

  mpDevice.reset();

  releaseRingBuffers();

//  RealTimeMod::finish();

  mIsInitialized=false;

  PVMSG("done\n");
}

//
// OPERATIONS
//
void 
ImageAcquireMod::process()
{
  if(mIsInitialized)
  {
    normalProcess();
  }
}

void 
ImageAcquireMod::normalProcess()
{
  int i;

//  mBenchmark.beginSample();

  // Advance ring buffers
  for (i = 0; i < mMultiResFrames.size(); ++i)
  {
    FrameHistory& fh = *mMultiResFrames[i];
    Image32Ptr pImage = fh.back();
    fh.pop_back();
    fh.push_front(pImage);
  }

  // Capture frame
  if (mpDevice->grabFrame() == 1)
  {
    // This is the last frame, and the video is about to loop.
    // Send a message indicating this event.
//    ImageAcquireMsgPtr pMsg(new ImageAcquireMsg(getVisualSensorId(),getCurrentTime()));
//    pMsg->setFlag(ImageAcquireMsg::LAST_FRAME);
//    pMsg->setVisualSensorId(getVisualSensorId());
//    sendMessage(pMsg);
  }

  // update time stamp and frame counter;
  mCurrentFrameIndex++;

  if (mFirstFrameTimeStamp == 0)
  {
    mFirstFrameTimeStamp = PvUtil::time();
  }

  MediaFrameInfoPtr pFrame = mpDevice->getFrameInfo(0);

  if (pFrame->beginTime == 0)
  {
    if (mIsRealTimeDevice)
    {
      mCurrentFrameTimeStamp = PvUtil::time();
    }
    else
    {
      mCurrentFrameTimeStamp = mFirstFrameTimeStamp + (double)mCurrentFrameIndex/getFramesPerSecond();
    }
  }
  else
  {
    mCurrentFrameTimeStamp = pFrame->beginTime;
  }

  // Copy the current frame to the visualization buffer.
//  if (isVisualize())
//  {
//    Image32& vf = getVisualizationFrame();
//    const Image32& src = *(pFrame->pRGBImage);
//    if ((vf.width() == 1) ||
//      (vf.width() == src.width() && vf.height() == src.height()))
//    {
//      vf = src;
//    }
//    else
//    {
//      vf.paste(src,0,0);
//    }
//  }
//
//  // Update the display window.
//  mpDevice->displayFrame(isVisualize());

  // Subsample the images.
  for (i = 1; i < mMultiResFrames.size(); ++i)
  {
    PvImageProc::subSample(
      *(mMultiResFrames[i-1]->front()),
      *(mMultiResFrames[i]->front()),
      1);
  }
//  mBenchmark.endSample();
}

void
ImageAcquireMod::initDevice(const std::string& deviceID)
{
  ImageAcquireDeviceBase* pDevice = NULL;

//  int delim = deviceID.find(':');

  std::string deviceClass = deviceID.substr(0,4);
  if (deviceClass != "http")
  {
	  deviceClass = "File";
  }
  std::string execPath;

//  if (delim == -1)
//  {
//    int numeric = deviceID.find_first_of("1234567890");
//    deviceClass = deviceID.substr(0, numeric);
//    //deviceClass = deviceID.substr(0, deviceID.length() - 1);
//  }
//  else
//  {
//    //IF there is a ':', the device class should be REF
//    execPath = deviceID.substr(0,delim);
//    deviceClass = deviceID.substr(delim+1, 3);
//  }


  mIsRealTimeDevice = true;

//  if (deviceClass == "BLANK")
//  {
//    pDevice = new ImageAcquireDeviceBlank();
//  }
//  else if (deviceClass == "MATROXIISTANDARD")
//  {
//#ifdef USE_MIL_LIBRARY
//    pDevice = new ImageAcquireDeviceMatroxMeteor2();
//#else
//    PvUtil::exitError("This executable does not support Matrox devices.");
//#endif
//  }
//#ifdef WIN32
//  else if (deviceClass == "AVI")
//  {
//    pDevice = new ImageAcquireDeviceAVI();
//    mIsRealTimeDevice = false;
//  }
//  else if (deviceClass == "USBCAM" || deviceClass == "DIRECTSHOW") // USBCAM is for backwards compatibility.
//  {
//    pDevice = new ImageAcquireDeviceDirectShow();
//  }
//  else if (deviceClass == "IP")
//  {
//    pDevice = new ImageAcquireDeviceIp();
//  }
//#ifdef USE_GEOVISION
//  else if (deviceClass == "GEOCAM")
//  {
//    pDevice = new ImageAcquireDeviceGeo();
//  }
//#endif
//  else if (deviceClass == "POINTGREY")
//  {
//#ifdef USE_POINTGREY
//    pDevice = new ait::ImageAcquireDevicePointGrey();
//#else
//    PvUtil::exitError("This executable does not support PointGrey devices.");
//#endif
//  }
//  else if (deviceClass == "WATCHNET")
//  {
//#ifdef USE_WATCHNET
//    pDevice = new ait::ImageAcquireDeviceWatchnet();
//#else
//    PvUtil::exitError("This executable does not support Watchnet devices.");
//#endif
//  }
//#endif
//  else if (deviceClass == "REF")
//  {
//    ImageAcquireModPtr pMod;
//    getModule(execPath + ":ImageAcquireMod", pMod);
//    PVASSERT(pMod->mpDevice.get());
//    mIsRealTimeDevice = pMod->isLiveVideoSource();
//    pDevice = new ImageAcquireDeviceReference(pMod->mpDevice);
//  }

//  pDevice = new ait::ImageAcquireDeviceOpenCV(deviceID);
  pDevice = new ait::ImageAcquireDeviceOpenCV();

  if (!pDevice)
  {
    PvUtil::exitError("Device %s not registered or supported.",deviceClass.c_str());
  }

  mpDevice.reset(pDevice);

  // Check if this camera must be synchronized with a master camera.
//  if (mpSyncMaster.get())
//  {
//    mpDevice->setSyncMaster(mpSyncMaster->mpDevice);
//  }

//  pDevice->setDeviceInfo(ImageAcquireDeviceInfoPtr(new ImageAcquireDeviceInfo(deviceID,
//    std::string("ImageAcquireControl/Devices/") + deviceID)));
  // These parameters should go.

  pDevice->initialize(mNumHistoryFrames, 1);
  mIsRealTimeDevice = false;

  mFrameSize(0) = (int)pDevice->getProperty(ImageAcquireProperty::SOURCEWIDTH)->getValuef();
  mFrameSize(1) = (int)pDevice->getProperty(ImageAcquireProperty::SOURCEHEIGHT)->getValuef();

  mSkipFrameInterval = mpDevice->getSkipFrameInterval();
}

ImageAcquirePropertyPtr
ImageAcquireMod::getProperty(const ImageAcquireProperty::Code code)
{
  return mpDevice->getProperty(code);
}

void
ImageAcquireMod::initRingBuffers()
{
  // Delete any previous state.
  mMultiResFrames.clear();

  int i,j;

  for (i = 0; i < mNumSubScaleLevels; ++i)
  {
    FrameHistoryPtr pNew(new FrameHistory(mNumHistoryFrames+1));
    for (j = 0; j < pNew->capacity(); ++j)
    {
      Image32Ptr pImg;
      if (i == 0)
      {
        pImg = mpDevice->getFrameInfo(j)->pRGBImage;
        pImg->setAlphaFlag(0);
      }
      else
      {
        pImg.reset(new Image32(mFrameSize(0)>>i,mFrameSize(1)>>i));
        PvImageProc::subSample(*((*(mMultiResFrames[i-1]))[j]),*pImg,1);
      }
      pNew->push_back(pImg);
    }
    mMultiResFrames.push_back(pNew);
  }
}

void
ImageAcquireMod::releaseRingBuffers()
{
  std::vector<FrameHistoryPtr>::iterator Itr = mMultiResFrames.begin();	

  for(Itr;Itr!=mMultiResFrames.end();Itr++)
    for(int i=0; i < (*Itr)->capacity(); i++)
      (*Itr)->pop_front();

  mMultiResFrames.clear();
}

Image32& 
ImageAcquireMod::getCurrentFrame()
{
  //PVMSG("get current frame called at %d\n",clock());
  return *(mMultiResFrames[0]->front());
}

Image32Ptr 
ImageAcquireMod::getCurrentFramePtr()
{
  return mMultiResFrames[0]->front();
}

void
ImageAcquireMod::getCurrentFrameInfo(FrameInfo& info)
{
  info.setFrameIndex(mCurrentFrameIndex);
  info.setTimeStamp(mCurrentFrameTimeStamp);
}

Image32& 
ImageAcquireMod::getVisualizationFrame()
{
  return *mpVisualizationFrame;
} 

Image32& 
ImageAcquireMod::getHistoryFrame(Int backIndex, Int subSampleLevel)
{
  if (subSampleLevel >= mMultiResFrames.size())
  {
    PvUtil::exitError("A subsample level higher than available was requested");
  }

  //if (!mInfraredFlashSubtraction)
  if(mpDevice->getConsecutiveFrameNum()!=2)
  {  
    if (backIndex >= mMultiResFrames[subSampleLevel]->size()-1)
    {
      PvUtil::exitError("More frames than available were requested in the back history.");
    }

    FrameHistory& fh = *mMultiResFrames[subSampleLevel];

    return *(fh[backIndex]);
  }
  else
  {
    if (backIndex >= mMultiResFrames[subSampleLevel]->size()/2)
    {
      PvUtil::exitError("More frames than available were requested in the back history.");
    }

    FrameHistory& fh = *mMultiResFrames[subSampleLevel];

    return *(fh[backIndex*2]);
  }
}

//void
//ImageAcquireMod::setSyncMaster(ImageAcquireModPtr pMaster)
//{
//  mpSyncMaster = pMaster;
//
//  // If this device is already initialized, set the master now. Otherwise
//  // it will be set in the initialization.
//  if (isInitialized())
//  {
//    mpDevice->setSyncMaster(mpSyncMaster->mpDevice);
//  }
//}

void 
ImageAcquireMod::setVisualizationFrameSize(const Vector2i& size)
{
  PVASSERT(mpVisualizationFrame.get());
  Vector2i newSize((std::max)(size(0),(int)mpVisualizationFrame->width()),
    (std::max)(size(1),(int)mpVisualizationFrame->height()));

  mpVisualizationFrame->resize(newSize(0),newSize(1));
  mpDevice->setVisualizationFrameSize(newSize);
}

int
ImageAcquireMod::getTotalNumberOfFrames()
{
  return mpDevice->getTotalNumberOfFrames();
}

void 
ImageAcquireMod::notifySettingsChange(const std::string& notifyString)
{
  if (notifyString == "ImageAcquireMod:CameraSettings")
  {
    mpDevice->setInteractive(true);
  }
}

//
// ACCESS
//

//
// INQUIRY
//

bool 
ImageAcquireMod::isLiveVideoSource()
{
  return mIsRealTimeDevice;
}


  }; // namespace vision

}; // namespace ait


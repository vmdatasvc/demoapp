/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#pragma warning(disable: 4503)
#endif

#include "RealTimeMod.hpp"
#include "ImageAcquireMod.hpp"
#include "VisionFactory.hpp"

namespace ait 
{

namespace vision
{

RealTimeMod::RealTimeMod(const std::string& moduleName)
: VisionMod(moduleName)
{
}

RealTimeMod::~RealTimeMod()
{
}

//void
//RealTimeMod::initExecutionGraph(std::list<VisionModPtr>& srcModules)
//{
//  VisionMod::initExecutionGraph(srcModules);
//
//  VisionFactory::instance()->getModule(getExecutionPathName(),mpImageAcquireMod);
//
//  // Set Image Acquire control as a source module.
//  srcModules.push_back(mpImageAcquireMod);
//}

void
RealTimeMod::init()
{
  VisionMod::init();
}

void
RealTimeMod::finish()
{
  mpImageAcquireMod.reset();

  VisionMod::finish();
}

//
// OPERATIONS
//

double 
RealTimeMod::getFramesPerSecond() const
{
  return getImageAcquireModule()->getFramesPerSecond();
}

double
RealTimeMod::getCurrentTime()
{
  return getImageAcquireModule()->getCurrentFrameTime();
}

//VisualSensorId
//RealTimeMod::getVisualSensorId()
//{
//  return getImageAcquireModule()->getVisualSensorId();
//}

ImageAcquireModPtr
RealTimeMod::getImageAcquireModule()
{
  return mpImageAcquireMod;
}

const ImageAcquireMod * const
RealTimeMod::getImageAcquireModule() const
{
  return mpImageAcquireMod.get();
}

Image32& 
RealTimeMod::getCurrentFrame()
{
  return getImageAcquireModule()->getCurrentFrame();
}

Image32&
RealTimeMod::getVisualizationFrame()
{
  return getImageAcquireModule()->getVisualizationFrame();
}

int 
RealTimeMod::getCurrentFrameNumber()
{
  return getImageAcquireModule()->getCurrentFrameNumber();
}

//
// ACCESS
//

//
// INQUIRY
//

}; // namespace vision

}; // namespace ait


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include <time.h>
#include <math.h>

#include "legacy/pv/PvRandom.hpp"

static const long IA=16807;
static const long IM=2147483647;
static const float AM=(1.0f/IM);
static const long IQ=127773;
static const long IR=2836;
//static const long NTAB=32;
static const long NDIV=(1+(IM-1)/NTAB);
static const float EPS=1.2e-7f;
static const float RNMX=(1.0f-EPS);

namespace ait
{

const int PvRandom::UNIFORM0TO1=0;
const int PvRandom::UNIFORMMIN1TO1=1;
const int PvRandom::GAUSSRAND=2;
const int PvRandom::UNIFORM0TON=3;

/*********************************************************************
 * ran1()
 * 
 * Creates a float random number between 0 and 1.
 * Taken from the numerical recipes.
 *
 * <input> 
 * 
 * long idum: pointer to seed that will be updated
 *
 * <output>
 *
 * float : random number
 *
 *********************************************************************/
float PvRandom::ran1(long &idum)
{
  int j;
  long k;
  float temp;
  
  if (idum <= 0 || !iy) {     // if idum less or equal zero, or first call

    if (-(idum) < 1) idum=1;  // if idum greater or equal zero, set idum to 1
    else idum = -(idum);      // make idum positive
    // idum is positive here
		for (j=NTAB+7;j>=0;j--) {
		  k=(idum)/IQ;
			idum=IA*(idum-k*IQ)-IR*k;
			if (idum < 0) idum += IM;
			if (j < NTAB) iv[j] = idum;
		}
		iy=iv[0];
  }
  k=(idum)/IQ;
  idum=IA*(idum-k*IQ)-IR*k;
  if (idum < 0) idum += IM;
  j=iy/NDIV;
  iy=iv[j];
  iv[j] = idum;
  if ((temp=AM*(float)iy) > RNMX) return RNMX;
  else return temp;
}

/*********************************************************************
 * uniform0to1()
 * 
 * Uniformly distributed random number between 0 and 1.
 *
 * <input> 
 * 
 * void
 *
 * <output>
 *
 * double : random number
 *
 *********************************************************************/
float PvRandom::uniform0to1(void)
{
  if(uniform0to1_first)
    {
      uniform0to1_seed=-(long)time(NULL);
      uniform0to1_first=false;
    }

  return ran1(uniform0to1_seed);
}

int PvRandom::uniform0toN(int n)
{
  double d = uniform0to1()*n;

  return (int)d == n ? 0 : (int)d;
}

float PvRandom::gaussRand(void)
{
  static int first=1;
  static int iset=0;
  static float gset;
  float fac,rsq,v1,v2;

  if(gaussRand_first)
    {
      gaussRand_seed=-time(NULL);
      gaussRand_first=false;
    }

  if  (iset == 0) {
    do {
      v1=2.0f*ran1(gaussRand_seed)-1.0f;
      v2=2.0f*ran1(gaussRand_seed)-1.0f;
      rsq=v1*v1+v2*v2;
    } while (rsq >= 1.0 || rsq == 0.0);
    fac=float(sqrt(-2.0f*log(rsq)/rsq));
    gset=v1*fac;
    iset=1;
    return v2*fac;
  } else {
    iset=0;
    return gset;
  }
}
 
float PvRandom::uniformMin1to1(void)
{
  if(uniformMin1to1_first)
    {
      uniformMin1to1_seed=-(long)time(NULL);
      uniformMin1to1_first=0;
    }

  return 2.0f*ran1(uniformMin1to1_seed)-1.0f;
}

long PvRandom::getSeed(int func)
{
  switch(func)
  {
  case UNIFORM0TO1: return uniform0to1_seed;
  case UNIFORMMIN1TO1: return uniformMin1to1_seed;
  case GAUSSRAND: return gaussRand_seed;
  case UNIFORM0TON: return uniform0to1_seed;
  default: return 0;
  }
}

void PvRandom::setSeed(int func, long value)
{
  switch(func)
  {
  case UNIFORM0TO1: uniform0to1_seed=value; break;
  case UNIFORMMIN1TO1: uniformMin1to1_seed=value; break;
  case GAUSSRAND: gaussRand_seed=value; break;
  case UNIFORM0TON: uniform0to1_seed=value; break;
  }

  iy = 0;

  return;
}

} // namespace ait

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include "legacy/pv/PvUtil.hpp"
//#include <Settings.hpp>

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "errno.h"

#ifdef WIN32
#include <windows.h>
#include <direct.h>
#include <process.h>
#include <mmsystem.h>
#elseif Q_OS_LINUX
#include <QErrorMessage>
#else
#include <limits.h>
#include <unistd.h>
#include <sys/time.h>
//#include <curses.h>
#endif

#ifdef PV_REPORT
PvReport report;
#endif

bool g_logFileAlso;

#ifdef WIN32
HANDLE PVMSG_HANDLE=stderr;
#endif

int PVMSG_METHOD=PVMSG_USE_ANSI;
void (*PVMSG_FUNC)(char *);

/**
 * Deployment mode can have the following values:
 * - 0 = Display errors on the screen.
 * - 1 = Write errors on log and exit silently.
 * - 2 = Throw an exception on error with the error message.
 * - 3 = Do nothing, just exit.
 */
int PvDeploymentMode = 0;

ait::LogWriter gDefaultLogWriter;
ait::LogWriter *PvUtil::mpLogWriter = &gDefaultLogWriter;

void PVMSG(const char *msg, ...)
{
  char pcBuffer[8192];
  DWORD written;
  va_list argptr;
  
  va_start(argptr,msg);

#ifdef WIN32
  _vsnprintf(pcBuffer,sizeof(pcBuffer),msg,argptr);  /* -- ANSI C function !! */
#elif WIN64
  vsnprintf(pcBuffer,sizeof(pcBuffer),msg,argptr);  /* -- ANSI C function !! */
#elif Q_OS_LINUX
  vsnprintf(pcBuffer,sizeof(pcBuffer),msg,argptr);  /* -- ANSI C function !! */
#endif

  va_end(argptr);

  //OutputDebugString(pcBuffer);
  
  switch(PVMSG_METHOD)
  {
  case PVMSG_USE_ANSI:
    printf("%s", pcBuffer);
    break;
  case PVMSG_USE_TRACE: 
#ifdef MFC
    TRACE("%s", pcBuffer);
#endif
    break;
  case PVMSG_USE_CONSOLE:

#ifdef WIN32
    WriteConsole(PVMSG_HANDLE,pcBuffer,strlen(pcBuffer),&written,NULL);
#elif Q_OS_LINUX
    // Qt console output here
#else
    fprintf(stderr,"%s", pcBuffer);
#endif

    if(g_logFileAlso){
      
      FILE *stream;
      
      char dbuffer [9];  //these are size nine right out of the MSDN library example
      char tbuffer [9];  
      _strdate( dbuffer );
      _strtime( tbuffer );
      
      stream = fopen("./log.txt", "a"); //open the file for appending
      
      fprintf(stream, "%s %s     %s", dbuffer, tbuffer, pcBuffer); //returns a negative if there are errors during the write.
      fclose(stream);    
    }
    break;
  case PVMSG_USE_OUTFUNC:
    PVMSG_FUNC(pcBuffer);
    break;
  case PVMSG_USE_LOGTOFILE:
    {
      FILE *stream;
      
      char dbuffer [9];  //these are size nine right out of the MSDN library example
      char tbuffer [9];  
      _strdate( dbuffer );
      _strtime( tbuffer );
      
      stream = fopen("./log.txt", "a"); //open the file for appending
      
      fprintf(stream, "%s %s     %s", dbuffer, tbuffer, pcBuffer); //returns a negative if there are errors during the write.
      fclose(stream);
      break;
    }
  case PVMSG_USE_NOTHING:
    break;//do nothing, save processor time
  }
}

void PvUtil::exitError(const char *msg, ...) 
{
  char pcBuffer[512];
  va_list argptr;
  
  va_start(argptr,msg);
  _vsnprintf(pcBuffer,sizeof(pcBuffer)-1,msg,argptr);  /* -- ANSI C function !! */
  va_end(argptr);

  if (!PvDeploymentMode)
  {
//    ait::Settings::writeXml("settings_error.xml",false);
  #ifndef WIN32
    fprintf(stderr,"%s\n", pcBuffer);
  #else
    MessageBox(NULL,pcBuffer,"Error - VideoMining",MB_OK|MB_ICONSTOP);
  #endif
  }
  else if (PvDeploymentMode == 1)
  {
    mpLogWriter->write(pcBuffer);
  }
  else if (PvDeploymentMode == 2)
  {
//    throw std::exception(pcBuffer);
	  throw std::exception();
  }
  
  ExitProcess(EXIT_FAILURE);
}

void PvUtil::error(const char *msg, ...)
{
  char pcBuffer[512];
  va_list argptr;
  
  va_start(argptr,msg);
  _vsnprintf(pcBuffer,sizeof(pcBuffer)-1,msg,argptr);  /* -- ANSI C function !! */
  va_end(argptr);

  if (!PvDeploymentMode)
  {
  #ifndef WIN32
    fprintf(stderr,"%s\n", pcBuffer);
  #elif Q_OS_LINUX
    // qt error dialog
    QErrorMessage eMsg;
    eMsg.showMessage(QString(pcBuffer));
  #else
    MessageBox(NULL,pcBuffer,"Error!",MB_OK|MB_ICONSTOP);
  #endif
  }
  else
  {
    mpLogWriter->write(pcBuffer);
  }
}


int PvUtil::question(const char *msg, ...)
{
  char pcBuffer[512];
  va_list argptr;
  
  va_start(argptr,msg);
  _vsnprintf(pcBuffer,sizeof(pcBuffer)-1,msg,argptr);  /* -- ANSI C function !! */
  va_end(argptr);

  if (!PvDeploymentMode)
  {
  #ifndef WIN32
    fprintf(stderr,"%s\n", pcBuffer);
  #elif Q_OS_LINUX
  #else
    return MessageBox(NULL,pcBuffer,"Question",MB_YESNOCANCEL|MB_ICONSTOP);
  #endif
  }
  else
  {
    mpLogWriter->write(pcBuffer);
//    return IDCANCEL;
    return -1;
  }
}


void PvUtil::sleep(const double sec)
{
#ifdef WIN32
  ::Sleep((long)(sec*1000));
#else
  ::usleep((long)(sec*1000000));
#endif
}

#ifdef WIN32

class InternalTimeInfo
{
public:
  InternalTimeInfo()
  {
    mStartSecondsSinceEpoch = PvUtil::systemTime();
    mStartTimeGetTime = timeGetTime();
  }

  double mStartSecondsSinceEpoch;
  DWORD mStartTimeGetTime;
};

boost::shared_ptr<InternalTimeInfo> gpInternalTimeInfo(new InternalTimeInfo());

#else
static double gEpochOffset = 0;
#endif // WIN32

double PvUtil::time()
{
#ifdef WIN32

  DWORD curTime = timeGetTime();

  InternalTimeInfo *pIti = gpInternalTimeInfo.get();

  DWORD elapsedTime = curTime-pIti->mStartTimeGetTime;

  // We don't want this value to overflow, so we will adjust the start
  // time accordingly. This condition will happen roughly once a day.
  if (elapsedTime > 1000*60*24)
  {
    InternalTimeInfo *pNewIti = new InternalTimeInfo();
    pNewIti->mStartSecondsSinceEpoch = pIti->mStartSecondsSinceEpoch + ((double)elapsedTime)/1000.0;
    pNewIti->mStartTimeGetTime = curTime;

    // This might not look very thread-safe, but actually it is because
    // assignment is atomic. It might happen that several threads update
    // this when the above condition happens, but it is safe.
    gpInternalTimeInfo.reset(pNewIti);

    elapsedTime = 0;
    pIti = pNewIti;
  }

  return pIti->mStartSecondsSinceEpoch + ((double)elapsedTime)/1000.0;

#else
  struct timeval tv;
  double current_t;

  gettimeofday( &tv, NULL );

  tv.tv_sec-=952022720;

  current_t = (double)tv.tv_sec + 1e-6*(double)tv.tv_usec;

  return current_t;
#endif
}

double PvUtil::systemTime()
{
#ifdef WIN32
    FILETIME ft;
    GetSystemTimeAsFileTime(&ft);
    LARGE_INTEGER li;
    li.LowPart = ft.dwLowDateTime;
    li.HighPart = ft.dwHighDateTime;
    // The time is in 100-nanosecond units.
    double epochSecs = ((double)li.QuadPart)/10000000;
    return epochSecs;
#else
  PvUtil::exitError("PvUtil::systemTime() is not implemented");
#endif
}

#ifdef WIN32
double 
PvUtil::timeFromTimeGetTime(DWORD windowsTime)
{
  InternalTimeInfo *pIti = gpInternalTimeInfo.get();
  return pIti->mStartSecondsSinceEpoch + ((double)(windowsTime-pIti->mStartTimeGetTime))/1000.0;
}
#endif

unsigned int PvUtil::intLog2(const unsigned int n)
{
  unsigned int m=n,ld=0;

  while((m & 1)==0 && m!=0)
  {
    m=m>>1;
    ld++;
  }

  if(m==0) PvUtil::exitError("PvUtil::intLog2(n): Error, n=%d is not a power of two!\n",n);

  return ld;
}

bool PvUtil::fileExists(const char *fname)
{
#ifdef WIN32  
  return (GetFileAttributes(fname) != 0xFFFFFFFF);
#else
  struct stat b;
  return (stat(fname, &b) == 0);
#endif  
}

void PvUtil::setLog(ait::LogWriter *pLog)
{
  mpLogWriter = pLog;
}

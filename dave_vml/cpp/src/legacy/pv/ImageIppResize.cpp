// The IPP Functions are placed on a separate file so they don't get linked unless
// specifically referenced.

#include <legacy/pv/ait/Image.hpp>
#include "legacy/pv/PvImageIO.hpp"

#ifdef USE_IPP
#include <ipp.h>
#endif //USE_IPP

namespace ait
{

void
Image8::rescale(const Image8& from,
                     const uint w,const uint h, const uint m)
{
#ifdef USE_IPP
  int interpolation;
  
  switch (m)
  {
  case RESCALE_SKIP:
    interpolation = IPPI_INTER_NN;
    break;
  case RESCALE_LINEAR:
    interpolation = IPPI_INTER_LINEAR;
    break;
  case RESCALE_CUBIC:
    interpolation = IPPI_INTER_CUBIC;
    break;
  case RESCALE_SUPER:
    interpolation = IPPI_INTER_SUPER;
    break;
  }

  const int newWidth = w;
  const int newHeight = h;

  const double xFactor = static_cast<double>(newWidth)/from.width();
  const double yFactor = static_cast<double>(newHeight)/from.height();

  resize(newWidth,newHeight);

  IppiSize srcSize = { from.width(), from.height() };
  IppiRect rSrc = { 0, 0, from.width(), from.height() };
  IppiSize dstSize = { newWidth, newHeight };
  ippiResize_8u_C1R(from.pointer(), srcSize, from.width(), rSrc,
                    pointer(), newWidth, dstSize, xFactor, yFactor,
                    interpolation);

#else

  Image<unsigned char>::rescale(from,w,h,m);

#endif // USE_IPP

}

void
Image32::rescale(const Image32& from,
                     const uint w,const uint h, const uint m)
{
#ifdef USE_IPP
  int interpolation;
  
  switch (m)
  {
  case RESCALE_SKIP:
    interpolation = IPPI_INTER_NN;
    break;
  case RESCALE_LINEAR:
    interpolation = IPPI_INTER_LINEAR;
    break;
  case RESCALE_CUBIC:
    interpolation = IPPI_INTER_CUBIC;
    break;
  case RESCALE_SUPER:
    interpolation = IPPI_INTER_SUPER;
    break;
  }

  const int newWidth = w;
  const int newHeight = h;

  const double xFactor = static_cast<double>(newWidth)/from.width();
  const double yFactor = static_cast<double>(newHeight)/from.height();

  resize(newWidth,newHeight);

  IppiSize srcSize = { from.width(), from.height() };
  IppiRect rSrc = { 0, 0, from.width(), from.height() };
  IppiSize dstSize = { newWidth, newHeight };
  IppStatus s = ippiResize_8u_C4R((unsigned char*)from.pointer(), srcSize, from.width()*4, rSrc,
                                   (unsigned char*)pointer(), newWidth*4, dstSize, xFactor, yFactor,
                                   interpolation);

  if (s != ippStsNoErr)
  {
    PvUtil::exitError("Ipp could not resize the image. IPPI_INTER_SUPER is not supported for color images.");
  }
#else
  Image<unsigned int>::rescale(from,w,h,m);
#endif // USE_IPP
}

} // namespace ait

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include "legacy/pv/PvSpline.hpp"

namespace ait
{

/**
 This function builds the spline from points given in a matrix. 
 Each row contains a point to interpolate. The value in the first column
 is the parametric value (e.g: time), and the components of the point are 
 in the next columns. The point can be n-dimensional. For example, 
 to interpolate the following points:

 @code 

 s(0) = (x0,y0,z0), 
 s(0.5) = (x1,y1,z1) and
 s(1) = (x2,y2,z2).

 the matrix is:

 0.0 x0 y0 z0
 0.5 x1 y1 z1
 1.0 x2 y2 z2
 @endcode
*/
void PvSpline::setControlPoints(Matrix<float> &pts)
{
  points=pts;
  
  channels=pts.cols()-1;
  n=pts.rows();
  
  Matrix<float> u(1,n-1);
  
  derivatives.resize(n,channels);
  
  PVASSERT(n > 2);

  for(unsigned int c=1;c<=channels;c++)
  {
    unsigned int i,k;
    float p,qn,sig,un;
    
    derivatives(0,c-1)=0.0f;
    u(0)=0.0f;
    
    for (i=1;i<n-1;i++) 
    {
      sig=(pts(i,0)-pts(i-1,0))/(pts(i+1,0)-pts(i-1,0));
      p=sig*derivatives(i-1,c-1)+2.0f;
      derivatives(i,c-1)=(sig-1.0f)/p;
      u(i)=(pts(i+1,c)-pts(i,c))/(pts(i+1,0)-pts(i,0)) - (pts(i,c)-pts(i-1,c))/(pts(i,0)-pts(i-1,0));
      u(i)=(6.0f*u(i)/(pts(i+1,0)-pts(i-1,0))-sig*u(i-1))/p;
    }
    qn=un=0.0;
    derivatives(n-1,c-1)=(un-qn*u(n-2))/(qn*derivatives(n-2,c-1)+1.0f);
    for (k=n-2;;k--)
    {
      derivatives(k,c-1)=derivatives(k,c-1)*derivatives(k+1,c-1)+u(k);
      if (k == 0) break;
    }
  }
}

/**
 This function evaluates the spline constructed with setControlPoints.
 @param t [in] Parametric value to evaluate the spline.
 @param x [out] Row vector where the interpolate valued will be stored. The
 matrix will be resized if necessary.
 */
void PvSpline::interpolate(float t, Matrix<float> &x)
{
  int klo,khi,k;
  float h,b,a;
  
  x.resize(1,channels);
  
  klo=0;
  khi=n-1;
  while (khi-klo > 1) 
  {
    k=(khi+klo) >> 1;
    if (points(k,0) > t) khi=k;
    else klo=k;
  }
  h=(points(khi,0)-points(klo,0));
  if (h == 0.0) PvUtil::exitError("PvSpline::interpolate: Bad input!");
  
  for(unsigned int c=1;c<=channels;c++)
  {
    a=(points(khi,0)-t)/h;
    b=(t-points(klo,0))/h;
    x(c-1)=a*points(klo,c)+b*points(khi,c)+((a*a*a-a)*derivatives(klo,c-1)+(b*b*b-b)*derivatives(khi,c-1))*(h*h)/6.0f;
  }
}

} // namespace ait

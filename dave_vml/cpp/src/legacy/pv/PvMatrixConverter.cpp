/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include <legacy/pv/ait/Matrix.hpp>
#include "legacy/pv/PvMatrixConverter.hpp"

namespace ait
{

/*********************************************************************
 * convert()
 * 
 * double to float
 *
 *********************************************************************/
void PvMatrixConverter::convert(const Matrix<double> &in, Matrix<float> &out)
{
  unsigned int rs=in.rows(),cs=in.cols(),s=rs*cs;

  double *src;
  double *end;
  float *dst;

  out.resize(rs,cs);

  src=in.pointer();
  dst=out.pointer();
  end=src+s;
 
  while(src<end) *(dst++)=(float)(*(src++));
}

} // namespace ait

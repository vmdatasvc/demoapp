/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include <legacy/pv/ait/Image.hpp>
#include "legacy/pv/PvImageIO.hpp"

#include <string.h>

namespace ait
{

/************************************************************************
 ** 
 **  Image8
 **
 ************************************************************************/

/**
 * Loads image in binary pgm format.
 * @param name [in] file name 
 * @return 'false' if failure, 'true' if success
 * @remark if the allocation of the new image fails, the program exits.
 */
int Image8::load(const char *name)
{
  unsigned char *image,*result;
  unsigned int width, height;

  result=it_loadpgm(name,&image, &width, &height);
  
  if(result!=NULL)
    {
      mx_fromarray(image,height,width);
      freeMat = true;
      return 1;
    }
  else
    {
      PvUtil::exitError("%s file is not found \n",name);
      return 0;
    }
}

/**
 * Saves image in binary pgm format.
 * @param name [in] file name 
 * @return 'false' if failure, 'true' if success
 */
int Image8::save(const char *name)
{
  return it_savepgm(name, mat[0], ncols, nrows);
}

/**
 * Sets the gray image to the specified band of the given 
 * RGB image.
 * 
 * Possible bands:
 * 
 * 'r' = red
 * 'g' = green
 * 'b' = blue
 * 'h' = hue
 * 's' = saturation
 * 'v' = value
 *
 * @param rgbimg [in] rgb image
 * @param band [in] band to extract
 */
void Image8::band(Image32 &rgbimg, char band)
{
  unsigned int i;
  unsigned int w,h,s;
  unsigned char *dst;
  unsigned int *src;

  h=rgbimg.height();
  w=rgbimg.width();
  s=w*h;

  mx_assert(h,w);

  src=rgbimg.pointer();
  dst=pointer();

  if(band<'a') band+='a'-'A';

  switch(band)
    {
    case 'r': for(i=0;i<s;i++) *(dst++)=RED(*(src++));
      break;
    case 'g': for(i=0;i<s;i++) *(dst++)=GREEN(*(src++));
      break;
    case 'b': for(i=0;i<s;i++) *(dst++)=BLUE(*(src++));
      break;
    case 'a': for(i=0;i<s;i++) *(dst++)=ALPHA(*(src++));
      break;
    case 'h': for(i=0;i<s;i++) *(dst++)=HUE(*(src++));
      break;
    case 's': for(i=0;i<s;i++) *(dst++)=SATURATION(*(src++));
      break;
    case 'i':
    case 'v': for(i=0;i<s;i++) *(dst++)=VALUE(*(src++));
      break;
    }
}

void Image8::AddConstantBrightness(unsigned char brightness){

	int	w=width();
	int h=height();
	unsigned char* pixels=pointer();
	int i = 0;
	int temp;

	for (i=0;i<w*h;i++){

		temp = pixels[i] + brightness;

		if (temp>255)
			pixels[i] = 255; 
		else
			pixels[i] = temp;

	}

}

void Image8::SubtractConstantBrightness(unsigned char brightness){

	int	w=width();
	int h=height();
	unsigned char* pixels=pointer();
	int i = 0;
	int temp;

	for (i=0;i<w*h;i++){

		temp = pixels[i] - brightness;

		if (temp<0)
			pixels[i] = 0; 
		else
			pixels[i] = temp;

	}

};

/************************************************************************
 ** 
 **  Image32
 **
 ************************************************************************/
//#include <direct.h>

/**
 * Load image in binary ppm, png or jpg format. It checks the extension
 * of the file name to determine the format. The default (if the extension
 * is not recognized) is ppm.
 * @param name [in] file name 
 * @return 'false' if failure, 'true' if success
 * @remark If the allocation of the new image fails, the program exits.
 */
int Image32::load(const char *name)
{
  unsigned int *image,*result;
  unsigned int width, height;

//  char path[1024];

  if (strlen(name) > 4)
  {
    if (!stricmp(name+strlen(name)-4,".png")) 
    {
      return loadpng(name);
    }
    else if (!stricmp(name+strlen(name)-4,".jpg") || !stricmp(name+strlen(name)-4,"jpeg"))
    {
      return loadjpg(name);
    }
  }

  result=it_loadppm(name,&image, &width, &height);
  
  if(result!=NULL)
  {
    mx_fromarray(image,height,width);
    freeMat = true;
    return 1;
  }
  else
  {
		PvUtil::exitError("%s file is not found \n",name);
    return 0;
  }
}

/**
 * Saves image in binary ppm, png or jpg format. It checks the extension
 * of the file name to determine the format. The default (if the extension
 * is not recognized) is ppm.
 * @param name [in] file name 
 * @return 'false' if failure, 'true' if success
 * @remark No exceptions can occur.
 */
int Image32::save(const char *name)
{
  if (strlen(name) > 4)
  {
    if (!stricmp(name+strlen(name)-4,".png"))
    {
      return savepng(name);
    }
    else if (!stricmp(name+strlen(name)-4,".jpg") || !stricmp(name+strlen(name)-4,"jpeg"))
    {
      return savejpg(name);
    }
  }
  return it_saveppm(name, pointer(), ncols, nrows);
}

/**
 * Loads image in png format.
 * @param name [in] file name 
 * @return 'false' if failure, 'true' if success
 * @remark If the allocation of the new image fails, the program exits.
 */
int Image32::loadpng(const char *name)
{
  unsigned int *image,*result;
  unsigned int width, height;
  
  result=it_loadpng(name,&image, &width, &height, &alpha);
  
  if(result!=NULL)
  {
    mx_fromarray(image,height,width);
    freeMat = true;
    return 1;
  }
  else
  {
    PvUtil::exitError("%s file is not found \n",name);
    return 0;
  }
}

/**\
 * Saves image in binary png format.
 * @param name [in] file name 
 * @return 'false' if failure, 'true' if success
 */
int Image32::savepng(const char *name)
{
  return it_savepng(name, pointer(), ncols, nrows, alpha);
}

/**
 * Loads image in JPG format.
 * @param name [in] file name 
 * @return 'false' if failure, 'true' if success
 * @remark If the allocation of the new image fails, the program exits.
 */
int Image32::loadjpg(const char *name)
{
  unsigned int *image,*result;
  unsigned int width, height;

  // loadjpg currently has error, so blocked. resolve it later
//  result=it_loadjpg(name,&image, &width, &height);
  
  if(result!=NULL)
    {
      mx_fromarray(image,height,width);
      freeMat = true;
      return 1;
    }
  else
    {
			PvUtil::exitError("%s file is not found \n",name);
       return 0;
    }
}

/**
 * Saves image in binary JPG format.
 * @param name [in] file name 
 * @return 'false' if failure, 'true' if success
 */
int Image32::savejpg(const char *name, const int quality)
{
  return it_savejpg(name, pointer(), ncols, nrows, quality);
}

/**
 * Swaps the color channels from RGB to BGR or viceversa. It checks
 * for the alpha flag. If this image is from different size, it will
 * resize it.
 * @param src [in] image to be swapped
 */
void Image32::swapChannels(const Image32& src)
{
  resize(src.width(),src.height());
  int memSize = src.width()*src.height();
  unsigned int * pSrc = src.pointer();
  unsigned int * pDst = pointer();

  if (src.alpha) {

    // The original file has an alpha channel defined. Swap channels and
    // keep alpha values.
    while (memSize > 0) {
      *pDst = (*pSrc & 0xFF000000) |
              ((*pSrc & 0xFF) <<16) |
              (*pSrc & 0xFF00) |
              ((*pSrc & 0xFF0000) >>16);
      pSrc++;
      pDst++;
      memSize--;
    }

  } else {

    // The image doesn't have alpha channel. Swap the buffers and set
    // alpha to 1 (0xFF for 8 bits)
    while (memSize > 0) {
      *pDst = 0xFF000000 |
              ((*pSrc & 0xFF) <<16) |
              (*pSrc & 0xFF00) |
              (*pSrc & 0xFF0000) >>16;
      pSrc++;
      pDst++;
      memSize--;
    }

  }

  alpha = src.alpha;
  internalFormat = src.internalFormat == PV_RGBA ? PV_BGRA : PV_RGBA;
}

void Image32::swapChannels(const Image32& src, Rectanglef& subRegion)
{
  PVASSERT(subRegion.x0 >= 0 && subRegion.y0 >= 0 &&
           subRegion.x1 <= 1 && subRegion.y1 <= 1 &&
           subRegion.x0 <= subRegion.x1 &&
           subRegion.y0 <= subRegion.y1);

  resize((int)(subRegion.width()*src.width()), (int)(subRegion.height()*src.height()));
  int memSize = ((int)(subRegion.width()*src.width()))*(int)((subRegion.height()*src.height()));
  unsigned int * pSrc = src.pointer();
  unsigned int * pDst = pointer();

	pSrc += src.width()*(int)(subRegion.y0*src.height())+(int)(subRegion.x0*src.width());

  unsigned int width = (unsigned int)(subRegion.width()*src.width());

  if (src.alpha) {
    
		unsigned int count = 0;

    // The original file has an alpha channel defined. Swap channels and
    // keep alpha values.
    while (memSize > 0) {
			if (count < width) {
        *pDst = (*pSrc & 0xFF000000) |
              ((*pSrc & 0xFF) <<16) |
              (*pSrc & 0xFF00) |
              ((*pSrc & 0xFF0000) >>16);
        pSrc++;
        pDst++;
        memSize--;
			  count++;
			}
			else if (count < src.width()) {
				count++;
				pSrc++;
			}
			else {
				count = 0;
			}
    }

  } else {

		unsigned int count = 0;

    // The image doesn't have alpha channel. Swap the buffers and set
    // alpha to 1 (0xFF for 8 bits)
    while (memSize > 0) {
      if (count < width) {
			  *pDst = 0xFF000000 |
              ((*pSrc & 0xFF) <<16) |
              (*pSrc & 0xFF00) |
              (*pSrc & 0xFF0000) >>16;
        pSrc++;
        pDst++;
        memSize--;
				count++;
			}
			else if (count < src.width()) {
				count++;
				pSrc++;
			}
			else {
				count = 0;
			}
    }

  }

  alpha = src.alpha;
  internalFormat = src.internalFormat == PV_RGBA ? PV_BGRA : PV_RGBA;
}

/**
 * Local histogram equalization
 * @param radius size of the local window: 12 for 24x24 face
 */
#define MAX_RADIUS 50
void
Image8::Lequalize(Image8 from, int radius)
{
  int width = from.width();
  int height = from.height();
  int xOffBuf[ 2 * MAX_RADIUS + 1 ];
  int *xOff = xOffBuf + MAX_RADIUS;
  int yOff;
  int hist[ 256 ];
  int numBelow;
  int windowSize;
  int p, prevP;
  int x, y;
  int x1, y1;
  float v;

  if( radius > MAX_RADIUS )
  {
	  std::cerr<<"Radius too large in Lequalize(): "<<radius<<"\n";
    exit( 1 );
  }

  for( yOff = -radius; yOff <= radius; yOff++ )
    xOff[ yOff ] = sqrt( (float)(radius * radius - yOff * yOff) );

  for( y = 0; y < height; y++ )
  {
    memset( hist, 0, sizeof( hist ) );
    numBelow = 0;
    prevP = 0;
      
    for( x = -radius; x < width; x++ )
	  {
	    windowSize = 0;
	    for( yOff = -radius; yOff <= radius; yOff++ )
	    {
	      y1 = y + yOff;

        if( 0 > y1 || y1 >= height )
		      continue;

   		  windowSize += std::min( width - 1, x + xOff[ yOff ] ) -
	      std::max( 0, x - xOff[ yOff ] ) + 1;

        x1 = x - xOff[ yOff ] - 1;

        if( x1 >= 0 )
	      {
	        p = from( x1, y1 );
		      hist[ p ]--;
		      numBelow -= (p < prevP);  
        }

    	  x1 = x + xOff[ yOff ];

	      if( x1 < width && x1 >= 0 ) // orig: if( x1 < width )
		    {
		      p = from( x1, y1 );
		      hist[ p ]++;
		      numBelow += (p < prevP);
		    }
 	    }
	  
      if( 0 <= x )
      {
        p = from( x, y );
	      while( prevP < p )
	        numBelow += hist[ prevP++ ];
	      while( prevP > p )
	        numBelow -= hist[ --prevP ];

        if( hist[ p ] == windowSize )
      		mat[y][x] = 128;
	      else
		    {
		      v = 255. * numBelow / (windowSize - hist[ p ]);
		      if( v <= 0 )
		        mat[y][x] = 0;
		      else if( v >= 254.5 )
		        mat[y][x] = 255;
		      else
		        mat[y][x] = (int)(v + 0.5);
		     }
	     }
	   }
   }
}

/**
 * Image rotate -> scale -> shift -> frame : x' = M_scale*M_rotate*(x - old_center) + tx + new_center
 * The expression inside the loop is derived using the inverse transform of the above:
 * x = M_scale^(-1)*M_rotate^(-1)*(x' - new_center - tx) + old_center
 */
void Image32::centerRotateRescaleShift(const Image32& from,
                     const float rotation, const float scaleFactor, 
					 const float xCenter, const float yCenter,
					 const float xShift, const float yShift,
					 const uint frameWidth, const uint frameHeight)
{
  int inWidth, inHeight;
  inWidth  = from.width();
  inHeight = from.height();

  int inHalfWidth = (int)(inWidth / 2);
  int inHalfHeight = (int)(inHeight / 2);

  int frameHalfWidth = (int)(frameWidth / 2);
  int frameHalfHeight = (int)(frameHeight / 2);

  double cost = cos(rotation);
  double sint = sin(rotation);

  resize(frameWidth, frameHeight);

  float fx, fy, tx, ty, a, b;
  int ix, iy;
  unsigned int pix1, pix2, pix3, pix4;
  const double K = 1.0 / scaleFactor; 

  unsigned int neutralLevel = PV_RGB(127,127,127);

  for(int y=0; y<frameHeight; y++)
    for(int x=0; x<frameWidth; x++)
      {
		tx = x - frameHalfWidth - xShift + 0.5f;
		ty = y - frameHalfHeight - yShift + 0.5f;

		fx = K*(tx*cost + ty*sint) + xCenter - 0.5f;
		fy = K*(-tx*sint + ty*cost) + yCenter - 0.5f;
	
		ix = (int)floor(fx);
		iy = (int)floor(fy);

		a = fy - (float)floor(fy);
		b = fx - (float)floor(fx);

		if(0<= ix+0 && ix+0 < inWidth && 0<= iy+0 && iy+0 < inHeight)
		  pix1 = from(ix+0, iy+0);
		else
		  pix1 = neutralLevel;

		if(0<= ix+1 && ix+1 < inWidth && 0<= iy+0 && iy+0 < inHeight)
		  pix2 = from(ix+1, iy+0);
		else
		  pix2 = neutralLevel;

		if(0<= ix+0 && ix+0 < inWidth && 0<= iy+1 && iy+1 < inHeight)
		  pix3 = from(ix+0, iy+1);
		else
      pix3 = neutralLevel;

		if(0<= ix+1 && ix+1 < inWidth && 0<= iy+1 && iy+1 < inHeight)
		  pix4 = from(ix+1, iy+1);
		else
		  pix4 = neutralLevel;

		float a1b1 = (1.0f-a)*(1.0f-b);
		float a1b = (1.0f-a)*b;
		float ab1 = a*(1.0f-b);
		float ab = a*b;

    float rr = 0.5f + a1b1*RED(pix1) + a1b*RED(pix2) + ab1*RED(pix3) + ab*RED(pix4);
    float gg = 0.5f + a1b1*GREEN(pix1) + a1b*GREEN(pix2) + ab1*GREEN(pix3) + ab*GREEN(pix4);
    float bb = 0.5f + a1b1*BLUE(pix1) + a1b*BLUE(pix2) + ab1*BLUE(pix3) + ab*BLUE(pix4);

    (*this)(x,y) = static_cast<unsigned int>(PV_RGB((unsigned char)(rr),(unsigned char)(gg),(unsigned char)(bb)));
	}
}

/**
 * Image rotate -> scale -> shift -> frame : x' = M_scale*M_rotate*(x - old_center) + tx + new_center
 * The expression inside the loop is derived using the inverse transform of the above:
 * x = M_scale^(-1)*M_rotate^(-1)*(x' - new_center - tx) + old_center
 */
void Image8::centerRotateRescaleShift(const Image8& from,
                     const float rotation, const float scaleFactor, 
					 const float xCenter, const float yCenter,
					 const float xShift, const float yShift,
					 const uint frameWidth, const uint frameHeight)
{
  int inWidth, inHeight;
  inWidth  = from.width();
  inHeight = from.height();

  int inHalfWidth = (int)(inWidth / 2);
  int inHalfHeight = (int)(inHeight / 2);

  int frameHalfWidth = (int)(frameWidth / 2);
  int frameHalfHeight = (int)(frameHeight / 2);

  double cost = cos(rotation);
  double sint = sin(rotation);

  resize(frameWidth, frameHeight);

  float fx, fy, tx, ty, a, b;
  int ix, iy;
  unsigned char pix1, pix2, pix3, pix4;
  const double K = 1.0 / scaleFactor; 

  unsigned char neutralLevel = 127;

  for(int y=0; y<frameHeight; y++)
    for(int x=0; x<frameWidth; x++)
      {
		tx = x - frameHalfWidth - xShift + 0.5f;
		ty = y - frameHalfHeight - yShift + 0.5f;

		fx = K*(tx*cost + ty*sint) + xCenter - 0.5f;
		fy = K*(-tx*sint + ty*cost) + yCenter - 0.5f;
	
		ix = (int)floor(fx);
		iy = (int)floor(fy);

		a = fy - (float)floor(fy);
		b = fx - (float)floor(fx);

		if(0<= ix+0 && ix+0 < inWidth && 0<= iy+0 && iy+0 < inHeight)
		  pix1 = from(ix+0, iy+0);
		else
		  pix1 = neutralLevel;

		if(0<= ix+1 && ix+1 < inWidth && 0<= iy+0 && iy+0 < inHeight)
		  pix2 = from(ix+1, iy+0);
		else
		  pix2 = neutralLevel;

		if(0<= ix+0 && ix+0 < inWidth && 0<= iy+1 && iy+1 < inHeight)
		  pix3 = from(ix+0, iy+1);
		else
      pix3 = neutralLevel;

		if(0<= ix+1 && ix+1 < inWidth && 0<= iy+1 && iy+1 < inHeight)
		  pix4 = from(ix+1, iy+1);
		else
		  pix4 = neutralLevel;

		float a1b1 = (1.0f-a)*(1.0f-b);
		float a1b = (1.0f-a)*b;
		float ab1 = a*(1.0f-b);
		float ab = a*b;

    float rr = 0.5f + a1b1*pix1 + a1b*pix2 + ab1*pix3 + ab*pix4;
    (*this)(x,y) = static_cast<unsigned char>(rr);
	}
}

// This is used to declare destructors on this object file, and
// avoid unused libraries to be loaded by the linker.
static void cppDestructors()
{
  {
    Matrix<float>* X = new Matrix<float>[1];
    delete [] X;
  }
  {
    Matrix<double>* X = new Matrix<double>[1];
    delete [] X;
  }
  {
    Image32* X = new Image32[1];
    delete [] X;
  }
  {
    Vector3<int>* X = new Vector3<int>[1];
    delete [] X;
  }
}

} // namespace ait

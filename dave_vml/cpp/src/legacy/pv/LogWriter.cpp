/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "legacy/pv/LogWriter.hpp"

#include <legacy/pv/PvUtil.hpp>

#include <time.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>

namespace ait 
{

LogWriter::LogWriter():
mStream(NULL),
mFilename("")
{
}

LogWriter::LogWriter(const std::string& filename, bool logTime): 
mStream(NULL)
{
	mFilename=filename;
	mLogTime = logTime;
}

void 
LogWriter::write(const std::string& str)
{
	
	// This code was taken from PvUtil::exitError().
	if(mLogTime)
	{
		_strdate( mDbuffer );
		_strtime( mTbuffer );
	}
	
	if (mStream)
	{
		if(mLogTime)
			fprintf(mStream, "%s %s     %s\n", mDbuffer, mTbuffer, str.c_str()); //returns a negative if there are errors during the write.    
		else
			fprintf(mStream, "%s\n", str.c_str()); //returns a negative if there are errors during the write.    
	}
	else
		if(mFilename!="")
			mStream = fopen(mFilename.c_str(), "a"); //open the file for appending
		else
			mStream = fopen("./main.log", "a"); //open the file for appending
}

void
LogWriter::flush()
{
	fflush(mStream);
}

LogWriter::~LogWriter()
{
	if (mStream) fclose(mStream);    
}

//
// OPERATIONS
//

//
// ACCESS
//

//
// INQUIRY
//



}; // namespace ait


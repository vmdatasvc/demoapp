// The IPP Functions are placed on a separate file so they don't get linked unless
// specifically referenced.

#include <legacy/pv/ait/Image.hpp>
#include "legacy/pv/PvImageIO.hpp"

#ifdef USE_IPP
#include <ipp.h>
#endif //USE_IPP

namespace ait
{

/**
 * Rotates image around its center.
 * @param from [in] Input Image 
 * @param angle [in] Angle by which image is rotated
 * @param mode [in] Method for interpolating pixels
 * @remark Currently uses IPP's rotate method only. If IPP is not installed or defined this method will be empty.
 */
  void Image32::rotate(const Image32& from, const double angle, const unsigned int bkColor, const uint mode)
  {
#ifdef USE_IPP
	  int interpolation;
	  
	  switch (mode)
	  {
	  case RESCALE_SKIP:
		  interpolation = IPPI_INTER_NN;
		  break;
	  case RESCALE_LINEAR:
		  interpolation = IPPI_INTER_LINEAR;
		  break;
	  case RESCALE_CUBIC:
		  interpolation = IPPI_INTER_CUBIC;
		  break;
	  case RESCALE_SUPER:
		  interpolation = IPPI_INTER_SUPER;
		   PvUtil::exitError("Ipp could not rotate the image. IPPI_INTER_SUPER is not supported for color images.");
		  break;
	  }

	  setAll(bkColor);

	  IppiSize srcSize = { from.width(), from.height() };
	  IppiRect rSrc = { 0, 0, from.width(), from.height() };
	  IppiRect rDst = {  0, 0, from.width(), from.height() };

	  IppStatus s = ippiRotateCenter_8u_C4R((unsigned char*)from.pointer(), srcSize, from.width()*4, rSrc,
		  (unsigned char*)pointer(), from.width()*4, rDst,
			angle, 0.5*from.width(), 0.5*from.height(), interpolation);

	  if (s != ippStsNoErr)
	  {
		  PvUtil::exitError("Ipp could not rotate the image (error code = %d).", s);
	  }
#else
	  
#endif // USE_IPP
  }

} // namespace ait

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "legacy/types/String.hpp"

extern "C" {
#include <stdio.h>
#include <stdarg.h>
};

namespace ait 
{

static const int MAX_BUFFER_SIZE = 4096;

std::string
aitSprintf(const char* fmt, ...)
{
  char buf[MAX_BUFFER_SIZE];
  va_list ap;
  va_start( ap, fmt );

#ifdef WIN32
  _vsnprintf (buf, MAX_BUFFER_SIZE, fmt, ap);
#else
  vsnprintf (buf, MAX_BUFFER_SIZE, fmt, ap);
#endif

  va_end (ap);

  return std::string(buf);
}

//
// OPERATIONS
//

//
// ACCESS
//

//
// INQUIRY
//

}; // namespace ait


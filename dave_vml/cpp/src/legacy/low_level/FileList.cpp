/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#pragma warning(disable:4503)
#endif

#include <legacy/types/strutil.hpp>

#include "legacy/low_level/FileList.hpp"

#ifdef WIN32
#include <windows.h>
#else
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fnmatch.h>
#endif

using namespace std;
using namespace ait;

static string composePath(const string& dir, const string& fname)
{
  if (fname.empty()) return dir;
  if (dir.empty() || dir == ".") return fname;
  if (dir[dir.length()-1] == '\\' || dir[dir.length()-1] == '/') return dir + fname;
#ifdef WIN32  
  return dir + "\\" + fname;
#else
  return dir + "/" + fname;
#endif
}

std::vector<std::string> 
FileList::getFileNames(const std::string& path0, const std::string& pattern0 )
{
  using namespace std;

  string path = normalize_path(path0);
  string pattern = normalize_path(pattern0);

  if (pattern0.empty())
  {
    string::size_type pos = path.rfind('/');
    if (pos == string::npos)
    {
      pattern = path;
      path = "";
    }
    else
    {
      pattern = path.substr(pos+1);
      path = path.substr(0,pos);
    }
  }

  std::vector<std::string> fileNames;

  string::size_type pos = pattern.find(';');

  if (pos != string::npos)
  {
    fileNames = getFileNames(path,pattern.substr(0,pos));
    std::vector<std::string> fileNamesTmp;
    fileNamesTmp = getFileNames(path,pattern.substr(pos+1));
    fileNames.insert(fileNames.end(), fileNamesTmp.begin(), fileNamesTmp.end());
    return fileNames;
  }

#ifdef WIN32  
  WIN32_FIND_DATA FileData; 
  HANDLE hSearch;
  string p = composePath(path,pattern);
 
  bool fFinished = false; 
 
  hSearch = FindFirstFile(p.c_str(), &FileData); 
  if (hSearch != INVALID_HANDLE_VALUE)
  { 
    while (!fFinished)
    { 
      if (!(FileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
      {
        fileNames.push_back(composePath(path,FileData.cFileName));
      }
      if (!FindNextFile(hSearch, &FileData)){
        fFinished = true;
      }
    } 
    // Close the search handle. 
    FindClose(hSearch); 
  }
#else
  DIR *_dir;

  _dir = opendir(path.c_str());
  if (_dir != NULL)
  {
    struct dirent *dp;
    int ndx;
    
    // in order to skip the . and the .. entries
    dp = readdir(_dir);
    dp = readdir(_dir);
    
    // now iterate through the entries
    for (ndx = 2, dp = readdir(_dir); dp != NULL; dp = readdir(_dir), ++ndx)
    {
      if ((pattern.length() == 0) || (fnmatch(pattern.c_str(), dp->d_name, 0) == 0))
      {
        fileNames.push_back(path + "/" + dp->d_name);
      }
    }
    closedir(_dir);
  }
#endif  

  return fileNames;
}


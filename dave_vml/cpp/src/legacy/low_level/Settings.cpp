/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include <list>
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>
//#include <cmd_line_utils.hpp>

#include "legacy/low_level/Settings.hpp"
#include "legacy/low_level/MemorySettings.hpp"

namespace ait 
{

Settings	*Settings::sDefaultSettings = NULL;

Settings::Settings(const std::string &appPath)
{
    mCurrentPath = appPath;
}

Settings::~Settings()
{
}

Settings& Settings::operator =(const Settings &set)
{
	return *this;
}

//
// OPERATIONS
//

std::string
Settings::getString(const std::string& key, 
                       const std::string& defaultValue, 
                       bool writeDefault)
{
//  std::string fullPath = mCurrentPath.empty() ? key : mCurrentPath + '\\' + key;
	std::string fullPath = mCurrentPath.empty() ? key : mCurrentPath + '/' + key;
  return MemorySettings::instance()->getString(fullPath,defaultValue,writeDefault);
//	std::string& ret = table[fullPath];

//	if (writeDefault)
//	{
//		ret = defaultValue;
//	}
//
//	return ret;
}

std::string
Settings::getString(const std::string& key)
{
//  std::string fullPath = mCurrentPath.empty() ? key : mCurrentPath + '\\' + key;
	std::string fullPath = mCurrentPath.empty() ? key : mCurrentPath + '/' + key;
    return MemorySettings::instance()->getString(fullPath);
//	std::string &ret = table[fullPath];
//	return ret;
}

void
Settings::setString(const std::string& key,
                       const std::string& val)
{
//  std::string fullPath = mCurrentPath.empty() ? key : mCurrentPath + '\\' + key;
	std::string fullPath = mCurrentPath.empty() ? key : mCurrentPath + '/' + key;
//
  MemorySettings::instance()->setString(fullPath,val);
//	table[fullPath] = val;
}

bool
Settings::getBool(const std::string& key,
                     const bool defaultValue,
                     bool writeDefault)
{
  std::string buf;
  buf = getString(key,"",false);
  if (buf.empty()) {
    if (writeDefault) setBool(key,defaultValue);
    return defaultValue;
  } else {
    return atoi(buf.c_str()) == 1;
  }
}

bool
Settings::getBool(const std::string& key)
{
  std::string buf;
  buf = getString(key);
  return atoi(buf.c_str()) == 1;
}

void
Settings::setBool(const std::string& key,
                     const bool val)
{
  char buf[4];
  sprintf(buf,"%d",(int)val);
  setString(key,buf);
}

int
Settings::getInt(const std::string& key,
                    const int defaultValue,
                    bool writeDefault)
{
  std::string buf;
  buf = getString(key,"",false);
  if (buf.empty()) {
    if (writeDefault) setInt(key,defaultValue);
    return defaultValue;
  } else {
    return atoi(buf.c_str());
  }
}

int
Settings::getInt(const std::string& key)
{
  std::string buf;
  buf = getString(key);
  return atoi(buf.c_str());
}

void
Settings::setInt(const std::string& key,
                    const int val)
{
  char buf[32];
  sprintf(buf,"%d",val);
  setString(key,buf);
}

std::list<int>
Settings::getIntList(const std::string& key,
           const std::string& defaultValue,
           bool writeDefault)
{
  std::string buf;
  std::list<int> result;

  boost::tokenizer<boost::char_separator<char> >::const_iterator iToken;  
  boost::char_separator<char> delim(",");
  
  buf = getString(key,"",false);
  if (buf.empty())
  {
    if (writeDefault) setString(key,defaultValue);
    
    boost::tokenizer<boost::char_separator<char> > doubleTokens(defaultValue, delim);
    
    for (iToken = doubleTokens.begin(); iToken != doubleTokens.end(); iToken++)         
      result.push_back(boost::lexical_cast<int>(*iToken));     
    return result;    
  }
  else
  {
    boost::tokenizer<boost::char_separator<char> > doubleTokens(buf, delim);
    for (iToken = doubleTokens.begin(); iToken != doubleTokens.end(); iToken++)
      result.push_back(boost::lexical_cast<int>(*iToken)); 
    return result;  
  }  
}


void 
Settings::setIntList(const std::string& key, const std::list<int> values)
{
  std::string valString = "";

  std::list<int>::const_iterator iVal;
  for (iVal = values.begin(); iVal != values.end(); iVal++)  
    valString += boost::lexical_cast<std::string>(*iVal) + ",";
  //remove trailing ' '
  valString = valString.substr(0, valString.length() - 1);
  setString(key,valString);  
}

double 
Settings::getDouble(const std::string& key)
{
  std::string buf;
  buf = getString(key);
  return atof(buf.c_str());
}

double
Settings::getDouble(const std::string& key,
                       const double defaultValue,
                       bool writeDefault)
{
  std::string buf;
  buf = getString(key,"",false);
  if (buf.empty()) {
    if (writeDefault) setDouble(key,defaultValue);
    return defaultValue;
  } else {
    return atof(buf.c_str());
  }
}


void
Settings::setDouble(const std::string& key,
                       const double val)
{
  char buf[32];
  sprintf(buf,"%lg",val);
  setString(key,buf);
}

std::list<double> 
Settings::getDoubleList(const std::string& key, 
           const std::string& defaultValue, 
           bool writeDefault)
{
  std::string buf;
  std::list<double> result;

  boost::tokenizer<boost::char_separator<char> >::const_iterator iToken;  
  boost::char_separator<char> delim(",");
  
  buf = getString(key,"",false);
  if (buf.empty())
  {
    if (writeDefault) setString(key,defaultValue);
    
    boost::tokenizer<boost::char_separator<char> > doubleTokens(defaultValue, delim);
    
    for (iToken = doubleTokens.begin(); iToken != doubleTokens.end(); iToken++)         
      result.push_back(boost::lexical_cast<double>(*iToken));     
    return result;    
  }
  else
  {
    boost::tokenizer<boost::char_separator<char> > doubleTokens(buf, delim);
    for (iToken = doubleTokens.begin(); iToken != doubleTokens.end(); iToken++)        
      result.push_back(boost::lexical_cast<double>(*iToken)); 
    return result;  
  }  
}

void 
Settings::setDoubleList(const std::string& key, const std::list<double> values)
{
  std::string valString = "";

  std::list<double>::const_iterator iVal;
  for (iVal = values.begin(); iVal != values.end(); iVal++)  
    valString += boost::lexical_cast<std::string>(*iVal) + ",";
  //remove trailing ' '
  valString = valString.substr(0, valString.length() - 1);

  setString(key,valString);  
}

Vector3f
Settings::getCoord3f(const std::string& key, 
                        const Vector3f& defaultValue, 
                        bool writeDefault)
{
  char buf[100];

  sprintf(buf,"(%lg,%lg,%lg)",defaultValue(0),
          defaultValue(1),defaultValue(2));
  std::string str = getString(key,buf,writeDefault);

  float v[3];
  sscanf(str.c_str(),"(%f,%f,%f)",&v[0],&v[1],&v[2]);
  return Vector3f(v[0],v[1],v[2]);
}

Vector3f
Settings::getCoord3f(const std::string& key)
{
  std::string str = getString(key);
  float v[3];
  sscanf(str.c_str(),"(%f,%f,%f)",&v[0],&v[1],&v[2]);
  return Vector3f(v[0],v[1],v[2]);
}

Vector3i
Settings::getCoord3i(const std::string& key, 
                        const Vector3i& defaultValue, 
                        bool writeDefault)
{
  char buf[100];

  sprintf(buf,"(%d,%d,%d)",defaultValue(0),
          defaultValue(1),defaultValue(2));
  std::string str = getString(key,buf,writeDefault);

  int v[3];
  sscanf(str.c_str(),"(%i,%i,%i)",&v[0],&v[1],&v[2]);
  return Vector3i(v[0],v[1],v[2]);
}

Vector3i
Settings::getCoord3i(const std::string& key)
{
  std::string str = getString(key);
  int v[3];
  sscanf(str.c_str(),"(%i,%i,%i)",&v[0],&v[1],&v[2]);
  return Vector3i(v[0],v[1],v[2]);
}

void
Settings::setCoord3f(const std::string& key,
                        const Vector3f& val)
{
  char buf[200];
  sprintf(buf,"(%lg,%lg,%lg)",val(0),val(1),val(2));
  setString(key,buf);
}

void
Settings::setCoord3i(const std::string& key,
                        const Vector3i& val)
{
  char buf[200];
  sprintf(buf,"(%i,%i,%i)",val(0),val(1),val(2));
  setString(key,buf);
}

Vector2f
Settings::getCoord2f(const std::string& key,
                        const Vector2f& defaultValue, 
                        bool writeDefault)
{
  char buf[100];

  sprintf(buf,"(%lg,%lg)",defaultValue(0),
          defaultValue(1));
  std::string str = getString(key,buf,writeDefault);

  float v[2];
  sscanf(str.c_str(),"(%f,%f)",&v[0],&v[1]);
  return Vector3f(v[0],v[1]);
}

Vector2f
Settings::getCoord2f(const std::string& key)
{
  std::string str = getString(key);
  float v[2];
  sscanf(str.c_str(),"(%f,%f)",&v[0],&v[1]);
  return Vector3f(v[0],v[1]);
}

void
Settings::setCoord2f(const std::string& key, 
                        const Vector2f& val)
{
  char buf[200];
  sprintf(buf,"(%lg,%lg)",val(0),val(1));
  setString(key,buf);
}

Vector2i
Settings::getCoord2i(const std::string& key,
                        const Vector2i& defaultValue, 
                        bool writeDefault)
{
  char buf[100];

  sprintf(buf,"(%i,%i)",defaultValue(0),
          defaultValue(1));
  std::string str = getString(key,buf,writeDefault);

  int v[2];
  sscanf(str.c_str(),"(%i,%i)",&v[0],&v[1]);
  return Vector2i(v[0],v[1]);
}

Vector2i
Settings::getCoord2i(const std::string& key)
{
  std::string str = getString(key);
  int v[2];
  sscanf(str.c_str(),"(%i,%i)",&v[0],&v[1]);
  return Vector2i(v[0],v[1]);
}

void
Settings::setCoord2i(const std::string& key, 
                        const Vector2i& val)
{
  char buf[200];
  sprintf(buf,"(%i,%i)",val(0),val(1));
  setString(key,buf);
}

Rectanglef
Settings::getRectf(const std::string& key, 
                           const Rectanglef& defaultValue, 
                           bool writeDefault)
{
  char buf[100];

  sprintf(buf,"(%lg,%lg,%lg,%lg)",defaultValue.x0,defaultValue.y0,
                                  defaultValue.x1,defaultValue.y1);
  std::string str = getString(key,buf,writeDefault);

  Rectanglef r;
  sscanf(str.c_str(),"(%f,%f,%f,%f)",&r.x0,&r.y0,&r.x1,&r.y1);
  return r;
}

Rectanglef
Settings::getRectf(const std::string& key)
{
  std::string str = getString(key);
  Rectanglef r;
  sscanf(str.c_str(),"(%f,%f,%f,%f)",&r.x0,&r.y0,&r.x1,&r.y1);
  return r;
}

void
Settings::setRectf(const std::string& key, const Rectanglef& val)
{
  char buf[200];
  sprintf(buf,"(%lg,%lg,%lg,%lg)",val.x0,val.y0,val.x1,val.y1);
  setString(key,buf);
}

Rectanglei
Settings::getRecti(const std::string& key, 
                           const Rectanglei& defaultValue, 
                           bool writeDefault)
{
  char buf[100];

  sprintf(buf,"(%i,%i,%i,%i)",defaultValue.x0,defaultValue.y0,
                                  defaultValue.x1,defaultValue.y1);
  std::string str = getString(key,buf,writeDefault);

  Rectanglei r;
  sscanf(str.c_str(),"(%i,%i,%i,%i)",&r.x0,&r.y0,&r.x1,&r.y1);
  return r;
}

Rectanglei
Settings::getRecti(const std::string& key)
{
  std::string str = getString(key);
  Rectanglei r;
  sscanf(str.c_str(),"(%i,%i,%i,%i)",&r.x0,&r.y0,&r.x1,&r.y1);
  return r;
}

void
Settings::setRecti(const std::string& key, const Rectanglei& val)
{
  char buf[200];
  sprintf(buf,"(%i,%i,%i,%i)",val.x0,val.y0,val.x1,val.y1);
  setString(key,buf);
}

void
Settings::deleteValue(const std::string& key)
{
  std::string fullPath = mCurrentPath.empty() ? key : mCurrentPath + '/' + key;
  return MemorySettings::instance()->deleteValue(fullPath);
//	table.erase(key);
}

void
Settings::changePath(std::string newPath)
{ 
  mCurrentPath = newPath;
}

std::string
Settings::replaceVars(const std::string& value, const std::string& currentPath)
{
//  return MemorySettings::instance()->replaceVars(value,currentPath);
}

void
Settings::parseCommandLine(const std::string& cmdLine)
{
//  CommandLineDict dict = parse_args(cmdLine);
//  if (dict.find("--settings") != dict.end())
//  {
//    parseXml(dict["--settings"][0]);
//  }
//  else if (dict.find("") != dict.end())
//  {
//    parseXml(dict[""][0]);
//  }
}

void
Settings::parseXml(const std::string& fname /* = "settings.xml" */, bool merge)
{
  MemorySettings::instance()->parseXml(fname, merge);
}

void
Settings::parseMemoryXml(const std::string& xmlStr, bool merge)
{
  MemorySettings::instance()->parseMemoryXml(xmlStr,merge);
}

void
Settings::reset()
{
  MemorySettings::instance()->reset();
}

void
Settings::setMemorySettings()
{
  MemorySettings::instance()->setMemorySettings();
}

void
Settings::writeXml(const std::string& fname, bool resolveMacros)
{
  MemorySettings::instance()->writeXml(fname,resolveMacros);
}

void
Settings::writeXml(bool resolveMacros)
{
  MemorySettings::instance()->writeXml(std::cout, resolveMacros);
}

}; // namespace ait


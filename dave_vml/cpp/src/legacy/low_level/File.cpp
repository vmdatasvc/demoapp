/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
#include "legacy/low_level/File.hpp"

#include <sys/types.h>
#include <sys/stat.h>
#include "legacy/pv/PvUtil.hpp"

#ifdef WIN32
#include <windows.h>
#endif

using namespace std;
using namespace ait;

void 
File::readTextFileToMemory(const std::string& fname, std::string& stringData)
{
  FILE *fp = fopen(fname.c_str(),"r");

  if (!fp)
  {
    throw File::file_not_found();
  }

  stringData = "";

  // Read 64K blocks. Unlike the binary version, for small
  // increments of the string, the runtime performance degrades
  // substantially.
  const int blockSize = 1024*64;

  int bytesRead = 0;

  while (!feof(fp))
  {
    const int newSize = stringData.length()+blockSize;
    stringData.resize(newSize);
    bytesRead = fread(&stringData[stringData.length()-blockSize],1,blockSize,fp);
  }

  if (!stringData.empty())
  {
    stringData.resize(stringData.length()-(blockSize-bytesRead));
  }

  fclose(fp);
}

void 
File::writeMemoryToTextFile(const std::string& fname, const std::string& data)
{
  FILE *fp = fopen(fname.c_str(),"w");

  if (!fp)
  {
    throw File::write_error();
  }

  if (!data.empty())
  {
    int numBytesWritten = fwrite(&data[0],1,data.size(),fp);

    if (numBytesWritten < data.size())
    {
      throw File::write_error();
    }
  }

  if (fclose(fp) != 0)
  {
    throw File::write_error();
  }
}

void 
File::readBinaryFileToMemory(const std::string& fname, std::vector<unsigned char>& data, int maxBytes)
{
  FILE *fp = fopen(fname.c_str(),"rb");

  if (!fp)
  {
    throw File::file_not_found();
  }

  data.clear();

  int blockSize = 1024*8;

  if (maxBytes > 0)
  {
    blockSize = std::min(maxBytes,blockSize);
  }

  int bytesRead = 0;

  while (!feof(fp) && (maxBytes == 0 || data.size() < maxBytes))
  {
    data.resize(data.size()+blockSize);
    bytesRead = fread(&data[data.size()-blockSize],1,blockSize,fp);
  }

  if (!data.empty())
  {
    data.resize(data.size()-(blockSize-bytesRead));

    if (maxBytes > 0 && data.size() > maxBytes)
    {
      data.resize(maxBytes);
    }
  }

  fclose(fp);
}

void 
File::writeMemoryToBinaryFile(const std::string& fname, std::vector<unsigned char>& data)
{
  FILE *fp = fopen(fname.c_str(),"wb");

  if (!fp)
  {
    throw File::write_error();
  }

  int numBytesWritten = fwrite(&data[0],1,data.size(),fp);

  if (numBytesWritten < data.size())
  {
    throw File::write_error();
  }

  if (fclose(fp) != 0)
  {
    throw File::write_error();
  }
}

std::string
File::getTempFileName()
{
#ifdef WIN32
  char path[MAX_PATH];
  GetTempPath(MAX_PATH,path);
  char fname[MAX_PATH];
  if (GetTempFileName(path,"vmt",0,fname) == 0)
  {
    throw write_error("Temporary file could not be created");
  }
  return std::string(fname);
#else
  return "";
#endif
}

void
File::deleteFile(const std::string& fname)
{
#ifdef WIN32
  if (DeleteFile(fname.c_str()) != S_OK)
  {
    throw write_error("Error deleting file: " + fname);
  }
#else
  return;
#endif
}

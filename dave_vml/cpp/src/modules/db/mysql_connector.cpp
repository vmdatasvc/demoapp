/*
 * mysql_connector.cpp
 *
 *  Created on: Nov 28, 2016
 *      Author: paulshin
 */

#include <sstream>
#include <iostream>
#include <vector>
#include <iomanip>
#include <stdio.h>

#include "modules/utility/strutil.hpp"
#include "modules/db/mysql_connector.hpp"

#define VISUALIZER_TESTBED_LIST_FILE    "/home/omniadmin/omnisensr-apps/config/OmniSensr_AWS_IoT_Connector/testbed_list.txt"

//#define AWS_RDS_PRODUCTION_URL  "tcp://production-1.cluster-cetyenwapymy.us-east-1.rds.amazonaws.com:3306"
#define AWS_RDS_PRODUCTION_URL  "production-1.cluster-cetyenwapymy.us-east-1.rds.amazonaws.com"
#define AWS_RDS_PRODUCTION_ID   "omniadmin"
#define AWS_RDS_PRODUCTION_PWD  "videoMININGLabs2016"

//#define AWS_RDS_TESTBED_URL  "tcp://vmiotstore.cxbttlf9n1ld.us-east-1.rds.amazonaws.com:3306"
#define AWS_RDS_TESTBED_URL  "vmiotstore.cxbttlf9n1ld.us-east-1.rds.amazonaws.com"
#define AWS_RDS_TESTBED_ID   "omniadmin"
#define AWS_RDS_TESTBED_PWD  "videoMININGlabs2015"

#define LOCAL_DB_ID		"root"
#define LOCAL_DB_URL	"127.0.0.1"

bool prepare_execute(sql::Connection & con, const char *sql);
sql::Statement* emulate_prepare_execute(sql::Connection & con, const char *sql);

mysql_connector::mysql_connector() {
	mIsInitialized = false;
	mIsConnectedToLocalDB = false;
	mTableRowSize = -1;
    mTableColumnSize = -1;
}

mysql_connector::~mysql_connector() {
    if(mIsConnectedToLocalDB) {
        delete mLocalDbConn;
        delete mTableResult_Raw;
    }

}

vector<string> mysql_connector::GetSchemaListFromAwsRds(bool isForTestbed) {
    const int buffer_size = 999;
    vector<string> schemaList;
    FILE *streamFromAP;
    char buffer[buffer_size];
    string lineInput;

    /* Example

        // -- Get the schema list from AWS RDS
        $ mysql --user=omniadmin --password=videoMININGLabs2016 -h production-1.cluster-cetyenwapymy.us-east-1.rds.amazonaws.com -e "SHOW DATABASES" | grep _

     */

    // -- For AWS account for Testbed
    if(isForTestbed == true) {
        mAwsDbUrl = AWS_RDS_TESTBED_URL;
        mAwsDbId = AWS_RDS_TESTBED_ID;
        mAwsDbPwd = AWS_RDS_TESTBED_PWD;
    } else {
        mAwsDbUrl = AWS_RDS_PRODUCTION_URL;
        mAwsDbId = AWS_RDS_PRODUCTION_ID;
        mAwsDbPwd = AWS_RDS_PRODUCTION_PWD;
    }

    ostringstream queryStr;


    // -- Get the DB list from AWS RDS
    queryStr << "mysql --user=" << mAwsDbId << " --password=" << mAwsDbPwd << " -h " << mAwsDbUrl << " -e \"SHOW DATABASES\" | grep _";

    streamFromAP = popen(queryStr.str().c_str(), "r");

    while(fgets(buffer, buffer_size, streamFromAP)) {	// -- Reads a line from the pipe and save it to buffer
        lineInput.clear();
        lineInput.append(buffer);

        if(lineInput.size() > 0) {
            // -- Break the line input into words and save it into a word vector
            istringstream iss (lineInput);
            vector<string> v;
            string s;
            while(iss >> s)
                v.push_back(s);

            if(v.size() > 0)
                schemaList.push_back(v[0]);
        }
    };

    if(streamFromAP != 0)
        pclose(streamFromAP);


    return schemaList;
}

vector<string> mysql_connector::GetTrackerDeviceListFromAwsRds(string storeName, bool isForTestbed) {
    const int buffer_size = 999;
    vector<string> schemaList;
    FILE *streamFromAP;
    char buffer[buffer_size];
    string lineInput;

    /* Example

        // -- Get the WiFi table list of a schema from AWS RDS
        $ mysql --user=omniadmin --password=videoMININGLabs2016 -h production-1.cluster-cetyenwapymy.us-east-1.rds.amazonaws.com -e "USE carealot_001; SHOW TABLES" | grep _wifi_data

     */

    // -- For AWS account for Testbed
    if(isForTestbed == true) {
        mAwsDbUrl = AWS_RDS_TESTBED_URL;
        mAwsDbId = AWS_RDS_TESTBED_ID;
        mAwsDbPwd = AWS_RDS_TESTBED_PWD;
    } else {
        mAwsDbUrl = AWS_RDS_PRODUCTION_URL;
        mAwsDbId = AWS_RDS_PRODUCTION_ID;
        mAwsDbPwd = AWS_RDS_PRODUCTION_PWD;
    }

    ostringstream queryStr;

    // -- dbName is created using the store name (e.g., carealot_001)
    mDbName = storeName;

    // -- Get the DB list from AWS RDS
    queryStr << "mysql --user=" << mAwsDbId << " --password=" << mAwsDbPwd << " -h " << mAwsDbUrl << " -e \"USE " <<  mDbName << "; SHOW TABLES\" | grep _wifi_data";

    streamFromAP = popen(queryStr.str().c_str(), "r");

    while(fgets(buffer, buffer_size, streamFromAP)) {	// -- Reads a line from the pipe and save it to buffer
        lineInput.clear();
        lineInput.append(buffer);

        if(lineInput.size() > 0) {
            // -- Break the line input into words and save it into a word vector
            istringstream iss (lineInput);
            vector<string> v;
            string s;
            while(iss >> s)
                v.push_back(s);

            if(v.size() > 0) {

                // -- Discard the suffix of the table name and extract only the device name like "omni001"
                std::size_t found = v[0].find_first_of("_");
                // -- If '_' is found, then discard the rest.
                if(found != string::npos)
                    v[0].resize(found);

                cout << "Device Name = " << v[0] << endl;

                schemaList.push_back(v[0]);
            }
        }
    };

    if(streamFromAP != 0)
        pclose(streamFromAP);


    return schemaList;
}

void mysql_connector::Initialize(string deviceName) {
    // -- Check if the target DB belongs the production accout or test account

    vector<string> thingName_vec = vml::split(mDbName, "_");

    ostringstream targetStoreName_slash;
    targetStoreName_slash << thingName_vec[0] << "/" << thingName_vec[1];

    ostringstream tableName;    // -- tableName is created using the device name and app name (e.g., omni005_wifi_data)
    tableName << deviceName << "_wifi_data";
    mTableName = tableName.str();

    tableName << "_post";
    mTableNamePost = tableName.str();       // -- Table Name to store the trajectories after post processing

    loadTestbedList(VISUALIZER_TESTBED_LIST_FILE);

    if(IsThisTestbed(targetStoreName_slash.str()) == true) {
    	mAwsDbUrl = AWS_RDS_TESTBED_URL;
        mAwsDbId = AWS_RDS_TESTBED_ID;
        mAwsDbPwd = AWS_RDS_TESTBED_PWD;
    } else {
    	mAwsDbUrl = AWS_RDS_PRODUCTION_URL;
		mAwsDbId = AWS_RDS_PRODUCTION_ID;
		mAwsDbPwd = AWS_RDS_PRODUCTION_PWD;
    }

    mTableRowSize = -1;
    mTableColumnSize = -1;
    mTestbedList.clear();

    mIsInitialized = true;
}

void mysql_connector::loadTestbedList(string testbedList) {
    mTestbedList.clear();

    FILE *fp_list;

    fp_list = fopen(testbedList.c_str(), "r");
    if(fp_list == NULL) {
        fprintf(stderr, "Error opening AWS_IOT_CONNECTOR_TESTBED_LIST_FILE file\n");
        //exit(0);
    } else {
        char buffer[255];

        // -- Read from remote broker and publish to local broker
        string lineInput;
        vector<string> measBuffer;
        while(fgets(buffer, 255, fp_list)) {	// -- Reads a line from the pipe and save it to buffer
            lineInput.clear();
            lineInput.append(buffer);

            // -- If there's any comment, then discard the rest of the line
            std::size_t found = lineInput.find_first_of("#");
            // -- If '#' is found, then discard the rest.
            if(found != string::npos)
                lineInput.resize(found);

            istringstream iss (lineInput);
            string testbed;
            iss >> testbed;

            //cout << "Testbed = [" << testbed << "]\n";

            mTestbedList.push_back(testbed);
        }

    }
}

bool mysql_connector::IsThisTestbed(string storeAddr) {

    bool isTestbed = false;
    for(deque<string>::iterator it = mTestbedList.begin(); it != mTestbedList.end(); ++it) {
        string testbed = *it;

        if(testbed.compare(storeAddr) == 0) {
            isTestbed = true;
            break;
        }
    }

    return isTestbed;
}

bool mysql_connector::ExportTableFromLocalDB() {

    ostringstream queryStr;

    /*
     * $ mysqldump -u [uname] -p[pass] db_name table1 > table_backup.sql
     * $ mysqldump -u [uname] -p[pass] db_name table1 | gzip > table_backup.sql.gz
     */

    // -- Export the table from the local DB to .sql file
    queryStr.clear(); queryStr.str("");
    //queryStr << "mysqldump -u " << LOCAL_DB_ID << " -h " << LOCAL_DB_URL << " " << mDbName << " " << mTableName << " > " << mDbName << "." << mTableName << ".sql";
    queryStr << "mysqldump -u " << LOCAL_DB_ID << " -h " << LOCAL_DB_URL << " " << mDbName << " " << mTableNamePost << " | gzip > " << mDbName << "." << mTableNamePost << ".sql.gz";
    cout << queryStr.str() << endl << endl;
    system(queryStr.str().c_str());

    return true;
}

bool mysql_connector::BackupAwsRdsToLocalDB() {

	ostringstream queryStr;

	/* Example
	 *
	 * -- How to backup a AWS RDS table as a  local .sql file
	 * $ mysqldump --user=omniadmin --password=videoMININGLabs2016 -h production-1.cluster-cetyenwapymy.us-east-1.rds.amazonaws.com carealot_001 omni005_wifi_data > carealot_001.omni005_wifi_data.sql
     * $ mysqldump --user=omniadmin --password=videoMININGLabs2016 -h production-1.cluster-cetyenwapymy.us-east-1.rds.amazonaws.com carealot_001 omni005_wifi_data | gzip > carealot_001.omni005_wifi_data.sql.gz
	 *
	 * -- How to create a table in a local DB
	 * $ mysql -u root -h 127.0.0.1 -e "CREATE DATABASE IF NOT EXISTS carealot_001"
	 *
	 * -- How to create a table in a local DB
	 * $ mysql -u root -h 127.0.0.1 -e "USE carealot_001; DROP TABLE IF EXISTS carealot_001.omni005_wifi_data"
	 *
	 * -- How to restore a backup to the table in the local DB
	 * $ mysql -u root -h 127.0.0.1 carealot_001 < carealot_001.omni005_wifi_data.sql
     * $ gunzip < carealot_001.omni005_wifi_data.sql.gz | mysql -u root -h 127.0.0.1 carealot_001
	 */

	// -- Get the entire DB from AWS RDS and save it to .sql locally
    //queryStr << "mysqldump --user=" << mAwsDbId << " --password=" << mAwsDbPwd << " -h " << mAwsDbUrl << " " << mDbName << " " << mTableName << " > " << mDbName << "." << mTableName << ".sql";
    queryStr << "mysqldump --user=" << mAwsDbId << " --password=" << mAwsDbPwd << " -h " << mAwsDbUrl << " " << mDbName << " " << mTableName << " | gzip > " << mDbName << "." << mTableName << ".sql.gz";
    cout << queryStr.str() << endl;
    system(queryStr.str().c_str());

	// -- Create the corresponding DB in the local DB
	queryStr.clear(); queryStr.str("");
    queryStr << "mysql -u " << LOCAL_DB_ID << " -h " << LOCAL_DB_URL << " -e \"CREATE DATABASE IF NOT EXISTS " << mDbName << "\"";
    cout << queryStr.str() << endl;
    system(queryStr.str().c_str());

	// -- Remove any previous table from the local DB
	queryStr.clear(); queryStr.str("");
	queryStr << "mysql -u " << LOCAL_DB_ID << " -h " << LOCAL_DB_URL << " -e \"USE " << mDbName << "; DROP TABLE IF EXISTS " << mDbName << "." << mTableName << "\"";
    cout << queryStr.str() << endl;
    system(queryStr.str().c_str());

	// -- Import the .sql table into the local DB
	queryStr.clear(); queryStr.str("");
    //queryStr << "mysql -u " << LOCAL_DB_ID << " -h " << LOCAL_DB_URL << " " << mDbName << " < " << mDbName << "." << mTableName << ".sql";
    queryStr << "gunzip < " << mDbName << "." << mTableName << ".sql.gz | mysql -u " << LOCAL_DB_ID << " -h " << LOCAL_DB_URL << " " << mDbName;
    cout << queryStr.str() << endl << endl;
    system(queryStr.str().c_str());

    return true;


/*
	// -- Compose a query (e.g., "SELECT * FROM carealot_001.omni005_wifi_data")
	istringstream queryStr;
	queryStr << "SELECT * FROM " << mDbName << "." << mTableName;
	//queryStr << "INSERT INTO" << mLocalDbName << "SELECT * FROM " << mDbName << "." << mTableName;

	try {

	      sql::Driver *driver;
	      sql::Connection *con;
	      sql::Statement *stmt;
	      sql::ResultSet *res;

	      // -- Create a connection
	      driver = get_driver_instance();
	      con = driver->connect(mDbUrl.c_str(), mDbId.c_str(), mDbPwd.c_str());

	        // -- Connect to the MySQL test database
	        con->setSchema(mDbName.c_str());

	        stmt = con->createStatement();
	        res = stmt->executeQuery(queryStr.str().c_str());


//	         			jsonStr << "{"
//	                            << "\"dev_id\":\"" << dev->GetDevAddr() << "\","
//	                            << "\"start_time\":" << timeMeas_start << ","
//	                            << "\"A4\":\"" << traj.str() << "\","
//	                            << "\"cnt\":" << dev->GetTrackedPosBufSize() << ","
//	                            << "\"duration\":" << tripDuration
//	                        << "}";


	      // -- Show the first or all rows
	      res->next();
          do {
	        cout << "...";
	        cout << "(start_time, dev_id, cnt, duration) = (" << (double)res->getDouble("start_time") << ", " << res->getString("dev_id") << ", " <<  res->getUInt("cnt")
	                        << ", " << res->getDouble("duration") << "): A4 = " << res->getString("A4") << endl << endl;
          } while (res->next());

	      delete res;
	      delete stmt;
	      delete con;

	    } catch (sql::SQLException &e) {
	      cout << "# ERR: SQLException in " << __FILE__;
	      cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
	      cout << "# ERR: " << e.what();
	      cout << " (MySQL error code: " << e.getErrorCode();
	      cout << ", SQLState: " << e.getSQLState() << " )" << endl;
	    }
*/
}

void mysql_connector::DisconnectToLocalDB() {
    mIsConnectedToLocalDB = false;
    mIsInitialized = false;

    delete mLocalDbConn;
}

bool mysql_connector::ConnectToLocalDB() {

    try {

		// -- Create a connection
		mLocalDbDriver = get_driver_instance();
		mLocalDbConn = mLocalDbDriver->connect(LOCAL_DB_URL, LOCAL_DB_ID, "");

		// -- Connect to the MySQL test database
		mLocalDbConn->setSchema(mDbName.c_str());

		mIsConnectedToLocalDB = true;

	} catch (sql::SQLException &e) {        
        /*
        The MySQL Connector/C++ throws three different exceptions:

        - sql::MethodNotImplementedException (derived from sql::SQLException)
        - sql::InvalidArgumentException (derived from sql::SQLException)
        - sql::SQLException (derived from std::runtime_error)
        */

        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;

        mIsConnectedToLocalDB = false;
        return EXIT_FAILURE;
    } catch (std::runtime_error &e) {
        cout << "# ERR: runtime_error in " << __FILE__;
        cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
        cout << "# ERR: " << e.what() << endl;
        cout << "not ok 1 - examples/resultset.cpp" << endl;
        return EXIT_FAILURE;
    }

     return true;
}

bool mysql_connector::GetProcessedTrackTable() {
    if(mIsConnectedToLocalDB == false)
        return false;

    // -- Compose a query (e.g., "SELECT * FROM carealot_001.omni005_wifi_data_post")
    ostringstream queryStr;
    queryStr << "SELECT * FROM " << mDbName << "." << mTableNamePost;

    sql::Statement *stmt;

    try {

        stmt = mLocalDbConn->createStatement();
        mTableResult_Processed = stmt->executeQuery(queryStr.str().c_str());

        delete stmt;

    } catch (sql::SQLException &e) {
        /*
        The MySQL Connector/C++ throws three different exceptions:

        - sql::MethodNotImplementedException (derived from sql::SQLException)
        - sql::InvalidArgumentException (derived from sql::SQLException)
        - sql::SQLException (derived from std::runtime_error)
        */

        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;

        return EXIT_FAILURE;
    } catch (std::runtime_error &e) {
        cout << "# ERR: runtime_error in " << __FILE__;
        cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
        cout << "# ERR: " << e.what() << endl;
        cout << "not ok 1 - examples/resultset.cpp" << endl;
        return EXIT_FAILURE;
    }

    return true;
}

bool mysql_connector::GetRawTrackTable() {
    if(mIsConnectedToLocalDB == false)
        return false;

    // -- Compose a query (e.g., "SELECT * FROM carealot_001.omni005_wifi_data")
    ostringstream queryStr;
    queryStr << "SELECT * FROM " << mDbName << "." << mTableName;

	sql::Statement *stmt;

	try {

		stmt = mLocalDbConn->createStatement();
        mTableResult_Raw = stmt->executeQuery(queryStr.str().c_str());

        mTableMetaData = mTableResult_Raw->getMetaData();

        // -- Get the row and column size
        mTableColumnSize = mTableMetaData->getColumnCount();
        mTableRowSize = mTableResult_Raw->rowsCount();

		delete stmt;

    } catch (sql::SQLException &e) {
        /*
        The MySQL Connector/C++ throws three different exceptions:

        - sql::MethodNotImplementedException (derived from sql::SQLException)
        - sql::InvalidArgumentException (derived from sql::SQLException)
        - sql::SQLException (derived from std::runtime_error)
        */

        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;

        return EXIT_FAILURE;
    } catch (std::runtime_error &e) {
        cout << "# ERR: runtime_error in " << __FILE__;
        cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
        cout << "# ERR: " << e.what() << endl;
        cout << "not ok 1 - examples/resultset.cpp" << endl;
        return EXIT_FAILURE;
    }

    return true;
}

bool mysql_connector::PrintAllRawTracks() {

    if(mIsConnectedToLocalDB == false)
        return false;

    try {


//            jsonStr << "{"
//                    << "\"dev_id\":\"" << dev->GetDevAddr() << "\","
//                    << "\"start_time\":" << timeMeas_start << ","
//                    << "\"A4\":\"" << traj.str() << "\","
//                    << "\"cnt\":" << dev->GetTrackedPosBufSize() << ","
//                    << "\"duration\":" << tripDuration
//                << "}";


            // -- Reset the cursor
            mTableResult_Raw->first();

            // -- Show all rows
            do {
            cout << "...";
            cout << "(start_time, dev_id, cnt, duration) = (" << (double)mTableResult_Raw->getDouble("start_time") << ", " << mTableResult_Raw->getString("dev_id") << ", " <<  mTableResult_Raw->getUInt("cnt")
                            << ", " << mTableResult_Raw->getDouble("duration") << "): A4 = " << mTableResult_Raw->getString("A4") << endl << endl;
            } while (mTableResult_Raw->next());

    } catch (sql::SQLException &e) {
        /*
        The MySQL Connector/C++ throws three different exceptions:

        - sql::MethodNotImplementedException (derived from sql::SQLException)
        - sql::InvalidArgumentException (derived from sql::SQLException)
        - sql::SQLException (derived from std::runtime_error)
        */

        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
        return EXIT_FAILURE;
    } catch (std::runtime_error &e) {
        cout << "# ERR: runtime_error in " << __FILE__;
        cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
        cout << "# ERR: " << e.what() << endl;
        cout << "not ok 1 - examples/resultset.cpp" << endl;
        return EXIT_FAILURE;
    }

     return true;
}

// -- Move to the selected row. Note that the first row's rowNum is 1, not 0.
WiFiTrack_t mysql_connector::GetRawTrackAtARow(uint rowNo) {

    WiFiTrack_t wifiTrack;

    if(mIsConnectedToLocalDB == false)
        return wifiTrack;

    /* Move the cursor to position 0 = before the first row */
    // -- (https://fossies.org/linux/mysql-connector-c++/examples/resultset.cpp)
    if (false != mTableResult_Raw->absolute(rowNo)) {
        throw runtime_error("Call did not fail although its not allowed to move the cursor before the first row");
    }


    ParseRawTrackFromDB(mTableResult_Raw->getString("A4"), wifiTrack);

    return wifiTrack;
}

void mysql_connector::ParseRawTrackFromDB(string trackStr, WiFiTrack_t &wifiTrack) {

    // -- Assign a unique color for visualization
    wifiTrack.r = rand()%200;
    wifiTrack.g = rand()%200;
    wifiTrack.b = rand()%200;

    wifiTrack.dev_id = mTableResult_Raw->getString("dev_id");
    wifiTrack.start_time = mTableResult_Raw->getDouble("start_time");
    wifiTrack.duration = mTableResult_Raw->getDouble("duration");
    wifiTrack.cnt = mTableResult_Raw->getUInt("cnt");
    wifiTrack.track.clear();



/*
 * A4 = 0.0,980,3034;1.5,980,3034;3.0,980,3053;518.2,955,3231;521.2,3164,3605;645.8,916,1954;647.3,910,1970;648.8,948,1936;
 */

    // -- erase '"' from the address field if there is
    if(trackStr.size() > 0)
        trackStr.erase(std::remove(trackStr.begin(), trackStr.end(), '"'), trackStr.end());

    // -- Split multiple locations into a vector
    vector<string> tracks_vec = vml::split(trackStr, ";");

    if(tracks_vec.size() > 0) {        
        int ind = 0;
        for(vector<string>::iterator it = tracks_vec.begin(); it != tracks_vec.end(); ++it) {
            string locStr = *it;
            if(locStr.size() == 0)
                continue;
            ind++;

            vector<string> each_loc = vml::split(locStr, ",");

            if(each_loc.size() != 3) {
                printf("Dev[%s]'s [%d]th track among [%d]: each_loc.size() != 3 \"%s\"\n", wifiTrack.dev_id.c_str(), ind, wifiTrack.cnt, locStr.c_str());
                continue;
            }
            double dt = atof(each_loc[0].c_str());

            WiFiPostData_t pos;
            pos.localtime = wifiTrack.start_time + dt;
            pos.posPhy.x = atoi(each_loc[1].c_str());
            pos.posPhy.y = atoi(each_loc[2].c_str());

            wifiTrack.track.push_front(pos);

        }
    } else {
        cout << "tracks_vec.size() == 0" << endl;
    }


    // -- Sort based on the time so that the first location is placed at front.
    if(wifiTrack.track.size() > 0) {
        sort(wifiTrack.track.begin(), wifiTrack.track.end(),
                [](WiFiPostData_t a, WiFiPostData_t b) { return a.localtime < b.localtime; }         // -- Ascending order (smaller one comes earlier)
             );
    }


    if(wifiTrack.track.size() != wifiTrack.cnt)
        printf("ERROR: (wifiTrack.trackedPos.size(), wifiTrack.cnt) = (%d, %d)\n", (int)wifiTrack.track.size(), wifiTrack.cnt);
}


void mysql_connector::ParseProcessedTrackFromDB(string trackStr, WiFiTrack_t &wifiTrack) {

    // -- Assign a unique color for visualization
    wifiTrack.r = rand()%200;
    wifiTrack.g = rand()%200;
    wifiTrack.b = rand()%200;

    wifiTrack.dev_id = mTableResult_Processed->getString("dev_id");
    wifiTrack.start_time = mTableResult_Processed->getDouble("start_time");
    wifiTrack.duration = mTableResult_Processed->getDouble("duration");
    wifiTrack.cnt = mTableResult_Processed->getUInt("cnt");

/*
 * A4 = 0.0,980,3034;1.5,980,3034;3.0,980,3053;518.2,955,3231;521.2,3164,3605;645.8,916,1954;647.3,910,1970;648.8,948,1936;
 */

    // -- erase '"' from the address field if there is
    if(trackStr.size() > 0)
        trackStr.erase(std::remove(trackStr.begin(), trackStr.end(), '"'), trackStr.end());

    // -- Split multiple locations into a vector
    vector<string> tracks_vec = vml::split(trackStr, ";");

    if(tracks_vec.size() > 0) {
        wifiTrack.refinedTrack.clear();
        int ind = 0;
        for(vector<string>::iterator it = tracks_vec.begin(); it != tracks_vec.end(); ++it) {
            string locStr = *it;
            if(locStr.size() == 0)
                continue;
            ind++;

            vector<string> each_loc = vml::split(locStr, ",");

            if(each_loc.size() != 3) {
                printf("Dev[%s]'s [%d]th track among [%d]: each_loc.size() != 3 \"%s\"\n", wifiTrack.dev_id.c_str(), ind, wifiTrack.cnt, locStr.c_str());
                continue;
            }
            double dt = atof(each_loc[0].c_str());

            WiFiPostData_t pos;
            pos.localtime = wifiTrack.start_time + dt;
            pos.posPhy.x = atoi(each_loc[1].c_str());
            pos.posPhy.y = atoi(each_loc[2].c_str());

            wifiTrack.refinedTrack.push_front(pos);
        }

        //printf("[mysql_connector] dev_id = %s\n", wifiTrack.dev_id.c_str());

    } else {
        cout << "tracks_vec.size() == 0" << endl;
    }


    // -- Sort based on the time so that the first location is placed at front.
    if(wifiTrack.refinedTrack.size() > 0) {
        sort(wifiTrack.refinedTrack.begin(), wifiTrack.refinedTrack.end(),
                [](WiFiPostData_t a, WiFiPostData_t b) { return a.localtime < b.localtime; }         // -- Ascending order (smaller one comes earlier)
             );
    }


    if(wifiTrack.refinedTrack.size() != wifiTrack.cnt)
        printf("ERROR: (wifiTrack.trackedPos.size(), wifiTrack.cnt) = (%d, %d)\n", (int)wifiTrack.track.size(), wifiTrack.cnt);
}

bool mysql_connector::GetAllRawTracks(deque<WiFiTrack_t> &wifiTracks) {
    wifiTracks.clear();

    if(mIsConnectedToLocalDB == false)
        return false;

    try {


//            jsonStr << "{"
//                    << "\"dev_id\":\"" << dev->GetDevAddr() << "\","
//                    << "\"start_time\":" << timeMeas_start << ","
//                    << "\"A4\":\"" << traj.str() << "\","
//                    << "\"cnt\":" << dev->GetTrackedPosBufSize() << ","
//                    << "\"duration\":" << tripDuration
//                << "}";


            // -- Reset the cursor
            mTableResult_Raw->first();

            // -- Show all rows
            do {
                WiFiTrack_t wifiTrack;

                ParseRawTrackFromDB(mTableResult_Raw->getString("A4"), wifiTrack);

                if(wifiTrack.track.size() > 0)
                    wifiTracks.push_back(wifiTrack);
            } while (mTableResult_Raw->next());

    } catch (sql::SQLException &e) {
        /*
        The MySQL Connector/C++ throws three different exceptions:

        - sql::MethodNotImplementedException (derived from sql::SQLException)
        - sql::InvalidArgumentException (derived from sql::SQLException)
        - sql::SQLException (derived from std::runtime_error)
        */

        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
        return EXIT_FAILURE;
    } catch (std::runtime_error &e) {
        cout << "# ERR: runtime_error in " << __FILE__;
        cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
        cout << "# ERR: " << e.what() << endl;
        cout << "not ok 1 - examples/resultset.cpp" << endl;
        return EXIT_FAILURE;
    }

    return true;
}

bool mysql_connector::AddToExistingTracks(deque<WiFiTrack_t> &wifiTracks, WiFiTrack_t &aTrack) {
    for(int i=0; i<(int)wifiTracks.size(); i++) {
        if(wifiTracks[i].dev_id.compare(aTrack.dev_id) == 0 && wifiTracks[i].start_time == aTrack.start_time) {
            wifiTracks[i].refinedTrack = aTrack.refinedTrack;
            return true;
        }
    }

    return false;
}

bool mysql_connector::GetAllProcessedTracks(deque<WiFiTrack_t> &wifiTracks) {

    if(mIsConnectedToLocalDB == false)
        return false;

    try {


//            jsonStr << "{"
//                    << "\"dev_id\":\"" << dev->GetDevAddr() << "\","
//                    << "\"start_time\":" << timeMeas_start << ","
//                    << "\"A4\":\"" << traj.str() << "\","
//                    << "\"cnt\":" << dev->GetTrackedPosBufSize() << ","
//                    << "\"duration\":" << tripDuration
//                << "}";


            // -- Reset the cursor
            mTableResult_Processed->first();

            // -- Show all rows
            //int cnt = 0;
            do {
                WiFiTrack_t wifiTrack;

                ParseProcessedTrackFromDB(mTableResult_Processed->getString("A4"), wifiTrack);

                if(wifiTrack.refinedTrack.size() > 0) {

                    // -- Find corresponding rawTrack and add the post-processed tracks
                    AddToExistingTracks(wifiTracks, wifiTrack);

                    //bool isFound = AddToExistingTracks(wifiTracks, wifiTrack);
                    //printf("[mysql_connector] AddToExistingTracks(): [%d]th isFound = %d\n", cnt, isFound);
                    //cnt++;
                }
            } while (mTableResult_Processed->next());

    } catch (sql::SQLException &e) {
        /*
        The MySQL Connector/C++ throws three different exceptions:

        - sql::MethodNotImplementedException (derived from sql::SQLException)
        - sql::InvalidArgumentException (derived from sql::SQLException)
        - sql::SQLException (derived from std::runtime_error)
        */

        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
        return EXIT_FAILURE;
    } catch (std::runtime_error &e) {
        cout << "# ERR: runtime_error in " << __FILE__;
        cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
        cout << "# ERR: " << e.what() << endl;
        cout << "not ok 1 - examples/resultset.cpp" << endl;
        return EXIT_FAILURE;
    }

    return true;
}

bool mysql_connector::UsePostProcessingTable() {

    // -- Compose a query/command (e.g., "SELECT * FROM carealot_001.omni005_wifi_data")
    ostringstream queryStr;
    //queryStr << "SELECT * FROM " << mDbName << "." << mTableName;

    try {
        sql::Statement *stmt;

        queryStr.str(""); queryStr.clear();
        queryStr << "USE " << mDbName << ";";
            cout << "[mysql_connector]: Command = " << queryStr.str() << endl;

        stmt = mLocalDbConn->createStatement();
        stmt->execute(queryStr.str());

    } catch (sql::SQLException &e) {
        /*
        The MySQL Connector/C++ throws three different exceptions:

        - sql::MethodNotImplementedException (derived from sql::SQLException)
        - sql::InvalidArgumentException (derived from sql::SQLException)
        - sql::SQLException (derived from std::runtime_error)
        */

        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
        return EXIT_FAILURE;
    } catch (std::runtime_error &e) {
        cout << "# ERR: runtime_error in " << __FILE__;
        cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
        cout << "# ERR: " << e.what() << endl;
        cout << "not ok 1 - examples/resultset.cpp" << endl;
        return EXIT_FAILURE;
    }

    return true;
}


bool mysql_connector::CreatePostProcessingTable() {

    // -- Compose a query/command (e.g., "SELECT * FROM carealot_001.omni005_wifi_data")
    ostringstream queryStr;
    //queryStr << "SELECT * FROM " << mDbName << "." << mTableName;

    try {
        sql::Statement *stmt;
        stmt = mLocalDbConn->createStatement();

        queryStr.str(""); queryStr.clear();
        queryStr << "DROP TABLE IF EXISTS " << mTableNamePost << ";";
        cout << "[mysql_connector]: Command = " << queryStr.str() << endl;

        stmt->execute(queryStr.str());

        // -- Create the post processing table
        queryStr.str(""); queryStr.clear();
        queryStr << "CREATE TABLE " << mTableNamePost << "("
                << "A4 longtext,cnt int(11) DEFAULT NULL,tracker_id varchar(18) DEFAULT NULL,duration int(11) DEFAULT NULL,start_time double NOT NULL,dev_id varchar(18) DEFAULT NULL"
                << ")"
                << " ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        cout << "[mysql_connector]: Command = " << queryStr.str() << endl;

        stmt->execute(queryStr.str());

        delete stmt;

    } catch (sql::SQLException &e) {
        /*
        The MySQL Connector/C++ throws three different exceptions:

        - sql::MethodNotImplementedException (derived from sql::SQLException)
        - sql::InvalidArgumentException (derived from sql::SQLException)
        - sql::SQLException (derived from std::runtime_error)
        */

        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
        return EXIT_FAILURE;
    } catch (std::runtime_error &e) {
        cout << "# ERR: runtime_error in " << __FILE__;
        cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
        cout << "# ERR: " << e.what() << endl;
        cout << "not ok 1 - examples/resultset.cpp" << endl;
        return EXIT_FAILURE;
    }

    return true;
}

bool mysql_connector::DeleteTrackToPostProcessingTable(WiFiTrack_t &wifiTrack) {
    // -- Compose a query/command (e.g., "SELECT * FROM carealot_001.omni005_wifi_data")
    ostringstream queryStr;

    try {
        sql::Statement *stmt;

        stmt = mLocalDbConn->createStatement();


        // -- Delete the existing entry in the table
        // -- "DELETE FROM your_table WHERE id_users=1 AND id_product=2"
        queryStr << "DELETE FROM " << mTableNamePost << " WHERE "
                 << " EXISTS(SELECT * FROM " << mTableNamePost << " WHERE " << "dev_id=" << wifiTrack.dev_id << " AND start_time=" <<  wifiTrack.start_time << ")"
                 << " AND dev_id=" << wifiTrack.dev_id << " AND start_time=" <<  wifiTrack.start_time
                 << ";";

        cout << "[mysql_connector]: Command = " << queryStr.str() << endl;

        stmt->executeQuery(queryStr.str().c_str());

        delete stmt;

    }
    catch (sql::SQLException &e) {
        /*
        The MySQL Connector/C++ throws three different exceptions:

        - sql::MethodNotImplementedException (derived from sql::SQLException)
        - sql::InvalidArgumentException (derived from sql::SQLException)
        - sql::SQLException (derived from std::runtime_error)
        */

//        cout << "# ERR: SQLException in " << __FILE__;
//        cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
//        cout << "# ERR: " << e.what();
//        cout << " (MySQL error code: " << e.getErrorCode();
//        cout << ", SQLState: " << e.getSQLState() << " )" << endl;

        return EXIT_FAILURE;
    } catch (std::runtime_error &e) {
//        cout << "# ERR: runtime_error in " << __FILE__;
//        cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
//        cout << "# ERR: " << e.what() << endl;
//        cout << "not ok 1 - examples/resultset.cpp" << endl;
        return EXIT_FAILURE;
    }

    return true;

}

bool mysql_connector::AddTrackToPostProcessingTable(WiFiTrack_t &wifiTrack) {
    // -- Compose a query/command (e.g., "SELECT * FROM carealot_001.omni005_wifi_data")
    ostringstream queryStr;

    if(wifiTrack.refinedTrack.size() == 0 || wifiTrack.track.size() == 0)
        return false;

    try {
         sql::PreparedStatement *pstmt;

        // -- Insert a new data into the table
        /* '?' is the supported placeholder syntax */
        queryStr.str(""); queryStr.clear();

        // -- "A4 longtext,cnt int(11) DEFAULT NULL,tracker_id varchar(18) DEFAULT NULL,duration int(11) DEFAULT NULL,start_time double NOT NULL,dev_id varchar(18) DEFAULT NULL"

        queryStr << "INSERT INTO " << mTableNamePost << "("
                << "A4,cnt,tracker_id,duration,start_time,dev_id"
                << ") VALUES (?,?,?,?,?,?);";

        //cout << "[mysql_connector]: Command = " << queryStr.str() << endl;

        pstmt = mLocalDbConn->prepareStatement(queryStr.str());

        // -- Compose "A4" field
        /*
         * A4 = 0.0,980,3034;1.5,980,3034;3.0,980,3053;518.2,955,3231;521.2,3164,3605;645.8,916,1954;647.3,910,1970;648.8,948,1936;
         */

        stringstream A4;

        // -- Go through from the end of the list since the end is the beginning of the trajectory.
        for(deque<WiFiPostData_t>::iterator it = wifiTrack.refinedTrack.begin(); it < wifiTrack.refinedTrack.end(); ++it) {
            WiFiPostData_t loc = *it;
            double elapsedTime = loc.localtime - wifiTrack.start_time;

            A4 << fixed << showpoint << setprecision(1);	// -- Make sure the precision is 1/10 second.
            A4 << elapsedTime << "," << loc.posPhy.x << "," << loc.posPhy.y << ";";
        }

        pstmt->setString(1, A4.str());
        pstmt->setInt(2, wifiTrack.refinedTrack.size());
        pstmt->setString(3, "001");
        pstmt->setDouble(4, wifiTrack.duration);
        pstmt->setDouble(5, wifiTrack.start_time);
        pstmt->setString(6, wifiTrack.dev_id);

        pstmt->execute();


        delete pstmt;

    } catch (sql::SQLException &e) {
        /*
        The MySQL Connector/C++ throws three different exceptions:

        - sql::MethodNotImplementedException (derived from sql::SQLException)
        - sql::InvalidArgumentException (derived from sql::SQLException)
        - sql::SQLException (derived from std::runtime_error)
        */

        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;

        return EXIT_FAILURE;
    } catch (std::runtime_error &e) {
        cout << "# ERR: runtime_error in " << __FILE__;
        cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
        cout << "# ERR: " << e.what() << endl;
        cout << "not ok 1 - examples/resultset.cpp" << endl;
        return EXIT_FAILURE;
    }

    return true;
}

project (vmldbLib)
set (LIB_NAME vmldb)

set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set (INCLUDE_ALL_DIRS	
	${VML_ROOT}/include
	${VML_ROOT}/include/modules/db
)

set (LIB_SRC
	mysql_connector.cpp
	)

vml_add_library (${LIB_NAME}
	STATIC
	${LIB_SRC}
	${PROJECT_HEADER_FILES}
	${INCLUDE_ALL_DIRS}
	)

target_link_libraries (${LIB_NAME}
	vmlutility
	mysqlcppconn
	)

include_directories(${PROJECT_NAME}  
	PUBLIC 
	${INCLUDE_ALL_DIRS}
)



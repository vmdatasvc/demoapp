/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * VisionSiteSetup.cpp
 *
 *  Created on: Jan 29, 2017
 *      Author: yyoon
 */

#include <modules/vision/VisionSiteSetup.hpp>
#include <modules/vision/VisionTrajectory.hpp>
#include <core/Error.hpp>
#include <modules/utility/strutil.hpp>
#include <modules/utility/MathUtil.hpp>
#include <core/CameraCalibration.hpp>
#include <modules/vision/VisionPostProcessing.hpp>


#include <fstream>
#include <tinyxml2.h>

#include <opencv2/highgui/highgui.hpp>

#define CAM_HEIGHT_OFFSET	0.01f
namespace vml {

// TrajectorySet
void TrajectorySet::addTrajectoryFromCSVLine(std::string csv_line)
{
	boost::shared_ptr<PostProcessingVisionTrajectory>	ptr;
	ptr.reset(new PostProcessingVisionTrajectory(mName));

	ptr->fromCSVString(csv_line);

	mTrajectoryList.push_back(ptr);
}

void TrajectorySet::aggregate(std::vector<boost::shared_ptr<PostProcessingVisionTrajectory> > &aggregated_list)
{
	for (auto it=mTrajectoryList.begin();it!=mTrajectoryList.end();it++)
	{
		aggregated_list.push_back(*it);
	}
}

void TrajectorySet::incrementalJoin()
{
	// use legacy incremental join algorithm
	for (unsigned int i=0;i<mJRThresholdVec.size();i++)
	{
		this->rectJoin(mJRThresholdVec[i]);
	}
}

void TrajectorySet::rectJoin(JRThresholdStruct thresholds)
{
	// sort trajectory list by start time
	std::sort(mTrajectoryList.begin(),mTrajectoryList.end(),VisionTrajectoryStartTimeCompare());

	// Take  1 traj at a time check with every other traj and  see if using the end node of first traj and start node of second traj do the following checks
	for (unsigned int i=0;i<mTrajectoryList.size();i++)
	{
		if (mTrajectoryList[i]->isValid())
		{
			std::vector<boost::shared_ptr<PostProcessingVisionTrajectory> > merge_candidates;
			std::vector<float>	merge_candidate_dist;
			for (unsigned int j=i+1;j<mTrajectoryList.size();j++)
			{
				if (mTrajectoryList[j]->isValid())
				{
					// if j's starttime is withtin threshold to i's endtime
					double	time_offset = mTrajectoryList[j]->getStartTime() - mTrajectoryList[j]->getEndTime();
					if (time_offset <= thresholds.max_time_diff)
					{
					    //  - check if the nodes time difference lie within the min and max time threshold
						if (time_offset < thresholds.min_time_diff)
						{
							continue;
						}

				        // - check if the nodes don't lie in ignore region
					    //  - check if the distance between the nodes lies within the min and max dist thresholds
						float endpoint_dist = mTrajectoryList[i]->getBackPoint().dist(mTrajectoryList[j]->getFrontPoint());
						if (endpoint_dist > thresholds.distance)
						{
							continue;
						}
						// check if the two trajectories belong to the same set
						if (mTrajectoryList[i]->belongToSameSet(mTrajectoryList[j]))
						{
							continue;
						}
					    //  - check if the nodes lie in the same aisle (ie not in the furniture file ignore regions
						if ((mpFurniture)&&(mpFurniture->doesIntersect(mTrajectoryList[i]->getBackPoint().loc,mTrajectoryList[j]->getFrontPoint().loc)))
						{
							continue;
						}

						// of all above test passes, this is in candidate pool
						merge_candidates.push_back(mTrajectoryList[j]);
						merge_candidate_dist.push_back(endpoint_dist);
					}
					else
					{
						break;	// since list is sorted by the start time, all the rest of list in this loop will have start time later than j,
					}
				} // of if (mTrajectoryList[j]->isValid())
			} // of for (j)

			// get the traj with the least distance from the first traj ie traj1 end time node dist - traj2 start time node dist is   minimum
			// join traj2 to traj1 and delete the second traj from the lists
			if (merge_candidates.size()>0)
			{
				float min_dist = FLT_MAX;
				int min_idx = -1;
				for (unsigned int k=0;k<merge_candidate_dist.size();k++)
				{
					if (merge_candidate_dist[k] < min_dist)
					{
						min_dist = merge_candidate_dist[k];
						min_idx = k;
					}
				}

				// merge with min_idx
				mTrajectoryList[i]->merge(merge_candidates[min_idx]);
				merge_candidates[min_idx]->setInvalid();
				mTrajectoryList[i]->setSetName(mName);

			}

		}
	}

	// compact trajectory list
	this->compact();
}

void TrajectorySet::overlapJoin()
{
	// join two trajectories with reasonable overlaps

	// sort aggregated list by start time
	std::sort(mTrajectoryList.begin(),mTrajectoryList.end(),VisionTrajectoryStartTimeCompare());

	// group trajectories that have time overlaps
	std::vector<boost::shared_ptr<PostProcessingVisionTrajectory> >	overlapping_group;
	// add the first trajectory in the group
	unsigned int cur_idx = 0;
	while (cur_idx < mTrajectoryList.size())
	{
		// initial construct of overlapping group
		overlapping_group.push_back(mTrajectoryList[cur_idx]);
		double group_max_end_time = mTrajectoryList[cur_idx]->getEndTime();
		cur_idx++;

		while (cur_idx < mTrajectoryList.size())
		{
			// check if start time of this trajectory is inside the group max end time
			if (mTrajectoryList[cur_idx]->getStartTime() < group_max_end_time)
			{
				// insert this trajectory to the group
				overlapping_group.push_back(mTrajectoryList[cur_idx]);
				group_max_end_time = std::max(group_max_end_time,mTrajectoryList[cur_idx]->getEndTime());
				cur_idx++;
			}
			else
			{
				break;
			}
		}

		// for each group, calculate endpoint distances, and merge those that are close to each other
		for (unsigned int i=0;i<overlapping_group.size();i++)
		{
			if (overlapping_group[i]->isValid())
			{
				for (unsigned int j=i+1;j<overlapping_group.size();j++)
				{
					if (overlapping_group[j]->isValid())
					{
						// check if i,j passing through furniture polygons

						// check and merge i,j
						if (overlapping_group[i]->getStartTime() == overlapping_group[j]->getStartTime())
						{
							// this case do happen.
							// if the start time of the two trajectories are same, it is not likely they should be merged
							continue;
						}
						if (overlapping_group[i]->belongToSameSet(overlapping_group[j]))
						{
							// these two trajectories belong to the same set. Do not join
							continue;
						}
						assert(overlapping_group[i]->getStartTime() < overlapping_group[j]->getStartTime());
						if (overlapping_group[i]->joinTrajectoryByOverlap(overlapping_group[j],mpFurniture))
						{
//							std::cout << "Trajectory (" << i << "," << j << ") are joined." << std::endl;
//							cv::Mat	temp_disp;
//							__getFloorplanImage().copyTo(temp_disp);
//							overlapping_group[i]->draw(temp_disp,CV_RGB(0,255,0));
//							__showDispImage(temp_disp,"Trajectory Display");

							// two trajectories are now joined.
							// change set name of the joined trajectory to this set name
							overlapping_group[i]->setSetName(mName);
						}
					}
				}
			}
		}

		overlapping_group.clear();
	}

	// compact trajectory list
	this->compact();
}

void TrajectorySet::join()
{
	if (!mpFurniture)
	{
		VML_WARN(MsgCode::logicErr, "Beware, no furniture polygons are provided...");
	}

	std::cout << "Joining " << mName << " at " << mLevelID << std::endl;
	// assert each camera set is merged and converted to floorplan

	// aggregate sub sets
	std::cout << "Aggregating the following sets: " << std::endl;
	for (auto cit = mSubSetList.begin();cit!=mSubSetList.end();cit++)
	{
		std::cout << "   - " << (*cit)->getName() << std::endl;
		(*cit)->aggregate(mTrajectoryList);
	}

	switch (mJoinMethod)
	{
		case VisionPostProcessing_OverlapJoin:
		{
			std::cout << "Run iterative overlap join:" << std::endl;
			// iterate until no more trajectories are joined.
			unsigned int	nTrajectory;
			unsigned int	nIteration = 1;
			do {
				nTrajectory = mTrajectoryList.size();
				std::cout << "Iteration #" << nIteration << ": " << mTrajectoryList.size() << " Trajectories to start...";
				this->overlapJoin();
				std::cout << "Done! " << nTrajectory - mTrajectoryList.size() << " of trajectories were merged." << std::endl;
			} while (nTrajectory != mTrajectoryList.size());

			std::cout << "Done iteration." << std::endl;

			break;
		}

		case VisionPostProcessing_JR:
		{
			std::cout << "Run rect join:" << std::endl;

			unsigned int	nTrajectory = mTrajectoryList.size();
			std::cout << mTrajectoryList.size() << " Trajectories to start...";
			this->incrementalJoin();
			std::cout << "Done! " << nTrajectory - mTrajectoryList.size() << " of trajectories were merged." << std::endl;
			break;
		}
		default:
		{
			// error. not supported
			VML_ERROR(MsgCode::notImplementedErr, "Unknown joining method enum " << mJoinMethod << std::endl);
		}
	}

	// set all merged trajectories to have current set name
	this->resetSetNameToTrajectories();

}

void TrajectorySet::recursiveJoin()
{
	// recursively join

	// if level is higher or equal to 1, join each subset first
	if (mSubSetList.empty())	// for level0, subset list should be empty
	{
		// this is leaf node set. do nothing.
		return;
	}

	for (auto git=mSubSetList.begin();git!=mSubSetList.end();git++)
	{
		(*git)->recursiveJoin();
	}

	join();
}


void TrajectorySet::compact()
{
	// delete invalid trajectory and compact the list
	int	num_valid_entry = 0;
	std::vector<boost::shared_ptr<PostProcessingVisionTrajectory> >::iterator cur_itr;
	std::vector<boost::shared_ptr<PostProcessingVisionTrajectory> >::iterator last_valid_position_itr;
	for (cur_itr=mTrajectoryList.begin(),last_valid_position_itr=cur_itr;
			cur_itr!=mTrajectoryList.end();cur_itr++)
	{
		if ((*cur_itr)->isValid())	// if this is a valid pointer
		{
			if (cur_itr != last_valid_position_itr)
			{
				*last_valid_position_itr = *cur_itr;
				cur_itr->reset();
			}
			last_valid_position_itr++;
			num_valid_entry++;
		}
		else
		{
			cur_itr->reset();
		}
	}
	// resize the list
	mTrajectoryList.resize(num_valid_entry);
}

void TrajectorySet::readFromFile(std::string file_name)
{
	std::cout << "Level" << mLevelID << " set <" << mName << ">: Reading trajectories...";
	// read csv files
	std::ifstream infile(file_name);
	std::string	linestr;
	while (	std::getline(infile,linestr) )
	{
		// check if the line starts with numeric
		if ( isdigit(linestr[0]) )
		{
			boost::shared_ptr<PostProcessingVisionTrajectory>		pTraj;
			pTraj.reset(new PostProcessingVisionTrajectory(mName));
			pTraj->fromCSVString(linestr);

			mTrajectoryList.push_back(pTraj);
		}
		// else, just skip it.
	}

	std::cout << "Done! " << mTrajectoryList.size() << " trajectories read." << std::endl;
}

void TrajectorySet::readFromDBTable()
{

}

void TrajectorySet::saveToFile(std::string file_name)
{
	// save to csv files
	std::ofstream outfile(file_name);

	// save header
	outfile << "Start Time,Duration,Trajectory" << std::endl;

	std::vector<boost::shared_ptr<PostProcessingVisionTrajectory> >::const_iterator	traj_itr;
	for (traj_itr=mTrajectoryList.begin();traj_itr != mTrajectoryList.end();traj_itr++)
	{
		outfile << (*traj_itr)->toCSVString(1) << std::endl;
	}

	outfile.close();
}

void TrajectorySet::saveToDBTable()
{

}

void TrajectorySet::resetSetNameToTrajectories()
{
	std::vector<boost::shared_ptr<PostProcessingVisionTrajectory> >::iterator itr;
	for (itr=mTrajectoryList.begin();itr!=mTrajectoryList.end();itr++)
	{
		(*itr)->setSetName(mName);
	}
}


void TrajectorySet::displayTrajectory(cv::Mat &display_image)
{
	for (auto itr=mTrajectoryList.begin();itr!=mTrajectoryList.end();itr++)
	{
		(*itr)->draw(display_image);
	}
}

void TrajectorySet::filterShortTrajectory(float short_length_threshold)
{
	unsigned int num_tracks_before = mTrajectoryList.size();

	std::cout << "Level" << mLevelID << " set <" << mName << ">: filtering short trajectory...";

	std::vector<boost::shared_ptr<PostProcessingVisionTrajectory> >::iterator cur_itr;
	std::vector<boost::shared_ptr<PostProcessingVisionTrajectory> >::iterator last_valid_position_itr;
	for (cur_itr=mTrajectoryList.begin(),last_valid_position_itr=cur_itr;
			cur_itr!=mTrajectoryList.end();cur_itr++)
	{
		// for debugging purpose, draw two candidate trajectories.
//		std::cout << "length" << (*cur_itr)->length() << std::endl;
//		cv::Mat	temp_disp;
//		__getScreenShotImage().copyTo(temp_disp);
//		(*cur_itr)->draw(temp_disp,CV_RGB(255,0,0));
//		cv::imshow("ScreenShot Display",temp_disp);
//		cv::waitKey(0);
		if ((*cur_itr)->length() < short_length_threshold)	// if this is too short
		{
			(*cur_itr)->setInvalid();
		}
	}

	this->compact();

	std::cout << num_tracks_before-mTrajectoryList.size() << " trajectories were short and removed." << std::endl;
}

void TrajectorySet::filterCartTrajectory(double start_time_window,
		float dist_threshold,
		float front_and_back_dist_threshold)
{
	unsigned int num_tracks_before = mTrajectoryList.size();

	std::cout << "Level" << mLevelID << " set <" << mName << ">: filtering cart trajectory...";

	// hardcoded params. Move to settings later
//	double		start_time_window = 0.6f;
//	float		dist_threshold = 5.f;
//	float		front_and_back_dist_threshold = 23.f;
	// sort trajectories via start time
	std::sort(mTrajectoryList.begin(),mTrajectoryList.end(),VisionTrajectoryStartTimeCompare());

	cv::Mat	temp_disp;

	// cluster trajectories that have similar start time
	for (unsigned int i=0;i<mTrajectoryList.size();i++)
	{
		if (mTrajectoryList[i]->isValid())
		{
			for (unsigned int j=i+1;j<mTrajectoryList.size();j++)
			{
				if (mTrajectoryList[j]->isValid())
				{
					// if the start time of trajectory[i] and trajectory[j] is within a window,
					// calculate their minDistance
					// trajectories are now in start-time ascending order, so later index has always later (larger) start time

					assert(mTrajectoryList[j] && mTrajectoryList[i]);

					// if the both ends points are close enough, cause shopper and cart supposed to enter and exit
					// the scene together
					if (std::max(mTrajectoryList[i]->distToFront(mTrajectoryList[j]->getFrontPoint()),mTrajectoryList[i]->distToBack(mTrajectoryList[j]->getBackPoint())) > front_and_back_dist_threshold)
					{
						continue;
					}

					if (mTrajectoryList[j]->getStartTime() - mTrajectoryList[i]->getStartTime() < start_time_window)
					{

//						std::cout << "front dist: " << mTrajectoryList[i]->distToFront(mTrajectoryList[j]->getFrontPoint()) << std::endl;
//						std::cout << "back dist: " << mTrajectoryList[i]->distToBack(mTrajectoryList[j]->getBackPoint()) << std::endl;


						bool  isin;
						float dist = mTrajectoryList[i]->minDistance(*(mTrajectoryList[j]),isin);

//						std::cout << "final dist : " << dist << std::endl;

						// for debugging purpose, draw two candidate trajectories.
//						__getScreenShotImage().copyTo(temp_disp);
//						mTrajectoryList[i]->draw(temp_disp,CV_RGB(255,0,0));
//						mTrajectoryList[j]->draw(temp_disp,CV_RGB(0,0,255));
//						cv::imshow("ScreenShot Display",temp_disp);
//						cv::waitKey(0);

						if (dist < dist_threshold)
						{
							// delete one with shorter distance because carts tend to stay but shoppers tend to
							// move around.  So, shoppers tend to have larger distance to the other (cart) trajectory
							if (isin)
							{
								// delete j
								mTrajectoryList[j]->setInvalid();
							}
							else
							{
								// delete i
								mTrajectoryList[i]->setInvalid();
							}

							break;	//
						}
					} // of if
					else
					{
						// mTrajectoryList is sorted by start time. if the start time gap between i and j gets larger than threshold, it's going to monotonically increase
						// so no need to look for further
						break;
					}

				} // of if (mTrajectoryList[j])

			} // of for (unsigned int j=i+1;...)

		} // of if (mTrajectoryList[i])
	}

	// compact trajectory set
	this->compact();

	std::cout << num_tracks_before-mTrajectoryList.size() << " trajectories were estimated as cart trajectories and removed." << std::endl;
}

void TrajectorySet::convertToFloorPlan(VisionSiteCalibration *site_calib)
{
	boost::shared_ptr<VisionCameraCalibration> cam_calib = 	site_calib->getCalibration(this->getName());
	std::vector<boost::shared_ptr<PostProcessingVisionTrajectory> >::iterator trajectory_itr;
	cv::Mat	hm = site_calib->getHomography();	// for world to floorplan
	for (trajectory_itr = mTrajectoryList.begin();trajectory_itr!=mTrajectoryList.end();trajectory_itr++)
	{
		// for debugging purpose,
//		std::cout << "Camera: " << (*trajectory_itr)->toXmlString(4) << std::endl;
		(*trajectory_itr)->convertToWorld(cam_calib);	// cam to world using intrinsic/extrinsic calibration
//		std::cout << "World: " << (*trajectory_itr)->toXmlString(4) << std::endl;
		// convert to floorplan by applying world to local homography
		(*trajectory_itr)->applyHomography(hm);
//		std::cout << "Floorplan: " << (*trajectory_itr)->toXmlString(4) << std::endl;
	}
}

void TrajectorySet::convertToWorld(VisionSiteCalibration *site_calib)
{
	boost::shared_ptr<VisionCameraCalibration> cam_calib = 	site_calib->getCalibration(this->getName());
	std::vector<boost::shared_ptr<PostProcessingVisionTrajectory> >::iterator trajectory_itr;
	for (trajectory_itr = mTrajectoryList.begin();trajectory_itr!=mTrajectoryList.end();trajectory_itr++)
	{
		// for debugging purpose,
//		std::cout << "Camera: " << (*trajectory_itr)->toXmlString(4) << std::endl;
		(*trajectory_itr)->convertToWorld(cam_calib);	// cam to world using intrinsic/extrinsic calibration
//		std::cout << "World: " << (*trajectory_itr)->toXmlString(4) << std::endl;
	}
}

void TrajectorySet::convertWorldToFloorPlan(VisionSiteCalibration *site_calib)
{
	std::vector<boost::shared_ptr<PostProcessingVisionTrajectory> >::iterator trajectory_itr;
	cv::Mat	hm = site_calib->getHomography();	// for world to floorplan
	for (trajectory_itr = mTrajectoryList.begin();trajectory_itr!=mTrajectoryList.end();trajectory_itr++)
	{
		// for debugging purpose,
		// convert to floorplan by applying world to local homography
		(*trajectory_itr)->applyHomography(hm);
//		std::cout << "Floorplan: " << (*trajectory_itr)->toXmlString(4) << std::endl;
	}
}

void TrajectorySet::addSubSet(boost::shared_ptr<TrajectorySet> p_subset)
{
	mSubSetList.push_back(p_subset);
}

void TrajectorySet::parseJoinSetting(std::string join_setting_string)
{
	std::vector<std::string>	tokens = split(join_setting_string,":");

	if (tokens[0] == "JO")
	{
		// this is join overlap
		mJoinMethod = VisionPostProcessing_OverlapJoin;
		// supposed to have single threshold next
		mJODistanceThreshold = atof(tokens[1].c_str());
	}
	else if (tokens[0] == "JR")
	{
		// this is join rect
		mJoinMethod = VisionPostProcessing_JR;
		// supposed to have series of comma-separated three numbers
		mJRThresholdVec.clear();
		for (unsigned int i=1;i<tokens.size();i++)
		{
			std::vector<std::string> params = split(tokens[i],",");
			assert(params.size() == 3);
			double min_td = atof(params[0].c_str());
			double max_td = atof(params[1].c_str());
			float d = atof(params[2].c_str());
			JRThresholdStruct	jts(min_td,max_td,d);
			mJRThresholdVec.push_back(jts);
		}
	}
	else
	{
		VML_ERROR(MsgCode::unknownMethodErr,"Join method " << tokens[0] << " is not know.");
	}
}


FurnitureLines::FurnitureLines()
{

}

FurnitureLines::~FurnitureLines()
{

}

void FurnitureLines::readfromFile(std::string file_name)
{
	// read csv lines
	std::ifstream infile(file_name);
	std::string	linestr;
	while (	std::getline(infile,linestr) )
	{
		// check if the line starts with numeric
		if ( isdigit(linestr[0]) )
		{
			std::vector<std::string> point_strs = split(linestr,",");
			assert(point_strs.size()==4);	// assert there are four coords
			float b_x = atof(point_strs[0].c_str());
			float b_y = atof(point_strs[1].c_str());
			float e_x = atof(point_strs[2].c_str());
			float e_y = atof(point_strs[3].c_str());

			cv::Point2f	b_(b_x,b_y);
			cv::Point2f e_(e_x,e_y);
			this->addFurnitureLine(b_,e_);
		}
		// else, just skip it.
	}

}

void FurnitureLines::addFurnitureLine(cv::Point2f a, cv::Point2f b)
{
	FurnitureLines::Line l(a,b);

	mLines.push_back(l);
}

bool FurnitureLines::doesIntersect(cv::Point2f l1b,cv::Point2f l1e)
{
	for (std::vector<FurnitureLines::Line>::const_iterator it=mLines.begin();
			it!=mLines.end();it++)
	{
		if (lineSegmentIntersect<float>(l1b,l1e,it->b,it->e,NULL))
		{
			return true;
		}
	}

	return false;
}


void FurnitureLines::draw(cv::Mat disp_image)
{
	for (std::vector<FurnitureLines::Line>::const_iterator it=mLines.begin();
			it!=mLines.end();it++)
	{
		cv::circle(disp_image,it->b,3,CV_RGB(0,0,255),2);
		cv::circle(disp_image,it->e,3,CV_RGB(255,0,0),2);
		cv::line(disp_image,it->b,it->e,CV_RGB(0,255,0));
	}
}

VisionCameraCoverage::VisionCameraCoverage()
{

}

VisionCameraCoverage::~VisionCameraCoverage()
{

}

void VisionCameraCoverage::readFromXMLFile(std::string filename)
{
	tinyxml2::XMLDocument	coveragefile;
	tinyxml2::XMLError		ret;

	if ((ret=coveragefile.LoadFile(filename.c_str())) != tinyxml2::XML_SUCCESS)
	{
		VML_ERROR(MsgCode::IOErr,"Could not XML parse "<<filename<<".")
	}

	// get root element
	tinyxml2::XMLElement *root = coveragefile.FirstChildElement("camera_models");

	// parse camera models
	for (tinyxml2::XMLElement* e = root->FirstChildElement("camera_model"); e!=NULL; e = e->NextSiblingElement("camera_model"))
	{
		CameraModelInfo	cameraModel;

		// parse name
		const char *temp_char = e->Attribute("name");
		std::string temp_str = temp_char;
		cameraModel.name = temp_str;
		// parse ccd_width
		temp_char = e->Attribute("ccd_width");
		cameraModel.ccd_width = atof(temp_char);
		// parse ccd_height
		temp_char = e->Attribute("ccd_height");
		cameraModel.ccd_height = atof(temp_char);
		// parse focal_length
		temp_char = e->Attribute("focal_length");
		cameraModel.focal_length = atof(temp_char);
		// parse distortion parameters
		temp_char = e->Attribute("distortion_parameters");
		temp_str.clear();
		temp_str = temp_char;
		parseDoubleList(temp_str,cameraModel.distortion_parameters);
		// parse stream url
		temp_char = e->Attribute("stream_url");
		cameraModel.stream_url = temp_char;
		// parse config_url
		temp_char = e->Attribute("config_url");
		cameraModel.config_url = temp_char;
		// parse username
		temp_char = e->Attribute("username");
		cameraModel.username = temp_char;
		// parse password
		temp_char = e->Attribute("password");
		cameraModel.password = temp_char;
		// parse camera coverages
		for (tinyxml2::XMLElement* ce = e->FirstChildElement("camera_height"); ce != NULL; ce = ce->NextSiblingElement("camera_height"))
		{
			CoverageModel	cm;
			temp_char = ce->Attribute("value");
			cm.camera_height = atof(temp_char);
			temp_char = ce->Attribute("coverage_width");
			cm.coverage_width = atof(temp_char);
			temp_char = ce->Attribute("coverage_height");
			cm.coverage_height = atof(temp_char);
			temp_char = ce->Attribute("undistorted_image_width");
			cm.undistorted_image_width = atof(temp_char);
			cameraModel.coverages.push_back(cm);
		}

		mVisionCoverageMap.insert(std::pair<std::string,CameraModelInfo>(cameraModel.name,cameraModel));
	}

	// initialize rotation and translation matrix

	// initialize homography

}

bool VisionCameraCoverage::getCoverage(std::string type, VisionCameraCoverage::CameraModelInfo &cminfo)
{
	auto coverage = mVisionCoverageMap.find(type);
	if (coverage != mVisionCoverageMap.end())
	{
		cminfo = coverage->second;
		return true;
	}
	else
	{
		return false;
	}
}


VisionCameraCalibration::VisionCameraCalibration(std::string name,
		std::string type,
		cv::Point2f	location,
		float	camera_height,
		float	orientation,
		float	focalLength,
		cv::Point2f	ccdSize,
		cv::Point2i	imageSize,
		boost::shared_ptr<CameraModelOpenCV> p_camera_model):
				mName(name),
				mType(type),
				mLocation(location),
				mCameraHeight(camera_height),
				mOrientation(orientation),
				mFocalLength(focalLength),
				mCcdSize(ccdSize),
				mImageSize(imageSize),
				mpCameraModel(p_camera_model)
{
	// create rotation matrix
	mRotation = cv::Mat_<float>(3,3);

	// legacy way of rotation matrix; from POSSE
	/*
	def rotate(x = 0.0, y = 0.0, z = 0.0, theta = 0.0):
	  u = float(x)
	  v = float(y)
	  w = float(z)
	  theta = radians(float(theta))

	  Lsquared = u*u + v*v + w*w
	  L = sqrt(Lsquared)

	  m = identity(4, float32)

	  m[0,0] = (u**2 + (v**2 + w**2)*cos(theta)) / Lsquared
	  m[0,1] = (u*v*(1 - cos(theta)) - w*L*sin(theta)) / Lsquared
	  m[0,2] = (u*w*(1 - cos(theta)) + v*L*sin(theta)) / Lsquared

	  m[1,0] = (u*v*(1 - cos(theta)) + w*L*sin(theta)) / Lsquared
	  m[1,1] = (v**2 + (u**2 + w**2)*cos(theta)) / Lsquared
	  m[1,2] = (v*w*(1 - cos(theta)) - u*L*sin(theta)) / Lsquared

	  m[2,0] = (u*w*(1 - cos(theta)) - v*L*sin(theta)) / Lsquared
	  m[2,1] = (v*w*(1 - cos(theta)) + u*L*sin(theta)) / Lsquared
	  m[2,2] = (w**2 + (u**2 + v**2)*cos(theta)) / Lsquared
	  return m
	  */

	/*
	 * Minimized version of legacy rotation matrix calculation:
	 * u = 0, v = 0, w = 1, theta = mOrientation.
	  m[0,0] = (cos(theta))
	  m[0,1] = - sin(theta)
	  m[0,2] = 0

	  m[1,0] = (sin(theta))
	  m[1,1] = (cos(theta))
	  m[1,2] = 0

	  m[2,0] = 0.f
	  m[2,1] = 0.f
	  m[2,2] = 1.f
	  */

	float rad_orientation = mOrientation*M_PI/180.f;

	/*
	float sin_theta = sin(rad_orientation);
	float cos_theta = cos(rad_orientation);
	mRotation.at<float>(0,0) = cos_theta;
	mRotation.at<float>(0,1) = -sin_theta;
	mRotation.at<float>(0,2) = 0.f;
	mRotation.at<float>(1,0) = sin_theta;
	mRotation.at<float>(1,1) = cos_theta;
	mRotation.at<float>(1,2) = 0.f;
	mRotation.at<float>(2,0) = 0.f;
	mRotation.at<float>(2,1) = 0.f;
	mRotation.at<float>(2,2) = 1.f;

	std::cout << "rot matrix : " << mRotation << std::endl;
	*/


	// using cv::Rodrigues
	cv::Vec3f	rot_vec(0.f,0.f,rad_orientation);
	cv::Rodrigues(rot_vec,mRotation);

	// translation vector
	mTranslation.x = mLocation.x;
	mTranslation.y = mLocation.y;
	mTranslation.z = 0.f;

}

VisionCameraCalibration::~VisionCameraCalibration()
{

}

//void VisionCameraCalibration::convertFromCamToFloorplan(std::vector<cv::Point2f> &point_list)
//{
//	// no inplace processing
//	std::vector<cv::Point2f> converted_point_list;
//
//	// world projection - includes undistortion
//	this->projectToWorld(converted_point_list,converted_point_list);
//	// floorplan projection
//	this->projectToFloorplan(converted_point_list,converted_point_list);
//	point_list = converted_point_list;
//}

void VisionCameraCalibration::projectToWorld(std::vector<cv::Point2f> &input_list,std::vector<cv::Point2f> &output_list)
{
	std::vector<cv::Point3f>	world_coords;
	mpCameraModel->perspectiveBackProjection(input_list,world_coords);

	// rotate and translate points
	// NOTE: use legacy way for now. Use cv::Rodrigues later
	for (auto witr = world_coords.begin();witr!=world_coords.end();witr++)
	{
		cv::Mat	_w(*witr,false);	// convert point to mat for matrix multiplication
		_w = mRotation*_w;
		(*witr) += mTranslation;
		// drop z to make it 2D floor-world
		cv::Point2f	f_coords;
		f_coords.x = witr->x;
		f_coords.y = witr->y;
		output_list.push_back(f_coords);
	}
}

VisionSiteCalibration::VisionSiteCalibration()
{
}

VisionSiteCalibration::~VisionSiteCalibration()
{
}

void VisionSiteCalibration::readFromXMLFiles(std::string site_calibration_filename,
		std::string camera_coverage_filename)
{
	// read camera coverage first
	mVisionCameraCoverage.readFromXMLFile(camera_coverage_filename);


	// read calibration xml file
	tinyxml2::XMLDocument	calibfile;
	tinyxml2::XMLError	ret;

	if ((ret=calibfile.LoadFile(site_calibration_filename.c_str())) != tinyxml2::XML_SUCCESS)
	{
		VML_ERROR(MsgCode::IOErr,"Could not XML parse "<<site_calibration_filename<<".")
	}

	// get root element
	tinyxml2::XMLElement *calibroot = calibfile.FirstChildElement("root");

	// parse local and world coords
	const tinyxml2::XMLElement *coordselement = calibroot->FirstChildElement("floorplan")->FirstChildElement("coord_mapping");
	const char* local_coords_char = coordselement->Attribute("local");
	std::string	local_coords_str(local_coords_char);
	std::vector<std::string> local_coords_strs = split(local_coords_str,";");
	for (unsigned int i=0;i<4;i++)
	{
		std::string coords_str_ = local_coords_strs[i];
		std::string coords_str = strip(coords_str_,"()");
		std::vector<std::string> coords_strs = split(coords_str,",");
		cv::Point2f	p;
		p.x = atof(coords_strs[0].c_str());
		p.y = atof(coords_strs[1].c_str());
		mLocal.push_back(p);
	}

	const char* world_coords_char = coordselement->Attribute("world");
	std::string	world_coords_str(world_coords_char);
	std::vector<std::string> world_coords_strs = split(world_coords_str,";");
	for (unsigned int i=0;i<4;i++)
	{
		std::string coords_str_ = world_coords_strs[i];
		std::string coords_str = strip(coords_str_,"()");
		std::vector<std::string> coords_strs = split(coords_str,",");
		cv::Point2f p;
		p.x = atof(coords_strs[0].c_str());
		p.y = atof(coords_strs[1].c_str());
		mWorld.push_back(p);
	}

	// initialize homography
	mFloorHomography = cv::findHomography(mWorld,mLocal,0);

	// parse camera calibration info
	for (tinyxml2::XMLElement* e = calibroot->FirstChildElement("camera"); e!=NULL; e = e->NextSiblingElement("camera"))
	{

		const char *temp_char = e->Attribute("name");
		std::string name_str = temp_char;
		temp_char = e->Attribute("type");
		std::string type_str = temp_char;

		tinyxml2::XMLElement *calib_e = e->FirstChildElement("calibration");
		std::vector<float>	float_vec;
		temp_char = calib_e->Attribute("location");
		std::string temp_str = temp_char;
		parseCoords(temp_str,float_vec);
		cv::Point2f location(float_vec[0],float_vec[1]);
		// NOTE: for opencv camera model, the z distance is from the focal point, which is an imaginary point
		//       that could be quite far from the actual ccd.
		//       our camera height is the distance from the floor to the lens, so there might be significant
		//       offset between the actual camera height to the focal-point height.
		//       Adding a constant term to compensate this. This is emperically chosen.
		float camera_height = float_vec[2] + CAM_HEIGHT_OFFSET;
		float_vec.clear();

		temp_char = calib_e->Attribute("orientation");
		float orientation = atof(temp_char);

		temp_char = calib_e->Attribute("focalLength");
		float focalLength = atof(temp_char);

		temp_char = calib_e->Attribute("ccdSize");
		temp_str = temp_char;
		parseCoords(temp_str,float_vec);
		cv::Point2f ccdSize(float_vec[0],float_vec[1]);

		std::vector<int>	int_vec;
		temp_char = calib_e->Attribute("imageSize");
		temp_str = temp_char;
		parseCoords(temp_str,int_vec);
		cv::Point2i imageSize(int_vec[0],int_vec[1]);

		// check if this type model is already in the map
		boost::shared_ptr<CameraModelOpenCV> model;
		std::map<std::string,boost::shared_ptr<CameraModelOpenCV> >::iterator it = mCameraModelMap.find(type_str);
		if (it == mCameraModelMap.end())
		{
			// create a model and insert
			// retrieve intrinsic camera params from coverage
			VisionCameraCoverage::CameraModelInfo cminfo;
			if (!mVisionCameraCoverage.getCoverage(type_str,cminfo))
			{
				VML_ERROR(MsgCode::notSupportedErr,"Cannot find camera info for " << type_str << " type camera in camera_coverage.xml. Aborting...")
			}
			model.reset(new CameraModelOpenCV(imageSize.y,imageSize.x,type_str,camera_height,cminfo.ccd_width,cminfo.ccd_height,cminfo.focal_length,cminfo.distortion_parameters));
		}
		else
		{
			model = it->second;
		}

		boost::shared_ptr<VisionCameraCalibration>	p_cam_calib;
		p_cam_calib.reset(new VisionCameraCalibration(name_str,type_str,location,camera_height,orientation,focalLength,ccdSize,imageSize,model));
		mpVisionCameraCalibrationMap.insert(std::pair<std::string,boost::shared_ptr<VisionCameraCalibration> >(p_cam_calib->getName(),p_cam_calib));
	}

}

boost::shared_ptr<VisionCameraCalibration>	VisionSiteCalibration::getCalibration(std::string camera_name)
{
	std::map<std::string,boost::shared_ptr<VisionCameraCalibration> >::iterator it = mpVisionCameraCalibrationMap.find(camera_name);
	if (it == mpVisionCameraCalibrationMap.end())
	{
		VML_ERROR(MsgCode::assertErr,"Cannot find "<<camera_name<<" in camera calibration map");
		return (boost::shared_ptr<VisionCameraCalibration>());	// return empty pointer to suppress warning
	}
	else
	{
		return it->second;
	}
}

void parseCoords(std::string coords_str, std::vector<float> &coords)
{
	std::string tempstr = strip(coords_str,"()");
	std::vector<std::string> strvec = split(tempstr,",");

	for (unsigned int i=0;i<strvec.size();i++)
	{
		float t = atof(strvec[i].c_str());
		coords.push_back(t);
	}
}

void parseCoords(std::string coords_str, std::vector<int> &coords)
{
	std::string tempstr = strip(coords_str,"()");
	std::vector<std::string> strvec = split(tempstr,",");

	for (unsigned int i=0;i<strvec.size();i++)
	{
		int t = atoi(strvec[i].c_str());
		coords.push_back(t);
	}
}

void parseFloatList(std::string float_list_str, std::vector<float> &flist)
{
	std::vector<std::string>	strvec = split(float_list_str,",");

	for (unsigned int i=0;i<strvec.size();i++)
	{
		float t = atof(strvec[i].c_str());
		flist.push_back(t);
	}
}

void parseDoubleList(std::string float_list_str, std::vector<double> &flist)
{
	std::vector<std::string>	strvec = split(float_list_str,",");

	for (unsigned int i=0;i<strvec.size();i++)
	{
		double t = atof(strvec[i].c_str());
		flist.push_back(t);
	}
}

} // of namespace vml

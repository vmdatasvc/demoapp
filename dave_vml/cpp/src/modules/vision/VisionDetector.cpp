/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * VisionDetector.cpp
 *
 *  Created on: Oct 6, 2015
 *  Updated on: Jan 5, 2016
 *      Author: yyoon
 */

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <modules/vision/VisionDetector.hpp>
#include <modules/vision/VisionTargetManager.hpp>
#include <modules/vision/VisionModuleManager.hpp>
#include <modules/vision/VisionTrackerTarget.hpp>
#include <modules/vision/VisionDetectorTarget.hpp>
#include <modules/vision/VisionVideoModule.hpp>

//#define DEBUG

#ifdef DEBUG
// for debugging
#include "core/Util.hpp"
#endif

namespace vml {

#ifdef VISIONDETECTOR_DEBUG_SHOW
const char* VisionDetector::displayname = "VisionDetectorDebugDisplay";
#endif

VisionDetector::VisionDetector() :
		mRows(0),
		mCols(0),
#ifdef BGSUBSAMPLE
		mRowsSub(0),
		mColsSub(0),
#endif
		mMaxTargets(0),
		mOpenSERadius(0),
		mCloseSERadius(0),
		mContourAreaMinThreshold(0),
		mMergeMahalanobisDistanceThreshold(0.f)
{
} // of VisionDetector

VisionDetector::~VisionDetector()
{
#if CV_MAJOR_VERSION == 2
	mpBgSubtractor.reset();
#elif CV_MAJOR_VERSION == 3

#endif
}


void VisionDetector::initialize(boost::shared_ptr<SETTINGS> settings)
{
	mOpenSERadius = settings->getInt("VisionDetector/morphOpenSERadius",5,false);
	mCloseSERadius = settings->getInt("VisionDetector/morphCloseSERadius",3,false);
	mMergeMahalanobisDistanceThreshold = settings->getFloat("VisionDetector/mergeMahalanobisDistanceThreshold",1.0,false);
	mpSettings = settings;

	// get parameters from settings
	mContourAreaMinThreshold = (int)(settings->getFloat("VisionDetector/contourAreaMinThreshold",0.0005f,false)*(float)(VisionModuleManager::instance()->getVideoModule()->getPixelCount()));

#ifdef VISIONDETECTOR_DEBUG_SHOW
	// set up debug display
	mDisplayImage = cv::Mat(VisionModuleManager::instance()->getVideoModule()->getRows(),VisionModuleManager::instance()->getVideoModule()->getCols(),CV_8UC3);
	cv::imshow(displayname,mDisplayImage);
#endif

	mpVideoModule = VisionModuleManager::instance()->getVideoModule();
	mRows = mpVideoModule->getRows();
	mCols = mpVideoModule->getCols();

	mForeground = cv::Mat(mRows,mCols,CV_8UC1);
}

void VisionDetector::detectForeground(void)
{
	mpVideoModule->currentFrame()->foreground_image.copyTo(mForeground);

	// fetch foreground map from VisionVideoModule
#ifdef VISIONDETECTOR_DEBUG_SHOW
	mDisplayImage = 0;
	mForeground.copyTo(mDisplayImage);
	debugShow(mForeground);
#endif

	// apply small morphological filter to reduce speckles
	static cv::Mat se_open;
	if (se_open.empty())
	{
		se_open = cv::getStructuringElement(cv::MORPH_ELLIPSE,cv::Size(mOpenSERadius,mOpenSERadius));
    }

	cv::morphologyEx(mForeground, mForeground, cv::MORPH_OPEN, se_open);

#ifdef VISIONDETECTOR_DEBUG_SHOW
	mDisplayImage = 0;
	mForeground.copyTo(mDisplayImage);
	debugShow(mForeground);
#endif

} // of detectBlobs()

void VisionDetector::detectTargets(void)
{
	// clear target list
	// reset all the pointers before clearing
	mDetectedTargetList.clear();

	// temporary list for blob contours
	std::vector<std::vector<cv::Point> > tempContourList;

	// optional: Apply slight opening one more time to disconnect merged blobs
	static cv::Mat se_open2;
	if (se_open2.empty())
	{
		se_open2 = cv::getStructuringElement(cv::MORPH_ELLIPSE,cv::Size(mOpenSERadius,mOpenSERadius));
    }

	cv::morphologyEx(mForeground, mForeground, cv::MORPH_OPEN, se_open2);


	mForeground.copyTo(mBlobImage);

#ifdef VISIONDETECTOR_DEBUG_SHOW
	mDisplayImage = 0;
	mBlobImage.copyTo(mDisplayImage);
	debugShow(mDisplayImage);
#endif


	// Contour-based target detection. Note that this is not gonna work for cases when human blob is joined with
	// nearby blobs (typically other human or cart).  We need more sophisticated human blob detection
	// TODO: HOG based

	// find connected components
	cv::Mat	tempForeground;
	mBlobImage.copyTo(tempForeground);
	// we need only outer contours, so use CV_RETR_EXTERNAL mode
	cv::findContours(tempForeground, tempContourList, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_TC89_KCOS );

#ifdef VISIONDETECTOR_DEBUG_SHOW
	// Debug output
	cv::drawContours(mDisplayImage,tempContourList,-1,CV_RGB(255,255,0));
	debugShow(mDisplayImage);
#endif

	// create target instance for each contour first
	if (tempContourList.size() > 0)
	{
		for (unsigned int i=0;i<tempContourList.size();i++)
		{
			// evaluate contour size, and discard if contour area is too small
			if (tempContourList[i].size()<3)
			{
				continue;
			}
			int contour_area = abs((int)cv::contourArea(tempContourList[i]));
			if (contour_area < mContourAreaMinThreshold)
			{
				continue;
			}

			// create target instance for this contour
			boost::shared_ptr<VisionDetectorTarget> temp_ptr;
			temp_ptr.reset(new VisionDetectorTarget(tempContourList[i],mpSettings));
			if (temp_ptr->isValid())
			{
				mDetectedTargetList.push_back(temp_ptr);

//                temp_ptr->drawTarget(mDisplayImage);
            }
			// otherwise, just delete this
			temp_ptr.reset();
		}
	} // of if (mTempContourList.size() > 0)

#ifdef VISIONDETECTOR_DEBUG_SHOW
    // display the surviving target contours
	for (unsigned int i=0;i<mDetectedTargetList.size();i++)
	{
		mDetectedTargetList[i]->drawTarget(mDisplayImage);
		debugShow(mDisplayImage);
	}
#endif

	// merge remaining contours to identify newly detected targets
	// discard all the remaining blobs inside track region

	// Delete invalid blobs that might have been used for tracking update already
	// NOTE: this is not necessary any more because tracking update now use foreground map instead of blob contours.
//	deleteInvalidTargets();

	if (mDetectedTargetList.size() > 0)
	{
		// merge by expected foreground blob area and distance
		// 1. sort by area
		std::sort(mDetectedTargetList.begin(),mDetectedTargetList.end(), VisionDetectorTargetAreaCompare());

		for (int i=0;i<(int)mDetectedTargetList.size();i++)
		{
			// if this target's area is already larger than the human shape area, continue
			if (!(mDetectedTargetList[i]->isValid()&&mDetectedTargetList[i]->needMerge()))
			{
				// this is already merged to something else
				continue;
			}

			int	n_remaining_contours = 0;
			// sort the rest of the list by the distance to the current blob
			std::vector<ContourDistanceType>	contour_dists;
			contour_dists.reserve(mDetectedTargetList.size()-i-1);
			for (int j=i+1;j<(int)mDetectedTargetList.size();j++)
			{
				if (!(mDetectedTargetList[j]->isValid()&&mDetectedTargetList[j]->needMerge()))
				{
					continue;
				}
				else{
					// if not, calculate the distance and push in distance list
					ContourDistanceType	temp_dist;
					temp_dist.contour_label = j;

					temp_dist.distance = mDetectedTargetList[i]->MahalanobisDistance(*mDetectedTargetList[j]);
					if (temp_dist.distance < mMergeMahalanobisDistanceThreshold)
					{
						contour_dists.push_back(temp_dist);
						n_remaining_contours++;
					}
				}
			}

			// sort the distance list
			std::sort(contour_dists.begin(),contour_dists.end(),ContourDistCompare());

			for (int _j=0;_j<n_remaining_contours;_j++)
			{
				int j = contour_dists[_j].contour_label;

				// check and merge
				mDetectedTargetList[i]->checkBlobMergeCriteriaAndMerge(mDetectedTargetList[j]);
			} // of for j
		} // of for(i=0;i<(int)mDetectedTargetList.size();i++)

	}

	deleteInvalidTargets();

#ifdef VISIONDETECTOR_DEBUG_SHOW
	for (int i=0;i<(int)mDetectedTargetList.size();i++)
	{
		mDetectedTargetList[i]->drawTarget(mDisplayImage);
	}

	debugShow(mDisplayImage);
#endif
}

void VisionDetector::deleteInvalidTargets()
{
	int	num_valid_entry = 0;
	std::vector<boost::shared_ptr<VisionDetectorTarget> >::iterator cur_itr;
	std::vector<boost::shared_ptr<VisionDetectorTarget> >::iterator last_valid_position_itr;
	for (cur_itr=mDetectedTargetList.begin(),last_valid_position_itr=cur_itr;
			cur_itr!=mDetectedTargetList.end();cur_itr++)
	{
		if ((*cur_itr)->isValid())	// if this is a valid pointer
		{
			if (cur_itr != last_valid_position_itr)
			{
				*last_valid_position_itr = *cur_itr;
				cur_itr->reset();
			}
			last_valid_position_itr++;
			num_valid_entry++;
		}
		else
		{
			cur_itr->reset();
		}
	}
	// resize the list
	mDetectedTargetList.resize(num_valid_entry);
}

//void VisionDetector::addNewTargetsToTargetManager(void)
//{
//	// add all the remaining valid blobs to the active target list
//	if (mDetectedTargetList.size() > 0)
//	{
//		std::vector<boost::shared_ptr<VisionDetectorTarget> >::iterator titr;
//		for (titr=mDetectedTargetList.begin();titr!=mDetectedTargetList.end();titr++)
//		{
//			if (((*titr)->isValid()) && ((*titr)->blobToPersonAreaRatio() > mcBlobToPersonAreaRatioThreshold)) // this shouldn't be false, but double checking
//			{
//				// create target instance
////				boost::shared_ptr<VisionTrackerTarget> tptr;
////				tptr.reset(new VisionTrackerTarget());
////				tptr->initialize((*titr));
//				// add to vision target manager
//				VisionModuleManager::instance()->getVisionTargetManager()->addNewTarget(*titr);	//
//			} // of if (blob_list[i].valid)
//		} // of for i
//	}
//}

void VisionDetector::retrieveDetectedTargets(std::vector<boost::shared_ptr<VisionDetectorTarget> > &dt_list)
{
	if (mDetectedTargetList.size() == 0)
	{
		// nothing to do.
		return;
	}
	else
	{
		// just copy
		dt_list = mDetectedTargetList;
		// clear
		mDetectedTargetList.clear();
	}
}

void VisionDetector::maskOffForegroundImage(cv::Mat &mask)
{
// when mask has the same size as foreground image
}

void VisionDetector::maskOffForegroundImage(cv::Mat &mask, cv::Rect crop_rect)
{
	// DEBUG: save foreground before mask off
// mask is a crop, crop_rect is the cropping info
	uchar *mask_p = (uchar*)mask.data;
	for (int i=0;i<mask.rows;i++)
	{
		for (int j=0;j<mask.cols;j++)
		{
			if (*mask_p)
			{
				cv::Point	pos = crop_rect.tl()+cv::Point(j,i);
				mForeground.at<uchar>(pos) = 0;
			}
			mask_p++;
		}
	}

	// DEBUG: save foreground after mask off

}

void VisionDetector::maskOffForegroundImage(cv::Rect mask_rect)
{
// mask off rect region.

}

void VisionDetector::drawDetectedTargets(cv::Mat &image) const
{
	for (std::vector<boost::shared_ptr<VisionDetectorTarget> >::const_iterator titr=mDetectedTargetList.begin();
			titr!=mDetectedTargetList.end();titr++)
	{
		if (*titr)	// after the compaction, all the entries should be valid, but to ensure safety
		{
			(*titr)->drawTarget(image);
		}
	}
}

#ifdef VISIONDETECTOR_DEBUG_SHOW
void VisionDetector::debugShow(cv::Mat &dispimage)
{
	if (dispimage.type() == CV_8UC3)
	{
		dispimage.copyTo(mDisplayImage);
	}
	else if (dispimage.type() == CV_8UC1)
	{
		cv::cvtColor(dispimage,mDisplayImage,CV_GRAY2RGB);
	}
	cv::imshow(displayname,mDisplayImage);
	cv::waitKey(20);
}
#endif

void VisionDetector::copyBackgroundImageTo(cv::Mat &bgimg) const
{
	// get background per-request-basis to save time
	// NOTE: if this is needed to be called more than twice, cache it to save time.
	mpBgSubtractor->getBackgroundImage(bgimg);
}

void VisionDetector::copyForegroundImageTo(cv::Mat &fgimg) const
{
	mForeground.copyTo(fgimg);
}

void VisionDetector::copyBlobImageTo(cv::Mat &blbimg) const
{
	mBlobImage.copyTo(blbimg);
}
} // of namespace vml

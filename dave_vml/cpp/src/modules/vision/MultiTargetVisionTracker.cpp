/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * MultiTargetVisionTracker.cpp
 *
 *  Created on: Oct 6, 2015
 *      Author: yyoon
 */

#include <core/Error.hpp>

#include <modules/vision/VisionModuleManager.hpp>
#include <modules/vision/VisionVideoModule.hpp>
#include <modules/iot/IOTCommunicator.hpp>
#include <modules/vision/VisionTargetManager.hpp>
#include <modules/vision/MultiTargetVisionTracker.hpp>
#include <modules/vision/VisionDetector.hpp>
#include <modules/vision/BaseVisionTracker.hpp>


#include <modules/utility/Util.hpp>

//#define	SHOW_TRACKER_DISPLAY

// Visual tracking codes here
namespace vml {

pthread_mutex_t		VisionMultitargetTrackerMutex;		// mutex for multitarget tracker
pthread_mutex_t		VisionMultitargetTrackerCompletedTrajectoryMutex;

/* class MultiTargetVisionTracker */
#ifdef _WIN32
/* class MultiTargetVisionTracker */
MultiTargetVisionTracker::MultiTargetVisionTracker(boost::shared_ptr<VisionVideoModule> vbp,
		boost::shared_ptr<SETTINGS> settings)
{

	// register modules
	VisionModuleManager::instance()->registerSettings(settings);
	mpSettings = VisionModuleManager::instance()->getSettings();
	VisionModuleManager::instance()->registerVideoModule(vbp);
	mpVideoModule = VisionModuleManager::instance()->getVideoModule();
	VisionModuleManager::instance()->registerDetector(boost::shared_ptr<VisionDetector>(new VisionDetector()));
	mpDetector = VisionModuleManager::instance()->getDetector();
	VisionModuleManager::instance()->registerVisionTargetManager(boost::shared_ptr<VisionTargetManager>(new VisionTargetManager()));
	mpVisionTargetManager = VisionModuleManager::instance()->getVisionTargetManager();

	// initialize modules
	mpDetector->initialize(mpSettings);
	mpVisionTargetManager->initialize(mpSettings);
}
#else
MultiTargetVisionTracker::MultiTargetVisionTracker(std::string video_url, boost::shared_ptr<SETTINGS> settings) :
		mVideoURL(video_url),
		nFrames(0),
#ifndef _WIN32
		mGrabberThread(0),
		mBackSubThread(0),
		mProcessorThread(0),
		mStopThreads(false),
		mbGrabReady(false),
		mbBgSubReady(false),
		mbBufferDone(false),
#endif
		mbMultithreadsOn(false),
		mbDisplayReady(false),
		mHeartBeatInterval(20.f),
		mMaxHeartBeatIntervalProportion(1.5f),
		mPreviousHeartbeatTime(gCurrentTime()),
		mpIOTCommunicator(NULL)
{
	// register modules
	// replace registered setting,
	VisionModuleManager::instance()->registerSettings(settings);
	mpSettings.swap(settings);

	// create a new detector instance and register
	VisionModuleManager::instance()->registerDetector(boost::shared_ptr<VisionDetector>(new VisionDetector()));
	mpDetector = VisionModuleManager::instance()->getDetector();
	VisionModuleManager::instance()->registerVisionTargetManager(boost::shared_ptr<VisionTargetManager>(new VisionTargetManager()));
	mpVisionTargetManager = VisionModuleManager::instance()->getVisionTargetManager();

	// create and register video module
	boost::shared_ptr<VisionVideoModule> temp_p_video;
	temp_p_video.reset(new vml::VisionVideoModule());
	VisionModuleManager::instance()->registerVideoModule(temp_p_video);
	mpVideoModule.swap(temp_p_video);

	// initialize settings variables
	mHeartBeatInterval = mpSettings->getDouble("heartbeatInterval",20.f,false);
	mMaxHeartBeatIntervalProportion = std::max(mpSettings->getDouble("maxHeartbeatIntervalProportion",1.5f,false),(double)(1.5f));
}
#endif

MultiTargetVisionTracker::~MultiTargetVisionTracker()
{
}

void MultiTargetVisionTracker::setIOTCommunicator(boost::shared_ptr<IOT_Communicator> iotc)
{
	VisionModuleManager::instance()->registerIOTCommunicator(iotc);
	mpIOTCommunicator.swap(iotc);
}


void MultiTargetVisionTracker::initializeModules()
{
	std::cout << "Initializing vision tracker modules." << std::endl;
	// asserting settings and video modules
	VML_ASSERT(mpSettings);
	VML_ASSERT(mpVideoModule);

	// initialize video module
	mpVideoModule->initialize(mpSettings,mVideoURL);
	// initialize modules
	std::cout << "Initializing detector and target manager" << std::endl;
	mpDetector->initialize(mpSettings);
	mpVisionTargetManager->initialize(mpSettings);

	std::cout << "All modules are initialized" << std::endl;
}

#ifdef _WIN32
bool MultiTargetVisionTracker::process(ait::Image32& img, double frameTime) // process each video frame
{
    // capture one frame
	// NOTE: this capture will initialize video buffer
    if (!(VisionModuleManager::instance()->getVideoModule()->grabFrame2(img,frameTime)))
    {
    	return false;
    }
//
//    if (!mHandlersInitialized)
//    {
//    	VisionModuleManager::instance()->registerDetector(boost::shared_ptr<VisionDetector>(new VisionDetector()));
//    	VisionModuleManager::instance()->registerVisionTargetManager(boost::shared_ptr<VisionTargetManager>(new VisionTargetManager()));
//
//    	mHandlersInitialized = true;
//
//    	// initialize modules
////    	VisionModuleManager::instance()->getDetector()->initialize();
//    	VisionModuleManager::instance()->getVisionTargetManager()->initialize();
//
//    } // of if (!mHandlersInitialized)

    if (!VisionModuleManager::instance()->getVideoModule()->isTraining())
    {
    	// if training complete
        // run blob detection first
        VisionModuleManager::instance()->getDetector()->detectForeground();
        // update existing targets
        VisionModuleManager::instance()->getVisionTargetManager()->updateTargets();

        // create new targets after verification
        VisionModuleManager::instance()->getDetector()->detectTargets();
        VisionModuleManager::instance()->getVisionTargetManager()->addNewTargetsFromDetector();
    }


    return true;
}

bool MultiTargetVisionTracker::trackerReady()
{
	if(!VisionModuleManager::instance()->getVideoModule()->isTraining())
	{
		return true;
	}
	else
	{
		return false;
	}
}
#else
bool MultiTargetVisionTracker::process() // process each video frame
{
    // capture one frame
    if (not mpVideoModule->grabFrame())
    {
    	return false;
    }

    // run blob detection first
    mpDetector->detectForeground();
    // update existing targets
    mpVisionTargetManager->updateTargets();

    // create new targets after verification
    mpDetector->detectTargets();
//    VisionModuleManager::instance()->getDetector()->addNewTargetsToTargetManager();
    mpVisionTargetManager->addNewTargetsFromDetector();

    nFrames++;

    return true;
}
#endif

#ifndef _WIN32
// 2-thread pipelining
void MultiTargetVisionTracker::_2ThreadPipeliningStart()
{
    pthread_attr_init(&mThreadAttr);
    pthread_attr_setdetachstate(&mThreadAttr, PTHREAD_CREATE_JOINABLE);

    // grab one frame to temp
	if (!mpVideoModule->grabAndBackgroundSubtractToGrabFrame())
	{
		// TODO: return exception

	}
	mbGrabReady = true;
	mStopThreads = false;
	mbMultithreadsOn = true;

	// start frame grab thread
    if(pthread_create(&mGrabberThread, &mThreadAttr, _2ThreadPipeliningFrameGrabThread, (void *)this))
    {
//        fprintf(stderr, "Error in pthread_create(frameGrabThread)\n");
    	// TODO: return exception

    } else {
//        printf( " -- pthread_create(frameGrabThread) -- \n");
    	// TODO: log message here
    }

    // start process thread
    if(pthread_create(&mProcessorThread, &mThreadAttr, _2ThreadPipeliningProcessThread, (void *)this))
    {
//        fprintf(stderr, "Error in pthread_create(frameGrabThread)\n");
    	// TODO: return exception

    } else {
//        printf( " -- pthread_create(frameGrabThread) -- \n");
    	// TODO: log message here
    }


}

void* MultiTargetVisionTracker::_2ThreadPipeliningFrameGrabThread(void *apdata)
{
	printf("Initiating frameGrabThread\n");
	MultiTargetVisionTracker* pc = (MultiTargetVisionTracker*)apdata;
	pc->_2ThreadPipeliningFrameGrabLoop();
	printf("frameGrabThread Done\n");
	return NULL;
}

void MultiTargetVisionTracker::_2ThreadPipeliningFrameGrabLoop(void)
{
	while (!mStopThreads)
	{
		while (mbGrabReady){
//			printf("Waiting temp frame to be cleared\n");
			usleep(10);	// wait until fetch is ready
		}

//		printf("Temp frame cleared. Grabbing new temp frame\n");
		if (!mpVideoModule->grabAndBackgroundSubtractToGrabFrame())
		{
			// skip this frame and grab next one
			// NOTE: Assuming frame rate is usually 5 fps, wait for 0.2 seconds to try to capture next frame.
			usleep(200000);
			continue;
		}

		pthread_mutex_lock(&VisionMultitargetTrackerMutex);
		mbGrabReady = true;
		pthread_mutex_unlock(&VisionMultitargetTrackerMutex);
	}

}

void* MultiTargetVisionTracker::_2ThreadPipeliningProcessThread(void *apdata)
{
	printf("Initiating processThread\n");
	MultiTargetVisionTracker* pc = (MultiTargetVisionTracker*)apdata;
	pc->_2ThreadPipeliningProcessLoop();
	printf("processingThread done\n");
	return NULL;
}

void MultiTargetVisionTracker::_2ThreadPipeliningProcessLoop(void)
{
	while(!mStopThreads)
	{
		// wait for grabTempFrame() to finish;
//		printf("mbGrabReady is : %d\n",mbGrabReady);
		while (!mbGrabReady){
//			printf("Waiting temp frame be updated\n");
			usleep(10);	// wait until grab is ready
		}
//		printf("Temp frame updated. Copying to frame buffer\n");
		mpVideoModule->setGrabFrameToCurrent();
//		printf("Copying to frame buffer done. Start processing\n");
		pthread_mutex_lock(&VisionMultitargetTrackerMutex);
		mbGrabReady = false;
		pthread_mutex_unlock(&VisionMultitargetTrackerMutex);

//		printf("mutexlock is the problem??\n");
		mpDetector->detectForeground();
		mpVisionTargetManager->updateTargets();
		mpDetector->detectTargets();
		mpVisionTargetManager->addNewTargetsFromDetector();
		nFrames++;
//		printf("Processing frame Done: nframes: %d\n",nFrames);

		// send iot heartbeat
		if (mpIOTCommunicator)
		{
			double curTime = gCurrentTime();
			if (curTime - mPreviousHeartbeatTime > mHeartBeatInterval)
			{
				mpIOTCommunicator->sendHeartbeat((unsigned int)(mHeartBeatInterval*mMaxHeartBeatIntervalProportion));
				mPreviousHeartbeatTime = curTime;
			}
		}

		// move complete trajectory to the completed trajectory list
		pthread_mutex_lock(&VisionMultitargetTrackerCompletedTrajectoryMutex);
		mpVisionTargetManager->retrieveCompletedTrajectories(mCompletedTrajectory);
		pthread_mutex_unlock(&VisionMultitargetTrackerCompletedTrajectoryMutex);
	}

}


// 3-thread pipelining
void MultiTargetVisionTracker::_3ThreadPipeliningStart()
{
    pthread_attr_init(&mThreadAttr);
    pthread_attr_setdetachstate(&mThreadAttr, PTHREAD_CREATE_JOINABLE);

    // allocate temp frames
    mpVideoModule->allocateTempFrames();

	mbGrabReady = false;
	mbBgSubReady = false;

	mStopThreads = false;
	mbMultithreadsOn = true;

	try {
		if (mpVideoModule->isValid())
		{
			// start frame grab thread
			if(pthread_create(&mGrabberThread, &mThreadAttr, _3ThreadPipeliningFrameGrabThread, (void *)this))
			{
		        VML_ERROR(MsgCode::runTimeErr, "Error in pthread_create(_3ThreadPipeliningFrameGrabThread)")

			} else {
		//        printf( " -- pthread_create(frameGrabThread) -- \n");
				// TODO: log message here
			}

			// start background subtraction thread
			if(pthread_create(&mBackSubThread, &mThreadAttr, _3ThreadPipeliningBackgroundSubtractThread, (void *)this))
			{
		        VML_ERROR(MsgCode::runTimeErr, "Error in pthread_create(_3ThreadPipeliningBackgroundSubtractThread)")

			} else {
		//        printf( " -- pthread_create(frameGrabThread) -- \n");
				// TODO: log message here
			}

			// start process thread
			if(pthread_create(&mProcessorThread, &mThreadAttr, _3ThreadPipeliningProcessThread, (void *)this))
			{
		        VML_ERROR(MsgCode::runTimeErr, "Error in pthread_create(_3ThreadPipeliningProcessThread)")

			} else {
		//        printf( " -- pthread_create(frameGrabThread) -- \n");
				// TODO: log message here
			}
		}
		else
		{
			// if video module is not valid, just run empty thread that only send heartbeat
			if(pthread_create(&mEmptyThread, &mThreadAttr, emptyProcessThread, (void *)this))
			{
		        VML_ERROR(MsgCode::runTimeErr, "Error in pthread_create(emptyProcessThread)")

			} else {
		//        printf( " -- pthread_create(frameGrabThread) -- \n");
			}
		}
	}
	catch (std::runtime_error &e)
	{
		mpIOTCommunicator->sendError(e.what(),true,true);
	}
}

void* MultiTargetVisionTracker::emptyProcessThread(void *apdata)
{
	MultiTargetVisionTracker* pc = (MultiTargetVisionTracker*)apdata;
	pc->emptyProcessLoop();
	return NULL;
}

void MultiTargetVisionTracker::emptyProcessLoop(void)
{
	try
	{
		while (!mStopThreads)
		{
			// send iot heartbeat
			this->sendHeartBeat();
			sleep(1);
		}
	}
	catch (std::runtime_error &e)
	{
		mpIOTCommunicator->sendError(e.what(),true,true);
	}

}

void* MultiTargetVisionTracker::_3ThreadPipeliningFrameGrabThread(void *apdata)
{
	printf("Initiating frameGrabThread\n");
	MultiTargetVisionTracker* pc = (MultiTargetVisionTracker*)apdata;
	pc->_3ThreadPipeliningFrameGrabLoop();
	printf("frameGrabThread Done\n");
	return NULL;
}

void MultiTargetVisionTracker::_3ThreadPipeliningFrameGrabLoop(void)
{
	try {
		while (!mStopThreads)
		{
			while (mbGrabReady){
	//			printf("Waiting temp frame to be cleared\n");
				usleep(10);	// wait until fetch is ready
			}

	//		printf("Temp frame cleared. Grabbing new temp frame\n");
			if (!mpVideoModule->grabInGrabFrame())
			{
				// if video frame grab is unavailable, stop tracker threads.
				std::cout << "Cannot grab video frame. Stopping Vision Tracker threads." << std::endl;
				mStopThreads = true;
			}

			pthread_mutex_lock(&VisionMultitargetTrackerMutex);
			mbGrabReady = true;
			pthread_mutex_unlock(&VisionMultitargetTrackerMutex);
		}
	}
	catch (std::runtime_error &e)
	{
		mpIOTCommunicator->sendError(e.what(),true,true);
	}

}

void* MultiTargetVisionTracker::_3ThreadPipeliningBackgroundSubtractThread(void *apdata)
{
	printf("Initiating backgroundSubtractThread\n");
	MultiTargetVisionTracker* pc = (MultiTargetVisionTracker*)apdata;
	pc->_3ThreadPipeliningBackgroundSubtractLoop();
	printf("backgroundSubtractThread Done\n");
	return NULL;
}

void MultiTargetVisionTracker::_3ThreadPipeliningBackgroundSubtractLoop(void)
{
	try {
		while (!mStopThreads)
		{
			while (mbBgSubReady || !mbGrabReady){
	//			printf("Waiting temp frame to be cleared\n");
				usleep(10);	// wait until fetch is ready
			}
	//		mpVideoModule->setGrabFrameToBackSubFrame();
			pthread_mutex_lock(&VisionMultitargetTrackerMutex);
			mpVideoModule->copyGrabFrameToBackSubFrame();
			mbGrabReady = false;
			pthread_mutex_unlock(&VisionMultitargetTrackerMutex);

	//		printf("Temp frame cleared. Grabbing new temp frame\n");
			mpVideoModule->backgroundSubtractionInBackSubFrame();

//#ifdef SHOW_TRACKER_DISPLAY
//			cv::Mat	dispImage;
//			cv::Mat grayImage;
//			mpVideoModule->getBackSubFrame()->foreground_image.copyTo(grayImage);
//			cv::cvtColor(grayImage,dispImage,CV_GRAY2BGR);
//			this->drawTrackingRegions(dispImage);
//			this->drawNoStartRegions(dispImage);
//			this->drawActiveTrajectories(dispImage);
//			this->drawActiveTargets(dispImage);
//			this->drawDetectedTargets(dispImage);
//			cv::cvtColor(dispImage,dispImage,CV_BGR2RGB);
//			cv::imshow("Tracker Display",dispImage);
//			cv::waitKey(10);
//#endif

			pthread_mutex_lock(&VisionMultitargetTrackerMutex);
			mbBgSubReady = true;
			pthread_mutex_unlock(&VisionMultitargetTrackerMutex);
		}
	}
	catch (std::runtime_error &e)
	{
		mpIOTCommunicator->sendError(e.what(),true,true);
	}

}
void* MultiTargetVisionTracker::_3ThreadPipeliningProcessThread(void *apdata)
{
	printf("Initiating processThread\n");
	MultiTargetVisionTracker* pc = (MultiTargetVisionTracker*)apdata;
	pc->_3ThreadPipeliningProcessLoop();
	printf("processingThread done\n");
	return NULL;
}

void MultiTargetVisionTracker::_3ThreadPipeliningProcessLoop(void)
{
	try {
		while(!mStopThreads)
		{
			// wait for grabTempFrame() to finish;
			while (!mbBgSubReady){
				usleep(10);	// wait until grab is ready
			}
			pthread_mutex_lock(&VisionMultitargetTrackerMutex);
			mpVideoModule->copyBackSubFrameToCurrent();
			mbBgSubReady = false;
			pthread_mutex_unlock(&VisionMultitargetTrackerMutex);

//#ifdef SHOW_TRACKER_DISPLAY
//			cv::Mat	dispImage;
//			cv::Mat grayImage;
//			mpVideoModule->currentFrame()->foreground_image.copyTo(grayImage);
//			cv::cvtColor(grayImage,dispImage,CV_GRAY2BGR);
//			this->drawTrackingRegions(dispImage);
//			this->drawNoStartRegions(dispImage);
//			this->drawActiveTrajectories(dispImage);
//			this->drawActiveTargets(dispImage);
//			this->drawDetectedTargets(dispImage);
//			cv::cvtColor(dispImage,dispImage,CV_BGR2RGB);
//			cv::imshow("Tracker Display",dispImage);
//			cv::waitKey(10);
//#endif

			//		printf("mutexlock is the problem??\n");
			mpDetector->detectForeground();
			mpVisionTargetManager->updateTargets();
			mpDetector->detectTargets();
			mpVisionTargetManager->addNewTargetsFromDetector();
			nFrames++;

			// send iot heartbeat
			this->sendHeartBeat();

			// return completed trajectory
			pthread_mutex_lock(&VisionMultitargetTrackerCompletedTrajectoryMutex);
			mpVisionTargetManager->retrieveCompletedTrajectories(mCompletedTrajectory);
			pthread_mutex_unlock(&VisionMultitargetTrackerCompletedTrajectoryMutex);

#ifdef SHOW_TRACKER_DISPLAY
			cv::Mat	dispImage;
			this->getGrabbedImage().copyTo(dispImage);
			this->drawTrackingRegions(dispImage);
			this->drawNoStartRegions(dispImage);
			this->drawActiveTrajectories(dispImage);
			this->drawActiveTargets(dispImage);
			this->drawDetectedTargets(dispImage);
			cv::cvtColor(dispImage,dispImage,CV_BGR2RGB);
			cv::imshow("Tracker Display",dispImage);
			cv::waitKey(10);
#endif

		}

	}
	catch (std::runtime_error &e)
	{
		mpIOTCommunicator->sendError(e.what(),true,true);
	}
}

void MultiTargetVisionTracker::stopThreads()
{
	mStopThreads = true;
	// block until all threads return
	pthread_join(mGrabberThread,NULL);
	pthread_join(mBackSubThread,NULL);
	pthread_join(mProcessorThread,NULL);
}
#endif

const cv::Mat&  MultiTargetVisionTracker::getGrabbedImage(void)
{
    return mpVideoModule->currentFrame()->image;
}

const cv::Mat&  MultiTargetVisionTracker::getForeground(void)
{
    return mpDetector->getForegroundImage();
}

const cv::Mat& MultiTargetVisionTracker::getBlobImage(void)
{
    return mpDetector->getBlobImage();
}

void MultiTargetVisionTracker::copyBackgroundImageTo(cv::Mat &bgimg) const
{
    mpDetector->copyBackgroundImageTo(bgimg);
}

const cv::Mat& MultiTargetVisionTracker::getBackProjection(void)
{
    return mpVisionTargetManager->getBackProjImage();
}

void MultiTargetVisionTracker::copyUndistortImageTo(cv::Mat &cur_undist)
{
	// for debugging, undistort the input image
	cur_undist.release();
	mpVideoModule->copyCurrentUndistortedImageTo(cur_undist);
}

unsigned int MultiTargetVisionTracker::getCurrentFrameNumber()
{
//	return VisionModuleManager::instance()->getVideoModule()->getCurrentFrameNumber();
	return nFrames;
}

unsigned int MultiTargetVisionTracker::getNumActiveTargets()
{
	return mpVisionTargetManager->getNumActiveTargets();
}

unsigned int MultiTargetVisionTracker::getNumTotalTargets()
{
	return mpVisionTargetManager->getNumTotalTargets();
}

unsigned int MultiTargetVisionTracker::getNumInactiveTargets()
{
	return mpVisionTargetManager->getNumInactiveTargets();
}

unsigned int MultiTargetVisionTracker::getNumCompletedTargets()
{
	return mpVisionTargetManager->getNumCompletedTargets();
}

#ifdef _WIN32
void MultiTargetVisionTracker::getCompletedTrajectories(std::vector<ait::vision::Trajectory> &trajlist)
{
	mpVisionTargetManager->retrieveCompletedTrajectories(trajlist);
}
void MultiTargetVisionTracker::finish(std::vector<ait::vision::Trajectory> &trajlist)
{
	// finish tracking and move all active and inactive tracks to completed tracks.
	mpVisionTargetManager->completeAllTargets();

	// output completed tracks
	mpVisionTargetManager->retrieveCompletedTrajectories(trajlist);
}

#else
void MultiTargetVisionTracker::getCompletedTrajectories(std::vector<VisionTrajectory> &trajlist)
{
	mpVisionTargetManager->retrieveCompletedTrajectories(trajlist);
}

void MultiTargetVisionTracker::MultithreadPipeliningGetCompletedTrajectories(std::vector<VisionTrajectory> &trajlist)
{
	pthread_mutex_lock(&VisionMultitargetTrackerCompletedTrajectoryMutex);
	trajlist.insert(trajlist.end(),mCompletedTrajectory.begin(),mCompletedTrajectory.end());
	mCompletedTrajectory.clear();
	pthread_mutex_unlock(&VisionMultitargetTrackerCompletedTrajectoryMutex);
}
#endif
// display methods

void MultiTargetVisionTracker::drawActiveTargets(cv::Mat &display_image)
{
	mpVisionTargetManager->drawActiveTargets(display_image);
}

void MultiTargetVisionTracker::drawActiveTrajectories(cv::Mat &display_image)
{
	mpVisionTargetManager->drawActiveTrajectories(display_image);
}
/*
void MultiTargetVisionTracker::drawUndistortedActiveTargets(cv::Mat &display_image)
{
	VisionModuleManager::instance()->getVisionTargetManager()->drawUndistortedActiveTargets(display_image);
}
*/

void MultiTargetVisionTracker::drawInactiveTargets(cv::Mat &display_image)
{
	mpVisionTargetManager->drawInactiveTargets(display_image);
}

void MultiTargetVisionTracker::drawDetectedTargets(cv::Mat &display_image)
{
	mpDetector->drawDetectedTargets(display_image);
}

void MultiTargetVisionTracker::drawTrackingRegions(cv::Mat &display_image)
{
	mpVideoModule->drawTrackRegions(display_image);
}

void MultiTargetVisionTracker::drawNoStartRegions(cv::Mat &display_image)
{
	mpVideoModule->drawNoStartRegions(display_image);
}


void MultiTargetVisionTracker::displayTracker(cv::Mat &display_image)
{
	mpVideoModule->currentFrame()->image.copyTo(display_image);
	mpVideoModule->drawTrackRegions(display_image);
	mpVisionTargetManager->drawActiveTrajectories(display_image);
	mpVisionTargetManager->drawActiveTargets(display_image);
	mpDetector->drawDetectedTargets(display_image);
	cv::cvtColor(display_image,display_image,CV_BGR2RGB);
}



void MultiTargetVisionTracker::setTrackingMethod(const TrackingMethods tm)
{
	BaseVisionTracker::setTrackingMethod(tm,VisionModuleManager::instance()->getSettings());
}

void MultiTargetVisionTracker::sendHeartBeat()
{
	// send iot heartbeat
	if (mpIOTCommunicator)
	{
		double curTime = gCurrentTime();
		if (curTime - mPreviousHeartbeatTime > mHeartBeatInterval)
		{
			mpIOTCommunicator->sendHeartbeat((unsigned int)(mHeartBeatInterval*mMaxHeartBeatIntervalProportion));
			mPreviousHeartbeatTime = curTime;
		}
	}
}

} // of namespace vml

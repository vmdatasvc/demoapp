/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * PersonShapeModel.cpp
 *
 *  Created on: Jun 22, 2015
 *      Author: yyoon
 */

#include <modules/vision/VisionVideoModule.hpp>
#include <modules/utility/Util.hpp>
#include <modules/utility/MathUtil.hpp>
#include <modules/vision/PersonShapeModel.hpp>

namespace vml {

PersonShapeModel::PersonShapeModel():
		mArea(0),
		mLength(0.f),
		mMajorAxisLength(0.f),
		mMinorAxisLength(0.f),
        mPersonRadius(0.f),
        mPersonHeight(0.f)
{
}

PersonShapeModel::~PersonShapeModel()
{
}

void PersonShapeModel::initialize(boost::shared_ptr<SETTINGS> settings,
								boost::shared_ptr<VisionVideoModule> video_module)
{
    mPersonRadius = settings->getFloat("Camera/personRadius",0.4f,false);
    mPersonHeight = settings->getFloat("Camera/personHeight",1.7f,false);
	mpSettings = settings;
	mpVideoModule = video_module;
	mpCameraModel = mpVideoModule->getCameraModel();
}

void PersonShapeModel::estimatePersonShape(int x, int y)
// NOTE: x,y is the blob center, so assume this is the body-mass-center of the cylinder shape
{
	estimatePersonShape(cv::Point(x,y));
}

void PersonShapeModel::estimatePersonShape(cv::Point center)
{
	mCenter = center;

	mpCameraModel->perspectiveCylinderProjection(center,mPersonRadius,mPersonHeight,mPersonShapeContour);

	// enforce the contour to be in the window
	mpVideoModule->enforceWindowRect(mPersonShapeContour);

	// get member data
	mHeadPosition = getHeadPosFromCenter(mCenter);
	mFootPosition = getFootPosFromCenter(mCenter);
	mBoundingRect = cv::boundingRect(mPersonShapeContour);
}

bool PersonShapeModel::updateShapeInfo()
{
	if (mPersonShapeContour.size() < 3)
	{
		return false;
	}

	// calculate pca
	if (!calcContourAxes(mPersonShapeContour,mMajorAxis,mMinorAxis,mMajorAxisLength,mMinorAxisLength))
	{
		return false;
	}

	// calculate Sinv
	cv::RotatedRect epEnclosure = cv::minAreaRect(mPersonShapeContour);
	mLength = std::max(epEnclosure.size.height,epEnclosure.size.width);
	// calculate inverse cov for mahalanobis distance
	cv::Moments	mm = cv::moments(mPersonShapeContour);

	double D = (mm.mu20*mm.mu02-mm.mu11*mm.mu11)/(mm.m00-1.f);

	mSinv[0] = mm.mu02/D;
	mSinv[1] = -mm.mu11/D;
	mSinv[2] = mm.mu20/D;

	mArea = abs((int)cv::contourArea(mPersonShapeContour));

	return true;
}

cv::Point PersonShapeModel::getFootPosFromCenter(cv::Point center) const
{
	return mpCameraModel->perspectiveCylinderAxisPointProjection(center,1.f,mPersonHeight);
}

cv::Point PersonShapeModel::getHeadPosFromCenter(cv::Point center) const
{
	return mpCameraModel->perspectiveCylinderAxisPointProjection(center,-1.f,mPersonHeight);
}

float PersonShapeModel::getFloorRadius() const
{
	std::vector<cv::Point> floor_circle;
	mpCameraModel->perspectiveCircleProjection(mFootPosition,mPersonRadius,floor_circle);
	double maxRadius = -1.f;
	for (unsigned int i=0;i<floor_circle.size();i++)
	{
		cv::Point dp = floor_circle[i] - mFootPosition;
		double radius = cv::norm(dp);
		if ( radius > maxRadius)
		{
			maxRadius = radius;
		}
	}

	return (float)maxRadius;

}

float PersonShapeModel::EuclideanDistance(PersonShapeModel &other) const
{
	return EuclideanDistance(other.mCenter);
}

float PersonShapeModel::EuclideanDistance(cv::Point other) const
{
	float dx = this->mCenter.x-other.x;
	float dy = this->mCenter.y-other.y;
	return (sqrtf(dx*dx+dy*dy));
}

float PersonShapeModel::MahalanobisDistance(PersonShapeModel &other) const
{
	return MahalanobisDistance(other.mCenter);
}

float PersonShapeModel::MahalanobisDistance(cv::Point other) const
{
	// 1. Use current target's Sinv only. This will make the distance non-commutative
	// 2. Use sum of Sinv
	//return ((mSinv[0]+ot.mSinv[0])*dx*dx + 2.f*(mSinv[1]+ot.mSinv[1])*dx*dy + (mSinv[2]+ot.mSinv[2])*dy*dy);
	// 3. Use min distance
	float dx = this->mCenter.x-other.x;
	float dy = this->mCenter.y-other.y;
	return (sqrtf(mSinv[0]*dx*dx+2.f*mSinv[1]*dx*dy+mSinv[2]*dy*dy));
}

void PersonShapeModel::draw(cv::Mat &image,cv::Scalar color) const
{
	if (mPersonShapeContour.size()>0)
	{
		cv::drawContours(image,
				std::vector<std::vector<cv::Point> >(1,mPersonShapeContour),-1,color);
	}
}

void PersonShapeModel::drawOccupancyMap(cv::Mat &hmap) const
{
	if (mPersonShapeContour.size()>0)
	{
		cv::drawContours(hmap,std::vector<std::vector<cv::Point> >(1,mPersonShapeContour),-1,cv::Scalar(255),CV_FILLED);
	}
}

void PersonShapeModel::drawOccupancyMapCropped(cv::Mat &hmap,cv::Rect crop_rect) const
{
	if (mPersonShapeContour.size()>0)
	{
		// crop estimated person shape
		std::vector<cv::Point>  croppedEstimatedPersonShape;
		for (unsigned int i=0;i<mPersonShapeContour.size();i++)
		{
			croppedEstimatedPersonShape.push_back(mPersonShapeContour[i] - crop_rect.tl());
		}
		cv::drawContours(hmap,std::vector<std::vector<cv::Point> >(1,croppedEstimatedPersonShape),-1,cv::Scalar(255),CV_FILLED);
	}
}

void PersonShapeModel::estimateOccupancyMap(cv::Mat &hmap, cv::Point temp_center) const
{
	std::vector<cv::Point> temp_contour;
	mpCameraModel->perspectiveCylinderProjection(temp_center,mPersonRadius,mPersonHeight,temp_contour);

	if (temp_contour.size()>0)
	{
		cv::drawContours(hmap,std::vector<std::vector<cv::Point> >(1,temp_contour),-1,cv::Scalar(255),CV_FILLED);
	}
}

void PersonShapeModel::estimateOccupancyMapCropped(cv::Mat &hmap,cv::Point temp_center, cv::Rect crop_rect) const
{
	std::vector<cv::Point> temp_contour;
	mpCameraModel->perspectiveCylinderProjection(temp_center,mPersonRadius,mPersonHeight,temp_contour);

	if (temp_contour.size()>0)
	{
		// crop estimated person shape
		std::vector<cv::Point>  croppedEstimatedPersonShape;
		std::vector<cv::Point>::iterator titr = temp_contour.begin();
		while (titr!=temp_contour.end())
		{
			croppedEstimatedPersonShape.push_back(*titr - crop_rect.tl());
			titr++;
		}
		cv::drawContours(hmap,std::vector<std::vector<cv::Point> >(1,croppedEstimatedPersonShape),-1,cv::Scalar(255),CV_FILLED);
	}
}


/*
void PersonShapeModel::estimateUndistortedPersonShapeConvexHull(int x, int y, std::vector<cv::Point> &cvxhull)
{
        // clear person shape
        cvxhull.clear();
        // Goal is to construct convex hull that's enclosing the given x,y coordinate as the foot location of a tracking subject
        // 1. Backproject x,y coord to world space
        // undistort the x,y
        // backproject to find the (x,y,z) coords in world
        float u = x;	// output is already normalized
        float v = y;

        // this is the top of the person
//	float Z = mCameraHeight-mPersonHeight;
        float Z = mCameraHeight;
        float X = Z*u;
        float Y = Z*v;

#if 0
        // cv::projectPoints crashes. don't know why. Taking alternative way

        // 2. construct human subject enclosure
        // use cylinder mesh
        std::vector<cv::Point3f>	cylinder3D;
        float	theta = 0.f;
        for (int i=0;i<(int)(2*M_PI/cCircleInc);i++,theta+=cCircleInc)
        {
                cv::Point3f tp;
                tp.x = X+mPersonRadius*cos(theta);
                tp.y = Y+mPersonRadius*sin(theta);
                tp.z = Z;
                cylinder3D.push_back(tp);
                tp.z = Z-mPersonHeight;
                cylinder3D.push_back(tp);
        }

        // 3. project enclosure to image space
        std::vector<cv::Point2f> cylinder2D(cylinder3D.size());
        cv::projectPoints(cylinder3D,cylinder2D,cv::Vec<float,3>(0,0,0),cv::Vec<float,3>(0,0,0),mK,mD);
#endif

        cvxhull.clear();
        float theta = 0.f;
        for (int i=0;i<(int)(2*M_PI/mCircleInc);i++,theta+=mCircleInc)
        {
                cv::Point tp;
//		float z = Z+mPersonHeight;
                float z = Z;
                float xprime = (X+mPersonRadius*cos(theta))/z;
                float yprime = (Y+mPersonRadius*sin(theta))/z;

                tp.x = (int)(fx*xprime+cx);
                tp.y = (int)(fx*yprime+cy);

                cvxhull.push_back(tp);

                z = Z;

                xprime = (X+mPersonRadius*cos(theta))/z;
                yprime = (Y+mPersonRadius*sin(theta))/z;
                r = xprime*xprime+yprime*yprime;
                r2 = r*r;
                r4 = r2*r2;
                r6 = r2*r4;

                dist_factor = (1.f+k1*r2+k2*r4+k3*r6);
                xdprime = xprime*dist_factor;
                ydprime = yprime*dist_factor;

                tp.x = (int)(fx*xdprime+cx);
                tp.y = (int)(fx*ydprime+cy);
                cvxhull.push_back(tp);
        }

        // convert projected enclosure to convex hull
        cv::convexHull(cvxhull,cvxhull);
}
*/

} // of namespace vml

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * VisionTrajectory.cpp
 *
 *  Created on: Jan 24, 2015
 *      Author: yyoon
 */

#include <modules/utility/DateTime.hpp>
#include <modules/vision/VisionTrajectory.hpp>
#include <modules/vision/VisionModuleManager.hpp>
#include <modules/vision/VisionVideoModule.hpp>
#include <modules/vision/VisionSiteSetup.hpp>
#include <modules/utility/Util.hpp>
#include <modules/utility/MathUtil.hpp>
#include <core/Error.hpp>

// for debugging
#include <modules/vision/VisionPostProcessing.hpp>

#include <tinyxml2.h>
#include <limits>
#include <math.h>

namespace vml {

float VisionTrajectory::TrajectoryPoint::dist(const VisionTrajectory::TrajectoryPoint other) const
{
	int dx=loc.x-other.loc.x;
	int dy=loc.y-other.loc.y;
	return sqrt((float)(dx*dx)+(float)(dy*dy));
}

cv::Point2f VisionTrajectory::TrajectoryPoint::undistort() const
{
	return VisionModuleManager::instance()->getVideoModule()->getCameraModel()->undistortPoint(loc);
}

VisionTrajectory::TrajectoryPoint VisionTrajectory::TrajectoryPoint::interpolate(VisionTrajectory::TrajectoryPoint b,
		VisionTrajectory::TrajectoryPoint e, double time)
{
	// no checking of time in between [b,e] time window.
	// in case when time is out of [b,e] it will extrapolate.

	VisionTrajectory::TrajectoryPoint	interpolated;
	double		time_fraction = (time-b.time)/(e.time - b.time);
	interpolated.time = time;
	interpolated.loc = b.loc + (e.loc - b.loc)*time_fraction;

	return interpolated;
}


/* class VisionTrajectory */
void VisionTrajectory::addTrajectoryPoint(cv::Point loc, double t)
{
	TrajectoryPoint tp;
	tp.loc = cv::Point2f(loc);
	tp.time = t;
	mTrajectoryPts.push_back(tp);
}

#ifdef _WIN32
void VisionTrajectory::convertToAITTrajectory(ait::vision::Trajectory &ait_traj)
{
	if (mTrajectoryPts.size() == 0)
	{
		return;
	}

	std::vector<TrajectoryPoint>::const_iterator titr;
	for (titr=mTrajectoryPts.begin();titr!=mTrajectoryPts.end();titr++)
	{
		ait_traj.add(ait::vision::Trajectory::NodePtr(new ait::vision::Trajectory::Node(titr->time,ait::Vector3f(titr->loc.x,titr->loc.y))));
	}
}
#endif

double VisionTrajectory::getStartTime(void) const
{
	if (!mTrajectoryPts.empty())
	{
		return mTrajectoryPts.front().time;
	}
	else
	{
		return -1.f;	// flag invalid time
	}
}

double VisionTrajectory::getEndTime(void) const
{
	if (!mTrajectoryPts.empty())
	{
		return mTrajectoryPts.back().time;
	}
	else
	{
		return -1.f;	// flag invalid time
	}
}

void VisionTrajectory::append(VisionTrajectory &tj)
{
	mTrajectoryPts.insert(mTrajectoryPts.end(),tj.mTrajectoryPts.begin(),tj.mTrajectoryPts.end());
}

void VisionTrajectory::prepend(VisionTrajectory &tj)
{
	std::vector<TrajectoryPoint> temp = mTrajectoryPts;
	mTrajectoryPts = tj.mTrajectoryPts;
	mTrajectoryPts.insert(mTrajectoryPts.end(),temp.begin(),temp.end());
}

float VisionTrajectory::length() const
{
	// returns the accumulated distance between each nodes.

	if (nodeCount() < 2)
	{
		return 0.f;
	}

	float	acc_dist = 0.f;
	for (unsigned int i=1;i<mTrajectoryPts.size();i++)
	{
		acc_dist += mTrajectoryPts[i].dist(mTrajectoryPts[i-1]);
	}

	return acc_dist;
}

double VisionTrajectory::getDuration() const
{
 	// returns the duration of the trajector in sec.
	if (mTrajectoryPts.size() < 2)
	{
		return 0.f;
	}

	return mTrajectoryPts.back().time - mTrajectoryPts.front().time;
}

void PostProcessingVisionTrajectory::undistort(boost::shared_ptr<CameraModelOpenCV> p_cam_model)
{
	// convert
}

void PostProcessingVisionTrajectory::convertToWorld(boost::shared_ptr<VisionCameraCalibration> p_calibration)
{
	// convert trajectory points to cv::Point2f
	std::vector<cv::Point2f>	trajectory_points;
	std::vector<cv::Point2f>	world_points;
	for (auto tit = mTrajectoryPts.begin();tit!=mTrajectoryPts.end();tit++)
	{
		trajectory_points.push_back(tit->loc);
	}

	p_calibration->projectToWorld(trajectory_points,world_points);

	VML_ASSERT(world_points.size() == mTrajectoryPts.size());

	// back to trajectory points
	auto pit=world_points.begin();
	for (auto tit = mTrajectoryPts.begin();tit!=mTrajectoryPts.end();tit++,pit++)
	{
		tit->loc = *pit;
	}
}

void PostProcessingVisionTrajectory::applyHomography(cv::Mat homography)
{
	for (auto tit = mTrajectoryPts.begin();tit!=mTrajectoryPts.end();tit++)
	{
		cv::Mat_<double>	hc(3,1);	// convert loc to homogeneous coords
		hc.at<double>(0,0) = tit->loc.x;
		hc.at<double>(1,0) = tit->loc.y;
		hc.at<double>(2,0) = 1.f;
		hc = homography*hc;
		tit->loc.x = (float)hc.at<double>(0,0);
		tit->loc.y = (float)hc.at<double>(1,0);
	}
}

// distance metrics for trajectory filtering/joining
float PostProcessingVisionTrajectory::minDistance(PostProcessingVisionTrajectory& other, bool &in)
{

	// calculate this to other
	float	this_2_other_dst = 0.f;
	// calculate mutual distance table in the process to be used for later
	std::matrix<float>	mutual_distance(this->nodeCount(),other.nodeCount());
	for (unsigned int i=0;i<this->nodeCount();i++)
	{
		float	min_point_dist = std::numeric_limits<float>::max();
		float	min_lineseg_dist = std::numeric_limits<float>::max();
		for (unsigned int j=0;j<other.nodeCount();j++)
		{
			mutual_distance(i,j) = this->mTrajectoryPts[i].dist(other.mTrajectoryPts[j]);
			if (mutual_distance(i,j) < min_point_dist)
			{
				min_point_dist = mutual_distance(i,j);
			}

			// calculate line segment distance for two adjacent line segments to j
			// determine which line segment has acute angle
			if(j<other.nodeCount()-1)
			{
				float temp_dist = lineSegmentDistance<float>(this->mTrajectoryPts[i].loc,other.mTrajectoryPts[j].loc,other.mTrajectoryPts[j+1].loc);
				if ((!(temp_dist<0.f))&&(temp_dist<min_lineseg_dist))
				{
					// this is in acute angle. calculate line segment distance
					min_lineseg_dist = temp_dist;
//					float td = lineSegmentDistance(this->mTrajectoryPts[i].loc,other.mTrajectoryPts[j].loc,other.mTrajectoryPts[j+1].loc);
		//			std::cout << "temp_dist :" << temp_dist << std::endl;
				}
			}
		}

		float min_dist = std::min(min_point_dist,min_lineseg_dist);
//		std::cout << "p_dist: " << min_point_dist << ", l dist: " << min_lineseg_dist << ", min_dist :" << min_dist << std::endl;
//		cv::Mat	temp_disp;
//		__getScreenShotImage().copyTo(temp_disp);
//		this->draw(temp_disp,CV_RGB(255,0,0));
//		other.draw(temp_disp,CV_RGB(0,0,255));
//		cv::circle(temp_disp,this->mTrajectoryPts[i].loc,1,CV_RGB(0,255,0),2);
//		cv::imshow(SCREENSHOTWINDOWNAME,temp_disp);
//		cv::waitKey(0);


		// take min of them and add it to this_2_other_dst;
//		if (min_dist <= 0.f)
//		{
//			std::cout << "break here";
//		}
		assert(!(min_dist < 0.f));
		this_2_other_dst += min_dist;
	}
	this_2_other_dst /= (float)(this->nodeCount());
//	std::cout << "this 2 other :" << this_2_other_dst << std::endl;

	// calculate other to this
	float	other_2_this_dst = 0.f;
	for (unsigned int j=0;j<other.nodeCount();j++)
	{
		float	min_point_dist = std::numeric_limits<float>::max();
		float	min_lineseg_dist = std::numeric_limits<float>::max();
		for (unsigned int i=0;i<this->nodeCount();i++)
		{
			if (mutual_distance(i,j) < min_point_dist)
			{
				min_point_dist = mutual_distance(i,j);
			}

			// calculate line segment distance for two adjacent line segments to j
			// determine which line segment has acute angle
			if(i<this->nodeCount()-1)
			{
				float temp_dist = lineSegmentDistance(other.mTrajectoryPts[j].loc,this->mTrajectoryPts[i].loc,this->mTrajectoryPts[i+1].loc);
				if ((!(temp_dist<0.f))&&(temp_dist<min_lineseg_dist))
				{
					// this is in acute angle. calculate line segment distance
					min_lineseg_dist = temp_dist;
		//			std::cout << "temp_dist :" << temp_dist << std::endl;
				}
			}
		}

/*
		cv::Mat	temp_disp;
		__getScreenShotImage().copyTo(temp_disp);
		this->draw(temp_disp,CV_RGB(255,0,0));
		other.draw(temp_disp,CV_RGB(0,0,255));
		// show min dist point
		cv::line(temp_disp,this->mTrajectoryPts[min_idx].loc,other.mTrajectoryPts[j].loc,CV_RGB(0,255,0));
		cv::imshow(SCREENSHOTWINDOWNAME,temp_disp);
		cv::waitKey(0);
*/
		float min_dist = std::min(min_point_dist,min_lineseg_dist);

		// take min of them and add it to this_2_other_dst;
//		if (min_dist <= 0.f)
//		{
//			std::cout << "break here";
//		}
		assert(!(min_dist < 0.f));
		other_2_this_dst += min_dist;
	}

	other_2_this_dst /= (float)(other.nodeCount());
//	std::cout << "other 2 thisr :" << other_2_this_dst << std::endl;

	// take min and return
	if (this_2_other_dst < other_2_this_dst)
	{
		in = true;
		return this_2_other_dst;
	}
	else
	{
		in = false;
		return other_2_this_dst;
	}
}

float PostProcessingVisionTrajectory::minDistanceWithTimeDomain(PostProcessingVisionTrajectory& other)
{
	return 0.f;

}

float PostProcessingVisionTrajectory::endPointDistance(PostProcessingVisionTrajectory& other)
{
	return 0.f;

}

float PostProcessingVisionTrajectory::endPointDistanceWithTimeDomain(PostProcessingVisionTrajectory& other)
{
	return 0.f;

}

float PostProcessingVisionTrajectory::distToFront(TrajectoryPoint other)
{
	return mTrajectoryPts.front().dist(other);
}

float PostProcessingVisionTrajectory::distToBack(TrajectoryPoint other)
{
	return mTrajectoryPts.back().dist(other);
}

bool PostProcessingVisionTrajectory::joinTrajectoryByOverlap(boost::shared_ptr<PostProcessingVisionTrajectory> other,
		boost::shared_ptr<FurnitureLines> p_furniture)
{
	// assert both trajectories are valid
	assert(this->mValid & other->mValid);

	// make sure other's start time is later than this.
	// NOTE: this condition should be enforced by caller of this method.
	assert(this->getStartTime() < other->getStartTime());

	// Determine if other is enclosed in this. If so, this trajectory pair is not to be joined
	// because this trajectory pair happened concurrently within a same time frame,
	// so probably not transitioning between multiple cameras.
	if (other->getEndTime() < this->getEndTime())
	{
		return false;
	}

	// check if the pair (this,other) has overlap
	double max_start_time = std::max(this->getStartTime(),other->getStartTime());
	double min_end_time = std::min(this->getEndTime(),other->getEndTime());
	if ( min_end_time > max_start_time )
	{
		// this pair has overlap.

		// We don't want to join inclusive trajectory. For example if one trajectory of the pair has
		// exceedingly large portion of overlaps, then it is likely a trajectory of the same camera set with
		// a merged trajectory.  Since the goal of this merge is to join inter-camera trajectories, avoid
		// joining this kind of pair.
		// check if overlapping time for either this or other does not exceed 80% of the either of the duration.
		double overlap_duration = min_end_time - max_start_time;
		if (std::max(overlap_duration/this->getDuration(),overlap_duration/other->getDuration()) > 0.8)
		{
			return false;
		}

		// construct overlapping points
		std::vector<TrajectoryPoint> tr1_overlap;
		std::vector<TrajectoryPoint> tr2_overlap;
		std::vector<float>			 dist_vector;


		// find time overlapping nodes for this
		std::vector<TrajectoryPoint>::iterator tr1_itr = this->mTrajectoryPts.begin();
		while (tr1_itr->time < max_start_time && tr1_itr < this->mTrajectoryPts.end())
		{
			tr1_itr++;
		}

		assert(tr1_itr != mTrajectoryPts.end());	// make sure tr1_itr doesn't exceed end of this trajectory

		// find time overlapping nodes for other
		std::vector<TrajectoryPoint>::iterator tr2_itr = other->mTrajectoryPts.begin();
		while (tr2_itr->time < max_start_time && tr2_itr < other->mTrajectoryPts.end())
		{
			tr2_itr++;
		}

		// check if line segment that connects time-overlapping portion intersect with furniture lines.
		if ((p_furniture)&&(p_furniture->doesIntersect(tr1_itr->loc,tr2_itr->loc)))
		{
			return false;
		}

		// find corresponding nodes (interpolate by timestamp if necessary)
		while (std::min(tr1_itr->time, tr2_itr->time)< min_end_time)
		{
			if (tr1_itr->time < tr2_itr->time)
			{
				// interpolate tr2 point in tr1's time stamp
				std::vector<TrajectoryPoint>::iterator prev_tr2_itr = tr2_itr-1;
				assert(prev_tr2_itr >= other->mTrajectoryPts.begin());
				TrajectoryPoint	tr2_interp = VisionTrajectory::TrajectoryPoint::interpolate(*prev_tr2_itr,*tr2_itr,tr1_itr->time);

				// calculate and insert distance
				dist_vector.push_back(tr1_itr->dist(tr2_interp));
				// insert interpolated point
				tr1_overlap.push_back(*tr1_itr);
				tr2_overlap.push_back(tr2_interp);

				tr1_itr++;
			}
			else if (tr2_itr->time < tr1_itr->time)
			{
				// interpolate tr1 point in tr2's time and move tr2
				std::vector<TrajectoryPoint>::iterator prev_tr1_itr = tr1_itr-1;
				assert(prev_tr1_itr >= this->mTrajectoryPts.begin());
				TrajectoryPoint	tr1_interp = VisionTrajectory::TrajectoryPoint::interpolate(*prev_tr1_itr,*tr1_itr,tr2_itr->time);

				// insert interpolated point
				dist_vector.push_back(tr2_itr->dist(tr1_interp));
				tr1_overlap.push_back(tr1_interp);
				tr2_overlap.push_back(*tr2_itr);
				tr2_itr++;
			}
			else
			{
				// means tr1_itr->time == tr2_itr->time. this will be a rare case for double time,
				// but may not be so rare when we sync the time stamps to have 1/5 (5 fps) sec precision.
				// in this case, no interpolation is necessary. just copy both points and move on

				// no need to interpolate. just copy tr1 and tr2 points and move both
				dist_vector.push_back(tr1_itr->dist(*tr2_itr));
				tr1_overlap.push_back(*tr1_itr);
				tr1_itr++;
				tr2_overlap.push_back(*tr2_itr);
				tr2_itr++;
			}
		} // of while

		assert(tr1_overlap.size() == tr2_overlap.size());
		if (tr1_overlap.size() < 2)
		{
			// if overlap point size is less than 3, too little overlap to consider
			return false;
		}

		// calculate distance between corresponding nodes
		unsigned int	count = 0;

		// calculate the number of overlapping
		for (std::vector<float>::const_iterator itr = dist_vector.begin();itr!=dist_vector.end();itr++)
		{
			// if point distance is less than threshold (35 pixels), count this point as close
			// TODO: This threshold is dependent to floorplan image resolution. Make it metric unit in world coord.
			if (*itr < 35.f)
			{
				count++;
			}
		}

		if ((double)count/(double)dist_vector.size() > 0.5f)
		{
			// this should be merged.
//			std::cout << "Min dists: " << this_min_dist << ", " << other_min_dist << std::endl;

//			if (min_dist < 15.f)
//			{
//				return false;
//			}

			// for display
			cv::Mat	temp_disp;
			if (VisionPostProcDisplay::isDisplayOn())
			{
				// show trajectories before merge
				VisionPostProcDisplay::getFloorplanImage().copyTo(temp_disp);
				this->draw(temp_disp,CV_RGB(255,0,0));
				other->draw(temp_disp,CV_RGB(0,0,255));
				VisionPostProcDisplay::showDispImage(temp_disp,FLOORPLANWINDOWNAME);
			}

			// show overlaps
//			__getFloorplanImage().copyTo(temp_disp);
//			for (unsigned int i=0;i<tr1_overlap.size();i++)
//			{
//				cv::circle(temp_disp,tr1_overlap[i].loc,2,CV_RGB(255,0,0),2);
//				cv::circle(temp_disp,tr2_overlap[i].loc,2,CV_RGB(0,0,255),2);
//			}
//			__showDispImage(temp_disp,FLOORPLANWINDOWNAME);


			// merge other to this
			std::vector<VisionTrajectory::TrajectoryPoint>	mergedPoints;

			// this has earlier start time, so start putting points from this until min start time
			std::vector<TrajectoryPoint>::iterator tr_itr = this->mTrajectoryPts.begin();
			while (tr_itr->time < max_start_time && tr_itr < this->mTrajectoryPts.end())
			{
				mergedPoints.push_back(*tr_itr);
				tr_itr++;
			}

//			__getFloorplanImage().copyTo(temp_disp);
//			unsigned int i=0;
//			for (;i<mergedPoints.size();i++)
//			{
//				cv::circle(temp_disp,mergedPoints[i].loc,2,CV_RGB(255,0,0),2);
//			}
//			__showDispImage(temp_disp,FLOORPLANWINDOWNAME);

			// now put overlapped portion of points
			// slowly transition from tr1 point to tr2 point
			// TODO: make time epsilon a controllable parameter
			double time_epsilon = 1e-2; // .01 sec

			for (unsigned int i=0;i<tr1_overlap.size();i++)
			{
				// if time interval from the last point in the merged point with the current point is too small, skip adding this point
				if (fabs(tr1_overlap[i].time - mergedPoints.back().time) < time_epsilon)
				{
					continue;
				}

				VisionTrajectory::TrajectoryPoint temp_point;
				temp_point.time = tr1_overlap[i].time;
				float	tr2_frac = (float)i/(float)(tr2_overlap.size()-1);
				temp_point.loc = (1.f-tr2_frac)*tr1_overlap[i].loc + tr2_frac*tr2_overlap[i].loc;

				mergedPoints.push_back(temp_point);
			}

//			for (;i<mergedPoints.size();i++)
//			{
//				cv::circle(temp_disp,mergedPoints[i].loc,2,CV_RGB(255,0,255),2);
//			}
//			__showDispImage(temp_disp,FLOORPLANWINDOWNAME);

			// now put the remainder of other points
			tr_itr = other->mTrajectoryPts.begin();
			while (tr_itr->time < min_end_time && tr_itr != other->mTrajectoryPts.end())
			{
				tr_itr++;
			}
			//
			while (tr_itr != other->mTrajectoryPts.end())
			{
				mergedPoints.push_back(*tr_itr);
				tr_itr++;
			}

//			for (;i<mergedPoints.size();i++)
//			{
//				cv::circle(temp_disp,mergedPoints[i].loc,2,CV_RGB(0,0,255),2);
//			}
//			__showDispImage(temp_disp,FLOORPLANWINDOWNAME);

			// replace this points with merged points
			this->mTrajectoryPts.clear();
			this->mTrajectoryPts = mergedPoints;

			// other has been merged to this, so invalidate other
			other->setInvalid();

			if (VisionPostProcDisplay::isDisplayOn())
			{
				// show trajectory after merge
				VisionPostProcDisplay::getFloorplanImage().copyTo(temp_disp);
				this->draw(temp_disp,CV_RGB(0,255,0));
				VisionPostProcDisplay::showDispImage(temp_disp,FLOORPLANWINDOWNAME);
			}

			return true;
		}
		else
		{
			// this pair shouldn't be merged
			return false;
		}

	}
	else
	{
		// this pair shouldn't be merged
		return false;
	}

}

bool PostProcessingVisionTrajectory::joinTrajectoryByRect(boost::shared_ptr<PostProcessingVisionTrajectory> other,
		float dist_thresh,
		float time_thresh,
		boost::shared_ptr<FurnitureLines> p_f)
{
	// implementing JR from POSSE
	return false;
}

// Adaptation from VMS Trajectory::toXmlString()
std::string VisionTrajectory::toXmlString(unsigned int decimals) const
{
	assert (mValid);

	const double startTime = getStartTime();
	std::stringstream outstr;
	outstr << "<trajectory id='" << mID << "' start_time='" << DateTime(startTime).toString() << "'>";
//	 std::string out = "<trajectory id='" + aitSprintf("%i",getId()) + "' start_time='" +
//	   DateTime(getStartTime()).toString() + "'>";
	std::vector<TrajectoryPoint>::const_iterator iNode;
	bool first = true;
	for (iNode = mTrajectoryPts.begin(); iNode != mTrajectoryPts.end(); iNode++)
	{
		if (first)
		{
			first = false;
		}
		else
		{
			outstr << ",";
		}
		outstr << std::fixed << std::setprecision(3) << iNode->time - startTime << std::setprecision(decimals) << "," << iNode->loc.x << "," << iNode->loc.y;
	 }
	 outstr << "</trajectory>";
	 return outstr.str();
}

std::string VisionTrajectory::toCSVString(unsigned int decimals) const
{
	assert (mValid);

	const double startTime = getStartTime();
	std::stringstream outstr;
	outstr << DateTime(startTime).toString() << "," << std::fixed << std::setprecision(3) << this->getDuration() << std::setprecision(decimals) << "\"";
	outstr << "<trajectory id='" << mID << "' start_time='" << DateTime(startTime).toString() << "'>";
//	 std::string out = "<trajectory id='" + aitSprintf("%i",getId()) + "' start_time='" +
//	   DateTime(getStartTime()).toString() + "'>";
	std::vector<TrajectoryPoint>::const_iterator iNode;
	bool first = true;
	for (iNode = mTrajectoryPts.begin(); iNode != mTrajectoryPts.end(); iNode++)
	{
		if (first)
		{
			first = false;
		}
		else
		{
			outstr << ",";
		}
		outstr << std::fixed << std::setprecision(3) << iNode->time - startTime << std::setprecision(decimals) << "," << iNode->loc.x << "," << iNode->loc.y;
	 }
	 outstr << "</trajectory>\"";
	 return outstr.str();
}

void PostProcessingVisionTrajectory::fromCSVString(std::string csvstr_)
{
	// if the end of string is ^M (carriage return from windows file), remove it
	std::string csvstr = csvstr_;
	if (csvstr.back() == '\15')	// if it is carriage return
	{
		csvstr.pop_back();
	}
	// delimiter is ','
	// find the first delim
	std::size_t starttime_pos = csvstr.find(',');
	std::string starttime_str = csvstr.substr(0,starttime_pos);
	std::size_t duration_pos = csvstr.find(',',starttime_pos+1);
	std::string duration_str = csvstr.substr(starttime_pos+1,duration_pos-(starttime_pos+1));
	// truncate leading and ending "
	std::string xml_str = csvstr.substr(duration_pos+2,csvstr.length()-(duration_pos+3));

	// convert
	DateTime startTime = DateTime::fromString(starttime_str);
//	double duration = atof(duration_str.c_str());	// duration can be calculated from last-start time

	// NOTE: currently frame rate is set to be 5 fps. Hence, each time between frames is set to be 0.2 sec
	//       sync all timestamps to have sub-decimal precision 1 so that trajectory joining could be easy.
	// TODO: make this optional.
	double startTimeSec = startTime.toSeconds();
	// convert to 1 sub-decimal precision
	startTimeSec = (round(startTimeSec*10.f))/10.f;

	this->fromXmlString(xml_str, startTime.toSeconds());

}

void PostProcessingVisionTrajectory::fromXmlString(std::string xmlstr, double start_time)
{
	// parse xml string
	tinyxml2::XMLDocument	xdoc;
	if (xdoc.Parse(xmlstr.c_str()) != tinyxml2::XML_SUCCESS)
	{
		// error.
		VML_ERROR(MsgCode::IOErr,"Could not parse xml string: " << xmlstr)
	}

	const tinyxml2::XMLElement *trajectoryRoot = xdoc.FirstChildElement("trajectory");

	// get trajectory id
	unsigned int	id;
	trajectoryRoot->QueryUnsignedAttribute("id",&id);
	this->mID = id;

	// start time is in the csv header, so usually not needed to parse start_time here.
//	const char* start_time_str = trajectoryRoot->Attribute("start_time");


	// add points
	const char*	pointstr = trajectoryRoot->GetText();
	// presumably pointstr should be a null-terminated string. So, parse
	char point_buffer[100];
	const char *sp = pointstr;
	char *dp = point_buffer;;
	while (*sp != '\0')
	{
		if (*sp == ';')
		{
			// met the delimeter. move source pointer
			sp++;
			// terminate point buffer
			*dp = '\0';
			// convert point buffer to a point
			TrajectoryPoint	tp;
			char num_buffer[20];
			char *np = num_buffer;
			dp = point_buffer;
			while (*dp != ',')
			{
				*np++ = *dp++;
			}
			dp++;
			*np = '\0';
			tp.time = round(atof(num_buffer)*10.f)/10.f + start_time;
			np = num_buffer;
			while (*dp != ',')
			{
				*np++ = *dp++;
			}
			dp++;
			*np = '\0';
			tp.loc.x = atof(num_buffer);
			np = num_buffer;
			while (*dp != '\0')
			{
				*np ++ = *dp++;
			}
			// reset dp to the beginning of point buffer

			dp = point_buffer;
			*np = '\0';
			tp.loc.y = atof(num_buffer);

			this->mTrajectoryPts.push_back(tp);
		}
		else
		{
			// copy char
			*dp++ = *sp++;
		}
	}

	// set this trajectory valid
	mValid = true;
}

void VisionTrajectory::draw(cv::Mat &disp_image, cv::Scalar line_color) const
{
	if (mTrajectoryPts.size()< 2)
	{
		return;
	}

	// draw green dot at the beginning, red dot at the end
	cv::circle(disp_image,mTrajectoryPts.front().loc,3,CV_RGB(0,255,0),2);
	cv::circle(disp_image,mTrajectoryPts.back().loc,3,CV_RGB(255,0,0),2);
	for (unsigned int i=1;i<mTrajectoryPts.size();i++)
	{
		cv::line(disp_image,mTrajectoryPts[i].loc,mTrajectoryPts[i-1].loc,line_color);
	}
}

std::ostream& operator<<(std::ostream& os, const VisionTrajectory &obj)
{
	for (unsigned int i=0;i<obj.mTrajectoryPts.size();i++)
	{
		os << "(" << obj.mTrajectoryPts[i].loc.x << "," << obj.mTrajectoryPts[i].loc.y << "), ";
	}
	return os;
}


} // of namespace vml

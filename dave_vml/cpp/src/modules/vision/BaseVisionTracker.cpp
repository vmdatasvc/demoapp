/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * BaseVisionTracker.cpp
 *
 *  Created on: Jan 24, 2015
 *      Author: yyoon
 */

#include <core/Image.hpp>
#include <modules/vision/VisionVideoModule.hpp>
#include <modules/vision/BaseVisionTracker.hpp>
#include <modules/vision/VisionDetectorTarget.hpp>
#include <modules/vision/VisionDetector.hpp>
#include <modules/utility/MathUtil.hpp>
#include <modules/vision/VisionModuleManager.hpp>


#ifdef DEBUG
// for debugging
#include "core/Util.hpp"
#endif

namespace vml {

#ifdef VISIONTRACKERS_DEBUG_SHOW
const char* BaseVisionTracker::displayname = "BaseVisionTrackerDebugDisplay";
cv::Mat BaseVisionTracker::mDisplayImage;
#endif

BaseVisionTracker::BaseVisionTracker(const int id,boost::shared_ptr<SETTINGS> settings):
		mID(id),
		rows(VisionModuleManager::instance()->getVideoModule()->getRows()),
		cols(VisionModuleManager::instance()->getVideoModule()->getCols()),
		mSearchWindowRatio(settings->getFloat("BaseVisionTracker/searchWindowRatio",2.f,false)),
		mRegionStatus(TrackingStatus_InStart),
		mTrackStatus(TrackingStatus_Initialized),
		mKF(cv::KalmanFilter(4,2,0)),
		mReactivated(false),
		mInactiveFrameCount(0),
		mArea(0),
		mConfidence(0.f),
		mcMergeLikelihoodThreshold(0.f),
		mcMatchLikelihoodThreshold(settings->getFloat("BaseVisionTracker/matchLikelihoodThreshold",0.4f,false)),
		mcInactiveTimeout((int)(settings->getDouble("BaseVisionTracker/inactiveTimeout",2.f,false)*5.f)),	// assuming 5 fps
		mcReactivateLikelihoodThreshold(settings->getFloat("BaseVisionTracker/reactivateLikelihoodThreshold",0.3,false)),
		mDisplayCurrentPosition(settings->getBool("BaseVisionTracker/displayCurrentPosition [bool]",true,false))
{
	mpVideoModule = VisionModuleManager::instance()->getVideoModule();
	mpVisionDetector = VisionModuleManager::instance()->getDetector();

	// TODO: Kalman filter uncertainty should be proportional to the estimated human size.
	// make initial and process noise the radius of expected human shape circle
	float	personRadiusPixel = mpVideoModule->getPersonRadiusPixel();
	mKFInitialStateUncertaintyLoc = personRadiusPixel*settings->getFloat("BaseVisionTracker/KalmanFilterInitialStateUncertaintyLoc",2.f,false);
	// need to square these cause these are covariance
	mKFInitialStateUncertaintyLoc *= mKFInitialStateUncertaintyLoc;
	mKFInitialStateUncertaintyVel = personRadiusPixel*settings->getFloat("BaseVisionTracker/KalmanFilterInitialStateUncertaintyVel",1.f,false);
	// need to square these cause these are covariance
	mKFInitialStateUncertaintyVel *= mKFInitialStateUncertaintyVel;

	mKFProcessNoiseLoc = personRadiusPixel*settings->getFloat("BaseVisionTracker/KalmanFilterProcessNoiseLoc",2.f,false);
	mKFProcessNoiseLoc *= mKFProcessNoiseLoc;

	mKFProcessNoiseVel = personRadiusPixel*settings->getFloat("BaseVisionTracker/KalmanFilterProcessNoiseVel",1.f,false);
	mKFProcessNoiseVel *= mKFProcessNoiseVel;

	mKFMeasurementNoise = personRadiusPixel*settings->getFloat("BaseVisionTracker/KalmanFilterMeasurementNoise",1.f,false);
	mKFMeasurementNoise *= mKFMeasurementNoise;

	mPersonShape.initialize(settings,mpVideoModule);

#ifdef VISIONTRACKERS_DEBUG_SHOW
	// set up debug display
	if (BaseVisionTracker::mDisplayImage.empty())
	{
		BaseVisionTracker::mDisplayImage = cv::Mat(VisionModuleManager::instance()->getVideoModule()->getRows(),VisionModuleManager::instance()->getVideoModule()->getCols(),CV_8UC3);
		cv::imshow(displayname,BaseVisionTracker::mDisplayImage);
	}
#endif

}

void BaseVisionTracker::initialize(boost::shared_ptr<VisionDetectorTarget> dt)
{
	mCenter = dt->getCenter();	//
	mPersonShape.estimatePersonShape(mCenter);
	mPosition = mPersonShape.getFootPosition();

	// initialize Kalman Filter
	cv::setIdentity(mKF.transitionMatrix);
	mKF.transitionMatrix.at<float>(0,2) = 1;
	mKF.transitionMatrix.at<float>(1,3) = 1;

	mKFMeasurement = cv::Mat_<float>(2,1);
	mKF.statePre.at<float>(0) = (float)mCenter.x;
	mKF.statePre.at<float>(1) = (float)mCenter.y;
	mKF.statePre.at<float>(2) = 0.f;
	mKF.statePre.at<float>(3) = 0.f;
	cv::setIdentity(mKF.measurementMatrix);
	cv::setIdentity(mKF.measurementNoiseCov, cv::Scalar::all(mKFMeasurementNoise));
	mKF.processNoiseCov = 0;
	mKF.processNoiseCov.at<float>(0,0) = mKF.processNoiseCov.at<float>(1,1) = mKFProcessNoiseLoc;
	mKF.processNoiseCov.at<float>(2,2) = mKF.processNoiseCov.at<float>(3,3) = mKFProcessNoiseVel;
	mKF.errorCovPost = 0;
	mKF.errorCovPost.at<float>(0,0) = mKF.errorCovPost.at<float>(1,1) = mKFInitialStateUncertaintyLoc;
	mKF.errorCovPost.at<float>(2,2) = mKF.errorCovPost.at<float>(3,3) = mKFInitialStateUncertaintyVel;
	mKF.errorCovPre = 0;
	mKF.errorCovPre.at<float>(0,0) = mKF.errorCovPre.at<float>(1,1) = mKFInitialStateUncertaintyLoc;
	mKF.errorCovPre.at<float>(2,2) = mKF.errorCovPre.at<float>(3,3) = mKFInitialStateUncertaintyVel;

	// calculate location uncertainty.
	// predict Kalman filter and update location uncertainty
	mKFPrediction = mCenter;

	mLocationUncertaintyInvKF[0] = 1.f/mKFInitialStateUncertaintyLoc;
	mLocationUncertaintyInvKF[2] = 1.f/mKFInitialStateUncertaintyLoc;
	mLocationUncertaintyInvKF[1] = 0.f;


	if (mpVideoModule->insideTrackRegion(mPosition))
	{
		mRegionStatus = TrackingStatus_InTrack;
	}
	else if (mpVideoModule->insideStartKillRegion(mPosition))
	{
		mRegionStatus = TrackingStatus_InStart;
	}
	else
	{
		mRegionStatus = TrackingStatus_Invalid;
	}

	// just use the bounding box + enlarged rectangle for search window
	// update crop rect
	cv::Rect tBoundingRect = mPersonShape.getBoundingRect();
	mCropRect.width = (int)(tBoundingRect.width*mSearchWindowRatio);
	mCropRect.height = (int)(tBoundingRect.height*mSearchWindowRatio);
	mCropRect.x = tBoundingRect.x + (int)(0.5*(1.f-mSearchWindowRatio)*(float)tBoundingRect.width);
	mCropRect.y = tBoundingRect.y + (int)(0.5*(1.f-mSearchWindowRatio)*(float)tBoundingRect.height);
	// make sure crop rect is inside image
	mpVideoModule->enforceWindowRect(mCropRect);

	// call subclass initialization
	this->initializeTrackerFeature(dt);
}

TrackingStatusEnum BaseVisionTracker::reactivate(std::vector<boost::shared_ptr<VisionDetectorTarget> > &dt_list)
{
	// recalculate positional uncertainty
	calcLocationUncertaintyKF();

	// Find best matching detected target for reactivation
	bool target_found = false;
	boost::shared_ptr<VisionDetectorTarget> maxLikelihood_dt;
	float	maxLikelihood = -1.f;

	// for each detected target calculate location likelihood
	// check if dt is valid
	std::vector<boost::shared_ptr<VisionDetectorTarget> >::iterator dt_itr = dt_list.begin();
	while(dt_itr != dt_list.end())
	{
		if ((*dt_itr)->isValid())
		{
			// find the detected target with maximum location likelihood
			// TODO: currently just use location likelihood, but research to use visual similarity later
			float location_likelihood = calculateLocationLikelihoodKF((*dt_itr)->getPosition());
			if (location_likelihood > maxLikelihood)
			{
				maxLikelihood_dt = (*dt_itr);
				maxLikelihood = location_likelihood;
			}
		}
		dt_itr++;
	}

	// if the location likelihood of the max target is larger than threshold, target is found
	// invalidate found target
	if (maxLikelihood > mcReactivateLikelihoodThreshold)
	{
		target_found = true;
	}
	else
	{
		target_found = false;
	}

	// if target found
	if (target_found)
	{
		maxLikelihood_dt->setInvalid();

		// reinitialize base tracker
		TrackingStatusEnum prevRegionStatus = mRegionStatus;

		// update Kalman filter from previous state
		// update the base features only when update was successful
		prevPosition = mCenter;

		cv::Point	newCenter = maxLikelihood_dt->getPosition();
		// newCenter is an observation. Update Kalman filter
		mKFMeasurement.at<float>(0) = (float)newCenter.x;
		mKFMeasurement.at<float>(1) = (float)newCenter.y;
		// update Kalman filter
		mKF.correct(mKFMeasurement);
		mKF.predict();

		// calculate location uncertainty.
		// predict using Kalman filter and update location uncertainty
		mKFPrediction.x = (int)mKF.statePre.at<float>(0);
		mKFPrediction.y = (int)mKF.statePre.at<float>(1);

		calcLocationUncertaintyKF();

		mCenter.x = (int)mKF.statePost.at<float>(0);	//
		mCenter.y = (int)mKF.statePost.at<float>(1);

		// update position
		mPersonShape.estimatePersonShape(mCenter);
		mPosition = mPersonShape.getFootPosition();

		// update crop rect
		cv::Rect tBoundingRect = mPersonShape.getBoundingRect();
		mCropRect.width = (int)(tBoundingRect.width*mSearchWindowRatio);
		mCropRect.height = (int)(tBoundingRect.height*mSearchWindowRatio);
		mCropRect.x = tBoundingRect.x + (int)(0.5*(1.f-mSearchWindowRatio)*(float)tBoundingRect.width);
		mCropRect.y = tBoundingRect.y + (int)(0.5*(1.f-mSearchWindowRatio)*(float)tBoundingRect.height);

		// make sure crop rect is inside image
		mpVideoModule->enforceWindowRect(mCropRect);

		if (mpVideoModule->insideTrackRegion(mPosition))
		{
			mRegionStatus = TrackingStatus_InTrack;
		}
		else if (mpVideoModule->insideStartKillRegion(mPosition))
		{
			mRegionStatus = TrackingStatus_InStart;
		}
		else
		{
			mRegionStatus = TrackingStatus_Invalid;
		}

		if (prevRegionStatus == TrackingStatus_InStart)
		{
			switch (mRegionStatus)
			{
				case TrackingStatus_InStart:
				case TrackingStatus_InTrack:
				{
					// keep tracking status active
					mTrackStatus = TrackingStatus_Active;
					break;
				}
				case TrackingStatus_Invalid:
				default:
				{
					// target entered invalid region. Kill the track?
					mTrackStatus = TrackingStatus_Finished;
					break;
				}

			}
		}
		else if (prevRegionStatus == TrackingStatus_InTrack)
		{
			switch (mRegionStatus)
			{
				case TrackingStatus_InTrack:
				{
					mTrackStatus = TrackingStatus_Active;
					break;
				}
				case TrackingStatus_InStart:
				case TrackingStatus_Invalid:
				default:
				{
					mTrackStatus = TrackingStatus_Finished;
				}

			}
		}

		if (mTrackStatus == TrackingStatus_Active)
		{
			// initialize tracker feature
			this->initializeTrackerFeature(maxLikelihood_dt);
			// target was reactivated;
			mInactiveFrameCount = 0;
		}

		return mTrackStatus;
	} // of if (target_found)
	else
	{
		// reactivation target not found.
		// increase inactive count
		mInactiveFrameCount++;

		// if inactive count is larger than threshold, finish this target
		if (mInactiveFrameCount > mcInactiveTimeout)
		{
			return TrackingStatus_Finished;
		}
		else
		{
			return TrackingStatus_Inactive;
		}
	}


}

TrackingStatusEnum BaseVisionTracker::update(cv::Mat *disp_map)
{

	// update tracker feature and get next track_box

	// update new position
	cv::Point newCenter;
	bool update_success = this->updateTrackerFeature(disp_map,newCenter);

	// update base tracker
	TrackingStatusEnum prevRegionStatus = mRegionStatus;

	if (update_success)
	{
		// update the base features only when update was successful
		prevPosition = mCenter;

		// update Kalman filter
		updateKalmanFilter(newCenter, update_success);

		mCenter.x = (int)mKF.statePost.at<float>(0);	//
		mCenter.y = (int)mKF.statePost.at<float>(1);
		// update person shape and position
		mPersonShape.estimatePersonShape(mCenter);
		mPosition = mPersonShape.getFootPosition();

		if (mpVideoModule->insideTrackRegion(mCenter))
		{
			mRegionStatus = TrackingStatus_InTrack;
		}
		else if (mpVideoModule->insideStartKillRegion(mCenter))
		{
			mRegionStatus = TrackingStatus_InStart;
		}
		else
		{
			mRegionStatus = TrackingStatus_Invalid;
		}

		// update crop rect
		cv::Rect tBoundingRect = mPersonShape.getBoundingRect();
		mCropRect.width = (int)(tBoundingRect.width*mSearchWindowRatio);
		mCropRect.height = (int)(tBoundingRect.height*mSearchWindowRatio);
		mCropRect.x = tBoundingRect.x + (int)(0.5*(1.f-mSearchWindowRatio)*(float)tBoundingRect.width);
		mCropRect.y = tBoundingRect.y + (int)(0.5*(1.f-mSearchWindowRatio)*(float)tBoundingRect.height);
		// make sure crop rect is inside image
		mpVideoModule->enforceWindowRect(mCropRect);

		if (prevRegionStatus == TrackingStatus_InStart)
		{
			switch (mRegionStatus)
			{
				case TrackingStatus_InStart:
				case TrackingStatus_InTrack:
				{
					// keep tracking status active
					mTrackStatus = TrackingStatus_Active;
					break;
				}
				case TrackingStatus_Invalid:
				default:
				{
					// target entered invalid region. Kill the track?
					mTrackStatus = TrackingStatus_Finished;
					break;
				}

			}
		}
		else if (prevRegionStatus == TrackingStatus_InTrack)
		{
			switch (mRegionStatus)
			{
				case TrackingStatus_InTrack:
				case TrackingStatus_InStart:	// if target enters start region from track region, keep tracking until it becomes invalid.
				{
					mTrackStatus = TrackingStatus_Active;
					break;
				}
				case TrackingStatus_Invalid:
				default:
				{
					mTrackStatus = TrackingStatus_Finished;
				}

			}
		}
	}
	else	// of if (update_success)
	{
		cv::Point dummy;
		// update was unsuccessful, increase Kalman filter positional uncertainty by passing dummy point, and false
		updateKalmanFilter(dummy,update_success);

		// check if the target was in start/kill region.
		if (prevRegionStatus == TrackingStatus_InStart)
		{
			// if the update was unsuccessful, but if it was previously in start/kill region, then kill it here
			mTrackStatus = TrackingStatus_Finished;
		}
		else if (prevRegionStatus == TrackingStatus_InTrack)
		{
			// if the target was in track region,
			switch (mRegionStatus)
			{
				case TrackingStatus_InTrack:
				{
					// update() is called only for active targets. So, if this becomes inactive,
					// this is probably the first time it became invalid. So, set inactive frame count as 1.
					mInactiveFrameCount = 1;
					// if update is not successful, then region status doesn't change
					mTrackStatus = TrackingStatus_Inactive;
					break;
				}
				case TrackingStatus_InStart:
					// if target becomes inactive in start/kill region, then finish it.
				case TrackingStatus_Invalid:
				default:
				{
					mTrackStatus = TrackingStatus_Finished;
				}
			} // of swith (mRegionStatus)
		}

	}

#ifdef DEBUG
	mCount++;
#endif

	return mTrackStatus;
}

void BaseVisionTracker::updateKalmanFilter(cv::Point newCenter, bool success)
{
	if (success)
	{
#ifdef VISIONTRACKERS_DEBUG_SHOW
		float	P_pre[4][4];
		float	P_post[4][4];
		float	x_pre[4];
		float	x_post[4];

		for (unsigned int i=0;i<4;i++)
			for (unsigned int j=0;j<4;j++)
			{
				P_pre[i][j] = mKF.errorCovPre.at<float>(i,j);
				P_post[i][j] = mKF.errorCovPost.at<float>(i,j);
			}

		for (unsigned int i=0;i<4;i++)
		{
			x_pre[i] = mKF.statePre.at<float>(i);
			x_post[i] = mKF.statePost.at<float>(i);
		}

		// TODO: insert Kalman filter uncertainty output to disk here
#endif

		// newCenter is an observation. Update Kalman filter
		mKFMeasurement.at<float>(0) = (float)newCenter.x;
		mKFMeasurement.at<float>(1) = (float)newCenter.y;
		mKF.correct(mKFMeasurement);

#ifdef VISIONTRACKERS_DEBUG_SHOW
		for (unsigned int i=0;i<4;i++)
			for (unsigned int j=0;j<4;j++)
			{
				P_pre[i][j] = mKF.errorCovPre.at<float>(i,j);
				P_post[i][j] = mKF.errorCovPost.at<float>(i,j);
			}

		for (unsigned int i=0;i<4;i++)
		{
			x_pre[i] = mKF.statePre.at<float>(i);
			x_post[i] = mKF.statePost.at<float>(i);
		}
#endif

		// predict using Kalman filter and update location uncertainty
		mKF.predict();

#ifdef VISIONTRACKERS_DEBUG_SHOW

		for (unsigned int i=0;i<4;i++)
			for (unsigned int j=0;j<4;j++)
			{
				P_pre[i][j] = mKF.errorCovPre.at<float>(i,j);
				P_post[i][j] = mKF.errorCovPost.at<float>(i,j);
			}

		for (unsigned int i=0;i<4;i++)
		{
			x_pre[i] = mKF.statePre.at<float>(i);
			x_post[i] = mKF.statePost.at<float>(i);
		}
#endif

		mKFPrediction.x = (int)mKF.statePre.at<float>(0);
		mKFPrediction.y = (int)mKF.statePre.at<float>(1);
		calcLocationUncertaintyKF();
	}
	else
	{
		// just increase process noise and predict
		mKF.predict();
		mKFPrediction.x = (int)mKF.statePre.at<float>(0);
		mKFPrediction.y = (int)mKF.statePre.at<float>(1);
		calcLocationUncertaintyKF();
	}
}

TrackingMethods BaseVisionTracker::readTrackingMethod(boost::shared_ptr<SETTINGS> settings)
{
	std::string trackingMethodStr = settings->getString("BaseVisionTracker/trackingMethod","TrackingMethod_Hybrid",false);

	if (trackingMethodStr == "TrackingMethod_HIST_HSV")
	{
		return TrackingMethod_HIST_HSV;
	}
	else if (trackingMethodStr == "TrackingMethod_HIST_RGB")
	{
		return TrackingMethod_HIST_RGB;
	}
#if 0
	else if (trackingMethodStr == "TrackingMethod_DLIB_Correlation_Tracking")
	{
		return TrackingMethod_DLIB_Correlation_Tracking;
	}
#endif
	else if (trackingMethodStr == "TrackingMethod_KDE")
	{
		return TrackingMethod_KDE;
	}
	else if (trackingMethodStr == "TrackingMethod_HybridHist")
	{
		return TrackingMethod_HybridHist;
	}
	else
	{
		VML_WARN(MsgCode::notImplementedErr,"Tracking method " << trackingMethodStr << " not implemented. Using default (hybrid)");
		return TrackingMethod_HybridHist;
	}
}

void BaseVisionTracker::setTrackingMethod(const TrackingMethods tm, boost::shared_ptr<SETTINGS> settings)
{
	switch (tm)
	{
		case TrackingMethod_HIST_HSV:
		{
			settings->setString("BaseVisionTracker/trackingMethod","TrackingMethod_HIST_HSV");
			break;
		}
		case TrackingMethod_HIST_RGB:
		{
			settings->setString("BaseVisionTracker/trackingMethod","TrackingMethod_HIST_RGB");
			break;
		}
		case TrackingMethod_KDE:
		{
			settings->setString("BaseVisionTracker/trackingMethod","TrackingMethod_KDE");
			break;
		}
		case TrackingMethod_HybridHist:
		{
			settings->setString("BaseVisionTracker/trackingMethod","TrackingMethod_HybridHist");
			break;
		}
#if 0	// DLIB tracker no longer supported
		case TrackingMethod_DLIB_Correlation_Tracking:
		{
			settings->setString("BaseVisionTracker/trackingMethod","TrackingMethod_DLIB_Correlation_Tracking");
			break;
		} // of case Correlation_Tracking:
#endif
		default:
		{
			VML_ERROR(MsgCode::notSupportedErr,"BaseVisionTracker::setTrackingMethod: Tracking method " << tm << " not supported");
		}
	}
}

void BaseVisionTracker::getHumanShapeMap(cv::Mat &hmap)
{
	hmap = cv::Scalar(0); // initialize
	mPersonShape.drawOccupancyMap(hmap);
}

void BaseVisionTracker::getCroppedHumanShapeMap(cv::Mat &hmap)
{
	hmap = cv::Scalar(0);
	mPersonShape.drawOccupancyMapCropped(hmap,mCropRect);
}

bool BaseVisionTracker::isInSearchArea(cv::Point position)
{
	cv::Point2f fpos(position);

	return mCropRect.contains(position);
}



void BaseVisionTracker::calcLocationUncertaintyKF()
{
	// testing. calculate location uncertainty inverse from Kalman filter
	float	p_pre[3];
	p_pre[0] = mKF.errorCovPre.at<float>(0,0);
	p_pre[1] = mKF.errorCovPre.at<float>(0,1);
	p_pre[2] = mKF.errorCovPre.at<float>(1,1);
	float D_ = p_pre[0]*p_pre[2]+p_pre[1]*p_pre[1];
	mLocationUncertaintyInvKF[0] = p_pre[2]/D_;
	mLocationUncertaintyInvKF[2] = p_pre[0]/D_;
	mLocationUncertaintyInvKF[1] = -p_pre[1]/D_;
}

float BaseVisionTracker::calculateLocationLikelihoodKF(cv::Point pos)
{
	cv::Point	np = mKFPrediction;

	float dx = (float)(pos.x-np.x);
	float dy = (float)(pos.y-np.y);

	return (exp(-(mLocationUncertaintyInvKF[0]*dx*dx+2.f*mLocationUncertaintyInvKF[1]*dx*dy+mLocationUncertaintyInvKF[2]*dy*dy)));
}

float BaseVisionTracker::calculateLocationLikelihoodKF(int x, int y)
{
	cv::Point	np = mKFPrediction;

	float dx = (float)(x-np.x);
	float dy = (float)(y-np.y);

	return (exp(-(mLocationUncertaintyInvKF[0]*dx*dx+2.f*mLocationUncertaintyInvKF[1]*dx*dy+mLocationUncertaintyInvKF[2]*dy*dy)));
}

boost::shared_ptr<VisionDetectorTarget> BaseVisionTracker::mergeAndFindCandidateBlob(cv::Mat likelihood_map)
{
	boost::shared_ptr<VisionDetectorTarget>	best_candidate;

	//*** find the list of close-by blobs
	std::vector<boost::shared_ptr<VisionDetectorTarget> >	candidate_blob_list;
	const std::vector<boost::shared_ptr<VisionDetectorTarget> > *detected_list = \
			VisionModuleManager::instance()->getDetector()->getDetectedTargetList();

	std::vector<boost::shared_ptr<VisionDetectorTarget> >::const_iterator ditr = detected_list->begin();
	for (;ditr!=detected_list->end();ditr++)
	{
		if ((*ditr)->isValid() && this->isInSearchArea((*ditr)->getPosition()))
		{
			// create pointer in the candidate list
			candidate_blob_list.push_back(*ditr);
			// evaluate back projection score (mean of backprojection map value)
			(*ditr)->evaluateLikelihood(mCropRect,likelihood_map);
		}
	}

	// now merge candidates in the blob list and move the position of current target
	// check if enough candidate blobs are in the list.
	if (candidate_blob_list.size() > 0)
	{
		// if enough candidate blobs, merge and update
		// sort candidate blob list with back projection score
		std::sort(candidate_blob_list.begin(),candidate_blob_list.end(),VisionDetectorTargetLikelihoodCompare());
		// for each blob, check others and merge
		for (unsigned int i=0;i<candidate_blob_list.size();i++)
		{
			boost::shared_ptr<VisionDetectorTarget> ti = candidate_blob_list[i];
			// check if mean backprojection score is above the threshold
			if ((ti->isValid()) && (ti->getLikelihood() > mcMatchLikelihoodThreshold))
			{
				for (unsigned int j=i+1;j<candidate_blob_list.size();j++)
				{
					boost::shared_ptr<VisionDetectorTarget> tj = candidate_blob_list[j];
					// check if i and j should be merged
					if ((tj->isValid()) && (tj->getLikelihood() > mcMatchLikelihoodThreshold))
					{
						ti->checkBlobMergeCriteriaAndMerge(tj);
					}

				}
			}
		}

		// find the blob that has the maximum back projection score
		// enforce motion model: select the blob that has the maximum backprojection and maximum location likelihood.
		// calculate distribution of next position gaussian model
		float max_score = -1.f;
		for (unsigned int i=0;i<candidate_blob_list.size();i++)
		{
			if (candidate_blob_list[i]->isValid())
			{
				float likelihood_score = candidate_blob_list[i]->getLikelihood();
//				float location_likelihood = calculateLocationLikelihood(candidate_blob_list[i]->getPosition());
//				likelihood_score *= location_likelihood;
				if ((likelihood_score > mcMatchLikelihoodThreshold)&&(likelihood_score > max_score))
				{
					best_candidate = candidate_blob_list[i];
					max_score = best_candidate->getLikelihood();
				}
			}
		}

		// NOTE: question here is that do we need to unmerge those merged but not used for update?
		//       probably not, cause the merge was done with size/distance criteria, which detector
		//       uses for merge, plus the likelihood criterion.
		//       So, it should be safe to assume those already merged are the subset of merged set
		//       in the detector.


	} // of if (candidate_blob_list.size() > 0)

	return best_candidate;
}


void BaseVisionTracker::drawTrackerHelper(cv::Mat &disp_image, cv::Scalar trackerColor)
{
	// draw personshape convexhull
	mPersonShape.draw(disp_image,trackerColor);
	// mark the center position
//		cv::circle(disp_image,mCenter,2,CV_RGB(0,255,0),2);
	// mark the foot position
	cv::circle(disp_image,mPosition,2,cv::Scalar(255,0,0));
	// draw target center coords
//	std::ostringstream coord_str;
//	cv::Point coord_pos = mpTracker->getPosition();
//	coord_str << "(" << coord_pos.x << "," << coord_pos.y << ")";
//	coord_pos.x = trackBox.x;
//	coord_pos.y = trackBox.y + trackBox.height + 10;
//	cv::putText(disp_image, coord_str.str(), coord_pos, CV_FONT_HERSHEY_PLAIN, 0.7f, cv::Scalar(255,0,0));

	// draw target id
	std::ostringstream id_str;
	id_str << mID;
	cv::putText(disp_image, id_str.str(), mPosition, CV_FONT_HERSHEY_PLAIN, 0.7f, cv::Scalar(255,255,0));
}

#ifdef VISIONTRACKERS_DEBUG_SHOW
void BaseVisionTracker::debugShow(cv::Mat &dispimage)
{
	if (dispimage.type() == CV_8UC3)
	{
		dispimage.copyTo(BaseVisionTracker::mDisplayImage);
	}
	else if (dispimage.type() == CV_8UC1)
	{
		cv::cvtColor(dispimage,BaseVisionTracker::mDisplayImage,CV_GRAY2RGB);
	}
	cv::imshow(displayname,BaseVisionTracker::mDisplayImage);
	cv::waitKey(20);
}
#endif

} // of namespace vml

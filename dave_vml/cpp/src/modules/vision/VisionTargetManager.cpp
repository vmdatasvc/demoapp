/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * VisionTargetManager.cpp
 *
 *  Created on: Jan 24, 2015
 *      Author: yyoon
 */

#include <opencv2/video/tracking.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <modules/vision/VisionVideoModule.hpp>
#include <modules/vision/VisionTargetManager.hpp>
#include <core/Error.hpp>
#include <modules/vision/VisionTrackerTarget.hpp>
#include <modules/vision/BaseVisionTracker.hpp>
#include <modules/vision/VisionModuleManager.hpp>
#include <modules/vision/VisionDetector.hpp>
#include <modules/vision/VisionDetectorTarget.hpp>
#include <modules/utility/TimeKeeper.hpp>
#include <modules/utility/DateTime.hpp>
// for debugging
#include <modules/utility/Util.hpp>

//#define DEBUG

namespace vml {

#ifdef VISIONMANAGER_DEBUG_SHOW
const char* VisionTargetManager::displayname = "VisionManagerDebug";
cv::Mat VisionTargetManager::mDisplayImage;
#endif

VisionTargetManager::VisionTargetManager():
		mLastID(0),
		mnActiveTargets(0),
		mnInactiveTargets(0),
		mTrackingMethod(TrackingMethod_HybridHist),
		mLastTimeCheck(-1.f),
		mInactiveMapWindowRadius(0),
		mVerifyTarget(true),
		mTrajectoryLengthThreshold(0.f),
		mTrajectoryDurationThreshold(0.f),
		mNewTargetMahalanobisDistanceThreshold(0.f),
		mNewTrajectoryID(0)
{
}

void VisionTargetManager::initialize(boost::shared_ptr<SETTINGS> settings)
{
	mTrajectoryDurationThreshold = settings->getDouble("VisionTargetManager/trajectoryDurationThreshold",4.f,false);
	mVerifyTarget = settings->getBool("VisionTargetManager/verifyTarget [bool]");
	mTrajectoryLengthThreshold = VisionModuleManager::instance()->getVideoModule()->getPersonRadiusPixel() *
			settings->getFloat("VisionTargetManager/trajectoryLengthThreshold",20.f,false);
	mNewTargetMahalanobisDistanceThreshold = settings->getFloat("VisionTargetManager/newTargetMahalanobisDistanceThreshold",2.f,false);

	mpSettings = settings;

	boost::shared_ptr<VisionVideoModule> video_mod = VisionModuleManager::instance()->getVideoModule();

	mBackProjImage = cv::Mat::zeros(video_mod->getRows(),video_mod->getCols(), CV_8UC1);
	mInactiveMapWindowRadius = video_mod->getDiagonal()*mpSettings->getFloat("VisionTargetManager/inactiveMapWindowRadiusProportion",0.1f,false);

	// initialize new trajectory id
	// find new trajectory epoch time
	DateTime trajectoryEpochDateTime;
	DateTime::Struct tstruct = trajectoryEpochDateTime.getStruct();
	double timeSinceEpoch = (double)(((tstruct.weekday*24+tstruct.hour)*60+tstruct.minute)*60)+tstruct.second;
	// set new trajectory initial id as the count of tenths of second from the epoch time
	mNewTrajectoryID = (unsigned int)(floor(timeSinceEpoch*10.f));


#ifdef VISIONMANAGER_DEBUG_SHOW
	VisionTargetManager::mDisplayImage = cv::Mat(VisionModuleManager::instance()->getVideoModule()->getRows(),VisionModuleManager::instance()->getVideoModule()->getCols(),CV_8UC3);
	// create display window
	cv::namedWindow(VisionTargetManager::displayname);
#endif
}

void VisionTargetManager::updateTargets(void)
{
	// initialize target activity map
//	mTargetActivityMap = 0;

#ifdef VISIONTRACKERS_DEBUG_SHOW
	// reset debug display
	BaseVisionTracker::mDisplayImage = 0;
#endif

	// initialize back projection image
	mBackProjImage = cv::Scalar(0);

	// update the current active targets
	// check if there are active targets
	if (mnActiveTargets>0)
	{
		std::map<unsigned int,boost::shared_ptr<VisionTrackerTarget> >::iterator active_target_itr = mTargetMap.begin();
		while (active_target_itr != mTargetMap.end())
		{
			if (active_target_itr->second->mActive)
			{
				boost::shared_ptr<VisionTrackerTarget> at_ptr = active_target_itr->second;
				TrackingStatusEnum update_status = at_ptr->updateTargetStatus(mBackProjImage);

				switch (update_status)
				{
					case TrackingStatus_Finished:
					{
						// move the track to the completed list and delete this target.
						if (at_ptr->mTrajectory.nodeCount() > 1)
						{
							mCompletedTargetList.push_back(at_ptr);
						}

						mnActiveTargets--;
						// remove current element from mTargetMap
						// NOTE: in c++11 map.erase() returns the next availabel iterator, but the current gcc doesn't support that
						// when upgrading gcc, visit here and update this.
#ifdef _WIN32
						active_target_itr = mTargetMap.erase(active_target_itr);
#else
						mTargetMap.erase(active_target_itr++);
#endif
						break;
					}
					case TrackingStatus_Inactive:
					{
						// set inactive. Try to reacquire the target later.
						mnActiveTargets--;
						mnInactiveTargets++;
						active_target_itr++;
						break;
					}
					case TrackingStatus_Active:
					default:
					{
						// nothing to do
						active_target_itr++;
					}
				} // of switch (update_status)
			} // of if (active_target_itr->mActive)
			else
			{
				active_target_itr++;
			}
		} // of while (not active_target_itr)
	}

#ifdef VISIONMANAGER_DEBUG_SHOW
	VisionModuleManager::instance()->getVideoModule()->currentFrame()->image.copyTo(mDisplayImage);
	VisionModuleManager::instance()->getVideoModule()->drawTrackRegions(mDisplayImage);
	drawActiveTrajectories(mDisplayImage);
	drawActiveTargets(mDisplayImage);
	cv::cvtColor(mDisplayImage,mDisplayImage,CV_BGR2RGB);
	debugShow();
#endif
}

void VisionTargetManager::addNewTargetsFromDetector()
{
	std::vector<boost::shared_ptr<VisionDetectorTarget> >  detected_target_list;
	VisionModuleManager::instance()->getDetector()->retrieveDetectedTargets(detected_target_list);

//	if (detected_target_list.size() == 0)
//	{
//		return;
//	}

	// Compare each targets with detected target list

	// 1. If target is inactive: re-activate inactive targets if possible.
	// 2. If target is active: check overlapping portion with each detected target. If overlap is large, disable the overlapping detected target

	std::map<unsigned int,boost::shared_ptr<VisionTrackerTarget> >::iterator target_itr = mTargetMap.begin();
	while (target_itr != mTargetMap.end())
	{
		if (!(target_itr->second->mActive)) // for all currently inactive targets
		{
			// try to reactivate this tracker
			TrackingStatusEnum update_status = target_itr->second->reactivateTarget(detected_target_list);

			switch (update_status)
			{
				case TrackingStatus_Active:
				{
					// reactivation successful.
					mnInactiveTargets--;
					mnActiveTargets++;
					target_itr++;
					break;
				}
				case TrackingStatus_Finished:
				{
					// inactive target timed out
					// move the track to the completed list and delete this target.
					if (target_itr->second->mTrajectory.nodeCount() > 1)
					{
						mCompletedTargetList.push_back(target_itr->second);
					}

					mnInactiveTargets--;
					// remove current element from mTargetMap
					// NOTE: in c++11 map.erase() returns the next availabel iterator, but the current gcc doesn't support that
					// when upgrading gcc, visit here and update this.
#ifdef _WIN32
					target_itr = mTargetMap.erase(target_itr);
#else
					mTargetMap.erase(target_itr++);
#endif
					break;
				}
				case TrackingStatus_Inactive:
				default:
				{
					// keep inactive.
					target_itr++;
				}
			}
		} // of inactive
		else
		{
			// check if there is any nearby detected target

			std::vector<boost::shared_ptr<VisionDetectorTarget> >::iterator dt_itr = detected_target_list.begin();
			while (dt_itr != detected_target_list.end())
			{
				if ((*dt_itr)->isValid())
				{
//					int id_ = target_itr->second->getID();
					float mdist = (*dt_itr)->MahalanobisDistance(target_itr->second->getPosition());
					if ( mdist < mNewTargetMahalanobisDistanceThreshold)
					{
						// If any newly detected target is "too" close to the existing active target, this shouldn't be added as a new target
						(*dt_itr)->setInvalid();
					}
				}

				dt_itr++;
			}

			target_itr++;
		}

	}

	// add all the remaining targets
	std::vector<boost::shared_ptr<VisionDetectorTarget> >::iterator dt_itr = detected_target_list.begin();
	while (dt_itr != detected_target_list.end())
	{
		// check if the detected target is valid and inside start region
		if (((*dt_itr)->isValid()) && ((*dt_itr)->isInStartRegion()))
		{
			// create target instance
			boost::shared_ptr<VisionTrackerTarget> tptr;
			tptr.reset(new VisionTrackerTarget(mLastID));
			tptr->initialize((*dt_itr));
			mTargetMap.insert(std::pair<unsigned int, boost::shared_ptr<VisionTrackerTarget> >(mLastID,tptr));
			mLastID++;
			mnActiveTargets++;
		}
		dt_itr++;
	}

}

void VisionTargetManager::completeAllTargets()
{
	std::map<unsigned int,boost::shared_ptr<VisionTrackerTarget> >::iterator active_target_itr = mTargetMap.begin();
	while (active_target_itr != mTargetMap.end())
	{
		// move the track to the completed list and delete this target.
		mCompletedTargetList.push_back(active_target_itr->second);
		// remove current element from mTargetMap
		// NOTE: in c++11 map.erase() returns the next availabel iterator, but the current gcc doesn't support that
		// when upgrading gcc, visit here and update this.
	#ifdef _WIN32
		active_target_itr = mTargetMap.erase(active_target_itr);
	#else
		mTargetMap.erase(active_target_itr++);
	#endif
		mnActiveTargets--;
	} // of while
}

void VisionTargetManager::drawActiveTargets(cv::Mat &image) const
{
	for (std::map<unsigned int,boost::shared_ptr<VisionTrackerTarget> >::const_iterator titr = mTargetMap.begin();titr != mTargetMap.end();titr++)
	{
		if (titr->second->mActive)
		{
			titr->second->drawTarget(image);
		}
	}

}

void VisionTargetManager::drawActiveTrajectories(cv::Mat &image) const
{
	for (std::map<unsigned int,boost::shared_ptr<VisionTrackerTarget> >::const_iterator titr = mTargetMap.begin();titr != mTargetMap.end();titr++)
	{
		if (titr->second->mActive)
		{
			titr->second->drawTrajectory(image);
		}
	}

}

/*
void VisionTargetManager::drawUndistortedActiveTargets(cv::Mat &image) const
{

	for (std::map<unsigned int,boost::shared_ptr<VisionTrackerTarget> >::const_iterator titr = mTargetMap.begin();titr != mTargetMap.end();titr++)
	{
		if (titr->second->mActive)
		{
			titr->second->drawUndistortedTarget(image);
		}
	}
}
*/

void VisionTargetManager::drawInactiveTargets(cv::Mat &image) const
{
	for (std::map<unsigned int,boost::shared_ptr<VisionTrackerTarget> >::const_iterator titr = mTargetMap.begin();titr != mTargetMap.end();titr++)
	{
		if (!titr->second->mActive)
		{
			titr->second->drawTarget(image);
		}
	}

}

void VisionTargetManager::drawAllTargets(cv::Mat &image) const
{
	for (std::map<unsigned int,boost::shared_ptr<VisionTrackerTarget> >::const_iterator titr = mTargetMap.begin();titr != mTargetMap.end();titr++)
	{
		titr->second->drawTarget(image);
	}

}

#ifdef _WIN32
void VisionTargetManager::retrieveCompletedTrajectories(std::vector<ait::vision::Trajectory>& trajlist)
{
	// TODO: lock mCompletedTargetList
	std::vector<boost::shared_ptr<VisionTrackerTarget> >::iterator titr;
	for(titr=mCompletedTargetList.begin();titr!=mCompletedTargetList.end();titr++)
	{
//		if ((*titr)->mTrajectory.length() > mShortTrajectoryThreshold)
		if (((*titr)->mTrajectory.duration() > mcTrajectoryDurationThreshold) ||
				((*titr)->mTrajectory.length() > mTrajectoryLengthThreshold))
		{
			ait::vision::Trajectory  temp_traj;

			(*titr)->mTrajectory.convertToAITTrajectory(temp_traj);
			// assign trajectory id here
			temp_traj.setID(mNewTrajectoryID++);
			trajlist.push_back(temp_traj);
			titr->reset();
		}
	}
	// clear completed target list
	mCompletedTargetList.clear();
}
#else
void VisionTargetManager::retrieveCompletedTrajectories(std::vector<VisionTrajectory>& trajlist)
{
	// TODO: lock mCompletedTargetList
	std::vector<boost::shared_ptr<VisionTrackerTarget> >::iterator titr;
	for(titr=mCompletedTargetList.begin();titr!=mCompletedTargetList.end();titr++)
	{
		// for debugging display
//		int	id = (*titr)->mID;
//		float traj_length = (*titr)->mTrajectory.length();
//		if ((*titr)->mTrajectory.length() > mShortTrajectoryThreshold)
		if (((*titr)->mTrajectory.getDuration() > mTrajectoryDurationThreshold) ||
				((*titr)->mTrajectory.length() > mTrajectoryLengthThreshold))
		{
			// assign trajectory id here
			(*titr)->mTrajectory.setID(mNewTrajectoryID++);
			trajlist.push_back((*titr)->mTrajectory);
			titr->reset();
		}
		// if accumulated trajectory distance is smaller than a threshold, don't put it in the completed list
	}
	// clear completed target list
	mCompletedTargetList.clear();
}
#endif

unsigned int VisionTargetManager::getNumTotalTargets(void)
{
	return mTargetMap.size();
}

unsigned int VisionTargetManager::getNumInactiveTargets()
{
	return mnInactiveTargets;
}

#ifdef VISIONMANAGER_DEBUG_SHOW
void VisionTargetManager::debugShow(cv::Mat &dispimage)
{
	if (dispimage.type() == CV_8UC3)
	{
		dispimage.copyTo(VisionTargetManager::mDisplayImage);
	}
	else if (dispimage.type() == CV_8UC1)
	{
		cv::cvtColor(dispimage,VisionTargetManager::mDisplayImage,CV_GRAY2RGB);
	}
	cv::imshow(displayname,VisionTargetManager::mDisplayImage);
	cv::waitKey(20);
}

void VisionTargetManager::debugShow(void)
{
	cv::imshow(displayname,VisionTargetManager::mDisplayImage);
	cv::waitKey(20);
}

#endif

} // of namespace vml

2017-04-25 - Tracker restarts and reconnects to video source when capture image fails. 
2017-04-10 - OmniSensr Vision App updated to take camType and camHeight from control message. Camera parameters (such as distortion, ccdHeight and ccdWidth) are now read from camera_coverage.xml deployed in config path using camType as the key. 
2017-04-07 - Vision Post Processing Module is added. Trajectory back projection to floorplan, cart trajectory filtering, overlap and rect join method added. 
2017-03-08 - Tracker idle when no track region (polygon) is provided. Default setting added. Default setting has empty track region.
2017-02-25 - Added heart beat to vision app
2017-02-14 - Vision tracker is updated to allow track region close to the image border
2016-10-31 - State update and control message handling were added to vision app
2016-10-31 - VisionTracker module to report frame rate
2016-10-20 - Switching from uv4l to mjpg-streamer input
2016-10-06 - Multi-thread pipeline tracker is modified to reuse preallocated image buffers
2016-09-03 - VisionTrajectory IDs made unique by timestamp
2016-08-23 - Track region coordinates are changed to be proportional to image dimension
2016-08-18 - Vision app name was changed to OmniSensr_Vision_Tracker
2016-08-16 - Implemented 3 thread pipelining
2016-08-13 - Implemented 2 thread pipelining 
2016-07-25 - Human Shape Radius parameter is updated to be resolution independent. Frequent target 
2016-06-22 - Version 5.7.0001.9999 Release
2016-06-21 - Target position for detector target use foot position. 
2016-06-20 - No start region definition was added to handle frozen isle. 
2016-06-17 - Added trajectory display in debug display.
2016-06-16 - Trajectory filtering now checks two categories. If a trajectory satisfies either of the following two categories, it will be reported: 1) If duration of the track is longer than a threshold; or 2). if length of the track is longer than a threshold.
2016-06-14 - Bug fixed in checking the target duration for filtering.
2016-06-08 - A parameter "zoomCompensationFactor" was added. Removed 14th parameter from the list of distortion parameters.  Parameter "blurSigma" was made independent from the image resolution. 
2016-06-07 - Returned location of the target in trajectory was set to use foot location.  Kalman filter parameters were set to be controlled proportional to the human shape size, so it was made independent to the image resolution.
2016-06-06 - Mahalanobis distance was used to filter out any detected target if it was too close to an existing target.
2016-06-04 - First working version of "Batman" tracker committed. Look at servvid001/Lab/R&D/VML_WeeklyMeeting/2016_06_10_yyoon_VisionTrackingUpdate.ppt for detailed algorithm description. 
2016-06-02 - Opencv convex hull code could not handle inplace processing, so it was changed to use temporary buffer. Reacquisition for lost target in track region was added.  Morphological filter for target detector use smaller structuring element. 
2016-05-12 - Kalman filter for trajectory smoothing was added.
2016-05-11 - Background subtraction stores two versions of output image: 1) foreground map is the noise-suppressed background-subtracted image; 2) blob map is the clustered foreground map using morphological filter.
2016-04-29 - Inactive timeout threshold was added. Target lost longer than this timeout will be killed and reported. zoomScale parameter was added at the end of distortion parameter list to compensate for zoom effect. This was needed to make the human shape estimation more precise. 
2016-04-27 - Tracker finish method was added to finish tracking and report all the outstanding tracks upon requested. 
2016-04-25 - figtree library path changed. 
2016-04-21 - Trajectory data structure was changed to use vml::VisionTrajectory internally for all cases, and converted to ait::vision::Trajectory when reported back to VMS. 
2016-04-14 - Tracks were filtered out by their length.
2016-04-13 - Windows migration bug fix.
2016-04-12 - Online clustering and epsilon cover clustering algorithms added. figtree library was added for fast kernel density estimation. Gaussian Kernel Density Estimation based tracker using CIELab color space was added.
2016-04-03 - Erode blob map slightly to exclude border pixels to be sampled for color histogram. Blur the input image before applying color tracking. Added rbChrom tracking method. 
2016-03-31 - Created VisionFrame that holds supplemental images (e.g., undistorted, blurred). Modified VisionVideoModule to be templatized by the VisionFrame type.


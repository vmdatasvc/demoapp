/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * VisionTrackers.cpp
 *
 *  Created on: Nov 2, 2015
 *      Author: yyoon
 */

#include <opencv2/video/tracking.hpp>
#include <opencv2/imgproc/types_c.h>
#include <opencv2/imgproc.hpp>

#include <core/Image.hpp>
#include <modules/utility/MathUtil.hpp>
#include <modules/vision/VisionVideoModule.hpp>
#include <modules/vision/VisionTrackers.hpp>
#include <modules/vision/VisionDetectorTarget.hpp>
#include <modules/vision/VisionDetector.hpp>

#include <figtree.h>

#ifndef _WIN32
#include "dlib/opencv.h"
#endif

//#define DEBUG_SHOW_STEPS	// uncomment this line to display step-by-step process for debugging purposes

#ifdef DEBUG
// for debugging
#include "core/Util.hpp"
#endif

namespace vml {

HSHistogramTracker::HSHistogramTracker(const int id, boost::shared_ptr<SETTINGS> settings):
		BaseVisionTracker(id, settings),
		mcHBins(settings->getInt("HSHistogramTracker/hBins",30,false)),
		mcSBins(settings->getInt("HSHistogramTracker/sBins",32,false)),
		mcHRangeMin(settings->getFloat("HSHistogramTracker/hRangeMin",0.f,false)),
		mcHRangeMax(settings->getFloat("HSHistogramTracker/hRangeMax",180.f,false)),
		mcSRangeMin(settings->getFloat("HSHistogramTracker/sRangeMin",10.f,false)),
		mcSRangeMax(settings->getFloat("HSHistogramTracker/sRangeMax",255.f,false))
{
}

void HSHistogramTracker::initializeTrackerFeature(boost::shared_ptr<VisionDetectorTarget> dt)
{

    cv::Mat cropped_image;
    mpVideoModule->currentFrame()->blurredImage(mCropRect).copyTo(cropped_image);

#ifdef VISIONTRACKERS_DEBUG_SHOW
    // debug show
	cv::Mat cropDisplayImage(cropped_image.rows,cropped_image.cols,CV_8UC3);
#endif

	// Detected contour map is used for masking the initial target
    cv::Mat_<uchar> detectedContourMap;
    dt->getBlobContourMap(mCropRect,detectedContourMap);

	// calculate hsv and mean saturation to determine which tracker to use
	// convert to hsv
	cv::Mat hsv;
	cv::cvtColor(cropped_image, hsv, CV_RGB2HSV);

	// if mean saturation value is within the given range, use HShistogram method
	int hsv_histSize[] = {mcHBins,mcSBins};
	const int	hsv_channels[] = {0,1};	// use h and s
	float	hranges[] = {mcHRangeMin,mcHRangeMax};
	float	sranges[] = {mcSRangeMin,mcSRangeMax};
	const float* hsv_ranges[] = {hranges,sranges};
	cv::Mat	hsvmask;
	cv::inRange(hsv,cv::Scalar(0,30,10),cv::Scalar(180,255,255),hsvmask);
	// create histogram
	detectedContourMap &= hsvmask;
	cv::calcHist(&hsv,1,hsv_channels, detectedContourMap, mHistogram, 2, hsv_histSize, hsv_ranges);
	// normalize it
	cv::normalize(mHistogram,mHistogram,0,255,cv::NORM_MINMAX);

}

bool HSHistogramTracker::updateTrackerFeature(cv::Mat *disp_map, cv::Point &new_center)
{

  	// crop out input image
	cv::Mat	cropped;
    mpVideoModule->currentFrame()->blurredImage(mCropRect).copyTo(cropped);

#ifdef VISIONTRACKERS_DEBUG_SHOW
    cv::Mat cropDisplayImage(cropped.rows,cropped.cols,CV_8UC3);

#ifdef DEBUG_SHOW_STEPS
    mDisplayImage = 0;
    cropped.copyTo(cropDisplayImage);
	cropDisplayImage.copyTo(mDisplayImage(mCropRect));
	debugShow(mDisplayImage);
#endif
#endif

	// generate backprojection map
	cv::Mat backproj(cropped.rows,cropped.cols,CV_8UC1);
	// convert to hsv
	cv::Mat	hsv;
	cv::cvtColor(cropped,hsv,CV_RGB2HSV);

	const int	hsv_channels[] = {0,1};	// use h and s
	float	hranges[] = {mcHRangeMin,mcHRangeMax};
	float	sranges[] = {mcSRangeMin,mcSRangeMax};
	const float	* hsv_ranges[] = {hranges,sranges};

	cv::Mat mask;
	// get mask. mask filter out instable pixel values (too dark or saturation close to 0)
	cv::inRange(hsv,cv::Scalar(0,30,10),cv::Scalar(180,255,255),mask);
	cv::calcBackProject(&hsv, 1, hsv_channels, mHistogram, backproj, hsv_ranges);

	backproj &= mask;

#ifdef VISIONTRACKERS_DEBUG_SHOW
#ifdef DEBUG_SHOW_STEPS
		// show red backprojection
		mDisplayImage = 0;
		cv::cvtColor(backproj,cropDisplayImage,CV_GRAY2RGB);
		cropDisplayImage.copyTo(mDisplayImage(mCropRect));
		debugShow(mDisplayImage);
#endif
#endif

#ifdef VISIONTRACKERS_DEBUG_SHOW
#ifndef DEBUG_SHOW_STEPS
	// convert back proj to blue
	{
		uchar *cdi_p = (uchar*)cropDisplayImage.data;
		uchar *bpj_p = (uchar*)backproj.data;
		for (int i=0;i<cropDisplayImage.rows;i++)
		{
			for (int j=0;j<cropDisplayImage.cols;j++)
			{
					*cdi_p = (uchar)(255.f*calculateLocationLikelihoodKF(j+mCropRect.x,i+mCropRect.y));
				cdi_p+=3;
				bpj_p++;
			}
		}

		cropDisplayImage.copyTo(mDisplayImage(mCropRect));
	}
#endif
	//		debugShow(mDisplayImage);
#endif

	// for debugging, put backproj into bpimage
	if (disp_map != NULL)
	{
		cv::Mat disp_crop;
		disp_crop = (*disp_map)(mCropRect);
		backproj.copyTo(disp_crop);
	}

	// convert the backprojection to likelihoods
	// likelihood is : hsv_likelihood*backgroundsub_likelihood*motionprediction_likelihood.

#ifdef VISIONTRACKERS_DEBUG_SHOW
	// display locational likelihood.
	{
		uchar *cdi_p = (uchar*)cropDisplayImage.data;
		cdi_p +=2;	// red channel
		for (int i=0;i<cropDisplayImage.rows;i++)
		{
			for (int j=0;j<cropDisplayImage.cols;j++)
			{
					*cdi_p = (uchar)(255.f*calculateLocationLikelihoodKF(j+mCropRect.x,i+mCropRect.y));
				cdi_p+=3;
			}
		}
		cropDisplayImage.copyTo(mDisplayImage(mCropRect));
	}
#ifdef DEBUG_SHOW_STEPS
	debugShow(mDisplayImage);
#endif
#endif

	// get background map
	// get background subtraction crop.
	cv::Mat targetForegroundMap;
	mpVisionDetector->getForegroundImage()(mCropRect).copyTo(targetForegroundMap);

#ifdef VISIONTRACKERS_DEBUG_SHOW
#ifdef DEBUG_SHOW_STEPS
	mDisplayImage = 0;
	cv::cvtColor(targetForegroundMap,cropDisplayImage,CV_GRAY2RGB);
	cropDisplayImage.copyTo(mDisplayImage(mCropRect));
	debugShow(mDisplayImage);
	mDisplayImage = 0;
#endif
#endif


	// calculate likelihood
	cv::Mat_<float>	fLikelihood(cropped.rows,cropped.cols);
	float *likelihood_p = (float*)fLikelihood.data;
	uchar *bpj_p = (uchar*)backproj.data;
	uchar *fmap_p = (uchar*)targetForegroundMap.data;
	for (int i=0;i<fLikelihood.rows;i++)
	{
		for (int j=0;j<fLikelihood.cols;j++)
		{
			if (*fmap_p)
			{
				*likelihood_p = (float)*bpj_p * calculateLocationLikelihoodKF(j+mCropRect.x,i+mCropRect.y);
			}
			else
			{
				*likelihood_p = 0.f;
			}
			likelihood_p++;
			bpj_p++;
			fmap_p++;
		}
	}

#ifdef VISIONTRACKERS_DEBUG_SHOW
	// convert back proj to blue
	{
		RGBb *cdi_p = (RGBb*)cropDisplayImage.data;
		likelihood_p = (float*)fLikelihood.data;
		for (int i=0;i<cropDisplayImage.rows;i++)
		{
			for (int j=0;j<cropDisplayImage.cols;j++)
			{
				uchar lv = (uchar)*likelihood_p;
				*cdi_p = RGBb(lv,lv,lv);
				cdi_p++;
				likelihood_p++;
			}
		}
		cropDisplayImage.copyTo(mDisplayImage(mCropRect));
	}
	debugShow(mDisplayImage);
#endif

	// run mean shift on likelihood
	// TODO: now, I use estimated person shape bounding box to use opencv mean-shift, but later change it to use estimated person shape directly
	cv::Rect trackBox;
	trackBox = mPersonShape.getBoundingRect();
#ifdef VISIONTRACKERS_DEBUG_SHOW
#ifdef DEBUG_SHOW_STEPS
	cropped.copyTo(cropDisplayImage);
	cropDisplayImage.copyTo(mDisplayImage(mCropRect));
	// draw expected human shape
//	if (mEstimatedPersonShape.size()>0)
//	{
//		cv::drawContours(mDisplayImage,
//				std::vector<std::vector<cv::Point> >(1,mEstimatedPersonShape),-1,cv::Scalar(0,125,255));
//	}
	mPersonShape.draw(mDisplayImage,cv::Scalar(0,125,255));
	cv::rectangle(mDisplayImage,mCropRect,cv::Scalar(0,255,0));
	cv::rectangle(mDisplayImage,mTrackBox,cv::Scalar(255,0,0));
	debugShow(mDisplayImage);
#else
	cv::rectangle(mDisplayImage,mCropRect,cv::Scalar(0,255,0));
	cv::rectangle(mDisplayImage,trackBox,cv::Scalar(255,0,0));
#endif
#endif

	mpVideoModule->enforceWindowRect(trackBox);
	trackBox -= mCropRect.tl();
	cv::meanShift(fLikelihood,trackBox,cv::TermCriteria(cv::TermCriteria::EPS | cv::TermCriteria::COUNT, 10, 1));
	trackBox += mCropRect.tl();
	mpVideoModule->enforceWindowRect(trackBox);
	cv::Point	newCenter = (trackBox.tl()+trackBox.br())/2;

#ifdef VISIONTRACKERS_DEBUG_SHOW
	cv::rectangle(mDisplayImage,trackBox,cv::Scalar(0,0,255));
	debugShow(mDisplayImage);
#endif

	// evaluate the likelihood of the mean-shift result before update feature
	// calculate mean likelihood
	// use backproj as scratch buffer
	backproj = 0;
	mPersonShape.estimateOccupancyMapCropped(backproj,newCenter,mCropRect);
	bpj_p = (uchar*)backproj.data;
	likelihood_p = (float*)fLikelihood.data;
	float	likelihood_avg = 0.f;
	int		likelihood_num = 0;
	for (int i=0;i<fLikelihood.rows;i++)
	{
		for (int j=0;j<fLikelihood.cols;j++)
		{
			if (*bpj_p)
			{
				likelihood_avg += *likelihood_p;
				likelihood_num++;
			}
			likelihood_p++;
			bpj_p++;
		}
	}

	bool	update_success;
	likelihood_avg /= (float)likelihood_num;
	if (likelihood_avg > mcMatchLikelihoodThreshold)
	{
		// update target
		// mask off background sub map to avoid double detection
		mpVisionDetector->maskOffForegroundImage(backproj,mCropRect);
		update_success = true;
	}
	else
	{
		// make this target inactive.
		update_success = false;
	}

	new_center = newCenter;
	return update_success;
}

void HSHistogramTracker::drawTracker(cv::Mat &disp_image)
{
	BaseVisionTracker::drawTrackerHelper(disp_image,cv::Scalar(255,0,0));
	// draw other attributes here (optionally trajectory)
}

RGBHistogramTracker::RGBHistogramTracker(const int id, boost::shared_ptr<SETTINGS> settings):
		BaseVisionTracker(id, settings),
		mcRGBBins(settings->getInt("RGBHistogramTracker/rgbBins",64,false)),
		mcColorRangeMin(settings->getFloat("RGBHistogramTracker/colorRangeMin",3.f,false)),
		mcColorRangeMax(settings->getFloat("RGBHistogramTracker/colorRangeMax",254.f,false))
{
}

void RGBHistogramTracker::initializeTrackerFeature(boost::shared_ptr<VisionDetectorTarget> dt)
{

    cv::Mat cropped_image;
    mpVideoModule->currentFrame()->blurredImage(mCropRect).copyTo(cropped_image);

#ifdef VISIONTRACKERS_DEBUG_SHOW
    // debug show
	cv::Mat cropDisplayImage(cropped_image.rows,cropped_image.cols,CV_8UC3);
#endif

	// Detected contour map is used for masking the initial target
    cv::Mat_<uchar> detectedContourMap;
    dt->getBlobContourMap(mCropRect,detectedContourMap);

	// use three channels
	const int rgb_histsize = mcRGBBins;
	const int	r_channel = 0;
	const int	g_channel = 1;
	const int	b_channel = 2;

	const float	rgb_ranges[] = {3.f,255.f};
	const float *prgb_ranges = rgb_ranges;

	cv::Mat rgb_mask;

	cv::inRange(cropped_image,cv::Scalar(3,3,3),cv::Scalar(254,254,254),rgb_mask);
	detectedContourMap &= rgb_mask;
	cv::calcHist(&cropped_image,1,&r_channel,detectedContourMap,mRHistogram,1,&rgb_histsize,&prgb_ranges);
	cv::normalize(mRHistogram,mRHistogram,0,255,cv::NORM_MINMAX);
	cv::calcHist(&cropped_image,1,&g_channel,detectedContourMap,mGHistogram,1,&rgb_histsize,&prgb_ranges);
	cv::normalize(mGHistogram,mGHistogram,0,255,cv::NORM_MINMAX);
	cv::calcHist(&cropped_image,1,&b_channel,detectedContourMap,mBHistogram,1,&rgb_histsize,&prgb_ranges);
	cv::normalize(mBHistogram,mBHistogram,0,255,cv::NORM_MINMAX);

}

bool RGBHistogramTracker::updateTrackerFeature(cv::Mat *disp_map, cv::Point &new_center)
{

  	// crop out input image
	cv::Mat	cropped;
    mpVideoModule->currentFrame()->blurredImage(mCropRect).copyTo(cropped);

#ifdef VISIONTRACKERS_DEBUG_SHOW
    cv::Mat cropDisplayImage(cropped.rows,cropped.cols,CV_8UC3);

#ifdef DEBUG_SHOW_STEPS
    mDisplayImage = 0;
    cropped.copyTo(cropDisplayImage);
	cropDisplayImage.copyTo(mDisplayImage(mCropRect));
	debugShow(mDisplayImage);
#endif
#endif

	cv::Mat backproj(cropped.rows,cropped.cols,CV_8UC1);

	const int r_channel = 0;
	const int g_channel = 1;
	const int b_channel = 2;
	const float rranges[] = {3.f,255.f};
	const float *p_ranges = rranges;
	cv::Mat	r_bpj;
	cv::Mat g_bpj;
	cv::Mat b_bpj;
	cv::Mat mask;
	cv::inRange(cropped,cv::Scalar(3,3,3),cv::Scalar(254,254,254),mask);
	cv::calcBackProject(&cropped,1,&r_channel,mRHistogram,r_bpj,&p_ranges);
	cv::calcBackProject(&cropped,1,&g_channel,mGHistogram,g_bpj,&p_ranges);
	cv::calcBackProject(&cropped,1,&b_channel,mBHistogram,b_bpj,&p_ranges);

#ifdef VISIONTRACKERS_DEBUG_SHOW
#ifdef DEBUG_SHOW_STEPS
	// show red backprojection
	mDisplayImage = 0;
	cv::cvtColor(r_bpj,cropDisplayImage,CV_GRAY2RGB);
	cropDisplayImage.copyTo(mDisplayImage(mCropRect));
	debugShow(mDisplayImage);
	// show green
	mDisplayImage = 0;
	cv::cvtColor(g_bpj,cropDisplayImage,CV_GRAY2RGB);
	cropDisplayImage.copyTo(mDisplayImage(mCropRect));
	debugShow(mDisplayImage);
	// show blue
	mDisplayImage = 0;
	cv::cvtColor(b_bpj,cropDisplayImage,CV_GRAY2RGB);
	cropDisplayImage.copyTo(mDisplayImage(mCropRect));
	debugShow(mDisplayImage);
#endif
#endif
	// take average backprojection
	uchar *p_rbpj = (uchar*)r_bpj.data;
	uchar *p_gbpj = (uchar*)g_bpj.data;
	uchar *p_bbpj = (uchar*)b_bpj.data;
	uchar *p_backproj = (uchar*)backproj.data;

	for (int i=0;i<r_bpj.rows*r_bpj.cols;i++)
	{
		float tf = (float)*p_rbpj++;
		tf += (float)*p_gbpj++;
		tf += (float)*p_bbpj++;
		tf /= 3.f;

		*p_backproj++ = (uchar)tf;
	}
#ifdef VISIONTRACKERS_DEBUG_SHOW
#ifdef DEBUG_SHOW_STEPS
	// show average
	mDisplayImage = 0;
	cv::cvtColor(backproj,cropDisplayImage,CV_GRAY2RGB);
	cropDisplayImage.copyTo(mDisplayImage(mCropRect));
	debugShow(mDisplayImage);
	mDisplayImage = 0;
#endif
#endif

#ifdef VISIONTRACKERS_DEBUG_SHOW
#ifndef DEBUG_SHOW_STEPS
	// convert back proj to blue
	{
		uchar *cdi_p = (uchar*)cropDisplayImage.data;
		uchar *bpj_p = (uchar*)backproj.data;
		for (int i=0;i<cropDisplayImage.rows;i++)
		{
			for (int j=0;j<cropDisplayImage.cols;j++)
			{
					*cdi_p = (uchar)(255.f*calculateLocationLikelihoodKF(j+mCropRect.x,i+mCropRect.y));
				cdi_p+=3;
				bpj_p++;
			}
		}

		cropDisplayImage.copyTo(mDisplayImage(mCropRect));
	}
#endif
	//		debugShow(mDisplayImage);
#endif

	// for debugging, put backproj into bpimage
	if (disp_map != NULL)
	{
		cv::Mat disp_crop;
		disp_crop = (*disp_map)(mCropRect);
		backproj.copyTo(disp_crop);
	}

	// convert the backprojection to likelihoods
	// likelihood is : hsv_likelihood*backgroundsub_likelihood*motionprediction_likelihood.

#ifdef VISIONTRACKERS_DEBUG_SHOW
	// display locational likelihood.
	{
		uchar *cdi_p = (uchar*)cropDisplayImage.data;
		cdi_p +=2;	// red channel
		for (int i=0;i<cropDisplayImage.rows;i++)
		{
			for (int j=0;j<cropDisplayImage.cols;j++)
			{
					*cdi_p = (uchar)(255.f*calculateLocationLikelihoodKF(j+mCropRect.x,i+mCropRect.y));
				cdi_p+=3;
			}
		}
		cropDisplayImage.copyTo(mDisplayImage(mCropRect));
	}
#ifdef DEBUG_SHOW_STEPS
	debugShow(mDisplayImage);
#endif
#endif

	// get background map
	// get background subtraction crop.
	cv::Mat targetForegroundMap;
	mpVisionDetector->getForegroundImage()(mCropRect).copyTo(targetForegroundMap);

#ifdef VISIONTRACKERS_DEBUG_SHOW
#ifdef DEBUG_SHOW_STEPS
	mDisplayImage = 0;
	cv::cvtColor(targetForegroundMap,cropDisplayImage,CV_GRAY2RGB);
	cropDisplayImage.copyTo(mDisplayImage(mCropRect));
	debugShow(mDisplayImage);
	mDisplayImage = 0;
#endif
#endif


	// calculate likelihood
	cv::Mat_<float>	fLikelihood(cropped.rows,cropped.cols);
	float *likelihood_p = (float*)fLikelihood.data;
	uchar *bpj_p = (uchar*)backproj.data;
	uchar *fmap_p = (uchar*)targetForegroundMap.data;
	for (int i=0;i<fLikelihood.rows;i++)
	{
		for (int j=0;j<fLikelihood.cols;j++)
		{
			if (*fmap_p)
			{
				*likelihood_p = (float)*bpj_p * calculateLocationLikelihoodKF(j+mCropRect.x,i+mCropRect.y);
			}
			else
			{
				*likelihood_p = 0.f;
			}
			likelihood_p++;
			bpj_p++;
			fmap_p++;
		}
	}

#ifdef VISIONTRACKERS_DEBUG_SHOW
	// convert back proj to blue
	{
		RGBb *cdi_p = (RGBb*)cropDisplayImage.data;
		likelihood_p = (float*)fLikelihood.data;
		for (int i=0;i<cropDisplayImage.rows;i++)
		{
			for (int j=0;j<cropDisplayImage.cols;j++)
			{
				uchar lv = (uchar)*likelihood_p;
				*cdi_p = RGBb(lv,lv,lv);
				cdi_p++;
				likelihood_p++;
			}
		}
		cropDisplayImage.copyTo(mDisplayImage(mCropRect));
	}
	debugShow(mDisplayImage);
#endif

	// run mean shift on likelihood
	// TODO: now, I use estimated person shape bounding box to use opencv mean-shift, but later change it to use estimated person shape directly
	cv::Rect trackBox;
	trackBox = mPersonShape.getBoundingRect();
#ifdef VISIONTRACKERS_DEBUG_SHOW
#ifdef DEBUG_SHOW_STEPS
	cropped.copyTo(cropDisplayImage);
	cropDisplayImage.copyTo(mDisplayImage(mCropRect));
	// draw expected human shape
//	if (mEstimatedPersonShape.size()>0)
//	{
//		cv::drawContours(mDisplayImage,
//				std::vector<std::vector<cv::Point> >(1,mEstimatedPersonShape),-1,cv::Scalar(0,125,255));
//	}
	mPersonShape.draw(mDisplayImage,cv::Scalar(0,125,255));
	cv::rectangle(mDisplayImage,mCropRect,cv::Scalar(0,255,0));
	cv::rectangle(mDisplayImage,mTrackBox,cv::Scalar(255,0,0));
	debugShow(mDisplayImage);
#else
	cv::rectangle(mDisplayImage,mCropRect,cv::Scalar(0,255,0));
	cv::rectangle(mDisplayImage,trackBox,cv::Scalar(255,0,0));
#endif
#endif

	mpVideoModule->enforceWindowRect(trackBox);
	trackBox -= mCropRect.tl();
	cv::meanShift(fLikelihood,trackBox,cv::TermCriteria(cv::TermCriteria::EPS | cv::TermCriteria::COUNT, 10, 1));
	trackBox += mCropRect.tl();
	mpVideoModule->enforceWindowRect(trackBox);
	cv::Point	newCenter = (trackBox.tl()+trackBox.br())/2;

#ifdef VISIONTRACKERS_DEBUG_SHOW
	cv::rectangle(mDisplayImage,trackBox,cv::Scalar(0,0,255));
	debugShow(mDisplayImage);
#endif

	// evaluate the likelihood of the mean-shift result before update feature
	// calculate mean likelihood
	// use backproj as scratch buffer
	backproj = 0;
	mPersonShape.estimateOccupancyMapCropped(backproj,newCenter,mCropRect);
	bpj_p = (uchar*)backproj.data;
	likelihood_p = (float*)fLikelihood.data;
	float	likelihood_avg = 0.f;
	int		likelihood_num = 0;
	for (int i=0;i<fLikelihood.rows;i++)
	{
		for (int j=0;j<fLikelihood.cols;j++)
		{
			if (*bpj_p)
			{
				likelihood_avg += *likelihood_p;
				likelihood_num++;
			}
			likelihood_p++;
			bpj_p++;
		}
	}

	bool	update_success;
	likelihood_avg /= (float)likelihood_num;
	if (likelihood_avg > mcMatchLikelihoodThreshold)
	{
		// update target
		// mask off background sub map to avoid double detection
		mpVisionDetector->maskOffForegroundImage(backproj,mCropRect);
		update_success = true;
	}
	else
	{
		// make this target inactive.
		update_success = false;
	}

	new_center = newCenter;
	return update_success;

}

void RGBHistogramTracker::drawTracker(cv::Mat &disp_image)
{
	BaseVisionTracker::drawTrackerHelper(disp_image,cv::Scalar(255,0,0));
	// draw other attributes here (optionally trajectory)
}

HybridHistogramTracker::HybridHistogramTracker(const int id, boost::shared_ptr<SETTINGS> settings):
		BaseVisionTracker(id, settings),
		mCount(0),
		mcSaturationLowThreshold(settings->getFloat("HybridHistogramTracker/saturationLowThreshold",50.f,false)),
		mIsHSHistTracker(false),
		mcHBins(settings->getInt("HSHistogramTracker/hBins",30,false)),
		mcSBins(settings->getInt("HSHistogramTracker/sBins",32,false)),
		mcHRangeMin(settings->getFloat("HSHistogramTracker/hRangeMin",0.f,false)),
		mcHRangeMax(settings->getFloat("HSHistogramTracker/hRangeMax",180.f,false)),
		mcSRangeMin(settings->getFloat("HSHistogramTracker/sRangeMin",10.f,false)),
		mcSRangeMax(settings->getFloat("HSHistogramTracker/sRangeMax",255.f,false)),
		mcRGBBins(settings->getInt("RGBHistogramTracker/rgbBins",64,false)),
		mcRGBRangeMin(settings->getFloat("RGBHistogramTracker/colorRangeMin",3.f,false)),
		mcRGBRangeMax(settings->getFloat("RGBHistogramTracker/colorRangeMax",254.f,false))
{
}

void HybridHistogramTracker::initializeTrackerFeature(boost::shared_ptr<VisionDetectorTarget> dt)
{
    cv::Mat cropped_image;
    mpVideoModule->currentFrame()->blurredImage(mCropRect).copyTo(cropped_image);

#ifdef VISIONTRACKERS_DEBUG_SHOW
    // debug show
	cv::Mat cropDisplayImage(cropped_image.rows,cropped_image.cols,CV_8UC3);
#endif

	// Detected contour map is used for masking the initial target
    cv::Mat_<uchar> detectedContourMap;
    dt->getBlobContourMap(mCropRect,detectedContourMap);

	// calculate hsv and mean saturation to determine which tracker to use
	// convert to hsv
	cv::Mat hsv;
	cv::cvtColor(cropped_image, hsv, CV_RGB2HSV);
	// calculate mean saturation
	// get mean saturation value
	float sat_mean = 0.f,blob_mass = 0.f;
	uchar *sat_p = (uchar*)hsv.data;
	sat_p++;
	uchar *mask_p = (uchar*)detectedContourMap.data;
	for (int i=0;i<hsv.rows*hsv.cols;i++)
	{
		if (*mask_p)
		{
			sat_mean += *sat_p;
			blob_mass += 1.f;
		}
		sat_p += 3;
		mask_p++;
	}
	sat_mean /= blob_mass;

	if (sat_mean > mcSaturationLowThreshold)
	{
		// if mean saturation value is within the given range, use HShistogram method
		int hsv_histSize[] = {mcHBins,mcSBins};
		const int	hsv_channels[] = {0,1};	// use h and s
		float	hranges[] = {mcHRangeMin,mcHRangeMax};
		float	sranges[] = {mcSRangeMin,mcSRangeMax};
		const float* hsv_ranges[] = {hranges,sranges};
		cv::Mat	hsvmask;
		cv::inRange(hsv,cv::Scalar(0,30,10),cv::Scalar(180,255,255),hsvmask);
		// create histogram
		detectedContourMap &= hsvmask;
		cv::calcHist(&hsv,1,hsv_channels, detectedContourMap, mHistogram, 2, hsv_histSize, hsv_ranges);
		// normalize it
		cv::normalize(mHistogram,mHistogram,0,255,cv::NORM_MINMAX);
		mIsHSHistTracker = true;
	}
	else
	{
		// use three channels
		const int rgb_histsize = mcRGBBins;
		const int	r_channel = 0;
		const int	g_channel = 1;
		const int	b_channel = 2;

		const float	rgb_ranges[] = {3.f,255.f};
		const float *prgb_ranges = rgb_ranges;

		cv::Mat rgb_mask;

		cv::inRange(cropped_image,cv::Scalar(3,3,3),cv::Scalar(254,254,254),rgb_mask);
		detectedContourMap &= rgb_mask;
		cv::calcHist(&cropped_image,1,&r_channel,detectedContourMap,mHistogram,1,&rgb_histsize,&prgb_ranges);
		cv::normalize(mHistogram,mHistogram,0,255,cv::NORM_MINMAX);
		cv::calcHist(&cropped_image,1,&g_channel,detectedContourMap,mHistogramG,1,&rgb_histsize,&prgb_ranges);
		cv::normalize(mHistogramG,mHistogramG,0,255,cv::NORM_MINMAX);
		cv::calcHist(&cropped_image,1,&b_channel,detectedContourMap,mHistogramB,1,&rgb_histsize,&prgb_ranges);
		cv::normalize(mHistogramB,mHistogramB,0,255,cv::NORM_MINMAX);
		mIsHSHistTracker = false;
	}

}

bool HybridHistogramTracker::updateTrackerFeature(cv::Mat *disp_map, cv::Point &new_center)
{

  	// crop out input image
	cv::Mat	cropped;
    mpVideoModule->currentFrame()->blurredImage(mCropRect).copyTo(cropped);

#ifdef VISIONTRACKERS_DEBUG_SHOW
    cv::Mat cropDisplayImage(cropped.rows,cropped.cols,CV_8UC3);

#ifdef DEBUG_SHOW_STEPS
    mDisplayImage = 0;
    cropped.copyTo(cropDisplayImage);
	cropDisplayImage.copyTo(mDisplayImage(mCropRect));
	debugShow(mDisplayImage);
#endif
#endif

	// generate backprojection map
	cv::Mat backproj(cropped.rows,cropped.cols,CV_8UC1);
	if (mIsHSHistTracker)
	{
		// convert to hsv
		cv::Mat	hsv;
		cv::cvtColor(cropped,hsv,CV_RGB2HSV);

		const int	hsv_channels[] = {0,1};	// use h and s
		float	hranges[] = {mcHRangeMin,mcHRangeMax};
		float	sranges[] = {mcSRangeMin,mcSRangeMax};
		const float	* hsv_ranges[] = {hranges,sranges};

		cv::Mat mask;
		// get mask. mask filter out instable pixel values (too dark or saturation close to 0)
		cv::inRange(hsv,cv::Scalar(0,30,10),cv::Scalar(180,255,255),mask);
		cv::calcBackProject(&hsv, 1, hsv_channels, mHistogram, backproj, hsv_ranges);

		backproj &= mask;

#ifdef VISIONTRACKERS_DEBUG_SHOW
#ifdef DEBUG_SHOW_STEPS
		// show red backprojection
		mDisplayImage = 0;
		cv::cvtColor(backproj,cropDisplayImage,CV_GRAY2RGB);
		cropDisplayImage.copyTo(mDisplayImage(mCropRect));
		debugShow(mDisplayImage);
#endif
#endif
	}
	else
	{
		const int r_channel = 0;
		const int g_channel = 1;
		const int b_channel = 2;
		const float rranges[] = {3.f,255.f};
		const float *p_ranges = rranges;
		cv::Mat	r_bpj;
		cv::Mat g_bpj;
		cv::Mat b_bpj;
		cv::Mat mask;
		cv::inRange(cropped,cv::Scalar(3,3,3),cv::Scalar(254,254,254),mask);
		cv::calcBackProject(&cropped,1,&r_channel,mHistogram,r_bpj,&p_ranges);
		cv::calcBackProject(&cropped,1,&g_channel,mHistogramG,g_bpj,&p_ranges);
		cv::calcBackProject(&cropped,1,&b_channel,mHistogramB,b_bpj,&p_ranges);

#ifdef VISIONTRACKERS_DEBUG_SHOW
#ifdef DEBUG_SHOW_STEPS
		// show red backprojection
		mDisplayImage = 0;
		cv::cvtColor(r_bpj,cropDisplayImage,CV_GRAY2RGB);
		cropDisplayImage.copyTo(mDisplayImage(mCropRect));
		debugShow(mDisplayImage);
		// show green
		mDisplayImage = 0;
		cv::cvtColor(g_bpj,cropDisplayImage,CV_GRAY2RGB);
		cropDisplayImage.copyTo(mDisplayImage(mCropRect));
		debugShow(mDisplayImage);
		// show blue
		mDisplayImage = 0;
		cv::cvtColor(b_bpj,cropDisplayImage,CV_GRAY2RGB);
		cropDisplayImage.copyTo(mDisplayImage(mCropRect));
		debugShow(mDisplayImage);
		// show average
#endif
#endif
		uchar *p_rbpj = (uchar*)r_bpj.data;
		uchar *p_gbpj = (uchar*)g_bpj.data;
		uchar *p_bbpj = (uchar*)b_bpj.data;
		uchar *p_backproj = (uchar*)backproj.data;

		for (int i=0;i<r_bpj.rows*r_bpj.cols;i++)
		{
			float tf = (float)*p_rbpj++;
			tf += (float)*p_gbpj++;
			tf += (float)*p_bbpj++;
			tf /= 3.f;

			*p_backproj++ = (uchar)tf;
		}
#ifdef VISIONTRACKERS_DEBUG_SHOW
#ifdef DEBUG_SHOW_STEPS
		// show average
		mDisplayImage = 0;
		cv::cvtColor(backproj,cropDisplayImage,CV_GRAY2RGB);
		cropDisplayImage.copyTo(mDisplayImage(mCropRect));
		debugShow(mDisplayImage);
		mDisplayImage = 0;
#endif
#endif
	}

#ifdef VISIONTRACKERS_DEBUG_SHOW
#ifndef DEBUG_SHOW_STEPS
	// convert back proj to blue
	{
		uchar *cdi_p = (uchar*)cropDisplayImage.data;
		uchar *bpj_p = (uchar*)backproj.data;
		for (int i=0;i<cropDisplayImage.rows;i++)
		{
			for (int j=0;j<cropDisplayImage.cols;j++)
			{
					*cdi_p = (uchar)(255.f*calculateLocationLikelihoodKF(j+mCropRect.x,i+mCropRect.y));
				cdi_p+=3;
				bpj_p++;
			}
		}

		cropDisplayImage.copyTo(mDisplayImage(mCropRect));
	}
#endif
	//		debugShow(mDisplayImage);
#endif

	// for debugging, put backproj into bpimage
	if (disp_map != NULL)
	{
		cv::Mat disp_crop;
		disp_crop = (*disp_map)(mCropRect);
		backproj.copyTo(disp_crop);
	}

	// convert the backprojection to likelihoods
	// likelihood is : hsv_likelihood*backgroundsub_likelihood*motionprediction_likelihood.

#ifdef VISIONTRACKERS_DEBUG_SHOW
	// display locational likelihood.
	{
		uchar *cdi_p = (uchar*)cropDisplayImage.data;
		cdi_p +=2;	// red channel
		for (int i=0;i<cropDisplayImage.rows;i++)
		{
			for (int j=0;j<cropDisplayImage.cols;j++)
			{
					*cdi_p = (uchar)(255.f*calculateLocationLikelihoodKF(j+mCropRect.x,i+mCropRect.y));
				cdi_p+=3;
			}
		}
		cropDisplayImage.copyTo(mDisplayImage(mCropRect));
	}
#ifdef DEBUG_SHOW_STEPS
	debugShow(mDisplayImage);
#endif
#endif

	// get background map
	// get background subtraction crop.
	cv::Mat targetForegroundMap;
	mpVisionDetector->getForegroundImage()(mCropRect).copyTo(targetForegroundMap);

#ifdef VISIONTRACKERS_DEBUG_SHOW
#ifdef DEBUG_SHOW_STEPS
	mDisplayImage = 0;
	cv::cvtColor(targetForegroundMap,cropDisplayImage,CV_GRAY2RGB);
	cropDisplayImage.copyTo(mDisplayImage(mCropRect));
	debugShow(mDisplayImage);
	mDisplayImage = 0;
#endif
#endif


	// calculate likelihood
	cv::Mat_<float>	fLikelihood(cropped.rows,cropped.cols);
	float *likelihood_p = (float*)fLikelihood.data;
	uchar *bpj_p = (uchar*)backproj.data;
	uchar *fmap_p = (uchar*)targetForegroundMap.data;
	for (int i=0;i<fLikelihood.rows;i++)
	{
		for (int j=0;j<fLikelihood.cols;j++)
		{
			if (*fmap_p)
			{
				*likelihood_p = (float)*bpj_p * calculateLocationLikelihoodKF(j+mCropRect.x,i+mCropRect.y);
			}
			else
			{
				*likelihood_p = 0.f;
			}
			likelihood_p++;
			bpj_p++;
			fmap_p++;
		}
	}

#ifdef VISIONTRACKERS_DEBUG_SHOW
	// convert back proj to blue
	{
		RGBb *cdi_p = (RGBb*)cropDisplayImage.data;
		likelihood_p = (float*)fLikelihood.data;
		for (int i=0;i<cropDisplayImage.rows;i++)
		{
			for (int j=0;j<cropDisplayImage.cols;j++)
			{
				uchar lv = (uchar)*likelihood_p;
				*cdi_p = RGBb(lv,lv,lv);
				cdi_p++;
				likelihood_p++;
			}
		}
		cropDisplayImage.copyTo(mDisplayImage(mCropRect));
	}
	debugShow(mDisplayImage);
#endif

	// run mean shift on likelihood
	// TODO: now, I use estimated person shape bounding box to use opencv mean-shift, but later change it to use estimated person shape directly
	cv::Rect trackBox;
	trackBox = mPersonShape.getBoundingRect();
#ifdef VISIONTRACKERS_DEBUG_SHOW
#ifdef DEBUG_SHOW_STEPS
	cropped.copyTo(cropDisplayImage);
	cropDisplayImage.copyTo(mDisplayImage(mCropRect));
	// draw expected human shape
//	if (mEstimatedPersonShape.size()>0)
//	{
//		cv::drawContours(mDisplayImage,
//				std::vector<std::vector<cv::Point> >(1,mEstimatedPersonShape),-1,cv::Scalar(0,125,255));
//	}
	mPersonShape.draw(mDisplayImage,cv::Scalar(0,125,255));
	cv::rectangle(mDisplayImage,mCropRect,cv::Scalar(0,255,0));
	cv::rectangle(mDisplayImage,mTrackBox,cv::Scalar(255,0,0));
	debugShow(mDisplayImage);
#else
	cv::rectangle(mDisplayImage,mCropRect,cv::Scalar(0,255,0));
	cv::rectangle(mDisplayImage,trackBox,cv::Scalar(255,0,0));
#endif
#endif

	mpVideoModule->enforceWindowRect(trackBox);
	trackBox -= mCropRect.tl();
	cv::meanShift(fLikelihood,trackBox,cv::TermCriteria(cv::TermCriteria::EPS | cv::TermCriteria::COUNT, 10, 1));
	trackBox += mCropRect.tl();
	mpVideoModule->enforceWindowRect(trackBox);
	cv::Point	newCenter = (trackBox.tl()+trackBox.br())/2;

#ifdef VISIONTRACKERS_DEBUG_SHOW
	cv::rectangle(mDisplayImage,trackBox,cv::Scalar(0,0,255));
	debugShow(mDisplayImage);
#endif

	// evaluate the likelihood of the mean-shift result before update feature
	// calculate mean likelihood
	// use backproj as scratch buffer
	backproj = 0;
	mPersonShape.estimateOccupancyMapCropped(backproj,newCenter,mCropRect);
	bpj_p = (uchar*)backproj.data;
	likelihood_p = (float*)fLikelihood.data;
	float	likelihood_avg = 0.f;
	int		likelihood_num = 0;
	for (int i=0;i<fLikelihood.rows;i++)
	{
		for (int j=0;j<fLikelihood.cols;j++)
		{
			if (*bpj_p)
			{
				likelihood_avg += *likelihood_p;
				likelihood_num++;
			}
			likelihood_p++;
			bpj_p++;
		}
	}

	bool	update_success;
	likelihood_avg /= (float)likelihood_num;
	if (likelihood_avg > mcMatchLikelihoodThreshold)
	{
		// update target
		// mask off background sub map to avoid double detection
		mpVisionDetector->maskOffForegroundImage(backproj,mCropRect);
		update_success = true;
	}
	else
	{
		// make this target inactive.
		update_success = false;
	}

	new_center = newCenter;
	return update_success;
}

void HybridHistogramTracker::drawTracker(cv::Mat &disp_image)
{
	if (mIsHSHistTracker)
	{
		BaseVisionTracker::drawTrackerHelper(disp_image, cv::Scalar(255,0,125));
	}
	else
	{
		BaseVisionTracker::drawTrackerHelper(disp_image, cv::Scalar(0,255,0));
	}
	// draw other attributes here (optionally trajectory)
}

///////////////////////////////////////////////////////////////////////////////
// KDE Tracker
#if 0
double KDETracker::CIELabPoint::dist(KDETracker::CIELabPoint other)
{
	double da = a-other.a;
	double db = b-other.b;
	return(sqrt(da*da+db*db));
}


void KDETracker::CIELabCluster::reavg(KDETracker::CIELabPoint other)
{
	double ma = center.a*mass;
	double mb = center.b*mass;

	mass += 1;	// mass of a point is 1

	center.a = (ma+other.a)/mass;
	center.b = (mb+other.b)/mass;
}

KDETracker::KDETracker(const int id, boost::shared_ptr<SETTINGS> settings):
		BaseVisionTracker(id, settings),
		mCount(0),
		mKernelBandwidth(settings->getDouble("KDETracker/kernelBandwidth",0.3f,false)),
		mKernelWeight(NULL)
{
}

KDETracker::~KDETracker()
{
	delete[] mKernelWeight;
}

void KDETracker::initialize(boost::shared_ptr<VisionDetectorTarget> dt)
{
	initializeBaseFeatures(dt->getPosition());

	cv::Mat contourMap;
//	dt->getTrackerInitializerInfo(init_rect,contourMap);
	dt->getBlobContourMap(mCropRect,contourMap);

	cv::Mat cropped_image;
	mpVideoModule->currentFrame()->blurredImage(mCropRect).copyTo(cropped_image);
	int	n_pts = cropped_image.rows*cropped_image.cols;

#ifdef DEBUG
	{
		char save_filename[256];
		cv::Mat cropped_image_disp;
		cv::cvtColor(cropped_image,cropped_image_disp,CV_RGB2BGR);
		sprintf(save_filename,"image_init_%03d.png",mID);
		cv::imwrite(save_filename,cropped_image_disp);
	}
#endif

	cv::Mat	cropped_f(cropped_image.rows,cropped_image.cols,CV_32FC3);
	cv::Mat	cropped_fc(cropped_image.rows,cropped_image.cols,CV_32FC3);

	// convert image to 32bit float
	uchar *p_8U = (uchar*)cropped_image.data;
	float *p_32F = (float*)cropped_f.data;
	for (int i=0;i<n_pts*3;i++)
	{
		*p_32F++ = ((float)*p_8U++)/255.f;
	}

	// use CIELab
	cv::cvtColor(cropped_f,cropped_fc,CV_RGB2Lab);

#ifdef DEBUG
	// convert to rb-chrom image for displaying purpose.
	{
		char save_filename[256];
		sprintf(save_filename,"image_init_%03d.bin",mID);
		save_raw(cropped_f,save_filename);
		sprintf(save_filename,"lab_init_%03d.bin",mID);
		save_raw(cropped_fc,save_filename);
		sprintf(save_filename,"ctmap_init_%03d.bin",mID);
		save_raw(contourMap,save_filename);
	}
#endif

	//** Gaussian KDE approach.
	// find e-cover of the points under initial blob map
	RGBf *p_cropped = (RGBf*)cropped_fc.data;
	uchar *p_map = (uchar*)contourMap.data;
	for (int i=0;i<n_pts;i++,p_cropped++,p_map++)
	{
		if (*p_map > 0)
		{
			mKernelSource.push_back(CIELabPoint((double)p_cropped->g(),(double)p_cropped->b()));
		}
	}

	// run e-cover
	// the result points are the estimated kernels
	// farthest first algorithm doesn't guarantee tight bound. Try online clustering
//	onlineClustering<CIELabPoint,CIELabCluster>(fg_pts,mKernelClusterRadius,mKernelClusters);

	// figtree is fast enough, so no clustering needed
	mKernelWeight = new double[mKernelSource.size()];
	for (unsigned int i=0;i<mKernelSource.size();i++)
	{
		mKernelWeight[i] = 1.f;
	}


//#ifdef DEBUG
	// save likelihood for displaying purpose
	cv::Mat pf(cropped_image.rows,cropped_image.cols,CV_64FC1);

	std::vector<CIELabPoint> f_data;
	f_data.resize(pf.rows*pf.cols);
	p_cropped = (RGBf*)cropped_fc.data;
	for (int i=0;i<n_pts;i++)
	{
		f_data[i].a = (double)p_cropped->g();
		f_data[i].b = (double)p_cropped->b();
		p_cropped++;
	}
	figtree(2,
			mKernelSource.size(),
			pf.rows*pf.cols,
			1,
			(double*)&mKernelSource[0],
			mKernelBandwidth,
			mKernelWeight,
			(double*)&f_data[0],
			1e-4,
			(double*)pf.data,
			FIGTREE_EVAL_AUTO,
			FIGTREE_PARAM_NON_UNIFORM);

	// normalize by max likelihood
//		double* p_pf = (double*)pf.data;
//		double l_max = -1.f;
//		for (int i=0;i<n_pts;i++)
//		{
//			if (*p_pf > l_max) l_max = *p_pf;
//			p_pf++;
//		}
//		p_pf = (double*)pf.data;
//		for (int i=0;i<n_pts;i++)
//		{
//			*p_pf /= l_max;
//			p_pf++;
//		}
	// normalize by weight sum
//		double* p_pf = (double*)pf.data;
//		for (int i=0;i<n_pts;i++)
//		{
//			*p_pf /= weight_sum;
//			p_pf++;
//		}

	// normalize by sigmoid midpoint at half of 99th percentile of pf
	// find 99th percentile value
	std::vector<double> temp_dvec;
	temp_dvec.resize(pf.rows*pf.cols);
	double *p_pf = (double*)pf.data;
	for (int i=0;i<pf.rows*pf.cols;i++)
	{
		temp_dvec[i] = *p_pf;
		p_pf++;
	}

	p_pf = (double*)pf.data;
	for (int i=0;i<pf.rows*pf.cols;i++)
	{
		if (*p_pf > 1.f) *p_pf = 1.f;
		p_pf++;
	}

#ifdef DEBUG
	{
		char save_filename[256];
		sprintf(save_filename,"pf_init_%03d.bin",mID);
		save_raw(pf,save_filename);
	}
#endif

}

TrackingStatusEnum KDETracker::update(cv::Mat *disp_map)
{

	// crop out input image
	cv::Mat	cropped_image;
	mpVideoModule->currentFrame()->blurredImage(mCropRect).copyTo(cropped_image);

	int	n_pts = cropped_image.rows*cropped_image.cols;

	cv::Mat	cropped_f(cropped_image.rows,cropped_image.cols,CV_32FC3);
	cv::Mat	cropped_fc;

	// convert image to 32bit float
	uchar *p_8U = (uchar*)cropped_image.data;
	float *p_32F = (float*)cropped_f.data;
	for (int i=0;i<n_pts*3;i++)
	{
		*p_32F++ = ((float)*p_8U++)/255.f;
	}

	// use CIELab
	cv::cvtColor(cropped_f,cropped_fc,CV_RGB2Lab);

#ifdef DEBUG
	char save_filename[256];
	sprintf(save_filename,"lab_%03d_%03d.bin",mID,mCount);
	save_raw(cropped_fc,save_filename);
#endif

	std::vector<CIELabPoint>	f_data;
	f_data.resize(n_pts);
	RGBf *p_cropped = (RGBf*)cropped_fc.data;
	std::vector<CIELabPoint>::iterator f_data_itr;
	f_data_itr = f_data.begin();
	for (int i=0;i<n_pts;i++,f_data_itr++)
	{
		f_data_itr->a = (double)p_cropped->g();
		f_data_itr->b = (double)p_cropped->b();
		p_cropped++;
	}


	cv::Mat	pf(cropped_image.rows,cropped_image.cols,CV_64FC1);  // double matrix for foreground likelihood

	// estimate KDE likelihood
	figtree(2,
			mKernelSource.size(),
			n_pts,
			1,
			(double*)&mKernelSource[0],
			mKernelBandwidth,
			mKernelWeight,
			(double*)&f_data[0],
			1e-4,
			(double*)pf.data,
			FIGTREE_EVAL_AUTO,
			FIGTREE_PARAM_NON_UNIFORM);

	// normalize by max likelihood
//	double* p_pf = (double*)pf.data;
//	double l_max = -1.f;
//	for (int i=0;i<n_pts;i++)
//	{
//		if (*p_pf > l_max) l_max = *p_pf;
//		p_pf++;
//	}
//
//	p_pf = (double*)pf.data;
//
//	for (int i=0;i<n_pts;i++)
//	{
//		*p_pf /= l_max;
//		p_pf++;
//	}
	// normalize by weight sum
//	double *p_pf = (double*)pf.data;
//	for (int i=0;i<n_pts;i++)
//	{
//		*p_pf /= weight_sum;
//		p_pf++;
//	}

	double *p_pf = (double*)pf.data;
	for (int i=0;i<pf.rows*pf.cols;i++)
	{
//		*p_pf = 1.f/(1.f+exp(-5.f*(*p_pf/midpoint - 1.f)));
//		*p_pf /= maxpoint;
		if (*p_pf > 1.f) *p_pf = 1.f;	// just clipping
		p_pf++;
	}


#ifdef DEBUG
	sprintf(save_filename,"pf_%03d_%03d.bin",mID,mCount);
	save_raw(pf,save_filename);
#endif

#if 0
	// 2. get the likelihood of background from background image
	cv::Mat background_crop;
	const cv::Mat &background = VisionModuleManager::instance()->getDetector()->getBackgroundImage();
	background(mCropRect).copyTo(background_crop);

	cv::Mat background_sample_map(background_crop.rows,background_crop.cols,CV_8UC1);
	background_sample_map = 0;

	// uniformly sample background pixel rb-chrom values and use it as background kde
	std::vector<RBChromPoint> b_kernels;
	p_cropped = (RGBb*)background_crop.data;
	uchar *p_bg_s = (uchar*)background_sample_map.data;
	int steps = 5;
	for (int i=0;i<background_crop.rows*background_crop.cols;i+=steps)
	{
		b_kernels.push_back(RBChromPoint((double)p_cropped->g()-(double)p_cropped->r(),
				(double)p_cropped->g()-(double)p_cropped->b()));
		*p_bg_s = 1;
		p_cropped+=steps;
		p_bg_s += steps;
	}

#ifdef DEBUG
	sprintf(save_filename,"bgsample_%03d_%03d.bin",mID,mCount);
	save_raw(background_sample_map,save_filename);
#endif

	epsilonCoveringFarthestFirst<RBChromPoint>(b_kernels,mKernelRadius,b_kernels);

	cv::Mat pb(cropped.rows,cropped.cols,CV_64FC1);

	// calculate background likelihood from kde.
	double *weights_b = new double[b_kernels.size()];
	for (unsigned int i=0;i<b_kernels.size();i++) weights_b[i] = 1.f;
	figtree(2,b_kernels.size(),pb.rows*pb.cols,1,(double*)&b_kernels[0],mKernelBandwidth,weights_b,(double*)&f_data[0],1e-2,(double*)pb.data);
	delete[] weights_b;

#ifdef DEBUG
	sprintf(save_filename,"pb_%03d_%03d.bin",mID,mCount);
	save_raw(pb,save_filename);
#endif

	// merge the foreground/background likelihood to calculate target likelihood.
	double *p_pf = (double*)pf.data;
	double *p_pb = (double*)pb.data;
	for (int i=0;i<pf.rows*pf.cols;i++)
	{
		*p_pf /= (*p_pf + *p_pb);
		p_pf++;
		p_pb++;
	}

#ifdef DEBUG
	sprintf(save_filename,"pfl_%03d_%03d.bin",mID,mCount);
	save_raw(pf,save_filename);
#endif
#endif
	// for now, convert to bpj
//	// find out the max of pf
//	double max_pf = -1.f;
//	double *p_pf = (double*)pf.data;
//	for (int i=0;i<pf.rows*pf.cols;i++)
//	{
//		if (*p_pf > max_pf) max_pf = *p_pf;
//	}

#ifdef DEBUG
	{
		cv::Mat backproj(pf.rows,pf.cols,CV_8UC1);
		uchar *p_bpj = (uchar*)backproj.data;
		p_pf = (double*)pf.data;
		for (int i=0;i<backproj.rows*backproj.cols;i++)
		{
			*p_bpj = (uchar)(*p_pf*255.f);
			p_bpj++;
			p_pf++;
		}

		sprintf(save_filename,"bpj_%03d_%03d.bin",mID,mCount);
		save_raw(backproj,save_filename);
	}
#endif

#if 0
	// NOTE: currently obsolete. fix later
	// for debugging, put pf into bpimage
	if (disp_map != NULL)
	{
		cv::Mat backproj(pf.rows,pf.cols,CV_8UC1);
		uchar *p_bpj = (uchar*)backproj.data;
		p_pf = (double*)pf.data;
		for (int i=0;i<backproj.rows*backproj.cols;i++)
		{
			*p_bpj = (uchar)(*p_pf*255.f);
			p_bpj++;
			p_pf++;
		}
		cv::Mat disp_crop;
		disp_crop = (*disp_map)(mCropRect);
		backproj.copyTo(disp_crop);
	}

	boost::shared_ptr<VisionDetectorTarget> match_blob = \
			this->mergeAndFindCandidateBlob(pf);

	bool found_match = false;
	if(match_blob)
	{
		// update the current position to max candidate position
		updateBaseFeatures(match_blob->getPosition());
		match_blob->getCvxHull(bestMatchingBlob);
		// TODO: Optional: update target feature based on this blob

		// set this blob invalid to be removed later
		match_blob->setInvalid();
		found_match = true;
	}
	else
	{
		bestMatchingBlob.clear();
		// if no candidate found
//		if (mLastStatus == TrackingStatus_Start)
//		{
//			// lost target. kill this
//			return TrackingStatus_Finished;
//		}
//		else
//		{
//			//use color update
//			mTrackBox = cv::boundingRect(mEstimatedPersonShape);
//			mTrackBox -= mCropRect.tl();
//			mTrackBox &= mWindowRect;
//			cv::meanShift(backproj,mTrackBox,cv::TermCriteria(cv::TermCriteria::EPS | cv::TermCriteria::COUNT, 10, 1));
//			mTrackBox += mCropRect.tl();
//			updateBaseFeatures((mTrackBox.tl()+mTrackBox.br())/2);
//		}
		found_match = false;
	}

	// TODO: the following state machine should be in base tracker update method

	// little state machine to control the transition between start/kill and track regions
	TrackingStatusEnum newStatus;

	if (found_match)
	{
		if (mLastStatus == TrackingStatus_Start)
		{
			if (mInTrackRegion)
			{
				newStatus = TrackingStatus_Active;
			}
			else if (mInStartRegion)
			{
				newStatus = TrackingStatus_Start;
			}
			else
			{
				newStatus = TrackingStatus_Finished;
			}
		}
		else if (mLastStatus == TrackingStatus_Active)
		{
			if (mInTrackRegion)
			{
				newStatus = TrackingStatus_Active;
			}
			else if (mInStartRegion)
			{
				// if in goes into start/kill region from track region, kill the track.
				newStatus = TrackingStatus_Finished;
			}
			else
			{
				// in this case, the target gets into invalid region. This behavior is not well defined.
				// assume this target is lost then
				// TODO: put the current target into inactive mode. Try to activate it later with other valid targets.
				// but if it doesn't get validated within certain time, terminate it and mark as lost track
				newStatus =  TrackingStatus_Finished;
			}
		}
		else if (mLastStatus == TrackingStatus_Inactive)
		{
			if (mInTrackRegion)
			{
				// found a new match. reinstate active
				newStatus = TrackingStatus_Active;
				mReactivated = true;
				mInactiveFrameCount = 0;
			}
			else if (mInStartRegion)
			{
				// if last status was inactive, that means this target was in active mode previously.
				// so when it enters start/kill region, kill the track
				newStatus = TrackingStatus_Finished;
			}
			else
			{
				newStatus = TrackingStatus_Finished;
			}
		}
	}
	else
	{
		if (mLastStatus == TrackingStatus_Start)
		{
			// if in start region, kill this so that a new target could be created
			newStatus = TrackingStatus_Finished;
		}
		else if (mLastStatus == TrackingStatus_Active)
		{
			// set inactive
			newStatus = TrackingStatus_Inactive;
			mInactiveFrameCount = 0;
		}
		else if (mLastStatus == TrackingStatus_Inactive)
		{
			newStatus = TrackingStatus_Inactive;
			// increase inactive count.
			mInactiveFrameCount++;
			// if count is over a threshold, kill this
			if (mInactiveFrameCount > mcInactiveTimeout)
			{
				newStatus = TrackingStatus_Finished;
			}
		}

	}

	mLastStatus = newStatus;
#endif

#ifdef DEBUG
	mCount++;
#endif

//	return newStatus;
}

TrackingStatusEnum KDETracker::reactivate(std::vector<boost::shared_ptr<VisionDetectorTarget> > &dt_list)
{
	// TODO: implement this
}

void KDETracker::drawTracker(cv::Mat &disp_image)
{
	BaseVisionTracker::drawTrackerHelper(disp_image,cv::Scalar(255,0,255));
	// draw other attributes here (optionally trajectory)
}
#endif

#if 0
DlibCorrelationTracker::DlibCorrelationTracker(boost::shared_ptr<SETTINGS> settings):
		BaseVisionTracker(settings),
		mcBoundingBoxRatio(settings->getFloat("DlibCorrelationTracker/boundingBoxRatio",1.2f,false))
{
	mTrackerColor = DLIBCORRELATION_TRACKBOX_COLOR;
}

void DlibCorrelationTracker::initialize(const cv::Mat &image,
		boost::shared_ptr<VisionDetectorTarget> dt)
{

	cv::Rect init_rect;
	cv::Mat contourMap;
	dt->getTrackerInitializerInfo(init_rect,contourMap);

	int rows = image.rows;
	int cols = image.cols;

	// convert cv::Mat to dlib image
	dlib::cv_image<dlib::rgb_pixel> dlib_image(image);

	// enlarge mTrackBox
	mTrackBox.width = (int)(init_rect.width*mcBoundingBoxRatio);
	mTrackBox.height = (int)(init_rect.height*mcBoundingBoxRatio);
	mTrackBox.x = init_rect.x+(int)(0.5*(1.f-mcBoundingBoxRatio)*(float)init_rect.width);
	mTrackBox.y = init_rect.y+(int)(0.5*(1.f-mcBoundingBoxRatio)*(float)init_rect.height);
	// make sure crop rect is inside image
	mTrackBox = mTrackBox & cv::Rect(0,0,cols-1, rows-1);

	// convert tBoundingRect to dlib rect
	mDlibTracker.start_track(dlib_image,
			dlib::drectangle((double)mTrackBox.tl().x,
								(double)mTrackBox.tl().y,
								(double)mTrackBox.br().x,
								(double)mTrackBox.br().y));

}

bool DlibCorrelationTracker::update(cv::Mat *disp_map)
{
	boost::shared_ptr<VideoBuffer<RGBbImage> > vb = VisionModuleManager::instance()->getVideoBuffer();

	cv::Mat	input_image = vb->currentFrame()->image;
	dlib::cv_image<dlib::rgb_pixel> dlib_image(input_image);
	mConfidence = (float)mDlibTracker.update(dlib_image);
	dlib::drectangle updated_box = mDlibTracker.get_position();

	// update mTrackBox;
	mTrackBox.x = (int)updated_box.left();
	mTrackBox.y = (int)updated_box.top();
	mTrackBox.height = (int)updated_box.height();
	mTrackBox.width = (int)updated_box.width();

	if(mConfidence > 2.f)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void DlibCorrelationTracker::drawTracker(cv::Mat &disp_image)
{
	BaseVisionTracker::drawTracker(disp_image);
	// draw other attributes here (optionally trajectory)
}
#endif

} // of namespace vml

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * VisionTrackerTarget.cpp
 *
 *  Created on: Jan 24, 2015
 *      Author: yyoon
 */

//#include <opencv2/video/tracking.hpp>
//#include <opencv2/imgproc/imgproc.hpp>

#include <modules/vision/VisionVideoModule.hpp>
#include <modules/vision/VisionTrackerTarget.hpp>
#include <modules/vision/VisionDetector.hpp>
#include <modules/vision/VisionDetectorTarget.hpp>
#include <modules/vision/BaseVisionTracker.hpp>
#include <modules/vision/VisionTrackers.hpp>
#include <modules/vision/VisionModuleManager.hpp>
// for debugging
#include <modules/utility/Util.hpp>

//#define DEBUG

namespace vml {

/* class VisionTrackerTarget */
VisionTrackerTarget::VisionTrackerTarget(const int id):
		mID(id),
		mValid(true),
		mActive(true)
{
	mpTargetDetector = VisionModuleManager::instance()->getDetector();
	mpVideoModule = VisionModuleManager::instance()->getVideoModule();
	mpSettings = VisionModuleManager::instance()->getSettings();
}

void VisionTrackerTarget::initialize(boost::shared_ptr<VisionDetectorTarget> dt)
{
//	cv::Rect boundingRect = cv::boundingRect(mCvxHull);	// crop rect to be three times larger than the bounding box
	TrackingMethods tm = BaseVisionTracker::readTrackingMethod(mpSettings);

	cv::Mat input_image = mpVideoModule->currentFrame()->image;

	switch (tm)
	{
		case TrackingMethod_HIST_HSV:
		{
			boost::shared_ptr<HSHistogramTracker> tp;
			tp.reset(new HSHistogramTracker(mID,mpSettings));
			tp->initialize(dt);
			mpTracker = tp;
			break;
		}
		case TrackingMethod_HIST_RGB:
		{
			boost::shared_ptr<RGBHistogramTracker> tp;
			tp.reset(new RGBHistogramTracker(mID, mpSettings));
			tp->initialize(dt);
			mpTracker = tp;
			break;
		}
#if 0
		case TrackingMethod_KDE:
		{
			boost::shared_ptr<KDETracker> tp;
			tp.reset(new KDETracker(mID,mpSettings));
			tp->initialize(dt);
			mpTracker = tp;
			break;
		}
#endif
		case TrackingMethod_HybridHist:
		{
			boost::shared_ptr<HybridHistogramTracker> tp;
			tp.reset(new HybridHistogramTracker(mID,mpSettings));
			tp->initialize(dt);
			mpTracker = tp;
			break;
		}
#if 0
		case TrackingMethod_DLIB_Correlation_Tracking:
		{
			boost::shared_ptr<DlibCorrelationTracker> tp;
			tp.reset(new DlibCorrelationTracker(mpSettings));
			tp->initialize(input_image,dt);
			mpTracker = tp;
			break;
		} // of case Correlation_Tracking:
#endif
		default:
		{
			VML_ERROR(MsgCode::notSupportedErr,"VisionTarget: Tracking method " << tm << " not supported");
		}
	} // of switch (hm)

}

void VisionTrackerTarget::merge(boost::shared_ptr<VisionTrackerTarget> pt)
{
	// merge the two trajectories.
	// Assuming the merged target was a old one, prepend trajectory
	mTrajectory.prepend(pt->mTrajectory);
	// reset this one as active
	mActive = true;
	// TODO: think about how to merge position

	// merge tracker
	// currently do nothing. use the current tracker
	mValid = true;
}

double VisionTrackerTarget::getLastTime(void) const
{
	// return time of the last trajectory
	return mTrajectory.getEndTime();
}

cv::Point VisionTrackerTarget::getPosition() const
{
	return mpTracker->getPosition();
}

bool VisionTrackerTarget::checkSimilarity(boost::shared_ptr<VisionTrackerTarget> pt)
{

	// RESEARCH: define how to check the similarity
	// 1. compare tracker similarity
	// 2. Assuming the targets are in the vicinity and no pose change, compare filter banks?
	return true;
}

TrackingStatusEnum VisionTrackerTarget::updateTargetStatus(cv::Mat &bpImage)
{

	// update the target location and feature based on the chosen tracking method
	TrackingStatusEnum update_status = mpTracker->update(&bpImage);

	// update trajectory and motion model.
	// add trajectory node only when the tracker is in track region
	if (mpTracker->isInTrackRegion() || mpTracker->isInStartRegion())
	{
		mTrajectory.addTrajectoryPoint(mpTracker->getPosition(),mpVideoModule->currentFrame()->time);
	}

	if (update_status == TrackingStatus_Inactive)
	{
		mActive = false;
	}

	return update_status;
}

TrackingStatusEnum VisionTrackerTarget::reactivateTarget(std::vector<boost::shared_ptr<VisionDetectorTarget> > &dt_list)
{
	TrackingStatusEnum reactivate_status = mpTracker->reactivate(dt_list);

	if (reactivate_status == TrackingStatus_Active)
	{
		mActive = true;
	}

	return reactivate_status;
}

void VisionTrackerTarget::drawTarget(cv::Mat &image) const
{
	// TODO: optionally draw trajectories here

	if (mActive)
	{
		if (mpTracker)
		{
			mpTracker->drawTracker(image);
		}
	}
	else
	{
		if (mpTracker)
		{
			// draw search window
			cv::rectangle(image,mpTracker->getCropRect(),CV_RGB(255,255,0));
		}
	}

}

/*
void VisionTrackerTarget::drawUndistortedTarget(cv::Mat &image) const
{
	// TODO: optionally draw trajectories here

	if (mActive)
	{
		if (mpTracker)
		{
			mpTracker->drawUndistortedTracker(image);
		}
	}

}
*/

//void VisionTrackerTarget::updateTargetActivityMap(cv::Mat &activity_map)
//{
//	mpTracker->updateTargetActivityMap(activity_map);
//}


} // of namespace vml

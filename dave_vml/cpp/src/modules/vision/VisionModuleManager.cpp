/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * VisionFactory.cpp
 *
 *  Created on: Feb 8, 2016
 *      Author: yyoon
 */

#include <boost/shared_ptr.hpp>
#include <modules/vision/VisionModuleManager.hpp>
#include <modules/vision/VisionDetector.hpp>
#include <modules/vision/VisionTargetManager.hpp>
#include <modules/vision/VisionVideoModule.hpp>
#include <modules/iot/IOTCommunicator.hpp>

namespace vml {

// global visionModuleManager instance
static VisionModuleManager gInstance;

VisionModuleManager* VisionModuleManager::instance()
{
	return &gInstance;
}

void VisionModuleManager::registerSettings(boost::shared_ptr<SETTINGS> settings)
{
	if (mpSettings)
	{
		mpSettings.reset();
	}

	mpSettings = settings;
}

boost::shared_ptr<SETTINGS> VisionModuleManager::getSettings(void)
{
	return mpSettings;
}

void VisionModuleManager::registerVideoModule(boost::shared_ptr<VisionVideoModule> vb)
{
	if (mpVideoBuffer)
	{
		mpVideoBuffer.reset();
	}

	mpVideoBuffer = vb;
}

boost::shared_ptr<VisionVideoModule> VisionModuleManager::getVideoModule(void)
{
	return mpVideoBuffer;
}

void VisionModuleManager::registerDetector(boost::shared_ptr<VisionDetector> det)
{
	if (mpDetector)
	{
		mpDetector.reset();
	}

	mpDetector = det;
}

boost::shared_ptr<VisionDetector> VisionModuleManager::getDetector(void)
{
	return mpDetector;
}

void VisionModuleManager::registerVisionTargetManager(boost::shared_ptr<VisionTargetManager> tm)
{
	if (mpVisionTargetManager)
	{
		mpVisionTargetManager.reset();
	}

	mpVisionTargetManager = tm;
}

boost::shared_ptr<VisionTargetManager> VisionModuleManager::getVisionTargetManager(void)
{
	return mpVisionTargetManager;
}

void VisionModuleManager::registerIOTCommunicator(boost::shared_ptr<IOT_Communicator> iotc)
{
	if (mpIOTCommunicator)
	{
		mpIOTCommunicator.reset();
	}

	mpIOTCommunicator = iotc;

}

boost::shared_ptr<IOT_Communicator> VisionModuleManager::getIOTCommunicator(void)
{
	return mpIOTCommunicator;
}


} // of namespace vml

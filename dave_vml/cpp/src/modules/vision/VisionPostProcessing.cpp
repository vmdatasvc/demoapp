/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * VisionPostProcessing.cpp
 *
 *  Created on: Jan 29, 2017
 *      Author: yyoon
 */

#include <fstream>

#include <modules/vision/VisionPostProcessing.hpp>
#include <modules/vision/VisionSiteSetup.hpp>
#include <modules/utility/Util.hpp>
#include <modules/utility/DateTime.hpp>
#include <core/Error.hpp>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

// for yaml parsing
//#include <yaml-cpp/yaml.h>

namespace vml {


namespace VisionPostProcDisplay	// for vision post processing display
{
static bool		display_on = false;
// for debugging display
static cv::Mat	floorplanImage;
static cv::Mat	screenShotImage;

#define	DISPLAY_WIDTH	1812
#define DISPLAY_HEIGHT	1400

//#define	DISPLAY_WIDTH	1366
//#define DISPLAY_HEIGHT	768

unsigned int display_width = DISPLAY_WIDTH;
unsigned int display_height = DISPLAY_HEIGHT;

void setDisplayDimension(unsigned int _w, unsigned int _h)
{
	display_width = _w;
	display_height = _h;
}

cv::Mat&	getFloorplanImage()
{
	return floorplanImage;
}

cv::Mat&	getScreenShotImage()
{
	return screenShotImage;
}

bool isDisplayOn()
{
	return display_on;
}

void showDispImage(cv::Mat &disp_image,const char *window_name, int wait_time)
{
	cv::Mat	_dimg;
	cv::Size _dsz(display_width,display_height);
	// resize disp image
	cv::resize(disp_image,_dimg, _dsz);
	cv::imshow(window_name,_dimg);
	cv::waitKey(wait_time);
}
} // of namespace VisionPostProcDisplay

// use global instance of settings
boost::shared_ptr<SETTINGS> gpSettings;

boost::shared_ptr<SETTINGS> getSettings()
{
	return gpSettings;
}

VisionPostProcessor::~VisionPostProcessor()
{

}

void VisionPostProcessor::initialize(std::string date,
		std::string data_path,
		std::string level1_join_setting,
		std::string level2_join_setting,
		std::string level3_join_setting)
{
	vml::FileParts	data_file_parts;

	setDate(date);

	if (VisionPostProcDisplay::isDisplayOn())
	{
		// debug display output initialize
		// read floorplan image for debugging
		data_file_parts.setpath(data_path);
		data_file_parts.appendpath("Floorplan");
		data_file_parts.setfilename("Floorplan");
		data_file_parts.setextention("jpg");

		VisionPostProcDisplay::floorplanImage = cv::imread(data_file_parts.fullpath(),CV_LOAD_IMAGE_COLOR);
		cv::namedWindow("Trajectory Display", cv::WINDOW_AUTOSIZE);

		data_file_parts.setfilename("cam002_screen");
		data_file_parts.setextention("png");
		VisionPostProcDisplay::screenShotImage = cv::imread(data_file_parts.fullpath(),CV_LOAD_IMAGE_COLOR);
		cv::namedWindow("ScreenShot Display", cv::WINDOW_AUTOSIZE);
	}

	// settings initialize
	// settings file should be in data path
	data_file_parts.setpath(data_path);
	data_file_parts.appendpath("Documents");
//	data_file_parts.setfilename("settings");
//	data_file_parts.setextention("xml");

//	gpSettings.reset(new SETTINGS);
//	// parse settings file
//	gpSettings->parseXml(data_file_parts.fullpath(),false);

	// build trajectory tree structure
	data_file_parts.setfilename("CameraSets");
	data_file_parts.setextention("txt");
	setLevel1(data_file_parts.fullpath(), level1_join_setting);

	// read group
	data_file_parts.setfilename("CameraGroups");
	data_file_parts.setextention("txt");
	setLevel2(data_file_parts.fullpath(), level2_join_setting);

	// read store setup
	data_file_parts.setfilename("CameraGroups_EntireStore");
	data_file_parts.setextention("txt");
	setLevel3(data_file_parts.fullpath(), level3_join_setting);

	// read site calibration
	data_file_parts.setfilename("Calibration");
	data_file_parts.setextention("xml");
	std::string calib_filename = data_file_parts.fullpath();
	data_file_parts.setfilename("camera_coverage");
	std::string coverage_filename = data_file_parts.fullpath();
	setCalibration(calib_filename,coverage_filename);

	// read trajectory files
	data_file_parts.setpath(data_path);
	data_file_parts.appendpath("Data");
	data_file_parts.appendpath("00_Base");
	readTrajectories(data_file_parts.path());

	// read furniture lines
	data_file_parts.setpath(data_path);
	data_file_parts.appendpath("Documents");
	data_file_parts.setfilename("Furniture_FullStore");
	data_file_parts.setextention("txt");
	setFurniture(data_file_parts.fullpath());

}

void VisionPostProcessor::setLevel1(std::string data_path, std::string join_setting)
{
	std::ifstream infile(data_path);
	std::string	linestr;
	while (	std::getline(infile,linestr) )
	{
		// parse level1
		std::string	lv1Name;
		int	lv1ID;

		std::size_t del_pos = linestr.find(':');
		std::string tmpstr = linestr.substr(0,del_pos);
		// there could be some other aisle keywords, so define here if needed
		if (tmpstr[0] == 'a' || tmpstr[0] == 'A')
		{
			// this is using 'aisle' keyword
			// skip 5 chars and read the number
			std::string dummystr = tmpstr.substr(5,tmpstr.size());
			lv1ID = atoi(dummystr.c_str());
		}
		lv1Name = tmpstr;

		boost::shared_ptr<TrajectorySet>	pTrLevel1;
		pTrLevel1.reset(new TrajectorySet(lv1Name,lv1ID,1,mpFurnitureLines));
		pTrLevel1->parseJoinSetting(join_setting);

		// parse cameras
		// truncate parentheses
		tmpstr = linestr.substr(del_pos+2,linestr.length()-(del_pos+3));

		std::stringstream cam_stream(tmpstr);
		std::string camstr;
		while (std::getline(cam_stream,camstr,','))
		{
			std::string lv0Name = "cam"+camstr;
			int lv0ID = atoi(camstr.c_str());

			boost::shared_ptr<TrajectorySet>	pLevel0;
			pLevel0.reset(new TrajectorySet(lv0Name,lv0ID,0,mpFurnitureLines));

			pTrLevel1->addSubSet(pLevel0);
			mTrajectorySetLevel0Map.insert(std::pair<int,boost::shared_ptr<TrajectorySet> >(lv0ID,pLevel0));
		} // of while (std::getline(cam_stream,camstr,','))

		mTrajectorySetLevel1Map.insert(std::pair<int,boost::shared_ptr<TrajectorySet> >(pTrLevel1->getID(),pTrLevel1));
	} // of while (std::getline(infile,linestr))
}

void VisionPostProcessor::setLevel2(std::string data_path, std::string join_setting)
{
	std::ifstream infile(data_path);
	std::string	linestr;
	while (	std::getline(infile,linestr) )
	{
		// parse group name
		std::string lv2Name;
		int	lv2ID;
		std::size_t del_pos = linestr.find(':');
		std::string tmpstr = linestr.substr(0,del_pos);
		// there could be some other aisle keywords, so define here if needed
		if (tmpstr[0] == 'g' || tmpstr[0] == 'G')
		{
			// this is using 'group' keyword
			// skip 5 chars and read the number
			std::string dummystr = tmpstr.substr(5,tmpstr.size());
			lv2ID = atoi(dummystr.c_str());
		}
		lv2Name = tmpstr;

		boost::shared_ptr<TrajectorySet>	pTrLevel2;
		pTrLevel2.reset(new TrajectorySet(lv2Name,lv2ID,2,mpFurnitureLines));
		pTrLevel2->parseJoinSetting(join_setting);

		// parse aisles
		// truncate parentheses
		tmpstr = linestr.substr(del_pos+2,linestr.length()-(del_pos+3));

		std::stringstream aisle_stream(tmpstr);
		std::string aislestr;
		while (std::getline(aisle_stream,aislestr,','))
		{
			if (aislestr[0] == 'a' || aislestr[0] == 'A')
			{
				// skip 5 chars and read the number
				std::string dummystr = aislestr.substr(5,aislestr.size());
				int aisleid = atoi(dummystr.c_str());
				auto titr = mTrajectorySetLevel1Map.find(aisleid);
				boost::shared_ptr<TrajectorySet>	paisle;
				if (titr != mTrajectorySetLevel1Map.end())
				{
					// found
					paisle = titr->second;
					pTrLevel2->addSubSet(paisle);
				}
				else
				{
					VML_ERROR(MsgCode::runTimeErr, "Level1: "<<aisleid<<" was not found in " << data_path);
				}
			}
			// else, that keyword is not defined yet.
			else
			{
				VML_ERROR(MsgCode::runTimeErr,"Keyword " << aislestr << " is not defined in parsing " << data_path);
			}

		} // of while (std::getline(cam_stream,camstr,','))

		mTrajectorySetLevel2Map.insert(std::pair<int,boost::shared_ptr<TrajectorySet> >(pTrLevel2->getID(),pTrLevel2));
	}

}
void VisionPostProcessor::setLevel3(std::string data_path, std::string join_setting)
{
	std::ifstream infile(data_path);
	std::string linestr;
	while (	std::getline(infile,linestr) )
	{
		std::size_t del_pos = linestr.find(':');
		std::string tmpstr = linestr.substr(0,del_pos);
		// parse store name
		// there is only one store currently, so id is assigned 0
		mpTrajectoryTreeRoot.reset(new TrajectorySet(tmpstr,0,3,mpFurnitureLines));
		mpTrajectoryTreeRoot->parseJoinSetting(join_setting);

		// parse groups
		// truncate parentheses
		tmpstr = linestr.substr(del_pos+2,linestr.length()-(del_pos+3));

		std::stringstream group_stream(tmpstr);
		std::string groupstr;
		while (std::getline(group_stream,groupstr,','))
		{
			if (groupstr[0] == 'g' || groupstr[0] == 'G')
			{
				// this is using 'group' keyword
				// skip 5 chars and read the number
				std::string dummystr = groupstr.substr(5,tmpstr.size());
				int groupid = atoi(dummystr.c_str());
				auto titr = mTrajectorySetLevel2Map.find(groupid);
				boost::shared_ptr<TrajectorySet>	pgroup;
				if (titr != mTrajectorySetLevel2Map.end())
				{
					// found
					pgroup = titr->second;
					mpTrajectoryTreeRoot->addSubSet(pgroup);
				}
				else
				{
					VML_ERROR(MsgCode::runTimeErr, "Level3: "<<groupid<<" was not found in " << data_path);
				}
			}
			// else, that keyword is not defined yet.
			else
			{
				VML_ERROR(MsgCode::runTimeErr,"Keyword " << groupstr << " is not defined in parsing " << data_path);
			}
		} // of while (std::getline(cam_stream,camstr,','))
	}
}
void VisionPostProcessor::setCalibration(std::string calib_path, std::string coverage_path)
{
	mSiteCalibration.readFromXMLFiles(calib_path,coverage_path);
}

void VisionPostProcessor::setFurniture(std::string data_path)
{
	mpFurnitureLines.reset(new FurnitureLines);
	mpFurnitureLines->readfromFile(data_path);
	// show furniture file lines
//	cv::Mat temp_display;
//	floorplanImage.copyTo(temp_display);
//	mpFurnitureLines->draw(temp_display);
//	__showDispImage(temp_display,FLOORPLANWINDOWNAME);
}

void VisionPostProcessor::readTrajectories(std::string data_path)
{
	assert(!mDateString.empty());

	vml::FileParts	data_file_parts;
	data_file_parts.setpath(data_path);

	for (auto camitr = mTrajectorySetLevel0Map.begin();camitr != mTrajectorySetLevel0Map.end();camitr++)
	{
		std::string filename = camitr->second->getName()+ "_" + mDateString;
		data_file_parts.setfilename(filename);
		data_file_parts.setextention("csv");
		camitr->second->readFromFile(data_file_parts.fullpath());

		// debug display
//		cv::Mat traj_display;
//		screenShotImage.copyTo(traj_display);
//		camitr->second->displayTrajectory(traj_display);
//		__showDispImage(traj_display,SCREENSHOTWINDOWNAME);
	}

}

void VisionPostProcessor::processLevel0(std::string save_path)
{
	cv::Mat temp_display;
	if (VisionPostProcDisplay::isDisplayOn())
	{
		// show all trajectories
		VisionPostProcDisplay::getFloorplanImage().copyTo(temp_display);
	}

	// For each level0 set, convert to world coords and filter small and cart trajectories
	for (auto camitr = mTrajectorySetLevel0Map.begin(); camitr != mTrajectorySetLevel0Map.end(); camitr++)
	{
		camitr->second->filterShortTrajectory(mShotTrajectoryFilterThreshold);

		if (mbSaveIntermediateResult)
		{
			vml::FileParts	save_file_parts;
			save_file_parts.setpath(save_path);
			save_file_parts.appendpath("01_Cleaned");
			save_file_parts.setfilename(camitr->second->getName()+"_"+mDateString);
			save_file_parts.setextention("csv");

			// check if output directory exists, if not create one
			save_file_parts.checkNCreatePath();
			camitr->second->saveToFile(save_file_parts.fullpath());
		}

		// for each base coords, merge cart tracks to shopper tracks.
		camitr->second->filterCartTrajectory(mCartTrajectoryFilterStartTimeWindow,mCartTrajectoryFilterDistThreshold,mCartTrajectoryFilterFrontBackDistThreshold);

		if (mbSaveIntermediateResult)
		{
			vml::FileParts	save_file_parts;
			save_file_parts.setpath(save_path);
			save_file_parts.appendpath("02_Merged");
			save_file_parts.setfilename(camitr->second->getName()+"_"+mDateString);
			save_file_parts.setextention("csv");

			// check if output directory exists, if not create one
			save_file_parts.checkNCreatePath();
			camitr->second->saveToFile(save_file_parts.fullpath());
		}

		// convert to world coords
		// TODO: In order to join the trajectory independent to the floorplan scale, join trajectories in world coordinate and map to floorplan later
		camitr->second->convertToWorld(&mSiteCalibration);

		if (mbSaveIntermediateResult)
		{
			vml::FileParts	save_file_parts;
			save_file_parts.setpath(save_path);
			save_file_parts.appendpath("03_Camera-World");
			save_file_parts.setfilename(camitr->second->getName()+"_"+mDateString);
			save_file_parts.setextention("csv");

			// check if output directory exists, if not create one
			save_file_parts.checkNCreatePath();
			camitr->second->saveToFile(save_file_parts.fullpath());
		}

		// convert to floorplan
		camitr->second->convertWorldToFloorPlan(&mSiteCalibration);

		if (mbSaveIntermediateResult)
		{
			vml::FileParts	save_file_parts;
			save_file_parts.setpath(save_path);
			save_file_parts.appendpath("04_Camera-Floorplan");
			save_file_parts.setfilename(camitr->second->getName()+"_"+mDateString);
			save_file_parts.setextention("csv");

			// check if output directory exists, if not create one
			save_file_parts.checkNCreatePath();
			camitr->second->saveToFile(save_file_parts.fullpath());
		}

		// for display purpose, show floorplan trajectories
		if (VisionPostProcDisplay::isDisplayOn())
		{
			camitr->second->displayTrajectory(temp_display);
		}

	}

	if (VisionPostProcDisplay::isDisplayOn())
	{
		VisionPostProcDisplay::showDispImage(temp_display,FLOORPLANWINDOWNAME,0);
	}

}

void VisionPostProcessor::joinLevel1(std::string save_path)
{
	for (auto level1itr = mTrajectorySetLevel1Map.begin(); level1itr != mTrajectorySetLevel1Map.end(); level1itr++)
	{
		level1itr->second->join();

		if (mbSaveIntermediateResult)
		{
			vml::FileParts	save_file_parts;
			save_file_parts.setpath(save_path);
			save_file_parts.appendpath("06_Merged-Sets");
			save_file_parts.setfilename(level1itr->second->getName()+"_"+mDateString);
			save_file_parts.setextention("csv");

			// check if output directory exists, if not create one
			save_file_parts.checkNCreatePath();
			level1itr->second->saveToFile(save_file_parts.fullpath());
		}

	}

}

void VisionPostProcessor::joinLevel2(std::string save_path)
{
	for (auto level2itr = mTrajectorySetLevel2Map.begin(); level2itr != mTrajectorySetLevel2Map.end(); level2itr++)
	{
		level2itr->second->join();

		if (mbSaveIntermediateResult)
		{
			vml::FileParts	save_file_parts;
			save_file_parts.setpath(save_path);
			save_file_parts.appendpath("08_InitialJoin");
			save_file_parts.setfilename(level2itr->second->getName()+"_"+mDateString);
			save_file_parts.setextention("csv");

			// check if output directory exists, if not create one
			save_file_parts.checkNCreatePath();
			level2itr->second->saveToFile(save_file_parts.fullpath());
		}

	}

}

void VisionPostProcessor::joinLevel3(std::string save_path)
{
	// currently there is only one level3
	mpTrajectoryTreeRoot->join();

	// final output. save unconditionally.
	vml::FileParts	save_file_parts;
	save_file_parts.setpath(save_path);
	save_file_parts.appendpath("10_IncrementalJoin");
	save_file_parts.setfilename(mDateString);
	save_file_parts.setextention("csv");

	// check if output directory exists, if not create one
	save_file_parts.checkNCreatePath();
	mpTrajectoryTreeRoot->saveToFile(save_file_parts.fullpath());
}

void VisionPostProcessor::run(std::string save_path)
{

	// traverse the trajectory tree and join
//	mpTrajectoryTreeRoot->recursiveJoin();

	processLevel0(save_path);
	joinLevel1(save_path);
	joinLevel2(save_path);
	joinLevel3(save_path);
}

void VisionPostProcessor::setDate(double date)
{
	// convert date to string
	DateTime	dt(date);
	mDateString = dt.toString();
}

void VisionPostProcessor::setDisplay(bool turn_on)
{
	VisionPostProcDisplay::display_on = turn_on;
}

void VisionPostProcessor::setDisplayDimension(unsigned int _w, unsigned int _h)
{
	VisionPostProcDisplay::setDisplayDimension(_w,_h);
}

} // of namespace vml

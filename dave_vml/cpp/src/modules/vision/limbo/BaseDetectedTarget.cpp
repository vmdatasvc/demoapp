/****************************************************************************/
 /*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * BaseDetectedTarget.cpp
 *
 *  Updated on: Jan 5, 2016
 *      Author: yyoon
 */

#include <modules/vision/BaseDetectedTarget.hpp>

namespace vml {

BaseDetectedTarget::BaseDetectedTarget(boost::shared_ptr<SETTINGS> settings):
		mcDetectionOverlapWithTrackingTargetThreshold(settings->getFloat("BaseDetectedTarget/detectionOverlapWithTrackingTargetThreshold"))
{
	mpSettings = settings;
}

BaseDetectedTarget::~BaseDetectedTarget()
{

}

bool BaseDetectedTarget::checkTargetActivityOverlap(cv::Mat &target_activity_map)
{
	cv::Mat cropped_at_map;
	target_activity_map(mDetectedBoundingRect).copyTo(cropped_at_map);

	cv::Scalar area = cv::sum(cropped_at_map)/255;
	if ((float)area[0]/(float)(mDetectedBoundingRect.height*mDetectedBoundingRect.width) > mcDetectionOverlapWithTrackingTargetThreshold)
	{
		return true;
	}
	else
	{
		return false;
	}
}

} // of namespace vml

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * BaseVisionTarget.cpp
 *
 *  Created on: Jan 24, 2015
 *      Author: yyoon
 */

//#include <opencv2/video/tracking.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "modules/vision/BaseVisionTarget.hpp"

//#include "dlib/opencv.h"

// for debugging
#include "core/Util.hpp"

//#define DEBUG

namespace vml {

/* class BaseVisionTarget */
BaseVisionTarget::BaseVisionTarget():
		mActive(true)
{

}

void BaseVisionTarget::merge(boost::shared_ptr<BaseVisionTarget> pt)
{
	// merge the two trajectories.
	// Assuming the merged target was a old one, prepend trajectory
#ifdef _WIN32
	mTrajectory.merge(pt->mTrajectory);
#else
	mTrajectory.prepend(pt->mTrajectory);
#endif
	// reset this one as active
	mActive = true;
	// TODO: think about how to merge position
}

double BaseVisionTarget::getLastTime(void) const
{
	// return time of the last trajectory
#ifdef _WIN32
	return mTrajectory.getEndTime();
#else
	return mTrajectory.getLastTime();
#endif
}

void BaseVisionTarget::drawTarget(cv::Mat &image) const
{
	// TODO: optionally draw trajectories here
}

} // of namespace vml

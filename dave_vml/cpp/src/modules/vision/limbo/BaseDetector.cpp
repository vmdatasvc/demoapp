/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * BaseDetector.cpp
 *
 *  Created on: Oct 6, 2015
 *  Updated on: Jan 5, 2016
 *      Author: yyoon
 */

#include <modules/vision/BaseDetector.hpp>

namespace vml {

BaseDetector::BaseDetector(boost::shared_ptr<VisionModuleManager> vf):
	mMaxTargets(0),
	mMinTargetArea(0)
{
	mpVisionModuleManager = vf;
}

BaseDetector::~BaseDetector()
{

}

} // of namespace vml

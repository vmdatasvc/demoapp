/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * VisionVideoModule.cpp
 *
 *  Created on: Mar 22, 2015
 *      Author: yyoon
 */

#include <modules/vision/VisionVideoModule.hpp>
#include <modules/utility/Util.hpp>
#include <modules/vision/PersonShapeModel.hpp>

namespace vml {

VisionFrame::VisionFrame(int rows,int cols):
	image(cv::Mat(rows,cols,CV_8UC3)),
	blurredImage(cv::Mat(rows,cols,CV_8UC3)),
	undistorted_image(cv::Mat(rows,cols,CV_8UC3)),
	foreground_image(cv::Mat(rows,cols,CV_8UC1)),
	time(-1.f)
{}

#ifdef _WIN32
bool VisionFrame::grab(ait::Image32 &img, double curTime)
{
	matPtr timg = VideoBuffer<VisionFrame>::Image32ToMat(img);
	cv::cvtColor(*timg,image,CV_BGRA2RGB);

	time = curTime;

	return true;
}
#else
bool VisionFrame::grab(cv::VideoCapture &vc, double epoch_time)
{
	if (not vc.read(image))
	{
		return false;
	}

	time = gCurrentTime();

	// cv::VideoCapture captures image in BGR moe. convert to RGB
	cv::cvtColor(image,image,CV_BGR2RGB);

	return true;
}
#endif

void VisionFrame::copyTo(VisionFrame &ot)
{
	image.copyTo(ot.image);
	blurredImage.copyTo(ot.blurredImage);
	foreground_image.copyTo(ot.foreground_image);
	ot.time = time;
}

VisionVideoModule::VisionVideoModule():
		mBlurSigma(-1.f),
		mStartRegionOffset(-1),
		mCenterPersonRadiusInPixel(-1.f),
		mShadowDetect(true)
{
}

VisionVideoModule::~VisionVideoModule()
{
	if (mpCameraModel)
	{
		mpCameraModel.reset();
	}
}

#ifdef _WIN32
void VisionVideoModule::initialize(boost::shared_ptr<SETTINGS> settings,ait::Image32& img,int buffer_size)
#else
void VisionVideoModule::initialize(boost::shared_ptr<SETTINGS> settings, std::string video_file_name)
#endif
{
	std::cout << "Initializing video module with video feed at " << video_file_name << std::endl;

	mVideoURL = video_file_name;

	mpSettings.swap(settings);

    // call superclass initialize
#ifdef _WIN32
	VideoBuffer<VisionFrame>::initialize(mpSettings,img,buffer_size);
#else
	VideoBuffer<VisionFrame>::initialize(mpSettings,video_file_name);
#endif

	if (!(this->isInitialized()))
	{
		// if video is not initialized, no need to proceed further.
		return;
	}

	// create camera model
	std::cout << "Creating CameraModleOpenCV with (" << mRows << "," << mCols << ") dimension" << std::endl;
	mpCameraModel.reset(new CameraModelOpenCV(mRows,mCols,mpSettings));

	mStartRegionOffset = mpSettings->getFloat("VisionVideoModule/startRegionOffset",1.5f,false);    // read tracking regions

    std::vector<double> temp_polygon;
#ifdef _WIN32
    std::list<double> temp_list;
    temp_list = mpSettings->getFloatList("TrackPolygon","",false);
    std::list<double>::iterator ti;
    for (ti=temp_list.begin();ti!=temp_list.end();ti++)
    {
            temp_polygon.push_back(*ti);
    }
#else
    temp_polygon = mpSettings->getDoubleVector("TrackPolygon","",false);
#endif

    VML_ASSERT(temp_polygon.size()%2==0);	// number of coords should be even

    const double frows = (double)mRows;
    const double fcols = (double)mCols;

    if (!temp_polygon.empty())
    {
        std::vector<double>::iterator titr;
        // check if average coords are less than 1
        double avg = 0.f;
        for (titr=temp_polygon.begin();titr!=temp_polygon.end();)
        {
                avg += *titr++;
        }
        avg /= (double)temp_polygon.size();

        if (avg < 1.f)
        {
        	// this polygon is proportional to the image dimension. Do proper conversion.
        	// polygon is not proportional.
            for (titr=temp_polygon.begin();titr!=temp_polygon.end();)
            {
    			cv::Point apoint;
    			apoint.x = (int)(fcols* *titr++);
    			apoint.y = (int)(frows* *titr++);
    			mTrackingRegion.push_back(apoint);
            }
        }
        else
        {
        	// polygon is not proportional.
            for (titr=temp_polygon.begin();titr!=temp_polygon.end();)
            {
    			cv::Point apoint;
    			apoint.x = (int)(*titr++);
    			apoint.y = (int)(*titr++);
    			mTrackingRegion.push_back(apoint);
            }
        }
    }

    // read no start region polygons.
    // check if no start zone is provided
    std::string nszstr = mpSettings->getString("NoStartZone/numPolygons","",false);
    if (!nszstr.empty())
    {
        std::vector<double>::iterator titr;
        double avg = 0.f;
        int npolygon = mpSettings->getInt("NoStartZone/numPolygons");
        mNoStartRegion.resize(npolygon);
        for (int i=0;i<npolygon;i++)
        {
                temp_polygon.clear();
                std::stringstream varname;
                varname << "NoStartZone/P" << i;
    #ifdef _WIN32
                std::list<double> temp_list;
                temp_list = mpSettings->getDoubleList(varname.str().c_str(),"",false);
                std::list<double>::iterator ti;
                for (ti=temp_list.begin();ti!=temp_list.end();ti++)
                {
                        temp_polygon.push_back(*ti);
                }
    #else
                temp_polygon = mpSettings->getDoubleVector(varname.str().c_str(),"",false);
    #endif
                VML_ASSERT(temp_polygon.size()!=0);
                VML_ASSERT(temp_polygon.size()%2==0);	// number of coords should be even
                if (avg < 1.f)
                {
                    for (titr=temp_polygon.begin();titr!=temp_polygon.end();)
                    {
            			cv::Point apoint;
            			apoint.x = (int)(fcols* *titr++);
            			apoint.y = (int)(frows* *titr++);
            			mNoStartRegion[i].push_back(apoint);
                    }
                }
                else
                {
                    for (titr=temp_polygon.begin();titr!=temp_polygon.end();)
                    {
						cv::Point apoint;
						apoint.x = (int)(*titr++);
						apoint.y = (int)(*titr++);
						mNoStartRegion[i].push_back(apoint);
                    }
                }
        }
    }

	if (!(this->isValid()))
	{
		// if this module is not valid, no need to proceed further
		return;
	}

	mWindowRect = cv::Rect(cv::Rect(0,0,mCols,mRows));

    // Start/Kill region definition needs camera model so it is initialized here
    // define start/kill regions as one human length from the track polygon
    mStartKillRegion.resize(mTrackingRegion.size());
    for (unsigned int i = 0; i<mTrackingRegion.size(); i++)
    {
    	mStartKillRegion[i] = enforceWindowRect(mpCameraModel->perspectiveCylinderAxisPointProjection(mTrackingRegion[i],-mStartRegionOffset,mpSettings->getFloat("Camera/personHeight",1.7f,false)));
    }

    // get person shape radius in pixel units for defining other image-resolution-dependent thresholds
    // using person shape class here is problematic cause the order of video module registration doesn't work.
    // manually calculate floor radius of the person shape radius
	std::vector<cv::Point> floor_circle;
	cv::Point footPosition(mCols/2,mRows/2);
	float personRadius = mpSettings->getFloat("Camera/personRadius",0.4f,false);
	mpCameraModel->perspectiveCircleProjection(footPosition,personRadius,floor_circle);
	this->enforceWindowRect(floor_circle);
	double maxRadius = -1.f;
	for (unsigned int i=0;i<floor_circle.size();i++)
	{
		cv::Point dp = floor_circle[i] - footPosition;
		double radius = cv::norm(dp);
		if ( radius > maxRadius)
		{
			maxRadius = radius;
		}
	}
	mCenterPersonRadiusInPixel = (float)maxRadius;

	mBlurSigma = mCenterPersonRadiusInPixel*mpSettings->getFloat("VisionVideoModule/blurSigmaRatioToPersonRadiusinPixel",0.1f,false);

	// background subtraction training
	mShadowDetect = mpSettings->getBool("VisionVideoModule/BackgroundSubtraction/shadowDetection [bool]",true,false);
	int hist_length = mpSettings->getInt("VisionVideoModule/BackgroundSubtraction/historyLength",500,false);
	float var_threshold = mpSettings->getFloat("VisionVideoModule/BackgroundSubtraction/varThreshold",8.f,false);

#ifdef _WIN32
	mFramesToTrain = mpSettings->getInt("VisionVideoModule/BackgroundSubtraction/numTrainingFrame",100,false);
#endif

	// create background subtractor
#if CV_MAJOR_VERSION == 2
	mpBgSubtractor.reset(new cv::BackgroundSubtractorMOG2(hist_length,var_threshold,mShadowDetect));
#elif CV_MAJOR_VERSION == 3
	mpBgSubtractor.release();
	mpBgSubtractor = cv::createBackgroundSubtractorMOG2(hist_length,var_threshold,mShadowDetect);
#endif

#ifdef BGSUBSAMPLE
	if (mRows > 240)
	{
		mRowsSub = mRows/2;
		mColsSub = mCols/2;
	}
	else
	{
		mRowsSub = mRows;
		mColsSub = mCols;
	}
#endif

#ifndef _WIN32
	// train background by grabbing images.
	int n_train_frames = mpSettings->getInt("VisionVideoModule/BackgroundSubtraction/numTrainingFrame",40,false);
#ifdef BGSUBSAMPLE
	cv::Mat temp_fg;
#endif

//	cv::Mat	temp_image;
	cv::Mat temp_subimage;
	cv::Mat temp_foreground;
	for (int i=0;i<n_train_frames;i++)
	{
#ifdef BGSUBSAMPLE
		this->grabFrame();
		this->currentFrame()->image.copyTo(temp_image);
		// pyramid down
		cv::pyrDown(temp_image,temp_subimage);

#if CV_MAJOR_VERSION == 2
		mpBgSubtractor->operator()(temp_subimage,temp_fg);
#elif CV_MAJOR_VERSION == 3
		mpBgSubtractor->apply(temp_subimage,temp_fg);
#endif
#else
		cv::Mat	temp_image;
		this->grabFrame();	// grab frame does background subtraction, so no need for further background subtraction here.
#endif
	} // of for

	// rewind image grabber if possible
	this->rewind(0);
#endif // of ifndef _WIN32
}

#ifdef _WIN32
// Interface to check training complete
// Expose to trackingSytem which in turn exposes to eventChannel
bool VisionVideoModule::isTraining(){
	if(	mFramesTrained<mFramesToTrain)
	{
		return true;
	}
	else
	{
		return false;
	}
}

#endif

void VisionVideoModule::allocateTempFrames()
{
	mpGrabFrame.reset(new VisionFrame(mRows,mCols));
	mpBackSubFrame.reset(new VisionFrame(mRows,mCols));
	mpProcessFrame.reset(new VisionFrame(mRows,mCols));

	// push one frame to buffer
	boost::shared_ptr<VisionFrame> tFramePtr;
	tFramePtr.reset(new VisionFrame(mRows,mCols));
	// push new frame to the buffer
	mFrameBuffer.push_front(tFramePtr);
//	mCurrentFrameNumber++;
}


#ifdef _WIN32
bool VisionVideoModule::grabFrame2(ait::Image32& img, double curFTime)
#else
bool VisionVideoModule::grabFrame(void)
#endif
{

	if (!isValid())
	{
		return false;
	}

#ifdef _WIN32
	bool grab_success = VideoBuffer<VisionFrame>::grabFrame2(img,curFTime);
#else
	bool grab_success = VideoBuffer<VisionFrame>::grabFrame();
#endif

	if (!grab_success)
	{
		return grab_success;
	}

	boost::shared_ptr<VisionFrame> cur_frame = this->currentFrame();

	// blur current image here
	if (mBlurSigma > 0.5f)
	{
		cv::GaussianBlur(cur_frame->image,cur_frame->blurredImage,cv::Size(0,0),mBlurSigma,mBlurSigma);
	}

	// background subtraction
#ifdef BGSUBSAMPLE

	cv::Mat inputimagesub;
	cv::Mat temp_foreground;
	cv::pyrDown(inputimage,inputimagesub);

#if CV_MAJOR_VERSION == 2
	mpBgSubtractor->operator()(inputimagesub,temp_foreground);
#elif CV_MAJOR_VERSION == 3
	mpBgSubtractor->apply(inputimagesub,temp_foreground);
#endif

	// upsample foreground
	cv::pyrUp(temp_foreground,mForeground);

#else
	// run background subtraction
#if CV_MAJOR_VERSION == 2
	mpBgSubtractor->operator()(cur_frame->blurredImage,cur_frame->foreground_image);
#elif CV_MAJOR_VERSION == 3
	mpBgSubtractor->apply(cur_frame->blurredImage,cur_frame->foreground_image);
#endif
#endif

	// remove shadows if shadow detect was enabled
	if (mShadowDetect)
	{
//		mForeground.setZeroMinThreshold(128); // set any pixel less than 128 (shadow) as zero
		uchar *fg_ptr = (uchar*)cur_frame->foreground_image.data;
		for (int i=0;i<mRows*mCols;i++,fg_ptr++)
		{
			if (*fg_ptr < 128)
			{
				*fg_ptr = 0;
			}
		}
	}

#ifdef _WIN32
	if (mFramesTrained<mFramesToTrain)
	{
		mFramesTrained++;
	}
#endif

	return true;
}

bool VisionVideoModule::grabAndBackgroundSubtractToGrabFrame(void)
{
	if (!isValid())
	{
		return false;
	}

	mpGrabFrame = VideoBuffer<VisionFrame>::grabAndReturnFrame();
	if (!mpGrabFrame)
	{
		return false;
	}

	// blur current image here
	if (mBlurSigma > 0.5f)
	{
		cv::GaussianBlur(mpGrabFrame->image,mpGrabFrame->blurredImage,cv::Size(0,0),mBlurSigma,mBlurSigma);
	}

	// background subtraction
#ifdef BGSUBSAMPLE

	cv::Mat inputimagesub;
	cv::Mat temp_foreground;
	cv::pyrDown(inputimage,inputimagesub);

#if CV_MAJOR_VERSION == 2
	mpBgSubtractor->operator()(inputimagesub,temp_foreground);
#elif CV_MAJOR_VERSION == 3
	mpBgSubtractor->apply(inputimagesub,temp_foreground);
#endif

	// upsample foreground
	cv::pyrUp(temp_foreground,mForeground);

#else
	// run background subtraction
#if CV_MAJOR_VERSION == 2
	mpBgSubtractor->operator()(inputimage,mForeground);
#elif CV_MAJOR_VERSION == 3
	mpBgSubtractor->apply(mpGrabFrame->blurredImage,mpGrabFrame->foreground_image);
#endif
#endif

	// remove shadows if shadow detect was enabled
	if (mShadowDetect)
	{
		uchar *fg_ptr = (uchar*)mpGrabFrame->foreground_image.data;
		for (int i=0;i<mRows*mCols;i++,fg_ptr++)
		{
			if (*fg_ptr < 128)
			{
				*fg_ptr = 0;
			}
		}
	}

	return true;
}

bool VisionVideoModule::grabAndBackgroundSubtractInGrabFrame(void)
{
	if (!isValid())
	{
		return false;
	}

	if (!VideoBuffer<VisionFrame>::grabInFrame(mpGrabFrame))
	{
		return false;
	}

	// blur current image here
	if (mBlurSigma > 0.5f)
	{
		cv::GaussianBlur(mpGrabFrame->image,mpGrabFrame->blurredImage,cv::Size(0,0),mBlurSigma,mBlurSigma);
	}

	// background subtraction
#ifdef BGSUBSAMPLE

	cv::Mat inputimagesub;
	cv::Mat temp_foreground;
	cv::pyrDown(inputimage,inputimagesub);

#if CV_MAJOR_VERSION == 2
	mpBgSubtractor->operator()(inputimagesub,temp_foreground);
#elif CV_MAJOR_VERSION == 3
	mpBgSubtractor->apply(inputimagesub,temp_foreground);
#endif

	// upsample foreground
	cv::pyrUp(temp_foreground,mForeground);

#else
	// run background subtraction
#if CV_MAJOR_VERSION == 2
	mpBgSubtractor->operator()(inputimage,mForeground);
#elif CV_MAJOR_VERSION == 3
	mpBgSubtractor->apply(mpGrabFrame->blurredImage,mpGrabFrame->foreground_image);
#endif
#endif

	// remove shadows if shadow detect was enabled
	if (mShadowDetect)
	{
		uchar *fg_ptr = (uchar*)mpGrabFrame->foreground_image.data;
		for (int i=0;i<mRows*mCols;i++,fg_ptr++)
		{
			if (*fg_ptr < 128)
			{
				*fg_ptr = 0;
			}
		}
	}

	return true;
}

void VisionVideoModule::setGrabFrameToCurrent(void)
{
	// insert the frame into the circular buffer
	this->setCurrentFrame(mpGrabFrame);
	mpGrabFrame.reset();
}

void VisionVideoModule::copyGrabFrameToCurrent(void)
{
	mpGrabFrame->copyTo(*currentFrame());
}

bool VisionVideoModule::grabToGrabFrame(void)
{
	if (!isValid())
	{
		return false;
	}

	mpGrabFrame = VideoBuffer<VisionFrame>::grabAndReturnFrame();

	if (!mpGrabFrame)
	{
		return false;
	}

	// blur current image here
	if (mBlurSigma > 0.5f)
	{
		cv::GaussianBlur(mpGrabFrame->image,mpGrabFrame->blurredImage,cv::Size(0,0),mBlurSigma,mBlurSigma);
	}

	return true;
}

bool VisionVideoModule::grabInGrabFrame(void)
{
	if (!isValid())
	{
		return false;
	}

	if (!VideoBuffer<VisionFrame>::grabInFrame(mpGrabFrame))
	{
		return false;
	}

	// blur current image here
	if (mBlurSigma > 0.5f)
	{
		cv::GaussianBlur(mpGrabFrame->image,mpGrabFrame->blurredImage,cv::Size(0,0),mBlurSigma,mBlurSigma);
	}

	return true;
}

void VisionVideoModule::backgroundSubtractionInBackSubFrame(void)
{
	// background subtraction
#ifdef BGSUBSAMPLE

	cv::Mat inputimagesub;
	cv::Mat temp_foreground;
	cv::pyrDown(inputimage,inputimagesub);

#if CV_MAJOR_VERSION == 2
	mpBgSubtractor->operator()(inputimagesub,temp_foreground);
#elif CV_MAJOR_VERSION == 3
	mpBgSubtractor->apply(inputimagesub,temp_foreground);
#endif

	// upsample foreground
	cv::pyrUp(temp_foreground,mForeground);

#else
	// run background subtraction
#if CV_MAJOR_VERSION == 2
	mpBgSubtractor->operator()(inputimage,mForeground);
#elif CV_MAJOR_VERSION == 3
	mpBgSubtractor->apply(mpBackSubFrame->blurredImage,mpBackSubFrame->foreground_image);
#endif
#endif

	// remove shadows if shadow detect was enabled
	if (mShadowDetect)
	{
		uchar *fg_ptr = (uchar*)mpBackSubFrame->foreground_image.data;
		for (int i=0;i<mRows*mCols;i++,fg_ptr++)
		{
			if (*fg_ptr < 128)
			{
				*fg_ptr = 0;
			}
		}
	}

}

void VisionVideoModule::setGrabFrameToBackSubFrame(void)
{
	mpBackSubFrame = mpGrabFrame;
}

void VisionVideoModule::copyGrabFrameToBackSubFrame(void)
{
	mpGrabFrame->copyTo(*mpBackSubFrame);
}

void VisionVideoModule::setBackSubFrameToCurrent(void)
{
	// insert the frame into the circular buffer
	this->setCurrentFrame(mpBackSubFrame);
}

void VisionVideoModule::copyBackSubFrameToCurrent(void)
{
	mpBackSubFrame->copyTo(*currentFrame());
}

void VisionVideoModule::copyCurrentUndistortedImageTo(cv::Mat &undist_img)
{
	undistortImage(this->currentFrame()->image,undist_img);
	/*
	if (mpCameraModel)
	{
		mpCameraModel->undistortImage(cur_frame->image,cur_frame->undistorted_image);
	}
	else
	{
		VML_WARN(MsgCode::paramErr,"Camera model was not enabled for this video buffer. Check it for undistorted");
		cur_frame->image.copyTo(cur_frame->undistorted_image);	// copy original image
	}
	*/
}

void VisionVideoModule::undistortImage(cv::Mat &input_img,cv::Mat &undistorted_image)
{
	if (mpCameraModel)
	{
		mpCameraModel->undistortImage(input_img,undistorted_image);
	}
	else
	{
		VML_WARN(MsgCode::paramErr,"Camera model was not enabled for this video buffer. Check it for undistorted");
		input_img.copyTo(undistorted_image);
	}
}

/*
void VisionVideoModule::estimateUndistortedPersonShapeConvexHull(int x, int y, std::vector<cv::Point> &cvxhull)
{
        // clear person shape
        cvxhull.clear();
        // Goal is to construct convex hull that's enclosing the given x,y coordinate as the foot location of a tracking subject
        // 1. Backproject x,y coord to world space
        // undistort the x,y
        // backproject to find the (x,y,z) coords in world
        float u = x;	// output is already normalized
        float v = y;

        // this is the top of the person
//	float Z = mCameraHeight-mPersonHeight;
        float Z = mCameraHeight;
        float X = Z*u;
        float Y = Z*v;

#if 0
        // cv::projectPoints crashes. don't know why. Taking alternative way

        // 2. construct human subject enclosure
        // use cylinder mesh
        std::vector<cv::Point3f>	cylinder3D;
        float	theta = 0.f;
        for (int i=0;i<(int)(2*M_PI/cCircleInc);i++,theta+=cCircleInc)
        {
                cv::Point3f tp;
                tp.x = X+mPersonRadius*cos(theta);
                tp.y = Y+mPersonRadius*sin(theta);
                tp.z = Z;
                cylinder3D.push_back(tp);
                tp.z = Z-mPersonHeight;
                cylinder3D.push_back(tp);
        }

        // 3. project enclosure to image space
        std::vector<cv::Point2f> cylinder2D(cylinder3D.size());
        cv::projectPoints(cylinder3D,cylinder2D,cv::Vec<float,3>(0,0,0),cv::Vec<float,3>(0,0,0),mK,mD);
#endif

        cvxhull.clear();
        float theta = 0.f;
        for (int i=0;i<(int)(2*M_PI/mCircleInc);i++,theta+=mCircleInc)
        {
                cv::Point tp;
//		float z = Z+mPersonHeight;
                float z = Z;
                float xprime = (X+mPersonRadius*cos(theta))/z;
                float yprime = (Y+mPersonRadius*sin(theta))/z;

                tp.x = (int)(fx*xprime+cx);
                tp.y = (int)(fx*yprime+cy);

                cvxhull.push_back(tp);

                z = Z;

                xprime = (X+mPersonRadius*cos(theta))/z;
                yprime = (Y+mPersonRadius*sin(theta))/z;
                r = xprime*xprime+yprime*yprime;
                r2 = r*r;
                r4 = r2*r2;
                r6 = r2*r4;

                dist_factor = (1.f+k1*r2+k2*r4+k3*r6);
                xdprime = xprime*dist_factor;
                ydprime = yprime*dist_factor;

                tp.x = (int)(fx*xdprime+cx);
                tp.y = (int)(fx*ydprime+cy);
                cvxhull.push_back(tp);
        }

        // convert projected enclosure to convex hull
        cv::convexHull(cvxhull,cvxhull);
}
*/

bool VisionVideoModule::insideTrackRegion(cv::Point pos)
{
	cv::Point2f posf(pos);

	if (cv::pointPolygonTest(mTrackingRegion,posf,false) > 0.f)
	{
			return true;
	}
	else
	{
			return false;
	}
}

bool VisionVideoModule::insideStartKillRegion(cv::Point pos)
{
#if 0
	bool inside = false;
	cv::Point2f	posf(pos);

	std::vector<std::vector<cv::Point> >::iterator poly_itr = mStartKillRegion.begin();
	for (;poly_itr != mStartKillRegion.end();poly_itr++)
	{
			if (cv::pointPolygonTest(*poly_itr,posf,false) > 0.f)
			{
					inside = true;
			}
	}

	return inside;
#endif
	cv::Point2f posf(pos);
	if (cv::pointPolygonTest(mStartKillRegion,posf,false) > 0.f)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool VisionVideoModule::insideNoStartRegion(cv::Point pos)
{
	cv::Point2f posf(pos);
	bool insideNoStart = false;
	for (unsigned int i=0;i<mNoStartRegion.size();i++)
	{
		if (cv::pointPolygonTest(mNoStartRegion[i],posf,false) > 0.f)
		{
			insideNoStart = true;
		}
	}

	return insideNoStart;
}

void VisionVideoModule::drawTrackRegions(cv::Mat &img)
{
	// draw Track Region
	cv::drawContours(img,std::vector<std::vector<cv::Point> >(1,mTrackingRegion),0,CV_RGB(0,0,255));
	// draw Start regions
//	cv::drawContours(img,mStartKillRegion,-1,CV_RGB(255,0,0));
	cv::drawContours(img,std::vector<std::vector<cv::Point> >(1,mStartKillRegion),0,CV_RGB(255,0,0));
}

void VisionVideoModule::drawNoStartRegions(cv::Mat &img)
{
	// draw No Start Regions
	cv::drawContours(img,mNoStartRegion,-1,CV_RGB(0,255,0));
}

void VisionVideoModule::enforceWindowRect(std::vector<cv::Point> &pts)
{
	for (unsigned int i=0;i<pts.size();i++)
	{
		if (!mWindowRect.contains(pts[i]))
		{
			pts[i] = enforcePointInRect(pts[i],mWindowRect);
		}
	}
}

void VisionVideoModule::enforceWindowRect(cv::Point &pt)
{
	if (!mWindowRect.contains(pt))
	{
		pt = enforcePointInRect(pt,mWindowRect);
	}
}

cv::Point VisionVideoModule::enforceWindowRect(const cv::Point pt)
{
	if (!mWindowRect.contains(pt))
	{
		return enforcePointInRect(pt,mWindowRect);
	}
	else
	{
		return pt;
	}

}

float VisionVideoModule::getFrameRate()
{
	return (float)mCurrentFrameNumber/(float)(currentFrame()->time - mEpochTime);
}


} // of namespace vml

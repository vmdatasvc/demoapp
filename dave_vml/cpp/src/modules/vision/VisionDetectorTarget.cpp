/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * VisionDetectorTarget.cpp
 *
 *  Created on: Jan 24, 2015
 *      Author: yyoon
 */

#include <opencv2/imgproc.hpp>
#include <modules/vision/VisionVideoModule.hpp>
#include <modules/vision/VisionDetectorTarget.hpp>
#include <modules/vision/VisionModuleManager.hpp>
//#include <core/Video.hpp>
#include <modules/utility/MathUtil.hpp>

// for debugging
#include <modules/utility/Util.hpp>
#include <modules/vision/VisionDetector.hpp>

namespace vml {

/* class VisualTrackingTarget */
VisionDetectorTarget::VisionDetectorTarget(std::vector<cv::Point> &contour,
		boost::shared_ptr<SETTINGS> settings):
//		mNew(false),
		mValid(true),
		mNeedMerge(true),
		mInTrackRegion(false),
		mInStartRegion(false),
		mCvxHullArea(0),
		mContoursArea(0),
		mLikelihood(-1.f)
{
	if (contour.size() < 3)
	{
		mValid = false;
		return;
	}

	mpTargetDetector = VisionModuleManager::instance()->getDetector();
	mpVideoModule = VisionModuleManager::instance()->getVideoModule();
	mpSettings = settings;

	mAreaThresholdHighRatio = mpSettings->getFloat("VisionDetector/highAreaRatioToHumanShape",1.5f,false);

	cv::convexHull(contour,mCvxHull);
	mCvxHullArea = abs((int)cv::contourArea(mCvxHull));

	mContoursArea = abs((int)cv::contourArea(contour));

	// NOTE: currently the target position is the center of the detected blob
	// TODO: but the position of the target (human shape) should be the location of the shopper feet.
	//  So, we need to fit the expected person shape (ellipsoid) and find the location corresponding
	//  to the feet, and use that as the position of the target.
	cv::Moments	mm = cv::moments(mCvxHull);
	mCenter.x = mm.m10/mm.m00;
	mCenter.y = mm.m01/mm.m00;

	// update person shape estimation info
//	updateEstimatedPersonShape();
	mPersonShape.initialize(settings,mpVideoModule);
	mPersonShape.estimatePersonShape(mCenter);
	if (!mPersonShape.updateShapeInfo())
	{
		mValid = false;
		return;
	}

	mPosition = mPersonShape.getFootPosition();

	// NOTE: The following filter cases only able to find isolated human blob. It's gonna miss when human blob
	//       is joined with other blobs (other human or carts typically).
	//       These special cases cannot be handled simply by analyzing contours.  We need more sophisticated human
	//       detector for this

	// filter out the following blobs
	// 1. if the area of the blob is too large (1.5 * estimated person area)
	// 2. If the blob is not too small, but if the major axis of the blob doesn't coincide with the estimated shape

	// Case 1.
	if (mCvxHullArea > (int)((float)mPersonShape.getArea()*mAreaThresholdHighRatio))
	{
		mValid = false;
		return;
	}

	// Case 2.
	if (mCvxHullArea > mPersonShape.getArea()*mpSettings->getFloat("VisionDetector/lowAreaRatioToHumanShape",0.7f,false))
	{
		// calculate major and minor axes of human shape and blob
		cv::Point2f h_major,h_minor,b_major,b_minor;
		float	h_lmaj,h_lmin,b_lmaj,b_lmin;

		h_major = mPersonShape.getMajorAxis();
		h_minor = mPersonShape.getMinorAxis();
		h_lmaj = mPersonShape.getMajorAxisLength();
		h_lmin = mPersonShape.getMinorAxisLength();

		if (!calcContourAxes(contour,b_major,b_minor,b_lmaj,b_lmin))
		{
			mValid = false;
			return;
		}

		if ((h_lmin/h_lmaj < 0.7)&&(b_lmin/b_lmaj < 0.7)&&(h_major.x*b_major.x+h_major.y*b_major.y < 0.8))
		{
			mValid = false;
			return;
		}
	}

	// if the blob is smaller than minAreaRatioToHumanShape, leave it as candidate so that it can be merged to other blobs

	evalRegionStatus();

//	// blobs are valid only when in track/start region
	// TODO: blobs can be close to the start/track region, but not entirely included.
	//       Evaluate blob position as the fitted person shape foot location, so that foot location inside start/track regions can be considered.
	if (!mInTrackRegion && !mInStartRegion)
	{
		mValid = false;
		return;
	}

//	mContours.push_back(boost::shared_ptr<std::vector<cv::Point> >(new std::vector<cv::Point>));
//	*(mContours[0]) = contour;
	mContours.push_back(contour);

	// calculate features (person shape and bounding rect) even when the blob is outside track regions
	// because those might be used for merging later.
	mDetectedBoundingRect = cv::boundingRect(mCvxHull);

	mValid = true;
	if (mCvxHullArea > mPersonShape.getArea())
	{
		mNeedMerge = false;
	}

}

VisionDetectorTarget::~VisionDetectorTarget()
{

}

void VisionDetectorTarget::evalRegionStatus()
{
	// check if position is inside track region
	mInTrackRegion = mpVideoModule->insideTrackRegion(mCenter);
	if (mInTrackRegion)
	{
		// enforcing track and start region disjoint
		mInStartRegion = false;
	}
	else
	{
		mInStartRegion = mpVideoModule->insideStartKillRegion(mCenter);
		if (mInStartRegion)
		{
			if (mpVideoModule->insideNoStartRegion(mCenter))
			{
				mInStartRegion = false;
			}
		}
	}
}

float VisionDetectorTarget::EuclideanDistance(VisionDetectorTarget &ot) const
{
	return(EuclideanDistance(ot.mCenter));
}

float VisionDetectorTarget::EuclideanDistance(cv::Point pos) const
{
	int dx = this->mCenter.x - pos.x;
	int dy = this->mCenter.y - pos.y;
	return(sqrt((float)(dx*dx+dy*dy)));
}

float VisionDetectorTarget::MahalanobisDistance(VisionDetectorTarget &ot) const
{
	return (mPersonShape.MahalanobisDistance(ot.mCenter));
}

float VisionDetectorTarget::MahalanobisDistance(cv::Point p) const
{
	return (mPersonShape.MahalanobisDistance(p));
}

void VisionDetectorTarget::checkBlobMergeCriteriaAndMerge(boost::shared_ptr<VisionDetectorTarget> other)
{
	std::vector<cv::Point>	temp_contours = mCvxHull;
	// concatenate the candidate contours into convex hulls
	temp_contours.insert(temp_contours.end(),other->mCvxHull.begin(),other->mCvxHull.end());
	std::vector<cv::Point> temp_cvxhull;
	cv::convexHull(temp_contours,temp_cvxhull);
	double temp_area = cv::contourArea(temp_cvxhull);
	// need to recalculate the distances because it might have been changed by merge
//	cv::RotatedRect minenc = cv::minAreaRect(temp_cvxhull);
//	float blob_length = std::max(minenc.size.height,minenc.size.width);
//	if ((temp_area < mEstimatedPersonArea) && (blob_length < mEstimatedPersonLength))

	// calculate major and minor axes of human shape and blob
	cv::Point2f h_major,h_minor,b_major,b_minor;
	float	h_lmaj,h_lmin,b_lmaj,b_lmin;

	h_major = mPersonShape.getMajorAxis();
	h_minor = mPersonShape.getMinorAxis();
	h_lmaj = mPersonShape.getMajorAxisLength();
	h_lmin = mPersonShape.getMinorAxisLength();

	if (!calcContourAxes(temp_cvxhull,b_major,b_minor,b_lmaj,b_lmin))
	{
		return;
	}

#ifdef VISIONDETECTOR_DEBUG_SHOW
	if (temp_area < (int)((float)mPersonShape.getArea()*mAreaThresholdHighRatio))
	{
		if ((h_lmin/h_lmaj > 0.7)||(b_lmin/b_lmaj > 0.7))
		{
			// display debug in detector debug window
//			cv::drawContours(mpTargetDetector->mDisplayImage,std::vector<std::vector<cv::Point> >(1,mPersonShape.get),0,CV_RGB(255,0,255));
			mPersonShape.draw(mpTargetDetector->mDisplayImage,CV_RGB(255,0,255));
			cv::drawContours(mpTargetDetector->mDisplayImage,std::vector<std::vector<cv::Point> >(1,temp_cvxhull),0,CV_RGB(0,255,0));
			mpTargetDetector->debugShow(mpTargetDetector->mDisplayImage);
		}

	}
#endif

	if ((temp_area < (int)(mPersonShape.getArea()*mAreaThresholdHighRatio))&&
		((h_lmin/h_lmaj > 0.7)||(b_lmin/b_lmaj > 0.7)||(h_major.x*b_major.x+h_major.y*b_major.y > 0.85)))
	{
		// if so, merge the two to i
		mCvxHull.clear();
		mCvxHull = temp_cvxhull;// replace contour with new hull
		mCvxHullArea = temp_area;
		mDetectedBoundingRect = cv::boundingRect(mCvxHull);
		cv::Moments mm = cv::moments(temp_cvxhull);
		mCenter.x = mm.m10/mm.m00;
		mCenter.y = mm.m01/mm.m00;
		// concatenate i and j's contours
		mContours.insert(mContours.end(),other->mContours.begin(),other->mContours.end());

		mLikelihood = std::max(mLikelihood,other->mLikelihood);
		mContoursArea += other->mContoursArea;

		// update person shape
		mPersonShape.estimatePersonShape(mCenter);
		if (!mPersonShape.updateShapeInfo())
		{
			// this is very highly unlikely, but we have to handle this situation.
			VML_WARN(MsgCode::numericalErr,"Error in updating person shape info for VisionDetectorTarget.");
		}

		mPosition = mPersonShape.getFootPosition();

		evalRegionStatus();

		// if merged, mark the other blob as invalid so that it could be removed later
		other->setInvalid();
	}
}

void VisionDetectorTarget::invalidateIfSmall()
{
	if (mCvxHullArea < mPersonShape.getArea()*mpSettings->getFloat("VisionDetector/minAreaRatioToHumanShape",0.5f,false))
	{
		mValid = false;
	}
}

void VisionDetectorTarget::drawTarget(cv::Mat &image) const
{
	// draw contours
	cv::drawContours(image,mContours,-1,CV_RGB(0,0,255));
	// draw convex hull
	cv::drawContours(image,std::vector<std::vector<cv::Point> >(1,mCvxHull),0,CV_RGB(255,0,255));
//	 draw bounding box
	cv::rectangle(image,mDetectedBoundingRect,CV_RGB(0,0,255));

}

void VisionDetectorTarget::fillConvexHullMap(cv::Mat_<uchar> &map)
{
	cv::drawContours(map,std::vector<std::vector<cv::Point> >(1,mCvxHull),-1,cv::Scalar(255),CV_FILLED);
}

void VisionDetectorTarget::getTrackerInitializerInfo(cv::Rect &init_rect, cv::Mat &contour_map)
{
	// create initial contour mask
	contour_map.create(mDetectedBoundingRect.height,mDetectedBoundingRect.width,CV_8UC1);
	contour_map = 0;

	// create temp polygon in the crop rect
	std::vector<std::vector<cv::Point> > contours_crop;
	for (std::vector<std::vector<cv::Point> >::iterator itr=mContours.begin();
			itr != mContours.end();itr++)
	{
		std::vector<cv::Point> contour_crop;
		std::vector<cv::Point>::iterator pitr;
		for(pitr=itr->begin();pitr!=itr->end();pitr++)
		{
			contour_crop.push_back(*pitr - mDetectedBoundingRect.tl());
		}
		contours_crop.push_back(contour_crop);
	}
	// fill contour mask
	cv::drawContours(contour_map,contours_crop,-1,cv::Scalar(255),CV_FILLED);

	init_rect = mDetectedBoundingRect;
}

void VisionDetectorTarget::getBlobContourMap(const cv::Rect crop_rect, cv::Mat &contour_map)
{
	// create initial contour mask
	contour_map.create(crop_rect.height,crop_rect.width,CV_8UC1);
	contour_map = 0;

	// create temp polygon in the crop rect
	std::vector<std::vector<cv::Point> > contours_crop;
	for (std::vector<std::vector<cv::Point> >::iterator itr=mContours.begin();
			itr != mContours.end();itr++)
	{
		std::vector<cv::Point> contour_crop;
		std::vector<cv::Point>::iterator pitr;
		for(pitr=itr->begin();pitr!=itr->end();pitr++)
		{
			contour_crop.push_back(*pitr - crop_rect.tl());
		}
		contours_crop.push_back(contour_crop);
	}
	// fill contour mask
	cv::drawContours(contour_map,contours_crop,-1,cv::Scalar(255),CV_FILLED);

}

bool VisionDetectorTarget::checkTargetActivityOverlap(cv::Mat &target_activity_map)
{
	// TODO: This function will be obsolete
	cv::Mat cropped_at_map;
	target_activity_map(mDetectedBoundingRect).copyTo(cropped_at_map);

	cv::Scalar area = cv::sum(cropped_at_map)/255;
	if ((float)area[0]/(float)(mDetectedBoundingRect.height*mDetectedBoundingRect.width) > 0.1)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void VisionDetectorTarget::getCvxHull(std::vector<cv::Point> &outvec)
{
	outvec = mCvxHull;
}

void VisionDetectorTarget::evaluateLikelihood(cv::Rect croprect, const cv::Mat likelihoodmap)
{
	// create cropped contours
	std::vector<std::vector<cv::Point> > cropped_contours;
	std::vector<std::vector<cv::Point> >::const_iterator contours_itr;
	double contours_area = 0.f;
	for (contours_itr = mContours.begin();contours_itr!=mContours.end();contours_itr++)
	{
		std::vector<cv::Point> cropped_contour;
		std::vector<cv::Point>::const_iterator contour_itr;
		for (contour_itr = contours_itr->begin();contour_itr != contours_itr->end();contour_itr++)
		{
			cropped_contour.push_back(*contour_itr - croprect.tl());
		}
		cropped_contours.push_back(cropped_contour);
		contours_area += cv::contourArea(cropped_contour);
	}
	// get mean likelihood
	cv::Mat mask;
	mask.create(likelihoodmap.rows,likelihoodmap.cols,CV_8UC1);
	mask = 0;

	cv::drawContours(mask,cropped_contours,-1,cv::Scalar(255),CV_FILLED);
//	cv::imwrite("mask.png",mask);

	double total_likelihood = 0.f;
	double *p_lmap = (double*)likelihoodmap.data;
	uchar  *p_mask = (uchar*)mask.data;
	for (int i=0;i<likelihoodmap.rows*likelihoodmap.cols;i++)
	{
		if (*p_mask) total_likelihood += *p_lmap;
		p_mask++;
		p_lmap++;
	}

	mLikelihood = (float)total_likelihood/(float)contours_area;
}

} // of namespace vml

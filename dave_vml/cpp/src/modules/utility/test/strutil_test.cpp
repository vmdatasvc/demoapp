#define BOOST_TEST_DYN_LINK

#define BOOST_TEST_MODULE "strutil"

#include <string>
#include <vector>
#include "modules/utility/strutil.hpp"

#include <boost/test/unit_test.hpp>

using std::string;
using std::vector;

BOOST_AUTO_TEST_CASE(split){

	// Split an empty string
	string str = "";
	string delim = "/";

	vector<string> result = vml::split(str, delim);

	BOOST_CHECK(result.size() == 0);

	// split a string with the delimiter not present
	str="foo";

	result = vml::split(str,delim);

	BOOST_CHECK(result.size() == 1);
	BOOST_CHECK(result[0] == "foo");

	// split a string with a single delimiter
	str = "foo/bar";

	result = vml::split(str,delim);

	BOOST_CHECK(result.size() == 2);
	BOOST_CHECK(result[0] == "foo");
	BOOST_CHECK(result[1] == "bar");

	// split a string with multiple delimiters
	str = "foo/bar/baz";

	result = vml::split(str,delim);

	BOOST_CHECK(result.size() == 3);
	BOOST_CHECK(result[0] == "foo");
	BOOST_CHECK(result[1] == "bar");
	BOOST_CHECK(result[2] == "baz");

	// split a string with a multi-character delimiter
	str = "foo--bar-baz";
	delim = "--";

	result = vml::split(str, delim);

	BOOST_CHECK(result.size() == 2);
	BOOST_CHECK(result[0] == "foo");
	BOOST_CHECK(result[1] == "bar-baz");

	// What should the result be if the delimiter itself is empty??

}

BOOST_AUTO_TEST_CASE(tokenize){
	// Split an empty string
	string str = "";
	string delim = "/";

	vector<string> result = vml::tokenize(str, delim);

	BOOST_CHECK(result.size() == 0);

	// split a string with the delimiter not present
	str="foo";

	result = vml::tokenize(str,delim);

	BOOST_CHECK(result.size() == 1);
	BOOST_CHECK(result[0] == "foo");

	// split a string with a single delimiter
	str = "foo/bar";

	result = vml::tokenize(str,delim);

	BOOST_CHECK(result.size() == 2);
	BOOST_CHECK(result[0] == "foo");
	BOOST_CHECK(result[1] == "bar");

	// split a string with multiple delimiters
	str = "foo/bar/baz";

	result = vml::tokenize(str,delim);

	BOOST_CHECK(result.size() == 3);
	BOOST_CHECK(result[0] == "foo");
	BOOST_CHECK(result[1] == "bar");
	BOOST_CHECK(result[2] == "baz");

	// split a string with a multi-character delimiter
	str = "foo1-bar1,baz1";
	delim = "-,";

	result = vml::tokenize(str, delim);

	BOOST_CHECK(result.size() == 3);
	BOOST_CHECK(result[0] == "foo1");
	BOOST_CHECK(result[1] == "bar1");
	BOOST_CHECK(result[2] == "baz1");

	// What should the result be if the delimiter itself is empty??

}

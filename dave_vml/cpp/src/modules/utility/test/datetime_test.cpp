#define BOOST_TEST_DYN_LINK

#define BOOST_TEST_MODULE "DateTime"

#include <iostream>
#include "modules/utility/DateTime.hpp"

#include <boost/test/unit_test.hpp>

using vml::DateTime;

BOOST_AUTO_TEST_CASE(DateTimefillStruct){
	DateTime::Struct dts;

	boost::posix_time::ptime pt = boost::posix_time::time_from_string("2017-03-02 08:01:32.25");

	fillStruct(pt, dts);

	BOOST_CHECK(dts.year == 2017);
	BOOST_CHECK(dts.month == 3);
	BOOST_CHECK(dts.day == 2);
	BOOST_CHECK(dts.hour == 8);
	BOOST_CHECK(dts.minute == 1);
	BOOST_CHECK(dts.second == 32.25);
	BOOST_CHECK(dts.weekday == 3);
}

BOOST_AUTO_TEST_CASE(DateTimetoSeconds){
	// Create a DateTime object
	// 3/2/2017 8:01:32.25 AM

	// See fillStruct test for validity
	DateTime::Struct dts;
	boost::posix_time::ptime pt = boost::posix_time::time_from_string("2017-03-02 08:01:32.25");
	fillStruct(pt, dts);

	unsigned long expected_sec = 1488441692;
	double recv_sec = toSeconds(dts, false);

	// must be larger than or = expected seconds
	BOOST_CHECK(recv_sec >= expected_sec);
	// when truncated, must be equal
	BOOST_CHECK(static_cast<unsigned long>(recv_sec) == expected_sec);
	// difference must be precisely 0.25
	BOOST_CHECK(recv_sec - expected_sec == 0.25);
}

BOOST_AUTO_TEST_CASE(DateTimeobjToSeconds){

	DateTime dt(2017,3,2,8,1,32.25);

	// See "date -d "2017-03-02 08:01:32" '+%s'" command

	// NOTE: this will ONLY work if you are in UTC+5 (EST)!!  Set your locale appropriately!
	unsigned long expected_sec = 1488459692;

	double recv_sec = dt.toSeconds();

	std::cout << recv_sec << std::endl;
	std::cout << expected_sec << std::endl;

	std::cout << recv_sec - expected_sec << std::endl;

	// must be larger than or = expected seconds
	BOOST_CHECK(recv_sec >= expected_sec);
	// when truncated, must be equal
	BOOST_CHECK(static_cast<unsigned long>(recv_sec) == expected_sec);
	// difference must be precisely 0.25
	BOOST_CHECK(recv_sec - expected_sec == 0.25);
}

using std::string;
using std::vector;


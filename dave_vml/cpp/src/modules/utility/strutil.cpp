/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include <algorithm>
#include <stdlib.h>
#include <ctype.h>

#include <iostream>
#include <sstream>

#include <modules/utility/strutil.hpp>


namespace vml
{

//std::stringstream getLineFromStdIn() {
//	std::string line;
//	std::getline(std::cin, line);
//	std::stringstream ss(line);
//
//	return ss;
//}

std::string getLineFromStdIn() {
	std::string line;
	std::getline(std::cin, line);
	//std::stringstream ss(line);

	return line;
}



std::string replace(const std::string& in, const std::string& strOld, const std::string& strNew)
{
  if (in.empty()) return in;

  std::string::size_type i = in.find(strOld);

  if (i == std::string::npos) return in;

  return in.substr(0,i) + strNew + replace(in.substr(i+strOld.length()),strOld,strNew);
}

std::vector<std::string> split(const std::string& str, const std::string& token)
{
  std::vector<std::string> vstr;

  if (str.empty()) return vstr;

  size_t head = 0, tail;
  do
  {
    tail = str.find(token,head);
    if (tail >= str.length())
    {
      vstr.push_back(str.substr(head));
    }
    else
    {
      vstr.push_back(str.substr(head,tail-head));
    }
    head = tail + token.length();
  } while (tail < str.length());

  return vstr;
}

// -- http://stackoverflow.com/questions/236129/split-a-string-in-c
// -- delimiters can be multiple characters
template<typename T>
std::vector<T>
stringsplit(const T & str, const T & delimiters) {
	std::vector<T> v;
    typename T::size_type start = 0;
    auto pos = str.find_first_of(delimiters, start);
    while(pos != T::npos) {
        if(pos != start) // ignore empty tokens
            v.emplace_back(str, start, pos - start);
        start = pos + 1;
        pos = str.find_first_of(delimiters, start);
    }
    if(start < str.length()) // ignore trailing delimiter
        v.emplace_back(str, start, str.length() - start); // add what's left of the string
    return v;
}

std::vector<std::string> tokenize(const std::string& str, const std::string& token_list)
{
  std::vector<std::string> vstr;

  if (str.empty()) return vstr;

  size_t head = 0, tail;
  do
  {
    tail = str.find_first_of(token_list,head);
    if (tail == std::string::npos)
    {
      if (head != str.length())
      {
        vstr.push_back(str.substr(head));
      }
    }
    else
    {
      if (tail != head)
      {
        vstr.push_back(str.substr(head,tail-head));
      }
    }
    head = tail + 1;
  } while (tail != std::string::npos);

  return vstr;
}

std::string join(const std::vector<std::string>& v, const std::string& token)
{
  std::string out;
  for (unsigned int i = 0; i < v.size(); ++i)
  {
    out += v[i];
    if (i != v.size()-1) out += token;
  }
  return out;
}

std::string normalize_path(const std::string& in)
{
  std::string out;

  if (in.substr(0,2) == "\\\\") return "\\\\" + normalize_path(in.substr(2));

  for (unsigned int i = 0; i < in.length(); ++i)
  {
    std::string::value_type cur = in[i];
    if (cur == '\\') cur = '/';
    if (i > 0 && in[i-1] == '/' && cur == '/') continue;
    out.append(1,cur);
  }

  return out;
}

std::vector<std::string> split_path(const std::string& str)
{
  std::string dir;
  std::string fname;
  std::string path = normalize_path(str);

  unsigned int i = path.find_last_of("/");
  if (i == std::string::npos)
  {
    fname = str;
  }
  else
  {
    fname = path.substr(i+1);
    dir = path.substr(0,i+1);
  }

  std::vector<std::string> v;
  v.push_back(dir);
  v.push_back(fname);

  return v;
}

std::string get_file_extension(const std::string& str)
{
  if (str.size() == 0) return "";
  int i = str.size()-1;
  while (i > 0)
  {
    if (str[i] == '.')
    {
      return str.substr(i,str.size()-i);
    }
    else if (str[i] == '\\' || str[i] == '/')
    {
      return "";
    }
    i--;
  }
  return "";
}

std::string get_file_name(const std::string& str)
{
  return split_path(str)[1];
}

std::string get_file_name_without_extension(const std::string& str)
{
  std::string fname = get_file_name(str);
  if (fname.size() == 0) return "";
  int i = fname.size()-1;
  while (i > 0)
  {
    if (fname[i] == '.')
    {
      return fname.substr(0,i);
    }
    i--;
  }
  return fname;
}

std::string tolower(const std::string& in)
{
  std::string out = in;
  for (unsigned int i = 0; i < out.size(); i++) out[i] = ::tolower(out[i]);
  return out;
}

std::string combine_path(const std::string& path1, const std::string& path2)
{
  if (path1 == "")
  {
    return path2;
  }
  if (path2 == "")
  {
    return path1;
  }
  if (is_full_path(path2))
  {
    return path2;
  }
  if ((path1[path1.length()-1] != '/') && 
      (path1[path1.length()-1] != '\\'))
  {
    return path1 + "/" + path2;
  }
  return path1 + path2;
}

bool is_full_path(const std::string& path)
{
  if (path.substr(0,1) == "/" || path.substr(0,2) == "\\\\" ||
    path.substr(1,1) == ":")
  {
    return true;
  }
  return false;
}

std::string strip(const std::string& in, const std::string& tokens)
{
  unsigned int head,tail;
  for (head = 0; head < in.length(); head++)
  {
    if (tokens.find(in[head]) == std::string::npos) break;
  }
  for (tail = in.length()-1; tail > head; tail--)
  {
    if (tokens.find(in[tail]) == std::string::npos) break;
  }
  if (tail < head) return "";
  return in.substr(head,tail+1-head);
}

std::string xml_escape(const std::string& in)
{
  std::string out = "";
  for (unsigned int i = 0; i < in.length(); i++)
  {
    if (in[i] == '<')
    {
      out += "&lt;";
    }
    else if (in[i] == '>')
    {
      out += "&gt;";
    }
    else
    {
      out += in[i];
    }
  }
  return out;
}

} // namespace ait

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * cloudinaryAPI.cpp
 *
 *  Created on: Oct 3, 2016
 *      Author: Dave Donghun kim
 */

#include <modules/utility/restAPI.hpp>
#include <errno.h>
#include <sys/stat.h>

#define DEBUG_RESTAPI 0

namespace vml
{

RestAPI::RestAPI()
{
	curl_global_init(CURL_GLOBAL_ALL);
}

RestAPI::~RestAPI()
{
	curl_global_cleanup();
}

void RestAPI::setConfig(std::string APIKey, std::string APISecret, std::string cloudName, std::string uploadPreset)
{
	std::string resourceType = "image";

	mParams.cloud_name = cloudName;
	mParams.api_key = APIKey;
	mParams.api_secret = APISecret;
	mParams.url ="https://api.cloudinary.com/v1_1/"+ cloudName +"/" + resourceType + "/upload";

	// For uploading without signature, require to set this parameter. Otherwise, just set NULL
	mParams.upload_preset = uploadPreset;
}

//void CloudinaryAPI::upload(char * file, std::string url, std::string cloudName, std::string APIKey, std::string APISecret, std::string pid)
long RestAPI::uploadBufferedData(std::string pid, bool singedOpt, char *imgBuffPtr, long imgBuffLength)
{
	// unixTime
    std::time_t t = std::time(0);
    std::stringstream ss;
    ss << t;
    mParams.timestamp = ss.str();
    //std::string unixTime = ss.str();

    mParams.public_id = pid;
    mParams.signedOption = singedOpt;

    // Create SHA1 signature required by cloudinary
     //std::string pid = "DKIM-cam000-0c16f18c-41b6-4746-8ea2-7a21cf72d355";
     std::string hashString = "public_id="+mParams.public_id +"&timestamp=" + mParams.timestamp + mParams.api_secret;
     mParams.signature = sha1(hashString);
     //std::string authSig = sha1(hashString);

    //Set headers as null
    struct curl_slist *headers = NULL;
    struct curl_httppost *formpost = NULL;
    struct curl_httppost *lastptr = NULL;

    // header
    curl_slist_append(headers,"Content-Type: multipart/form-data");

    if(singedOpt)
    {
		curl_formadd(&formpost,
				&lastptr,
				CURLFORM_COPYNAME, "api_key",
				CURLFORM_COPYCONTENTS, mParams.api_key.c_str(),
				CURLFORM_END);

		 curl_formadd(&formpost,
			   &lastptr,
			   CURLFORM_COPYNAME, "public_id",
			   CURLFORM_COPYCONTENTS,  mParams.public_id.c_str(),
			   CURLFORM_END);


		  curl_formadd(&formpost,
				&lastptr,
				CURLFORM_COPYNAME, "signature",
				CURLFORM_COPYCONTENTS, mParams.signature.c_str(),
				CURLFORM_END);

		  curl_formadd(&formpost,
				  &lastptr,
				  CURLFORM_COPYNAME, "timestamp",
				  CURLFORM_COPYCONTENTS, mParams.timestamp.c_str(),
				  CURLFORM_END);

    }
    else
    {
    	if(mParams.upload_preset.c_str() == NULL)
    	{
    		printf("Cloudinary: Error! need to provide a upload_preset!\n");
    		return 100;
    	}
    	else
    	{
    		curl_formadd(&formpost,
                &lastptr,
                CURLFORM_COPYNAME, "upload_preset",
                CURLFORM_COPYCONTENTS, mParams.upload_preset.c_str(),
                CURLFORM_END);
    	}
    }

    // buffered data
	curl_formadd(&formpost,
			   &lastptr,
			   CURLFORM_COPYNAME, "file",
			   CURLFORM_BUFFER, "local_file_name.jpg",
			   CURLFORM_BUFFERPTR, imgBuffPtr,
			   CURLFORM_BUFFERLENGTH, imgBuffLength,
			   CURLFORM_END);

     CURL *curlhandle = curl_easy_init();
     long res_code;

     if(curlhandle)
     {


		if(DEBUG_RESTAPI)
		{
			// ask libcurl to show us the verbose output (DEBUG ONLY)
			curl_easy_setopt(curlhandle, CURLOPT_VERBOSE, 1L);
			// For getting a body with error message in the first place
			curl_easy_setopt(curlhandle, CURLOPT_FAILONERROR, 0);
		}
         curl_easy_setopt(curlhandle, CURLOPT_URL, mParams.url.c_str());
         curl_easy_setopt(curlhandle, CURLOPT_POST, 1L);

         // Second, specify the POST data
         curl_easy_setopt(curlhandle, CURLOPT_HTTPHEADER, headers);
         curl_easy_setopt(curlhandle, CURLOPT_HTTPPOST, formpost);

         CURLcode res = curl_easy_perform(curlhandle);
         curl_easy_getinfo(curlhandle, CURLINFO_RESPONSE_CODE, &res_code);

		if(DEBUG_RESTAPI)
		{
			if(res != CURLE_OK)
			{
				printf("Cloudinary: failed: %s\n", curl_easy_strerror(res));

				//ret= false;
			}
			else
			{
				printf("Cloudinary: successed: %s\n",  curl_easy_strerror(res));
				//ret = true;
			}
		}

          // always cleanup
          curl_easy_cleanup(curlhandle);
          curl_formfree(formpost);
    }

     return res_code;
}

// upload a file stored in disk
long RestAPI::uploadFile(std::string pid, bool singedOpt, std::string filename)
{
	// unixTime
    std::time_t t = std::time(0);
    std::stringstream ss;
    ss << t;
    mParams.timestamp = ss.str();
    //std::string unixTime = ss.str();

    mParams.public_id = pid;
    mParams.signedOption = singedOpt;

    // Create SHA1 signature required by cloudinary
     //std::string pid = "DKIM-cam000-0c16f18c-41b6-4746-8ea2-7a21cf72d355";
     std::string hashString = "public_id="+mParams.public_id +"&timestamp=" + mParams.timestamp + mParams.api_secret;
     mParams.signature = sha1(hashString);
     //std::string authSig = sha1(hashString);

    //Set headers as null
    struct curl_slist *headers = NULL;
    struct curl_httppost *formpost = NULL;
    struct curl_httppost *lastptr = NULL;

    // header
    curl_slist_append(headers,"Content-Type: multipart/form-data");

    if(singedOpt)
    {
		curl_formadd(&formpost,
				&lastptr,
				CURLFORM_COPYNAME, "api_key",
				CURLFORM_COPYCONTENTS, mParams.api_key.c_str(),
				CURLFORM_END);

		 curl_formadd(&formpost,
			   &lastptr,
			   CURLFORM_COPYNAME, "public_id",
			   CURLFORM_COPYCONTENTS,  mParams.public_id.c_str(),
			   CURLFORM_END);


		  curl_formadd(&formpost,
				&lastptr,
				CURLFORM_COPYNAME, "signature",
				CURLFORM_COPYCONTENTS, mParams.signature.c_str(),
				CURLFORM_END);

		  curl_formadd(&formpost,
				  &lastptr,
				  CURLFORM_COPYNAME, "timestamp",
				  CURLFORM_COPYCONTENTS, mParams.timestamp.c_str(),
				  CURLFORM_END);
    }
    else
    {
    	if(mParams.upload_preset.c_str() == NULL)
    	{
    		printf("Cloudinary: Error! need to provide a upload_preset!\n");
    		return 100;
    	}
    	else
    	{
    		curl_formadd(&formpost,
                &lastptr,
                CURLFORM_COPYNAME, "upload_preset",
                CURLFORM_COPYCONTENTS, mParams.upload_preset.c_str(),
                CURLFORM_END);
    	}

    }
      curl_formadd(&formpost,
               &lastptr,
               CURLFORM_COPYNAME, "file",
               CURLFORM_FILE, filename.c_str(),
               CURLFORM_END);

     CURL *curlhandle = curl_easy_init();
     long res_code;

     if(curlhandle)
     {


    	if(DEBUG_RESTAPI)
    	{
    		// ask libcurl to show us the verbose output (DEBUG ONLY)
    		curl_easy_setopt(curlhandle, CURLOPT_VERBOSE, 1L);
    		// For getting a body with error message in the first place
    		curl_easy_setopt(curlhandle, CURLOPT_FAILONERROR, 0);
    	}
         curl_easy_setopt(curlhandle, CURLOPT_URL, mParams.url.c_str());
         curl_easy_setopt(curlhandle, CURLOPT_POST, 1L);

         // Second, specify the POST data
         curl_easy_setopt(curlhandle, CURLOPT_HTTPHEADER, headers);
         curl_easy_setopt(curlhandle, CURLOPT_HTTPPOST, formpost);

         CURLcode res = curl_easy_perform(curlhandle);
         curl_easy_getinfo(curlhandle, CURLINFO_RESPONSE_CODE, &res_code);

		if(DEBUG_RESTAPI)
		{
			if(res != CURLE_OK)
			{
				printf("Cloudinary: failed: %s\n", curl_easy_strerror(res));

				//ret= false;
			}
			else
			{
				printf("Cloudinary: successed: %s\n",  curl_easy_strerror(res));
				//ret = true;
			}
		}
          // always cleanup
          curl_easy_cleanup(curlhandle);
          curl_formfree(formpost);
    }

     return res_code;
}

//==========  for an FTP over SSL (FTPS) upload ===================//
// requirement: username, password
void RestAPI::setConfigFTPS(std::string url, std::string username, std::string password)
{
	mParamsSFTP.url = url;
	mParamsSFTP.user = username;
	mParamsSFTP.password = password;
	mParamsSFTP.userpwd = username+":"+password;
}

long RestAPI::uploadBufferedDataFTPS(std::string pid, bool singedOpt, char *imgBuffPtr, long imgBuffLength)
{
	long res_code = 200;

	return res_code;
}

// upload a file stored in disk
long RestAPI::uploadFileFTPS(std::string pid, std::string filename)
{

    //Set headers as null
    struct curl_slist *headers = NULL;
/*
    struct curl_httppost *formpost = NULL;
    struct curl_httppost *lastptr = NULL;

    // header
    curl_slist_append(headers,"Content-Type: multipart/form-data");

    if(singedOpt)
    {
		curl_formadd(&formpost,
				&lastptr,
				CURLFORM_COPYNAME, "api_key",
				CURLFORM_COPYCONTENTS, mParams.api_key.c_str(),
				CURLFORM_END);

		 curl_formadd(&formpost,
			   &lastptr,
			   CURLFORM_COPYNAME, "public_id",
			   CURLFORM_COPYCONTENTS,  mParams.public_id.c_str(),
			   CURLFORM_END);


		  curl_formadd(&formpost,
				&lastptr,
				CURLFORM_COPYNAME, "signature",
				CURLFORM_COPYCONTENTS, mParams.signature.c_str(),
				CURLFORM_END);

		  curl_formadd(&formpost,
				  &lastptr,
				  CURLFORM_COPYNAME, "timestamp",
				  CURLFORM_COPYCONTENTS, mParams.timestamp.c_str(),
				  CURLFORM_END);
    }
    else
    {
    	if(mParams.upload_preset.c_str() == NULL)
    	{
    		printf("Cloudinary: Error! need to provide a upload_preset!\n");
    		return 100;
    	}
    	else
    	{
    		curl_formadd(&formpost,
                &lastptr,
                CURLFORM_COPYNAME, "upload_preset",
                CURLFORM_COPYCONTENTS, mParams.upload_preset.c_str(),
                CURLFORM_END);
    	}

    }

      curl_formadd(&formpost,
               &lastptr,
               CURLFORM_COPYNAME, "file",
               CURLFORM_FILE, filename.c_str(),
               CURLFORM_END);
*/

      std::string upload_file_as = pid + ".jpg";
      std::string rename_file_to = pid + ".jpg";

      std::string buf_1 = "RNFR " + upload_file_as;
      std::string buf_2 = "RNTO " + rename_file_to;

     CURL *curlhandle = curl_easy_init();
     curl_off_t fsize;
     struct stat file_info;

     long res_code;

     if(stat(filename.c_str(), &file_info)) {
    	 printf("Couldn't open '%s'\n", filename.c_str());
    	 return 201;
     }
     fsize = (curl_off_t)file_info.st_size;
     printf("File size:%" CURL_FORMAT_CURL_OFF_T " bytes.\n", fsize);

     FILE *hd_src = fopen( filename.c_str(), "rb");

     if(curlhandle)
     {
    	 headers = curl_slist_append(headers, buf_1.c_str());
    	 headers = curl_slist_append(headers, buf_2.c_str());

    	 if(DEBUG_RESTAPI)
    	 {
    		 // ask libcurl to show us the verbose output (DEBUG ONLY)
    		 curl_easy_setopt(curlhandle, CURLOPT_VERBOSE, 1L);
    		 // For getting a body with error message in the first place
    		 curl_easy_setopt(curlhandle, CURLOPT_FAILONERROR, 0);
    	 }

          // ssl (ref) http://stackoverflow.com/questions/20472045/ftp-over-ssl-by-libcurl-in-windows
          curl_easy_setopt(curlhandle, CURLOPT_USERNAME, mParamsSFTP.user.c_str());
          curl_easy_setopt(curlhandle, CURLOPT_PASSWORD, mParamsSFTP.password.c_str());
          curl_easy_setopt(curlhandle, CURLOPT_SSL_VERIFYPEER, 1L);
          curl_easy_setopt(curlhandle, CURLOPT_SSL_VERIFYHOST, 2L);
          curl_easy_setopt(curlhandle, CURLOPT_SSLVERSION, CURL_SSLVERSION_DEFAULT);
          curl_easy_setopt(curlhandle, CURLOPT_FTPSSLAUTH, CURLFTPAUTH_SSL);

          // enable uploading
         curl_easy_setopt(curlhandle, CURLOPT_UPLOAD, 1L);

         /* We activate SSL and we require it for both control and data */
         //curl_easy_setopt(curlhandle, CURLOPT_USE_SSL, CURLUSESSL_ALL);

         // specify target
         curl_easy_setopt(curlhandle, CURLOPT_URL, mParamsSFTP.url.c_str());
         //curl_easy_setopt(curlhandle, CURLOPT_USERPWD, mParamsSFTP.userpwd.c_str());
         curl_easy_setopt(curlhandle, CURLOPT_PORT, 21);

         // pass in that last of FTP commands to run after the transfer
         curl_easy_setopt(curlhandle, CURLOPT_POSTQUOTE, headers);

         // now specify which file to upload
         curl_easy_setopt(curlhandle, CURLOPT_INFILE, hd_src);
         curl_easy_setopt(curlhandle, CURLOPT_INFILESIZE_LARGE, (curl_off_t)fsize);

         // Second, specify the POST data
//         curl_easy_setopt(curlhandle, CURLOPT_HTTPHEADER, headers);
//         curl_easy_setopt(curlhandle, CURLOPT_HTTPPOST, formpost);

         CURLcode res = curl_easy_perform(curlhandle);
         curl_easy_getinfo(curlhandle, CURLINFO_RESPONSE_CODE, &res_code);

         if(DEBUG_RESTAPI)
         {
          	  if(res != CURLE_OK)
          	  {
          		  printf("ftps: failed: %s\n", curl_easy_strerror(res));

          		  //ret= false;
          	  }
          	  else
          	  {
          		  printf("ftps: successed: %s\n",  curl_easy_strerror(res));
          		  //ret = true;
          	  }
         }
          // always cleanup
          curl_easy_cleanup(curlhandle);
//          curl_formfree(formpost);
    }

     return res_code;
}

//==========  for an SFTP (SSH FTP) upload ===================//
// requirement: username, password
void RestAPI::setConfigSFTP(std::string url, std::string username, std::string password)
{
	mParamsSFTP.url = url;
	mParamsSFTP.user = username;
	mParamsSFTP.password = password;
	mParamsSFTP.userpwd = username+":"+password;
}

long RestAPI::uploadBufferedDataSFTP(std::string pid, bool singedOpt, char *imgBuffPtr, long imgBuffLength)
{
	  //Set headers as null
	    struct curl_slist *headers = NULL;

	    std::string upload_file_as = mParamsSFTP.url + pid + ".jpg";

	     CURL *curlhandle = curl_easy_init();
	     //curl_off_t fsize;
	     //struct stat file_info;

	     long res_code;


	     if(curlhandle)
	     {

	    	 if(DEBUG_RESTAPI)
	    	 {
	    		 // ask libcurl to show us the verbose output (DEBUG ONLY)
	    		 curl_easy_setopt(curlhandle, CURLOPT_VERBOSE, 1L);
	    		 // For getting a body with error message in the first place
	    		 curl_easy_setopt(curlhandle, CURLOPT_FAILONERROR, 0);
	    	 }
	          curl_easy_setopt(curlhandle, CURLOPT_USERNAME, mParamsSFTP.user.c_str());
	          curl_easy_setopt(curlhandle, CURLOPT_PASSWORD, mParamsSFTP.password.c_str());

	          // ssh
	          curl_easy_setopt(curlhandle,CURLOPT_SSH_AUTH_TYPES, CURLSSH_AUTH_PASSWORD);

	          // enable uploading
	         curl_easy_setopt(curlhandle, CURLOPT_UPLOAD, 1L);

	         // specify target
	         curl_easy_setopt(curlhandle, CURLOPT_URL, upload_file_as.c_str()); //mParamsSFTP.url.c_str());

	         // pass in that last of FTP commands to run after the transfer
	         curl_easy_setopt(curlhandle, CURLOPT_POSTQUOTE, headers);

	         // now specify which file to upload
	         curl_easy_setopt(curlhandle, CURLOPT_INFILE, imgBuffPtr);
	         curl_easy_setopt(curlhandle, CURLOPT_INFILESIZE_LARGE, (curl_off_t)imgBuffLength);

	         CURLcode res = curl_easy_perform(curlhandle);
	         curl_easy_getinfo(curlhandle, CURLINFO_RESPONSE_CODE, &res_code);

			if(DEBUG_RESTAPI)
			{
				if(res != CURLE_OK)
				{
					printf("ftps: failed: %s\n", curl_easy_strerror(res));
					//ret= false;
				}
				else
				{
					printf("ftps: successed: %s\n",  curl_easy_strerror(res));
					//ret = true;
				}
			}
	          // always cleanup
	          curl_easy_cleanup(curlhandle);
	    }

	     return res_code;

	return res_code;
}

/* define this to switch off the use of ssh-agent in this program */
#undef DISABLE_SSH_AGENT

// upload a file stored in disk
// e.g.) url = "sftp://localhost:22/dir/" and pid = "uuid"  ->  saved in .../dir/uuid.jpg
bool RestAPI::uploadFileSFTP(std::string pid, std::string filename)
{

    //Set headers as null
	bool ret=false;
    struct curl_slist *headers = NULL;

    std::string ext = filename.substr(filename.find_last_of(".")+1);

    std::string upload_file_as = mParamsSFTP.url + pid + "." + ext; // ".jpg";

     CURL *curlhandle = curl_easy_init();
     curl_off_t fsize;
     struct stat file_info;

     long res_code=0;

     if(stat(filename.c_str(), &file_info)) {
    	 printf("Couldn't open '%s'\n", filename.c_str());
    	 return 201;
     }
     fsize = (curl_off_t)file_info.st_size;
     printf("File size:%" CURL_FORMAT_CURL_OFF_T " bytes.\n", fsize);

     FILE *hd_src = fopen( filename.c_str(), "rb");

     if(curlhandle)
     {

		if(DEBUG_RESTAPI)
		{
			// ask libcurl to show us the verbose output (DEBUG ONLY)
			curl_easy_setopt(curlhandle, CURLOPT_VERBOSE, 1L);
			// For getting a body with error message in the first place
			curl_easy_setopt(curlhandle, CURLOPT_FAILONERROR, 0);
		}
          curl_easy_setopt(curlhandle, CURLOPT_USERNAME, mParamsSFTP.user.c_str());
          curl_easy_setopt(curlhandle, CURLOPT_PASSWORD, mParamsSFTP.password.c_str());

          // ssh
          curl_easy_setopt(curlhandle,CURLOPT_SSH_AUTH_TYPES, CURLSSH_AUTH_PASSWORD);

          // enable uploading
         curl_easy_setopt(curlhandle, CURLOPT_UPLOAD, 1L);

         // create dir
         curl_easy_setopt(curlhandle, CURLOPT_FTP_CREATE_MISSING_DIRS, 1L);

         // specify target
         curl_easy_setopt(curlhandle, CURLOPT_URL, upload_file_as.c_str()); //mParamsSFTP.url.c_str());

         // pass in that last of FTP commands to run after the transfer
         curl_easy_setopt(curlhandle, CURLOPT_POSTQUOTE, headers);

         // now specify which file to upload
         curl_easy_setopt(curlhandle, CURLOPT_INFILE, hd_src);
         curl_easy_setopt(curlhandle, CURLOPT_INFILESIZE_LARGE, (curl_off_t)fsize);

         CURLcode res = curl_easy_perform(curlhandle);
         curl_easy_getinfo(curlhandle, CURLINFO_RESPONSE_CODE, &res_code);

          if(res != CURLE_OK)
          {
				if(DEBUG_RESTAPI)
					printf("sftp: failed: %s\n", curl_easy_strerror(res));

              ret= false;
          }
          else
          {
				if(DEBUG_RESTAPI)
					printf("sftp: succeeded: %s\n",  curl_easy_strerror(res));

              ret = true;
          }

          // always cleanup
          curl_easy_cleanup(curlhandle);
    }

     return ret;
}


std::size_t RestAPI::fwriteCB(void *buffer, std::size_t size, std::size_t nmemb, void *stream)
{
  struct FtpFile *out=(struct FtpFile *)stream;

  if(out && !out->stream)
  {
    /* open file for writing */
    out->stream=fopen(out->filename, "wb");

    if(!out->stream)
      return -1; /* failure, can't open file to write */
  }

  return fwrite(buffer, size, nmemb, out->stream);
}

// download to where and with what name
bool RestAPI::downloadFileSFTP(std::string filename, std::string targetfilePath)
{
	  CURL *curlhandle;
	  CURLcode res;

	  struct FtpFile ftpfile={
			  targetfilePath.c_str(),  // name to store the file as if successful
			  NULL
	  };

	  std::string downloadfile_url = mParamsSFTP.url + filename;

	  curl_global_init(CURL_GLOBAL_DEFAULT);

	  curlhandle = curl_easy_init();

	  if(curlhandle)
	  {
		  if(DEBUG_RESTAPI)
		  {
			  // Switch on full protocol/debug output
			  curl_easy_setopt(curlhandle, CURLOPT_VERBOSE, 1L);

			  // For getting a body with error message in the first place
			  curl_easy_setopt(curlhandle, CURLOPT_FAILONERROR, 0);
		  }

		  curl_easy_setopt(curlhandle, CURLOPT_URL, downloadfile_url.c_str());

		  // Define our callback to get called when there's data to be written
		  curl_easy_setopt(curlhandle, CURLOPT_WRITEFUNCTION, fwriteCB);

		  // Set a pointer to our struct to pass to the callback
		  curl_easy_setopt(curlhandle, CURLOPT_WRITEDATA, &ftpfile);

		  curl_easy_setopt(curlhandle, CURLOPT_USERNAME, mParamsSFTP.user.c_str());

		  curl_easy_setopt(curlhandle, CURLOPT_PASSWORD, mParamsSFTP.password.c_str());

		  // ssh
		  curl_easy_setopt(curlhandle,CURLOPT_SSH_AUTH_TYPES, CURLSSH_AUTH_PASSWORD);

		  res = curl_easy_perform(curlhandle);

		  // always cleanup
		  curl_easy_cleanup(curlhandle);

		  if(CURLE_OK != res)
		  {
			  fprintf(stderr, "curl told us %d\n", res);
		  }
	  }

	  if(ftpfile.stream)
	  {
		  fclose(ftpfile.stream); /* close the local file */
	  }

	  curl_global_cleanup();

	  return true;
}


std::size_t RestAPI::WriteMemoryCB(void *contents, std::size_t size, std::size_t nmemb, void *userp)
{
    std::size_t realsize = size * nmemb;

    struct MemoryStruct *mem = (struct MemoryStruct *)userp;

    mem->memory = (char *)std::realloc(mem->memory, mem->size + realsize + 1);

    if(mem->memory == NULL)
    {
        /* out of memory! */
        printf("not enough memory (realloc returned NULL)\n");
        return 0;
    }
    //std::vector<uchar>::size_type sz = std::strlen((const char *)chunk.memory);
    //long imgBuffLength = static_cast<long>(chunk.size);
    //std::vector<char>::size_type size = strlen(chunk.memory);
    //std::vector<uchar> bufferedData((uchar *)chunk.memory, imgBuffLength);  //chunk.memory+ size); //

    //cv::Mat data_mat(bufferedData);
    //fdata = cv::imdecode(bufferedData, cv::IMREAD_UNCHANGED);//  cv::IMREAD_ANYCOLOR);

    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;

    return realsize;
}

bool RestAPI::downloadBufferedDataSFTP(std::string filename, cv::Mat &fdata)
{
    CURL        *curlhandle;
    CURLcode    res;
    bool        ret;

    struct MemoryStruct buffer;

    std::string downloadfile_url = mParamsSFTP.url + filename;

    buffer.memory = (char *)std::malloc(1);  /* will be grown as needed by the realloc above */
    buffer.size = 0;    /* no data at this point */

    curl_global_init(CURL_GLOBAL_ALL);

    /* init the curl session */
    curlhandle = curl_easy_init();


    if(curlhandle)
    {

       if(DEBUG_RESTAPI)
       {
          // Switch on full protocol/debug output
          curl_easy_setopt(curlhandle, CURLOPT_VERBOSE, 1L);

          // For getting a body with error message in the first place
          curl_easy_setopt(curlhandle, CURLOPT_FAILONERROR, 0);
        }

        /* specify URL to get */
        curl_easy_setopt(curlhandle, CURLOPT_URL, downloadfile_url.c_str());

        /* send all data to this function  */
        curl_easy_setopt(curlhandle, CURLOPT_WRITEFUNCTION, WriteMemoryCB);

        /* we pass our 'chunk' struct to the callback function */
        curl_easy_setopt(curlhandle, CURLOPT_WRITEDATA, (void *)&buffer);

        /* some servers don't like requests that are made without a user-agent
           field, so we provide one */
        curl_easy_setopt(curlhandle, CURLOPT_USERAGENT, "libcurl-agent/1.0");


        curl_easy_setopt(curlhandle, CURLOPT_USERNAME, mParamsSFTP.user.c_str());

        curl_easy_setopt(curlhandle, CURLOPT_PASSWORD, mParamsSFTP.password.c_str());

        // ssh
        curl_easy_setopt(curlhandle,CURLOPT_SSH_AUTH_TYPES, CURLSSH_AUTH_PASSWORD);


        /* get it! */
        res = curl_easy_perform(curlhandle);

        /* check for errors */
        if(res != CURLE_OK)
        {
            if(DEBUG_RESTAPI)
            {
                fprintf(stderr, "curl_easy_perform() failed: %s\n",
                curl_easy_strerror(res));
            }
            ret= false;
        }
        else {

            // Save char data to cv::Mat
            fdata = cv::imdecode(cv::Mat(1,buffer.size, CV_8UC1, buffer.memory), cv::IMREAD_UNCHANGED);

            if(DEBUG_RESTAPI)
            {
                printf("%lu bytes retrieved\n", (long)buffer.size);
            }
            ret = true;
        }


        /* cleanup curl stuff */
        curl_easy_cleanup(curlhandle);
    }

    free(buffer.memory);

    /* we're done with libcurl, so clean it up */
    curl_global_cleanup();

    return ret;

}

int RestAPI::writer(char *data, std::size_t size, std::size_t nmemb, std::string *writerData)
{
	if (writerData == NULL) {
		std::cout << "error copy html!" << std::endl;
		return 0;
	}
	writerData->append(data, size * nmemb);

	return size * nmemb;
}

// for Amazon Rekognition API
// 1. Create response json object.
// Json::Value response;
//
// 2. Specify API request in a map object.
// const string api_addr_base = "https://rekognition.com/func/api/?";
// map<string, string> query_config;
// query_config["api_key"] = "YOUR_API_KEY";
// query_config["api_secret"] = "YOUR_API_SECRET";
// query_config["jobs"] = "face_part";
// query_config["urls"] = "http://rekognition.com/static/img/people.jpg";

bool RestAPI::reckognitionAPICall(const std::string& api_addr_base, const std::map<std::string,std::string>& query_config, nlohmann::json &response)//, Json::Value* response)
{
	CURL *curl;
	curl = curl_easy_init();
	if (!curl) {
		std::cerr << "CURL init error!" << std::endl;
		return false;
	}

	curl_easy_setopt(curl, CURLOPT_POST, 1L);
	curl_easy_setopt(curl, CURLOPT_HEADER, 0L);
	curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1L);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
	curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
	curl_easy_setopt(curl, CURLOPT_AUTOREFERER, 1L);
	curl_easy_setopt(curl, CURLOPT_TIMEOUT, 3);
	curl_easy_setopt(curl, CURLOPT_USERAGENT,
			"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) "
			"AppleWebKit/534.7 (KHTML, like Gecko) Chrome/7.0.517.41 Safari/534.7");
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writer);
	curl_easy_setopt(curl, CURLOPT_URL, api_addr_base.c_str());

	std::string post_fields = "";
	for (std::map<std::string, std::string>::const_iterator iter = query_config.begin();
			iter != query_config.end(); ++iter) {
		post_fields.append("&");
		post_fields.append(iter->first);
		post_fields.append("=");
		post_fields.append(iter->second);
	}

	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, post_fields.c_str());

	std::string photos_page;
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &photos_page);

	try {
		int ret = curl_easy_perform(curl);
		if (ret != CURLE_OK) {
			std::cerr << "Returned code: " << ret << std::endl;
			std::cerr << "CURL calling error!" << std::endl;
			curl_easy_cleanup(curl);
			return false;
		}
	} catch (...) {
		std::cerr << "CURL calling error!" << std::endl;
		curl_easy_cleanup(curl);
		return false;
	}

	std::cerr << "reponse: " << photos_page << std::endl;

	// Json::Reader reader;
	response = nlohmann::json::parse(photos_page);
	if(std::distance(response.begin(),response.end()) == 0)
	//if (!reader.parse(photos_page, *response))
	{
		std::cout << "Failed to parse response." << std::endl;
		curl_easy_cleanup(curl);
		return false;
	}
	curl_easy_cleanup(curl);

	return true;
}



}  // of namespace vml

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

//#include <legacy/pv/PvUtil.hpp>
#include <modules/utility/DateTime.hpp>
//#include <legacy/types/String.hpp>
#include <exception>
#include <iomanip>
#include <core/Error.hpp>
#include <ctime>
#include <boost/date_time/local_time_adjustor.hpp>
#include <boost/date_time/c_local_time_adjustor.hpp>

#ifdef WIN32

#include <windows.h>

static FILETIME getFileTime(const double sec)
{
  LARGE_INTEGER curTime;
  curTime.QuadPart = (LONGLONG)((long double)sec*10000000);
  FILETIME utc;
  utc.dwLowDateTime = curTime.LowPart;
  utc.dwHighDateTime = curTime.HighPart;
  return utc;
}

static bool fillStruct(FILETIME t, ait::DateTime::Struct& s)
{
  SYSTEMTIME sysTime;
  if (FileTimeToSystemTime(&t,&sysTime) == 0) return false;

  s.year = (int)sysTime.wYear;
  s.month = (int)sysTime.wMonth;
  s.day = (int)sysTime.wDay;
  s.hour = (int)sysTime.wHour;
  s.minute = (int)sysTime.wMinute;
  s.second = (double)sysTime.wSecond+((double)sysTime.wMilliseconds)/1000.0;
  s.weekday = (int)(sysTime.wDayOfWeek+6)%7;
  s.julian = 0;
  s.daylight = 0;

  return true;
}

static double toSeconds(const ait::DateTime::Struct& s, bool isLocalTime)
{
  SYSTEMTIME sysTime;
  ZeroMemory(&sysTime,sizeof(sysTime));
  sysTime.wYear = s.year;
  sysTime.wMonth = s.month;
  sysTime.wDay = s.day;
  sysTime.wHour = s.hour;
  sysTime.wMinute = s.minute;
  sysTime.wSecond = (WORD)(int)s.second;
  sysTime.wMilliseconds = (WORD)((s.second-sysTime.wSecond)*1000);

  FILETIME ft;

  if (SystemTimeToFileTime(&sysTime,&ft) == 0)
  {
    throw std::exception("Invalid Date/Time structure");
  }

  FILETIME utc;

  if (isLocalTime)
  {
    LocalFileTimeToFileTime(&ft,&utc);
  }
  else
  {
    utc = ft;
  }

  LARGE_INTEGER li;

  li.LowPart = utc.dwLowDateTime;
  li.HighPart = utc.dwHighDateTime;

  return (double)(((long double)li.QuadPart)/10000000);
}

#else
	// if not windows
bool fillStruct(boost::posix_time::ptime t, vml::DateTime::Struct& s)
{
	struct tm pt_tm = boost::posix_time::to_tm(t);

	boost::posix_time::time_duration td = t - boost::posix_time::from_time_t(0);

	// get the number of microseconds (fractional only)
	unsigned long usec = td.total_microseconds() - static_cast<unsigned long>(td.total_seconds()) * 1000000 ;

	s.year = pt_tm.tm_year+1900;
	s.month = pt_tm.tm_mon+1;
	s.day = pt_tm.tm_mday;
	s.hour = pt_tm.tm_hour;
	s.minute = pt_tm.tm_min;
	s.second = (double)pt_tm.tm_sec+usec/static_cast<double>(1000000);
	s.weekday = (int)(pt_tm.tm_wday+6)%7; // NOTE: Don't know why but make Monday 0.
	s.julian = 0;
	s.daylight = pt_tm.tm_isdst;

	return true;
}


double toSeconds(const vml::DateTime::Struct& s, bool isLocalTime)
{
	struct tm pt_tm;
	pt_tm.tm_year = s.year-1900;
	pt_tm.tm_mon = s.month-1;
	pt_tm.tm_mday = s.day;
	pt_tm.tm_hour = s.hour;
	pt_tm.tm_min = s.minute;
	pt_tm.tm_sec = 0; // NOTE: will be added as total microseconds in 3 lines
	pt_tm.tm_isdst = s.daylight;

	boost::posix_time::ptime pt = boost::posix_time::ptime_from_tm(pt_tm);

	// Account for fractional seconds here
	pt += boost::posix_time::microseconds(s.second * 1000000);

	if (isLocalTime)
	{
		boost::posix_time::time_duration utc_offset = boost::date_time::c_local_adjustor<boost::posix_time::ptime>::utc_to_local(pt) - pt;
		pt -= utc_offset;
	}

	boost::posix_time::time_duration td = pt - boost::posix_time::from_time_t(0);

	return td.total_microseconds() / static_cast<double>(1000000);

}

#endif

namespace vml
{

const std::string DateTime::defaultFormatStr = "%a, %d %b %Y %H:%M:%S";
const std::string DateTime::sqlFormatStr = "%Y-%m-%d %H:%M:%S";
const std::string DateTime::fileFormatStr = "%Y%m%d_%H%M%S";
const std::string DateTime::vmsFormatStr = "%Y-%m-%dT%H:%M:%S.%f";


DateTime::DateTime(const int year, const int month, const int day, 
    const int hour, const int minute, const double second)
{
  mStruct.year = year;
  mStruct.month = month;
  mStruct.day = day;
  mStruct.hour = hour;
  mStruct.minute = minute;
  mStruct.second = second;
  
  // This will fill the rest of the DateTime structure fields.
  double seconds = toSeconds();
  setLocal(seconds);
}

//
// OPERATORS
//

//
// OPERATIONS
//

bool 
DateTime::secsToUTC(const double sec, Struct& s)
{
#ifdef WIN32
  return fillStruct(getFileTime(sec),s);
#else
  // convert sec to POSIX ptime (local)
  std::time_t	_sec = floor(sec);
  long _msec = round((sec-static_cast<double>(_sec))*1000000);

  boost::posix_time::ptime pt = boost::posix_time::from_time_t(_sec);
  boost::posix_time::time_duration _fracsec(0,0,0,_msec);
  pt += _fracsec;
  return fillStruct(pt,s);
#endif
}

bool 
DateTime::getUTC(Struct& s)
{
	// NOTE: it's probably more efficient to call boost::posix_time::second_clock::universal_time()
	//  but just call gCurrentTime() and convert it to ptime to make it compatible with other time-related code
  return secsToUTC(gCurrentTime(),s);
}

bool 
DateTime::secsToLocal(const double sec, Struct& s)
{
#ifdef WIN32
  FILETIME utc = getFileTime(sec);
  FILETIME local;
  FileTimeToLocalFileTime(&utc,&local);
  return fillStruct(local,s);
#else
  // convert sec to POSIX ptime (local)
  std::time_t	_sec = floor(sec);
  long _msec = round((sec-static_cast<double>(_sec))*1000000);

  boost::posix_time::ptime pt = boost::posix_time::from_time_t(_sec);
  boost::posix_time::time_duration _fracsec(0,0,0,_msec);
  pt += _fracsec;

  // convert utc to local using local adjustor
  boost::posix_time::ptime local = boost::date_time::c_local_adjustor<boost::posix_time::ptime>::utc_to_local(pt);
  return fillStruct(local,s);
#endif
}

bool 
DateTime::getLocal(Struct& s)
{
  return secsToLocal(gCurrentTime(),s);
}

std::string
DateTime::getElem(char fmt) const
{
  switch(fmt) {
  case 'a':
    switch(mStruct.weekday)
    {
    case 0: return "Mon";
    case 1: return "Tue";
    case 2: return "Wed";
    case 3: return "Thu";
    case 4: return "Fri";
    case 5: return "Sat";
    case 6: return "Sun";
    default: return "Invalid";
    }
  case 'b':
    switch(mStruct.month)
    {
    case 1: return "Jan";
    case 2: return "Feb";
    case 3: return "Mar";
    case 4: return "Apr";
    case 5: return "May";
    case 6: return "Jun";
    case 7: return "Jul";
    case 8: return "Aug";
    case 9: return "Sep";
    case 10: return "Oct";
    case 11: return "Nov";
    case 12: return "Dec";
    default: return "Invalid";
    }
  case 'd':
  {
	  std::stringstream tstr;
	  tstr << std::setprecision(2) << std::setfill('0') << std::setw(2) << mStruct.day;
	  return tstr.str();
  }
  //	    return aitSprintf("%02d",mStruct.day);
  case 'H':
  {
	  std::stringstream tstr;
	  tstr << std::setprecision(2) << std::setfill('0') << std::setw(2) << mStruct.hour;
	  return tstr.str();
  }
//    return aitSprintf("%02d",mStruct.hour);
  case 'm':
  {
	  std::stringstream tstr;
	  tstr << std::setprecision(2) << std::setfill('0') <<std::setw(2) <<  mStruct.month;
	  return tstr.str();
  }
//    return aitSprintf("%02d",mStruct.month);
  case 'M':
  {
	  std::stringstream tstr;
	  tstr << std::setprecision(2) << std::setfill('0') << std::setw(2) << mStruct.minute;
	  return tstr.str();
  }
//    return aitSprintf("%02d",mStruct.minute);
  case 'S':
  {
	  std::stringstream tstr;
	  tstr << std::setprecision(2) << std::setfill('0') << std::setw(2) << floor(mStruct.second);
	  return tstr.str();
  }
//    return aitSprintf("%02d",(int)mStruct.second);
  case 'Y':
  {
	  std::stringstream tstr;
	  tstr << std::setprecision(4) << std::setfill('0') << std::setw(4) << mStruct.year;
	  return tstr.str();
  }
//    return aitSprintf("%04d",mStruct.year);
  case 'f':
  {
	  std::stringstream tstr;
	  tstr << std::fixed << std::setprecision(6) << std::setfill('0') << std::setw(6) << (int)((mStruct.second - floor(mStruct.second))*1000000);
	  return tstr.str();
  }
//    return aitSprintf("%03d",(int)((mStruct.second - (int)mStruct.second)*1000));
  case '%':
    return "%";
  default:
//    PvUtil::exitError("Invalid code or not implemented.");
	  VML_ERROR(MsgCode::notImplementedErr,"Invalid code or not implemented.");
  }

  return "Invalid";
}

std::string 
DateTime::format(const std::string fmt) const
{
  std::string out;
  for (unsigned int i = 0; i < fmt.length(); i++)
  {
    if (fmt[i] == '%')
    {
      if (i == fmt.length()-1) VML_ERROR(MsgCode::notSupportedErr,"Format ends in %%");
      out += getElem(fmt[i+1]);
      i++;
    }
    else
    {
      out += fmt[i];
    }
  }
  return out;
}

double 
DateTime::utcToSecs() const
{
  return ::toSeconds(mStruct,false);
}

double 
DateTime::toSeconds() const
{
  return ::toSeconds(mStruct,true);
}

std::string
DateTime::toString() const
{
  return format(DateTime::vmsFormatStr);
}

DateTime
DateTime::fromString(std::string str)
{
  DateTime dt(
    atoi(str.substr(0,4).c_str()),
    atoi(str.substr(5,2).c_str()),
    atoi(str.substr(8,2).c_str()),
    atoi(str.substr(11,2).c_str()),
    atoi(str.substr(14,2).c_str()),
    atof(str.substr(17,6).c_str())
    );
  return dt;
}

DateTime 
DateTime::findNextRoundTime(const DateTime& start, double intervalInSecs)
{
  DateTime dt0(start.getStruct().year,start.getStruct().month,start.getStruct().day,0,0,0);
  double roundStart = dt0.toSeconds();
  double startSeconds = start.toSeconds();

  if (intervalInSecs > 60*60*24)
  {
//    throw std::bad_exception("Maximum number of seconds is 86400 (24hrs)");
	  throw std::bad_exception();
  }

  if (startSeconds == roundStart)
  {
    return roundStart + intervalInSecs;
  }

  int numIntervals = (int)((startSeconds-roundStart)/intervalInSecs);

  return roundStart + intervalInSecs*(numIntervals+1);
}

//
// ACCESS
//

//
// INQUIRY
//

}; // namespace ait


/*
 * ParseConfigTxt.cpp
 *
 *  Created on: May 18, 2016
 *      Author: paulshin
 */

#include "modules/utility/ParseConfigTxt.hpp"

#include <fstream>
#include <iostream>
#include <vector>

#include <boost/algorithm/string.hpp>

using std::string;
using std::ifstream;
using std::map;
using std::vector;

using std::cerr;
using std::endl;

using boost::algorithm::trim;
using boost::algorithm::split;

ParseConfigTxt::ParseConfigTxt(const string& configpath) {
	parse(configpath);
};

void ParseConfigTxt::parse(const string& configpath) {

	ifstream configfile;
	string line;
	vector<string> tokens;
	unsigned int lineno = 0;

	//configfile.exceptions(ifstream::failbit);
	configfile.open(configpath.c_str());

	if (configfile){
		while (getline(configfile, line)){
			++lineno;
			// find a comment and remove it from the string
			line = line.substr(0, line.find("#"));

			// trim whitespace from the string
			trim(line);

			// split the string by whitespace, but ignore empty lines
			if(line.size() > 0){
				tokens.clear();
				split(tokens, line, boost::is_any_of("\t "), boost::token_compress_on);

				// NOTE: because we trimmed, we know it is impossible for us to have a token of size 0
				if(tokens.size() == 1){
					cerr << "[ParseConfigTxt(" << configpath << ")] Warning: Parameter '"<< tokens[0]
					     << "' on line " << lineno << " has no value, ignoring" << endl;
				} else {
					if(mParams.count(tokens[0])){
						cerr << "[ParseConfigTxt(" << configpath << ")] Warning: duplicate parameter '" << tokens[0]
						     << "' found on line " << lineno << ", overwriting value" << endl;
					}
					if(tokens.size() > 2){
						cerr << "[ParseConfigTxt (" << configpath << ")] Warning: Parameter '" << tokens[0]
						     << "' found with multiple values on line " << lineno << ", ignoring extra values" << endl;
					}

					// set the map to associate the key-value pair
					mParams[tokens[0]] = tokens[1];
				}
			}
		}
	} else {
		cerr << "[ParseConfigTxt] Error: Config file " << configpath << " not found." << endl;
	}
}

bool ParseConfigTxt::getValue_charArr(const std::string& param, char *value, unsigned int buffLength) const{
	string tmp_ret;
	bool success = getValue(param, tmp_ret);

	// Note the strict equality; length() does not include the null terminator in the size
	if(success && buffLength > tmp_ret.length()){
		strcpy(value, tmp_ret.c_str());
	}else{
		success = false;
	}

	return success;
}




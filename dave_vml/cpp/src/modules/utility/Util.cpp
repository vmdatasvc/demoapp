/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
/*
 * Util.cpp
 *
 *  Created on: Oct 1, 2015
 *      Author: yyoon
 */

#include <iostream>

#include <modules/utility/Util.hpp>
#include <modules/utility/Serialize.hpp>


namespace vml
{

// saving raw matrices in binary serialization
// can be read in matlab using mexReadRawMat

void save_raw(const cv::Mat &mat,
              const char* savename)
{
	serializer tser;
	tser.serialize_cvMatGen(mat);
	tser.save(savename);
}

template <typename T>
void save_raw(const T *map,
		      const int height,
		      const int width,
		      const int nbands,
		      const char* savename)
{
	cv::Mat mat(height,width,CV_MAKETYPE(cv::DataType<T>::type, nbands),(void*)map);
	save_raw(mat,savename);
}

// explicit template instantiation
template
void save_raw(const uchar *map,
		      const int height,
		      const int width,
		      const int nbands,
		      const char* savename);

template
void save_raw(const char *map,
		      const int height,
		      const int width,
		      const int nbands,
		      const char* savename);

template
void save_raw(const ushort *map,
		      const int height,
		      const int width,
		      const int nbands,
		      const char* savename);

template
void save_raw(const short *map,
		      const int height,
		      const int width,
		      const int nbands,
		      const char* savename);

template
void save_raw(const int *map,
		      const int height,
		      const int width,
		      const int nbands,
		      const char* savename);

template
void save_raw(const float *map,
		      const int height,
		      const int width,
		      const int nbands,
		      const char* savename);

template
void save_raw(const double *map,
		      const int height,
		      const int width,
		      const int nbands,
		      const char* savename);

// saving arrays
template <typename T>
void save_raw(const T *data,
              const int size,
              const char* savename)
{
	cv::Mat mat(1,size,CV_MAKETYPE(cv::DataType<T>::type,1),(void*)data);
	save_raw(mat,savename);
}

// explicit template instantiation
template
void save_raw(const uchar *data,
              const int size,
              const char* savename);

template
void save_raw(const char *data,
              const int size,
              const char* savename);

template
void save_raw(const ushort *data,
              const int size,
              const char* savename);

template
void save_raw(const short *data,
              const int size,
              const char* savename);

template
void save_raw(const int *data,
              const int size,
              const char* savename);

template
void save_raw(const float *data,
              const int size,
              const char* savename);

template
void save_raw(const double *data,
              const int size,
              const char* savename);

template <typename T>
void save_raw(const T **data,
			  const int dim1,
			  const int dim2,
			  const char *savename)
{
	cv::Mat mat(dim1,dim2,CV_MAKETYPE(cv::DataType<T>::type,1));
	for (int i=0;i<dim1;i++)
	{
		T*	mat_p = mat.ptr<T>(i);
		for (int j=0;j<dim2;j++)
		{
			mat_p[j] = data[i][j];
		}
	}

	save_raw(mat,savename);
}

// explicit instantiation
template
void save_raw(const uchar **data,
			  const int dim1,
			  const int dim2,
			  const char *savename);

template
void save_raw(const char **data,
			  const int dim1,
			  const int dim2,
			  const char *savename);

template
void save_raw(const ushort **data,
			  const int dim1,
			  const int dim2,
			  const char *savename);

template
void save_raw(const short **data,
			  const int dim1,
			  const int dim2,
			  const char *savename);

template
void save_raw(const int **data,
			  const int dim1,
			  const int dim2,
			  const char *savename);

template
void save_raw(const float **data,
			  const int dim1,
			  const int dim2,
			  const char *savename);

template
void save_raw(const double **data,
			  const int dim1,
			  const int dim2,
			  const char *savename);

} // of namespace vml


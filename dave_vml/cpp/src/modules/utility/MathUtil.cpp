/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
/*
 * MathUtil.cpp
 *
 *  Created on: Mar 10, 2016
 *      Author: yyoon
 */

#include <modules/utility/MathUtil.hpp>
#include <opencv2/imgproc.hpp>

namespace vml
{

float MahalanobisDistance(float x1, float y1, float x2, float y2, float Sinv[])
{
	float dx = x2-x1;
	float dy = y2-y1;
	return sqrt(dx*dx*Sinv[0]+2.f*dx*dy*Sinv[1]+dy*dy*Sinv[2]);
}

bool calcContourAxes(std::vector<cv::Point> &contour, cv::Point2f &major, cv::Point2f &minor, float &major_length, float &minor_length)
{

	if (contour.size() < 3)
	{
		return false;
	}

	// calculate moments of contour
	cv::Moments	mm = cv::moments(contour);

	// calculate cov matrix
	cv::Mat_<float>  cov_mat(2,2);

	float denom = mm.m00-1.f;
	cov_mat.at<float>(0,0) = mm.mu20/denom;
	cov_mat.at<float>(0,1) = cov_mat.at<float>(1,0) = mm.mu11/denom;
	cov_mat.at<float>(1,1) = mm.mu02/denom;

	cv::Mat_<float> eigen_vectors;
	cv::Vec2f eigen_values;
	bool success = cv::eigen(cov_mat,eigen_values,eigen_vectors);

	if (success)
	{
		major.x = eigen_vectors.at<float>(0,0);
		major.y = eigen_vectors.at<float>(0,1);
		minor.x = eigen_vectors.at<float>(1,0);
		minor.y = eigen_vectors.at<float>(1,1);
		major_length = sqrt(eigen_values(0));
		minor_length = sqrt(eigen_values(1));
	}

	return success;
}


#if 0
// code for calculating 2-D Gaussian kernel from major and minor axis vectors
void calcLocationUncertainty()
{
	if ((mDeltaPosition.x == 0.f)&&(mDeltaPosition.y==0.f))
	{
		float denom = mPosUncertainty*mPosBandwidth;
		denom *= denom; // make it square for cov.
		mLocationUncertaintyInv[0] = mLocationUncertaintyInv[2] = 1.f/denom;
		mLocationUncertaintyInv[1] = 0.f;
	}
	else
	{
		float dx_sq = mDeltaPosition.x*mDeltaPosition.x;
		float dy_sq = mDeltaPosition.y*mDeltaPosition.y;
		float dp_norm_sq = dx_sq+dy_sq;
		float cos_sq = dx_sq/dp_norm_sq;
		float sin_sq = dy_sq/dp_norm_sq;
		float sincos = mDeltaPosition.x*mDeltaPosition.y/dp_norm_sq;

		float l2 = mPosUncertainty;
		float l1 = sqrt(dp_norm_sq)>l2?sqrt(dp_norm_sq):l2;

		mLocationUncertaintyInv[0] = (sin_sq/l2+cos_sq/l1)/mPosBandwidth;
		mLocationUncertaintyInv[1] = (1.f/l1-1.f/l2)*sincos/mPosBandwidth;
		mLocationUncertaintyInv[2] = (cos_sq/l2+sin_sq/l1)/mPosBandwidth;
	}
}

float calculateLocationLikelihood(cv::Point pos)
{
	cv::Point	np = mKFPrediction;

	float dx = (float)(pos.x-np.x);
	float dy = (float)(pos.y-np.y);

	return (exp(-(mLocationUncertaintyInv[0]*dx*dx+2.f*mLocationUncertaintyInv[1]*dx*dy+mLocationUncertaintyInv[2]*dy*dy)));
}

float calculateLocationLikelihood(int x, int y)
{
	cv::Point	np = mKFPrediction;

	float dx = (float)(x-np.x);
	float dy = (float)(y-np.y);

	return (exp(-(mLocationUncertaintyInv[0]*dx*dx+2.f*mLocationUncertaintyInv[1]*dx*dy+mLocationUncertaintyInv[2]*dy*dy)));
}
#endif

} // of namespace vml


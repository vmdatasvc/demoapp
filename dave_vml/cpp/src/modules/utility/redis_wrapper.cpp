#include <iostream>
#include <hiredis/hiredis.h>
#include <string>
#include <vector>
#include <algorithm>
#include "modules/utility/redis_wrapper.hpp"
using std::string;
using std::cout;
using std::endl;
using std::vector;

/* Constructor sets the default connection configuration*/
redis_wrapper::redis_wrapper() {
	redis_host = "127.0.0.1";
	redis_port = 6379;
	connected = false;
}

redis_wrapper::~redis_wrapper() {
	if (connected) redisFree(c);
}

bool redis_wrapper::checkConnection() {
	int rc = 0;
	if (!connected)
		rc =connect();
	if (rc == 0) {
		redisReply *reply;
		reply = (redisReply*) redisCommand(c, "PING");
		if (reply && reply->type != REDIS_REPLY_ERROR) {
			//cout << "Connected" << endl;
			connected = true;
			freeReplyObject(reply);
		}
		else {
			connected = false;
		}
	}
	return connected;
}

int redis_wrapper::connect() {
	int rc = 0;
	struct timeval timeout = { 1, 500000 }; // 1.5 seconds
	c = redisConnectWithTimeout(redis_host.c_str(), redis_port, timeout);
	if (c == NULL || c->err) {
		rc = 1;
		if (c) {
	    	cout << "Connection to redis error: " << string(c->errstr) << endl;
	    	redisFree(c);
		} else {
			cout << "Connection to redis error: can't allocate redis context" << endl;;
		}
		connected = false;
	}
	else {
		connected = true;
	}
	return rc;
}

void redis_wrapper::disconnect() {
	if(connected) {
		connected = false;
		redisFree(c);
	}
}

int redis_wrapper::set(string key, string val) {
	redisReply *reply;
	int rc = 0;
	if (checkConnection()) {
		reply = (redisReply*) redisCommand(c, "SET %s %s", key.c_str(), val.c_str());
		if (reply->type == REDIS_REPLY_ERROR) {
			rc = 1;
			cout << "Failed to set key \"" << key << "\"" << endl;
		}
		else {
			//cout << "Set successful for key \"" << key << "\"" << endl;
		}
		freeReplyObject(reply);
	}
	return rc;
}

int redis_wrapper::getInteger(string key) {
	int ret;
	ret = std::stoi(getString(key));
	return ret;
}

string redis_wrapper::getString(string key) {
	redisReply *reply;
	string ret = "";
	if (checkConnection()) {
		reply = (redisReply*) redisCommand(c, "GET %s", key.c_str());
		if (reply->type == REDIS_REPLY_STRING) {
			ret = reply->str;
		}
		else {
			cout << "Did not receive string in key \"" << key << "\"" << endl;
			cout << "Reply type " << reply->type << endl;
		}
		freeReplyObject(reply);
	}
	return ret;
}

int redis_wrapper::list_lpush(string key, string val) {
	int rc = 0;
	redisReply *reply;
	if (checkConnection()) {
		reply = (redisReply*) redisCommand(c, "LPUSH %s %s", key.c_str(), val.c_str());
		if (reply->type == REDIS_REPLY_ERROR) {
			rc = 1;
			cout << "Failed to LPUSH key \"" << key << "\"" << endl;
		}
		else {
			//cout << "LPUSH successful for key \"" << key << "\"" << endl;
		}
		freeReplyObject(reply);
	}
	return rc;
}

int redis_wrapper::list_rpush(string key, string val) {
	int rc = 0;
	redisReply *reply;
	if (checkConnection()) {
		reply = (redisReply*) redisCommand(c, "RPUSH %s %s", key.c_str(), val.c_str());
		if (reply->type == REDIS_REPLY_ERROR) {
			rc = 1;
			cout << "Failed to RPUSH key \"" << key << "\"" << endl;
		}
		else {
			//cout << "RPUSH successful for key \"" << key << "\"" << endl;
		}
		freeReplyObject(reply);
	}
	return rc;
}

string redis_wrapper::list_lpop(string key) {
	string ret = "";
	redisReply *reply;
	if (checkConnection()) {
		reply = (redisReply*) redisCommand(c, "LPOP %s", key.c_str());
		if(reply->type != REDIS_REPLY_ERROR) {
			if (reply->type == REDIS_REPLY_STRING) {
				ret = reply->str;
			}
			else {
				cout << "LPOP element not a string" << endl;
			}
		}
		else {
			cout << "Failed to LPOP key \"" << key << "\"" << endl;
		}
		freeReplyObject(reply);
	}
	return ret;
}

string redis_wrapper::list_rpop(string key) {
	string ret = "";
	redisReply *reply;
	if (checkConnection()) {
		reply = (redisReply*) redisCommand(c, "RPOP %s", key.c_str());
		if (reply->type != REDIS_REPLY_ERROR) {
			if (reply->type == REDIS_REPLY_STRING) {
				ret = reply->str;
			}
			else {
				cout << "RPOP element not a string" << endl;
			}
		}
		else {
			cout << "Failed to RPOP key \"" << key << "\"" << endl;
		}
		freeReplyObject(reply);
	}
	return ret;
}

string redis_wrapper::list_select(string key, int idx) {
	redisReply *reply;
	string ret = "";
	if (checkConnection()) {
		reply = (redisReply*) redisCommand(c, "LINDEX %s %d", key.c_str(), idx);
		if (reply->type != REDIS_REPLY_ERROR) {
			if (reply->type == REDIS_REPLY_STRING) {
				ret = reply->str;
			}
		}
		else {
			cout << "Failed to select element in key \"" << key << "\"" << endl;
		}
		freeReplyObject(reply);
	}
	return ret;
}

vector<string> redis_wrapper::list_range(string key, int idx1, int idx2) {
	redisReply *reply;
	vector<string> ret;
	if (checkConnection()) {
		reply = (redisReply*) redisCommand(c, "LRANGE %s %d %d", key.c_str(), idx1, idx2);
		if (reply->type != REDIS_REPLY_ERROR) {
			if (reply->type == REDIS_REPLY_ARRAY) {
				string temp;
				for (size_t i = 0; i < reply->elements; i++) {
					temp = (reply->element)[i]->str;
					ret.push_back(temp);
				}
			}
		}
		else {
			cout << "Failed to select elements in key \"" << key << "\"" << endl;
		}
		freeReplyObject(reply);
	}
	return ret;
}

int redis_wrapper::list_len(string key) {
	int ret;
	redisReply *reply;
	if (checkConnection()) {
		reply = (redisReply*) redisCommand(c, "LLEN %s", key.c_str());
		if (reply->type != REDIS_REPLY_ERROR) {
			if (reply->type == REDIS_REPLY_INTEGER) {
				ret = reply->integer;
			}
		}
		else {
			cout << "Failed to get element count of key \"" << key << "\"" << endl;
		}
		freeReplyObject(reply);
	}
	return ret;
}

int redis_wrapper::list_set(string key, int idx, string val) {
	int rc = 0;
	redisReply *reply;
	if (checkConnection()) {
		reply = (redisReply*) redisCommand(c, "LSET %s %d %s", key.c_str(), idx, val.c_str());
		if (reply->type == REDIS_REPLY_ERROR) {
			rc = 1;
			cout << "Failed to LSET key \"" << key << "\"" << endl;
		}
		else {
			//cout << "LSET successful for key \"" << key << "\"" << endl;
		}
		freeReplyObject(reply);
	}
	return rc;
}

void redis_wrapper::del(string key) {
	redisReply *reply;
	if (checkConnection()) {
		reply = (redisReply*) redisCommand(c, "DEL %s", key.c_str());
		if (reply->type != REDIS_REPLY_ERROR) {
			cout << "Key \"" << key << "\" in redis database is deleted";
		}
		else {
			cout << "Failed to delete key \"" << key << "\" in redis database";
		}
		freeReplyObject(reply);
	}
}

void* redis_wrapper::command(const char *format) {
	return redisCommand(c, format);
}


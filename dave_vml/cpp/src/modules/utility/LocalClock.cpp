/*
 * LocalClock.cpp
 *
 *  Created on: Sep 9, 2015
 *      Author: pshin
 */

#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#include "modules/utility/LocalClock.hpp"

LocalClock::LocalClock() {

	/* Obtain the time of day, and convert it to a tm struct. */
	gettimeofday (&mInitTv, NULL);
	mInitPtm = localtime (&mInitTv.tv_sec);
	mInitDateTime_lf = mInitTv.tv_sec+(mInitTv.tv_usec/1000000.0);		// -- Unit: sec
}

LocalClock::~LocalClock() {
}

void LocalClock::updateTime(double datetime_lf, int precision)
{
	char time_string[99];
	long milliseconds;

	/* Obtain the time of day, and convert it to a tm struct. */
	if(datetime_lf < 0) {
		gettimeofday (&mTv, NULL);
	} else {
		mTv.tv_sec = (int)datetime_lf;
		mTv.tv_usec = (datetime_lf - mTv.tv_sec)*1000*1000;
	}

	mPtm = localtime (&mTv.tv_sec);
	mDateTime_lf = mTv.tv_sec+(mTv.tv_usec/1000000.0);		// -- Unit: sec
	/* Format the date and time, down to a single second. */
	strftime (time_string, sizeof (time_string), "%Y-%m-%d %H:%M:%S", mPtm);

	/* Compute milliseconds from microseconds. */
	milliseconds = mTv.tv_usec / 1000;

	/* Print the formatted time, in seconds, followed by a decimal point
	and the milliseconds. */
	char dateTime[99], timeOnly[99];

	if(precision == 0) {
		sprintf(dateTime, "%s", time_string);
	} else if(precision == 1) {
		sprintf(dateTime, "%s.%ld", time_string, milliseconds/100);
	} else if(precision == 2) {
		sprintf(dateTime, "%s.%ld", time_string, milliseconds/10);
	} else {
		//sprintf(dateTime, "%s.%03ld", time_string, milliseconds);
		sprintf(dateTime, "%s.%ld", time_string, milliseconds);
	}

	mDateTime_str.clear();
	mDateTime_str.append(dateTime);

	strftime (time_string, sizeof (time_string), "%Y-%m-%d", mPtm);
	mDateOnly_str.clear();
	mDateOnly_str.append(time_string);

	strftime (time_string, sizeof (time_string), "%H:%M:%S", mPtm);

	if(precision == 0) {
		sprintf(timeOnly, "%s", time_string);
	} else if(precision == 1) {
		sprintf(timeOnly, "%s.%01ld", time_string, milliseconds);
	} else if(precision == 2) {
		sprintf(timeOnly, "%s.%02ld", time_string, milliseconds);
	} else {
		sprintf(timeOnly, "%s.%03ld", time_string, milliseconds);
	}

	mTimeOnly_str.clear();
	mTimeOnly_str.append(time_string);
	mTimeOnly_str.append(dateTime);
}



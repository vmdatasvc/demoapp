#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include "unistd.h"

#include "modules/utility/TimeKeeper.hpp"

static double start_time;
static double saved_time;


void gStartTime()
{
   start_time = -1;
   start_time = saved_time = gCurrentTime();
}


void gSaveTime()
{
   saved_time = gCurrentTime();
}

void gSetSaveTime(double newTime)
{
   saved_time = newTime;
}

void gSetStartTime(double newTime)
{
	start_time = newTime;
}

void gPrintTime()
{
   double prev_time = saved_time;
   gSaveTime();
   printf("%lf\n", saved_time - prev_time);
}

void gPrintTimeSinceLastSaved()
{
   printf("%lf\n", gCurrentTime() - saved_time);
}

double gTimeSinceLastSaved()
{
   return (gCurrentTime() - saved_time);   
}

void gPrintTimeSinceStart()
{
   printf("%lf\n", gCurrentTime() - start_time);
}

double gTimeSinceStart()
{
   return (gCurrentTime() - start_time);   
}


double gCurrentTime()
{
   timeval tv;   
   while(gettimeofday(&tv, NULL) != 0) {
	   usleep(10);
   }
   
   return ((double)(tv.tv_sec) + (double)(tv.tv_usec) / 1000000.0);    
}



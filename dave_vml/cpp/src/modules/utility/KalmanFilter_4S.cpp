/*
 * KalmanFilter_4S.cpp
 *
 *  Created on: Dec 11, 2011
 *      Author: paulshin
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "modules/utility/KalmanFilter_4S.hpp"
#include "modules/utility/TimeKeeper.hpp"
#include "modules/utility/matrix.h"
//#include "Config/Config.hpp"

#define	KALMAN_TIME_THRESHOLD	300		// -- Unit: sec
//#define mKalmanTimeScale			30		// -- Time unit transform

KalmanFilter_4S::KalmanFilter_4S() {
		mInitTime = gCurrentTime();				// -- Unit: sec
		mMeasurementTime_last = mInitTime;		// -- Unit: sec
		mMeasurementTime_init = mInitTime;
		mKalmanProcessNoiseVariance = 10000*10000;
		mKalmanPredictNoiseVariance = 10000*10000;
		mKalmanMeasurementNoiseVariance = 10000*10000;
		mNoOfUpdatesSoFar = 0;
		mMinCovCoefficient_pos = 1.0;
		mMinCovCoefficient_accel = 1.0;

		mNeedToInitialize = true;

		DeepInitialize();		// -- Initial distance = 30 meter
}
KalmanFilter_4S::~KalmanFilter_4S() {}

void KalmanFilter_4S::ResetColor() {
	mColor.r = rand()%200+50;
	mColor.g = rand()%200+50;
	mColor.b = rand()%200+50;
}

void KalmanFilter_4S::DeepInitialize(double kalmanProcessNoiseVariance_t, double kalmanMeasurementNoiseVariance_t, double kalmanTimeScale, double minCovCoefficient_pos, double minCovCoefficient_accel, double x, double y, double P11, double P33) {
	mKalmanProcessNoiseVariance = kalmanProcessNoiseVariance_t;
	mKalmanMeasurementNoiseVariance = kalmanMeasurementNoiseVariance_t;
	mKalmanTimeScale = kalmanTimeScale;
	mMinCovCoefficient_pos = minCovCoefficient_pos;
	mMinCovCoefficient_accel = minCovCoefficient_accel;

	mCenterOfSensingField_X = -9999;
	mCenterOfSensingField_Y = -9999;

	mInitTime = gCurrentTime();				// -- Unit: sec
	mMeasurementTime_last = mInitTime;		// -- Unit: sec
	mMeasurementTime_init = mInitTime;		// -- Unit: sec
	mNoOfUpdatesSoFar = 0;

	ResetColor();

	Initialize(x, y, P11, P33);
}

void KalmanFilter_4S::Initialize(double x, double y, double P11, double P33) {
	double tau = KALMAN_TIME_THRESHOLD* mKalmanTimeScale;

	double x_0[4] = {x, y, 	0, 	0};
	double P_0[16] = {	P11, 	0, 		0, 		0,
						0, 		P11, 	0,		0,
						0, 		0,		P33, 	0,
						0, 		0, 		0, 		P33};
	double Phi_0[16] = {	1,		0,		tau,	0,
							0,		1,		0,		tau,
							0,		0,		1,		0,
							0,		0,		0,		1};
	double G_0[8] = {	tau*tau/2.0,	0,
						0,				tau*tau/2.0,
						tau,			0,
						0,				tau};

	double H_0[8] = {1,		0,		0,		0,
					0,		1,		0,		0};

	Q[0][0] = Q[1][1] = mKalmanProcessNoiseVariance;
	Q[0][1] = Q[1][0] = 0;

	R[0][0] = R[1][1] = mKalmanMeasurementNoiseVariance;
	R[0][1] = R[1][0] = 0;

	memcpy(x_h_update, x_0, 4*sizeof(double));
	memcpy(P_update, P_0, 16*sizeof(double));
	memcpy(Phi, Phi_0, 16*sizeof(double));
	memcpy(G, G_0, 8*sizeof(double));
	memcpy(H, H_0, 8*sizeof(double));

//	if(0) {
//		printf("MOTE[%d] KF_4S:Initialize() -> x_h_update = ", mId);
//		for(int m=0; m<4; m++) {
//			printf(" %7.2f", x_h_update[m]);
//		}
//		printf("\n");
//	}

}

double KalmanFilter_4S::GetTimeElapsedFromLastUpdate() {
	return (gCurrentTime() - mInitTime) - mMeasurementTime_last;		// -- Unit: sec
}

double KalmanFilter_4S::GetTimeElapsedFromInitialUpdate() {
	return (gCurrentTime() - mInitTime) - mMeasurementTime_init;		// -- Unit: sec
}

double KalmanFilter_4S::GetTimeElapsedFromInitialUpdateToLastUpdate() {
	return mMeasurementTime_last - mMeasurementTime_init;		// -- Unit: sec
}

double KalmanFilter_4S::GetTimeElapsedFromInitialCreation() {
	return (gCurrentTime() - mInitTime);		// -- Unit: sec
}


void KalmanFilter_4S::PredictForPrediction() {

	double tau = (double)((gCurrentTime() - mInitTime) - mMeasurementTime_last)* mKalmanTimeScale;		// -- Unit: sec

		double Phi_0[16] = {1,		0,		tau,	0,
							0,		1,		0,		tau,
							0,		0,		1,		0,
							0,		0,		0,		1};

		double G_0[8] = {	tau*tau/2.0,	0,
							0,				tau*tau/2.0,
							tau,			0,
							0,				tau};

		memcpy(Phi_predict, Phi_0, 16*sizeof(double));
		memcpy(G, G_0, 8*sizeof(double));

		// -- x_h_predictpredict = Phi_predict*x_h_update;
		matrixMult(&Phi_predict[0][0], 4, 4, x_h_update, 4, 1, x_h_predictpredict, 1, N_TRANSPOSE);

		// -- P_predictpredict = Phi_predict*P_update*Phi_predict.t() + Phi_predict*G*Q*G.t()*Phi_predict.t()*tau;
		double tmp0[16], tmp1[16], tmp2[8], tmp3[8];
		matrixMult(&Phi_predict[0][0], 4, 4, &P_update[0][0], 4, 4, tmp0, 1, N_TRANSPOSE);	// -- tmp0 = Phi_predict*P_update
		matrixMult(tmp0, 4, 4, &Phi_predict[0][0], 4, 4, tmp1, 1, TRANSPOSE);				// -- tmp1 = Phi_predict*P_update*(Phi_predict)'
		matrixMult(&Phi_predict[0][0], 4, 4, &G[0][0], 4, 2, tmp2, 1, N_TRANSPOSE);			// -- tmp2 = Phi_predict*G
		matrixMult(tmp2, 4, 2, &Q[0][0], 2, 2, tmp3, 1, N_TRANSPOSE);						// -- tmp3 = Phi_predict*G*Q
		matrixMult(tmp3, 4, 2, tmp2, 4, 2, tmp0, tau, TRANSPOSE);							// -- tmp0 = Phi_predict*G*Q*(Phi_predict*G)'*tau
		matrixAdd(tmp1, tmp0, 4, 4, &P_predictpredict[0][0]);								// -- P_predictpredict = Phi_predict*P_update*(Phi_predict)' + Phi_predict*G*Q*(Phi_predict*G)'*tau
}

void KalmanFilter_4S::PredictForUpdate(double x, double y) {


	double tau = (double)((gCurrentTime() - mInitTime) - mMeasurementTime_last)* mKalmanTimeScale;		// -- Unit: sec

	if(tau > KALMAN_TIME_THRESHOLD* mKalmanTimeScale) {
		tau = KALMAN_TIME_THRESHOLD* mKalmanTimeScale;
		Initialize(x, y);
	}

		double Phi_0[16] = {	1,		0,		tau,	0,
								0,		1,		0,		tau,
								0,		0,		1,		0,
								0,		0,		0,		1};

		double G_0[8] = {	tau*tau/2.0,	0,
							0,				tau*tau/2.0,
							tau,			0,
							0,				tau};

		memcpy(Phi_predict, Phi_0, 16*sizeof(double));
		memcpy(G, G_0, 8*sizeof(double));

		// -- x_h_predict = Phi_predict*x_h_update;
		matrixMult(&Phi_predict[0][0], 4, 4, x_h_update, 4, 1, x_h_predict, 1, N_TRANSPOSE);

		// -- P_predict = Phi_predict*P_update*Phi_predict.t() + Phi_predict*G*Q*G.t()*Phi_predict.t()*tau;
		double tmp0[16], tmp1[16], tmp2[8], tmp3[8];
		matrixMult(&Phi_predict[0][0], 4, 4, &P_update[0][0], 4, 4, tmp0, 1, N_TRANSPOSE);	// -- tmp0 = Phi_predict*P_update
		matrixMult(tmp0, 4, 4, &Phi_predict[0][0], 4, 4, tmp1, 1, TRANSPOSE);				// -- tmp1 = Phi_predict*P_update*(Phi_predict)'
		matrixMult(&Phi_predict[0][0], 4, 4, &G[0][0], 4, 2, tmp2, 1, N_TRANSPOSE);			// -- tmp2 = Phi_predict*G
		matrixMult(tmp2, 4, 2, &Q[0][0], 2, 2, tmp3, 1, N_TRANSPOSE);						// -- tmp3 = Phi_predict*G*Q
		matrixMult(tmp3, 4, 2, tmp2, 4, 2, tmp0, tau, TRANSPOSE);							// -- tmp0 = Phi_predict*G*Q*(Phi_predict*G)'*tau
		matrixAdd(tmp1, tmp0, 4, 4, &P_predict[0][0]);										// -- P_predict = Phi_predict*P_update*Phi_predict.t() + Phi_predict*G*Q*G.t()*Phi_predict.t()*tau

}

void KalmanFilter_4S::Update(double x, double y, double measurementNoiseScaleFactor) {

	if(mNoOfUpdatesSoFar == 0) {
		Initialize(x, y);
		mMeasurementTime_init = gCurrentTime() - mInitTime;		// -- Unit: sec
	}

	mMeasurementTime_last = gCurrentTime() - mInitTime;		// -- Unit: sec
	mNoOfUpdatesSoFar++;

	Update_wo_timstamping(x, y, measurementNoiseScaleFactor);

}

/*
 *  measurementNoiseScaleFactor is used to adjust the uncertainty of the measurement input.
 *  If higher than 1, then its uncertainty is larger than the default measurement noise variance
 *  It's unit is the square of the data unit.
 */

void KalmanFilter_4S::Update_wo_timstamping(double x, double y, double measurementNoiseScaleFactor) {

	PredictForUpdate(x, y);

	z[0] = (double)x;
	z[1] = (double)y;

	double R_[2][2];
	//memcpy(R_,R,sizeof(double)*4);
	matrixScale(&R[0][0], measurementNoiseScaleFactor, 2, 2, &R_[0][0]);

//	R_[0][0] *= measurementNoiseScaleFactor;
//	R_[1][1] *= measurementNoiseScaleFactor;


	//Kgain = P_predict*H.t()*(H*P_predict*H.t() + R).i();
	{
		double tmp0[8], tmp1[4], tmp2[4];
		matrixMult(&P_predict[0][0], 4, 4, &H[0][0], 2, 4, tmp0, 1, TRANSPOSE);	// -- tmp0 = P_predict*H.t()
		matrixMult(&H[0][0], 2, 4, tmp0, 4, 2, tmp1, 1, N_TRANSPOSE);			// -- tmp1 = H*P_predict*H.t()
		matrixAdd(tmp1, &R_[0][0], 2, 2, tmp2);									// -- tmp2 = H*P_predict*H.t() + R
		matrixInv2x2(tmp2, tmp1);												// -- tmp1 = (H*P_predict*H.t() + R).i()
		matrixMult(tmp0, 4, 2, tmp1, 2, 2, &Kgain[0][0], 1, N_TRANSPOSE);
	}

	//x_h_update = x_h_predict + Kgain*(z - H*x_h_predict);
	{
		double tmp0[2], tmp1[2], tmp2[4];
		matrixMult(&H[0][0], 2, 4, x_h_predict, 4, 1, tmp0, 1, N_TRANSPOSE);	// -- tmp0 = H*x_h_predict
		matrixSub(z, tmp0, 2, 1, tmp1);											// -- tmp1 = z - H*x_h_predict
		matrixMult(&Kgain[0][0], 4, 2, tmp1, 2, 1, tmp2, 1, N_TRANSPOSE);		// -- tmp2 = Kgain*(z - H*x_h_predict)
		matrixAdd(x_h_predict, tmp2, 4, 1, x_h_update);							// -- x_h_update = x_h_predict + Kgain*(z - H*x_h_predict)
	}

	//P_update = P_predict - Kgain*H*P_predict;
	{
		double tmp0[8], tmp1[16];
		matrixMult(&H[0][0], 2, 4, &P_predict[0][0], 4, 4, tmp0, 1, N_TRANSPOSE);		// -- tmp0 = H*P_predict
		matrixMult(&Kgain[0][0], 4, 2, tmp0, 2, 4, tmp1, 1, N_TRANSPOSE);				// -- tmp1 = Kgain*H*P_predict
		matrixSub(&P_predict[0][0], tmp1, 4, 4, &P_update[0][0]);						// -- P_update = P_predict - Kgain*H*P_predict
	}

	// -- Set the minimum variance of the state
	{
		for(int m=0; m<2; m++)
			if(P_update[m][m] < mMinCovCoefficient_pos) P_update[m][m] = mMinCovCoefficient_pos;
		for(int m=2; m<4; m++)
			if(P_update[m][m] < mMinCovCoefficient_accel) P_update[m][m] = mMinCovCoefficient_accel;

	}

//	if(0) {
//
//		printf("MOTE[%d] KF_4S:Update() -> Kgain = \n", mId);
//		for(int m=0; m<2; m++) {
//			for(int n=0; n<4; n++) {
//				printf(" %7.2f", Kgain[n][m]);
//			}
//			printf("\n");
//		}
//
//		printf("MOTE[%d] KF_4S:Update() -> P_predict = \n", mId);
//		for(int m=0; m<4; m++) {
//			for(int n=0; n<4; n++) {
//				printf(" %7.2f", P_predict[n][m]);
//			}
//			printf("\n");
//		}
//
//		printf("MOTE[%d] KF_4S:Update() -> x_h_predict = ", mId);
//		for(int m=0; m<4; m++) {
//			printf(" %7.2f", x_h_predict[m]);
//		}
//		printf("\n");
//
//
//		printf("MOTE[%d] KF_4S:Update() -> x_h_update = ", mId);
//		for(int m=0; m<4; m++) {
//			printf(" %7.2f", x_h_update[m]);
//		}
//		printf("\n");
//	}

}


// -- 0 ~ 1 Gaussian Random Number
//double KalmanFilter_4S::GaussianRandNumGenerator() {
//	/* Include requisits */
//	//#include <cstdlib>
//	//#include <ctime>
//
//	/* Generate a new random seed from system time - do this once in your constructor */
//	//srand(time(0));
//
//	/* Setup constants */
//	const static int q = 15;
//	const static double c1 = (1 << q) - 1;
//	const static double c2 = ((int)(c1 / 3)) + 1;
//	const static double c3 = 1.f / c1;
//
//	/* random number in range 0 - 1 not including 1 */
//	double random = 0.0;
//
//	/* the white noise */
//	double noise = 0.0;
//
//	int numSamples = 5;
//
//	for (int i = 0; i < numSamples; i++)
//	{
//	    random = ((double)rand() / (double)(RAND_MAX + 1.0f));
//	    noise = (2.f * ((random * c2) + (random * c2) + (random * c2)) - 3.f * (c2 - 1.f)) * c3;
//	}
//	return (double) noise;
//}


void KalmanFilter_4S::get_state(double *eX) {

	memcpy(eX, x_h_update, 4*sizeof(double));

}

void KalmanFilter_4S::set_state(const double *eX) {

	memcpy(x_h_update, eX, 4*sizeof(double));

}


void KalmanFilter_4S::get_predicted_state(double *eX) {

	memcpy(eX, x_h_predictpredict, 4*sizeof(double));

}

void KalmanFilter_4S::getCovariance(int *cov) {

	for(int i=0; i<4; i++)
		for(int j=0; j<4; j++) {
			cov[i*4+j] = (int)P_update[i][j];
		}

}

void KalmanFilter_4S::getCovariance_float(double *cov) {
	for(int i=0; i<4; i++)
		for(int j=0; j<4; j++) {
			cov[i*4+j] = (double)P_update[i][j];
		}
}

void KalmanFilter_4S::setCovariance_float(const double *cov) {
	for(int i=0; i<4; i++)
		for(int j=0; j<4; j++) {
			P_update[i][j] = cov[i*4+j];
		}
}

//void KalmanFilter_4S::getXYCovariance_float(double (*cov)[2]) {
//
//	for(int i=0; i<2; i++)
//		for(int j=0; j<2; j++)
//			cov[i][j] = P_update[i][j];
//}

//void KalmanFilter_4S::setXYCovariance(int *cov) {
//
//	for(int i=0; i<2; i++)
//		for(int j=0; j<2; j++)
//			P_update[i][j] = (double)cov[i*4+j];
//}
//
//void KalmanFilter_4S::setXYCovariance_int16_onlyXY(int16_t *cov) {
//	for(int i=0; i<2; i++)
//		for(int j=0; j<2; j++)
//			P_update[i][j] = (double)cov[i*2+j];
//}
//
//
//void KalmanFilter_4S::setProcessNoise(double q00, double q11) {
//	Q[0][0] = q00;
//	Q[1][1] = q11;
//}
//
//void KalmanFilter_4S::setMeasurementNoise(double r00, double r11) {
//
//	R[0][0] = r00;
//	R[1][1] = r11;
//}

//void KalmanFilter_4S::getXYCovariance_float_predicted(double (*cov)[2]) {
//
//	for(int i=0; i<2; i++)
//		for(int j=0; j<2; j++)
//			cov[i][j] = P_predictpredict[i][j];
//}
/*
void KalmanFilter_4S::drawTrackedTargetID(int feature_id)
{
//   glLineWidth(2.0);
//
//   char text[64];
//   char* p;
//
//   glDisable(GL_LIGHTING);
//   glPushMatrix();
//   glTranslatef(mWHc(1,4), mWHc(2,4), mWHc(3,4)-10);
//   sprintf(text, "%d\n", mId);
//   for (p = text; *p; p++)
//      glutStrokeCharacter(GLUT_STROKE_MONO_ROMAN, *p);
//s
//   glPopMatrix();

   glDisable(GL_DEPTH_TEST);
   glDisable(GL_LIGHTING);

	   glColor3f(0.6f, 0.1f, 0.3f);
	   gl_font(FL_HELVETICA_BOLD, 14);

   glRasterPos3d((double)x_h_update[0], (double)x_h_update[1], (double)5.0f);

   char text[32];
   sprintf(text,"%d(%d,%d)<-%d", feature_id, current_cluster_id, mAge, lastlyUpdatedFrom);
   gl_draw(text);


   glEnable(GL_DEPTH_TEST);
   glEnable(GL_LIGHTING);

}


const double DEG2RAD = 3.14159/180.0f;

void KalmanFilter_4S::drawUncertaintyEllipse(double scale, double r, double g, double b, bool line_stipple, double line_width)  {

//	printf("MOTE[%d] - drawUncertaintyEllipse() 0 \n", mId);

	if(x_h_update[0] == 0 && x_h_update[1] == 0) return;

//	printf("MOTE[%d] - drawUncertaintyEllipse() 1 \n", mId);

	double rho;			// -- Vanishing correlation coefficient
	double sigma[2];
	sigma[0] = (double)sqrt(P_update[0][0]);
	sigma[1] = (double)sqrt(P_update[1][1]);
	rho = (double)(P_update[0][1]/(sigma[0]*sigma[1]));


//	printf("MOTE[%d] DRAW: KF P_update = \n", mId);
//	for(int i=0; i<4; i++) {
//		for(int j=0; j<4; j++) {
//			printf("%7.1f ", P_update[i][j]);
//		}
//		printf("\n");
//	}


	double width, height;	// -- the principal semi-diameters of the ellipse
	double a;		// -- the angle between x1 axis and the semi-diameter of length p1

	if(sigma[0] != sigma[1])
		a = atan((double)(    2.0*rho*sigma[0]*sigma[1] / ( SQR(sigma[0])-SQR(sigma[1]) )    ))/2.0;
	else
		a = 0;

	width = SQR(sigma[0]*sigma[1])*(1-SQR(rho)) / (   SQR(sigma[1]*cos(a)) - 2.0*rho*sigma[0]*sigma[1]*sin(a)*cos(a) + SQR(sigma[0]*sin(a))   );
	height = SQR(sigma[0]*sigma[1])*(1-SQR(rho)) / (   SQR(sigma[1]*sin(a)) + 2.0*rho*sigma[0]*sigma[1]*sin(a)*cos(a) + SQR(sigma[0]*cos(a))   );

	if(width > 400 || width < 10) width = 40;
	if(height > 400 || height < 10) height = 40;

	width = MIN(100, MAX(20, width));
	height = MIN(100, MAX(20, height));

	//GLDrawEllipse(12, width, height, (int)geX[0], (int)geX[1], false, a);
	//drawEllipse(36, (int)x_h_update(1,1), (int)x_h_update(2,1), width*1.0, height*1.0, a, 0.0f, 0.0f, 0.8f);



	   double xradius = (double)MAX(5, width*scale);
	   double yradius = (double)MAX(5, height*scale);
	   double rotation_in_radian = (double)a;
	   double segments;

	   double degInRad;


	   //printf("MOTE[%d] - drawUncertaintyEllipse() (sigma[0], sigma[1], rho, r_x, r_y, rotation) = (%.5lf, %.5lf, %.5lf, %.2f, %.2f, %.2f) \n", mId, sigma[0], sigma[1], rho, xradius, yradius, rotation_in_radian);

	   	glDisable(GL_LIGHTING);
	   	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

	   	glEnable(GL_LINE_SMOOTH);

	   	if(line_stipple) {
			// -- Draw Lines based on its types: http://fly.cc.fer.hr/~unreal/theredbook/chapter02.html
			glEnable (GL_LINE_STIPPLE);
			glLineStipple((GLint) 6, (GLushort) 0xCCCC);
			segments = 8;
	   	} else {
	   		segments = 36;
	   	}

	       glLineWidth(line_width);
	   	   //glColor4f(0.0f, 0.0f, 0.8f, 0.8f);
	       glColor4f(r, g, b, 1.0f);

	   glBegin(GL_LINE_LOOP);

	   for (double i=0; i < 360.0f; i += 360.0f/(double)segments)
	   {
	      //convert degrees into radians
	      degInRad = i*DEG2RAD + rotation_in_radian;
	      glVertex3f((int)x_h_update[0] + cos(degInRad)*xradius, (int)x_h_update[1] + sin(degInRad)*yradius, 8.0);
	   }

	   glEnd();

	   if(line_stipple)
		   glDisable (GL_LINE_STIPPLE);


	   // -- Put the ID on the top the ellipse of the predicted position
	   {
		   glDisable(GL_DEPTH_TEST);

			   glColor3f(0.6f, 0.1f, 0.3f);
			   gl_font(FL_HELVETICA, 14);

			   glRasterPos3d((double)x_h_update[0] + cos((70+mId*10)*DEG2RAD + rotation_in_radian)*xradius, (double)x_h_update[1] + sin((70+mId*10)*DEG2RAD + rotation_in_radian)*yradius, 8.0);

		   char text[32];
		   sprintf(text,"%d", mId);
		   gl_draw(text);


		   glEnable(GL_DEPTH_TEST);
	   }


	   glEnable(GL_LIGHTING);

}

void KalmanFilter_4S::drawUncertaintyEllipse_prediction(double scale, double r, double g, double b, double line_width) {

	if(x_h_predictpredict[0] == 0 && x_h_predictpredict[1] == 0) return;

	double rho;			// -- Vanishing correlation coefficient
	double sigma[2];
	sigma[0] = (double)sqrt(P_predictpredict[0][0]);
	sigma[1] = (double)sqrt(P_predictpredict[1][1]);
	rho = (double)(P_predictpredict[0][1]/(sigma[0]*sigma[1]));

	double width, height;	// -- the principal semi-diameters of the ellipse
	double a;		// -- the angle between x1 axis and the semi-diameter of length p1

	if(sigma[0] != sigma[1])
		a = atan((double)(    2.0*rho*sigma[0]*sigma[1] / ( SQR(sigma[0])-SQR(sigma[1]) )    ))/2.0;
	else
		a = 0;

	width = SQR(sigma[0]*sigma[1])*(1-SQR(rho)) / (   SQR(sigma[1]*cos(a)) - 2.0*rho*sigma[0]*sigma[1]*sin(a)*cos(a) + SQR(sigma[0]*sin(a))   );
	height = SQR(sigma[0]*sigma[1])*(1-SQR(rho)) / (   SQR(sigma[1]*sin(a)) + 2.0*rho*sigma[0]*sigma[1]*sin(a)*cos(a) + SQR(sigma[0]*cos(a))   );

//	if(width > 999) width = 100;
//	if(height > 999) height = 100;

	//GLDrawEllipse(12, width, height, (int)geX[0], (int)geX[1], false, a);
	//drawEllipse(12, (int)x_h_predictpredict(1,1), (int)x_h_predictpredict(2,1), width*0.01, height*0.01, a, 0.8f, 0.0f, 0.0f);

	   double xradius = width*scale; //0.01;
	   double yradius = height*scale; //0.01;
	   double rotation_in_radian = a;
	   double segments = 36;

	   double degInRad;

	   	glDisable(GL_LIGHTING);
	   	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

	   	glEnable(GL_LINE_SMOOTH);

	    // -- Draw Lines based on its types: http://fly.cc.fer.hr/~unreal/theredbook/chapter02.html
	//	glEnable (GL_LINE_STIPPLE);
	//	glLineStipple((GLint) 6, (GLushort) 0xCCCC);

	       glLineWidth(line_width);
	   	   //glColor4f(0.8f, 0.0f, 0.0f, 1.0f);
	       glColor4f(r, g, b, 1.0f);

	   glBegin(GL_LINE_LOOP);

	   for (double i=0; i < 360.0f; i += 360.0f/(double)segments)
	   {
	      //convert degrees into radians
	      degInRad = i*DEG2RAD + rotation_in_radian;
	      glVertex3f((int)x_h_predictpredict[0] + cos(degInRad)*xradius, (int)x_h_predictpredict[1] + sin(degInRad)*yradius, 8.0);
	   }
	   glEnd();

//	   // -- Put the ID on the top the ellipse of the predicted position
//	   {
//		   glDisable(GL_DEPTH_TEST);
//
//			   glColor3f(0.6f, 0.1f, 0.3f);
//			   gl_font(FL_HELVETICA, 14);
//
//			#ifdef NEWMAT_IMPLEMENTATION
//			   glRasterPos3d((double)x_h_update(1,1), (double)x_h_update(2,1), (double)5.0f);
//			#else
//			   glRasterPos3d((double)x_h_predictpredict[0] + cos((70+mId*5)*DEG2RAD + rotation_in_radian)*xradius, (double)x_h_predictpredict[1] + sin((70+mId*5)*DEG2RAD + rotation_in_radian)*yradius, 8.0);
//			#endif
//
//		   char text[32];
//		   sprintf(text,"%d", mId);
//		   gl_draw(text);
//
//
//		   glEnable(GL_DEPTH_TEST);
//	   }

	//   glDisable (GL_LINE_STIPPLE);
	   glEnable(GL_LIGHTING);

}

void KalmanFilter_4S::drawUncertaintyEllipse_prediction_vis(double scale, double r, double g, double b) {

	if(x_h_predictpredict_vis[0] == 0 && x_h_predictpredict_vis[1] == 0) return;

	double rho;			// -- Vanishing correlation coefficient
	double sigma[2];
	sigma[0] = (double)sqrt(P_predictpredict_vis[0][0]);
	sigma[1] = (double)sqrt(P_predictpredict_vis[1][1]);
	rho = (double)(P_predictpredict_vis[0][1]/(sigma[0]*sigma[1]));

	double width, height;	// -- the principal semi-diameters of the ellipse
	double a;		// -- the angle between x1 axis and the semi-diameter of length p1

	if(sigma[0] != sigma[1])
		a = atan((double)(    2.0*rho*sigma[0]*sigma[1] / ( SQR(sigma[0])-SQR(sigma[1]) )    ))/2.0;
	else
		a = 0;

	width = SQR(sigma[0]*sigma[1])*(1-SQR(rho)) / (   SQR(sigma[1]*cos(a)) - 2.0*rho*sigma[0]*sigma[1]*sin(a)*cos(a) + SQR(sigma[0]*sin(a))   );
	height = SQR(sigma[0]*sigma[1])*(1-SQR(rho)) / (   SQR(sigma[1]*sin(a)) + 2.0*rho*sigma[0]*sigma[1]*sin(a)*cos(a) + SQR(sigma[0]*cos(a))   );

//	if(width > 999) width = 100;
//	if(height > 999) height = 100;

	//GLDrawEllipse(12, width, height, (int)geX[0], (int)geX[1], false, a);
	//drawEllipse(12, (int)x_h_predictpredict(1,1), (int)x_h_predictpredict(2,1), width*0.01, height*0.01, a, 0.8f, 0.0f, 0.0f);

	   double xradius = width*scale; //0.00002;
	   double yradius = height*scale; //0.00002;
	   double rotation_in_radian = a;
	   double segments = 36;

	   double degInRad;

	   	glDisable(GL_LIGHTING);
	   	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

	   	glEnable(GL_LINE_SMOOTH);

	    // -- Draw Lines based on its types: http://fly.cc.fer.hr/~unreal/theredbook/chapter02.html
	//	glEnable (GL_LINE_STIPPLE);
	//	glLineStipple((GLint) 6, (GLushort) 0xCCCC);

	       glLineWidth(2.0);
	   	   //glColor4f(0.8f, 0.0f, 0.0f, 1.0f);
	       glColor4f(r, g, b, 1.0f);

	   glBegin(GL_LINE_LOOP);

	   for (double i=0; i < 360.0f; i += 360.0f/(double)segments)
	   {
	      //convert degrees into radians
	      degInRad = i*DEG2RAD + rotation_in_radian;
	      glVertex3f((int)x_h_predictpredict_vis[0] + cos(degInRad)*xradius, (int)x_h_predictpredict_vis[1] + sin(degInRad)*yradius, 8.0);
	   }

	   glEnd();

	//   glDisable (GL_LINE_STIPPLE);
	   glEnable(GL_LIGHTING);
}


*/

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * FaceData.cpp
 *
 *  Created on: Aug 8, 2016
 *      Author: dkim
 */

#include <modules/ml/FaceData.hpp>

namespace vml {

/* class FaceData */

void TargetFaceData::addSingleFaceData(cv::Mat face, cv::Point loc, cv::Point fsize, double t)
{
	SingleFaceDataType facetp;
	facetp.data = face;
	facetp.loc = loc;
	facetp.faceSize = fsize;
	facetp.time = t;
	mFaceDataVec.push_back(facetp);
}

void TargetFaceData::addSingleFaceDataDMG(cv::Mat face, cv::Point loc, cv::Point fsize, double t, DMGData dmg)
{
	SingleFaceDataType facetp;
	facetp.data = face;
	facetp.loc = loc;
	facetp.faceSize = fsize;
	facetp.time = t;

	facetp.genderLabel = dmg.genderLabel;
	facetp.genderScore = dmg.genderScore;
	facetp.genderString = dmg.genderString;

	facetp.ageLabel = dmg.ageLabel;
	facetp.ageScore = dmg.ageScore;
	facetp.ageString = dmg.ageString;

	facetp.ethnicityLabel = dmg.ethnicityLabel;
	facetp.ethnicityString = dmg.ethnicityString;

	for(int i=0; i<4; i++)
	{
		facetp.ethnicityScore[i] = dmg.ethnicityScore[i];
	}

	mFaceDataVec.push_back(facetp);
}

} // of namespace vml

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * FaceDetector.cpp
 *
 *  Created on: Jan 8, 2016
 *      Author: dkim, yyoon
 */

#include "opencv2/imgproc/imgproc.hpp"
#include "modules/ml/FaceDetector.hpp"
//#include <modules/ml/FaceBaseDetector.hpp>

//#define DEBUG
#ifdef DEBUG
#define SAVE_FRAME_NUM 11
#endif

namespace vml {

FaceDetector::FaceDetector():
				scaleFactor(1.2f),
				minNeighbors(2),
				minFaceSize(20),
				maxFaceSize(300),
				mFPFiltering(true),
				mFPFilterFeature(1)
{
}

FaceDetector::~FaceDetector()
{
}

void FaceDetector::initialize(boost::shared_ptr<FaceVideoModule> video_buffer,
		boost::shared_ptr<Settings> settings)
{
	mpVideoBuffer = video_buffer;

	// get parameters from settings
	std::string cascadeFilePath = settings->getString("FaceDetection/cascadeFile"," ", false);
    scaleFactor = settings->getFloat("FaceDetection/scaleFactor",1.2f,false);
    minNeighbors = settings->getInt("FaceDetection/minNeighbors",6,false);
	minFaceSize = settings->getInt("FaceDetection/minFaceSize",20,false);
	maxFaceSize = settings->getInt("FaceDetection/maxFaceSize",240,false);

	// initialize a Face detector
	mpCascade.load(cascadeFilePath.c_str());

	// For non-face filter
    mFPFiltering = settings->getBool("FaceDetection/NFFiltering", false, false);
    mFPFilterFeature = settings->getInt("FaceDetection/NFFilteringFeature", 1, false);

    // load the model of a face filter
	psvm.reset(new vml::MLSVM(settings));
	pfeature.reset(new vml::MLFeature(settings));

	std::string modelFileName = settings->getString("FaceDetection/NFFModelFile"," ", false);
	psvm->loadModelFromFile(modelFileName);

	mBlobMap = cv::Mat_<unsigned char>(mpVideoBuffer->getRows(),mpVideoBuffer->getCols());

}

void FaceDetector::getDetectedTargetList(std::vector<boost::shared_ptr<FaceDetectorTarget> > &d)
{
	d = mDetectedList;	// copy mDetectedList to destination
	mDetectedList.clear();
}

bool FaceDetector::detect(void)
{
	//const cv::Mat	&inputimage = mpVideoBuffer->currentFrame()->image;
	const cv::Mat	&in = mpVideoBuffer->currentFrame()->image;

	cv::Mat	inputimage;
	//cv::Mat in;
	//mpVideoBuffer->getCurrentUndistortedImage(in);
	//mpVideoBuffer->currentFrame()->image.copyTo(inputimage);
	in.copyTo(inputimage);

	std::vector<cv::Point> troi = mpVideoBuffer->getTrackingRegion();
	cv::Rect track_roi = cv::Rect(troi[0], troi[2]);

	if(track_roi.area()  <= 1 )
	{
		return false;
	}

	cv::Mat roi_image = cv::Mat(track_roi.height,track_roi.width,inputimage.channels());
	inputimage(track_roi).copyTo(roi_image);

    cv::Point delta_loc(track_roi.x, track_roi.y);

	#ifdef DEBUG
		if (mpVideoBuffer->getCurrentFrameNumber() == SAVE_FRAME_NUM)
		{
			cv::Mat tempimg;
			inputimage.copyTo(tempimg);

			cv::imwrite("inputimage.png",tempimg);
		}
	#endif

		// clear target list
		// reset all the pointers before clearing
		mFaces.clear();
		mDetectedList.clear();
		//mTempContourList.clear();
		//mFaceMap = 0;
		mBlobMap = 0;

		//printf("Before face detection..\n");

		// face detection
		mpCascade.detectMultiScale(roi_image,mFaces,scaleFactor, minNeighbors,0,cv::Size(minFaceSize,minFaceSize), cv::Size(maxFaceSize,maxFaceSize));
		//printf("After face detection..\n");

		if( mFaces.size() < 1)
		{
			return false;
		}
		else
		{
			for(unsigned int i=0;i< mFaces.size();i++)
			{
				mFaces[i].x += delta_loc.x;
				mFaces[i].y += delta_loc.y;
			}

			// do false face filtering, if checked the option of false face filtering
			if(mFPFiltering)
			{
				mFacesAF.clear();
				mFalsePositiveFaces.clear();
				FPFiltering(inputimage, mFaces);
				mFaces = mFacesAF;
			}

			mDetectedList.resize(mFaces.size());

			// output: create mDetectedList for all detected faces
			for(unsigned int i=0;i< mFaces.size();i++)
			{
				if (mFaces[i].area() > minFaceSize*minFaceSize)
				{
					// create target instance from a detected face
					mDetectedList[i].reset(new FaceDetectorTarget(mFaces[i]));
				}
				// else, this face is too small. do nothing
			}

/*
			// compact the detected list (?)
			int	num_valid_entry = 0;
			std::vector<boost::shared_ptr<FaceDetectorTarget> >::iterator cur_itr;
			std::vector<boost::shared_ptr<FaceDetectorTarget> >::iterator last_valid_position_itr;
			for (cur_itr=mDetectedList.begin(),last_valid_position_itr=cur_itr;cur_itr!=mDetectedList.end();cur_itr++)
			{
				if (*cur_itr)	// if this is a valid pointer
				{
					if (cur_itr != last_valid_position_itr)
					{
						*last_valid_position_itr = *cur_itr;
						cur_itr->reset();
					}
					cv::drawContours(mBlobMap,std::vector<std::vector<cv::Point> >(1,(*last_valid_position_itr)->contour1),-1,cv::Scalar(255),CV_FILLED);
					last_valid_position_itr++;
					num_valid_entry++;
				}
			}
			// resize the list
			mDetectedList.resize(num_valid_entry);
*/

			return true;
		}

} // of process()

bool FaceDetector::detectInDetectorFrame(void)
{
	cv::Mat &inputimage = mpVideoBuffer->getDetectorFrame()->image;
	//const cv::Mat	&inputimage = mpVideoBuffer->currentFrame()->image;
	//const cv::Mat	&in = mpVideoBuffer->currentFrame()->image;

	//cv::Mat	inputimage;
	//cv::Mat in;
	//mpVideoBuffer->getCurrentUndistortedImage(in);
	//mpVideoBuffer->currentFrame()->image.copyTo(inputimage);
	//in.copyTo(inputimage);

	std::vector<cv::Point> troi = mpVideoBuffer->getTrackingRegion();
	cv::Rect track_roi = cv::Rect(troi[0], troi[2]);

	if(track_roi.area()  <= 1 )
	{
		return false;
	}

	cv::Mat roi_image = cv::Mat(track_roi.height,track_roi.width,inputimage.channels());
	inputimage(track_roi).copyTo(roi_image);

    cv::Point delta_loc(track_roi.x, track_roi.y);

	#ifdef DEBUG
		if (mpVideoBuffer->getCurrentFrameNumber() == SAVE_FRAME_NUM)
		{
			cv::Mat tempimg;
			inputimage.copyTo(tempimg);

			cv::imwrite("inputimage.png",tempimg);
		}
	#endif

		// clear target list
		// reset all the pointers before clearing
		mFaces.clear();
		mDetectedList.clear();
		//mTempContourList.clear();
		//mFaceMap = 0;
		mBlobMap = 0;

		//printf("Before face detection..\n");

		// face detection
		mpCascade.detectMultiScale(roi_image,mFaces,scaleFactor, minNeighbors,0,cv::Size(minFaceSize,minFaceSize), cv::Size(maxFaceSize,maxFaceSize));
		//printf("After face detection..\n");

		if( mFaces.size() < 1)
		{
			return false;
		}
		else
		{
			for(unsigned int i=0;i< mFaces.size();i++)
			{
				mFaces[i].x += delta_loc.x;
				mFaces[i].y += delta_loc.y;
			}

			// do false face filtering, if checked the option of false face filtering
			if(mFPFiltering)
			{
				mFacesAF.clear();
				mFalsePositiveFaces.clear();
				FPFiltering(inputimage, mFaces);
				mFaces = mFacesAF;
			}

			mDetectedList.resize(mFaces.size());

			// output: create mDetectedList for all detected faces
			for(unsigned int i=0;i< mFaces.size();i++)
			{
				if (mFaces[i].area() > minFaceSize*minFaceSize)
				{
					// create target instance from a detected face
					mDetectedList[i].reset(new FaceDetectorTarget(mFaces[i]));
				}
				// else, this face is too small. do nothing
			}

/*
			// compact the detected list (?)
			int	num_valid_entry = 0;
			std::vector<boost::shared_ptr<FaceDetectorTarget> >::iterator cur_itr;
			std::vector<boost::shared_ptr<FaceDetectorTarget> >::iterator last_valid_position_itr;
			for (cur_itr=mDetectedList.begin(),last_valid_position_itr=cur_itr;cur_itr!=mDetectedList.end();cur_itr++)
			{
				if (*cur_itr)	// if this is a valid pointer
				{
					if (cur_itr != last_valid_position_itr)
					{
						*last_valid_position_itr = *cur_itr;
						cur_itr->reset();
					}
					cv::drawContours(mBlobMap,std::vector<std::vector<cv::Point> >(1,(*last_valid_position_itr)->contour1),-1,cv::Scalar(255),CV_FILLED);
					last_valid_position_itr++;
					num_valid_entry++;
				}
			}
			// resize the list
			mDetectedList.resize(num_valid_entry);
*/

			return true;
		}

} // of process()

bool FaceDetector::detectInGrabFrame(void)
{
	cv::Mat &inputimage = mpVideoBuffer->getGrabFrame()->image;
	//const cv::Mat	&inputimage = mpVideoBuffer->currentFrame()->image;
	//const cv::Mat	&in = mpVideoBuffer->currentFrame()->image;

	//cv::Mat	inputimage;
	//cv::Mat in;
	//mpVideoBuffer->getCurrentUndistortedImage(in);
	//mpVideoBuffer->currentFrame()->image.copyTo(inputimage);
	//in.copyTo(inputimage);

	std::vector<cv::Point> troi = mpVideoBuffer->getTrackingRegion();
	cv::Rect track_roi = cv::Rect(troi[0], troi[2]);

	if(track_roi.area()  <= 1 )
	{
		return false;
	}

	cv::Mat roi_image = cv::Mat(track_roi.height,track_roi.width,inputimage.channels());
	inputimage(track_roi).copyTo(roi_image);

    cv::Point delta_loc(track_roi.x, track_roi.y);

	#ifdef DEBUG
		if (mpVideoBuffer->getCurrentFrameNumber() == SAVE_FRAME_NUM)
		{
			cv::Mat tempimg;
			inputimage.copyTo(tempimg);

			cv::imwrite("inputimage.png",tempimg);
		}
	#endif

		// clear target list
		// reset all the pointers before clearing
		mFaces.clear();
		mFacesAF.clear();
		mDetectedList.clear();
		//mTempContourList.clear();
		//mFaceMap = 0;
		mBlobMap = 0;

		//printf("Before face detection..\n");

		// face detection
		mpCascade.detectMultiScale(roi_image,mFaces,scaleFactor, minNeighbors,0,cv::Size(minFaceSize,minFaceSize), cv::Size(maxFaceSize,maxFaceSize));
		//printf("After face detection..\n");

		if( mFaces.size() < 1)
		{
			return false;
		}
		else
		{
			for(unsigned int i=0;i< mFaces.size();i++)
			{
				mFaces[i].x += delta_loc.x;
				mFaces[i].y += delta_loc.y;
			}

			// do false face filtering, if checked the option of false face filtering
			if(mFPFiltering)
			{
				mFacesAF.clear();
				mFalsePositiveFaces.clear();
				FPFiltering(inputimage, mFaces);
				mFaces = mFacesAF;
			}

			mDetectedList.resize(mFaces.size());

			// output: create mDetectedList for all detected faces
			for(unsigned int i=0;i< mFaces.size();i++)
			{
				if (mFaces[i].area() > minFaceSize*minFaceSize)
				{
					// create target instance from a detected face
					mDetectedList[i].reset(new FaceDetectorTarget(mFaces[i]));
				}
				// else, this face is too small. do nothing
			}

/*
			// compact the detected list (?)
			int	num_valid_entry = 0;
			std::vector<boost::shared_ptr<FaceDetectorTarget> >::iterator cur_itr;
			std::vector<boost::shared_ptr<FaceDetectorTarget> >::iterator last_valid_position_itr;
			for (cur_itr=mDetectedList.begin(),last_valid_position_itr=cur_itr;cur_itr!=mDetectedList.end();cur_itr++)
			{
				if (*cur_itr)	// if this is a valid pointer
				{
					if (cur_itr != last_valid_position_itr)
					{
						*last_valid_position_itr = *cur_itr;
						cur_itr->reset();
					}
					cv::drawContours(mBlobMap,std::vector<std::vector<cv::Point> >(1,(*last_valid_position_itr)->contour1),-1,cv::Scalar(255),CV_FILLED);
					last_valid_position_itr++;
					num_valid_entry++;
				}
			}
			// resize the list
			mDetectedList.resize(num_valid_entry);
*/

			return true;
		}

} // of process()

// optional face drawing code
void FaceDetector::drawValidTargets(cv::Mat &image) const
{
	for (std::vector<boost::shared_ptr<FaceDetectorTarget> >::const_iterator titr=mDetectedList.begin();
				titr!=mDetectedList.end();titr++)
	{
		if (*titr)	// after the compaction, all the entries should be valid, but to ensure safety
		{
			// draw face
			cv::rectangle(image,(*titr)->bounding_box,CV_RGB(0,200,255));
		}
	}
}


void FaceDetector::getDetectedFaceRects(std::vector<cv::Rect> &d) const
{
	d = mFaces;	// copy mFaces to destination
}

void FaceDetector::getDetectedFalsePositiveFaceRects(std::vector<cv::Rect> &d) const
{
	d = mFalsePositiveFaces;	// copy mFaces to destination
}


void FaceDetector::FPFiltering(const cv::Mat &image, std::vector<cv::Rect> &facevec )
{

    cv::Mat cropped, face_cand;

    // False positive face filtering
    for (unsigned int i=0; i<facevec.size();i++)
    {
        cropped = image(facevec[i]);
        cropped.copyTo(face_cand);

        // 1. color feature-based filtering
        if(mFPFilterFeature == 0)  // color
        {
			if(FPFilteringByColor(face_cand))
			{
				mFacesAF.push_back(facevec[i]);
			}
			else
			{
				mFalsePositiveFaces.push_back(facevec[i]);
			}
        }
        else
        {
			// 2. HoG feature-based filtering
			if(FPFilteringByHOG(face_cand))
			 {
				 mFacesAF.push_back(facevec[i]);
			 }
			 else
			 {
				 mFalsePositiveFaces.push_back(facevec[i]);
			 }
        }
    }

}

bool FaceDetector::FPFilteringByColor(cv::Mat &cimg)
{
    cv::Mat dimg;
    cv::cvtColor(cimg, dimg, CV_BGR2YCrCb);

    // Extract channel individually
    cv::Mat planes[3];
    cv::split(dimg, planes);

    //cv::Scalar mean_ch1 = cv::mean(planes[0]);
    cv::Scalar mean_ch2 = cv::mean(planes[1]);
    cv::Scalar mean_ch3 = cv::mean(planes[2]);

    //float mch1 = mean_ch1.val[0];
    float mch2 = mean_ch2.val[0];
    float mch3 = mean_ch3.val[0];

    if (mch3 > 135 &&  (mch2-62) > 0 )
    {
        return false;
    }

    return true;

}

bool FaceDetector::FPFilteringByHOG(cv::Mat &cimg)
{

	// svm-based prediction
	pfeature->extFeature(cimg);
    double estimate = psvm->predict(pfeature->featData);
    if(estimate ==1) return true;
    else return false;
}

} // of namespace vml

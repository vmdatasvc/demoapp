/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * FaceTarget.cpp
 *
 *  Created on: Jan 28, 2016
 *      Author: dkim
 */

#include <opencv2/video/tracking.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "modules/ml/FaceDetector.hpp"
#include "modules/ml/FaceTarget.hpp"
#include "modules/ml/FaceTracker.hpp"

#include "dlib/opencv.h"

// for uuid generation
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/lexical_cast.hpp>

#include <modules/ml/jsoncpp/jsoncpp.hpp>
#include <fstream>
#include <sstream>

#include <modules/utility/Serialize.hpp>

// for debugging
#include <modules/utility/Util.hpp>

//#define DEBUG

namespace vml {


/* class FaceTarget */
FaceTarget::FaceTarget(unsigned int id):
		mID(id),
		mActive(true),
		mDetectionInactiveFrameCount(0)
//		mcTargetVerificationProportionToTrackbox(0.1f)
{

	//mpTargetDetector = FaceModuleManager::instance()->getDetector();
	mpVideoModule = FaceModuleManager::instance()->getVideoModule();
	mpSettings = FaceModuleManager::instance()->getSettings();

	mcVerfyFaceSimiliarityThreshold = mpSettings->getFloat("FaceTarget/verfyTargetVisualSimiliarityThreshold", 0.3f, false);
	mcReactivateLikelihoodThreshold = mpSettings->getFloat("FaceTarget/reactivateLikelihoodThreshold", 0.9f, false);
	mDetectionInactiveFrameCountTh = mpVideoModule->getFrameRate()*mpSettings->getInt("FaceTarget/detectionInactiveTimeOut", 30.0f, false);

	//double start_time = gCurrentTime();
	// For non-face filter
    // load the model of a face filter
	//mpSVM.reset(new vml::MLSVM(mpSettings));
	mpFeature.reset(new vml::MLFeature(mpSettings));

	//std::string modelFileName = mpSettings->getString("FaceDetection/NFFModelFile"," ", false);
	//mpSVM->loadModelFromFile(modelFileName);

	//double reset_time = gCurrentTime();
	//printf("TIME: mpFeature.reset: %f\n", reset_time - start_time);
}

bool FaceTarget::verifyTargetStatus(const cv::Mat &bp_map)
{
	cv::Mat cropped_map;
	cv::Rect trackBox = this->mpTracker->getTrackBox();
#ifdef DEBUG
	cv::Rect disp_rect = trackBox;
	disp_rect.x -= disp_rect.width;
	disp_rect.y -= disp_rect.height;
	disp_rect.width *=3;
	disp_rect.height *=3;

	//disp_rect = disp_rect & cv::Rect(0,0,mpVideoBuffer->getCols()-1, mpVideoBuffer->getRows()-1);

	cv::Mat t_bp_map;
	bp_map(disp_rect).copyTo(t_bp_map);
	cv::imwrite("bp_map.png",t_bp_map);

#endif

	bp_map(trackBox).copyTo(cropped_map);

	// Based on the bp_map

	cv::Scalar val = cv::sum(cropped_map);

	if ((float)val[0]/(float)(trackBox.height*trackBox.width*255) < 0.1)
	{
			// lost track.
			return false;
	}
	else
	{
			return true;
	}
	/*
	cv::Scalar area = cv::sum(cropped_map);
	if ((float)area[0]/(float)(trackBox.height*trackBox.width) < mcTargetVerificationProportionToTrackbox)
	{
		// lost track.
		return false;
	}
	else
	{
		return true;
	}
	 */
}

//void FaceTarget::initializeFeature(const cv::Mat &input_image)
void FaceTarget::initialize(boost::shared_ptr<FaceDetectorTarget> dt)
{
	FaceTrackingMethods tm = FaceBaseTracker::readTrackingMethod(mpSettings);

	switch (tm)
	{
		case FaceTrackerFeature_HIST_HSV:
		{
			boost::shared_ptr<SingleFaceTrackerHSHist> tp;
			tp.reset(new SingleFaceTrackerHSHist(mpSettings));
			tp->initialize(dt);
			mpTracker = tp;
			break;
		} // of case HIST_HSV
		case FaceTrackerFeature_HIST_YCrCb:
		{
			boost::shared_ptr<SingleFaceTrackerCrCbHist> tp;
			tp.reset(new SingleFaceTrackerCrCbHist(mpSettings));
			tp->initialize(dt);
			mpTracker = tp;
			break;
		} // of case HIST_rgCHROM
		case FaceTrackerFeature_HIST_rgCHROM:
		{
			boost::shared_ptr<SingleFaceTrackerRGChromHist> tp;
			tp.reset(new SingleFaceTrackerRGChromHist(mpSettings));
			tp->initialize(dt);
			mpTracker = tp;
			break;
		} // of case HIST_rgCHROM
		case FaceTrackerFeature_HIST_RGB:
		{
			boost::shared_ptr<SingleFaceTrackerRGBHist> tp;
			tp.reset(new SingleFaceTrackerRGBHist(mpSettings));
			tp->initialize(dt);
			mpTracker = tp;
			break;
		} // of case HIST_RGB
		case FaceTrackerFeature_DLIB:
		{
			boost::shared_ptr<SingleFaceTrackerDlib> tp;
			tp.reset(new SingleFaceTrackerDlib(mpSettings));
			tp->initialize(dt);
			mpTracker = tp;
			break;
		} // of case Correlation_Tracking:
		default:
		{
			VML_ERROR(MsgCode::notSupportedErr,"FaceTarget: Tracking method " << tm << " not supported");
		}
	} // of switch (hm)


	// initialize members based on the current contour
//	cv::Rect tBoundingRect = cv::boundingRect(mCvxHull);	// crop rect to be three times larger than the bounding box
	// initialize tracking feature based on the chosen tracking method
//	mpTracker->initialize(input_image,tBoundingRect,&mContours);

}

/*
bool FaceTarget::verifyFaceDetection(const cv::Mat &input_image, cv::Rect &box, cv::Mat &faceimage)
{
	// Given the local region of box, check the face detection to collect the right face during tracking a person
	// crop the Box with margin in the input_image
	cv::Mat cropped_ref = input_image(box);
	cv::Mat cropped_image;
	cropped_ref.copyTo(cropped_image);

    cv::cvtColor(cropped_image, faceimage, CV_RGB2BGR);

#ifdef FACETARGET_DEBUG_IMAGES
	cv::imwrite("faceDataSave_verifyFD.png", faceimage);
#endif

    mpFeature->extFeature(cropped_image);
    double estimate = mpSVM->predict(mpFeature->featData);

    if(estimate ==1)
    {
    	return true;
    }
    else
    {
    	return false;
    }

}
*/

TrackingStatusEnum FaceTarget::updateTargetStatus(cv::Mat &bpImage)
{

	// update the target location and feature based on the chosen tracking method
		TrackingStatusEnum update_status = mpTracker->update(&bpImage);

		// Added a terminating condition for a tracker stuck without any face detection
		if(mDetectionInactiveFrameCount > mDetectionInactiveFrameCountTh)
		{
			update_status = FaceTrackingStatus_Inactive;
		}
		// update trajectory and motion model.
		// add trajectory node only when the tracker is in track region
		if (mpTracker->isInTrackRegion())
		{
			mTrajectory.addTrajectoryPoint(mpTracker->getPosition(),mpVideoModule->currentFrame()->time);
		}

		if (update_status == FaceTrackingStatus_Inactive)
		{
			mActive = false;
		}

		return update_status;

}

/*
bool FaceTarget::updateTargetStatus(const cv::Mat &input_image,
		cv::Mat &bpImage,
		double time)

 	bool update_succesful = mpTracker->update(input_image, &bpImage);

    // Test validity using the bpImage and input_image of the detected target
//    bool valid_target;
//    cv::Rect mBox = mpTracker->getTrackBox();
//    float confidence = evalValidTarget(bpImage, input_image, mBox);
//    valid_target = confidence > 0.5? true:false;
//    update_succesful &= valid_target;

	// 2. Optical-Flow-based tracking (optional)

    if (update_succesful)
	{
		// update trajectory and motion model.
		mTrajectory.addTrajectoryPoint(mpTracker->getPosition(),time);

	}

	return update_succesful;

}
*/

// Verify a detected face from the active or inactive target being tracked
float FaceTarget::verifyFaceSimilarity(boost::shared_ptr<FaceDetectorTarget> &dt)
{
	cv::Mat input_image = mpVideoModule->currentFrame()->image;


	cv::Mat dt_face, tr_face;
	input_image(dt->bounding_box).copyTo(dt_face);

	tr_face = this->mFaceData.getFaceData()[0].data;

	return mpFeature->verifySimilarity(tr_face, dt_face);

}

bool FaceTarget::verifyActiveTarget(boost::shared_ptr<FaceDetectorTarget> &dt)
{

	//float slikelihood = verifyFaceSimilarity(dt);

	float location_likelihood =  mpTracker->calculateReactivationLikelihood(dt); // 0 or 1
	float visual_likelihood = verifyFaceSimilarity(dt);    // [-1,1]

	float slikelihood = location_likelihood * visual_likelihood;

	if(slikelihood > mcVerfyFaceSimiliarityThreshold)
	{
		dt->setInvalid();
		return true;
	}

	return false;

	 //return mpTracker->verifyActiveTargetFromDetection(dt);
}

TrackingStatusEnum FaceTarget::reactivateTarget(std::vector<boost::shared_ptr<FaceDetectorTarget> > &dt_list)
{
	//
	boost::shared_ptr<FaceDetectorTarget> maxLikelihood_dt;

	//bool target_found = false;
	float	maxLikelihood = -1.f;
	std::vector<boost::shared_ptr<FaceDetectorTarget> >::iterator dt_itr = dt_list.begin();
	while(dt_itr != dt_list.end())
	{
		if ((*dt_itr)->isValid())
		{
			// find the detected target with maximum location likelihood
			// TODO: currently just use location likelihood, but research to use visual similarity later
			float location_likelihood =  mpTracker->calculateReactivationLikelihood((*dt_itr)); // 0 or 1
			float visual_likelihood = verifyFaceSimilarity((*dt_itr));    // [-1,1]

			float reactivation_likelihood = location_likelihood * visual_likelihood;
			if (reactivation_likelihood > maxLikelihood)
			{
				maxLikelihood_dt = (*dt_itr);
				maxLikelihood = reactivation_likelihood;
			}

		}
		dt_itr++;
	}

	TrackingStatusEnum reactivate_status;

    if (maxLikelihood > mcReactivateLikelihoodThreshold)
	{
		//target_found = true;

		reactivate_status = mpTracker->reactivateStatus(maxLikelihood_dt);

		if (reactivate_status == FaceTrackingStatus_Active)
		{
			mActive = true;
		}

	}
    else
    {
    	//target_found = false;
    }
/*
	else
	{
		target_found = false;

		// reactivation target not found.
		// increase inactive count
		mpTracker->increaseInactiveFrameCount();

		// if inactive count is larger than threshold, finish this target
		if (mpTracker->getInactiveFrameCount() > mpTracker->getInactiveTimeOutValue())
		{
			reactivate_status = FaceTrackingStatus_Finished;
		}
		else
		{
			reactivate_status = FaceTrackingStatus_Inactive;
		}

	}
*/
/*
	// original
	TrackingStatusEnum reactivate_status = mpTracker->reactivate(dt_list);0
*/


	return reactivate_status;
}

double FaceTarget::getLastTime(void) const
{
	// return time of the last trajectory
	return mTrajectory.getEndTime();
}


cv::Point FaceTarget::getPosition() const
{
	return mpTracker->getPosition();    // return the center
}

void FaceTarget::drawTarget(cv::Mat &image) const
{
	// draw search window
	cv::rectangle(image,mpTracker->getCropRect(),CV_RGB(0,255,0));

	// draw trackbox
//	cv::Point2f vertices[4];
//	mTrackBox.points(vertices);
//	for (int i=0;i<4;i++)
//	{
//		cv::line(image,vertices[i],vertices[(i+1)%4], cv::Scalar(0,255,0));
//	}

    // draw meanShift trackbox
	mpTracker->drawTrackBox(image);

	// draw estimated person shape
	cv::drawContours(image,
			std::vector<std::vector<cv::Point> >(1,mEstimatedPersonShape),-1,cv::Scalar(255,20,20));

	cv::Rect	trackBox = mpTracker->getTrackBox();
	// draw target id
	std::ostringstream id_str;
	id_str << mID;
	cv::Point id_pos(trackBox.x - 20 , trackBox.y + 15 );
	cv::putText(image, id_str.str(), id_pos /*trackBox.tl()*/, 4 /*CV_FONT_HERSHEY_PLAIN*/, 0.7f, cv::Scalar(0,0,255)); //cv::Scalar(125,255,0));

	// draw target confidence level
	std::ostringstream conf_str;
	conf_str.precision(2);
	conf_str << mpTracker->getConfidence();
	cv::Point tc_pos(trackBox.x + trackBox.width +5, trackBox.y + trackBox.height );
	cv::putText(image, conf_str.str(), tc_pos /*trackBox.br()*/, 4 /*CV_FONT_HERSHEY_PLAIN*/, 0.6f, cv::Scalar(255,0,0));

	// draw target center coords
//	std::ostringstream coord_str;
//	cv::Point coord_pos = mpTracker->getPosition();
//	coord_str << "(" << coord_pos.x << "," << coord_pos.y << ")";
//	coord_pos.x = trackBox.x;
//	coord_pos.y = trackBox.y + trackBox.height + 10;
//	cv::putText(image, coord_str.str(), coord_pos, CV_FONT_HERSHEY_PLAIN, 0.7f, cv::Scalar(255,0,0));

}

void FaceTarget::updateTargetActivityMap(cv::Mat &activity_map)
{
	mpTracker->updateTargetActivityMap(activity_map);
}


cv::Mat FaceTarget::formatImagesForPCA(const std::vector<cv::Mat> &data)
{
    cv::Mat dst(data.size(), data[0].rows*data[0].cols, CV_32F);

    for(unsigned int i = 0; i < data.size(); i++)
    {
        cv::Mat image_row = data[i].clone().reshape(1,1);
        cv::Mat row_i = dst.row(i);
        image_row.convertTo(row_i, CV_32F);
    }
    return dst;
}

//void FaceTarget::selectFaces(const std::vector<SingleCFaceDataType> fvec, int numFaces, std::vector<int> &sidx)
void FaceTarget::selectFaces(int numFaces, std::vector<int> &sidx)
{
	// If the number of Faces is larger than the fvec.size(), do clustering.
	// INPUT: Different size of faces, number of Faces
	// OUTPUT: std::vector<int> idx (selected face indexes), then we filter the unselected faces

	const std::vector<SingleFaceDataType> fvec = mFaceData.getFaceData();

	std::vector<cv::Mat> ivec;

	// Normalize the size of faces
	int minSize = 100000;
    for(int i=0; i< (int)fvec.size(); i++)
	{
		if(minSize > fvec[i].data.rows)
		{
			minSize = fvec[i].data.rows;
		}
	}
    for(int i=0; i< (int)fvec.size(); i++)
	{
        cv::Mat gface(fvec[i].data.rows, fvec[i].data.cols, CV_8UC1);
		// resize and change it to a gray-scale image
        cv::cvtColor(fvec[i].data, gface, CV_BGR2GRAY);
        cv::resize(gface, gface, cv::Size(minSize,minSize), cv::INTER_CUBIC);
        //cv::cvtColor(fvec[i].data, ivec[i], CV_BGR2GRAY);
        //cv::resize(ivec[i], ivec[i], cv::Size(minSize,minSize), cv::INTER_CUBIC);

        ivec.push_back(gface);
	}

	// do PCA
	cv::Mat faceData = formatImagesForPCA(ivec);
	cv::PCA pca(faceData, cv::Mat(), cv::PCA::DATA_AS_ROW, 0.80);

	// projection
	cv::Mat points = pca.project(faceData);

	// K-means clustering
	cv::Mat labels;
	cv::Mat centers;  // clusterCount, 1, points.type());

	cv::kmeans(points, numFaces, labels, cv::TermCriteria( CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 10, 1.0), 3, cv::KMEANS_PP_CENTERS, centers);

	// output the index corresponding to samples which are close to the center of clusters
	// default value of indexes is 0. Just make the selected sample index one.
    cv::Mat distMat(numFaces,fvec.size(),CV_32F);
	for(int i=0;i<numFaces; i++)
	{
		for(int j=0;j<(int)fvec.size(); j++)
		{
			cv::Mat a, b;

			a = centers.row(i);
			b = points.row(j);


			if(labels.at<unsigned char>(j,0) == i)
			{
				distMat.at<float>(i,j) = cv::norm(a,b,cv::NORM_L2);
			}
			else
			{
				distMat.at<float>(i,j) = 1000000.0f;
			}
		}
		double minv,maxv;
		cv::Point min_loc, max_loc;
		cv::minMaxLoc(distMat.row(i), &minv, &maxv, &min_loc, &max_loc);

        sidx[(int)min_loc.x] = 1;
	}

	// Store the selected face data
	for(int k=0;k<(int)fvec.size();k++)
	{
		if(sidx[k])
		{
			mFaceData.addSelectedFaceDataVec(fvec[k]);
		}
	}

}


// Check up the case, numFaces = 0
void FaceTarget::storeFaceData( std::string path, int numFaces)
{
	// uuid (universally unique identifier) generation
	boost::uuids::uuid idx = boost::uuids::random_generator()();

	std::string id = boost::lexical_cast<std::string>(idx);

	mFaceData.id = id;

	// get face data
	const std::vector<SingleFaceDataType> facevec  = mFaceData.getFaceData();

    if(facevec.size() < 1)
    {
#ifdef FACETARGET_DEBUG    // no face data, but push to save the images (Exception)
        cout << "Face vector is empty. Check out!!" << endl;
#endif
        return;
    }

    // face Selection  -> output the indextm_year The number of years since 1900.
	std::vector<int> sidx;
    sidx.resize((int)facevec.size());

	std::fill(sidx.begin(), sidx.end(), 1);

	// How to handle numFaces = 0 and numFaces =1?  --->
    if(numFaces ==1)
    {
    	mFaceData.addSelectedFaceDataVec(facevec[0]);
    }
    else if(numFaces > 1)
	{
		if(numFaces < (int)facevec.size())
		{
			std::fill(sidx.begin(), sidx.end(), 0);
			//selectFaces(facevec, numFaces, sidx);
			selectFaces(numFaces, sidx);
		}
	}

	// save the face meta data as a JSON file
	Json::Value root, darr, farr, sarr, esarr, tarr,iarr, karr, parrx, parry;

	// a target ID
	root["faceId"] = mFaceData.id;

	// number of face images to be saved
	//root["faceImage"]["faceNum"] = (int)facevec.size();

	//if(numFaces) {
	//	root["faceImage"]["faceNum"] = numFaces;
	//}
	//else {
	//	root["faceImage"]["faceNum"] = (int)facevec.size();
	//}
	//root["faceImage"]["faceTotalNum"] = (int)facevec.size();

	// links of the saved face images
	std::string oss;

	for(int i=0, cn=0;i< (int)facevec.size();i++)
	{
		if(sidx[i])
		{
			//oss = boost::lexical_cast<std::string>(static_cast<long>(100.0f*facevec[i].time));
			// later, able to add a specific camID
			//std::string facefile =  path + oss + ".jpg";

			//time_t temp = (time_t)facevec[i].time;
			//tm *bar = gmtime(&temp);

			std::ostringstream ossr;
			//ossr << bar->tm_mon <<  bar->tm_mday << bar->tm_year <<"_" << bar->tm_hour << bar->tm_min << bar->tm_sec;
			//ossr << mCFaceData.id <<"_"<< bar->tm_hour << bar->tm_min << bar->tm_sec << "_" << i;
			ossr << mFaceData.id << "_" << i;
			std::string facefile =  path + ossr.str() + ".jpg";


			// write a face image to a local disk(path)
			cv::imwrite(facefile.c_str(), facevec[i].data);

			// write its link for a JSON file
			farr[cn]= facefile;

			parrx[cn] = (double)facevec[i].loc.x / (double)mpVideoModule->getCols();
			parry[cn] = (double)facevec[i].loc.y / (double)mpVideoModule->getRows();
			karr[cn] = facevec[i].time;

			//iarr[cn] = i;
			//karr[cn] = facevec[i].data.rows;
			cn++;
		}
	}

	// normalized positions
	root["faceURL"] =farr;
	root["facePosX"] = parrx;
	root["facePosY"] = parry;
	root["faceTimestamp"] = karr;  // facevec[0].time; // or initial time for a target to be appeared

	//root["faceImage"]["location"]= farr;
	//root["faceImage"]["timestamp"]= karr;
	//root["faceImage"]["selectedIndex"] = iarr;


	// demographics label and score for a target
	//darr[0] = mFaceData.mGenderStringFusion;
	//darr[1] = mFaceData.mAgeStringFusion;
	//darr[2] = mFaceData.mEthnicityStringFusion;
	//root["demographics"]["label"] = darr;

	sarr[0] = mFaceData.mGenderScoreFusion;
	sarr[1] = mFaceData.mAgeScoreFusion;
		esarr[0] = mFaceData.mEthnicityScoreFusion[0];
		esarr[1] = mFaceData.mEthnicityScoreFusion[1];
		esarr[2] = mFaceData.mEthnicityScoreFusion[2];
		esarr[3] = mFaceData.mEthnicityScoreFusion[3];
	sarr[2] = esarr;
	///root["demographics"]["score"] = sarr;

	root["gender"] = sarr[0];
	root["age"] =  sarr[1];
	root["ethnicity"] =sarr[2];


	// tracking time period
	const std::vector<FaceTrajectory::TrajectoryPoint> &pts =this->mTrajectory.getTrajectory();;

	root["startTimeTrack"] = pts[0].time;
	root["endTimeTrack"] = pts[(int)pts.size()-1].time;

	// For storing a tracking trajectory
	if(0)
	{
		//const std::vector<FaceTrajectory::TrajectoryPoint> &pts =this->mTrajectory.getTrajectory();;

		Json::Value ltarr;
		for(int i=0; i< (int)pts.size(); i++)
		{
			ltarr[0] = pts[i].loc.x;
			ltarr[1] = pts[i].loc.y;
			ltarr[2] = pts[i].time;

			tarr[i] = ltarr;
		}
		root["trajectory"]["timestamps"]= tarr;
		root["trajectory"]["totalTimestamps"] = (int)pts.size();
	}

	//  For monitoring the estimated demographics
/*	for(int i=0;i<facevec.size();i++)
	{
		std::string fno =boost::lexical_cast<std::string>(i+1);

		//root[fno] = i+1;"

        Json::Value jdata, jdmg, arr, garr, aarr, earr;

        jdmg["gender"]= facevec[i].genderString; //[facevec[i].genderLabel,facevec[i].genderScore];
		jdmg["genderScore"]= facevec[i].genderScore;
		jdmg["age"]= facevec[i].ageString; //[facevec[i].ageLabel,facevec[i].ageScore];
		jdmg["ageScore"]= facevec[i].ageScore;
		jdmg["ethnicity"]= facevec[i].ethnicityString; //[facevec[i].ethnicityLabel,facevec[i].ethnicityScore];

		arr[0] = facevec[i].ethnicityScore[0];
		arr[1] = facevec[i].ethnicityScore[1];
		arr[2] = facevec[i].ethnicityScore[2];mStoreTargetPath
		arr[3] = facevec[i].ethnicityScore[3];

		jdmg["ethnicityScore"]= arr;

		root[fno]["attributes"] = jdmg;

        jdata["x"] = (int)facevec[i].loc.x;
        jdata["y"] = (int)facevec[i].loc.y;
        jdata["width"] = (int)facevec[i].data.rows;
        jdata["height"] = (int)facevec[i].data.cols;

        jdata["time"] = facevec[i].time;
        root[fno]["box"]= jdata;
	}
*/

	//oss = boost::lexical_cast<std::string>(static_cast<long>(100.0f*facevec[0].time));
	//std::string filename = path + oss + ".json";

	time_t temp = (time_t)facevec[0].time;
	tm *bar = gmtime(&temp);

	std::ostringstream ossr;
	//ossr << bar->tm_mon <<  bar->tm_mday << bar->tm_year <<"_" << bar->tm_hour << bar->tm_min << bar->tm_sec;

	ossr << mFaceData.id <<"_"<< bar->tm_hour << bar->tm_min << bar->tm_sec;
	std::string filename = path + ossr.str() + ".json";

	std::ofstream savefile;
	savefile.open(filename.c_str());
	Json::StyledWriter styledWriter;
	savefile << styledWriter.write(root);
	savefile.close();


}

void FaceTarget::completeFaceData(int numFaces)
{
	// uuid (universally unique identifier) generation
	boost::uuids::uuid idx = boost::uuids::random_generator()();

	std::string id = boost::lexical_cast<std::string>(idx);

	mFaceData.id = id;

	// get face data
	const std::vector<SingleFaceDataType> facevec  = mFaceData.getFaceData();
	mFaceData.clearSelectedFaceDataVec();

    if(facevec.size() < 1)
    {
#ifdef FACETARGET_DEBUG    // no face data, but push to save the images (Exception)
        cout << "Face vector is empty. Check out!!" << endl;
#endif
        return;
    }

    // face Selection  -> output the indextm_year The number of years since 1900.
	std::vector<int> sidx;
    sidx.resize((int)facevec.size());

	std::fill(sidx.begin(), sidx.end(), 1);

    if(numFaces ==1)
    {
    	if(facevec.size() == 1)
    	{
    		mFaceData.addSelectedFaceDataVec(facevec[0]);
    	}
    	else
    	{
   			std::fill(sidx.begin(), sidx.end(), 0);

   			// output is selectedFaceDataVec
    		selectFaces(numFaces, sidx);
    	}
    }
    else if(numFaces > 1)
	{
    	if(facevec.size() == 1)
    	{
    		mFaceData.addSelectedFaceDataVec(facevec[0]);
    	}
    	else
    	{
			std::fill(sidx.begin(), sidx.end(), 0);

    		if(numFaces < (int)facevec.size())
    		{
    			selectFaces(numFaces, sidx);
    		}
    		else
    		{
    			selectFaces((int)facevec.size(), sidx);
    		}
    	}
	}

	// save the face meta data as a JSON file
	Json::Value root, darr, farr, sarr, esarr, tarr,iarr, karr;

	// a target ID
	root["faceId"] = mFaceData.id;

	sarr[0] = mFaceData.mGenderScoreFusion;
	sarr[1] = mFaceData.mAgeScoreFusion;
	esarr[0] = mFaceData.mEthnicityScoreFusion[0];
	esarr[1] = mFaceData.mEthnicityScoreFusion[1];
	esarr[2] = mFaceData.mEthnicityScoreFusion[2];
	esarr[3] = mFaceData.mEthnicityScoreFusion[3];
	sarr[2] = esarr;
	//root["demographics"]["score"] = sarr;

	root["gender"] = sarr[0];
	root["age"] =  sarr[1];
	root["ethnicity"] =sarr[2];

	// tracking time period
	const std::vector<FaceTrajectory::TrajectoryPoint> &pts =this->mTrajectory.getTrajectory();;

	root["startTimeTrack"] = pts[0].time;
	root["endTimeTrack"] = pts[(int)pts.size()-1].time;

	// Trajectory
	if(0)
	{
		//const std::vector<FaceTrajectory::TrajectoryPoint> &pts =this->mTrajectory.getTrajectory();;

		Json::Value ltarr;
		for(int i=0; i< (int)pts.size(); i++)
		{
			ltarr[0] = pts[i].loc.x;
			ltarr[1] = pts[i].loc.y;
			ltarr[2] = pts[i].time;

			tarr[i] = ltarr;
		}
		root["trajectory"]["timestamps"]= tarr;
		root["trajectory"]["totalTimestamps"] = (int)pts.size();
	}

	mFaceData.mvJson = root;

}

void FaceTarget::completeFaceDataDMG(boost::shared_ptr<aitvml::Recognition> dmgRecognizer, int numFaces)
{
	// uuid (universally unique identifier) generation
	boost::uuids::uuid idx = boost::uuids::random_generator()();

	std::string id = boost::lexical_cast<std::string>(idx);

	mFaceData.id = id;

	// get face data
	const std::vector<SingleFaceDataType> facevec  = mFaceData.getFaceData();
	mFaceData.clearSelectedFaceDataVec();

    if(facevec.size() < 1)
    {
#ifdef FACETARGET_DEBUG    // no face data, but push to save the images (Exception)
        cout << "Face vector is empty. Check out!!" << endl;
#endif
        return;
    }

    // face Selection  -> output the indextm_year The number of years since 1900.
	std::vector<int> sidx;
    sidx.resize((int)facevec.size());

	std::fill(sidx.begin(), sidx.end(), 1);

    if(numFaces ==1)
    {
    	if(facevec.size() == 1)
    	{
    		mFaceData.addSelectedFaceDataVec(facevec[0]);
    	}
    	else
    	{
   			std::fill(sidx.begin(), sidx.end(), 0);

   			// output is selectedFaceDataVec
    		selectFaces(numFaces, sidx);
    	}
    }
    else if(numFaces > 1)
	{
    	if(facevec.size() == 1)
    	{
    		mFaceData.addSelectedFaceDataVec(facevec[0]);
    	}
    	else
    	{
			std::fill(sidx.begin(), sidx.end(), 0);

    		if(numFaces < (int)facevec.size())
    		{
    			selectFaces(numFaces, sidx);
    		}
    		else
    		{
    			selectFaces((int)facevec.size(), sidx);
    		}
    	}
	}

    // Recognize with selected faces
    //DMGRecogFromSelectedFaces(mFaceData);

    std::vector<SingleFaceDataType> selectedfacevec  = mFaceData.getSelectedFaceData();

    for(unsigned int i=0; i< selectedfacevec.size(); i++)
    {
    	cv::Mat faceChip, faceChipNorm;
    	selectedfacevec[i].data.copyTo(faceChip);
    	cv::resize(faceChip, faceChipNorm,cv::Size(30,30));

    	// Demographics recognition using the facechip
    	dmgRecognizer->cvtMatToImage32(faceChipNorm, &dmgRecognizer->faceImage);
    	dmgRecognizer->processFromFace();

    	selectedfacevec[i].genderScore = dmgRecognizer->mEstGenderScore;
    	selectedfacevec[i].ageScore = dmgRecognizer->mEstAgeScore;
    	selectedfacevec[i].ethnicityScore[0] = dmgRecognizer->mEstAfrScore;
    	selectedfacevec[i].ethnicityScore[1] = dmgRecognizer->mEstCauScore;
    	selectedfacevec[i].ethnicityScore[2] = dmgRecognizer->mEstHisScore;
    	selectedfacevec[i].ethnicityScore[3] = dmgRecognizer->mEstOriScore;

    	selectedfacevec[i].genderLabel = dmgRecognizer->mEstGenderLabel;
    	selectedfacevec[i].ageLabel = dmgRecognizer->mEstAgeLabel;
    	selectedfacevec[i].ethnicityLabel = dmgRecognizer->mEstEthnicityLabel;

    	selectedfacevec[i].genderString = dmgRecognizer->mEstGenderString;
    	selectedfacevec[i].ageString = dmgRecognizer->mEstAgeString;
    	selectedfacevec[i].ethnicityString = dmgRecognizer->mEstEthnicityString;
    }

    if(selectedfacevec.size() > 1)
    {
		DMGData dmgFusion;
		DMGDecisionFusion(selectedfacevec, dmgFusion);
		mFaceData.mGenderScoreFusion = dmgFusion.genderScore;
		mFaceData.mAgeScoreFusion = dmgFusion.ageScore;
		mFaceData.mEthnicityScoreFusion[0] = dmgFusion.ethnicityScore[0];
		mFaceData.mEthnicityScoreFusion[1] = dmgFusion.ethnicityScore[1];
		mFaceData.mEthnicityScoreFusion[2] = dmgFusion.ethnicityScore[2];
		mFaceData.mEthnicityScoreFusion[3] = dmgFusion.ethnicityScore[3];
		mFaceData.mGenderLabelFusion = dmgFusion.genderLabel;
		mFaceData.mAgeLabelFusion = dmgFusion.ageLabel;
		mFaceData.mEthnicityLabelFusion = dmgFusion.ethnicityLabel;
		mFaceData.mGenderStringFusion = dmgFusion.genderString;
		mFaceData.mAgeStringFusion = dmgFusion.ageString;
		mFaceData.mEthnicityStringFusion = dmgFusion.ethnicityString;
    }
    else
    {
		mFaceData.mGenderScoreFusion = selectedfacevec[0].genderScore;
		mFaceData.mAgeScoreFusion = selectedfacevec[0].ageScore;
		mFaceData.mEthnicityScoreFusion[0] = selectedfacevec[0].ethnicityScore[0];
		mFaceData.mEthnicityScoreFusion[1] = selectedfacevec[0].ethnicityScore[1];
		mFaceData.mEthnicityScoreFusion[2] = selectedfacevec[0].ethnicityScore[2];
		mFaceData.mEthnicityScoreFusion[3] = selectedfacevec[0].ethnicityScore[3];
		mFaceData.mGenderLabelFusion = selectedfacevec[0].genderLabel;
		mFaceData.mAgeLabelFusion = selectedfacevec[0].ageLabel;
		mFaceData.mEthnicityLabelFusion = selectedfacevec[0].ethnicityLabel;
		mFaceData.mGenderStringFusion = selectedfacevec[0].genderString;
		mFaceData.mAgeStringFusion = selectedfacevec[0].ageString;
		mFaceData.mEthnicityStringFusion = selectedfacevec[0].ethnicityString;
    }


	// save the face meta data as a JSON file
	Json::Value root, darr, farr, sarr, esarr, tarr,iarr, karr;

	// a target ID
	root["faceId"] = mFaceData.id;

	sarr[0] = mFaceData.mGenderScoreFusion;
	sarr[1] = mFaceData.mAgeScoreFusion;
	esarr[0] = mFaceData.mEthnicityScoreFusion[0];
	esarr[1] = mFaceData.mEthnicityScoreFusion[1];
	esarr[2] = mFaceData.mEthnicityScoreFusion[2];
	esarr[3] = mFaceData.mEthnicityScoreFusion[3];
	sarr[2] = esarr;
	//root["demographics"]["score"] = sarr;

	root["gender"] = sarr[0];
	root["age"] =  sarr[1];
	root["ethnicity"] =sarr[2];

	// tracking time period
	const std::vector<FaceTrajectory::TrajectoryPoint> &pts =this->mTrajectory.getTrajectory();;

	root["startTimeTrack"] = pts[0].time;
	root["endTimeTrack"] = pts[(int)pts.size()-1].time;

	// Trajectory
	if(0)
	{
		//const std::vector<FaceTrajectory::TrajectoryPoint> &pts =this->mTrajectory.getTrajectory();;

		Json::Value ltarr;
		for(int i=0; i< (int)pts.size(); i++)
		{
			ltarr[0] = pts[i].loc.x;
			ltarr[1] = pts[i].loc.y;
			ltarr[2] = pts[i].time;

			tarr[i] = ltarr;
		}
		root["trajectory"]["timestamps"]= tarr;
		root["trajectory"]["totalTimestamps"] = (int)pts.size();
	}

	mFaceData.mvJson = root;

}

void FaceTarget::DMGDecisionFusion(const std::vector<SingleFaceDataType> dmgvec, DMGData &dmgFusion)
{
	// Method 1: Majority voting
	// fusion, then setup the values to elementary DMGData structure
	// : score-level fusion
	float gscore=0.0f, ascore=0.0f, escore[4]={0.0f};

	SingleFaceDataType  itr;
	int cntGender=0, cntAge=0, cntEthnicity=0;

	for (int i=0;i < (int)dmgvec.size();i++)
	{
		itr = dmgvec[i];

		// for gender
		if(itr.genderLabel != -1)
		{
			gscore += itr.genderScore;
			cntGender++;
		}
		if(itr.ageLabel != -1)
		{
			ascore += itr.ageScore;
			cntAge++;
		}

		if(itr.ethnicityLabel != -1)
		{
			for (int j=0; j<4; j++)
			{
				escore[j] += itr.ethnicityScore[j];
			}
			cntEthnicity++;
		}
	}
	if(cntGender > 0)
	{
		gscore /= cntGender;
	}
	else
		gscore = -1.0f;

	if(cntAge > 0)
		ascore /= cntAge;
	else
		ascore = -1.0f;

	if(cntEthnicity > 0)
	{
		for (int j=0; j<4; j++)
		{
			escore[j] /= cntEthnicity;
		}
	}
	else
	{
		for (int j=0; j<4; j++)
		{
			escore[j] = -1.0f;
		}
	}

	// Fused decision
	// Gender
	dmgFusion.genderScore = gscore;
	if (gscore <= -10.0)
	{
		dmgFusion.genderLabel = -1;
		dmgFusion.genderString = "Unknown";
	}
	else if( gscore < 0.1 )
	{
		dmgFusion.genderLabel = 0;
		dmgFusion.genderString = "female";
	}
	else
	{
		dmgFusion.genderLabel = 1;
		dmgFusion.genderString = "male";
	}

	// Age
	dmgFusion.ageScore = ascore;
	if( ascore <= -10.0)
	{
		dmgFusion.ageLabel = -1;
		dmgFusion.ageString = "Unknown";
	}
	else if( ascore < 18 )
	{
		dmgFusion.ageLabel = 0;
		dmgFusion.ageString = "child";
	}
	else if(ascore >=18 && ascore <30)
	{
		dmgFusion.ageLabel = 1;
		dmgFusion.ageString = "young adult";
	}
	else if(ascore >=30 && ascore <55)
	{
		dmgFusion.ageLabel = 2;
		dmgFusion.ageString = "adult";
	}
	else
	{
		dmgFusion.ageLabel = 3;
		dmgFusion.ageString = "senior";
	}

	// Ethnicity
	for (int i=0;i<4;i++)
	{
		dmgFusion.ethnicityScore[i] = escore[i];
	}
	if(escore[0] <= -10.0  && escore[0] <= -10.0  &&  escore[0] <= -10.0 && escore[0] <= -10.0 )
	{
		dmgFusion.ethnicityLabel = -1;
		dmgFusion.ethnicityString = "Unknown";
	}
	else
	{

		if(escore[0]>= escore[1] && escore[0]>= escore[2] && escore[0]>= escore[3])
		{
			dmgFusion.ethnicityLabel = 0;
			dmgFusion.ethnicityString = "african american";
		}
		else if(escore[1]>= escore[0] && escore[1]>= escore[2] && escore[1]>= escore[3])
		{
			dmgFusion.ethnicityLabel = 1;
			dmgFusion.ethnicityString = "caucasian";
		}
		else if(escore[2]>= escore[0] && escore[2]>= escore[1] && escore[2]>= escore[3])
		{
			dmgFusion.ethnicityLabel = 2;
			dmgFusion.ethnicityString = "hispanic";
		}
		else if(escore[3]>= escore[0] && escore[3]>= escore[1] && escore[3]>= escore[2])
		{
			dmgFusion.ethnicityLabel = 3;
			dmgFusion.ethnicityString = "oriental";
		}
	}
	// code here for a new method later

}


} // of namespace vml

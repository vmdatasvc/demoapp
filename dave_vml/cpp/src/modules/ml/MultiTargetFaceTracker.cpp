/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * MultiTargetFaceTracker.cpp
 *
 *  Created on: Jan 26, 2016
 *      Author: dkim
 */

#include <modules/ml/FaceVideoModule.hpp>
#include <modules/ml/MultiTargetFaceTracker.hpp>
#include <modules/ml/FaceModuleManager.hpp>
#include <modules/ml/FaceBaseTracker.hpp>
#include <modules/ml/FaceDetector.hpp>
//#include <modules/ml/FaceTargetManager.hpp>
#include <modules/ml/FaceTargetManager.hpp>


//#include <core/Util.hpp>
#include <modules/utility/Util.hpp>

//#define DEBUG_MULTITHREAD

// Visual tracking codes here
namespace vml {

pthread_mutex_t		FaceMultitargetTrackerMutex;		// mutex for multitarget tracker
pthread_mutex_t		FaceMultitargetTrackerCompletedFaceDataMutex;

/* class MultiTargetFaceTracker */
MultiTargetFaceTracker::MultiTargetFaceTracker() :
		nFrames(0),
		mProcessorThread(0),
		mGrabberThread(0),
		mDetectorThread(0),
		mUpdateThread(0),
		mStopThreads(false),
		mbGrabReady(false),
		mbUpdateReady(false),
		mbDetectorReady(false),
		mbMultithreadsOn(false),
		mHeartbeatInterval(20.f),
		mPreviousHeartbeatTime(gCurrentTime()),
		mMaxHeartbeatIntervalProportion(1.5f)
{
	// register modules
	FaceModuleManager::instance()->registerSettings(boost::shared_ptr<SETTINGS>(new SETTINGS));
	// cacheing modules for faster access
	mpSettings = FaceModuleManager::instance()->getSettings();

	FaceModuleManager::instance()->registerVideoModule(boost::shared_ptr<FaceVideoModule>(new FaceVideoModule));
	mpVideoModule = FaceModuleManager::instance()->getVideoModule();

	FaceModuleManager::instance()->registerDetector(boost::shared_ptr<FaceDetector>(new FaceDetector));
	mpFaceDetector = FaceModuleManager::instance()->getDetector();

	FaceModuleManager::instance()->registerFaceTargetManager(boost::shared_ptr<FaceTargetManager>(new FaceTargetManager()));
	mpFaceTargetManager = FaceModuleManager::instance()->getFaceTargetManager();

	mDoDemoRecog = mpSettings->getBool("DMGRecognition/doDMGRecognition [bool]", false, false);
	mDoDemoRecogEnd = mpSettings->getBool("DMGRecognition/doDMGRecognitionEnd [bool]", false, false);

	if(mDoDemoRecog || mDoDemoRecogEnd)
	{
		FaceModuleManager::instance()->registerDMGRecognizer(boost::shared_ptr<aitvml::Recognition>(new aitvml::Recognition()));
		mpDMGRecognizer = FaceModuleManager::instance()->getDMGRecognizer();
	}
}

MultiTargetFaceTracker::~MultiTargetFaceTracker()
{
}

// NOTE: readSettings/initializeVideo is provided for Linux-based system.
//       in VMS, settings and video modules are set and initialized before tracker was created.
void MultiTargetFaceTracker::readSettings(const std::string& fname,bool merge)
{
	mpSettings->parseXml(fname,merge);
}

void MultiTargetFaceTracker::setSettings(boost::shared_ptr<SETTINGS> settings)
{
	// replace registered setting,
	FaceModuleManager::instance()->registerSettings(settings);
	mpSettings.swap(settings);

	// initialize setting variables
	mHeartbeatInterval = mpSettings->getDouble("IoT/heartbeatIntearval", 20.f, false);
	mMaxHeartbeatIntervalProportion = std::max(mpSettings->getDouble("IoT/maxHeartbeatIntervalProportion", 1.5f, false), (double)1.5f);
}

void MultiTargetFaceTracker::setIOTCommunicator(boost::shared_ptr<IOT_Communicator> iotc)
{
	mpIOTCommunicator = iotc;
}

void MultiTargetFaceTracker::setVideoModule(boost::shared_ptr<FaceVideoModule> video_module)
{
	// register video module
	FaceModuleManager::instance()->registerVideoModule(video_module);
	mpVideoModule.swap(video_module);
}

void MultiTargetFaceTracker::initializeVideo(std::string fname)
{
	mpVideoModule->initialize(mpSettings,fname);
}

void MultiTargetFaceTracker::initializeModules()
{
	// asserting settings and video modules
	VML_ASSERT(mpSettings);
	VML_ASSERT(mpVideoModule);

	// initialize modules
	mpFaceDetector->initialize(mpVideoModule, mpSettings);

	if(mDoDemoRecog || mDoDemoRecogEnd)
	{
		mpDMGRecognizer->initialize(mpSettings);
	}
	else
	{
		mpDMGRecognizer = NULL;
	}

	mpFaceTargetManager->initialize(mpVideoModule,mpFaceDetector,mpDMGRecognizer, mpSettings);

}


// For single thread
void MultiTargetFaceTracker::singleThreadStart() // process each video frame
{
	   pthread_attr_init(&mThreadAttr);
	   pthread_attr_setdetachstate(&mThreadAttr, PTHREAD_CREATE_JOINABLE);

	    // grab one frame to temp
		if (!mpVideoModule->grabFrame())
		{
			// TODO: return exception
		}

	   mStopThreads = false;
		// start frame grab thread
	    if(pthread_create(&mProcessorThread, &mThreadAttr, singleProcessThread, (void *)this))
	    {
	//        fprintf(stderr, "Error in pthread_create(frameGrabThread)\n");
	    	// TODO: return exception

	    } else {
	//        printf( " -- pthread_create(frameGrabThread) -- \n");
	    	// TODO: log message here
	    }
}

void* MultiTargetFaceTracker::singleProcessThread(void* apdata) // process each video frame
{
	MultiTargetFaceTracker* pc = (MultiTargetFaceTracker*)apdata;
	pc->singleProcessLoop();
	return NULL;
}

//#define DEBUG_MULTITHREAD
 void MultiTargetFaceTracker::singleProcessLoop(void) // process each video frame
{

	while(!mStopThreads)
	{
#ifdef DEBUG_MULTITHREAD
		double start_time = gCurrentTime();
#endif
		if(not mpVideoModule->grabFrame())
		{
			//printf("Error: GrabFrame again!\n");
			usleep(10);
			continue;
		}
#ifdef DEBUG_MULTITHREAD
		double grab_time= gCurrentTime();
#endif

		// update existing targets
		mpFaceTargetManager->updateTargets();

#ifdef DEBUG_MULTITHREAD
		double update_time= gCurrentTime();
#endif

		// run face detection first
		mpFaceDetector->detect();

#ifdef DEBUG_MULTITHREAD
		double dt_time= gCurrentTime();
#endif
		// create new targets after verification
		mpFaceTargetManager->getDetectedTargetList();
		mpFaceTargetManager->addNewTargetsFromDetector();

#ifdef DEBUG_MULTITHREAD
		double ant_time= gCurrentTime();

		printf("TIME: single thread (Grab): %f\n", grab_time-start_time);
		printf("TIME: single thread (Update): %f\n", update_time-grab_time);
		printf("TIME: single thread (Detect): %f\n", dt_time-update_time);
		printf("TIME: single thread (AddNewTartget): %f\n", ant_time-dt_time);

#endif

		// clear completed target list  // Testing purpose
		//mpFaceTargetManager->clearCompletedTargetList();

#ifdef DEBUG_MULTITHREAD
		double elapsed_time= gCurrentTime() - start_time;
		printf("TIME: single thread: %f\n", elapsed_time);
#endif

		nFrames++;

#ifdef DEBUG_MULTITHREAD
		printf("Processing frame Done: nframes: %d\n",nFrames);
#endif

		// send a heartbeat
		this->sendHeartBeat();

	}
}

// 2 threads methods
void MultiTargetFaceTracker::_2ThreadPipeliningStart()
{
	pthread_attr_init(&mThreadAttr);
    pthread_attr_setdetachstate(&mThreadAttr, PTHREAD_CREATE_JOINABLE);

    // allocate temp frames
    mpVideoModule->allocateTempFrames(2);

	mbDetectorReady = false;
	mStopThreads = false;
	mbMultithreadsOn = true;

	try {
		if(pthread_create(&mDetectorThread, &mThreadAttr, _2ThreadPipeliningDetectorThread, (void *)this))
			{
		//        fprintf(stderr, "Error in pthread_create(frameGrabThread)\n");
				// TODO: return exception

			} else {
		//        printf( " -- pthread_create(frameGrabThread) -- \n");
				// TODO: log message here
			}

		if(pthread_create(&mUpdateThread, &mThreadAttr, _2ThreadPipeliningUpdateThread, (void *)this))
			{
		//        fprintf(stderr, "Error in pthread_create(frameGrabThread)\n");
				// TODO: return exception

			} else {
		//        printf( " -- pthread_create(frameGrabThread) -- \n");
				// TODO: log message here
			}
	}
	catch (std::runtime_error &e)
	{
		mpIOTCommunicator->sendError(e.what(),true,true);
	}

}

void* MultiTargetFaceTracker::_2ThreadPipeliningDetectorThread(void *apdata)
{
	printf("Initiating detectorThread\n");
	MultiTargetFaceTracker* pc = (MultiTargetFaceTracker*)apdata;
	pc->_2ThreadPipeliningDetectorLoop();
	printf("detectorThread Done\n");
	return NULL;

}
void MultiTargetFaceTracker::_2ThreadPipeliningDetectorLoop(void)
{
	try {
		while(!mStopThreads)
		{

			//double start_time = gCurrentTime();
			//printf("1 mbDetectorReady is : %d\n",mbDetectorReady);

			while(mbDetectorReady) {
				usleep(10); // wait until fetch is ready
			}

			//printf("Grab..\n");
			if (!mpVideoModule->grabInGrabFrame())
			{
				// TODO: return exception
				//mStopThreads = true;
				//printf("Error: GrabFrame again!\n");
#ifdef DEBUG_MULTITHREAD
				std::cout << "Error: GrabFrame.." << std::endl;
#endif
				usleep(10);
				continue;
			}

			//printf("Detect..\n");
			// run face detection first
			mpFaceDetector->detectInGrabFrame();

			pthread_mutex_lock(&FaceMultitargetTrackerMutex);
			mpVideoModule->copyGrabFrameToCurrent();
			//pthread_mutex_unlock(&FaceMultitargetTrackerMutex);


			//pthread_mutex_lock(&FaceMultitargetTrackerMutex);
			// copy original image and detection list to the buffer
			//printf("Mutex: copy current to GrabFrame\n");
			//mpVideoModule->copyCurrentToGrabFrame();
			//printf("Mutex: change mbDetecorReady as true\n");
			mbDetectorReady = true;
			pthread_mutex_unlock(&FaceMultitargetTrackerMutex);

			//double elapsed_time = start_time- loop1_start_time;
			//printf("TIME: 1st thread wait = %f\n", elapsed_time);

			//double elapsed_time2 = gCurrentTime()- loop1_start_time;
			//printf("TIME: 1st thread loop = %f\n", elapsed_time2);

#ifdef DEBUG_MULTITHREAD
			std::cout << "Detect thread.." << std::endl;
#endif

		}
	}
	catch (std::runtime_error &e)
	{
		mpIOTCommunicator->sendError(e.what(),true,true);
	}

}
void* MultiTargetFaceTracker::_2ThreadPipeliningUpdateThread(void *apdata)
{
	printf("Initiating updateThread\n");
	MultiTargetFaceTracker* pc = (MultiTargetFaceTracker*)apdata;
	pc->_2ThreadPipeliningUpdateLoop();
	printf("updateThread done\n");
	return NULL;
}
void MultiTargetFaceTracker::_2ThreadPipeliningUpdateLoop(void)
{
	try {

		while(!mStopThreads)
		{
			// wait for grabTempFrame() to finish;
			//printf("2 mbDetectorReady is : %d\n",mbDetectorReady);
			while (!mbDetectorReady){
				//printf("Waiting temp frame be updated\n");
				usleep(10);	// wait until grab is ready
			}

			//mpVideoModule->setGrabFrameToCurrent();
			pthread_mutex_lock(&FaceMultitargetTrackerMutex);
			// copy original image and detection list from the buffer
			//printf("Mutex: set Grab frame to current.\n");

			mpVideoModule->copyGrabFrameToCurrent();
			//printf("Mutex: get the detected target list.\n");
			mpFaceTargetManager->getDetectedTargetList();

			mbDetectorReady = false;

			pthread_mutex_unlock(&FaceMultitargetTrackerMutex);

			//printf("Add New targets from detector\n");
			// create new targets after verification	// move complete trajectory to the completed trajectory list
			mpFaceTargetManager->addNewTargetsFromDetector();

			//printf("UpdateTargets\n");
			// update existing targets
			mpFaceTargetManager->updateTargets();

			//if(nFrames == 16)
			//	printf("This..\n");

			nFrames++;

			// move complete trajectory to the completed trajectory list
			// clear completed target list  // Testing purpose
			//pthread_mutex_lock(&FaceMultitargetTrackerCompletedFaceDataMutex);
			//mpFaceTargetManager->retrieveCompletedFaceData(mCompletedFaceData);
			//pthread_mutex_unlock(&FaceMultitargetTrackerCompletedFaceDataMutex);

			//double elapsed_time = start_time - loop2_start_time;
			//printf("TIME: 2nd thread wait = %f\n", elapsed_time);

			//double elapsed_time2 = gCurrentTime()-loop2_start_time;
			//printf("TIME: 2nd thread loop = %f\n", elapsed_time2);

			// send a heartbeat
			this->sendHeartBeat();

#ifdef DEBUG_MULTITHREAD
			std::cout << "Update thread.." << std::endl;
#endif
		}

	}
	catch (std::runtime_error &e)
	{
		mpIOTCommunicator->sendError(e.what(),true,true);
	}


}

// 3 threads methods
void 	MultiTargetFaceTracker::_3ThreadPipeliningStart()
{
    pthread_attr_init(&mThreadAttr);
    pthread_attr_setdetachstate(&mThreadAttr, PTHREAD_CREATE_JOINABLE);

    // allocate temp frames
    mpVideoModule->allocateTempFrames(3);

	mbGrabReady = false;
	mbDetectorReady = false;

	mStopThreads = false;
	mbMultithreadsOn = true;

	// start frame grab thread
    if(pthread_create(&mGrabberThread, &mThreadAttr, _3ThreadPipeliningFrameGrabThread, (void *)this))
    {
//        fprintf(stderr, "Error in pthread_create(frameGrabThread)\n");
    	// TODO: return exception

    } else {
//        printf( " -- pthread_create(frameGrabThread) -- \n");
    	// TODO: log message here
    }

	// start background subtraction thread
    if(pthread_create(&mDetectorThread, &mThreadAttr, _3ThreadPipeliningDetectorThread, (void *)this))
    {
//        fprintf(stderr, "Error in pthread_create(frameGrabThread)\n");
    	// TODO: return exception

    } else {
//        printf( " -- pthread_create(frameGrabThread) -- \n");
    	// TODO: log message here
    }

	// start process thread
    if(pthread_create(&mUpdateThread, &mThreadAttr, _3ThreadPipeliningUpdateThread, (void *)this))
    {
//        fprintf(stderr, "Error in pthread_create(frameGrabThread)\n");
    	// TODO: return exception

    } else {
//        printf( " -- pthread_create(frameGrabThread) -- \n");
    	// TODO: log message here
    }

}
void* 	MultiTargetFaceTracker::_3ThreadPipeliningFrameGrabThread(void *apdata)
{
	printf("Initiating frameGrabThread\n");
	MultiTargetFaceTracker* pc = (MultiTargetFaceTracker*)apdata;
	pc->_3ThreadPipeliningFrameGrabLoop();
	printf("frameGrabThread Done\n");
	return NULL;
}
void 	MultiTargetFaceTracker::_3ThreadPipeliningFrameGrabLoop(void)
{
	while (!mStopThreads)
	{
		while (mbGrabReady){
//			printf("Waiting temp frame to be cleared\n");
			usleep(10);	// wait until fetch is ready
		}

//		printf("Temp frame cleared. Grabbing new temp frame\n");
		if (!mpVideoModule->grabInGrabFrame())
		{
			// TODO: return exception
			usleep(10);
			continue;
		}

		pthread_mutex_lock(&FaceMultitargetTrackerMutex);
		mbGrabReady = true;
		pthread_mutex_unlock(&FaceMultitargetTrackerMutex);
	}
}
void* 	MultiTargetFaceTracker::_3ThreadPipeliningDetectorThread(void *apdata)
{
	printf("Initiating detectorThread\n");
	MultiTargetFaceTracker* pc = (MultiTargetFaceTracker*)apdata;
	pc->_3ThreadPipeliningDetectorLoop();
	printf("detectorThread Done\n");
	return NULL;
}
void 	MultiTargetFaceTracker::_3ThreadPipeliningDetectorLoop(void)
{
	while (!mStopThreads)
	{
		//double start_time = gCurrentTime();

		while (mbDetectorReady || !mbGrabReady){
//			printf("Waiting temp frame to be cleared\n");
			usleep(10);	// wait until fetch is ready
		}

		pthread_mutex_lock(&FaceMultitargetTrackerMutex);
		mpVideoModule->copyGrabFrameToDetectorFrame();
		mbGrabReady = false;
		pthread_mutex_unlock(&FaceMultitargetTrackerMutex);

		//printf("Temp frame cleared. Grabbing new temp frame\n");
		mpFaceDetector->detectInDetectorFrame();

		pthread_mutex_lock(&FaceMultitargetTrackerMutex);
		mbDetectorReady = true;
		pthread_mutex_unlock(&FaceMultitargetTrackerMutex);

	}

}
void* 	MultiTargetFaceTracker::_3ThreadPipeliningUpdateThread(void *apdata)
{
	printf("Initiating updateThread\n");
	MultiTargetFaceTracker* pc = (MultiTargetFaceTracker*)apdata;
	pc->_3ThreadPipeliningUpdateLoop();
	printf("updateThread done\n");
	return NULL;
}
void 	MultiTargetFaceTracker::_3ThreadPipeliningUpdateLoop(void)
{
	while(!mStopThreads)
		{
			// wait for grabTempFrame() to finish;
	//		printf("mbGrabReady is : %d\n",mbGrabReady);
			while (!mbDetectorReady){
	//			printf("Waiting temp frame be updated\n");
				usleep(10);	// wait until grab is ready
			}
	//		printf("Temp frame updated. Copying to frame buffer\n");
	//		mpVideoModule->setBackSubFrameToCurrent();
	//		printf("Copying to frame buffer done. Start processing\n");
			pthread_mutex_lock(&FaceMultitargetTrackerMutex);
			mpVideoModule->copyDetectorFrameToCurrent();
			mpFaceTargetManager->getDetectedTargetList();
			mbDetectorReady = false;
			pthread_mutex_unlock(&FaceMultitargetTrackerMutex);

	//		printf("mutexlock is the problem??\n");
			mpFaceTargetManager->addNewTargetsFromDetector();
			mpFaceTargetManager->updateTargets();

			nFrames++;

			// return completed trajectory
			// clear completed target list  // Testing purpose
			//pthread_mutex_lock(&FaceMultitargetTrackerCompletedFaceDataMutex);
			//mpFaceTargetManager->retrieveCompletedFaceData(mCompletedFaceData);
			//pthread_mutex_unlock(&FaceMultitargetTrackerCompletedFaceDataMutex);

			// send iot heartbeat
			this->sendHeartBeat();
		}
}

void MultiTargetFaceTracker::stopThreads()
{
	mStopThreads = true;
	// block until all threads return
	if(mGrabberThread) pthread_join(mGrabberThread,NULL);
	if(mProcessorThread) pthread_join(mProcessorThread,NULL);
	if(mUpdateThread) pthread_join(mUpdateThread,NULL);
	if(mDetectorThread) pthread_join(mDetectorThread,NULL);

}

void MultiTargetFaceTracker::stopThreads(int numThread)
{
	mStopThreads = true;

	if(numThread == 1)
	{
		pthread_join(mProcessorThread,NULL);
	}
	else if(numThread >= 2)
	{
		// block until all threads return
		pthread_join(mUpdateThread,NULL);
		pthread_join(mDetectorThread,NULL);
		if(numThread >=3)
		{
			pthread_join(mGrabberThread,NULL);
		}
	}
	pthread_join(mGrabberThread,NULL);
}


bool MultiTargetFaceTracker::process() // process each video frame
{

    if (not mpVideoModule->grabFrame())
    {
    	return false;
    }

    // run face detection first
    mpFaceDetector->detect();

    // create new targets after verification
    mpFaceTargetManager->getDetectedTargetList();
    mpFaceTargetManager->addNewTargetsFromDetector();

    // update existing targets
    mpFaceTargetManager->updateTargets();

	// clear completed target list  // Testing purpose
    //mpFaceTargetManager->clearCompletedTargetList();


    nFrames++;
    return true;


}

const cv::Mat&  MultiTargetFaceTracker::getGrabbedImage(void)
{
    return FaceModuleManager::instance()->getVideoModule()->currentFrame()->image;
}
/*
const cv::Mat&  MultiTargetFaceTracker::getForeground(void)
{
	boost::shared_ptr<FaceDetector> detectorPtr = boost::dynamic_pointer_cast<FaceDetector>(FaceModuleManager::instance()->getDetector());
    return detectorPtr->getForegroundImage();
}

const cv::Mat& MultiTargetFaceTracker::getBackground(void)
{
	boost::shared_ptr<FaceDetector> detectorPtr = boost::dynamic_pointer_cast<FaceDetector>(FaceModuleManager::instance()->getDetector());
    return detectorPtr->getBackgroundImage();
}



const cv::Mat& MultiTargetFaceTracker::getBlobImage(void)
{
	boost::shared_ptr<FaceDetector> detectorPtr = boost::dynamic_pointer_cast<FaceDetector>(FaceModuleManager::instance()->getDetector());
    return detectorPtr->getBlobImage();
}
*/

const cv::Mat& MultiTargetFaceTracker::getBackProjection(void)
{
    return FaceModuleManager::instance()->getFaceTargetManager()->getBackProjImage();
}
/*
void MultiTargetFaceTracker::getUndistortImage(cv::Mat &cur_undist)
{
	// for debugging, undistort the input image
	cur_undist.release();
	FaceModuleManager::instance()->getVideoModule()->getCurrentUndistortedImage(cur_undist);
}
*/

int MultiTargetFaceTracker::getCurrentFrameNumber()
{
	//return FaceModuleManager::instance()->getVideoModule()->getCurrentFrameNumber();
	return nFrames;

}

unsigned int MultiTargetFaceTracker::getNumActiveTargets()
{
	return FaceModuleManager::instance()->getFaceTargetManager()->getNumActiveTargets();
}

unsigned int MultiTargetFaceTracker::getNumTotalTargets()
{
	return FaceModuleManager::instance()->getFaceTargetManager()->getNumTotalTargets();
}

unsigned int MultiTargetFaceTracker::getNumInactiveTargets()
{
	return FaceModuleManager::instance()->getFaceTargetManager()->getNumInactiveTargets();
}

unsigned int MultiTargetFaceTracker::getNumCompletedTargets()
{
	return FaceModuleManager::instance()->getFaceTargetManager()->getNumCompletedTargets();
}

void MultiTargetFaceTracker::getCompletedTrajectories(std::vector<FaceTrajectory> &trajlist)
{
	FaceModuleManager::instance()->getFaceTargetManager()->retrieveCompletedTrajectories(trajlist);
}

void MultiTargetFaceTracker::getCompletedFaceData(std::vector<TargetFaceData> &fdlist)
{
	FaceModuleManager::instance()->getFaceTargetManager()->retrieveCompletedFaceData(fdlist);
}
// display methods

void MultiTargetFaceTracker::drawActiveTargets(cv::Mat &display_image)
{
	FaceModuleManager::instance()->getFaceTargetManager()->drawActiveTargets(display_image);
}

void MultiTargetFaceTracker::drawActiveTargetsWithDMG(cv::Mat &display_image)
{
	FaceModuleManager::instance()->getFaceTargetManager()->drawActiveTargetsWithDMG(display_image);
}

void MultiTargetFaceTracker::drawActiveTrajectories(cv::Mat &display_image)
{
	FaceModuleManager::instance()->getFaceTargetManager()->drawActiveTrajectories(display_image);
}


void  MultiTargetFaceTracker::drawValidNewTargets(cv::Mat &display_image)
{
	FaceModuleManager::instance()->getDetector()->drawValidTargets(display_image);
}

void MultiTargetFaceTracker::drawTrackingRegions(cv::Mat &display_image)
{
	FaceModuleManager::instance()->getVideoModule()->drawTrackRegions(display_image);
}
/*
void MultiTargetVisionTracker::drawUndistortedActiveTargets(cv::Mat &display_image)
{
	VisionModuleManager::instance()->getVisionTargetManager()->drawUndistortedActiveTargets(display_image);
}


void MultiTargetFaceTracker::drawInactiveTargets(cv::Mat &display_image)
{
	FaceModuleManager::instance()->getFaceTargetManager()->drawInactiveTargets(display_image);
}

void MultiTargetFaceTracker::drawDetectedTargets(cv::Mat &display_image)
{
	FaceModuleManager::instance()->getDetector()->drawDetectedTargets(display_image);
}
*/
//void MultiTargetVisionTracker::drawActiveTargetMap(cv::Mat &display_image)
//{
//	VisionModuleManager::instance()->getVisionTargetManager()->getTargetActivityMap().copyTo(display_image);
//}

void MultiTargetFaceTracker::setTrackingMethod(const FaceTrackingMethods tm)
{
	//FaceModuleManager::instance()->getFaceTargetManager()->setTrackingMethod(hm);
	FaceBaseTracker::setTrackingMethod(tm,FaceModuleManager::instance()->getSettings());
}


void MultiTargetFaceTracker::sendHeartBeat(void)
{
	// send iot heartbeat
	if (mpIOTCommunicator)
	{
		double curTime = gCurrentTime();

#ifdef DEBUG_MULTITHREAD
			std::cout << "Test a condition for a heartbeat: curTime-mPreviousHeartbeatTime=" << curTime-mPreviousHeartbeatTime << std::endl;
#endif
		if (curTime - mPreviousHeartbeatTime > mHeartbeatInterval)
		{
			mpIOTCommunicator->sendHeartbeat((unsigned int)(mHeartbeatInterval*mMaxHeartbeatIntervalProportion));
			mPreviousHeartbeatTime = curTime;
#ifdef DEBUG_MULTITHREAD
			std::cout << "Sent a heartbeat.." << std::endl;
#endif

		}
	}
}

} // of namespace vml

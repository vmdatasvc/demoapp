/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * FaceTargetManager.cpp
 *
 *  Created on: Aug 8, 2016
 *      Author: dkim
 */

#include <opencv2/video/tracking.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "modules/ml/FaceModuleManager.hpp"
#include "modules/ml/FaceDetector.hpp"
#include "modules/ml/FaceTargetManager.hpp"
#include "modules/ml/FaceTracker.hpp"

#include "dlib/opencv.h"

// for uuid generation
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/lexical_cast.hpp>

#include <modules/ml/jsoncpp/jsoncpp.hpp>
#include <fstream>
#include <sstream>

#include <modules/utility/Serialize.hpp>

// for debugging
#include <modules/utility/Util.hpp>

//#define DEBUG

namespace vml {

FaceTargetManager::FaceTargetManager():
				mLastID(0),
				mnActiveTargets(0),
				//mVerifyTarget(true),
				//mInactiveTargetInterval(2.f),
				//mInactiveMapWindowRadius(0.1f),
				//mNewTargetDistanceThreshold(10.f),
				mnInactiveTargets(0),
				mStoreFaceNumber(1),
				mStoreTarget(false),
				mStoreImageRatio(1.5f),
				mDoDMGRecognition(false),
				mDoDMGRecognitionEnd(false)
{

}

void FaceTargetManager::initialize(boost::shared_ptr<FaceVideoModule> video,
		boost::shared_ptr<FaceDetector> target_detector,
		boost::shared_ptr<aitvml::Recognition>  target_recognizer,
		boost::shared_ptr<Settings> settings)
{
	mpVideoBuffer = video;
	mpFaceDetector=target_detector;
	mpDMGRecognizer=target_recognizer;

	mpSettings=settings;

	mBackProjImage = cv::Mat::zeros(video->getRows(),video->getCols(), CV_8UC1);
	mTargetActivityMap = cv::Mat_<unsigned char>::zeros(video->getRows(), video->getCols());

	mInactiveTargetMap = cv::Mat_<int>((int)video->getRows(),(int)video->getCols());
	mInactiveTargetMap = -1; // initialize with -1

	mTrackingMethod = FaceBaseTracker::readTrackingMethod(settings);

	//mInactiveTargetInterval = settings->getDouble("FaceTargetManager/inactiveTargetInterval",2.f,false);
	//mInactiveMapWindowRadius = video->getDiagonal()*settings->getFloat("FaceTargetManager/inactiveMapWindowRadiusProportion",0.1f,false);
	//mVerifyTarget = settings->getBool("FaceTargetManager/verifyTarget [bool]");
	//mNewTargetDistanceThreshold = settings->getFloat("FaceTargetManager/newTargetDistanceThreshold",10.f,false);

	// For saving FaceData
	mStoreTarget = settings->getBool("FaceTargetManager/storeTarget [bool]", false, false);
	mStoreTargetPath = settings->getString("FaceTargetManager/storeTargetPath", "", false);
	mStoreFaceNumber = settings->getInt("IoT/numberStoreFaces", 1, false);
	mStoreImageRatio = settings->getFloat("FaceTargetManager/storeImageRatio", 1.5f, false);
	mDoDMGRecognition = settings->getBool("DMGRecognition/doDMGRecognition [bool]", false, false);
	mDoDMGRecognitionEnd = settings->getBool("DMGRecognition/doDMGRecognitionEnd [bool]", false, false);

	mBackProjImage = cv::Mat::zeros(video->getRows(),video->getCols(), CV_8UC1);
	// initialize the recognizer
    //mpDMGRecognizer.reset(new aitvml::Recognition(settings));
    //mpDMGRecognizer->initFromFace(); //mRecognizer->initFromFace();
}

// use this function
void FaceTargetManager::DMGRecognitionFromBox(boost::shared_ptr<FaceTarget> at_ptr, const boost::shared_ptr<FaceFrame> current_frame, cv::Rect box)
{
	cv::Mat faceChip, faceChipNorm;

	// crop the face
	cv::Mat cropped_ref = current_frame->image(box);
	//cv::Mat cropped_image;
	cropped_ref.copyTo(faceChip);

	// scale normalization  (the normalized size is fixed here... )
	// why is the input image RGB mode?
	cv::cvtColor(faceChip, faceChip, CV_RGB2BGR);
	cv::resize(faceChip, faceChipNorm,cv::Size(30,30));

	// Demographics recognition using the facechip
	mpDMGRecognizer->cvtMatToImage32(faceChipNorm, &mpDMGRecognizer->faceImage);
	mpDMGRecognizer->processFromFace();

	// Make the DMGData structure
	DMGData dmg;

	dmg.genderScore = mpDMGRecognizer->mEstGenderScore;
	dmg.ageScore = mpDMGRecognizer->mEstAgeScore;
	dmg.ethnicityScore[0] = mpDMGRecognizer->mEstAfrScore;
	dmg.ethnicityScore[1] = mpDMGRecognizer->mEstCauScore;
	dmg.ethnicityScore[2] = mpDMGRecognizer->mEstHisScore;
	dmg.ethnicityScore[3] = mpDMGRecognizer->mEstOriScore;

	dmg.genderLabel = mpDMGRecognizer->mEstGenderLabel;
	dmg.ageLabel = mpDMGRecognizer->mEstAgeLabel;
	dmg.ethnicityLabel = mpDMGRecognizer->mEstEthnicityLabel;

	dmg.genderString = mpDMGRecognizer->mEstGenderString;
	dmg.ageString = mpDMGRecognizer->mEstAgeString;
	dmg.ethnicityString = mpDMGRecognizer->mEstEthnicityString;

	cv::Point center;
	center.x =(int)(box.x + (float)box.width /2.f); //boost::lexical_cast<int>((float)sfvec[j].box.x + (float)sfvec[j].box.width /2.f);
	center.y = (int)(box.y + (float)box.height /2.f); //boost::lexical_cast<int>((float)sfvec[j].box.y + (float)sfvec[j].box.height /2.f);

	cv::Point fsize;
	fsize.x = box.width;
	fsize.y = box.height;

	int nx,ny,nwidth, nheight;

	nwidth = (int)(box.width*mStoreImageRatio);// boost::lexical_cast<int>((float)sfvec[j].box.width*ImgR);
	nheight = (int)(box.height*mStoreImageRatio); // boost::lexical_cast<int>((float)sfvec[j].box.height*ImgR);

	// For using Cloud API, the size of an image including a face must be over 80 pixels for its width and height.
	if(nwidth < 80)	{
		nwidth = 80;
		mStoreImageRatio = 80.f /(float)box.width;   // here, a face image is rectangle so that we can calculate the ratio only with a width.
	}
	if(nheight < 80) nheight = 80;

	nx = (int)(center.x-(float)nwidth/2.f); //boost::lexical_cast<int>((float)center.x-(float)nwidth/2.f);
	ny = (int)(center.y-(float)nheight/2.f); //boost::lexical_cast<int>((float)center.y-(float)nheight/2.f);

	// Boarder padding
	int addx=0,addy=0,addw=0,addh=0;
	if(nx <0)
	{
		addx = -1*nx;
		nx=0;
	}
	if(ny <0)
	{
		addy = -1*ny;
		ny=0;
	}
	if(nx+nwidth >= current_frame->image.cols)
	{
		addw =  nx + nwidth - current_frame->image.cols;
		nwidth = current_frame->image.cols -1 - nx;
	}
	if(ny+nheight >= current_frame->image.rows)
	{
		addh = ny + nheight -current_frame->image.rows;
		nheight = current_frame->image.rows -1 -ny;
	}

	cv::Rect nbox = cv::Rect(nx,ny,nwidth,nheight);

	cv::Mat nface = current_frame->image(nbox);
	cv::Mat src, dst;
	nface.copyTo(src);

	cv::copyMakeBorder( src, dst, addy, addh, addx, addw, cv::BORDER_REPLICATE, cv::Scalar(0,0,0) );
	cv::cvtColor(dst, dst, CV_RGB2BGR);

	at_ptr->mFaceData.addSingleFaceDataDMG(dst, at_ptr->mpTracker->getPosition(),fsize,current_frame->time, dmg);

	// Fusion on the real-time (update the labels and scores)
	DMGData dmgFusion;
	std::vector<SingleFaceDataType> dmgvec;
	dmgvec = at_ptr->mFaceData.getFaceData();
	at_ptr->DMGDecisionFusion(dmgvec, dmgFusion);
	at_ptr->mFaceData.mGenderScoreFusion = dmgFusion.genderScore;
	at_ptr->mFaceData.mAgeScoreFusion = dmgFusion.ageScore;
	at_ptr->mFaceData.mEthnicityScoreFusion[0] = dmgFusion.ethnicityScore[0];
	at_ptr->mFaceData.mEthnicityScoreFusion[1] = dmgFusion.ethnicityScore[1];
	at_ptr->mFaceData.mEthnicityScoreFusion[2] = dmgFusion.ethnicityScore[2];
	at_ptr->mFaceData.mEthnicityScoreFusion[3] = dmgFusion.ethnicityScore[3];
	at_ptr->mFaceData.mGenderLabelFusion = dmgFusion.genderLabel;
	at_ptr->mFaceData.mAgeLabelFusion = dmgFusion.ageLabel;
	at_ptr->mFaceData.mEthnicityLabelFusion = dmgFusion.ethnicityLabel;
	at_ptr->mFaceData.mGenderStringFusion = dmgFusion.genderString;
	at_ptr->mFaceData.mAgeStringFusion = dmgFusion.ageString;
	at_ptr->mFaceData.mEthnicityStringFusion = dmgFusion.ethnicityString;

}

// Add new target data as FaceTarget
void FaceTargetManager::addFaceDataFromBox(boost::shared_ptr<FaceTarget> at_ptr, const boost::shared_ptr<FaceFrame> current_frame, cv::Rect box)
{
	cv::Mat faceChip, faceChipNorm;

	// crop the face
	cv::Mat cropped_ref = current_frame->image(box);
	//cv::Mat cropped_image;
	cropped_ref.copyTo(faceChip);

	// scale normalization  (the normalized size is fixed here... )
	// why is the input image RGB mode?
	cv::cvtColor(faceChip, faceChip, CV_RGB2BGR);
	cv::resize(faceChip, faceChipNorm,cv::Size(30,30));

	cv::Point center;
	center.x =(int)(box.x + (float)box.width /2.f); //boost::lexical_cast<int>((float)sfvec[j].box.x + (float)sfvec[j].box.width /2.f);
	center.y = (int)(box.y + (float)box.height /2.f); //boost::lexical_cast<int>((float)sfvec[j].box.y + (float)sfvec[j].box.height /2.f);

	cv::Point fsize;
	fsize.x = box.width;
	fsize.y = box.height;

	int nx,ny,nwidth, nheight;

	nwidth = (int)(box.width*mStoreImageRatio);// boost::lexical_cast<int>((float)sfvec[j].box.width*ImgR);
	nheight = (int)(box.height*mStoreImageRatio); // boost::lexical_cast<int>((float)sfvec[j].box.height*ImgR);

	// For using Cloud API, the size of an image including a face must be over 80 pixels for its width and height.
	if(nwidth < 80)	{
		nwidth = 80;
		mStoreImageRatio = 80.f /(float)box.width;   // here, a face image is rectangle so that we can calculate the ratio only with a width.
	}
	if(nheight < 80) nheight = 80;

	nx = (int)(center.x-(float)nwidth/2.f); //boost::lexical_cast<int>((float)center.x-(float)nwidth/2.f);
	ny = (int)(center.y-(float)nheight/2.f); //boost::lexical_cast<int>((float)center.y-(float)nheight/2.f);

	// Boarder padding
	int addx=0,addy=0,addw=0,addh=0;
	if(nx <0)
	{
		addx = -1*nx;
		nx=0;
	}
	if(ny <0)
	{
		addy = -1*ny;
		ny=0;
	}
	if(nx+nwidth >= current_frame->image.cols)
	{
		addw =  nx + nwidth - current_frame->image.cols;
		nwidth = current_frame->image.cols -1 - nx;
	}
	if(ny+nheight >= current_frame->image.rows)
	{
		addh = ny + nheight -current_frame->image.rows;
		nheight = current_frame->image.rows -1 -ny;
	}

	cv::Rect nbox = cv::Rect(nx,ny,nwidth,nheight);

	cv::Mat nface = current_frame->image(nbox);
	cv::Mat src, dst;
	nface.copyTo(src);

	cv::copyMakeBorder( src, dst, addy, addh, addx, addw, cv::BORDER_REPLICATE, cv::Scalar(0,0,0) );
	cv::cvtColor(dst, dst, CV_RGB2BGR);

	//at_ptr->mFaceData.addSingleFaceDataDMG(dst, at_ptr->mpTracker->getPosition(),current_frame->time, dmg);
	at_ptr->mFaceData.addSingleFaceData(dst, at_ptr->mpTracker->getPosition(),fsize, current_frame->time);

	// Initialize data when demographics module is not used.
	at_ptr->mFaceData.mGenderScoreFusion = NOT_A_VALID;
	at_ptr->mFaceData.mAgeScoreFusion = NOT_A_VALID;
	at_ptr->mFaceData.mEthnicityScoreFusion[0] = NOT_A_VALID;
	at_ptr->mFaceData.mEthnicityScoreFusion[1] = NOT_A_VALID;
	at_ptr->mFaceData.mEthnicityScoreFusion[2] = NOT_A_VALID;
	at_ptr->mFaceData.mEthnicityScoreFusion[3] = NOT_A_VALID;
	at_ptr->mFaceData.mGenderLabelFusion = NOT_A_VALID;
	at_ptr->mFaceData.mAgeLabelFusion = NOT_A_VALID;
	at_ptr->mFaceData.mEthnicityLabelFusion = NOT_A_VALID;
	at_ptr->mFaceData.mGenderStringFusion = "NN";
	at_ptr->mFaceData.mAgeStringFusion = "NN";
	at_ptr->mFaceData.mEthnicityStringFusion = "NN";
}

void FaceTargetManager::updateTargets(void)
{
	// initialize target activity map
	mTargetActivityMap = 0;

	// get current image
    const boost::shared_ptr<FaceFrame> current_frame = mpVideoBuffer->currentFrame();

//	cv::Mat blob_map;
//	mpFaceDetector->getBlobMap(blob_map);


	mBackProjImage = cv::Scalar(0);


    if (mnActiveTargets>0 || mnInactiveTargets >0)
	{
		std::map<unsigned int,boost::shared_ptr<FaceTarget> >::iterator active_target_itr = mTargetList.begin();
		while (active_target_itr != mTargetList.end())
		{
			if (active_target_itr->second->mActive)
			{
				boost::shared_ptr<FaceTarget> at_ptr = active_target_itr->second;
				TrackingStatusEnum update_status = at_ptr->updateTargetStatus(mBackProjImage);

				switch (update_status)
				{
					case FaceTrackingStatus_Finished:
					{
						mnActiveTargets--;
						// remove current element from mTargetList
						// NOTE: in c++11 map.erase() returns the next available iterator, but the current gcc doesn't support that
						// when upgrading gcc, visit here and update this.

						// Data Storing Process for a completed target
						if(mDoDMGRecognitionEnd)
						{
							active_target_itr->second->completeFaceDataDMG(mpDMGRecognizer, mStoreFaceNumber);
						}
						else
						{
							active_target_itr->second->completeFaceData(mStoreFaceNumber);
						}
						if(mStoreTarget)
						{
							active_target_itr->second->storeFaceData(mStoreTargetPath, mStoreFaceNumber);
						}

						// move the track to the completed list and delete this target.
						if (at_ptr->mTrajectory.nodeCount() > 1)
						{
							mCompletedTargetList.push_back(at_ptr);
						}

						mTargetList.erase(active_target_itr++);
						break;
					}
					case FaceTrackingStatus_Inactive:
					{
						mInactiveTargetMap(at_ptr->mpTracker->getPosition()) = active_target_itr->first;
						// set inactive. Try to re-acquire the target later.
						mnActiveTargets--;
						mnInactiveTargets++;
						active_target_itr++;
						break;
					}
					case FaceTrackingStatus_Active:
					{
						at_ptr->incDetectionInactiveFrameCount();
						at_ptr->updateTargetActivityMap(mTargetActivityMap);
						//DMGRecognition(at_ptr, current_frame);
						if(mDoDMGRecognition)
							DMGRecognitionFromBox(at_ptr, current_frame,at_ptr->mpTracker->getTrackBox());
						else
							addFaceDataFromBox(at_ptr, current_frame,at_ptr->mpTracker->getTrackBox());

						active_target_itr++;
						break;
					}
					default:
					{
						// nothing to do
						active_target_itr++;
					}
				} // of switch (update_status)

			} // of if (active_target_itr->mActive)
			else
			{
				// If a tracker is inactive during some time, then finish it. (Dave)
				boost::shared_ptr<FaceTarget> at_ptr = active_target_itr->second;
				if (at_ptr->mpTracker->getInactiveFrameCount() > at_ptr->mpTracker->getInactiveTimeOutValue() )
				{
					mnInactiveTargets--;
					// Data Storing Process for a completed target
					if(mDoDMGRecognitionEnd)
					{
						at_ptr->completeFaceDataDMG(mpDMGRecognizer, mStoreFaceNumber);
					}
					else
					{
						at_ptr->completeFaceData(mStoreFaceNumber);
					}
					mCompletedTargetList.push_back(at_ptr);

					mTargetList.erase(active_target_itr++);
				}
				else
				{
                    at_ptr->mpTracker->increaseInactiveFrameCount();
					active_target_itr++;
				}
			}
		} // of while (not active_target_itr)
	}

}



void FaceTargetManager::targetTrackerAssignHelper(boost::shared_ptr<FaceTarget> target,
		boost::shared_ptr<FaceDetectorTarget> blob)
{
	switch (mTrackingMethod)
	{
		case FaceTrackerFeature_HIST_HSV:
		{
			target->mpTracker.reset(new SingleFaceTrackerHSHist(mpSettings));
			break;
		} // of case HIST_HSV
		case FaceTrackerFeature_HIST_YCrCb:
		{
			target->mpTracker.reset(new SingleFaceTrackerCrCbHist(mpSettings));
			break;
		} // of case HIST_YCrCb
		case FaceTrackerFeature_HIST_rgCHROM:
		{
			target->mpTracker.reset(new SingleFaceTrackerRGChromHist(mpSettings));
			break;
		} // of case HIST_rgCHROM
		case FaceTrackerFeature_HIST_RGB:
		{
			target->mpTracker.reset(new SingleFaceTrackerRGBHist(mpSettings));
			break;
		} // of case HIST_RGB
		case FaceTrackerFeature_DLIB:
		{
			target->mpTracker.reset(new SingleFaceTrackerDlib(mpSettings));
			break;
		} // of case Correlation_Tracking:
		case FaceTrackerFeature_Hybrid:
		{
			// Hybrid tracking.
			// If mean saturation value is within the specified range, use HSV histogram
			// otherwise, use raw RGB tracking

			// check foreground pixel saturation
			cv::Mat cropped;
			cv::Mat image;
			mpVideoBuffer->currentFrame()->image.copyTo(image);

			cv::Rect cropRect = blob->bounding_box;
			image(cropRect).copyTo(cropped);

			// convert to hsv
			RGBbImage hsv;
			cv::cvtColor(cropped, hsv, CV_RGB2HSV);
			cv::Mat	hsvmask;
			cv::inRange(hsv,cv::Scalar(0,30,10),cv::Scalar(180,245,255),hsvmask);

			// create contour mask
			cv::Mat contourMask;
			// create temp polygon in the crop rect
			std::vector<std::vector<cv::Point> > contours_crop;
			contours_crop = blob->contours;
/*
			for (std::vector<boost::shared_ptr<std::vector<cv::Point> > >::iterator itr=blob->contours.begin();
					itr != blob->contours.end();itr++)
			{
				std::vector<cv::Point> contour_crop;
				std::vector<cv::Point>::iterator pitr;
				//for(pitr=(*itr)->begin();pitr!=(*itr)->end();pitr++)
				for(pitr=itr->begin();pitr!=itr->end();pitr++)
				{
					contour_crop.push_back(*pitr - cropRect.tl());
				}
				contours_crop.push_back(contour_crop);
			}
*/
			// create polygon mask
			contourMask = cv::Mat(cropRect.height,cropRect.width,CV_8UC1,cv::Scalar(0));
			// fill mask
			cv::drawContours(contourMask,contours_crop,-1,cv::Scalar(255),CV_FILLED);

			hsvmask &= contourMask;

			// get mean saturation value
			float sat_mean = 0.f,blob_mass = 0.f;
			uchar *sat_p = (uchar*)hsv.data;
			sat_p++;
			uchar *mask_p = (uchar*)hsvmask.data;
			for (int i=0;i<hsv.rows*hsv.cols;i++)
			{
				if (*mask_p)
				{
					sat_mean += *sat_p;
					blob_mass += 1.f;
				}
				sat_p += 3;
				mask_p++;
			}
			sat_mean /= blob_mass;

			if (sat_mean > mpSettings->getFloat("HybridColorHistogramTracker/saturationLowThreshold",50.f,false))
			{
				// if mean saturation value is within the given range, use HSVhistogram method
				target->mpTracker.reset(new SingleFaceTrackerHSHist(mpSettings));
			}
			else
			{
				// if not, use RGB histogram method.
				target->mpTracker.reset(new SingleFaceTrackerRGBHist(mpSettings));
			}
			break;
		}
		default:
		{
			VML_ERROR(MsgCode::notSupportedErr,"FaceTarget: Tracking method " << mTrackingMethod << " not supported");
		}
	} // of switch (hm)

}

void FaceTargetManager::getDetectedTargetList()
{
	FaceModuleManager::instance()->getDetector()->getDetectedTargetList(detected_target_list);
	//printf("number of detected faces: %d\n", detected_target_list.size());
}

void FaceTargetManager::addNewTargetsFromDetector()
{
	//std::vector<boost::shared_ptr<FaceDetectorTarget> > detected_target_list;
	//FaceModuleManager::instance()->getDetector()->getDetectedTargetList(detected_target_list);

	if (detected_target_list.size() == 0 )
	{
		return;
	}

	// Compare each targets with detected target list

	std::map<unsigned int,boost::shared_ptr<FaceTarget> >::iterator target_itr = mTargetList.begin();
	while (target_itr != mTargetList.end())
	{
		if (target_itr->second->mActive) // for all currently active targets
		{
			// check if there is any nearby detected target (overlapping test)
			// TO-DO: Face verification between the initial face of a tracker and detected targets.
			//        This set the validity of detected targets for the next process.

			std::vector<boost::shared_ptr<FaceDetectorTarget> >::iterator dt_itr = detected_target_list.begin();
			while (dt_itr != detected_target_list.end())
			{
                if ((*dt_itr)->isValid())
				{
                	if(target_itr->second->verifyActiveTarget(*dt_itr))
                	{
                		// (Refinement) if an active target is detected by the detector, update its center for correction.
                    	// update the center
                		target_itr->second->mpTracker->setTrackBox((*dt_itr)->bounding_box);
                		target_itr->second->mpTracker->updateSearchWindowExternal();

                		// update the mDetectionInactiveFrameCount
                		target_itr->second->resetDetectionInactiveFrameCount();
                	}

				}

                dt_itr++;
			}

            target_itr++;
		}
		else
		{
			// for all currently inactive targets
			// try to reactivate this tracker
			TrackingStatusEnum update_status = target_itr->second->reactivateTarget(detected_target_list);

			switch (update_status)
			{
				case FaceTrackingStatus_Active:
				{
					// reactivation successful.
					mnInactiveTargets--;
					mnActiveTargets++;
					target_itr++;

					break;
				}
				case FaceTrackingStatus_Finished:
				{
					// inactive target timed out
					// move the track to the completed list andreactivate_status delete this target.
					if (target_itr->second->mTrajectory.nodeCount() > 1)
					{
						mCompletedTargetList.push_back(target_itr->second);
					}

					mnInactiveTargets--;
					// remove current element from mTargetMap
					// NOTE: in c++11 map.erase() returns the next availabel iterator, but the current gcc doesn't support that
					// when upgrading gcc, visit here and update this.

					// Data Storing Process for a completed target
					if(mDoDMGRecognitionEnd)
					{
						target_itr->second->completeFaceDataDMG(mpDMGRecognizer, mStoreFaceNumber);
					}
					else
					{
						target_itr->second->completeFaceData(mStoreFaceNumber);
					}
					if(mStoreTarget)
					{
						target_itr->second->storeFaceData(mStoreTargetPath, mStoreFaceNumber);
					}

					mTargetList.erase(target_itr++);
					break;
				}
                case FaceTrackingStatus_Inactive:
				default:
				{
					// keep inactive. do nothing
					target_itr++;
				}
			}
		}

	}

	// add all the remaining targets
	std::vector<boost::shared_ptr<FaceDetectorTarget> >::iterator dt_itr = detected_target_list.begin();
	while (dt_itr != detected_target_list.end())
	{
		// check if the detected target is valid and inside track region (dave)
		if (((*dt_itr)->isValid()) && ((*dt_itr)->isInTrackRegion()))  // ((*dt_itr)->isInStartRegion()))
		{
			//double start_time = gCurrentTime();

            // create target instancedt_itr
			boost::shared_ptr<FaceTarget> tptr;
			tptr.reset(new FaceTarget(mLastID));

			//double reset_time = gCurrentTime();

			tptr->initialize((*dt_itr));

			//double init_time = gCurrentTime();

			mTargetList.insert(std::pair<unsigned int, boost::shared_ptr<FaceTarget> >(mLastID,tptr));
			mLastID++;
			mnActiveTargets++;

			//double insert_time = gCurrentTime();

			// comment for new pipeline (demographics are working at post process in cloud or other processor
			if(mDoDMGRecognition)
				DMGRecognitionFromBox(tptr, mpVideoBuffer->currentFrame(),(*dt_itr)->bounding_box);
			else
				addFaceDataFromBox(tptr, mpVideoBuffer->currentFrame(),(*dt_itr)->bounding_box);

			//double add_time = gCurrentTime();

			//printf("TIME: addNewTargetsFromDetector (for reset): %f\n",reset_time - start_time);
			//printf("TIME: addNewTargetsFromDetector (for init): %f\n",init_time - reset_time);
			//printf("TIME: addNewTargetsFromDetector (for insert): %f\n",insert_time - init_time);
			//printf("TIME: addNewTargetsFromDetector (for add): %f\n",add_time - insert_time);
		}
		dt_itr++;
	}

}

void FaceTargetManager::verifyActiveTargetFromDetectedTarget(boost::shared_ptr<FaceTarget> activeTarget, boost::shared_ptr<FaceDetectorTarget> dt)
{
	cv::Mat blob_map;
	mpFaceDetector->getBlobMap(blob_map);
	//blob_map &= mTargetActivityMap;

    cv::Mat cropped_map;
	blob_map(activeTarget->mpTracker->getTrackBox()).copyTo(cropped_map);
    cv::Scalar area = cv::sum(cropped_map);
    if ((float)area[0]/(float)(activeTarget->mpTracker->getTrackBox().area()) > 0.3f) // if overlapped pixels are larger than 80% of the blob area
    {
    	dt->setInvalid();
    }
}

void FaceTargetManager::drawActiveTargets(cv::Mat &image) const
{
	for (std::map<unsigned int,boost::shared_ptr<FaceTarget> >::const_iterator titr = mTargetList.begin();titr != mTargetList.end();titr++)
	{
		if (titr->second->mActive)
		{
			titr->second->drawTarget(image);
		}
	}

}

void FaceTargetManager::drawActiveTargetsWithDMG(cv::Mat &image) const
{
	for (std::map<unsigned int,boost::shared_ptr<FaceTarget> >::const_iterator titr = mTargetList.begin();titr != mTargetList.end();titr++)
	{
		if (titr->second->mActive)
		{
			titr->second->drawTarget(image);

			// add on the demographics labels
			cv::Rect cropBox = titr->second->mpTracker->getCropRect();
			//cv::Rect trackBox = titr->second->mpTracker->getTrackBox();

			// draw the demographics labels
			//std::ostringstream conf_str;
			//cv::Point pos(trackBox.x + trackBox.width, trackBox.y);
			//conf_str << titr->second->mCFaceData.mGenderLabelFusion << "-" << titr->second->mCFaceData.mAgeLabelFusion << "-" << titr->second->mCFaceData.mEthnicityLabelFusion;
			//cv::putText(image, conf_str.str(), pos, CV_FONT_HERSHEY_PLAIN, 0.9f, cv::Scalar(200,0,0));

			std::ostringstream gender_str, age_str, eth_str;
			cv::Point gender_pos(cropBox.x + 5 , cropBox.y - 5);
			gender_str << titr->second->mFaceData.mGenderStringFusion;
			cv::putText(image, gender_str.str(), gender_pos, 4, 0.8f, cv::Scalar(255,0,0));

			cv::Point age_pos(cropBox.x + 5, cropBox.y + 15);
			age_str << titr->second->mFaceData.mAgeStringFusion;
			cv::putText(image, age_str.str(), age_pos, 4, 0.8f, cv::Scalar(0,255,255));

			cv::Point eth_pos(cropBox.x + 5 , cropBox.y + cropBox.height - 5);
			eth_str << titr->second->mFaceData.mEthnicityStringFusion;
			cv::putText(image, eth_str.str(), eth_pos, 4, 0.8f, cv::Scalar(255,200,255));


		}
	}

}


void FaceTargetManager::drawActiveTrajectories(cv::Mat &image) const
{
	for (std::map<unsigned int,boost::shared_ptr<FaceTarget> >::const_iterator titr = mTargetList.begin();titr != mTargetList.end();titr++)
	{
		if (titr->second->mActive)
		{
			titr->second->drawTrajectory(image);
		}
	}

}


void FaceTargetManager::retrieveCompletedTrajectories(std::vector<FaceTrajectory>& trajlist)
{
	// TODO: lock mCompletedTargetList
	std::vector<boost::shared_ptr<FaceTarget> >::iterator titr;
	for(titr=mCompletedTargetList.begin();titr!=mCompletedTargetList.end();titr++)
	{
		trajlist.push_back((*titr)->mTrajectory);
		titr->reset();
	}
	// clear completed target list
	mCompletedTargetList.clear();
}

void FaceTargetManager::retrieveCompletedFaceData(std::vector<TargetFaceData>& fdlist)
{
	// TODO: lock mCompletedTargetList
	std::vector<boost::shared_ptr<FaceTarget> >::iterator titr;
	for(titr=mCompletedTargetList.begin();titr!=mCompletedTargetList.end();titr++)
	{
		fdlist.push_back((*titr)->mFaceData);
		titr->reset();
	}
	// clear completed target list
	mCompletedTargetList.clear();
}

unsigned int FaceTargetManager::getNumTotalTargets(void)
{
	return mTargetList.size();
}

unsigned int FaceTargetManager::getNumInactiveTargets()
{
	unsigned int numInactiveTargets = 0;
	std::map<unsigned int,boost::shared_ptr<FaceTarget> >::iterator inactive_target_itr = mTargetList.begin();
	while (inactive_target_itr != mTargetList.end())
	{
		if (not inactive_target_itr->second->mActive) // for all currently inactive targets
		{
			numInactiveTargets++;
		}
		inactive_target_itr++;
	}

	return numInactiveTargets;
}

} // of namespace vml

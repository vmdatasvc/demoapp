/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * FaceDetectorTarget.cpp
 *
 *  Created on: Aug 8, 2016
 *      Author: dkim
 */

#include "modules/ml/FaceDetectorTarget.hpp"
#include <modules/ml/FaceVideoModule.hpp>
#include <modules/ml/FaceModuleManager.hpp>

namespace vml {

FaceDetectorTarget::FaceDetectorTarget(cv::Rect fd):
		contours_area(0),
		contour1_area(0),
		contour2_area(0),
		valid(false),
		mValid(true),
		mInTrackRegion(false),
		mInStartRegion(false)
{
	mpVideoModule = FaceModuleManager::instance()->getVideoModule();


	bounding_box = fd;

	contour1.resize(4);
	contour1[0].x =  bounding_box.x;
	contour1[0].y =  bounding_box.y;
	contour1[1].x =  bounding_box.x + bounding_box.width;
	contour1[1].y =  bounding_box.y;
	contour1[2].x =  bounding_box.x + bounding_box.width;
	contour1[2].y =  bounding_box.y + bounding_box.height;
	contour1[3].x =  bounding_box.x;
	contour1[3].y =  bounding_box.y + bounding_box.height;

	contours.push_back(contour1);
	contour1_area = bounding_box.area();

	mCenter = (bounding_box.tl() + bounding_box.br() ) / 2;

	evalRegionStatus();

}

FaceDetectorTarget::~FaceDetectorTarget()
{
}

float FaceDetectorTarget::EuclideanDistance(FaceDetectorTarget &ot) const
{
	return(EuclideanDistance(ot.mCenter));
}

float FaceDetectorTarget::EuclideanDistance(cv::Point pos) const
{
	int dx = this->mCenter.x - pos.x;
	int dy = this->mCenter.y - pos.y;
	return(sqrt((float)(dx*dx+dy*dy)));
}

void FaceDetectorTarget::evalRegionStatus()
{
	// check if position is inside track region
	//mInTrackRegion = mpVideoModule->insideTrackRegion(mCenter);
	mInTrackRegion = mpVideoModule->insideTrackRegionBox(bounding_box);

	if (mInTrackRegion)
	{
		// enforcing track and start region disjoint
		mInStartRegion = false;
	}
	else
	{
		mInStartRegion = mpVideoModule->insideStartKillRegion(mCenter);
		if (mInStartRegion)
		{
			if (mpVideoModule->insideNoStartRegion(mCenter))
			{
				mInStartRegion = false;
			}
		}
	}

}


} // of namespace vml

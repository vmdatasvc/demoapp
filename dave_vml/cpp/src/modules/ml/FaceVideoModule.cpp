/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * FaceVideoModule.cpp
 *
 *  Created on: Mar 31, 2016
 *  Modified on: Dec 19, 2016
 *      Author: dkim
 */

#include <modules/ml/FaceVideoModule.hpp>
#include <modules/utility/Util.hpp>
#include <boost/lexical_cast.hpp>

#define USE_TRACK_POLYGON

namespace vml {

FaceFrame::FaceFrame(int rows,int cols):
	image(cv::Mat(rows,cols,CV_8UC3)), //RGBbImage(rows,cols)),
	undistorted_image(cv::Mat(rows,cols,CV_8UC3)), //RGBbImage(rows,cols)),
	time(-1.f)
{}

bool FaceFrame::grab(cv::VideoCapture &vc, double epoch_time)
{
	if (not vc.read(image))
	{
		return false;
	}

	time = gCurrentTime();
	//time = epoch_time + vc.get(CV_CAP_PROP_POS_MSEC)*1e-3;

	// cv::VideoCapture captures image in BGR moe. convert to RGB
	cv::cvtColor(image,image,CV_BGR2RGB);

	return true;
}

void FaceFrame::copyTo(FaceFrame &ot)
{
	image.copyTo(ot.image);
	undistorted_image.copyTo(ot.undistorted_image);
	//blurredImage.copyTo(ot.blurredImage);
	//foreground_image.copyTo(ot.foreground_image);
	ot.time = time;
}

/*
FaceVideoModule::FaceVideoModule(boost::shared_ptr<SETTINGS> settings):
		VideoBuffer<FaceFrame>()
{
	mpSettings = settings;
}
*/

FaceVideoModule::FaceVideoModule():
		VideoBuffer<FaceFrame>()
		//mStartRegionOffset(-1)
{
}


FaceVideoModule::~FaceVideoModule()
{
	if (mpCameraModel)
	{
		mpCameraModel.reset();
	}
}

void FaceVideoModule::initialize(boost::shared_ptr<SETTINGS> settings, std::string video_file_name)
{
	mpSettings = settings;

	// call superclass initialize
	VideoBuffer<FaceFrame>::initialize(mpSettings,video_file_name);

	//mStartRegionOffset = mpSettings->getFloat("FaceTracker/startRegionOffset",1.5f,false);

	 std::vector<int> temp_polygon;

#ifdef USE_TRACK_POLYGON
	 temp_polygon = settings->getIntVector("FaceTracker/TrackPolygon","",false);

	 int img_res_width = settings->getInt("FaceTracker/inputImageWidth",1280, false);
	 int img_res_hieght = settings->getInt("FaceTracker/inputImageHeight",960, false);
	 // the polygon points are obtained on 1280 x 960 video from Vision Team

	 float xratio =  (float)mCols / (float)img_res_width;
	 float yratio =  (float)mRows / (float)img_res_hieght;


#else
	std::vector<double> temp_roi;
	 temp_roi = settings->getDoubleVector("FaceTracker/TrackROI","",false);

	 int x,y,w,h;

	 x = boost::lexical_cast<int>(mCols* temp_roi[0]);
	 y = boost::lexical_cast<int>(mRows* temp_roi[1]);
	 w = boost::lexical_cast<int>(mCols* temp_roi[2]);
	 h = boost::lexical_cast<int>(mRows* temp_roi[3]);

	//printf("x=%d, y=%d, w=%d, h=%d", x,y,w,h);

	 temp_polygon.push_back(x);
	 temp_polygon.push_back(y);
	 temp_polygon.push_back(x+w);
	 temp_polygon.push_back(y);
	 temp_polygon.push_back(x+w);
	 temp_polygon.push_back(y+h);
	 temp_polygon.push_back(x);
	 temp_polygon.push_back(y+h);

#endif

	 VML_ASSERT(temp_polygon.size()!=0);
     VML_ASSERT(temp_polygon.size()%2==0);	// number of coords should be even

     std::vector<int>::iterator titr;
     for (titr=temp_polygon.begin();titr!=temp_polygon.end();)
     {
             cv::Point apoint;

             apoint.x = (int)((float)(*titr++) * xratio);
             apoint.y = (int)((float)(*titr++) * yratio);
             mTrackingRegion.push_back(apoint);
     }

     // read no start region polygons.
     // check if no start zone is provided
     std::string nszstr = settings->getString("NoStartZone/numPolygons","",false);

     if (!nszstr.empty())
      {

          int npolygon = settings->getInt("NoStartZone/numPolygons");
          mNoStartRegion.resize(npolygon);
          for (int i=0;i<npolygon;i++)
          {
                  temp_polygon.clear();
                  std::stringstream varname;
                  varname << "NoStartZone/P" << i;
                  temp_polygon = settings->getIntVector(varname.str().c_str(),"",false);
                  VML_ASSERT(temp_polygon.size()!=0);
                  VML_ASSERT(temp_polygon.size()%2==0);	// number of coords should be even
                  std::vector<int>::iterator titr2;
                  for (titr2=temp_polygon.begin();titr2!=temp_polygon.end();)
                  {
                          cv::Point apoint;
                          apoint.x = *titr2++;
                          apoint.y = *titr2++;
                          mNoStartRegion[i].push_back(apoint);
                  }
          }
      }
     mWindowRect = cv::Rect(cv::Rect(0,0,mCols,mRows));

    // create camera model
   	//mpCameraModel.reset(new CameraModelOpenCV(mRows,mCols,mpSettings));


 	// define start/kill regions as one human length from the track polygon
 	// Set the start/kill region as the tracking region in my case
    mStartKillRegion.resize(mTrackingRegion.size());
 	for (unsigned int i = 0; i<mTrackingRegion.size(); i++)
 	{
 		mStartKillRegion[i] = mTrackingRegion[i];
 	}


}

bool FaceVideoModule::grabFrame(void)
{
	bool grab_success = VideoBuffer<FaceFrame>::grabFrame();

	if (!grab_success)
	{
		return grab_success;
	}

	boost::shared_ptr<FaceFrame> cur_frame = this->currentFrame();

	// undistort current image
	if (mpCameraModel)
	{
		mpCameraModel->undistortImage(cur_frame->image,cur_frame->undistorted_image);
	}
	else
	{
		VML_WARN(MsgCode::paramErr,"Camera model was not enabled for this video buffer. Check it for undistorted");
		cur_frame->image.copyTo(cur_frame->undistorted_image);	// copy original image
	}

	return true;
}


void FaceVideoModule::allocateTempFrames(int numThread)
{
	if(numThread >=2)
	{
		mpGrabFrame.reset(new FaceFrame(mRows,mCols));
		if(numThread >2)
		{
			mpDetectorFrame.reset(new FaceFrame(mRows,mCols));
			mpUpdateFrame.reset(new FaceFrame(mRows,mCols));
		}
	}

	// push one frame to buffer
	boost::shared_ptr<FaceFrame> tFramePtr;
	tFramePtr.reset(new FaceFrame(mRows,mCols));
	// push new frame to the buffer
	mFrameBuffer.push_front(tFramePtr);
	mCurrentFrameNumber++;
}

bool FaceVideoModule::grabInGrabFrame(void)
{
	if (!VideoBuffer<FaceFrame>::grabInFrame(mpGrabFrame))
	{
		return false;
	}

	return true;
}

bool FaceVideoModule::grabToGrabFrame(void)
{
	mpGrabFrame = VideoBuffer<FaceFrame>::grabAndReturnFrame();

	if (!mpGrabFrame)
	{
		return false;
	}

	// image processing here


	return true;
}

void FaceVideoModule::setGrabFrameToCurrent(void)
{
	// insert the frame into the circular buffer
	this->setCurrentFrame(mpGrabFrame);
	mpGrabFrame.reset();
}
void FaceVideoModule::setGrabFrameToDetectorFrame(void)
{
	mpDetectorFrame = mpGrabFrame;
}

void FaceVideoModule::setDetectorFrameToCurrent(void)
{
	// insert the frame into the circular buffer
	this->setCurrentFrame(mpDetectorFrame);
}

void FaceVideoModule::copyGrabFrameToDetectorFrame(void)
{
	mpGrabFrame->copyTo(*mpDetectorFrame);
}

void FaceVideoModule::copyDetectorFrameToCurrent(void)
{
	mpDetectorFrame->copyTo(*currentFrame());
}


void FaceVideoModule::copyGrabFrameToCurrent(void)
{
	mpGrabFrame->copyTo(*currentFrame());
}

void FaceVideoModule::copyCurrentToGrabFrame(void)
{
	this->currentFrame()->copyTo(*mpGrabFrame);
}

void FaceVideoModule::copyCurrentToDetectorFrame(void)
{
	this->currentFrame()->copyTo(*mpDetectorFrame);
}


void FaceVideoModule::getCurrentUndistortedImage(cv::Mat &undist_img)
{
	this->currentFrame()->undistorted_image.copyTo(undist_img);
}

void FaceVideoModule::undistortImage(cv::Mat &input_img,cv::Mat &undistorted_image)
{
	if (mpCameraModel)
	{
		mpCameraModel->undistortImage(input_img,undistorted_image);
	}
	else
	{
		VML_WARN(MsgCode::paramErr,"Camera model was not enabled for this video buffer. Check it for undistorted");
		input_img.copyTo(undistorted_image);
	}
}

bool FaceVideoModule::insideTrackRegionBox(cv::Rect box)
{

	bool mbP1 = insideTrackRegion(box.tl());
	bool mbP2 = insideTrackRegion(box.br());


	if(mbP1 == true && mbP2 == true)
	{
			return true;
	}
	else
	{
			return false;
	}
}


bool FaceVideoModule::insideTrackRegion(cv::Point pos)
{
	cv::Point2f posf(pos);

	if (cv::pointPolygonTest(mTrackingRegion,posf,false) > 0.f)
	{
			return true;
	}
	else
	{
			return false;
	}
}

bool FaceVideoModule::insideStartKillRegion(cv::Point pos)
{
#if 0
	bool inside = false;
	cv::Point2f	posf(pos);

	std::vector<std::vector<cv::Point> >::iterator poly_itr = mStartKillRegion.begin();
	for (;poly_itr != mStartKillRegion.end();poly_itr++)
	{
			if (cv::pointPolygonTest(*poly_itr,posf,false) > 0.f)
			{
					inside = true;
			}
	}

	return inside;
#endif
	cv::Point2f posf(pos);
	if (cv::pointPolygonTest(mStartKillRegion,posf,false) > 0.f)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool FaceVideoModule::insideNoStartRegion(cv::Point pos)
{
	cv::Point2f posf(pos);
	bool insideNoStart = false;
	for (unsigned int i=0;i<mNoStartRegion.size();i++)
	{
		if (cv::pointPolygonTest(mNoStartRegion[i],posf,false) > 0.f)
		{
			insideNoStart = true;
		}
	}

	return insideNoStart;
}

void FaceVideoModule::drawTrackRegions(cv::Mat &img)
{
	// draw Track Region
	cv::drawContours(img,std::vector<std::vector<cv::Point> >(1,mTrackingRegion),0,CV_RGB(255,0,0));
	// draw Start regions
//	cv::drawContours(img,mStartKillRegion,-1,CV_RGB(255,0,0));
	cv::drawContours(img,std::vector<std::vector<cv::Point> >(1,mStartKillRegion),0,CV_RGB(0,0,255));
}

void FaceVideoModule::enforceWindowRect(std::vector<cv::Point> &pts)
{
	for (unsigned int i=0;i<pts.size();i++)
	{
		if (!mWindowRect.contains(pts[i]))
		{
			pts[i] = enforcePointInRect(pts[i],mWindowRect);
		}
	}
}

void FaceVideoModule::enforceWindowRect(cv::Point &pt)
{
	if (!mWindowRect.contains(pt))
	{
		pt = enforcePointInRect(pt,mWindowRect);
	}
}

cv::Point FaceVideoModule::enforceWindowRect(const cv::Point pt)
{
	if (!mWindowRect.contains(pt))
	{
		return enforcePointInRect(pt,mWindowRect);
	}
	else
	{
		return pt;
	}

}

float FaceVideoModule::getFrameRate()
{
	return (float)mCurrentFrameNumber/(float)(currentFrame()->time - mEpochTime);
}


} // of namespace vml

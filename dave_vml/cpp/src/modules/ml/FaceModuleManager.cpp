/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * FaceModuleManager.cpp
 *
 *  Created on: Aug 2, 2016
 *      Author: dkim
 */

#include <boost/shared_ptr.hpp>
#include <modules/ml/FaceModuleManager.hpp>
#include <modules/ml/FaceDetector.hpp>
#include <modules/ml/FaceTarget.hpp>
#include <modules/ml/FaceVideoModule.hpp>

namespace vml {

// global faceModuleManager instance
static FaceModuleManager gInstance;

FaceModuleManager* FaceModuleManager::instance()
{
	return &gInstance;
}

void FaceModuleManager::registerSettings(boost::shared_ptr<SETTINGS> settings)
{
	if (mpSettings)
	{
		mpSettings.reset();
	}

	mpSettings = settings;
}

boost::shared_ptr<SETTINGS> FaceModuleManager::getSettings(void)
{
	return mpSettings;
}

void FaceModuleManager::registerVideoModule(boost::shared_ptr<FaceVideoModule> vb)
{
	if (mpVideoBuffer)
	{
		mpVideoBuffer.reset();
	}

	mpVideoBuffer = vb;
}

boost::shared_ptr<FaceVideoModule> FaceModuleManager::getVideoModule(void)
{
	return mpVideoBuffer;
}

void FaceModuleManager::registerDetector(boost::shared_ptr<FaceDetector> det)
{
	if (mpDetector)
	{
		mpDetector.reset();
	}

	mpDetector = det;
}

boost::shared_ptr<FaceDetector> FaceModuleManager::getDetector(void)
{
	return mpDetector;
}

void FaceModuleManager::registerFaceTargetManager(boost::shared_ptr<FaceTargetManager> tm)
{
	if (mpFaceTargetManager)
	{
		mpFaceTargetManager.reset();
	}

	mpFaceTargetManager = tm;
}

boost::shared_ptr<FaceTargetManager> FaceModuleManager::getFaceTargetManager(void)
{
	return mpFaceTargetManager;
}

void FaceModuleManager::registerDMGRecognizer(boost::shared_ptr<aitvml::Recognition> drg)
{
	if (mpDMGRecognizer)
	{
		mpDMGRecognizer.reset();
	}

	mpDMGRecognizer = drg;
}

boost::shared_ptr<aitvml::Recognition> FaceModuleManager::getDMGRecognizer(void)
{
	return mpDMGRecognizer;
}

} // of namespace vml

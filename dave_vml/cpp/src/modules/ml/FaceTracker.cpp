/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * FaceTracker.cpp
 *
 *  Created on: Jan 26, 2016
 *      Author: dkim
 */

#include <modules/utility/Util.hpp>
#include <dlib/opencv.h>
#include <modules/ml/FaceTracker.hpp>
#include <modules/ml/FaceVideoModule.hpp>


//#define DEBUG

// Visual tracking codes here
namespace vml {

SingleFaceTrackerHSHist::SingleFaceTrackerHSHist(boost::shared_ptr<Settings> settings):
		FaceBaseTracker(settings),
		mcHBins(settings->getInt("TrackerHSHist/hBins",30,false)),
		mcSBins(settings->getInt("TrackerHSHist/sBins",32,false)),
		mcHRangeMin(settings->getFloat("TrackerHSHist/hRangeMin",0.f,false)),
		mcHRangeMax(settings->getFloat("TrackerHSHist/hRangeMax",180.f,false)),
		mcSRangeMin(settings->getFloat("TrackerHSHist/sRangeMin",10.f,false)),
		mcSRangeMax(settings->getFloat("TrackerHSHist/sRangeMax",255.f,false)),
		terminationTH(0.f)
{
	mUseCamShift = settings->getBool("FaceTracker/useCamShift [bool]",false,false);
	ratioBP = settings->getFloat("FaceTracker/endTrackerParam",0.65f,false);
}


void SingleFaceTrackerHSHist::initializeTrackerFeature(boost::shared_ptr<FaceDetectorTarget> dt)
{
	cv::Mat input_image = mpVideoModule->currentFrame()->image;
	cv::Rect init_rect = dt->bounding_box;
	std::vector<std::vector<cv::Point> > init_contour = dt->contours;

	this->rows = input_image.rows;
	this->cols = input_image.cols;

	// set mTrackBoxMS for meanShift tracking
	mTrackBox = init_rect;

	this->updateSearchWindow();
	this->updateContourMask(init_contour);

	cv::Mat cropped;
	input_image(mCropRect).copyTo(cropped);

	int hsv_histSize[] = {mcHBins,mcSBins};
	const int	hsv_channels[] = {0,1};	// use h and s
	float	hranges[] = {mcHRangeMin,mcHRangeMax};
	float	sranges[] = {mcSRangeMin,mcSRangeMax};
	const float* hsv_ranges[] = {hranges,sranges};
	// convert to hsv
	cv::Mat hsv;
	cv::cvtColor(cropped, hsv, CV_RGB2HSV);
	cv::Mat	hsvmask;

	// color-based mask
	cv::inRange(hsv,cv::Scalar(0,30,10),cv::Scalar(180,255,255),hsvmask);
	// create histogram
	mContourMask &= hsvmask;

	// location-based mask
	cv::Mat lmask = cv::Mat::zeros(cropped.rows, cropped.cols, CV_8UC1);
	lmask(cv::Rect((int)(cropped.cols/5),(int)(cropped.rows/5),(int)((4*cropped.cols)/5),(int)((4*cropped.rows)/5))) = 1;
	mContourMask &= lmask;

	cv::calcHist(&hsv,1,hsv_channels, mContourMask, mHistogram, 2, hsv_histSize, hsv_ranges);
	// normalize it
	cv::normalize(mHistogram,mHistogram,0,255,cv::NORM_MINMAX);

	// determine a threshold for termination criteria of this target tracker
	cv::Mat backproj;
	cv::calcBackProject(&hsv, 1, hsv_channels, mHistogram, backproj, hsv_ranges);

	//  a parameter of trackers: mean likelihood ratio for the termination condition
	terminationTH = evalBPmap(backproj);

    if(terminationTH == 0)
           terminationTH = -1.0f;

}

bool SingleFaceTrackerHSHist::updateTrackerFeature(cv::Mat *disp_map,cv::Point &new_center)
{
	cv::Mat input_image = mpVideoModule->currentFrame()->image;

	// crop out input image
	cv::Mat	cropped;
	input_image(mCropRect).copyTo(cropped);

	cv::Mat disp_crop;
	if (disp_map != NULL)
	{
		disp_crop = (*disp_map)(mCropRect);
	}

	const int	hsv_channels[] = {0,1};	// use h and s
	float	hranges[] = {mcHRangeMin,mcHRangeMax};
	float	sranges[] = {mcSRangeMin,mcSRangeMax};
	const float	* hsv_ranges[] = {hranges,sranges};

	// convert to hsv
	cv::Mat	hsv;
	cv::cvtColor(cropped,hsv,CV_RGB2HSV);
	cv::Mat backproj(cropped.rows,cropped.cols,CV_8UC1);;
	cv::Mat mask;

	// get mask. mask filter out instable pixel values (too dark or saturation close to 0)
	cv::inRange(hsv,cv::Scalar(0,30,10),cv::Scalar(180,255,255),mask);
	cv::calcBackProject(&hsv, 1, hsv_channels, mHistogram, backproj, hsv_ranges);

	#ifdef DEBUG
		cv::Mat tempbpj;
		backproj.copyTo(tempbpj);
		cv::imwrite("hsvbackproj.png",tempbpj);
		save_raw(mHistogram,"hsvhist.bin");
		save_raw(tempbpj,"hsvbackproj.bin");
		cv::Mat tempbpjc;
		cv::cvtColor(tempbpj,tempbpjc,CV_GRAY2RGB);
		cv::Rect temprect = mTrackBox;
		temprect -= mCropRect.tl();
		cv::rectangle(tempbpjc,temprect,CV_RGB(0,0,255));
		cv::imwrite("hsvbeforeupdate.png",tempbpjc);
	#endif

	// apply camshift
	backproj &= mask;

	// for debugging, put backproj into bpimage
	if (disp_map != NULL)
	{
		backproj.copyTo(disp_crop);
	}

	// calculate likelihood
	cv::Mat_<float>	fLikelihood(cropped.rows,cropped.cols);

	float *likelihood_p = (float*)fLikelihood.data;
	uchar *bpj_p = (uchar*)backproj.data;
	for (int i=0;i<fLikelihood.rows;i++)
	{
		for (int j=0;j<fLikelihood.cols;j++)
		{
			if(mUseKF)
			{
				*likelihood_p = (float)*bpj_p * calculateLocationLikelihoodKF(j+mCropRect.x,i+mCropRect.y);
			}
			else
			{
				*likelihood_p = (float)*bpj_p;
			}
			likelihood_p++;
			bpj_p++;
		}
	}
		// run mean shift on likelihood
	// TODO: now, I use estimated person shape bounding box to use opencv mean-shift, but later change it to use estimated person shape directly
	//cv::Rect trackBox;
	//trackBox = mTrackBox;

	//mpVideoModule->enforceWindowRect(trackBox);
	mTrackBox -= mCropRect.tl();

    if(mUseCamShift == true)
    {
    	cv::CamShift(fLikelihood, mTrackBox, cv::TermCriteria(cv::TermCriteria::EPS | cv::TermCriteria::COUNT, 10,1));
    }
    else
    {
    	cv::meanShift(fLikelihood,mTrackBox,cv::TermCriteria(cv::TermCriteria::EPS | cv::TermCriteria::COUNT, 10, 1));
    }

    mTrackBox += mCropRect.tl();
	mpVideoModule->enforceWindowRect(mTrackBox);

	cv::Point	newCenter = (mTrackBox.tl()+mTrackBox.br())/2;

	bool	update_success;

    if(mUseKF)
    	mConfidence = evalBPmap2(backproj, fLikelihood) / terminationTH;
    else
    	mConfidence = evalBPmap(backproj) / terminationTH;

	if (mConfidence > ratioBP) // mcMatchLikelihoodThreshold)
	{
		// update target
		// mask off background sub map to avoid double detection
		//mpVisionDetector->maskOffForegroundImage(backproj,mCropRect);
		update_success = true;
	}
	else
	{
		// make this target inactive.
		update_success = false;
	}

	new_center = newCenter;

	return update_success;

}

float SingleFaceTrackerHSHist::evalBPmap2(cv::Mat &backproj, cv::Mat &fLikelihood)
{
	// evaluate the likelihood of the mean-shift result before update feature
		// calculate mean likelihood
		// use backproj as scratch buffer
		//backproj = 0;
		//mPersonShape.estimateOccupancyMapCropped(backproj,newCenter,mCropRect);

		uchar *bpj_p = (uchar*)backproj.data;
		float *likelihood_p = (float*)fLikelihood.data;
		float	likelihood_avg = 0.f;
		int		likelihood_num = 0;
		for (int i=0;i<fLikelihood.rows;i++)
		{
			for (int j=0;j<fLikelihood.cols;j++)
			{
				if (*bpj_p)
				{
					likelihood_avg += *likelihood_p;
					likelihood_num++;
				}
				likelihood_p++;
				bpj_p++;
			}
		}
		likelihood_avg /= (float)likelihood_num;

		return likelihood_avg;
}

/*
bool SingleFaceTrackerHSHist::update(const cv::Mat &input_image,
		cv::Mat *disp_map)
{
	if(terminationTH <= 0)
		return false;

	mConfidence = 0;

	// crop out input image
	cv::Mat	cropped;
	input_image(mCropRect).copyTo(cropped);

#ifdef DEBUG
	cv::imwrite("cropped.png",cropped);
#endif

	cv::Mat disp_crop;
	if (disp_map != NULL)
	{
		disp_crop = (*disp_map)(mCropRect);
	}

	const int	hsv_channels[] = {0,1};	// use h and s
	float	hranges[] = {mcHRangeMin,mcHRangeMax};
	float	sranges[] = {mcSRangeMin,mcSRangeMax};
	const float	* hsv_ranges[] = {hranges,sranges};

	// convert to hsv
	cv::Mat	hsv;
	cv::cvtColor(cropped,hsv,CV_RGB2HSV);

#ifdef DEBUG
	save_raw(hsv,"hsvcrop.bin");
#endif

    cv::Mat backproj(cropped.rows,cropped.cols,CV_8UC1);;
	cv::Mat mask;
	// get mask. mask filter out instable pixel values (too dark or saturation close to 0)
	cv::inRange(hsv,cv::Scalar(0,30,10),cv::Scalar(180,255,255),mask);
	cv::calcBackProject(&hsv, 1, hsv_channels, mHistogram, backproj, hsv_ranges);

#ifdef DEBUG
	cv::Mat tempbpj;
	backproj.copyTo(tempbpj);
	cv::imwrite("hsvbackproj.png",tempbpj);
	save_raw(mHistogram,"hsvhist.bin");
	save_raw(tempbpj,"hsvbackproj.bin");
	cv::Mat tempbpjc;
	cv::cvtColor(tempbpj,tempbpjc,CV_GRAY2RGB);
	cv::Rect temprect = mTrackBox;
	temprect -= mCropRect.tl();
	cv::rectangle(tempbpjc,temprect,CV_RGB(0,0,255));
	cv::imwrite("hsvbeforeupdate.png",tempbpjc);
#endif

	// apply camshift
	backproj &= mask;

	// for debugging, put backproj into bpimage
	if (disp_map != NULL)
	{
		backproj.copyTo(disp_crop);
	}


	bool	update_success;
	if(mUseKF)
	{
		// calculate likelihood
		cv::Mat_<float>	fLikelihood(cropped.rows,cropped.cols);
		float *likelihood_p = (float*)fLikelihood.data;
		uchar *bpj_p = (uchar*)backproj.data;

		float	likelihood_avg = 0.f;
		int		likelihood_num = 0;

		for (int i=0;i<fLikelihood.rows;i++)
		{
			for (int j=0;j<fLikelihood.cols;j++)
			{
				if(*bpj_p)
				{
					float valLiklihoodKF =calculateLocationLikelihoodKF(j+mCropRect.x,i+mCropRect.y);
					*likelihood_p = (float)(*bpj_p)/terminationTH *valLiklihoodKF;
                }
                else
                {
                    *likelihood_p = 0.f;
                }
				likelihood_p++;
				bpj_p++;
				likelihood_num++;
				likelihood_avg += (*likelihood_p);

			}
		}
		likelihood_avg /= (float)likelihood_num;

		// evaluation the target candidates
		if(likelihood_num ==0)	mConfidence = 0.f;
		else	mConfidence = likelihood_avg;

		//cv::Scalar fLikelihoodAvg = cv::mean(fLikelihood);
		//float likelihoodTh = 2.5f;   // 10% of 255

		//if(likelihood_avg <= mLikelihoodTh)
		if(mConfidence < ratioBP)
		{
			return false;
			//update_success=false;
		}
		else
		{
			update_success=true;
			backproj = likelihood_avg;
		}
	}
	else
	{
		// evaluation the target candidates
		mConfidence = evalBPmap(backproj) / terminationTH;
		if(mConfidence < ratioBP)
		{
			return false;
		}
		else
		{
			update_success = true;
		}
	}

//	save_raw(backproj,"backproj_mask.bin");
    mTrackBox -= mCropRect.tl();

    if(mUseCamShift == true)
    {
    	cv::RotatedRect mRotTrackBox = cv::CamShift(backproj, mTrackBox, cv::TermCriteria(cv::TermCriteria::EPS | cv::TermCriteria::COUNT, 10,1));
    }
    else
    {
    	cv::meanShift(backproj,mTrackBox,cv::TermCriteria(cv::TermCriteria::EPS | cv::TermCriteria::COUNT, 10, 1));
    }
    mTrackBox += mCropRect.tl();

#ifdef DEBUG
    //temprect = mTrackBox;
    //temprect -= mCropRect.tl();
    //cv::cvtColor(tempbpj,tempbpjc,CV_GRAY2RGB);
    //cv::rectangle(tempbpjc,temprect,CV_RGB(0,0,255));
    //cv::imwrite("hsvafterupdate.png",tempbpjc);
    cv::imwrite("bp_image.png", disp_crop);
#endif

	if(mUseKF)
	{
	    cv::Point	newCenter = (mTrackBox.tl()+mTrackBox.br())/2;
		if(update_success)
		{
			// update
			this->updateKalmanFilter(newCenter,update_success);
			mCenter.x = (int)mKF.statePost.at<float>(0);	//
			mCenter.y = (int)mKF.statePost.at<float>(1);
		}
		else
		{
			cv::Point dummy;
			this->updateKalmanFilter(dummy,update_success);
		}
	}

	this->updateSearchWindow();

	return update_success;
}
*/

void SingleFaceTrackerHSHist::drawTrackBox(cv::Mat &disp_image)
{
	cv::rectangle(disp_image,mTrackBox,HSHIST_TRACKBOX_COLOR);
}

float SingleFaceTrackerHSHist::evalBPmap(cv::Mat &bp_image)
{

    cv::Mat mask;
	cv::inRange(bp_image,10,255,mask);

	cv::Scalar meanVal, devVal;
	cv::meanStdDev(bp_image,meanVal, devVal, mask);


    return meanVal[0]/devVal[0];
}

// YCrCb Histogram-based tracker
SingleFaceTrackerCrCbHist::SingleFaceTrackerCrCbHist(boost::shared_ptr<Settings> settings):
		FaceBaseTracker(settings),
		mcCrBins(settings->getInt("TrackerCrCbHist/CrBins",30,false)),
		mcCbBins(settings->getInt("TrackerCrCbHist/CbBins",30,false)),
		mcCrRangeMin(settings->getFloat("TrackerCrCbHist/CrRangeMin",0.f,false)),
		mcCrRangeMax(settings->getFloat("TrackerCrCbHist/CrRangeMax",255.f,false)),
		mcCbRangeMin(settings->getFloat("TrackerCrCbHist/CbRangeMin",0.f,false)),
		mcCbRangeMax(settings->getFloat("TrackerCrCbHist/CbRangeMax",255.f,false)),
		terminationTH(0.f)
{

	mUseCamShift = settings->getBool("FaceTracker/useCamShift [bool]",false,false);
	ratioBP = settings->getFloat("FaceTracker/endTrackerParam",0.65f,false);

}

void SingleFaceTrackerCrCbHist::initializeTrackerFeature(boost::shared_ptr<FaceDetectorTarget> dt)
{

	cv::Mat input_image = mpVideoModule->currentFrame()->image;
	cv::Rect init_rect = dt->bounding_box;
	std::vector<std::vector<cv::Point> > init_contour = dt->contours;

	this->rows = input_image.rows;
	this->cols = input_image.cols;


	// set mTrackBoxMS for meanShift tracking
	mTrackBox = init_rect;

	this->updateSearchWindow();
	this->updateContourMask(init_contour);

	cv::Mat cropped;
	input_image(mCropRect).copyTo(cropped);

	int ycrcb_histSize[] = {mcCrBins,mcCbBins};
	const int	ycrcb_channels[] = {1,2};	// use cr and cb
	float	crranges[] = {mcCrRangeMin,mcCrRangeMax};
	float	cbranges[] = {mcCbRangeMin,mcCbRangeMax};
	const float* ycrcb_ranges[] = {crranges,cbranges};
	// convert to ycrcb
	cv::Mat ycrcb;
	cv::cvtColor(cropped, ycrcb, CV_RGB2YCrCb);
	cv::Mat	ycrcbmask;

	// color-based mask
	cv::inRange(ycrcb,cv::Scalar(10,10,10),cv::Scalar(245,245,245),ycrcbmask);

	// location-based mask
	cv::Mat lmask = cv::Mat::zeros(cropped.rows, cropped.cols, CV_8UC1);
	lmask(cv::Rect((int)(cropped.cols/5),(int)(cropped.rows/5),(int)((4*cropped.cols)/5),(int)((4*cropped.rows)/5))) = 1;

	mContourMask &= lmask;

	// create histogram
	mContourMask &= ycrcbmask;
	cv::calcHist(&ycrcb,1,ycrcb_channels, mContourMask, mHistogram, 2, ycrcb_histSize, ycrcb_ranges);
	// normalize it
	cv::normalize(mHistogram,mHistogram,0,255,cv::NORM_MINMAX);

	// determine a threshold for termination criteria of this target tracker
	cv::Mat backproj;
	cv::calcBackProject(&ycrcb, 1, ycrcb_channels, mHistogram, backproj, ycrcb_ranges);

	// Backprojection mask
	//cv::Mat	bpmask;
	//cv::inRange(backproj,150, 255,bpmask);
	//backproj &= bpmask;

	//  a parameter of trackers: mean likelihood ratio for the termination condition

	terminationTH = evalBPmap(backproj);

    if(terminationTH == 0)
           terminationTH = -1.0f;

}

bool SingleFaceTrackerCrCbHist::updateTrackerFeature(cv::Mat *disp_map,cv::Point &new_center)
{
	cv::Mat input_image = mpVideoModule->currentFrame()->image;

	// crop out input image
	cv::Mat	cropped;
	input_image(mCropRect).copyTo(cropped);

	cv::Mat disp_crop;
	if (disp_map != NULL)
	{
		disp_crop = (*disp_map)(mCropRect);
	}

	const int	ycrcb_channels[] = {1,2};	// use cr and cb
	float	crranges[] = {mcCrRangeMin,mcCrRangeMax};
	float	cbranges[] = {mcCbRangeMin,mcCbRangeMax};
	const float	* ycrcb_ranges[] = {crranges,cbranges};

	// convert to ycrcb
	cv::Mat	ycrcb;
	cv::cvtColor(cropped,ycrcb,CV_RGB2YCrCb);

	#ifdef DEBUG
		save_raw(ycrcb,"ycrcb_crop.bin");
	#endif

	cv::Mat backproj;
	cv::Mat mask;
	// get mask. mask filter out instable pixel values (too dark or saturation close to 0)
	cv::inRange(ycrcb,cv::Scalar(10,10,10),cv::Scalar(245,245,245),mask);
	cv::calcBackProject(&ycrcb, 1, ycrcb_channels, mHistogram, backproj, ycrcb_ranges);

	#ifdef DEBUG
		cv::Mat tempbpj;
		backproj.copyTo(tempbpj);
		cv::imwrite("ycrcbbackproj.png",tempbpj);
		save_raw(mHistogram,"ycrcbhist.bin");
		save_raw(tempbpj,"ycrcbbackproj.bin");
		cv::Mat tempbpjc;
		cv::cvtColor(tempbpj,tempbpjc,CV_GRAY2RGB);
		cv::Rect temprect = mTrackBox;
		temprect -= mCropRect.tl();
		cv::rectangle(tempbpjc,temprect,CV_RGB(0,0,255));
		cv::imwrite("ycrcbbeforeupdate.png",tempbpjc);
	#endif

	// apply camshift
	backproj &= mask;

	// for debugging, put backproj into bpimage
	if (disp_map != NULL)
	{
		backproj.copyTo(disp_crop);
	}

	// calculate likelihood
	cv::Mat_<float>	fLikelihood(cropped.rows,cropped.cols);

	float *likelihood_p = (float*)fLikelihood.data;
	uchar *bpj_p = (uchar*)backproj.data;
	for (int i=0;i<fLikelihood.rows;i++)
	{
		for (int j=0;j<fLikelihood.cols;j++)
		{
			if(mUseKF)
			{
				*likelihood_p = (float)*bpj_p * calculateLocationLikelihoodKF(j+mCropRect.x,i+mCropRect.y);
			}
			else
			{
				*likelihood_p = (float)*bpj_p;
			}
			likelihood_p++;
			bpj_p++;
		}
	}
		// run mean shift on likelihood
	//cv::Rect trackBox;
	//trackBox = mTrackBox;

	//mpVideoModule->enforceWindowRect(trackBox);
	mTrackBox -= mCropRect.tl();

    if(mUseCamShift == true)
    {
    	cv::CamShift(fLikelihood, mTrackBox, cv::TermCriteria(cv::TermCriteria::EPS | cv::TermCriteria::COUNT, 10,1));
    }
    else
    {
    	cv::meanShift(fLikelihood,mTrackBox,cv::TermCriteria(cv::TermCriteria::EPS | cv::TermCriteria::COUNT, 10, 1));
    }

    mTrackBox += mCropRect.tl();
	//mpVideoModule->enforceWindowRect(mTrackBox);

	cv::Point	newCenter = (mTrackBox.tl()+mTrackBox.br())/2;

    bool	update_success;

    if(mUseKF)
    	mConfidence = evalBPmap2(backproj, fLikelihood) / terminationTH;
    else
    	mConfidence = evalBPmap(backproj) / terminationTH;

	if (mConfidence > ratioBP) // mcMatchLikelihoodThreshold)
	{
		// update target
		// mask off background sub map to avoid double detection
		//mpVisionDetector->maskOffForegroundImage(backproj,mCropRect);
		update_success = true;
	}
	else
	{
		// make this target inactive.
		update_success = false;
	}

	new_center = newCenter;

	return update_success;

}

float SingleFaceTrackerCrCbHist::evalBPmap2(cv::Mat &backproj, cv::Mat &fLikelihood)
{
	// evaluate the likelihood of the mean-shift result before update feature
		// calculate mean likelihood
		// use backproj as scratch buffer
		//backproj = 0;
		//mPersonShape.estimateOccupancyMapCropped(backproj,newCenter,mCropRect);

		uchar *bpj_p = (uchar*)backproj.data;
		float *likelihood_p = (float*)fLikelihood.data;
		float	likelihood_avg = 0.f;
        int		likelihood_num = 0;;
		for (int i=0;i<fLikelihood.rows;i++)
		{
			for (int j=0;j<fLikelihood.cols;j++)
			{
				if (*bpj_p)
				{
					likelihood_avg += *likelihood_p;
					likelihood_num++;
				}
				likelihood_p++;
				bpj_p++;
			}
		}
		likelihood_avg /= (float)likelihood_num;

		return likelihood_avg;
}
/*
bool SingleFaceTrackerCrCbHist::update(const cv::Mat &input_image,
		cv::Mat *disp_map)
{

	if(terminationTH <=0)
		return false;

	mConfidence = 0;

	// crop out input image
	cv::Mat	cropped;
	input_image(mCropRect).copyTo(cropped);

#ifdef DEBUG
	cv::imwrite("cropped.png",cropped);
#endif

	cv::Mat disp_crop;
	if (disp_map != NULL)
	{
		disp_crop = (*disp_map)(mCropRect);
	}

	const int	ycrcb_channels[] = {1,2};	// use cr and cb
	float	crranges[] = {mcCrRangeMin,mcCrRangeMax};
	float	cbranges[] = {mcCbRangeMin,mcCbRangeMax};
	const float	* ycrcb_ranges[] = {crranges,cbranges};

	// convert to ycrcb
	cv::Mat	ycrcb;
	cv::cvtColor(cropped,ycrcb,CV_RGB2YCrCb);

#ifdef DEBUG;
	save_raw(ycrcb,"ycrcb_crop.bin");
#endif

	cv::Mat backproj;
	cv::Mat mask;
	// get mask. mask filter out instable pixel values (too dark or saturation close to 0)
	cv::inRange(ycrcb,cv::Scalar(10,10,10),cv::Scalar(245,245,245),mask);
	cv::calcBackProject(&ycrcb, 1, ycrcb_channels, mHistogram, backproj, ycrcb_ranges);

#ifdef DEBUG
	cv::Mat tempbpj;
	backproj.copyTo(tempbpj);
	cv::imwrite("ycrcbbackproj.png",tempbpj);
	save_raw(mHistogram,"ycrcbhist.bin");
	save_raw(tempbpj,"ycrcbbackproj.bin");
	cv::Mat tempbpjc;
	cv::cvtColor(tempbpj,tempbpjc,CV_GRAY2RGB);
	cv::Rect temprect = mTrackBox;
	temprect -= mCropRect.tl();
	cv::rectangle(tempbpjc,temprect,CV_RGB(0,0,255));
	cv::imwrite("ycrcbbeforeupdate.png",tempbpjc);
#endif

	// apply camshift
	backproj &= mask;
	// for debugging, put backproj into bpimage
	if (disp_map != NULL)
	{
		backproj.copyTo(disp_crop);
	}

	bool	update_success;
	if(mUseKF)
	{
		// calculate likelihood
		cv::Mat_<float>	fLikelihood(cropped.rows,cropped.cols);
		float *likelihood_p = (float*)fLikelihood.data;
		uchar *bpj_p = (uchar*)backproj.data;

		float	likelihood_avg = 0.f;
		int		likelihood_num = 0;

		for (int i=0;i<fLikelihood.rows;i++)
		{
			for (int j=0;j<fLikelihood.cols;j++)
			{
				if(*bpj_p)
				{
					float valLiklihoodKF =calculateLocationLikelihoodKF(j+mCropRect.x,i+mCropRect.y);
					*likelihood_p = (float)(*bpj_p) *valLiklihoodKF;
                }
                else
                {
                    *likelihood_p = 0.f;
                }
				likelihood_p++;
				bpj_p++;
				likelihood_num++;
				likelihood_avg += (*likelihood_p);

			}
		}
		likelihood_avg /= (float)likelihood_num;

		// evaluation the target candidates
		if(likelihood_num ==0)	mConfidence = 0.f;
		else	mConfidence = likelihood_avg /terminationTH;

		//cv::Scalar fLikelihoodAvg = cv::mean(fLikelihood);
		//float likelihoodTh = 2.5f;   // 10% of 255

		//if(likelihood_avg <= mLikelihoodTh)
        if(mConfidence < 0.1  || isnan(mConfidence))
		{
			return false;
			//update_success=false;
		}
		else
		{
			update_success=true;
			backproj = likelihood_avg;
		}
	}
	else
	{
		// evaluation the target candidates
		mConfidence = evalBPmap(backproj) / terminationTH;
		if(mConfidence < ratioBP)
		{
			return false;
		}
		else
		{
			update_success = true;
		}
	}


//	save_raw(backproj,"backproj_mask.bin");
    mTrackBox -= mCropRect.tl();

    if(mUseCamShift == true)
    {
    	cv::RotatedRect mRotTrackBox = cv::CamShift(backproj, mTrackBox, cv::TermCriteria(cv::TermCriteria::EPS | cv::TermCriteria::COUNT, 10,1));
    }
    else
    {
    	cv::meanShift(backproj,mTrackBox,cv::TermCriteria(cv::TermCriteria::EPS | cv::TermCriteria::COUNT, 10, 1));
    }
    mTrackBox += mCropRect.tl();

#ifdef DEBUG
    //temprect = mTrackBox;
    //temprect -= mCropRect.tl();
    //cv::cvtColor(tempbpj,tempbpjc,CV_GRAY2RGB);
    //cv::rectangle(tempbpjc,temprect,CV_RGB(0,0,255));
    //cv::imwrite("hsvafterupdate.png",tempbpjc);
    cv::imwrite("bp_image.png", disp_crop);
#endif


	if(mUseKF)
	{
	    cv::Point	newCenter = (mTrackBox.tl()+mTrackBox.br())/2;
		if(update_success)
		{
			// update
			this->updateKalmanFilter(newCenter,update_success);
			mCenter.x = (int)mKF.statePost.at<float>(0);	//
			mCenter.y = (int)mKF.statePost.at<float>(1);
		}
		else
		{
			cv::Point dummy;
			this->updateKalmanFilter(dummy,update_success);
		}
	}

	this->updateSearchWindow();

	return true;
}
*/
void SingleFaceTrackerCrCbHist::drawTrackBox(cv::Mat &disp_image)
{
	cv::rectangle(disp_image,mTrackBox,HSHIST_TRACKBOX_COLOR);
}

float SingleFaceTrackerCrCbHist::evalBPmap(cv::Mat &bp_image)
{

    cv::Mat mask, maskHL;
	cv::inRange(bp_image,10,255,mask);   // filtered likelihood

//	float likelihood;
	/*
	cv::inRange(bp_image,180,255,maskHL);   // filtered likelihood
	cv::Mat tmp = cv::Mat::ones(maskHL.rows, maskHL.cols, CV_8UC1);
	cv::Scalar areaSize = cv::sum(tmp);

	cv::Scalar area = cv::sum(maskHL);

    float validArea = area[0] / (areaSize[0]*255);

	if(validArea < 0.5)
		likelihood = 0.f;
	else
	{
*/
    // Mean value-based likelihood

//		cv::Scalar meanVal = cv::mean(bp_image, mask);
//		likelihood = meanVal[0];
//	}

	// Gaussian weighed likelihood


//   return likelihood;

	cv::Scalar meanVal, devVal;
	cv::meanStdDev(bp_image,meanVal, devVal, mask);


    return meanVal[0]/devVal[0];
}



// RGChromHist
SingleFaceTrackerRGChromHist::SingleFaceTrackerRGChromHist(boost::shared_ptr<Settings> settings):
		FaceBaseTracker(settings),
		mcRGBins(settings->getInt("TrackerRGChromHist/rgBins",30,false)),
		mcColorRangeMin(settings->getFloat("TrackerRGChromHist/colorRangeMin",0.35f,false)),
		mcColorRangeMax(settings->getFloat("TrackerRGChromHist/colorRangeMax",2.f,false)),
		terminationTH(0.f)
{
	mUseCamShift = settings->getBool("FaceTracker/useCamShift [bool]",false,false);
	ratioBP = settings->getFloat("FaceTracker/endTrackerParam",0.65f,false);

}

void SingleFaceTrackerRGChromHist::initializeTrackerFeature(boost::shared_ptr<FaceDetectorTarget> dt)
{
	cv::Mat input_image;
	input_image = mpVideoModule->currentFrame()->image;

	cv::Rect init_rect;
	std::vector<std::vector<cv::Point> > init_contour;
	init_rect = dt->bounding_box;
	init_contour = dt->contours;

	this->rows = input_image.rows;
	this->cols = input_image.cols;

	// set mTrackBoxMS for meanShift tracking
	mTrackBox = init_rect;

	this->updateSearchWindow();
	this->updateContourMask(init_contour);

	cv::Mat cropped;
	input_image(mCropRect).copyTo(cropped);
//	save_raw(cropped,"init_crop.bin");

	// use rg-chromaticity
	// convert cropped window to RGBfImage
	RGBfImage cropped_rgb(cropped.rows,cropped.cols);
	uchar *p_cropped = (uchar*)cropped.data;
	float *p_cropped_rgb = (float*)cropped_rgb.data;
	for (int i=0;i<cropped_rgb.pixelCount()*3;i++)
	{
		*p_cropped_rgb++ = (float)*p_cropped++/255.f;
	}

	// convert to rg-chrom
	cropped_rgb.rg_chrom();

//		save_raw(cropped_rgb,"rg_chrom.bin");

	// use two channels
	const int rg_histsize[] = {mcRGBins,mcRGBins};
	const int rg_channels[] = {0,1};

	float r_ranges[] = {0.012f, 1.f};
	float g_ranges[] = {0.012f, 1.f};

	const float *rg_ranges[] = {r_ranges,g_ranges};

	cv::Mat rgbmask;

	cv::inRange(cropped,cv::Scalar(3,3,3),cv::Scalar(254,254,254),rgbmask);
	mContourMask &= rgbmask;

	// location-based mask
	cv::Mat lmask = cv::Mat::zeros(cropped.rows, cropped.cols, CV_8UC1);
	lmask(cv::Rect((int)(cropped.cols/4),(int)(cropped.rows/4),(int)((3*cropped.cols)/4),(int)((3*cropped.rows)/4))) = 1;
	mContourMask &= lmask;

	cv::calcHist(&cropped_rgb,1,rg_channels,mContourMask,mHistogram,2,rg_histsize,rg_ranges);
	cv::normalize(mHistogram,mHistogram,0,255,cv::NORM_MINMAX);

	// determine a threshold for termination criteria of this target tracker
	cv::Mat backproj;
	cv::calcBackProject(&cropped_rgb, 1, rg_channels, mHistogram, backproj, rg_ranges);

	//  a parameter of trackers: mean likelihood ratio for the termination condition
	terminationTH = evalBPmap(backproj);

    if(terminationTH == 0)
           terminationTH = -1.0f;

//		save_raw(mHistogram,"r_hist.bin");
//		save_raw(mGHistogram, "g_hist.bin");

}

bool SingleFaceTrackerRGChromHist::updateTrackerFeature(cv::Mat *disp_map,cv::Point &new_center)
{
	cv::Mat input_image = mpVideoModule->currentFrame()->image;

	// crop out input image
	cv::Mat	cropped;
	input_image(mCropRect).copyTo(cropped);

	cv::Mat disp_crop;
	if (disp_map != NULL)
	{
		disp_crop = (*disp_map)(mCropRect);
	}

	// use rg-chromaticity
	// convert cropped window to RGBfImage
	RGBfImage cropped_rgb(cropped.rows,cropped.cols);
	uchar *p_cropped = (uchar*)cropped.data;
	float *p_cropped_rgb = (float*)cropped_rgb.data;
	for (int i=0;i<cropped_rgb.pixelCount()*3;i++)
	{
		*p_cropped_rgb++ = (float)*p_cropped++/255.f;
	}

	// convert to rg-chrom
	cropped_rgb.rg_chrom();

	// use two channels
	const int rg_channels[] = {0,1};

	float r_ranges[] = {0.012f, 1.f};
	float g_ranges[] = {0.012f, 1.f};

	const float *rg_ranges[] = {r_ranges,g_ranges};

	cv::Mat	mask;
	cv::Mat	rg_bpj;

	cv::inRange(cropped,cv::Scalar(3,3,3),cv::Scalar(254,254,254),mask);
	cv::calcBackProject(&cropped_rgb,1,rg_channels,mHistogram,rg_bpj,rg_ranges);

	cv::Mat	backproj(rg_bpj.rows,rg_bpj.cols,CV_8UC1);

	// convert backproj depth in order to bitwise and with mask
	uchar *bpj = (uchar*)backproj.data;
	float *rbpj = (float*)rg_bpj.data;
	for (int i=0;i<rg_bpj.rows*rg_bpj.cols;i++)
	{
		*bpj++ = (uchar)*rbpj++;
	}
	// for debugging, put backproj into bpimage
	if (disp_map != NULL)
	{
		backproj.copyTo(disp_crop);
	}

	// calculate likelihood
	cv::Mat_<float>	fLikelihood(cropped.rows,cropped.cols);

	float *likelihood_p = (float*)fLikelihood.data;
	uchar *bpj_p = (uchar*)backproj.data;
	for (int i=0;i<fLikelihood.rows;i++)
	{
		for (int j=0;j<fLikelihood.cols;j++)
		{
			if(mUseKF)
			{
				*likelihood_p = (float)*bpj_p * calculateLocationLikelihoodKF(j+mCropRect.x,i+mCropRect.y);
			}
			else
			{
				*likelihood_p = (float)*bpj_p;
			}
			likelihood_p++;
			bpj_p++;
		}
	}
		// run mean shift on likelihood
	// TODO: now, I use estimated person shape bounding box to use opencv mean-shift, but later change it to use estimated person shape directly
	//cv::Rect trackBox;
	//trackBox = mTrackBox;

	//mpVideoModule->enforceWindowRect(trackBox);
	mTrackBox -= mCropRect.tl();

    if(mUseCamShift == true)
    {
    	cv::CamShift(fLikelihood, mTrackBox, cv::TermCriteria(cv::TermCriteria::EPS | cv::TermCriteria::COUNT, 10,1));
    }
    else
    {
    	cv::meanShift(fLikelihood,mTrackBox,cv::TermCriteria(cv::TermCriteria::EPS | cv::TermCriteria::COUNT, 10, 1));
    }

    mTrackBox += mCropRect.tl();
	mpVideoModule->enforceWindowRect(mTrackBox);

	cv::Point	newCenter = (mTrackBox.tl()+mTrackBox.br())/2;

	//float	likelihood_avg = 0.f;
	//likelihood_avg = evalBPmap2(backproj, fLikelihood);

	bool	update_success;

    if(mUseKF)
    	mConfidence = evalBPmap2(backproj, fLikelihood) / terminationTH;
    else
    	mConfidence = evalBPmap(backproj) / terminationTH;

	if (mConfidence > ratioBP) // mcMatchLikelihoodThreshold)
	{
		// update target
		// mask off background sub map to avoid double detection
		//mpVisionDetector->maskOffForegroundImage(backproj,mCropRect);
		update_success = true;
	}
	else
	{
		// make this target inactive.
		update_success = false;
	}

	new_center = newCenter;

	return update_success;

}

float SingleFaceTrackerRGChromHist::evalBPmap2(cv::Mat &backproj, cv::Mat &fLikelihood)
{
	// evaluate the likelihood of the mean-shift result before update feature
		// calculate mean likelihood
		// use backproj as scratch buffer
		//backproj = 0;
		//mPersonShape.estimateOccupancyMapCropped(backproj,newCenter,mCropRect);

		uchar *bpj_p = (uchar*)backproj.data;
		float *likelihood_p = (float*)fLikelihood.data;
		float	likelihood_avg = 0.f;
		int		likelihood_num = 0;
		for (int i=0;i<fLikelihood.rows;i++)
		{
			for (int j=0;j<fLikelihood.cols;j++)
			{
				if (*bpj_p)
				{
					likelihood_avg += *likelihood_p;
					likelihood_num++;
				}
				likelihood_p++;
				bpj_p++;
			}
		}
		likelihood_avg /= (float)likelihood_num;

		return likelihood_avg;
}

/*
bool SingleFaceTrackerRGChromHist::update(const cv::Mat &input_image,
		cv::Mat *disp_map)
{
	// crop out input image
	cv::Mat	cropped;
	input_image(mCropRect).copyTo(cropped);
	cv::Mat disp_crop;
	if (disp_map != NULL)
	{
		disp_crop = (*disp_map)(mCropRect);
	}

	// use rg-chromaticity
	// convert cropped window to RGBfImage
	RGBfImage cropped_rgb(cropped.rows,cropped.cols);
	uchar *p_cropped = (uchar*)cropped.data;
	float *p_cropped_rgb = (float*)cropped_rgb.data;
	for (int i=0;i<cropped_rgb.pixelCount()*3;i++)
	{
		*p_cropped_rgb++ = (float)*p_cropped++/255.f;
	}

	// convert to rg-chrom
	cropped_rgb.rg_chrom();

	// use two channels
	const int rg_channels[] = {0,1};

	float r_ranges[] = {0.012f, 1.f};
	float g_ranges[] = {0.012f, 1.f};

	const float *rg_ranges[] = {r_ranges,g_ranges};

	cv::Mat	mask;
	cv::Mat	rg_bpj;

	cv::inRange(cropped,cv::Scalar(3,3,3),cv::Scalar(254,254,254),mask);
	cv::calcBackProject(&cropped_rgb,1,rg_channels,mHistogram,rg_bpj,rg_ranges);

	// evaluation the target candidates
	mConfidence = evalBPmap(rg_bpj) / terminationTH;
	if(mConfidence < ratioBP )
	{
		return false;
	}

	cv::Mat	backproj(rg_bpj.rows,rg_bpj.cols,CV_8UC1);

	// convert backproj depth in order to bitwise and with mask
	uchar *bpj = (uchar*)backproj.data;
	float *rbpj = (float*)rg_bpj.data;
	for (int i=0;i<rg_bpj.rows*rg_bpj.cols;i++)
	{
		*bpj++ = (uchar)*rbpj++;
	}

	// apply camshift
	backproj &= mask;
	// for debugging, put backproj into bpimage
	if (disp_map != NULL)
	{
		backproj.copyTo(disp_crop);
	}

//	save_raw(backproj,"backproj_mask.bin");
    mTrackBox -= mCropRect.tl();

    if(mUseCamShift == true)
    {
    	cv::RotatedRect mRotTrackBox = cv::CamShift(backproj, mTrackBox, cv::TermCriteria(cv::TermCriteria::EPS | cv::TermCriteria::COUNT, 10,1));
    }
    else
    {
    	cv::meanShift(backproj,mTrackBox,cv::TermCriteria(cv::TermCriteria::EPS | cv::TermCriteria::COUNT, 10, 1));
    }
    mTrackBox += mCropRect.tl();

	this->updateSearchWindow();

	return true;
}
*/

void SingleFaceTrackerRGChromHist::drawTrackBox(cv::Mat &disp_image)
{
	cv::rectangle(disp_image,mTrackBox,RGCHROMHIST_TRACKBOX_COLOR);
}

float SingleFaceTrackerRGChromHist::evalBPmap(cv::Mat &bp_image)
{

    cv::Mat mask;
	cv::inRange(bp_image,10,255,mask);

	cv::Scalar meanVal, devVal;
	cv::meanStdDev(bp_image,meanVal, devVal, mask);


    return meanVal[0]/devVal[0];
}

SingleFaceTrackerRGBHist::SingleFaceTrackerRGBHist(boost::shared_ptr<Settings> settings):
		FaceBaseTracker(settings),
		mcRGBBins(settings->getInt("TrackerRGBHist/rgbBins",64,false)),
		mcColorRangeMin(settings->getFloat("TrackerRGBHist/colorRangeMin",3.f,false)),
		mcColorRangeMax(settings->getFloat("TrackerRGBHist/colorRangeMax",254.f,false)),
		terminationTH(0.f)
{
	mUseCamShift = settings->getBool("FaceTracker/useCamShift [bool]",false,false);
	ratioBP = settings->getFloat("FaceTracker/endTrackerParam",0.65f,false);

}


void SingleFaceTrackerRGBHist::initializeTrackerFeature(boost::shared_ptr<FaceDetectorTarget> dt)
{
	cv::Mat input_image;
	input_image = mpVideoModule->currentFrame()->image;

	cv::Rect init_rect;
	std::vector<std::vector<cv::Point> > init_contour;
	init_rect = dt->bounding_box;
	init_contour = dt->contours;

	this->rows = input_image.rows;
	this->cols = input_image.cols;

	// set mTrackBoxMS for meanShift tracking
	mTrackBox = init_rect;

	this->updateSearchWindow();
	this->updateContourMask(init_contour);

	cv::Mat cropped;
	input_image(mCropRect).copyTo(cropped);
//	save_raw(cropped,"init_crop.bin");

	// use three channels
	const int rgb_histsize = mcRGBBins;
	const int	r_channel = 0;
	const int	g_channel = 1;
	const int	b_channel = 2;

	const float	rgb_ranges[] = {3.f,255.f};
	const float *prgb_ranges = rgb_ranges;

	cv::Mat rgbmask;
	cv::inRange(cropped,cv::Scalar(3,3,3),cv::Scalar(254,254,254),rgbmask);
	mContourMask &= rgbmask;
	// location-based mask
	cv::Mat lmask = cv::Mat::zeros(cropped.rows, cropped.cols, CV_8UC1);
	lmask(cv::Rect((int)(cropped.cols/4),(int)(cropped.rows/4),(int)((3*cropped.cols)/4),(int)((3*cropped.rows)/4))) = 1;
	mContourMask &= lmask;

	cv::calcHist(&cropped,1,&r_channel,mContourMask,mRHistogram,1,&rgb_histsize,&prgb_ranges);
	cv::normalize(mRHistogram,mRHistogram,0,255,cv::NORM_MINMAX);
	cv::calcHist(&cropped,1,&g_channel,mContourMask,mGHistogram,1,&rgb_histsize,&prgb_ranges);
	cv::normalize(mGHistogram,mGHistogram,0,255,cv::NORM_MINMAX);
	cv::calcHist(&cropped,1,&b_channel,mContourMask,mBHistogram,1,&rgb_histsize,&prgb_ranges);
	cv::normalize(mBHistogram,mBHistogram,0,255,cv::NORM_MINMAX);

	// determine a threshold for termination criteria of this target tracker
	const float rranges[] = {0.f,255.f};
	const float *p_ranges = rranges;
	cv::Mat	r_bpj;
	cv::Mat g_bpj;
	cv::Mat b_bpj;
	cv::Mat mask;
	cv::inRange(cropped,cv::Scalar(3,3,3),cv::Scalar(254,254,254),mask);
	cv::calcBackProject(&cropped,1,&r_channel,mRHistogram,r_bpj,&p_ranges);
	cv::calcBackProject(&cropped,1,&g_channel,mGHistogram,g_bpj,&p_ranges);
	cv::calcBackProject(&cropped,1,&b_channel,mBHistogram,b_bpj,&p_ranges);
	cv::Mat backproj(r_bpj.rows,r_bpj.cols,CV_8UC1);

	uchar *p_rbpj = (uchar*)r_bpj.data;
	uchar *p_gbpj = (uchar*)g_bpj.data;
	uchar *p_bbpj = (uchar*)b_bpj.data;
	uchar *p_backproj = (uchar*)backproj.data;

	for (int i=0;i<r_bpj.rows*r_bpj.cols;i++)
	{
		float tf = (float)*p_rbpj++;
		tf += (float)*p_gbpj++;
		tf += (float)*p_bbpj++;
		tf /= 3.f;

		*p_backproj++ = (uchar)tf;
	}

	//  a parameter of trackers: mean likelihood ratio for the termination condition
	terminationTH = evalBPmap(backproj);

    if(terminationTH == 0)
           terminationTH = -1.0f;
}

bool SingleFaceTrackerRGBHist::updateTrackerFeature(cv::Mat *disp_map,cv::Point &new_center)
{
	cv::Mat input_image = mpVideoModule->currentFrame()->image;

	// crop out input image
	cv::Mat	cropped;
	input_image(mCropRect).copyTo(cropped);

	cv::Mat disp_crop;
	if (disp_map != NULL)
	{
		disp_crop = (*disp_map)(mCropRect);
	}

	const int r_channel = 0;
	const int g_channel = 1;
	const int b_channel = 2;
	const float rranges[] = {0.f,255.f};
	const float *p_ranges = rranges;
	cv::Mat	r_bpj;
	cv::Mat g_bpj;
	cv::Mat b_bpj;
	cv::Mat mask;
	cv::inRange(cropped,cv::Scalar(3,3,3),cv::Scalar(254,254,254),mask);
	cv::calcBackProject(&cropped,1,&r_channel,mRHistogram,r_bpj,&p_ranges);
	cv::calcBackProject(&cropped,1,&g_channel,mGHistogram,g_bpj,&p_ranges);
	cv::calcBackProject(&cropped,1,&b_channel,mBHistogram,b_bpj,&p_ranges);

	cv::Mat backproj(r_bpj.rows,r_bpj.cols,CV_8UC1);

	uchar *p_rbpj = (uchar*)r_bpj.data;
	uchar *p_gbpj = (uchar*)g_bpj.data;
	uchar *p_bbpj = (uchar*)b_bpj.data;
	uchar *p_backproj = (uchar*)backproj.data;

	for (int i=0;i<r_bpj.rows*r_bpj.cols;i++)
	{
		float tf = (float)*p_rbpj++;
		tf += (float)*p_gbpj++;
		tf += (float)*p_bbpj++;
		tf /= 3.f;

		*p_backproj++ = (uchar)tf;
	}

#ifdef DEBUG
	cv::Mat tempbpj;
	backproj.copyTo(tempbpj);
	cv::imwrite("rgbbackproj.png",tempbpj);
	save_raw(mRHistogram,"rhist.bin");
	save_raw(mGHistogram,"ghist.bin");
	save_raw(mBHistogram,"bhist.bin");
	save_raw(tempbpj,"rgbbackproj.bin");
	cv::Mat tempbpjc;
	cv::cvtColor(tempbpj,tempbpjc,CV_GRAY2RGB);
	cv::Rect temprect = mTrackBox;
	temprect -= mCropRect.tl();
	cv::rectangle(tempbpjc,temprect,CV_RGB(0,0,255));
	cv::imwrite("rgbbeforeupdate.png",tempbpjc);
#endif

	// apply camshift
	backproj &= mask;

	// for debugging, put backproj into bpimage
	if (disp_map != NULL)
	{
		backproj.copyTo(disp_crop);
	}

	// calculate likelihood
	cv::Mat_<float>	fLikelihood(cropped.rows,cropped.cols);

	float *likelihood_p = (float*)fLikelihood.data;
	uchar *bpj_p = (uchar*)backproj.data;
	for (int i=0;i<fLikelihood.rows;i++)
	{
		for (int j=0;j<fLikelihood.cols;j++)
		{
			if(mUseKF)
			{
				*likelihood_p = (float)*bpj_p * calculateLocationLikelihoodKF(j+mCropRect.x,i+mCropRect.y);
			}
			else
			{
				*likelihood_p = (float)*bpj_p;
			}
			likelihood_p++;
			bpj_p++;
		}
	}
		// run mean shift on likelihood
	// TODO: now, I use estimated person shape bounding box to use opencv mean-shift, but later change it to use estimated person shape directly
	//cv::Rect trackBox;
	//trackBox = mTrackBox;

	//mpVideoModule->enforceWindowRect(trackBox);
	mTrackBox -= mCropRect.tl();

    if(mUseCamShift == true)
    {
    	cv::CamShift(fLikelihood, mTrackBox, cv::TermCriteria(cv::TermCriteria::EPS | cv::TermCriteria::COUNT, 10,1));
    }
    else
    {
    	cv::meanShift(fLikelihood,mTrackBox,cv::TermCriteria(cv::TermCriteria::EPS | cv::TermCriteria::COUNT, 10, 1));
    }

    mTrackBox += mCropRect.tl();
	mpVideoModule->enforceWindowRect(mTrackBox);

	cv::Point	newCenter = (mTrackBox.tl()+mTrackBox.br())/2;

	//float	likelihood_avg = 0.f;
	//likelihood_avg = evalBPmap2(backproj, fLikelihood);

	bool	update_success;

    if(mUseKF)
    	mConfidence = evalBPmap2(backproj, fLikelihood) / terminationTH;
    else
    	mConfidence = evalBPmap(backproj) / terminationTH;

	if (mConfidence > ratioBP) // mcMatchLikelihoodThreshold)
	{
		// update target
		// mask off background sub map to avoid double detection
		//mpVisionDetector->maskOffForegroundImage(backproj,mCropRect);
		update_success = true;
	}
	else
	{
		// make this target inactive.
		update_success = false;
	}

	new_center = newCenter;

	return update_success;

}

float SingleFaceTrackerRGBHist::evalBPmap2(cv::Mat &backproj, cv::Mat &fLikelihood)
{
	// evaluate the likelihood of the mean-shift result before update feature
		// calculate mean likelihood
		// use backproj as scratch buffer
		//backproj = 0;
		//mPersonShape.estimateOccupancyMapCropped(backproj,newCenter,mCropRect);

		uchar *bpj_p = (uchar*)backproj.data;
		float *likelihood_p = (float*)fLikelihood.data;
		float	likelihood_avg = 0.f;
		int		likelihood_num = 0;
		for (int i=0;i<fLikelihood.rows;i++)
		{
			for (int j=0;j<fLikelihood.cols;j++)
			{
				if (*bpj_p)
				{
					likelihood_avg += *likelihood_p;
					likelihood_num++;
				}
				likelihood_p++;
				bpj_p++;
			}
		}
		likelihood_avg /= (float)likelihood_num;

		return likelihood_avg;
}
/*
bool SingleFaceTrackerRGBHist::update(const cv::Mat &input_image,
		cv::Mat *disp_map)
{
	// crop out input image
	cv::Mat	cropped;
	input_image(mCropRect).copyTo(cropped);

#ifdef DEBUG
	cv::imwrite("rgbcropped.png",cropped);
#endif

	cv::Mat disp_crop;
	if (disp_map != NULL)
	{
		disp_crop = (*disp_map)(mCropRect);
	}

	const int r_channel = 0;
	const int g_channel = 1;
	const int b_channel = 2;
	const float rranges[] = {0.f,255.f};
	const float *p_ranges = rranges;
	cv::Mat	r_bpj;
	cv::Mat g_bpj;
	cv::Mat b_bpj;
	cv::Mat mask;
	cv::inRange(cropped,cv::Scalar(3,3,3),cv::Scalar(254,254,254),mask);
	cv::calcBackProject(&cropped,1,&r_channel,mRHistogram,r_bpj,&p_ranges);
	cv::calcBackProject(&cropped,1,&g_channel,mGHistogram,g_bpj,&p_ranges);
	cv::calcBackProject(&cropped,1,&b_channel,mBHistogram,b_bpj,&p_ranges);

	cv::Mat backproj(r_bpj.rows,r_bpj.cols,CV_8UC1);

	uchar *p_rbpj = (uchar*)r_bpj.data;
	uchar *p_gbpj = (uchar*)g_bpj.data;
	uchar *p_bbpj = (uchar*)b_bpj.data;
	uchar *p_backproj = (uchar*)backproj.data;

	for (int i=0;i<r_bpj.rows*r_bpj.cols;i++)
	{
		float tf = (float)*p_rbpj++;
		tf += (float)*p_gbpj++;
		tf += (float)*p_bbpj++;
		tf /= 3.f;

		*p_backproj++ = (uchar)tf;
	}

#ifdef DEBUG
	cv::Mat tempbpj;
	backproj.copyTo(tempbpj);
	cv::imwrite("rgbbackproj.png",tempbpj);
	save_raw(mRHistogram,"rhist.bin");
	save_raw(mGHistogram,"ghist.bin");
	save_raw(mBHistogram,"bhist.bin");
	save_raw(tempbpj,"rgbbackproj.bin");
	cv::Mat tempbpjc;
	cv::cvtColor(tempbpj,tempbpjc,CV_GRAY2RGB);
	cv::Rect temprect = mTrackBox;
	temprect -= mCropRect.tl();
	cv::rectangle(tempbpjc,temprect,CV_RGB(0,0,255));
	cv::imwrite("rgbbeforeupdate.png",tempbpjc);
#endif

	// apply camshift
	backproj &= mask;
	// for debugging, put backproj into bpimage
	if (disp_map != NULL)
	{
		backproj.copyTo(disp_crop);
	}

//	save_raw(backproj,"backproj_mask.bin");
    mTrackBox -= mCropRect.tl();

    if(mUseCamShift == true)
    {
    	cv::RotatedRect mRotTrackBox = cv::CamShift(backproj, mTrackBox, cv::TermCriteria(cv::TermCriteria::EPS | cv::TermCriteria::COUNT, 10,1));
    }
    else
    {
    	cv::meanShift(backproj,mTrackBox,cv::TermCriteria(cv::TermCriteria::EPS | cv::TermCriteria::COUNT, 10, 1));
    }
    mTrackBox += mCropRect.tl();

#ifdef DEBUG
	temprect = mTrackBox;
	temprect -= mCropRect.tl();
	cv::cvtColor(tempbpj,tempbpjc,CV_GRAY2RGB);
	cv::rectangle(tempbpjc,temprect,CV_RGB(0,0,255));
	cv::imwrite("rgbafterupdate.png",tempbpjc);
#endif
	this->updateSearchWindow();

	return true;
}
*/

void SingleFaceTrackerRGBHist::drawTrackBox(cv::Mat &disp_image)
{
	cv::rectangle(disp_image,mTrackBox,RGBHIST_TRACKBOX_COLOR);
}

float SingleFaceTrackerRGBHist::evalBPmap(cv::Mat &bp_image)
{

    cv::Mat mask;
	cv::inRange(bp_image,10,255,mask);

	cv::Scalar meanVal, devVal;
	cv::meanStdDev(bp_image,meanVal, devVal, mask);

    return meanVal[0]/devVal[0];
}

SingleFaceTrackerDlib::SingleFaceTrackerDlib(boost::shared_ptr<Settings> settings):
		FaceBaseTracker(settings),
		mcBoundingBoxRatio(settings->getFloat("TrackerDlib/boundingBoxRatio",1.2f,false))
{

}

void SingleFaceTrackerDlib::initializeTrackerFeature(boost::shared_ptr<FaceDetectorTarget> dt)
{
	cv::Mat cropped, input_image;
	input_image = mpVideoModule->currentFrame()->image;

	cv::Rect init_rect;
	std::vector<std::vector<cv::Point> > init_contour;
	init_rect = dt->bounding_box;
	init_contour = dt->contours;
	this->rows = input_image.rows;
	this->cols = input_image.cols;

	// convert cv::Mat to dlib image
	dlib::cv_image<dlib::rgb_pixel> dlib_image(input_image);

	// enlarge mTrackBox
	mTrackBox.width = (int)(init_rect.width*mcBoundingBoxRatio);
	mTrackBox.height = (int)(init_rect.height*mcBoundingBoxRatio);
	mTrackBox.x = init_rect.x+(int)(0.5*(1.f-mcBoundingBoxRatio)*(float)init_rect.width);
	mTrackBox.y = init_rect.y+(int)(0.5*(1.f-mcBoundingBoxRatio)*(float)init_rect.height);
	// make sure crop rect is inside image
	mTrackBox = mTrackBox & cv::Rect(0,0,this->cols-1, this->rows-1);

	this->updateSearchWindow();
	// convert tBoundingRect to dlib rect
	mDlibTracker.start_track(dlib_image,
			dlib::drectangle((double)mTrackBox.tl().x,
								(double)mTrackBox.tl().y,
								(double)mTrackBox.br().x,
								(double)mTrackBox.br().y));

}


bool SingleFaceTrackerDlib::updateTrackerFeature(cv::Mat *disp_map,cv::Point &new_center)
{
	cv::Mat input_image = mpVideoModule->currentFrame()->image;

	dlib::cv_image<dlib::rgb_pixel> dlib_image(input_image);
	mConfidence = (float)mDlibTracker.update(dlib_image);
	dlib::drectangle updated_box = mDlibTracker.get_position();

	// update mTrackBox;
	mTrackBox.x = (int)updated_box.left();
	mTrackBox.y = (int)updated_box.top();
	mTrackBox.height = (int)updated_box.height();
	mTrackBox.width = (int)updated_box.width();

	new_center = (mTrackBox.tl() + mTrackBox.br()) / 2;

	//this->updateSearchWindow();	// really not necessary, but for compatibility

	if(mConfidence > 2.f)
	{
		return true;
	}
	else
	{
		return false;
	}

}

void SingleFaceTrackerDlib::drawTrackBox(cv::Mat &disp_image)
{
	cv::rectangle(disp_image,mTrackBox,DLIBCORRELATION_TRACKBOX_COLOR);
}

} // of namespace vml

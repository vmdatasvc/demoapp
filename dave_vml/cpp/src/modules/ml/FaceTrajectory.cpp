/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * FaceTrajectory.cpp
 *
 *  Created on: Aug 3, 2016
 *      Author: dkim
 */

#include <modules/utility/DateTime.hpp>
#include <modules/ml/FaceTrajectory.hpp>
#include <modules/ml/FaceModuleManager.hpp>
#include <modules/ml/FaceVideoModule.hpp>

namespace vml {

float FaceTrajectory::TrajectoryPoint::dist(const FaceTrajectory::TrajectoryPoint other, bool floor_length) const
{
	int dx=0;
	int dy=0;
	if (floor_length)
	{
		// TODO: calculate reverse-projected distance on the floor, using camera calibration.
	}
	else
	{
		dx=loc.x-other.loc.x;
		dy=loc.y-other.loc.y;
	}

	return fabs((float)(dx*dx)+(float)(dy*dy));
}

cv::Point FaceTrajectory::TrajectoryPoint::undistort() const
{
	return FaceModuleManager::instance()->getVideoModule()->getCameraModel()->undistortPoint(loc);
}

/* class FaceTrajectory */
void FaceTrajectory::addTrajectoryPoint(cv::Point loc, double t)
{
	TrajectoryPoint tp;
	tp.loc = loc;
	tp.time = t;
	mTrajectoryPts.push_back(tp);
}

double FaceTrajectory::getStartTime(void) const
{
	if (!mTrajectoryPts.empty())
	{
		return mTrajectoryPts.front().time;
	}
	else
	{
		return -1.f;	// flag invalid time
	}
}

double FaceTrajectory::getEndTime(void) const
{
	if (!mTrajectoryPts.empty())
	{
		return mTrajectoryPts.back().time;
	}
	else
	{
		return -1.f;	// flag invalid time
	}
}

void FaceTrajectory::append(FaceTrajectory &tj)
{
	mTrajectoryPts.insert(mTrajectoryPts.end(),tj.mTrajectoryPts.begin(),tj.mTrajectoryPts.end());
}

void FaceTrajectory::prepend(FaceTrajectory &tj)
{
	std::vector<TrajectoryPoint> temp = mTrajectoryPts;
	mTrajectoryPts = tj.mTrajectoryPts;
	mTrajectoryPts.insert(mTrajectoryPts.end(),temp.begin(),temp.end());
}

float FaceTrajectory::length(bool floor_length) const
{
	// returns the accumulated distance between each nodes.
	// TODO: caculate the distance as on the floor after reverse projection.
	// Currently returns undistorted distance

	if (nodeCount() < 2)
	{
		return 0.f;
	}

	float	acc_dist = 0.f;
	for (unsigned int i=1;i<mTrajectoryPts.size();i++)
	{
		acc_dist += mTrajectoryPts[i].dist(mTrajectoryPts[i-1],floor_length);
	}

	return acc_dist;
}

double FaceTrajectory::duration() const
{
 	// returns the duration of the trajector in sec.
	if (mTrajectoryPts.size() < 2)
	{
		return 0.f;
	}

	return(mTrajectoryPts.back().time - mTrajectoryPts.front().time);
}


std::string FaceTrajectory::toXmlString(unsigned int decimals) const
{
	const double startTime = getStartTime();
	std::stringstream outstr;
	outstr << "<trajectory id='" << mID << "' start_time='" << DateTime(startTime).toString() << "'>";
//	 std::string out = "<trajectory id='" + aitSprintf("%i",getId()) + "' start_time='" +
//	   DateTime(getStartTime()).toString() + "'>";
	std::vector<TrajectoryPoint>::const_iterator iNode;
	bool first = true;
	for (iNode = mTrajectoryPts.begin(); iNode != mTrajectoryPts.end(); iNode++)
	{
		if (!first)
		{
			outstr << ";";
			first = false;
		}
		outstr << std::fixed << std::setprecision(3) << iNode->time - startTime << std::setprecision(decimals) << iNode->loc.x << iNode->loc.y;
	 }
	 outstr << "</trajectory>";
	 return outstr.str();
}

void FaceTrajectory::draw(cv::Mat &disp_image) const
{
	if (mTrajectoryPts.size()< 2)
	{
		return;
	}

	// draw green dot at the beginning, red dot at the end
	cv::circle(disp_image,mTrajectoryPts.front().loc,3,CV_RGB(0,255,0),2);
	for (unsigned int i=1;i<mTrajectoryPts.size();i++)
	{
		cv::line(disp_image,mTrajectoryPts[i].loc,mTrajectoryPts[i-1].loc,CV_RGB(250,250,0)); //CV_RGB(125,0,255));
	}
}

} // of namespace vml


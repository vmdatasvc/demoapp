/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/*																			*/
/****************************************************************************/

/*
 * Recognition.cpp
 *
 *  Created on: Jan 12, 2016
 *      Author: dkim
 */

// Disable MSVC warning
#ifdef _MSC_VER 
#pragma warning(disable:4786)
#pragma warning(disable:4503)
#endif

#include <modules/ml/AITPort/strutil.hpp>
//#include "core/strutil.hpp"
//#include "core/Settings.hpp"
#include <modules/ml/AITPort/DateTime.hpp>

#include "modules/ml/Recognition.hpp"           // Modified by Dave

#include "modules/ml/AITPort/image_proc.hpp"
#include "modules/ml/AITPort/PvImageProc.hpp"
#include "modules/ml/AITPort/IlluminationGradient.hpp"
#include "modules/ml/AITPort/SkinToneMotionDetector.hpp"


#define RAD2DEG 57.29578

//#define VISUAL_FEEDBACK              // By Dave

namespace aitvml
{

Recognition::Recognition():
		imgWidth(60),
		imgHeight(60),
		mNormFaceSizeForSVM(30),
		doGender(1),
		doEthnicity(1),
		doAge(1),
		doNonFaceFilter(0),
		doPose(0),
		mNonFaceThres(1.f),
		m3DFrontalYawThres(20.f),
		m3DFrontalPitchThres(20.f),
		mMin3DFrontalFaceRatio(0.5)
{
	mEstGenderLabel= -1;
	mEstAgeLabel= -1;
	mEstEthnicityLabel= -1;
	mEstGenderScore=0.f;
	mEstAgeScore=0.f;
	mEstAfrScore=0.f;
	mEstCauScore=0.f;
	mEstHisScore=0.f;
	mEstOriScore=0.f;
	mEstGenderString= "Unknown";
	mEstAgeString= "Unknown";
	mEstEthnicityString= "Unknown";

	videoWidth=0;
	videoHeight=0;

	numA3NNModel=0;
	numRefineA3NNModel=0;
	numYPModel=0;

	fpFaceXYWH=NULL;
	initTime = 0.f;
	time=0.f;
}

void Recognition::initialize(boost::shared_ptr<vml::Settings> settings)
{

	  a3nnModelList = settings->getString("DMGRecognition/a3nnModelList","A3NNModelList100L200.txt",false);
	  a3nnModelDir = settings->getString("DMGRecognition/a3nnModelDir", "../../../../data/ml/facial_geometry_estimator/face_2D_XYSO",false);
	  a3nnLsvFile = settings->getString("DMGRecognition/a3nnLsvFile","../../../../data/ml/facial_geometry_estimator/face_2D_XYSO/lsv.save",false);
	  refineA3nnModelList = settings->getString("DMGRecognition/refineA3nnModelList","ModelList50.txt",false);
	  refineA3nnModelDir = settings->getString("DMGRecognition/refineA3nnModelDir","../../../../data/ml/facial_geometry_estimator/face_2D_XYSO_refine",false);
	  refineA3nnLsvFile = settings->getString("DMGRecognition/refineA3nnLsvFile","../../../../data/ml/facial_geometry_estimator/face_2D_XYSO_refine/lsv.save",false);
	  numA3NNModel = settings->getInt("DMGRecognition/numEstA3NNModel",100,false); // 100;

	  // For RVM
	  numRefineA3NNModel = settings->getInt("DMGRecognition/numEstRefineA3NNModel",0,false);
	  mGSvmModelFile = settings->getString("DMGRecognition/gSvmModelFile","../../../../data/ml/svm/vms/RVM-Gender/svs_Gender.txt",false);
	  mAfrSvmModelFile = settings->getString("DMGRecognition/afrSvmModelFile","../../../../data/ml/svm/vms/RVM-ACHO/svs_A.txt",false);
	  mCauSvmModelFile = settings->getString("DMGRecognition/cauSvmModelFile","../../../../data/ml/svm/vms/RVM-ACHO/svs_C.txt",false);
	  mHisSvmModelFile = settings->getString("DMGRecognition/hisSvmModelFile","../../../../data/ml/svm/vms/RVM-ACHO/svs_H.txt",false);
	  mOriSvmModelFile = settings->getString("DMGRecognition/oriSvmModelFile","../../../../data/ml/svm/vms/RVM-ACHO/svs_O.txt",false);
	  mAgeSvmModelFile = settings->getString("DMGRecognition/ageSvmModelFile","../../../../data/ml/svm/vms/RVM-Age/svs.txt",false);

	  mNFSvmModelFile = settings->getString("DMGRecognition/nonFaceSvmModelFile","../../../../data/ml/svm/vms/RVM-NonFaceFilter/svs.txt",false);

	  // Initializes SparseYawPitchNN 3D facial pose
	  imgWidth = 60;
	  imgHeight = 60;

	  ypModelList = settings->getString("DMGRecognition/ypModelList","ModelList.txt",false);
	  ypModelDir= settings->getString("DMGRecognition/ypModelDir","../../../../data/ml/facial_geometry_estimator/face_3D_YawPitch/sparse25/20-10-5-lr0.0075-0.001/",false);
	  ypLsvFile = settings->getString("DMGRecognition/ypModelFile","../../../../data/ml/facial_geometry_estimator/face_3D_YawPitch/lsv5000.save",false);
	  numYPModel = settings->getInt("DMGRecognition/nYPModel",9,false);

	  doPose = settings->getInt("DMGRecognition/doPose",0,false);
	  doGender = settings->getInt("DMGRecognition/doGender",1,false);
	  doAge = settings->getInt("DMGRecognition/doAge",1,false);
	  doEthnicity = settings->getInt("DMGRecognition/doEthnicity",1,false);
	  doNonFaceFilter = settings->getInt("DMGRecognition/doNonFaceFilter",0,false);
	  //showSkinToneWindow = 0;

	  m3DFrontalYawThres = settings->getFloat("DMGRecognition/3DFrontalYawThres",20.0,false);
	  m3DFrontalPitchThres = settings->getFloat("DMGRecognition/3DFrontalPitchThres",20.0,false);
	  mMin3DFrontalFaceRatio = settings->getFloat("DMGRecognition/Min3DFrontalFaceRatio",0.5,false);
	  mNormFaceSizeForSVM = settings->getInt("DMGRecognition/NormFaceSizeForSVM",30,false);

	  mNonFaceThres =  settings->getFloat("DMGRecognition/nonFaceThres",1.0,false);

	  initFromFace();
}

Recognition::~Recognition()
{
}

void Recognition::finish()
{

	//mpFront.reset();

 }
void Recognition::initFromFace()
{

  videoWidth = faceImage.width();
  videoHeight = faceImage.height();

  // Initializes Arclet3DNN face localization
  mA3NN.init(a3nnModelList, a3nnModelDir, a3nnLsvFile);
  mRefineA3NN.init(refineA3nnModelList,refineA3nnModelDir,refineA3nnLsvFile);

  if(doPose)
  {
    mYPNN.init(ypModelList, ypModelDir, ypLsvFile, imgWidth, imgHeight);
  }

  m3DFrontalYawThres = m3DFrontalYawThres/RAD2DEG;
  m3DFrontalPitchThres = m3DFrontalPitchThres/RAD2DEG;

  // ====   Normalized size ( 30 by 30 ) for SVM classifiers   =========='
  if(doGender)
  {
    mGSvm.init(mGSvmModelFile, mNormFaceSizeForSVM, mNormFaceSizeForSVM);
  }

  if(doEthnicity)
  {
    mAfrSvm.init(mAfrSvmModelFile, mNormFaceSizeForSVM, mNormFaceSizeForSVM);
    mCauSvm.init(mCauSvmModelFile, mNormFaceSizeForSVM, mNormFaceSizeForSVM);
    mHisSvm.init(mHisSvmModelFile, mNormFaceSizeForSVM, mNormFaceSizeForSVM);
    mOriSvm.init(mOriSvmModelFile, mNormFaceSizeForSVM, mNormFaceSizeForSVM);
  }

  if(doAge)
  {
#ifdef DEMOGRAPHICS_RECOGNITION_DEBUG
    PVMSG("Age file: %s\n",mAgeSvmModelFile.c_str());
#endif
    mAgeSvm.init(mAgeSvmModelFile, mNormFaceSizeForSVM, mNormFaceSizeForSVM);
  }

  if(doNonFaceFilter)
  {
#ifdef DEMOGRAPHICS_RECOGNITION_DEBUG
    PVMSG("NonFaceFilter file: %s\n",mNFSvmModelFile.c_str());
#endif
    mNonFaceSvm.init(mNFSvmModelFile, mNormFaceSizeForSVM, mNormFaceSizeForSVM);
  }

}

bool Recognition::nonFaceFiltering(cv::Mat faceMat)
{
	float mNonFaceThres = 1.0;

	aitvml::Image8 img;
	cvtMatToImage8(faceMat, &img);
	float confidence = mNonFaceSvm.classify(img);

	if (confidence < mNonFaceThres)
	{
		return true;
	}
	else {
		return false;
	}

}

void Recognition::processFromFace()
{

	Image32 frame;

	frame = faceImage;


	Image8 grayFrame;
  
	PvImageConverter::convert(frame,grayFrame);

  //PvImageProc::EqualizeImage(grayFrame);   // not good
  
   std::vector<FaceDataPtr> faces;

   // The Classs Face is just a bounding box.
   Face f;

   f.w = frame.width();
   f.h = frame.height();
   f.x = 0;
   f.y = 0;

   int fId =0;

   FaceDataPtr pFace(new FaceData(fId++,0,time,(int)(f.x),(int)(f.y), (int)(f.w),(int)(f.h)));

   //float mUpscaleFactor = 1.0f;
   //FaceDataPtr pFace(new FaceData(fId++,0,time,(int)((float)f.x/mUpscaleFactor),(int)((float)f.y/mUpscaleFactor),
	//  (int)((float)f.w/mUpscaleFactor),(int)((float)f.h/mUpscaleFactor)));

   //if (f.w >= mMinFaceClassificationSize.x && f.h >= mMinFaceClassificationSize.y)
   //{

	  localizeFace(grayFrame, pFace);

	  if(doPose)
	  {
		  poseEstimateFace(grayFrame, pFace);
		  //PVMSG("Detected face: %d %d %d %d pose %f %f ",f.x,f.y,f.w,f.h,RAD2DEG*(pFace->yw),RAD2DEG*(pFace->pt));

		  if(pFace->is3DFrontal)
		  {
			  faces.push_back(pFace);
//			  PVMSG(" --> Frontal!!");
		  }
	  }
	  else
	  {
		  faces.push_back(pFace);
	  }
//	  PVMSG("\n");
  //}

   	classifyFaceSingle(grayFrame, pFace);

	mEstGenderLabel = pFace->iGender;
	mEstAgeLabel = pFace->iAge;
	mEstEthnicityLabel = pFace->iEthnicity;

	mEstGenderString = pFace->iGenderString;
	mEstAgeString = pFace->iAgeString;
	mEstEthnicityString = pFace->iEthnicityString;

	mEstGenderScore = pFace->mGenderScore;
	mEstAgeScore = pFace->mAge;
	mEstAfrScore = pFace->mAfrScore;
	mEstCauScore = pFace->mCauScore;
	mEstHisScore = pFace->mHisScore;
	mEstOriScore = pFace->mOriScore;

#ifdef	DEMOGRAPHICS_RECOGNITION_DEBUG
	printf("------ Estimated result ------");
	if (doGender)
		printf("\nGender = %d(%s), score = %f", mEstGenderLabel, mEstGenderString.c_str(), mEstGenderScore);
	if (doAge)
		printf("\nAge = %d(%s), score = %f", mEstAgeLabel, mEstAgeString.c_str(), mEstAgeScore);
	if (doEthnicity)
		printf("\nEthnicity = %d(%s), score = (%f, %f, %f, %f)\n", mEstEthnicityLabel, mEstEthnicityString.c_str(), mEstAfrScore, mEstCauScore, mEstHisScore, mEstOriScore);
#endif

}

void Recognition::localizeFace(Image8 grayFrame, FaceDataPtr pFace)
{
  int x = pFace->x;
  int y = pFace->y;
  int w = pFace->w;
  int h = pFace->h;
  
  mA3NN.estimateModel(numA3NNModel, 2, 2, grayFrame, x, y, w, h);

  // Correct the bias introduced by the models
  mA3NN.mEstMidEyeMouthDist -= 0.5f;
  mA3NN.mEstOrientation += 0.024f;
  

//      Image8 a3NNChip(30,30);
  
  mRefineA3NN.estimateRefined(numRefineA3NNModel, grayFrame, pFace->mFaceChip, 
  (2*x+w)/2 + mA3NN.mEstMidEyeX - 30.0,(2*y+h)/2 + mA3NN.mEstMidEyeY - 30.0,
  mA3NN.mEstMidEyeMouthDist*(w/30.0),mA3NN.mEstOrientation);

  // Correct the bias introduced by the models
  mRefineA3NN.mEstMidEyeMouthDist -= 0.5f;
  mRefineA3NN.mEstOrientation += 0.02f;

  (*pFace).ex = mA3NN.mEstMidEyeX - 30.0 + mRefineA3NN.mEstMidEyeX;
  (*pFace).ey = mA3NN.mEstMidEyeY - 30.0 + mRefineA3NN.mEstMidEyeY;
  (*pFace).sz = mA3NN.mEstMidEyeMouthDist + mRefineA3NN.mEstMidEyeMouthDist - 12.5;
  (*pFace).oR = mA3NN.mEstOrientation + mRefineA3NN.mEstOrientation;


  vision::IlluminationGradient illGraident;
  Image8 temp(pFace->mFaceChip);
  illGraident.process(pFace->mFaceChip, temp);

  // Histogram equalizer
  PvImageProc::EqualizeImage(pFace->mFaceChip);
  

/*
#ifdef VISUAL_FEEDBACK
    OpenCvUtils locu;

	fImg1 = locu.convertPvImageToIpl(pFace->mFaceChip);

	cvNamedWindow("2D Normalized face", 0); //CV_WINDOW_AUTOSIZE);
	cvShowImage("2D Normalized face", fImg1);
	cvMoveWindow("2D Normalized face",1330,535);
	//cvWaitKey(0);

	// Save image
	pFace->mFaceChip.save("Norm2DFaceData.pgm"); 
#endif
*/

//  Image8 temp(pFace->mFaceChip);
//  pFace->mFaceChip.Lequalize(temp, 12);
}

void Recognition::poseEstimateFace(Image8 grayFrame, FaceDataPtr pFace)
{
  int x = pFace->x;
  int y = pFace->y;
  int w = pFace->w;
  int h = pFace->h;  
  float eX = (2*x + w)/2 + (*pFace).ex - 30.0;
  float eY = (2*y + h)/2 + (*pFace).ey - 30.0;
  float sZ = ((*pFace).sz)*(w/(float)30.0);
  float oR = (*pFace).oR;
  
  Image8 ypChip(30,30);
  mYPNN.estimateYawPitch(numYPModel, grayFrame, ypChip, eX, eY, sZ, oR);
  (*pFace).yw = mYPNN.mEstYaw;
  (*pFace).pt = mYPNN.mEstPitch;
  
  //float poseWeight = cos(2*(*pFace).yw)*cos(2*(*pFace).pt);

//  PVMSG("%f < %f ? %f < %f ",RAD2DEG*(*pFace).yw,RAD2DEG*m3DFrontalYawThres,RAD2DEG*(*pFace).pt,RAD2DEG*m3DFrontalPitchThres);

  if( fabs((*pFace).yw) < m3DFrontalYawThres && fabs((*pFace).pt) < m3DFrontalPitchThres)
  {
	  (*pFace).is3DFrontal = 1;
  }
  else
  {	
	  (*pFace).is3DFrontal = 0;
  }

  /*
#ifdef VISUAL_FEEDBACK
  //ypChip.save("Nomr3DFaceData.pgm");
  OpenCvUtils locu;

  fImg2 = locu.convertPvImageToIpl(ypChip);


//  int cx, cy, ux, uy, lx, ly;
//  cx = (int)(ypChip.width()/2);
// cy = (int)(ypChip.height())/2;

//  ux = (int)ypChip.width()/180;
//  uy = (int)ypChip.width()/180;

//  lx = mYPNN.mEstYaw*RAD2DEG * ux;
//  ly = mYPNN.mEstPitch*RAD2DEG * uy;

//  cvLine(fImg2, cvPoint(cx,cy), cvPoint(lx,ly), CV_RGB(255,0,0),1,8, 0);

//  cvNamedWindow("3D Normalized face", 0);
//  cvShowImage("3D Normalized face",fImg2);
//  cvMoveWindow("3D Normalized face",1015,850);
  //cvWaitKey(0);

  printf("\n----------------------------------------------------------------------------", eX, eY, sZ, oR*RAD2DEG, mYPNN.mEstYaw*RAD2DEG, mYPNN.mEstPitch*RAD2DEG);
  printf("\n3D Normalized Face Info:  eX=%f, eY=%f, sZ=%f, oR=%f, yaw=%f, pitch=%f\n", eX, eY, sZ, oR*RAD2DEG, mYPNN.mEstYaw*RAD2DEG, mYPNN.mEstPitch*RAD2DEG);
  printf("------------------------------------------------------------------------------\n", eX, eY, sZ, oR*RAD2DEG, mYPNN.mEstYaw*RAD2DEG, mYPNN.mEstPitch*RAD2DEG);

#endif
*/

}

void Recognition::classifyFaceSingle(Image8 grayFrame, FaceDataPtr pFace)
{

  //int x, y, w, h;
  //, eX, eY, sZ, oR;
//  if(!mClassifyAppearanceModelOnly)
//  {
    //x = pFace->x;
    //y = pFace->y;
    //w = pFace->w;
    //h = pFace->h;
    //eX = (2*x + w)/2 + (*pFace).ex - 30.0;
    //eY = (2*y + h)/2 + (*pFace).ey - 30.0;
    //sZ = ((*pFace).sz)*(w/(float)30.0);
    //oR = (*pFace).oR;
//  }
  
  //mClassifyAppearanceModelOnly =1;  To check up the effect of the mode
  float genderScore;
  float genderTh = 0.1;   // Added by Dave
  //(*pFace).iGender = 2*(genderScore>genderTh)-1;			// Modified by Dave
  string gender;

  if(doGender)
  {
	  genderScore = mGSvm.classify(pFace->mFaceChip);

	  if(genderScore < genderTh)		// Modified by Dave
	  {
	    gender = "Female";
		(*pFace).iGender = 0;
	  }
	  else
	  {
	    gender = "Male";
		(*pFace).iGender = 1;
	  }
  }
  else
  {
    genderScore = -10.0f;
	(*pFace).iGender = -1;
	gender = "Unknown";
  }
  (*pFace).iGenderString = gender;

  
  float ageScore;
  string age;

  if(doAge)
  {
	 ageScore = mAgeSvm.classify(pFace->mFaceChip);

	  if(ageScore < 18)
	  {
	    age = "Child";
	    (*pFace).iAge = 0;
	  }
	  if(18 <= ageScore && ageScore < 30)
	  {
	    age = "Young Adult";
	    (*pFace).iAge = 1;
	  }
	  if(30 <= ageScore && ageScore < 55)
	  {
	    age = "Adult";
	    (*pFace).iAge = 2;
	  }
	  if(55 <= ageScore)
	  {
	    age = "Senior";
	    (*pFace).iAge = 3;
	  }
  }
  else
  {
    ageScore = -10.0f;
    age = "Unknown";
    (*pFace).iAge = -1;
  }
  (*pFace).iAgeString = age;

  
  float afrScore, cauScore, hisScore, oriScore;
  string ethnicity;
  
  if(doEthnicity)
  {

	  afrScore = mAfrSvm.classify(pFace->mFaceChip);
	  cauScore = mCauSvm.classify(pFace->mFaceChip);
	  hisScore = mHisSvm.classify(pFace->mFaceChip);
	  oriScore = mOriSvm.classify(pFace->mFaceChip);

	  if(afrScore >= cauScore && afrScore >= hisScore && afrScore >= oriScore)
	  {
	    ethnicity = "African";
	    (*pFace).iEthnicity = 0;
	  }
	  if(cauScore >= afrScore && cauScore >= hisScore && cauScore >= oriScore)
	  {
	    ethnicity = "Caucasian";
	    (*pFace).iEthnicity = 1;
	  }
	  if(hisScore >= afrScore && hisScore >= cauScore && hisScore >= oriScore)
	  {
	    ethnicity = "Hispanic";
	    (*pFace).iEthnicity = 2;
	  }
	  if(oriScore >= afrScore && oriScore >= cauScore && oriScore >= hisScore)
	  {
	    ethnicity = "Oriental";
	    (*pFace).iEthnicity = 3;
	  }
  }
  else
  {
    afrScore = -10.0;
    cauScore = -10.0;
    hisScore = -10.0;
    oriScore = -10.0;

    ethnicity = "Unknown";
    (*pFace).iEthnicity = -1;
  }
  (*pFace).iEthnicityString = ethnicity;
  
  

/*
#ifdef VISUAL_FEEDBACK

  pFace->mFaceChip.save("inputDR.pgm");
  OpenCvUtils locu;

  fImg3 = locu.convertPvImageToIpl(faceChip);

  cvNamedWindow("Demographics Rec. Input", 0);	
  cvShowImage("Demographics Rec. Input",fImg3);

  printf("Doing ClassifyFace...\n");
  //cvWaitKey(0);
#endif
*/
  // FaceChip Image
  tmpImg = pFace->mFaceChip;

  (*pFace).mGenderScore = genderScore;
  (*pFace).mAfrScore = afrScore;
  (*pFace).mCauScore = cauScore;
  (*pFace).mHisScore = hisScore;
  (*pFace).mOriScore = oriScore;
  (*pFace).mAge = ageScore;


}

void Recognition::cvtMatToImage32(cv::Mat imat, aitvml::Image32 *img)
{
       int w = imat.cols;
       int h = imat.rows;

       (*img) = aitvml::Image32(w,h);
/*
       cv::Mat tmpMat =  cv::Mat(h,w,CV_8UC4);


       if(img->getInternalFormat() == 0)
       {
           cv::cvtColor(imat,tmpMat, CV_RGB2BGRA);
       }
       else
       {
           cv::cvtColor(imat,tmpMat, CV_RGB2RGBA);
       }


       for(int y=0; y<h; y++)
       {
           unsigned char *pSrcData = ((unsigned char *)tmpMat.data) + y*w*4;
           unsigned char *pDstData = ((unsigned char *)img->pointer()) + y*w*4;

           for(int x=0; x<w; x++)
           {
                memcpy(pSrcData, pDstData, sizeof(unsigned char)*4);
           }
       }


//       memcpy((unsigned char *)tmpMat.data, (unsigned char *)img->pointer(), sizeof(unsigned char)*w*h*4);


*/
       for(int y=0; y<h; y++)
       {
           unsigned char *pSrcData = ((unsigned char *)imat.data) + y*w*3;
           unsigned char *pDstData = ((unsigned char *)img->pointer()) + y*w*4;

           for(int x=0; x<w; x++)
           {
               if(img->getInternalFormat() == 0)
               {
                    pDstData[0] = pSrcData[2];
                    pDstData[1] = pSrcData[1];
                    pDstData[2] = pSrcData[0];
               }
               else
               {
                   pDstData[0] = pSrcData[0];
                   pDstData[1] = pSrcData[1];
                   pDstData[2] = pSrcData[2];
               }
               pSrcData += 3;
               pDstData += 4;
            }
       }


}

void Recognition::cvtImage32ToMat(aitvml::Image32 img, cv::Mat *imat)
{

    int w = img.width();
    int h = img.height();


    cv::Mat tmpMat =  cv::Mat(h,w,CV_8UC4, (void*)img.pointer());

    if(img.getInternalFormat() == 0)
    {
        cv::cvtColor(tmpMat, (*imat), CV_BGRA2RGB);
    }
    else
    {
        cv::cvtColor(tmpMat, (*imat), CV_BGRA2BGR);
    }


/*
    (*imat) = cv::Mat(h,w,CV_8UC3);

    for(int y=0; y<h; y++)
    {
        unsigned char *pSrcData = (unsigned char *)img.pointer() + y*w*4;
        unsigned char *pDstData = (unsigned char *)imat->data + y*w*3;

        for(int x=0; x<w; x++)
        {
            if(img.getInternalFormat() == 0)
            {
                 pDstData[0] = pSrcData[2];
                 pDstData[1] = pSrcData[1];
                 pDstData[2] = pSrcData[0];
            }
            else
            {
                pDstData[0] = pSrcData[0];
                pDstData[1] = pSrcData[1];
                pDstData[2] = pSrcData[2];
            }
            pSrcData += 4;
            pDstData += 3;
         }
    }
*/

}

void Recognition::cvtMatToImage8(cv::Mat imat, aitvml::Image8 *img)
{
    int w = imat.cols;
    int h = imat.rows;

    (*img) = aitvml::Image8(w,h);


    for(int y=0; y<h; y++)
    {
        unsigned char *pSrcData = ((unsigned char *)imat.data) + y*w;
        unsigned char *pDstData = ((unsigned char *)img->pointer()) + y*w;

        for(int x=0; x<w; x++)
        {
            pDstData[0] = pSrcData[0];
            pSrcData ++;
            pDstData ++;
         }
    }

}

void Recognition::cvtImage8ToMat(aitvml::Image8 img, cv::Mat *imat)
{

    int w = img.width();
    int h = img.height();

    (*imat) = cv::Mat(h,w,CV_8UC1);

    for(int y=0; y<h; y++)
    {
        unsigned char *pSrcData = (unsigned char *)img.pointer() + y*w;
        unsigned char *pDstData = (unsigned char *)imat->data + y*w;

        for(int x=0; x<w; x++)
        {
            pDstData[0] = pSrcData[0];
            pSrcData ++;
            pDstData ++;
         }
    }

}


}; // namespace vml


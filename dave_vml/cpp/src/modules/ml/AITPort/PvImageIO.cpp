/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include <png.h>
#include "modules/ml/AITPort/PvUtil.hpp"
#include "modules/ml/AITPort/PvImageIO.hpp"
#include "modules/ml/AITPort/PvImageArcDefs.hpp"

extern "C"
{
#include <stdio.h>
#undef FAR
#include <jpeglib.h>
}

namespace aitvml
{

/************************************************************************
 ** 
 ** Image IO functions
 **
 ************************************************************************/

/*********************************************************************
 * it_loadpgm()
 * 
 * Load image in raw pgm format.
 *
 * <input> 
 * 
 * char *name            : file name 
 * unsigned char **image : pointer to a place where the pointer to the
 *                         loaded image can be stored 
 * unsigned int *width   : pointer to storage place for width
 * unsigned int *height  : pointer to storage place for height
 *
 * <output>
 *
 * unsigned char *: returns pointer to image, NULL if failure.
 *
 * <error handling>
 * 
 * The function prints error messages.
 *
 *********************************************************************/
unsigned char *it_loadpgm(const char *name,unsigned char **image,
			  unsigned int *width,unsigned int *height)
{
  FILE *fid;
  unsigned int intense;
  int status;
  char header[256],*ptr;

  fid=fopen(name,"rb");
  if(fid==NULL)
    {
      PvUtil::exitError("it_loadpgm '%s'",name);
      return(NULL);
    }
  status=3;
  while(status)
    {
      fgets(header,1000,fid);  // P5
      if(header[0]!='#')
	{
	  switch(status)
	    {
	    case 1:
	      intense=atoi(header);
	      break;
	    case 2:
	      *width=strtol(header,&ptr,0);
	      *height=atoi(ptr);
	      break;
	    case 3:
	      if(header[0]!='P' || header[1] !='5')
		{
		  PVMSG("Don't recognize image format!\n");
		  fclose(fid);
		  return(NULL);
		}
	    }
	  status--;
	}
    }
  *image= new unsigned char [(*width)*(*height)];
  if(*image==NULL)
    {
      PvUtil::exitError("it_loadpgm '%s'",name);
    }
  else
    {
      intense=fread(*image,1,*width*(*height),fid);
      if(intense!=(*width)*(*height))
	{
	  PVMSG("it_loadpgm: expected %d bytes, found %d\n",*width*(*height),intense);
	}
    }
  fclose(fid);
  //PVMSG("it_loadpgm: loaded image of size %dx%d!\n",*width,*height);
  return(*image);
}

/*********************************************************************
 * it_savepgm()
 * 
 * Save image in raw pgm format.
 *
 * <input> 
 * 
 * char *name           : file name 
 * unsigned char *image : pointer to image
 * unsigned int width   : width of image
 * unsigned int height  : height of image
 *
 * <output>
 *
 * int: returns'true' if success, 'false' if failure.
 *
 * <error handling>
 * 
 * The function prints error messages.
 *
 *********************************************************************/
int it_savepgm(const char *name,unsigned char *image,
	       unsigned int width,unsigned int height)
{
  unsigned int len;
  FILE *fid;

  fid=fopen(name,"wb");
  if(fid==NULL)
    {
      PvUtil::error("it_savepgm");
      return 0;
    }
  fprintf(fid,"P5\n# File created by 'imagetools' (NOK 1999)\n%d %d\n%d\n",width,height,255);
  PVMSG("saving '%s' of width %d and height %d\n",name,width,height);
  len=fwrite(image,1,width*height,fid);
  if(len!=width*height)
    {
      PvUtil::error("it_savepgm");
    }
  fclose(fid);
  return(1);
}

/*********************************************************************
 * it_loadpgm()
 * 
 * Load image in raw ppm format.
 *
 * <input> 
 * 
 * char *name           : file name 
 * unsigned int **image : pointer to a place where the pointer to the
 *                        loaded image can be stored 
 * unsigned int *width  : pointer to storage place for width
 * unsigned int *height : pointer to storage place for height
 *
 * <output>
 *
 * unsigned int *: Returns pointer to image, NULL if failure.
 *
 * <error handling>
 * 
 * The function prints error messages.
 *
 *********************************************************************/
unsigned int *it_loadppm(const char *name,unsigned int **image,
			 unsigned int *width,unsigned int *height)
{
  unsigned int i,j;
  FILE *fid;
  unsigned int intense;
  int status;
  char header[256],*ptr;
  unsigned char *pixels;
  
  fid=fopen(name,"rb");
  if(fid==NULL)
  {
    PvUtil::exitError("it_loadppm '%s'",name);
    return(NULL);
  }
  status=3;
  while(status)
  {
    fgets(header,1000,fid);  // P6
    if(header[0]!='#')
    {
      switch(status)
      {
      case 1:
        intense=atoi(header);
        break;
      case 2:
        *width=strtol(header,&ptr,0);
        *height=atoi(ptr);
        break;
      case 3:
        if(header[0]!='P' || header[1] !='6')
        {
          PVMSG("Don't recognize image format!\n");
          fclose(fid);
          return(NULL);
        }
      }
      status--;
    }
    //else printf("%s",header);
  }
  pixels=new unsigned char [(*width)*(*height)*3];
  if(pixels==NULL)
  {
    PvUtil::exitError("it_loadppm '%s'",name);
  }
  else
  {
    *image=new unsigned int [(*width)*(*height)];
    if(*image==NULL)
    {
      delete [] pixels;
      PvUtil::exitError("it_loadppm '%s'",name);
    }
    else
    {
      intense=fread(pixels,3,(*width)*(*height),fid);
      if(intense!=(*width)*(*height))
      {
        PvUtil::exitError("it_loadppm: expected %d pixel, found %d",*width*(*height),intense);
      }
      else
      {
        j=0;
        for(i=0;i<(*width)*(*height)*3;i+=3)
        {
          (*image)[j++]=PV_RGB(pixels[i],pixels[i+1],pixels[i+2]);
        }
      }
    }
  }
  delete [] pixels;
  fclose(fid);
  PVMSG("it_loadppm: loaded image of size %dx%d!\n",*width,*height);
  return(*image);
}

/*********************************************************************
 * it_saveppm()
 * 
 * Save image in raw ppm format.
 *
 * <input> 
 * 
 * char *name           : file name 
 * unsigned int *image  : pointer to image
 * unsigned int width   : width of image
 * unsigned int height  : height of image
 *
 * <output>
 *
 * int: Returns 'true' if success, 'false' if failure.
 *
 * <error handling>
 * 
 * The function prints error messages.
 *
 *********************************************************************/
int it_saveppm(const char *name,unsigned int *image,
	       unsigned int width,unsigned int height)
{
  unsigned int i;
  unsigned char rgb[3];
  int len;
  FILE *fid;

  fid=fopen(name,"wb");
  if(fid==NULL)
    {
      PvUtil::error("it_saveppm %s",name);
      return(0);  // arc:  Modified this to return 0 instead of NULL since the routine is expecting an integer as a return value
    }
  fprintf(fid,"P6\n# File created by 'imagetools' (NOK 1999)\n%d %d\n%d\n",width,height,255);
  PVMSG("saving '%s' of width %d and height %d\n",name,width,height);
  for(i=0;i<width*height;i++)
    {
      rgb[2]=BLUE(image[i]);
      rgb[1]=GREEN(image[i]);
      rgb[0]=RED(image[i]);
      len=fwrite(rgb,3,1,fid);
      if(len!=1)
	{
	  PvUtil::error("it_saveppm %s",name);
	}
    }
  fclose(fid);
  return(1);
}

/*********************************************************************
 * it_loadpng()
 * 
 * Load an RGB or ARGB image from png format.
 *
 * <input> 
 * 
 * char *name           : file name 
 * unsigned int **image : pointer to a place where the pointer to the
 *                        loaded image can be stored 
 * unsigned int *width  : pointer to storage place for width
 * unsigned int *height : pointer to storage place for height
 * bool *alpha          : set to true if the image has an alpha channel
 *
 * <output>
 *
 * unsigned int *: Returns pointer to image, NULL if failure.
 *
 * <error handling>
 * 
 * The function prints error messages.
 *
 *********************************************************************/
#define PNG_BYTES_TO_CHECK 4

unsigned int *it_loadpng(const char *name,unsigned int **image,
			 unsigned int *width0,unsigned int *height0,bool *alpha)
{
  FILE *fid;
  unsigned char *pixels;

  try
  {

    // PNG Variables
    png_structp png_ptr;
    png_infop info_ptr;
    unsigned int sig_read = 0;
    png_uint_32 width, height;
    int bit_depth, color_type, interlace_type;

    unsigned char buf[PNG_BYTES_TO_CHECK];

    // Open the prospective PNG file.
    if ((fid = fopen(name, "rb")) == NULL)
    {
      throw 0;
    }
    // Read in some of the signature bytes
    if (fread(buf, 1, PNG_BYTES_TO_CHECK, fid) != PNG_BYTES_TO_CHECK)
    {
      throw 0;
    }

    // Compare the first PNG_BYTES_TO_CHECK bytes of the signature.
    // Return nonzero (true) if they match

    if (png_sig_cmp(buf, (png_size_t)0, PNG_BYTES_TO_CHECK)) {
      throw 0;
    }

    fseek(fid,0,SEEK_SET);

    // Create and initialize the png_struct with the desired error handler
    // functions.  If you want to use the default stderr and longjump method,
    // you can supply NULL for the last three parameters.  We also supply the
    // the compiler header file version, so that we know if the application
    // was compiled with a compatible version of the library.  REQUIRED

    png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING,
                                     NULL, 
                                     NULL, 
                                     NULL);

    if (!png_ptr) throw 0;

    // Allocate/initialize the memory for image information.  REQUIRED.
    info_ptr = png_create_info_struct(png_ptr);

    if (!info_ptr) throw 0;

    // Set error handling if you are using the setjmp/longjmp method (this is
    // the normal method of doing things with libpng).  REQUIRED unless you
    // set up your own error handlers in the png_create_read_struct() earlier.

    if (setjmp(png_jmpbuf(png_ptr)))
    {
      throw 0;
    }

    // Set up the input control if you are using standard C streams
    png_init_io(png_ptr, fid);

    // If we have already read some of the signature
    png_set_sig_bytes(png_ptr, sig_read);

    // The call to png_read_info() gives us all of the information from the
    // PNG file before the first IDAT (image data chunk).  REQUIRED
    png_read_info(png_ptr, info_ptr);

    png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth, &color_type,
                 &interlace_type, NULL, NULL);

    // tell libpng to strip 16 bit/color files down to 8 bits/color
    png_set_strip_16(png_ptr);

    // Extract multiple pixels with bit depths of 1, 2, and 4 from a single
    // byte into separate bytes (useful for paletted and grayscale images).

    png_set_packing(png_ptr);

    // Expand paletted colors into true RGB triplets
    if (color_type == PNG_COLOR_TYPE_PALETTE)
      png_set_expand(png_ptr);

    // Expand grayscale images to the full 8 bits from 1, 2, or 4 bits/pixel
    if (color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
      png_set_expand(png_ptr);

    // Expand paletted or RGB images with transparency to full alpha channels
    // so the data will be available as RGBA quartets.
    if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS))
        png_set_expand(png_ptr);

    // flip the RGB pixels to BGR (or RGBA to BGRA)
    if (color_type & PNG_COLOR_MASK_COLOR) png_set_bgr(png_ptr);

    if (color_type == PNG_COLOR_TYPE_RGB_ALPHA) {
      // swap the RGBA or GA data to ARGB or AG (or BGRA to ABGR)
      //png_set_swap_alpha(png_ptr);
    } else {
      // Add filler (or alpha) byte (before/after each RGB triplet)
      png_set_filler(png_ptr, 0x00, PNG_FILLER_AFTER);
    }

    // Allocate the memory to hold the image using the fields of info_ptr.

    unsigned char **row_pointers = new unsigned char *[height];
    unsigned int rowbytes = width*4;
    pixels = new unsigned char [rowbytes*height];

    for (unsigned int row = 0; row < height; row++) {
       row_pointers[row] = &pixels[rowbytes*row];
    }

    // Read the entire image in one go
    png_read_image(png_ptr, row_pointers);

    // Delete the row pointer array
    delete row_pointers;

    // read rest of file, and get additional chunks in info_ptr - REQUIRED
    png_read_end(png_ptr, info_ptr);

    // At this point you have read the entire image

    *width0 = width;
    *height0 = height;
    *image = (unsigned int *)pixels;
    *alpha = (png_get_channels(png_ptr, info_ptr) == 4);

    // clean up after the read, and free any memory allocated - REQUIRED
    png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp)NULL);

    // close the file
    fclose(fid);

    return (unsigned int *)pixels;
  }
  catch (...)
  {
    PvUtil::exitError("it_loadpng '%s'",name);
  }

  return NULL;
}

/*********************************************************************
 * it_savepng()
 * 
 * Save image in PGN RGB or RGBA format.
 *
 * <input> 
 * 
 * char *name           : file name 
 * unsigned int *image  : pointer to image
 * unsigned int width   : width of image
 * unsigned int height  : height of image
 * bool alpha           : alpha channel flag
 *
 * <output>
 *
 * int: Returns 'true' if success, 'false' if failure.
 *
 * <error handling>
 * 
 * The function prints error messages.
 *
 *********************************************************************/
int it_savepng(const char *name,unsigned int *image,
	       unsigned int width, unsigned int height, bool alpha)
{
  try
  {

    FILE *fp;
    png_structp png_ptr;
    png_infop info_ptr;

    // open the file
    fp = fopen(name, "wb");
    if (fp == NULL) throw 0;

    // Create and initialize the png_struct with the desired error handler
    // functions.
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING,NULL,NULL,NULL);

    if (!png_ptr) throw 0;

    // Allocate/initialize the image information data.  REQUIRED
    info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr) throw 0;

    // Set error handling.  REQUIRED if you aren't supplying your own
    // error handling functions in the png_create_write_struct() call.
    if (setjmp(png_jmpbuf(png_ptr))) throw 0;

    // set up the output control if you are using standard C streams
    png_init_io(png_ptr, fp);

    // Set the image information here.  Width and height are up to 2^31,
    // bit_depth is one of 1, 2, 4, 8, or 16, but valid values also depend on
    // the color_type selected. color_type is one of PNG_COLOR_TYPE_GRAY,
    // PNG_COLOR_TYPE_GRAY_ALPHA, PNG_COLOR_TYPE_PALETTE, PNG_COLOR_TYPE_RGB,
    // or PNG_COLOR_TYPE_RGB_ALPHA.  interlace is either PNG_INTERLACE_NONE or
    // PNG_INTERLACE_ADAM7, and the compression_type and filter_type MUST
    // currently be PNG_COMPRESSION_TYPE_BASE and PNG_FILTER_TYPE_BASE. REQUIRED

    png_set_IHDR(png_ptr, info_ptr, width, height, 
                 8, // bit depth
                 alpha ? PNG_COLOR_TYPE_RGB_ALPHA : PNG_COLOR_TYPE_RGB,
                 PNG_INTERLACE_NONE, 
                 PNG_COMPRESSION_TYPE_BASE, 
                 PNG_FILTER_TYPE_BASE);

    // otherwise, if we are dealing with a color image then
    /*
    png_color_16 sig_bit;
    sig_bit.red = 8;
    sig_bit.green = 8;
    sig_bit.blue = 8;
    // if the image has an alpha channel then
    sig_bit.alpha = alpha ? 8 : 0;
    png_set_sBIT(png_ptr, info_ptr, sig_bit);
    */


    // Optional gamma chunk is strongly suggested if you have any guess
    // as to the correct gamma of the image.
    //png_set_gAMA(png_ptr, info_ptr, gamma);

    // Write the file header information.  REQUIRED
    png_write_info(png_ptr, info_ptr);

    // flip BGR pixels to RGB
    png_set_bgr(png_ptr);

    // set filler
    if (!alpha) {
      png_set_filler(png_ptr, 0, PNG_FILLER_AFTER);
    }

    // Write the image
    unsigned char **row_pointers = new unsigned char *[height];
    unsigned int rowbytes = width*4;
    png_byte *pixels = (png_byte *)image;

    for (unsigned int row = 0; row < height; row++) {
       row_pointers[row] = &pixels[rowbytes*row];
    }

    // write out the entire image data in one call
    png_write_image(png_ptr, row_pointers);

    // It is REQUIRED to call this to finish writing the rest of the file
    png_write_end(png_ptr, info_ptr);

    // clean up after the write, and free any memory allocated
    png_destroy_write_struct(&png_ptr, &info_ptr);

    delete [] row_pointers;

    // close the file
    fclose(fp);

    // that's it
    return (1);
  }
  catch (...)
  {
    PvUtil::error("Error creating file [%s]\n",name);
  }

  return 0;  // arc:  changed this to return 0 instead of NULL since the function wants an int as a return value
}

/*********************************************************************
 * it_savejpg()
 * 
 * Save image in jpg format.
 *
 * <input> 
 * 
 * char *name           : file name 
 * unsigned int *image  : pointer to image
 * unsigned int width   : width of image
 * unsigned int height  : height of image
 * int quality          : Compression quality [0..100]
 *
 * <output>
 *
 * int: Returns 'true' if success, 'false' if failure.
 *
 * <error handling>
 * 
 * The function prints error messages.
 *
 *********************************************************************/
int it_savejpg(const char *name,unsigned int *image,
	       unsigned int width,unsigned int height, int quality)
{
  struct jpeg_compress_struct cinfo;
	struct jpeg_error_mgr jerr;

	cinfo.err = jpeg_std_error(&jerr);
	jpeg_create_compress(&cinfo);

  unsigned char *data=(unsigned char *)image;

	FILE * outfile;

	if ((outfile = fopen(name, "wb")) == NULL) 
  {
    PvUtil::error("it_savejpg: Can't open file '%s'.",name);
    return 0;
	}

	jpeg_stdio_dest(&cinfo, outfile);

	cinfo.image_width      = width; 	 /* image width and height, in pixels */
	cinfo.image_height     = height;
	cinfo.input_components = 4;	       /* # of color components per pixel */
	cinfo.in_color_space   = JCS_RGB;  /* colorspace of input image */

	jpeg_set_defaults(&cinfo);
	/* Make optional parameter settings here */
	jpeg_set_quality (&cinfo, quality, TRUE);

  jpeg_start_compress(&cinfo, TRUE);

  while (cinfo.next_scanline < cinfo.image_height)
  {
    jpeg_write_scanlines(&cinfo, &data, 1);
    data+=4*width;
  }

  jpeg_finish_compress(&cinfo);

  fclose(outfile);

  jpeg_destroy_compress(&cinfo);

  return 1;
}
#if 0
/*********************************************************************
 * it_loadjpg()
 * 
 * Load image in jpg format.
 *
 * <input> 
 * 
 * char *name           : file name 
 * unsigned int *image  : pointer to image
 * unsigned int width   : width of image
 * unsigned int height  : height of image
 *
 * <output>
 *
 * int: Returns 'true' if success, 'false' if failure.
 *
 * <error handling>
 * 
 * The function prints error messages.
 *
 *********************************************************************/
unsigned int *it_loadjpg(const char *name,unsigned int **image,
			 unsigned int *width,unsigned int *height)
{
  unsigned int i;

  struct jpeg_decompress_struct cinfo;
	struct jpeg_error_mgr jerr;

  unsigned char *data;

  *image=NULL;

  cinfo.err = jpeg_std_error(&jerr);
	jpeg_create_decompress(&cinfo);

	FILE *infile;

	if ((infile = fopen(name, "rb")) == NULL)
  {
    PvUtil::exitError("it_loadjpg: Error opening file.");
    goto FILE_ERROR;
  }

	jpeg_stdio_src(&cinfo, infile);

  jpeg_read_header(&cinfo, TRUE);

  //cinfo.do_fancy_upsampling = false;

  jpeg_start_decompress(&cinfo);

	*width=cinfo.output_width; 	/* image width and height, in pixels */
	*height=cinfo.output_height;

  if(cinfo.output_components!=4)
  {
    PvUtil::exitError("it_loadjpg: Error, found %d instead of 4 components in JPEG image.",cinfo.output_components);
    goto MEM_ERROR;
  }

  *image=new unsigned int [(*width)*(*height)];

	if (*image==NULL)
  {
    PvUtil::exitError("it_loadjpg: Error allocating image memory.");
    goto MEM_ERROR;
  }

  data=(unsigned char *)*image;

  JSAMPLE **pp = new JSAMPLE* [*height];

  JSAMPLE **p0 = pp;

  for (i = 0; i < *height; i++)
  {
    pp[i] = data + i*(*width)*cinfo.output_components;
  }

  int rowsLeft = *height;

  while (rowsLeft > 0)
  {
    int rowsParsed = jpeg_read_scanlines(&cinfo, p0, rowsLeft);
    rowsLeft -= rowsParsed;
    p0 += rowsParsed;
  }

  delete [] pp;

MEM_ERROR:

  jpeg_finish_decompress(&cinfo);

  fclose(infile);

FILE_ERROR:

  jpeg_destroy_decompress(&cinfo);

  return *image;
}
#endif
} // namespace aitvml

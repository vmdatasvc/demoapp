/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "modules/ml/AITPort/TextCodec.hpp"
#include <vector>
#include <boost/shared_ptr.hpp>
#include <modules/ml/AITPort/File.hpp>

namespace aitvml
{

std::string TextCodec::CRYPT_KEY("!*&AD<");

TextCodec::TextCodec()
{
}

TextCodec::~TextCodec()
{
}

void
TextCodec::encodeToFile(const std::string& fileName, const std::string& data)
{
  std::vector<Uint8> compressed;

  encodeToMemory(data, compressed);

  try{
    File::writeMemoryToBinaryFile(fileName, compressed);
  }

  catch(File::write_error){
    printf("Error: Unable to write...\n");
  }
}

void
TextCodec::encodeToMemory(const std::string &inData, std::vector<Uint8> & outData)
{
  Ulong compressedLength=0, uncompressedLength;

  outData.clear();
  outData.resize(inData.length()+12);

  uncompressedLength = inData.length();

  // temporary comment out. install zlib later
//  compress2((unsigned char*)&outData[0], &compressedLength, (unsigned char*)&inData[0], uncompressedLength, 1);

  char *uncomprLengthCharPtr = (char*)&uncompressedLength;

  outData.insert(outData.begin(), *(uncomprLengthCharPtr+3));
  outData.insert(outData.begin(), *(uncomprLengthCharPtr+2));
  outData.insert(outData.begin(), *(uncomprLengthCharPtr+1));
  outData.insert(outData.begin(), *(uncomprLengthCharPtr));
  outData.resize(compressedLength+4);

  encrypt(outData);
}

void
TextCodec::decodeFromFile(const std::string& fileName, std::string& stringData)
{
  if (isEncoded(fileName))
  {
    std::vector<unsigned char> data;
    File::readBinaryFileToMemory(fileName,data);
    decodeFromMemory(data,stringData);
  }

  else
  {
    File::readTextFileToMemory(fileName,stringData);
  }
}

void
TextCodec::decodeFromMemory(std::vector<Uint8>& encodedString, std::string& stringData)
{
  std::string compressed = "";
  std::string uncompressed = "";
 
  Uint32 *uncompressedLength;
  //Uint32 compressedLength;

  uncompressedLength = (Uint32*)(&encodedString[0]);

  uncompressed.resize(*uncompressedLength);
  //compressedLength = encodedString.size()-4;

  decrypt(encodedString);

  stringData.resize(*uncompressedLength);

  // temporary comment out
//  uncompress((unsigned char*)&stringData[0], (Ulong*)uncompressedLength, (unsigned char*)&encodedString[4], compressedLength);

  encrypt(encodedString); // We want to keep the state of encodedString constant
}

bool 
TextCodec::isEncoded(const std::string& fileName)
{
  std::vector<Uint8> header;
  header.clear();

  boost::shared_ptr<File> inFile(new File());

	try{
		inFile->readBinaryFileToMemory(fileName, header, CRYPT_KEY.length()+4);
	}

  catch(File::file_not_found)
	{
    printf("Error: File not found...\n");
		return false;
	}

  decrypt(header);

  // Check the first two bytes of the compressed code
  if ((header[4] == 120) && (header[5] == 1)) // Is encoded...
  {
    return true;
  }

  else // Is not encoded...
  {
    return false;
  }

}

void
TextCodec::encrypt(std::vector<unsigned char>& buffer)
{
  for (unsigned int i = 4; i < CRYPT_KEY.length()+4; i++)
  {
    buffer[i] ^= CRYPT_KEY[i-4];
  }
}

void
TextCodec::decrypt(std::vector<unsigned char>& buffer)
{
  for (unsigned int i = 4; i < CRYPT_KEY.length()+4; i++)
  {
    buffer[i] ^= CRYPT_KEY[i-4];
  }
}

}; // namespace aitvml

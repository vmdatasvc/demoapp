/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
//  As the Interface between Opencv v2 and Image32/Image8

/*
 * OpenCVUtils2.cpp
 *
 *  Created on: Jan 12, 2016
 *      Author: dkim
 */

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "modules/ml/AITPort/OpenCvUtils2.hpp"

namespace aitvml
{

namespace vision
{


void OpenCvUtils2::cvtMatToImage32(cv::Mat imat, aitvml::Image32 *img)
{
       int w = imat.cols;
       int h = imat.rows;

       (*img) = aitvml::Image32(w,h);

       for(int y=0; y<h; y++)
       {
           unsigned char *pSrcData = ((unsigned char *)imat.data) + y*w*3;
           unsigned char *pDstData = ((unsigned char *)img->pointer()) + y*w*4;

           for(int x=0; x<w; x++)
           {
               if(img->getInternalFormat() == 0)
               {
                    pDstData[0] = pSrcData[2];
                    pDstData[1] = pSrcData[1];
                    pDstData[2] = pSrcData[0];
               }
               else
               {
                   pDstData[0] = pSrcData[0];
                   pDstData[1] = pSrcData[1];
                   pDstData[2] = pSrcData[2];
               }
               pSrcData += 3;
               pDstData += 4;
            }
       }

}

void OpenCvUtils2::cvtImage32ToMat(aitvml::Image32 img, cv::Mat *imat)
{

    int w = img.width();
    int h = img.height();

    (*imat) = cv::Mat(h,w,CV_8UC3);

    for(int y=0; y<h; y++)
    {
        unsigned char *pSrcData = (unsigned char *)img.pointer() + y*w*4;
        unsigned char *pDstData = (unsigned char *)imat->data + y*w*3;

        for(int x=0; x<w; x++)
        {
            if(img.getInternalFormat() == 0)
            {
                 pDstData[0] = pSrcData[2];
                 pDstData[1] = pSrcData[1];
                 pDstData[2] = pSrcData[0];
            }
            else
            {
                pDstData[0] = pSrcData[0];
                pDstData[1] = pSrcData[1];
                pDstData[2] = pSrcData[2];
            }
            pSrcData += 4;
            pDstData += 3;
         }
    }

}

void OpenCvUtils2::cvtMatToImage8(cv::Mat imat, aitvml::Image8 *img)
{
    int w = imat.cols;
    int h = imat.rows;

    (*img) = aitvml::Image8(w,h);

    for(int y=0; y<h; y++)
    {
        unsigned char *pSrcData = ((unsigned char *)imat.data) + y*w;
        unsigned char *pDstData = ((unsigned char *)img->pointer()) + y*w;

        for(int x=0; x<w; x++)
        {
            pDstData[0] = pSrcData[0];
            pSrcData ++;
            pDstData ++;
         }
    }

}

void OpenCvUtils2::cvtImage8ToMat(aitvml::Image8 img, cv::Mat *imat)
{

    int w = img.width();
    int h = img.height();

    (*imat) = cv::Mat(h,w,CV_8UC1);

    for(int y=0; y<h; y++)
    {
        unsigned char *pSrcData = (unsigned char *)img.pointer() + y*w;
        unsigned char *pDstData = (unsigned char *)imat->data + y*w;

        for(int x=0; x<w; x++)
        {
            pDstData[0] = pSrcData[0];
            pSrcData ++;
            pDstData ++;
         }
    }

}



}; // namespace vision

}; // namespace aitvml


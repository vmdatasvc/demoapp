/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif
#include <vector>
#include "modules/ml/AITPort/FaceDetectorAdaBoostForeground.hpp"
#include "modules/ml/AITPort/Settings.hpp"
//#include "core/Settings.hpp"

namespace aitvml
{

namespace vision
{

// object initialized in the constructor
FaceDetectorAdaBoostForeground::FaceDetectorAdaBoostForeground()
: mIsInitialized(true), mpCascade(0), mScaleFactor(1.2f),
  mNumNeighbors(2)
{
  // The minimum size for the Ada Boost Face detector is 24x24.
  mTrainingMinSize.set(24,24);
}

FaceDetectorAdaBoostForeground::~FaceDetectorAdaBoostForeground()
{

/*
#ifdef OLDCV
  if (mpCascade)
  {
    cvReleaseHidHaarClassifierCascade( &mpCascade );
  }
#endif
*/
}


void FaceDetectorAdaBoostForeground::init(const std::string modelName, double scaleFactor)
{
    std::string modelName1 = Settings::replaceVars(modelName);
    if (!PvUtil::fileExists(modelName1.c_str()))
    {
      PvUtil::exitError("File %s not found!!",modelName1.c_str());
    }

    mpCascade.load(modelName1.c_str());

   mScaleFactor = scaleFactor;

//	Settings::parseXml("c:/Documents and Settings/hmoon/My Documents/Workspace/FaceDetec/settings.xml");
//	Settings bdSz("FaceDetec");
//
//	int minWidth = bdSz.getInt("minWidth",24);
//	int minHeight = bdSz.getInt("minHeight",24);
//	int maxWidth = bdSz.getInt("maxWidth",120);
//	int maxHeight = bdSz.getInt("maxHeight",120);
//
//	std::cout << minWidth << "\n";
//	std::cout << minHeight << "\n";
//	std::cout << maxWidth << "\n";
//	std::cout << maxHeight << "\n";
//
//	minSize = cvSize(minWidth, minHeight);
//	maxSize = cvSize(maxWidth, maxHeight);
}


//
// OPERATIONS
//

void 
FaceDetectorAdaBoostForeground::detect(const Image8 &grayImage, bool async)
{}

void 
FaceDetectorAdaBoostForeground::detect(const Image8 &grayImage, const Image8 &fgImage, bool async)
{
  std::vector<Rectanglei> vecRect;

	// Optimization for reducing the search space given to the face detector.
  Image8 subsampledImage, subsampledFGImage;
  float resampleFactor = 1, dum;

  cv::Size minFaceSize(0.0);

  if (mResampleForMinSize)
  {
    resampleFactor = resampleForMinSize(grayImage,subsampledImage);
    dum = resampleForMinSize(fgImage,subsampledFGImage);
  }
  else
  {
    minFaceSize = cv::Size(static_cast<int>(mMinFaceSize(0)*resampleFactor),
                         static_cast<int>(mMinFaceSize(1)*resampleFactor));
  }
  
  // These are the parameters for cvHaarDetectObjects
  cv::Size maxFaceSize = cv::Size(static_cast<int>(mMaxFaceSize(0)*resampleFactor),
                              static_cast<int>(mMaxFaceSize(1)*resampleFactor));

	Image8 const *pImg, *pFGImg;

  if (resampleFactor < 1.0f)
  {
    pImg = &subsampledImage;
    pFGImg = &subsampledFGImage;
  }
  else
  {
    pImg = &grayImage;
    pFGImg = &fgImage;
  }

  unsigned char* pixels = pImg->pointer();
  unsigned char* pixelsFG = pFGImg->pointer();
  
  assert(pixels!=NULL);
  
  int w=pImg->width();
  int h=pImg->height();
  
  // This Face detector won't detect Faces smaller than 20x20
  if (w < 20 || h < 20)
  {
    nFaces = 0;
    return;
  }
  
  //
   cv::Mat imgMat(w,h, CV_8UC1);
   imgMat.data = pixels;

   std::vector<cv::Rect> faces;

   double scaleFactor = 1.1;
   int min_nbr = 2;
   int minsize = 20;
   int maxsize = 160;

   // Need Foreground masking!! ====

   //mpCascade.detectMultiScale(imgMat, faces, 1.1, 2, 0 | CASCADE_SCALE_IMAGE, cv::Size(minsize,minsize), cv::Size(maxsize,maxsize));
   mpCascade.detectMultiScale(imgMat, faces, 1.1, 2, 0, cv::Size(minsize,minsize), cv::Size(maxsize,maxsize));


   cv::Rect face_rect;

   int count1 = 0;
   int count2 = 0;
   int scale = 1;

   //  iter = vecRect.begin();
   for(int i = 0; i <  faces.size(); i++ )
   {
 	  	  face_rect.x = faces[i].x;
 	  	  face_rect.y = faces[i].y;
 	  	  face_rect.width = faces[i].width;
 	  	  face_rect.height = faces[i].height;

     vecRect.push_back(Rectangle<int>(face_rect.x*scale, face_rect.y*scale,
       face_rect.width*scale + face_rect.x*scale,
       face_rect.height*scale + face_rect.y*scale));
     int check = 0;

     for(int k = 0; k < count1; k++)
     {
       int  xx0 = std::max(vecRect[i].x0,vecRect[k].x0);
       int  yy0 = std::max(vecRect[i].y0,vecRect[k].y0);
       int  xx1 = std::min(vecRect[i].x1,vecRect[k].x1);
       int  yy1 = std::min(vecRect[i].y1,vecRect[k].y1);
       if(((xx1-xx0) > 0)&&((yy1-yy0) > 0))
       {
         if((xx1-xx0)*(yy1-yy0) > 0.5*(vecRect[k].x1-vecRect[k].x0)*(vecRect[k].y1-vecRect[k].y0))
         {
           //  overlap..
           check++;
         }
       }
     }
     if(!check)
     {
       faceArray[count2].x = face_rect.x*scale;
       faceArray[count2].y = face_rect.y*scale;
       faceArray[count2].w = face_rect.width*scale;
       faceArray[count2].h = face_rect.height*scale;
       count2++;
     }
     count1++;
   }

   nFaces = count2;

   // Now we have to scan the array and adjust the sizes so it fits the original image.
   for (int i = 0; i < nFaces; i++)
   {
     faceArray[i].x = (int)(faceArray[i].x/resampleFactor);
     faceArray[i].y = (int)(faceArray[i].y/resampleFactor);
     faceArray[i].w = (int)(faceArray[i].w/resampleFactor);
     faceArray[i].h = (int)(faceArray[i].h/resampleFactor);
   }
}



//
// ACCESS
//

//
// INQUIRY
//


}; // namespace vision

}; // namespace aitvml


/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
#include "modules/ml/AITPort/FaceDetector.hpp"
#include "modules/ml/AITPort/PvImageProc.hpp"

#include <limits>

//using namespace vml;
namespace aitvml
{

FaceDetector::FaceDetector(void) 
: isInitialized(false), 
  mpMaskIntegralImage(NULL), 
  mMaskAreaFraction(1),
  mMaxFaceSize(std::numeric_limits<int>::max(),std::numeric_limits<int>::max()),
  mResampleForMinSize(false)
{
  initialize();
}

FaceDetector::~FaceDetector(void)
{
  unInitialize();
}

void FaceDetector::initialize(void)
{
  if(isInitialized) unInitialize();

  nFaces=0;
  
  faceArray=new Face [FD_MAX_NR_OF_FACES];

  // initialize every face
  for(int i=0;i<FD_MAX_NR_OF_FACES;i++)
  {
    face(i).initialize();
  }

  isInitialized=true;
}

void FaceDetector::unInitialize(void)
{
  if(!isInitialized)
  {
    PvUtil::exitError("FaceDetector::unInitialize(): Error, not initialized!");
  }

  delete [] faceArray;

  if (mpMaskIntegralImage)
  {
    delete mpMaskIntegralImage;
    mpMaskIntegralImage = NULL;
  }

  isInitialized=false;
}

void 
FaceDetector::setMask(const Image8 *pMask, const float areaFraction)
{
  if (!pMask)
  {
    if (mpMaskIntegralImage) 
    {
      delete mpMaskIntegralImage;
      mpMaskIntegralImage = NULL;
    }
    return;
  }

  if (!mpMaskIntegralImage)
  {
    mpMaskIntegralImage = new Image<unsigned short>(pMask->width(),pMask->height());
  }

  computeIntegralImage(*pMask,*mpMaskIntegralImage);
  mMaskAreaFraction = areaFraction;
}

float 
FaceDetector::resampleForMinSize(const Image8& in, Image8& out)
{
  // If no min size was requested, return immediately.
  if (mMinFaceSize(0) == 0) return 1;

  // The requested size must be of the same aspect ratio of the mask size, we will just take the
  // height for this calculation.
  float resampleFactor = ((float)mTrainingMinSize(1))/mMinFaceSize(1);

  // If we resample by this factor the input image, then the minimum face size detected will
  // be equivalent to the desired mMinFaceSize. Let's do only subsamplings. This will reduce
  // the search space and improve performance.
  Vector2ui newSize((unsigned int)(resampleFactor*in.width()),(unsigned int)(resampleFactor*in.height()));

  // If the resulting image size is not significantly smaller (less than 95%), it will cost more to 
  // copy and resample than just operating on the original image.
  if (newSize(0)*newSize(1) < in.width()*in.height()*95/100)
  {      
//      out.rescale(in,newSize(0),newSize(1),RESCALE_LINEAR);

    return resampleFactor;
  }

  return 1;
}

} //namespace aitvml

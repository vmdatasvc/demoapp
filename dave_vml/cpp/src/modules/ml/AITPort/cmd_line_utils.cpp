/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#pragma warning(disable:4503)
#endif

#include <modules/ml/AITPort/strutil.hpp>

#include "modules/ml/AITPort/cmd_line_utils.hpp"

namespace aitvml
{

static CommandLineDict build_dict(const std::vector<std::string>& argList)
{
  // Now parse the arguments.
  std::string optionName;
  std::vector<std::string>::const_iterator iArg;
  CommandLineDict dict;

  // Create a list for the default arguments
  dict[""] = std::vector<std::string>();

  for (iArg = argList.begin(); iArg != argList.end(); iArg++)
  {
    if (iArg->substr(0,2) == "--")
    {
      optionName = *iArg;
      // Add empty list to the dictionary
      if (dict.find(*iArg) == dict.end())
      {
        dict[*iArg] = std::vector<std::string>();
      }
    }
    else
    {
      dict[optionName].push_back(*iArg);
      optionName = "";
    }
  }

  return dict;
}

static void
tokenize_args(const std::string& args, std::vector<std::string>& argList)
{
  // This code is written to be short and simple, not
  // efficient.

  unsigned int i;
  std::string token;
  for (i = 0; i < args.length();)
  {
    unsigned int end = args.find_first_of(" \t\"",i);

    if (end == std::string::npos)
    {
      token = args.substr(i,end);
    }
    else
    {
      if (args[end] == '"')
      {
        unsigned int endq = args.find_first_of("\"",end+1);
        if (endq == std::string::npos)
        {
          throw bad_quotes();
        }
        token = args.substr(end+1,endq-end-1);
        end = endq;
      }
      else
      {
        token = args.substr(i,end-i);
      }
    }

    if (!token.empty())
    {
      argList.push_back(token);
    }

    if (end == std::string::npos) break;

    i = end+1;
  }
}

static void tokenize_args(int argc, char **argv, std::vector<std::string>& argList)
{
  int i;
  for (i = 0; i < argc; i++)
  {
    argList.push_back(argv[i]);  
  }
}

CommandLineDict
parse_args(const std::string& args)
{
  // First tokenize by removing spaces and parsing quotes.
  std::vector<std::string> argList;
  tokenize_args(args,argList);
  return build_dict(argList);
}

CommandLineDict
parse_args(int argc, char **argv)
{
  std::vector<std::string> argList;
  tokenize_args(argc,argv,argList);
  return build_dict(argList);
}

std::string print_args(const CommandLineDict& dict)
{
  std::string out = "{";

  CommandLineDict::const_iterator i;
  for (i = dict.begin(); i != dict.end();)
  {
    out += "'" + i->first + "':[";
    
    if (!(i->second.empty()))
    {
      out += "'" + join(i->second,std::string("','")) + "'";
    }
    out += "]";
    ++i;
    if (i != dict.end()) out += ",";
  }

  out += "}";

  return out;
}

static void
build_list(const std::vector<std::string>& argList, std::vector<CommandLineArgumentPtr>& list)
{
  CommandLineArgumentPtr pCurArg;
  std::vector<std::string>::const_iterator iArg;

  for (iArg = argList.begin(); iArg != argList.end(); ++iArg)
  {
    CommandLineArgumentPtr pNewArg;
    if (iArg->substr(0,2) == "--")
    {
      pNewArg.reset(new CommandLineArgument);
      pNewArg->option = *iArg;
      pCurArg = pNewArg;
    }
    else
    {
      if (!pCurArg.get())
      {
        pNewArg.reset(new CommandLineArgument);
        pCurArg = pNewArg;
      }
      pCurArg->value = *iArg;
      pCurArg.reset();
    }
    if (pNewArg.get())
    {
      list.push_back(pNewArg);
    }
  }
}

std::vector<CommandLineArgumentPtr>
parse_args_list(const std::string& args)
{
  std::vector<std::string> argList;
  tokenize_args(args,argList);
  std::vector<CommandLineArgumentPtr> l;
  build_list(argList,l);
  return l;
}

std::vector<CommandLineArgumentPtr>
parse_args_list(int argc, char **argv)
{
  std::vector<std::string> argList;
  tokenize_args(argc,argv,argList);
  std::vector<CommandLineArgumentPtr> l;
  build_list(argList,l);
  return l;
}

std::string
print_args(const std::vector<CommandLineArgumentPtr>& l)
{
  std::vector<CommandLineArgumentPtr>::const_iterator iArg;

  std::string s = "[";
  for (iArg = l.begin(); iArg != l.end(); ++iArg)
  {
    if (s.size() > 2) s += ",";
    s += "{'" + (*iArg)->option + "','" + (*iArg)->value + "'}";
  }
  s+= "]";
  return s;
}

} // namespace aitvml

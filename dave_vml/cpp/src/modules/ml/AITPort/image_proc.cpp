/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include "modules/ml/AITPort/image_proc.hpp"

#ifdef USE_IPP
#include <ipp.h>
#else
// use opencv GaussianBlur
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#endif

#include <modules/ml/AITPort/AitBaseTypes.hpp>

namespace aitvml
{

#ifdef USE_IPP
#define IPP_IMG_PTR(img,roi) ((unsigned char *)(img.pointer()+roi.y0*img.width()+roi.x0))
#define IPP_IMG_STEP(img) (img.width()*sizeof(img(0,0)))
#define IPP_ROI(roi) (IppiSize(roi.width(),roi.height()))
#endif

namespace image_proc
{
bool smoothImage(const Image8& in, Image8& out, 
                 const SmoothMode mode, const int maskSizei)
{
  out.resize(in.width(), in.height());

  int offset = (maskSizei-1)/2;

 //set border to zero
  //out.zero();
  out.rectFill(Rectanglei(0,0,out.width(),offset), 0);
  out.rectFill(Rectanglei(0,out.height()-offset-1,out.width(),out.height()), 0);
  out.rectFill(Rectanglei(0,offset+1,offset, out.height()-offset-2), 0);
  out.rectFill(Rectanglei(out.width()-offset-1,offset+1,out.width(),out.height()-offset-2),0);

//	int RoiW = in.width()-2*offset;
//  int RoiH = in.height()-2*offset;
 
#if USE_IPP
  IppiSize dstRoiSize = {RoiW, RoiH};
#endif
	
  if (mode == SMOOTH_GAUSSIAN)
  {
#if USE_IPP
    IppiMaskSize maskSize;

    switch (maskSizei)
    {
    case 3:
      maskSize = ippMskSize3x3;
      break;
    case 5:
      maskSize = ippMskSize5x5;
      break;
    default:
      PvUtil::exitError("Only gaussian masks of size 3x3 and 5x5 are supported");
    }

	  ippiFilterGauss_8u_C1R(in.pointer() + offset*(in.width()+1), in.width(),
		                       out.pointer() + offset*(out.width()+1), out.width(), 
                           dstRoiSize, maskSize);
#else
	switch (maskSizei)
	{
	case 3:
	case 5:
		break;
	default:
		PvUtil::exitError("Only gaussian masks of size 3x3 and 5x5 are rupported");
	}

	cv::Size	maskSize(maskSizei,maskSizei);

	// convert images to cv images
    cv::Mat_<unsigned char> inCVHeader(in.height(),in.width(),(unsigned char*)in.pointer());
    cv::Mat_<unsigned char> outCVHeader(out.height(),out.width(),(unsigned char*)out.pointer());
    // run opencv gaussian
    cv::GaussianBlur(inCVHeader, outCVHeader, maskSize, 0.f, 0.f);	// sigma set as 0.f so automatically generated
#endif
    }
	else if (mode == SMOOTH_MEDIAN)
	{
#ifdef USE_IPP
		IppiSize maskSize;

		switch (maskSizei)
		{
		case 3:
				maskSize.width = 3;
				maskSize.height = 3;
		  break;
		case 5:
		  maskSize.width = 5;
				maskSize.height = 5;
		  break;
		default:
		  PvUtil::exitError("Only masks of size 3x3 and 5x5 are supported");
		}	// of switch

		IppiPoint anchor;
		anchor.x = (maskSizei - 1)/2;
		anchor.y = anchor.x;

		ippiFilterMedian_8u_C1R(in.pointer() + offset*(in.width()+1),in.width(), 
														out.pointer() + offset*(out.width()+1), out.width(), 
														dstRoiSize, maskSize, anchor);
#else
		switch (maskSizei)
		{
		case 3:
		case 5:
			break;
		default:
			PvUtil::exitError("Only masks of size 3x3 and 5x5 are supported");
		}

		// convert images to cv images
	    cv::Mat_<unsigned char> inCVHeader(in.height(),in.width(),(unsigned char*)in.pointer());
	    cv::Mat_<unsigned char> outCVHeader(out.height(),out.width(),(unsigned char*)out.pointer());

		cv::medianBlur(inCVHeader, outCVHeader, maskSizei);
#endif
  } // of else if (mode == SMOOTH_MEDIAN)
  else
  {
    if (maskSizei%2 != 1) 
    {
      PvUtil::exitError("Only odd mask sizes are allowed for mean smooth filters\n");
    }

#ifdef USE_IPP
    IppiSize maskSize = {maskSizei,maskSizei};

    IppiPoint anchor = { offset, offset };
      
    ippiFilterBox_8u_C1R(in.pointer() + offset*(in.width()+1), in.width(),
		                     out.pointer() + offset*(out.width()+1), out.width(), 
                         dstRoiSize, maskSize, anchor);
#else
	// convert images to cv images
    cv::Mat_<unsigned char> inCVHeader(in.height(),in.width(),(unsigned char*)in.pointer());
    cv::Mat_<unsigned char> outCVHeader(out.height(),out.width(),(unsigned char*)out.pointer());

    cv::Size	maskSize(maskSizei, maskSizei);
    cv::Point	anchor(offset, offset);

    cv::boxFilter(inCVHeader, outCVHeader, -1, maskSize, anchor);
#endif
  }

  return true;
}

bool smoothImage(Image8& img, 
                 const SmoothMode mode, const int maskSize)
{
  Image8 out(img.width(),img.height());
  out.zero();
  bool res = smoothImage(img,out,mode,maskSize);
  img = out;
  return res;
}

bool smoothImage(const Image32& in, Image32& out, 
                 const SmoothMode mode, const int maskSizei)
{
  if (maskSizei == 1) 
  {
    out = in;
    return true;
  }
	
	int inW = in.width();
	int inH = in.height();
	int outW = out.width();
	int outH = out.height();

  out.resize(inW, inH);

  int offset = (maskSizei-1)/2;
	
	outW=inW;
	outH=inH;

  //set border to zero
  //out.zero();
  out.rectFill(Rectanglei(0,0,outW,offset), PV_RGB(0,0,0));
  out.rectFill(Rectanglei(0,outH-offset-1,outW,outH), PV_RGB(0,0,0));
  out.rectFill(Rectanglei(0,offset+1,offset, outH-offset-2), PV_RGB(0, 0,0));
  out.rectFill(Rectanglei(outW-offset-1,offset+1,outW,outH-offset-2),PV_RGB(0,0,0));
  
//  int RoiW = inW-2*offset;
//  int RoiH = inH-2*offset;
 
#ifdef USE_IPP
  IppiSize dstRoiSize = {RoiW, RoiH};
#endif
	
  if (mode == SMOOTH_GAUSSIAN)
  {
#ifdef USE_IPP
    IppiMaskSize maskSize;

    switch (maskSizei)
    {
    case 3:
      maskSize = ippMskSize3x3;
      break;
    case 5:
      maskSize = ippMskSize5x5;
      break;
    default:
      PvUtil::exitError("Only gaussian masks of size 3x3 and 5x5 are supported");
    }

	  ippiFilterGauss_8u_C4R((unsigned char *)(in.pointer() + offset*(inW+1)), inW*4,
		                       (unsigned char *)(out.pointer() + offset*(outW+1)), outW*4, 
                           dstRoiSize, maskSize);
#else
		switch (maskSizei)
		{
		case 3:
		case 5:
			break;
		default:
			PvUtil::exitError("Only gaussian masks of size 3x3 and 5x5 are rupported");
		}

		cv::Size	maskSize(maskSizei,maskSizei);

		// convert images to cv images
	    cv::Mat_<unsigned int> inCVHeader(in.height(),in.width(),(unsigned int*)in.pointer());
	    cv::Mat_<unsigned int> outCVHeader(out.height(),out.width(),(unsigned int*)out.pointer());
	    // run opencv gaussian
	    cv::GaussianBlur(inCVHeader, outCVHeader, maskSize, 0.f, 0.f);	// sigma set as 0.f so automatically generated
#endif
		
  }
	else if (mode == SMOOTH_MEDIAN)
  {
#ifdef USE_IPP
    IppiSize maskSize;

		switch (maskSizei)
    {
    case 3:
			maskSize.width = 3;
			maskSize.height = 3;
      break;
    case 5:
      maskSize.width = 5;
			maskSize.height = 5;
      break;
    default:
      PvUtil::exitError("Only masks of size 3x3 and 5x5 are supported");
    }

		IppiPoint anchor;
		anchor.x = (maskSizei - 1)/2;
		anchor.y = anchor.x;

	 ippiFilterMedian_8u_C4R((unsigned char *)(in.pointer() + offset*(inW+1)),inW*4, 
														(unsigned char *)(out.pointer() + offset*(outW+1)), outW*4, 
														dstRoiSize, maskSize, anchor);
#else
		switch (maskSizei)
		{
		case 3:
		case 5:
			break;
		default:
			PvUtil::exitError("Only masks of size 3x3 and 5x5 are supported");
		}

		// convert images to cv images
	    cv::Mat_<unsigned int> inCVHeader(in.height(),in.width(),(unsigned int*)in.pointer());
	    cv::Mat_<unsigned int> outCVHeader(out.height(),out.width(),(unsigned int*)out.pointer());

		cv::medianBlur(inCVHeader, outCVHeader, maskSizei);
#endif
  }
  else
  {
    if (maskSizei%2 != 1) 
    {
      PvUtil::exitError("Only odd mask sizes are allowed for mean smooth filters\n");
    }

#ifdef USE_IPP
    IppiSize maskSize = {maskSizei,maskSizei};

    IppiPoint anchor = { offset, offset };
      
    ippiFilterBox_8u_C4R((unsigned char *)(in.pointer() + offset*(inW+1)), inW*4,
		                     (unsigned char *)(out.pointer() + offset*(outW+1)), outW*4, 
                         dstRoiSize, maskSize, anchor);
#else
	// convert images to cv images
    cv::Mat_<unsigned int> inCVHeader(in.height(),in.width(),(unsigned int*)in.pointer());
    cv::Mat_<unsigned int> outCVHeader(out.height(),out.width(),(unsigned int*)out.pointer());

    cv::Size	maskSize(maskSizei, maskSizei);
    cv::Point	anchor(offset, offset);

    cv::boxFilter(inCVHeader, outCVHeader, -1, maskSize, anchor);
#endif
  }

  return true;
}

bool smoothImage(Image32& img, 
                 const SmoothMode mode, const int maskSize)
{
  if (maskSize == 1) return true;
  Image32 out(img.width(),img.height());
  out.zero();
  bool res = smoothImage(img,out,mode,maskSize);
  img = out;
  return res;
}

void calcMeanStdDev(const Image8& img, float& mean, float& stdDev)
{
  /*
  IppiSize roiSize = {img.width(), img.height()};

  Ipp64f ippMean, ippStdDev;

  ippiMean_StdDev_8u_C1R(img.pointer(),img.width(),roiSize,&ippMean,&ippStdDev);

  mean = ippMean;
  stdDev = ippStdDev;
  */
  
  int size = img.size();

  mean = 0;
  stdDev = 0;

  if (size == 0)
  {
    return;
  }

  for (int i = 0; i < size; i++) mean += img(i);
  mean /= size;

  if (size > 1)
  {
    float d;
    for (int i = 0; i < size; i++) 
    {
      d = mean-img(i);
      stdDev += d*d;
    }
    stdDev = sqrt(stdDev/(size-1));
  }
}

void calcMeanStdDev(const Image32& img, Vector3f& mean, Vector3f& stdDev)
{
  mean.set(0,0,0);
  stdDev.set(0,0,0);

  int size = img.size();

  if (size == 0)
  {
    return;
  }

  for (int i = 0; i < size; i++) 
  {
    Uint32 pix = img(i);
    mean(0) += RED(pix);
    mean(1) += GREEN(pix);
    mean(2) += BLUE(pix);
  }
  mean(0) /= size;
  mean(1) /= size;
  mean(2) /= size;

  if (size > 1)
  {
    float d;
    for (int i = 0; i < size; i++) 
    {
      Uint32 pix = img(i);
      d = mean(0)-RED(pix);
      stdDev(0) += d*d;
      d = mean(1)-GREEN(pix);
      stdDev(1) += d*d;
      d = mean(2)-BLUE(pix);
      stdDev(2) += d*d;
    }
    stdDev(0) = sqrt(stdDev(0)/(size-1));
    stdDev(1) = sqrt(stdDev(1)/(size-1));
    stdDev(2) = sqrt(stdDev(2)/(size-1));
  }
}

void
copyRGB2Gray(Image32 &in, Image8& out, int channel)
{
	int w=in.width();
	int h=in.height();

	out.resize(w,h);

	unsigned int* imgPtr1 = in.pointer();
	unsigned char* imgPtr2 = out.pointer();

	switch(channel)
	{
	case 0:
		for(int i=0; i<w*h; i++)			
			imgPtr2[i] = RED(imgPtr1[i]);
		break;
	case 1:
		for(int i=0; i<w*h; i++)			
			imgPtr2[i] = GREEN(imgPtr1[i]);
		break;
	case 2:
		for(int i=0; i<w*h; i++)			
			imgPtr2[i] = BLUE(imgPtr1[i]);
		break;
	case 3:
		for(int i=0; i<w*h; i++)			
			imgPtr2[i] = ALPHA(imgPtr1[i]);
		break;
	default:
		for(int i=0; i<w*h; i++)			
			imgPtr2[i] = 0.299*RED(imgPtr1[i])+ 0.587*GREEN(imgPtr1[i]) + 0.144*BLUE(imgPtr1[i]);
	}
}


void
copyGray2RGB(Image8 &in, Image32& out, int channel)
{
	int w=in.width();
	int h=in.height();

	out.resize(w,h);

	unsigned char* imgPtr1 = in.pointer();
	unsigned int* imgPtr2 = out.pointer();

    int i=0;

	switch(channel)
	{
	case 0:
		for(i=0; i<w*h; i++)			
			imgPtr2[i] = imgPtr1[i];
		break;
	case 1:
		for(i=0; i<w*h; i++)		
        {
			imgPtr2[i] = imgPtr1[i];
			imgPtr2[i] = imgPtr2[i]<<9;
        }
		break;
	case 2:
		for(i=0; i<w*h; i++)			
        {
			imgPtr2[i] = imgPtr1[i];
			imgPtr2[i] = imgPtr2[i]<<16;
        }
		break;
	case 3:
		for(i=0; i<w*h; i++)			
        {
			imgPtr2[i] = imgPtr1[i];
			imgPtr2[i] = imgPtr2[i]<<24;
        }
		break;
	default:
			for(i=0; i<w*h; i++)		
			{
				imgPtr2[i] = imgPtr1[i];
				imgPtr2[i] = imgPtr2[i]<<8;
				imgPtr2[i] += imgPtr1[i];
				imgPtr2[i] = imgPtr2[i]<<8;
				imgPtr2[i] += imgPtr1[i];
			}
	}

}

}; // namespace image_proc

}; // namespace aitvml

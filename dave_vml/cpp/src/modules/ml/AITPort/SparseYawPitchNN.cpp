//#include "SparseGeometricNN.hpp"
#include "modules/ml/AITPort/SparseYawPitchNN.hpp"
#include "modules/ml/AITPort/PvUtil.hpp"
#include "modules/ml/AITPort/image_proc.hpp"
#define Pi 3.1415926536

using namespace std;
using namespace Torch;

namespace aitvml
{

  // Constructor
void SparseYawPitchNN::init(string mModelListFile_, string mModelDir_, string mLsvFile_, int imgWidth_, int imgHeight_)
{
  get_args();

  mModelListFile = mModelListFile_;
  mModelDir = mModelDir_;
  mLsvFile = mLsvFile_;
	imgWidth = imgWidth_;
	imgHeight = imgHeight_;

  DiskXFile::setLittleEndianMode();
  Allocator *allocator = new Allocator;
  DiskXFile *model = NULL;

  mMvNorm.reserve(25);
  mModelYaw.reserve(25);
  mModelPitch.reserve(25);

	std::string str = mModelDir + "/" + mModelListFile;
  if( (fp = fopen(str.c_str(), "r")) )
  {}
  else
  {
    PvUtil::exitError("Model list file %s not found!\n", str.c_str());
	exit(1);
  }

  int counter = 0;

// Read the NN model files and construct NNs
  while(fscanf(fp, "%f %f\n",&mYaw, &mPitch) == 2)
  {
//    cout<<counter<<" "<<mYaw<<" "<<mPitch<<endl;
    mModelYaw.push_back((mYaw/180)*Pi);
    mModelPitch.push_back((mPitch/180)*Pi);

    std::string index = aitvml::aitSprintf("/YP_%02d_%02d/Train.model",(int)mYaw,(int)mPitch);
    string modelFile(mModelDir + index);

//	  cout << modelFile<<endl;

    model = new(allocator) DiskXFile(modelFile.c_str(), "r");
    mCmd.loadXFile(model);

//    printf("model_file:%s numInputs=%d numHiddenUnits1=%d numHiddenUnits2=%d numHiddenUnits3=%d numTargets=%d\n",
//    mModelListFile.c_str(),numInputs,numHiddenUnits1,numHiddenUnits2,numHiddenUnits3,numTargets); 
 
    Linear *c1 = new(allocator) Linear(numInputs, numHiddenUnits1,&mRandom);
    c1->setROption("weight decay", weight_decay);
    Tanh*c2 = new(allocator) Tanh(numHiddenUnits1);
    Linear *c3 = new(allocator) Linear(numHiddenUnits1, numHiddenUnits2,&mRandom);
    c3->setROption("weight decay", weight_decay);
    Tanh *c4 = new(allocator) Tanh(numHiddenUnits2);
    Linear *c5 = new(allocator) Linear(numHiddenUnits2, numHiddenUnits3,&mRandom);
    c5->setROption("weight decay", weight_decay);
    Tanh *c6 = new(allocator) Tanh(numHiddenUnits3);
    Linear *c7 = new(allocator) Linear(numHiddenUnits3, numTargets,&mRandom);
    c7->setROption("weight decay", weight_decay);

    Torch::ConnectedMachine *pCc = new Torch::ConnectedMachine();

    pCc->addFCL(c1);    
    pCc->addFCL(c2);
    pCc->addFCL(c3);
    pCc->addFCL(c4);
    pCc->addFCL(c5);
    pCc->addFCL(c6);
    pCc->addFCL(c7);

    pCc->build();
    pCc->setPartialBackprop();

    MeanVarNorm *mvN;
  	mvN = new(allocator) MeanVarNorm(numInputs, numTargets);
    mvN->loadXFile(model);

    mMvNorm.push_back(mvN);

    pCc->loadXFile(model);

    mMlp.push_back(pCc);

  	counter++;
  }
  fclose(fp);

  numModel = counter;
  printf("numModel = %d\n",numModel);

	mLsv=0;

	FILE *fpLSV;
  if( (fpLSV=fopen(mLsvFile.c_str(),"r")) )
    {
      mLsv=lsv_read(mLsvFile.c_str());
      fclose(fpLSV);
    }
  else
    {    
			fprintf(stderr, "Need lsv.save!\n");
		exit(1);
		}
  
  if(mLsv->num_filters!=static_cast<unsigned int>(numInputs))
  {
    fprintf(stderr, "The lsv5000 dim: %d and the model dim: %d doesn't match\n", mLsv->num_filters, numInputs);
    exit(1);
  }

 fclose(fp);
}

void SparseYawPitchNN::init(string mModelListFile_, string mModelDir_, int imgWidth_, int imgHeight_)
{
  std::string lsvFile("c:/ai/common/data/facial_geometry_estimator/face_3D_YawPitch/lsv5000.save");
  init(mModelListFile_, mModelDir_, lsvFile, imgWidth_, imgHeight_);
}

// Torch3 interface to read Torch style trained model file
// It seems that the unnecessary options were added from training; 
// need to remove these within the training code
void SparseYawPitchNN::get_args()
{
  //  char *modelListFile, *mModelDir, *imageFile;

  // Put the help line at the beginning
//  mCmd.info(help1);

  // Train mode
  mCmd.addText("\nArguments:");
  mCmd.addSCmdArg("file", &mFileParams.file, "the train file");
  mCmd.addICmdArg("n_inputs", &numInputs, "input dimension of the data", true);
  mCmd.addICmdArg("n_targets", &numTargets, "output dim. (regression) or # of classes (classification)", true);

  mCmd.addText("\nModel Options:");
  mCmd.addICmdOption("-class", &mFileParams.class_against_the_others, -1, "train the given class against the others", true);
  mCmd.addICmdOption("-nhu1", &numHiddenUnits1, 25, "number of hidden units", true);
  mCmd.addICmdOption("-nhu2", &numHiddenUnits2, 50, "number of hidden units", true);
  mCmd.addICmdOption("-nhu3", &numHiddenUnits3, 50, "number of hidden units", true);
  mCmd.addBCmdOption("-rm", &mFileParams.regression_mode, false, "regression mode ?", true);

  mCmd.addText("\nLearning Options:");
  mCmd.addICmdOption("-iter", &mFileParams.max_iter, 25, "max number of iterations");
  mCmd.addRCmdOption("-lr", &mFileParams.learning_rate, 0.01, "learning rate");
  mCmd.addRCmdOption("-e", &mFileParams.accuracy, 0.00001, "end accuracy");
  mCmd.addRCmdOption("-lrd", &mFileParams.decay, 0, "learning rate decay");
  mCmd.addICmdOption("-kfold", &mFileParams.k_fold, -1, "number of folds, if you want to do cross-validation");
  mCmd.addRCmdOption("-wd", &weight_decay, 0, "weight decay", true);

  mCmd.addText("\nMisc Options:");
  mCmd.addICmdOption("-seed", &mFileParams.the_seed, -1, "the random seed");
  mCmd.addICmdOption("-load", &mFileParams.max_load, -1, "max number of examples to load for train");
  mCmd.addICmdOption("-load_valid", &mFileParams.max_load_valid, -1, "max number of examples to load for valid");
  mCmd.addSCmdOption("-valid", &mFileParams.valid_file, "", "validation file, if you want it");
  mCmd.addSCmdOption("-dir", &mFileParams.dir_name, ".", "directory to save measures");
  mCmd.addSCmdOption("-save", &mFileParams.model_file, "", "the model file");
  mCmd.addBCmdOption("-bin", &mFileParams.binary_mode, false, "binary mode for files");

  // Test mode
  mCmd.addMasterSwitch("--test");
  mCmd.addText("\nArguments:");

  mCmd.addText("\nMisc Options:");
  mCmd.addICmdOption("-load", &mFileParams.max_load, -1, "max number of examples to load for train");
  mCmd.addSCmdOption("-dir", &mFileParams.dir_name, ".", "directory to save measures");
  mCmd.addBCmdOption("-bin", &mFileParams.binary_mode, false, "binary mode for files");

  // Read the command line
  char **argv;
  argv = (char **)malloc(2*sizeof(char *));
  argv[0] = (char *)malloc(11*sizeof(char));
  argv[1] = (char *)malloc(7*sizeof(char));
  strcpy(argv[0], "executable");
  strcpy(argv[1], "--test");
//  argv[0] = "executable";
//  argv[1] = "--test";
  //int mode = mCmd.read(2, argv);
}

//get_args()
//{
//  int max_load, k_fold, the_seed, class_against_the_others;
//  real accuracy, decay;
//  char *dir_name, *valid_file;
//  bool regression_mode;
//
//  // Train mode
//  mCmd.addICmdArg("n_inputs", &numInputs, "input dimension of the data", true);
//  mCmd.addICmdArg("n_targets", &numTargets, "output dim. (regression) or # of classes (classification)", true);
//  mCmd.addICmdOption("-class", &class_against_the_others, -1, "train the given class against the others", true);
//  mCmd.addICmdOption("-nhu1", &numHiddenUnits1, 25, "number of hidden units", true);
//  mCmd.addICmdOption("-nhu2", &numHiddenUnits2, 50, "number of hidden units", true);
//  mCmd.addICmdOption("-nhu3", &numHiddenUnits3, 50, "number of hidden units", true);
//  mCmd.addBCmdOption("-rm", &regression_mode, false, "regression mode ?", true);
//  mCmd.addRCmdOption("-e", &accuracy, (float)0.00001, "end accuracy");
//  mCmd.addRCmdOption("-lrd", &decay, 0, "learning rate decay");
//  mCmd.addICmdOption("-kfold", &k_fold, -1, "number of folds, if you want to do cross-validation");
//  mCmd.addRCmdOption("-wd", &weight_decay, 0, "weight decay", true);
//  mCmd.addICmdOption("-seed", &the_seed, -1, "the random seed");
//  mCmd.addSCmdOption("-valid", &valid_file, "", "validation file, if you want it");
//
//  // Test mode
//  mCmd.addMasterSwitch("--test");
//  mCmd.addICmdOption("-load", &max_load, -1, "max number of examples to load for train");
//  mCmd.addSCmdOption("-dir", &dir_name, ".", "directory to save measures");
//
//  // Read the command line
//  int argc = 2;
//  char **argv;
//  argv = (char **)malloc(2*sizeof(char *));
//  argv[0] = (char *)malloc(11*sizeof(char));
//  argv[1] = (char *)malloc(7*sizeof(char));
//  argv[0] = "executable";
//  argv[1] = "--test";
//  int mode = mCmd.read(argc, argv);
//}

void SparseYawPitchNN::estimateYawPitch(int numEstModel, Image8 faceImage, Image8& outImage, float X, float Y, float S, float O)
{
  unsigned char *ucImage;
  vector< float > responses;
  responses.reserve(numEstModel);
  
  Image8 tempImage(faceImage);

  outImage.centerRotateRescaleShift(tempImage, -O, 12.5f/S, X, Y-4.28f*(S/12.5f), 0.0f, -1-0*4.28f, imgWidth, imgHeight);

  float *responseVec;
	responseVec = (float *)malloc(mLsv->num_filters*sizeof(float));

  Image8 ttImage(outImage);

	//PvImageProc::EqualizeImage(outImage);
  aitvml::image_proc::smoothImage(outImage, aitvml::image_proc::SMOOTH_GAUSSIAN, 3);
  aitvml::image_proc::smoothImage(outImage, aitvml::image_proc::SMOOTH_GAUSSIAN, 3);

  ucImage = (unsigned char *) malloc(imgWidth*imgHeight * sizeof(unsigned char));
  int count=0;
  for(int y=0; y<imgHeight; y++)
    for(int x=0; x<imgWidth; x++)
      ucImage[count++] = outImage(x,y);


  lsv_apply_filters(mLsv, responseVec, ucImage);
	 
  float min, sum;

	 // Model loop
  min = 10.0;
  Sequence inputs(1, numInputs);
  for(int m = 0; m<numEstModel; m++)
  {
  // Normalize the input to NN
    for(int i=0; i < inputs.frame_size; i++)
	  {
      inputs.frames[0][i] = responseVec[i];
//	  printf("%d ",inputData[i]);
	    inputs.frames[0][i] = (inputs.frames[0][i] - mMvNorm[m]->inputs_mean[i]) / mMvNorm[m]->inputs_stdv[i]; 
	  }
		// NN forward
   	mMlp[m]->forward(&inputs);
	
    responses.push_back(mMlp[m]->outputs->frames[0][0]);

  	if(responses[m] < min)
	    min = responses[m];
  }

  free(ucImage);
	free(responseVec);
  
  for(int m = 0; m<numEstModel; m++)
  {
    responses[m] = responses[m] - min;
	  responses[m] = pow(responses[m], 2 );
  }
  
  sum = 0.0;
  mEstYaw = 0.0; 
  mEstPitch = 0.0;

  for(int m=0; m<numEstModel; m++)
  {
    mEstYaw += responses[m]*mModelYaw[m];
    mEstPitch += responses[m]*mModelPitch[m];
    sum += responses[m];
  }

  mEstYaw /= sum;
  mEstPitch /= sum;
//  printf("************************************************************************************ sum = %f yaw = %f pitch = %f\n",sum, mEstYaw, mEstPitch);
}

} // namespace aitvml

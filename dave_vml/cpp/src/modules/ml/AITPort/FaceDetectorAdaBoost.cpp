/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif
#include <vector>
#include "modules/ml/AITPort/FaceDetectorAdaBoost.hpp"
#include "modules/ml/AITPort/Settings.hpp"
//#include "core/Settings.hpp"

namespace aitvml
{

namespace vision
{

// object initialized in the constructor
FaceDetectorAdaBoost::FaceDetectorAdaBoost()
: mIsInitialized(true), //mpCascade(0),
  mNumNeighbors(2),
  scaleFactor(1.2f),
  min_nbr(2),
  minsize(20),
  maxsize(200)
{
  // The minimum size for the Ada Boost face detector is 24x24.
  mTrainingMinSize.set(24,24);

}

FaceDetectorAdaBoost::~FaceDetectorAdaBoost()
{
  //mpCascade = 0;
}


void FaceDetectorAdaBoost::init(const std::string modelName)
{
/*
	std::string modelName1 = Settings::replaceVars(modelName);
     if (!PvUtil::fileExists(modelName1.c_str()))
     {
       PvUtil::exitError("File %s is not found!",modelName1.c_str());
     }

     mpCascade.load(modelName1.c_str());
*/
	 mpCascade.load(modelName.c_str());

     scaleFactor = 1.2;
     min_nbr = 2;
     minsize = 20;
     maxsize = 160;

}

void FaceDetectorAdaBoost::init(const std::string modelName, float scaleF, int minNbr, int minFaceSize, int maxFaceSize)
{
/*
     std::string modelName1 = Settings::replaceVars(modelName);
     if (!PvUtil::fileExists(modelName1.c_str()))
     {
       PvUtil::exitError("File %s is not found!",modelName1.c_str());
     }

     mpCascade.load(modelName1.c_str());
*/
	 mpCascade.load(modelName.c_str());

     scaleFactor = scaleF;
     min_nbr = minNbr;
     minsize = minFaceSize;
     maxsize = maxFaceSize;

}


//
// OPERATIONS
//
/*
void 
FaceDetectorAdaBoost::detect(const Image8 &grayImage, bool async)
{
  std::vector<Rectanglei> vecRect;

	// Optimization for reducing the search space given to the face detector.
  Image8 subsampledImage;
  float resampleFactor = 1;

  cv::Size minFaceSize(0,0);

  //CvSize minFaceSize = cvSize(0,0);

  if (mResampleForMinSize)
  {
    resampleFactor = resampleForMinSize(grayImage,subsampledImage);
  }
  else
  {
    minFaceSize = cv::Size(static_cast<int>(mMinFaceSize(0)*resampleFactor),
                         static_cast<int>(mMinFaceSize(1)*resampleFactor));
  }
  
  // These are the parameters for cvHaarDetectObjects
  cv::Size maxFaceSize = cv::Size(static_cast<int>(mMaxFaceSize(0)*resampleFactor),
                              static_cast<int>(mMaxFaceSize(1)*resampleFactor));

  Image8 const *pImg;

  if (resampleFactor < 1.0f)
  {
    pImg = &subsampledImage;
  }
  else
  {
    pImg = &grayImage;
  }

  unsigned char* pixels = pImg->pointer();
  
  assert(pixels!=NULL);
  
  int w=pImg->width();
  int h=pImg->height();
  
  // This face detector won't detect faces smaller than 20x20
  if (w < 20 || h < 20)
  {
    nFaces = 0;
    return;
  }
  
  //  Detection (Image8 to Mat format)
  cv::Mat imgMat(h,w, CV_8UC1, (void*)pImg->pointer());

//  imgMat.data = pixels;


  std::vector<cv::Rect> faces;

  int scale = 1;

  mpCascade.detectMultiScale(imgMat, faces, scaleFactor,min_nbr, 0, cv::Size(minsize,minsize), cv::Size(maxsize,maxsize));


  cv::Rect face_rect;

  int count1 = 0;
  int count2 = 0;

  //  iter = vecRect.begin();
  for(int i = 0; i <  faces.size(); i++ )
  {
	  	  face_rect.x = faces[i].x;
	  	  face_rect.y = faces[i].y;
	  	  face_rect.width = faces[i].width;
	  	  face_rect.height = faces[i].height;

    vecRect.push_back(Rectangle<int>(face_rect.x*scale, face_rect.y*scale, 
      face_rect.width*scale + face_rect.x*scale,
      face_rect.height*scale + face_rect.y*scale));
    int check = 0;
    
    for(int k = 0; k < count1; k++)
    {
      int  xx0 = std::max(vecRect[i].x0,vecRect[k].x0);
      int  yy0 = std::max(vecRect[i].y0,vecRect[k].y0);
      int  xx1 = std::min(vecRect[i].x1,vecRect[k].x1);
      int  yy1 = std::min(vecRect[i].y1,vecRect[k].y1);
      if(((xx1-xx0) > 0)&&((yy1-yy0) > 0))
      {
        if((xx1-xx0)*(yy1-yy0) > 0.5*(vecRect[k].x1-vecRect[k].x0)*(vecRect[k].y1-vecRect[k].y0))
        {
          //  overlap..
          check++;
        }
      }
    }
    if(!check)
    {
      faceArray[count2].x = face_rect.x*scale;
      faceArray[count2].y = face_rect.y*scale;
      faceArray[count2].w = face_rect.width*scale;
      faceArray[count2].h = face_rect.height*scale;
      count2++;
    }
    count1++;
  }

  nFaces = count2;

  // Now we have to scan the array and adjust the sizes so it fits the original image.
  for (int i = 0; i < nFaces; i++)
  {
    faceArray[i].x = (int)(faceArray[i].x/resampleFactor);
    faceArray[i].y = (int)(faceArray[i].y/resampleFactor);
    faceArray[i].w = (int)(faceArray[i].w/resampleFactor);
    faceArray[i].h = (int)(faceArray[i].h/resampleFactor);
  }
}
*/

void
FaceDetectorAdaBoost::detect(const Image8 &grayImage, bool async)
{

  Image8 const *pImg;
  pImg = &grayImage;

  int w=pImg->width();
  int h=pImg->height();

  // This face detector won't detect faces smaller than 20x20
  if (w < 20 || h < 20)
  {
    nFaces = -1;
    return;
  }

  //  Detection (Image8 to Mat format)
  cv::Mat imgMat(h,w, CV_8UC1, (void*)pImg->pointer());

  float resampleFactor = 1;

  std::vector<Rectanglei> vecRect;
  std::vector<cv::Rect> faces;

   mpCascade.detectMultiScale(imgMat, faces, scaleFactor, min_nbr, 0, cv::Size(minsize,minsize), cv::Size(maxsize,maxsize));

   cv::Rect face_rect;

   int scale = 1;
   int count1 = 0;
   int count2 = 0;

   for(unsigned int i = 0; i <  faces.size(); i++ )
   {
 	  face_rect.x = faces[i].x;
 	  face_rect.y = faces[i].y;
 	  face_rect.width = faces[i].width;
 	  face_rect.height = faces[i].height;

 	  vecRect.push_back(Rectangle<int>(face_rect.x*scale, face_rect.y*scale,
       face_rect.width*scale + face_rect.x*scale,
       face_rect.height*scale + face_rect.y*scale));

 	  int check = 0;
 	  for(int k = 0; k < count1; k++)
 	  {
 		  int  xx0 = std::max(vecRect[i].x0,vecRect[k].x0);
 		  int  yy0 = std::max(vecRect[i].y0,vecRect[k].y0);
 		  int  xx1 = std::min(vecRect[i].x1,vecRect[k].x1);
 		  int  yy1 = std::min(vecRect[i].y1,vecRect[k].y1);
 		  if(((xx1-xx0) > 0)&&((yy1-yy0) > 0))
 		  {
 			  if((xx1-xx0)*(yy1-yy0) > 0.5*(vecRect[k].x1-vecRect[k].x0)*(vecRect[k].y1-vecRect[k].y0))
 			  {
 				  //  overlap..
 				  check++;
 			  }
 		  }
 	  }
 	  if(!check)
 	  {
 		  faceArray[count2].x = face_rect.x*scale;
 		  faceArray[count2].y = face_rect.y*scale;
 		  faceArray[count2].w = face_rect.width*scale;
 		  faceArray[count2].h = face_rect.height*scale;
 		  count2++;
 	  }
 	  count1++;
   }

   nFaces = count2;

   // Now we have to scan the array and adjust the sizes so it fits the original image.
   for (unsigned int i = 0; i < nFaces; i++)
   {
     faceArray[i].x = (int)(faceArray[i].x/resampleFactor);
     faceArray[i].y = (int)(faceArray[i].y/resampleFactor);
     faceArray[i].w = (int)(faceArray[i].w/resampleFactor);
     faceArray[i].h = (int)(faceArray[i].h/resampleFactor);
   }
}


void
FaceDetectorAdaBoost::detect(const cv::Mat imgMat, bool async)
{
  std::vector<Rectanglei> vecRect;

  float resampleFactor = 1;
  int w=imgMat.cols;
  int h=imgMat.rows;

  // This face detector won't detect faces smaller than 20x20
  if (w < 20 || h < 20)
  {
    nFaces = 0;
    return;
  }


  std::vector<cv::Rect> faces;

  mpCascade.detectMultiScale(imgMat, faces, scaleFactor,min_nbr, 0, cv::Size(minsize,minsize), cv::Size(maxsize,maxsize));

  cv::Rect face_rect;

  int scale = 1;
  int count1 = 0;
  int count2 = 0;

  for(unsigned int i = 0; i <  faces.size(); i++ )
  {
	  face_rect.x = faces[i].x;
	  face_rect.y = faces[i].y;
	  face_rect.width = faces[i].width;
	  face_rect.height = faces[i].height;

	  vecRect.push_back(Rectangle<int>(face_rect.x*scale, face_rect.y*scale,
      face_rect.width*scale + face_rect.x*scale,
      face_rect.height*scale + face_rect.y*scale));

	  int check = 0;
	  for(int k = 0; k < count1; k++)
	  {
		  int  xx0 = std::max(vecRect[i].x0,vecRect[k].x0);
		  int  yy0 = std::max(vecRect[i].y0,vecRect[k].y0);
		  int  xx1 = std::min(vecRect[i].x1,vecRect[k].x1);
		  int  yy1 = std::min(vecRect[i].y1,vecRect[k].y1);
		  if(((xx1-xx0) > 0)&&((yy1-yy0) > 0))
		  {
			  if((xx1-xx0)*(yy1-yy0) > 0.5*(vecRect[k].x1-vecRect[k].x0)*(vecRect[k].y1-vecRect[k].y0))
			  {
				  //  overlap..
				  check++;
			  }
		  }
	  }
	  if(!check)
	  {
		  faceArray[count2].x = face_rect.x*scale;
		  faceArray[count2].y = face_rect.y*scale;
		  faceArray[count2].w = face_rect.width*scale;
		  faceArray[count2].h = face_rect.height*scale;
		  count2++;
	  }
	  count1++;
  }

  nFaces = count2;

  // Now we have to scan the array and adjust the sizes so it fits the original image.
  for (unsigned int i = 0; i < nFaces; i++)
  {
    faceArray[i].x = (int)(faceArray[i].x/resampleFactor);
    faceArray[i].y = (int)(faceArray[i].y/resampleFactor);
    faceArray[i].w = (int)(faceArray[i].w/resampleFactor);
    faceArray[i].h = (int)(faceArray[i].h/resampleFactor);
  }
}




}; // namespace vision

}; // namespace aitvml


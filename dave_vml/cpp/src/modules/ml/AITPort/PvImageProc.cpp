/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include "modules/ml/AITPort/PvImageProc.hpp"
#include "modules/ml/AITPort/PvImageArcDefs.hpp"
#include "modules/ml/AITPort/AitBaseTypes.hpp"

#include <string.h>	// for memset TODO: set UNIX directive here

#include <limits>

using namespace aitvml;

void PvImageProc::getSpaceRange(colorSpaceConst space, 
                                float& ch1Min, float& ch2Min, float& ch3Min,
                                float& ch1Max, float& ch2Max, float& ch3Max)
{
  switch (space)
  {
  case I1I2I3_SPACE:
    break;
  case RGB_SPACE:
    ch1Min = ch2Min = ch3Min = 0;
    ch1Max = ch2Max = ch3Max = 255;
    break;
  case CHROMA_SPACE:
    ch1Min = ch2Min = ch3Min = 0;
    ch1Max = ch2Max = ch3Max = 1;
    break;
  case CRCGI_SPACE:
    ch1Min = ch2Min = ch3Min = 0;
    ch1Max = ch2Max = ch3Max = 255;
    break;
  case HSV_SPACE:
    ch1Min =ch2Min = ch3Min =0;
    ch1Max = 2*M_PI;//h
    ch2Max = 1.0;    //s
    ch3Max = 255;   //v

//    PvUtil::exitError("Undefined!");
    break;
  case XYZ_SPACE:
    ch1Min = ch2Min = ch3Min = 0;
    convertRGBtoXYZ(255,255,255,ch1Max,ch2Max,ch3Max);
    break;
  default:
    // undefined behavior here; I'm going to ruin your day if you fall into this
	// Going to return NaN and make you deal with it downstream!
	ch1Min = ch2Min = ch3Min = std::numeric_limits<float>::quiet_NaN();
	ch1Max = ch2Max = ch3Max = std::numeric_limits<float>::quiet_NaN();
  }
}

void PvImageProc::ComputeHistogram (unsigned char *pixel, int width, int height,
                         unsigned int *histogram, unsigned int histsize)
{
  unsigned int arraysize;
  unsigned int i;

  memset(histogram, 0, histsize*sizeof(unsigned int));

  arraysize = width*height;
  for (i=0;i<arraysize;i++) {
    if (pixel[i] < histsize)
      (histogram[pixel[i]])++;
  }
} /* iiComputeHistogram */

void PvImageProc::ComputeLookup(unsigned int *histogram, float *lookup, int width,
                     int height, unsigned int histsize)
{
  unsigned int i;
  float        cumprob;
  float        imagearea;

  cumprob = 0.0f;
  memset(lookup, 0, histsize*sizeof(float));
  imagearea = (float)width*(float)height;
  for (i=0; i<histsize; i++) {
    cumprob += (float)histogram[i]/imagearea;
    lookup[i] = cumprob*((float)histsize-1.001f);
  }
} /* iiComputeLookup */

//Equalize The histogram of the image.

void PvImageProc::EqualizeImage(unsigned char *grayimage, int width,
                     int height)
{
  unsigned int      histogram[256];
  float             lookup[256];
  unsigned long int imagearea;
  unsigned long int i;
  
  imagearea = width*height;
  ComputeHistogram(grayimage, width, height, histogram, 256);
  ComputeLookup(histogram, lookup, width, height, 256);
  
  for (i=0;i<imagearea;i++)
    grayimage[i] = (unsigned char)lookup[grayimage[i]];
 
} /* iiEqualizeImage */


void PvImageProc::EqualizeImage(Image8& pvGrayImage){

	int	w=pvGrayImage.width();
	int h=pvGrayImage.height();
	unsigned char* pixels=pvGrayImage.pointer();

	EqualizeImage(pixels, w,h);

};
/*********************************************************************
 * mean()
 *
 * Get mean value of RGB Image
 * 
 *********************************************************************/
Vector3f PvImageProc::mean(const Image32 &img)
{
  unsigned int r=0,g=0,b=0;
  unsigned int s=img.width()*img.height();
  unsigned char *data=(unsigned char *)img.pointer();
  unsigned char *dataEnd=data+4*s;
 
  while(data<dataEnd)
  {
    r+=data[R_IDX];
    g+=data[G_IDX];
    b+=data[B_IDX];
    data+=4;
  }

  return Vector3f(float(r)/s,float(g)/s,float(b)/s);
}


/*********************************************************************
 * meanI1I2I3()
 *
 * Get I1I2I3 mean value of RGB Image
 *********************************************************************/
Vector3f PvImageProc::meanI1I2I3(const Image32 &img)
{
  float mI1,mI2,mI3;
  Vector3f avg=mean(img);

	// convert to I1 I2 I3
  convertRGBtoI1I2I3(avg(0),avg(1),avg(2),mI1,mI2,mI3);

  avg(0) = mI1;
  avg(1) = mI2;
  avg(2) = mI3;
  
  return avg;
}

void PvImageProc::meanAndVariance(const colorSpaceConst space,const Image32 &img, Vector3f &mean, Vector3f &var)
{
  switch(space)
  {
  case I1I2I3_SPACE: meanAndVarianceI1I2I3(img,mean,var); break;
  case RGB_SPACE: meanAndVariance(img,mean,var); break;
  case CHROMA_SPACE: meanAndVarianceChroma(img,mean,var); break;
  case CRCGI_SPACE: meanAndVarianceCrCgI(img,mean,var); break;
  case HSV_SPACE: meanAndVarianceHSV(img,mean,var); break;
  case XYZ_SPACE: meanAndVarianceXYZ(img,mean,var); break;
  default: 
    {
      PvUtil::exitError("Error, invalid color space.");
    }
  }
}

/*********************************************************************
 * meanAndVariance()
 *
 * Get mean and variance of RGB Image
 * 
 *********************************************************************/
void PvImageProc::meanAndVariance(const Image32 &img, Vector3f &avg, Vector3f &var)
{
  float mr=0.0,mg=0.0,mb=0.0;
  float r=0.0,g=0.0,b=0.0;
  float dr,dg,db;
  unsigned int s=img.width()*img.height();
  unsigned char *data=(unsigned char *)img.pointer();
  unsigned char *dataEnd=data+4*s;

  avg=mean(img);

  mr=avg(0);
  mg=avg(1);
  mb=avg(2);

  while(data<dataEnd)
  {
    dr=mr-data[R_IDX];r+=dr*dr;
    dg=mg-data[G_IDX];g+=dg*dg;
    db=mb-data[B_IDX];b+=db*db;
    data+=4;
  }

  var(0)=sqrt(r/(s-1));
  var(1)=sqrt(g/(s-1));
  var(2)=sqrt(b/(s-1));
}

void PvImageProc::meanAndVariance(const Image32 &img, Vector3f &avg, Matrix<float> &var)
{
  float mr=0.0,mg=0.0,mb=0.0;
  //float r=0.0,g=0.0,b=0.0;
  float dr,dg,db;
  unsigned int s=img.width()*img.height();
  unsigned char *data=(unsigned char *)img.pointer();
  unsigned char *dataEnd=data+4*s;

  avg=mean(img);

  mr=avg(0);
  mg=avg(1);
  mb=avg(2);

  var.zero();

  while(data<dataEnd)
  {
    dr=mr-data[R_IDX];
    dg=mg-data[G_IDX];
    db=mb-data[B_IDX];
    var(0,0)+=dr*dr;
    var(1,1)+=dg*dg;
    var(2,2)+=db*db;
    var(0,1)+=dr*dg;
    var(0,2)+=dr*db;
    var(1,2)+=dg*db;
    data+=4;
  }

  var(0,0)=var(0,0)/(s-1);
  var(1,1)=var(1,1)/(s-1);
  var(2,2)=var(2,2)/(s-1);

  var(0,1)=var(0,1)/(s-1);
  var(1,2)=var(1,2)/(s-1);
  var(0,2)=var(0,2)/(s-1);

  var(1,0)=var(0,1);
  var(2,1)=var(1,2);
  var(2,0)=var(0,2);

  var.inv();
}

void PvImageProc::meanAndVarianceChroma(const Image32 &img,Vector3f &avg, Vector3f &var)
{
  float r,g,b;
  float vr,vg,vb;
  float Cr,Cg,Cb;
  float dr,dg,db;

  float mCr=0.0,mCg=0.0,mCb=0.0;
  
  unsigned int s=img.width()*img.height();
  unsigned char *data=(unsigned char *)img.pointer();
  unsigned char *dataEnd=data+4*s;

  // mean
  while(data<dataEnd)
  {
    r = data[R_IDX];
    g = data[G_IDX];
    b = data[B_IDX];

    convertRGBtoChroma(r,g,b,Cr,Cg,Cb);
	      
    mCr+=Cr;
    mCg+=Cg;
    mCb+=Cb;

    data+=4;
  }

  mCr/=s; mCg/=s; mCb/=s;

  avg(0)=mCr; avg(1)=mCg; avg(2)=mCb;

  // variance
  data=(unsigned char *)img.pointer();
  vr=vg=vb=0.0f;
  while(data<dataEnd)
  {
	  r = data[R_IDX];
    g = data[G_IDX];
    b = data[B_IDX];
	
    convertRGBtoChroma(r,g,b,Cr,Cg,Cb);
	
    dr=mCr-Cr;vr+=dr*dr;
    dg=mCg-Cg;vg+=dg*dg;
    db=mCb-Cb;vb+=db*db;

    data+=4;
  }

  var(0)=sqrt(vr/(s-1));
  var(1)=sqrt(vg/(s-1));
  var(2)=sqrt(vb/(s-1));
}

void PvImageProc::meanAndVarianceI1I2I3(const Image32 &img,Vector3f &avg, Vector3f &var)
{
	float mI1,mI2,mI3;
  float r=0.0,g=0.0,b=0.0;
  float R,G,B;
  float fI1,fI2,fI3;
  float dr,dg,db;
  unsigned int s=img.width()*img.height();
  unsigned char *data=(unsigned char *)img.pointer();
  unsigned char *dataEnd=data+4*s;

  avg=meanI1I2I3(img);

  mI1=avg(0);
  mI2=avg(1);
  mI3=avg(2);

  // variance
  data=(unsigned char *)img.pointer();
  while(data<dataEnd)
  {
	  R = data[R_IDX];
    G = data[G_IDX];
    B = data[B_IDX];

    convertRGBtoI1I2I3(R,G,B,fI1,fI2,fI3);

    dr=mI1-fI1;r+=dr*dr;
    dg=mI2-fI2;g+=dg*dg;
    db=mI3-fI3;b+=db*db;
    data+=4;
  }

  var(0)=sqrt(r/(s-1));
  var(1)=sqrt(g/(s-1));
  var(2)=sqrt(b/(s-1));
}

void PvImageProc::meanAndVarianceHSV(const Image32 &img,Vector3f &avg, Vector3f &var)
{
  float r,g,b;
  float ch1,ch2,ch3;
  float d1,d2,d3;

  float v1=0.0f,v2=0.0f,v3=0.0f;
  float m1=0.0f,m2=0.0f,m3=0.0f;
  
  unsigned int s=img.width()*img.height();
  unsigned char *data=(unsigned char *)img.pointer();
  unsigned char *dataEnd=data+4*s;

  // mean
  while(data<dataEnd)
  {
    r = data[R_IDX];
    g = data[G_IDX];
    b = data[B_IDX];

    convertRGBtoHSV(r,g,b,ch1,ch2,ch3);
	      
    m1+=ch1;
    m2+=ch2;
    m3+=ch3;

    data+=4;
  }

  m1/=s; m2/=s; m3/=s;

  // variance
  data=(unsigned char *)img.pointer();
  while(data<dataEnd)
  {
	  r = data[R_IDX];
    g = data[G_IDX];
    b = data[B_IDX];
	
    convertRGBtoHSV(r,g,b,ch1,ch2,ch3);
	
    d1=m1-ch1;v1+=d1*d1;
    d2=m2-ch2;v2+=d2*d2;
    d3=m3-ch3;v3+=d3*d3;

    data+=4;
  }

  var(0)=sqrt(v1/(s-1));
  var(1)=sqrt(v2/(s-1));
  var(2)=sqrt(v3/(s-1));
  
  avg(0)=m1; 
  avg(1)=m2; 
  avg(2)=m3;
}

void PvImageProc::meanAndVarianceCrCgI(const Image32 &img,Vector3f &avg, Vector3f &var)
{
  float ch1,ch2,ch3;
  float d1,d2,d3;

  float v1=0.0f,v2=0.0f,v3=0.0f;
  float m1=0.0f,m2=0.0f,m3=0.0f;
  
  unsigned int s=img.width()*img.height();
  int s1 = s;
  unsigned char *data=(unsigned char *)img.pointer();

  Matrix<Vector3f> transformedImage(img.width(), img.height());
  Vector3f *tData = transformedImage.pointer();

  while(s1-->0)
  {
    convertRGBtoCrCgI(data[R_IDX],data[G_IDX],data[B_IDX],ch1,ch2,ch3);
    m1+=ch1;
    m2+=ch2;
    m3+=ch3;
    tData->set(ch1,ch2,ch3);
    data+=4;
    tData++;
  }

  m1/=s; m2/=s; m3/=s;

  // variance
  tData=transformedImage.pointer();
  s1 = s;
  while(s1-->0)
  {
    d1=m1-(*tData)(0);v1+=d1*d1;
    d2=m2-(*tData)(1);v2+=d2*d2;
    d3=m3-(*tData)(2);v3+=d3*d3;

    tData++;
  }

  var(0)=sqrt(v1/(s-1));
  var(1)=sqrt(v2/(s-1));
  var(2)=sqrt(v3/(s-1));
  
  avg(0)=m1; 
  avg(1)=m2; 
  avg(2)=m3;
}

void PvImageProc::meanAndVarianceXYZ(const Image32 &img,Vector3f &avg, Vector3f &var)
{
  float ch1,ch2,ch3;
  float d1,d2,d3;

  float v1=0.0f,v2=0.0f,v3=0.0f;
  float m1=0.0f,m2=0.0f,m3=0.0f;
  
  unsigned int s=img.width()*img.height();
  int s1 = s;
  unsigned char *data=(unsigned char *)img.pointer();

  Matrix<Vector3f> transformedImage(img.width(), img.height());
  Vector3f *tData = transformedImage.pointer();

  while(s1-->0)
  {
    convertRGBtoXYZ(data[R_IDX],data[G_IDX],data[B_IDX],ch1,ch2,ch3);
    m1+=ch1;
    m2+=ch2;
    m3+=ch3;
    tData->set(ch1,ch2,ch3);
    data+=4;
    tData++;
  }

  m1/=s; m2/=s; m3/=s;

  // variance
  tData=transformedImage.pointer();
  s1 = s;
  while(s1-->0)
  {
    d1=m1-(*tData)(0);v1+=d1*d1;
    d2=m2-(*tData)(1);v2+=d2*d2;
    d3=m3-(*tData)(2);v3+=d3*d3;

    tData++;
  }

  var(0)=sqrt(v1/(s-1));
  var(1)=sqrt(v2/(s-1));
  var(2)=sqrt(v3/(s-1));
  
  avg(0)=m1; 
  avg(1)=m2; 
  avg(2)=m3;
}


/*********************************************************************
 * sumOperatorFast()
 *
 * Calc sum over areas of size (2n+1 x 2n+1).
 * 
 *********************************************************************/
void PvImageProc::sumOperatorFast(Image<int> &img, unsigned int n, SumOperator &sumOp)
{
  unsigned int w=sumOp.w;
  unsigned int h=sumOp.h;
  int *row=sumOp.row;

  unsigned int i,j;

  const unsigned int ns=n, ne=w-n;

  // image data
  int *data=img.pointer();

  // mask width
  const unsigned int m=2*n+1;

  // sum storage
  int *sum=row+(m+1)*w;
  int *sumPtr;
  int *sumStart=sum+ns,*sumEnd=sum+ne;

  // current working accus
  int *row0=row, *rowN=row+m*w;

  // pointer variables
  int *rowPtr;
  int *dataPtr;

  if(sumOp.w != img.width() || sumOp.h != img.height() || sumOp.n!=n)
  {
    PvUtil::exitError("PvImageProc::sumOperatorFast(): Error, allocator is incompatible with function call.");
  }

  // init sum
  for(i=ns;i<ne;i++) sum[i]=0;

  // init row accus
  rowPtr=row;
  dataPtr=data;
  for(i=0;i<m;i++)
  {
    rowSum(dataPtr,rowPtr,n,w);
    for(j=ns;j<ne;j++) sum[j]+=rowPtr[j];
    rowPtr+=w;
    dataPtr+=w;
  }

  // perform averaging
  data=data+w*n;
  for(i=n;i<h-n-1;i++)
  {
    dataPtr=data+n;
    sumPtr=sumStart;
    while(sumPtr<sumEnd)
    {
      *(dataPtr++)=*(sumPtr++);
    }
    rowSum(data+w*(n+1),rowN,n,w);
    sumPtr=sumStart;
    j=ns;
    while(sumPtr<sumEnd)
    {
      *(sumPtr++)+=rowN[j]-row0[j];
      j++;
    }
    data+=w;
    row0+=w;
    rowN+=w;
    if(row0==sum) row0=row;
    else if(rowN==sum) rowN=row;
  }

  // store last result
  dataPtr=data+n;
  sumPtr=sumStart;
  while(sumPtr<sumEnd)
  {
    *(dataPtr++)=*(sumPtr++);
  }
}

/*********************************************************************
 * sumOperatorAlloc()
 *********************************************************************/
void PvImageProc::sumOperatorAlloc(Image<int> &img, const unsigned int n, SumOperator &sumOp)
{
  if(sumOp.isAllocated)
  {
    delete [] sumOp.row;
  }
  sumOp.w=img.width();
  sumOp.h=img.height();
  sumOp.n=n;
  sumOp.row=new int [(3*(2*(n)+3)*(img.width()))];
  sumOp.isAllocated=true;
}

/*********************************************************************
 * sumOperatorFree()
 *********************************************************************/
void PvImageProc::sumOperatorFree(SumOperator &sumOp)
{
  if(sumOp.isAllocated)
  {
    delete [] sumOp.row;
  }
  sumOp.isAllocated=false;
}

/*********************************************************************
 * sumOperator()
 *********************************************************************/
void PvImageProc::sumOperator(Image<int> &img,const int n)
{
  SumOperator sumOp;

  sumOperatorAlloc(img,n,sumOp);

  sumOperatorFast(img,n,sumOp);

  sumOperatorFree(sumOp);
}

// apply mean filter of size (2n+1 x 2n+1)
void PvImageProc::avgOperatorFast(Image32 &img,const int n, AvgOperator &avgOp)
{
  int w=avgOp.w;
  int h=avgOp.h;
  int *row=avgOp.row;

  int i,j;

  // image sizes
  int w3=w*3,w4=w*4;
  int ns=n*3, ne=w3-ns;

  // image data
  unsigned char *data=(unsigned char *)img.pointer();

  // mask width
  int m=2*n+1;

  // mask size
  int s=m*m;


  // sum storage
  int *sum=row+3*(m+1)*w;
  int *sumStart=sum+3*n,*sumEnd=sum+3*(w-n),*sumPtr;

  // current working accus
  int *row0=row, *rowN=row+3*m*w;

  // pointer variables
  int *rowPtr;
  unsigned char *dataPtr;

  // init sum
  for(i=ns;i<ne;i++) sum[i]=0;

  // init row accus
  rowPtr=row;
  dataPtr=data;
  for(i=0;i<m;i++)
  {
    rowSumRGB(dataPtr,rowPtr,n,w);
    for(j=ns;j<ne;j++) sum[j]+=rowPtr[j];
    rowPtr+=w3;
    dataPtr+=w4;
  }

  //for(j=0;j<3*w;j++) PVMSG("%d ",sum[j]);

  // perform averaging
  data=data+w4*n;
  for(i=n;i<h-n-1;i++)
  {
    dataPtr=data+4*n;
    sumPtr=sumStart;
    while(sumPtr<sumEnd)
    {
      *(dataPtr++)=*(sumPtr++)/s;
      *(dataPtr++)=*(sumPtr++)/s;
      *(dataPtr++)=*(sumPtr++)/s;
      dataPtr++;
    }
    rowSumRGB(data+w4*(n+1),rowN,n,w);
    sumPtr=sumStart;
    j=ns;
    while(sumPtr<sumEnd)
    {
      *(sumPtr++)+=rowN[j]-row0[j];
      j++;
    }
    data+=w4;
    row0+=w3;
    rowN+=w3;
    if(row0==sum) row0=row;
    else if(rowN==sum) rowN=row;
  }

  // store last result
  dataPtr=data+4*n;
  sumPtr=sumStart;
  while(sumPtr<sumEnd)
  {
    *(dataPtr++)=*(sumPtr++)/s;
    *(dataPtr++)=*(sumPtr++)/s;
    *(dataPtr++)=*(sumPtr++)/s;
    dataPtr++;
  }
}


/*********************************************************************
 * avgOperatorAlloc()
 *********************************************************************/
void PvImageProc::avgOperatorAlloc(Image32 &img, const int n, AvgOperator &avgOp)
{
  if(avgOp.isAllocated)
  {
    delete [] avgOp.row;
  }
  avgOp.w=img.width();
  avgOp.h=img.height();
  avgOp.row=new int [(3*(2*(n)+3)*(img.width()))];
  avgOp.isAllocated=true;
}

/*********************************************************************
 * avgOperatorFree()
 *********************************************************************/
void PvImageProc::avgOperatorFree(AvgOperator &avgOp)
{
  if(avgOp.isAllocated)
  {
    delete [] avgOp.row;
  }
  avgOp.isAllocated=false;
}

// apply averaging filter of size (2n+1 x 2n+1)
void PvImageProc::avgOperator(Image32 &img,const int n)
{
  AvgOperator avgOp;

  avgOperatorAlloc(img,n,avgOp);

  avgOperatorFast(img,n,avgOp);

  avgOperatorFree(avgOp);
}

void PvImageProc::subSample(const Image32 &img0, Image32 &img1,const int level)
{
  /*
  if(level==1)
  {
    subSample(img0,img1);
    return;
  }
  */

  // This is less efficient, even using IPP. There should be a special case for 
  // subsample in the rescale function.
  //img1.rescale(img0,img0.width()<<level,img0.height()<<level,RESCALE_SKIP);

  int w=img0.width()>>level, h=img0.height()>>level, s=w*h;
  unsigned int skip=1<<level,skipRow=(skip-1)*img0.width();
  unsigned int *data0=img0.pointer();
  unsigned int *data1;
  unsigned int *dataEnd,*dataRowEnd;

  if(level==0)
  {
    img1=img0;
    return;
  }

  img1.resize(w,h);

  data1=img1.pointer();
  dataEnd=data1+s;

  while(data1<dataEnd)
  {
    dataRowEnd=data1+w;
    while(data1<dataRowEnd)
    {
      *(data1++)=*data0;
      data0+=skip;
    }
    data0+=skipRow;
  }
}

//New and Improved!
void PvImageProc::subSample(const Image32 &img0, Image32 &img1)
{
  int w=img0.width()>>1, h=img0.height()>>1, s=w*h;

  unsigned int skipRow=img0.width()*4;

  unsigned char *data0;
  unsigned char *data1;
  unsigned char *dataEnd,*dataRowEnd;


  data0 = (unsigned char *)img0.pointer();

  img1.resize(w,h);

  data1=(unsigned char *)img1.pointer();
  dataEnd=data1+s*4;

  unsigned int r,g,b;

  while(data1<dataEnd)
  {
    dataRowEnd=data1+(w*4);
    while(data1<dataRowEnd)
    {
      r=data0[R_IDX];
      r+=data0[R_IDX+4];
      r+=data0[R_IDX+skipRow];
      r+=data0[R_IDX+skipRow+4];
      r=r>>2;
      g=data0[G_IDX];
      g+=data0[G_IDX+4];
      g+=data0[G_IDX+skipRow];
      g+=data0[G_IDX+skipRow+4];
      g=g>>2;
      b=data0[B_IDX];
      b+=data0[B_IDX+4];
      b+=data0[B_IDX+skipRow];
      b+=data0[B_IDX+skipRow+4];
      b=b>>2;
      *data1=PV_RGB(r,g,b);
      
      data1+=4;
      data0+=8;
    }
    data0+=skipRow;
  }
}

/*********************************************************************
 * Distance Measurements
 *********************************************************************/
void PvImageProc::measure(const colorSpaceConst space,Image32 &img, Vector3f &mean, Vector3f &var, 
	  short int metric, Image<unsigned short> &distImg)
{
  switch(space)
  {
  case I1I2I3_SPACE: measureI1I2I3(img,mean,var,metric,distImg); break;
  case RGB_SPACE: measureRGB(img,mean,var,metric,distImg); break;
  case CHROMA_SPACE: measureChroma(img,mean,var,metric,distImg); break;
  case CRCGI_SPACE: measureCrCgI(img,mean,var,metric,distImg); break;
  case HSV_SPACE: measureHSV(img,mean,var,metric,distImg); break;
  case XYZ_SPACE: measureXYZ(img,mean,var,metric,distImg); break;
  default: 
    {
      PvUtil::exitError("Error, invalid color space.");
    }
  }
}

void PvImageProc::measureRGB(Image32 &img, Vector3f &RGBmean, Vector3f &RGBvar, 
	  short int metric, Image<unsigned short> &distImg)
{
	unsigned char *ptrImg = (unsigned char *) img.pointer();
	int w = img.width();
	int h = img.height();
	unsigned char *ptrEnd = (unsigned char *) ptrImg + 4*w*h;
	unsigned short *ptrDist;

	float R,G,B;
	float distance;
  const float oneStdDev=1.0f/1.0f;
	float iVarR = oneStdDev/RGBvar(0)/RGBvar(0);
	float iVarG = oneStdDev/RGBvar(1)/RGBvar(1);
	float iVarB = oneStdDev/RGBvar(2)/RGBvar(2);

	float MeanR = RGBmean(0);
	float MeanG = RGBmean(1);
	float MeanB = RGBmean(2);
	float t0,t1,t2;

	distImg.resize(w,h);
	ptrDist	= distImg.pointer();
	// this can be made faster, but it would assume a specific RGB order
	switch(metric)
	{
	case METRIC_MAHAL:
	while(ptrImg<ptrEnd)
	{
		// collect data
		R = (float)ptrImg[R_IDX];
		G = (float)ptrImg[G_IDX];
		B = (float)ptrImg[B_IDX];

		// perform distance measurement
		t0 = (R-MeanR);
		t1 = (G-MeanG);
		t2 = (B-MeanB);

    //PVMSG("%g %g %g, %g %g %g, diff=%g %g %g\n",R,G,B,MeanR,MeanG,MeanB,t0,t1,t2);

		distance = 16000.0f*sqrt(t0*t0*iVarR+t1*t1*iVarG+t2*t2*iVarB);

    //PVMSG("%g\n",distance);

    *ptrDist = distance<=65535 ? (unsigned short) distance : 65535;

		// increment
		ptrDist++;
		ptrImg+=4;
	}
	break;
	case METRIC_PROB:

    while(ptrImg<ptrEnd)
	{
		// collect data
		R = (float)ptrImg[R_IDX];
		G = (float)ptrImg[G_IDX];
		B = (float)ptrImg[B_IDX];

		// perform distance measurement
		t0 = (R-MeanR);
		t1 = (G-MeanG);
		t2 = (B-MeanB);

    //PVMSG("%g %g %g, %g %g %g, diff=%g %g %g\n",R,G,B,MeanR,MeanG,MeanB,t0,t1,t2);

		*ptrDist = (Uint16)(65535.0*exp(-0.5*(t0*t0*iVarR+t1*t1*iVarG+t2*t2*iVarB)));

    //PVMSG("%d\n",*ptrDist);

		// increment
		ptrDist++;
		ptrImg+=4;
	}
	break;
	};


}

void PvImageProc::measureRGB(Image32 &img, Vector3f &RGBmean, Matrix<float> &RGBvar, 
	  short int metric, Image<unsigned short> &distImg)
{
	unsigned char *ptrImg = (unsigned char *) img.pointer();
	int w = img.width();
	int h = img.height();
	unsigned char *ptrEnd = (unsigned char *) ptrImg + 4*w*h;
	unsigned short *ptrDist;

	float R,G,B;
	float distance;

  float iVarR = RGBvar(0,0);
	float iVarG = RGBvar(1,1);
	float iVarB = RGBvar(2,2);
  float iVarRG = RGBvar(0,1);
	float iVarRB = RGBvar(0,2);
	float iVarGB = RGBvar(1,2);

	float MeanR = RGBmean(0);
	float MeanG = RGBmean(1);
	float MeanB = RGBmean(2);
	float t0,t1,t2;

	distImg.resize(w,h);
	ptrDist	= distImg.pointer();
	// this can be made faster, but it would assume a specific RGB order
	switch(metric)
	{
	case METRIC_MAHAL:
	while(ptrImg<ptrEnd)
	{
		// collect data
		R = (float)ptrImg[R_IDX];
		G = (float)ptrImg[G_IDX];
		B = (float)ptrImg[B_IDX];

		// perform distance measurement
		t0 = (R-MeanR);
		t1 = (G-MeanG);
		t2 = (B-MeanB);

    //PVMSG("%g %g %g, %g %g %g, diff=%g %g %g\n",R,G,B,MeanR,MeanG,MeanB,t0,t1,t2);

		distance = 16000.0f*sqrt(t0*t0*iVarR+t1*t1*iVarG+t2*t2*iVarB
                           +t0*t1*iVarRG*2+t0*t2*iVarRB*2+t1*t2*iVarGB*2);

    //PVMSG("%g\n",distance);

    *ptrDist = distance<=65535 ? (unsigned short) distance : 65535;

		// increment
		ptrDist++;
		ptrImg+=4;
	}
	break;
	};
}

void PvImageProc::measureI1I2I3(Image32 &img, Vector3f &Imean, Vector3f &Ivar, 
	  short int metric, Image<unsigned short> &distImg)
{
	unsigned char *ptrImg = (unsigned char *) img.pointer();
	int w = img.width();
	int h = img.height();
	unsigned char *ptrEnd = (unsigned char *) ptrImg + 4*w*h;
	unsigned short *ptrDist; 
	unsigned char R,G,B;
  const float oneStdDev=1.0f/1.0f;
	float iVarR=oneStdDev/Ivar(0)/Ivar(0);
	float iVarG=oneStdDev/Ivar(1)/Ivar(1);
	float iVarB=oneStdDev/Ivar(2)/Ivar(2);
	float MeanR=Imean(0);
	float MeanG=Imean(1);
	float MeanB=Imean(2);
	float fI1,fI2,fI3;
	float t0,t1,t2;
	float distance;

	distImg.resize(w,h);
	ptrDist	= distImg.pointer();
	// this can be made faster, but it would assume a specific RGB order
	switch(metric)
	{
	case METRIC_MAHAL:
	while(ptrImg<ptrEnd)
	{
		R=ptrImg[R_IDX];
		G=ptrImg[G_IDX];
		B=ptrImg[B_IDX];

    convertRGBtoI1I2I3(R,G,B,fI1,fI2,fI3);

    // perform distance measurement
		t0 = (fI1-MeanR);
		t1 = (fI2-MeanG);
		t2 = (fI3-MeanB);

		distance = 16000.0f*sqrt(t0*t0*iVarR+t1*t1*iVarG+t2*t2*iVarB);
		*ptrDist = (Uint16)(65535.0f*exp(-0.5f*(t0*t0*iVarR+t1*t1*iVarG+t2*t2*iVarB)));

		//PVMSG("Distance %f\n",distance);
		// assign
    *ptrDist = distance<=65535 ? (unsigned short) distance : 65535;
		// increment
		ptrDist++;
		ptrImg+= 4;
	}
	break;
	case METRIC_PROB:
	while(ptrImg<ptrEnd)
	{
		R=ptrImg[R_IDX];
		G=ptrImg[G_IDX];
		B=ptrImg[B_IDX];

    convertRGBtoI1I2I3(R,G,B,fI1,fI2,fI3);

    // perform distance measurement
		t0 = (fI1-MeanR);
		t1 = (fI2-MeanG);
		t2 = (fI3-MeanB);

		*ptrDist = (Uint16)(65535.0*exp(-0.5*(t0*t0*iVarR+t1*t1*iVarG+t2*t2*iVarB)));

    ptrDist++;
		ptrImg+= 4;
	}
	break;
	};
}

void PvImageProc::measureChroma(Image32 &img, Vector3f &mean, Vector3f &var, 
	                           short int metric, Image<unsigned short> &distImg)
{
	int w = img.width(),h = img.height();
	unsigned char *ptrImg = (unsigned char *) img.pointer();
	unsigned char *ptrEnd = (unsigned char *) ptrImg + 4*w*h;
	unsigned short *ptrDist; 
	unsigned char R,G,B;
	float ch0,ch1,ch2;

  const float oneStdDev=1.0f/1.0f;
	float iVar0=oneStdDev/var(0)/var(0);
	float iVar1=oneStdDev/var(1)/var(1);
	float iVar2=oneStdDev/var(2)/var(2);

  float Mean0=mean(0);
	float Mean1=mean(1);
	float Mean2=mean(2);

  float t0,t1,t2;
	float distance;

	distImg.resize(w,h);

  ptrDist	= distImg.pointer();
	
  // this can be made faster, but it would assume a specific RGB order
  switch(metric)
  {
  case METRIC_MAHAL:
    while(ptrImg<ptrEnd)
    {
      R = ptrImg[R_IDX];
      G = ptrImg[G_IDX];
      B = ptrImg[B_IDX];

      // convert to chromacity scale
      convertRGBtoChroma(R,G,B,ch0,ch1,ch2);
      
      // perform distance measurement
      t0 = (ch0-Mean0);
      t1 = (ch1-Mean1);
      t2 = (ch2-Mean2);
      
      distance = 16000.0f*sqrt(t0*t0*iVar0+t1*t1*iVar1+t2*t2*iVar2);
      
      *ptrDist = distance<=65535 ? (unsigned short) distance : 65535;
      
      ptrDist++;
      ptrImg+=4;
    }
    break;
  };
}

void PvImageProc::measureHSV(Image32 &img, Vector3f &mean, Vector3f &var, 
	                           short int metric, Image<unsigned short> &distImg)
{
	int w = img.width(),h = img.height();
	unsigned char *ptrImg = (unsigned char *) img.pointer();
	unsigned char *ptrEnd = (unsigned char *) ptrImg + 4*w*h;
	unsigned short *ptrDist; 
	unsigned char R,G,B;
	float ch0,ch1,ch2;

  const float oneStdDev=1.0f/1.0f;
	float iVar0=oneStdDev/var(0)/var(0);
	float iVar1=oneStdDev/var(1)/var(1);
	float iVar2=oneStdDev/var(2)/var(2);

  float Mean0=mean(0);
	float Mean1=mean(1);
	float Mean2=mean(2);

  float t0,t1,t2;
	float distance;

	distImg.resize(w,h);

  ptrDist	= distImg.pointer();
	
  // this can be made faster, but it would assume a specific RGB order
  switch(metric)
  {
  case METRIC_MAHAL:
    while(ptrImg<ptrEnd)
    {
      R = ptrImg[R_IDX];
      G = ptrImg[G_IDX];
      B = ptrImg[B_IDX];

      // convert to chromacity scale
      convertRGBtoHSV(R,G,B,ch0,ch1,ch2);
      
      // perform distance measurement
      t0 = (ch0-Mean0);
      t1 = (ch1-Mean1);
      t2 = (ch2-Mean2);
      
      distance = 16000.0f*sqrt(t0*t0*iVar0+t1*t1*iVar1+t2*t2*iVar2);
      
      *ptrDist = distance<=65535 ? (unsigned short) distance : 65535;
      
      ptrDist++;
      ptrImg+=4;
    }
    break;
  };
}

void PvImageProc::measureCrCgI(Image32 &img, Vector3f &mean, Vector3f &var, 
	                           short int metric, Image<unsigned short> &distImg)
{
	int w = img.width(),h = img.height();
	unsigned char *ptrImg = (unsigned char *) img.pointer();
	unsigned char *ptrEnd = (unsigned char *) ptrImg + 4*w*h;
	unsigned short *ptrDist; 
	unsigned char R,G,B;
	float ch0,ch1,ch2;

  const float oneStdDev=1.0f/1.0f;
	float iVar0=oneStdDev/var(0)/var(0);
	float iVar1=oneStdDev/var(1)/var(1);
	float iVar2=oneStdDev/var(2)/var(2);

  float Mean0=mean(0);
	float Mean1=mean(1);
	float Mean2=mean(2);

  float t0,t1,t2;
	float distance;

	distImg.resize(w,h);

  ptrDist	= distImg.pointer();
	
  // this can be made faster, but it would assume a specific RGB order
  switch(metric)
  {
  case METRIC_MAHAL:
    while(ptrImg<ptrEnd)
    {
      R = ptrImg[R_IDX];
      G = ptrImg[G_IDX];
      B = ptrImg[B_IDX];

      // convert to chromacity scale
      convertRGBtoCrCgI(R,G,B,ch0,ch1,ch2);
      
      // perform distance measurement
      t0 = (ch0-Mean0);
      t1 = (ch1-Mean1);
      t2 = (ch2-Mean2);
      
      distance = 16000.0f*sqrt(t0*t0*iVar0+t1*t1*iVar1+t2*t2*iVar2);
      
      *ptrDist = distance<=65535 ? (unsigned short) distance : 65535;
      
      ptrDist++;
      ptrImg+=4;
    }
    break;
  };
}

void PvImageProc::measureXYZ(Image32 &img, Vector3f &mean, Vector3f &var, 
	                           short int metric, Image<unsigned short> &distImg)
{
	int w = img.width(),h = img.height();
	unsigned char *ptrImg = (unsigned char *) img.pointer();
	unsigned char *ptrEnd = (unsigned char *) ptrImg + 4*w*h;
	unsigned short *ptrDist; 
	unsigned char R,G,B;
	float ch0,ch1,ch2;

  const float oneStdDev=1.0f/1.0f;
	float iVar0=oneStdDev/var(0)/var(0);
	float iVar1=oneStdDev/var(1)/var(1);
	float iVar2=oneStdDev/var(2)/var(2);

  float Mean0=mean(0);
	float Mean1=mean(1);
	float Mean2=mean(2);

  float t0,t1,t2;
	float distance;

	distImg.resize(w,h);

  ptrDist	= distImg.pointer();
	
  // this can be made faster, but it would assume a specific RGB order
  switch(metric)
  {
  case METRIC_MAHAL:
    while(ptrImg<ptrEnd)
    {
      R = ptrImg[R_IDX];
      G = ptrImg[G_IDX];
      B = ptrImg[B_IDX];

      // convert to chromacity scale
      convertRGBtoXYZ(R,G,B,ch0,ch1,ch2);
      
      // perform distance measurement
      t0 = (ch0-Mean0);
      t1 = (ch1-Mean1);
      t2 = (ch2-Mean2);
      
      distance = 16000.0f*sqrt(t0*t0*iVar0+t1*t1*iVar1+t2*t2*iVar2);
      
      *ptrDist = distance<=65535 ? (unsigned short) distance : 65535;
      
      ptrDist++;
      ptrImg+=4;
    }
    break;
  };
}

void PvImageProc::ContrastEnhance(int* input,int size,int low,int high)
{
  PVASSERT(low < high);
  PVASSERT(low >= 0);
  PVASSERT(high <= 255);

  int i; //Loop variable

  //Find maximum and minimum values.
  int min = minimum(input,size);
  int max = maximum(input,size);

  if(max == min) //i.e. all values are the same, return the array as it is.
    return;

  //Works only for 8 bit images(0-255)
  //Begin
  //for(i=0;i<size;i++)
  //{
    //input[i] = (255- input[i])/(float)(max - min);
  //}
  //End
  //Works only for 8 bit images(0-255)
  

  //More general portion is below.
  float m,c;
  
  m = ((float)(high-low))/(max - min);
  c = (float)low - (m*min);

  for(i=0;i<size;i++)
  {
    if(input[i] == min)
      input[i] = low;
    else if(input[i] == max)
      input[i] = high;
    else
      input[i] = (Int)(m * input[i] + c);
  }
  
  return;
}

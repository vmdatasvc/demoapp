//#include "opencv2/opencv.hpp"
//#include "cv.h"
//#include "cv.hpp"
//#include "cvaux.h"
//#include "highgui.h"

#include "math.h"
#include "modules/ml/AITPort/Image.hpp"
#include "modules/ml/AITPort/OpenCvUtils.hpp"
#include "modules/ml/AITPort/ColorConvert.hpp"
#include "modules/ml/AITPort/SkinToneDetector.hpp"
#include "modules/ml/AITPort/PvImageProc.hpp"
//#include "ipp.h"

#include <limits>

namespace aitvml
{

SkinToneDetector::SkinToneDetector()
{
  //CSA HACK

  CvSize size;
  size.width = 5;
  size.height = 5;

  mTemp = cvCreateImage(size, IPL_DEPTH_8U, 3 );
  mTempIn = cvCreateImage(size, IPL_DEPTH_8U, 3 );
  mTempOut = cvCreateImage(size, IPL_DEPTH_8U, 1 );

  //CSAHACK
  mSkinRegion = cvCreateImage(size, IPL_DEPTH_8U, 1);
}

SkinToneDetector::~SkinToneDetector()
{

}

void
SkinToneDetector::findSkinTone( const IplImage *image, float radius)
{
  CvSize size = cvGetSize(image);

  cvReleaseImage( &mTemp );
  
  mTemp = cvCreateImage( size, IPL_DEPTH_8U, 3 );
  cvCvtColor( image, mTemp, CV_RGB2YCrCb );
  
  uchar *color_data = 0;
//  uchar *color_data2 = 0;
  int color_step = 0;
  cvGetRawData( mTemp, &color_data, &color_step, &size ); 
  
  uchar *skin_data = 0;
  int skin_step = 0;
  
  cvReleaseImage( &mSkinRegion);
  mSkinRegion = cvCreateImage(size, IPL_DEPTH_8U, 1);
  cvGetRawData( mSkinRegion, &skin_data, &skin_step, &size );
  double skinScore;
  double cost, sint;
  cost = cos(Theta);
  sint = sin(Theta);
  double A2, B2;
  A2 = Af*Af;
  B2 = Bf*Bf;
  float fac = 255.0/radius;

  for( ; size.height--; color_data += color_step, skin_data += skin_step)
  {
    for( int x = 0; x < size.width; x++ )
    {
      unsigned char Y = color_data[x*3];
      unsigned char Cr = color_data[x*3+1];
      unsigned char Cb = color_data[x*3+2];
//      printf("%d %d %d\n",Y,Cr,Cb);
      double TCr = TransCr(Y, Cr);
      double TCb = TransCb(Y, Cb);

      double xx = cost*(TCb - Cx) + sint*(TCr - Cy);
      double yy = -sint*(TCb - Cx) + cost*(TCr - Cy); 

      skinScore = (xx - ECx)*(xx - ECx)/A2 + (yy - ECy)*(yy - ECy)/B2;
//      printf("%f ",skinScore);
//      uchar val = (uchar)(255-127*skinScore);
//     skin_data[x] = (uchar)MAX(val, 0);

      //if(skinScore<=radius)
      //  skin_data[x] = (uchar)255;
      //else
      //  skin_data[x] = (uchar)0;

      if(skinScore<=radius)
		  skin_data[x] = (uchar)(fac*(radius-skinScore));
	  else
		  skin_data[x] = (uchar)0;
    }
  }

  cvErode(mSkinRegion, mSkinRegion);
  cvDilate(mSkinRegion, mSkinRegion);
}

void
SkinToneDetector::findSkinTone( const Image32 *pvImage , Image8 *pvSkinRegion, float radius, float downSampleFactor)
{
  mInputImage = aitvml::vision::OpenCvUtils::convertPvImageToIpl(*pvImage);

  CvSize size;
  size.width = (int)(pvImage->width()/downSampleFactor);
  size.height = (int)(pvImage->height()/downSampleFactor);

  cvReleaseImage( &mTempIn );
  mTempIn = cvCreateImage( size, IPL_DEPTH_8U, 3 );
  cvResize(mInputImage, mTempIn, CV_INTER_NN);

  findSkinTone(mTempIn, radius);

  cvReleaseImage(&mInputImage);

  size.width = pvImage->width();
  size.height = pvImage->height();

  cvReleaseImage( &mTempOut );
  mTempOut = cvCreateImage( size, IPL_DEPTH_8U, 1 );
  cvResize(mSkinRegion, mTempOut, CV_INTER_NN);

  aitvml::vision::OpenCvUtils::copyIplToPvImage(mTempOut,*pvSkinRegion);
}

void
SkinToneDetector::pvFindSkinTone(const Image32 *pvImage, Image8 *skinRegion)
{
  int w = pvImage->width();
  int h = pvImage->height();

  Image32 convertImage(w, h);

  Rectanglei roi(0, 0, w, h);

  aitvml::ColorConvert::convert(*pvImage, PvImageProc::RGB_SPACE, convertImage, PvImageProc::YCBCR_SPACE, roi);
  unsigned int* pixels = convertImage.pointer();
  
  double skinScore;
  double cost, sint;
  cost = cos(Theta);
  sint = sin(Theta);
  double A2, B2;
  A2 = Af*Af;
  B2 = Bf*Bf;

  for( int y = 0; y < h; y++)
  {
    for( int x = 0; x < w; x++ )
    {

      unsigned int pixel = pixels[y*pvImage->width() + x];
      unsigned char Y = (((pixel)) & 0xff);
      unsigned char Cr = (((pixel)>>8) & 0xff);
      unsigned char Cb = (((pixel)>>16) & 0xff);

//      printf("%d %d %d\n",Y,Cr,Cb);
      double TCr = TransCr(Y, Cr);
      double TCb = TransCb(Y, Cb);

      double xx = cost*(TCb - Cx) + sint*(TCr - Cy);
      double yy = -sint*(TCb - Cx) + cost*(TCr - Cy); 

      skinScore = (xx - ECx)*(xx - ECx)/A2 + (yy - ECy)*(yy - ECy)/B2;
//      printf("%f ",skinScore);
//      uchar val = (uchar)(255-127*skinScore);
//     skin_data[x] = (uchar)MAX(val, 0);
      if(skinScore<=1.0)
        (*skinRegion)(x,y) = (uchar)255;
      else
        (*skinRegion)(x,y) = (uchar)0;
    }
  }

  Vector2i maskSize(3, 3);
  erode(*skinRegion, maskSize);
  dilate(*skinRegion, maskSize);

//  Image8 tmp1(*skinRegion);
//  erode(tmp1, *skinRegion, maskSize); 
//  Image8 tmp2(*skinRegion);
//  dilate(tmp2, *skinRegion, maskSize);

}

double 
SkinToneDetector::MeanCr(unsigned char Y)
{
  if(Y < K_l)
    return 154.0 + 10.0*(K_l - Y)/(K_l - Y_min);
  else if(K_h <= Y)
    return 154.0 + 22.0*(Y - K_h)/(Y_max - K_h);

  // return a very bad number.  Should not get here
  return std::numeric_limits<double>::quiet_NaN();
}

double 
SkinToneDetector::MeanCb(unsigned char Y)
{
  if(Y < K_l)
    return 108 + 10*(K_l - Y)/(K_l - Y_min);
  else if(K_h <= Y)
    return 108 + 10*(Y - K_h)/(Y_max - K_h);

  // return a very bad number.  Should not get here
  return std::numeric_limits<double>::quiet_NaN();
}

double 
SkinToneDetector::WidthCr(unsigned char Y)
{
  if(Y < K_l)
    return WL_Cr + (Y - Y_min)*(W_Cr - WL_Cr)/(K_l - Y_min);
  else if(K_h < Y)
    return WH_Cr + (Y_max - Y)*(W_Cr - WH_Cr)/(Y_max - K_h);

  // return a very bad number.  Should not get here
  return std::numeric_limits<double>::quiet_NaN();
}

double 
SkinToneDetector::WidthCb(unsigned char Y)
{
  if(Y < K_l)
    return WL_Cb + (Y - Y_min)*(W_Cb - WL_Cb)/(K_l - Y_min);
  else if(K_h < Y)
    return WH_Cb + (Y_max - Y)*(W_Cb - WH_Cb)/(Y_max - K_h);

  // return a very bad number.  Should not get here
  return std::numeric_limits<double>::quiet_NaN();
}

double 
SkinToneDetector::TransCr(unsigned char Y, unsigned char Cr)
{
  if( K_l < Y && Y < K_h)
    return Cr;
  else
    return (Cr - MeanCr(Y)) * (W_Cr / WidthCr(Y)) + MeanCr(K_h);
}

double 
SkinToneDetector::TransCb(unsigned char Y, unsigned char Cb)
{
  if( K_l < Y && Y < K_h)
    return Cb;
  else
    return (Cb - MeanCb(Y)) * (W_Cb / WidthCb(Y)) + MeanCb(K_h);
}

} // namespace aitvml

/* End of file. */

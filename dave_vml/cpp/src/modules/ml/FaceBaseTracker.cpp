/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * FaceBaseTracker.cpp
 *
 *  Created on: Jan 26, 2016
 *      Author: dkim
 */

#include <opencv2/video/tracking.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "core/Image.hpp"
#include "modules/ml/FaceBaseTracker.hpp"
#include <modules/ml/FaceVideoModule.hpp>
#include <modules/ml/FaceDetector.hpp>
#include <modules/ml/mlfeature.hpp>

//#include "dlib/opencv.h"

//#define DEBUG
//#define FACETRACKERS_DEBUG_SHOW

#ifdef DEBUG
// for debugging
#include "modules/utility/Util.hpp"
#endif

namespace vml {

FaceBaseTracker::FaceBaseTracker(boost::shared_ptr<Settings> settings):
		mSearchWindowRatio(settings->getFloat("FaceTracker/searchWindowRatio",2.f,false)),
		rows(0),
		cols(0),
		mArea(0),
		mConfidence(0.f),
		mRegionStatus(FaceTrackingStatus_InStart),
		mTrackStatus(FaceTrackingStatus_Initialized),
		mKF(cv::KalmanFilter(4,2,0)),
		mInactiveFrameCount(0),
		mcInactiveTimeout(settings->getInt("FaceTracker/inactiveTimeout",3,false))
		//mcReactivateLikelihoodThreshold(settings->getFloat("FaceTracker/reactivateLikelihoodThreshold",0.0f,false)),

{

	mpVideoModule = FaceModuleManager::instance()->getVideoModule();
	mpFaceDetector = FaceModuleManager::instance()->getDetector();

	// For reactivation
	mpFeature.reset(new vml::MLFeature(settings));


	mUseKF = settings->getBool("FaceTracker/doKalmanFilter",false,false);

	if(mUseKF)
	{
		mKFInitialStateUncertaintyLoc = settings->getFloat("FaceTracker/KalmanFilter/InitialStateUncertaintyLoc",2.f,false);
		// need to square these cause these are covariance
		mKFInitialStateUncertaintyLoc *= mKFInitialStateUncertaintyLoc;
		mKFInitialStateUncertaintyVel = settings->getFloat("FaceTracker/KalmanFilter/InitialStateUncertaintyVel",1.f,false);
		// need to square these cause these are covariance
		mKFInitialStateUncertaintyVel *= mKFInitialStateUncertaintyVel;

		mKFProcessNoiseLoc = settings->getFloat("FaceTracker/KalmanFilter/ProcessNoiseLoc",2.f,false);
		mKFProcessNoiseLoc *= mKFProcessNoiseLoc;

		mKFProcessNoiseVel = settings->getFloat("FaceTracker/KalmanFilter/ProcessNoiseVel",1.f,false);
		mKFProcessNoiseVel *= mKFProcessNoiseVel;

		mKFMeasurementNoise = settings->getFloat("FaceTracker/KalmanFilter/MeasurementNoise",1.f,false);
		mKFMeasurementNoise *= mKFMeasurementNoise;
	}
}

void FaceBaseTracker::initialize(boost::shared_ptr<FaceDetectorTarget> dt)
{
	//double start_time = gCurrentTime();

	mCenter = dt->getCenter();

	if(mUseKF)
	{
		initializeKalman(dt->bounding_box);
	}

	if (mpVideoModule->insideTrackRegion(mCenter))
	{
		mRegionStatus = FaceTrackingStatus_InTrack;
	}
	else if (mpVideoModule->insideStartKillRegion(mCenter))
	{
		mRegionStatus = FaceTrackingStatus_InStart;
	}
	else
	{
		mRegionStatus = FaceTrackingStatus_Invalid;
	}

	// update crop rect
	mTrackBox = dt->bounding_box;
	updateSearchWindow();

	//double update_time = gCurrentTime();
//	cv::Rect tBoundingRect = dt->bounding_box;
//	mCropRect.width = (int)(tBoundingRect.width*mSearchWindowRatio);
//	mCropRect.height = (int)(tBoundingRect.height*mSearchWindowRatio);
//	mCropRect.x = tBoundingRect.x + (int)(0.5*(1.f-mSearchWindowRatio)*(float)tBoundingRect.width);
//	mCropRect.y = tBoundingRect.y + (int)(0.5*(1.f-mSearchWindowRatio)*(float)tBoundingRect.height);
	// make sure crop rect is inside image
	mpVideoModule->enforceWindowRect(mCropRect);

	// call subclass initialization
	this->initializeTrackerFeature(dt);

	//double initTracker_time = gCurrentTime();


	//printf("TIME: updateSearchWindow (for Creation): %f\n", update_time -start_time);
	//printf("TIME: initializeTrackerFeature (for Creation): %f\n",initTracker_time- update_time);
}

void FaceBaseTracker::initializeKalman(cv::Rect trackRect)
{

	float	faceRadiusPixel = (float)trackRect.width/2.f;

	mKFInitialStateUncertaintyLoc *= faceRadiusPixel;
	mKFInitialStateUncertaintyVel *= faceRadiusPixel;
	mKFProcessNoiseLoc *= faceRadiusPixel;
	mKFProcessNoiseVel *= faceRadiusPixel;
	mKFMeasurementNoise *= faceRadiusPixel;

	mCenter.x =  trackRect.x + (int)((float)trackRect.width/2.0f);
	mCenter.y =  trackRect.y + (int)((float)trackRect.height/2.0f);

	// initialize Kalman Filter
	cv::setIdentity(mKF.transitionMatrix);
	mKF.transitionMatrix.at<float>(0,2) = 1;
	mKF.transitionMatrix.at<float>(1,3) = 1;

	mKFMeasurement = cv::Mat_<float>(2,1);
	mKF.statePre.at<float>(0) = (float)mCenter.x;
	mKF.statePre.at<float>(1) = (float)mCenter.y;
	mKF.statePre.at<float>(2) = 0.f;
	mKF.statePre.at<float>(3) = 0.f;
	cv::setIdentity(mKF.measurementMatrix);
	cv::setIdentity(mKF.measurementNoiseCov, cv::Scalar::all(mKFMeasurementNoise));
	mKF.processNoiseCov = 0;
	mKF.processNoiseCov.at<float>(0,0) = mKF.processNoiseCov.at<float>(1,1) = mKFProcessNoiseLoc;
	mKF.processNoiseCov.at<float>(2,2) = mKF.processNoiseCov.at<float>(3,3) = mKFProcessNoiseVel;
	mKF.errorCovPost = 0;
	mKF.errorCovPost.at<float>(0,0) = mKF.errorCovPost.at<float>(1,1) = mKFInitialStateUncertaintyLoc;
	mKF.errorCovPost.at<float>(2,2) = mKF.errorCovPost.at<float>(3,3) = mKFInitialStateUncertaintyVel;
	mKF.errorCovPre = 0;
	mKF.errorCovPre.at<float>(0,0) = mKF.errorCovPre.at<float>(1,1) = mKFInitialStateUncertaintyLoc;
	mKF.errorCovPre.at<float>(2,2) = mKF.errorCovPre.at<float>(3,3) = mKFInitialStateUncertaintyVel;

	// calculate location uncertainty.
	// predict Kalman filter and update location uncertainty
	mKFPrediction = mCenter;

	mLocationUncertaintyInvKF[0] = 1.f/mKFInitialStateUncertaintyLoc;
	mLocationUncertaintyInvKF[2] = 1.f/mKFInitialStateUncertaintyLoc;
	mLocationUncertaintyInvKF[1] = 0.f;

}


TrackingStatusEnum FaceBaseTracker::reactivateStatus(boost::shared_ptr<FaceDetectorTarget> &maxLikelihood_dt)
{
	if(mUseKF)
	{
		// recalculate positional uncertainty
		calcLocationUncertaintyKF();
	}

		maxLikelihood_dt->setInvalid();

		// reinitialize base tracker
		TrackingStatusEnum prevRegionStatus = mRegionStatus;

		cv::Point	newCenter = maxLikelihood_dt->getCenter();

		if(mUseKF)
		{
			// update Kalman filter from previous state
			// update the base features only when update was successful
			prevPosition = mCenter;

			// newCenter is an observation. Update Kalman filter
			mKFMeasurement.at<float>(0) = (float)newCenter.x;
			mKFMeasurement.at<float>(1) = (float)newCenter.y;
			// update Kalman filter
			mKF.correct(mKFMeasurement);
			mKF.predict();

			// calculate location uncertainty.
			// predict using Kalman filter and update location uncertainty
			mKFPrediction.x = (int)mKF.statePre.at<float>(0);
			mKFPrediction.y = (int)mKF.statePre.at<float>(1);

			calcLocationUncertaintyKF();

			mCenter.x = (int)mKF.statePost.at<float>(0);	//
			mCenter.y = (int)mKF.statePost.at<float>(1);
		}
		else
		{
			mCenter = newCenter;
		}
		// update position
		//mPersonShape.estimatePersonShape(mCenter);
		//mPosition = mPersonShape.getFootPosition();

		// update crop rect
		//cv::Rect tBoundingRect = mPersonShape.getBoundingRect();
		mTrackBox = maxLikelihood_dt->bounding_box;

		updateSearchWindow();

		if (mpVideoModule->insideTrackRegion(mCenter))
		{
			mRegionStatus = FaceTrackingStatus_InTrack;
		}
		else if (mpVideoModule->insideStartKillRegion(mCenter))
		{
			mRegionStatus = FaceTrackingStatus_InStart;
		}
		else
		{
			mRegionStatus = FaceTrackingStatus_Invalid;
		}

		if (prevRegionStatus == FaceTrackingStatus_InStart)
		{
			switch (mRegionStatus)
			{
				case FaceTrackingStatus_InStart:
				case FaceTrackingStatus_InTrack:
				{
					// keep tracking status active
					mTrackStatus = FaceTrackingStatus_Active;
					break;
				}
				case FaceTrackingStatus_Invalid:
				default:
				{
					// target entered invalid region. Kill the track?
					mTrackStatus = FaceTrackingStatus_Finished;
					break;
				}

			}
		}
		else if (prevRegionStatus == FaceTrackingStatus_InTrack)
		{
			switch (mRegionStatus)
			{
				case FaceTrackingStatus_InTrack:
				{
					mTrackStatus = FaceTrackingStatus_Active;
					break;
				}
				case FaceTrackingStatus_InStart:
				case FaceTrackingStatus_Invalid:
				default:
				{
					mTrackStatus = FaceTrackingStatus_Finished;
				}

			}
		}

		if (mTrackStatus == FaceTrackingStatus_Active)
		{
			// initialize tracker feature
			this->initializeTrackerFeature(maxLikelihood_dt);
			// target was reactivated;
			mInactiveFrameCount = 0;
		}

		return mTrackStatus;
}


TrackingStatusEnum FaceBaseTracker::reactivateStatus(boost::shared_ptr<FaceDetectorTarget> &maxLikelihood_dt, bool target_found)
{
	TrackingStatusEnum  ret;

	if(mUseKF)
	{
		// recalculate positional uncertainty
		calcLocationUncertaintyKF();
	}

	// if target found
	if (target_found)
	{
		maxLikelihood_dt->setInvalid();

		// reinitialize base tracker
		TrackingStatusEnum prevRegionStatus = mRegionStatus;

		cv::Point	newCenter = maxLikelihood_dt->getCenter();

		if(mUseKF)
		{
			// update Kalman filter from previous state
			// update the base features only when update was successful
			prevPosition = mCenter;

			// newCenter is an observation. Update Kalman filter
			mKFMeasurement.at<float>(0) = (float)newCenter.x;
			mKFMeasurement.at<float>(1) = (float)newCenter.y;
			// update Kalman filter
			mKF.correct(mKFMeasurement);
			mKF.predict();

			// calculate location uncertainty.
			// predict using Kalman filter and update location uncertainty
			mKFPrediction.x = (int)mKF.statePre.at<float>(0);
			mKFPrediction.y = (int)mKF.statePre.at<float>(1);

			calcLocationUncertaintyKF();

			mCenter.x = (int)mKF.statePost.at<float>(0);	//
			mCenter.y = (int)mKF.statePost.at<float>(1);
		}
		else
		{
			mCenter = newCenter;
		}
		// update position
		//mPersonShape.estimatePersonShape(mCenter);
		//mPosition = mPersonShape.getFootPosition();

		// update crop rect
		//cv::Rect tBoundingRect = mPersonShape.getBoundingRect();
		mTrackBox = maxLikelihood_dt->bounding_box;

		updateSearchWindow();

		if (mpVideoModule->insideTrackRegion(mCenter))
		{
			mRegionStatus = FaceTrackingStatus_InTrack;
		}
		else if (mpVideoModule->insideStartKillRegion(mCenter))
		{
			mRegionStatus = FaceTrackingStatus_InStart;
		}
		else
		{
			mRegionStatus = FaceTrackingStatus_Invalid;
		}

		if (prevRegionStatus == FaceTrackingStatus_InStart)
		{
			switch (mRegionStatus)
			{
				case FaceTrackingStatus_InStart:
				case FaceTrackingStatus_InTrack:
				{
					// keep tracking status active
					mTrackStatus = FaceTrackingStatus_Active;
					break;
				}
				case FaceTrackingStatus_Invalid:
				default:
				{
					// target entered invalid region. Kill the track?
					mTrackStatus = FaceTrackingStatus_Finished;
					break;
				}

			}
		}
		else if (prevRegionStatus == FaceTrackingStatus_InTrack)
		{
			switch (mRegionStatus)
			{
				case FaceTrackingStatus_InTrack:
				{
					mTrackStatus = FaceTrackingStatus_Active;
					break;
				}
				case FaceTrackingStatus_InStart:
				case FaceTrackingStatus_Invalid:
				default:
				{
					mTrackStatus = FaceTrackingStatus_Finished;
				}

			}
		}

		if (mTrackStatus == FaceTrackingStatus_Active)
		{
			// initialize tracker feature
			this->initializeTrackerFeature(maxLikelihood_dt);
			// target was reactivated;
			mInactiveFrameCount = 0;
		}

		//return mTrackStatus;
		ret = mTrackStatus;
	} // of if (target_found)
/*
	else
	{
		// reactivation target not found.
		// increase inactive count
		mInactiveFrameCount++;

		// if inactive count is larger than threshold, finish this target
		if (mInactiveFrameCount > mcInactiveTimeout)
		{
			//return FaceTrackingStatus_Finished;
			ret= FaceTrackingStatus_Finished;
		}
		else
		{
			//return FaceTrackingStatus_Inactive;
			ret= FaceTrackingStatus_Inactive;
		}
	}
*/

	return ret;

}

TrackingStatusEnum FaceBaseTracker::reactivate(std::vector<boost::shared_ptr<FaceDetectorTarget> > &dt_list)
{
	if(mUseKF)
	{
		// recalculate positional uncertainty
		calcLocationUncertaintyKF();
	}

	// Find best matching detected target for reactivation
	bool target_found = false;
	boost::shared_ptr<FaceDetectorTarget> maxLikelihood_dt;
	float	maxLikelihood = -1.f;

	// for each detected target calculate location likelihood
	// check if dt is valid
	std::vector<boost::shared_ptr<FaceDetectorTarget> >::iterator dt_itr = dt_list.begin();
	while(dt_itr != dt_list.end())
	{
		if ((*dt_itr)->isValid())
		{
			// find the detected target with maximum location likelihood
			// TODO: currently just use location likelihood, but research to use visual similarity later
			float reactivation_likelihood;
			if(mUseKF)
			{
				reactivation_likelihood = calculateLocationLikelihoodKF((*dt_itr)->getCenter());
			}
			else
			{
				reactivation_likelihood = calculateReactivationLikelihood((*dt_itr));
			}
			if (reactivation_likelihood > maxLikelihood)
			{
				maxLikelihood_dt = (*dt_itr);
				maxLikelihood = reactivation_likelihood;
			}

		}
		dt_itr++;
	}

	// if the location likelihood of the max target is larger than threshold, target is found
	// invalidate found target
    if (maxLikelihood > 0.0f) //mcReactivateLikelihoodThreshold)
	{
		target_found = true;
	}
	else
	{
		target_found = false;
	}

	// if target found
	if (target_found)
	{
		maxLikelihood_dt->setInvalid();

		// reinitialize base tracker
		TrackingStatusEnum prevRegionStatus = mRegionStatus;

		cv::Point	newCenter = maxLikelihood_dt->getCenter();

		if(mUseKF)
		{
			// update Kalman filter from previous state
			// update the base features only when update was successful
			prevPosition = mCenter;

			// newCenter is an observation. Update Kalman filter
			mKFMeasurement.at<float>(0) = (float)newCenter.x;
			mKFMeasurement.at<float>(1) = (float)newCenter.y;
			// update Kalman filter
			mKF.correct(mKFMeasurement);
			mKF.predict();

			// calculate location uncertainty.
			// predict using Kalman filter and update location uncertainty
			mKFPrediction.x = (int)mKF.statePre.at<float>(0);
			mKFPrediction.y = (int)mKF.statePre.at<float>(1);

			calcLocationUncertaintyKF();

			mCenter.x = (int)mKF.statePost.at<float>(0);	//
			mCenter.y = (int)mKF.statePost.at<float>(1);
		}
		else
		{
			mCenter = newCenter;
		}
		// update position
		//mPersonShape.estimatePersonShape(mCenter);
		//mPosition = mPersonShape.getFootPosition();

		// update crop rect
		//cv::Rect tBoundingRect = mPersonShape.getBoundingRect();
		mTrackBox = maxLikelihood_dt->bounding_box;

		updateSearchWindow();
/*
		cv::Rect tBoundingRect = mTrackBox;
		mCropRect.width = (int)(tBoundingRect.width*mSearchWindowRatio);
		mCropRect.height = (int)(tBoundingRect.height*mSearchWindowRatio);
		mCropRect.x = tBoundingRect.x + (int)(0.5*(1.f-mSearchWindowRatio)*(float)tBoundingRect.width);
		mCropRect.y = tBoundingRect.y + (int)(0.5*(1.f-mSearchWindowRatio)*(float)tBoundingRect.height);
*/
		// make sure crop rect is inside image
//		mpVideoModule->enforceWindowRect(mCropRect);

		if (mpVideoModule->insideTrackRegion(mCenter))
		{
			mRegionStatus = FaceTrackingStatus_InTrack;
		}
		else if (mpVideoModule->insideStartKillRegion(mCenter))
		{
			mRegionStatus = FaceTrackingStatus_InStart;
		}
		else
		{
			mRegionStatus = FaceTrackingStatus_Invalid;
		}

		if (prevRegionStatus == FaceTrackingStatus_InStart)
		{
			switch (mRegionStatus)
			{
				case FaceTrackingStatus_InStart:
				case FaceTrackingStatus_InTrack:
				{
					// keep tracking status active
					mTrackStatus = FaceTrackingStatus_Active;
					break;
				}
				case FaceTrackingStatus_Invalid:
				default:
				{
					// target entered invalid region. Kill the track?
					mTrackStatus = FaceTrackingStatus_Finished;
					break;
				}

			}
		}
		else if (prevRegionStatus == FaceTrackingStatus_InTrack)
		{
			switch (mRegionStatus)
			{
				case FaceTrackingStatus_InTrack:
				{
					mTrackStatus = FaceTrackingStatus_Active;
					break;
				}
				case FaceTrackingStatus_InStart:
				case FaceTrackingStatus_Invalid:
				default:
				{
					mTrackStatus = FaceTrackingStatus_Finished;
				}

			}
		}

		if (mTrackStatus == FaceTrackingStatus_Active)
		{
			// initialize tracker feature
			this->initializeTrackerFeature(maxLikelihood_dt);
			// target was reactivated;
			mInactiveFrameCount = 0;
		}

		return mTrackStatus;
	} // of if (target_found)
	else
	{
		// reactivation target not found.
		// increase inactive count
		mInactiveFrameCount++;

		// if inactive count is larger than threshold, finish this target
		if (mInactiveFrameCount > mcInactiveTimeout)
		{
			return FaceTrackingStatus_Finished;
		}
		else
		{
			return FaceTrackingStatus_Inactive;
		}
	}

}

bool FaceBaseTracker::verifyActiveTargetFromDetection( boost::shared_ptr<FaceDetectorTarget> &dt)
{
	cv::Mat blob_map;
	mpFaceDetector->getBlobMap(blob_map);
	//blob_map &= mTargetActivityMap;

    cv::Mat cropped_map;
	blob_map(this->getTrackBox()).copyTo(cropped_map);
    cv::Scalar area = cv::sum(cropped_map);
    if ((float)area[0]/(float)(this->getTrackBox().area()) > 0.3f) // if overlapped pixels are larger than 80% of the blob area
    {
    	dt->setInvalid();
    	return true;
    }

    return false;
}

TrackingStatusEnum FaceBaseTracker::update(cv::Mat *disp_map)
{

	// update tracker feature and get next track_box

	// update new position
	cv::Point newCenter;
	bool update_success = this->updateTrackerFeature(disp_map,newCenter);  // mTrackBox is updated here.

	// update base tracker
	TrackingStatusEnum prevRegionStatus = mRegionStatus;

	if (update_success)
	{
		if(mUseKF)
		{
			// update the base features only when update was successful
			prevPosition = mCenter;

			// update Kalman filter
			updateKalmanFilter(newCenter, update_success);

			mCenter.x = (int)mKF.statePost.at<float>(0);	//
			mCenter.y = (int)mKF.statePost.at<float>(1);
		}
		else
		{
			mCenter = newCenter;
		}
		// update person shape and position
//		mPersonShape.estimatePersonShape(mCenter);
//		mPosition = mPersonShape.getFootPosition();

		if (mpVideoModule->insideTrackRegion(mCenter))
		{
			mRegionStatus = FaceTrackingStatus_InTrack;
		}
		else if (mpVideoModule->insideStartKillRegion(mCenter))
		{
			mRegionStatus = FaceTrackingStatus_InStart;
		}
		else
		{
			mRegionStatus = FaceTrackingStatus_Invalid;
		}

		updateSearchWindow();
/*
		// update crop rect
		//cv::Rect tBoundingRect = mPersonShape.getBoundingRect();
		//cv::Rect tBoundingRect = mTrackBox;
		mCropRect.width = (int)(mTrackBox.width*mSearchWindowRatio);
		mCropRect.height = (int)(mTrackBox.height*mSearchWindowRatio);
		mCropRect.x = mTrackBox.x + (int)(0.5*(1.f-mSearchWindowRatio)*(float)mTrackBox.width);
		mCropRect.y = mTrackBox.y + (int)(0.5*(1.f-mSearchWindowRatio)*(float)mTrackBox.height);
		// make sure crop rect is inside image
		mpVideoModule->enforceWindowRect(mCropRect);
*/
		// If the starting region is same to the tracking region, the following is not happened.
		if (prevRegionStatus == FaceTrackingStatus_InStart)
		{
			switch (mRegionStatus)
			{
				case FaceTrackingStatus_InStart:
				case FaceTrackingStatus_InTrack:
				{
					// keep tracking status active
					mTrackStatus = FaceTrackingStatus_Active;
					break;
				}
				case FaceTrackingStatus_Invalid:
				default:
				{
					// target entered invalid region. Kill the track?
					mTrackStatus = FaceTrackingStatus_Finished;
					break;
				}

			}
		}
		else if (prevRegionStatus == FaceTrackingStatus_InTrack)
		{
			switch (mRegionStatus)
			{
				case FaceTrackingStatus_InTrack:
				{
					mTrackStatus = FaceTrackingStatus_Active;
					break;
				}
				case FaceTrackingStatus_InStart:
				case FaceTrackingStatus_Invalid:
				default:
				{
					mTrackStatus = FaceTrackingStatus_Finished;
				}

			}
		}
	}
	else	// of if (update_success)
	{
		if(mUseKF)
		{
			cv::Point dummy;
			updateKalmanFilter(dummy,update_success);
		}

		// update() is called only for active targets.
		// if update is not successful, then region status doesn't change
		mInactiveFrameCount = 1;
		mTrackStatus = FaceTrackingStatus_Inactive;
	}

#ifdef DEBUG
	mCount++;
#endif

	return mTrackStatus;
}


FaceTrackingMethods FaceBaseTracker::readTrackingMethod(boost::shared_ptr<Settings> settings)
{
	std::string trackingMethodStr = settings->getString("FaceTracker/trackingMethod","FaceTrackerFeature_HIST_HSV");

	if (trackingMethodStr == "FaceTrackerFeature_HIST_HSV")
	{
		return FaceTrackerFeature_HIST_HSV;
	}
	else if (trackingMethodStr == "FaceTrackerFeature_HIST_YCrCb")
	{
		return FaceTrackerFeature_HIST_YCrCb;
	}
	else if (trackingMethodStr == "FaceTrackerFeature_HIST_rgCHROM")
	{
		return FaceTrackerFeature_HIST_rgCHROM;
	}
	else if (trackingMethodStr == "FaceTrackerFeature_HIST_RGB")
	{
		return FaceTrackerFeature_HIST_RGB;
	}
	else if (trackingMethodStr == "FaceTrackerFeature_DLIB")
	{
		return FaceTrackerFeature_DLIB;
	}
	else if (trackingMethodStr == "FaceTrackerFeature_Hybrid")
	{
		return FaceTrackerFeature_Hybrid;
	}
	else
	{
		VML_WARN(MsgCode::notImplementedErr,"Face Tracking method " << trackingMethodStr << " not implemented. Using default (FaceTrackerFeature_HIST_HSV)");
		return FaceTrackerFeature_HIST_HSV;
	}
}

void FaceBaseTracker::setTrackingMethod(const FaceTrackingMethods tm, boost::shared_ptr<Settings> settings)
{
	switch (tm)
	{
		case FaceTrackerFeature_HIST_HSV:
		{
			settings->setString("FaceTracker/trackingMethod","FaceTrackerFeature_HIST_HSV");
			break;
		}
		case FaceTrackerFeature_HIST_YCrCb:
		{
			settings->setString("FaceTracker/trackingMethod","FaceTrackerFeature_HIST_YCrCb");
			break;
		}
		case FaceTrackerFeature_HIST_rgCHROM:
		{
			settings->setString("FaceTracker/trackingMethod","FaceTrackerFeature_HIST_rgCHROM");
			break;
		}
		case FaceTrackerFeature_HIST_RGB:
		{
			settings->setString("FaceTracker/trackingMethod","FaceTrackerFeature_HIST_RGB");
			break;
		}
		case FaceTrackerFeature_DLIB:
		{
			settings->setString("FaceTracker/trackingMethod","FaceTrackerFeature_DLIB");
			break;
		} // of case Correlation_Tracking:
		case FaceTrackerFeature_Hybrid:
		{
			settings->setString("FaceTracker/trackingMethod","FaceTrackerFeature_Hybrid");
			break;
		}
		default:
		{
			VML_ERROR(MsgCode::notSupportedErr,"FaceBaseTracker::setTrackingMethod: Tracking method " << tm << " not supported");
		}
	}
}

void FaceBaseTracker::updateTargetActivityMap(cv::Mat &activity_map)
{
	cv::rectangle(activity_map,mTrackBox,cv::Scalar(255),CV_FILLED);
}

void FaceBaseTracker::updateSearchWindow(void)
{

#if USE_CAMSHIFT
	// for now, don't use motion model, just use twice the bounding box of mTrackBox
	cv::Rect tBoundingRect = mTrackBox.boundingRect();
	// set tBoundingRect to the global coord
	tBoundingRect += mCropRect.tl();
	// calculate new search window size
	mCropRect.width = (int)(tBoundingRect.width*cSearchWindowRatio);
	mCropRect.height = (int)(tBoundingRect.height*cSearchWindowRatio);
	mCropRect.x = tBoundingRect.x+(int)(0.5*(1.f-cSearchWindowRatio)*(float)tBoundingRect.width);
	mCropRect.y = tBoundingRect.y+(int)(0.5*(1.f-cSearchWindowRatio)*(float)tBoundingRect.height);
	// make sure crop rect is inside image
	mCropRect = mCropRect & cv::Rect(0,0,input_image.cols-1, input_image.rows-1);
	cv::Point new_loc = mTrackBox.center;
#else
	// calculate new search window size
	mCropRect.width = (int)(mTrackBox.width*mSearchWindowRatio);
	mCropRect.height = (int)(mTrackBox.height*mSearchWindowRatio);
	mCropRect.x = mTrackBox.x+(int)(0.5*(1.f-mSearchWindowRatio)*(float)mTrackBox.width);
	mCropRect.y = mTrackBox.y+(int)(0.5*(1.f-mSearchWindowRatio)*(float)mTrackBox.height);
	// make sure crop rect is inside image
    cv::Rect wholewindow(0,0,cols,rows);
	mCropRect = mCropRect & wholewindow;
#endif

	// update center
	mCenter.x = (int)(mTrackBox.x + (float)mTrackBox.width/2.f);
	mCenter.y = (int)(mTrackBox.y + (float)mTrackBox.height/2.f);

}


void FaceBaseTracker::updateContourMask(std::vector<std::vector<cv::Point> > contour)
{
	// create temp polygon in the crop rect
	std::vector<std::vector<cv::Point> > contours_crop;
	for (std::vector<std::vector<cv::Point> >::iterator itr=contour.begin();
			itr != contour.end();itr++)
	{
		std::vector<cv::Point> contour_crop;
		std::vector<cv::Point>::iterator pitr;
		for(pitr=itr->begin();pitr!=itr->end();pitr++)
		{
			contour_crop.push_back(*pitr - mCropRect.tl());
		}
		contours_crop.push_back(contour_crop);
	}
	// create polygon mask
	mContourMask = cv::Mat(mCropRect.height,mCropRect.width,CV_8UC1,cv::Scalar(0));
	// fill mask
	cv::drawContours(mContourMask,contours_crop,-1,cv::Scalar(255),CV_FILLED);
}

/*
void FaceBaseTracker::updateContourMask(std::vector<std::vector<cv::Point> > *contour)
{
	// create temp polygon in the crop rect
	std::vector<std::vector<cv::Point> > contours_crop;
	for (std::vector<std::vector<cv::Point> >::iterator itr=contour->begin();
			itr != contour->end();itr++)
	{
		std::vector<cv::Point> contour_crop;
		std::vector<cv::Point>::iterator pitr;
		for(pitr=itr->begin();pitr!=itr->end();pitr++)
		{
			contour_crop.push_back(*pitr - mCropRect.tl());
		}
		contours_crop.push_back(contour_crop);
	}
	// create polygon mask
	mContourMask = cv::Mat(mCropRect.height,mCropRect.width,CV_8UC1,cv::Scalar(0));
	// fill mask
	cv::drawContours(mContourMask,contours_crop,-1,cv::Scalar(255),CV_FILLED);
}
*/

//-------- For Kalman filter --------------//
void FaceBaseTracker::updateKalmanFilter(cv::Point newCenter, bool success)
{
	if (success)
	{
#ifdef FACETRACKERS_DEBUG_SHOW
		float	P_pre[4][4];
		float	P_post[4][4];
		float	x_pre[4];
		float	x_post[4];

		for (unsigned int i=0;i<4;i++)
			for (unsigned int j=0;j<4;j++)
			{
				P_pre[i][j] = mKF.errorCovPre.at<float>(i,j);
				P_post[i][j] = mKF.errorCovPost.at<float>(i,j);
			}

		for (unsigned int i=0;i<4;i++)
		{
			x_pre[i] = mKF.statePre.at<float>(i);
			x_post[i] = mKF.statePost.at<float>(i);
		}

		// TODO: insert Kalman filter uncertainty output to disk here
#endif

		// newCenter is an observation. Update Kalman filter
		mKFMeasurement.at<float>(0) = (float)newCenter.x;
		mKFMeasurement.at<float>(1) = (float)newCenter.y;
		mKF.correct(mKFMeasurement);

#ifdef FACETRACKERS_DEBUG_SHOW
		for (unsigned int i=0;i<4;i++)
			for (unsigned int j=0;j<4;j++)
			{
				P_pre[i][j] = mKF.errorCovPre.at<float>(i,j);
				P_post[i][j] = mKF.errorCovPost.at<float>(i,j);
			}

		for (unsigned int i=0;i<4;i++)
		{
			x_pre[i] = mKF.statePre.at<float>(i);
			x_post[i] = mKF.statePost.at<float>(i);
		}
#endif

		// predict using Kalman filter and update location uncertainty
		mKF.predict();

#ifdef FACETRACKERS_DEBUG_SHOW

		for (unsigned int i=0;i<4;i++)
			for (unsigned int j=0;j<4;j++)
			{
				P_pre[i][j] = mKF.errorCovPre.at<float>(i,j);
				P_post[i][j] = mKF.errorCovPost.at<float>(i,j);
			}

		for (unsigned int i=0;i<4;i++)
		{
			x_pre[i] = mKF.statePre.at<float>(i);
			x_post[i] = mKF.statePost.at<float>(i);
		}
#endif

		mKFPrediction.x = (int)mKF.statePre.at<float>(0);
		mKFPrediction.y = (int)mKF.statePre.at<float>(1);
		calcLocationUncertaintyKF();
	}
	else
	{
		// just increase process noise and predict
		mKF.predict();
		mKFPrediction.x = (int)mKF.statePre.at<float>(0);
		mKFPrediction.y = (int)mKF.statePre.at<float>(1);
		calcLocationUncertaintyKF();
	}
}

void FaceBaseTracker::calcLocationUncertaintyKF()
{
	// testing. calculate location uncertainty inverse from Kalman filter
	float	p_pre[3];
	p_pre[0] = mKF.errorCovPre.at<float>(0,0);
	p_pre[1] = mKF.errorCovPre.at<float>(0,1);
	p_pre[2] = mKF.errorCovPre.at<float>(1,1);
	float D_ = p_pre[0]*p_pre[2]+p_pre[1]*p_pre[1];
	mLocationUncertaintyInvKF[0] = p_pre[2]/D_;
	mLocationUncertaintyInvKF[2] = p_pre[0]/D_;
	mLocationUncertaintyInvKF[1] = -p_pre[1]/D_;
}

float FaceBaseTracker::calculateLocationLikelihoodKF(cv::Point pos)
{
	cv::Point	np = mKFPrediction;

	float dx = (float)(pos.x-np.x);
	float dy = (float)(pos.y-np.y);

	return (exp(-(mLocationUncertaintyInvKF[0]*dx*dx+2.f*mLocationUncertaintyInvKF[1]*dx*dy+mLocationUncertaintyInvKF[2]*dy*dy)));
}

float FaceBaseTracker::calculateLocationLikelihoodKF(int x, int y)
{
	cv::Point	np = mKFPrediction;

	float dx = (float)(x-np.x);
	float dy = (float)(y-np.y);

	return (exp(-(mLocationUncertaintyInvKF[0]*dx*dx+2.f*mLocationUncertaintyInvKF[1]*dx*dy+mLocationUncertaintyInvKF[2]*dy*dy)));
}

float FaceBaseTracker::calculateReactivationLikelihood(boost::shared_ptr<FaceDetectorTarget> dt)
{
	cv::Point	np = mCenter;
	cv::Point	pos = dt->mCenter;

	float dx = (float)(pos.x-np.x);
	float dy = (float)(pos.y-np.y);

	float	rx = (float)(dt->bounding_box.width*mSearchWindowRatio)/2;
	float	ry = (float)(dt->bounding_box.height*mSearchWindowRatio)/2;

    float dist = sqrt(dx*dx+dy*dy);
    float distTh = sqrt(rx*rx+ ry*ry);

    //mInactiveMapWindowRadius
    if ((dist < distTh) && (dist < INT_MAX))
    {
        return 1;
    }
    else
    {
        return 0;
    }

    //return (exp(-(dx*dx/(2*rx*rx) + dy*dy/(2*ry*ry) )) / (2*rx*ry*CV_PI) );
}



} // of namespace vml

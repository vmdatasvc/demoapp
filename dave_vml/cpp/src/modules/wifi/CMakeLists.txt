project (vmlwifiLib)
set (LIB_NAME vmlwifi)

set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set (LIB_SRC
	SnifferPacket.cpp
	TrackerPacket.cpp
	)

vml_add_library (${LIB_NAME}
	STATIC
	${LIB_SRC}
	${PROJECT_HEADER_FILES}
	)

target_link_libraries (${LIB_NAME} 
	vmlcore
	boost_serialization)


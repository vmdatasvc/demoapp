/*
 * TrackerPacket.cpp
 *
 *  Created on: Jan 24, 2017
 *      Author: jwallace
 */

#include "modules/wifi/TrackerPacket.hpp"

void TrackerPacket::print(std::ostream& os) const {
	os << "Timestamp: " << timestamp
	   << ", Name: " << capture_name;
	payload.print(os);
}

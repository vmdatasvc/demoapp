/*
 * SnifferPacket.cpp
 *
 *  Created on: Jan 24, 2017
 *      Author: jwallace
 */

#include "modules/wifi/SnifferPacket.hpp"

#include <sstream>
#include <limits>
#include <cstdio>

#include <boost/array.hpp>

#include <boost/algorithm/hex.hpp>

#include <iostream>
using std::cout;
using std::endl;

using std::string;
using std::ostringstream;
using std::ostream;

SnifferPacket::SnifferPacket() : type(Invalid), channel(0), rss(std::numeric_limits<int8_t>::max()) {
	// set the MAC to the broadcast address
	//mac = {0xff};
}

SnifferPacket::SnifferPacket(DeviceType t, unsigned char c, const std::string& mac_in, int8_t level)
	: type(t), channel(c), mac_str(mac_in), rss(level) {

	/*
	cout << "Constructing with MAC:" << mac_in << endl;
	std::sscanf(mac_in.c_str(), "%02x%02x%02x%02x%02x%02x", &mac[0], &mac[1], &mac[2], &mac[3], &mac[4], &mac[5]);

	//boost::algorithm::hex(mac_in.begin(), mac_in.end(), mac.begin());

	cout << std::hex << "Enc MAC:";
	for(auto it = mac.begin(); it!=mac.end(); it++){
		cout << static_cast<unsigned int>(*it) << ":";
	}
	cout << endl;
	string str;

	boost::algorithm::hex(mac.begin(), mac.end(), str.begin());
	cout << "hex: " << str << endl;
	*/
	
}

SnifferPacket::SnifferPacket(DeviceType t, unsigned char c, const uint8_t* mac_in, int8_t level)
	: type(t), channel(c), rss(level) {

	boost::array<uint8_t, 6> arr;

	memcpy(&arr[0], mac_in, 6);

	boost::algorithm::hex(arr.begin(), arr.end(), std::back_inserter(mac_str));
	// copy the input mac - assume buffer sizes are good here
	//memcpy(&mac[0], mac_in, sizeof(mac));
}

/*string SnifferPacket::getMAC_str() const {
	string retval;
	boost::algorithm::unhex(mac.begin(), mac.end(), retval.begin());
	return retval;
}*/

void SnifferPacket::print(ostream& os) const {
	os << "Type: " << static_cast<int>(type)
	   << ", Channel: " << static_cast<int>(channel)
	   << ", MAC: " << getMAC_str()
	   << ", RSS: " << rss;
}

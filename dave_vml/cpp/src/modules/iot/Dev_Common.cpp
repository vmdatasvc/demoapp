#include "modules/iot/Dev_Common.hpp"

#include <fstream>
#include <vector>

// string manipulator here
#include <boost/algorithm/string.hpp>
#include <boost/bind.hpp>

using std::string;
using std::vector;
using std::ifstream;

#ifdef DEBUG_DEV_INFO

#include <iostream>

using std::cout;
using std::endl;

#endif

DeviceInfo::DeviceInfo(){
	// create a network monitor
	NetworkMonitor::callback_t change_cb = boost::bind(&DeviceInfo::updateNetworkInfo, this);

	mNetMonitor = new NetworkMonitor(change_cb);

	// -- Read IP address
	mAP_IPaddr = mNetMonitor->get_ipaddr_str();

	// -- Read Serial Number
	// NOTE: designed to replicate the following (we'll be a bit more specific with the grep though):
	// cat /usr/local/www/serial_number.txt | grep Serial | sed 's/:/ /' | awk '{print $2}'
	string line;
	vector<string> tokens, sub_tok;
	ifstream serial_f("/usr/local/www/serial_number.txt");
	while(serial_f.is_open() && mSerialNumber.length() == 0 && getline(serial_f, line)){
		boost::algorithm::trim(line);
		boost::algorithm::split(tokens, line, boost::is_any_of(":"));

		// NOTE: if we never trip this conditional, mSerialNumber will end up being the empty string
		if(tokens.size() >= 2 && tokens[0] == "SerialNumber"){
			boost::algorithm::trim(tokens[1]);
			boost::algorithm::split(sub_tok, tokens[1], boost::is_any_of(" \t"), boost::token_compress_on);
			mSerialNumber = sub_tok[0];
		}
	}
#ifdef DEBUG_DEV_INFO
	if (!serial_f.is_open()) cout << "Failed to open serial_number.txt" << endl;
#endif
	serial_f.close();

	// -- Read Store Name
	// Store name is defined to be the first non-empty line of /usr/local/www/store_name.txt
	ifstream store_f("/usr/local/www/store_name.txt");
	while(store_f.is_open() && mStoreName.length() == 0 && getline(store_f, line)){
		boost::algorithm::trim(line);
		mStoreName = line;
	}
#ifdef DEBUG_DEV_INFO
	if (!store_f.is_open()) cout << "Failed to open store_name.txt" << endl;
#endif
	store_f.close();

	// -- Read Device Name
	// Device name is defined to be the first non-empty line of /usr/local/www/dev_name.txt
	ifstream dev_f("/usr/local/www/dev_name.txt");
	while(dev_f.is_open() && mDevName.length() == 0 && getline(dev_f, line)){
		boost::algorithm::trim(line);
		mDevName = line;
	}
#ifdef DEBUG_DEV_INFO
	if (!dev_f.is_open()) cout << "Failed to open dev_name.txt" << endl;
#endif
	dev_f.close();

#ifdef DEBUG_DEV_INFO
	cout << "[Dev_Common] My IP Address = " << mAP_IPaddr << endl;
	cout << "[Dev_Common] My MAC Address = " << mMAC_addr << endl;
	cout << "[Dev_Common] serialNumber = " << mSerialNumber << endl;
	cout << "[Dev_Common] store_name = " << mStoreName << endl;
	cout << "[Dev_Common] device_name = " << mDevName << endl;
#endif

	// create a thread to run the network monitor
	mMonitorThread = new boost::thread(&NetworkMonitor::run, mNetMonitor);

}


DeviceInfo::~DeviceInfo(){
	// stop the network monitoring thread
	if(mMonitorThread){
		// stop (no exception handling in the NetworkMonitor)
		mMonitorThread->interrupt();
		// .. and die
		delete mMonitorThread;

	}
	// delete the network monitor
	if(mNetMonitor){
		delete mNetMonitor;
	}
}

string DeviceInfo::GetMyStoreName_dash_() const{
	return boost::algorithm::replace_all_copy(mStoreName, "/", "-");
}

void DeviceInfo::updateNetworkInfo(){
	// NOTE: Although this is called in a separate thread, I don't need any mutex here
	// because strings are threadsafe as of gcc 3.1.?
	mAP_IPaddr = mNetMonitor->get_ipaddr_str();
	mMAC_addr = mNetMonitor->get_macaddr_str();
}


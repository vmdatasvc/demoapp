#include "modules/iot/NetworkMonitor.hpp"

#include <net/if.h>

#include <poll.h>
#include <sys/socket.h>

#include <linux/rtnetlink.h>

#include <iostream>
#include <sstream>

#include <boost/array.hpp>
#include <boost/algorithm/hex.hpp>
#include <boost/algorithm/string.hpp>

using std::map;
using std::cout;
using std::endl;
using std::string;
using std::pair;

// ===================== Constructor =================================
NetworkMonitor::NetworkMonitor(callback_t change_in, unsigned int nsec, callback_t panic_in) 
	: change_action(change_in), panic_sec(nsec), panic_action(panic_in){
	
	get_wired_devices();
	socket_setup();
	update_wired_status();
	
}

// ===================== Destructor ==================================
NetworkMonitor::~NetworkMonitor(){
	// close the socket?
	close(sock);
}

// ===================== Static Functions ============================

bool NetworkMonitor::is_wired(struct udev_device* dev){

	/* Only print information for non-wlan, non-bond, and non-loopback devices 
		   NOTE: the loopback interface is special, because it will not have a 
		   parent, but an ethernet WILL!*/
	
	const char* dev_type = udev_device_get_devtype(dev);
	return udev_device_get_parent(dev) && 
	       (!dev_type || 
		      (strcmp(dev_type, "wlan") != 0 && 
			   strcmp(dev_type, "bond") != 0
			  )
		   );
}

string NetworkMonitor::format_ipv4_addr(struct in_addr addr){
	std::ostringstream out;
	unsigned int bytemask = 255;
	
	// Trust me, this works!  Magic!
	// But really, just goes through each byte of the 4 bytes of 
	// addr.s_addr, shifts into the LSB, masks to only that byte
	// then prints it as an unsigned int.
	// Then, print a ".", only if I'm not on the last time through the loop
	for (unsigned int i=0; i<4; i++){
		out << ( bytemask & (addr.s_addr >> (i*8)) ) << (i==3 ? "" : ".");
	}
	
	return out.str();

}

string NetworkMonitor::format_mac_addr(const unsigned char* mac){
	string retval;
	if(mac){
		// copy into a boost array so I can use boost::algorithm::hex
		boost::array<uint8_t, 6> arr;
		memcpy(&arr[0], mac, 6);
		boost::algorithm::hex(arr.begin(), arr.end(), std::back_inserter(retval));
		boost::to_lower(retval);
		// convert to MAC (lowercase, no colons)

	} else {
		retval = "000000000000";
	}
	return retval;
}
// ===================== Member Functions ============================

bool NetworkMonitor::all_up() const{
	bool up = true;
	for (auto it=wired_status.begin(); up && it != wired_status.end(); it++){
		up = up && (it->second.status & ALL_UP) == ALL_UP;
	}	
	return up;
}

bool NetworkMonitor::any_up() const{
	bool up = false;
	for (auto it=wired_status.begin(); (!up) && it != wired_status.end(); it++){
		up = up || (it->second.status & ALL_UP) == ALL_UP;
	}	
	return up;
}

// Returns the first non-zero IPv4 address (in raw format)
struct in_addr NetworkMonitor::get_ipaddr() const{
	struct in_addr addr;
	addr.s_addr = 0;
	for (auto it=wired_status.begin(); addr.s_addr==0 && it != wired_status.end(); it++){
		if(it->second.addr.s_addr != 0){
			addr = it->second.addr;
		}
	}	
	return addr;
}

// returns the MAC address of the first non-zero IPv4 address
// If there are no non-zero IPv4 address, returns the first wired MAC address
// If there are no wired MAC addresses, returns null
const uint8_t* NetworkMonitor::get_macaddr() const{
	const uint8_t* mac = 0;

	for (auto it=wired_status.begin(); mac==0 && it != wired_status.end(); it++){
		if(it->second.addr.s_addr != 0){
			mac = it->second.mac;
		}
	}

	if(mac == 0 && wired_status.size() > 0){
		mac = wired_status.begin()->second.mac;
	}

	return mac;
}

void NetworkMonitor::update_wired_status(){
    // create a poll structure to listen for information from the kernel
	pollfd fds;
	fds.fd = sock;
	fds.events = POLLPRI | POLLERR | POLLIN;
	int ret = 0;

	// struct used for RTM_GETLINK and RTM_GETADDR messages
	struct {
		struct nlmsghdr  nl;
		union {
			struct ifinfomsg ifinfo;
			struct ifaddrmsg ifaddr;
		};
	} req;

	// Form the RTM_GETLINK message	
	bzero(&req, sizeof(req));
	
	req.nl.nlmsg_len = NLMSG_LENGTH(sizeof(struct ifinfomsg));
	
	req.nl.nlmsg_flags = NLM_F_REQUEST | NLM_F_ROOT;
	req.nl.nlmsg_type = RTM_GETLINK;
	
	req.ifinfo.ifi_family = AF_UNSPEC;
	req.ifinfo.ifi_change = 0xFFFFFFFF;

	// send the RTM_GETLINK message
	send_linkrequest(&req.nl, req.nl.nlmsg_len);
	
	
	// get the response	
	ret = poll(&fds, 1, 0);
	while (ret == 1){
		read_status();
		ret = poll(&fds, 1, 0);
	}
	
	// Form the RTM_GETADDR message
	bzero(&req, sizeof(req));
	
	req.nl.nlmsg_len = NLMSG_LENGTH(sizeof(struct ifaddrmsg));
	req.nl.nlmsg_flags = NLM_F_REQUEST | NLM_F_ROOT;
	req.nl.nlmsg_type = RTM_GETADDR;
	
	req.ifaddr.ifa_family = AF_INET;
	
	// send the RTM_GETADDR message
	send_linkrequest(&req.nl, req.nl.nlmsg_len);
	
	// get the response	
	ret = poll(&fds, 1, 0);
	while (ret == 1){
		read_status();
		ret = poll(&fds, 1, 0);
	}

}


void NetworkMonitor::run(){

	// Now, time to listen for changes	
	time_t curr_time;
	time_t timeout_time;
	int timeout = -1;
	bool prev_up = all_up();

	time(&curr_time);
	timeout_time = curr_time + panic_sec;

    // create a poll structure to listen for information from the kernel
	pollfd fds;
	int ret = 0;

	fds.fd = sock;
	fds.events = POLLPRI | POLLERR | POLLIN;
	
	if(!all_up()){
		time(&curr_time);
		
		timeout_time = curr_time + panic_sec;
		timeout = difftime(timeout_time, curr_time) * 1000;
	}	
	
	while(1){
	
		ret = poll(&fds, 1, timeout);
		if (ret == 1){
			//printf("something to read!\n");
			
			bool status_change = read_status();
			
			// if something changed...
			if(status_change){
			
				// Execute the callback registered for change
				// NOTE: read_status() has already made the changes to the 
				// wired_status structure!
				if(change_action){
					change_action();
				}
				
				// we're down!
				//cout << "Something changed!!" << endl;

				if(!all_up()){
					//cout << "Link down" << endl;
					// get the current time
					time(&curr_time);		
					
					// if we were previously up, set the timeout time to now + PANIC_TIME
					if(prev_up){
						timeout_time = curr_time + panic_sec;
					} else {
						prev_up = false;
					}
					
					// set the timeout to wait from now until the timeout time
					// ONLY set the timeout time to not -1 if we have a panic action
					if(panic_sec != static_cast<unsigned int>(-1)){
						timeout = difftime(timeout_time, curr_time) * 1000;
					}

					
					
				} else {
					//cout << "Link up" << endl;
					// OK, we're up
					prev_up = true;
					timeout = -1;
				}
				//
				//cout << "New timeout: " << timeout << endl;
			}
		} else {
			//cout << "Nothing to read!  PANIC!!!" << endl;
			
			// Execute the callback for panic
			if(panic_action){
				panic_action();
			}
			
			update_wired_status();
			
			timeout = -1;
			if(!all_up()){
				time(&curr_time);
				// set the timeout to wait from now until the timeout time

				// NOTE: no need to check for panic_sec != -1 here, because
				// if we're here, we MUST have timed out, which means that the
				// timeout != -1, which can ONLY happen if panic_sec != -1.
				timeout_time = curr_time + panic_sec;
				timeout = difftime(timeout_time, curr_time) * 1000;
			}
			
		}
	}
}

void NetworkMonitor::socket_setup(){
	// open netlink socket
    sock = socket(AF_NETLINK,SOCK_RAW,NETLINK_ROUTE);
     
    if (sock>0){
    
		struct sockaddr_nl addr;

		addr.nl_family = AF_NETLINK;
		addr.nl_pad = 0;
		addr.nl_pid = 0;
		
		// listen for up/down events
		addr.nl_groups = RTMGRP_LINK | RTMGRP_IPV4_IFADDR;

		if (bind(sock,(struct sockaddr *)&addr,sizeof(addr))<0)
		    sock = -1;
    }
}

bool NetworkMonitor::read_status(){
	bool change = false;
	int status;
	char buf[4096];
	struct iovec iov = { buf, sizeof buf };
	struct sockaddr_nl snl;
	struct msghdr msg = { (void*)&snl, sizeof snl, &iov, 1, NULL, 0, 0};
	struct nlmsghdr *h;
	bzero(buf, sizeof(buf));

	//printf("getting ready to read...\n");
	status = recvmsg(sock, &msg, 0);
	
	if (status > 0){
	
		for(h = (struct nlmsghdr *) buf; NLMSG_OK (h, (unsigned int)status); h = NLMSG_NEXT (h, status)) {
			if (h->nlmsg_type == RTM_NEWLINK){
				//printf("netlink_link\n");
				// new link message here, heads up!
			    struct ifinfomsg *ifi;
			    ifi = (ifinfomsg*) NLMSG_DATA(h);
			    
			    // check to see if it's a wired interface:
			    if(wired_status.count(ifi->ifi_index)){
			    	//printf("WIRED!! netlink_link_state: \nLink index: %d\nStatus: %s\nFlags: %x\n", ifi->ifi_index,
				    //	(ifi->ifi_flags & IFF_RUNNING)?"Up":"Down", ifi->ifi_flags);
				    change = true;
				    if (ifi->ifi_flags & IFF_RUNNING){
						wired_status[ifi->ifi_index].status |= LINK_UP;
				    } else {
				    	wired_status[ifi->ifi_index].status &= ~LINK_UP;
				    }

				    // get the MAC address
				    int len = h->nlmsg_len - NLMSG_LENGTH(sizeof(*ifi));
				    for(struct rtattr* attr = IFLA_RTA(ifi); RTA_OK(attr, len); attr = RTA_NEXT(attr, len)){
				    	switch(attr->rta_type){
				    	case IFLA_ADDRESS:
				    		memcpy(wired_status[ifi->ifi_index].mac, (uint8_t*) RTA_DATA(attr), 6);
				    		break;
				    	default:
				    		// do nothing
				    		break;
				    	}
				    }


				}


			} else if (h->nlmsg_type & (RTM_NEWADDR | RTM_DELADDR) ){
			    struct ifaddrmsg *ifa;
			    struct rtattr *rta;
				int rtattrlen;
				struct in_addr *inp;

			    
			    ifa = (ifaddrmsg*) NLMSG_DATA(h);
			    rta = (rtattr*) IFA_RTA(ifa);
			    //printf("netlink_addr: %d\n", ifa->ifa_index);
			    
			    // check to see if it's a wired interface, and IPv4:
			    if(wired_status.count(ifa->ifa_index) && ifa->ifa_family == AF_INET){
			    	//printf("WIRED!! netlink_addr_state: \nLink index: %d\nStatus: %s\n", ifa->ifa_index, (h->nlmsg_type == RTM_NEWADDR)?"Up":"Down");
			    	change = true;
			    	if (h->nlmsg_type == RTM_NEWADDR){
			    		wired_status[ifa->ifa_index].status |= ADDR_UP;
			    		
			    		rtattrlen = IFA_PAYLOAD(h);

						for (; RTA_OK(rta, rtattrlen); rta = RTA_NEXT(rta, rtattrlen)) {
     
							if(rta->rta_type == IFA_ADDRESS){
								inp = (struct in_addr *)RTA_DATA(rta);
								wired_status[ifa->ifa_index].addr = *inp;
							}
						}
	    		
			    	} else {
			    		// Link address was removed, set "address up bit to false, and set the IP address to "0.0.0.0"
			    		wired_status[ifa->ifa_index].status &= ~ADDR_UP;
			    		wired_status[ifa->ifa_index].addr.s_addr = 0;
			    	}
				} 
			}
		}
	}
	
	return change;

}

/*
Gets a list of all of the wired devices, using udev.  The return
value will be a mapping of the interface index to the status, where
here the status will be initialized to 0 (i.e., everything down)
*/
void NetworkMonitor::get_wired_devices(){
	struct udev *udev;
	struct udev_enumerate *enumerate;
	struct udev_list_entry *devices, *dev_list_entry;
	struct udev_device *dev;
	
	// clear the wired_status
	wired_status.clear();

	/* Create the udev object */
	udev = udev_new();
	if (!udev) {
		printf("Can't create udev\n");
		exit(1);
	}

	/* Create a list of the devices in the 'net' subsystem. */
	enumerate = udev_enumerate_new(udev);
	udev_enumerate_add_match_subsystem(enumerate, "net");
	udev_enumerate_scan_devices(enumerate);
	devices = udev_enumerate_get_list_entry(enumerate);

	/* For each item enumerated, print out its information.
	   udev_list_entry_foreach is a macro which expands to
	   a loop. The loop will be executed for each member in
	   devices, setting dev_list_entry to a list entry
	   which contains the device's path in /sys. */
	udev_list_entry_foreach(dev_list_entry, devices) {
	
		/* Will hold the absolute path */
		const char *path, *dev_name;
		
		/* Get the filename of the /sys entry for the device
		   and create a udev_device object (dev) representing it */
		path = udev_list_entry_get_name(dev_list_entry);
		dev = udev_device_new_from_syspath(udev, path);

//		printf("Device Path: %s\n", path);

		if ( is_wired(dev) ){
			dev_name = udev_device_get_sysname(dev);
			
			// NOTE: the 1st line will create using the default constructor
			// The second + 3rd lines line will modify the newly created record
			IFStatus* new_stat = &wired_status[if_nametoindex(dev_name)];
			new_stat->status = 0;
			new_stat->addr.s_addr = 0;
		
/*			printf("Found an ethernet device: %s\n", dev_name);
			printf("IF index is: %d\n", if_nametoindex(dev_name));

			printf("Type: %s\nMAC: %s\nState: %s\nParent: %p\n",
				udev_device_get_devtype(dev),
		        udev_device_get_sysattr_value(dev,"address"),
		        udev_device_get_sysattr_value(dev, "operstate"),
				(void *) udev_device_get_parent(dev));
*/				
		}

		udev_device_unref(dev);
	}
	/* Free the enumerator object */
	udev_enumerate_unref(enumerate);


}

void NetworkMonitor::send_linkrequest(void* buf, int len){

	struct sockaddr_nl pa;
	bzero(&pa, sizeof(pa));
	pa.nl_family = AF_NETLINK;

	struct msghdr msg;
	struct iovec iov;
		
	bzero(&msg, sizeof(msg));
	msg.msg_name = (void *) &pa;
	msg.msg_namelen = sizeof(pa);
	
	// place the pointer & size of the RTNETLINK
	// message in the struct msghdr
	iov.iov_base = buf;
	iov.iov_len = len;
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;

	// send the RTNETLINK message to kernel
	sendmsg(sock, &msg, 0);
}

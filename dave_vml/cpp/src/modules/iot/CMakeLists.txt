project (vmliotLib)
set (LIB_NAME vmliot)

set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(IOT_CLIENT_DIR "../../../external/aws-iot-sdk/aws_iot_src")

set (INCLUDE_ALL_DIRS	
	${VML_ROOT}/include/modules/iot
	${IOT_CLIENT_DIR}/shadow
	${IOT_CLIENT_DIR}/utils
	${IOT_CLIENT_DIR}/protocol/mqtt
	#${Hiredis_INCLUDE_DIR}
)

set (LIB_SRC
	MQTT-client-cpp.cpp
	IoT_Gateway.cpp
	IOTCommunicator.cpp
	Dev_Common.cpp
	NetworkMonitor.cpp
	MQTTClient.cpp
	)

vml_add_library (${LIB_NAME}
	STATIC
	${LIB_SRC}
	${PROJECT_HEADER_FILES}
	${INCLUDE_ALL_DIRS}
	#${Hiredis_LIBS_DIR}
	)

target_link_libraries (${LIB_NAME}
	vmlutility
	mosquitto
	udev
	mosquitto
	boost_system
	boost_regex
	boost_thread
	boost_date_time
	)

include_directories(${PROJECT_NAME}  
	PUBLIC 
	${INCLUDE_ALL_DIRS}
)


#Prep ourselves for compiling boost
find_package(Boost COMPONENTS unit_test_framework REQUIRED)
include_directories (${Boost_INCLUDE_DIRS})

#I like to keep test files in a separate source directory called test
file(GLOB TEST_SRCS RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} test/*.cpp)

#Run through each source
foreach(testSrc ${TEST_SRCS})
	#Extract the filename without an extension (NAME_WE)
	get_filename_component(testName ${testSrc} NAME_WE)

	#Add compile target
	add_executable(${testName} ${testSrc})

	#link to Boost libraries AND your targets and dependencies
	target_link_libraries(${testName} ${Boost_LIBRARIES} 
		vmlutility
		vmliot
		udev
		mosquitto
		boost_system
		boost_regex
		boost_thread
		boost_date_time
		${Hiredis_LIBS})

	#I like to move testing binaries into a testBin directory
	set_target_properties(${testName} PROPERTIES 
		RUNTIME_OUTPUT_DIRECTORY  ${CMAKE_BINARY_DIR}/testBin)

	#Finally add it to test execution - 
	#Notice the WORKING_DIRECTORY and COMMAND
	add_test(NAME ${testName}
		WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/testBin 
		COMMAND ${CMAKE_BINARY_DIR}/testBin/${testName} )
endforeach(testSrc)


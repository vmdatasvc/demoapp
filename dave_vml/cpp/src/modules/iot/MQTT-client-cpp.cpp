/*
 * MQTT-client-cpp.cpp
 *
 *  Created on: Apr 28, 2015
 *      Author: pshin
 */

#include <modules/iot/MQTT-client-cpp.hpp>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>
#include <unistd.h>		// -- for getpid()

/*
 * To install Mosquitto libraries on ubuntu,

$ sudo apt-add-repository ppa:mosquitto-dev/mosquitto-ppa
$ sudo apt-get update
$ sudo apt-get install libmosquitto-dev libmosquittopp-dev mosquitto-clients

 */


#define DEBUG_MQTT_MSG	true
///* How many seconds the broker should wait between sending out
// * keep-alive messages. */
//#define KEEPALIVE_SECONDS 60
//
///* Hostname and port for the MQTT broker. */
//#define BROKER_HOSTNAME "iot.eclipse.org" // "test.mosquitto.org" //"dev.rabbitmq.com" // "iot.eclipse.org"		// -- "localhost"
//#define BROKER_PORT 1883
//#define PUBLISH_TOPIC "tockkk"

using namespace std;

MosqMQTT::MosqMQTT(const char * _id, bool clean_session) : mosquittopp(_id, clean_session)
{
	 mosqpp::lib_init();        // Mandatory initialization for mosquitto library
	 mPid = getpid();

	 mClientId << _id << "-" << mPid;

	 reinitialise(mClientId.str().c_str(), clean_session);
}

MosqMQTT::~MosqMQTT() {
	 loop_stop();            // Kill the thread
	 mosqpp::lib_cleanup();    // Mosquitto library cleanup
 }

void MosqMQTT::start() {

	if(DEBUG_MQTT_MSG)
		cout << ">> myMosq - initiate Connection" << endl;

	 connect(mBrokerAddr.c_str(), mBrokerPort, mKeepalive);

	 if(DEBUG_MQTT_MSG)
	 		cout << ">> myMosq - loop_start" << endl;
	 loop_start();            // Start thread managing connection / publish / subscribe
}

// -- This should be called before start()
void MosqMQTT::subscribeToTopic(string subTopic) {
	 //return subscribe(NULL, (const char *)subTopic.c_str(), qos);

	mSubTopics.push_back(subTopic);
}

int MosqMQTT::send_message(string pubTopic, string _message, int qos)
 {
	 // Send message - depending on QoS, mosquitto lib managed re-submission this the thread
	 //
	 // * NULL : Message Id (int *) this allow to latter get status of each message
	 // * topic : topic to be used
	 // * lenght of the message
	 // * message
	 // * qos (0,1,2)
	 // * retain (boolean) - indicates if message is retained on broker or not
	 // Should return MOSQ_ERR_SUCCESS

	int ret = -999;

	try {
		ret = publish(NULL, pubTopic.c_str(),_message.size(), _message.c_str(),qos,false);
	} catch (exception& e) {
		if(DEBUG_MQTT_MSG)
			printf("Error: Failed to publish\n%s\n", e.what());
	}

	 return ret; // -- If ret == MOSQ_ERR_SUCCESS, then ret would be zero;
 }

bool MosqMQTT::match(const char *topic, const char *key) {
    return 0 == strncmp(topic, key, strlen(key));
}

void MosqMQTT::on_message(const struct mosquitto_message *msg) {
    if (msg == NULL) { return; }

    if(DEBUG_MQTT_MSG)
		printf("-- got a msg @ Topic: %s (%d, QoS %d, %s), MSG: '%s'\n",
			msg->topic, msg->payloadlen, msg->qos, msg->retain ? "R" : "!r",
			(char *)msg->payload);

//    if (match(msg->topic, "tickkk")) {
//        if (0 == strncmp((const char*)msg->payload, "tick", 4 /*msg->payloadlen*/)) {
//
//            // -- Compose a message to publish
//            string payload;
//            payload.append(mPubTopic);
//            cout << "payload = " << payload << endl;
//
//            // -- Publish a message to the broker w/ the PubTopic
//            //int res = publish(NULL, mPubTopic, payloadlen, payload, 0, false);
//            //int res = publish(NULL, mPubTopic, payload.size(), payload.c_str(), 0, false);
//            int res = send_message(mPubTopic.c_str(), payload, 0);
//
//            //if (res != MOSQ_ERR_SUCCESS)
//			if (!res)
//                printf("##### NOT Published ####\n");
//
//        } else {
//            printf("invalid 'tick' message\n");
//        }
//    } else if (match(msg->topic, "control/")) {
//        /* This will cover both "control/all" and "control/$(PID)".
//         * We won'st see "control/$(OTHER_PID)" because we won't be
//         * subscribed to them.*/
//        if (0 == strncmp((const char*)msg->payload, "halt", msg->payloadlen)) {
//            printf("*** halt\n");
//            (void)disconnect();
//        }
//    }

    fflush(stdout);
}

void MosqMQTT::on_disconnect(int rc) {
	if(DEBUG_MQTT_MSG)
		cout << ">> myMosq - disconnection(" << rc << ")" << endl;
}

void MosqMQTT::on_connect(int rc)
{
	if ( rc == 0 ) {	 /* success */
		if(DEBUG_MQTT_MSG)
			cout << ">> myMosq - connected with server" << endl;

    	// -- Subscribe to topics after a successful connection is made
    	for(vector<string>::iterator it = mSubTopics.begin(); it != mSubTopics.end(); ++it) {
    		try {
    			string itt = *it;
    			subscribe(NULL, (const char*)(itt.c_str()), 1);

    			if(DEBUG_MQTT_MSG)
    				printf("[MQTT-client-cpp] Subscribe to %s\n", itt.c_str());
			} catch (exception& e) {
				if(DEBUG_MQTT_MSG)
					printf("Error: Failed to subscribe\n%s\n", e.what());
			}
    	}

	} else {
		if(DEBUG_MQTT_MSG)
			cout << ">> myMosq - Impossible to connect with server(" << rc << ")" << endl;
	}

 }

void MosqMQTT::on_publish(int mid) {
//	if(DEBUG_MQTT_MSG)
//		cout << ">> myMosq - Message (" << mid << ") succeed to be published " << endl;
}

void MosqMQTT::on_subscribe(int mid, int qos_count, const int *granted_qos) {
	if(DEBUG_MQTT_MSG)
		cout << ">> myMosq - Message (" << mid << ") succeed to be subscribed " << endl;
}

//int MQTT_client() {
//
//	// -- Create a mosquitto MQTT client
//	MosqMQTT* myMQTT= new MosqMQTT();
//
//	// -- Initialize the mosquitto MQTT client
//	{
//		//myMQTT->threaded_set(true);		// -- Tell if this application is threaded or not
//
//		myMQTT->mKeepalive = 60;    // Basic configuration setup for myMosq class
//		myMQTT->mBrokerPort = BROKER_PORT;
//		myMQTT->mBrokerAddr.append(BROKER_HOSTNAME);
//		myMQTT->mPubTopic.append(PUBLISH_TOPIC);
//
//		myMQTT->mSubTopics.push_back("tickkk");
//		myMQTT->mSubTopics.push_back("control/all");
//		{
//			size_t sz = 32;
//			char control_pid[sz];
//			snprintf(control_pid, sz, "control/%d", myMQTT->mPid);
//			myMQTT->mSubTopics.push_back(control_pid);
//		}
//	}
//
//	// -- Start the mosquitto MQTT client
//	myMQTT->start();
//
//	while(1) {
//		sleep(1);
//	}
//
//	return 1;
//}
//
//void *MQTT_Thread()
//{
//
//	//RemoteTcpdumpAnalyzer* pb = (RemoteTcpdumpAnalyzer*)apdata;
//
//	MQTT_client();
//
//	return NULL;
//}
//
//extern pthread_mutex_t gMutex;
//
//pthread_attr_t mThreadAttr;
//pthread_t mMQTT_thread;
//
//int main() {
//
//	pthread_attr_init(&mThreadAttr);
//	pthread_attr_setdetachstate(&mThreadAttr, PTHREAD_CREATE_JOINABLE);
//
//	if (pthread_create(&mMQTT_thread, &mThreadAttr, (void *(*)(void*))MQTT_Thread, NULL))
//	{
//		fprintf(stderr, "Error in pthread_create(MQTT_Thread)\n");
//		return false;
//	} else {
//		printf( " -- pthread_create(MQTT_Thread) -- \n");
//	}
//
//	while(1) {
//		sleep(1);
//	}
//
//}

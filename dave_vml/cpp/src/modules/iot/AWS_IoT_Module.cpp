/*
 * AWS_IoT_Module.cpp
 *
 *  Created on: Feb 2, 2016
 *      Author: paulshin
 */

#include "modules/iot/AWS_IoT_Module.hpp"
#include "modules/utility/TimeKeeper.hpp"
#include <vector>
#include <iosfwd>
#include <algorithm>
#include <string>
#include "modules/utility/JsonUtil.hpp"
#include <hiredis/hiredis.h>

#define DEBUG_MSG 	false

using namespace std;

fpActionCallback_t		AWS_IoT_Module::mShadowUpdateCallbackPtr;
json AWS_IoT_Module::mDeltaMsgJson;
bool AWS_IoT_Module::mIsDeltaMsgRecived;
bool AWS_IoT_Module::mRecover;
bool AWS_IoT_Module::mLoadDataFromDB;
json previousReport;
json lastUpdate;
string path_to_previous_report_json = "config/OmniSensr_AWS_IoT_Connector/previousReport.json";
string path_to_last_update_json = "config/OmniSensr_AWS_IoT_Connector/lastUpdate.json";
string s_patt = "##separate##";

pthread_mutex_t _gMutexx_;

AWS_IoT_Module::AWS_IoT_Module() {

	pthread_attr_init(&mThreadAttr);
	pthread_attr_setdetachstate(&mThreadAttr, PTHREAD_CREATE_JOINABLE);

	mIsDeltaMsgRecived = false;

	mThingShadowInfo = ShadowParametersDefault;

	mJsonDocumentBuffer_Periodic_size = sizeof(mJsonDocumentBuffer_Periodic) / sizeof(mJsonDocumentBuffer_Periodic[0]);
	mJsonDocumentBuffer_Delta_size = sizeof(mJsonDocumentBuffer_Delta) / sizeof(mJsonDocumentBuffer_Delta[0]);

	mDeltaObject.pData = mJsonDocumentBuffer_Delta;
	mDeltaObject.pKey = "state";
	mDeltaObject.type = SHADOW_JSON_OBJECT;
	mDeltaObject.cb = (jsonStructCallback_t)DeltaCallback;

	mStateUpdateIntervalScale = 1.0f;

	mRecover = true;
	mLoadDataFromDB = true;
	//mMqttClient.setAutoReconnectStatus(true);

}

AWS_IoT_Module::~AWS_IoT_Module() {

}

json AWS_IoT_Module::GetPreviousReport() {
	return previousReport;
}

void AWS_IoT_Module::ThingShadow_Init(char *thingName, char *myClientID, char *hostUrl, unsigned int hostPort, char *certDirect
									, char *rootCaFileName, char *certFileName, char *privateKeyFileName, unsigned int mqttCommandTimeout_ms, unsigned int tlsHandshakeTimeout_ms
		) {
	mThingShadowInfo.pMyThingName = thingName;
	mThingShadowInfo.pMqttClientId = myClientID;
	mThingShadowInfo.pHost = hostUrl;
	mThingShadowInfo.port = hostPort;

	mThingShadowInfo.mqttCommandTimeout_ms = mqttCommandTimeout_ms;
	mThingShadowInfo.tlsHandshakeTimeout_ms = tlsHandshakeTimeout_ms;


	sprintf(mCertDirectory, "%s", certDirect);
	sprintf(mHostAddress, "%s", hostUrl);
	mMQTT_Port = hostPort;

	char mCurrentWD[PATH_MAX + 1];
	getcwd(mCurrentWD, sizeof(mCurrentWD));
	sprintf(mRootCA, "%s/%s/%s", mCurrentWD, mCertDirectory, rootCaFileName);
	sprintf(mClientCRT, "%s/%s/%s", mCurrentWD, mCertDirectory, certFileName);
	sprintf(mClientKey, "%s/%s/%s", mCurrentWD, mCertDirectory, privateKeyFileName);

//	DEBUG("Using rootCA %s", mRootCA);
//	DEBUG("Using clientCRT %s", mClientCRT);
//	DEBUG("Using clientKey %s", mClientKey);

	mThingShadowInfo.pClientCRT = mClientCRT;
	mThingShadowInfo.pClientKey = mClientKey;
	mThingShadowInfo.pRootCA = mRootCA;

	DEBUG("ThingShadowInfo.pMyThingName = %s", mThingShadowInfo.pMyThingName);
	DEBUG("ThingShadowInfo.pMqttClientId = %s", mThingShadowInfo.pMqttClientId);
	DEBUG("ThingShadowInfo.pHost = %s", mThingShadowInfo.pHost);
	DEBUG("ThingShadowInfo.port = %d", mThingShadowInfo.port);
	DEBUG("ThingShadowInfo.pClientCRT = %s", mThingShadowInfo.pClientCRT);
	DEBUG("ThingShadowInfo.pClientKey = %s", mThingShadowInfo.pClientKey);
	DEBUG("ThingShadowInfo.pRootCA = %s", mThingShadowInfo.pRootCA);

	// -- Load previoudReport.json to previousReport
	previousReport.clear();
	JsonUtil::readJson(path_to_previous_report_json, previousReport);

	// -- Load lastUpdate.json to lastUpdate
	lastUpdate.clear();
	JsonUtil::readJson(path_to_last_update_json, lastUpdate);
}

IoT_Error_t AWS_IoT_Module::Shadow_ConnectInit() {
	INFO("[AWS_IoT_Module] Shadow Init");

	IoT_Error_t rc = NONE_ERROR;

	rc = aws_iot_mqtt_autoreconnect_set_status(true);

	if (NONE_ERROR != rc) {
		ERROR("Failed to set autoreconnect");
		return rc;
	}

	// -- Disable old delta msgs
	aws_iot_shadow_disable_discard_old_delta_msgs();

	// -- MQTT init
	aws_iot_mqtt_init(&mMqttClient);

	rc = aws_iot_shadow_init(&mMqttClient);
	if (NONE_ERROR != rc) {
		ERROR("Shadow Connection Error");
		return rc;
	}

	INFO("[AWS_IoT_Module] Shadow Connect");
	rc = aws_iot_shadow_connect(&mMqttClient, &mThingShadowInfo);
	if (NONE_ERROR != rc) {
		ERROR("Shadow Connection Error");
		return rc;
	}


	/*
	 * Register the jsonStruct object as delta object
	 */
	rc = aws_iot_shadow_register_delta(&mMqttClient, &mDeltaObject);

	for(int appType = 0; appType < MAX_APP_TYPE_NUM; appType++) {
		for(vector<jsonStruct_t *>::iterator it = mAppParamsToBeReportedAtThisRound[appType].begin(); it != mAppParamsToBeReportedAtThisRound[appType].end(); ++it) {
			jsonStruct_t * appParams = *it;

			if(appParams->cb != NULL)
				rc = aws_iot_shadow_register_delta(&mMqttClient, appParams);
		}
	}

	if (NONE_ERROR != rc) {
		ERROR("Shadow Register Delta Error");
	}

	return rc;
}

int AWS_IoT_Module::Shadow_addParam(jsonStruct_t *paramNew, unsigned int appType) {
	int ret;
	bool isFound = false;

	pthread_mutex_lock(&_gMutexx_);
	for(vector<jsonStruct_t *>::iterator it = mAppParamsToBeReportedAtThisRound[appType].begin(); it != mAppParamsToBeReportedAtThisRound[appType].end(); ++it) {
		jsonStruct_t * appParams = *it;

		if(paramNew == appParams) {
			isFound = true;
			break;
		}
	}

	if(isFound == false) {
		mAppParamsToBeReportedAtThisRound[appType].push_back(paramNew);
	}
	ret = mAppParamsToBeReportedAtThisRound[appType].size();
	pthread_mutex_unlock(&_gMutexx_);

//	printf("mAppParams [%d]th: Key = %s\n", mAppParams.size(), paramNew->pKey);

	return ret;

}

void AWS_IoT_Module::DeltaCallback(const char *pJsonValueBuffer, const uint32_t valueLength, jsonStruct_t *pContext) {

	char valueOnlyBuf[AWS_IOT_MQTT_RX_BUF_LEN] = {'\0'};
	snprintf(valueOnlyBuf, valueLength+1, "%.*s", valueLength+1, pJsonValueBuffer);

	INFO("---------------------------------------------------------------------------------------");
	INFO("[AWS_IoT_Module] Received Delta message %.*s --> [%s]", valueLength, pJsonValueBuffer, valueOnlyBuf);
	INFO("---------------------------------------------------------------------------------------");

	stringstream deltaMsgReceived("");
	deltaMsgReceived.clear();

	mIsDeltaMsgRecived = true;

	deltaMsgReceived << valueOnlyBuf << endl;

	mDeltaMsgJson.clear();
	mDeltaMsgJson << deltaMsgReceived;


//	// define parser callback
//	json::parser_callback_t deltaCallBackForFilteringForEachApp = [](int depth, json::parse_event_t event, json & parsed)
//	{
////		ostringstream appTypeStr;
////		appTypeStr << appType;
//
//	    // skip object elements with key "Thumbnail"
//	    if (event == json::parse_event_t::key and parsed == json("Thumbnail"))
//		//if (event == json::parse_event_t::key and parsed == json(appTypeStr.str()))
//	    {
//	        return true;
//	    }
//	    else
//	    {
//	        return false;
//	    }
//	};
//
//	// -- DEBUG
////	{
////		std::cout << "[AWS_IoT_Module] Received Delta message: \n";
////		std::cout << std::setw(2) << mDeltaMsgJson << '\n';
////	}
//
//	for(int i=0; i<MAX_APP_TYPE_NUM; i++) {
//		mDeltaMsgJsonPerApp[i] = json::parse(deltaMsgReceived.str(), deltaCallBackForFilteringForEachApp);
//	}
}

//void AWS_IoT_Module::Shadow_UpdateMsg(float temperature, char *shadowMsg) {
//
//	sprintf(shadowMsg, "(Temperature = %f)", temperature);
//
//}

//void AWS_IoT_Shadow::parseInputArgsForConnectParams(int argc, char** argv) {
//	int opt;
//
//	while (-1 != (opt = getopt(argc, argv, "h:p:c:n:"))) {
//		switch (opt) {
//		case 'h':
//			strcpy(mHostAddress, optarg);
//			DEBUG("Host %s", optarg);
//			break;
//		case 'p':
//			mMQTT_Port = atoi(optarg);
//			DEBUG("arg %s", optarg);
//			break;
//		case 'c':
//			strcpy(mCertDirectory, optarg);
//			DEBUG("cert root directory %s", optarg);
//			break;
//		case 'n':
//			mNumPubs = atoi(optarg);
//			DEBUG("num pubs %s", optarg);
//			break;
//		case '?':
//			if (optopt == 'c') {
//				ERROR("Option -%c requires an argument.", optopt);
//			} else if (isprint(optopt)) {
//				WARN("Unknown option `-%c'.", optopt);
//			} else {
//				WARN("Unknown option character `\\x%x'.", optopt);
//			}
//			break;
//		default:
//			ERROR("ERROR in command line argument parsing");
//			break;
//		}
//	}
//
//}



// -- Pass in the previous and current report, return only the difference
json getUpdateJson(json &prev, json cur) {
	json diff = json::diff(prev, cur);
	json update;
	for (auto it = diff.begin(); it != diff.end(); ) {
		string op = (*it)["op"];
		// -- Drop remove operations in diff
		if (op.compare("remove") == 0) {
			diff.erase(it);
			continue;
		}
		// -- Objects in replace and add operations should be added to update json
		if (op.compare("replace") == 0 || op.compare("add") == 0) {
			JsonUtil::addPatchToJson(*it, update);
		}
		it++;
	}
	// -- Update the previousReport json
	prev = prev.patch(diff);
	return update;
}

// -- Format json string in mJsonDocumnetBuffer_Periodic char array
void AWS_IoT_Module::ParseParamsFromBuffer(IoT_Error_t &rc) {
	// -- See if there is any state info to report.
	int paramToReport_num = 0;
	for(int appType = 0; appType < MAX_APP_TYPE_NUM; appType++) {
		for(vector<jsonStruct_t *>::iterator it = mAppParamsToBeReportedAtThisRound[appType].begin(); it != mAppParamsToBeReportedAtThisRound[appType].end(); ++it) {
			paramToReport_num++;
		}
	}
	for (uint i = 0; i < mJsonDocumentBuffer_Periodic_size; i++) {
		mJsonDocumentBuffer_Periodic[i] = '\0';
	}
	if(paramToReport_num > 0) {
		// -- Initialize the Shadow message buffer
		rc = aws_iot_shadow_init_json_document(mJsonDocumentBuffer_Periodic, mJsonDocumentBuffer_Periodic_size);
		if (NONE_ERROR != rc) {
			ERROR("[AWS_IoT_Module] aws_iot_shadow_init_json_document() error %d", rc);

		} else if(rc == NONE_ERROR) {

			pthread_mutex_lock(&_gMutexx_);

			for(int appType = 0; appType < MAX_APP_TYPE_NUM; appType++) {

				mAppParam_size[appType] = 0;

				if(mAppParamsToBeReportedAtThisRound[appType].size() > 0) {

					// -- Initialize per-app message buffer

					for(vector<jsonStruct_t *>::iterator it = mAppParamsToBeReportedAtThisRound[appType].begin(); it != mAppParamsToBeReportedAtThisRound[appType].end(); ++it) {
						jsonStruct_t *param = (jsonStruct_t *)(*it);

						// -- Send the state info only if it is refreshed by the corresponding app
						mAppParam_arr[appType][mAppParam_size[appType]] = *param;


						// -- DEBUG
						mAppParam_size[appType]++;



						if(mAppParam_size[appType] >= MAX_STATE_PARAM_NUM_PER_APP)
							break;
					}

					if (NONE_ERROR != rc)
						ERROR("aws_iot_shadow_add_reported() error %d", rc);


					mAppParamsToBeReportedAtThisRound[appType].clear();

				}
			}

			rc = aws_iot_shadow_add_reported_array(mJsonDocumentBuffer_Periodic, mJsonDocumentBuffer_Periodic_size, MAX_APP_TYPE_NUM, MAX_STATE_PARAM_NUM_PER_APP, mAppParam_size, (jsonStruct_t *)mAppParam_arr);

			pthread_mutex_unlock(&_gMutexx_);

			if (NONE_ERROR != rc)
				ERROR("[AWS_IoT_Module] aws_iot_shadow_init_json_document() error %d", rc);

		}

		if (rc == NONE_ERROR) {

			// -- Add some more meta-data to the message buffer in the JSON format and finalize the message composition
			rc = aws_iot_finalize_json_document(mJsonDocumentBuffer_Periodic, mJsonDocumentBuffer_Periodic_size);
		}
	}
}

// -- Convert mJsonDocumentBuffer_Periodic char array to json and store in currentReport.
// -- Returns necessary update json. Saves current state to local currentState.json.
// -- Modify mJsonDocumentBuffer_Periodic according to the update json.
json AWS_IoT_Module::UpdateReport(IoT_Error_t &rc) {
	json currentReport;
	ParseParamsFromBuffer(rc);
	//printf("mJsonDocu: %s\n", mJsonDocumentBuffer_Periodic);
	if (rc == NONE_ERROR && strlen(mJsonDocumentBuffer_Periodic) > 0) {
		// -- Convert JsonDocumnetBuffer to json object and get the update json
		try {
			currentReport = json::parse(mJsonDocumentBuffer_Periodic);
		}
		catch (...) {
			cout << "[Aws_IoT_Module::UpdateReport] Error parsing json string:" << endl;
			printf("%s\n", mJsonDocumentBuffer_Periodic);
			currentReport.clear();
			return currentReport;
		}
		if (previousReport.size() == 0) {
			previousReport = currentReport;
		}
		else {
			json update = getUpdateJson(previousReport, currentReport);
			// -- If the only difference is clientToken then discard
			if (update.size() == 1 && update["clientToken"] != NULL) {
				update.clear();
			}
			// -- Store update to currentReport
			currentReport = update;
		}
		// -- If there are things to report, copy into mJsonDocumentBuffer_Periodic
		if (currentReport.size() > 0) {
			// -- If there's no client token, get one from previous report
			if (currentReport["clientToken"] == NULL) {
				currentReport["clientToken"] = previousReport["clientToken"];
			}
			string currentReport_string = currentReport.dump();
//			cout << "[AWS_IoT_Module] Current State:" << endl;
//			cout << previousReport.dump(4) << endl;
			cout << "[AWS_IoT_Module] Update Json:" << endl;
			cout << currentReport.dump() << endl;
			for (uint i = 0; i < currentReport_string.length() && i < mJsonDocumentBuffer_Periodic_size; i++) {
				mJsonDocumentBuffer_Periodic[i] = currentReport_string[i];
			}
			if (currentReport_string.length() < mJsonDocumentBuffer_Periodic_size) {
				mJsonDocumentBuffer_Periodic[currentReport_string.length()] = '\0';
			}

			// -- Save previousReport into currentState.json
			JsonUtil::writeJson(path_to_previous_report_json, previousReport);
		}
		else {
			// -- Set the first char to 0 so strlen will return 0
			mJsonDocumentBuffer_Periodic[0] = '\0';
		}

	}
	return currentReport;
}

void AWS_IoT_Module::SaveDataMsgToDB() {
	if(mPubMsg_Queue_out.size() > 0) {
		unsigned int j;
		redisContext *c;
		redisReply *reply;
		const char *hostname = "127.0.0.1";
		int port = 6379;

		struct timeval timeout = { 1, 500000 }; // 1.5 seconds
		c = redisConnectWithTimeout(hostname, port, timeout);
		if (c == NULL || c->err) {
			if (c) {
				printf("Connection to redis error: %s\n", c->errstr);
				redisFree(c);
			} else {
				printf("Connection to redis error: can't allocate redis context\n");
			}
			exit(1);
		}
		for(vector<PubMsg_t>::iterator it = mPubMsg_Queue_out.begin(); it != mPubMsg_Queue_out.end(); ++it) {
			PubMsg_t *msg_to_pub = &*it;

			// -- Combine topic message and qos into a string and save into redis database
			string combined_msg = msg_to_pub->topic + s_patt + msg_to_pub->msg + s_patt
					+ std::to_string((int)msg_to_pub->qos) + s_patt + std::to_string(msg_to_pub->appType);
			reply = (redisReply*)redisCommand(c,"RPUSH datachannel %s", combined_msg.c_str());
			if (reply->type == REDIS_REPLY_ERROR) {
				cout << "[AWS_IOT_MODULE] Msg failed to save in redis: " << combined_msg << endl;
			}
			else {
				cout << "[AWS_IOT_MODULE] Msg saved in redis: " << combined_msg << endl;
			}
			freeReplyObject(reply);

		}
		redisFree(c);
	}
}

void AWS_IoT_Module::PeriodicProcessing_Loop() {

	IoT_Error_t rc = NONE_ERROR;

	double currTime, prevTime_shadowUpdate, prevTime_delta, prevTime_pub, prevTime_full, prevTime_desired;

	mPrevTime_shadowUpdate = prevTime_delta = prevTime_pub = prevTime_full = gCurrentTime() - mInitialTime;
	// -- Set to negative value so the it will be triggered when the app first launch
	prevTime_desired = -mPeriodicRefreshDesiredInterval;
	// -- Push out the very first state quickly
	mPrevTime_shadowUpdate -= mPeriodicShadowReportInterval;
	string shadowupdate = mShadowUpdateTopic;
	string s_json;

	INFO("[AWS_IoT_Module] PeriodicProcessing loop is created. \n");

	while(1) {
		rc = NONE_ERROR;
		if(mMQTT_Subscriber.size() > 0)
			for(vector<SubParam_t>::iterator it = mMQTT_Subscriber.begin(); it != mMQTT_Subscriber.end(); ++it) {
				SubParam_t *subParam = &*it;
				INFO("[AWS_IoT_Module] Subscribing to %s...", subParam->param.pTopic);
				rc = aws_iot_mqtt_subscribe(&subParam->param);
				if (NONE_ERROR != rc) {
					ERROR("Error subscribing %d", rc);
					break;
				}
				else {
					cout <<"[AWS_IoT_Module] Subscribed to " << subParam->topic << endl;
				}
			}
		if (rc == NONE_ERROR) {
				// -- Send previous update to AWS if device had been offline
			if (mRecover) {
				// -- If lastUpdate is not empty, send to AWS
				if (lastUpdate.size() > 0) {
					cout << "[Aws_IoT_Module] Send last update:" << endl;
					cout << lastUpdate.dump() << endl;
					string json_s = lastUpdate.dump();
					uint i = 0;
					for (i = 0; i < json_s.length() && i < mJsonDocumentBuffer_Periodic_size; i++) {
						mJsonDocumentBuffer_Periodic[i] = json_s[i];
					}
					mJsonDocumentBuffer_Periodic[i] = '\0';
					// -- Set shadow stats
					if(mStat_ShadowPubMsgSizes.size() > MAX_STAT_BUFFER_SIZE)
						mStat_ShadowPubMsgSizes.pop_front();
					mStat_ShadowPubMsgSizes.push_back(strlen(mJsonDocumentBuffer_Periodic));

					// -- Actually send the Shadow update to AWS IoT
					//rc = aws_iot_shadow_update(&mMqttClient, gMyThingName, mJsonDocumentBuffer_Periodic, mShadowUpdateCallbackPtr, NULL, 4, true);
					//rc = aws_iot_shadow_update(&mMqttClient, gMyThingName, mJsonDocumentBuffer_Periodic, NULL, NULL, 4, true);		// -- Don't need to get ACK from cloud
					s_json = string (mJsonDocumentBuffer_Periodic);
					rc = mMQTT_Publisher.Publish(QOS_1, shadowupdate, s_json);
					INFO("\n=======================================================================================\n");
					if (NONE_ERROR != rc)
					{
//						ERROR("[AWS_IoT_Module] aws_iot_shadow_update() error %d", rc);
						ERROR("[AWS_IoT_Module] Error sending shadow update %d", rc);
					}
					else {
						mRecover = false;
					}
				}
			}
		}
		// loop and publish a change to Thing Shadow in mAppParams
		while (NONE_ERROR == rc || IoT_Error_t::NETWORK_ATTEMPTING_RECONNECT == rc || RECONNECT_SUCCESSFUL == rc) {
			// -- Lets check for the incoming messages (ThingShadow & Data) for a while like 200 ms.
			rc = aws_iot_shadow_yield(&mMqttClient, 100);
			if (NONE_ERROR != rc) {
				cout << "[Aws_IoT_Module] Disconnected from thing shadow: " << rc << endl;
				continue;
			}
			rc = aws_iot_mqtt_yield(100);
			if (NONE_ERROR != rc) {
				cout << "[Aws_IoT_Module] Disconnected from AWS MQTT: " << rc << endl;
				continue;
			}

			currTime = gCurrentTime() - mInitialTime;
			// -- Refresh local desired by sending get messages to AWS periodically
			if (currTime - prevTime_desired >= mPeriodicRefreshDesiredInterval) {
				rc = aws_iot_shadow_get(&mMqttClient, gMyThingName, NULL, NULL, 4, true);
				if (NONE_ERROR == rc) {
					cout << "[AWS_IoT_Module] Send get message" << endl;
					prevTime_desired = currTime;
				}
				else {
					cout << "[AWS_IoT_Module] Failed to send get message" << endl;
				}
			}


			// -- Send data by publishing to a topic via MQTT
			if(currTime - prevTime_pub >= mMinPeriodicDataPublishInterval) {
				prevTime_pub = currTime;

				pthread_mutex_lock(&_gMutexx_);
				mPubMsg_Queue_out = mPubMsg_Queue_in;
				mPubMsg_Queue_in.clear();
				pthread_mutex_unlock(&_gMutexx_);

				// -- Publish message in database if mLoadDataFromDB is true
				if (mLoadDataFromDB) {
					unsigned int j;
					redisContext *c;
					redisReply *reply;
					const char *hostname = "127.0.0.1";
					int port = 6379;

					struct timeval timeout = { 1, 500000 }; // 1.5 seconds
					c = redisConnectWithTimeout(hostname, port, timeout);
					if (c == NULL || c->err) {
						if (c) {
							printf("Connection to redis error: %s\n", c->errstr);
							redisFree(c);
						} else {
							printf("Connection to redis error: can't allocate redis context\n");
						}
						exit(1);
					}
					reply = (redisReply*)redisCommand(c,"LPOP datachannel");
					while (reply->type != REDIS_REPLY_ERROR && reply->len > 0){
						string entire_msg(reply->str);
						int patt_len = s_patt.length();
						auto idx1 = entire_msg.find(s_patt);
						auto idx2 = string::npos;
						auto idx3 = string::npos;
						if (idx1 != string::npos) {
							idx2 = entire_msg.find(s_patt, idx1 + patt_len);
						}
						if (idx2 != string::npos) {
							idx3 = entire_msg.find(s_patt, idx2 + patt_len);

						}
						if (idx1 == string::npos || idx2 == string::npos || idx3 == string::npos) {
							cout << "[Aws_IoT_Mod] data channel msg wrong format" << endl;
							freeReplyObject(reply);
							reply = (redisReply*)redisCommand(c,"LPOP datachannel");
							continue;
						}
						string topic = entire_msg.substr(0, idx1);
						string msg = entire_msg.substr(idx1 + patt_len, idx2 - idx1 - patt_len);
						string qos = entire_msg.substr(idx2 + patt_len, idx3 - idx2 - patt_len);
						string appT = entire_msg.substr(idx3 + patt_len);
						//cout << "[Aws_IoT_Mod] data channel:\ntopic: " << topic << ", msg: " << msg << ", qos: " << qos << endl;
						freeReplyObject(reply);
						int ret = 99;
						if ((ret = mMQTT_Publisher.Publish((QoSLevel)atoi(qos.c_str()), topic, msg) != NONE_ERROR)) {
							ERROR("Error publishing [%d], msg size = %d Byte", ret, (int)(msg.size()));
							rc = PUBLISH_ERROR;
						}
						else {
							// -- Set data size stats
							if(mStat_DataPubMsgSizes.size() > MAX_STAT_BUFFER_SIZE)
								mStat_DataPubMsgSizes.pop_front();
							mStat_DataPubMsgSizes.push_back(msg.length());
						}
						reply = (redisReply*)redisCommand(c,"LPOP datachannel");
					}
					freeReplyObject(reply);
					redisFree(c);
					mLoadDataFromDB = false;
				}
				if(mPubMsg_Queue_out.size() > 0) {
					int err_count = 0;
					for(vector<PubMsg_t>::iterator it = mPubMsg_Queue_out.begin(); it != mPubMsg_Queue_out.end(); ++it) {
						PubMsg_t *msg_to_pub = &*it;

						//DEBUG("mPubMsg_Publisher.mPubParams:  topic [%s]\n%s",  msg_to_pub.topic.c_str(), (char *)msg_to_pub.msg.c_str());
						int ret = 99;
						if ((ret = mMQTT_Publisher.Publish(msg_to_pub->qos, msg_to_pub->topic, msg_to_pub->msg) != NONE_ERROR)) {
							ERROR("Error publishing [%d], msg size = %d Byte", ret, (int)(msg_to_pub->msg.size()));
							rc = PUBLISH_ERROR;
							err_count++;
						}
						else {
							// -- Set data size stats
							if(mStat_DataPubMsgSizes.size() > MAX_STAT_BUFFER_SIZE)
								mStat_DataPubMsgSizes.pop_front();
							mStat_DataPubMsgSizes.push_back(msg_to_pub->msg.length());
						}
					}
					// -- If all messages failed to send, save in database
					if (err_count == mPubMsg_Queue_out.size()) {
						mLoadDataFromDB = true;
						SaveDataMsgToDB();
					}
					mPubMsg_Queue_out.clear();
				}
			}
			if (rc != NONE_ERROR) break;
			// -- Upon the detection of a update delta message, it tries to update Shadow with the new value from the delta message
			//			if (*mMessageArrivedOnDelta == true && currTime - prevTime_delta >= mAWS_IOT_MODULE_MIN_DELTA_MSG_RESPONSE_WAIT_TIME) {
			//				prevTime_delta = currTime;
			//
			//				// -- Resend the current Shadow message to let AWS IoT know the current Shadow values are updated according to the 'desired' Shadow values.
			//				// -- AWS IoT then looks at the 'reported' Shadow values and see if they are the same as the 'desired' values. If they are not the same, then issues another Shadow Delta message to the device.
			//				{
			//					// -- Initialize the Shadow message buffer
			//					rc = aws_iot_shadow_init_json_document(mJsonDocumentBuffer_Delta, mJsonDocumentBuffer_Delta_size);
			//
			//					// -- Update Shadow at AWS IoT with the new values created within the device
			//					if (rc == NONE_ERROR) {
			//
			//						// -- Add the Shadow values for the attributes to the Shadow message buffer
			//						if(mAppParams.size() > 0) {
			//							for(vector<jsonStruct_t *>::iterator it = mAppParams.begin(); it != mAppParams.end(); ++it) {
			//									jsonStruct_t * param = *it;
			//									rc = aws_iot_shadow_add_reported(mJsonDocumentBuffer_Delta, mJsonDocumentBuffer_Delta_size, 1, param);
			//							}
			//						}
			//
			//						if (rc == NONE_ERROR) {
			//
			//							// -- Add some more meta-data to the message buffer in the JSON format and finalize the message composition
			//							rc = aws_iot_finalize_json_document(mJsonDocumentBuffer_Delta, mJsonDocumentBuffer_Delta_size);
			//
			//							if (rc == NONE_ERROR) {
			//								DEBUG("*****************************************************************************************\n");
			//								DEBUG("Update upon Delta Thing Shadow: %s", mJsonDocumentBuffer_Delta);
			//
			//								// -- Actually send the Shadow update to AWS IoT
			//								rc = aws_iot_shadow_update(&mMqttClient, gMyThingName, mJsonDocumentBuffer_Delta, mShadowUpdateCallbackPtr, NULL, 4, true);
			//								DEBUG("*****************************************************************************************\n");
			//							}
			//						}
			//					}
			//				}
			//
			//				//pthread_mutex_lock(&_gMutexx_);
			//				*mMessageArrivedOnDelta = false;
			//				//pthread_mutex_unlock(&_gMutexx_);
			//			}


			json currentReport;
			// -- Check if its time to send report to AWS
			if (currTime - prevTime_shadowUpdate >= max((double)MIN_STATE_REPORT_INTERVAL, (double)mPeriodicShadowReportInterval*mStateUpdateIntervalScale)) {
				prevTime_shadowUpdate = currTime;
				// -- Send full ToBeReport if its time, else just send the update
				// -- Don't send full report if mPeriodicFullReportInterval is set to zero or negative
				if (mPeriodicFullReportInterval > 0 && currTime - prevTime_full >= (double)mPeriodicFullReportInterval) {
					cout << "[AWS_IoT_Module] Report every thing" << endl;
					ParseParamsFromBuffer(rc);
					if (rc == NONE_ERROR) {
						// -- If there is something to report, update it in prviousReport
						if (strlen(mJsonDocumentBuffer_Periodic) > 0) {
							try {
								currentReport = json::parse(mJsonDocumentBuffer_Periodic);

								prevTime_full = currTime;
								// -- Save currentReport to previousReport json file
								json diff = json::diff(previousReport, currentReport);
								for (auto it = diff.begin(); it != diff.end(); ) {
									string op = (*it)["op"];
									if (op.compare("remove") == 0) {
										diff.erase(it);
									}
									else {
										it++;
									}

								}
								previousReport = previousReport.patch(diff);
								JsonUtil::writeJson(path_to_previous_report_json, previousReport);
							}
							catch (...) {
								cout << "[Aws_IoT_Module] Error with parsing mJsonDocu string:" << endl;
								printf("%s\n", mJsonDocumentBuffer_Periodic);
							}
						}
						// -- Update lastUpdate json
						lastUpdate = previousReport;
						JsonUtil::writeJson(path_to_last_update_json, lastUpdate);
						// -- Copy the json string to mJsonDocumentBuffer
						string json_string = previousReport.dump();
						uint i = 0;
						for (i = 0; i < json_string.length() && i < mJsonDocumentBuffer_Periodic_size; i++) {
							mJsonDocumentBuffer_Periodic[i] = json_string[i];
						}
						mJsonDocumentBuffer_Periodic[i] = '\0';

						// -- Set shadow stats
						if(mStat_ShadowPubMsgSizes.size() > MAX_STAT_BUFFER_SIZE)
							mStat_ShadowPubMsgSizes.pop_front();
						mStat_ShadowPubMsgSizes.push_back(strlen(mJsonDocumentBuffer_Periodic));

						// -- Actually send the Shadow update to AWS IoT
						//rc = aws_iot_shadow_update(&mMqttClient, gMyThingName, mJsonDocumentBuffer_Periodic, mShadowUpdateCallbackPtr, NULL, 4, true);
						//rc = aws_iot_shadow_update(&mMqttClient, gMyThingName, mJsonDocumentBuffer_Periodic, NULL, NULL, 4, true);		// -- Don't need to get ACK from cloud
						s_json = string (mJsonDocumentBuffer_Periodic);
						rc = mMQTT_Publisher.Publish(QOS_1, shadowupdate, s_json);
						INFO("\n=======================================================================================\n");
						if (NONE_ERROR != rc)
						{
//							ERROR("[AWS_IoT_Module] aws_iot_shadow_update() error %d", rc);
							ERROR("[AWS_IoT_Module] Error sending shadow update %d", rc);
						}
					}
					else {
						cout << "[AWS_IoT_Module] Error when creating json string" << endl;
					}
				}
				else {
					// -- Get update json to send to AWS
					currentReport = UpdateReport(rc);
					if (NONE_ERROR != rc){
						ERROR("[AWS_IoT_Module] Error Parsing json from buffer %d", rc);
					}
					if (currentReport.size() == 0) {
						cout << "[AWS_IoT_Module] Nothing to update" << endl;
					}

					// -- Don't fire aws_iot_shadow_update if there is nothing to update or if we encounter error
					if (currentReport.size() > 0 && rc == NONE_ERROR) {
						INFO("\n=======================================================================================\n");
						INFO("[AWS_IoT_Module] Report Thing Shadow: %s", mJsonDocumentBuffer_Periodic);

						// -- Set shadow stats
						if(mStat_ShadowPubMsgSizes.size() > MAX_STAT_BUFFER_SIZE)
							mStat_ShadowPubMsgSizes.pop_front();
						mStat_ShadowPubMsgSizes.push_back(strlen(mJsonDocumentBuffer_Periodic));

						// -- Update lastUpdate json
						lastUpdate = currentReport;
						JsonUtil::writeJson(path_to_last_update_json, lastUpdate);

						// -- Actually send the Shadow update to AWS IoT
						//rc = aws_iot_shadow_update(&mMqttClient, gMyThingName, mJsonDocumentBuffer_Periodic, mShadowUpdateCallbackPtr, NULL, 4, true);
						//rc = aws_iot_shadow_update(&mMqttClient, gMyThingName, mJsonDocumentBuffer_Periodic, NULL, NULL, 4, true);		// -- Don't need to get ACK from cloud
						s_json = string (mJsonDocumentBuffer_Periodic);
						rc = mMQTT_Publisher.Publish(QOS_1, shadowupdate, s_json);
						INFO("\n=======================================================================================\n");
						if (NONE_ERROR != rc)
						{
//							ERROR("[AWS_IoT_Module] aws_iot_shadow_update() error %d", rc);
							ERROR("[AWS_IoT_Module] Error sending shadow update %d", rc);
						}
					}
				}
			}

			usleep(1000*10);
		}

		INFO("[AWS_IoT_Module] Disconnecting");

		if (NONE_ERROR != Shadow_Disconnect()) {
			ERROR("[AWS_IoT_Module] Disconnect error %d", rc);
		}
		else {
			;
		}

		sleep(1);

		if (NONE_ERROR != (rc = Shadow_ConnectInit())) {
			ERROR("[AWS_IoT_Module] awsShadow.ShadowConnectInit() error %d", rc);
			mRecover = true;
		}

		// -- If there is error with connecting to thing shadow, store data in data channel to local db
		if (NONE_ERROR != rc) {
			currTime = gCurrentTime() - mInitialTime;

			// -- Store data channel data in local database periodically
			if(currTime - prevTime_pub >= mMinPeriodicDataPublishInterval) {
				prevTime_pub = currTime;

				pthread_mutex_lock(&_gMutexx_);
				mPubMsg_Queue_out = mPubMsg_Queue_in;
				mPubMsg_Queue_in.clear();
				pthread_mutex_unlock(&_gMutexx_);
				if (mPubMsg_Queue_out.size() > 0) {
					mLoadDataFromDB = true;
					SaveDataMsgToDB();
					mPubMsg_Queue_out.clear();
				}
			}
		}
	}
}


void* AWS_IoT_Module::PeriodicProcessingThread(void *apdata) {

	AWS_IoT_Module* pb = (AWS_IoT_Module*)apdata;

	pb->PeriodicProcessing_Loop();

    return NULL;
}

IoT_Error_t AWS_IoT_Module::Run() {

	IoT_Error_t rc = NONE_ERROR;

	mInitialTime = gCurrentTime();

	rc = Shadow_ConnectInit();

	if (NONE_ERROR != rc) {
		ERROR("awsShadow.ShadowConnectInit() error %d", rc);
	}

	// -- If set, then erase the remote ThingShadow at AWS IoT Cloud
	if(mNeedToEraseThingShadow > 0)
	{
		sleep(1);

			DEBUG("[AWS_IoT_Module] Deleting Thing Shadow");

		int ret = 99;
		// -- Delete the Thing Shadow at the remote AWS IoT Server
		while((ret = Shadow_Delete()) != NONE_ERROR) {

			DEBUG("[AWS_IoT_Module] Delete Failed [%d].. re-attempting", ret);

			sleep(2);
		}

		DEBUG("[AWS_IoT_Module] Deleted Thing Shadow");

		// -- Reset the local version number of the Thing Shadow
		Shadow_Reset_LastReceivedVersion();
	}

//	// -- Set LoadDataFromDB flag to true if there are data in db
//	unsigned int j;
//	redisContext *c;
//	redisReply *reply;
//	const char *hostname = "127.0.0.1";
//	int port = 6379;
//	struct timeval timeout = { 1, 500000 }; // 1.5 seconds
//	c = redisConnectWithTimeout(hostname, port, timeout);
//	if (c == NULL || c->err) {
//		if (c) {
//			printf("Connection error: %s\n", c->errstr);
//			redisFree(c);
//		} else {
//			printf("Connection error: can't allocate redis context\n");
//		}
//		// exit(1);
//	}
//	reply = (redisReply*)redisCommand(c,"LLEN datachannel");
//	if (reply->len > 0 && atoi(reply->str) > 0) {
//		mLoadDataFromDB = true;
//	}
//	cout << "Load from db: " << mLoadDataFromDB << endl;
//	freeReplyObject(reply);
//	redisFree(c);

/*
	Thread 1:
		- Send periodic Shadow report and queued publish messages
	Main thread:
		- Add the on-demand publish messages into queue
*/
	if(pthread_create(&mPeriodicProcessing_thread, &mThreadAttr, PeriodicProcessingThread, (void *)this))
	{
		fprintf(stderr, "Error in pthread_create(PeriodicProcessingThread in AWS_IoT_Module)\n");
		return GENERIC_ERROR;
	}

		return rc;
}


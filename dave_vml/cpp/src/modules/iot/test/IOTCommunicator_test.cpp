#define BOOST_TEST_DYN_LINK

//Define our Module name (prints at testing)
 #define BOOST_TEST_MODULE "IOTCommunicator"

#include <boost/bind.hpp>
#include <boost/algorithm/string.hpp>

#include <string>
#include <iostream>
#include <sstream>
using std::istringstream;

#include "json.hpp"

#include "modules/iot/MQTTClient.hpp"

//VERY IMPORTANT - include this last
#include <boost/test/unit_test.hpp>

// evil encapsulation destroying, but needed to test
#define private public
#define protected public

#include "modules/iot/IOTCommunicator.hpp"

using std::string;
using json = nlohmann::json;

void setString(string& dest, const mosquitto_message* msg){
	string msg_str(static_cast<const char*>(msg->payload), msg->payloadlen);
	dest = msg_str;
	//cout << "setString: " << dest << std::endl;
}

void setStringStr(string& dest, const string& msg){
	dest = msg;
	//cout << "setStringStr: " << dest << std::endl;
}

void setStringRecvd(string& dest, const string& val, const string& msg){
	dest = val;
	//cout << "setStringRecvd: " << dest << std::endl;
}

// run a series of checks to ensure that the key exists
// and is a properly formatted timestamp.  Returns true if 
// the value at the given exists and is a properly formatted timestamp,
// false otherwise
bool checkTimestamp(const json& payload, const string& key){
	// check the existence of the key
	if(payload[key] == nullptr){
		return false;
	}
		
	// make sure it is a string type
	if(!payload[key].is_string()){
		return false;
	}
	
	// and formatted correctly (TODO)
	
	return true;
}

// --------- Test the constructor --------------
BOOST_AUTO_TEST_CASE(constructors){
	IOT_Communicator iot("test");
	usleep(100*1000);

	BOOST_CHECK(iot.appName == "test");
	BOOST_CHECK(iot.dataTopic == "data/test");
	BOOST_CHECK(iot.statusTopic == "shadow/update");
	BOOST_CHECK(iot.deltaTopic == "shadow/update/delta");
	BOOST_CHECK(iot.heartbeatTopic == "heartbeat/test");
	BOOST_CHECK(iot.crashTopic == "appCrash/test");
	BOOST_CHECK(iot.controlTopics.size() >= 1);
	BOOST_CHECK(iot.controlTopics[0] == "control/test");
	
	// Sender and receiver should be connected; this is localhost, after all
	BOOST_CHECK(iot.recv.isConnected());
	BOOST_CHECK(iot.send.isConnected());
}


// ------------ Test the senders -----------------
BOOST_AUTO_TEST_CASE(sendState){
	IOT_Communicator iot("test2");
	MQTTReceiver mqr;
	string dest;
	json json_in, json_state, json_rep, json_des, json_out;
	mqr.subscribe("shadow/update", boost::bind(setString, boost::ref(dest), _1));
	mqr.start();
	
	usleep(100*1000);

	json_out["iVar"] = 1;
	// NOTE: let's use a float that is precisely representable in IEEE754
	json_out["fVar"] = 0.5;
	json_out["sVar"] = "foo";
	

	// default state sending
	BOOST_CHECK(iot.sendState(json_out));
	usleep(100*1000);
	
	istringstream(dest) >> json_in;

	// "state" must always exist and be the top level
	BOOST_CHECK(json_in["state"] != nullptr);
	// after that, "reported" must exist, and "desired" must not exist
	json_state = json_in["state"];
	BOOST_CHECK(json_state["reported"] != nullptr);
	BOOST_CHECK(json_state["reported"]["test2"] != nullptr);
	BOOST_CHECK(json_state["desired"] == nullptr);
	json_rep = json_state["reported"]["test2"];
	

	// Make sure that the set variables are correct
	BOOST_CHECK(json_rep["iVar"] == 1);
	BOOST_CHECK(json_rep["fVar"] == 0.5);
	BOOST_CHECK(json_rep["sVar"] == "foo");
	
	// change up the params
	json_out["iVar"] = 2;
	json_out["fVar"] = 0.25;
	json_out["sVar"] = "bar";
	
	// request to also set as desired state
	BOOST_CHECK(iot.sendState(json_out, true) == true);
	usleep(100*1000);
	
	istringstream(dest) >> json_in;

	// "state" must always exist and be the top level
	BOOST_CHECK(json_in["state"] != nullptr);
	// after that, "reported" must exist, and "desired" must exist
	json_state = json_in["state"];
	BOOST_CHECK(json_state["reported"] != nullptr);
	BOOST_CHECK(json_state["reported"]["test2"] != nullptr);
	BOOST_CHECK(json_state["desired"] != nullptr);
	BOOST_CHECK(json_state["desired"]["test2"] != nullptr);
	json_rep = json_state["reported"]["test2"];
	json_des = json_state["desired"]["test2"];
	
	// Make sure that the set variables are correct
	BOOST_CHECK(json_rep["iVar"] == 2);
	BOOST_CHECK(json_rep["fVar"] == 0.25);
	BOOST_CHECK(json_rep["sVar"] == "bar");
	
	BOOST_CHECK(json_des["iVar"] == 2);
	BOOST_CHECK(json_des["fVar"] == 0.25);
	BOOST_CHECK(json_des["sVar"] == "bar");
	
	// change up the params AGAIN!
	json_out["iVar"] = 3;
	json_out["fVar"] = 0.125;
	json_out["sVar"] = "baz";
	
	// send state, explicitly setting desired to "FALSE"
	BOOST_CHECK(iot.sendState(json_out, false) == true);
	usleep(100*1000);
	
	istringstream(dest) >> json_in;
	// "state" must always exist and be the top level
	BOOST_CHECK(json_in["state"] != nullptr);
	// after that, "reported" must exist, and "desired" must exist
	json_state = json_in["state"];
	BOOST_CHECK(json_state["reported"] != nullptr);
	BOOST_CHECK(json_state["reported"]["test2"] != nullptr);
	BOOST_CHECK(json_state["desired"] == nullptr);
	json_rep = json_state["reported"]["test2"];
	
	// Make sure that the set variables are correct
	BOOST_CHECK(json_rep["iVar"] == 3);
	BOOST_CHECK(json_rep["fVar"] == 0.125);
	BOOST_CHECK(json_rep["sVar"] == "baz");
}

BOOST_AUTO_TEST_CASE(sendData){
	IOT_Communicator iot("test");
	MQTTReceiver mqr;
	string dest;
	mqr.subscribe("data/test", boost::bind(setString, boost::ref(dest), _1));
	mqr.start();
	
	usleep(100*1000);

	// test for const char*
	BOOST_CHECK(iot.sendData("foo"));

	// wait 1/10th of a second for the callback to fire
	usleep(100*1000);
	// NOTE: the receipt comes with an extra newline on the end!
	dest = dest.substr(0, dest.size()-1);
	BOOST_CHECK(boost::trim_copy(dest) == "foo");
	
	// test for std::string
	string msg = "bar";
	BOOST_CHECK(iot.sendData(msg));

	usleep(100*1000);
	dest = dest.substr(0, dest.size()-1);

	//cout << "DEST: " << dest << std::endl;
	BOOST_CHECK(dest == "bar");
	
	// test for json
	json json_t;
	json_t["foo"] = 1;
	BOOST_CHECK(iot.sendData(json_t));

	usleep(100*1000);
	//cout << "DEST: " << dest << std::endl;
	json json_in;
	std::istringstream(dest) >> json_in;
	BOOST_CHECK(json_in["foo"] == 1);
}

BOOST_AUTO_TEST_CASE(sendError){
	IOT_Communicator iot("test");
	MQTTReceiver mqr;
	string dest;
	json json_in;
	mqr.subscribe("appCrash/test", boost::bind(setString, boost::ref(dest), _1));
	mqr.start();
	
	usleep(100*1000);

	// send default (fatal error, no restart)
	BOOST_CHECK(iot.sendError("default"));
	usleep(100*1000);

	istringstream(dest) >> json_in;
	BOOST_CHECK(checkTimestamp(json_in, "date"));
	BOOST_CHECK(json_in["name"] == "test");
	BOOST_CHECK(json_in["actionRequested"] == "stop");
	BOOST_CHECK(json_in["info"] == "default");
	
	// send non-fatal error
	BOOST_CHECK(iot.sendError("nfe", false));
	usleep(100*1000);
	
	istringstream(dest) >> json_in;

	BOOST_CHECK(checkTimestamp(json_in, "date"));
	BOOST_CHECK(json_in["name"] == "test");
	BOOST_CHECK(json_in["actionRequested"] == "report");
	BOOST_CHECK(json_in["info"] == "nfe");
	
	// send non-fatal error, requesting restart
	BOOST_CHECK(iot.sendError("nfe_r", false, true));
	usleep(100*1000);
	
	istringstream(dest) >> json_in;
	BOOST_CHECK(checkTimestamp(json_in, "date"));
	BOOST_CHECK(json_in["name"] == "test");
	BOOST_CHECK(json_in["actionRequested"] == "report");
	BOOST_CHECK(json_in["info"] == "nfe_r");
	
	// send non-fatal error, explicitly not requesting restart
	BOOST_CHECK(iot.sendError("nfe_nr", false, false));
	usleep(100*1000);
	
	istringstream(dest) >> json_in;
	BOOST_CHECK(checkTimestamp(json_in, "date"));
	BOOST_CHECK(json_in["name"] == "test");
	BOOST_CHECK(json_in["actionRequested"] == "report");
	BOOST_CHECK(json_in["info"] == "nfe_nr");
	
	
	// send fatal error
	BOOST_CHECK(iot.sendError("fe", true));
	usleep(100*1000);
	
	istringstream(dest) >> json_in;
	BOOST_CHECK(checkTimestamp(json_in, "date"));
	BOOST_CHECK(json_in["name"] == "test");
	BOOST_CHECK(json_in["actionRequested"] == "stop");
	BOOST_CHECK(json_in["info"] == "fe");
	
	// send fatal error, requesting restart
	BOOST_CHECK(iot.sendError("fe_r", true, true));
	usleep(100*1000);
	
	istringstream(dest) >> json_in;

	BOOST_CHECK(checkTimestamp(json_in, "date"));
	BOOST_CHECK(json_in["name"] == "test");
	BOOST_CHECK(json_in["actionRequested"] == "restart");
	BOOST_CHECK(json_in["info"] == "fe_r");
	
	// send fatal error, explicitly requesting no restart
	BOOST_CHECK(iot.sendError("fe_nr", true, false));
	usleep(100*1000);
	
	istringstream(dest) >> json_in;
	BOOST_CHECK(checkTimestamp(json_in, "date"));
	BOOST_CHECK(json_in["name"] == "test");
	BOOST_CHECK(json_in["actionRequested"] == "stop");
	BOOST_CHECK(json_in["info"] == "fe_nr");
}

BOOST_AUTO_TEST_CASE(sendHeartbeat){
	IOT_Communicator iot("test");
	MQTTReceiver mqr;
	string dest;
	json json_in;
	mqr.subscribe("heartbeat/test", boost::bind(setString, boost::ref(dest), _1));
	mqr.start();
	
	usleep(100*1000);

	BOOST_CHECK(iot.sendHeartbeat());
	usleep(100*1000);
	
	istringstream(dest) >> json_in;

	// right now, just check the existence of a heartbeat key
	BOOST_CHECK(checkTimestamp(json_in, "heartbeat"));
	BOOST_CHECK(json_in["name"] == "test");
	BOOST_CHECK(json_in["interval"] == 300);

	// Send another heartbeat with a specific interval
	dest = "";

	BOOST_CHECK(iot.sendHeartbeat(60));
	usleep(100*1000);

	istringstream(dest) >> json_in;
	BOOST_CHECK(checkTimestamp(json_in, "heartbeat"));
	BOOST_CHECK(json_in["name"] == "test");
	BOOST_CHECK(json_in["interval"] == 60);

}

// --------- Test the control/delta callbacks ----------------

BOOST_AUTO_TEST_CASE(deltaCallback){
	IOT_Communicator iot("test");
	MQTTSender mqs;
	string dest, dest2;
	json json_out, json_in, json_state;
	
	usleep(100*1000);

	// register my callbacks
	iot.RegisterDeltaCallback(boost::bind(setStringStr, boost::ref(dest), _1));
	iot.RegisterDeltaCallback(boost::bind(setStringRecvd, boost::ref(dest2), "foo", _1));
	
	// construct a proper delta message
	json_out["state"]["delta"]["test"]["iVal"] = 1;
	json_out["state"]["delta"]["test"]["fVal"] = 0.5;
	json_out["state"]["delta"]["test"]["sVal"] = "bar";
	
	mqs.publish("shadow/update/delta", json_out.dump());
	usleep(100*1000);
	
	// check to make sure the callback fired
	BOOST_CHECK(dest2 == "foo");
	
	istringstream(dest) >> json_in;

	BOOST_CHECK(json_in["state"] != nullptr);
	BOOST_CHECK(json_in["state"]["delta"] != nullptr);
	BOOST_CHECK(json_in["state"]["delta"]["test"] != nullptr);
	json_state = json_in["state"]["delta"]["test"];
	
	BOOST_CHECK(json_state["iVal"] == 1);
	BOOST_CHECK(json_state["fVal"] == 0.5);
	BOOST_CHECK(json_state["sVal"] == "bar");
	
}

BOOST_AUTO_TEST_CASE(controlCallback){
	IOT_Communicator iot("test");
	MQTTSender mqs;
	string dest, dest2;
	
	usleep(100*1000);

	// register my callbacks
	iot.RegisterControlCallback(boost::bind(setStringStr, boost::ref(dest), _1));
	iot.RegisterControlCallback(boost::bind(setStringRecvd, boost::ref(dest2), "foo", _1));

	mqs.publish("control/test", "cmd_msg");
	usleep(100*1000);
	
	BOOST_CHECK(dest2 == "foo");

	// remember to get rid of the extra newline!
	BOOST_CHECK(dest.substr(0,dest.size()-1) == "cmd_msg");
}

/*
 * MQTTReceiver.cpp
 *
 *  Created on: Jan 9, 2017
 *      Author: jwallace
 */

#include "MQTTClient.hpp"

using std::string;
using std::multimap;

const int MQTTClient::LIBINIT_VAL = mosquitto_lib_init();

MQTTClient::MQTTClient(const char* hostname, int port, int keepalive,
		const char *username, const char *password,
		const char *cafile, const char *capath, const char *certfile, const char *keyfile) : connected(false){

	client = mosquitto_new(NULL, true, this);

	if(cafile){
		mosquitto_tls_set(client, cafile, capath, certfile, keyfile, NULL);
	}
	if(username){
		mosquitto_username_pw_set(client, username, password);
	}

	// set the connect/disconnect/error callbacks
	mosquitto_connect_callback_set(client, MQTTClient::connect_callback);
	mosquitto_disconnect_callback_set(client, MQTTClient::disconnect_callback);
	mosquitto_log_callback_set(client, MQTTClient::log_callback);

	mosquitto_connect(client, hostname, port, keepalive);

}

MQTTClient::~MQTTClient(){
	mosquitto_destroy(client);
}

void MQTTClient::connect_callback(mosquitto* conn, void* obj, int rc){
	MQTTClient* mqc = static_cast<MQTTClient*>(obj);
	if(rc == 0){
		// if we are now connected, set the connection status
		mqc->setConnected(true);
		// and attempt to resubscribe to any topics
		mqc->resubscribe();
	}
}

void MQTTClient::disconnect_callback(mosquitto* conn, void* obj, int){
	MQTTClient* mqc = static_cast<MQTTClient*>(obj);
	mqc->setConnected(false);
	// attempt to reconnect
	mosquitto_reconnect(conn);
}

void MQTTClient::start(){
	mosquitto_loop_start(client);
}

void MQTTClient::run(){
	mosquitto_loop_forever(client, -1, 1);
}

void MQTTClient::stop(){
	mosquitto_loop_stop(client, true);
}

MQTTSender::MQTTSender(const char* hostname, int port, int keepalive,
		const char *username, const char *password,
		const char *cafile, const char *capath, const char *certfile, const char *keyfile)
	: MQTTClient(hostname, port, keepalive, username, password, cafile, capath, certfile, keyfile){

	// set the publish callback
	mosquitto_publish_callback_set(client, MQTTSender::publish_callback);

}

int MQTTSender::publish(const string& topic, const string& payload, int qos){
	return mosquitto_publish(client, NULL, topic.c_str(), payload.length(), payload.c_str(), qos, false);
}

MQTTReceiver::MQTTReceiver(const char* hostname, int port, int keepalive,
		const char *username, const char *password,
		const char *cafile, const char *capath, const char *certfile, const char *keyfile)
	: MQTTClient(hostname, port, keepalive, username, password, cafile, capath, certfile, keyfile){

	// set the subscribe and message callbacks
	mosquitto_subscribe_callback_set(client, MQTTReceiver::subscribe_callback);
	mosquitto_message_callback_set(client, MQTTReceiver::message_callback);
}

MQTTReceiver::~MQTTReceiver() {
}

int MQTTReceiver::subscribe(const string& topic, recv_callback sub_action, int qos){

	// attempt to subscribe
	int retval = mosquitto_subscribe(client, NULL, topic.c_str(), qos);

	// if successful, add the callback to the list of actions to take
	if(retval == MOSQ_ERR_SUCCESS){
		action_lock.lock();
		subscribe_topics[topic] = qos;
		topic_actions.insert(action_map_t::value_type(convertTopicPattern(topic), sub_action));
		action_lock.unlock();
	}

	return retval;

}

int MQTTReceiver::unsubscribe(const string& topic){

	int retval = mosquitto_unsubscribe(client, NULL, topic.c_str());

	if(retval == MOSQ_ERR_SUCCESS){
		action_lock.lock();
		subscribe_topics.erase(topic);
		topic_actions.erase(convertTopicPattern(topic));
		action_lock.unlock();
	}

	return retval;
}

void MQTTReceiver::resubscribe(){
	action_lock.lock();
	for(auto it=subscribe_topics.begin(); it!= subscribe_topics.end(); it++){
		mosquitto_subscribe(client, NULL, it->first.c_str(), it->second);
	}
	action_lock.unlock();

}

void MQTTReceiver::message_callback(mosquitto* conn, void* obj, const mosquitto_message *msg){

	// First, convert my obj to a MQTTReceiver object
	MQTTReceiver* mqr = static_cast<MQTTReceiver*>(obj);

	// delegate to my object
	mqr->processMessage(msg);
}

void MQTTReceiver::processMessage(const mosquitto_message *msg){

	// For each message, we will check to see if the message matches any patterns
	action_lock.lock();
	// TODO: I wish there was a better way to do this than by iterating over the entire structure
	for(action_map_t::const_iterator it=topic_actions.begin(); it!=topic_actions.end(); ++it){
		if(boost::regex_match(msg->topic, it->first)){
			it->second(msg);
		}
	}
	action_lock.unlock();
}

boost::regex MQTTReceiver::convertTopicPattern(const string& pattern){
	// We want to turn a MQTT pattern into a regex.
	// To do this, equivalent with sed: 's:/#$:/.*:' and 's:/+(/|$):/[^/]*\1:'
	static boost::regex multilevel("/#$");
	static boost::regex singlelevel("/\\+(/|$)");

	string regex_patt = boost::regex_replace(pattern, multilevel, "/.*");
	regex_patt = boost::regex_replace(regex_patt, singlelevel, "/[^/]*$1");

	return boost::regex(regex_patt);
}

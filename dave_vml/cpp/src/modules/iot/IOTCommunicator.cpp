/*
 * IOTCommunicator.cpp
 *
 *  Created on: Jan 12, 2017
 *      Author: jwallace
 */

#include "modules/iot/IOTCommunicator.hpp"

#include <boost/algorithm/string.hpp>
#include <boost/bind.hpp>

#include <ctime>

using std::string;
using std::vector;
using std::ostringstream;

IOT_Communicator::IOT_Communicator(const string& appName_in, const string& aws_prefix) : appName(appName_in) {
	// TODO: expose the MQTT client connection parameters if needed

	// configure the topics
	dataTopic = "data/" + appName;
	statusTopic = aws_prefix + "shadow/update";
	deltaTopic = aws_prefix + "shadow/update/delta";

	heartbeatTopic = "heartbeat/" + appName;
	crashTopic = "appCrash/" + appName;

	privateTopic = appName;

	string controlBase = "control/" + appName;
	vector<string> subtopics;
	boost::algorithm::split(subtopics, devinfo.GetMyStoreName_(), boost::is_any_of("/"));
	subtopics.push_back(devinfo.GetMyDevName_());

	for (auto it = subtopics.begin(); it != subtopics.end() && it->size() > 0; it++){
		controlTopics.push_back(controlBase);
		controlBase += ("/" + (*it));
	}

	controlTopics.push_back(controlBase);

	// start the MQTT Receiver
	send.start();
	recv.start();
}

IOT_Communicator::~IOT_Communicator() {
	// Stop the MQTT Receiver
	send.stop();
	recv.stop();
}

bool IOT_Communicator::RegisterDeltaCallback(callback_t delta_action){
	return recv.subscribe(deltaTopic, boost::bind(&IOT_Communicator::translator, delta_action, _1));
}


bool IOT_Communicator::RegisterControlCallback(callback_t control_action){
	bool all_sub = true;
	for(auto it=controlTopics.begin(); it!= controlTopics.end(); it++){
		all_sub &= recv.subscribe(*it, boost::bind(&IOT_Communicator::translator, control_action, _1));
	}
	return all_sub;
}

bool IOT_Communicator::sendHeartbeat(unsigned int interval){
	ostringstream os;
	os << "{\"heartbeat\":\"" << getTimestamp() << "\","
	   << "\"name\":\"" << appName << "\","
	   << "\"interval\":" << interval << "}";

	return (send.publish(heartbeatTopic, os.str()) == MOSQ_ERR_SUCCESS);
}

string IOT_Communicator::getTimestamp(){
	return boost::posix_time::to_iso_string(boost::posix_time::second_clock::universal_time());
}

void IOT_Communicator::translator(callback_t action, const struct mosquitto_message *message){

	// explicitly construct a string based on the payload and payload length
	string msg((const char*) message->payload, message->payloadlen);
	action(msg);

}



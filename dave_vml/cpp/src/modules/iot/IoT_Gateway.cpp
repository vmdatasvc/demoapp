/*
 * IoT_Gateway.cpp
 *
 *  Created on: Mar 3, 2016
 *      Author: paulshin
 */
#include <cstdio>
#include <cstdlib>
#include <sstream>
#include <iostream>
#include "unistd.h"
#include <string.h>
#include <iomanip>
#include <pthread.h>

#include "modules/iot/IoT_Common.hpp"
#include "modules/utility/TimeKeeper.hpp"
#include "modules/utility/ParseConfigTxt.hpp"

#include "modules/iot/IoT_Gateway.hpp"


#include "aws_iot_mqtt_interface.h"

using namespace std;

#define DEBUG_GATEWAY	false
#define DEBUG_MQTT_AT_GATEWAY		false

#define	PERIODIC_PROCESSING_CHECK_INTERVAL		0.2		// -- Unit: sec

extern pthread_mutex_t gMutex;

vector<devState_t > IoT_Gateway::mDevState;


/**
 * @brief IoT_Gateway::IoT_Gateway
 * @param _id
 * @param clean_session
 *
 * Constructor function
 */
IoT_Gateway::IoT_Gateway(const char * _id, bool clean_session) : MosqMQTT(_id, clean_session) {

        pthread_attr_init(&mThreadAttr);
        pthread_attr_setdetachstate(&mThreadAttr, PTHREAD_CREATE_JOINABLE);

        // -- Initialization for MosqMQTT class
        {
                 mIsMqttConnected = false;

                 mosqpp::lib_init();        // Mandatory initialization for mosquitto library
                 mPid = getpid();

                 mClientId.str("");
                 mClientId.clear();
                 mClientId << devinfo.GetMyStoreName_dash_() << "-" << devinfo.GetMyDevName_() << "-" << _id << "-" << mPid;

                 // -- Use a unique ID for the MQTT Client
                 reinitialise((const char*)mClientId.str().c_str(), true);
        }

        mDefaultReportInterval = 10.0f;		// -- Unit: [sec]
        mStateMessageId = 0;
        mStateUpdateIntervalScale = 1.0f;
}

/**
 * @brief IoT_Gateway::~IoT_Gateway
 *
 * Destructor
 */
IoT_Gateway::~IoT_Gateway() {

        // -- Setting for MosqMQTT class
        {
                 loop_stop();            // Kill the thread
                 mosqpp::lib_cleanup();    // Mosquitto library cleanup
        }
}



// -- Functions for MosqMQTT class
/**
 * @brief IoT_Gateway::on_message
 * @param msg
 *
 * This function is called whenever it receives a message from the local mosquitto broker to each app, and handles the incoming messages via subscriptions.
 */
void IoT_Gateway::on_message(const struct mosquitto_message *msg) {
    if (msg == NULL) { return; }

    if(DEBUG_MQTT_AT_GATEWAY)
		printf("-- got message @ %s: (%d, QoS %d, %s) '%s'\n",
			msg->topic, msg->payloadlen, msg->qos, msg->retain ? "R" : "!r",
			(char *)msg->payload);

    if(msg->topic == NULL || msg->payload == NULL)
    	return;

    string msg_topic(msg->topic);
    string msg_payload((char *)msg->payload);
    msg_payload.resize(msg->payloadlen);

    string topic_deltaState(AWS_IOT_SHADOW_DELTA_CHANNEL_NAME);
	string topic_control(AWS_IOT_CONTROL_CHANNEL_PREAMBLE);

//	if(DEBUG_MQTT_AT_GATEWAY)
//		printf("Valid Delta Msg? %d, msg_payload.size() = %d\n", msg_topic.compare(0,topic_deltaState.size(), topic_deltaState), (int)msg_payload.size());

    if(msg_topic.empty() == true || msg_payload.empty() == true)
		return;

    // -- If received Delta Thing Shadow message, then pass the entire message to the internal apps.
    if(msg_topic.compare(0,topic_deltaState.size(), topic_deltaState) == 0) {

    	// -- TODO: In the next phase, we may want to enable per-parameter callback function
    	// -- To do that, we need to parse the delta message here and pass the 'desired' value by calling the per-parameter callback function

    	mDeltaStateCallbackFunc(msg_payload);
    }

    // -- If received a message via Control channel, then pass the entire message to the internal apps.
    else if(msg_topic.compare(0,topic_control.size(), topic_control) == 0) {

    	mControlChannelCallbackFunc(msg_topic, msg_payload);
    }

}

// -- Functions for MosqMQTT class
/**
 * @brief IoT_Gateway::on_connect
 * @param rc
 *
 * This function is called whenever a successful connection is made to the local mosquitto broker.
 */
void IoT_Gateway::on_connect(int rc)
{
	if ( rc == 0 ) {	 /* success */
		if(DEBUG_MQTT_AT_GATEWAY)
			cout << ">> myMosq - connected with server" << std::endl;

		mIsMqttConnected = true;

    	// -- Subscribe to topics after a successful connection is made.
    	for(std::vector<string>::iterator it = mSubTopics.begin(); it != mSubTopics.end(); ++it) {
    		string itt = *it;

    		if(DEBUG_MQTT_AT_GATEWAY)
    			printf("[IoT_Gateway] Subscribing to [%s]\n", itt.c_str());

    		try {
    			this->subscribe(NULL, (const char*)(itt.c_str()), QOS_1);
			} catch (exception& e) {
				if(DEBUG_MQTT_AT_GATEWAY)
					printf("Error: Failed to subscribe\n%s\n", e.what());
			}
    	}

	} else {
		if(DEBUG_MQTT_AT_GATEWAY)
			cout << ">> myMosq - Impossible to connect with server(" << rc << ")" << std::endl;

		// -- Start the mosquitto MQTT client
		start();
	}
 }

// -- Functions for MosqMQTT class
/**
 * @brief IoT_Gateway::on_disconnect
 * @param rc
 *
 * This function is called whenever a dis-connection happens from an app to the local mosquitto broker.
 */

void IoT_Gateway::on_disconnect(int rc)
{

	if(DEBUG_MQTT_AT_GATEWAY)
		cout << ">> myMosq - disconnection(" << rc << ")" << endl;

	mIsMqttConnected = false;

	while(1) {

		sleep(1);

		int ret;

		try {
			ret = this->reconnect();
		} catch (exception& e) {
			if(DEBUG_MQTT_AT_GATEWAY)
				printf("Error: Failed to reconnect\n%s\n", e.what());
		}


		if(DEBUG_MQTT_AT_GATEWAY)
			cout << ">> myMosq - reconnect(" << ret << ")" << endl;

		if(ret == MOSQ_ERR_SUCCESS) break;
		else usleep(1e5);
	}

 }

// -- Functions for MosqMQTT class
/**
 * @brief IoT_Gateway::on_subscribe
 * @param mid
 * @param qos_count
 * @param granted_qos
 *
 * This function is called whenever a successful subscription is made to the local mosquitto broker.
 */
void IoT_Gateway::on_subscribe(int mid, int qos_count, const int *granted_qos) {
	if(DEBUG_MQTT_AT_GATEWAY)
		cout << ">>>>>>>>>>>> myMosq - Message (" << mid << ") succeed to be subscribed  >>>>>>>" << endl;
}

/**
 * @brief IoT_Gateway::MessageSending_Loop
 *
 * Periodically checks the outgoing message queue in an infinite loop for State and Data.
 */
void IoT_Gateway::MessageSending_Loop() {

	if(DEBUG_GATEWAY)
		printf("[IoT_Gateway] PeriodicProcessing loop is created. \n");

	int ret = 0;

	// -- Periodically send the state report to the ThingShadow.
	while(1) {

		// -- See if there's states to be reported. If so, periodically report the state param according to its own report interval
		if(this->mIsMqttConnected == true && mDevState.size() > 0)
			SendStateInfo();

		// -- Send State Information to Thing Shadow if there's any thing to send.
		if(this->mIsMqttConnected == true && mStateMsgToSend.str().size() > 0) {

			ostringstream msgToSend;

			// -- Put the state message num
			msgToSend << "# "	<< "MessageId" << " " << 0 << " " << mStateMessageId << endl;

			pthread_mutex_lock(&gMutex);
			mStateMessageId++;

			msgToSend << mStateMsgToSend.str();

			mStateMsgToSend.str("");
			mStateMsgToSend.clear();
			pthread_mutex_unlock(&gMutex);


			if((ret = this->send_message(mMqttPubTopic_State.c_str(), msgToSend.str().c_str(), this->mQoS)) != 0) {
				// -- Do something

				if(DEBUG_GATEWAY)
					printf("MQTT: State info sending was FAILED.\n");

			} else {
				// -- Do something
				pthread_mutex_lock(&gMutex);
				mStateMsgToSend.clear();
				mStateMsgToSend.str("");

				if(DEBUG_GATEWAY)
					printf("MQTT: State info sending was successful.\n");
				pthread_mutex_unlock(&gMutex);
			}
		}


		// -- Send Data to Data channel
		if(this->mIsMqttConnected == true && mDataMsgToSend.size() > 0)
		{
			vector<pair<string,string> > dataToSend;

			pthread_mutex_lock(&gMutex);
			// -- Insert the rest data
			dataToSend = mDataMsgToSend;

			mDataMsgToSend.clear();
			pthread_mutex_unlock(&gMutex);

			// -- Send each JSON message as a single message to AWS. TODO: Need to see if we can combine multiple JSON messages into a single message.
			for(vector<pair<string,string> >::iterator it = dataToSend.begin(); it != dataToSend.end(); ++it) {
				pair<string,string> msg = *it;

				while(this->send_message(msg.first.c_str(), msg.second, this->mQoS) != 0) {
					// -- Do something

					if(DEBUG_GATEWAY)
						printf("MQTT: Data sending was FAILED.\n");

					usleep(500);
				}
			}
		}

		usleep(1000*1000*PERIODIC_PROCESSING_CHECK_INTERVAL);
	}
}

/**
 * @brief IoT_Gateway::MessageSendingThread
 * @param apdata
 * @return
 *
 * Call the message sending loop
 */
void* IoT_Gateway::MessageSendingThread(void *apdata) {

	IoT_Gateway* pb = (IoT_Gateway*)apdata;

	pb->MessageSending_Loop();

	return NULL;
}

/**
 * @brief IoT_Gateway::Run
 * @param appID
 * @param topic_state
 * @param topic_data
 * @param iot_gateway_configfile
 * @return
 *
 * Main run function. Load config file and create a worker thread that process the incoming packets and outgoing packets.
 */
bool IoT_Gateway::Run(string appID, string topic_state, string topic_data, string iot_gateway_configfile) {

	mMqttConfigPath = iot_gateway_configfile;
	mMqttPubTopic_State = topic_state;
	mMqttPubTopic_Data = topic_data;

	mInitialTime = gCurrentTime();

	// -- Read config file
	{
		ParseConfigTxt config(mMqttConfigPath);

		if(config.getValue_string("VERSION", mSoftwareVersion)) {
			printf("IoT Gateway VERSION = %s\n", mSoftwareVersion.c_str());
		}
		if(config.getValue_int("KEEPALIVE_SECONDS", this->mKeepalive)) {
			printf("KEEPALIVE_SECONDS = %d\n", this->mKeepalive);
		}
		if(config.getValue_string("BROKER_HOSTNAME", this->mBrokerAddr)) {
			printf("BROKER_HOSTNAME = %s\n", this->mBrokerAddr.c_str());
		}
		if(config.getValue_string("BROKER_USERNAME", this->mUsername)) {
			printf("BROKER_USERNAME = %s\n", this->mUsername.c_str());
		}
		if(config.getValue_string("BROKER_PASSWORD", this->mPassword)) {
			printf("BROKER_PASSWORD = %s\n", this->mPassword.c_str());
		}
		if(config.getValue_int("BROKER_QOS", this->mQoS)) {
			printf("BROKER_QOS = %d\n", this->mQoS);
		}
		if(config.getValue_int("BROKER_PORT", this->mBrokerPort)) {
			printf("BROKER_PORT = %d\n", this->mBrokerPort);
		}
	}

	// -- Initialize and start the mosquitto MQTT client
	{
		//mqtt->threaded_set(true);		// -- Tell if this application is threaded or not
		this->username_pw_set((const char*)this->mUsername.c_str(), (const char*)this->mPassword.c_str());		// -- Set username and pwd to the broker
		//mqtt->will_set(mqtt->mPubTopic.c_str(), 0, NULL, 1, true);

		// -- Subscribe to Delta State channel
		// -- Note: This should be done before start(), since the topics will be subscribed upon connection.
		// --       Otherwise, you need to directly call subscribe() to subscribe to a topic immediately assuming the connection is already made.
		this->subscribeToTopic(AWS_IOT_SHADOW_DELTA_CHANNEL_NAME);

		ostringstream contChTopic;

		// -- Subscribe to Control channel
		// -- Note: This should be done before start(), since the topics will be subscribed upon connection.
		// --       Otherwise, you need to directly call subscribe() to subscribe to a topic immediately assuming the connection is already made.
		contChTopic << AWS_IOT_CONTROL_CHANNEL_PREAMBLE;
		this->subscribeToTopic(contChTopic.str());		// -- Subscribe to a 'cross-app' control channel: to all the nodes

		contChTopic << "/" << appID;
		this->subscribeToTopic(contChTopic.str());		// -- Subscribe to a control channel for this app: to all the nodes in this store

		contChTopic << "/" << devinfo.GetMyStoreName_();
		this->subscribeToTopic(contChTopic.str());		// -- Subscribe to a control channel for this app: to all the nodes in this store

		contChTopic << "/" << devinfo.GetMyDevName_();
		this->subscribeToTopic(contChTopic.str());		// -- Subscribe to a control channel for this app: to this node

		// -- Start the mosquitto MQTT client
		this->start();
	}

	if(pthread_create(&mMessageSending_thread, &mThreadAttr, MessageSendingThread, (void *)this))
	{
		fprintf(stderr, "Error in pthread_create(MessageSendingThread in IoT_Gateway)\n");
		return false;
	} else {
		return true;
	}
}

/**
 * @brief IoT_Gateway::AddStateParam
 * @param type
 * @param key
 * @param value
 * @param cb
 * @param reportInterval    Unit: [second]
 * @return current size of the device state list
 */
size_t IoT_Gateway::AddStateParam(unsigned int type, string key, void *value, void (*cb)(std::string key, std::string value), double reportInterval	/*Unit: [sec]*/) {
	devState_t state;
	state.type = type;
	state.key = key;
	state.value = value;
	state.cb_func = cb;

	state.reportInterval = reportInterval;
	state.time_lastReported = gTimeSinceStart() - reportInterval + 5;		// -- Make sure it reports first and then wait for the next turn.

	mDevState.push_back(state);
	return mDevState.size();
}

/**
 * @brief IoT_Gateway::GetStateParamReportInterval
 * @param key
 * @return
 *
 * Get the current state param report interval of a state param with the corresponding input key.
 */
double IoT_Gateway::GetStateParamReportInterval(string key) {
	for(vector<devState_t>::iterator it =  mDevState.begin(); it !=  mDevState.end(); ++it) {
		devState_t *state = &*it;
		if(state->key.compare(key) == 0) {
			return max((double)MIN_STATE_REPORT_INTERVAL, state->reportInterval*mStateUpdateIntervalScale);
		}
	}
	return -1;
}

/**
 * @brief IoT_Gateway::GetStateParamReportInterval
 * @param valuePtr
 * @return
 *
 * Get the current state param report interval of a state param with the corresponding input pointer to the state param.
 */
double IoT_Gateway::GetStateParamReportInterval(void *valuePtr) {
	for(vector<devState_t>::iterator it =  mDevState.begin(); it !=  mDevState.end(); ++it) {
		devState_t *state = &*it;
		if(state->value == valuePtr) {
			return max((double)MIN_STATE_REPORT_INTERVAL, state->reportInterval*mStateUpdateIntervalScale);
		}
	}
	return -1;
}

/**
 * @brief IoT_Gateway::UpdateStateParamReportInterval
 * @param key
 * @param newReportInterval
 * @return
 *
 * Update the state param report interval of a state param with the corresponding input key.
 */
bool IoT_Gateway::UpdateStateParamReportInterval(string key, double newReportInterval) {
	for(vector<devState_t>::iterator it =  mDevState.begin(); it !=  mDevState.end(); ++it) {
		devState_t *state = &*it;
		if(state->key.compare(key) == 0) {
			state->reportInterval = newReportInterval;
			return true;
		}
	}

	return false;
}

/**
 * @brief IoT_Gateway::UpdateStateParamReportInterval
 * @param valuePtr
 * @param newReportInterval
 * @return
 *
 * Update the state param report interval of a state param with the corresponding input pointer to the state param.
 */
bool IoT_Gateway::UpdateStateParamReportInterval(void *valuePtr, double newReportInterval) {
	for(vector<devState_t>::iterator it =  mDevState.begin(); it !=  mDevState.end(); ++it) {
		devState_t *state = &*it;
		if(state->value == valuePtr) {
			state->reportInterval = newReportInterval;
			return true;
		}
	}

	return false;
}

/**
 * @brief IoT_Gateway::SendStateInfo
 *
 * Compose a state info message and put it into out-going buffer.
 */
void IoT_Gateway::SendStateInfo() {

	double currTime = gTimeSinceStart();

	pthread_mutex_lock(&gMutex);

		double elapsedTime = 0;
		for(vector<devState_t >::iterator it = mDevState.begin(); it != mDevState.end(); it++) {
			devState_t *state = &*it;

			elapsedTime = currTime - state->time_lastReported;

			// -- If the time interval has not reached yet, then skip reporting.
			if(elapsedTime < max((double)MIN_STATE_REPORT_INTERVAL, state->reportInterval*mStateUpdateIntervalScale))
				continue;

			state->time_lastReported = currTime;

			switch(state->type) {
				case SHADOW_JSON_INT32:
				case SHADOW_JSON_INT16:
				case SHADOW_JSON_INT8: {
					// -- Insert the state information
					mStateMsgToSend << "# "	<< state->key << " " << state->type << " " << *(int *)state->value << endl;
				}
					break;
				case SHADOW_JSON_UINT32:
				case SHADOW_JSON_UINT16:
				case SHADOW_JSON_UINT8: {
					// -- Insert the state information
					mStateMsgToSend << "# "	<< state->key << " " << state->type << " " << *(unsigned int *)state->value << endl;
				}
					break;
				case SHADOW_JSON_FLOAT: {
					// -- Insert the state information
					mStateMsgToSend << fixed << setprecision(IOT_FLOAT_PRECISION);
					mStateMsgToSend << "# "	<< state->key << " " << state->type << " " << *(float *)state->value << endl;
				}
					break;
				case SHADOW_JSON_DOUBLE: {
					// -- Insert the state information
					mStateMsgToSend << fixed << setprecision(IOT_DOUBLE_PRECISION);
					mStateMsgToSend << "# "	<< state->key << " " << state->type << " " << *(double *)state->value << endl;
				}
					break;
				case SHADOW_JSON_BOOL: {
					// -- Insert the state information
					mStateMsgToSend << "# "	<< state->key << " " << state->type << " " << *(bool *)state->value << endl;
				}
					break;

				case SHADOW_JSON_STRING: {
					// -- Insert the state information
					mStateMsgToSend << "# "	<< state->key << " " << state->type << " " << *(string *)state->value << endl;
				}
					break;

				default:
					break;
			}
		}
	pthread_mutex_unlock(&gMutex);

	if(DEBUG_GATEWAY)
		printf("==> Sending state info [Topic]: %s, [State Msg]:\n%s", mMqttPubTopic_State.c_str(), mStateMsgToSend.str().c_str());
}

/**
 * @brief IoT_Gateway::SendData
 * @param payload
 * @return
 *
 * Put the Data msg into the outgoing msg queue. This will be published to the default data channel
 */
VM_IoT_Error_t IoT_Gateway::SendData(const string &payload) {

	if(payload.size() > MAX_AWS_IOT_DATA_MSG_SIZE*1000)
		return TOO_LARGE_PAYLOAD;

	// -- Insert the rest data
	pthread_mutex_lock(&gMutex);
	//mDataMsgToSend << payload << endl;
	mDataMsgToSend.push_back(make_pair(mMqttPubTopic_Data, payload));
	pthread_mutex_unlock(&gMutex);

	if(DEBUG_GATEWAY)
		printf("[App --> Connector] Msg: [Topic: %s]\n%s", mMqttPubTopic_Data.c_str(), payload.c_str());

	return NO_PROBLEM;

}

/**
 * @brief IoT_Gateway::SendData
 * @param topic_suffix
 * @param payload
 * @return
 *
 * Put the Data msg into the outgoing msg queue. This will be published to the subtopic specified by the input argument, topic_suffix, under default data channel
 */
VM_IoT_Error_t IoT_Gateway::SendData(const string &topic_suffix, const string &payload) {

	if(payload.size() > MAX_AWS_IOT_DATA_MSG_SIZE*1000)
		return TOO_LARGE_PAYLOAD;

	ostringstream topic;

	topic << mMqttPubTopic_Data << topic_suffix;

	// -- Insert the rest data
	pthread_mutex_lock(&gMutex);
	//mDataMsgToSend << payload << endl;
	mDataMsgToSend.push_back(make_pair(topic.str(), payload));
	pthread_mutex_unlock(&gMutex);

	if(DEBUG_GATEWAY)
		printf("[App --> Connector] Msg: [Topic: %s]\n%s", mMqttPubTopic_Data.c_str(), payload.c_str());

	return NO_PROBLEM;

}

/**
 * @brief IoT_Gateway::FindStateParam
 * @param paramKey
 * @return
 *
 * Find the state param pointer corresponding to the input paramKey.
 */
devState_t *IoT_Gateway::FindStateParam(string paramKey) {

	devState_t *ret = NULL;

	pthread_mutex_lock(&gMutex);
	if(mDevState.size() > 0)
		for(vector<devState_t>::iterator it = mDevState.begin(); it != mDevState.end(); ++it) {
			devState_t *stateParam = &*it;
			if(stateParam->key.compare(paramKey) == 0) {
				ret = stateParam;
				break;
			}
		}
	pthread_mutex_unlock(&gMutex);

	return ret;
}

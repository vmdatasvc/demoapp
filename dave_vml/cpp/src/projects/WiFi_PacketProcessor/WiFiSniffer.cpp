//============================================================================
// Name        : WiFi_PacketProcessor.cpp
// Author      : Paul J. Shin
// Version     :
// Copyright   : Your copyright notice
// Description :
//============================================================================

#include "projects/WiFi_PacketProcessor/WiFiSniffer.hpp"

#include "modules/iot/IoT_Common.hpp"

#include <algorithm>
#include <vector>
#include <limits>
#include <sstream>
#include <fstream>
#include <iomanip>

#include <poll.h>
#include <signal.h>

#include <stdint.h>
#include <pcap.h>

extern "C"{
#include "radiotap/radiotap.h"
#include "radiotap/radiotap_iter.h"
}

#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/device/file_descriptor.hpp>

#include <boost/serialization/vector.hpp>
#include <boost/archive/text_oarchive.hpp>


#include <boost/algorithm/string.hpp>
#include <boost/algorithm/hex.hpp>
#include <boost/lexical_cast.hpp>

#include <random>

#include <cstdio>

//#include <iostream>
//using std::cout;

using std::ostream;
using std::ostringstream;
using std::istringstream;
using std::ifstream;
using std::endl;

using std::pop_heap;
using std::push_heap;
using std::make_heap;

using std::string;
using std::vector;
using std::deque;
using std::map;

using json=nlohmann::json;

/* Defined in include/linux/ieee80211.h */
struct ieee80211_hdr {
	uint16_t /*__le16*/frame_control;
	uint16_t /*__le16*/duration_id;
	uint8_t addr1[6];
	uint8_t addr2[6];
	uint8_t addr3[6];
	uint16_t /*__le16*/seq_ctrl;
	//uint8_t addr4[6];
}__attribute__ ((packed));

WiFiSniffer::WiFiSniffer(const std::string& configfn) :
		OmniSensrApp(string(APP_ID_WIFI_PROCESSOR) + "2", configfn) {

	// set some default values
	is_injecting = false;

	nic = "";
	device_silence_interval=250;
	max_device_time = 300;
	use_nonbeacon = true;
	use_broadcast = true;
	use_probe = true;
	meas_summarization = false;
	inj_nic_str = "";

	nic_fn = "/usr/local/www/nic.txt";

	packet_injection_interval = 250;
	backoff_interval = 25;

	heartbeat_interval = 60;
	cleanup_interval = 30;
	processing_interval = 100;

	// register some callbacks for delta functions
	vector<string> ro_vars;
	ro_vars.push_back("totalDeviceNum");
	ro_vars.push_back("WiFiInjection");

	for (auto it = ro_vars.begin(); it != ro_vars.end(); it++) {
		registerDeltaCallback(*it,
				boost::bind(&WiFiSniffer::unsettableDeltaCallback,
						this, *it, _1));
	}

	state_lock.lock();
	appState["totalDeviceNum"] = 0;
	appState["WiFiInjection"] = false;
	state_lock.unlock();

	injection_thread = 0;
}

WiFiSniffer::~WiFiSniffer() {
	removeAllDevices();
}

unsigned int WiFiSniffer::initConfig(ParseConfigTxt& parser) {

	unsigned int nparams = 0;
	nparams += parser.getValue("NIC", nic);

	nparams += parser.getValue("DEVICE_SILENCE_INTERVAL", device_silence_interval);
	nparams += parser.getValue("TIME_THRESHOLD_DEVICE_DISAPPEAR", max_device_time);
	nparams += parser.getValue("ONLY_NON_BEACON_PACKETS", use_nonbeacon);
	nparams += parser.getValue("ONLY_BROADCAST_PACKETS", use_broadcast);
	nparams += parser.getValue("ONLY_PROBE_PACKETS", use_probe);
	nparams += parser.getValue("USE_MEASUREMENT_SUMMRIZATION", meas_summarization);

	nparams += parser.getValue("INJECTION_NICS", inj_nic_str);
	nparams += parser.getValue("NIC_FILENAME", nic_fn);

	nparams += parser.getValue("CALIB_PACKET_INJECTION_INTERVAL", packet_injection_interval);
	nparams += parser.getValue("CALIB_RANDOM_BACKOFF_INTERVAL",	backoff_interval);

	nparams += parser.getValue("HEARTBEAT_INTERVAL", heartbeat_interval);
	nparams += parser.getValue("CLEANUP_INTERVAL", cleanup_interval);
	nparams += parser.getValue("PROCESSING_INTERVAL", processing_interval);

	// generate the list of devices for use in injecting
	inj_nic_list.clear();
	boost::algorithm::split(inj_nic_list, inj_nic_str, boost::is_any_of(","));

	return nparams;
}

void WiFiSniffer::saveState(ostream& os) const {
	os << "NIC " << nic << endl;

	os << "DEVICE_SILENCE_INTERVAL " << device_silence_interval << endl;
	os << "TIME_THRESHOLD_DEVICE_DISAPPEAR " << max_device_time << endl;
	os << "ONLY_NON_BEACON_PACKETS " << use_nonbeacon << endl;
	os << "ONLY_BROADCAST_PACKETS " << use_broadcast << endl;
	os << "ONLY_PROBE_PACKETS " << use_probe << endl;
	os << "USE_MEASUREMENT_SUMMRIZATION " << meas_summarization << endl;

	os << "INJECTION_NICS " << inj_nic_str << endl;
	os << "CALIB_PACKET_INJECTION_INTERVAL " << packet_injection_interval << endl;
	os << "CALIB_RANDOM_BACKOFF_INTERVAL " << backoff_interval << endl;

	os << "NIC_FILENAME " << nic_fn << endl;

	os << "HEARTBEAT_INTERVAL " << heartbeat_interval << endl;
	os << "CLEANUP_INTERVAL " << cleanup_interval << endl;
	os << "PROCESSING_INTERVAL " << processing_interval << endl;
}

void WiFiSniffer::processCommand(const string& cmd) {

	json json_cmd;
	try{

		istringstream(cmd) >> json_cmd;
		//json_cmd.parse(cmd);

		json json_wifimode = json_cmd["WiFiInjection"];
		if(json_wifimode.is_boolean()){
			setWiFiMode(json_wifimode.get<bool>());
		}else{
			sendError("Bad format for WiFiInjection command", false);
		}
	}catch (const std::exception& e){
		sendError("Error parsing command", false);
	}

}

void WiFiSniffer::start() {

	// check to see if I have the nic or inj_nic_str
	if(nic.size() == 0 || inj_nic_str.size() == 0){
		if(!readNicData()){
			throw std::runtime_error("Could not find appropriate NICs");
		}
	}

	// some time parameters
	boost::posix_time::ptime curr_time(boost::posix_time::microsec_clock::universal_time());
	boost::posix_time::ptime last_heartbeat(curr_time);
	boost::posix_time::ptime last_cleanup(curr_time);

	// send a heartbeat to begin
	heartbeat(3*heartbeat_interval);

	monitor_thread = new boost::thread(&WiFiSniffer::runSniffing, this);

	// TODO: configure wake intervals dependent on processing
	//bool data_avail = false;

	while(1){
		main_lock.lock();
		main_cond.timed_wait(main_lock, boost::posix_time::milliseconds(processing_interval));
		curr_time = boost::posix_time::microsec_clock::universal_time();

		main_lock.unlock();
		//cout << "woke up mainloop" << endl;

		// Perform packet summarization and send for every device
		buffer_lock.lock();
		//cout << "processing buffer" << endl;
		for(auto it = device_meas_buffer.begin(); it!= device_meas_buffer.end(); it++){
			summarizeAndSend(it->first, curr_time);
		}
		buffer_lock.unlock();

		// Clean up devices and/or send heartbeat
		if(static_cast<unsigned int>(abs((curr_time - last_heartbeat).total_seconds())) > heartbeat_interval){
			heartbeat(3*heartbeat_interval);
			last_heartbeat = curr_time;
		}

		if(static_cast<unsigned int>(abs((curr_time - last_cleanup).total_seconds())) > cleanup_interval){
			//cout << "Cleaning devices" << endl;
			cleanDevices();
			last_cleanup = curr_time;
		}

	}

}

void WiFiSniffer::runInjection(){
	// First, build the packet of interest

	// Radiotap header
	static const uint8_t u8aRadiotapHeader[] = {

	  0x00, 0x00, // <-- radiotap version (ignore this)
	  0x18, 0x00, // <-- number of bytes in our header (count the number of "0x"s)

	  /**
	   * The next field is a bitmap of which options we are including.
	   * The full list of which field is which option is in ieee80211_radiotap.h,
	   * but I've chosen to include:
	   *   0x00 0x01: timestamp
	   *   0x00 0x02: flags
	   *   0x00 0x03: rate
	   *   0x00 0x04: channel
	   *   0x80 0x00: tx flags (seems silly to have this AND flags, but oh well)
	   */
	  0x0f, 0x80, 0x00, 0x00,

	  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // <-- timestamp

	  /**
	   * This is the first set of flags, and we've set the bit corresponding to
	   * IEEE80211_RADIOTAP_F_FCS, meaning we want the card to add a FCS at the end
	   * of our buffer for us.
	   */
	  0x10,

	  0x00, // <-- rate
	  0x00, 0x00, 0x00, 0x00, // <-- channel

	  /**
	   * This is the second set of flags, specifically related to transmissions. The
	   * bit we've set is IEEE80211_RADIOTAP_F_TX_NOACK, which means the card won't
	   * wait for an ACK for this frame, and that it won't retry if it doesn't get
	   * one.
	   */
	  0x08, 0x00,
	};

	// serial number
	std::vector<uint8_t> mac_sn;
	// convert the serial number in string format to bytes
	string serial = getSerialNumber();
	// left pad to max(12, len + len %2) with '0'
	serial.insert(serial.begin(), serial.size() % 2, '0');


	boost::algorithm::unhex(serial, std::back_inserter(mac_sn));

//	cout << "My SN: " << serial << endl;

//	cout << "Mac (conv): ";
//	for(auto it=mac_sn.begin(); it!= mac_sn.end(); it++){
//		cout << std::hex << static_cast<unsigned int>(*it) << std::dec << ":";
//	}
//	cout << endl;
	// make sure I have 6 bytes - pad left with 0s if this happens
	if(mac_sn.size() < 6){

		sendError("Serial Number MAC encoding yielded too few bytes", false);
	}
	while(mac_sn.size() < 6){
		mac_sn.insert(mac_sn.begin(), 0);
	}


//	cout << "Mac (conv): ";
//	for(auto it=mac_sn.begin(); it!= mac_sn.end(); it++){
//		cout << std::hex << static_cast<unsigned int>(*it) << ":";
//	}
//	cout << endl;
	// make sure I have 6 bytes - pad left with 0s if this happens
	// destination address (MUST be FF:FF:FF:FF:FF:FF)
	const uint8_t mac_da[6] = { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
	// source address (MUST be FE:FE:FE:*:*:*)
	const uint8_t mac_sa[6] = { 0xfe, 0xfe, 0xfe, 0xff, 0xff, 0xff};

	size_t sz = sizeof(u8aRadiotapHeader) + sizeof(struct ieee80211_hdr) + 4;
	uint8_t *buf = (uint8_t*)malloc(sz);
	uint8_t* rt = buf;
	struct ieee80211_hdr* hdr = (struct ieee80211_hdr *) (buf+sizeof(u8aRadiotapHeader));

	// copy the radiotap header into the start of the packet
	memcpy(rt, u8aRadiotapHeader, sizeof(u8aRadiotapHeader));

	// construct the 802.11 header
	// Frame control should indicate a probe request
	uint8_t fcchunk[2];
	fcchunk[0] = 0x40;
	fcchunk[1] = 0x00;
	memcpy(&hdr->frame_control, fcchunk, sizeof(fcchunk));

	hdr->duration_id = 0xffff;
	// set the DA, SA, BSSID appropriately (see above)
	memcpy(&hdr->addr1[0], mac_da, 6*sizeof(uint8_t));
	memcpy(&hdr->addr2[0], mac_sa, 6*sizeof(uint8_t));
	memcpy(&hdr->addr3[0], &mac_sn[0], 6*sizeof(uint8_t));
	hdr->seq_ctrl = 0;

	// NOTE: at this point, "buf" contains our packet and "sz" is the correct size
	char errbuf[PCAP_ERRBUF_SIZE];
	vector<pcap_t *> ppcap_list;

	// generate random numbers
	std::random_device seed;
	std::mt19937 gen(seed());
	std::uniform_int_distribution<unsigned int> dist(0, backoff_interval);

	try{


		for(auto it=inj_nic_list.begin(); it != inj_nic_list.end(); it++){
			//cout << "Bringing up " << *it << endl;

			int sysret = system(string("ifup " + (*it) ).c_str());
			if(sysret == 0){
				pcap_t* ppcap = pcap_open_live(it->c_str(), 800, 1, 20, errbuf);
				if(ppcap){
					ppcap_list.push_back(ppcap);
				}else{
					sendError(string("Failed to pcap_open_live for " + (*it) + ": " ) + errbuf, false);
				}
			} else {
				sendError("Error bringing " + (*it) + " up", false);
			}
		}

		// Open the packet to do injection

		int ret = 0;

		while(is_injecting && ppcap_list.size() > 0 && ret == 0){

			usleep(1000*(packet_injection_interval + dist(gen)));

			//cout << "Preparing to blast" << endl;

			for(auto pit=ppcap_list.begin(); pit != ppcap_list.end(); pit++){

				//cout << "Blast" << endl;
				ret += pcap_sendpacket(*pit, buf, sz);
				// sleep a tiny bit between packets (1/10Ks)
				usleep(100);
			}

		}

		//cout << "Injection status: " << is_injecting << endl;

		if(ret != 0){
			sendError("Packet Injection Failed", false);
		}

	} catch (const boost::thread_interrupted& e){
		// We're done with the thread
	} catch (const std::exception& e){
		// something unexpected happened
		string err = "Unexpected error in packet injection: ";
		err += e.what();
		sendError(err, false);
	}

	// clean up the injectors
	for(auto pit=ppcap_list.begin(); pit != ppcap_list.end(); pit++){
		pcap_close(*pit);
	}
	for(auto it=inj_nic_list.begin(); it != inj_nic_list.end(); it++){
		int sysret = system(string("ifdown " + (*it)).c_str());
		if(sysret != 0){
			sendError("Error bringing " + (*it) + " down");
		}
	}

	free(buf);


}

void WiFiSniffer::runSniffing_libpcap(){
	//PCAP vars
	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_t *ppcap;

	ppcap = pcap_open_live(nic.c_str(), 4096, 1, processing_interval, errbuf);

	if(ppcap != NULL){
		while(1){
			//cout << "PCAP Dispatch" << endl;
			pcap_dispatch(ppcap,-1,pcap_callback,reinterpret_cast<u_char*>(this));
		}
	} else {
		sendError(string("Problem with pcap_open_live: ") + errbuf, true, true);
	}
}

void WiFiSniffer::pcap_callback(u_char* arg, const struct pcap_pkthdr* pcap_hdr, const u_char* buf){
	WiFiSniffer* obj = reinterpret_cast<WiFiSniffer*>(arg);
	obj->parsePacket(buf,pcap_hdr->len);
}


void WiFiSniffer::parsePacket(const uint8_t* buf, size_t buflen){

	//cout << "Parsing packet of " << buflen << " bytes" << endl;

	// First, parse the radiotap header for RSS and channel
	ieee80211_radiotap_header* hdr = (ieee80211_radiotap_header*) buf;

	struct ieee80211_radiotap_iterator itr;
	int ret = ieee80211_radiotap_iterator_init(&itr, hdr, buflen, NULL);
	int8_t rss = std::numeric_limits<int8_t>::max();
	unsigned char channel = 0;

	while(!ret){
		ret = ieee80211_radiotap_iterator_next(&itr);
		if(ret){
			continue;
		}

		switch(itr.this_arg_index){
		struct chan{
			uint16_t chan_val;
			uint16_t chan_flags;
		};
		chan* ch_raw;
		case IEEE80211_RADIOTAP_DBM_ANTSIGNAL:
			// signal level
			rss = *itr.this_arg;
			break;
		case IEEE80211_RADIOTAP_CHANNEL:
			// channel
			ch_raw = (chan*)(itr.this_arg);

			// Possible bug?  Do I need ntohs here?		
			if(ch_raw->chan_flags & IEEE80211_CHAN_2GHZ){
				channel = (ch_raw->chan_val - 2412) / 5 + 1;
			}
			break;
		default:
			// don't care
			break;
		}
	}

	// Now that we've handled the radiotap header, let's parse the 802.11 header
	struct ieee80211_hdr* hdr_80211 = (struct ieee80211_hdr*) (buf + itr._max_length);

	uint8_t fc_chunk[2];
	memcpy(fc_chunk, &hdr_80211->frame_control, 2);

	
	// fc_type:
	// 0x40 == probe request
	// 0x50 == probe response
	// 0x80 == beacon
	// pretty much everything else == data

	bool is_beacon = (fc_chunk[0] == 0x80);
	bool is_ap = is_beacon || (fc_chunk[0] == 0x50);
	bool is_probereq = (fc_chunk[0] == 0x40);

	//if(is_probereq){
	//	cout << "FC:" << std::hex << static_cast<unsigned int>(fc_chunk[0]) << std::dec << endl;
	//	cout << "Channel: " << static_cast<unsigned int>(channel) << endl;
	//	cout << "RSS: " << static_cast<int>(rss) << endl;
	//}


	//uint8_t* dest_mac = hdr_80211->addr1;
	uint8_t* dev_mac = hdr_80211->addr2;
	uint8_t* bssid_mac = hdr_80211->addr3;

	bool is_bcast = true;
	for(unsigned int i=0; i<sizeof(hdr_80211->addr1); i++){
		is_bcast &= (hdr_80211->addr1[i] == 0xff);
	}

	bool is_omnisensr = is_probereq;
	for(unsigned int i=0; i<3; i++){
		is_omnisensr &= (dev_mac[i] == 0xfe);
	}

	// now, check to see if we have a packet that we care about
	bool valid_packet = (is_bcast || !use_broadcast);
	valid_packet &= (is_probereq || !use_probe);
	valid_packet &= ( !(use_nonbeacon && is_beacon) );

	// TODO: scramble the dev_mac (consistently) if not an omnisensr - for privacy

	// we ALWAYS care about calibration packets
	valid_packet |= is_omnisensr;

	// sanity check: make sure we have RSS and channel
	valid_packet &= (rss != std::numeric_limits<int8_t>::max() && channel != 0);

	SnifferPacket::DeviceType dev_type = SnifferPacket::Station;
	if(is_omnisensr){
		dev_type = SnifferPacket::OmniSensr;
	} else if(is_ap){
		dev_type = SnifferPacket::AP;
	}

	if(valid_packet){

		SnifferPacket p(dev_type, channel, (is_omnisensr ? bssid_mac : dev_mac), rss);
		// check to make sure it's not me getting cross-talk when injecting:
		if(is_injecting){
			string serial = getSerialNumber();
			// left pad to max(12, len + len %2) with '0'
			serial.insert(serial.begin(), serial.size() % 2, '0');
			if(boost::iequals(serial, p.getMAC_str())){
				valid_packet = false;
			}
		}

		if(valid_packet){

			updateDevice(p);
    	    buffer_lock.lock();
        	// push a new SnifferPacket onto the end of the queue
			device_meas_buffer[p.getMAC_str()].push_back(p);
			buffer_lock.unlock();
		
			// NOtify main loop that there's data to be had
			main_lock.lock();
			main_cond.notify_one();
			main_lock.unlock();
		}
		//cout << "Packet: FC: " << std::hex << static_cast<unsigned int>(fc_chunk[0]) << "-" << static_cast<unsigned int>(fc_chunk[1]) << std::dec << " " << p.getMAC_str() << " " << static_cast<int>(p.getRSS()) << " " << static_cast<unsigned int>(p.getChannel()) << endl;
	}

}

void WiFiSniffer::summarizeAndSend(const std::string& dev_name, const boost::posix_time::ptime& curr_time){
	ostringstream os;
	boost::archive::text_oarchive archive(os);

	device_lock.lock();
	// find the device
	auto it = device_map.find(dev_name);
	auto it_q = device_meas_buffer.find(dev_name);

	bool packets_avail = false;

	vector<SnifferPacket> batch_data;

	// we REALLY should have found the device, but always good to be sure
	if(it != device_map.end() && it_q != device_meas_buffer.end() && it_q->second.size() > 0){

		if(it->second.getType() != SnifferPacket::OmniSensr ){
			// when did I last see the device, and has it been sufficiently long?
			if((curr_time - it->second.lastSeen()).total_milliseconds() > device_silence_interval){
				packets_avail=true;
				summarizeData(batch_data, it_q->second);
				it_q->second.clear();
			}

		} else {
			packets_avail=true;
			// If we are an OmniSensr, we must be doing some calibration
			// no summarization here, just send them along;
			batch_data.insert(batch_data.end(), it_q->second.begin(), it_q->second.end());

			// clear the buffer
			it_q->second.clear();
		}
	}

	device_lock.unlock();

	if(packets_avail){
		archive << batch_data;
		sendPrivateData(os.str(), "packets");
	}
}

void WiFiSniffer::updateDevice(const SnifferPacket& pack) {
	device_lock.lock();
	string dev_mac = pack.getMAC_str();
	auto it = device_map.find(dev_mac);
	if(it == device_map.end()){
		// create a new device and add it to my tracking structures
		device_map.emplace(std::make_pair(dev_mac, Device(dev_mac, pack.getType())));

		// We added to the # of devices being tracked, publish to thingshadow
		updateState("totalDeviceNum", device_map.size());
	} else {
		// update the time of the found device
		it->second.update();
	}
	device_lock.unlock();
}

void WiFiSniffer::removeAllDevices() {
	device_lock.lock();

	// If we are removing entries, send an update setting the # of devices to 0
	if(device_map.size() > 0){
		updateState("totalDeviceNum", 0);
	}

	// just clear the map
	device_map.clear();

	// Release the lock of the device structures
	device_lock.unlock();

}

void WiFiSniffer::cleanDevices() {
	// make sure the device structures are locked
	device_lock.lock();
	buffer_lock.lock();

	bool removed = false;

	boost::posix_time::ptime now = boost::posix_time::microsec_clock::universal_time();
	// go through every device, deleting the older ones
	for(auto it=device_map.begin(); it!=device_map.end(); ){

		if(static_cast<unsigned int>(abs((now - it->second.lastSeen()).total_seconds())) > max_device_time){
			removed = true;
			device_meas_buffer.erase(it->first);
			device_map.erase(it++);
		} else {
			++it;
		}
	}

	// send an update to the ThingShadow if we modified number of devices
	if(removed){
		updateState("totalDeviceNum",device_map.size());
	}

	// release the device structures
	buffer_lock.unlock();
	device_lock.unlock();


}

void WiFiSniffer::unsettableDeltaCallback(const std::string& name,
		const std::string& val) {
	sendError("Cannot set " + name + " from ThingShadow", false);
}

bool WiFiSniffer::setWiFiMode(bool setInjecting){
	bool retval = false;

	//cout << "Setting Wifi mode to " << setInjecting << endl;
	injector_lock.lock();

	// time to change something
	if(setInjecting != is_injecting){
	
		//cout << "something changed!" << endl;
		if(!is_injecting){
			// start the injection thread
			if(injection_thread && !injection_thread->timed_join(boost::posix_time::milliseconds(1))){
				sendError("Injection thread already running unexpectedly", false);
			}else{
				if(injection_thread){
					delete injection_thread;
				}
				injection_thread = new boost::thread(&WiFiSniffer::runInjection, this);
			}
		}

		is_injecting = setInjecting;
		updateState("WiFiInjection", setInjecting);
	}
	injector_lock.unlock();

	return retval;
}

bool WiFiSniffer::readNicData(){
	string lnic, inic, line;
	vector<string> tokens;
	ifstream nicf(nic_fn.c_str());
	while(getline(nicf, line)){
		// remove comments
		line = line.substr(0, line.find("#"));

		// trim whitespace
		boost::trim(line);

		// only operate on non-empty lines
		if(line.size() > 0){
			boost::split(tokens,line,boost::is_any_of("="));
			if(tokens.size() == 2){
				if(tokens[0] == "inject"){
					inic = tokens[1];
				} else if (tokens[0] == "listen"){
					lnic = tokens[1];
				}
			}
		}

	}

	if(nic.size() == 0 && lnic.size() > 0){
		nic = lnic;
	}

	if(inj_nic_str.size() == 0 && nic.size() > 0){
		inj_nic_str = inic;
		inj_nic_list.clear();
		boost::split(inj_nic_list, inj_nic_str, boost::is_any_of(","));
	}

	// make sure we have some actual nics to listen to
	return (inj_nic_list.size() > 0 && nic.size() > 0);
}


int main(int argc, char** argv){
	string configfn = "config/WiFi_PacketProcessor/WiFiSniffer_config.txt";

	if(argc >= 2){
		configfn = argv[1];
	}

	WiFiSniffer wfs(configfn);
	wfs.Run();

	return 0;
}

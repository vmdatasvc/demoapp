//============================================================================
// Name        : WiFi_PacketProcessor.cpp
// Author      : Paul J. Shin
// Version     :
// Copyright   : Your copyright notice
// Description :
//============================================================================

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <cstdarg>
#include <string>
#include <string.h>
#include <sys/time.h>
#include "unistd.h"
#include <cstdio>
#include <sstream>
#include <pthread.h>
#include <iomanip>
#include <time.h>       /* time */

#include <deque>
#include <list>

#include <boost/algorithm/string/predicate.hpp>		// -- For boost::iequals() for case insensitive comparison

#include "aws_iot_error.h"
#include "aws_iot_shadow_json_data.h"

#include "modules/wifi/MessageType.hpp"
#include "modules/iot/IoT_Common.hpp"
#include "modules/iot/IoT_Gateway.hpp"
#include "modules/utility/TimeKeeper.hpp"
#include "modules/utility/ParseConfigTxt.hpp"

#include "modules/iot/MQTT-client-cpp.hpp"		// -- For internal MQTT

#include "modules/iot/IOTCommunicator.hpp"

#include "WiFi_PacketProcessor.hpp"

// -- Refer to https://github.com/nlohmann/json
#include "json.hpp"

using json = nlohmann::json;

/* -- How to deploy and run
 *
 * sshpass -p 'videomininglabs' scp PacketProcessing_at_Sniffer.cpp root@10.250.1.45:
 * sshpass -p 'videomininglabs' ssh root@10.250.1.45 'g++ PacketProcessing_at_Sniffer.cpp -o PacketProcessing_at_Sniffer'
 * sshpass -p 'videomininglabs' ssh root@10.250.1.45 './PacketProcessing_at_Sniffer'
 *
 */

using namespace std;

//#define EthernetTesting		// -- In case of testing in a desktop machine, then define this. Otherwise, comment out.

bool gDebugMqtt = true;
bool gDebugIoT = true;
bool DEBUG_FUNCTION_CALL = false;

bool gDebugMsg = false;
//bool SHOW_RAW_MESSAGE = false;

typedef shared_ptr<RemoteDevice_short> RemoteDevice_short_Ptr;

float gPeriodicBufferProcessingInterval;
float gPeriodicStateReportInterval;
int gCalibRandomBackoff = 1000;		// -- Unit: msec
string gNIC;
string gSoftwareVersion;
string gOmniSensrAppPath;
int gDebugVerboseMode = 0;
bool gOnlyNonBeaconPackets = true, gOnlyBroadcastPackets = false, gUseSlidingWindowBuffer = false, gUseMeasurementSummarization = false;
int gTimeThresholdForDevAbsency = 10;	// -- Unit: min

string gAppID;
string gAppType;
string gAP_IPaddr, gAP_SN, gAP_DevName, gAP_StoreName;
double gInitialTime;
FILE *gStreamFromAP;
deque<tcpdump_t>	gMeasBuffer_AP;	// -- Temporary storage of measurements to be processed after a collection process
list<RemoteDevice_short_Ptr> 	gFoundDevice;

//bool gStartWiFiCalib = false;		// -- If this flag is on, then initiate WiFi_PacketInjectinTester with a random backoff
bool gIsWiFiInjectionRunning = false;
unsigned int gWiFiMode = WIFI_TRACKING;

int gTotalDeviceNum = 0;

pthread_attr_t gThreadAttr;
pthread_t gWiFiCalib_thread;

// -- ######################################################################
// -- Declare iot gateway
IoT_Gateway	gIoT_Gateway;
pthread_mutex_t gMutex;
// -- ######################################################################

//void showRawMessage(char *buffer) {
////#if SHOW_RAW_MESSAGE
//////		pthread_mutex_lock(&gMutex);
////		printf("%.3lf AP[%s] %s", gCurrentTime() - gInitialTime, gAP_IPaddr, buffer);
////		fflush(stdout);
//////		pthread_mutex_unlock(&gMutex);
////#endif
//}

// -- Parse a line of tcpdump input, and try to retrieve info such as src, rss, timestamp, etc.
// -- If the input has a source MAC addr, then return true. Otherwise, return false
bool parse_tcpdump_input(tcpdump_t *meas, char *buffer) {
	if(DEBUG_FUNCTION_CALL) fprintf(stdout, "parse_tcpdump_input() is called. ");

	// parse the line into blank-delimited tokens
	int n_token = 0; // a for-loop index
	// array to store memory addresses of the tokens in buf
	char* token[MAX_TOKENS_PER_LINE] = {}; // initialize to 0

	// parse the line
	token[0] = strtok(buffer, DELIMITER); // first token
	if (token[0]) // zero if line is blank
	{
	  for (n_token = 1; n_token < MAX_TOKENS_PER_LINE; n_token++)
	  {
		token[n_token] = strtok(0, DELIMITER); // subsequent tokens
		if (!token[n_token]) break; // no more tokens
	  }
	}

	//printf("parse_tcpdump_input() 1: n_token = %d\n", n_token);

	for (int i = 0; i < n_token; i++) { // n_token = #of tokens
		if(strncmp((const char*)token[i], "TA",2) == 0 || strncmp((const char*)token[i], "SA",2) == 0) {		// -- Found the source address information

			meas->src.append(token[i]+3, 17);		// -- Find the MAC addr of the sender
			meas->capturedBy = gAP_IPaddr;			// -- Store what AP captured the RSS measurement

			// -- TODO: Don't know why but atoi() in convert_tttt_timestamp() sometimes causes segmentation fault error.
			// -- Find timestamp (refer to http://www.alexonlinux.com/tcpdump-for-dummies#controlling_time_stamp)
			//convert_tttt_timestamp(token[0], token[1], &meas->timestamp);

			for (int j = 0; j < n_token; j++) { // n_token = #of tokens
				if(strncmp((const char*)token[j],"DA",2) == 0 || strncmp((const char*)token[j],"RA",2) == 0) {	// -- Found the destination address information
					// -- Store the MAC addr of the Receiver
					meas->dst.append(token[j]+3, 17);
					break;
				}
			}

			for (int j = 0; j < n_token; j++) { // n_token = #of tokens
				if(strncmp((const char*)token[j],"BSSID",5) == 0) {	// -- Found the BSSID information
					// -- Store the MAC addr of the Receiver
					meas->bssid.append(token[j]+6, 17);
					break;
				}
			}

			// -- Detect a calibration packet and extract the serial number from the destination address field
			// -- The source address of a calibration packet is supposed to start with "fe:fe:fe" and it should be a directed Probe Request or a NULL data packet so its destination address must not be "ff:ff:ff:ff:ff:ff"
			// -- This should be consistent with WiFi_PacketInjectionTester.
			if(meas->src.compare(0,8,"fe:fe:fe") == 0 && meas->src.compare(gAP_SN) != 0)
			{
				char SN[12] = {'\0'};		// -- Serial Number of the OmniSensr
				const char *dest = meas->bssid.c_str();

				int sn_c = 0;
				for(int i=1; i<17; i++) {
					if((i+1)%3 != 0) {
						SN[sn_c++] = dest[i];
//						printf("SN[%d] = %c\n", sn_c, SN[sn_c -1]);
					}
				}
				meas->src = SN;		// -- Embed the SN to the Source Address
				meas->deviceType = OmniSensr;
//				printf("SN = %s\n", meas->SN.c_str());
			} else {

				// -- Find the type of the device among Station and AP
				// -- Probe response & Beacon packets are always transmitted from an AP
				meas->deviceType = 0;	// -- Set a default value
				bool isFromAP = false;
				for(int j=i+1; j < n_token-1; j++) {
					if(	(strncmp((const char*)token[j], "Probe", sizeof(char)*5) == 0 && strncmp((const char*)token[j+1], "Response", sizeof(char)*8) == 0)
							//|| (strncmp((const char*)token[j], "Probe", sizeof(char)*5) == 0 && strncmp((const char*)token[j+1], "Request", sizeof(char)*7) == 0)
							|| (strncmp((const char*)token[j], "Beacon", sizeof(char)*6) == 0)
					) {
						isFromAP = true;
						meas->deviceType = AP;

						if(gOnlyNonBeaconPackets) {
							break;
						}
					}
				}

				if(isFromAP) {
					if(meas->deviceType != OmniSensr && gOnlyNonBeaconPackets) {
						if(DEBUG_FUNCTION_CALL) fprintf(stdout, "parse_tcpdump_input() Ended FALSE: a BEACON packet \n");
						return false;
					}
				}

				// -- If we want to get broadcast packets only, then filter it out.
				if(meas->deviceType != OmniSensr && gOnlyBroadcastPackets == true) {
					if(meas->dst.compare("ff:ff:ff:ff:ff:ff") != 0) {
						return false;
					}
				}
			}

			for (int j = 0; j < n_token; j++) { // n_token = #of tokens
				if(strncmp((const char*)token[j],"Mb/s",4) == 0) {	// -- Find a reference point
					// -- Find channel info. Channel 1 = 2412 MHz, Channel 2 = 2417 MHz, ..., Channel 11 = 2462 MHz
					meas->channel = (atoi(token[j+1])-2412)/5 + 1;	// -- Actual channel (not channel index)
					// -- Find RSS
					meas->rss = atoi(strtok(token[j+4], "dB"));

//					// -- Find WiFi protocol type (e.g., a/b/g/n/ac)
//					if(strncmp((const char*)token[j+3], "11b", sizeof(char)*3) == 0) { meas->protocol = B_802_11; strncpy(meas->protocol_s, token[10], sizeof(char)*3); }
//					else if(strncmp((const char*)token[j+3], "11g", sizeof(char)*3) == 0) { meas->protocol = G_802_11; strncpy(meas->protocol_s, token[10], sizeof(char)*3); }
//					else if(strncmp((const char*)token[j+3], "11n", sizeof(char)*3) == 0) { meas->protocol = N_802_11; strncpy(meas->protocol_s, token[10], sizeof(char)*3); }
//					else meas->protocol = 0xff;
//
//					meas->protocol_s[3] = '\0';
					break;
				}
			}

			if(DEBUG_FUNCTION_CALL) fprintf(stdout, "parse_tcpdump_input() Ended TRUE: SRC found \n");

//			if(VERBOSE_CAMERA_NODE > 2)
//        	printf("%.2d_%.2d_%.2d %.2d:%.2d:%9.6f Node[%s] RSS=%d dB @Ch %d\n", meas->timestamp.year, meas->timestamp.month, meas->timestamp.date,
//        										meas->timestamp.hour, meas->timestamp.minute,meas->timestamp.second, meas->src,  meas->rss, meas->channel);
			return true;
		}
	}

	if(DEBUG_FUNCTION_CALL) fprintf(stdout, "parse_tcpdump_input() Ended FALSE: No SRC found \n");

	return false;
}


char packeBuffering() {
	char buffer[MAX_BUFFER], buffer_t[MAX_BUFFER];
    int cnt = 0;

	memset(buffer, '\0', MAX_BUFFER);
	memset(buffer_t, '\0', MAX_BUFFER);

	while(cnt < MAX_BUFFER) {
		buffer[cnt] = fgetc(gStreamFromAP);
		//printf("%c", buffer[cnt]);
		cnt++;
		if(buffer[cnt-1] == '\n') break;
	}

	if(cnt < 10) return '1';

	memcpy(buffer_t, buffer, cnt);

	tcpdump_t	meas;
	meas.src.clear();
	meas.dst.clear();

	//printf("AP[%s]: [%d bytes] %s", gAP_IPaddr, cnt, buffer);

	if(parse_tcpdump_input(&meas, buffer)) {	// -- Found a tcpdump input that has a source addr info

		// -- Mark the captured time
		//meas.timstamp_at_AP = gCurrentTime() - gInitialTime;
		meas.localtime = gCurrentTime();

		// -- If buffer is overflowed, then remove the older packets to make a room.
		while(gMeasBuffer_AP.size() >= MAX_MEASUREMENT_BUFFER_PER_AP)
			gMeasBuffer_AP.pop_front();

		gMeasBuffer_AP.push_back(meas);
		//printf("AP[%s]: mMeasBuffer.unProcessedMeasSize = %d\n", gAP_IPaddr, mMeasBuffer.unProcessedMeasSize);

//#ifdef	ONLY_FOR_THIS_CELL_WIFI_ADDR
//		if(strncmp((const char*)meas.src, (const char*)ONLY_FOR_THIS_CELL_WIFI_ADDR, 17) == 0)
//#endif
//		showRawMessage(buffer_t);
	}

	return buffer[0];
}

RemoteDevice_short_Ptr findDevice(string addr) {
	if(DEBUG_FUNCTION_CALL) printf("findDevice() is called. \n");

	if(addr.empty()) return NULL;

	RemoteDevice_short_Ptr dev_ = NULL;

	if(gFoundDevice.size() > 0)
		for(list<RemoteDevice_short_Ptr>::iterator it = gFoundDevice.begin(); it != gFoundDevice.end();++it) {
			RemoteDevice_short_Ptr dev = *it;

			if(dev->GetDevAddr().compare(addr) == 0) {
				dev_ = dev;
				break;
			}
		}

	if(DEBUG_FUNCTION_CALL) printf("findDevice End. 2\n");
	return dev_;
}

//bool RuleToRemoveDevice(RemoteDevice_short_Ptr dev) { return dev->mTobeRemoved == true;}

// -- If a device is lastly seen longer than a threshold, then remove it from the list
void clearUpDeviceList() {

	double curr_time = gCurrentTime() - gInitialTime;
	int cnt = 0;

	pthread_mutex_lock(&gMutex);
	for(list<RemoteDevice_short_Ptr>::iterator it = gFoundDevice.begin(); it != gFoundDevice.end();)
	{
		RemoteDevice_short_Ptr dev = *it;
		bool needToRemove = false;
		cnt++;

			if(curr_time - dev->mLastlySeenTime > gTimeThresholdForDevAbsency*60 /* unit: [min]*/) {

				if(gDebugVerboseMode > 1)
					printf("%.2lf REMOVE Node[%s] (i.e., %dth node) since it has not been seen for %.1f sec (among %d)\n", curr_time, dev->GetDevAddr().c_str(), cnt, curr_time - dev->mLastlySeenTime, (int)gFoundDevice.size());

				//dev->mTobeRemoved = true;
				needToRemove = true;
			}

			if(needToRemove == true) {
				it = gFoundDevice.erase(it);
			} else {
				++it;
			}
	}

	// -- Remove the devices from the list
	//gFoundDevice.remove_if(RuleToRemoveDevice);

	pthread_mutex_unlock(&gMutex);

}


bool initializeRemoteDevice(RemoteDevice_short_Ptr src, tcpdump_t *meas) {

	src->Initialize();

	src->SetDevAddr(meas->src);

	src->mSiteInitialTime = gInitialTime;
	src->mLastlySeenTime = gCurrentTime() - gInitialTime;

//	time(&src->mTime_stat.time_prev);

	// -- Store the measurement
	src->PutToMeasBuf(*meas);

	// -- In case that the newly found device is an AP, and its channel is already known, then assign the same channel

	// -- Find the manufacturer of the device by looking up the OUI table
	//findManufacturer(src->GetDevAddr(), src->mDeviceBrand);

	// -- TODO: In case that the newly found device is an AP or a Service Agent, then we probably want to discard it.

	return true;
}



// -- Add the measurement to a database
// -- Create a log file per found MAC address, and store (timestamp, rss)
void addCollectedMeasurementToDeviceBuffer(tcpdump_t *meas) {		// -- called in PacketAnalyzerThread_TCPIP
	if(DEBUG_FUNCTION_CALL) fprintf(stdout, "add_measurement_to_buffer() is called. ");

	// -- If the measurement is for a new device, then allocate resources for the new device and store it to its device buffer
	if(meas->rss >= -90)
	{	// -- Discard too distant object
			RemoteDevice_short_Ptr device = findDevice(meas->src);

			if(device == NULL) {

					// -- If device is still NULL, then we must create a brand-new entry for this src. Otherwise, we reuse the entry.

					// -- Don't add any more device to the list if the track device list is already full.
					if(gFoundDevice.size() >= MAX_FOUND_DEVICES) {
						if(DEBUG_FUNCTION_CALL) printf("addCollectedMeasurementToDeviceBuffer() is end at 4. \n");
						return;
					} else {

						device = make_shared<RemoteDevice_short>();

						// -- Initialize and store the measurement
						if(initializeRemoteDevice(device, meas) == true) {
							gFoundDevice.push_back(device);
						} else
							device.reset();

						if(gDebugMsg)
							printf("ADD a new device [%s] -- %dth device\n", meas->src.c_str(), (int)gFoundDevice.size());
					}

					// -- Create a new log file
		#if CREATE_MEASUREMENT_LOGFILE
					char filename[255];
					sprintf(filename, "measurements/%s.txt", meas->src.c_str());
					FILE *fp;

					fp = fopen(filename, "w");
					if(fp == NULL) fprintf(stderr, "Error opening a NEW measurements file");
					else {
						fprintf(fp, "%.3lf RSS %d Ch %2d\n", gCurrentTime() - gInitialTime, meas->rss, meas->channel);
						 fclose(fp);
					}
		#endif

			}

			if(device != NULL)
			{
				// -- Add the measurement to the device buffer
				if(device->PutToMeasBuf(*meas) == false) {
					if(DEBUG_FUNCTION_CALL) printf("addCollectedMeasurementToDeviceBuffer() is end at 5. \n");
					return;
				}
			}

		}


	if(DEBUG_FUNCTION_CALL) fprintf(stdout, "add_measurement_to_buffer End.\n");
}

// -- Refer to http://xoax.net/cpp/ref/cpp_examples/incl/mean_med_mod_array/
int GetMedian(int daArray[], int iSize) {
    // Allocate an array of the same size and sort it.
	int dpSorted[iSize];
    for (int i = 0; i < iSize; ++i) {
        dpSorted[i] = daArray[i];
    }
    for (int i = iSize - 1; i > 0; --i) {
        for (int j = 0; j < i; ++j) {
            if (dpSorted[j] > dpSorted[j+1]) {
            	int dTemp = dpSorted[j];
                dpSorted[j] = dpSorted[j+1];
                dpSorted[j+1] = dTemp;
            }
        }
    }

    // Middle or average of middle values in the sorted array.
    int dMedian = 0.0;
    if ((iSize % 2) == 0) {
        dMedian = (int)((dpSorted[iSize/2] + dpSorted[(iSize/2) - 1])/2.0f+0.5f);
    } else {
        dMedian = dpSorted[iSize/2];
    }
    return dMedian;
}


// -- Perform a summarization process for an AP, which summarizes all the packets with the same src-AP pair into a single measurement
// -- This is necessary to avoid any bias in case that multiple APs captured packets from a src, yet the # of measurements captured at each AP is different.
bool measurementCompression(list<tcpdump_t> &measBuf, list<tcpdump_t> &compressedMeas) {

	compressedMeas.clear();

		for(list<tcpdump_t>::iterator it = measBuf.begin(); it !=  measBuf.end(); ++it) {
			tcpdump_t *meas = &*it;
			if(meas == NULL) continue;

			if(meas->rss < 0 && meas->rss > -100 && meas->src.empty() == false) {
				float rss_avg = 0;
//				int rss_max = -999;
				int cnt = 0;

//				int rsss[measBuf.size()];
//				int rss_median = 0;

				for(list<tcpdump_t>::iterator itt = it; itt != measBuf.end(); ++itt) {
					tcpdump_t *meas_rest = &*itt;

					//if(meas->capturedBy.compare(meas_rest->capturedBy) == 0 && meas_rest->rss < 0)	// -- When doing at WiFiTracker
					//if(meas->src.compare(meas_rest->src) == 0 && meas_rest->rss < 0 && && meas_rest->rss > -100)	// -- When doing at OmniSensr
					if(boost::iequals(meas_rest->src, meas->src) == true && meas_rest->rss < 0 && meas_rest->rss > -100)			// -- When doing at WiFiTracker
					{

						rss_avg += (float)meas_rest->rss;
//						rss_max = max(rss_max, meas_rest->rss);
//						rsss[cnt] = meas_rest->rss;
						cnt++;

						// -- Mark this measurement to indicate it is already processed
						meas_rest->rss = 255;
					}
				}

				if(cnt > 0) {
					rss_avg /= (float)cnt;

//					rss_median = GetMedian(rsss, cnt);

//					meas->rss = rss_median;
					meas->rss = rss_avg;//*0.4 + rss_max*0.6;
//					meas->rss = rss_max;

	//				if(VERBOSE_CAMERA_NODE >= 2)
//					if(cnt > 1)
//						printf("%d: Node[%s] -- AP[%s]: %d of meas is summarized from %d --> RSS (max, avg, median) = (%d, %d, %d), taken at %.2lf\n",
//								(int)compressedMeas.size(), meas->src.c_str(), meas->capturedBy.c_str(), cnt,
//								(int)measBuf.size(), rss_max, (int)(rss_avg + 0.5f), rss_median, meas->timstamp_at_AP);

					compressedMeas.push_back(*meas);
				}
			}
		}

	return true;
}

void periodicProcessing_per_Station(RemoteDevice_short_Ptr dev) {		// -- called in PacketAnalyzerThread_TCPIP
	if(DEBUG_FUNCTION_CALL) fprintf(stdout, "periodicProcessing_per_Station() is called. ");

	if(dev->GetMeasBufSize() == 0)
		return;

	double currTime = gCurrentTime() - gInitialTime;
	dev->mLastlySeenTime = currTime;

	if(gUseMeasurementSummarization == true) {

		list<tcpdump_t> collectedMeas_Dev;
		collectedMeas_Dev.clear();

		// -- Fill the first half of measBuf with the previous set of measurements
		collectedMeas_Dev = *(dev->AtMeasBuf());						// -- Copy the elements of the latest half of the all measurements

		// -- Fill the second half of measBuf with the current set of measurements
		if(gUseSlidingWindowBuffer == true) {
			collectedMeas_Dev.splice(collectedMeas_Dev.begin(), dev->mMeasBuffer_Dev_prev);		// -- Move the elements of the old half of the all measurements
			dev->mMeasBuffer_Dev_prev.clear();

			// -- Move the elements of the latest half of the all measurements to the old half
			dev->mMeasBuffer_Dev_prev.splice(dev->mMeasBuffer_Dev_prev.begin(), *(dev->AtMeasBuf()));
		}

		// -- (3) Perform the packet compression/summarization process for each device, which summarizes all the packets with the same src-dst pair during a period into a single measurement
		// -- This is necessary to avoid any bias in case that multiple APs captured packets from a src, yet the # of measurements captured at each AP is different.
		// -- This will also mitigate (1) any packet interference caused by multi-path fading and (2) the effect when an AP captures a packet at a channel yet the packet is actually transmitted at a different channel.
		// -- This would work as a moving window based median filter
		if(collectedMeas_Dev.size() > 0)
			if(measurementCompression(collectedMeas_Dev, dev->mSummarizedMeas_Dev) == false) {
				// -- DO something
			}
	} else {

		// -- Report the received measurement without any summarization
		dev->mSummarizedMeas_Dev = *(dev->AtMeasBuf());
	}

	dev->mMeasBuffer_Dev.clear();

	// -- The # of the compressedMeas packets in the buffer for a device should be the # of APs who captured packets  from the device
	// -- If the # of the summarized packets is at least three, then we can triangulate or perform fingerprint-based location estimation.

	if(DEBUG_FUNCTION_CALL) fprintf(stdout, "periodicProcessing_per_Station End.\n");
}

bool ParseSetting(const char *configFile) {

	ParseConfigTxt config(configFile);

	int paramReadCnt = 0;
	if(config.getValue_string("VERSION", gSoftwareVersion)) {
		paramReadCnt++;
		printf("VERSION = %s\n", gSoftwareVersion.c_str());
	}
	if(config.getValue_string("NIC", gNIC)) {
		paramReadCnt++;
		printf("NIC = %s\n", gNIC.c_str());
	}
	if(config.getValue_float("PERIODIC_BUFFER_PROCESSING_INTERVAL", gPeriodicBufferProcessingInterval)) {
		paramReadCnt++;
		printf("PERIODIC_BUFFER_PROCESSING_INTERVAL = %.2f\n", gPeriodicBufferProcessingInterval);
	}
	if(config.getValue_float("IOT_PERIODIC_STATE_REPORT_INTERVAL", gPeriodicStateReportInterval)) {
		paramReadCnt++;
		printf("IOT_PERIODIC_STATE_REPORT_INTERVAL = %.2f\n", gPeriodicStateReportInterval);
	}
	if(config.getValue_int("WIFICALIB_RANDOM_BACKOFF_INTERVAL", gCalibRandomBackoff)) {
		paramReadCnt++;
		printf("WIFICALIB_RANDOM_BACKOFF_INTERVAL = %d\n", gCalibRandomBackoff);
	}
	if(config.getValue_string("OMNISENSOR_APP_ROOT_PATH", gOmniSensrAppPath)) {
		paramReadCnt++;
		printf("OMNISENSOR_APP_ROOT_PATH = %s\n", gOmniSensrAppPath.c_str());
	}

	if(config.getValue_int("TIME_THRESHOLD_DEVICE_DISAPPEAR", gTimeThresholdForDevAbsency)) {
		paramReadCnt++;
		printf("TIME_THRESHOLD_DEVICE_DISAPPEAR = %d\n", gTimeThresholdForDevAbsency);
	}

	if(config.getValue_int("VERBOSE_MODE", gDebugVerboseMode)) {
		paramReadCnt++;
		printf("VERBOSE_MODE = %d\n", gDebugVerboseMode);
	}
	if(config.getValue_bool("ONLY_NON_BEACON_PACKETS", gOnlyNonBeaconPackets)) {
		paramReadCnt++;
		printf("ONLY_NON_BEACON_PACKETS = %d\n", gOnlyNonBeaconPackets);
	}
	if(config.getValue_bool("ONLY_BROADCAST_PACKETS", gOnlyBroadcastPackets)) {
		paramReadCnt++;
		printf("ONLY_BROADCAST_PACKETS = %d\n", gOnlyBroadcastPackets);
	}
	if(config.getValue_bool("USE_SLIDING_WINDOW_BUFFER", gUseSlidingWindowBuffer)) {
		paramReadCnt++;
		printf("USE_SLIDING_WINDOW_BUFFER = %d\n", gUseSlidingWindowBuffer);
	}
	if(config.getValue_bool("USE_MEASUREMENT_SUMMRIZATION", gUseMeasurementSummarization)) {
		paramReadCnt++;
		printf("USE_MEASUREMENT_SUMMRIZATION = %d\n", gUseMeasurementSummarization);
	}

	bool ret = false;
	if(paramReadCnt != (int)config.paramSize()) {
		fprintf(stderr, "!! paramReadCnt[%d] != config.paramSize() [%d] !!", paramReadCnt, config.paramSize());
		ret = false;
	} else
		ret = true;

	return ret;
}

// -- Scramble MAC address and RSS to a random one
string scrambleDATA(int deviceType, int channel, string src, int rss) {
	string data;

	// -- Put the device type at the front
	char type[2] = {'\0'};
	sprintf(type, "%d", deviceType);
	type[1] = '\0';
	data.append(type);

	// -- Put the channel
	char ch[2] = {'\0'};
	sprintf(ch, "%x", channel);
	ch[1] = '\0';
	data.append(ch);

	// -- Put the device MAC address
	char dst[13];
	int j=0;
	for(uint i=0; i<src.size(); i++) {
		if(src.c_str()[i] != ':')
			dst[j++] = src[i];
	}
	dst[j] = '\0';
	data.append(dst);

	// -- Put the RSS value
	char rs[5] = {'\0'};
	sprintf(rs, "%.2x", abs(rss)*2);
	data.append(rs);

	return data;
}

// -- Switch the device's wifi mode from the default sniffer mode to the packet injection mode.
void WiFiModeSwitch_PacketInjection() {

	// -- Switch to Packet Injection mode
	system("sudo iwconfig wlan0 mode managed; sudo ifconfig wlan0 up");
	system("sudo iwconfig wlan1 mode managed; sudo ifconfig wlan1 up");
	system("sudo iwconfig wlan2 mode managed; sudo ifconfig wlan2 up");

	if(gDebugMsg)
		cout << "[WiFi_PacketProcessor] Switched Sniffer mode to Packet Injection mode\n";
}

// -- Switch the device's wifi mode from the packet injection mode to the default packet sniffer mode.
void WiFiModeSwitch_PacketSniffer() {

	// -- Switch back to Packet Sniffer mode
	system("sudo ifconfig wlan0 down; sudo iwconfig wlan0 mode monitor; sudo ifconfig mon0 up");
	system("sudo ifconfig wlan1 down; sudo iwconfig wlan1 mode monitor; sudo ifconfig mon1 up");
	system("sudo ifconfig wlan2 down; sudo iwconfig wlan2 mode monitor; sudo ifconfig mon2 up");

	if(gDebugMsg)
		cout << "[WiFi_PacketProcessor] Switched Packet Injection mode to Sniffer mode\n";

	// -- Try to kill itself, then the systemctl will re-initiate this process. That's what I want to do.
	fprintf(stdout, "[WiFi_PacketProcessor] Kill itself to be restarted\n");
	exit(0);
}


// -- TODO: Don't know if we leave the app to parse the entire message or if we make a nested structure like Thing Shadow so that each app knows its own control field. For now, I will just leave the full freedom to the app.
void IoT_ControlChannel_CallBack_func(string topic, string controlMsg) {
	// -- Parse the message as you wish and take actions if needed. The message must be in a JSON format
	json controlMsgJson;

	try {
		controlMsgJson = json::parse(controlMsg.c_str());

		if(gDebugIoT)
		{
			printf("+++++++ [WiFi_PacketProcessor] Control Message Received +++++++++++++++++++++++\n");
			std::cout << std::setw(2) << controlMsgJson << '\n';
			printf("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
		}

//		if(gDebugIoT)
//		{
//			//printf("******* Control Channel Msg Received *******\n%s\n********************************************\n", controlMsg.c_str());
//
//			// special iterator member functions for objects
////			cout << "Control Msg rsved in JSON: " << "\n";
////			cout << setw(4) << controlMsgJson << "\n\n";
//
//			// special iterator member functions for objects
//			for (json::iterator it = controlMsgJson.begin(); it != controlMsgJson.end(); ++it) {
//				string key = it.key();
//				ostringstream value;
//				value << it.value();
//
//				cout << key << " : " << value.str() << "\n";
//			}
//		}

		// -- Flatten the nested JSON into un-nested one
		json controlMsgJson_flat = controlMsgJson.flatten();

//		if(gDebugIoT)
//		{
//			cout << "Control Msg rsved in JSON (flattened): " << "\n";
//			cout << setw(4) << controlMsgJson_flat << "\n\n";
//		}


		// -- Try to access keys and get their values with a type
		// -- Note: Must catch the two exceptions {out_of_range, domain_error} whenever accessing a value.
		// -- 		Otherwise, it will exit the program if the type does not match or if the key does not exist.
		// -- ########### ADD USER-DEFINED ACTION BELOW #####################

		// -- Check if WiFi Calib command is set. If so, it will initiate packet injection for WiFi calibration. For example,
		if(controlMsgJson_flat.find("/wifiCalib/Mode") != controlMsgJson_flat.end()) {
			unsigned int wifiCalibMode = controlMsgJson_flat.at("/wifiCalib/Mode");

			// -- Force quit packet injection
			if(wifiCalibMode == WIFI_TRACKING)
				system("sudo killall packetInjectionTest");

			if(wifiCalibMode != gWiFiMode) {

				pthread_mutex_lock(&gMutex);
				gWiFiMode = wifiCalibMode;
				pthread_mutex_unlock(&gMutex);

				if(wifiCalibMode == WIFI_TRACKING) {
					// -- Switch back to Packet Sniffer mode and kill itself, then the systemctl will re-initiate this process. That's what I want to do.
					WiFiModeSwitch_PacketSniffer();
				}

			}

		}


	} catch (std::invalid_argument& e) {		// -- Handle exception when the received msg does not comply with JSON standard
		// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
    	//std::cout << "invalid_argument: " << e.what() << '\n';
	} catch (std::out_of_range& e) {	// -- Handle exception when a non-existing filed is accessed.
		// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
		//std::cout << "out of range: " << e.what() << '\n';
	} catch (std::domain_error& e) {	// -- Handle exception when an incompatible conversion is attempted (e.g., float --> string).
		// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
	   // std::cout << "domain error: " << e.what() << '\n';
	}

}

/*
 * Note that all the types of JsonPrimitiveType of AWS ThingShadow parameters are grouped into just 6 types: int, uint, float, double, bool, string.
 */
void IoT_DeltaState_CallBack_func(string entireDeltaMsg) {

	json entireDeltaMsgJson = json::parse(entireDeltaMsg);

	// -- Check if there is at least a delta state destined to this app. If not, then exit this function
	if(entireDeltaMsgJson.find(gAppType) == entireDeltaMsgJson.end())
		return;

	json deltaMsgJson = json::parse(entireDeltaMsgJson.at(gAppType).dump());

	// -- If there is at least a delta state destined to this app, then start processing it by looking up the registered state params.
	try {

		if(gDebugIoT)
		{
			printf("+++++++ [WiFi_PacketProcessor] Delta State Received +++++++++++++++++++++++\n");
			std::cout << std::setw(2) << deltaMsgJson << '\n';
			printf("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
		}

		for (auto it = deltaMsgJson.begin(); it != deltaMsgJson.end(); ++it) {

			ostringstream actualValue;
			actualValue << it.value();

			devState_t *stateParam = gIoT_Gateway.FindStateParam(it.key());

					pthread_mutex_lock(&gMutex);
					// -- Check if there's matching state parameter
					if(stateParam != NULL) {

						if(gDebugIoT) printf("[Delta Message] stateParam FOUND (key = %s)\n", stateParam->key.c_str());

						// -- Do something here to react to the received delta message to this particular state parameter.
						switch(stateParam->type) {
							case SHADOW_JSON_INT32:
							case SHADOW_JSON_INT16:
							case SHADOW_JSON_INT8: {
								int stateParam_desired;
								sscanf(actualValue.str().c_str(), "%d", &stateParam_desired);

								if(gDebugIoT)
									printf("(report, desired) = (%d, %d) -- > ", *(int*)stateParam->value, stateParam_desired);

								// -- Replace the state parameter with the desired value
								*(int*)stateParam->value = stateParam_desired;

								// -- Reset this so that this state can be reported again immediately.
								stateParam->time_lastReported = 0;

								if(gDebugIoT)
									printf("(%d)\n", *(int*)stateParam->value);

							}
								break;
							case SHADOW_JSON_UINT32:
							case SHADOW_JSON_UINT16:
							case SHADOW_JSON_UINT8: {
								unsigned int stateParam_desired;
								sscanf(actualValue.str().c_str(), "%u", &stateParam_desired);

								if(gDebugIoT)
									printf("(report, desired) = (%u, %u) --> ", *(unsigned int*)stateParam->value, stateParam_desired);

								// -- Replace the state parameter with the desired value
								*(unsigned int*)stateParam->value = stateParam_desired;

								// -- Reset this so that this state can be reported again immediately.
								stateParam->time_lastReported = 0;

								if(gDebugIoT)
									printf("(%u)\n", *(unsigned int*)stateParam->value);
							}
								break;
							case SHADOW_JSON_FLOAT: {
								float stateParam_desired;
								sscanf(actualValue.str().c_str(), "%f", &stateParam_desired);

								if(gDebugIoT) {
									printf("[%s] ", actualValue.str().c_str());
									cout << fixed << setprecision(IOT_FLOAT_PRECISION);
									cout << " (report, desired) = ("<< *(float*)stateParam->value << ", " << stateParam_desired << ") --> ";
								}

								// -- Replace the state parameter with the desired value
								*(float*)stateParam->value = stateParam_desired;

								// -- Reset this so that this state can be reported again immediately.
								stateParam->time_lastReported = 0;

								if(gDebugIoT) {
									cout << fixed << setprecision(IOT_FLOAT_PRECISION);
									cout << "("<< *(float*)stateParam->value << ")" << endl;
								}
							}
								break;
							case SHADOW_JSON_DOUBLE: {
								double stateParam_desired;
								sscanf(actualValue.str().c_str(), "%lf", &stateParam_desired);

								if(gDebugIoT) {
									printf("[%s] ", actualValue.str().c_str());
									cout << fixed << setprecision(IOT_DOUBLE_PRECISION);
									cout << " (report, desired) = ("<< *(double*)stateParam->value << ", " << stateParam_desired << ") --> ";
								}

								// -- Replace the state parameter with the desired value
								*(double*)stateParam->value = stateParam_desired;

								// -- Reset this so that this state can be reported again immediately.
								stateParam->time_lastReported = 0;

								if(gDebugIoT) {
									cout << fixed << setprecision(IOT_DOUBLE_PRECISION);
									cout << "("<< *(double*)stateParam->value << ")" << endl;
								}
							}
								break;
							case SHADOW_JSON_BOOL: {
								char stateParam_desired[99] = {'\0'};
								sscanf(actualValue.str().c_str(), "%s", stateParam_desired);

								if(gDebugIoT)
									printf("(report, desired) = (%d, %s) --> ", *(bool*)stateParam->value, stateParam_desired);

								// -- Replace the state parameter with the desired value
								if(stateParam_desired[0] == 't')	// -- true
									*(bool*)stateParam->value = true;
								if(stateParam_desired[0] == 'f')	// -- false
									*(bool*)stateParam->value = false;

								// -- Reset this so that this state can be reported again immediately.
								stateParam->time_lastReported = 0;

								if(gDebugIoT)
									printf("(%d)\n", *(bool*)stateParam->value);

							}
								break;

							case SHADOW_JSON_STRING: {
								if(gDebugIoT)
									printf("(report, desired) = (%s, %s) -->", (*(string*)stateParam->value).c_str(), actualValue.str().c_str());

								// -- String-type value in JSON is wrapped with double-quotation marks, so get rid of them.
								string strExtracted = actualValue.str().substr(1,actualValue.str().size()-2);

								// -- Replace the state parameter with the desired value
								*(string*)stateParam->value = strExtracted;

								// -- Reset this so that this state can be reported again immediately.
								stateParam->time_lastReported = 0;

								if(gDebugIoT)
									printf("(%s)\n", (*(string*)stateParam->value).c_str());

							}
								break;

							default:
								break;
						}
					}
					pthread_mutex_unlock(&gMutex);
		}

	} catch (std::invalid_argument& e) {		// -- Handle exception when the received msg does not comply with JSON standard
		// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
    	//std::cout << "invalid_argument: " << e.what() << '\n';
    } catch (std::out_of_range& e) {	// -- Handle exception when a non-existing filed is accessed.
		// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
		//std::cout << "out of range: " << e.what() << '\n';
	} catch (std::domain_error& e) {	// -- Handle exception when an incompatible conversion is attempted (e.g., float --> string).
		// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
	   // std::cout << "domain error: " << e.what() << '\n';
	}
}

void* WiFiCalibThread(void *apdata) {

	pthread_mutex_lock(&gMutex);
	gIsWiFiInjectionRunning = true;
	pthread_mutex_unlock(&gMutex);

	int randomBackoff = rand() % gCalibRandomBackoff;
	usleep(randomBackoff*1000);

	// -- Kill any existing packetInjectionTest
	system("sudo killall packetInjectionTest");

	// -- Switch the device's wifi mode from the default sniffer mode to the packet injection mode.
	WiFiModeSwitch_PacketInjection();

	sleep(1);

	if(gDebugMsg)
		cout << "[WiFi_PacketProcessor] Injecting Packets for WiFi Calibration....\n";

	ostringstream cmd;

	cmd << "sudo " << gOmniSensrAppPath << "./packetInjectionTest mon0 & "
			<< "sudo " << gOmniSensrAppPath << "./packetInjectionTest mon1 & "
			<< "sudo " << gOmniSensrAppPath << "./packetInjectionTest mon2";

//	cmd << "sudo " << gOmniSensrAppPath << "./packetInjectionTest bond0 & ";

	system(cmd.str().c_str());

	sleep(10);	// -- Just to avoid initiating packet injection test due to duplicate commands arriving at the similar times.

	pthread_mutex_lock(&gMutex);
	gIsWiFiInjectionRunning = false;
	pthread_mutex_unlock(&gMutex);

	return NULL;

}

/*
	bool gDebugMqtt = true;
	bool gDebugIoT = true;

	bool gDebugMsg = false;
	bool SHOW_RAW_MESSAGE = false;
 */
int main(int argc, char *argv[]) {
	fprintf(stdout, "Usage: $ ./WiFi_PacketProcessor (optional)[{SHOW_DEBUG_IOT} {SHOW_DEBUG_MSG}]\n");

	if(argc == 1) {
		fprintf(stdout, "Will go with the default settings: (SHOW_DEBUG_IOT, SHOW_DEBUG_MSG) = (%d, %d)\n", gDebugIoT, gDebugMsg);
	} else if(argc != 3) {
		exit(0);
	} else {
		gDebugIoT = atoi(argv[1]);
		gDebugMsg = atoi(argv[2]);

		fprintf(stdout, "New setting: (SHOW_DEBUG_IOT, SHOW_DEBUG_MSG) = (%d, %d)\n", gDebugIoT, gDebugMsg);
	}

	srand(time(NULL));
	pthread_attr_init(&gThreadAttr);
	pthread_attr_setdetachstate(&gThreadAttr, PTHREAD_CREATE_JOINABLE);

	gInitialTime = gCurrentTime();

	// -- Find the IP address
	gAP_IPaddr = gIoT_Gateway.GetMyIpAddr();
	gAP_SN = gIoT_Gateway.GetMySN();
	gAP_DevName = gIoT_Gateway.GetMyDevName();
	gAP_StoreName = gIoT_Gateway.GetMyStoreName();

//	if(RegisterIPaddress() == false) {
//		printf("Failed to get IP address \n");
//		exit(0);
//	}

	gMeasBuffer_AP.clear();


	// -- Read config file
	ParseSetting((const char *)APP_CONFIG_FILE);

	string mqttTopicState, mqttTopicData;

		// -- Define the topic for state
		{
			gAppID = APP_ID_WIFI_PROCESSOR;

			mqttTopicState.clear();
			mqttTopicState.append(AWS_IOT_APP_STATE_CHANNEL_PREAMBLE);
			mqttTopicState.append("/");
			mqttTopicState.append(gAppID);

			printf("[WiFi_PacketProcessor] mqttTopicState = %s\n", mqttTopicState.c_str());
		}

		// -- Set the AppId in string. (e.g., mAppType = "8" for WiFi Tracker)
		{
			ostringstream appIdStr;
			appIdStr.str("");
			appIdStr.clear();
			appIdStr << wifiRSS;

			gAppType = appIdStr.str();
		}

		// -- Define the topic for data
		{
			// -- Compose the publish topic
			// -- AWS_IoT_Gateway will publish data to a topic 'data/<app_id>/<store_name_prefix>/<store_name_suffix>/<device_name>' (e.g., data/wifiRss/vmhq/16801/cam001)
			mqttTopicData.append(AWS_IOT_APP_DATA_CHANNEL_PREAMBLE);
			mqttTopicData.append("/");
			mqttTopicData.append(gAppID);
			mqttTopicData.append("/");
			mqttTopicData.append(gAP_StoreName);
			mqttTopicData.append("/");
			mqttTopicData.append(gAP_DevName);
			//mqttTopicData.append(gAP_SN);		// -- TODO: Need to switch to DeviceName, instead of SN.

			printf("[WiFi_PacketProcessor] mqttTopicData = %s\n", mqttTopicData.c_str());
		}



	{
		string cmd;
		cmd.clear();
#ifdef EthernetTesting

		cmd.append("tcpdump -B 4096 -U -nevv -tttt -s 64 -i ");
		cmd.append("eth0");
		cmd.append(" \"link[0] != 0x80\"");

#else
			cmd.append(TCPDUMP_COMMAND_1_Custom);
			cmd.append(gNIC);
			cmd.append(TCPDUMP_COMMAND_2);
#endif


		cout << "Run: \"" << cmd <<"\"" << endl;
		gStreamFromAP = popen(cmd.c_str(), "r");
		if(gStreamFromAP == NULL) {
			perror ("Error opening connection");
			return false;
		}
	}

	// -- ######################################################################

	// -- Register Delta State callback function
	gIoT_Gateway.RegisterDeltaCallbackFunc(&IoT_DeltaState_CallBack_func);

	// -- Register Control Channel callback function
	gIoT_Gateway.RegisterControlCallbackFunc(&IoT_ControlChannel_CallBack_func);

	// -- Start running aws-iot-gateway
	gIoT_Gateway.Run(gAppID, mqttTopicState, mqttTopicData, IOT_GATEWAY_CONFIG_FILE);
	// -- ######################################################################

	// -- Add state parameters

	/*
	 * << Things to be Reported >>
	 *
	 * gIsWiFiCalibRunning					bool
	 * gTotalDeviceNum						int
	 *
	 * << Things to be Synced >>
	 * gPeriodicStateReportInterval		float
	 */

	gIoT_Gateway.AddStateParam(SHADOW_JSON_BOOL, "isWiFiInjectionRunning", &gIsWiFiInjectionRunning, NULL, gPeriodicStateReportInterval);
	gIoT_Gateway.AddStateParam(SHADOW_JSON_UINT8, "WiFiMode", &gWiFiMode, NULL, gPeriodicStateReportInterval);
	gIoT_Gateway.AddStateParam(SHADOW_JSON_INT16, "totalDeviceNum", &gTotalDeviceNum, NULL, gPeriodicStateReportInterval);
	//gIoT_Gateway.AddStateParam(SHADOW_JSON_FLOAT, "StateReportInterval", &gPeriodicStateReportInterval, NULL, gPeriodicStateReportInterval);


	// -- ######################################################################

	{
		char beginChar;

		double currTime, prevTime_bufferProc,  prevTime_clearupDev, prevTime_heartbeat;

		// IOT Communicator for heartbeating
		IOT_Communicator iotc(APP_ID_WIFI_PROCESSOR);
		iotc.sendHeartbeat();

		currTime = gCurrentTime();
		prevTime_heartbeat = currTime;
		prevTime_bufferProc = currTime;



		do {

			// -- (1) Get, parse, and store the sniffed packets from tcpdump into a buffer
			// -- Temp. commented
			beginChar = packeBuffering();

			currTime = gCurrentTime();

			// -- Periodically send the State Info to Thing Shadow
			if(currTime - prevTime_clearupDev >= 30) {
				prevTime_clearupDev = currTime;

				// -- Discard devices that are not active
				clearUpDeviceList();
			}

			// -- Periodic processing of the buffered packets at AP-level
			if(currTime - prevTime_bufferProc >= gPeriodicBufferProcessingInterval) {
				prevTime_bufferProc = currTime;

				// -- (2) Dispatch the collected packets in the AP buffer to each device buffer
				if(gMeasBuffer_AP.size() > 0) {
					for(deque<tcpdump_t>::iterator it = gMeasBuffer_AP.begin(); it != gMeasBuffer_AP.end(); ++it)
					{
						tcpdump_t *meas = &*it;
						addCollectedMeasurementToDeviceBuffer(meas);
					}

					gMeasBuffer_AP.clear();
				}

				// -- (3) Perform packet summarization process at each device
				if(gFoundDevice.size() > 0) {
					 gTotalDeviceNum = gFoundDevice.size();

					for(list<RemoteDevice_short_Ptr>::iterator it = gFoundDevice.begin(); it != gFoundDevice.end();++it)
					{
						RemoteDevice_short_Ptr dev = *it;

						// -- See if there's any unprocessed measurements
						if(dev->GetMeasBufSize() > 0)
						{
							periodicProcessing_per_Station(dev);
						}
					}

					// -- Clear the buffer once it is processed.
					gMeasBuffer_AP.clear();
				}

				// -- (4) Send out all the summarized packets
				string payload;
				payload.clear();

				if(gFoundDevice.size() > 0)
					for(list<RemoteDevice_short_Ptr>::iterator it = gFoundDevice.begin(); it != gFoundDevice.end();++it)
					{
						RemoteDevice_short_Ptr dev = *it;
							if(dev->mSummarizedMeas_Dev.size() > 0) {
								for(list<tcpdump_t>::iterator itt = dev->mSummarizedMeas_Dev.begin(); itt != dev->mSummarizedMeas_Dev.end(); ++itt) {
									tcpdump_t *meas = &*itt;

									if(meas->channel <= 0 || meas->channel > 11
											|| meas->rss > 0 || meas->rss < -100
											|| meas->deviceType < 0
											)
										continue;

									// -- scramble the MAC address
									string scrmbldMAC = scrambleDATA(meas->deviceType, meas->channel, meas->src, meas->rss);
									scrmbldMAC.append("\n");

//									printf("(type,src,rss) = (%d, %s, %d) --> %s\n", meas->deviceType, meas->src, meas->rss, scrmbldMAC.c_str());

									payload.append(scrmbldMAC);
								}
								// -- Enable no-buffering when printing in a pipe
								//setvbuf(stdout, NULL, _IONBF, 0);		// -- _IONBF for no-buffering
							}

						dev->mSummarizedMeas_Dev.clear();
					}

#ifdef EthernetTesting
				{
					ostringstream data_cnt_str;
					payload.clear();
					static int data_cnt = 0;
					data_cnt_str << "Data Count: " << data_cnt++;
					payload = data_cnt_str.str();
				}
#endif
				// -- Send data
				if(payload.size() > 0)
				{
					// -- ######################################################################
					// -- Put into a message buffer for transmission
					// -- The data message will be sent to a topic named by 'AWS_IOT_APP_DATA_CHANNEL_PREAMBLE/<store_name>/<serial_number>/<app_id>'
					ostringstream msg;

					//msg << "#" << gAP_SN << " " << gAP_IPaddr << endl;
					msg << "#" << gAP_DevName << " " << gAP_IPaddr << endl;
					msg << payload;

					gIoT_Gateway.SendData(msg.str());
					// -- ######################################################################
				}
			}

			// Send heartbeat, once per minute
			if(currTime - prevTime_heartbeat > 60){
				iotc.sendHeartbeat();
				prevTime_heartbeat = currTime;
			}

			// -- Check if a WiFi Calibration process needs to be started. If so, initiate injecting null packets for WiFi calibration with a random backoff
			//if(gStartWiFiCalib == true) {
			// -- Check if the desired mode is either WIFI_CALIBRATION or WIFI_CALIB_VERIFICATION. If so, it will initiate packet injection for WiFi calibration.
			if(gWiFiMode == WIFI_CALIBRATION || gWiFiMode == WIFI_CALIB_VERIFICATION) {
				if(gIsWiFiInjectionRunning == false) {
					if(pthread_create(&gWiFiCalib_thread, &gThreadAttr, WiFiCalibThread, (void *)NULL)) {
						// -- There's some error
						printf("pthread_create ERRROR\n");
					}
				}
			}

			usleep(10*1000);		// -- Pause 10ms between two iterations

		} while(beginChar != EOF);

		fprintf(stderr, "Node[%s]: $$$$ Connection was closed, so packetStreamParsing() ended $$$$\n", gAP_IPaddr.c_str());

		pclose(gStreamFromAP);
	}

	return 0;
}

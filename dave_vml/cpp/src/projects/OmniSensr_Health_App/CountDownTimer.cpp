/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include "CountDownTimer.hpp"
//boost::mutex mCoutMutex;

CountDownTimer::CountDownTimer(int timerInterval, IntervaledTaskPtr workerObject)
        : mExpCallback(workerObject)
		, mQuit(false)
		, mLocService()
        , mWork( mLocService )
        , mTimerThread( boost::bind( &CountDownTimer::RunIOService,this ) )
        , mTimer( mLocService,boost::posix_time::seconds(timerInterval) )
{
//	mTimer.async_wait( boost::bind( &CountDownTimer::OnTimerExpire, this) );
	handle = boost::bind(&CountDownTimer::OnTimerExpire, this, _1);
	mTimer.async_wait(handle);
	if(mExpCallback)
	{
		mExpCallback->updateFromShadow(timerInterval);
	}
}

void CountDownTimer::RunIOService()
{
	mLocService.run();
};

CountDownTimer::~CountDownTimer()
{
//	if(1){
//	  boost::mutex::scoped_lock lock(mCoutMutex);
//	  std::cout <<"~CountDownTimer() Interval: " << mExpCallback->getTaskInterval() << std::endl;
//	}
	boost::mutex::scoped_lock lock(mTimerMutex);
	mQuit =true;
	mLocService.stop();
	mTimerThread.join();
}



void CountDownTimer::OnTimerExpire(const boost::system::error_code& error)
{
//	if(1){
//		boost::mutex::scoped_lock lock(mCoutMutex);
//		std::cout <<" OnTimerExpire fired in thread:" << boost::this_thread::get_id() << std::endl;
//	}
	if (error != boost::asio::error::operation_aborted) {
		boost::mutex::scoped_lock lock(mTimerMutex);
		if(mQuit)
			return;
		if(mExpCallback)
		{
			mExpCallback->DoTask();
			mTimer.expires_at(mTimer.expires_at() + boost::posix_time::seconds(mExpCallback->getInitInterval()));
			mExpCallback->setInterval(mExpCallback->getInitInterval());
			mTimer.async_wait(handle);
		}
	}
	// Post Each IntetvalTask Callback to Thread Pool Queue
};

void CountDownTimer::TimerReset(int newInterval)
{
	boost::mutex::scoped_lock lock(mTimerMutex);
	if(mQuit)	// check destructor
		return;
	if(mExpCallback)
	{
		mExpCallback->setInterval(newInterval);
		mExpCallback->resetCount();
//		TODO:update this part using mTimer....
		// Update on June 28 to test TimerReset
//		std::cout << "Timer Resetting" << std::endl;
//		std::cout << mTimer.expires_at() << std::endl;
		if (mTimer.expires_from_now(boost::posix_time::seconds(newInterval)) > 0 ) {
			mTimer.async_wait(handle);
		}
//		mTimer.expires_at(boost::posix_time::seconds(newInterval));
//		std::cout << mTimer.expires_at() << std::endl;
//		sleep(10);
	}
}

// Updated on June 27, to get task interval
int CountDownTimer::getInterval(){
	int interval = 0;
	if (mExpCallback){
		interval = mExpCallback->getTaskInterval();
	}
	return interval;
}

//int main(){
//	// Update on June 28 to test CountDownTimer functions
//	int timerInterval = 6;
//	IntervaledTaskPtr workerObject = null;
//	CountDownTimer cdtObject = new CountDownTimer(timerInterval,workerObject);
//	// Test getInterval()
//	cdtObject.getInterval();
//	// Test TimerReset()
//	return 0;
//}

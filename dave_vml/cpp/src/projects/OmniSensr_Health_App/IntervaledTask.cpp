/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
#include "IntervaledTask.hpp"

//if using namespace declare here
//namespace healthapp 
//{
//Class Constructor Implementation

/*
IntervaledTask::IntervaledTask() 
{
	//Constructor implementation
}

//Class Destructor Implementation
IntervaledTask::~IntervaledTask()
{
	//destructor implementation
}
*/
void IntervaledTask::updateFromShadow(int newInterval)
{
	mInitialInterval = newInterval;
	mTaskInterval = newInterval;
};
void IntervaledTask::setInterval(int newInterval)
{
	mTaskInterval = newInterval;
};

int IntervaledTask::getTaskInterval()
{
	return mTaskInterval;
};

void IntervaledTask::resetCount() {
	restartCount = 0;
}

int IntervaledTask::getInitInterval() {
	return mInitialInterval;
}

/*
void 
IntervaledTask::DoTask()
{
}
*/

// close namespace
//}


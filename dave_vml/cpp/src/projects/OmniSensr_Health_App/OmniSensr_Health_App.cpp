//============================================================================
// Name        : OmniSensr_Health_App.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description :
//============================================================================

#define DEBUG_HEALTHAPP true

#include "OmniSensr_Health_App.hpp"

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include "modules/utility/redis_wrapper.hpp"
#include <boost/shared_ptr.hpp>
#include <boost/functional/hash.hpp>
#include "modules/utility/TimeKeeper.hpp"
#include "modules/iot/IOTUtility.hpp"
#include "json.hpp"
#include "modules/iot/MQTTClient.hpp"
#include <IntervaledTaskFactory.hpp>
#include <boost/thread.hpp>
#include "modules/utility/JsonUtil.hpp"
#include <pigpiod_if2.h>

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <unistd.h>
#include <ctime>

using std::cout;
using std::string;
using std::ostringstream;
using std::vector;
using std::endl;
using json = nlohmann::json;

OmniSensr_Health_App::OmniSensr_Health_App(const std::string& configPath) : OmniSensrApp("healthApp", configPath){
	// set initial variable values here
	heartbeat_timeout = 300;
	snapShotUrl = "localhost:8080/?action=snapshot";
	mInitialTime = 0;
	mIoT_State_Report_Interval = 4.0;
	mIoT_Heartbeat_Interval = 600;
	mIoT_Cpu_Collect_Interval = -1;
	mIoT_Temp_Collect_Interval = -1;
	mIoT_Cpu_Report_Interval = -1;
	mIoT_Temp_Report_Interval = -1;
	mIoT_App_Cpu_Collect_Interval = -1;
	mIoT_App_Mem_Collect_Interval = -1;
	mIoT_App_Cpu_Report_Interval = -1;
	mIoT_App_Mem_Report_Interval = -1;
	mIoT_Cpu_Load = 0.0;
	mIoT_Cpu_Temp = 0.0;

	// GPIO pins
	watchdog_pin = 19;
	ledset_pin[0] = make_pair(4, 5);
	ledset_pin[1] = make_pair(6, 7);
	ledset_pin[2] = make_pair(8, 9);

	devSubtopic = getStoreLocation() + "/" + getDeviceName();

	// Get the current state of the services
	mIoT_WiFi_Role = getServiceRunState("OmniSensr_Service_Wifi");
	mIoT_Face_Role = getServiceRunState("OmniSensr_Service_Demogrph");
	mIoT_Visn_Role = getServiceRunState("OmniSensr_Service_Vision");
	mIoT_Traq_Role = getServiceRunState("OmniSensr_Service_WifiTrack");
	//Get our current OS Version
	mIoT_OSVer=getOSVer();
	loadQAState();

	// initailize countdown timers
	wifiTimer = nullptr;
	demoTimer = nullptr;
	visnTimer = nullptr;
	traqTimer = nullptr;
	connTimer = nullptr;

	cpuCollectTimer = nullptr;
	tempCollectTimer = nullptr;

	AppCPUTimer = nullptr;
	AppMEMTimer = nullptr;
	mIoT_Cpu_Threshold = 0;
	mIoT_Temp_Threshold = 0;
	mIoT_Heartbeat_Interval = 600;

	watchdog_interval = 30;
	OmniSensrVer = 1;
}

OmniSensr_Health_App::~OmniSensr_Health_App(){
	// Nothing to do here
	cout << "Exiting healthApp" << endl;
}

// trim from start
std::string &OmniSensr_Health_App::ltrim(std::string &s) {
	s.erase(s.begin(), std::find_if(s.begin(), s.end(),
			std::not1(std::ptr_fun<int, int>(std::isspace))));
	return s;
}
// trim from end
std::string &OmniSensr_Health_App::rtrim(std::string &s) {
	s.erase(std::find_if(s.rbegin(), s.rend(),
			std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
	return s;
}

// trim from both ends
std::string &OmniSensr_Health_App::trim(std::string &s) {
	return ltrim(rtrim(s));
}

// Make system level call to determine our current OS Version
// Version info updated into /usr/local/www/version.txt
// by ~/omnisensr-runners/sync.sh
string OmniSensr_Health_App::getOSVer()
{
	string cmd;
	string rtnVal;
	rtnVal.clear();
	cmd.clear();
	cmd = "cat /usr/local/www/version.txt";
	FILE *fp = popen(cmd.c_str(), "r");
	char versionNumber[32] = "";
	if(fp){
		//		fscanf(fp, "%s",versionNumber);
		if(fgets(versionNumber, 32, fp) != NULL){
			rtnVal=trim(rtnVal.assign(versionNumber));
		}
		pclose(fp);
	}
	else {
		string msg = "Failed to get version number from /usr/local/www/version.txt";
		cout << msg << endl;
		sendError(msg, false);
	}
	if(DEBUG_HEALTHAPP)
		printf("[HealthApp] Current OSVer:==>%s<==\n",rtnVal.c_str());

	return rtnVal;
}

void OmniSensr_Health_App::loadQAState(){
	// read our last known QA state and version
	json lastQAState;
	std::ifstream ifs("config/OmniSensr_Health_App/lastQAState.json",std::ifstream::in);
	if(ifs.good()){
		ifs >> lastQAState;
	}
	else {
		lastQAState["QAPassed"] = false;
		lastQAState["QAVer"] = "0.0";
	}
	auto it_QAState = lastQAState.find("QAPassed");
	if(it_QAState != lastQAState.end()){
		mIoT_QA_OK = *it_QAState;
	}
	it_QAState = lastQAState.find("QAVer");
	if(it_QAState != lastQAState.end()){
		mIoT_QAVer = *it_QAState;
		mIoT_QAVer = trim(mIoT_QAVer);
	}
}

// Make system level call to determine the running state of each OmniSensr service app
// Constructs a systemctl status OmniSensr_Service_"X" for each of the 4 services
// Checks the return status string for "Active: active (running)"
bool OmniSensr_Health_App::getServiceRunState(string serviceName){
	char Launch_Result[128]="";
	size_t whchResult = std::string::npos;
	string result_lookup;
	result_lookup.clear();
	string cmd ("systemctl status ");
	cmd.append(serviceName).append("|grep \"Active:\"");
	FILE *fp = popen(cmd.c_str(), "r");
	if(fp == NULL) {
		perror ("Error calling systemctl status");
		sendError("Error calling systemctl status", true, true);
		return false;
	}

	else {
		if(fgets(Launch_Result, 128, fp) != NULL){
			result_lookup.append(Launch_Result);
			whchResult = result_lookup.find("(running)");
		}
		pclose(fp);
	}
	if(DEBUG_HEALTHAPP){
		printf("[HealthApp] Check status:%s \n",serviceName.c_str());
		printf("[HealthApp] Results String:%s \n",result_lookup.c_str());
		printf("[HealthApp] %s is Running? %s \n",serviceName.c_str(),(whchResult != std::string::npos) ? "true" :" false");
	}
	return (whchResult != std::string::npos);
}

// translate app name to service name and call startStopServiceByName
void OmniSensr_Health_App::startStopServiceByAppName(string appName, string value) {
	string role = "";
	if (appName.compare("wifiRSS") == 0) {
		role = "WiFi_Role";
	}
	else if (appName.compare("visionApp") == 0) {
		role = "Visn_Role";
	}
	else if (appName.compare("demoApp") == 0) {
		role = "Face_Role";
	}
	else if (appName.compare("wifiTrack") == 0) {
		role = "Traq_Role";
	}
	else if (appName.compare("connectorApp") == 0) {
		role = "Conn";
	}
	else if (appName.compare("mjpg") == 0) {
		role = "Mjpg";
	}
	if (!role.empty()) startStopServiceByName(role,value);
}

void OmniSensr_Health_App::restartServiceByAppName(string serviceName) {
	string cmd = "sudo systemctl restart OmniSensr_Service_";
	if (serviceName.compare("wifiRSS") == 0) {
		cmd.append("Wifi");
	}
	else if (serviceName.compare("visionApp") == 0) {
		cmd.append("Vision");
	}
	else if (serviceName.compare("demoApp") == 0) {
		cmd.append("Demogrph");
	}
	else if (serviceName.compare("wifiTrack") == 0) {
		cmd.append("WifiTrack");
	}
	else if (serviceName.compare("connectorApp") == 0) {
		cmd.append("AWS_IoT");
	}
	else if (serviceName.compare("mjpg") == 0) {
		cmd.append("Camera");
	}
	else
		return;
	cmd.append(".service");
	FILE* fp = popen(cmd.c_str(), "r");
	if (!fp) {
		string msg = "Failed to restart service: " + serviceName;
		cout << msg << endl;
		sendError(msg, false);
	}
	else {
		cout << "Restart service: " + serviceName << endl;
		pclose(fp);
	}
}

//Make system level call to get each service into its appropriate running state
// as set by the AWS IoT device Shadow
void OmniSensr_Health_App::startStopServiceByName(string serviceName,string value) {
	// don't do anything if stop connectorApp is called
	if (serviceName.compare("connectorApp") == 0 && value[0] == 'f')
		return;
	if(DEBUG_HEALTHAPP)
		printf("[HealthApp] checkServiceByName: %s : %s\n",serviceName.c_str(),value.c_str());
	string service_lookup ("WiFi|Visn|Face|Traq|Conn|Mjpg");
	size_t whchService = service_lookup.find(serviceName.substr(0,4));
	string service_name ("OmniSensr_Service_");
	bool *role = NULL;
	bool toVal = (value.find("true")!=string::npos);
	if(whchService != string::npos){
		switch(int(whchService/5)){
		case 0:
			service_name.append("Wifi");
			role = &mIoT_WiFi_Role;
			break;
		case 1:service_name.append("Vision");
		role = &mIoT_Visn_Role;
		break;
		case 2:service_name.append("Demogrph");
		role = &mIoT_Face_Role;
		break;
		case 3:service_name.append("WifiTrack");
		role = &mIoT_Traq_Role;
		break;
		case 4:service_name.append("AWS_IoT");
		break;
		case 5:service_name.append("Camera");
		break;
		}
		service_name.append(".service");
		ostringstream reportStrStrm;
		for(int i=0;i<3;i++){
			string command_str ("sudo systemctl");
			if(toVal){
				switch (i){
				case 0:
					command_str.append(" enable ").append(service_name);
					break;
				case 1:
					command_str.append(" daemon-reload ");
					break;
				case 2:
					command_str.append(" start ").append(service_name);
					break;
				}

			}
			else {
				switch (i){
				case 0:
					command_str.append(" stop ").append(service_name);
					break;
				case 1:
					command_str.append(" disable ").append(service_name);
					break;
				case 2:
					command_str.append(" daemon-reload ");
					break;
				}
			}
			if(DEBUG_HEALTHAPP)
				printf("[HealthApp] Process: %s\n",command_str.c_str());
			FILE *fp = popen(command_str.c_str(), "r");
			if(fp == NULL) {
				perror ("Error calling systemctl\n");
				sendError("Error calling systemctl", false);
				return;
			}
			pclose(fp);
		}
		//Find our registered state variable and update it to the current state of this service
		bool check = getServiceRunState(service_name);
		if(role) *role = check;
		if(DEBUG_HEALTHAPP)
			printf("[HealthApp] Status For: %s - %s\n",service_name.c_str(), check ? "true" : "false");
	}

}

// Reboot the device
void OmniSensr_Health_App::rebootDevice() {
	string cmd = "sudo shutdown -r now";
	FILE *fp = popen(cmd.c_str(), "r");
	if (fp) {
		cout << "Rebooting" << endl;
		pclose(fp);
	}
	else {
		cout << "Failed to reboot" << endl;
		sendError("Failed to reboot", false);
	}
}

// Reset the countdown timer when a heartbeat comes in
void OmniSensr_Health_App::localHeartbeat_callback(const struct mosquitto_message *message) {
	string topic (message->topic);
	string payload ((char*)message->payload);
	payload.resize(message->payloadlen);
	// the minimum time interval for timeout.
	uint interval = 60;
	try {
		json data = json::parse(payload);
		interval = (!data.value("interval", json()).is_null()) ? max(interval, data["interval"].get<uint>()) : interval;
	} catch (std::invalid_argument &e) {
		cout << "Failed to parse heartbeat" << endl;
		//sendError("Failed to parse heartbeat", false);
		return;
	}
	// reset timer for corresponding app
	if (topic.find("connectorApp") != string::npos) {
		cout << "Received heartbeat from connectorApp" << endl;
		connTimer->TimerReset(interval);
	}
	else if (mIoT_WiFi_Role && topic.find("wifiRSS") != string::npos) {
		cout << "Received heartbeat from wifiRSS" << endl;
		wifiTimer->TimerReset(interval);
	}
	else if (mIoT_Visn_Role && topic.find("visionApp") != string::npos) {
		cout << "Received heartbeat from visionApp" << endl;
		visnTimer->TimerReset(interval);
	}
	else if (mIoT_Face_Role && topic.find("demoApp") != string::npos) {
		cout << "Received heartbeat from demoApp" << endl;
		demoTimer->TimerReset(interval);
	}
	else if (mIoT_Traq_Role && topic.find("wifiTrack") != string::npos) {
		cout << "Received heartbeat from wifiTrack" << endl;
		traqTimer->TimerReset(interval);
	}

}

// Start or stop services according to the requested action from appCrash message
void OmniSensr_Health_App::localAppCrash_callback(const struct mosquitto_message *message) {
	string payload ((char *)message->payload);
	string appName;
	payload.resize(message->payloadlen);

	// send message out to data channel
	sendData(payload, devSubtopic + "/appCrash");

	json msg;
	try {
		msg = json::parse(payload);
	} catch(std::invalid_argument &e) {
		cout << "Failed to parse appCrash message" << endl;
		//sendError("Failed to parse appCrash message", false);
		return;
	}
	if(!msg.value("name", json()).is_null()) {
		appName = msg["name"];
	}
	else {
		cout << "No name field in appCrash message" << endl;
		//sendError("No name field in appCrash message", false);
		return;
	}
	if(!msg.value("actionRequested", json()).is_null()) {
		string action = msg["actionRequested"];
		if (action.compare("start") == 0) {
			startStopServiceByAppName(appName, "true");
		}
		else if (action.compare("stop") == 0) {
			startStopServiceByAppName(appName, "false");
		}
		else if (action.compare("restart") == 0) {
			// set role to false because it failed to heartbeat
			if (appName.compare("wifiRSS") == 0) {
				mIoT_WiFi_Role = false;
			}
			else if (appName.compare("visionApp") == 0) {
				mIoT_Visn_Role = false;
			}
			else if (appName.compare("demoApp") == 0) {
				mIoT_Face_Role = false;
			}
			else if (appName.compare("wifiTrack") == 0) {
				mIoT_Traq_Role = false;
			}
			restartServiceByAppName(appName);
		}
		else if (action.compare("report") == 0) {
			// do nothing
		}
		else {
			cout << "actionRequested: " << action << " not recognized action" << endl;
		}
	}
	else {
		cout << "No actionRequested field in appCrash message" << endl;
		//sendError("No actionRequested field in appCrash message", false);
	}
}

// Process metric messages
void OmniSensr_Health_App::localMetric_callback(const struct mosquitto_message *message) {
	string topic(message->topic);
	if (topic.substr(topic.find("/") + 1).compare("cpuCollect") == 0) {
		int load = std::stoi((char*)message->payload);
		//		cout << "cpu: " << load << endl;
		cpuStat_lock.lock();
		cpuStat.push_back(load);
		cpuStat_lock.unlock();
	}
	else if (topic.substr(topic.find("/") + 1).compare("tempCollect") == 0) {
		float temp = std::stof((char*)message->payload);
		//		cout << "temp: " << temp << endl;
		tempStat_lock.lock();
		tempStat.push_back(temp);
		tempStat_lock.unlock();
	}
	else if (topic.substr(topic.find("/") + 1).compare("cpuInfo") == 0) {
		if (message->payloadlen <= 0) {
			sendError("Failed to get cpuInfo", false);
			return;
		}
		float wifiCPU, visnCPU, demoCPU, traqCPU;
		sscanf((char*)message->payload, "%f %f %f %f", &wifiCPU, &visnCPU, &demoCPU, &traqCPU);
		cpuStat_lock.lock();
		if (wifiCPU != -1) {
			wifiCPUStat.push_back(wifiCPU);
		}
		if (visnCPU != -1) {
			visnCPUStat.push_back(visnCPU);
		}
		if (demoCPU != -1) {
			demoCPUStat.push_back(demoCPU);
		}
		if (traqCPU != -1) {
			traqCPUStat.push_back(traqCPU);
		}
		cpuStat_lock.unlock();
	}
	else if (topic.substr(topic.find("/") + 1).compare("memInfo") == 0) {
		if (message->payloadlen <= 0) {
			sendError("Failed to get memInfo", false);
			return;
		}
		float wifiMEM, visnMEM, demoMEM, traqMEM;
		sscanf((char*)message->payload, "%f %f %f %f", &wifiMEM, &visnMEM, &demoMEM, &traqMEM);
		cpuStat_lock.lock();
		if (wifiMEM != -1) {
			wifiMEMStat.push_back(wifiMEM);
		}
		if (visnMEM != -1) {
			visnMEMStat.push_back(visnMEM);
		}
		if (demoMEM != -1) {
			demoMEMStat.push_back(demoMEM);
		}
		if (traqMEM != -1) {
			traqMEMStat.push_back(traqMEM);
		}
		cpuStat_lock.unlock();
	}
}

// Grab camera snapshot and upload to FTP server
void OmniSensr_Health_App::getSnapshot(const std::string& param) {
	// format the filename to YYYYMMDD_HHmmss_micsec.jpg
	string filename = boost::posix_time::to_iso_string(boost::posix_time::microsec_clock::universal_time());
	string year = filename.substr(0, 4);
	string month = filename.substr(4, 2);
	string day = filename.substr(6, 2);
	string times = filename.substr(9);
	times[6] = '_';
	filename = year + month + day + "_" + times + ".jpg";
	string filepath = filename;
	// grab snapshot from url
	string cmd = "wget --output-document=\"" + filepath + "\" " + snapShotUrl;
	FILE *fp = popen(cmd.c_str(), "r");
	if (fp == NULL) {
		cout << "Failed to execute getSnapshot" << endl;
		sendError("Failed to execute getSnapshot", false);
		return;
	}
	pclose(fp);

	// get ftp info from file
	json ftpinfo;
	string host = "";
	string user = "";
	string password = "";
	if (JsonUtil::readJson("config/OmniSensr_Health_App/cmsftp.txt", ftpinfo)) {
		cout << "Failed to open cmsftp.txt file" << endl;
		sendError("Failed to open cmsftp.txt file", false);
		return;
	}
	else {
		host = ftpinfo["host"].get<std::string>();
		user = ftpinfo["user"].get<std::string>();
		password = ftpinfo["password"].get<std::string>();
	}

	// upload file to FTP
	cmd = "curl -T ";
	cmd.append(filepath);
	cmd.append(" " + host);
	string storeName = getStoreLocation();
	storeName.erase(storeName.find("/"), 1);
	boost::algorithm::to_lower(storeName);
	cmd.append(storeName + "/" + getDeviceName() + "/overview/");
	cmd.append(year + "/" + month + "/" + day + "/");
	// create folder if not exist
	cmd.append(" --ftp-create-dirs");
	cmd.append(" --user " + user + ":" + password);
	cmd.append(" 2>&1 | grep curl");
	fp = popen(cmd.c_str(), "r");
	char buff[128] = "";
	bool success = true;
	if (fp == NULL) {
		cout << "Failed to send snapshot to FTP" << endl;
		sendError("Failed to send snapshot to FTP", false);
		success = false;
	}
	else {
		// check if FTP connect and upload didn't fail
		while(fgets(buff, 128, fp)) {
			string temp(buff);
			if (temp.find("curl: (67)") != string::npos) {
				cout << "Access denied for connection to FTP" << endl;
				sendError("Access denied for connection to FTP", false);
				success = false;
			}
			else if (temp.find("curl: (7)") != string::npos) {
				cout << "Connection timeout to FTP" << endl;
				sendError("Connection timeout to FTP", false);
				success = false;
			}
			else if (temp.find("curl:") != string::npos) {
				cout << temp << endl;
				sendError(temp, false);
				success = false;
			}
		}
		pclose(fp);
	}

	// remove the local image
	cmd = "rm " + filepath;
	fp = popen(cmd.c_str(), "r");
	if (fp == NULL) {
		cout << "Failed to delete snapshot" << endl;
		//sendError("Failed to delete snapshot", false);
	}
	else
		pclose(fp);

	// send success message through data channel
	if (success) {
		string msg = "{\"result\":\"success\", \"requestVer\":" + param + "}";
		sendData(msg, devSubtopic + "/snapshot");
	}
	return;
}

void OmniSensr_Health_App::setLEDColor(int setnum, std::string color) {
	int red = ledset_pin[setnum].first;
	int green = ledset_pin[setnum].second;
	int pi = pigpio_start(NULL, NULL);
	if (pi < 0) {
		cout << "[setLEDColor] Failed to connect to pigpiod" << endl;
		sendError("[setLEDColor] Failed to connect to pigpiod", false);
		return;
	}
	set_mode(pi, red, PI_OUTPUT);
	set_mode(pi, green, PI_OUTPUT);
	if (color.compare("red") == 0) {
		gpio_write(pi, red, PI_HIGH);
		gpio_write(pi, green, PI_LOW);
	}
	else if (color.compare("green") == 0) {
		gpio_write(pi, red, PI_LOW);
		gpio_write(pi, green, PI_HIGH);
	}
	else if (color.compare("yellow") == 0) {
		gpio_write(pi, red, PI_HIGH);
		gpio_write(pi, green, PI_HIGH);
	}
	else if (color.compare("off") == 0) {
		gpio_write(pi, red, PI_LOW);
		gpio_write(pi, green, PI_LOW);
	}
	pigpio_stop(pi);
}

// process a command, cmd comes from local control channel
void OmniSensr_Health_App::processCommand(const std::string& cmd){
	json msg;
	string appName;
	string action;
	try {
		msg = json::parse(cmd);
	} catch (std::invalid_argument &e) {
		cout << "Failed to parse control message" << endl;
		//sendError("Failed to parse control message", false);
		return;
	}
	if (!msg.value("name", json()).is_null()) {
		appName = msg["name"];
	}
	else {
		cout << "Control message missing name field" << endl;
		//sendError("Control message missing name field", false);
		return;
	}
	if (!msg.value("actionRequested", json()).is_null()) {
		action = msg["actionRequested"];
	}
	else {
		cout << "Control message missing actionRequested field" << endl;
		//sendError("Control message missing actionRequested field", false);
		return;
	}
	if (appName.compare("healthApp") == 0) {
		// start a thread to run getSnapshot
		if (action.compare("snapshot") == 0) {
			if (!msg.value("requestVer", json()).is_null() && msg["requestVer"].is_number()) {
				boost::thread snap_thread(boost::bind(&OmniSensr_Health_App::getSnapshot, this, _1), std::to_string(msg["requestVer"].get<unsigned long long>()));
				snap_thread.join();
			}
			else {
				cout << "Snapshot request missing requestVer field" << endl;
				//sendError("Snapshot request missing requestVer field", false);
			}
		}
		else if (action.compare("osupdate") == 0) {
			if (!msg.value("OS_Ver", json()).is_null() && msg["OS_Ver"].is_string()) {
				updateOSVer(msg["OS_Ver"].get<string>());
			}
			else {
				cout << "OS update request missing OS_Ver field" << endl;
				//sendError("OS update request missing OS_Ver field", false);
			}
		}
		else if (action.compare("qa") == 0) {
			if (!msg.value("QA_Ver", json()).is_null() && msg["QA_Ver"].is_string()) {
				updateQAThreadFunc(msg["QA_Ver"].get<string>());
			}
			else {
				cout << "QA request missing QA_Ver field" << endl;
				updateQAThreadFunc("");
//				sendError("QA request missing QA_Ver field", false);
			}
		}
		else if (action.compare("LED") == 0) {
			if (!msg.value("LED_Set", json()).is_null() && !msg.value("Color", json()).is_null()) {
				if (msg["LED_Set"].is_number_integer() && msg["Color"].is_string())
					setLEDColor(msg["LED_Set"].get<int>(), msg["Color"].get<string>());
				else {
					cout << "Request to change LED message has worng format" << endl;
					//sendError("Request to change LED message has worng format", false);
				}
			}
			else {
				cout << "Request to change LED missing LED_Set or Color" << endl;
				//sendError("Request to change LED missing LED_Set or Color", false);
			}
		}
		else if (action.compare("reboot") == 0) {
			rebootDevice();
		}
	}
	else if (appName.compare("sshd") == 0) {
		string cmd = "sudo systemctl ";
		cmd.append(action);
		cmd.append(" ssh.service");
		FILE *fp = popen(cmd.c_str(), "r");
		if (!fp) {
			cout << "Failed to " << action << " ssh.service" << endl;
			sendError("Failed to " + action + " ssh.service", false);
		}
		else {
			cout << action << " ssh.service" << endl;
			pclose(fp);
		}
	}
	else if (appName.compare("connectorApp") == 0 || appName.compare("mjpg")){
		// start, stop AWS_IoT.service in new thread"Snapshot request missing requestVer field"
		if (action.compare("start") == 0) {
			boost::thread service_thread(boost::bind(&OmniSensr_Health_App::startStopServiceByAppName, this, _1, _2), appName, "true");
			service_thread.join();
		}
		else if (action.compare("stop") == 0) {
			boost::thread service_thread(boost::bind(&OmniSensr_Health_App::startStopServiceByAppName, this, _1, _2), appName, "false");
			service_thread.join();
		}
		else if (action.compare("restart") == 0) {
			boost::thread service_thread(boost::bind(&OmniSensr_Health_App::restartServiceByAppName, this, _1), appName);
			service_thread.join();
		}
	}
	else {
		// start, stop services in new thread
		if (action.compare("start") == 0) {
			boost::thread service_thread(boost::bind(&OmniSensr_Health_App::Service_callback, this, _1, _2), appName, "true");
			service_thread.join();
		}
		else if (action.compare("stop") == 0) {
			boost::thread service_thread(boost::bind(&OmniSensr_Health_App::Service_callback, this, _1, _2), appName, "false");
			service_thread.join();
		}
		else if (action.compare("restart") == 0) {
			boost::thread service_thread(boost::bind(&OmniSensr_Health_App::restartServiceByAppName, this, _1), appName);
			service_thread.join();
		}
	}
}

// Delta callbacks to start threads
void OmniSensr_Health_App::updateQAThreadFunc(const std::string& param) {
	boost::thread qa_thread(boost::bind(&OmniSensr_Health_App::updateQAVer, this, _1), param);
	qa_thread.join();
}

void OmniSensr_Health_App::Role_ThreadFunc(const std::string& param, const std::string& app) {
	boost::thread service_thread(boost::bind(&OmniSensr_Health_App::Service_callback, this, _1, _2), app, param);
	service_thread.join();
}

// Reset or stop cpu collect timer when delta is received
void OmniSensr_Health_App::CpuCollectInterval_callback(const std::string& param) {
	int val;
	try {
		val = boost::lexical_cast<int>(param);
	} catch(boost::bad_lexical_cast &e) {
		cout << "CpuCollect Interval: Bad cast" << endl;
		sendError("CpuCollect Interval: Bad cast", false);
		return;
	}
	mIoT_Cpu_Collect_Interval = val;
	cout << "New CpuCollectIntrvl: " << mIoT_Cpu_Collect_Interval << endl;
	// stop the timer if desired value is not greater than zero
	if (val <= 0) {
		cpuCollectTimer = nullptr;
		cpuStat.clear();
	}
	else {
		// reset timer
		cpuCollectTimer.reset(new CountDownTimer(mIoT_Cpu_Collect_Interval, IntervaledTaskFactory::make_task("CPUCollect")));
	}
}

// Reset or stop temp collect timer when delta is received
void OmniSensr_Health_App::TempCollectInterval_callback(const std::string& param) {
	int val;
	try {
		val = boost::lexical_cast<int>(param);
	} catch(boost::bad_lexical_cast &e) {
		cout << "TempCollect Interval: Bad cast" << endl;
		sendError("TempCollect Interval: Bad cast", false);
		return;
	}
	mIoT_Temp_Collect_Interval = val;
	cout << "New TempCollectIntrvl: " << mIoT_Temp_Collect_Interval << endl;
	// stop the timer if desired value is not greater than zero
	if (val <= 0) {
		tempCollectTimer = nullptr;
		tempStat.clear();
	}
	else {
		// reset timer
		tempCollectTimer.reset(new CountDownTimer(mIoT_Temp_Collect_Interval, IntervaledTaskFactory::make_task("TempCollect")));
	}
}

// setup countdown timer for collecting cpu/memory usage of apps
void OmniSensr_Health_App::AppCollectInterval_callback(const std::string& param, const std::string& type) {
	int val;
	try {
		val = boost::lexical_cast<int>(param);
	} catch(boost::bad_lexical_cast &e) {
		cout << "AppCollect Interval: Bad cast" << endl;
		sendError("AppCollect Interval: Bad cast", false);
		return;
	}
	if (type.compare("CPU") == 0) {
		mIoT_App_Cpu_Collect_Interval = val;
		state_lock.lock();
		appState["AppCpuCollectIntrvl"] = mIoT_App_Cpu_Collect_Interval;
		state_lock.unlock();
		sendState("AppCpuCollectIntrvl");
	}
	else if (type.compare("MEM") == 0) {
		mIoT_App_Mem_Collect_Interval = val;
		state_lock.lock();
		appState["AppMemCollectIntrvl"] = mIoT_App_Mem_Collect_Interval;
		state_lock.unlock();
		sendState("AppMemCollectIntrvl");
	}
	// stop the timer if desired value is not greater than zero
	if (val <= 0) {
		if (type.compare("CPU") == 0) {
			AppCPUTimer = nullptr;
			wifiCPUStat.clear();
			visnCPUStat.clear();
			demoCPUStat.clear();
			traqCPUStat.clear();
		}
		else if (type.compare("MEM") == 0) {
			AppMEMTimer = nullptr;
			wifiMEMStat.clear();
			visnMEMStat.clear();
			demoMEMStat.clear();
			traqMEMStat.clear();
		}
	}
	else {
		// reset timer
		if (type.compare("CPU") == 0)
			AppCPUTimer.reset(new CountDownTimer(mIoT_App_Cpu_Collect_Interval, IntervaledTaskFactory::make_task("AppCPUCollect")));
		else if (type.compare("MEM") == 0)
			AppMEMTimer.reset(new CountDownTimer(mIoT_App_Mem_Collect_Interval, IntervaledTaskFactory::make_task("AppMEMCollect")));
	}
}

// Save QA stat in lastQAState.json file
void OmniSensr_Health_App::saveQAState(bool pass_fail,string os_ver){
	// read our last known QA state and version
	json lastQAState;
	lastQAState["QAPassed"] = pass_fail;
	lastQAState["QAVer"] = trim(os_ver);
	std::ofstream ofs;
	ofs.open("config/OmniSensr_Health_App/lastQAState.json");
	ofs << lastQAState;
	ofs.close();
}

// Make a system level call to the _test_WiFi_n_Bluetooth.sh script
// this will update our current QA version to the current OS version
void OmniSensr_Health_App::updateQAVer(const std::string& param)
{
	// string type from json will add quotes when ask for value
//	string desiredVer = param.substr(1, param.length() - 2);
	string desiredVer = param;
	// quit function if QAVer equals desired and QA_OK is true
	cout << "QAVer: " << mIoT_QAVer << " desired: " << desiredVer << endl;
	cout << "QA_OK: " << mIoT_QA_OK << endl;
	if (mIoT_QAVer.compare(desiredVer) == 0 && mIoT_QA_OK) {
		ostringstream os;
		os << "{\"name\":\"healthApp\",\"actionRequested\":\"qa\",";
		os << "\"QA_Ver\":\"" << mIoT_QAVer << "\", \"QA_OK\":" << ((mIoT_QA_OK) ? "true" : "false");
		os << "}";
		sendData(os.str(), devSubtopic + "/qa");
		auto it = ignoreVariables.find("QA_Ver");
		if (it != ignoreVariables.end())
			ignoreVariables.erase(it);
		return;
	}
	// add QAVer to ignoreVariables to prevent local delta firing this function repeatedly
	ignoreVariables.insert("QA_Ver");
	string cmd;
	cmd.clear();
	cmd = "cd /home/omniadmin/QA_test;./_test_WiFi_n_Bluetooth.sh | grep \"QA Testing for\"";
	if(DEBUG_HEALTHAPP)
		printf("[HealthApp] Launching QA Test...\n");
	FILE *fp = popen(cmd.c_str(), "r");
	char QA_Result[128]="";
	string result_lookup;
	result_lookup.clear();
	if(fp == NULL) {
		perror ("Error calling QA Test");
		sendError("Failed to call QA_Test", false);
		return;
	}
	else {
		if(fgets(QA_Result, 128, fp) != NULL)
			result_lookup.append(QA_Result);
		if(DEBUG_HEALTHAPP)
			printf("[HealthApp] Results_lookup : %s\n",result_lookup.c_str());
		std::size_t whchResult = result_lookup.find("PASSED!!");
		if(DEBUG_HEALTHAPP)
			printf("[HealthApp] QA Test Passed = %s\n",(whchResult != std::string::npos) ? "true" : "false");
		mIoT_QA_OK =(whchResult != std::string::npos);
		pclose(fp);

	}
	if(DEBUG_HEALTHAPP)
		printf("[HealthApp] Update QA Version to current OS Version...\n");
	mIoT_QAVer = getOSVer();

	// save QA state
	saveQAState(mIoT_QA_OK, mIoT_QAVer);

	// remove from ignore
	auto it = ignoreVariables.find("QA_Ver");
	if (it != ignoreVariables.end())
		ignoreVariables.erase(it);
	ostringstream os;
	os << "{\"name\":\"healthApp\",\"actionRequested\":\"qa\",";
	os << "\"QA_Ver\":\"" << mIoT_QAVer << "\", \"QA_OK\":" << ((mIoT_QA_OK) ? "true" : "false");
	os << "}";
	sendData(os.str(), devSubtopic + "/qa");
}

// Setup cron to start delay_sync.sh at 2am.
// The delayed time is an offset calculated for each device.
void OmniSensr_Health_App::updateOSVer(const std::string& param)
{
	mIoT_OSVer = getOSVer();
	// string type from json will add quotes when ask for value
//	string desiredVer = param.substr(1, param.length() - 2);
	string desiredVer = param;
	if (mIoT_OSVer.compare(desiredVer) == 0) {
		// Do QA if QAVer is not the same as OSVer
		updateQAThreadFunc(param);
		sendData("{\"name\":\"healthApp\", \"actionRequested\":\"osupdate\", \"OS_Ver\": \"" + param + "\", \"Schdueled\":\"none\"}", devSubtopic + "/osupdate");
		auto it = ignoreVariables.find("OS_Ver");
		if (it != ignoreVariables.end())
			ignoreVariables.erase(it);
		return;

	}
	// setup sync in cron
	FILE *fp = popen("cd /home/omniadmin/omnisensr-runners; ./kickoff.sh cron", "r");
	if (!fp) {
		cout << "Failed to add sync in cron" << endl;
		sendError("Failed to add sync in cron", false);
		return;
	}
	else
		pclose(fp);
	// add OSVer to ignoreVariables to prevent local delta firing this function repeatedly
	ignoreVariables.insert("OS_Ver");
	if(DEBUG_HEALTHAPP)
		printf("[HealthApp] current OSVer %s, updateOSVer: %s\n", mIoT_OSVer.c_str(), desiredVer.c_str());

	// set time offset for cron
	boost::hash<string> hash_fn;
	size_t hash = hash_fn(getStoreLocation());
	int offset = (hash % 10) * 21 + (getDeviceName().back() - '0') * 2;
	std::ofstream ofs;
	ofs.open("/home/omniadmin/omnisensr-runners/sleeptime.txt");
	ofs << std::to_string(offset) << "m";
	ofs.close();

	// report scheduled update time
	boost::posix_time::ptime now = boost::posix_time::second_clock::local_time();
	boost::posix_time::time_duration time_diff = boost::posix_time::second_clock::universal_time() - now;
	// time difference between local time zone and UTC
	string time_s = boost::posix_time::to_iso_string(now);
	// set time to 2 am this day
	time_s.erase(9);
	time_s.append("020000");
	boost::posix_time::ptime twoAM = boost::posix_time::from_iso_string(time_s);
	boost::posix_time::ptime scheduled = twoAM + boost::posix_time::minutes(offset) + time_diff;
	string scheduled_s;
	// if current time has passed 2 am, set the schedule to tomorrow
	if (now > twoAM) {
		scheduled += boost::posix_time::hours(24);
	}
	scheduled_s = boost::posix_time::to_iso_string(scheduled);
	sendData("{\"name\":\"healthApp\",\"actionRequested\":\"osupdate\", \"OS_Ver\":\"" + param + "\", \"Schdueled\":\"" + scheduled_s + "\"}", devSubtopic + "/osupdate");

	//	// -- for testing: set cron to run the next minute
	//	time_t t = time(0);
	//	struct tm *now = localtime(&t);
	//	if (now->tm_min != 59) {
	//		now->tm_min += 1;
	//	}
	//	else if (now->tm_hour != 23) {
	//		now->tm_hour += 1;
	//	}
	//	else {
	//		now->tm_hour = 0;
	//		now->tm_min = 0;
	//	}
	//	cout << "set cron update time to " << now->tm_hour << ":" << now->tm_min << endl;
	//	string cmd = "printf \"" + std::to_string(now->tm_min) + " " + std::to_string(now->tm_hour) + " * * * omniadmin cd ~/omnisensr-runners; chmod +x *.sh; ./delay_sync.sh\\n#\\n\" | sudo tee /etc/cron.d/sync";
	//	FILE *fp = popen(cmd.c_str(), "r");
	//	if (fp == NULL) {
	//		cout << "Failed to update sync cron" << endl;
	//	}
	//	pclose(fp);

}

// Callback to process a delta for services
void OmniSensr_Health_App::Service_callback(const std::string& appName, const std::string& param){
	CountDownTimerPtr* cdtPtr;
	string roleName;
	string heartbeatTask;
	string servicename;
	bool* role;
	if (appName.compare("wifiRSS") == 0) {
		cdtPtr = &wifiTimer;
		roleName = "WiFi_Role";
		heartbeatTask = "WifiAppHeartBeat";
		role = &mIoT_WiFi_Role;
		servicename = "OmniSensr_Service_Wifi";
	}
	else if (appName.compare("demoApp") == 0) {
		cdtPtr = &demoTimer;
		roleName = "Face_Role";
		heartbeatTask = "DemoAppHeartBeat";
		role = &mIoT_Face_Role;
		servicename = "OmniSensr_Service_Demogrph";
	}
	else if (appName.compare("visionApp") == 0) {
		cdtPtr = &visnTimer;
		roleName = "Visn_Role";
		heartbeatTask = "VisnAppHeartBeat";
		role = &mIoT_Visn_Role;
		servicename = "OmniSensr_Service_Vision";
	}
	else if (appName.compare("wifiTrack") == 0) {
		cdtPtr = &traqTimer;
		roleName = "Traq_Role";
		heartbeatTask = "TraqAppHeartBeat";
		role = &mIoT_Traq_Role;
		servicename = "OmniSensr_Service_WifiTrack";
	}
	else {
		cout << "Unrecognized app name" << endl;
		return;
	}
	// if the desired value is "false", set countdown timer to nullptr
	if (param[0] == 'f') {
		if (cdtPtr && *cdtPtr != nullptr)
			*cdtPtr = nullptr;
	}
	else {
		// if countdown timer is not initiated, reset it
		if (cdtPtr && *cdtPtr == nullptr)
			cdtPtr->reset(new CountDownTimer(heartbeat_timeout, IntervaledTaskFactory::make_task(heartbeatTask)));
	}
	cout << "Try to set " << roleName << " to " << param << endl;

	*role = getServiceRunState(servicename);
	cout << "Currently " << roleName << " is " << *role << endl;
	// ignore further delta when trying to set role
	if (*role != (param[0] == 't' ? true : false)) {
		ignoreVariables.insert(roleName);
		// try to start or stop service
		startStopServiceByName(roleName, param);
		// wait for 25 secs and check state
		sleep(25);
		*role = getServiceRunState(servicename);
	}

	// remove from ignore set
	auto it = ignoreVariables.find(roleName);
	if(it != ignoreVariables.end())
		ignoreVariables.erase(it);
}

void OmniSensr_Health_App::pokeWatchDog() {
	int pi = pigpio_start(NULL, NULL);
	if (pi < 0) {
		cout << "[pokeWatchDog] Failed to connect to pigpiod" << endl;
		sendError("[pokeWatchDog] Failed to connect to pigpiod", false);
		return;
	}
	set_mode(pi, watchdog_pin, PI_OUTPUT);
	gpio_write(pi, watchdog_pin, PI_LOW);
	sleep(1);
	gpio_write(pi, watchdog_pin, PI_HIGH);
	sleep(1);
	gpio_write(pi, watchdog_pin, PI_LOW);
	pigpio_stop(pi);
}

// main processing loop code goes here
void OmniSensr_Health_App::start(){
	// send the full status
	sendFullState();
	// stop sync in cron
	FILE *fp = popen("sudo rm /etc/cron.d/sync;sudo rm /etc/cron.d/cron.update", "r");
	if (!fp) {
		cout << "Failed to remove sync in cron.d" << endl;
		sendError("Failed to remove sync in cron.d", false);
	}
	pclose(fp);
	// get device revision number and write model version to /usr/local/www/model.txt
	fp = popen("cat /proc/cpuinfo | grep ^Revision | awk \'{print $3}\'", "r");
	if (!fp) {
		cout << "Failed to get device revision number" << endl;
		sendError("Failed to get device revision number", false);
	}
	else {
		char buff[50] = "";
		if (fgets(buff, 50, fp)) {
			string model = "";
			string temp(buff);
			if (temp.compare("a020a0") == 0) {
				cout << "This is a compute module" << endl;
				model = "OmniSensr-v2";
				OmniSensrVer = 2;
			}
			else {
				cout << "This is a raspberry pi" << endl;
				model = "OmniSensr-v1";
				OmniSensrVer = 1;
			}
			pclose(fp);
			string cmd = "sudo echo " + model + " > /usr/local/www/model.txt";
			fp = popen(cmd.c_str(), "r");
		}
		pclose(fp);
	}
	// turn all led off
	if (OmniSensrVer == 2) {
		setLEDColor(0, "off");
		setLEDColor(1, "off");
		setLEDColor(2, "off");
	}
	// set a mqtt receiver to get heartbeat from apps
	MQTTReceiver mqttRecv;
	boost::function<void (const struct mosquitto_message *message)> heartbeat_cb = boost::bind(&OmniSensr_Health_App::localHeartbeat_callback, this, _1);
	boost::function<void (const struct mosquitto_message *message)> appcrash_cb = boost::bind(&OmniSensr_Health_App::localAppCrash_callback, this, _1);
	boost::function<void (const struct mosquitto_message *message)> metric_cb = boost::bind(&OmniSensr_Health_App::localMetric_callback, this, _1);
	mqttRecv.subscribe("heartbeat/+", heartbeat_cb, 1);
	mqttRecv.subscribe("appCrash/+", appcrash_cb, 1);
	mqttRecv.subscribe("metric/+", metric_cb, 1);
	mqttRecv.start();

	// set countdown timers for apps
	connTimer.reset(new CountDownTimer(heartbeat_timeout, IntervaledTaskFactory::make_task("ConnAppHeartBeat")));
	if (mIoT_WiFi_Role) wifiTimer.reset(new CountDownTimer(heartbeat_timeout, IntervaledTaskFactory::make_task("WifiAppHeartBeat")));
	if (mIoT_Face_Role) demoTimer.reset(new CountDownTimer(heartbeat_timeout, IntervaledTaskFactory::make_task("DemoAppHeartBeat")));
	if (mIoT_Visn_Role) visnTimer.reset(new CountDownTimer(heartbeat_timeout, IntervaledTaskFactory::make_task("VisnAppHeartBeat")));
	if (mIoT_Traq_Role) traqTimer.reset(new CountDownTimer(heartbeat_timeout, IntervaledTaskFactory::make_task("TraqAppHeartBeat")));

	// set countdown timers for metrics
	if (mIoT_Cpu_Collect_Interval != -1)
		cpuCollectTimer.reset(new CountDownTimer(mIoT_Cpu_Collect_Interval, IntervaledTaskFactory::make_task("CPUCollect")));
	if (mIoT_Temp_Collect_Interval != -1)
		tempCollectTimer.reset(new CountDownTimer(mIoT_Temp_Collect_Interval, IntervaledTaskFactory::make_task("TempCollect")));

	// set to negative value so heartbeat will be send right away
	double prevHeartbeat = -mIoT_Heartbeat_Interval;
	double prevCpu, prevTemp, prevAppCPU, prevAppMEM, prevWatchDog;
	prevCpu = prevTemp = prevAppCPU = prevAppMEM = gCurrentTime();
	prevWatchDog = -120;
	double curTime;
	while (1) {
		curTime = gCurrentTime();
		// send heartbeat through data channel
		if (curTime - prevHeartbeat >= mIoT_Heartbeat_Interval) {
			prevHeartbeat = curTime;
			string tsJson = "{\"heartbeat\":\"" + IOTUtility::getTimestamp() + "\", \"Interval\": " + std::to_string((uint)mIoT_Heartbeat_Interval) +"}";
			sendData(tsJson, devSubtopic + "/heartbeat");
		}
		// send device cpu usage through data channel
		if (mIoT_Cpu_Report_Interval > 0 && curTime - prevCpu >= mIoT_Cpu_Report_Interval && cpuStat.size() > 0) {
			int sum = 0;
			float avg;
			ostringstream os;
			os << "{\"CPU\": \"";
			prevCpu = curTime;
			cpuStat_lock.lock();
			for (uint i = 0; i < cpuStat.size(); i++) {
				sum += cpuStat[i];
			}
			avg = (float)sum / cpuStat.size();
			cpuStat.clear();
			cpuStat_lock.unlock();
			os << avg << "%\"}";
			if (avg > mIoT_Cpu_Threshold)
				sendData(os.str(), devSubtopic + "/metric");
			mIoT_Cpu_Load = round(avg * 10000) / 10000.0;
		}

		// send device temperature through data channel
		if (mIoT_Temp_Report_Interval > 0 && curTime - prevTemp >= mIoT_Temp_Report_Interval && tempStat.size() > 0) {
			float sum = 0;
			float avg;
			ostringstream os;
			os << "{\"Temp\": \"";
			prevTemp = curTime;
			tempStat_lock.lock();
			for (uint i = 0; i < tempStat.size(); i++) {
				sum += tempStat[i];
			}
			avg = (float)sum / tempStat.size();
			tempStat.clear();
			tempStat_lock.unlock();
			os << avg << "C\"}";
			if (avg > mIoT_Temp_Threshold)
				sendData(os.str(), devSubtopic + "/metric");
			mIoT_Cpu_Temp = round(avg * 10000) / 10000.0;
		}
		// send device cpu usage per app
		if (mIoT_App_Cpu_Report_Interval > 0 && curTime - prevAppCPU >= mIoT_App_Cpu_Report_Interval) {
			float sum ;
			float wifi_avg, visn_avg, demo_avg, traq_avg;
			prevAppCPU = curTime;
			json info;
			ostringstream os;
			cpuStat_lock.lock();
			if (wifiCPUStat.size() > 0) {
				sum = 0;
				for (uint i = 0; i < wifiCPUStat.size(); i++) {
					sum += wifiCPUStat[i];
				}
				wifi_avg = (float)sum / wifiCPUStat.size();
				wifiCPUStat.clear();
				os.str("");
				os.clear();
				os << wifi_avg << "%";
				info["wifiRSS"] = os.str();
			}
			if (visnCPUStat.size() > 0) {
				sum = 0;
				for (uint i = 0; i < visnCPUStat.size(); i++) {
					sum += visnCPUStat[i];
				}
				visn_avg = (float)sum / visnCPUStat.size();
				visnCPUStat.clear();
				os.str("");
				os.clear();
				os << visn_avg << "%";
				info["visionApp"] = os.str();
			}
			if (demoCPUStat.size() > 0) {
				sum = 0;
				for (uint i = 0; i < demoCPUStat.size(); i++) {
					sum += demoCPUStat[i];
				}
				demo_avg = (float)sum / demoCPUStat.size();
				demoCPUStat.clear();
				os.str("");
				os.clear();
				os << demo_avg << "%";
				info["demoApp"] = os.str();
			}
			if (traqCPUStat.size() > 0) {
				sum = 0;
				for (uint i = 0; i < traqCPUStat.size(); i++) {
					sum += traqCPUStat[i];
				}
				traq_avg = (float)sum / traqCPUStat.size();
				traqCPUStat.clear();
				os.str("");
				os.clear();
				os << traq_avg << "%";
				info["wifiTrack"] = os.str();
			}
			cpuStat_lock.unlock();
			if (info.size() > 0)
				sendData(info.dump(), devSubtopic + "/metric/cpuInfo");
		}
		// send device memory usage per app
		if (mIoT_App_Mem_Report_Interval > 0 && curTime - prevAppMEM >= mIoT_App_Mem_Report_Interval) {
			float sum ;
			float wifi_avg, visn_avg, demo_avg, traq_avg;
			prevAppMEM = curTime;
			json info;
			ostringstream os;
			cpuStat_lock.lock();
			if (wifiMEMStat.size() > 0) {
				sum = 0;
				for (uint i = 0; i < wifiMEMStat.size(); i++) {
					sum += wifiMEMStat[i];
				}
				wifi_avg = (float)sum / wifiMEMStat.size();
				wifiMEMStat.clear();
				os.str("");
				os.clear();
				os << wifi_avg << "%";
				info["wifiRSS"] = os.str();
			}
			if (visnMEMStat.size() > 0) {
				sum = 0;
				for (uint i = 0; i < visnMEMStat.size(); i++) {
					sum += visnMEMStat[i];
				}
				visn_avg = (float)sum / visnMEMStat.size();
				visnMEMStat.clear();
				os.str("");
				os.clear();
				os << visn_avg << "%";
				info["visionApp"] = os.str();
			}
			if (demoMEMStat.size() > 0) {
				sum = 0;
				for (uint i = 0; i < demoMEMStat.size(); i++) {
					sum += demoMEMStat[i];
				}
				demo_avg = (float)sum / demoMEMStat.size();
				demoMEMStat.clear();
				os.str("");
				os.clear();
				os << demo_avg << "%";
				info["demoApp"] = os.str();
			}
			if (traqMEMStat.size() > 0) {
				sum = 0;
				for (uint i = 0; i < traqMEMStat.size(); i++) {
					sum += traqMEMStat[i];
				}
				traq_avg = (float)sum / traqMEMStat.size();
				traqMEMStat.clear();
				os.str("");
				os.clear();
				os << traq_avg << "%";
				info["wifiTrack"] = os.str();
			}
			cpuStat_lock.unlock();
			if (info.size() > 0)
				sendData(info.dump(), devSubtopic + "/metric/memInfo");
		}

		// poke watchdog
		if (OmniSensrVer == 2 && curTime - prevWatchDog >= watchdog_interval) {
			boost::thread watchdog(boost::bind(&OmniSensr_Health_App::pokeWatchDog, this));
			watchdog.join();
			prevWatchDog = curTime;
		}
		updateAppState();
		sleep(5);
	}

	// save my state so I can restart from the last known good state
	SaveSetting();
}



// Load variables from config file and register variables, callbacks in thing shadow
unsigned int OmniSensr_Health_App::initConfig(ParseConfigTxt& parser){
	unsigned int n_params = 0;

	n_params += parser.getValue("PERIODIC_DATA_BUFFER_PROCESSING_INTERVAL", mData_Buffer_Interval);
	n_params += parser.getValue("VERSION", mSoftware_ver);
	n_params += parser.getValue("HEARTBEAT_INTERVAL", mIoT_Heartbeat_Interval);
	n_params += parser.getValue("CPU_COLLECT_INTERVAL", mIoT_Cpu_Collect_Interval);
	n_params += parser.getValue("CPU_REPORT_INTERVAL", mIoT_Cpu_Report_Interval);
	n_params += parser.getValue("TEMP_COLLECT_INTERVAL", mIoT_Temp_Collect_Interval);
	n_params += parser.getValue("TEMP_REPORT_INTERVAL", mIoT_Temp_Report_Interval);
	n_params += parser.getValue("CPU_THRESHOLD", mIoT_Cpu_Threshold);
	n_params += parser.getValue("TEMP_THRESHOLD", mIoT_Temp_Threshold);
	n_params += parser.getValue("APP_CPU_COLLECT_INTERVAL", mIoT_App_Cpu_Collect_Interval);
	n_params += parser.getValue("APP_CPU_REPORT_INTERVAL", mIoT_App_Cpu_Report_Interval);
	n_params += parser.getValue("APP_MEM_COLLECT_INTERVAL", mIoT_App_Mem_Collect_Interval);
	n_params += parser.getValue("APP_MEM_REPORT_INTERVAL", mIoT_App_Mem_Report_Interval);
	// This one should be captured in OmniSensrApp
	parser.getValue("PERIODIC_STATE_REPORTING_INTERVAL", mIoT_State_Report_Interval);

	registerDeltaCallback("WiFi_Role", boost::bind(&OmniSensr_Health_App::Role_ThreadFunc, this, _1, "wifiRSS"));
	registerDeltaCallback("Face_Role", boost::bind(&OmniSensr_Health_App::Role_ThreadFunc, this, _1, "demoApp"));
	registerDeltaCallback("Visn_Role", boost::bind(&OmniSensr_Health_App::Role_ThreadFunc, this, _1, "visionApp"));
	registerDeltaCallback("Traq_Role", boost::bind(&OmniSensr_Health_App::Role_ThreadFunc, this, _1, "wifiTrack"));

	registerDeltaCallback("QA_Ver", boost::bind(&OmniSensr_Health_App::updateQAThreadFunc, this, _1));
	registerDeltaCallback("OS_Ver", boost::bind(&OmniSensr_Health_App::updateOSVer, this, _1));

	registerVariable("HeartbeatIntrvl", mIoT_Heartbeat_Interval);
	registerVariable("QA_OK", mIoT_QA_OK);
	registerVariable("CpuReportIntrvl", mIoT_Cpu_Report_Interval);
	registerVariable("TempReportIntrvl", mIoT_Temp_Report_Interval);
	registerVariable("AppCpuReportIntrvl", mIoT_App_Cpu_Report_Interval);
	registerVariable("AppMemReportIntrvl", mIoT_App_Mem_Report_Interval);

	registerDeltaCallback("CpuCollectIntrvl", boost::bind(&OmniSensr_Health_App::CpuCollectInterval_callback, this, _1));
	registerDeltaCallback("TempCollectIntrvl", boost::bind(&OmniSensr_Health_App::TempCollectInterval_callback, this, _1));

	registerDeltaCallback("AppCpuCollectIntrvl", boost::bind(&OmniSensr_Health_App::AppCollectInterval_callback, this, _1, "CPU"));
	registerDeltaCallback("AppMemCollectIntrvl", boost::bind(&OmniSensr_Health_App::AppCollectInterval_callback, this, _1, "MEM"));
	// we can't really change this because periodic state report is done in OmniSensrApp
	registerVariable("ReportIntrvl", mIoT_State_Report_Interval);

	registerVariable("Cpu_Load", mIoT_Cpu_Load);
	registerVariable("Cpu_Temp", mIoT_Cpu_Temp);
	registerVariable("Cpu_Threshold", mIoT_Cpu_Threshold);
	registerVariable("Temp_Threshold", mIoT_Temp_Threshold);

	return n_params;
}

void OmniSensr_Health_App::saveState(std::ostream& os) const {

}

// Update the states, remove variables that are in ignore set
void OmniSensr_Health_App::updateAppState() {
	state_lock.lock();
	appState["WiFi_Role"] = mIoT_WiFi_Role;
	appState["Visn_Role"] = mIoT_Visn_Role;
	appState["Traq_Role"] = mIoT_Traq_Role;
	appState["Face_Role"] = mIoT_Face_Role;
	appState["OS_Ver"] = mIoT_OSVer;
	appState["QA_OK"] = mIoT_QA_OK;
	appState["QA_Ver"] = mIoT_QAVer;
	appState["ReportIntrvl"] = mIoT_State_Report_Interval;
	appState["HeartbeatIntrvl"] = mIoT_Heartbeat_Interval;
	appState["CpuCollectIntrvl"] = mIoT_Cpu_Collect_Interval;
	appState["CpuReportIntrvl"] = mIoT_Cpu_Report_Interval;
	appState["TempCollectIntrvl"] = mIoT_Temp_Collect_Interval;
	appState["TempReportIntrvl"] = mIoT_Temp_Report_Interval;
	appState["AppCpuCollectIntrvl"] = mIoT_App_Cpu_Collect_Interval;
	appState["AppMemCollectIntrvl"] = mIoT_App_Mem_Collect_Interval;
	appState["AppCpuReportIntrvl"] = mIoT_App_Cpu_Report_Interval;
	appState["AppMemReportIntrvl"] = mIoT_App_Mem_Report_Interval;
	appState["Cpu_Load"] = mIoT_Cpu_Load;
	appState["Cpu_Temp"] = mIoT_Cpu_Temp;
	appState["Cpu_Threshold"] = mIoT_Cpu_Threshold;
	appState["Temp_Threshold"] = mIoT_Temp_Threshold;
	for (auto it = ignoreVariables.begin(); it != ignoreVariables.end(); it++) {
		appState.erase(*it);
	}
	state_lock.unlock();
}

int main() {

	OmniSensr_Health_App sampleApp("config/OmniSensr_Health_App/OmniSensr_Health_App_config.txt");
	sampleApp.Run();

	// will never get here
	return 0;
}

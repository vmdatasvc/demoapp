/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
#include "IntervaledTaskFactory.hpp"
#include "IntervaledTask.hpp"
#include <string>
#include <fcntl.h>
#include <unistd.h>
#include <iostream>
#include <sstream>
#include "modules/iot/MQTTClient.hpp"
#include "modules/iot/IOTUtility.hpp"

using namespace std;

int GetCPULoad() {
	FILE *fp = popen("top -bn2|grep \"%Cpu(s)\"|tail -1|awk \'{print $8}\'", "r");
	char buff[50] = "";
	float load;
	if (fp) {
		if (fgets(buff, 50, fp)) {
			sscanf(buff, "%f", &load);
		}
		load = 100.0 - load;
		pclose(fp);
	}
	else {
		cout << "Failed to get cpu info" << endl;
	}
	return (int)load;
}

float GetTemp() {
	FILE *fp = popen("/opt/vc/bin/vcgencmd measure_temp", "r");
	char buff[50] = "";
	float temp;
	if (fp) {
		if (fgets(buff, 50, fp)) {
			sscanf(buff, "temp=%f\'C", &temp);
		}
		pclose(fp);
	}
	else {
		cout << "Failed to get temperature info" << endl;
	}
	return temp;
}

string GetAppCPU() {
	// save "ps" result if PS and get balues respectively
	string cmd = "PS=$(ps -elf); ";
	// get wifiRSS pid
	cmd.append("WIFI=$(echo \"$PS\" | awk \'$15 ~ /WiFi_PacketProcessor/ {print $4}\'); if [ -z \"$WIFI\" ]; then WIFI=-1; fi; ");
	// get visionApp pid
	cmd.append("VISN=$(echo \"$PS\" | awk \'$15 ~ /OmniSensr_Vision_Tracker/ {print $4}\'); if [ -z \"$VISN\" ]; then VISN=-1; fi; ");
	// get demoApp pid
	cmd.append("DEMO=$(echo \"$PS\" | awk \'$15 ~ /OmniSensr_DMG_Recognizer/ {print $4}\'); if [ -z \"$DEMO\" ]; then DEMO=-1; fi; ");
	// get wifiTrack pid
	cmd.append("TRAQ=$(echo \"$PS\" | awk \'$15 ~ /WiFi_Shopper_Tracker/ {print $4}\'); if [ -z \"$TRAQ\" ]; then TRAQ=-1; fi; ");
	// print out pids
	cmd.append("echo \"$WIFI\" \"$VISN\" \"$DEMO\" \"$TRAQ\"");
	int wifiPid, visnPid, traqPid, demoPid;
	char buff[256] = "";
	FILE *fp = popen(cmd.c_str(), "r");
	if (!fp) {
		cout << "Failed to get app pid" << endl;
		return "";
	}
	else {
		if (fgets(buff, 256, fp)) {
			sscanf(buff, "%d %d %d %d", &wifiPid, &visnPid, &demoPid, &traqPid);
		}
		pclose(fp);
	}
	// save "top" result in TOP and get values respectively
	cmd = "TOP=$(top -bn2); ";
	// get wifiRSS load
	if (wifiPid != -1)
		cmd.append("WIFI=$(echo \"$TOP\" | awk \'$1 == " + std::to_string(wifiPid) + " {print $9}\' | tail -1); ");
	else
		cmd.append("WIFI=\"-1\"; ");
	// get visionApp load
	if (visnPid != -1)
		cmd.append("VISN=$(echo \"$TOP\" | awk \'$1 == " + std::to_string(visnPid) + " {print $9}\' | tail -1); ");
	else
		cmd.append("VISN=\"-1\"; ");
	// get demoApp load
	if (demoPid != -1)
		cmd.append("DEMO=$(echo \"$TOP\" | awk \'$1 == " + std::to_string(demoPid) + " {print $9}\' | tail -1); ");
	else
		cmd.append("DEMO=\"-1\"; ");
	// get wifiTrack load
	if (traqPid != -1)
		cmd.append("TRAQ=$(echo \"$TOP\" | awk \'$1 == " + std::to_string(traqPid) + " {print $9}\' | tail -1); ");
	else
		cmd.append("TRAQ=\"-1\"; ");
	// print out values
	cmd.append("echo \"$WIFI\" \"$VISN\" \"$DEMO\" \"$TRAQ\"");
	fp = popen(cmd.c_str(), "r");
	if (!fp) {
		cout << "Failed to get app cpu load" << endl;
	}
	else {
		fgets(buff, 256, fp);
		pclose(fp);
	}
	return string(buff);
}

string GetAppMEM() {
	// save "ps" result if PS and get balues respectively
	string cmd = "PS=$(ps -elf); ";
	// get wifiRSS pid
	cmd.append("WIFI=$(echo \"$PS\" | awk \'$15 ~ /WiFi_PacketProcessor/ {print $4}\'); if [ -z \"$WIFI\" ]; then WIFI=-1; fi; ");
	// get visionApp pid
	cmd.append("VISN=$(echo \"$PS\" | awk \'$15 ~ /OmniSensr_Vision_Tracker/ {print $4}\'); if [ -z \"$VISN\" ]; then VISN=-1; fi; ");
	// get demoApp pid
	cmd.append("DEMO=$(echo \"$PS\" | awk \'$15 ~ /OmniSensr_DMG_Recognizer/ {print $4}\'); if [ -z \"$DEMO\" ]; then DEMO=-1; fi; ");
	// get wifiTrack pid
	cmd.append("TRAQ=$(echo \"$PS\" | awk \'$15 ~ /WiFi_Shopper_Tracker/ {print $4}\'); if [ -z \"$TRAQ\" ]; then TRAQ=-1; fi; ");
	// print out pids
	cmd.append("echo \"$WIFI\" \"$VISN\" \"$DEMO\" \"$TRAQ\"");
	int wifiPid, visnPid, traqPid, demoPid;
	char buff[256] = "";
	FILE *fp = popen(cmd.c_str(), "r");
	if (!fp) {
		cout << "Failed to get app pid" << endl;
		return "";
	}
	else {
		if (fgets(buff, 256, fp)) {
			sscanf(buff, "%d %d %d %d", &wifiPid, &visnPid, &demoPid, &traqPid);
		}
		pclose(fp);
	}
	// save "top" result in TOP and get values respectively
	cmd = "TOP=$(top -bn2); ";
	// get wifiRSS load
	if (wifiPid != -1)
		cmd.append("WIFI=$(echo \"$TOP\" | awk \'$1 == " + std::to_string(wifiPid) + " {print $10}\' | tail -1); ");
	else
		cmd.append("WIFI=\"-1\"; ");
	// get visionApp load
	if (visnPid != -1)
		cmd.append("VISN=$(echo \"$TOP\" | awk \'$1 == " + std::to_string(visnPid) + " {print $10}\' | tail -1); ");
	else
		cmd.append("VISN=\"-1\"; ");
	// get demoApp load
	if (demoPid != -1)
		cmd.append("DEMO=$(echo \"$TOP\" | awk \'$1 == " + std::to_string(demoPid) + " {print $10}\' | tail -1); ");
	else
		cmd.append("DEMO=\"-1\"; ");
	// get wifiTrack load
	if (traqPid != -1)
		cmd.append("TRAQ=$(echo \"$TOP\" | awk \'$1 == " + std::to_string(traqPid) + " {print $10}\' | tail -1); ");
	else
		cmd.append("TRAQ=\"-1\"; ");
	// print out values
	cmd.append("echo \"$WIFI\" \"$VISN\" \"$DEMO\" \"$TRAQ\"");
	fp = popen(cmd.c_str(), "r");
	if (!fp) {
		cout << "Failed to get app cpu load" << endl;
	}
	else {
		fgets(buff, 256, fp);
		pclose(fp);
	}
	return string(buff);
}

class IntervaledTaskAppCPUCollect : public IntervaledTask{
public:
	// Reference: https://www.raspberrypi.org/forums/viewtopic.php?t=64835&p=479657
	void DoTask()
	{
		mqttSender.publish("metric/cpuInfo", GetAppCPU());
		//std::cout << "Task Interval: " << mTaskInterval << "Seconds. cpu=" << GetCPULoad() << "%" << std::endl;
	}
};

class IntervaledTaskAppMEMCollect : public IntervaledTask{
public:
	// Reference: https://www.raspberrypi.org/forums/viewtopic.php?t=64835&p=479657
	void DoTask()
	{
		mqttSender.publish("metric/memInfo", GetAppMEM());
		//std::cout << "Task Interval: " << mTaskInterval << "Seconds. cpu=" << GetCPULoad() << "%" << std::endl;
	}
};

class IntervaledTaskCPUCollect : public IntervaledTask{
public:
	// Reference: https://www.raspberrypi.org/forums/viewtopic.php?t=64835&p=479657
	void DoTask()
	{
		mqttSender.publish("metric/cpuCollect", std::to_string(GetCPULoad()));
		//std::cout << "Task Interval: " << mTaskInterval << "Seconds. cpu=" << GetCPULoad() << "%" << std::endl;
	}
};

class IntervaledTaskCPUReport : public IntervaledTask{
public:
	void DoTask()
	{
		std::ostringstream reportStrStrm;
		reportStrStrm << "CPU: " << GetCPULoad() << "%";
		cout << "[data channel] " <<  reportStrStrm.str() << endl;

//		std::cout << "Task Interval: " << mTaskInterval << "Seconds.  Reporting CPU now" << std::endl;
//		int i;
//		i=system ("mosquitto_pub -q 1 -d -h localhost -p 1883 -I test_temp_2 -t health -m \"Reporting CPU now\"");
	}
};

class IntervaledTaskTempCollect : public IntervaledTask{
public:
	void DoTask()
	{
		//std::cout << "Task Interval: " << mTaskInterval  << "Seconds.  " << system("/opt/vc/bin/vcgencmd measure_temp");
		mqttSender.publish("metric/tempCollect", std::to_string(GetTemp()));
	}
};

class IntervaledTaskTempReport : public IntervaledTask{
public:
	void DoTask()
	{
		std::ostringstream reportStrStrm;
		reportStrStrm << "Temp: " <<  system("/opt/vc/bin/vcgencmd measure_temp");
		//pIoTGateway->SendData(reportStrStrm.str());
		cout << "[data channel] " <<  reportStrStrm.str() << endl;
//		std::cout << "Task Interval: " << mTaskInterval << "Seconds. Reporting Temperature now" << std::endl;
//		int i;
//		i=system ("mosquitto_pub -q 1 -d -h localhost -p 1883 -I test_temp_2 -t health -m \"Reporting Temperature now\"");
	}
};

class IntervaledTaskHeartBeat : public IntervaledTask{
public:
	IntervaledTaskHeartBeat (std::string app, int numRestart) : appName(app) {
		numToStop = numRestart;
		restartCount = 0;
	}
	std::string appName;
	void DoTask()
	{
		std::cout << "Task Interval: " << mTaskInterval << " Seconds. " << appName << " Missing Heartbeat!" << std::endl;
		if (restartCount < numToStop) {
			string topic = "appCrash/" + appName;
			string payload = "{\"date\":\"" + IOTUtility::getTimestamp() + "\", \"name\":\"" + appName + "\", \"actionRequested\":\"restart\", \"info\":\"App failed to heartbeat\"}";
			if (mqttSender.publish(topic, payload))
				cout << "publish is not successful: " << payload << endl;
			restartCount++;
		}
		else {
			if (restartCount == numToStop) {
				cout << "Send " << numToStop << " appCrash restarts for " << appName << " still no heartbeat. Stop sending restart message" << endl;
				string topic = "appCrash/" + appName;
				string payload = "{\"date\":\"" + IOTUtility::getTimestamp() + "\", \"name\":\"" + appName + "\", \"actionRequested\":\"report\", \"info\":\"Send " + std::to_string(numToStop) + " appCrash restarts, still no heartbeat. Stop sending restart message\"}";
				if (mqttSender.publish(topic, payload))
					cout << "publish is not successful: " << payload << endl;
				restartCount++;
			}
		}
	}
};

class NullIntervaledTask : public IntervaledTask{
public:
	void DoTask()
	{
//		std::cout << "Task Interval: " << mTaskInterval << "Seconds. Missing Heartbeat!";
	}
};

IntervaledTaskPtr IntervaledTaskFactory::make_task(std::string choose, int numRestart) // string
{
	if (choose == "CPUCollect"){
		return IntervaledTaskPtr(new IntervaledTaskCPUCollect);
	}else if (choose == "CPUReport"){
		return IntervaledTaskPtr(new IntervaledTaskCPUReport);
	}else if (choose == "TempCollect"){
		return IntervaledTaskPtr(new IntervaledTaskTempCollect);
	}else if (choose == "TempReport"){
		return IntervaledTaskPtr(new IntervaledTaskTempReport);
	}else if (choose == "WifiAppHeartBeat"){
		return IntervaledTaskPtr(new IntervaledTaskHeartBeat("wifiRSS", numRestart));
	}else if (choose == "TraqAppHeartBeat"){
		return IntervaledTaskPtr(new IntervaledTaskHeartBeat("wifiTrack", numRestart));
	}else if (choose == "DemoAppHeartBeat"){
		return IntervaledTaskPtr(new IntervaledTaskHeartBeat("demoApp", numRestart));
	}else if (choose == "VisnAppHeartBeat"){
		return IntervaledTaskPtr(new IntervaledTaskHeartBeat("visionApp", numRestart));
	}else if (choose == "ConnAppHeartBeat"){
		return IntervaledTaskPtr(new IntervaledTaskHeartBeat("connectorApp", numRestart));
	}else if (choose == "AppCPUCollect"){
		return IntervaledTaskPtr(new IntervaledTaskAppCPUCollect);
	}else if (choose == "AppMEMCollect"){
		return IntervaledTaskPtr(new IntervaledTaskAppMEMCollect);
	}

return IntervaledTaskPtr(new NullIntervaledTask);
}


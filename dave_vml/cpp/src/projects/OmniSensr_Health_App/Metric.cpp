/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
#include "Metric.hpp"
using namespace std;

// Split long string into string[5], e.g. {CPU:{Report:35,Collect:5}} to {"CPU","Report","35","Collect","5"}
vector<std::string> Metric::strToArray(std::string s)
{
	std::vector<std::string> strArray;
	s.erase(std::remove(s.begin(),s.end(),'"'),s.end());
	s.erase(std::remove(s.begin(),s.end(),'{'),s.end());
	s.erase(std::remove(s.begin(),s.end(),'}'),s.end());
	s.erase(std::remove(s.begin(),s.end(),' '),s.end());
	std::replace( s.begin(), s.end(), ',', ':'); // replace all ',' to ':'
	std::string delimiter = ":";
	size_t pos = 0;
	std::string token;
	while ((pos = s.find(delimiter)) != std::string::npos) {
		token = s.substr(0, pos);
		strArray.push_back(token);
		s.erase(0, pos + delimiter.length());
	}
	strArray.push_back(s);
	return strArray;
}

Metric::Metric(std::string str)
{
	std::vector<std::string> strArray = Metric::strToArray(str);
//	for (std::vector<std::string>::const_iterator i = strArray.begin(); i != strArray.end(); ++i)
//	    std::cout << *i << ' ';
	if(strArray.size()<5)
	{
			strArray.push_back("");
	}
//	mMetricName = "\"" + strArray[0] + "\"";
	mMetricName = strArray[0];
    std::string metricIntervals = "{";
    for(int i=0; i < int(strArray.size()); i++)
	{
//		std::cout << strArray[i] << std::endl;
		if (strArray[i] == "Collect")
		{
			CollectTimer = CountDownTimerPtr(new CountDownTimer(std::stoi(strArray[i+1]),IntervaledTaskFactory::make_task(strArray[0]+strArray[i])));
			metricIntervals = metricIntervals + "\"" + strArray[i] + "\":" + strArray[i+1] + ",";
//			metricIntervals = metricIntervals + strArray[i] + ":" + strArray[i+1] + ",";
		}
		else if (strArray[i] == "Report"||strArray[i] == "HeartBeat")
		{
			ReportTimer = CountDownTimerPtr(new CountDownTimer(std::stoi(strArray[i+1]),IntervaledTaskFactory::make_task(strArray[0]+strArray[i])));
			metricIntervals = metricIntervals + "\"" + strArray[i] + "\":" + strArray[i+1] + ",";
//			metricIntervals = metricIntervals + strArray[i] + ":" + strArray[i+1] + ",";
		}
	}
	metricIntervals.replace(metricIntervals.length()-1,1,"}");
	mCurrentMetricSettings = metricIntervals;
	std::cout <<"mCurrentMetricSettings: " << mCurrentMetricSettings << std::endl;
}

// Updated on June 28, check current interval to update or not
void Metric::UpdateInterval(std::string str)
{
		std::vector<std::string> strArray = Metric::strToArray(str);
		bool intervalChanged = false;
		std::string metricIntervals = "{";
		for (int i = 0; i < int(strArray.size()); i++){
			if (strArray[i] == "Collect" )
			{
				if (CollectTimer->getInterval() != std::stoi(strArray[i+1])){
					intervalChanged = true;
					CollectTimer->TimerReset(std::stoi(strArray[i+1]));
				}
				metricIntervals = metricIntervals + "\"" + strArray[i] + "\":" + strArray[i+1] + ",";
			}
			if (strArray[i] == "Report" || strArray[i] == "HeartBeat")
			{
				if (ReportTimer->getInterval() != std::stoi(strArray[i+1])){
					intervalChanged = true;
					ReportTimer->TimerReset(std::stoi(strArray[i+1]));
				}
				metricIntervals = metricIntervals + "\"" + strArray[i] + "\":" + strArray[i+1] + ",";
			}
		}
		metricIntervals.replace(metricIntervals.length()-1,1,"}");
		if (intervalChanged)
		{
			mCurrentMetricSettings = metricIntervals;
			intervalChanged = false;
		}
		std::cout <<"current mCurrentMetricSettings: " << mCurrentMetricSettings << std::endl;
}

Metric::~Metric()
{
}

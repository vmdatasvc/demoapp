project (OmniSensr_Health_App)
set (PROJECT_NAME OmniSensr_Health_App)
set (APP_DIR .)
set (APP_INCLUDE_DIRS ${APP_DIR})

SET(CMAKE_CXX_FLAGS "-std=c++11")

set (PROJECT_SRC	
	OmniSensr_Health_App.cpp
	CountDownTimer.cpp
	IntervaledTask.cpp
	IntervaledTaskFactory.cpp
	#Metric.cpp
)

set (INCLUDE_ALL_DIRS	
	${PROJECT_HEADER_FILES}
	${VML_ROOT}/include/modules/utility
	${VML_ROOT}/include/modules/iot
)

set (SRC_FILES
	${VML_ROOT}/src/modules/utility
	${VML_ROOT}/src/modules/iot
)

add_executable(${PROJECT_NAME} 
	${PROJECT_SRC}	
	${SRC_FILES}
)
target_include_directories(${PROJECT_NAME}  
	PUBLIC 
	${INCLUDE_ALL_DIRS}
	${APP_INCLUDE_DIRS}
	${VML_ROOT}/include/projects/${PROJECT_NAME} 
)

include_directories( 
	${INCLUDE_ALL_DIRS}
)

target_link_libraries(${PROJECT_NAME} 
	LINK_PUBLIC 
	vmlutility
	vmlcore 
	vmliot
	pthread
	mosquitto
	mosquittopp
	pigpiod_if2
	rt
)
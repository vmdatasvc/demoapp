/*
 * main.cpp
 *
 *  Created on: Feb 2, 2016
 *      Author: paulshin
 */

#include <vector>
#include "modules/utility/TimeKeeper.hpp"
#include "modules/iot/AWS_IoT_Module.hpp"

using namespace std;

	// -- AWS IoT Parameters
	char gIoTHostUrl[255];
	char gMyThingName[MAX_SIZE_OF_THING_NAME];
	char gMyClientID[MAX_SIZE_OF_UNIQUE_CLIENT_ID_BYTES];
	char gRootCAFileName[255];
	char gCertificateFileName[255];
	char gPrivateKeyFileName[255];
	char gCertDirectory[255];

//bool gMessageArrivedOnDelta;

AWS_IoT_Module gAwsIotMod;

/*	-- Thing Shadow parameters
 * 	To be able to get delta message and its corresponding callback functions to be triggered by aws_iot_sdk, the callback functions and the variables must be static.
 * 	For now, making them 'static' in a class and use them with aws_iot_sdk needs some more sophisticated implementation, I will just declare them here outside of the AWS_IoT_Shadow class.
 * 	By that way, we can easily create and configure different callback functions and delta variables, although this may not be an ideal choice in OOP sense.
 */
ShadowParam_t<float> gTemperature_float;
ShadowParam_t<float> gApplicationParam_float;
ShadowParam_t<bool> gWindowActuator_bool;

// --  The current parameter is automatically updated with the 'desired' parameter whenever this corresponding delta callback is called
void windowActuate_Callback(const char *pJsonValueBuffer, uint32_t valueLength, jsonStruct_t *pContext) {
	if (pContext != NULL) {
		char valueOnlyBuf[AWS_IOT_MQTT_RX_BUF_LEN] = {'\0'};
		snprintf(valueOnlyBuf, valueLength+1, "%.*s", valueLength+1, pJsonValueBuffer);
//		printf("[Delta Msg Received]: %s\n", valueOnlyBuf);

		//pthread_mutex_lock(&AwsIotMod.mMutex);
		bool windowActuate_desired = *(bool *)(pContext->pData);
//		gMessageArrivedOnDelta = true;
		//pthread_mutex_unlock(&AwsIotMod.mMutex);

		INFO("Delta - [Window] state changed to [%s]%d, while gWindowActuator_bool.value = %d", valueOnlyBuf, windowActuate_desired, gWindowActuator_bool.value);
	}
}

// --  The current parameter is automatically updated with the 'desired' parameter whenever this corresponding delta callback is called
void appParam_float_Callback(const char *pJsonValueBuffer, uint32_t valueLength, jsonStruct_t *pContext) {
	if (pContext != NULL) {
		char valueOnlyBuf[AWS_IOT_MQTT_RX_BUF_LEN] = {'\0'};
		snprintf(valueOnlyBuf, valueLength+1, "%.*s", valueLength+1, pJsonValueBuffer);
//		printf("[Delta Msg Received]: %s\n", valueOnlyBuf);

		//pthread_mutex_lock(&AwsIotMod.mMutex);
		float appParam_float_desired = *(float *)(pContext->pData);
//		gMessageArrivedOnDelta = true;
		//pthread_mutex_unlock(&AwsIotMod.mMutex);

		INFO("Delta - [appParam_float] state changed to [%s]%f, while gApplicationParam_float.value = %f", valueOnlyBuf, appParam_float_desired, gApplicationParam_float.value);
	}
}

// --  The current parameter is automatically updated with the 'desired' parameter whenever this corresponding delta callback is called
void temperature_Callback(const char *pJsonValueBuffer, uint32_t valueLength, jsonStruct_t *pContext) {
	if (pContext != NULL) {
		char valueOnlyBuf[AWS_IOT_MQTT_RX_BUF_LEN] = {'\0'};
		snprintf(valueOnlyBuf, valueLength+1, "%.*s", valueLength+1, pJsonValueBuffer);
//		printf("[Delta Msg Received]: %s\n", valueOnlyBuf);

		//pthread_mutex_lock(&AwsIotMod.mMutex);
		float temperature_desired = *(float *)(pContext->pData);
//		gMessageArrivedOnDelta = true;
		//pthread_mutex_unlock(&AwsIotMod.mMutex);

		INFO("Delta - [temperature] state changed to [%s]%f, , while gTemperature_float.value = %f", valueOnlyBuf, temperature_desired, gTemperature_float.value);
	}
}

// -- Callback function when delta message for "state" is received
void ShadowUpdateCallback(const char *pThingName, ShadowActions_t action, Shadow_Ack_Status_t status, const char *pReceivedJsonDocument, void *pContextData) {

	//pthread_mutex_lock(&AwsIotMod.mMutex);
	if (status == SHADOW_ACK_TIMEOUT) {
		INFO("Update Timeout--");
	} else if (status == SHADOW_ACK_REJECTED) {
		INFO("Update RejectedXX");
	} else if (status == SHADOW_ACK_ACCEPTED) {
		INFO("Update Accepted !!");
	}
	//pthread_mutex_unlock(&AwsIotMod.mMutex);
}

int MQTT_SubscribeCallback(MQTTCallbackParams params) {

	//pthread_mutex_lock(&AwsIotMod.mMutex);
	INFO("-------------------------------------------------------------");
	INFO("Subscribe callback");
	INFO("%.*s\t%.*s",
			(int)params.TopicNameLen, params.pTopicName,
			(int)params.MessageParams.PayloadLen, (char*)params.MessageParams.pPayload);
	INFO("--------------------------------------------------------------");
	//pthread_mutex_unlock(&AwsIotMod.mMutex);

	return 0;
}

// -- Parse AWS IoT Parameters
bool ParseSetting(const char *configpath) {
	FILE *fp;
	char variable[255], c;

	fp = fopen(configpath, "r");
	if(fp == NULL) fprintf(stderr, "Error opening %s file\n", configpath);
	else {
		while(fscanf(fp, "%s", variable) != EOF) {

			// -- Skip comment line
			// -- TODO: Maybe improved by using C++ type approach as in http://forums.devarticles.com/c-c-help-52/ignoring-commented-lines-when-using-fscanf-382249.html
			if(variable[0] == '#') {
				do {
					c = fgetc(fp);
				} while(c != '\n' && c != EOF);
				if(c == EOF) break;
				continue;
			}

			if(strncmp(variable, "AWS_IOT_MQTT_HOST", 17) == 0) {
				fscanf(fp, "%s", gIoTHostUrl);
				printf("AWS_IOT_MQTT_HOST = %s\n", gIoTHostUrl);
			} else

			if(strncmp(variable, "AWS_IOT_MY_THING_NAME", 21) == 0) {
				fscanf(fp, "%s", gMyThingName);
				printf("AWS_IOT_MY_THING_NAME = %s\n", gMyThingName);

				sprintf(gMyClientID, "%s-ClientID-%d", gMyThingName, getpid());
				printf("AWS_IOT_MQTT_CLIENT_ID = %s\n", gMyClientID);
			} else

			if(strncmp(variable, "AWS_IOT_ROOT_CA_FILENAME", 24) == 0) {
				fscanf(fp, "%s", gRootCAFileName);
				printf("AWS_IOT_ROOT_CA_FILENAME = %s\n", gRootCAFileName);
			} else

			if(strncmp(variable, "AWS_IOT_CERTIFICATE_FILENAME", 20) == 0) {
				fscanf(fp, "%s", gCertificateFileName);
				printf("AWS_IOT_CERTIFICATE_FILENAME = %s\n", gCertificateFileName);
			} else

			if(strncmp(variable, "AWS_IOT_PRIVATE_KEY_FILENAME", 20) == 0) {
				fscanf(fp, "%s", gPrivateKeyFileName);
				printf("AWS_IOT_PRIVATE_KEY_FILENAME = %s\n", gPrivateKeyFileName);
			} else

			if(strncmp(variable, "AWS_IOT_CERT_DIRECTORY", 22) == 0) {
				fscanf(fp, "%s", gCertDirectory);
				printf("AWS_IOT_CERT_DIRECTORY = %s\n", gCertDirectory);
			}

		}
	}

	fclose(fp);
	return true;
}

int main(int argc, char** argv) {


	if (argc < 2) {
		std::cout << "Usage is $ ./AWS_IoT_SDK_PubSubShadowCpp <path/to/aws_iot_config_params.txt> \n"; // Inform the user of how to use the program
		fprintf(stdout, "But, Will use default parameters\n");

		const char defaultConfigFile[] = "../../cpp/config/aws_iot_sdk_PubSubShadow_cpp/aws_iot_config_params.txt";
		// -- AWS IoT Parameters
		ParseSetting(defaultConfigFile);

	} else if (argc > 2) { // Check the value of argc. If not enough parameters have been passed, inform user and exit.
        std::cout << "Usage is $ ./AWS_IoT_SDK_PubSubShadowCpp <path/to/aws_iot_config_params.txt> \n"; // Inform the user of how to use the program
        exit(0);
	} else {
		fprintf(stdout, "Location to configuration parameter file is: %s\n", argv[1]);
		// -- AWS IoT Parameters
		ParseSetting((const char *)argv[1]);
	}

	// -- Must be done AFTER setting up AWS IoT Parameters
	gAwsIotMod.ThingShadow_Init(gMyThingName, gMyClientID, gIoTHostUrl, AWS_IOT_MQTT_PORT, gCertDirectory,
								gRootCAFileName, gCertificateFileName, gPrivateKeyFileName);

//	gMessageArrivedOnDelta = false;

	// -- Pass the callback function pointer into the class
	AWS_IoT_Module::mShadowUpdateCallbackPtr = &ShadowUpdateCallback;

	// -- Subscribe to topics for Data Reception
//	awsShadow.MQTT_subscribe(QOS_1, "sdkTest/sub", &MQTT_SubscribeCallback);
//	awsShadow.MQTT_subscribe(QOS_1, "sdkTest/pub", &MQTT_SubscribeCallback);
	gAwsIotMod.MQTT_subscribe(QOS_1, "sdkTest/#", &MQTT_SubscribeCallback);

	// -- Create a link between this flag into an internal flag in AwsIotMod, so that AwsIotMod can re-send the updated Delta Shadow message with the updated states.
//	gAwsIotMod.SetDeltaMessageArrivalEvent(&gMessageArrivedOnDelta);

	// -- Set the Shadow and Delta objects.
	// -- Note that this must be done BEFORE calling ShadowConnectInit().
	{
		gApplicationParam_float.value = 0.0;
		gApplicationParam_float.json.cb = appParam_float_Callback;
		gApplicationParam_float.json.pKey = "appParam_float";
		gApplicationParam_float.json.pData = &gApplicationParam_float.value;	// --  This parameter is automatically updated with the 'desired' parameter whenever the corresponding delta callback is called.
		gApplicationParam_float.json.type = SHADOW_JSON_FLOAT;

		gAwsIotMod.Shadow_addParam(&gApplicationParam_float.json);

		gWindowActuator_bool.value = true;
		gWindowActuator_bool.json.cb = windowActuate_Callback;
		gWindowActuator_bool.json.pData = &gWindowActuator_bool.value;	// --  This parameter is automatically updated with the 'desired' parameter whenever the corresponding delta callback is called.
		gWindowActuator_bool.json.pKey = "windowOpen";
		gWindowActuator_bool.json.type = SHADOW_JSON_BOOL;

		gAwsIotMod.Shadow_addParam(&gWindowActuator_bool.json);

		gTemperature_float.value = 25.00;
		gTemperature_float.json.cb = temperature_Callback;
		gTemperature_float.json.pData = &gTemperature_float.value;	// --  This parameter is automatically updated with the 'desired' parameter whenever the corresponding delta callback is called.
		gTemperature_float.json.pKey = "temperature";
		gTemperature_float.json.type = SHADOW_JSON_FLOAT;

		gAwsIotMod.Shadow_addParam(&gTemperature_float.json);
	}

	//awsShadow.parseInputArgsForConnectParams(argc, argv);

	INFO("\nAWS IoT SDK Version(dev) %d.%d.%d-%s\n", VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH, VERSION_TAG);

	IoT_Error_t rc = NONE_ERROR;

	// -- Initiate AWS IoT module
	if (NONE_ERROR != gAwsIotMod.Run()) {
		ERROR("awsShadow.run() error %d", rc);
	}

	//	// -- Delete the Thing Shadow at AWS IoT Server
	//	while(gAwsIotMod.Shadow_Delete() != NONE_ERROR) {
	//		usleep(1000*10);
	//	}
	//
	//	// -- Reset the local version number of the Thing Shadow
	//	gAwsIotMod.Shadow_Reset_LastReceivedVersion();


	double PERIODIC_DATA_GENERATION_INTERVAL = 1.0;	// -- Unit: [sec]

	double currTime = 0, prevTime_periodic = gCurrentTime();
	int32_t i = 0;

	// -- Simulate periodic data generation
	while (1) {

		currTime = gCurrentTime();

		// -- Send periodic Thing Shadow report
		if(currTime - prevTime_periodic >= PERIODIC_DATA_GENERATION_INTERVAL) {
			prevTime_periodic = currTime;

			char cPayload[100];
			sprintf(cPayload, "%s : %d ", "hello from SDK", i++);
			string msg(cPayload);

			// -- Publish Data messages via AWS IoT module
			gAwsIotMod.MQTT_publish(QOS_1, "sdkTest/sub", msg);
			gAwsIotMod.MQTT_publish(QOS_1, "sdkTest/pub", msg);
			gAwsIotMod.MQTT_publish(QOS_1, "sdkTest/ssssss", msg);
		}

		usleep(1000*10);
	}


	return 0;
}

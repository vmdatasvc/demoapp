//============================================================================
// Name        : OmniSensr_SampleApp.cpp
// Author      : Paul J. Shin
// Version     :
// Copyright   : Your copyright notice
// Description :
//============================================================================

#include "OmniSensr_SampleApp.hpp"

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include "modules/utility/redis_wrapper.hpp"
#include <boost/shared_ptr.hpp>
#include "json.hpp"

#include <iostream>
#include <sstream>

#include "modules/iot/IoT_Common.hpp"

using std::cout;

using std::string;
using std::ostringstream;
using std::vector;
using std::endl;

OmniSensr_SampleApp::OmniSensr_SampleApp(const std::string& configPath) : OmniSensrApp(APP_ID_SAMPLE_APP, configPath){

	// set the default values of my parameters
	priv_float = 1.0f;
	priv_int = 2;
	priv_str = "Default private string";

	// configuration parameters exposed
	pub_float = -priv_float;
	pub_int = -priv_int;
	pub_str = "Default public string";

	control_int = 0;

	// 60 seconds between wakeups
	sleep_interval = 60;

	killed = false;

}

OmniSensr_SampleApp::~OmniSensr_SampleApp(){
	// Nothing to do here
}

// main processing loop code goes here
void OmniSensr_SampleApp::start(){

	// Example for using redis wrapper
	boost::shared_ptr<redis_wrapper> rwrapper;
	// Reset pointer when intend to use
	rwrapper.reset(new redis_wrapper());
	if (rwrapper->connect())
		cout << "Failed to connect to redis" << endl;
	if (rwrapper->set("exampleKey", "OK"))
		cout << "Failed to set key" << endl;
	string ret = rwrapper->getString("exampleKey");
	cout << "ret: " << ret << endl;
	// depending on the frequency you use the database, decide whether to disconnect and connect later
	rwrapper->disconnect();


	bool soRudelyAwoken;

	// send the full status
	sendFullState();

	nlohmann::json dataMsg;
	int count = 0;
	while(!killed){

		state_lock.lock();
		appState["pubInt"] = (int)appState["pubInt"] + 1;
		state_lock.unlock();
		dataMsg["dataCount"] = count++;
		sendData(dataMsg.dump());

		main_lock.lock();
		// send a heartbeat.  Should expect another one in no more than 2 times the sleep interval
		// (NOTE: we will certainly wake up after the interval has passed, but we may have some
		// processing to do.  The 2x multiplier gives us a guaranteed buffer, even in the case
		// of the interval == 1.
		heartbeat(2*sleep_interval);
		soRudelyAwoken = main_cond.timed_wait(main_lock, boost::posix_time::seconds(sleep_interval));

		if(soRudelyAwoken){
			cout << "MAIN: Woke up on signal from another thread." << endl;
			// lock the control message log
			msg_lock.lock();
			auto it = control_msg_list.rbegin();
			string msg = "No commands yet";
			if(it != control_msg_list.rend()){
				ostringstream os;
				os << "Number of commands: " << control_msg_list.size() << ".  ";
				os << "Last command received: '" << (*it) << "'";
				msg = os.str();
			}
			// unlock the control message log
			msg_lock.unlock();

			sendData(msg);

			// unlock the main lock (will reacquire at the top of the loop
		} else{
			cout << "MAIN: Woke up on my own.  I'm alive." << endl;
		}

		main_lock.unlock();


	}


	// save my state so I can restart from the last known good state
	SaveSetting();


}

// process a command
void OmniSensr_SampleApp::processCommand(const std::string& cmd){
	// lock the message log
	msg_lock.lock();
	control_msg_list.push_back(cmd);
	msg_lock.unlock();

	// check for a command of the form "key=value"
	vector<string> tokens;
	boost::algorithm::split(tokens, cmd, boost::is_any_of("="));
	if(tokens.size() == 2){
		// strip out any whitespace
		// tokens[0] is the key
		boost::algorithm::trim(tokens[0]);
		// tokens[1] is the value
		boost::algorithm::trim(tokens[1]);

		if(tokens[0] == "interval"){
			try{
				sleep_interval = boost::lexical_cast<int>(tokens[1]);
				cout << "CB: Reset the interval to " << sleep_interval << " seconds" << endl;
			} catch (boost::bad_lexical_cast& e){
				sendError("Bad Inverval Format", false);
			}


		} else if(tokens[0] == "controlInt"){
			try{
				control_int = boost::lexical_cast<int>(tokens[1]);
				// change the controlInt variable and report the change
				state_lock.lock();
				appState["controlInt"] = control_int;
				sendState("controlInt");
				state_lock.lock();

				cout << "CB: Set the control int parameter to " << control_int << endl;
			} catch (boost::bad_lexical_cast& e) {
				sendError("Bad Control Int Format", false);
			}
		}
	} else if(boost::algorithm::trim_copy(cmd) == "die"){
		// Also listen for the special command "die", which will exit the main event loop
		killed = true;
	}


	// lock the main thread lock
	main_lock.lock();
	//signal to the event loop to wake up!
	main_cond.notify_one();
	// and allow the event loop to proceed
	main_lock.unlock();
}

unsigned int OmniSensr_SampleApp::initConfig(ParseConfigTxt& parser){
	unsigned int n_params = 0;

	// un-exposed variables
	n_params += parser.getValue("PRIVATE_FLOAT", priv_float);
	n_params += parser.getValue("PRIVATE_INT", priv_int);
	n_params += parser.getValue("PRIVATE_STRING", priv_str);

	// un-exposed variable, but modifiable through command channel
	n_params += parser.getValue("SLEEP_INTERVAL", sleep_interval);

	// publicly exposed simple variables (in ThingShadow)
	n_params += parser.getValue("PUBLIC_INT", pub_int);
	registerVariable("pubInt", pub_int);

	n_params += parser.getValue("PUBLIC_FLOAT", pub_float);
	registerVariable("pubFloat", pub_float);

	n_params += parser.getValue("PUBLIC_STRING", pub_str);
	registerVariable("pubStr", pub_str);
//	registerDeltaCallback("pubFloat", boost::bind(&OmniSensr_SampleApp::pubFloat_callback, this, _1));
	// publicly exposed "complex" variable
	n_params += parser.getValue("CONTROL_INT", control_int);
	//  Add my callback
	bool cbreg = registerDeltaCallback("controlInt", boost::bind(&OmniSensr_SampleApp::controlInt_callback, this, _1));
	// this REALLY should succeed, as the only way it can fail is if another
	// thread has locked the callback map, but we haven't started any other
	// threads yet!
	// But, best to be thorough anyway...
	if(cbreg){
		state_lock.lock();
		appState["controlInt"] = control_int;
		state_lock.unlock();
	}

	return n_params;
}


void OmniSensr_SampleApp::saveState(std::ostream& os) const {
	os << "PRIVATE_FLOAT " << priv_float << endl;
	os << "PRIVATE_INT " << priv_int << endl;
	os << "PRIVATE_STRING " << priv_str << endl;

	os << "SLEEP_INTERVAL " << sleep_interval << endl;

	os << "PUBLIC_INT " << pub_int << endl;
	os << "PUBLIC_FLOAT " << pub_float << endl;
	os << "PUBLIC_STRING " << pub_str << endl;

	os << "CONTROL_INT " << control_int << endl;
}

// callbacks to process a delta of the controlInt
void OmniSensr_SampleApp::controlInt_callback(const std::string& param){

	// I can't set the control int, but I will wake the main thread,
	// but ONLY if a valid int was sent
	try{
		boost::lexical_cast<int>(param);
		// wake up the main event loop
		main_lock.lock();
		main_cond.notify_one();
		main_lock.unlock();
	}catch(boost::bad_lexical_cast& e){
		// send a non-fatal warning on the appCrash channel
		sendError("Bad control int", false);
	}
}

int main() {

	OmniSensr_SampleApp sampleApp("config/OmniSensr_SampleApp/OmniSensr_SampleApp_config.txt");
	sampleApp.Run();

	// will never get here, unless the "die" command was sent
	return 0;
}

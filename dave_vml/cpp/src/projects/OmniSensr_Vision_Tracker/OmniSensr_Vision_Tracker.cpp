/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/****************************************************************************/

// OmniVisionTracker.cpp
//
//   command line vision tracker tool for OmniSensr
//
//   Youngrock Yoon
//   2016.7.28

#include <cstdio>
#include <getopt.h>
#include <unistd.h>
#include <boost/algorithm/string.hpp>

#include <modules/utility/Util.hpp>
#include <core/Error.hpp>
#include <modules/vision/VisionVideoModule.hpp>
#include <modules/utility/TimeKeeper.hpp>
// -- AWS IoT SDK files for State Parameter Type definition
#include <aws_iot_error.h>
#include <aws_iot_shadow_json_data.h>

#include <modules/iot/IoT_Common.hpp>
#include <modules/iot/IoT_Gateway.hpp>
#include <modules/iot/IOTCommunicator.hpp>	// for heartbeat

#include <modules/vision/VisionTargetManager.hpp>
#include <modules/vision/MultiTargetVisionTracker.hpp>
#include <modules/vision/VisionSiteSetup.hpp>

#include <json.hpp>

#include <string>
#include <sstream>
#include <iostream>

#include <boost/filesystem.hpp>

using json = nlohmann::json;
using std::string;
using std::ostringstream;
using std::endl;
using std::fixed;
using std::setprecision;
using std::cout;

#define IOT_GATEWAY_CONFIG_FILE		"/home/omniadmin/omnisensr-apps/config/IoT_Gateway/IoT_Gateway_config.txt"
#define CONFIG_PATH				"/home/omniadmin/omnisensr-apps/config/OmniSensr_Vision_Tracker"
#define TEST_VIDEO_640X480_FILE		"/home/omniadmin/OmniVisionTracker/cam007_20160122_1630.avi"
#define TEST_VIDEO_320X240_FILE		"/home/omniadmin/OmniVisionTracker/20150601_120000_168.avi"
#define INPUT_VIDEO_URL				"http://127.0.0.1:8080/?action=stream.mjpg"	// mjpg-streamer url

#define DEBUG	0

// globals
bool	bProcessStop = false;
bool	bThreadsStop = false;
bool	bVerbose = true;
bool	bTestRun = false;
bool	bRun320x240 = false;
int		nProcessingThreads = 3;
double	dLogInterval = 120.f;
int		nTrackerMonitorInterval = 5;

//vml::Logger	logger;		// for verbose output

std::string	config_path = CONFIG_PATH;
std::string input_source_addr = INPUT_VIDEO_URL;	// default is localhost mjpg-streamer

// tracker module pointers
boost::shared_ptr<SETTINGS> pSettings;
boost::shared_ptr<vml::MultiTargetVisionTracker> pTracker;
boost::shared_ptr<IOT_Communicator>		pIOTc;

// IoT members
IoT_Gateway*	IoTGateway = NULL;
// IoT app id
AppType_t		appID = visionApp;
std::string		appType;

// IoT state variables;
int		IoTState_BackgroundSubtraction_historyLength;
int		IoTState_BackgroundSubtraction_numTrainingFrame;
float	IoTState_Camera_personRadius;
float	IoTState_Camera_personHeight;
float	IoTState_VisionTargetManager_inactiveMapWindowRadiusProportion;
float	IoTState_VisionTargetManager_trajectoryLengthThreshold;
float	IoTState_VisionTargetManager_trajectoryDurationThreshold;
std::string	IoTState_BaseVisionTracker_trackingMethod;
float	IoTState_BaseVisionTracker_searchWindowRatio;
float	IoTState_BaseVisionTracker_inactiveTimeout;
double	IoTState_DataReportRefreshInterval;
double	IoTState_StateReportRefreshInterval;
int		IoTState_LiveProjectVersion;
// track region control
std::string	IoTControl_TrackRegion;

typedef struct {
	JsonPrimitiveType type;
	std::string key;
	std::string path;
	bool		response_to_delta;
	void*		pVar;
} stateKeyMapStructType;

typedef stateKeyMapStructType	*stateKeyMapPtr;

std::map<std::string,stateKeyMapPtr>	stateKeyMap;
std::map<std::string,stateKeyMapPtr>	settingsPathMap;

#define N_STATEKEYMAP	13

stateKeyMapStructType	stateMapping[N_STATEKEYMAP] = {
	{SHADOW_JSON_INT32,"histLength","VisionVideoModule/BackgroundSubtraction/historyLength",true,&IoTState_BackgroundSubtraction_historyLength},
	{SHADOW_JSON_INT32,"numTrainFrame","VisionVideoModule/BackgroundSubtraction/numTrainingFrame",true,&IoTState_BackgroundSubtraction_numTrainingFrame},
	{SHADOW_JSON_FLOAT,"personRadius","Camera/personRadius",true,&IoTState_Camera_personRadius},
	{SHADOW_JSON_FLOAT,"personHeight","Camera/personHeight",true,&IoTState_Camera_personHeight},
	{SHADOW_JSON_FLOAT,"inactiveTimeout","BaseVisionTracker/inactiveTimeout",true,&IoTState_BaseVisionTracker_inactiveTimeout},
	{SHADOW_JSON_FLOAT,"inactiveMapRad", "VisionTargetManager/inactiveMapWindowRadiusProportion",true,&IoTState_VisionTargetManager_inactiveMapWindowRadiusProportion},
	{SHADOW_JSON_FLOAT,"trajLengthThresh", "VisionTargetManager/trajectoryLengthThreshold",true,&IoTState_VisionTargetManager_trajectoryLengthThreshold},
	{SHADOW_JSON_FLOAT, "trajDuraThresh","VisionTargetManager/trajectoryDurationThreshold",true,&IoTState_VisionTargetManager_trajectoryDurationThreshold},
	{SHADOW_JSON_STRING, "tracMethod","BaseVisionTracker/trackingMethod",true,&IoTState_BaseVisionTracker_trackingMethod},
	{SHADOW_JSON_FLOAT, "srchWindowRatio", "BaseVisionTracker/searchWindowRatio",true,&IoTState_BaseVisionTracker_searchWindowRatio},
	{SHADOW_JSON_DOUBLE,"dataRefreshInt", "dataReportRefreshInterval",true, &IoTState_DataReportRefreshInterval},
	{SHADOW_JSON_DOUBLE,"stateRefreshInt", "stateReportRefreshInterval",true, &IoTState_StateReportRefreshInterval},
	{SHADOW_JSON_INT32,"liveProjVer", "LiveProjectVersion",false, &IoTState_LiveProjectVersion}
};

// Multi-threading members
pthread_attr_t	threadAttr;
pthread_t		dataReportThread;
pthread_t		stateReportThread;
pthread_mutex_t	gMutex;		// NOTE: need this because IoT_Gateway class declares gMutex as extern, so this should be somewhere in the source.
pthread_mutex_t appMutex;	// for syncing apps

vml::FileParts	config_file_parts;
std::string		settings_file_path;
std::string		camcoverage_file_path;

// Error report and exit
void exitError(std::string msg);

// -- Define callback functions for messages from Delta State and Control Channel
// thread prototypes
static void *dataReportThreadFunc(void *apData);
void startTracker();
void stopTracker();
void stateInitialization();
void resetSettings();
void initializeSettings();
void updateStateFromSettings();

// IoT callbacks
void IoT_DeltaState_CallBack_func(std::string deltaMsg);
void IoT_ControlChannel_CallBack_func(std::string topic, std::string controlMsg);

// Body
void exitError(std::string msg)
{
	std::cout << msg << std::endl;
	pIOTc->sendError(msg);
	// stop all threads
	stopTracker();
	pIOTc.reset();
	// TODO: IoTGateway is blocking exit. Need to add IoTGateway termination method.
	if (IoTGateway)
	{
		delete(IoTGateway);
		IoTGateway = NULL;
	}
	exit(vml::MsgCode::IOErr);
}

void* dataReportThreadFunc(void *apdata) {

	vml::MultiTargetVisionTracker* pt = (vml::MultiTargetVisionTracker*)apdata;
	const int	dataReportRefreshInterval = (int)(1000000.f*pSettings->getDouble("dataReportRefreshInterval",2.f,false));

	do {

		usleep(dataReportRefreshInterval);	// data report interval. Default is 2 sec.

		// -- Periodically process/send data to the IoT Cloud

		// construct data message
		// get completed trajectories
		std::vector<vml::VisionTrajectory>	trajectories;	// buffer for completed trajectory
		if (pt->isRunning())
		{
			pt->MultithreadPipeliningGetCompletedTrajectories(trajectories);
		}

		if (trajectories.size() > 0)
		{
			// display completed trajectories
			if (bVerbose)
			{
				std::cout << trajectories.size() << " trajectories are completed and being reported" << std::endl;
			}

			// add data body
			for (unsigned int i=0;i<trajectories.size();i++)
			{
				std::stringstream msgstream;

				// initialize header
				msgstream << "{ \"a4\":\"";

				msgstream << trajectories[i].toXmlString(pSettings->getInt("trajectoryReportDecimals",1,false));
				msgstream << "\" }";

				// send the data
				IoTGateway->SendData(msgstream.str());
			}

		}

	} while(!bThreadsStop);

    return NULL;
}

void IoT_ControlChannel_CallBack_func(std::string topic, std::string controlMsg) {

	stopTracker();	// stop tracker before update control message

	json controlMsgJson;

	try {
		controlMsgJson = json::parse(controlMsg.c_str());

		std::cout << "Control message received: " << std::setw(2) << controlMsgJson << std::endl;

		for (json::iterator it = controlMsgJson.begin(); it != controlMsgJson.end(); ++it) {

			std::string key = it.key();
			if (key == "TrackPolygon")
			{
				// change track polygon settings
				std::string trackpolystr = it.value();
				std::vector<std::string> trackpolystrsplit;
				boost::split(trackpolystrsplit,trackpolystr,boost::is_any_of(", "));
				std::vector<double>	trackpoly;
				for (auto titr = trackpolystrsplit.begin();titr!=trackpolystrsplit.end();titr++)
				{
					double tpvert = atof(titr->c_str());
					trackpoly.push_back(tpvert);
				}

				pSettings->setDoubleVector(key,trackpoly);
			}
			else if (key == "NoStartZone")
			{
				int n_notrackpoly = 0;
				// no start zone is a nested json.
				for(json::iterator it2 = it->begin(); it2 != it->end(); it2++)
				{
					std::string key2 = it2.key();
					if (key2[0] == 'P')
					{
						// this is no-track polygon
						// convert the value string to double array
						std::string notrackpolystr = it2.value();
						std::vector<std::string> notrackpolystrsplit;
						boost::split(notrackpolystrsplit,notrackpolystr,boost::is_any_of(", "));
						std::vector<double>	notrackpoly;
						for (auto titr = notrackpolystrsplit.begin();titr!=notrackpolystrsplit.end();titr++)
						{
							double ntpvert = atof(titr->c_str());
							notrackpoly.push_back(ntpvert);
						}
						n_notrackpoly++;

						// set settings
						std::string settings_path = "NoStartZone/" + key2;
						pSettings->setDoubleVector(settings_path,notrackpoly);
					}
				}

				// reset number of no track polygon number
				pSettings->setInt("NoStartZone/numPolygons",n_notrackpoly);

			}
			else if (key == "camHeight")
			{
				std::string tmp_str = it.value();
				float camHeight = atof(tmp_str.c_str());
				// set settings
				pSettings->setFloat("Camera/height",camHeight);
			}
			else if (key == "camType")
			{
				std::string tmp_str = it.value();
				// set settings
				pSettings->setString("Camera/modelName",tmp_str);
			}
			else if (key == "liveProjVer")
			{
				std::string ver_str = it.value();
				int ver = atoi(ver_str.c_str());
				// track polygon version is a part of state.  Update state
				devState_t *stateParam = IoTGateway->FindStateParam(key);
				pthread_mutex_lock(&gMutex);
				*(int*)stateParam->value = ver;
				// -- Reset this so that this state can be reported again immediately.
				stateParam->time_lastReported = 0;
				pthread_mutex_unlock(&gMutex);
				pSettings->setInt(stateKeyMap[key]->path,ver);
			}
			else if (it.key() == "KillApp")
			{
				if (it.value() == true)
				{
					// kill this app
					stopTracker();	// stop tracker
					// stop threads
					bThreadsStop = true;
					// stop main process
					bProcessStop = true;
				}
			}
			else
			{
				std::ostringstream msgstr;
				msgstr << *it;
				std::cout << "WARNING: Unsupported control message " << msgstr.str();
			}
			// currently other control message is not supported

		}


	} catch (std::invalid_argument& e) {		// -- Handle exception when the received msg does not comply with JSON standard
		std::stringstream emsg;
		emsg << "IoT_ControlChannel_CallBack_func: invalid_argument: " << e.what() << std::endl;
		pIOTc->sendError(emsg.str());
	} catch (std::out_of_range& e) {	// -- Handle exception when a non-existing filed is accessed.
		std::stringstream emsg;
		emsg << "IoT_ControlChannel_CallBack_func: out of range: " << e.what() << std::endl;
		pIOTc->sendError(emsg.str());
	} catch (std::domain_error& e) {	// -- Handle exception when an incompatible conversion is attempted (e.g., float --> string).
		std::stringstream emsg;
		emsg << "IoT_ControlChannel_CallBack_func: domain error: " << e.what() << std::endl;
		pIOTc->sendError(emsg.str());
	}

	// store settings and restart app
	// tracker should be stopped. let health app standby enough time to wait for the tracker app to restart
	pIOTc->sendHeartbeat(pSettings->getInt("restartHeartbeatInterval",60,false));
	// stop tracker
	stopTracker();
	// New control message received. reset settings,then restart tracker.
	// reset settings instance
	resetSettings();
	// Because some settings in this control message are dependent to other settings, re-reset settings
	resetSettings();

	// restart tracker
	startTracker();
}

/*
 * Note that all the types of JsonPrimitiveType of AWS ThingShadow parameters are grouped into just 6 types: int, uint, float, double, bool, string.
 *
 * TODO: This is basically a JSON parser plus extra, so we can make this better by using a formal JSON parser.
 */

void IoT_DeltaState_CallBack_func(std::string entireDeltaMsg) {
	json entireDeltaMsgJson = json::parse(entireDeltaMsg);
	bool	restart_tracker = false;

	// -- Check if there is at least a delta state destined to this app. If not, then exit this function
	if(entireDeltaMsgJson.find(appType) == entireDeltaMsgJson.end())
		return;

	json deltaMsgJson = json::parse(entireDeltaMsgJson.at(appType).dump());

	// -- If there is at least a delta state destined to this app, then start processing it by looking up the registered state params.
	try {

		std::cout << "Delta State received: " << std::setw(2) << deltaMsgJson << std::endl;

		for (auto it = deltaMsgJson.begin(); it != deltaMsgJson.end(); ++it) {

			std::ostringstream actualValue;
			actualValue << it.value();

			devState_t *stateParam = IoTGateway->FindStateParam(it.key());

			pthread_mutex_lock(&gMutex);
			// -- Check if there's matching state parameter
			if(stateParam != NULL) {
				 std::cout << "[Delta Message] stateParam FOUND (key = %s)\n" << stateParam->key << std::endl;

				// -- Do something here to react to the received delta message to this particular state parameter.
				switch(stateParam->type) {
					case SHADOW_JSON_INT32:
					case SHADOW_JSON_INT16:
					case SHADOW_JSON_INT8: {
						if (stateKeyMap[stateParam->key]->response_to_delta)
						{
							int stateParam_desired;
							sscanf(actualValue.str().c_str(), "%d", &stateParam_desired);

							std::cout << "(report, desired) = (" << *(int*)stateParam->value << " , " << stateParam_desired << " -- > ";

							// -- Replace the state parameter with the desired value
							*(int*)stateParam->value = stateParam_desired;
							// -- Reset this so that this state can be reported again immediately.
							stateParam->time_lastReported = 0;

							// reset settings
							pSettings->setInt(stateKeyMap[stateParam->key]->path,stateParam_desired);

							std::cout << "(" <<  *(int*)stateParam->value << ")" << std::endl;

							restart_tracker = true;

						}

					}
						break;
					case SHADOW_JSON_UINT32:
					case SHADOW_JSON_UINT16:
					case SHADOW_JSON_UINT8: {
						if (stateKeyMap[stateParam->key]->response_to_delta)
						{
							unsigned int stateParam_desired;
							sscanf(actualValue.str().c_str(), "%u", &stateParam_desired);

							std::cout << "(report, desired) = (" <<  *(unsigned int*)stateParam->value << " , " << stateParam_desired << ") --> " << std::endl;

							// -- Replace the state parameter with the desired value
							*(unsigned int*)stateParam->value = stateParam_desired;
							// -- Reset this so that this state can be reported again immediately.
							stateParam->time_lastReported = 0;
							// reset settings. NOTE: vml::Settings doesn't support unsigned int. Convert to Int
							pSettings->setInt(stateKeyMap[stateParam->key]->path,stateParam_desired);

							std::cout << "(" <<  *(unsigned int*)stateParam->value << " )" << std::endl;

							restart_tracker = true;
						}
					}
						break;
					case SHADOW_JSON_FLOAT: {
						if (stateKeyMap[stateParam->key]->response_to_delta)
						{
							float stateParam_desired;
							sscanf(actualValue.str().c_str(), "%f", &stateParam_desired);

							std::cout << "[" << actualValue.str() << "] ";
							std::cout << std::fixed << std::setprecision(IOT_FLOAT_PRECISION);
							std::cout << " (report, desired) = ("<< *(float*)stateParam->value << ", " << stateParam_desired << ") --> ";

							// -- Replace the state parameter with the desired value
							*(float*)stateParam->value = stateParam_desired;
							// -- Reset this so that this state can be reported again immediately.
							stateParam->time_lastReported = 0;
							// reset settings
							pSettings->setFloat(stateKeyMap[stateParam->key]->path,stateParam_desired);

							std::cout << std::fixed << std::setprecision(IOT_FLOAT_PRECISION);
							std::cout << "("<< *(float*)stateParam->value << ")" << std::endl;

							restart_tracker = true;
						}
					}
						break;
					case SHADOW_JSON_DOUBLE: {
						if (stateKeyMap[stateParam->key]->response_to_delta)
						{
							double stateParam_desired;
							sscanf(actualValue.str().c_str(), "%lf", &stateParam_desired);

							std::cout << "[ " << actualValue.str() << "] ";
							std::cout << std::fixed << std::setprecision(IOT_DOUBLE_PRECISION);
							std::cout << " (report, desired) = ("<< *(double*)stateParam->value << ", " << stateParam_desired << ") --> ";

							// -- Replace the state parameter with the desired value
							*(double*)stateParam->value = stateParam_desired;
							// -- Reset this so that this state can be reported again immediately.
							stateParam->time_lastReported = 0;
							// reset settings
							pSettings->setDouble(stateKeyMap[stateParam->key]->path,stateParam_desired);

							std::cout << std::fixed << std::setprecision(IOT_DOUBLE_PRECISION);
							std::cout << "("<< *(double*)stateParam->value << ")" << std::endl;

							restart_tracker = true;
						}
					}
						break;
					case SHADOW_JSON_BOOL: {
						if (stateKeyMap[stateParam->key]->response_to_delta)
						{
							char stateParam_desired[99] = {'\0'};
							sscanf(actualValue.str().c_str(), "%s", stateParam_desired);

							std::cout << "(report, desired) = (" << *(bool*)stateParam->value << " , " << stateParam_desired << ") --> ";


							// -- Replace the state parameter with the desired value
							if(stateParam_desired[0] == 't')	// -- true
							{
								*(bool*)stateParam->value = true;
								// reset settings
								pSettings->setBool(stateKeyMap[stateParam->key]->path,true);
							}
							if(stateParam_desired[0] == 'f')	// -- false
							{
								*(bool*)stateParam->value = false;
								// reset settings
								pSettings->setBool(stateKeyMap[stateParam->key]->path,false);
							}
							// -- Reset this so that this state can be reported again immediately.
							stateParam->time_lastReported = 0;

							std::cout << "(" << *(bool*)stateParam->value << " )" << std::endl;
							restart_tracker = true;
						}
					}
						break;

					case SHADOW_JSON_STRING: {
						if (stateKeyMap[stateParam->key]->response_to_delta)
						{
							// -- String-type value in JSON is wrapped with double-quotation marks, so get rid of them.
							std::string strExtracted = actualValue.str().substr(1,actualValue.str().size()-2);

							std::cout << "(report, desired) = (" << *(std::string*)stateParam->value << " , " << strExtracted << ") -->";

							// -- Replace the state parameter with the desired value
							*(std::string*)stateParam->value = strExtracted;
							// -- Reset this so that this state can be reported again immediately.
							stateParam->time_lastReported = 0;

							// reset settings
							pSettings->setString(stateKeyMap[stateParam->key]->path,strExtracted);

							std::cout << "(" << *(std::string*)stateParam->value << ")" << std::endl;

							restart_tracker = true;
						}


					}
						break;

					default:
						break;
				}
			}
			pthread_mutex_unlock(&gMutex);
		}

	} catch (std::invalid_argument& e) {		// -- Handle exception when the received msg does not comply with JSON standard
		std::stringstream emsg;
		emsg << "IoT_DeltaState_CallBack_func: invalid_argument: " << e.what() << std::endl;
		pIOTc->sendError(emsg.str());
    } catch (std::out_of_range& e) {	// -- Handle exception when a non-existing filed is accessed.
		std::stringstream emsg;
		emsg << "IoT_DeltaState_CallBack_func: out of range: " << e.what() << std::endl;
		pIOTc->sendError(emsg.str());
	} catch (std::domain_error& e) {	// -- Handle exception when an incompatible conversion is attempted (e.g., float --> string).
		std::stringstream emsg;
		emsg << "IoT_DeltaState_CallBack_func: domain_error : " << e.what() << std::endl;
		pIOTc->sendError(emsg.str());
	}

	if (restart_tracker)
	{
		// tracker should be stopped. let health app standby enough time to wait for the tracker app to restart
		pIOTc->sendHeartbeat(pSettings->getInt("restartHeartbeatInterval",60,false));

		// restart tracker
		stopTracker();	// stop tracker
		// delta state received and updated. Save settings, reload settings, then restart tracker.
		// reset settings instance
		resetSettings();
		startTracker();	// restart tracker
	}
}

void stateInitialization()
{
	double	stateReportInterval = pSettings->getDouble("stateReportRefreshInterval",900.f,false);	// default is 15 min.
	for (int i=0;i<N_STATEKEYMAP;i++)
	{
		// add state key and settings path mapping
		stateKeyMap[stateMapping[i].key] = &(stateMapping[i]);
		settingsPathMap[stateMapping[i].path] = &(stateMapping[i]);
		IoTGateway->AddStateParam(stateMapping[i].type,stateMapping[i].key,stateMapping[i].pVar,NULL,stateReportInterval);

	}

}

void updateStateFromSettings()
{
	for (int i=0;i<N_STATEKEYMAP;i++)
	{
		devState_t *stateParam = IoTGateway->FindStateParam(stateMapping[i].key);

		pthread_mutex_lock(&gMutex);
		switch (stateMapping[i].type)
		{
			case SHADOW_JSON_INT32:
			case SHADOW_JSON_INT16:
			case SHADOW_JSON_INT8:
			{
				*((int*)stateParam->value) = pSettings->getInt(stateMapping[i].path);
//				int	val = pSettings->getInt(stateMapping[i].path);
//				*((int*)stateParam->value) = val;
//				std::cout << "State: " << stateParam->key << ", val: " << *((int*)stateParam->value) << std::endl;
				break;
			}
			case SHADOW_JSON_UINT32:
			case SHADOW_JSON_UINT16:
			case SHADOW_JSON_UINT8:
			{
				*((unsigned int*)stateParam->value) = (unsigned int)pSettings->getInt(stateMapping[i].path);
				break;
			}
			case SHADOW_JSON_FLOAT:
			{
				*((float*)stateParam->value) = pSettings->getFloat(stateMapping[i].path);
				break;
			}
			case SHADOW_JSON_DOUBLE:
			{
				*((double*)stateParam->value) = pSettings->getDouble(stateMapping[i].path);
				break;
			}
			case SHADOW_JSON_BOOL:
			{
				*((bool*)stateParam->value) = pSettings->getBool(stateMapping[i].path);
				break;
			}
			case SHADOW_JSON_STRING:
			{
				*((std::string*)stateParam->value) = pSettings->getString(stateMapping[i].path);
				break;
			}
			default:
				break;
		} // of switch
		pthread_mutex_unlock(&gMutex);
	} // of for
}
void startTracker()
{
	if (pTracker && pTracker->isRunning())
	{
		// give warning and just return;
		return;
	}

	pthread_mutex_lock(&appMutex);

	bThreadsStop = false;


	pTracker.reset(new vml::MultiTargetVisionTracker(input_source_addr,pSettings));

	pTracker->setIOTCommunicator(pIOTc);

	// initialize tracker modules
	std::cout << "Initializing Tracker..." << std::endl;
	pTracker->initializeModules();

	std::cout << "Starting Tracker..." << std::endl;
	// initialize and start threads
	if (nProcessingThreads == 3)
	{
		pTracker->_3ThreadPipeliningStart();
	}
	else
	{
		pTracker->_2ThreadPipeliningStart();
	}
	std::cout << "Tracker started!" << std::endl;

	pthread_mutex_unlock(&appMutex);

	// start data report thread
	if(pthread_create(&dataReportThread, &threadAttr, dataReportThreadFunc, (void *)pTracker.get()))
	{
		std::cerr << "Error in creating dataReportThread" << std::endl;
		exit(0);
	}


}

void stopTracker()
{
	pthread_mutex_lock(&appMutex);

	bThreadsStop = true;	// stop each threads

	std::cout << "Stopping Tracker..." << std::endl;

	// stop data report thread
	pthread_join(dataReportThread,NULL);

	if (pTracker && pTracker->isRunning())
	{
		pTracker->stopThreads();
		std::cout << "Tracker stopped!" << std::endl;
		pTracker.reset();
	}
	std::cout << "All modules reset." << std::endl;
	pthread_mutex_unlock(&appMutex);

}

void resetSettings()
{
	pSettings->writeXml(settings_file_path.c_str(),false);	// save settings

	// flush file buffer
	sync();  // this is always successful.

	// wait for a sec to set the file just in case
	sleep(1);

	// reinitialize settings
	initializeSettings();

	// settings has been parsed. Update state
	updateStateFromSettings();
}

void initializeSettings()
{
	vml::FileParts	temp_file_parts;
	// pre-parse settings instance before state initialization.
	pSettings.reset(new SETTINGS);
	// parse settings file
	std::cout << "Reading settings at " << settings_file_path << std::endl;
	// check settings file exist
	if (! boost::filesystem::exists(settings_file_path))
	{
		exitError("Cannot find "+settings_file_path+". Aborting.");
	}
	pSettings->parseXml(settings_file_path,false);

	// get camera model name
	std::string cam_model_name = pSettings->getString("Camera/modelName","nonmodel",false);
	if (cam_model_name == "nonmodel")
	{
		// cannot find camera model name
		exitError("Cannot find Camera/modelName in settings.xml. Aborting.");
	}

	// read distortion parameters from camera_coverage.xml
	std::cout << "Reading camera coverage at " << camcoverage_file_path << std::endl;
	// check camera_coverage file exists
	if (!boost::filesystem::exists(camcoverage_file_path))
	{
		exitError("Cannot find "+camcoverage_file_path+". Aborting.");
	}

	vml::VisionCameraCoverage	camcover;
	camcover.readFromXMLFile(camcoverage_file_path);

	vml::VisionCameraCoverage::CameraModelInfo	caminfo;
	if (camcover.getCoverage(cam_model_name,caminfo))
	{
		// assign camera info
		pSettings->setFloat("Camera/ccdHeight",caminfo.ccd_height);
		pSettings->setFloat("Camera/ccdWidth",caminfo.ccd_width);
		pSettings->setDoubleVector("Camera/distortionParameters",caminfo.distortion_parameters);
	}
	else
	{
		std::cout << "Warning: camera model name "+cam_model_name+" is not found in camera coverage. Tracker will idle." << std::endl;
		// delete camera settings if exists
		pSettings->setFloat("Camera/ccdHeight",0.f);
		pSettings->setFloat("Camera/ccdWidth",0.f);
		pSettings->setString("Camera/distortionParameters","");
	}
}

void display_usage( const char *argv0 )
{

	vml::FileParts	cmd_name(argv0);
	printf("\n");

	printf("Usage:  %s [options] [input_source_add]\n", cmd_name.filename().c_str());
	printf("  Options:\n");
	printf("\t [-h | --help]            print this message\n");
	printf("\t [-s | --silent]          silent mode (default off)\n");
	printf("\t [-l | --loginterval      interval for process logging (default 60 sec)\n");
	printf("\t [-c | --config]          config path\n");
	printf("\t [-m | --mthread]         number of threads. [2 or 3] (default 3)\n");
	printf("\t [-6 | --640]             run 640x480 default test\n");
	printf("\t [-3 | --320]             run 320x240 default test\n");
	printf("\n\t If input source address is not provided, use default local host video streaming\n");
	printf("  Examples:\n");
	printf("\t %s http://192.168.0.159:8080/?action=stream.mjpg\n",cmd_name.filename().c_str());
	printf("\t %s -s direct\n",cmd_name.filename().c_str());
	printf("\n");

	exit(1);

}

int main(int argc, char* argv[])
{
	std::string	settingsPath = "";

	static struct option longopts[] = {
			{ "help", 		no_argument,		NULL, 		'h'},
			{ "silent",		no_argument,		NULL, 		's'},
			{ "loginterval",	required_argument,	NULL, 		'l'},
			{ "config",		required_argument,	NULL, 		'c'},
			{ "mthread", 	required_argument,  NULL,       'm'},
			{ "640",        no_argument,        NULL,       '6'},
			{ "320",        no_argument,        NULL,       '3'},
			{ NULL,			0,					NULL,		0}
	};

	int c;
	while ((c = getopt_long(argc, argv, "hsl:c:m:63d", longopts, NULL)) != -1)
	{
		switch (c)
		{
			case 's':
			{
				bVerbose = false;
				break;
			}
			case 'h':
			{
				display_usage(argv[0]);
				return 0;
			}
			case 'l':
			{
				dLogInterval = atof(optarg);
				break;
			}
			case 'c':
			{
				config_path = optarg;
				break;
			}
			case 'm':
			{
				nProcessingThreads = atoi(optarg);
				if (nProcessingThreads > 3) nProcessingThreads = 3;
				if (nProcessingThreads < 2) nProcessingThreads = 2;
				break;
			}
			case '6':
			{
				bTestRun = true;
				bRun320x240 = false;
				break;
			}
			case '3':
			{
				bTestRun = true;
				bRun320x240 = true;
				break;
			}
			default:
			{
				std::cerr << "Unrecognized commandline option -" << c << ". Aborting" << std::endl;
				display_usage(argv[0]);
				return (-1);
			}
		} // of switch (c)
	} // of while

	if (optind == argc)
	{
		if (bTestRun)
		{
			// test mode requested
			if (bRun320x240)
			{
				input_source_addr = TEST_VIDEO_320X240_FILE;
			}
			else
			{
				input_source_addr = TEST_VIDEO_640X480_FILE;
			}
		}
		else
		{
			input_source_addr = INPUT_VIDEO_URL;
		}
	}
	else {
		input_source_addr = argv[optind];
	}

	IoTGateway = new IoT_Gateway;

	// initialize settings path
	config_file_parts.setpath(config_path);
	config_file_parts.setfilename("settings");
	config_file_parts.setextention("xml");
	settings_file_path = config_file_parts.fullpath();
	config_file_parts.setfilename("camera_coverage");
	camcoverage_file_path = config_file_parts.fullpath();

	// set appType
	std::ostringstream appTypeStr;
	appTypeStr << appID;
	appType = appTypeStr.str();


	// initialize IoT
	std::string mqttTopicState, mqttTopicData;
	mqttTopicState.append(AWS_IOT_APP_STATE_CHANNEL_PREAMBLE);
	mqttTopicState.append("/");
	mqttTopicState.append(APP_ID_VISION_TRACKER);

	// -- AWS_IoT_Gateway will publish data to a topic 'data/<app_id>/<store_name_prefix>/<store_name_suffix>/<device_name>' (e.g., data/wifiRss/vmhq/16801/cam001)
	mqttTopicData.append(AWS_IOT_APP_DATA_CHANNEL_PREAMBLE);
	mqttTopicData.append("/");
	mqttTopicData.append(APP_ID_VISION_TRACKER);
	mqttTopicData.append("/");
	mqttTopicData.append(IoTGateway->GetMyStoreName());
	mqttTopicData.append("/");
	mqttTopicData.append(IoTGateway->GetMyDevName());

	// register delta state callback function
	IoTGateway->RegisterDeltaCallbackFunc(&IoT_DeltaState_CallBack_func);
	// register control channel callback function
	IoTGateway->RegisterControlCallbackFunc(&IoT_ControlChannel_CallBack_func);

	// start IoT threads
	IoTGateway->Run(APP_ID_VISION_TRACKER,mqttTopicState,mqttTopicData,IOT_GATEWAY_CONFIG_FILE);

	// instance of new IoTcommunicator to report error and heartbeat
	pIOTc.reset(new IOT_Communicator(APP_ID_VISION_TRACKER));

	initializeSettings();
	stateInitialization();
	updateStateFromSettings();

	// -- ######################################################################
	pthread_attr_init(&threadAttr);
	pthread_attr_setdetachstate(&threadAttr, PTHREAD_CREATE_JOINABLE);

	// start tracker
	startTracker();

	double	lastFrameRateReportTime = gCurrentTime();
	while (!bProcessStop)
	{
		sleep(nTrackerMonitorInterval);	// Monitor tracker status interval.

		if (pTracker && (!pTracker->isRunning()))
		{
			std::cout << "Tracker halted. Restarting tracker..." << std::endl;
			stopTracker();
			sleep(2);
			startTracker();
		}

		pthread_mutex_lock(&appMutex);

		if ((gCurrentTime() - lastFrameRateReportTime) > dLogInterval)
		{
			if (bVerbose && pTracker)
			{
				float fps = pTracker->getFrameRate();
				std::cout << "Processed with " << std::fixed << std::setprecision(2) << fps << "fps, (" << pTracker->getCurrentFrameNumber() << " frames)" << std::endl;
			}
			lastFrameRateReportTime = gCurrentTime();
		}
		pthread_mutex_unlock(&appMutex);
	}

	// delete IoT when exit
	if (IoTGateway)
	{
		delete(IoTGateway);
	}

	return 0;
}


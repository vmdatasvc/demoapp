///  \file    GUI.cpp
///  \brief   GUI library

#include "projects/WiFi_Shopper_Tracker/Config.hpp"
#include "projects/WiFi_Shopper_Tracker/GUI.h"
#include <stdio.h>
#include <stdlib.h>

using namespace cv;


//#define PROJ_MULT 2
#define PROJ_MULT 1
//#define FCL

#define TARGET_NAME "Baby"

//***************************************************************************
//Draws the uncertainty ellipse
//***************************************************************************
void draw_uncertainty_ellipse(int obj_cmd, int obj_id, float x, float y, IplImage *img,
		CvMat *cov_matrix, CvScalar color, int room_dim_x, int room_dim_y,
		float chi) {

	float x_ratio = (float)img->width/room_dim_x;
	float y_ratio = (float)img->height/room_dim_y;

	CvMat *R = cvCreateMat(2, 2, CV_32FC1);
	CvMat *D = cvCreateMat(2, 1, CV_32FC1);

	CvFont font;
	int thickness = 3*PROJ_MULT;

	cvInitFont(&font, CV_FONT_VECTOR0, 0.5*PROJ_MULT, 0.5*PROJ_MULT, 0, 1,
			CV_AA);

	cvSVD(cov_matrix, D, R);

	int center_x = (int)(x_ratio*x);
	int center_y = (int)(y_ratio*y);

	//	cvEllipse(img,center,cvSize((int)(x_ratio*(chi/sqrt(cvmGet(D,0,0)))),(int)(y_ratio*(chi/sqrt(cvmGet(D,1,0))))),0,0,360,CV_RGB(0,255,0),1,8,0);

	if(cvmGet(D, 0, 0) < 0.0001 || cvmGet(D, 1, 0) < 0.0001) {
		return;
	} else {

		int width = (int)(x_ratio*(chi/sqrt(cvmGet(D, 0, 0))));
		int height = (int)(y_ratio*(chi/sqrt(cvmGet(D, 1, 0))));

		if (((cvmGet(cov_matrix, 0, 0) != 0) || (cvmGet(cov_matrix, 0, 1) != 0)
				|| (cvmGet(cov_matrix, 1, 0) != 0) || (cvmGet(cov_matrix, 1, 1)
				!= 0))
			&& center_x < room_dim_x*2 && center_y < room_dim_y*2
			&& center_x >= 0 && center_y >= 0
			&& width >= 0 && height >= 0 && width < 1000 && height <= 1000
			&& thickness <= 255
			) {

			cvEllipse(img, cvPoint(center_x, center_y), cvSize(width, height), acos(cvmGet(R, 0, 0))*180/M_PI,
					0, 360, color, thickness, 8, 0);
		}
	}

	if (obj_id > 0) {
		char str_obj_id[99] = {'\0'};
		sprintf(str_obj_id, "%d", obj_id);
		cvPutText(img, str_obj_id, cvPoint((int)(x_ratio*x),(int)(y_ratio*y)),&font,CV_RGB(0,0,0));
	}


}
//***************************************************************************
//Draws a dashed line
//***************************************************************************
void draw_dashed_line(IplImage *img, CvPoint p1, CvPoint p2, CvScalar color) {

	float dx = (float)((p2.x-p1.x)/10);
	float dy = (float)((p2.y-p1.y)/10);

	int x = p1.x, y = p1.y;

	for (int i=0; i<5; i++) {

		cvLine(img, cvPoint(x, y), cvPoint(x+(int)dx, y+(int)dy), color, 3);

		x += (int)round(2*dx);
		y += (int)round(2*dy);
	}
}

////***************************************************************************
////Shows the floor plan of a space
////***************************************************************************
//void show_store_layout(IplImage *img, char *win_name, int room_dim_x, int room_dim_y) {
//
//	float x_ratio = (float)img->width/room_dim_x;
//	float y_ratio = (float)img->height/room_dim_y;
//
//	cv::Mat layout = cv::imread(STORE_LAYOUT_FILENAME, CV_LOAD_IMAGE_COLOR);
//	cv::Mat layout_resized;
//	cv::Size size(img->width, img->height);
//	cv::resize(layout, layout_resized, size);
//	IplImage
//
//}

#if defined(MANUAL_SYNTHETIC_MAP)

//***************************************************************************
//Shows the floor plan of a space
//***************************************************************************
void show_map(float x, float y, Mat &img, char *win_name, int room_dim_x,
		int room_dim_y) {

	float x_ratio = (float)img.size().width/room_dim_x;
	float y_ratio = (float)img.size().height/room_dim_y;

	static bool isAlreadyLoaded = false;
	static vector<CvPoint> linePts;		// -- Two consecutive points will be two edges of a line

	if(isAlreadyLoaded == false) {

		FILE *fp;
		char filename[255];
		sprintf(filename, GUI_MODEL_FILENAME);
		fp = fopen(filename, "r");
		char c[99];

		CvPoint pt_prev, pt_curr, var;

		while(fscanf(fp, "%s", c) != EOF) {
			if(c[0] == '#') {
				// -- Skip the line
				int c = 0;
				while (c != '\n') {
					c = fgetc(fp);
				};
				continue;
			} else if(c[0] == '*') {
					// -- This will be a beginning point of a line
					fscanf(fp, "%d %d", &pt_prev.x, &pt_prev.y);
					//printf("Beginning Line (%d, %d)\n", pt_prev.x, pt_prev.y);
					continue;
			} else {
				var.x = atoi(c);
				fscanf(fp, "%d", &var.y);
				var.x += (var.x > 0); var.x -= (var.x < 0);
				var.y += (var.y > 0); var.y -= (var.y < 0);

				pt_curr.x = pt_prev.x + var.x;
				pt_curr.y = pt_prev.y + var.y;
				linePts.push_back(pt_prev);
				linePts.push_back(pt_curr);

				//printf("Line (%d, %d) - (%d, %d)\n", pt_prev.x, pt_prev.y, pt_curr.x, pt_curr.y);

				pt_prev = pt_curr;
			}
		}

		// -- Normalize
		if(linePts.size() > 0)
			for(unsigned int i=0; i<linePts.size(); i++) {
				linePts.at(i).x *= x_ratio;
				linePts.at(i).y *= y_ratio;
			}

		fclose(fp);

		isAlreadyLoaded = true;
	}

	//white background
	// -- For Color chart, refer to http://www.theodora.com/html_colors.html
	rectangle(img,cvPoint(0,0),cvPoint(img.size().width,img.size().height),CV_RGB(0xff,0xdd,0x75),CV_FILLED,8,0);


	//draws the floor plan
	if(linePts.size() > 0)
		for(unsigned int i=0; i<linePts.size(); i += 2) {
			line(img, cvPoint(linePts.at(i).x, linePts.at(i).y),cvPoint(linePts.at(i+1).x, linePts.at(i+1).y), CV_RGB(0,0,155),3,8,0);
		}
}
#endif

//***************************************************************************
//Shows the position of the ball in the room
//***************************************************************************
void show_room(float x, float y, IplImage *img, char *win_name, int room_dim_x,
		int room_dim_y) {

	int line_dist = 30;
	float x_ratio = (float)img->width/room_dim_x;
	float y_ratio = (float)img->height/room_dim_y;

	CvFont font;

	cvInitFont(&font, CV_FONT_VECTOR0, 0.3*PROJ_MULT, 0.4*PROJ_MULT, 0, 1,
			CV_AA);

	char str_coords[30];

	//white background
#ifndef FCL
	cvRectangle(img,cvPoint(0,0),cvPoint(img->width,img->height),CV_RGB(255,255,255),CV_FILLED,8,0);

#else
	cvRectangle(img, cvPoint(0, 0), cvPoint(img->width, img->height), CV_RGB(0,0,0), CV_FILLED, 8, 0);
	//		cvRectangle(img,cvPoint(0,0),cvPoint(img->width,img->height),CV_RGB(0,0,0),CV_FILLED,8,0);
#endif		

	//draws the tiles
	for (int line_pos_x=(int)(15*x_ratio);line_pos_x<img->width;line_pos_x+= (int)(floor(line_dist*x_ratio))) {
		cvLine(img, cvPoint(line_pos_x,0),cvPoint(line_pos_x,img->height), CV_RGB(128,128,128),1,8,0);
	}

	for (int line_pos_y=0;line_pos_y<img->height;line_pos_y+= (int)(floor(line_dist*y_ratio))) {
		cvLine(img, cvPoint(0,line_pos_y),cvPoint(img->width,line_pos_y), CV_RGB(128,128,128),1,8,0);
	}


	//draws the object
	//		if ((x != 0) || (y != 0))
	//			cvCircle(img,cvPoint((int)(x_ratio*x),(int)(y_ratio*y)),10,CV_RGB(128,128,0),-1,8,0);
	//temporarily not showing the target - just uncertainty ellipses
	//probably will show the target after filtering


	//prints the coordinates of the target -> this should go to draw_uncertainty ellipse
	sprintf(str_coords, "(%3.2f,%3.2f)", x, y);
	cvPutText(img, str_coords, cvPoint(2, img->height-10), &font, CV_RGB(255,0,0));

}
//***************************************************************************
//Shows the markers on the floor
//***************************************************************************
void show_markers(int x_bot, int y_bot, int x, int y, IplImage *img,
		int room_dim_x, int room_dim_y) {

	float x_ratio = (float)img->width/room_dim_x;
	float y_ratio = (float)img->height/room_dim_y;

	int line_pos_x = (int)((15+x_bot*30-1)*x_ratio);
	int line_pos_y = (int)((y_bot*30-2)*x_ratio);

	for (int i=y_bot; i<y; i++) {
		for (int j=x_bot; j<x; j++) {
			//cvCircle(img,cvPoint(x_ratio*(j*35+15),y_ratio*(i*35)),2,CV_RGB(0,0,0),-1,8,0);
			cvLine(img, cvPoint(line_pos_x-2, line_pos_y), cvPoint(
					line_pos_x+2, line_pos_y), CV_RGB(0,0,0), 2, 8, 0);
			cvLine(img, cvPoint(line_pos_x, line_pos_y-2), cvPoint(line_pos_x,
					line_pos_y+2), CV_RGB(0,0,0), 2, 8, 0);
			line_pos_x += (int)(30*x_ratio);
		}
		line_pos_x = (int)((15+x_bot*30-1)*x_ratio);
		line_pos_y += (int)(30*y_ratio);
	}
}
//***************************************************************************
//Shows the furniture in the room
//***************************************************************************
void show_furniture(IplImage *img, int room_dim_x, int room_dim_y) {

	int x1, y1, x2, y2;

	float x_ratio = (float)img->width/room_dim_x;
	float y_ratio = (float)img->height/room_dim_y;

	//desk under the shelves
	x1 = (int)(4.5*12*2.54*x_ratio);
	y1 = (int)(24.5*12*2.54*y_ratio);
	x2 = (int)(room_dim_x*x_ratio);
	y2 = (int)(room_dim_y*y_ratio);

	cvRectangle(img, cvPoint(x1, y1), cvPoint(x2, y2), CV_RGB(154,80,14), CV_FILLED);

	//other big desk
	x1 = (int)(16*12*2.54*x_ratio);
//	y1 = (int)(8*12*2.54*y_ratio);
	y1 = (int)(0*12*2.54*y_ratio);
	x2 = (int)(room_dim_x*x_ratio);
	y2 = (int)(room_dim_y*y_ratio);

	cvRectangle(img, cvPoint(x1, y1), cvPoint(x2, y2), CV_RGB(154,80,14), CV_FILLED);

	//little desk by the video wall
//	x1 = (int)(16*12*2.54*x_ratio);
//	y1 = (int)(0*12*2.54*y_ratio);
//	x2 = (int)(room_dim_x*x_ratio);
//	y2 = (int)(2.5*12*2.54*x_ratio);

//	cvRectangle(img, cvPoint(x1, y1), cvPoint(x2, y2), CV_RGB(154,80,14), CV_FILLED);

	//video wall
	x1 = (int)(5*12*2.54*x_ratio);
	y1 = (int)(0*12*2.54*y_ratio);
	x2 = (int)(12.5*12*2.54*x_ratio);
	y2 = (int)(2*12*2.54*x_ratio);

	cvRectangle(img, cvPoint(x1, y1), cvPoint(x2, y2), CV_RGB(0,0,0), CV_FILLED);

	//small bump on the wall
	x1 = 0;
	y1 = (int)(13.5*12*2.54*y_ratio);
	x2 = (int)(0.5*12*2.54*x_ratio);
	y2 = (int)(17.5*12*2.54*x_ratio);

	cvRectangle(img, cvPoint(x1, y1), cvPoint(x2, y2), CV_RGB(180,180,180), CV_FILLED);

}



void show_sensors(WiFiSensor_short_Ptr ap, Mat &img, float fontSizeScale) {

//	float x_ratio = (float)img.size().width/room_dim_x;
//	float y_ratio = (float)img.size().height/room_dim_y;
//	float x, y;

	char str_coords[30];

	//draws all the camera centers
	//for (int i=0; i<num_cameras; i++)
	{

//		x = x_ratio*ap->mSensorPos_x;
//		y = y_ratio*ap->mSensorPos_y;

		CvScalar dev_color;

		if(ap->mIsCurrentlyReceivingMessages == true)
			dev_color = CV_RGB(0x63,0xe9,0xfc);
		else
			dev_color = CV_RGB(0x00,0x00,0x00);

		//printf("AP[%s]: (x,y) = (%d, %d) \n", ap->mIPaddr.c_str(), (int)x, (int)y);

		// -- For Color chart, refer to http://www.theodora.com/html_colors.html
		circle(img,cvPoint(ap->mSensorPos_x_map,ap->mSensorPos_y_map), 7 /*radius*/,dev_color,CV_FILLED,8,0);
		circle(img,cvPoint(ap->mSensorPos_x_map,ap->mSensorPos_y_map), 7 /*radius*/,CV_RGB(0xca,0x00,0xca),2 /*thickness, CV_FILLED*/,8,0);

		//sprintf(str_coords, "%s [%s]", ap->mSerialNumber.c_str(), ap->mIPaddr.c_str());
		//sprintf(str_coords, "%s [%s]", &ap->mSerialNumber.c_str()[ap->mSerialNumber.size()-4], ap->mIPaddr.c_str());
		//sprintf(str_coords, "%s [%s]", ap->mDevName.c_str(), ap->mIPaddr.c_str());
		sprintf(str_coords, "%s [%s][%s]", ap->mDevName.c_str(), &ap->mSerialNumber.c_str()[ap->mSerialNumber.size()-4], ap->mIPaddr.c_str());

        putText(img, str_coords, cvPointFrom32f(cvPoint2D32f(ap->mSensorPos_x_map-50, ap->mSensorPos_y_map+20)), 0, 0.4*PROJ_MULT*fontSizeScale,CV_RGB(0,0,255), 1, CV_AA);
	}
}



//void show_sensors(int sensorNo, CvPoint sensor_pos, int num_cameras, char *sensor_id, Mat &img, int room_dim_x, int room_dim_y) {
//
//	float x_ratio = (float)img.size().width/room_dim_x;
//	float y_ratio = (float)img.size().height/room_dim_y;
//
//	float x, y;
//
//	char str_coords[30];
//
//	//draws all the camera centers
//	//for (int i=0; i<num_cameras; i++)
//	{
//
//		x = x_ratio*sensor_pos.x;
//		y = y_ratio*sensor_pos.y;
//
//		// -- For Color chart, refer to http://www.theodora.com/html_colors.html
//		circle(img,cvPointFrom32f(cvPoint2D32f(x,y)), 10 /*radius*/,CV_RGB(0x63,0xe9,0xfc),CV_FILLED,8,0);
//		circle(img,cvPointFrom32f(cvPoint2D32f(x,y)), 10 /*radius*/,CV_RGB(0xca,0x00,0xca),2 /*thickness, CV_FILLED*/,8,0);
//
//		sprintf(str_coords, "%s", sensor_id);
//		putText(img, str_coords, cvPointFrom32f(cvPoint2D32f(x-20, y-15)), 0, 0.6*PROJ_MULT,CV_RGB(0,0,255), 1, CV_AA);
//	}
//}


//***************************************************************************
//Shows the cameras on the room
//***************************************************************************
void show_cameras(void *cluster_list, int num_cameras, CvMat **H,
		IplImage *img, int room_dim_x, int room_dim_y) {

	CvMat *point_tmp = cvCreateMat(3, 1, CV_32FC1);
	float x, y;//, xh, yh, xm, ym;

//	SET_CLUSTER::iterator cluster_iter;
//
//	cluster_type tmp_cluster;

	float x_ratio = (float)img->width/room_dim_x;
	float y_ratio = (float)img->height/room_dim_y;

	CvFont font;

	cvInitFont(&font, CV_FONT_VECTOR0, 0.5*PROJ_MULT, 0.5*PROJ_MULT, 0, 1,
			CV_AA);

	char str_coords[30];

	//int cluster_count = 0;

	//draws all the camera centers
	for (int i=0; i<num_cameras; i++) {

		//gets the camera position
		cvmSet(point_tmp, 0, 0, CAM_RES_X/2);
		cvmSet(point_tmp, 1, 0, CAM_RES_Y/2);
		cvmSet(point_tmp, 2, 0, 1);
		cvMatMul(H[i],point_tmp,point_tmp);

		//DEBUG
		//printf("camera center: (%f,%f)\n",cvmGet(point_tmp,0,0)/cvmGet(point_tmp,2,0),cvmGet(point_tmp,1,0)/cvmGet(point_tmp,2,0));


		x = x_ratio*cvmGet(point_tmp, 0, 0)/cvmGet(point_tmp, 2, 0);
		y = y_ratio*cvmGet(point_tmp, 1, 0)/cvmGet(point_tmp, 2, 0);

#ifndef FCL
		cvCircle(img,cvPointFrom32f(cvPoint2D32f(x,y)),5*PROJ_MULT,CV_RGB(100,100,100),CV_FILLED,8,0);
#else
		cvCircle(img, cvPointFrom32f(cvPoint2D32f(x, y)), 5*PROJ_MULT, CV_RGB(150,150,150), CV_FILLED, 8, 0);
#endif

		sprintf(str_coords, "%01d", i+1);
		cvPutText(img, str_coords, cvPointFrom32f(cvPoint2D32f(x-10, y-10)),
					&font, CV_RGB(0,0,255));
	}

	//draws the cluster head (blue)
//	for (cluster_iter=cluster_list->begin(); cluster_iter!=cluster_list->end(); cluster_iter++) {
//		tmp_cluster = *cluster_iter;
//
//		//gets the camera position
//		cvmSet(point_tmp, 0, 0, CAM_RES_X/2);
//		cvmSet(point_tmp, 1, 0, CAM_RES_Y/2);
//		cvmSet(point_tmp, 2, 0, 1);
//		cvMatMul(H[tmp_cluster.head-1],point_tmp,point_tmp);
//
//		xh = x_ratio*cvmGet(point_tmp, 0, 0)/cvmGet(point_tmp, 2, 0);
//		yh = y_ratio*cvmGet(point_tmp, 1, 0)/cvmGet(point_tmp, 2, 0);
//
//		cluster_count++;
//		sprintf(str_coords, "%01d", cluster_count);
//		cvPutText(img, str_coords, cvPointFrom32f(cvPoint2D32f(xh+10, yh-3)),
//				&font, CV_RGB(255,0,0));
//
//		//draws the cluster members (green)
//		for (int i=0; i<tmp_cluster.size; i++) {
//
//			//checks to make sure the cluster member does not belong to other cluster
//			//if (tmp_cluster.member_heads[i] == tmp_cluster.head) {
//			if (tmp_cluster.actual_members[i] != 0) {
//
//				//gets the camera position
//				cvmSet(point_tmp, 0, 0, CAM_RES_X/2);
//				cvmSet(point_tmp, 1, 0, CAM_RES_Y/2);
//				cvmSet(point_tmp, 2, 0, 1);
//				cvMatMul(H[tmp_cluster.actual_members[i]-1],point_tmp,point_tmp);
//
//				xm = x_ratio*cvmGet(point_tmp, 0, 0)/cvmGet(point_tmp, 2, 0);
//				ym = y_ratio*cvmGet(point_tmp, 1, 0)/cvmGet(point_tmp, 2, 0);
//
//				//draws the line between the cluster head and the connected neighbors
//#ifndef FCL
//				cvLine(img, cvPointFrom32f(cvPoint2D32f(xm, ym)),
//						cvPointFrom32f(cvPoint2D32f(xh, yh)), CV_RGB(200,200,0), 3);
//				cvCircle(img,cvPointFrom32f(cvPoint2D32f(xm,ym)),8*PROJ_MULT,CV_RGB(0,180,0),CV_FILLED,8,0);
//#else
//				cvLine(img, cvPointFrom32f(cvPoint2D32f(xm, ym)),
//						cvPointFrom32f(cvPoint2D32f(xh, yh)), CV_RGB(50,255,50), 3);
//				cvCircle(img, cvPointFrom32f(cvPoint2D32f(xm, ym)),
//						8*PROJ_MULT, CV_RGB(170,100,170), CV_FILLED, 8, 0);
//#endif
//				sprintf(str_coords, "%01d", cluster_count);
//				cvPutText(img, str_coords, cvPointFrom32f(cvPoint2D32f(xm+10,
//						ym-3)), &font, CV_RGB(255,0,0));
//
//			}
//
//		}
//
//		//draws lines between the cluster head and any neighbor which is
//		//not connected to anyone
//		for (int i=0; i<tmp_cluster.num_neighbors; i++) {
//			//			if ((tmp_cluster.members[i] != 0) && (tmp_cluster.member_heads[i] == 0)) {
//			if (tmp_cluster.members[i] != 0) {
//
//				//gets the camera position
//				cvmSet(point_tmp, 0, 0, CAM_RES_X/2);
//				cvmSet(point_tmp, 1, 0, CAM_RES_Y/2);
//				cvmSet(point_tmp, 2, 0, 1);
//				cvMatMul(H[tmp_cluster.members[i]-1],point_tmp,point_tmp);
//
//				xm = x_ratio*cvmGet(point_tmp, 0, 0)/cvmGet(point_tmp, 2, 0);
//				ym = y_ratio*cvmGet(point_tmp, 1, 0)/cvmGet(point_tmp, 2, 0);
//
//				//cluster number
//				//				sprintf(str_coords,"%01d",cluster_count);
//				//				cvPutText(img,str_coords,cvPoint(xm+10,ym+3),&font,CV_RGB(255,0,0));
//
//				//draws the line between the cluster head and the unconnected neighbors
//				//cvLine(img,cvPoint(xm,ym),cvPoint(xh,yh),CV_RGB(255,255,0));
//#ifndef FCL
//				draw_dashed_line(img,cvPointFrom32f(cvPoint2D32f(xm,ym)),cvPointFrom32f(cvPoint2D32f(xh,yh)),CV_RGB(200,200,0));
//#else
//				draw_dashed_line(img, cvPointFrom32f(cvPoint2D32f(xm, ym)),
//						cvPointFrom32f(cvPoint2D32f(xh, yh)), CV_RGB(0,200,0));
//#endif
//			}
//		}
//
//		//draws the cluster head
//#ifndef FCL
//		cvCircle(img, cvPointFrom32f(cvPoint2D32f(xh, yh)), 10*PROJ_MULT,
//				CV_RGB(0,0,180), CV_FILLED, 8, 0);
//#else
//		cvCircle(img, cvPointFrom32f(cvPoint2D32f(xh, yh)), 10*PROJ_MULT,
//				CV_RGB(0,0,255), CV_FILLED, 8, 0);
//#endif
//
//	}

}
//***************************************************************************
//Shows lines on the room
//***************************************************************************
void show_line(CvPoint p1, CvPoint p2, IplImage *img, CvScalar color,
		int room_dim_x, int room_dim_y) {

	float x_ratio = (float)img->width/room_dim_x;
	float y_ratio = (float)img->height/room_dim_y;

	cvLine(img, cvPoint((int)(p1.x*x_ratio), (int)(p1.y*y_ratio)), cvPoint(
			(int)(p2.x*x_ratio), (int)(p2.y*y_ratio)), color, 1, 8, 0);

}

//***************************************************************************
//Shows alert message
//***************************************************************************
void show_alert(int type, int xpos,int ypos,IplImage *img, int room_dim_x, int room_dim_y) {
	
	CvFont font;

	cvInitFont(&font, CV_FONT_VECTOR0, 1.5*PROJ_MULT, 1.5*PROJ_MULT, 0, 2.5*PROJ_MULT,
			CV_AA);

	int x_s = xpos * (float)img->width/room_dim_x;
	int y_s = ypos * (float)img->height/room_dim_y;

	cvPutText(img, "Alert!", cvPointFrom32f(cvPoint2D32f(x_s-50,
			y_s+80)), &font, CV_RGB(255,0,0));

//	if (type == AL_STOPPED) {
//		cvPutText(img, TARGET_NAME, cvPointFrom32f(cvPoint2D32f(x_s-55,
//				y_s+160)), &font, CV_RGB(255,0,0));
//		cvPutText(img, "not", cvPointFrom32f(cvPoint2D32f(x_s-50,
//				y_s+230)), &font, CV_RGB(255,0,0));
//		cvPutText(img, "moving", cvPointFrom32f(cvPoint2D32f(x_s-50,
//				y_s+300)), &font, CV_RGB(255,0,0));
//	} else if (type == AL_LOST) {
//		cvPutText(img, TARGET_NAME, cvPointFrom32f(cvPoint2D32f(x_s-55,
//				y_s+160)), &font, CV_RGB(255,0,0));
//		cvPutText(img, "lost", cvPointFrom32f(cvPoint2D32f(x_s-50,
//				y_s+230)), &font, CV_RGB(255,0,0));
//	}
	
	cvLine(img, cvPointFrom32f(cvPoint2D32f(x_s-15, y_s-15)),
			cvPointFrom32f(cvPoint2D32f(x_s+15, y_s+15)), CV_RGB(255,0,0), 4);

	cvLine(img, cvPointFrom32f(cvPoint2D32f(x_s-15, y_s+15)),
			cvPointFrom32f(cvPoint2D32f(x_s+15, y_s-15)), CV_RGB(255,0,0), 4);
}
//***************************************************************************
//DEBUG: show robot destination
//***************************************************************************
void show_dest(int xpos,int ypos,IplImage *img, int room_dim_x, int room_dim_y) {
	
	int x_s = xpos * (float)img->width/room_dim_x;
	int y_s = ypos * (float)img->height/room_dim_y;

	cvLine(img, cvPointFrom32f(cvPoint2D32f(x_s-10, y_s-10)),
			cvPointFrom32f(cvPoint2D32f(x_s+10, y_s+10)), CV_RGB(255,0,0), 3);

	cvLine(img, cvPointFrom32f(cvPoint2D32f(x_s-10, y_s+10)),
			cvPointFrom32f(cvPoint2D32f(x_s+10, y_s-10)), CV_RGB(255,0,0), 3);
}

//***************************************************************************
//Shows previous target positions
//***************************************************************************
void show_positions(VCT_POINTS *points,IplImage *img, int room_dim_x, int room_dim_y) {

   unsigned int i;
   point_type *tmp_point;
   
	int x_s = 0;
	int y_s = 0;
	
	int prev_x;
	int prev_y;
   
   for(i=0; i < points->size(); i++)
   {
	   tmp_point = &(*points)[i];
	   prev_x = x_s;
	   prev_y = y_s;
	   x_s = tmp_point->x * (float)img->width/room_dim_x;
	   y_s = tmp_point->y * (float)img->height/room_dim_y;
	   cvLine(img, cvPointFrom32f(cvPoint2D32f(x_s-5, y_s)),
				cvPointFrom32f(cvPoint2D32f(x_s+5, y_s)), CV_RGB(255,128,50), 2*PROJ_MULT);
	
	   cvLine(img, cvPointFrom32f(cvPoint2D32f(x_s, y_s+5)),
				cvPointFrom32f(cvPoint2D32f(x_s, y_s-5)), CV_RGB(255,128,50), 2*PROJ_MULT);
	   
	   if ((prev_x != 0) || (prev_y != 0))
			   cvLine(img, cvPointFrom32f(cvPoint2D32f(prev_x, prev_y)),
						cvPointFrom32f(cvPoint2D32f(x_s, y_s)), CV_RGB(255,128,50), 2*PROJ_MULT);
			   
   }

}


/*
 * RemoteDevice.cpp
 *
 *  Created on: Jun 27, 2014
 *      Author: pshin
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <pthread.h>

#include <algorithm>

#include "modules/utility/TimeKeeper.hpp"
#include "projects/WiFi_Shopper_Tracker/Config.hpp"
#include "projects/WiFi_Shopper_Tracker/RemoteDevice.hpp"

RemoteDevice::RemoteDevice() {
}

RemoteDevice::~RemoteDevice() {
}

void RemoteDevice::DevInitialize() {

	mTobeRemoved = false;

	mColor_r = random()%200;
	mColor_g = random()%200;
	mColor_b = random()%200;

	memset(mTime_stat.timediff_hist, '\0', sizeof(int)*TIMEDIFF_HIST_LENGTH);
	memset(mRSS_stat.rss_hist_per_ch, '\0', sizeof(int)*MAX_CHANNELS*MAX_RSS_IND_SIZE);
	memset(mRSS_stat.rss_hist_per_ch_r, '\0', sizeof(int)*MAX_CHANNELS*MAX_RSS_IND_SIZE);
	memset(mRSS_stat.rss_hist, '\0', sizeof(int)*MAX_RSS_IND_SIZE);
	memset(mRSS_stat.rss_hist_filtered, '\0', sizeof(int)*MAX_RSS_IND_SIZE);
	mDeviceManuf.clear();
	mRSS_stat.rss_hist_cnt = 0;
	mRSS_stat.rss_hist_per_ch_cnt = 0;
	mRSS_stat.rss_hist_per_ch_r_cnt = 0;
	mRSS_stat.rss_hist_filtered_cnt = 0;
	mChannel = 0;	// -- Since the AP's channel is not confirmed yet, it is set to be 0 as an initial value. If this device is AP, then the channel will be confirmed later.
	mProtocolType = -1;
	mDeviceType = -1;


	mMeasBuffer.clear();
	mKalmanAP_Signals.clear();
	mChosenSensorsForTriangulation.clear();

	mSiteInitialTime = mDeviceInitialTime = gCurrentTime();

	mTrackedPos_m_age = 0;
	mTrackedPos.clear();
	mTrackCorruption_age = 0;

	// -- Calculate the accumulation
	mTimediff_bin_width_accumulated[0] =  gTimediff_bin_width[0];
	for(unsigned int j=1; j<gNo_timediff_bin_width; j++) {
		mTimediff_bin_width_accumulated[j] = mTimediff_bin_width_accumulated[j-1] + gTimediff_bin_width[j];
	}

	//mIsDevOfInterest = false;
	mTotalShoppingTime = 0;
	mTotalShoppingTravelDist = 0;
	mTotalLocalizationCntDuringShopping = 0;
}

void RemoteDevice::PutToTrackedPosBuf(TrackedPos_t_ pos, int maxSize) {

	// -- The latest tracked position will be at front of the queue
	mTrackedPos.push_front(pos);
	mTrackedPos_m_age++;

	if((int)mTrackedPos.size() > maxSize)
		mTrackedPos.pop_back();
}

bool RemoteDevice::PutToMeasBuf(tcpdump_t meas, int maxSize) {

	// -- The oldest measurement will be at front of the queue
	if((int)mMeasBuffer.size() < maxSize) {
		mMeasBuffer.push_back(meas);
		return true;
	}
	return false;
}

void RemoteDevice::AddToTimeDiffHistogram(tcpdump_t meas) {
	if(DEBUG_FUNCTION_CALL) printf("AddToTimeDiffHistogram() is called\n");

	time_t time_curr;
	time(&time_curr);

	// -- Calculate the timediff  between previously received RSS measurement and the current measurement
	//meas->timediff = abs((int)difftime(mTime_stat.time_prev, time_curr));
	double timediff = abs((int)(difftime(mTime_stat.time_prev, time_curr) + 0.5));

	time(&mTime_stat.time_prev);

	int index = (int)timediff + 1;

	if(index >= TIMEDIFF_HIST_LENGTH)
		index = TIMEDIFF_HIST_LENGTH - 1;

	// -- Store it to histogram
	mTime_stat.timediff_hist[index]++;

	if(DEBUG_FUNCTION_CALL) printf("AddToTimeDiffHistogram() is Ended\n");
}

// -- Calculate the time different between two timestamp in seconds. Assumed that t1 is newer than t0.
// -- TODO: Would not work in case that t1 is the beginning of a month and t0 is the end of the previous month
//float RemoteDevice::time_diff_seconds(timestamp_t t0, timestamp_t t1) {
//	float timediff_day = (t1.date - t0.date);
//	float timediff_hour = timediff_day*24 + (t1.hour - t0.hour);
//	float timediff_min = timediff_hour*60 + (t1.minute - t0.minute);
//	float timediff_seconds = timediff_min*60 + (t1.second - t0.second);
//
//	return timediff_seconds;
//}

void RemoteDevice::ConstructTimeDiffCDF() {
	if(DEBUG_FUNCTION_CALL) printf("constructTimediffCDF() is called\n");

	// -- Construct a CDF statistic of time_diff between sampled RSS measurements from Stations
	// -- It is faster to see if it is an AP or not, since AP is mostly static and most likely already known.
	if(!(mDeviceType == AP))
	{
		// -- Calculate the count in each time interval bin from measurements
		unsigned int timediff_ind = 0;
		unsigned int timediff_cnt = 0;
		unsigned int ind_cnt = 0;		// -- Index of raw .timediff_hist[] measurements
		unsigned int timediff_bin_cnt_accumulated[gNo_timediff_bin_width];					// -- Total count in each time interval bin

		while(1) {

			timediff_cnt += mTime_stat.timediff_hist[ind_cnt];

			if(timediff_ind < gNo_timediff_bin_width && ind_cnt > mTimediff_bin_width_accumulated[timediff_ind]) {
				timediff_bin_cnt_accumulated[timediff_ind] = timediff_cnt;

				// -- Debug
				//printf("(in %2d sec, timediff_cnt) = (%3d, %3d)\n", timediff_ind+1, timediff_bin_width_accumulated[timediff_ind], timediff_cnt);

				timediff_ind++;
			}

			ind_cnt++;

			if(ind_cnt >= TIMEDIFF_HIST_LENGTH) {
				if(timediff_ind == gNo_timediff_bin_width) {
					timediff_ind--;
					timediff_bin_cnt_accumulated[timediff_ind] = timediff_cnt;
				} else if (timediff_ind < gNo_timediff_bin_width) {
					for(unsigned int m=timediff_ind; m< gNo_timediff_bin_width; m++)
						timediff_bin_cnt_accumulated[m] = timediff_cnt;
				}
				//printf("(accum[%2d], timediff_cnt) = (%3d, %3d)\n", timediff_ind, timediff_bin_width_accumulated[timediff_ind], timediff_cnt);
				timediff_ind++;
				break;
			}
		}

		// -- Convert the count in each bin to a percentage for CDF
		mTime_stat.timediff_total_cnt = timediff_cnt;
		for(unsigned int j=0; j<timediff_ind; j++) {
			mTime_stat.timediff_cdf[j] = (timediff_bin_cnt_accumulated[j]*100.f)/(float)mTime_stat.timediff_total_cnt;
		}

	}

	if(DEBUG_FUNCTION_CALL) printf("constructTimediffCDF() is Ended\n");
}

void RemoteDevice::ShowStats_RssHist_PerChannel() {

	if(mDeviceType == AP)
		printf("\n       AP[%s] ------------------------ Per-Channel RSS Histogram ----------------------- Total # : %d (%s) @Ch %d\n", mDeviceAddr.c_str(), mRSS_stat.rss_hist_per_ch_cnt, mDeviceManuf.c_str(), mChannel);
	else if(mDeviceType == OmniSensr)
		printf("\nOmniSensr[%s] ------------------------ Per-Channel RSS Histogram ----------------------- Total # : %d (%s) @Ch %d\n", mDeviceAddr.c_str(), mRSS_stat.rss_hist_per_ch_cnt, mDeviceManuf.c_str(), mChannel);
	else
		printf("\n  Station[%s] ------------------------ Per-Channel RSS Histogram ----------------------- Total # : %d (%s)\n", mDeviceAddr.c_str(), mRSS_stat.rss_hist_per_ch_cnt, mDeviceManuf.c_str());
	printf("dB:      0   -5  -10  -15  -20  -25  -30  -35  -40  -45  -50  -55  -60  -65  -70  -75  -80  -85  -90  -95\n");
	for(int m=0; m<MAX_CHANNELS; m++) {
		printf("Ch %2d:", m+1);
		for(int n=0; n<MAX_RSS_IND_SIZE; n++) {
			if(mRSS_stat.rss_hist_per_ch[m][n] == 0)	printf("   . ");
			else	printf("%4d ",mRSS_stat.rss_hist_per_ch[m][n]);
		}
		printf("\n");
	}
}

void RemoteDevice::ShowStats_RssHist_PerChannel_Revised() {
	double currTime = gCurrentTime();

	if(mDeviceType == OmniSensr)
		printf("\n%.2lf (%.2f) OmniSensr[%s] ------------------------ REVISED Per-Channel RSS Histogram ----------------------- Total # : %d (%s)\n", currTime - mSiteInitialTime, currTime - mDeviceInitialTime, mDeviceAddr.c_str(), mRSS_stat.rss_hist_per_ch_r_cnt, mDeviceManuf.c_str());
	else
		printf("\n%.2lf (%.2f) Station[%s] ------------------------ REVISED Per-Channel RSS Histogram ----------------------- Total # : %d (%s)\n", currTime - mSiteInitialTime, currTime - mDeviceInitialTime, mDeviceAddr.c_str(), mRSS_stat.rss_hist_per_ch_r_cnt, mDeviceManuf.c_str());

	printf("dB:      0   -5  -10  -15  -20  -25  -30  -35  -40  -45  -50  -55  -60  -65  -70  -75  -80  -85  -90  -95\n");
	for(int m=0; m<MAX_CHANNELS; m++) {
		printf("Ch %2d:", m+1);
		for(int n=0; n<MAX_RSS_IND_SIZE; n++) {
			if(mRSS_stat.rss_hist_per_ch_r[m][n] == 0)	printf("   . ");
			else	printf("%4d ",mRSS_stat.rss_hist_per_ch_r[m][n]);
		}
		printf("\n");
	}
}

void RemoteDevice::ShowStats_RssHist_Total() {
	double currTime = gTimeSinceStart();

	if(mDeviceType != AP) {
		if(mDeviceType == OmniSensr)
			printf("\n%.2lf (%.2f) OmniSensr[%s] ------------------------ Collected RSS Histogram ----------------------- Total # : %d (%s)\n", currTime - mSiteInitialTime, currTime - mDeviceInitialTime, mDeviceAddr.c_str(), mRSS_stat.rss_hist_cnt, mDeviceManuf.c_str());
		else
			printf("\n%.2lf (%.2f) Station[%s] ------------------------ Collected RSS Histogram ----------------------- Total # : %d (%s)\n", currTime - mSiteInitialTime, currTime - mDeviceInitialTime, mDeviceAddr.c_str(), mRSS_stat.rss_hist_cnt, mDeviceManuf.c_str());

		printf("dB:      0   -5  -10  -15  -20  -25  -30  -35  -40  -45  -50  -55  -60  -65  -70  -75  -80  -85  -90  -95\n      ");
			for(int n=0; n<MAX_RSS_IND_SIZE; n++) {
				if(mRSS_stat.rss_hist[n] == 0)	printf("   . ");
				else	printf("%4d ",mRSS_stat.rss_hist[n]);
			}
			printf("\n\n");

		if(mDeviceType == OmniSensr)
			printf("\n%.2lf (%.2f) OmniSensr[%s] ------------------------ Estimated RSS Histogram ----------------------- Total # : %d (%s)\n", currTime - mSiteInitialTime, currTime - mDeviceInitialTime, mDeviceAddr.c_str(), mRSS_stat.rss_hist_filtered_cnt, mDeviceManuf.c_str());
		else
			printf("\n%.2lf (%.2f) Station[%s] ------------------------ Estimated RSS Histogram ----------------------- Total # : %d (%s)\n", currTime - mSiteInitialTime, currTime - mDeviceInitialTime, mDeviceAddr.c_str(), mRSS_stat.rss_hist_filtered_cnt, mDeviceManuf.c_str());
		printf("dB:      0   -5  -10  -15  -20  -25  -30  -35  -40  -45  -50  -55  -60  -65  -70  -75  -80  -85  -90  -95\n      ");
			for(int n=0; n<MAX_RSS_IND_SIZE; n++) {
				if(mRSS_stat.rss_hist_filtered[n] == 0)	printf("   . ");
				else	printf("%4d ",mRSS_stat.rss_hist_filtered[n]);
			}
			printf("\n\n");
	}
}

void RemoteDevice::ShowStats_TimeDiffHistCDF() {
	double currTime = gTimeSinceStart();

	if(mDeviceType == AP)
		printf("\n%.3lf (%.2f)        AP[%s] ++++++++++++++++++++++++ Time Diff Histogram CDF +++++++++++++++++++++++++++++++++++ (%s) @Ch: %d\n     ",  currTime - mSiteInitialTime, currTime - mDeviceInitialTime, mDeviceAddr.c_str(), mDeviceManuf.c_str(), mChannel);
	else if(mDeviceType == OmniSensr)
		printf("\n%.3lf (%.2f) OmniSensr[%s] ++++++++++++++++++++++++ Time Diff Histogram CDF +++++++++++++++++++++++++++++++++++ (%s)\n     ",  currTime - mSiteInitialTime, currTime - mDeviceInitialTime, mDeviceAddr.c_str(), mDeviceManuf.c_str());
	else
		printf("\n%.3lf (%.2f)   Station[%s] ++++++++++++++++++++++++ Time Diff Histogram CDF +++++++++++++++++++++++++++++++++++ (%s)\n     ",  currTime - mSiteInitialTime, currTime - mDeviceInitialTime, mDeviceAddr.c_str(), mDeviceManuf.c_str());

	for(unsigned int j=0; j<gNo_timediff_bin_width; j++) printf("%4d", mTimediff_bin_width_accumulated[j]);
	printf("  [sec]\n");
	int cdf_percentage_decrement = RSS_CDF_INTERVAL;
	int cdf_prev = 0;
	for(int j=100; j>0; j -= cdf_percentage_decrement) {
		printf("%3d%%:", j);
		for(unsigned int m=0; m<gNo_timediff_bin_width; m++) {
			if((int)mTime_stat.timediff_cdf[m] >= j) {
				if(cdf_prev != (int)mTime_stat.timediff_cdf[m]) {
					printf("   + %d", (int)(mTime_stat.timediff_cdf[m]/100.0f*(float)mTime_stat.timediff_total_cnt + 0.5f));
					cdf_prev = (int)mTime_stat.timediff_cdf[m];
				} else
					printf("   +");
				break;
			}
			printf("   .");
		}
		printf("\n");
	}

	fflush(stdout);
}


=================================
< At Wi-Fi Packet Processor at each OmniSensr (OS) >

(1) Get, parse, and store the sniffed packets from tcpdump into a buffer
	- Src & dst address
	- Captured by what OS
	- RSS
	- Device type (i.e., AP or not. If a 'Probe Response' or 'Beacon' packet is sent from a device, this device must be an AP)
	- Packet type (i.e., Broadcast or unicast)
	- Timestamp
	
(1-1) We discard Unicast packets since broadcast packets are sent with the max tx power from mobile devices while unicast packets may have different tx power level, which we cannot identify. Thus, using only broadcast packets would give us more accurate results. One may argue that discarding unicast packets may decrease the detection rates of shoppers. It is true, but when a mobile device is sending unicast messages, it must be in an active communication mode, and so there must be broadcast messages as well to find nearby familiar APs. Thus, we should be able to detect the shoppers in a similar rate when buffering packets for summarization.

(2) Dispatch the collected packets in the AP buffer to each device buffer
	- Go through the buffered packets
	- Look for the buffered packets are from known sender in the device list
	- If so, transfer the packets in the OS buffer onto the device buffer
	- If not, create a new device, and transfer the packets

(3) Periodically perform the packet compression/summarization process for each device, which summarizes all the packets with the same src-dst pair during a period into a single measurement.
This is necessary to avoid any bias in case that multiple OSes captured packets from a src, yet the # of measurements captured at different OSes may be different.
This will also mitigate (1) any packet interference caused by multi-path fading and (2) the effect when an AP captures a packet at a channel yet the packet is actually transmitted at a different channel.
	- In each device's buffer, finds all the packets  with the same src

(4) Maintain two 2-second packet buffer to perform moving window-based 4-second median filtering for RSS from a src. At every 3 second, a 6-second median RSS for a src is reported to the server.

< At Wi-Fi Shopper Tracker at each retail store >

(1) In PacketStreamParsingThread in each MonitoringAP::run(), for each received packet from an AP, create a measurement data with 
	(1) The captured time (which is the arrival time at this server)
	(2) The address of the OmniSensr who captured this packet
	(3) The source of this packet
	(4) The RSS

(2) If this packet is from a new source, then create a new device buffer and transfer to it. Otherwise, transfer this measurement data to the buffer of the corresponding source.
	- If the source of this packet is either an AP or a service agent, then discard it. Now, each device would have one measurement from each AP that captured any packet from the device	

(3) Upon arrival of an RSS measurement, convert RSS into a Distance: By doing this, we know the distance from a device to each AP
	- We may use different calibration parameters for different operating systems such as Android and iOS.

(4) Each device at the server runs a Kalman filter for each OmniSensr who can capture packets from the device. Find the corresponding OmniSensr by looking up who captured this packet and update the Kalman filter to estimate the true distance between this device and the OmniSensr.


- In PeriodicBufferProcessingThread in main() -

(5) Estimate the actual location of the device by doing trilateration
	- [Sensor selection] Select a set of APs with "good/believable" measurements
		- [Weak signal pruning] Take only the APs with the stronger 50% of the RSS measurements by sorting the measurements from all OmniSensrs according to the distance from the device and by discarding the 50% measurements. 
			- Rationale: The stronger signals have better spatial resolution due to the wireless signal fading model.
		- [Abnormal sensor pruning] Cluster the selected APs based on their geographic location using a Mean-shift and select the APs only in the biggest cluster by further pruning out the APs in other clusters if any
			- Rationale: Stronger signals would be measured at the APs geographically close to the source. Thus the APs themselves should be close to each other. If there's any AP that is geographically distant from other APs, then it must be an erroneous one.
	- [Localization] Perform least-square minimization using the stronger signals to estimate the best location that is closest to all the selected OmniSensrs
	
	- (Optional) Select OmniSensrs among all, which have small distance errors from the estimated location (which imply that their measurements has low noise)
	- (Optional) Estimate again the best location only using the selected OmniSensrs. 
	- (Optional) Compute the distance change of both locations from the previous tracked location, and choose the cloest one.

(6) Update the position Kalman filter with the newly detected location.

(7) Compute the distance from the location estimated from the position Kalman filter to each OmniSensr.

(8) (Back-propagation) Update the distance Kalman filters corresponding to each OmniSensr. This will suppress any erroneous measurements and make all the measurements across OmniSensrs more consistent.

--------------------------------------------------------------
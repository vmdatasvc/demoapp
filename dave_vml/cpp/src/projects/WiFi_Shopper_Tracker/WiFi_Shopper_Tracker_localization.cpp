/*
 * WiFi_Shopper_Tracker_localization.cpp
 *
 *  Created on: Aug 2, 2016
 *      Author: paulshin
 */


#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <algorithm>
#include <cctype>
#include <iostream>
#include <tuple>
#include <limits>

#include <dlib/optimization.h>

#include <boost/algorithm/string/predicate.hpp>		// -- For boost::iequals() for case insensitive comparison


#include "modules/utility/TimeKeeper.hpp"
#include "modules/utility/ParseConfigTxt.hpp"

#include "projects/WiFi_Shopper_Tracker/Config.hpp"
#include "projects/WiFi_Shopper_Tracker/MonitoringAP.hpp"
#include "projects/WiFi_Shopper_Tracker/WiFi_Shopper_Tracker.hpp"


using namespace std;

extern pthread_mutex_t gMutex;

// ----------------------------------------------------------------------------------------

typedef dlib::matrix<double,2,1> localizationInputVector;
typedef dlib::matrix<double,2,1> localizationParameterVector;

// ----------------------------------------------------------------------------------------

// We will use this function to generate data.  It represents a function of 2 variables
// and 3 parameters.   The least squares procedure will be used to infer the values of
// the 3 parameters based on a set of input/output pairs.
double localizationModel (
    const localizationInputVector& ap_POS,
    const localizationParameterVector& params
)
{
    const double X = params(0);
    const double Y = params(1);
    //const double p2 = params(2);

    const double pos_x = ap_POS(0);
    const double pos_y = ap_POS(1);

    const double ret = sqrt(pow(X-pos_x,2) + pow(Y-pos_y,2));

    return ret;
}

// ----------------------------------------------------------------------------------------

// This function is the "residual" for a least squares problem.   It takes an input/output
// pair and compares it to the output of our model and returns the amount of error.  The idea
// is to find the set of parameters which makes the residual small on all the data pairs.
// -- data.first := pos_AP
// -- data.second := distToAP
// -- params := (X, Y)
double localizationModelResidual_Weighted(
    const pair<localizationInputVector, double>& data,
    const localizationParameterVector& params
)
{
	double errorToBeMinimized;

	// -- We want to minimize { (Distance from the currently estimated (X, Y) to the AP) - (Distance from the true (x,y) to the AP, which is estimated based on RSS) }

	// -- Method 1: Weighted circular algorithm based on a weighted least square error criterion

	// -- based on "Weighted Least Squares Techniques for Improved Received Signal Strength Based Localization", Paula Tarrio et al., 2011
	// -- This method give weights on the measurement in such a way that a weight is calculated based on the estimated distance from the AP to the signal source,
	// -- under the assumption that the closer node's measurement (i.e., stronger signal) would have lower uncertainty.

	// -- Cost Function: Error = 1/d^2 * {((X-x')^2 + (Y-y')^2)^0.5 - d}^2

	double a = localizationModel(data.first, params);
	double b = data.second;

	errorToBeMinimized = 1.f/pow(b,2) * pow(a - b, 2) * 100.0f;
	//errorToBeMinimized = 1.f/fabs(b) * pow(a - b, 2) * 10000.0f;
	//errorToBeMinimized = 1.f/log(fabs(b)) * pow(a - b, 2) * 10.0f;

    return errorToBeMinimized;
}

// This function is the "residual" for a least squares problem.   It takes an input/output
// pair and compares it to the output of our model and returns the amount of error.  The idea
// is to find the set of parameters which makes the residual small on all the data pairs.
// -- data.first := pos_AP
// -- data.second := distToAP
// -- params := (X, Y)
double localizationModelResidual_Plain(
    const pair<localizationInputVector, double>& data,
    const localizationParameterVector& params
)
{
	double errorToBeMinimized;

	// -- We want to minimize { (Distance from the currently estimated (X, Y) to the AP) - (Distance from the true (x,y) to the AP, which is estimated based on RSS) }

	// -- Cost Function: Error = ((X-x')^2 + (Y-y')^2)^0.5 - d

	double a = localizationModel(data.first, params);
	double b = data.second;

	// -- Method 2: Plain circular algorithm based on a least square error criterion
	errorToBeMinimized = a - b;

    return errorToBeMinimized;
}
// ----------------------------------------------------------------------------------------

// This function is the derivative of the residual() function with respect to the parameters.
localizationParameterVector localizationModelResidual_Weighted_Derivative(
    const pair<localizationInputVector, double>& data,
    const localizationParameterVector& params
)
{

	// -- Cost Function: f() = Error = 1/d^2 * {((X-x')^2 + (Y-y')^2)^0.5 - d}^2
	// -- Derivative of the cost function, f(),:
	/*
	 * Let a = (X-x')^2 + (Y-y')^2
	 * A = a^0.5
	 *
	 * Then, f() = 1/d^2 * (A - d)^2
	 *
	 * df()/dx = 2/d^2 * (A - d) * dA/dx
	 * df()/dy = 2/d^2 * (A - d) * dA/dy
	 *
	 * dA/dx = 0.5 * a^(-0.5)*da/dx = (X-x')/A
	 * dA/dy = 0.5 * a^(-0.5)*da/dx = (Y-y')/A
	 *
	 * Thus,
	 * df()/dx = 2/d^2 * (A - d) * (X-x')/A
	 * df()/dy = 2/d^2 * (A - d) * (Y-y')/A
	 */

    localizationParameterVector der;

    const double X = params(0);
    const double Y = params(1);

    const double pos_x = data.first(0);
    const double pos_y = data.first(1);

    // -- Let a = (X-x')^2 + (Y-y')^2
    const double a = pow(X-pos_x,2) + pow(Y-pos_y,2);

    // -- Let A = a^0.5
    const double A = sqrt(a);

    // -- Partial derivative with respect to X (i.e., dX) of A
    double dA_dx = (X-pos_x)/sqrt(a);

    // -- Partial derivative with respect to Y (i.e., dY) of A
    double dA_dy = (Y-pos_y)/sqrt(a);

    // -- Common term: 2/d^2 * (A - d)
    double commonTerm = 2.0f/pow(data.second,2) * (A - data.second);

    // -- Partial derivative with respect to X (i.e., dX)
    der(0) = commonTerm * dA_dx;

    // -- Partial derivative with respect to Y (i.e., dY)
    der(1) = commonTerm * dA_dy;

    return der;
}

// This function is the derivative of the residual() function with respect to the parameters.
localizationParameterVector localizationModelResidual_Plain_Derivative(
    const pair<localizationInputVector, double>& data,
    const localizationParameterVector& params
)
{
    localizationParameterVector der;

    const double X = params(0);
    const double Y = params(1);

    const double pos_x = data.first(0);
    const double pos_y = data.first(1);

    const double temp_sq = pow(X-pos_x,2) + pow(Y-pos_y,2);

    // -- Partial derivative with respect to X (i.e., dX) of temp
    der(0) = (X-pos_x)/sqrt(temp_sq);

    // -- Partial derivative with respect to Y (i.e., dx) of temp
    der(1) = (Y-pos_y)/sqrt(temp_sq);

    return der;
}

// -- Refer to http://xoax.net/cpp/ref/cpp_examples/incl/mean_med_mod_array/
int GetMedian(int daArray[], int iSize) {
    // Allocate an array of the same size and sort it.
    //double* dpSorted = new double[iSize];
	int dpSorted[iSize];
    for (int i = 0; i < iSize; ++i) {
        dpSorted[i] = daArray[i];
    }
    for (int i = iSize - 1; i > 0; --i) {
        for (int j = 0; j < i; ++j) {
            if (dpSorted[j] > dpSorted[j+1]) {
            	int dTemp = dpSorted[j];
                dpSorted[j] = dpSorted[j+1];
                dpSorted[j+1] = dTemp;
            }
        }
    }

    // Middle or average of middle values in the sorted array.
    int dMedian = 0.0;
    if ((iSize % 2) == 0) {
        dMedian = (int)((dpSorted[iSize/2] + dpSorted[(iSize/2) - 1])/2.0f+0.5f);
    } else {
        dMedian = dpSorted[iSize/2];
    }
    //delete [] dpSorted;
    return dMedian;
}

// -- Perform a summarization process at a Device, which summarizes all the packets with the same Device-AP pair into a single measurement
// -- This is necessary to avoid any bias in case that there are multiple packets from a single AP for the same device. This may happen because of the QoS in MQTT communication.
bool measurementCompression(vector<tcpdump_t> &measBuf, vector<tcpdump_t> &compressedMeas) {
	compressedMeas.clear();

		for(vector<tcpdump_t>::iterator it = measBuf.begin(); it !=  measBuf.end(); ++it) {
			tcpdump_t *meas = &*it;
			if(meas == NULL) continue;

			if(meas->rss < 0 && meas->rss >= -100 && meas->src.empty() == false) {
				float rss_avg = 0;
//				int rss_max = -999;
				int cnt = 0;

//				int rsss[measBuf.size()];
//				int rss_median = 0;

				for(vector<tcpdump_t>::iterator itt = it; itt !=  measBuf.end(); ++itt) {
					tcpdump_t *meas_rest = &*itt;
					if(meas_rest == NULL) continue;

					//if(meas->capturedBy.compare(meas_rest->capturedBy) == 0 && meas_rest->rss < 0)	// -- When doing at WiFiTracker
					if(boost::iequals(meas->capturedBy, meas_rest->capturedBy) == true && meas_rest->rss < 0 && meas_rest->rss > -100)			// -- When doing at WiFiTracker
					//if(meas->src.compare(meas_rest->src) == 0 && meas_rest->rss < 0)	// -- When doing at OmniSensr
					{

						rss_avg += (float)meas_rest->rss;
//						rss_max = max(rss_max, meas_rest->rss);
//						rsss[cnt] = meas_rest->rss;
						cnt++;

						// -- Mark this measurement to indicate it is already processed
						meas_rest->rss = 255;
					}
				}

				if(cnt > 0) {
					rss_avg /= (float)cnt;
//					rss_median = GetMedian(rsss, cnt);

					tcpdump_t newMeas = *meas;

//					newMeas.rss = rss_median;
					newMeas.rss = rss_avg;
//					newMeas.rss = rss_max;

					compressedMeas.push_back(newMeas);
				}
			}
		}

	return true;
}


void WiFi_Shopper_Tracker::periodicMeasurementProcessing_per_Station(RemoteDevicePtr dev) {		// -- called in PacketAnalyzerThread_TCPIP
	if(DEBUG_FUNCTION_CALL) printf("periodicMeasurementProcessing_per_Station() is called. \n");

	if(dev == NULL) {
		if(DEBUG_FUNCTION_CALL) printf("periodicMeasurementProcessing_per_Station() is Ended. 0\n");
		return;
	}

	//MeasurementStorate_t measBuf;
	vector<tcpdump_t> compressedMeas;

	if(USE_MEASUREMENT_SUMMRIZATION == true) {
		vector<tcpdump_t> compressedMeas_temp;

		compressedMeas_temp = *(dev->AtMeasBuf());

		if(compressedMeas_temp.size() > 0) {
			if(measurementCompression(compressedMeas_temp, compressedMeas) == false) {
				// -- DO something

				dev->ClearMeasBuf();

				if(DEBUG_FUNCTION_CALL) printf("periodicMeasurementProcessing_per_Station() is Ended. 1\n");
				return;
			}
		} else {
			return;
		}
	} else {
		compressedMeas = *(dev->AtMeasBuf());
	}



	bool isDevOfInt = isDevicesOfInterest(dev->GetDevAddr());
	bool isOmniSensr = dev->GetDeviceType() == OmniSensr;

	// -- The # of the compressedMeas packets in the buffer for a device should be the # of APs who captured packets  from the device
	// -- If the # of the summarized packets is at least three, then we can triangulate or perform fingerprint-based location estimation.

	// -- (5) Convert RSS into a Distance
	// -- By doing this, we know the distance of a device from each AP
	{
		WiFiSensor_short_Ptr ap;
		if(compressedMeas.size() > 0)
			for(vector<tcpdump_t>::iterator it = compressedMeas.begin(); it !=  compressedMeas.end(); ++it) {
				tcpdump_t *meas = &*it;
				if(meas == NULL) continue;

				ap = findRunningWiFiSensor(meas->capturedBy);

				if(ap != NULL) {

					//printf("compressedMeas[%d].src = %s\n", i, meas->src.c_str());

					// -- RSS-to-Distance conversion
					// -- Channel 1, 6, and 11 would be stored in Bin 0, 1, and 2, respectively.
					int ind_ch = ConvertChannelIntoIndex(meas->channel);

					if(ind_ch >= 0 && ind_ch < NUM_CHANNELS_TO_CAPTURE) {
						meas->distToAP = ap->convertRSStoDist_log(meas->rss, ind_ch, dev->mPerDevRssCalibOffset, dev->GetDeviceType() == OmniSensr);

					} else {
						meas->distToAP = -9999;
					}

					if((mOnlyDevOfInterest && (isDevOfInt || isOmniSensr))
							|| !mOnlyDevOfInterest )
						showParsedMessage(meas);
				} else {
					meas->distToAP = -9999;
				}
			}
	}

	*(dev->AtMeasBuf()) = compressedMeas;

	if(DEBUG_FUNCTION_CALL) printf("periodicMeasurementProcessing_per_Station() is Ended. 2\n");
}


int WiFi_Shopper_Tracker::periodicProcessing_per_Station(RemoteDevicePtr dev) {		// -- called in PacketAnalyzerThread_TCPIP
	if(DEBUG_FUNCTION_CALL) printf("periodicProcessing_per_Station() is called. \n");

	if(dev == NULL)
		return ERROR_NULL_OMNISENSR_POINTER;

	vector<tcpdump_t> compressedMeas;

	compressedMeas = *(dev->AtMeasBuf());
	dev->ClearMeasBuf();

	bool isDevOfInt = isDevicesOfInterest(dev->GetDevAddr());


//	if(mDebugLocalization)
//		if(dev->GetDeviceType() == OmniSensr) {
//			printf("OS[%s] compressedMeas.size() = %d\n", dev->GetDevAddr().c_str(), (int)compressedMeas.size());
//		}

//	#if ONLY_FOR_DEVICES_OF_INTEREST
//	if(isDevOfInt)
//	#endif
//		if(mDebugVerboseLevel > 1)	printf("Node[%s] periodicProcessing_per_Station is called. compressedMeas.unProcessedMeasSize = %d\n", dev->GetDevAddr().c_str(), (int)compressedMeas.size());

	//if(compressedMeas.unProcessedMeasSize > MAX_MEASUREMENT_BUFFER_PER_DEV || compressedMeas.unProcessedMeasSize <= 0) {
//	if(dev->mMeasBuffer_m.size() > MAX_MEASUREMENT_BUFFER_PER_DEV || compressedMeas.unProcessedMeasSize <= 0) {
//
//		#if ONLY_FOR_DEVICES_OF_INTEREST
//		//if(strncmp((const char*)dev->getAddr().c_str(), (const char*)ONLY_FOR_THIS_CELL_WIFI_ADDR, 17) == 0)
//		//if(isDevicesOfInterest(dev->GetDevAddr()) == true)
//		if(isDevOfInt)
//		#endif
//			if(mDebugVerboseLevel > 1) printf("Node[%s] periodicProcessing_per_Station is called. compressedMeas.unProcessedMeasSize = %d CORRUPTED\n", dev->getAddr().c_str(), compressedMeas.unProcessedMeasSize);
//
//		dev->setUnprocessedMeasSize(0);
//
//		if(DEBUG_FUNCTION_CALL) printf("periodicProcessing_per_Station() is ended. at 1 \n");
//
//		return;
//	}

	//double currTime = gTimeSinceStart();
	double currTime = gCurrentTime();

	// -- Perform a summarization process for each AP, which summarizes all the packets with the same src-dst pair into a single measurement
	// -- This is necessary to avoid any bias in case that multiple APs captured packets from a src, yet the # of measurements captured at each AP is different.



	// -- The # of the compressedMeas packets in the buffer for a device should be the # of APs who captured packets  from the device
	// -- If the # of the summarized packets is at least three, then we can triangulate or perform fingerprint-based location estimation.

	// -- (5) Convert RSS into a Distance
	// -- By doing this, we know the distance of a device from each AP
	{
		WiFiSensor_short_Ptr ap;
		if(compressedMeas.size() > 0)
			for(vector<tcpdump_t>::iterator it = compressedMeas.begin(); it !=  compressedMeas.end(); ++it) {
				tcpdump_t *meas = &*it;
				if(meas == NULL) continue;

				if(meas->distToAP <= 0 || meas->distToAP > 10000)
					continue;

				ap = findRunningWiFiSensor(meas->capturedBy);

				if(ap != NULL) {

					// -- (6) Update a Kalman filter to estimate the RSS values for each src-dst pair
					{
						KalmanFilter_4S *kal = findKalmanSignal(dev, ap);

						if(kal != NULL) {

#ifdef USE_RADIO_FINGERPRINT_BASED_LOCALIZATION

							 // -- Kalman filter for this AP is already created

								 if(dev->mKalmanAP_Signals[j].GetKalmanAge() == 0) {
									 dev->mKalmanAP_Signals[j].InitializeStates(meas->rss, meas->rss);
								 }

								dev->mKalmanAP_Signals[j].Update(meas->rss, meas->rss);

								if(mDebugVerboseLevel > 1)
								{
									dev->mKalmanAP_Signals[j].get_state(state);

									printf("Dev[%s] @AP[%s] (rss_raw, rss_kalman) = (%d, %.2f)\n", dev->GetDevAddr().c_str(), ap->mIPaddr.c_str(), meas->rss, state[0]);
								}

#else

								 if(kal->GetKalmanAge() == 0
										 || kal->mNeedToInitialize == true
										 ) {
									 kal->Initialize(meas->distToAP, meas->distToAP, kalman_signal_covariance_11, kalman_signal_covariance_33);
									 kal->mNeedToInitialize = false;

	//									if(isDevOfInt == true) {
	//										double sta[4];
	//										kal->get_state(sta);
	//										printf("Node[%s] AP[%s] INITIALIZED (rss, distToAP) = (%d, %.2f)\n", meas->src.c_str(), ap->mIPaddr.c_str(), meas->rss, meas->distToAP);
	//									}
								 }

								 double sta_prev[4];
								 kal->get_state(sta_prev);

								 kal->Update(meas->distToAP, meas->distToAP);

								 double sta_curr[4];
								 kal->get_state(sta_curr);

								 if(sta_curr[0] <= 0 || sta_curr[0] > mReliableDistanceInMeas_max*2) {
									 kal->Initialize(sta_prev[0], sta_prev[1], kalman_signal_covariance_11, kalman_signal_covariance_33);
									 kal->mNeedToInitialize = true;
								 }

								// -- Hard reset the state if we don't want to utilize Kalman filtering for RSS
								 if(mKalmanRssEnable == false) {
									if(meas->distToAP > 0 && meas->distToAP <= mReliableDistanceInMeas_max*2) {
//										 double newState[4] = {meas->distToAP, meas->distToAP, sta_curr[2], sta_curr[3]};
//										 kal->set_state(newState);

										 kal->Initialize(meas->distToAP, meas->distToAP, kalman_signal_covariance_11, kalman_signal_covariance_33);
									} else {
										 kal->Initialize(sta_prev[0], sta_prev[1], kalman_signal_covariance_11, kalman_signal_covariance_33);
										 kal->mNeedToInitialize = true;
									}
								 }


//							if(isDevOfInt == true) {
//								double sta[4];
//								kal->get_state(sta);
//								printf("Node[%s] AP[%s] (rss, distToAP, estimated distToAP) = (%d, %.2f, %.2lf)\n", meas->src.c_str(), ap->mIPaddr.c_str(), meas->rss, meas->distToAP, sta[0]);
//							}

#endif
						} else {

							printf("Can't find corresponding mKalmanAP_Signals. Something weird: dev[%s], dev->GetKalmanSignalListSize() = %d \n", dev->GetDevAddr().c_str(), (int)dev->GetKalmanSignalListSize());

							if(mDebugVerboseLevel > 1)
							{
								for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it)
								{
									WiFiSensor_short_Ptr ap = *it;
									printf("ap[%s] is at (%d, %d)\n", ap->mIPaddr.c_str(), ap->mSensorPos_x, ap->mSensorPos_y);
								}

								for(list<KalmanFilter_4S>::iterator it = dev->mKalmanAP_Signals.begin(); it != dev->mKalmanAP_Signals.end(); ++it) {
									KalmanFilter_4S *kal = &*it;
									if(kal == NULL) continue;
									printf(" (mCenterOfSensingField_X, mCenterOfSensingField_Y) = (%d, %d)\n", kal->mCenterOfSensingField_X, kal->mCenterOfSensingField_Y);
								}
							}
						}
					}

					if((mOnlyDevOfInterest && isDevOfInt == true )
							|| !mOnlyDevOfInterest )
						if(mDebugVerboseLevel >= 2)
							printf("$$$ Node[%s] @AP[%s]: rss = %d, dist = %.2f\n",
									meas->src.c_str(), ap->mIPaddr.c_str(), meas->rss, meas->distToAP);
				} else {
					meas->distToAP = numeric_limits<float>::max();

					if((mOnlyDevOfInterest && isDevOfInt == true )
							|| !mOnlyDevOfInterest )
						showParsedMessage(meas);

					// -- Debug
					printf("Can't find corresponding AP. Something weird: capturedBy = %s \n", meas->capturedBy.c_str());
				}
			}

//		if(SHOW_STAT_HISTOGRAM)
//		{
			if((int)compressedMeas.size() > 0)
				for(vector<tcpdump_t>::iterator it = compressedMeas.begin(); it !=  compressedMeas.end(); ++it) {
					tcpdump_t *meas = &*it;
					if(meas == NULL) continue;

					int rss_ind = abs(meas->rss)/RSS_CDF_INTERVAL;
					if(rss_ind < MAX_RSS_IND_SIZE && meas->distToAP != -99) {
						dev->mRSS_stat.rss_hist_filtered[rss_ind]++;
						dev->mRSS_stat.rss_hist_filtered_cnt++;
					}

					// -- Process only one sample during the sampling period if there is.
					// -- Process this only AFTER packetSummarization();
					dev->AddToTimeDiffHistogram(*meas);		// TODO: This would cause Seg Fault error. Need to find out!
				}
//		}

		// -- Estimate the current location of the device based on either Trilateration- or Fingerprint-based position mapping
        Point pt;
		pt.x = pt.y = 0;

		// -- (7) Estimate the actual location of the device by doing trilateration
		float distAvgErrorSquared = locationEstimation_Trilateration(dev, &pt);

		if(distAvgErrorSquared > 0 && pt.x != 0 && pt.y != 0) {

				// -- See if the current localization is valid by checking if the estimated location is inside the convexhull of the participating nodes.
				if((int)dev->mChosenSensorsForTriangulation.size() >= mNumSensorForLocalization_min) {

					float vertx[dev->mChosenSensorsForTriangulation.size()+1];
					float verty[dev->mChosenSensorsForTriangulation.size()+1];
					int ind = 0;
					for(vector<tuple<WiFiSensor_short_Ptr, double, double> >::iterator it = dev->mChosenSensorsForTriangulation.begin(); it != dev->mChosenSensorsForTriangulation.end(); ++it) {
						WiFiSensor_short_Ptr sensr = get<0>(*it);
						vertx[ind] = sensr->mSensorPos_x;
						verty[ind] = sensr->mSensorPos_y;
						ind++;
					}

					// -- Insert the latest location as well if it is not too far from the BoundingBox of the participating nodes
                    Point2f closestPtInConvexHull;
					bool isInConvexHull = false;
					int boundingBoxMargin = mLocalizationBoundingBoxMargin;
					if((int)dev->AtTrackedPosBuf()->size() > 1) {
						//isInConvexHull = isInsidePolygon(ind, vertx, verty, (float)pos_st[0], (float)pos_st[1]);
						isInConvexHull = isInsideBoundingBox(ind, vertx, verty, boundingBoxMargin, (float)pt.x, (float)pt.y, closestPtInConvexHull.x, closestPtInConvexHull.y);
						if(isInConvexHull == false) {
							TrackedPos_t_ loc_latest =  *(dev->AtTrackedPosBuf()->begin());
							float distToBoundingBox = sqrt(pow(loc_latest.pos.x - closestPtInConvexHull.x,2) + pow(loc_latest.pos.y - closestPtInConvexHull.y,2));

							if(distToBoundingBox < mLocalizationMaxDistFromConvexHull) {
									TrackedPos_t_ loc_prev =  *(dev->AtTrackedPosBuf()->begin());
									vertx[ind] = loc_prev.pos.x;
									verty[ind] = loc_prev.pos.y;
									ind++;
							}
						}
					}

					// -- Insert the latest location as well if it is not too far to the BoundingBox of the participating nodes
//						if((int)dev->AtTrackedPosBuf()->size() > 1) {
//							TrackedPos_t_ loc_prev =  *(dev->AtTrackedPosBuf()->begin());
//							vertx[ind] = loc_prev.pos.x;
//							verty[ind] = loc_prev.pos.y;
//							ind++;
//						}

				if(isInConvexHull == false) {
					if((int)dev->AtTrackedPosBuf()->size() < 2) {
						boundingBoxMargin *= 2;
					} else if((int)dev->AtTrackedPosBuf()->size() < 5) {
						boundingBoxMargin *= 1.5;
					}
					//isInConvexHull = isInsidePolygon(ind, vertx, verty, (float)pos_st[0], (float)pos_st[1]);
					isInConvexHull = isInsideBoundingBox(ind, vertx, verty, boundingBoxMargin, (float)pt.x, (float)pt.y, closestPtInConvexHull.x, closestPtInConvexHull.y);
				}

				// -- If the estimated location is inside the polygon, update the position estimator by (1) the RSS-based localization
				if(isInConvexHull == false) {

						if(mDebugLocalization)
							//if(dev->GetDeviceType() == OmniSensr)
							if(isDevicesOfInterest(dev->GetDevAddr()))
							{
								double dist = sqrt(pow(pt.x - closestPtInConvexHull.x, 2) + pow(pt.y - closestPtInConvexHull.y, 2)) / 100.f;	// -- Unit: meter
								//printf("OS[%s]: pt(%d, %d) is OUTSIDE of ConvexHull, so we don't use this! \n", dev->GetDevAddr().c_str(), pt.x, pt.y);
								printf("OS[%s]: pt(%d, %d) is OUTSIDE of ConvexHull ==> closestPt(%d, %d), Dist = %f [m], so we don't use this! \n", dev->GetDevAddr().c_str(), pt.x, pt.y, (int)closestPtInConvexHull.x, (int)closestPtInConvexHull.y, dist);
							}

//						// -- Feed a point on the edge of the boundary that is closest to the detected position instead of giving up.
//						pt.x = closestPtInConvexHull.x;
//						pt.y = closestPtInConvexHull.y;

					// -- We don't use this estimation
					return ERROR_TARGET_NOT_IN_CONVEXHULL;
				}

			} else {
				return ERROR_TOO_FEW_CHOSEN_SENSORS;
			}

			// -- (9) Update the Kalman filter to estimate the location of the device
			if(mKalmanPosEnable == true)
			{
				// -- Store the current state just in case of potential corruption
				double speed, tau, st_prev[4];

				dev->mKalmanPos_m.get_state(st_prev);
				tau = dev->mKalmanPos_m.GetTimeElapsedFromLastUpdate();

				float distChange = sqrt(pow(st_prev[0] - pt.x,2) + pow(st_prev[1] - pt.y,2));
				speed = distChange / tau;

				double dist_to_ROI = distToInStore(pt.x, pt.y);

				bool isKalmanPosSuccessful = true;

				// -- If the change of location is too large, then consider it corrupted. And recover the previous measurement.
				// -- Update Kalman filter for the object location
				if(
						dev->mKalmanPos_m.GetKalmanAge() > 5 &&
								(	//(speed > 500 && tau < 20 && tau > PERIODIC_BUFFER_PROCESSING_INTERVAL*0.7) ||
										//distChange > 3000
										dist_to_ROI > 2000		// -- Unit: cm
										//|| (dist_to_ROI <= 0 && distChange > 3000)
								)

				) {

					isKalmanPosSuccessful = false;

					dev->mTrackCorruption_age++;

					if(dev->mTrackCorruption_age > 1) {

						// -- Initialize Kalman filters (signal & pos)
						dev->mKalmanPos_m.mNeedToInitialize = true;
						for(list<KalmanFilter_4S>::iterator it = dev->mKalmanAP_Signals.begin(); it != dev->mKalmanAP_Signals.end(); ++it) {
							KalmanFilter_4S *kal = &*it;
							if(kal == NULL) continue;
							kal->mNeedToInitialize = true;
						}
					}

					if((mOnlyDevOfInterest && isDevOfInt == true )
							|| !mOnlyDevOfInterest )
						printf("Node[%s]:Corrupted mKalmanPos: (GetTimeElapsedFromLastUpdate(), speed, tau, pt.x, pt.y, distToROI) = (%.2f, %d, %d, %d, %d, %d)\n",
								dev->GetDevAddr().c_str(), dev->mKalmanPos_m.GetTimeElapsedFromLastUpdate(), (int)speed, (int)tau, pt.x, pt.y, (int)dist_to_ROI);

					return ERROR_TARGET_NOT_IN_ROI;

				} else {
					double pos_st_before[4] = {0}, pos_st_after[4] = {0};

					dev->mTrackCorruption_age = 0;

					// -- Update Kalman filter for the object location
					if(dev->mKalmanPos_m.mNeedToInitialize == true || dev->mKalmanPos_m.GetKalmanAge() == 0
							//|| (dev->mKalmanPos_m.GetKalmanAge() < 10 && distChange > 1000 /* Unit: cm  */)
							//|| (dist_to_ROI <= 0 && distChange > 1000)
							|| tau > mKalmanInitThreshold_time	/* Unit: sec */
							|| (distChange > mKalmanInitThreshold_dist /* Unit: cm*/)
							)	{

						double weight = 1.0 - (dev->mChosenSensorsForTriangulation.size() - mNumSensorForLocalization_min)/(double)mNumSensorForLocalization_max;
						dev->mKalmanPos_m.Initialize(pt.x, pt.y, kalman_position_covariance_11, kalman_position_covariance_33);
						dev->mKalmanPos_m.Update(pt.x, pt.y, weight);
						dev->mKalmanPos_m.mNeedToInitialize = false;

//						if(isDevOfInt == false)
//							dev->ClearTrackedPosBuf();
					}
					else {

						// -- TODO: Use the distance error from the detected location to each APs used for the localization as a measure of uncertainty and feed it into the position estimator.
						//XXXXXXXXXX

								// -- If the estimated location is inside the polygon, update the position estimator by (1) the RSS-based localization

								// -- Give more weight if the # of sensors are larger (Smaller weightScale produces actually larger weight)
								//double weightScale = 2.0 - (dev->mChosenSensorsForTriangulation.size() - mNumSensorForLocalization_min)/(double)mNumSensorForLocalization_max;

								// -- Smaller error gets more weight, range [1,2] (Smaller weightScale produces actually larger weight)
								// -- weightScale: Make this range configurable
								double weightScale = 2.0f - 1.0f*MAX(0, (mLocalizationMaxAvgSqrErrorDistance - distAvgErrorSquared))/mLocalizationMaxAvgSqrErrorDistance;

									dev->mKalmanPos_m.get_state(pos_st_before);

								dev->mKalmanPos_m.Update(pt.x, pt.y, weightScale);

									dev->mKalmanPos_m.get_state(pos_st_after);
//
////									//if(mDebugLocalization)
//									{
//										if((pt.x < 0 || pt.y < 0 || pos_st_after[0] < 0 || pos_st_after[1] < 0) && isDevOfInt == true) {
//											printf("Weird Loc!! Node[%s]: pt = (%d, %d), pt_estimated = (%f, %f) --> (%f, %f) \n ", dev->GetDevAddr().c_str(), pt.x, pt.y, pos_st_before[0], pos_st_before[1], pos_st_after[0], pos_st_after[1]);
//											for(int i=0; i<ind; i++) {
//												printf("(vertx, verty) = (%f, %f)\n", vertx[i], verty[i]);
//											}
//										}
//									}
					}

					//if(mDebugLocalization)
					{
						double pos_st_after[4];
						dev->mKalmanPos_m.get_state(pos_st_after);

						if((pt.x < 0 || pt.y < 0 || pos_st_after[0] < 0 || pos_st_after[1] < 0) && isDevOfInt == true) {
							printf("Weird Loc!! Node[%s]: pt = (%d, %d), pt_estimated = (%f, %f) --> (%f, %f) w/ vertices:\n ", dev->GetDevAddr().c_str(), pt.x, pt.y, pos_st_before[0], pos_st_before[1], pos_st_after[0], pos_st_after[1]);
						}
					}

					double kalmanPos_st[4];
					double estimatedCov[4][4];
					dev->mKalmanPos_m.get_state(kalmanPos_st);
					dev->mKalmanPos_m.getCovariance_float(&estimatedCov[0][0]);

					if(estimatedCov[0][0] < 0
							|| kalmanPos_st[0] < -999999999 ||  kalmanPos_st[0] > 999999999
							|| kalmanPos_st[1] < -999999999 ||  kalmanPos_st[1] > 999999999
							|| kalmanPos_st[0] == 0 ||  kalmanPos_st[1] == 0
							) {
						isKalmanPosSuccessful = false;
						dev->mKalmanPos_m.mNeedToInitialize = true;
					}

					// --
//					if(mDebugLocalization)
//						if(isDevicesOfInterest(dev->GetDevAddr()) == true || dev->GetDeviceType() == OmniSensr) {
//							printf("Node[%s] Estimated location (%.1f, %.1f) @age [%d], dist_to_ROI = %.1lf\n", dev->GetDevAddr().c_str(), kalmanPos_st[0], kalmanPos_st[1], dev->mKalmanPos_m.GetKalmanAge(), dist_to_ROI);
//						}

#if defined(USE_RADIO_FINGERPRINT_BASED_LOCALIZATION)

					// -- Update the Kalman filter for signal strength at each AP EVEN for the APs who didn't get the packets. This will suppress any erroneous measurements and make all the measurements across APs more consistent.
					if(0)
					{
						for(uint j=0; j<dev->GetKalmanSignalListSize(); j++) {

							float distToAP = sqrt((double)pow(dev->mKalmanAP_Signals[j].mCenterOfSensingField_X - kalmanPos_st[0],2) + pow(dev->mKalmanAP_Signals[j].mCenterOfSensingField_Y - kalmanPos_st[1],2));
							WiFiSensor_short_Ptr ap = findRunningWiFiSensor(dev->mKalmanAP_Signals[j].mCenterOfSensingField_X, dev->mKalmanAP_Signals[j].mCenterOfSensingField_Y);
							if(ap != NULL) {
								//int rss = ap->convertDistToRSS(distToAP);
								int rss; // = ap->convertDist_logToRSS(distToAP);

								// -- RSS-to-Distance conversion
								if(isAndroid(dev->GetDevAddr().c_str())) {
									rss = ap->convertDist_logToRSS_Android(distToAP);
								} else if(isIOS(dev->GetDevAddr().c_str())) {
									rss = ap->convertDist_logToRSS_iOS(distToAP);
								} else {
									rss = ap->convertDist_logToRSS_All(distToAP);
								}



								//dev->mKalmanAP_Signals[j].Update(distToAP, distToAP);
								dev->mKalmanAP_Signals[j].Update_wo_timstamping(rss, rss);
							}
						}
					}

#else
					// -- Update the Kalman filter for signal strength at each AP EVEN for the APs who didn't get the packets. This will suppress any erroneous measurements and make all the measurements across APs more consistent.
					if(mKalmanPosFeedbackToRssEnable && isKalmanPosSuccessful)
						if(dist_to_ROI <= 0 && dev->mTrackCorruption_age == 0 && dev->mKalmanPos_m.GetKalmanAge() > 3)
						{
							//for(uint j=0; j<dev->GetKalmanSignalListSize(); j++) {
							for(list<KalmanFilter_4S>::iterator it = dev->mKalmanAP_Signals.begin(); it != dev->mKalmanAP_Signals.end(); ++it) {
								KalmanFilter_4S *kal = &*it;
								if(kal == NULL) continue;

								// -- Check if this sensor is among the sensors chosen for this round of localization.
//								bool isChosenSensr = false;
//								if(dev->mChosenSensorsForTriangulation.size() > 0)
//									for(vector<tuple<WiFiSensor_short_Ptr, double, double> >::iterator itt = dev->mChosenSensorsForTriangulation.begin(); itt != dev->mChosenSensorsForTriangulation.end(); ++itt) {
//										WiFiSensor_short_Ptr sensr = get<0>(*itt);
//										if(kal->mCenterOfSensingField_X == sensr->mSensorPos_x && kal->mCenterOfSensingField_Y == sensr->mSensorPos_y) {
//											isChosenSensr = true;
//											break;
//										}
//									}

								// -- Check if this sensor has received any measurement for this round of localization. If not, then update the kalman filter with the latest location to prepare for the upcoming reception.
								bool hasMeasReceived = false;
								if(dev->mMeasForTriangulation.size() > 0)
									for(vector<pair<WiFiSensor_short_Ptr, double> >::iterator it = dev->mMeasForTriangulation.begin(); it != dev->mMeasForTriangulation.end(); ++it) {
										//pair<WiFiSensor_short_Ptr, double> rss = *it;		// -- {AP pointer, distance from the signal source to the AP [Unit: cm]}
										WiFiSensor_short_Ptr sensr = (*it).first;
										if(kal->mCenterOfSensingField_X == sensr->mSensorPos_x && kal->mCenterOfSensingField_Y == sensr->mSensorPos_y) {
											hasMeasReceived = true;
											break;
										}
									}

								// -- Update the signal kalman filter of the sensors that are not chosen for this round of localization.
								// -- This is just to prepare (i.e., reduce the noise) the non-participant sensors for the future when they may be participating as the target moves.
								float distToAP = sqrt((double)pow(kal->mCenterOfSensingField_X - kalmanPos_st[0],2) + pow(kal->mCenterOfSensingField_Y - kalmanPos_st[1],2));

								if(//isChosenSensr == false ||
										hasMeasReceived == false ||
										distToAP > CALIB_MAX_SIGNAL_DISTANCE*0.5 ||
										kal->GetTimeElapsedFromLastUpdate() < mKalmanMeasurementLifetimeForTracking
								) {
									//kal->Update(distToAP, distToAP, mKalmanPosFeedbackToRssNoiseScale);
									kal->Update_wo_timstamping(distToAP, distToAP, mKalmanPosFeedbackToRssNoiseScale);
								}
							}
						}
#endif

					if(isKalmanPosSuccessful == false) {
						return ERROR_KALMAN_FAILURE;

					} else if(dev->mKalmanPos_m.GetKalmanAge() > 1 && dist_to_ROI <= 1000) {
						// -- Store in a storage

						TrackedPos_t_ trackedDev;

						trackedDev.pos.x = (int)(kalmanPos_st[0]+0.5f);
						trackedDev.pos.y = (int)(kalmanPos_st[1]+0.5f);
						trackedDev.localtime = currTime;
						trackedDev.isReported = false;

//						mClock.updateTime();
//						trackedDev.localtime = mClock.mDateTime_lf;

						trackedDev.sensr_center.x = dev->mChosenSensorsForTriangulation_center_curr.x;
						trackedDev.sensr_center.y = dev->mChosenSensorsForTriangulation_center_curr.y;

						if(mDebugLocalization)
						{
                            Point pt_prev = Point(dev->AtTrackedPosBuf()->begin()->pos.x, dev->AtTrackedPosBuf()->begin()->pos.y);
							double distFromPrev = sqrt(pow(pt_prev.x - trackedDev.pos.x,2) + pow(pt_prev.y - trackedDev.pos.y,2));

							if(isDevOfInt == true) {
								if((pt.x < 0 || pt.y < 0 || kalmanPos_st[0] < 0 || kalmanPos_st[1] < 0)) {
									printf("##### Weird Loc!! #### Node[%s]: pt = (%d, %d), pt_estimated = (%f, %f) --> (%f, %f), sensr_center = (%d, %d), distFromPrev = %.2f [m] @Age = %d \n ", dev->GetDevAddr().c_str(),
												pt.x, pt.y, pos_st_before[0], pos_st_before[1], kalmanPos_st[0], kalmanPos_st[1], trackedDev.sensr_center.x, trackedDev.sensr_center.y, distFromPrev/100.0f, dev->mKalmanPos_m.GetKalmanAge());
								} else if(distFromPrev > 1000) {
									printf(" -- Big Jump!! Node[%s]: pt = (%d, %d), pt_estimated = (%f, %f) --> (%f, %f), sensr_center = (%d, %d), distFromPrev = %.2f [m] @Age = %d \n ", dev->GetDevAddr().c_str(),
												pt.x, pt.y, pos_st_before[0], pos_st_before[1], kalmanPos_st[0], kalmanPos_st[1], trackedDev.sensr_center.x, trackedDev.sensr_center.y, distFromPrev/100.0f, dev->mKalmanPos_m.GetKalmanAge());
								}
							}
						}

						// ----- We got the position of a device --------

						if(mDebugLocalization)
							if(isDevicesOfInterest(dev->GetDevAddr())) {
								printf("Localization: Dev[%s] pt = (%d, %d) @age %d\n", dev->GetDevAddr().c_str(), trackedDev.pos.x, trackedDev.pos.y, dev->GetTrackedPosBufSize());
							}

						// ----------------------------------------------
						dev->PutToTrackedPosBuf(trackedDev, mTrackedPoints_max);

						{
							if(mDebugVerboseLevel > 2) {
								if(isDevicesOfInterest(dev->GetDevAddr()) == true) {
									double estimatedCov[4][4];
									dev->mKalmanPos_m.getCovariance_float(&estimatedCov[0][0]);
									double estimatedStd = sqrt(estimatedCov[0][0]);
									printf("Node[%s] KalmanPOS's estimated (Var, Std)  = (%.4f, %.4f [cm]) \n", dev->GetDevAddr().c_str(), (float)(estimatedCov[0][0]), (float)(estimatedStd));
								}
							}
						}
							// -- Record error distance compared to ground truth
							#ifdef USE_WIFI_PLAYER
								DistErrorCollect(dev, trackedDev.pos.x, trackedDev.pos.y);
							#endif
					} else {
						return ERROR_TARGET_NOT_IN_ROI;
					}

				}
			}	// -- End of if(mKalmanPosEnable == true)
			else {

				// -- Store in a storage
				TrackedPos_t_ trackedDev;

				// -- No actual tracking. Just tracking by detection

				// -- Update the Kalman filter for signal strength at each AP EVEN for the APs who didn't get the packets. This will suppress any erroneous measurements and make all the measurements across APs more consistent.
				if(mKalmanPosFeedbackToRssEnable)
				{
					//for(uint j=0; j<dev->GetKalmanSignalListSize(); j++) {
					for(list<KalmanFilter_4S>::iterator it = dev->mKalmanAP_Signals.begin(); it != dev->mKalmanAP_Signals.end(); ++it) {
						KalmanFilter_4S *kal = &*it;
						if(kal == NULL) continue;

							// -- Check if this sensor is among the sensors chosen for this round of localization.
							bool isChosenSensr = false;
							for(vector<tuple<WiFiSensor_short_Ptr, double, double> >::iterator itt = dev->mChosenSensorsForTriangulation.begin(); itt != dev->mChosenSensorsForTriangulation.end(); ++itt) {
								WiFiSensor_short_Ptr sensr = get<0>(*itt);
								if(kal->mCenterOfSensingField_X == sensr->mSensorPos_x && kal->mCenterOfSensingField_Y == sensr->mSensorPos_y) {
									isChosenSensr = true;
									break;
								}
							}

							// -- Update the signal kalman filter of the sensors that are not chosen for this round of localization.
							// -- This is just to prepare (i.e., reduce the noise) the non-participant sensors for the future when they may be participating as the target moves.
							if(isChosenSensr == true) {
								continue;
							} else
							{
									float distToAP = sqrt((double)pow(kal->mCenterOfSensingField_X - pt.x,2) + pow(kal->mCenterOfSensingField_Y - pt.y,2));		// -- Unit: [cm]
									//kal->Update(distToAP, distToAP, mKalmanPosFeedbackToRssNoiseScale);

									if(distToAP > CALIB_MAX_SIGNAL_DISTANCE*0.5 || kal->GetTimeElapsedFromLastUpdate() < mKalmanMeasurementLifetimeForTracking) {

										kal->Update_wo_timstamping(distToAP, distToAP, mKalmanPosFeedbackToRssNoiseScale);
									}
							}

					}
				}

				trackedDev.pos.x = pt.x;
				trackedDev.pos.y = pt.y;
				trackedDev.localtime =  currTime;

//				mClock.updateTime();
//				trackedDev.localtime = mClock.mDateTime_lf;

				trackedDev.isReported = false;

				trackedDev.sensr_center.x = dev->mChosenSensorsForTriangulation_center_curr.x;
				trackedDev.sensr_center.y = dev->mChosenSensorsForTriangulation_center_curr.y;

				dev->PutToTrackedPosBuf(trackedDev, mTrackedPoints_max);

				if(mDebugLocalization)
					if(isDevicesOfInterest(dev->GetDevAddr())) {
						printf("Localization: Dev[%s] pt = (%d, %d) @age %d\n", dev->GetDevAddr().c_str(), trackedDev.pos.x, trackedDev.pos.y, dev->GetTrackedPosBufSize());
					}

//				if(dev->GetDeviceType() == OmniSensr) {
//					printf("Localization: OmniSensr[%s] pt = (%d, %d)\n", dev->GetDevAddr().c_str(), trackedDev.pos.x, trackedDev.pos.y);
//				}

			}

			// -- Update the statistics
			//if(SHOW_STAT_HISTOGRAM)
			{
				dev->ConstructTimeDiffCDF();

				bool isOmniSensr = dev->GetDeviceType() == OmniSensr;
				//bool isDevOfInterest = isDevicesOfInterest(dev->GetDevAddr());

				if(isOmniSensr == true && (int)dev->AtTrackedPosBuf()->size() > 0) {

					// -- Analyze the spatial accuracy based on the ground truth (i.e., the known location of the OmniSensrs)
					WiFiSensor_short_Ptr os = findRunningWiFiSensor(dev->GetDevAddr());

					if(os == NULL)
						return ERROR_NULL_OMNISENSR_POINTER;


					float distError = sqrt(pow(os->mSensorPos_x - dev->AtTrackedPosBuf()->begin()->pos.x,2) + pow(os->mSensorPos_y - dev->AtTrackedPosBuf()->begin()->pos.y,2));

					if((int)dev->mAccuracyStat.distError.size() > mStatAccuracySize_max)
						dev->mAccuracyStat.distError.pop_front();

					dev->mAccuracyStat.distError.push_back(distError);

					// -- Calculate avg and std
					if(dev->mAccuracyStat.distError.size() % (SHOW_STAT_CALCULATION_INTERVAL+1) == 0)
					{
						double distErrorAvg = 0, distErrorStd = 0;
						for(deque<float>::iterator it = dev->mAccuracyStat.distError.begin(); it != dev->mAccuracyStat.distError.end(); ++it) {
							float disterr = *it;
							distErrorAvg += disterr;
						}
						distErrorAvg /= (double)dev->mAccuracyStat.distError.size();
						for(deque<float>::iterator it = dev->mAccuracyStat.distError.begin(); it != dev->mAccuracyStat.distError.end(); ++it) {
							float disterr = *it;
							distErrorStd += pow(disterr - distErrorAvg,2);
						}
						distErrorStd /= (double)dev->mAccuracyStat.distError.size();
						distErrorStd = sqrt(distErrorStd);

						dev->mAccuracyStat.distErrorAvg = distErrorAvg;
						dev->mAccuracyStat.distErrorStd = distErrorStd;

						//printf("++ Accuracy Stat ++ OS[%s]: distError (avg, std) = (%.2f, %.2f) [m]\n", dev->GetDevAddr().c_str(), dev->mAccuracyStat.distErrorAvg/100.0, dev->mAccuracyStat.distErrorStd/100.0);
					}


				}
			}

		} else if(distAvgErrorSquared < 0) {		// -- End of if(distAvgErrorSquared > 0 && pt.x != 0 && pt.y != 0)

			if(mDebugLocalizationErrorCode && isDevicesOfInterest(dev->GetDevAddr())) printf("ERROR!!: Node[%s]: locationEstimation_Trilateration() failed because of Error Code = %d\n", dev->GetDevAddr().c_str(), (int)distAvgErrorSquared);

//			if((mOnlyDevOfInterest && isDevOfInt == true )
//					|| !mOnlyDevOfInterest )
//					if(mDebugVerboseLevel > 1)
//						printf("Node[%s]: Location Estimation FAIL (distAvgErrorSquared, pt.x, pt.y) = (%.3f, %d, %d)  \n", dev->GetDevAddr().c_str(), distAvgErrorSquared, pt.x, pt.y);

			return (int)distAvgErrorSquared;

		} else {		// -- End of if(distAvgErrorSquared > 0 && pt.x != 0 && pt.y != 0)

			if(mDebugLocalizationErrorCode && isDevicesOfInterest(dev->GetDevAddr())) printf("ERROR!!: Node[%s]: locationEstimation_Trilateration() failed because of Error Code = %d, and pt = (%d, %d)\n", dev->GetDevAddr().c_str(), (int)distAvgErrorSquared, pt.x, pt.y);

			if((mOnlyDevOfInterest && isDevOfInt == true )
					|| !mOnlyDevOfInterest )
					if(mDebugVerboseLevel > 1)
						printf("Node[%s]: Location Estimation FAIL (distAvgErrorSquared, pt.x, pt.y) = (%.3f, %d, %d)  \n", dev->GetDevAddr().c_str(), distAvgErrorSquared, pt.x, pt.y);

			return (int)distAvgErrorSquared;
		}


	}

	if(DEBUG_FUNCTION_CALL) printf("periodicProcessing_per_Station End.\n");

	return SUCCESS;
}



// -- Distance from (x,y) to the region of interest (that is in a rectangular shape)
// -- Refer to http://gamedev.stackexchange.com/questions/44483/how-do-i-calculate-distance-from-a-point-to-a-rectangle
double WiFi_Shopper_Tracker::distToInStore(int px, int py) {
//	// -- Assumes that ROI is a rectangle with top-left and bottom-right corners: (0,0), (mStoreWidth_PHY_x, mStoreWidth_PHY_y)
//	int x = mStoreWidth_PHY_x * 0.5;
//	int y = mStoreWidth_PHY_y * 0.5;
//	int width = mStoreWidth_PHY_x;
//	int height = mStoreWidth_PHY_y;
//
//	double dx = MAX(fabs(px - x) - width / 2, 0);
//	double dy = MAX(fabs(py - y) - height / 2, 0);
//	return sqrt(dx * dx + dy * dy);

	// -- TODO: Why am I doing all these things for each call. Should pre-compute this and use it repeatedly.
	float vertx[mRunningWiFiSensor.size()];
	float verty[mRunningWiFiSensor.size()];
	int ind = 0;
	for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it) {
		WiFiSensor_short_Ptr sensr = *it;
		vertx[ind] = sensr->mSensorPos_x;
		verty[ind] = sensr->mSensorPos_y;
		ind++;
	}

    Point2f closestPtInConvexHull;
	bool isInBB = isInsideBoundingBox(ind, vertx, verty, mInStoreMapBBMargin, (float)px, (float)py, closestPtInConvexHull.x, closestPtInConvexHull.y);

	double dist = 0;

	if(isInBB == true)	dist = 0;
	else dist = sqrt((double)pow(closestPtInConvexHull.x,2) + (double)pow(closestPtInConvexHull.y,2));

	return dist;
}

// -- Find the distance and the projection of a point on a line
Point2f WiFi_Shopper_Tracker::findClosestPointToLine(Point2f P, Point2f LinePt0, Point2f LinePt1)
{
    Point2f v = LinePt1 - LinePt0;
    Point2f w = P - LinePt0;

    double c1 = w.dot(v);
    double c2 = v.dot(v);
    double b = c1 / c2;

    Point2f P_proj = LinePt0 + b * v;

    //double distance = sqrt(pow(P.x-Pb.x,2) + pow(P.y-Pb.y,2));

    return P_proj;

//     Vector v = L.P1 - L.P0;
//     Vector w = P - L.P0;

//     double c1 = dot(w,v);
//     double c2 = dot(v,v);
//     double b = c1 / c2;

//     Point Pb = L.P0 + b * v;
//     return d(P, Pb);
}

// -- Refer to http://stackoverflow.com/questions/11716268/point-in-polygon-algorithm
// -- The point on the edge of the polygon is assumed be INSIDE the polygon.
bool WiFi_Shopper_Tracker::isInsidePolygon(int nvert, double *vertx, double *verty, double testx, double testy)
{
  int i, j;
  bool c = false;
  for (i = 0, j = nvert-1; i < nvert; j = i++) {
    if ( ((verty[i]>=testy) != (verty[j]>=testy)) &&
     (testx <= (vertx[j]-vertx[i]) * (testy-verty[i]) / (verty[j]-verty[i]) + vertx[i]) )
       c = !c;
  }

  // -- Return false if the test point is outside the polygon. Return true if inside
  return c;
}

// -- Refer to http://stackoverflow.com/questions/11716268/point-in-polygon-algorithm
// -- The point on the edge of the polygon is assumed be INSIDE the polygon.
bool WiFi_Shopper_Tracker::isInsidePolygonVector(Point point, vector<Point> &polyPts) {
  int i, j, nvert = polyPts.size();
  bool c = false;

  for(i = 0, j = nvert - 1; i < nvert; j = i++) {
    if( ( (polyPts[i].y >= point.y ) != (polyPts[j].y >= point.y) ) &&
        (point.x <= (polyPts[j].x - polyPts[i].x) * (point.y - polyPts[i].y) / (polyPts[j].y - polyPts[i].y) + polyPts[i].x)
      )
      c = !c;
  }

  return c;
}

// --
bool WiFi_Shopper_Tracker::isInsideBoundingBox(int nvert, float *vertx, float *verty, int margin, float testx, float testy, float &closestPtx, float &closestPty)
{
    Point pt_lt(numeric_limits<int>::max(),numeric_limits<int>::max());	// -- left-top point
    Point pt_rb(numeric_limits<int>::min(),numeric_limits<int>::min());	// -- right-bottom point

	for(int i=0; i<nvert; i++) {
		if(pt_lt.x > vertx[i] - margin) pt_lt.x = vertx[i] - margin;
		if(pt_rb.x < vertx[i] + margin) pt_rb.x = vertx[i] + margin;

		if(pt_lt.y > verty[i] - margin) pt_lt.y = verty[i] - margin;
		if(pt_rb.y < verty[i] + margin) pt_rb.y = verty[i] + margin;
	}

	bool ret = false;

	// -- If inside, then true.
	if(testx >= pt_lt.x && testx <= pt_rb.x
			&& testy >= pt_lt.y && testy <= pt_rb.y) {
		ret = true;

		closestPtx = testx;
		closestPty = testy;
	}

	// -- If outside, then find the closest point in the bounding box to the test point
	if(ret == false) {
		if(testx < pt_lt.x)	closestPtx = pt_lt.x;
		else if(testx >= pt_lt.x && testx < pt_rb.x) closestPtx = testx;
		else closestPtx = pt_rb.x;

		if(testy < pt_lt.y)	closestPty = pt_lt.y;
		else if(testy >= pt_lt.y && testx < pt_rb.y) closestPty = testy;
		else closestPty = pt_rb.y;
	}

  // -- Return 0 if the test point is outside the polygon. Return 1 if inside
  return ret;
}

/*
 * Input: Actual channel like 1, 6, 11
 * Output: Channel index like 0, 1, 2
 */
int WiFi_Shopper_Tracker::ConvertChannelIntoIndex(int channel) {
	// -- Channel 1, 6, and 11 would be stored in Bin 0, 1, and 2, respectively.
	int ind_channel;
	if(channel == 1) ind_channel = 0;
	else if(channel == 6) ind_channel = 1;
	else if(channel == 11) ind_channel = 2;
	else
		ind_channel = -1;

	return ind_channel;
}

float distEMD(Mat A, Mat B) {

	if(A.cols != B.cols) {
		printf("A.cols != B.cols\n");
		return -1;
	}

	 //make signature
	 Mat sig1_(1, A.cols, CV_32FC1), sig2_(1, B.cols, CV_32FC1);

	 //fill value into signature
	 for(int h=0; h< A.cols; h++)
	 {
	   float binval = (float)A.at<float>(h);
	   sig1_.at<float>( h) = binval;

	   binval = (float)B.at<float>(h);
	   sig2_.at< float>(h) = binval;
	 }

	 //compare similarity of 2images using emd.
	 float emd_ = cv::EMD(sig1_, sig2_, CV_DIST_L2); //emd 0 is best matching.

//	 printf("A-B similarity %5.5f \n", emd_ );

	 return emd_;
}

// -- To make ascending
bool wayToSort(float a, float b) { return a < b;}

// -- Compute the distance between a histogram of Gaussians and a histogram of a point using a Mahalanobis distance
//double WiFi_Shopper_Tracker::distBtwFingerprints_Mahal(fingerprint_runtime_t fp_run, fingerprint_t fp_map) {
//
//	if(fp_run.sensors.size() != fp_map.sensors.size()) {
//		//printf("fp_run.sensors.size() != fp_map.sensors.size() (%d, %d)\n", fp_run.sensors.size(), fp_map.sensors.size());
//		return -1;
//	}
//
//	int cnt = 0;
//	double dist[99] = {0};
//
//	list<OmniSensr_runtime_t>::iterator itr = fp_run.sensors.begin();
//	for(list<calibOmniSensr_t>::iterator itm = fp_map.sensors.begin(); itm != fp_map.sensors.end(); ++itm, ++itr) {
//		calibOmniSensr_t os_map = *itm;
//		OmniSensr_runtime_t os_run = *itr;
//
//		if(fabs(os_run.rss) < 20 || fabs(os_run.rss) > 75
//				|| fabs(os_map.mu) < 20 || fabs(os_map.mu) > 75
//				|| fabs(os_map.std) > 20
//				) {
//			dist[cnt] = -1;
//		} else {
//
//			// -- Compute the normalized distance
//			//dist[cnt] = pow((os_run.rss - os_map.mu)/os_map.std, 2);
//			dist[cnt] = fabs(pow(os_run.rss - os_map.mu, 2));
//		}
//		cnt++;
//	}
//
//	// -- Sort in an ascending order
//	sort(dist, dist + cnt, wayToSort);
//
////	printf("dist = ");
////	for(int i=0; i < cnt;i++)
////		printf("%.2f ", dist[i]);
////	printf("\n");
//
//	// -- Remove some outliers by taking only some part of the array
//	int No_outliers = 1;
//
//	int k=0;
//	double dist_in = 0;
//	if(cnt - No_outliers > 0) {
//		for(int i=No_outliers; i < cnt;i++) {
//			if(dist[i] >= 0) {
//				dist_in += dist[i];
//				k++;
//			}
//		}
//
//		if(k>0)
//			dist_in /= (double)k;
//		else
//			return -1;
//	} else
//		return -1;
//
//	return dist_in;
//}
//
//
//// -- Compute the distance between two radio fingerprints using histogram distance measure
//double WiFi_Shopper_Tracker::distBtwFingerprints(fingerprint_runtime_t fp_run, fingerprint_t fp_map) {
//	// -- Create two random vectors that has 12 dimensions where each dimension has values [0, 100]
//	int nBins = fp_run.sensors.size();
//
//	Mat A_fl(1, nBins, DataType<float>::type);
//	Mat B_fl(1, nBins, DataType<float>::type);
//
//	printf("A_fl = ");
//	int cnt_a = 0;
//
//	for(list<OmniSensr_runtime_t>::iterator it = fp_run.sensors.begin(); it != fp_run.sensors.end(); ++it) {
//		OmniSensr_runtime_t os_run = *it;
//		A_fl.at<float>(cnt_a) = fabs(os_run.rss);
//		printf("%3.0f, ", A_fl.at<float>(cnt_a));
//
//		// -- If any of the element is not vaild, then return with a faulty value.
//		if(os_run.rss == -1) return -1;
//
//		cnt_a++;
//	}
//	printf("\n");
//
//	printf("B_fl = ");
//	int cnt_b = 0;
//	for(list<calibOmniSensr_t>::iterator it = fp_map.sensors.begin(); it != fp_map.sensors.end(); ++it) {
//		calibOmniSensr_t os_map = *it;
//		B_fl.at<float>(cnt_b) = fabs(os_map.mu);
//		printf("%3.0f, ", B_fl.at<float>(cnt_b));
//		cnt_b++;
//	}
//	printf("\n");
//
//	if(cnt_a != cnt_b) {
//		printf("cnt_a != cnt_b (%d != %d)\n", cnt_a, cnt_b);
//		return -1;
//	}
//
//	float dist;
//
////	dist = distEMD(A_fl, B_fl);									// -- Bad
////	printf("distEMD = %.2f\n", dist);
//
////	 dist = compareHist(A_fl, B_fl, CV_COMP_CORREL);			// -- Bad
////	 printf("[CORREL] A-B similarity %5.5f \n", dist );
//
//	 dist = compareHist(A_fl, B_fl, CV_COMP_CHISQR);			// -- Bad
////	 printf("[CHISQR] A-B similarity %5.5f \n", dist );
////
////	 dist = compareHist(A_fl, B_fl, CV_COMP_INTERSECT);			// -- Bad
////	 printf("[INTERSECT] A-B similarity %5.5f \n", dist );
////
////	 dist = compareHist(A_fl, B_fl, CV_COMP_BHATTACHARYYA);		// -- Bad
////	 printf("[BHATTACHARYYA] A-B similarity %5.5f \n", dist );
//
//	return dist;
//
//}
//
// -- Estimate the current position of a device
//int WiFi_Shopper_Tracker::locationEstimationBasedOnRadioFingerprint(RemoteDevicePtr dev, Point *pt) {
//
//	uint N_AP = 3;
//
//	double estimatedRSS[dev->GetKalmanSignalListSize()][4];
//	for(unsigned int i=0; i<dev->GetKalmanSignalListSize(); i++)
//		for(unsigned int j=0; j<4; j++)
//			estimatedRSS[i][j] = -1;
//
//	unsigned int noAvailableAPs = 0;
//
//	// -- Get the estimated current distance from each AP
//	for(unsigned int j=0; j<dev->GetKalmanSignalListSize(); j++) {
//		if(dev->mKalmanAP_Signals[j].GetKalmanAge() > 0 && dev->mKalmanAP_Signals[j].GetTimeElapsedFromLastUpdate() < MEASUREMENT_LIFETIME_FOR_TRACKING)
//		{
//			dev->mKalmanAP_Signals[j].get_state(estimatedRSS[j]);
//			noAvailableAPs++;
//		}
//	}
//
//	if(noAvailableAPs < N_AP)  {
//		if(mDebugVerboseLevel > 1) printf("Node[%s]: locationEstimation_MeanShift() End. False since noAvailableAPs < N_AP (%d < %d)\n", dev->getAddr().c_str(), noAvailableAPs, N_AP);
//		return 2;
//	}
//
//
//	// -- Construct a radio fingerprint for this device at this position
//	fingerprint_runtime_t fp_run;
//
//	list<fingerprint_t>::iterator it = mRadioMap.begin();
//
//	//printf("Dev[%s] unProcessedMeasSize = %d\n", dev->getAddr(), dev->mMeasBuffer_m.unProcessedMeasSize);
//
//	for(list<calibOmniSensr_t>::iterator iit = (*it).sensors.begin(); iit != (*it).sensors.end(); ++iit) {
//		calibOmniSensr_t *os_map = &(*iit);
//		OmniSensr_runtime_t os;
//		os.addr = os_map->addr;
//		//for(uint i=0; i < dev->GetKalmanSignalListSize(); i++) {
//		int i = 0;
//
//		// -- TODO: Need to correct how to construct a radio fingerprint here
//		for(vector<tcpdump_t>::iterator it = dev->mMeasBuffer_m.begin(); it !=  dev->mMeasBuffer_m.end(); ++it) { ??????
//			tcpdump_t *meas = &*it;
//			//printf("(%s) vs (%s): RSS= %.2f\n", os.addr.c_str(), dev->mMeasBuffer_m[i].capturedBy.c_str(), estimatedRSS[i][0]);
//
//			MonitoringAP_short_Ptr ap = findRunningAP(meas->capturedBy); ???????????
//
//			//if(os.addr.compare(0,os.addr.size(),ap->mIPaddr) == 0 || os.addr.compare(0,os.addr.size(),ap->mDevName) == 0) {
//			if(os.addr.compare(ap->mIPaddr) == 0 || os.addr.compare(ap->mDevName) == 0) {
//
//				//printf("(%s) vs (%s): RSS= %.2f\n", os.addr.c_str(), dev->mMeasBuffer_m[i].capturedBy.c_str(), estimatedRSS[i][0]);
//
//				if(estimatedRSS[i][0] < 0 && estimatedRSS[i][0] > -100) {
//					os.rss = estimatedRSS[i][0];
//				} else
//					os.rss = -200;
//
//				fp_run.sensors.push_back(os);
//				break;
//			}
//			i++;
//		}
//	}
//
//	// -- Print the generated Radio Map in Statistics
//	if(0) {
//		printf("// -- Device[%s]'s radio fingerprint: \n", dev->getAddr().c_str());
//			for(list<OmniSensr_runtime_t>::iterator iit = fp_run.sensors.begin(); iit != fp_run.sensors.end(); ++iit) {
//				OmniSensr_runtime_t os = *iit;
//				printf("%s %.2f\n",os.addr.c_str(), os.rss);
//			}
//			printf("\n");
//	}
//
//	if(fp_run.sensors.size() < N_AP)  {
//		if(mDebugVerboseLevel > 1) printf("Node[%s]: locationEstimation_MeanShift() End. False since fp_run.sensors.size() < N_AP (%d < %d)\n", dev->getAddr().c_str(), noAvailableAPs, N_AP);
//		return 2;
//	}
//
//
//	// -- Compare this device's fingerprint to the radio map
//	double dist[mRadioMap.size()];
//	double dist_min = 999999;
//	int cnt = 0;
//	static float scale = 1.0f;
//	for(it = mRadioMap.begin(); it != mRadioMap.end(); ++it) {
//		fingerprint_t fp_map = *it;
//
//		if(dev->mKalmanPos_m.GetKalmanAge() > 0) {
//			double state[4];
//			dev->mKalmanPos_m.get_state(state);
//			//double timeElapsed = dev->mKalmanPos_m.GetTimeElapsedFromLastUpdate();
//			double timeElapsed = (gTimeSinceStart()) - ((TrackedPos_t_)(dev->AtTrackedPosBuf()->front())).localtime;
//
//			// -- Assume that the max speed of human is MAX_SHOPPER_SPEED (e.g., 300 cm/s)
//			float speedToMoveHere = sqrt(pow(fp_map.x - state[0], 2) + pow(fp_map.y - state[1], 2))/timeElapsed;
//
//			if(speedToMoveHere > MAX_SHOPPER_SPEED*scale) {
//				//printf("dev[%s] to Loc (%d, %d): speedToMoveHere = %.2f. So skipping.. \n", dev->getAddr().c_str(), (int)fp_map.x, (int)fp_map.y, speedToMoveHere);
//				continue;
//			}
//		}
//
//		//dist[cnt] = distBtwFingerprints(fp_run, fp_map);
//		dist[cnt] = distBtwFingerprints_Mahal(fp_run, fp_map);
//
//		//printf("Dev distance to Loc[%d] (%d, %d) = %.2f\n", cnt, (int)fp_map.x, (int)fp_map.y, dist[cnt]);
//
//		// -- if dist[cnt] == 0, then it would imply that there's not enough measurements in fp_run. Thus, we just stop here.
//		if(dist[cnt] < 0)
//			break;
//
//		if(dist_min > dist[cnt]) {
//			dist_min = dist[cnt];
//			pt->x = fp_map.x;
//			pt->y = fp_map.y;
//
//				// -- DEBUG
//				if(0) {
//					printf("(dist_min, fp_map.x, fp_map.y) = (%.2f, %.2f, %.2f)\n", dist_min, fp_map.x, fp_map.y);
//					printf("os_run = ");
//					for(list<OmniSensr_runtime_t>::iterator it = fp_run.sensors.begin(); it != fp_run.sensors.end(); ++it) {
//						printf("%.2f ", (*it).rss);
//					}
//					printf("\n");
//					printf("os_map = ");
//					for(list<calibOmniSensr_t>::iterator it = fp_map.sensors.begin(); it != fp_map.sensors.end(); ++it) {
//						printf("%.2f ", (*it).mu);
//					}
//					printf("\n");
//				}
//		}
//
//		cnt++;
//	}
//
//	//printf("# of point candidates considered = %d\n", cnt);
//
//	// -- Make the search space not too small and not too large
//	if(dev->mKalmanPos_m.GetKalmanAge() > 0) {
//		if(cnt < 5)
//			scale *= 1.2;
//		else if(cnt > 8)
//			scale /= 1.2;
//	}
//
//
//	if(DEBUG_FUNCTION_CALL) printf("Node[%s]: locationEstimation_MeanShift() is called. \n", dev->getAddr().c_str());
//	if(dist_min < 999999)
//		return 7;
//	else
//		return 3;
//}


// -- Get the (counter-clock wise) angle made by three point (e.g., angle between ba and bc) [0, 360]
// -- from http://snipplr.com/view/44716/
double getRotateAngle( Point a, Point b, Point c)
{

    Point2f ba(a.x - b.x, a.y - b.y);
    Point2f bc(c.x - b.x, c.y - b.y);

	const double epsilon = 1.0e-6;
	const double PI = acos(-1.0); // 180 degree
	double angle = 0;

	// normalize
    Point2f norVec1, norVec2;
	norVec1.x = ba.x / sqrt(pow(ba.x, 2) + pow(ba.y, 2));
	norVec1.y = ba.y / sqrt(pow(ba.x, 2) + pow(ba.y, 2));
	norVec2.x = bc.x / sqrt(pow(bc.x, 2) + pow(bc.y, 2));
	norVec2.y = bc.y / sqrt(pow(bc.x, 2) + pow(bc.y, 2));

	// dot product
	double dotProd = (norVec1.x * norVec2.x) + (norVec1.y * norVec2.y);
	if ( abs(dotProd - 1.0) <= epsilon )
		angle = 0;
	else if ( abs(dotProd + 1.0) <= epsilon )
		angle = PI;
	else {
		double cross = 0;
		angle = acos(dotProd);
		//cross product (clockwise or counter-clockwise)
		cross = (norVec1.x * norVec2.y) - (norVec2.x * norVec1.y);

		if (cross < 0) // vec1 rotate clockwise to vec2
				angle = 2 * PI - angle;
	}

	return angle*(180 / PI);

}

// -- Get the angle made by three point (e.g., angle between ba and bc) [0, 360]
int GetAngleABC( Point a, Point b, Point c )
{

    Point ba(a.x - b.x, a.y - b.y);
    Point bc(c.x - b.x, c.y - b.y);

	float dot = (ba.x * bc.x + ba.y * bc.y);	// dot product
	float det = (ba.x * bc.y - ba.y * bc.x);	// determinant

	float angle = atan2 (det, dot) * 180. / M_PI;	// -180 ~ 180
	angle = angle < 0 ? angle + 360. : angle;		// 0 ~ 360

	return (int)angle;
}

// -- Ascending order (smaller one comes earlier)
bool wayToSort_float(float a, float b) { return a < b;}

// -- Ascending order (smaller one comes earlier): Compare the second element in {WiFiSensor_short_Ptr, estimatedDistToAP}
bool wayToSort_pairs(pair<WiFiSensor_short_Ptr, double> a, pair<WiFiSensor_short_Ptr, double> b) { return a.second < b.second;}

// -- Ascending order (smaller one comes earlier): Compare the third element in {WiFiSensor_short_Ptr, estimatedDistToAP, distance from mean-shift centroid}
//bool wayToSort_tuples(tuple<WiFiSensor_short_Ptr, double, double> a, tuple<WiFiSensor_short_Ptr, double, double> b) { return fabs(get<1>(a) - get<2>(a)) < fabs(get<1>(b) - get<2>(b));}
//bool wayToSort_tuples(tuple<WiFiSensor_short_Ptr, double, double> a, tuple<WiFiSensor_short_Ptr, double, double> b) { return fabs(get<1>(a) - get<2>(a))*get<2>(a) < fabs(get<1>(b) - get<2>(b))*get<2>(b);}
bool wayToSort_tuples(tuple<WiFiSensor_short_Ptr, double, double> a, tuple<WiFiSensor_short_Ptr, double, double> b) { return fabs(get<1>(a) - get<2>(a)) + get<2>(a)*2 < fabs(get<1>(b) - get<2>(b)) + get<2>(b)*2;}

//bool WiFi_Shopper_Tracker::Trilateration_RANSAC(vector<pair<Point, double> > *rss_data, RemoteDevicePtr dev, Point *pt) {
//
//	// -- (2) Convert RSS to Distance using calibrated rss-dist transform model
//
//	pair<localizationInputVector, double> pp[dev->mMeasForTriangulation.size()];
//
//		// -- (3) Estimate the location of the device that minimizes the sum of error distance using non-linear least square
//		int i=0;
//		for(vector<pair<Point, double> >::iterator it = dev->mMeasForTriangulation.begin(); it != dev->mMeasForTriangulation.end(); ++it, ++i) {
//			localizationInputVector pos_AP;
//			pair<Point, double> rss_dd = *it;
//			pos_AP(0) =  rss_dd.first.x;
//			pos_AP(1) =  rss_dd.first.y;
//
//			pp[i] = make_pair(pos_AP, rss_dd.second);
//		}
//
//		bool gotLocation = false;
//
//		// -- If this is the beginning of the track, then just use some strong signals to localize. Otherwise, localize with some qualified points.
//		if(dev->GetTrackedPosAge() >= 4) {
//
//			unsigned int MaxNoOfIteration = mRunningWiFiSensor.size()*3;	// --	####################
//			unsigned int MaxNoOfWait = mRunningWiFiSensor.size()*0.4;	// --	####################
//
//			float diff_sum_min = 999999999999;
//
//			unsigned int iteration_cnt = 0;
//			unsigned int wait_cnt = 0;
//
//			do {
//
//				unsigned int NoOfPointsToUse = 5;	// --	####################
//				vector<unsigned int> rind;
//
//				// -- Select some random points
//				//printf("rind = ");
//				for(uint i=0; i<NoOfPointsToUse; i++) {
//					int ind = rand()%dev->mMeasForTriangulation.size();
//
//					rind.push_back(ind);
//
//					//printf("%d ", ind);
//				}
//
//				// -- Check if they are qualified
//				Point p_cur(dev->AtTrackedPosBuf()->begin()->pos.x, dev->AtTrackedPosBuf()->begin()->pos.y);
//
//				// -- This will the x axis where we calculate the angle from
//				Point p_cur_x_axis(dev->AtTrackedPosBuf()->begin()->pos.x + 100, dev->AtTrackedPosBuf()->begin()->pos.y);
//
//				//printf(" deg= ");
//
//				int quadrants[4] = {0};
//				int noOfQuadrantsCovered = 0;
//				for(vector<unsigned int>::iterator it = rind.begin(); it != rind.end(); ++it) {
//					Point loc(dev->mMeasForTriangulation.at((*it)).first.x, dev->mMeasForTriangulation.at((*it)).first.y);
//
//					int deg = (int)getRotateAngle(p_cur_x_axis, p_cur, loc);
//
//					if(deg < 90) quadrants[0] = 1;
//					else if(deg < 180) quadrants[1] = 1;
//					else if(deg < 270) quadrants[2] = 1;
//					else quadrants[3] = 1;
//				}
//
//				// -- To see how many quadrants are covered
//				for(int i=0; i<4; i++)
//					noOfQuadrantsCovered += quadrants[i];
//
//				//printf(" noOfQuadrantsCovered = %d \n", noOfQuadrantsCovered);
//
//				// -- Qualified only if at least three quadrants are covered
//				if(noOfQuadrantsCovered >= 3) {
//
//					vector<pair<localizationInputVector, double> > data_samples;
//
//					for(vector<unsigned int>::iterator it = rind.begin(); it != rind.end(); ++it) {
//
//						// -- Just use the strongest signals and discard too weak signals since weak signals would case more spatial variation given some RSS variation
//						if(pp[*it].second > 8000) continue;
//
//						data_samples.push_back(pp[*it]);
//					}
//
//					if(data_samples.size() < 3) {
//						if(DEBUG_FUNCTION_CALL) printf("Node[%s]: Trilateration_RANSAC() End at 2. \n", dev->GetDevAddr().c_str());
//						return false;
//					}
//
//					// -- Debug
//					if(mDebugVerboseLevel > 1) {
//						printf("Node[%s]:\n", dev->GetDevAddr().c_str());
//						for(uint i=0; i<dev->mMeasForTriangulation.size(); i++)
//							printf("pos.x, pos.y, dist = %d, %d, %f\n", (int)pp[i].first(0), (int)pp[i].first(1), (float)pp[i].second);
//						cout << endl;
//					}
//
//					localizationParameterVector x;
//
//					try {
//						// Now let's use the solve_least_squares_lm() routine to estimate the best position in trilateration
//						x = 0.75;
//
//						// If we didn't create the residual_derivative function then we could
//						// have used this method which numerically approximates the derivatives for you.
//						dlib::solve_least_squares_lm(dlib::objective_delta_stop_strategy(1e-1),
//													   localizationModelResidual,
//													   //dlib::derivative(residual),
//													   localizationModelResidualDerivative,
//													   data_samples, x);
//
//					} catch (exception& e) {
//						cout << e.what() << endl;
//					}
//
//					// -- Calculate the error distance from the estimated location to cloes
//					{
//						float diff_sum = 0;
//						for(vector<unsigned int>::iterator it = rind.begin(); it != rind.end(); ++it) {
//
//
//							// -- Calculate the distance from the estimated location to each of the selected APs Unit: [cm]
//							float dist_p = sqrt(pow(x(0) - dev->mMeasForTriangulation.at((*it)).first.x,2) + pow(x(1) - dev->mMeasForTriangulation.at((*it)).first.y,2));
//
//							// -- Calculate the squared sum of the difference between what distance is actually measured and what distance is supposed to be measured according to the current estimated location
//							float diff = pow(dev->mMeasForTriangulation.at((*it)).second - dist_p, 2);
//							diff_sum += diff;
//						}
//
//						if(diff_sum < diff_sum_min) {
//							//rind_best = rind;
//							diff_sum_min = diff_sum;
//
//							*pt = Point((int)(x(0)+0.5f), (int)(x(1)+0.5f));
//							gotLocation = true;
//
//							wait_cnt = 0;
//						} else
//							wait_cnt++;
//
//						if(wait_cnt > MaxNoOfWait)
//							break;
//					}
//
//				}
//
//				iteration_cnt++;
//
//			} while(iteration_cnt < MaxNoOfIteration);
//
//
//			if(gotLocation == true)
//				return gotLocation;
//
//		}
//
//		if(gotLocation == false) {
//			vector<pair<localizationInputVector, double> > data_samples;
//
//			// -- Use the strongest 60-70% of signals, and discard too weak signals since weak signals would case more spatial variation given some RSS variation
//			for(unsigned int j=0; j < dev->mMeasForTriangulation.size(); j++) {
//
//				// -- Just use the first four strongest signals and discard too weak signals since weak signals would case more spatial variation given some RSS variation
//				if(data_samples.size() >= 4 && pp[j].second > 8000) break;
//				//if(data_samples.size() >= MAX(4, dev->GetKalmanSignalListSize()*0.7)) break;
//
//				data_samples.push_back(pp[j]);
//			}
//
//			if(data_samples.size() < 3) {
//				if(DEBUG_FUNCTION_CALL) printf("Node[%s]: Trilateration_RANSAC() End at 2. \n", dev->GetDevAddr().c_str());
//				return false;
//			}
//
//			localizationParameterVector x;
//
//			try {
//				// Now let's use the solve_least_squares_lm() routine to estimate the best position in trilateration
//				x = 0.75;
//
//				// If we didn't create the residual_derivative function then we could
//				// have used this method which numerically approximates the derivatives for you.
//				dlib::solve_least_squares_lm(dlib::objective_delta_stop_strategy(1e-1),
//											   localizationModelResidual,
//											   //dlib::derivative(residual),
//											   localizationModelResidualDerivative,
//											   data_samples, x);
//
//			} catch (exception& e) {
//				cout << e.what() << endl;
//			}
//
//			*pt = Point((int)(x(0)+0.5f), (int)(x(1)+0.5f));
//
//			return true;
//		}
//
//		return gotLocation;
//}

bool WiFi_Shopper_Tracker::GetCentroidOfInlierSensorsUsingMeanShift(deque<Point> &positions, Point *pt, int max_interation_no = 20, int MinPointsInACluster = 5) {
	if(DEBUG_FUNCTION_CALL) printf("GetCentroidOfInlierSensorsUsingMeanShift() is called \n");

	if(positions.size() == 0) {
		if(DEBUG_FUNCTION_CALL) printf("GetCentroidOfInlierSensorsUsingMeanShift() Ends because no data in positions vector. \n");
		return false;
	}

// -- Compute the centroid using mean-shift like algorithm

        Point centroid_prev[5]; // -- {center, top-left, bottom-left, bottom-right, top-right}
        Point centroid_curr;
        Point pos_top_left, pos_bottom_right;

		// -- Compute the centroid of all the points to use it as the initial centroid
		centroid_prev[0].x = 0;
		centroid_prev[0].y = 0;
		pos_top_left.x = INT_MAX;
		pos_top_left.y = -(INT_MAX -1);
		pos_bottom_right.x = -(INT_MAX -1);
		pos_bottom_right.y = INT_MAX;

        for(deque<Point>::iterator it = positions.begin();  it != positions.end(); ++it) {
			centroid_prev[0].x += (*it).x;
			centroid_prev[0].y += (*it).y;

			pos_top_left.x = pos_top_left.x < (*it).x ? pos_top_left.x : (*it).x;
			pos_top_left.y = pos_top_left.y > (*it).y ? pos_top_left.y : (*it).y;
			pos_bottom_right.x = pos_bottom_right.x > (*it).x ? pos_bottom_right.x : (*it).x;
			pos_bottom_right.y = pos_bottom_right.y < (*it).y ? pos_bottom_right.y : (*it).y;
		}

		// -- Center
		centroid_prev[0].x /= (float)positions.size();
		centroid_prev[0].y /= (float)positions.size();

		// -- Top-left
		centroid_prev[1].x = pos_top_left.x; 	centroid_prev[1].y = pos_top_left.y;
		// -- Bottom-left
		centroid_prev[2].x = pos_top_left.x; 	centroid_prev[2].y = pos_bottom_right.y;
		// -- Bottom-right
		centroid_prev[3].x = pos_bottom_right.x; 	centroid_prev[3].y = pos_bottom_right.y;
		// -- top-right
		centroid_prev[4].x = pos_bottom_right.x; 	centroid_prev[4].y = pos_top_left.y;

        Point centroidEstimates[5];

		float radius_dyn = 0;
		float radius_best[5];

		// -- Max distance from the center to the left-top or right-top coner
		float radius_sta = (sqrt(pow(centroid_prev[0].x - pos_top_left.x,2) + pow(centroid_prev[0].y - pos_top_left.y,2))
							+ sqrt(pow(centroid_prev[0].x - pos_bottom_right.x,2) + pow(centroid_prev[0].y - pos_bottom_right.y,2)))*0.5; 	// -- Unit: [centimeter]

        deque<Point> pos_t[5];
		//deque<WiFiSensor_short_Ptr> chosenSensors[5];


		// -- Perform mean-shift from all five initial starting points and find the five local optimum
		for(int k=0; k<5; k++) {
			centroidEstimates[k].x = 0;
			centroidEstimates[k].y = 0;
			radius_best[k] = 0;

			radius_dyn = radius_sta*1.2;

			int radiusExpansionCnt = 0;

			int iter_cnt = 0;

			for(int i=0; i<max_interation_no && iter_cnt < max_interation_no*3; i++) {
				iter_cnt++;
				pos_t[k].clear();
				// -- Put the points within the radius from the previous centroid
                for(deque<Point>::iterator j = positions.begin();  j != positions.end(); ++j) {
					float dist = sqrt(pow(centroid_prev[k].x - (*j).x,2) + pow(centroid_prev[k].y - (*j).y,2));
					if(dist < radius_dyn) {
						pos_t[k].push_back(*j);
					}
				}

				if(pos_t[k].size() == 0) {
					radius_dyn *= 1.2f;
					i--;

					radiusExpansionCnt++;

					if(radiusExpansionCnt > mMeanShiftRadiusExpansionCnt_max)
						return false;
					else
						continue;
				}

				// -- Compute the centroid of the selected points
				centroid_curr.x = 0;
				centroid_curr.y = 0;
                for(deque<Point>::iterator it = pos_t[k].begin(); it != pos_t[k].end(); ++it) {
					centroid_curr.x += (*it).x;
					centroid_curr.y += (*it).y;
				}
				centroid_curr.x /= (float)pos_t[k].size();
				centroid_curr.y /= (float)pos_t[k].size();

				// -- If the position difference between the previous and current centroid is smaller than a threshold, then stop.
				//float dist = sqrt(pow(centroid_curr.x - centroid_prev[k].x,2) + pow(centroid_curr.y - centroid_prev[k].y,2));

				//printf("inter-centroid dist = %.2f\n", dist);

				// -- If the centroid doesn't change much AND the # of points within the radius is less than or equal to the miminum # of points needed, then stop the iteration.
				if((int)pos_t[k].size() <= (int)MinPointsInACluster) {
					centroidEstimates[k].x = centroid_curr.x;
					centroidEstimates[k].y = centroid_curr.y;

					radius_best[k] = radius_dyn;

					//printf("[k= %d] Stopped at %d (among %d) (positions.size(), pos_t[%d].size, MinPointsInACluster) = (%d, %d, %d)\n", k, i, max_interation_no, k, positions.size(), pos_t[k].size(), MinPointsInACluster);

					break;
				} else {
                    centroid_prev[k] = Point(centroid_curr.x, centroid_curr.y);
					radius_dyn *= 0.8;
				}
			}
		}

		// -- Find the global optimum
		int best_pos_density_ind = -1;

		if(0) {
			float min_dist_cluster = numeric_limits<float>::max();
			float distToClosestFourAPs[5] = {0};

			// -- For the five starting positions, count the # of points from the centroid
			for(int k=0; k<5; k++) {
				if(centroidEstimates[k].x == 0 || centroidEstimates[k].y == 0 || radius_best[k] == 0) continue;

				float dist[positions.size()];
				int cnt = 0;

					// -- From each of five local optimum, calculate the squared distance to the closest four APs.

                    for(deque<Point>::iterator j = positions.begin();  j != positions.end(); ++j, ++cnt) {
						dist[cnt] = pow(centroidEstimates[k].x - (*j).x,2) + pow(centroidEstimates[k].y - (*j).y,2);
					}

					// -- Sort based on the distance so that closest AP comes first, since we want to use only the closest APs
					sort(dist, dist + positions.size(), wayToSort_float);

					distToClosestFourAPs[k] = 0;
					for(int m=0; m < MAX(4,positions.size()*0.75); m++) {
						distToClosestFourAPs[k] += dist[m];
					}

			}

			for(int k=0; k<5; k++) {
				if(centroidEstimates[k].x != 0 && centroidEstimates[k].y != 0 && radius_best[k] != 0)
					if(min_dist_cluster > distToClosestFourAPs[k]) {
						min_dist_cluster = distToClosestFourAPs[k];
						best_pos_density_ind = k;
					}
			}
		} else {
			// -- Select the one with the smallest radius
			float radius_optimum = numeric_limits<float>::max();
			for(int k=0; k<5; k++) {
				if(centroidEstimates[k].x == 0 || centroidEstimates[k].y == 0 || radius_best[k] == 0) continue;

				if(radius_optimum > radius_best[k]) {
					radius_optimum = radius_best[k];
					best_pos_density_ind = k;
				}
			}

		}

		// -- Store it into as the final result
		if(best_pos_density_ind >= 0) {
			pt->x = centroidEstimates[best_pos_density_ind].x;
			pt->y = centroidEstimates[best_pos_density_ind].y;

			if(DEBUG_FUNCTION_CALL) printf("GetCentroidOfInlierSensorsUsingMeanShift() Ends w/ true. \n");

			return true;
		} else {
			if(DEBUG_FUNCTION_CALL) printf("GetCentroidOfInlierSensorsUsingMeanShift() Ends w/ false. \n");

			return false;
		}
}


//bool WiFi_Shopper_Tracker::GetInlierSensorsUsingMeanShift(RemoteDevicePtr dev, vector<pair<WiFiSensor_short_Ptr, double> > &positions, Point *pt, int max_interation_no = 20, int MinPointsInACluster = 4) {
//
//	if(dev == NULL || positions.size() == 0) return false;
//
//// -- Compute the centroid using mean-shift like algorithm
//
//		Point centroid_prev[5]; // -- {center, top-left, bottom-left, bottom-right, top-right}
//		Point centroid_curr;
//		Point pos_top_left, pos_bottom_right;
//
//		// -- Compute the centroid of all the points to use it as the initial centroid
//		centroid_prev[0].x = 0;
//		centroid_prev[0].y = 0;
//		pos_top_left.x = INT_MAX;
//		pos_top_left.y = -(INT_MAX -1);
//		pos_bottom_right.x = -(INT_MAX -1);
//		pos_bottom_right.y = INT_MAX;
//
//		for(vector<pair<WiFiSensor_short_Ptr, double> >::iterator it = positions.begin();  it != positions.end(); ++it) {
//			int x = (*it).first->mSensorPos_x;
//			int y = (*it).first->mSensorPos_y;
//			centroid_prev[0].x += x;
//			centroid_prev[0].y += y;
//
//			pos_top_left.x = pos_top_left.x < x ? pos_top_left.x : x;
//			pos_top_left.y = pos_top_left.y > y ? pos_top_left.y : y;
//			pos_bottom_right.x = pos_bottom_right.x > x ? pos_bottom_right.x : x;
//			pos_bottom_right.y = pos_bottom_right.y < y ? pos_bottom_right.y : y;
//		}
//
//		// -- Center
//		centroid_prev[0].x /= (float)positions.size();
//		centroid_prev[0].y /= (float)positions.size();
//
//		// -- Top-left
//		centroid_prev[1].x = pos_top_left.x; 	centroid_prev[1].y = pos_top_left.y;
//		// -- Bottom-left
//		centroid_prev[2].x = pos_top_left.x; 	centroid_prev[2].y = pos_bottom_right.y;
//		// -- Bottom-right
//		centroid_prev[3].x = pos_bottom_right.x; 	centroid_prev[3].y = pos_bottom_right.y;
//		// -- top-right
//		centroid_prev[4].x = pos_bottom_right.x; 	centroid_prev[4].y = pos_top_left.y;
//
//		Point centroidEstimates[5];
//
//		float radius_dyn = 0;
//
//		// -- Max distance from the center to the left-top or right-top coner
//		float radius_sta = (sqrt(pow(centroid_prev[0].x - pos_top_left.x,2) + pow(centroid_prev[0].y - pos_top_left.y,2))
//							+ sqrt(pow(centroid_prev[0].x - pos_bottom_right.x,2) + pow(centroid_prev[0].y - pos_bottom_right.y,2)))*0.5; 	// -- Unit: [centimeter]
//
//		vector<pair<WiFiSensor_short_Ptr, double> > pos_t[5];
//		vector<pair<WiFiSensor_short_Ptr, double> > pos_t_prev;
//		//deque<WiFiSensor_short_Ptr> chosenSensors[5];
//
//		// -- Perform mean-shift from all five initial starting points and find the five local optimum
//		for(int k=0; k<5; k++) {
//			centroidEstimates[k].x = 0;
//			centroidEstimates[k].y = 0;
//
//			radius_dyn = radius_sta*1.2;
//
//			for(int i=0; i<max_interation_no; i++) {
//				pos_t_prev = pos_t[k];
//				pos_t[k].clear();
//				// -- Put the points within the radius from the previous centroid
//				for(vector<pair<WiFiSensor_short_Ptr, double> >::iterator it = positions.begin();  it != positions.end(); ++it) {
//					int x = (*it).first->mSensorPos_x;
//					int y = (*it).first->mSensorPos_y;
//					float dist = sqrt(pow(centroid_prev[k].x - x,2) + pow(centroid_prev[k].y - y,2));
//					if(dist < radius_dyn) {
//						pos_t[k].push_back(*it);
//					}
//				}
//
//				if(pos_t[k].size() == 0) {
//					radius_dyn *= 1.2f;
//					i--;
//					continue;
//				}
//
//				// -- Compute the centroid of the selected points
//				centroid_curr.x = 0;
//				centroid_curr.y = 0;
//				for(vector<pair<WiFiSensor_short_Ptr, double> >::iterator it = pos_t[k].begin(); it != pos_t[k].end(); ++it) {
//					int x = (*it).first->mSensorPos_x;
//					int y = (*it).first->mSensorPos_y;
//					centroid_curr.x += x;
//					centroid_curr.y += y;
//				}
//				centroid_curr.x /= (float)pos_t[k].size();
//				centroid_curr.y /= (float)pos_t[k].size();
//
//				// -- If the position difference between the previous and current centroid is smaller than a threshold, then stop.
//				//float dist = sqrt(pow(centroid_curr.x - centroid_prev[k].x,2) + pow(centroid_curr.y - centroid_prev[k].y,2));
//
//				//printf("inter-centroid dist = %.2f\n", dist);
//
//				// -- If the centroid doesn't change much AND the # of points within the radius is less than or equal to the miminum # of points needed, then stop the iteration.
//				if((int)pos_t[k].size() < (int)MinPointsInACluster) {
//					centroidEstimates[k].x = centroid_curr.x;
//					centroidEstimates[k].y = centroid_curr.y;
//
//					// -- Recover the set of selected APs which has at least the minimum # points in a cluster (which woud be the previously selected set)
//					pos_t[k] = pos_t_prev;
//
//					//printf("[k= %d] Stopped at %d (among %d) (positions.size(), pos_t[%d].size, MinPointsInACluster) = (%d, %d, %d)\n", k, i, max_interation_no, k, positions.size(), pos_t[k].size(), MinPointsInACluster);
//
//					break;
//				} else {
//					centroid_prev[k] = Point(centroid_curr.x, centroid_curr.y);
//					radius_dyn *= 0.9;
//				}
//			}
//		}
//
//		// -- Find the global optimum
//		int best_pos_density_ind = -1;
//		float min_dist_cluster = numeric_limits<float>::max();
//		float distToClosestFourAPs[5] = {0};
//		{
//			// -- For the five starting positions, count the # of points from the centroid
//			for(int k=0; k<5; k++) {
//				if(centroidEstimates[k].x == 0 && centroidEstimates[k].y == 0) continue;
//
//				float dist[MinPointsInACluster];
//				int cnt = 0;
//				// -- From each of five local optimum, calculate the squared distance to the closest four APs.
//				{
//					for(vector<pair<WiFiSensor_short_Ptr, double> >::iterator it = positions.begin();  it != positions.end(); ++it, ++cnt) {
//						WiFiSensor_short_Ptr ap = (*it).first;
//						dist[cnt] = pow(centroidEstimates[k].x - ap->mSensorPos_x,2) + pow(centroidEstimates[k].y - ap->mSensorPos_y,2);
//					}
//
//					// -- Sort based on the distance so that closest AP comes first, since we want to use only the closest APs
//					sort(dist, dist + positions.size(), wayToSort_float);
//
//					distToClosestFourAPs[k] = 0;
//					for(int m=0; m<4; m++) {
//						distToClosestFourAPs[k] += dist[m];
//					}
//				}
//			}
//
//			for(int k=0; k<5; k++) {
//				if(centroidEstimates[k].x != 0 && centroidEstimates[k].y != 0)
//					if(min_dist_cluster > distToClosestFourAPs[k]) {
//						min_dist_cluster = distToClosestFourAPs[k];
//						best_pos_density_ind = k;
//					}
//			}
//		}
//
//		// -- Store it into as the final result
//		if(best_pos_density_ind >= 0) {
//			pt->x = centroidEstimates[best_pos_density_ind].x;
//			pt->y = centroidEstimates[best_pos_density_ind].y;
//
//			dev->m_ChosenSensorsForTriangulation = pos_t[best_pos_density_ind];
//
//			if(mDebugVerboseLevel > 1) {
//				// -- Print out what sensors are finally selected
//				printf("Node[%s]: m_ChosenSensorsForTriangulation = (Total %d) \n", dev->GetDevAddr().c_str(), (int)dev->m_ChosenSensorsForTriangulation.size());
//				for(vector<pair<WiFiSensor_short_Ptr, double> >::iterator it = dev->m_ChosenSensorsForTriangulation.begin();  it != dev->m_ChosenSensorsForTriangulation.end(); ++it) {
//					WiFiSensor_short_Ptr ap = (*it).first;
//					printf("%s ", ap->mDevName.c_str());
//				}
//				cout << endl;
//			}
//
//			return true;
//		} else {
//			return false;
//		}
//}

// -- Use the stronger half (No smart sensor selection)
//float WiFi_Shopper_Tracker::Trilateration_trivial(vector<pair<WiFiSensor_short_Ptr, double> > *rss_data, RemoteDevicePtr dev, Point *pt) {
//int WiFi_Shopper_Tracker::Trilateration_trivial(vector<pair<WiFiSensor_short_Ptr, double> > *rss_data, RemoteDevicePtr dev, Point *pt, float *avgSquaredError_ret, float *avgError_ret) {
//	if(dev == NULL)
//		return -1;
//
//	vector<pair<localizationInputVector, double> > data_samples;
//	localizationParameterVector x;
//	localizationInputVector pos_AP;
//
//	int pp_size = dev->mMeasForTriangulation.size();
//	tuple<WiFiSensor_short_Ptr, double, double> pp_curr;
//	tuple<WiFiSensor_short_Ptr, double, double> pp[pp_size];	// -- {WiFiSensor_short_Ptr, estimatedDistToAP, distance from mean-shift centroid}
//
//	// -- (3) Estimate the location of the device that minimizes the sum of error distance using non-linear least square
//			//for(unsigned int i=0; i<noAvailableAPs; i++) {
//			int i=0;
//			for(vector<pair<WiFiSensor_short_Ptr, double> >::iterator it = dev->mMeasForTriangulation.begin(); it != dev->mMeasForTriangulation.end(); ++it, ++i) {
//				pair<WiFiSensor_short_Ptr, double> rss_dd = *it;
//
//				pp_curr = make_tuple(rss_dd.first, rss_dd.second, 1);	// --	{WiFiSensor_short_Ptr, estimatedDistToAP, error distance}
//
//				pp[i] = pp_curr;	// --	{WiFiSensor_short_Ptr, estimatedDistToAP, error distance}
//				//printf("Node[%s]: pp[%d] ((%d,%d), %.2lf, %.2f)\n", dev->GetDevAddr().c_str(), i, (int)rss_dd.first->mSensorPos_x, (int)rss_dd.first->mSensorPos_y, rss_dd.second, distError);
//
//			}
//
//			// -- Use the strongest 60-70% of signals, and discard too weak signals since weak signals would case more spatial variation given some RSS variation
//			dev->mChosenSensorsForTriangulation.clear();
//			for(i=0; i < pp_size; i++) {
//
//				if(get<0>(pp[i]) == NULL) continue;
//
//				pos_AP(0) =  get<0>(pp[i])->mSensorPos_x;
//				pos_AP(1) =  get<0>(pp[i])->mSensorPos_y;
//
//				data_samples.push_back(make_pair(pos_AP, get<1>(pp[i])));
//
//				dev->mChosenSensorsForTriangulation.push_back(pp[i]);
//				if(isDevicesOfInterest(dev->GetDevAddr()) == true)
//					if(mDebugVerboseLevel > 1)
//						printf("Node[%s]: mChosenSensorsForTriangulation.push_back(%s) -- %dth among %d\n", dev->GetDevAddr().c_str(), get<0>(pp[i])->mIPaddr.c_str(), (int)dev->mChosenSensorsForTriangulation.size(), pp_size);
//
//				//printf("data_samples.push_back(%d, %d, %.2lf)\n", (int)pp[j].first(0), (int)pp[j].first(1), pp[j].second);
//			}
//
//			// -- Generate the convexhull of the chosen sensors
//			if(dev->mChosenSensorsForTriangulation.size() >= 3)
//			{
//				vector<Point2f> originalPoints;   // Your original points
//				dev->mChosenSensorsConvexHullPts.clear();
//
//				for(vector<tuple<WiFiSensor_short_Ptr, double, double> >::iterator it = dev->mChosenSensorsForTriangulation.begin(); it != dev->mChosenSensorsForTriangulation.end(); ++it) {
//					WiFiSensor_short_Ptr os = get<0>(*it);
//					originalPoints.push_back(Point2f(os->mSensorPos_x, os->mSensorPos_y));
//				}
//
//				// Calculate convex hull of original points (which points positioned on the boundary)
//				cv::convexHull(Mat(originalPoints), dev->mChosenSensorsConvexHullPts);
//
//				if(mDebugLocalization && isDevicesOfInterest(dev->GetDevAddr()) == true)
//					if(dev->mChosenSensorsConvexHullPts.size() > 1) {
//						printf("Node[%s] Convexhull points from %d sensors:", dev->GetDevAddr().c_str(), (int)dev->mChosenSensorsForTriangulation.size());
//						for(vector<Point2f>::iterator itt = dev->mChosenSensorsConvexHullPts.begin(); itt != dev->mChosenSensorsConvexHullPts.end(); ++itt) {
//							Point2f pt = *itt;
//							printf("(%.2f, %.2f) ", pt.x, pt.y);
//						}
//						printf("\n");
//					}
//			}
//
//			double initX = 2.0;
//
//			// -- Only if the squared error distance from the centroid to the selected sensors is within a threshold, confirm the selection and exclude the outlier.
//			try {
//				// Now let's use the solve_least_squares_lm() routine to estimate the best position in trilateration
//				x(0) = initX;
//				x(1) = initX;
//
//				// If we didn't create the residual_derivative function then we could
//				// have used this method which numerically approximates the derivatives for you.
//
//
//				// -- We want to minimize { (Distance from the currently estimated (X, Y) to the AP) - (Distance from the true (x,y) to the AP, which is estimated based on RSS) }
//				if(mLocalizationAlgorithmType == WEIGHTED_CIRCULAR) {
//
//
//					// -- Method 1: Weighted circular algorithm based on a weighted least square error criterion
//
//					// -- based on "Weighted Least Squares Techniques for Improved Received Signal Strength Based Localization", Paula Tarrio et al., 2011
//					// -- This method give weights on the measurement in such a way that a weight is calculated based on the estimated distance from the AP to the signal source,
//					// -- under the assumption that the closer node's measurement (i.e., stronger signal) would have lower uncertainty.
//
//					dlib::solve_least_squares_lm(dlib::objective_delta_stop_strategy(1e-3),
//												   localizationModelResidual_Weighted,
//												   //dlib::derivative(localizationModelResidual_Weighted),
//												   localizationModelResidual_Weighted_Derivative,
//												   data_samples, x);
//
//				} else if(mLocalizationAlgorithmType == PLAIN_CIRCULAR) {
//
//					// -- Method 2: Plain circular algorithm based on a least square error criterion
//					dlib::solve_least_squares_lm(dlib::objective_delta_stop_strategy(1e-3),
//												   localizationModelResidual_Plain,
//												   //dlib::derivative(localizationModelResidual_Plain),
//												   localizationModelResidual_Plain_Derivative,
//												   data_samples, x);
//
//				} else {
//
//					fprintf(stderr, "Localization Algorithm Type is INVALID\n");
//					exit(0);
//				}
//
//			} catch (exception& e) {
//				cout << e.what() << endl;
//
//				if(DEBUG_FUNCTION_CALL) printf("Node[%s]: Trilateration_trivial() Ends at 3. \n", dev->GetDevAddr().c_str());
//
//				return -3;
//			}
//
//		if(x(0) == initX || x(1) == initX) {
//			if(DEBUG_FUNCTION_CALL) printf("Node[%s]: Trilateration_trivial() Ends at 4. \n", dev->GetDevAddr().c_str());
//
//			return -4;
//		}
//
//		*pt = Point((int)(x(0)+0.5f), (int)(x(1)+0.5f));
//
//		// -- Compute the sum of squared error distance between the distance from the estimated location to the AP and the distance converted from RSS measurement
//		float avgSquaredError = -0.01;
//		float avgError = -0.01;
//		{
//
//			int cnt = 0;
//			for(i=0; i < pp_size; i++) {
//
//				if(get<0>(pp[i]) == NULL) continue;
//
//				int posX =  get<0>(pp[i])->mSensorPos_x;
//				int posY =  get<0>(pp[i])->mSensorPos_y;
//
//				float distFromEstimatedLocToAP = sqrt(pow(pt->x - posX,2) + pow(pt->y - posY,2));
//				float distError = distFromEstimatedLocToAP - get<1>(pp[i]);
//				float distErrorSquared = pow(distError,2);
//
//				avgSquaredError += distErrorSquared;
//				avgError += distError;
//
//			}
//
//			if(cnt > 0) {
//				avgSquaredError /= cnt;
//				avgError /= cnt;
//			}
//		}
//
//		*avgSquaredError_ret = avgSquaredError;
//		*avgError_ret = avgError;
//
//		if(DEBUG_FUNCTION_CALL) printf("Node[%s]: Trilateration_trivial() End. \n", dev->GetDevAddr().c_str());
//
//		return 1;
//}

//#define Use_MeanShiftOLD 	1

// -- Perform trilateration using the wifi sensors closest to the shopper device (i.e., WiFi Sensors detecting the strongest signals)
int WiFi_Shopper_Tracker::Trilateration_closest(RemoteDevicePtr dev, Point *pt, float *avgSquaredError_ret, float *avgError_ret) {
	if(DEBUG_FUNCTION_CALL) printf("Node[%s]: Trilateration_closest() is called. \n", dev->GetDevAddr().c_str());

	if(dev == NULL)
		return ERROR_NULL_OMNISENSR_POINTER;

//#if Use_MeanShiftOLD
	// -- Run a mean-shift to exclude any outlier sensor and find the best four sensors: If the RSS measurements are accurate enough, the sensors' geographical locations would be similarly since they must be close to the shopper device.
    deque<Point> data_pts;
	for(vector<pair<WiFiSensor_short_Ptr, double> >::iterator it = dev->mMeasForTriangulation.begin(); it != dev->mMeasForTriangulation.end(); ++it) {
		pair<WiFiSensor_short_Ptr, double> rss = *it;		// -- {AP pointer, distance from the signal source to the AP [Unit: cm]}

        data_pts.push_back(Point(rss.first->mSensorPos_x, rss.first->mSensorPos_y));

		// -- Just use the first four strongest signals and discard too weak signals since weak signals would case more spatial variation given some RSS variation
		//if(data_pts.size() >= 4 || rss.second > AP_MAX_RELIABLE_DISTANCE) break;
		//if(data_pts.size() >= MAX(5, dev->GetKalmanSignalListSize()*0.7) || rss.second > AP_MAX_RELIABLE_DISTANCE) break;
		if(data_pts.size() >= MIN(mNumSensorForSelection_max, MAX(mNumSensorForSelection_min, dev->GetKalmanSignalListSize()*0.8)) || rss.second > mReliableDistanceInMeas_max) break;
	}
    Point pt_center = Point(0,0);
	bool isSuccessful = GetCentroidOfInlierSensorsUsingMeanShift(data_pts, &pt_center, dev->GetKalmanSignalListSize()*2, mNumSensorForLocalization_min);

//#else
//
//	// -- Run a mean-shift to exclude any outlier sensor and find the best four sensors: If the RSS measurements are accurate enough, the sensors' geographical locations would be similarly since they must be close to the shopper device.
//	vector<pair<WiFiSensor_short_Ptr, double> > data_pts;
//	for(vector<pair<WiFiSensor_short_Ptr, double> >::iterator it = dev->mMeasForTriangulation.begin(); it != dev->mMeasForTriangulation.end(); ++it) {
//		pair<WiFiSensor_short_Ptr, double> rss = *it;
//
//		data_pts.push_back(*it);
//
//		// -- Just use the first four strongest signals and discard too weak signals since weak signals would case more spatial variation given some RSS variation
//		if(data_pts.size() >= MIN(mNumSensorForSelection_max, MAX(mNumSensorForSelection_min, dev->GetKalmanSignalListSize()*0.5)) || rss.second > AP_MAX_RELIABLE_DISTANCE) break;
//	}
//
//	Point pt_center;
//	bool isSuccessful = GetInlierSensorsUsingMeanShift(dev, data_pts, &pt_center, dev->GetKalmanSignalListSize()*2, mNumSensorForLocalization_min);
//#endif

	if(isSuccessful == false) {
		if(DEBUG_FUNCTION_CALL) printf("Node[%s]: Trilateration_closest() Ends at 1. \n", dev->GetDevAddr().c_str());

		return ERROR_MEANSHIFT_CLUSTERING_FAILURE;
	}

	dev->mChosenSensorsForTriangulation_center_curr = pt_center;

	// -- (2) Convert RSS to Distance using calibrated rss-dist transform model

	vector<pair<localizationInputVector, double> > data_samples;
	localizationParameterVector x;
	localizationInputVector pos_AP;

//#if Use_MeanShiftOLD
	//pair<WiFiSensor_short_Ptr, double> pp[dev->mMeasForTriangulation.size()];
	//int pp_size = dev->mMeasForTriangulation.size(); //MIN(dev->mMeasForTriangulation.size(), (int)(MAX(mNumSensorForSelection_max*1.2f, dev->mMeasForTriangulation.size() * 0.5f) + 0.5f));		// -- To discard weak signals
	int pp_size = MIN(dev->mMeasForTriangulation.size(), (int)(MAX(mNumSensorForSelection_max, dev->mMeasForTriangulation.size() * 0.5f) + 0.5f));		// -- To discard weak signals
	//int goodAPs_size = pp_size;//MIN(dev->mMeasForTriangulation.size(), (int)(MAX(mRunningWiFiSensor.size() * 0.5, dev->mMeasForTriangulation.size() * 0.5f) + 0.5f)); 	// -- Consider only the stronger half as good APs
	tuple<WiFiSensor_short_Ptr, double, double> pp_curr;
	tuple<WiFiSensor_short_Ptr, double, double> pp[pp_size];	// -- {WiFiSensor_short_Ptr, estimatedDistToAP, distance from mean-shift centroid}
	//tuple<WiFiSensor_short_Ptr, double, double> goodAPs[goodAPs_size];	// -- {WiFiSensor_short_Ptr, estimatedDistToAP, distance from mean-shift centroid}

		// -- (3) Estimate the location of the device that minimizes the sum of error distance using non-linear least square
		//for(unsigned int i=0; i<noAvailableAPs; i++) {
		int pp_num=0;
		for(vector<pair<WiFiSensor_short_Ptr, double> >::iterator it = dev->mMeasForTriangulation.begin(); it != dev->mMeasForTriangulation.end(); ++it) {
			pair<WiFiSensor_short_Ptr, double> rss_dd = *it;		// -- {AP pointer, distance from the signal source to the AP [Unit: cm]}
//			pos_AP(0) =  rss_dd.first->mSensorPos_x;
//			pos_AP(1) =  rss_dd.first->mSensorPos_y;

			// -- NOTE: Don't know why but it is critical to make sure that the distance sample is greater than zero. It must not be zero. Otherwise, solve_least_squares_lm will be hung up.
			if(rss_dd.first == NULL || rss_dd.second == 0) continue;

			float distFromCentroidToSensr = sqrt(pow(pt_center.x - rss_dd.first->mSensorPos_x,2)+pow(pt_center.y - rss_dd.first->mSensorPos_y,2));
			float distFromCentroidToEdgeOfEstimatedDist = fabs((float)(distFromCentroidToSensr - rss_dd.second));

//			printf("pp[%d] (%d, %d, %.2lf)\n", i, (int)rss_dd.first->mSensorPos_x, (int)rss_dd.first->mSensorPos_y, rss_dd.second);

			//pp_curr = make_tuple(rss_dd.first, rss_dd.second, distFromCentroidToSensr);	// --	{WiFiSensor_short_Ptr, estimatedDistToAP, distance from mean-shift centroid}
			pp_curr = make_tuple(rss_dd.first, rss_dd.second, distFromCentroidToEdgeOfEstimatedDist);	// --	{WiFiSensor_short_Ptr, estimatedDistToAP, error distance}

			if(pp_num < pp_size) {
				pp[pp_num++] = pp_curr;	// --	{WiFiSensor_short_Ptr, estimatedDistToAP, error distance}
				//printf("Node[%s]: pp[%d] ((%d,%d), %.2lf, %.2f)\n", dev->GetDevAddr().c_str(), i, (int)rss_dd.first->mSensorPos_x, (int)rss_dd.first->mSensorPos_y, rss_dd.second, distError);
			} else {
				break;
			}

			// -- Consider only the stronger half as good APs
//			if(i < goodAPs_size) {
//				goodAPs[i] = pp_curr;	// --	{WiFiSensor_short_Ptr, estimatedDistToAP, error distance}
//				//printf("Node[%s]: goodAPs[%d] ((%d,%d), %.2lf, %.2f)\n", dev->GetDevAddr().c_str(), i, (int)rss_dd.first->mSensorPos_x, (int)rss_dd.first->mSensorPos_y, rss_dd.second, distError);
//			}
		}

		// -- Sort based on the error distance between the estimated distance based on the RSS received and the estimated distance from the centroid center.
		sort(pp, pp + pp_num, wayToSort_tuples);
		//sort(goodAPs, goodAPs + goodAPs_size, wayToSort_tuples);

		vector<tuple<WiFiSensor_short_Ptr, double, double> > chosenSensorsForTriangulation_prev = dev->mChosenSensorsForTriangulation;

		// -- Use the strongest 60-70% of signals, and discard too weak signals since weak signals would case more spatial variation given some RSS variation
		int numOfDataSamples = MIN(mNumSensorForLocalization_max, MAX(mNumSensorForLocalization_min, dev->GetKalmanSignalListSize()*0.5));
		dev->mChosenSensorsForTriangulation.clear();

		for(int i=0; i < pp_num; i++) {

//			// -- NOTE: Don't know why but it is critical to make sure that the distance sample is greater than zero. It must not be zero. Otherwise, solve_least_squares_lm will be hung up.
//			if(get<0>(pp[i]) == NULL || get<1>(pp[i]) == 0) continue;

			pos_AP(0) =  get<0>(pp[i])->mSensorPos_x;
			pos_AP(1) =  get<0>(pp[i])->mSensorPos_y;

//			if(isDevicesOfInterest(dev->GetDevAddr()) == true)
//				if(mDebugVerboseLevel > 1)
//					printf("Node[%s]: mChosenSensorsForTriangulation.push_back(%s) -- %dth among %d\n", dev->GetDevAddr().c_str(), get<0>(pp[i])->mIPaddr.c_str(), dev->mChosenSensorsForTriangulation.size(), pp_size);

			if((int)data_samples.size() < numOfDataSamples) {
				data_samples.push_back(make_pair(pos_AP, get<1>(pp[i])));		// -- data.first := pos_AP, data.second := distToAP

				dev->mChosenSensorsForTriangulation.push_back(pp[i]);

//				if(mDebugLocalization)
//					if(isDevicesOfInterest(dev->GetDevAddr()) == true)
//							printf("Node[%s]: mChosenSensorsForTriangulation.push_back(%s) -- %dth among %d\n", dev->GetDevAddr().c_str(), get<0>(pp[i])->mIPaddr.c_str(), (int)dev->mChosenSensorsForTriangulation.size(), numOfDataSamples);
			} else {
				break;
			}

			//printf("data_samples.push_back(%d, %d, %.2lf)\n", (int)pp[j].first(0), (int)pp[j].first(1), pp[j].second);
		}

		// -- Include the measurement from the sensors that were chosen in the previous round, in order to avoid too abrupt change in localization
		// -- TODO: Do we really need this? Need to verify using ground truth.
		if(1) {
			double currTime = gCurrentTime();

			if(chosenSensorsForTriangulation_prev.size() > 3
					&& dev->GetTrackedPosAge() > 0
					&& currTime - dev->AtTrackedPosBuf()->begin()->localtime < MIN(30, MAX(10, mKalmanMeasurementLifetimeForTracking*5))
					)
				for(vector<tuple<WiFiSensor_short_Ptr, double, double> >::iterator it = chosenSensorsForTriangulation_prev.begin(); it != chosenSensorsForTriangulation_prev.end(); ++it) {
					WiFiSensor_short_Ptr os_prev = get<0>(*it);
					bool isExisting = false;
					for(vector<tuple<WiFiSensor_short_Ptr, double, double> >::iterator jt = dev->mChosenSensorsForTriangulation.begin(); jt != dev->mChosenSensorsForTriangulation.end(); ++jt) {
						WiFiSensor_short_Ptr os_curr = get<0>(*jt);
						if(os_prev == os_curr) {
							isExisting = true;
							break;
						}
					}

					if(isExisting == false) {
						pos_AP(0) =  os_prev->mSensorPos_x;
						pos_AP(1) =  os_prev->mSensorPos_y;
						data_samples.push_back(make_pair(pos_AP, get<1>(*it)));
					}
				}
		}

		if(mDebugLocalization)
			if(isDevicesOfInterest(dev->GetDevAddr()) == true)
					printf("Node[%s]: # of ChosenSensorsForTriangulation = %d (among %d of available measurements)\n", dev->GetDevAddr().c_str(), (int)dev->mChosenSensorsForTriangulation.size(), (int)dev->mMeasForTriangulation.size());

		// -- Generate the convexhull of the chosen sensors
		if(dev->mChosenSensorsForTriangulation.size() >= 3)
		{
			vector<Point2f> originalPoints;   // Your original points
			dev->mChosenSensorsConvexHullPts.clear();

			for(vector<tuple<WiFiSensor_short_Ptr, double, double> >::iterator it = dev->mChosenSensorsForTriangulation.begin(); it != dev->mChosenSensorsForTriangulation.end(); ++it) {
				WiFiSensor_short_Ptr os = get<0>(*it);
                originalPoints.push_back(Point2f(os->mSensorPos_x, os->mSensorPos_y));
			}

			// Calculate convex hull of original points (which points positioned on the boundary)
			cv::convexHull(Mat(originalPoints), dev->mChosenSensorsConvexHullPts);

//			if(mDebugLocalization)
//				if(isDevicesOfInterest(dev->GetDevAddr()) == true)
//					if(dev->mChosenSensorsConvexHullPts.size() > 1) {
//						printf("Node[%s] Convexhull points from %d sensors:", dev->GetDevAddr().c_str(), (int)dev->mChosenSensorsForTriangulation.size());
//						for(vector<Point2f>::iterator itt = dev->mChosenSensorsConvexHullPts.begin(); itt != dev->mChosenSensorsConvexHullPts.end(); ++itt) {
//							Point2f pt = *itt;
//							printf("(%.2f, %.2f) ", pt.x, pt.y);
//						}
//						printf("\n");
//					}
		} else {
			return ERROR_TOO_FEW_CHOSEN_SENSORS;
		}

//		dev->mChosenSensorsForTriangulation.clear();
//		for(i=0; i < goodAPs_size; i++) {
//			// -- Mark the good APs chosen.
//			if(get<0>(goodAPs[i]) != NULL)
//				dev->mChosenSensorsForTriangulation.push_back(goodAPs[i]);
//
//			if(isDevicesOfInterest(dev->GetDevAddr()) == true)
		//				if(mDebugVerboseLevel > 1)
//				printf("Node[%s]: mChosenSensorsForTriangulation.push_back(%s) -- %dth among %d\n", dev->GetDevAddr().c_str(), get<0>(goodAPs[i])->mIPaddr.c_str(), dev->mChosenSensorsForTriangulation.size(), goodAPs_size);
//		}
//#else
//		for(vector<pair<WiFiSensor_short_Ptr, double> >::iterator it = dev->m_ChosenSensorsForTriangulation.begin(); it != dev->m_ChosenSensorsForTriangulation.end(); ++it) {
//			pair<WiFiSensor_short_Ptr, double> rss_dd = *it;
//
//			pos_AP(0) =  rss_dd.first->mSensorPos_x;
//			pos_AP(1) =  rss_dd.first->mSensorPos_y;
//
//			data_samples.push_back(make_pair(pos_AP, rss_dd.second));
//
//			if(mDebugVerboseLevel > 1)
//				printf("Node[%s]: [%d]th (among %d) -- data_samples.push_back(%d, %d, %.2lf)\n", dev->GetDevAddr().c_str(), data_samples.size(), dev->m_ChosenSensorsForTriangulation.size(), (int)pos_AP(0), (int)pos_AP(1), rss_dd.second);
//
//			if(data_samples.size() >= MIN(mNumSensorForLocalization_max, MAX(mNumSensorForLocalization_min, dev->GetKalmanSignalListSize()*0.5))) break;
//		}
//
//		// -- Mark the APs chosen.
//		//dev->mChosenSensorsForTriangulation = dev->m_ChosenSensorsForTriangulation;
//#endif

		if((int)data_samples.size() < mNumSensorForLocalization_min) {
			if(DEBUG_FUNCTION_CALL) printf("Node[%s]: Trilateration_closest() Ends at 2. \n", dev->GetDevAddr().c_str());

			return ERROR_TOO_SMALL_DATA_SAMPLES_FOR_LOCALIZATION;
		}


		// -- Debug
//		if(mDebugVerboseLevel > 1) {
//			printf("Node[%s]:\n", dev->GetDevAddr().c_str());
//			for(uint i=0; i<dev->mMeasForTriangulation.size(); i++)
//				printf("pos.x, pos.y, dist = %d, %d, %f\n", (int)pp[i].first(0), (int)pp[i].first(1), (float)pp[i].second);
//			cout << endl;
//		}

		double initX = 1.0;

				// -- DEBUG
//				if(mDebugLocalization)
//				{
//					printf("data_samples.size() = %d\n", (int)data_samples.size());
//					int cnt = 0;
//					for(vector<pair<localizationInputVector, double> >::iterator it = data_samples.begin(); it != data_samples.end(); ++it) {
//						pair<localizationInputVector, double> datum = *it;
//						int posAP_x = datum.first(0);
//						int posAP_y = datum.first(1);
//						double estimatedDistToAP = datum.second;
//
//						printf("data_samples[%d]: (x, y, estimatedDistToAP) = (%d, %d, %f [m])\n", cnt, posAP_x, posAP_y, estimatedDistToAP/100.0f);
//						cnt++;
//					}
//				}

		// -- Only if the squared error distance from the centroid to the selected sensors is within a threshold, confirm the selection and exclude the outlier.
		// -- NOTE: Don't know why but it is critical to make sure that the distance sample is greater than zero. It must not be zero. Otherwise, solve_least_squares_lm will be hung up.
		try {
			// Now let's use the solve_least_squares_lm() routine to estimate the best position in trilateration
			x(0) = initX;
			x(1) = initX;

			// If we didn't create the residual_derivative function then we could
			// have used this method which numerically approximates the derivatives for you.

			// -- We want to minimize { (Distance from the currently estimated (X, Y) to the AP) - (Distance from the true (x,y) to the AP, which is estimated based on RSS) }
			if(mLocalizationAlgorithmType == WEIGHTED_CIRCULAR) {


				// -- Method 1: Weighted circular algorithm based on a weighted least square error criterion

				// -- based on "Weighted Least Squares Techniques for Improved Received Signal Strength Based Localization", Paula Tarrio et al., 2011
				// -- This method give weights on the measurement in such a way that a weight is calculated based on the estimated distance from the AP to the signal source,
				// -- under the assumption that the closer node's measurement (i.e., stronger signal) would have lower uncertainty.


				dlib::solve_least_squares_lm(dlib::objective_delta_stop_strategy(1e+0),
											   localizationModelResidual_Weighted,
											   //dlib::derivative(localizationModelResidual_Weighted),
											   localizationModelResidual_Weighted_Derivative,
											   data_samples,
											   x);

			} else if(mLocalizationAlgorithmType == PLAIN_CIRCULAR) {

				// -- Method 2: Plain circular algorithm based on a least square error criterion

				dlib::solve_least_squares_lm(dlib::objective_delta_stop_strategy(1e+0),
											   localizationModelResidual_Plain,
											   //dlib::derivative(localizationModelResidual_Plain),
											   localizationModelResidual_Plain_Derivative,
											   data_samples,
											   x);

			} else {

				fprintf(stderr, "Localization Algorithm Type is INVALID\n");
				exit(0);
			}

		} catch (exception& e) {
			cout << e.what() << endl;

			if(DEBUG_FUNCTION_CALL) printf("Node[%s]: Trilateration_closest() Ends at 3. \n", dev->GetDevAddr().c_str());

			return ERROR_LEAST_SQUARE_EXCEPTION;
		}

	if(x(0) == initX || x(1) == initX) {
		if(DEBUG_FUNCTION_CALL) printf("Node[%s]: Trilateration_closest() Ends at 4. \n", dev->GetDevAddr().c_str());

		return ERROR_LEAST_SQUARE_FAILURE;
	}

    *pt = Point((int)(x(0)+0.5f), (int)(x(1)+0.5f));

	if((int)dev->mChosenSensorsForTriangulation.size() < mNumSensorForLocalization_min)
		return ERROR_TOO_FEW_CHOSEN_SENSORS;

	// -- Compute the sum of squared error distance between the distance from the estimated location to the AP and the distance converted from RSS measurement
	double avgSquaredError = -0.01;
	double avgError = -0.01;
//#if Use_MeanShiftOLD
		{
			for(int i=0; i < pp_num; i++) {

				int pos_AP_x =  get<0>(pp[i])->mSensorPos_x;
				int pos_AP_y =  get<0>(pp[i])->mSensorPos_y;

				// -- The distance from the estimated location to the AP
				double distFromEstimatedLocToAP = sqrt(pow(pt->x - pos_AP_x,2) + pow(pt->y - pos_AP_y,2));

				// -- Weigh more on the measurements from closer nodes
				//double weight = pow((mReliableDistanceInMeas_max - MIN(mReliableDistanceInMeas_max, distFromEstimatedLocToAP))/(double)mReliableDistanceInMeas_max, 2);	 // -- Range: [0,1]
				double weight = pow((mReliableDistanceInMeas_max - MIN(mReliableDistanceInMeas_max, distFromEstimatedLocToAP))/(double)mReliableDistanceInMeas_max, 4);	 // -- Range: [0,1]

				// -- The difference between the distance from the estimated location to the AP and the distance estimated by the Kalman filter based on the signal strength
				// -- If this is positive, then  the signal strength-based distance estimation would need to be increased by lowering the RSS measurement (i.e., increase the RSS offset value).
				double distError = fabs(distFromEstimatedLocToAP - (double)get<1>(pp[i])) * weight;

				//float distErrorSquared = pow(distError,2);
				//float distErrorSquared = fabs(distFromEstimatedLocToAP - get<1>(chosenSensr));

				double distErrorSquared = pow(distError,2);


				avgSquaredError += distErrorSquared;
				avgError += distError;
			}
			avgSquaredError /= (double)pp_num;
			avgError /= (double)pp_num;
		}

//#else
//		for(vector<pair<WiFiSensor_short_Ptr, double> >::iterator it = dev->m_ChosenSensorsForTriangulation.begin(); it != dev->m_ChosenSensorsForTriangulation.end(); ++it) {
//			pair<WiFiSensor_short_Ptr, double> chosenSensr = *it;
//			float distFromEstimatedLocToAP = sqrt(pow(pt->x - chosenSensr.first->mSensorPos_x,2) + pow(pt->y - chosenSensr.first->mSensorPos_y,2));
//			float distErrorSquared = pow(distFromEstimatedLocToAP - chosenSensr.second,2);
//			//float distErrorSquared = fabs(distFromEstimatedLocToAP - get<1>(chosenSensr));
//
//			avgSquaredError += distErrorSquared;
//		}
//		avgSquaredError /= (float)dev->m_ChosenSensorsForTriangulation.size();
//#endif

	*avgSquaredError_ret = avgSquaredError;
	*avgError_ret = avgError;

//	if(mDebugLocalization)
//		if(isDevicesOfInterest(dev->GetDevAddr()) == true) {
//			for(vector<tuple<WiFiSensor_short_Ptr, double, double> >::iterator it = dev->mChosenSensorsForTriangulation.begin(); it != dev->mChosenSensorsForTriangulation.end(); ++it) {
//				tuple<WiFiSensor_short_Ptr, double, double> chosenSensr = *it;
//
//				KalmanFilter_4S *kal = findKalmanSignal(dev, get<0>(chosenSensr));
//
//				double estimatedDistToAP[4];
//				double estimatedCov[4][4];
//
//				kal->get_state(estimatedDistToAP);
//				kal->getCovariance_float(&estimatedCov[0][0]);
//
//				printf("Node[%s] KalmanSignal for Chosen OS [%s]'s estimated (DistToAP, Var)  = (%.2f [m], %.2f) \n", dev->GetDevAddr().c_str(), get<0>(chosenSensr)->mDevName.c_str(), (float)(estimatedDistToAP[0]/100), (float)(estimatedCov[0][0]));
//			}
//		}

	if(DEBUG_FUNCTION_CALL) printf("Node[%s]: Trilateration_closest() End. \n", dev->GetDevAddr().c_str());

	return SUCCESS;
}

// -- Estimate the current position of a device
float WiFi_Shopper_Tracker::locationEstimation_Trilateration(RemoteDevicePtr dev, Point *pt) {
	if(DEBUG_FUNCTION_CALL) printf("Node[%s]: locationEstimation_Trilateration() is called. \n", dev->GetDevAddr().c_str());

	if(dev == NULL)
		return ERROR_NULL_OMNISENSR_POINTER;

	// -- Method 1: Perform trilateration using Non-linear Least Square method (i.e., Bundle Adjustment) using Levenberg-Marquardt

	// -- For more information about trilateration, see:
	// -- (1) http://inside.mines.edu/~whereman/papers/Murphy-Hereman-Trilateration-1995.pdf
	// -- (2) http://inside.mines.edu/~whereman/talks/TurgutOzal-11-Trilateration.pdf
	// -- For more information about general Least-Squres, see:
	// -- (1) http://www.orbitals.com/self/least/least.htm or http://www.orbitals.com/self/least/least.pdf
	// -- For more information about Non-linear Least-Square function in 'dlib' (http://dlib.net/) library, see:
	// -- (1) http://dlib.net/least_squares_ex.cpp.html

//	printf("0.1: dev->mKalmanAP_Signals.size() = %d\n", (int)dev->mKalmanAP_Signals.size());

    //pair<Point, double> rss_ap[dev->mKalmanAP_Signals.size()];		// -- (location of AP, distance from the signal source to the AP [Unit: cm])
	pair<WiFiSensor_short_Ptr, double> rss_ap[dev->mKalmanAP_Signals.size()];		// -- (location of AP, distance from the signal source to the AP [Unit: cm])
	int cnt = 0;

//	printf("0.2: dev->mKalmanAP_Signals.size() = %d\n", (int)dev->mKalmanAP_Signals.size());

	// -- Get the estimated current distance from each AP
	for(list<KalmanFilter_4S>::iterator it = dev->mKalmanAP_Signals.begin(); it != dev->mKalmanAP_Signals.end(); ++it) {
		KalmanFilter_4S *kal = &*it;

		if(kal == NULL) continue;

		if(kal->GetKalmanAge() > 4 && kal->GetTimeElapsedFromLastUpdate() < mKalmanMeasurementLifetimeForTracking)
		{
			double estimatedDistToAP[4] = {0};
			double estimatedCov[4][4] = {{0}};

			kal->get_state(estimatedDistToAP);
			kal->getCovariance_float(&estimatedCov[0][0]);

			//double estimatedStd = sqrt(estimatedCov[0][0]);

			WiFiSensor_short_Ptr os = findRunningWiFiSensor(kal->mCenterOfSensingField_X, kal->mCenterOfSensingField_Y);

			if(os == NULL)
				continue;

			// -- Discard any measurements of OmniSensr that are from itself
			if(dev->GetDeviceType() == OmniSensr)
				if(findRunningWiFiSensor(dev->GetDevAddr()) == os)
					continue;

//			if(mDebugLocalization)
//				if(isDevicesOfInterest(dev->GetDevAddr()) == true) {
//					printf("Node[%s] KalmanSignal [%s]'s estimated (Dist, Var)  = (%.2f [m], %.2f) \n", dev->GetDevAddr().c_str(), os->mDevName.c_str(), (float)(estimatedDistToAP[0]/100), (float)(estimatedCov[0][0]));
//				}

//			kal->PredictForPrediction();
//			kal->get_predicted_state(estimatedDistToAP);

			// -- [MUST] To make sure the measurement is usable. Otherwise, the dlib's least-square optimizer would take so long to get a result.
			if(estimatedDistToAP[0] < mReliableDistanceInMeas_max && estimatedDistToAP[0] >= 0
					//&& estimatedStd < mKalmanRssMaxStdAllowed
					) {

                //pair<Point, double> rss_ap;		// -- {AP pointer, distance from the signal source to the AP [Unit: cm]}
				rss_ap[cnt].first = os;
				rss_ap[cnt].second = estimatedDistToAP[0];
				cnt++;
			}
		}
	}

	if(cnt < mMinNumMeasToDoLocalization) {
		if(DEBUG_FUNCTION_CALL) printf("Node[%s]: locationEstimation_Trilateration() End at 1. \n", dev->GetDevAddr().c_str());

		if(mDebugLocalizationErrorCode && isDevicesOfInterest(dev->GetDevAddr())) printf("ERROR!!: Node[%s]: Trilateration_closest() failed because of ERROR_TOO_SMALL_MEASUREMENTS_RECEIVED, when # of valid meas = %d\n", dev->GetDevAddr().c_str(), cnt);

		return ERROR_TOO_SMALL_MEASUREMENTS_RECEIVED;
	}

	// -- Sort based on the distance from the location to the AP so that closer APs comes first, in case we want to use only the stronger signals, {WiFiSensor_short_Ptr, estimatedDistToAP}
	sort(rss_ap, rss_ap + cnt, wayToSort_pairs);

	//vector<pair<WiFiSensor_short_Ptr, double> > rss_data;
	dev->mMeasForTriangulation.clear();
	for(int i=0; i<cnt; i++) {
		//rss_data.push_back(rss_ap[i]);
		dev->mMeasForTriangulation.push_back(rss_ap[i]);
	}

//	if(mDebugLocalization)
//	{
//		printf("Node[%s]: [rss_data] # of Available Measurements = %d: ", dev->GetDevAddr().c_str(), (int)rss_data.size());
//		for(vector<pair<WiFiSensor_short_Ptr, double> >::iterator it = rss_data.begin(); it != rss_data.end(); ++it) {
//			pair<WiFiSensor_short_Ptr, double> rss = *it;
//			printf("%.1lf ", rss.second);
//		}
//		printf("\n");
//	}

	pt->x = 0;
	pt->y = 0;

	float distAvgErrorSquared = numeric_limits<float>::max();
	float distAvgError = numeric_limits<float>::max();

#if DO_TRILATERATION_TRIVIAL
	// -- Use only stronger half, yet with no smart sensor selection
	bool isSuccessful = Trilateration_trivial(&rss_data, dev, pt, &distAvgErrorSquared, &distAvgError);

#else
	// -- Perform trilateration using the wifi sensors closest to the shopper device (i.e., WiFi Sensors detecting the strongest signals)
	// -- Will return a minus value if it fails.
	int errorCode = Trilateration_closest(dev, pt, &distAvgErrorSquared, &distAvgError);

	if(errorCode < 0) {

		if(mDebugLocalizationErrorCode && isDevicesOfInterest(dev->GetDevAddr())) printf("ERROR!!: Node[%s]: Trilateration_closest() failed because of Error Code = %d, when (distAvgErrorSquared, distAvgError) = (%f, %f)\n", dev->GetDevAddr().c_str(), errorCode, distAvgErrorSquared, distAvgError);

		return errorCode;
	}
//	else {
//		if(mDebugLocalizationErrorCode && isDevicesOfInterest(dev->GetDevAddr())) printf("Node[%s]: Trilateration_closest() was successful.\n", dev->GetDevAddr().c_str());
//	}

	distAvgErrorSquared /= (100*100.0f); // -- Convert unit from cm^2 to meter^2
	distAvgError /= 100.0f; // -- Convert unit from cm to meter

	if(mDebugLocalization && isDevicesOfInterest(dev->GetDevAddr()))
		printf("Node[%s]: (distAvgError [m], distAvgErrorSquared [m^2]) = (%f, %f)\n", dev->GetDevAddr().c_str(), distAvgError, distAvgErrorSquared);

	// -- Adjust the per-device RSS offset adjustment
	if(mAutomaticPerDevRssOffsetAdjustment)
	{
		// -- The difference between the distance from the estimated location to the AP and the distance estimated by the Kalman filter based on the signal strength
		// -- If this is positive, then  the signal strength-based distance estimation would need to be increased by lowering the RSS measurement (i.e., increase the RSS offset value).

		if(fabs(distAvgError) > 3.0 /*unit: meter*/) {
			// -- Adjust further if the per-dev RSS offset is not deviated too much from the global rss offset (e.g., 15%)
			if(fabs(dev->mPerDevRssCalibOffset - mWifiCalibRssOffset) < fabs(mWifiCalibRssOffset)*0.10) {
				if(distAvgError > 0)
					dev->mPerDevRssCalibOffset *= 0.95;//0.98;		// -- Increase
				else
					dev->mPerDevRssCalibOffset *= 1.05;//1.02;		// -- Decrease
			}
		}
	}

//	if(mDebugLocalization)
//		if(isDevicesOfInterest(dev->GetDevAddr()) == true && dev->GetDeviceType() != OmniSensr) {
//			printf("Node[%s] Avg Error Dist to AP btw Signal Estimation and Location Estimation: (abs, actual) = (%.4f, %.4f) [meter], pt = (%d,%d), mPerDevRssCalibOffset = %.2f \n", dev->GetDevAddr().c_str(), sqrt(distAvgErrorSquared), distAvgError, pt->x, pt->y, dev->mPerDevRssCalibOffset);
//
//			if(distAvgErrorSquared >= 0) {
//				int r = 0, g = 0, b = 0;
//				int thickness = 1;
//				r = 0xad; g = 0x28; b = 0x00;
//				thickness = 2;
//				float scale = mStoreWidth_IMG_y / (float)mStoreWidth_PHY_y;
//
//				circle(mImg_map,Point(pt->x*scale, pt->y*scale), thickness*3+1 /*radius*/, CV_RGB(r,g,b), CV_FILLED, 16,0);
//				circle(mImg_map,Point(pt->x*scale, pt->y*scale), thickness*3+1 /*radius*/, CV_RGB(0,100,155), 3, 16,0);
//
//				char text[99];
//				sprintf(text, "[%s] %d", dev->GetDevAddr().c_str(), dev->GetTrackedPosAge());
//				putText(mImg_map, text, Point(pt->x*scale+5, pt->y*scale-3), 0, 0.4,CV_RGB(255,0,0), 1, CV_AA);
//			}
//		}


	float distAvgErrorSquaredThreshold = mLocalizationMaxAvgSqrErrorDistance;

	// -- Give more error margin for the early tracking stage
	if(dev->mKalmanPos_m.GetKalmanAge() < 3) distAvgErrorSquaredThreshold *= 2.0f;
	else if(dev->mKalmanPos_m.GetKalmanAge() < 5) distAvgErrorSquaredThreshold *= 1.5f;

	// -- If the error distance is too large, then discard it, since it would imply that there's no consensus among sensors (i.e., low confidence on the RSS measurements)
	double distToOrigin = sqrt(pt->x*pt->x + pt->y*pt->y);		// -- TODO: Why some points are (2,1), (1,2), or (2,2)?
	if(distAvgErrorSquared > distAvgErrorSquaredThreshold || distAvgErrorSquared <= 0 || (pt->x == 0 && pt->y == 0) || distToOrigin < 10) {
		if(DEBUG_FUNCTION_CALL) printf("Node[%s]: locationEstimation_Trilateration() End at 2. \n", dev->GetDevAddr().c_str());
		return ERROR_TOO_LARGE_ERROR_DISTANCE;
	}
#endif

	if(DEBUG_FUNCTION_CALL) printf("Node[%s]: locationEstimation_Trilateration() End. TRUE\n", dev->GetDevAddr().c_str());
	return distAvgErrorSquared;
}


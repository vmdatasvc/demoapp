/*
 * RemoteSniffer.cpp
 *
 *  Created on: Jan 24, 2017
 *      Author: jwallace
 */

#include "RemoteSniffer.hpp"

#include "modules/wifi/SnifferPacket.hpp"
#include "modules/wifi/TrackerPacket.hpp"

#include <sstream>

#include <boost/bind.hpp>

#include <boost/serialization/vector.hpp>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/date_time/posix_time/posix_time.hpp>

#include "json.hpp"

using std::string;
using std::ostringstream;
using std::istringstream;

using json=nlohmann::json;

#include <iostream>

RemoteSniffer::RemoteSniffer(const std::string& hostname, float x, float y, float z, const std::string& name, const std::string& localApp)
	: remote_recv(hostname.c_str()),
	  remote_send(hostname.c_str()),
	  last_seen(boost::posix_time::microsec_clock::universal_time()),
	  saw_packet(false),
	  _x(x), _y(y), _z(z), _name(name) {

	string packetTopic = APP_ID_WIFI_PROCESSOR;
	packetTopic += "2";
	
	packetTopic += "/packets";
	remote_recv.subscribe(packetTopic, boost::bind(&RemoteSniffer::aggregateData, this, _1));

	string heartbeatTopic = "heartbeat/";
	heartbeatTopic += APP_ID_WIFI_PROCESSOR;
	heartbeatTopic += "2";

	remote_recv.subscribe(heartbeatTopic, boost::bind(&RemoteSniffer::heartbeatResponse, this, _1));


	// set up the local packet topic
	localTopic = localApp + "/packets";

	cmdTopic = "control/";
	cmdTopic += APP_ID_WIFI_PROCESSOR;
	cmdTopic += "2";

	remote_recv.start();
	remote_send.start();
	local_send.start();

}

bool RemoteSniffer::setInjecting(bool param){
	json cmd_json;
	cmd_json["WiFiInjection"] = param;

	if(!param){
		std::cout << "Stopping injecting for " << _name << std::endl;
	}

	return remote_send.publish(cmdTopic, cmd_json.dump());
}

void RemoteSniffer::aggregateData(const struct mosquitto_message* message){

	saw_packet = true;

	ostringstream os;
	boost::archive::text_oarchive outarch(os);

	// get a timestamp (in UTC, please!)
	boost::posix_time::ptime ts = boost::posix_time::microsec_clock::universal_time();

	//for each packet read (may be more than one), construct a SnifferPacket object
	string msg(static_cast<const char*>(message->payload), message->payloadlen);

	istringstream is(msg);
	boost::archive::text_iarchive inarch(is);

	std::vector<SnifferPacket> sniff_v;
	inarch >> sniff_v;
	std::vector<TrackerPacket> track_v;
	track_v.reserve(sniff_v.size());

	for(auto it=sniff_v.begin(); it!=sniff_v.end(); it++){
		track_v.push_back(TrackerPacket(*it, ts, _name));
	}

	outarch << track_v;

	// send the new data packet on the local
	local_send.publish(localTopic, os.str());

}

void RemoteSniffer::heartbeatResponse(const struct mosquitto_message* message){
	// don't trust the remote clock in the heartbeat message.  Trust in your own clock.
	last_seen = boost::posix_time::microsec_clock::universal_time();
}

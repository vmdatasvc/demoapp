/*
 * MonitoringAP_short.cpp
 *
 *  Created on: Jun 30, 2014
 *      Author: pshin
 */

#include <iomanip>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <cstdarg>
#include <vector>
//#include <boost/algorithm/string/predicate.hpp>
#include <sys/param.h>	// for MAX() and MIN()
#include <cmath>
#include <iostream>
#include <pthread.h>

#include "modules/iot/IoT_Common.hpp"
#include "projects/WiFi_Shopper_Tracker/Config.hpp"
#include "projects/WiFi_Shopper_Tracker/RemoteDevice.hpp"
#include "modules/wifi/MessageType.hpp"

#include "projects/WiFi_Shopper_Tracker/MonitoringAP.hpp"
#include "modules/utility/TimeKeeper.hpp"

extern pthread_mutex_t gMutex;

using namespace std;

WiFiSensor_short::WiFiSensor_short() {

	for(int ch=0; ch<NUM_CHANNELS_TO_CAPTURE; ch++)
		mWiFiCalibStarted[ch] = false;

	mIsIPinitialized = false;
	mIsCurrentlyReceivingMessages = true;

#ifdef WIFI_CALIBRATION_MODE
	mRSS_avg = 0;
	mRSS_cnt = 0;
#endif

	mKillRequestPending = false;

	pthread_attr_init(&mThreadAttr);
	pthread_attr_setdetachstate(&mThreadAttr, PTHREAD_CREATE_JOINABLE);
	mStreamProcessing_thread = 0;

}

WiFiSensor_short::~WiFiSensor_short() {

}

KalmanFilter_4S *WiFiSensor_short::findKalmanCalibRSS(int sensorPos_x, int sensorPos_y, int ind_ch) {
	if(DEBUG_FUNCTION_CALL) printf("findKalmanSignal() is called. \n");

	KalmanFilter_4S *kal_ = NULL;

		if(mKalmanCalibRSS[ind_ch].size() > 0)
			for(list<KalmanFilter_4S>::iterator it = mKalmanCalibRSS[ind_ch].begin(); it != mKalmanCalibRSS[ind_ch].end(); ++it)
			{
				KalmanFilter_4S *kal = &*it;
				if(kal->mCenterOfSensingField_X == sensorPos_x && kal->mCenterOfSensingField_Y == sensorPos_y) {
					kal_ = kal;
					break;
				}
			}

	if(DEBUG_FUNCTION_CALL) printf("findKalmanSignal End.\n");
	return kal_;
}

// -- Connect to a remote WiFi Sensor, subscribe to its WiFi Data channel, and publish the stream to the local broker with the same topic.
bool WiFiSensor_short::ConnectAndGetStream(string subTopic, string trackerID_suffix) {

	mSubTopic_remoteData = subTopic;
	mTrackerID_suffix = trackerID_suffix;

	/* On connection, a client sets the "clean session" flag, which is sometimes also known as the "clean start" flag.
	 * If clean session is set to false, then the connection is treated as durable.
	 * This means that when the client disconnects, any subscriptions it has will remain and any subsequent QoS 1 or 2 messages
	 * will be stored until it connects again in the future. If clean session is true, then all subscriptions will be removed for the client
	 * when it disconnects.*/
	bool clean_session = true;

	// -- Client to local broker
	mMQTT_forwarder = new MosqMQTT_fwd((const char*)mDevName.c_str(), clean_session);

	// -- Initialize the mosquitto MQTT client
	{
		//myMQTT->threaded_set(true);		// -- Tell if this application is threaded or not
		//mMQTT_forwarder->username_pw_set((const char*)MQTT_BROKER_USERNAME, (const char*)MQTT_BROKER_PASSWORD);		// -- Set username and pwd to the broker
		//mMQTT_forwarder->will_set(mqtt->mPubTopic.c_str(), 0, NULL, 1, true);

		mMQTT_forwarder->mKeepalive = MQTT_BROKER_KEEP_ALIVE;    // Basic configuration setup for myMosq class
		mMQTT_forwarder->mBrokerPort = MQTT_BROKER_PORT;
		mMQTT_forwarder->mBrokerAddr = MQTT_BROKER_URL;

//		mMQTT_forwarder->mPubTopic_Data = subTopic;
//		mMQTT_forwarder->mPubTopic_Data.append("/");
//		mMQTT_forwarder->mPubTopic_Data.append(deviceID_suffix);
	}

	printf("MQTT is starting...\n");

	// -- Start the mosquitto MQTT client
	mMQTT_forwarder->start();

	// -- Create a MQTT thread for each remote OmniSensr running WiFi_PacketProcessor
	{
		if(pthread_create(&mStreamProcessing_thread, &mThreadAttr, StreamProcessingThread, (void *)this))
		{
			fprintf(stderr, "Error in pthread_create(PacketStreamParsingThread)\n");
			return false;
		} else {
			printf( " -- pthread_create(PacketStreamParsingThread) -- \n");
			return true;
		}
	}

}

void WiFiSensor_short::SendWiFiCalibMsg(unsigned int mode) {
	// -- Connect to the AP and get the results through a MQTT broker


    ostringstream cmd_pub_control, controlMsg, controlTopic;

    // -- the control messages should be published to 'control/<app_id>/<store_name>/<device_name>' (e.g., control/wifiRSS/vmhq/16801/cam001)
    controlTopic << AWS_IOT_CONTROL_CHANNEL_PREAMBLE << "/" << APP_ID_WIFI_PROCESSOR; // <<"/" << mIoTGateway.GetMyStoreName() << "/" << mIoTGateway.GetMyDevName();

    controlMsg << "\'{\"wifiCalib\":{\"Mode\":" << mode << "}}\'";


	char mqtt_sub_cmd[299];
	sprintf(mqtt_sub_cmd, "mosquitto_pub -h %s -p %d -q %d -k %d -t \'%s\' -m ", mIPaddr.c_str(),
							MQTT_BROKER_PORT, MQTT_BROKER_QOS, MQTT_BROKER_KEEP_ALIVE, controlTopic.str().c_str());

	// -- Put a timeout to the system command
	cmd_pub_control << "timeout 7 ";

    cmd_pub_control << mqtt_sub_cmd << controlMsg.str();

	printf("[WiFiSensor_short [%s]] mqtt_sub_cmd = %s\n", mIPaddr.c_str(), cmd_pub_control.str().c_str());

	system(cmd_pub_control.str().c_str());
}

void WiFiSensor_short::StreamProcessing_Loop() {

	// -- Connect to the AP and get the results through a MQTT broker
	char mqtt_sub_cmd[299];
	sprintf(mqtt_sub_cmd, "mosquitto_sub -h %s -p %d -q %d -k %d -t \"%s\" -v", mIPaddr.c_str(),
							MQTT_BROKER_PORT, MQTT_BROKER_QOS, MQTT_BROKER_KEEP_ALIVE, mSubTopic_remoteData.c_str());

	printf("[WiFiSensor_short [%s]] mqtt_sub_cmd = %s\n", mIPaddr.c_str(), mqtt_sub_cmd);

	// -- Subscribe to a MQTT broker
	FILE *streamFromRemote;
	streamFromRemote = popen(mqtt_sub_cmd, "r");
//	int streamFromRemoteFd = fileno(streamFromRemote);

    char buffer[MAX_BUFFER];

	// -- Read from remote broker and publish to local broker
    string lineInput;
    string topic, SN, IP;
    ostringstream payload;


//    while(true) {
//    	ssize_t rSize = read(streamFromRemoteFd, buffer, sizeof(buffer));
//
//        if (rSize == -1)
//            continue;

	while(fgets(buffer, MAX_BUFFER, streamFromRemote)) {	// -- Reads a line from the pipe and save it to buffer
		lineInput.clear();
		lineInput.append(buffer);

        if(mKillRequestPending == true)
            break;

		// -- Break the line input into words and save it into a word vector
		istringstream iss (lineInput);
		vector<string> v;
		string s;
		while(iss >> s)
			v.push_back(s);

		// -- If the size of word vector is greater than one, then it should be the first line of message containing {topic, #SN, IP}
		if(v.size() == 3) {

			//printf("[streamFromRemote HEADER] %s", lineInput.c_str());

			// -- If there's a buffer to send out, then flush it first before getting the next message
			if(payload.str().size() > 0 && topic.size() > 0) {
				// -- Publish to the local broker
				ostringstream topic_tracker;
				topic_tracker << topic << "/" << mTrackerID_suffix;

				if(mMQTT_forwarder->send_message(topic_tracker.str(), payload.str(), MQTT_BROKER_QOS /* QoS level */)) {
					printf("### mMQTT_forwarder send FAIL ### topic[%s]: payload [%s]\n", topic.c_str(), payload.str().c_str());
				}

				// -- Initialize payload
				payload.str("");
				payload.clear();
			}

			topic = v[0];
			SN = v[1];
			IP = v[2];

			// -- Insert the header
			payload << SN << " " << IP << endl;

			SN = SN.substr(1);	// -- Get rid of the first '#' from the string
		}
		else if(v.size() == 1) {

			//printf("[streamFromRemote BODY] %s", lineInput.c_str());

			// -- Insert the body message
			payload << lineInput;
		}
	}

    if(streamFromRemote != 0)
    	pclose(streamFromRemote);

	pthread_exit(NULL);

}

void* WiFiSensor_short::StreamProcessingThread(void *apdata) {
	WiFiSensor_short* pb = (WiFiSensor_short*)apdata;

	pb->StreamProcessing_Loop();

	return NULL;
}

bool WiFiSensor_short::AddToPerDistanceBinBuffer(calibMeas_t &m, int ind_channel, int maxSize) {

	// -- Choose the bin index
	int ind_bin;

//#if USE_LOG_DISTANCE_SPACE == true
	double dist_reference = 500;	// -- Unit: [cm]
	ind_bin = (int)((log((double)m.dist) - log(dist_reference)) / ((log((double)CALIB_MAX_SIGNAL_DISTANCE) - log(dist_reference))/(double)CALIB_NUM_DISTANCE_BINS));
//#else
//	ind_bin = (int)(m.dist / (CALIB_MAX_SIGNAL_DISTANCE/(float)CALIB_NUM_DISTANCE_BINS) + 0.5f);
//#endif

//		printf("(dist, CALIB_MAX_SIGNAL_DISTANCE) = (%f, %d), (log(m.dist) - log(dist_reference), log(CALIB_MAX_SIGNAL_DISTANCE), CALIB_NUM_DISTANCE_BINS) = (%f, %f, %d), ind_bin = %d\n",
//				m.dist, CALIB_MAX_SIGNAL_DISTANCE, log(m.dist) - log(dist_reference), log(CALIB_MAX_SIGNAL_DISTANCE), CALIB_NUM_DISTANCE_BINS, ind_bin);

	if(ind_bin < 0) ind_bin = 0;

//	else if(ind_bin >= CALIB_NUM_DISTANCE_BINS) ind_bin = CALIB_NUM_DISTANCE_BINS-1;
	if(ind_bin >= CALIB_NUM_DISTANCE_BINS) {
//		ind_bin = CALIB_NUM_DISTANCE_BINS - 1;
//
//		// -- Forget the older one if there are too many measurements
//		if(mCalibMeas[ind_channel][ind_bin].size() >= CALIB_MAX_NUM_MEASUREMENT_PER_BIN)
//			mCalibMeas[ind_channel][ind_bin].pop_front();
//
//		mCalibMeas[ind_channel][ind_bin].push_back(m);

		return false;
	} else {

		// -- Forget the older one if there are too many measurements
		if((int)mCalibMeas[ind_channel][ind_bin].size() >= maxSize)
			mCalibMeas[ind_channel][ind_bin].pop_front();

		mCalibMeas[ind_channel][ind_bin].push_back(m);

		return true;
	}
}

void WiFiSensor_short::AddToPerPeerBuffer(calibMeas_t &m, int ind_channel, int maxSize) {
	// -- Find peer
	calibMeasPerPeer_t *peerBuffer = findPeerBuffer(m.src, ind_channel);

	// -- Check if this is the first measurement from a new peer. If so, create a new buffer and add it.
	if(peerBuffer == NULL) {
		calibMeasPerPeer_t peerBufferNew;
		peerBufferNew.SN = m.src;
		peerBufferNew.measBuffer.push_back(m);

		// -- Add to the buffer list
		mCalibMeas_per_peer[ind_channel].push_back(peerBufferNew);

	} else {

		// -- Forget the older one if there are too many measurements
		if((int)peerBuffer->measBuffer.size() >= maxSize)
			peerBuffer->measBuffer.pop_front();

		// -- Add to the buffer
		peerBuffer->measBuffer.push_back(m);
	}
}


calibMeasPerPeer_t *WiFiSensor_short::findPeerBuffer(string peerSN, int ind_channel) {
	if(peerSN.empty()) return NULL;

	if(mCalibMeas_per_peer[ind_channel].size() > 0)
		for(list<calibMeasPerPeer_t>::iterator it = mCalibMeas_per_peer[ind_channel].begin(); it != mCalibMeas_per_peer[ind_channel].end(); ++it) {
			calibMeasPerPeer_t *measList_per_peer = &*it;
			if(measList_per_peer->SN.compare(peerSN) == 0)
				return measList_per_peer;
		}

	return NULL;
}


//float WiFiSensor_short::convertRSStoDist_log_Android(int r, int rssCalibOffset) {
//	//float dist = exp(RSS_to_LogDist_a*r + RSS_to_LogDist_b);
//	float dist = exp(mCalib_a_Android*(r-rssCalibOffset) + mCalib_b_Android);
//	return MAX(dist, 0);
//}
//
//int WiFiSensor_short::convertDist_logToRSS_Android(float dist, int rssCalibOffset) {
//	//float dist = exp(RSS_to_LogDist_a*r + RSS_to_LogDist_b);
//	int rss = (int)((log(dist) - mCalib_b_Android)/mCalib_a_Android + 0.5f);
//	return MIN(-5, rss+rssCalibOffset);
//}
//
//float WiFiSensor_short::convertRSStoDist_log_iOS(int r, int rssCalibOffset) {
//	//float dist = exp(RSS_to_LogDist_a*r + RSS_to_LogDist_b);
//	float dist = exp(mCalib_a_iOS*(r-rssCalibOffset) + mCalib_b_iOS);
//	return MAX(dist, 0);
//}
//
//int WiFiSensor_short::convertDist_logToRSS_iOS(float dist, int rssCalibOffset) {
//	//float dist = exp(RSS_to_LogDist_a*r + RSS_to_LogDist_b);
//	int rss = (int)((log(dist) - mCalib_b_iOS)/mCalib_a_iOS + 0.5f);
//	return MIN(-5, rss+rssCalibOffset);
//}
//
//float WiFiSensor_short::convertRSStoDist_log_All(int r, int rssCalibOffset) {
//	//float dist = exp(RSS_to_LogDist_a*r + RSS_to_LogDist_b);
//	float dist = exp(mCalib_a_all*(r-rssCalibOffset) + mCalib_b_all);
//	return MAX(dist, 0);
//}
//
//int WiFiSensor_short::convertDist_logToRSS_All(float dist, int rssCalibOffset) {
//	//float dist = exp(RSS_to_LogDist_a*r + RSS_to_LogDist_b);
//	int rss = (int)((log(dist) - mCalib_b_all)/mCalib_a_all + 0.5f);
//	return MIN(-5, rss+rssCalibOffset);
//}

float WiFiSensor_short::convertRSStoDist_log(int r, int ind_ch, int rssCalibOffset, bool isOmniSensr) {

	// -- If there is no calib info available, then return negative distance which is not a feasible number.
	if(mCalib_a[ind_ch] == 0 || mCalib_b[ind_ch] == 0)
		return -99999;

	if(r >= 0)
		return -99999;

	// -- This distant is the actual distance from the AP to the signal source
	//float dist = exp(RSS_to_LogDist_a*r + RSS_to_LogDist_b);
	float dist = exp(mCalib_a[ind_ch]*(r-rssCalibOffset) + mCalib_b[ind_ch]);

	// -- Calculate the distance only in x-y plane
	float distPlane;
	if(isOmniSensr == true)
		distPlane = dist;
	else {
		if(dist > mSensorPos_z)
			distPlane = sqrt(pow(dist,2) - pow(mSensorPos_z,2));
		else
			distPlane = dist;
	}

	return distPlane;//MAX(distPlane, 1);
}

int WiFiSensor_short::convertDist_logToRSS(float distPlane, int ind_ch, int rssCalibOffset, bool isOmniSensr) {

	// -- If there is no calib info available, then return positive RSS value which is not a feasible number.
	if(mCalib_a[ind_ch] == 0 || mCalib_b[ind_ch] == 0)
		return 99999;

	if(distPlane <= 0)
		return 99999;

	// -- Calculate the distance only in x-y plane
	float dist;
	if(isOmniSensr == true)
		dist = distPlane;
	else {
		if(distPlane > mSensorPos_z)
			dist = sqrt(pow(distPlane,2) + pow(mSensorPos_z,2));
		else
			dist = distPlane;
	}

	//float dist = exp(RSS_to_LogDist_a*r + RSS_to_LogDist_b);
	int rss = (int)((log((double)dist) - mCalib_b[ind_ch])/mCalib_a[ind_ch] + 0.5f) + rssCalibOffset;

	return rss;//MIN(-5, rss);
}

//float MonitoringAP_short::convertRSStoDistance(int r) {
//
//	if(DEBUG_FUNCTION_CALL) printf("convertRSStoDistance() is called. ");
//
//	float dist = 0;		// -- Unit: cm
//
//	// -- Estimate distance using a simple linearized mapping function
//
//	// -- Piece-wise linearized mapping function training
//	static bool isTrainingDone = false;
//
//	const int no_pieces =  sizeof(gDistance_trained)/sizeof(int) - 1;
//
//	// -- Piece-wise linear function parameters: y = a*x + b; // -- NOTE: (x = rss, y = dist [meter]) coordinates
//	static float a[no_pieces] = {0};	// -- slope
//	static float b[no_pieces] = {0};
//
//	if(isTrainingDone == false) {
//		// -- NOTE: (x = rss, y = dist [meter]) coordinates
//
//		for(int i=0; i<no_pieces; i++) {
//			a[i] = (gDistance_trained[i] - gDistance_trained[i+1])/(gRSS_trained[i] - gRSS_trained[i+1]);
//		}
//
//		for(int i=0; i<no_pieces; i++) {
//			b[i] = gDistance_trained[i] - a[i]*gRSS_trained[i];
//		}
//
//		isTrainingDone = true;
//	}
//
//	dist = 999999;
//
//	for(int i=0; i<no_pieces; i++)
//	{
//		if(r <= gRSS_trained[i]) {
//			dist = (float)(a[i]*r + b[i]);
//		}
//	}
//
//	if(dist == 999999) {
//		dist = (float)(a[0]*r + b[0]);
//	}
//
//	// -- In case dist is negative
//	dist = MAX(dist, 1);
//
//	if(DEBUG_FUNCTION_CALL) printf("convertRSStoDistance() is Ended. ");
//
//	return dist;
//}
//
//int MonitoringAP_short::convertDistToRSS(float dist) {
//
//	if(DEBUG_FUNCTION_CALL) printf("convertDistToRSS() is called. ");
//
//
//	//float dist = 0;		// -- cm
//	int r = 0;
//
//	// -- Estimate distance using a simple linearized mapping function
//
//	// -- Piece-wise linearized mapping function training
//	static bool isTrainingDone = false;
//
//	// -- Piece-wise linear function parameters: y = a*x + b;
//	static float a[2] = {0};	// -- slope
//	static float b[2] = {0};
//
//	if(isTrainingDone == false) {
//		// -- (dist [meter], rss) coordinates
//
//		for(unsigned int i=0; i<sizeof(gDistance_trained)/sizeof(int) - 1; i++) {
//			a[i] = (gRSS_trained[i] - gRSS_trained[i+1])/(gDistance_trained[i] - gDistance_trained[i+1]);
//		}
//
//		for(unsigned int i=0; i<sizeof(gDistance_trained)/sizeof(int) - 1; i++) {
//			b[i] = gRSS_trained[i] - a[i]*gDistance_trained[i];
//		}
//	}
//
//	if(dist <= gDistance_trained[1]) r = (float)(a[0]*dist + b[0]);
//	else r = (float)(a[1]*dist + b[1]);
//
//	// -- In case r is positive
//	r = MIN(r, 0);
//
//	if(DEBUG_FUNCTION_CALL) printf("convertDistToRSS() is Ended. ");
//
//	return r;
//}

void WiFiSensor_short::setSensorPos(int x, int y, int z) {
	mSensorPos_x = x;
	mSensorPos_y = y;
	mSensorPos_z = z;
}

void WiFiSensor_short::initNode(string DevName, string IPaddr, string _NIC, string SN) {
	//memcpy(mAP_IPaddr_s, IPaddr, strlen(IPaddr));
	//strncpy(mAP_IPaddr_s, IPaddr, 17);

	mDevName = DevName;
	mIPaddr = IPaddr;

	//mAP_IPaddr_s[17] = '\0';
	mSerialNumber = SN;
	mIsIPinitialized = true;
}


MosqMQTT_fwd::MosqMQTT_fwd(const char * _id, bool clean_session) : MosqMQTT(_id, clean_session) {

	 mIsConnected = false;

	 mosqpp::lib_init();        // Mandatory initialization for mosquitto library
	 mPid = getpid();

	 mClientId.str("");
	 mClientId.clear();
	 mClientId << _id << "-" << mPid;

	 reinitialise(mClientId.str().c_str(), clean_session);
}
MosqMQTT_fwd::~MosqMQTT_fwd() {
     loop_stop();            // Kill the thread
     mosqpp::lib_cleanup();    // Mosquitto library cleanup
}

void MosqMQTT_fwd::on_connect(int rc)
{
    if ( rc == 0 ) {     /* success */
        if(DEBUG_MQTT_AT_TRACKER)
            cout << ">> myMosq - connected with server" << endl;

        mIsConnected = true;

        // -- Subscribe to topics after a successful connection is made
        if(mSubTopics.size() > 0)
			for(vector<string>::iterator it = mSubTopics.begin(); it != mSubTopics.end(); ++it) {
				try {
					string itt = *it;
					int ret;
					if((ret = subscribe(NULL, (const char*)(itt.c_str()), 1)) == MOSQ_ERR_SUCCESS) {

						if(DEBUG_MQTT_AT_TRACKER)
							printf("[Mosquitto MQTT] Subscribed to %s \n", itt.c_str());
					} else {
						fprintf(stderr,"[Mosquitto MQTT] Subscribed to %s: FAIL [%d]\n", itt.c_str(), ret);
					}
				} catch (exception& e) {
					if(DEBUG_MQTT_AT_TRACKER)
						printf("Error: Failed to subscribe\n%s\n", e.what());
				}
			}

    } else {
        if(DEBUG_MQTT_AT_TRACKER)
            cout << ">> myMosq - Impossible to connect with server(" << rc << ")" << endl;

        // -- Start the mosquitto MQTT client
        start();
    }

 }

void MosqMQTT_fwd::on_disconnect(int rc)
{

    if(DEBUG_MQTT_AT_TRACKER)
        cout << ">> myMosq - disconnection(" << rc << ")" << endl;

    mIsConnected = false;

    while(1) {

        int ret = reconnect();

        if(DEBUG_MQTT_AT_TRACKER)
            cout << ">> myMosq - reconnect(" << ret << ")" << endl;

        if(ret == MOSQ_ERR_SUCCESS) break;
        else usleep(1e5);
    }
 }


void MosqMQTT_fwd::on_message(const struct mosquitto_message *msg) {
    if (msg == NULL) { return; }

    if(DEBUG_MQTT_AT_TRACKER)
		printf("-- got a msg @ Topic: %s (%d, QoS %d, %s), MSG: \n%s",
			msg->topic, msg->payloadlen, msg->qos, msg->retain ? "R" : "!r",
			(char *)msg->payload);

    if(msg->topic == NULL || msg->payload == NULL)
    	return;

	string msg_topic(msg->topic);
	string msg_payload((char *)msg->payload);
	msg_payload.resize(msg->payloadlen);

    if(msg_topic.empty() == true || msg_payload.empty() == true)
		return;

}



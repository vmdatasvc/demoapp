/*
 * WiFiTracker.cpp
 *
 *  Created on: Jan 24, 2017
 *      Author: jwallace
 */

#include "projects/WiFi_Shopper_Tracker/WiFiTracker.hpp"

#include "modules/iot/IoT_Common.hpp"

#include <fstream>
#include <sstream>
#include <algorithm>
#include <limits>

#include <boost/serialization/vector.hpp>

#include <boost/archive/text_iarchive.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

// Linear algebra library
#include <Eigen/Core>

// Include the implementation of the localizer here
#include "OLSLocalizer.hpp"
// Needed for weighted regression
#include "OLSRegression.hpp"

using std::vector;
using std::string;
using std::set;
using std::map;
using std::ostream;
using std::ostringstream;
using std::istringstream;
using std::ifstream;
using std::ofstream;
using std::endl;
using std::pair;

#include <iostream>
using std::cout;

using json=nlohmann::json;

WiFiTracker::WiFiTracker(const string& configfn) : OmniSensrApp(string(APP_ID_WIFI_TRACKER) + "2", configfn) {

	killed = false;
	curr_mode = Tracking;

	calib_max_distance = 200;
	calib_indep_channel = true;
	calib_min_packets = 20;
	calib_max_packets = 100;
	calib_equal_ch_wt = true;
	calib_equal_sensr_wt = true;
	calib_max_time = 600;
	device_plane = 3.0;

	track_known_devices = false;
	track_ap = false;
	track_service_agent = false;
	track_null_dev = false;
	packet_window = 2000;
	min_stationary_packets = 10;
	min_stationary_timespan = 15;

	min_trilateration_sensrs = 5;
	max_trilateration_sensrs = std::numeric_limits<unsigned int>::max();
	max_trilateration_distance = 200;

	// Get the specific localization implementation
	loc_impl = new OLSLocalizer<track_packet_buff_t::const_iterator>(this);

	min_track_time = 2*60;
	max_track_time = 120*60;
	min_trajectory_points = 5;
	max_dwell_radius = 5.0;
	max_device_time = 15*60;

	replay_wifi = false;
	realtime_reporting = false;
	public_realtime = false;

	registerVariable("realtimeReporting", realtime_reporting);
	registerVariable("publicRealtime", public_realtime);

	state_lock.lock();
	appState["wifiMode"] = Tracking;
	state_lock.unlock();
	registerDeltaCallback("wifiMode", boost::bind(&WiFiTracker::readOnlyCallback, this, "wifiMode", _1));

	heartbeat_interval = 120;

	// TODO: set the timestamps appropriately - perhaps to the epoch?
	curr_time = boost::posix_time::time_from_string("1970-01-01 00:00:00.000");
	process_time = boost::posix_time::time_from_string("1970-01-01 00:00:00.000");

	curr_realtime = boost::posix_time::second_clock::universal_time();
	last_heartbeat = curr_realtime;

	string packetTopic = APP_ID_WIFI_TRACKER;
	packetTopic += "/packets";

	data_recv.subscribe(packetTopic, boost::bind(&WiFiTracker::processPacket, this, _1));

	data_recv.start();
}

WiFiTracker::~WiFiTracker() {
	for(auto it=remote_monitors.begin(); it != remote_monitors.end(); it++){
		delete *it;
	}
	remote_monitors.clear();

	if(loc_impl){
		delete loc_impl;
	}

	for(auto it=device_tracks.begin(); it != device_tracks.end(); it++){
		for(auto it_b = it->second.begin(); it_b != it->second.end(); it++){
			delete (*it_b);
		}
	}

	for(auto it=sensor_tracks.begin(); it != sensor_tracks.end(); it++){
		for(auto it_b = it->second.begin(); it_b != it->second.end(); it++){
			delete (*it_b);
		}
	}

}

unsigned int WiFiTracker::initConfig(ParseConfigTxt& parser){
	unsigned int nparams = 0;

	// Calibration parameters
	nparams += parser.getValue("CALIB_MAX_DIST", calib_max_distance);
	nparams += parser.getValue("CALIB_INDEP_CHANNEL", calib_indep_channel);
	nparams += parser.getValue("CALIB_MIN_PACKETS", calib_min_packets);
	nparams += parser.getValue("CALIB_MAX_PACKETS", calib_max_packets);
	nparams += parser.getValue("CALIB_CHANNEL_EQ_WT", calib_equal_ch_wt);
	nparams += parser.getValue("CALIB_SENSOR_EQ_WT", calib_equal_sensr_wt);
	nparams += parser.getValue("CALIB_MAX_TIME", calib_max_time);
	nparams += parser.getValue("DEVICE_PLANE", device_plane);

	// Tracking parameters
	nparams += parser.getValue("ONLY_FOR_DEVICES_OF_INTEREST", track_known_devices);
	nparams += parser.getValue("TRACK_AP", track_ap);
	nparams += parser.getValue("TRACK_SERVICE_AGENT", track_service_agent);
	nparams += parser.getValue("TRACK_NULL_DEVICE", track_null_dev);
	nparams += parser.getValue("PACKET_WINDOW", packet_window);
	nparams += parser.getValue("MIN_STATIONARY_PACKETS", min_stationary_packets);
	nparams += parser.getValue("MIN_STATIONARY_TIMESPAN", min_stationary_timespan);

	// Localization Parameters
	nparams += parser.getValue("MIN_TRILATERATION_SENSR", min_trilateration_sensrs);
	nparams += parser.getValue("MAX_TRILATERATION_SENSR", max_trilateration_sensrs);
	nparams += parser.getValue("MAX_TRILATERATION_DISTANCE", max_trilateration_distance);

	// Track Summarization Parameters
	nparams += parser.getValue("MIN_SHOPPING_TIME", min_track_time);
	nparams += parser.getValue("MAX_SHOPPING_TIME", max_track_time);
	nparams += parser.getValue("MIN_SHOPPER_DETECTION", min_trajectory_points);
	nparams += parser.getValue("MAX_DWELL_RADIUS", max_dwell_radius);
	nparams += parser.getValue("TIME_THRESHOLD_DEVICE_DISAPPEAR", max_device_time);

	// debugging parameters
	nparams += parser.getValue("REALTIME_REPORTING", realtime_reporting);
	nparams += parser.getValue("PUBLIC_REALTIME", public_realtime);


	nparams += parser.getValue("KNOWN_AP_LIST_FILENAME", known_AP_fn);
	nparams += parser.getValue("KNOWN_SERVICE_AGENT_LIST_FILENAME", known_agent_fn);
	nparams += parser.getValue("MONITORING_AP_IP_ADDR_LIST_FILENAME", cam_details_fn);
	nparams += parser.getValue("CALIBRATION_FILENAME", calibration_params_fn);
	nparams += parser.getValue("KNOWN_DEVICES_FILENAME", known_dev_fn);

	nparams += parser.getValue("REPLAY_WIFI", replay_wifi);

	nparams += parser.getValue("HEARTBEAT_INTERVAL", heartbeat_interval);

	return nparams;
}

void WiFiTracker::saveState(ostream& os) const{

	// Calibration parameters
	os << "CALIB_MAX_DIST " << calib_max_distance << endl;
	os << "CALIB_INDEP_CHANNEL " << calib_indep_channel << endl;
	os << "CALIB_MIN_PACKETS " << calib_min_packets << endl;
	os << "CALIB_MAX_PACKETS " << calib_max_packets << endl;
	os << "CALIB_CHANNEL_EQ_WT " << calib_equal_ch_wt << endl;
	os << "CALIB_SENSOR_EQ_WT " << calib_equal_sensr_wt << endl;
	os << "CALIB_MAX_TIME " << calib_max_time << endl;
	os << "DEVICE_PLANE " << device_plane << endl;

	// Which devices to track?
	os << "ONLY_FOR_DEVICES_OF_INTEREST " << track_known_devices << endl;
	os << "TRACK_AP " << track_ap << endl;
	os << "TRACK_SERVICE_AGENT " << track_service_agent << endl;
	os << "TRACK_NULL_DEVICE " << track_null_dev << endl;
	os << "PACKET_WINDOW " << packet_window << endl;

	// Localization  Params
	os << "MIN_TRILATERATION_SENSR " << min_trilateration_sensrs << endl;
	os << "MAX_TRILATERATION_SENSR " << max_trilateration_sensrs << endl;
	os << "MAX_TRILATERATION_DISTANCE " << max_trilateration_distance << endl;

	// Track summarization
	os << "MIN_SHOPPING_TIME " << min_track_time << endl;
	os << "MAX_SHOPPING_TIME " << max_track_time << endl;
	os << "MIN_SHOPPER_DETECTION " << min_trajectory_points << endl;
	os << "MAX_DWELL_RADIUS " << max_dwell_radius << endl;
	os << "TIME_THRESHOLD_DEVICE_DISAPPEAR " << max_device_time << endl;

	os << "REALTIME_REPORTING " << realtime_reporting << endl;
	os << "PUBLIC_REALTIME" << public_realtime << endl;

	set_lock.lock();
	if (known_AP_fn.size() > 0){
		os << "KNOWN_AP_LIST_FILENAME " << known_AP_fn << endl;
		saveList(known_AP_fn, known_APs);
	}
	if (known_agent_fn.size() > 0){
		os << "KNOWN_SERVICE_AGENT_LIST_FILENAME " << known_agent_fn << endl;
		saveList(known_agent_fn, known_service_agents);
	}
	if(known_dev_fn.size() > 0){
		os << "KNOWN_DEVICES_FILENAME " << known_dev_fn << endl;
		saveList(known_dev_fn, devices_of_interest);
	}
	set_lock.unlock();


	os << "MONITORING_AP_IP_ADDR_LIST_FILENAME " << cam_details_fn << endl;
	os << "CALIBRATION_FILENAME " << calibration_params_fn << endl;

	os << "REPLAY_WIFI " << replay_wifi << endl;
	os << "HEARTBEAT_INTERVAL " << heartbeat_interval << endl;

}

void WiFiTracker::processCommand(const string& cmd) {

	try{
		json cmd_msg;
		istringstream(cmd) >> cmd_msg;

		// set the calibration mode
		if(cmd_msg["wifiCalib"] != nullptr && cmd_msg["wifiCalib"]["Mode"] != nullptr){
			if(!cmd_msg["wifiCalib"]["Mode"].is_number()){
				sendError("Improper format for Calibration mode in command channel", false);
			}
			if(static_cast<unsigned int>(curr_mode) != cmd_msg["wifiCalib"]["Mode"]){
				try{
					curr_mode = static_cast<WiFiMode>(cmd_msg["wifiCalib"]["Mode"].get<int>());
					state_lock.lock();
					appState["wifiMode"] = curr_mode;
					sendState("wifiMode");
					state_lock.unlock();
				} catch (std::exception& e){
					sendError(string("Error processing Calibration mode: ") + e.what(), false);
				}
			}
		}

		// clear the agent list
		if(cmd_msg["wifiTrack"] != nullptr){
			if(cmd_msg["wifiTrack"]["ResetAgentList"] != nullptr){
				try{
					if(cmd_msg["wifiTrack"]["ResetAgentList"].get<bool>()){
						set_lock.lock();
						known_service_agents.clear();
						set_lock.unlock();
					}
				}catch (std::exception& e){
					sendError(string("Error processing Reset Agent List command: ") + e.what(), false);
				}
			}
		}

	}catch(std::exception& e){
		sendError(string("Error processing command, caught exception: ") + e.what(), false);
	}

	// wake the mainloop
	main_lock.lock();
	main_cond.notify_one();
	main_lock.unlock();
}

void WiFiTracker::start(){
	// First things first, load up the data from the camera details
	bool read_aps = readAPList(cam_details_fn);
	if(!read_aps){
		sendError("Could not read camera details");
		throw std::runtime_error("No camera details");
	}

	if(calibration_params_fn.size() == 0){
		sendError("Calibration filename not provided");
		throw std::runtime_error("No calibration filename");
	}

	set_lock.lock();
	// try to read the known APs
	if(known_AP_fn.size() > 0){
		readList(known_AP_fn, known_APs);
	}
	// try to read the known service agents
	if(known_agent_fn.size() > 0){
		readList(known_agent_fn, known_service_agents);
	}
	set_lock.unlock();

	// try to read the devices of interest
	if(known_dev_fn.size() > 0){
		readList(known_dev_fn, devices_of_interest);
	}

	cout << "All Files read!" << endl;

	// attempt to read the calibration file
	if (!readCalibration(calibration_params_fn)){
		// if we're here, there's no (or insufficient) calibration parameters
		// we must calibrate
		curr_mode = Calibration;
		state_lock.lock();
		appState["wifiMode"] = curr_mode;
		sendState("wifiMode");
		state_lock.unlock();
	}

	// And we should be ready to track now
	while(!killed){
		switch(curr_mode){
		case Calibration:
			cout << "Entering calibration mode" << endl;
			// we must calibrate
			calibrate();
			break;
		case Tracking:
		case Verification:
			cout << "Entering tracking mode" << endl;
			// track the shoppers
			trackShoppers();
			break;
		}
	}

	// Let us save our state before we exit
	SaveSetting();

}

bool WiFiTracker::calibrate() {

	// PREcondition: we MUST be in "calibrate" mode

	// did calibration succeed?
	bool success = true;

	// add OmniSensr names here when ready for calibration
	set<string> notReadyForCalibration;
	set<string> allActiveSensr_SN;

	// sleep 15 seconds to allow all wifiSniffers to come online
	sleep(15);

	for(auto it = remote_sn_map.begin(); it != remote_sn_map.end(); it++){
		it->second->setInjecting(true);
	}

	// sleep for a second to make sure we're capturing packets
	sleep(1);

	// First things first, set all alive Remote Sniffers to injection mode
	for(auto it = remote_sn_map.begin(); it != remote_sn_map.end(); it++){
		// check to make sure that this sensor is alive
		if(it->second->isAlive()){
			// go ahead and make doubleplussure that we're in injecting mode
			it->second->setInjecting(true);
			notReadyForCalibration.insert(it->second->getName());
			allActiveSensr_SN.insert(it->first);
		} else {
			// probably won't get this command, but good to be sure
			it->second->setInjecting(false);
		}
	}

	// clear the packet buffer
	buffer_lock.lock();
	device_buffer.clear();
	buffer_lock.unlock();

	// wait here until we have a sufficient amount of data OR too much time has elapsed
	boost::posix_time::ptime calib_start = boost::posix_time::second_clock::universal_time();
	boost::posix_time::ptime curr_time = calib_start;

	// main calibration loop
	heartbeat(3*heartbeat_interval);
	boost::posix_time::ptime heartbeat_time = curr_time;

	while(notReadyForCalibration.size() >0 &&
		  static_cast<unsigned int>(abs((curr_time - calib_start).total_seconds())) < calib_max_time){

		// wait for data to be added
		main_lock.lock();
		main_cond.timed_wait(main_lock,boost::posix_time::milliseconds(heartbeat_interval * 1000));
		main_lock.unlock();

		// check to see if anybody new is ready for calibration
		for(auto it = notReadyForCalibration.begin(); it != notReadyForCalibration.end(); ){

			// As we get calibration data, remove it from the set that tells us that
			// it's not ready
			if(calibrationDataAvailable(*it, allActiveSensr_SN)){
				notReadyForCalibration.erase(it++);
			} else {
				++it;
			}
		}

		curr_time = boost::posix_time::second_clock::universal_time();

		// send a heartbeat if we need to
		if(static_cast<unsigned int>(abs((curr_time - heartbeat_time).total_seconds())) > heartbeat_interval){
			heartbeat_time = curr_time;
			heartbeat(3*heartbeat_interval);
		}
	}

	// OK, we've collected our data, set all Remote Sniffers (regardless of alive status) to sniffing mode only
	for(auto it = remote_monitors.begin(); it != remote_monitors.end(); it++){
		(*it)->setInjecting(false);
	}

	// Let's process this data now!
	buffer_lock.lock();

	if(notReadyForCalibration.size() > 0){
		sendError("Did not receive enough calibration packets, attempting with what we have", false);
	}

	bool all_pass = true;
	for(auto it = device_buffer.begin(); it != device_buffer.end(); it++){
		// NOTE: unlike in tracking mode, here the key of the device buffer is the
		// name of the OmniSensr that is receiving the packets

		// For each OmniSensr, run the calibration parameters
		boost::array<float, 7> params = calibrateSensor(it->first, it->second);
		// check for NaN in the parameters, which indicates a failure in calibration
		// calibration passed means that all parameters are finite
		bool calib_pass = true;
		for(auto p_it = params.begin(); p_it != params.end(); p_it++){
			calib_pass &= std::isfinite(*p_it);
		}

		if(calib_pass){
			calib_params[it->first] = params;
		}
		all_pass &= calib_pass;

	}

	// calibration success is dependent on all calibration parameters passing
	success &= all_pass;

		// clear the buffer; we're done with it
	device_buffer.clear();
	buffer_lock.unlock();

	// if we were successful, set the mode to tracking
	if(success){
		curr_mode = Tracking;
		state_lock.lock();
		appState["wifiMode"] = curr_mode;
		sendState("wifiMode");
		state_lock.unlock();
		if(!saveCalibration(calibration_params_fn)){
			sendError("Could not save calibration data to file", false);
		}
	}

	// just for good measure, send a heartbeat as we leave
	heartbeat(3*heartbeat_interval);

	return success;

}

bool WiFiTracker::calibrationDataAvailable(const string& receiver_name, const set<string>& sender_SN) const{
	bool retval = false;

	//std::cout << "Checking for " << receiver_name << std::endl;

	// get the RemoteSniffer pointer of the receiver
	RemoteSniffer* recv = 0;
	auto nm_it = remote_name_map.find(receiver_name);
	auto dev_it = device_buffer.find(receiver_name);
	if(nm_it == remote_name_map.end() || dev_it == device_buffer.end()){
		// bail!  this should NEVER happen
		//cout << "Bad day" << endl;
		return false;
	}

	recv = nm_it->second;

	// mapping of sender serial No. to # of packets recv'd deom that sender
	map<string, boost::array<unsigned int, 3> > num_packet_map;
	// initialize everything to 0:
	for(auto s_it = sender_SN.begin(); s_it != sender_SN.end(); s_it++){

		// only initialize for senders who AREN't me! and who aren't too far away
		auto sn_it = remote_sn_map.find(*s_it);
		//cout << "S: " << *s_it
		//		<< " Present: " << (sn_it != remote_sn_map.end())
		//		<< " Me: " << (recv != sn_it->second)
		//		<< " Dist: " << recv->getSqDistance(*(sn_it->second))
		//		<< " Max_d: " << (calib_max_distance * calib_max_distance) << endl;

		if(sn_it != remote_sn_map.end() &&
		   recv != sn_it->second &&
		   recv->getSqDistance(*(sn_it->second)) < calib_max_distance * calib_max_distance){

			string sn_mac = *s_it;
			sn_mac.insert(sn_mac.begin(), 12-sn_mac.size(), '0');
			boost::to_upper(sn_mac);
			std::fill_n(&num_packet_map[sn_mac][0], 3, 0);
		}
	}

	// Walk through the packet map, totaling the number of packets per channel and per sender
	for(auto it = dev_it->second.begin(); it != dev_it->second.end(); it++){
		//cout << "Walking for " << boost::to_upper_copy(it->getPayload().getMAC_str()) << endl;

		// Find the sender in the num_packet_map
		auto npm_it = num_packet_map.find(boost::to_upper_copy(it->getPayload().getMAC_str()));
		if(npm_it != num_packet_map.end()){
			++(npm_it->second[calib_indep_channel * ((it->getPayload().getChannel() - 1) / 5)]);
		}
	}

	// Check to make sure that every sender has sent a minimum number of packets
	retval = true;
	for(auto npm_it = num_packet_map.begin(); retval && npm_it != num_packet_map.end(); npm_it++){
		//auto sn_it = remote_sn_map.find(npm_it->first.substr(1,std::string::npos));
		//std::cout << "R: " << receiver_name << " S: " << npm_it->first << "(" << (sn_it == remote_sn_map.end() ? "END" : sn_it->second->getName())
		//		<< ") D: " << (sn_it == remote_sn_map.end() ? -1.0 : recv->getSqDistance(*(sn_it->second))) << " ";
		for(unsigned int i=0; i <= static_cast<unsigned int>(calib_indep_channel * 2); i++){
			//std::cout << npm_it->second[i] << " ";
			retval &= (npm_it->second[i] >= calib_min_packets);
		}
		//std::cout << std::endl;
	}

	return retval;
}

boost::array<float, 7> WiFiTracker::calibrateSensor(const std::string& name, const track_packet_buff_t& pack_buff) const{
	// At this point, I know that I have sufficient data to calibrate a sensor.  No need to check.

	// initialize a bogus calibration
	boost::array<float, 7> retval;
	std::fill(retval.begin(), retval.end(), std::numeric_limits<float>::quiet_NaN());

	// Get the pointer to the omnisensor we're calibrating
	auto name_it = remote_name_map.find(name);
	// this reeally should never happen, but if it does, we can't calibrate!
	if(name_it == remote_name_map.end()){
		return retval;
	}
	RemoteSniffer& calib_os = *(name_it->second);

	// mapping of serial no -> (squared) distance
	map<string, float> sqdistance_map;
	// mapping of serial no -> # of packets for each channel
	map<string, boost::array<unsigned int, 3> > numpacket_map;

	// square our max distance for convenience later
	float max_sq_dist = calib_max_distance * calib_max_distance;

	for (auto it=remote_sn_map.begin(); it!=remote_sn_map.end(); it++){
		float sqdist = it->second->getSqDistance(calib_os);

		// TODO: make this a function
		string sn_str = it->first;
		sn_str.insert(sn_str.begin(), 12-sn_str.size(), '0');
		boost::to_upper(sn_str);

		sqdistance_map[sn_str] = sqdist;
		// initialize all channels to 0, but only if the distance is
		// less than our maximum
		if(sqdist < max_sq_dist){
			std::fill_n(&numpacket_map[sn_str][0], 3, 0);
		}
	}

	// Our goal is to find a least squares solution to
	// RSS = a_0 + a_1 * log(dist), where RSS and dist are known, a_0 and a_1
	// are free parameters.

	// If d is a column vector of log of distances, we can represent this in matrix form as:
	// [1 d] * [a_0; a_1] = [RSS], or using A = [1 d] (a nx2 matrix, where n is the number of measurements)
	// and b is a nx1 column vector of measurements, and x = [a_0; a_1], a 2x1 vector, then:
	// A*x = b, and there are a number of fast solvers for this equation.

	// Let's create a matrix of the maximum dimension
	// NOTE: instead of doing this 3 times for each channel, let's do all channels simultaneously
	unsigned int max_rows = (numpacket_map.size() - 1) * calib_max_packets * (1 + 2*calib_indep_channel);
	unsigned int max_cols = 2 + (4*calib_indep_channel);

	// "mapping" of array position to ID and channel of the packet
	vector<pair<std::string, unsigned char> > sender_map;
	// preallocate the max size
	sender_map.reserve(max_rows);

	// we must initialize to 0 - each row will set at most 2 (of potentially 6) measurements
	Eigen::MatrixXf A = Eigen::MatrixXf::Zero(max_rows, max_cols);

	// OK to leave the vectors uninitialized
	Eigen::VectorXf b = Eigen::VectorXf::Zero(max_rows);

	unsigned int nrows = 0;
	//cout << "Max dist: " << max_sq_dist;
	// Walk through all of the packets, inserting them into the matrix
	for(auto it=pack_buff.begin(); it!=pack_buff.end(); it++){
		string sn = it->getPayload().getMAC_str();
		auto dist_it = sqdistance_map.find(boost::to_upper_copy(sn));

		// If this is true, then the packet is eligible for inclusion in the calibration
		// (i.e., the distance b/w sender and receiver is sufficiently small)
		if(dist_it != sqdistance_map.end() && dist_it->second < max_sq_dist){

			//cout << "Looking at " << sn << " D: " << dist_it->second << endl;

			// get the channel index
			// NOTE: if we are calibrating all channels at once, will be 0.
			// o/w: 1 -> 0, 6 -> 1, 11 -> 2
			unsigned char ch_idx = calib_indep_channel * ((it->getPayload().getChannel() - 1) / 5);
			//cout << "Looking at packet, ch_idx " << static_cast<unsigned int>(ch_idx) << endl;

			// If we have not yet reached the maximum packet count for the channel, add the params to the matrix
			auto count_it = numpacket_map.find(sn);
			if(count_it != numpacket_map.end() && count_it->second[ch_idx] < calib_max_packets){
				A(nrows, 2*ch_idx) = 1;
				// Since dist_it->second holds the squared distance, and we want the log of the distance.
				// log(sqrt(d_sq)) = 1/2*log(d_sq)
				A(nrows, 2*ch_idx + 1) = log(dist_it->second) / 2;
				b(nrows) = it->getPayload().getRSS();
				sender_map.push_back(std::make_pair(sn, ch_idx));
				++count_it->second[ch_idx];
				++nrows;
			}
		}
	}

	// At this point, we have our matrix A and vector b, which both have nrows rows (must be < max_rows)
	// Let's get a view into A, which is essentially a submatrix of the correct number of rows
	auto A_sub = A.topRows(nrows);
	auto b_sub = b.head(nrows);

	//std::cout << "A: " <<  endl << A_sub << endl;
	//std::cout << "b: " << endl << b_sub << endl;

	// NOTE: this is to be used for weighted linear least squares, if we want
	Eigen::VectorXf w = Eigen::VectorXf::Ones(nrows);

	// Generate weights based on calib_equal_*_wt parameters
	if(calib_equal_ch_wt || calib_equal_sensr_wt){
		map<pair<string, unsigned char>, unsigned int> channel_total;

		// Total the number of per-sensor, per-channel packets
		for(unsigned int i=0; i<sender_map.size(); i++){
			string sensr = "";
			unsigned char channel = 0;
			if(calib_equal_ch_wt){
				channel = sender_map[i].second;
			}
			if(calib_equal_sensr_wt){
				sensr = sender_map[i].first;
			}
			++channel_total[std::make_pair(sensr, channel)];
		}

		// set the weight to 1/(# of packets)
		for(unsigned int i=0; i<sender_map.size(); i++){
			string sensr = "";
			unsigned char channel = 0;
			if(calib_equal_ch_wt){
				channel = sender_map[i].second;
			}
			if(calib_equal_sensr_wt){
				sensr = sender_map[i].first;
			}
			w(i) = 1/static_cast<float>(channel_total[std::make_pair(sensr, channel)]);
		}

		// NOTE: no need to normalize w, but we will anyway (will need to in the OLSLocalizer
		w *= (w.size()/w.norm());
	}

	Eigen::VectorXf x;
	Eigen::MatrixXf cov;
	float chisq;

	OLSRegression::weighted_regression<float>(A_sub, w, b_sub, x, cov, chisq);

	//cout << "x: " << endl << x << endl;
	//cout << "chisq: " << chisq << endl;

	// OK, now x has the values that we want, copy them into the retval
	// Note that if we are combining channels, this will make 3 copies of the solution
	for(unsigned int idx=0; idx<6; idx++){
		retval[idx] = x( (idx % 2) + calib_indep_channel * 2 * (idx/2));
	}

	// The R^2 value for the quality of the calibration
	float r_sq = 1 - chisq / (w.array() * (b_sub.array() - b_sub.mean()).array().square()).sum();
	retval[6] = r_sq;

	return retval;
}

void WiFiTracker::trackShoppers() {

	last_heartbeat = boost::posix_time::microsec_clock::universal_time();
	heartbeat(3*heartbeat_interval);

	// only proceed with this processing loop iff we are NOT calibrating
	while(!killed && (curr_mode != Calibration)){
		main_lock.lock();
		main_cond.timed_wait(main_lock,boost::posix_time::seconds(heartbeat_interval));
		main_lock.unlock();


		// check the current time and the last processed time
		curr_realtime = boost::posix_time::microsec_clock::universal_time();

		// run the localization for each device being tracked

		// need to lock both the packet buffer and the track buffer
		track_lock.lock();
		buffer_lock.lock();
		track_point_buff_t tmp_buff;
		for(auto it=device_buffer.begin(); it!=device_buffer.end(); it++){
			auto buf_it = it->second.begin();
			if(buf_it != it->second.end()){
				tmp_buff.clear();
				if(buf_it->getPayload().getType() == SnifferPacket::OmniSensr ||
				   buf_it->getPayload().getType() == SnifferPacket::AP){
					localizeSensor(it->second, tmp_buff);
					if(tmp_buff.size() > 0){
						sensor_tracks[it->first].insert(tmp_buff.begin(), tmp_buff.end());
					}
				} else {
					localizeShopper(it->second, tmp_buff);
					if(tmp_buff.size() > 0){
						device_tracks[it->first].insert(tmp_buff.begin(), tmp_buff.end());
					}
				}
			}
		}
		// OK, we're done with the packet buffer, so we can let other threads go back to filling it
		// while we work on summarizing the tracks and sending them on
		buffer_lock.unlock();

		// Now, run the track summarization for each device being tracked to determine
		// what to send to AWS
		for(auto it=device_tracks.begin(); it!=device_tracks.end(); it++){
			summarizeDeviceTrack(it->first, it->second);
		}



		// remove any sensor tracks that are empty
		for(auto it=sensor_tracks.begin(); it != sensor_tracks.end(); ){
			if(it->second.empty()){
				sensor_tracks.erase(it++);
			} else {
				++it;
			}
		}
		track_lock.unlock();

		buffer_lock.lock();
		// remove any buffers that are empty
		for(auto it=device_buffer.begin(); it != device_buffer.end(); ){
			if(it->second.empty()){
				device_buffer.erase(it++);
			} else {
				++it;
			}
		}
		buffer_lock.unlock();

		// OK, we should be ready for new data now

		// if a sufficient amount of (real) time has passed, send a heartbeat
		if(static_cast<unsigned int>(abs((curr_realtime - last_heartbeat).total_seconds())) > heartbeat_interval){
			last_heartbeat = boost::posix_time::microsec_clock::universal_time();
			heartbeat(3*heartbeat_interval);
		}
	}

	// we are exiting the tracking function.  clear ALL buffers!
	track_lock.lock();
	for(auto it=device_tracks.begin(); it!=device_tracks.end(); it++){
		for(auto it_q = it->second.begin(); it_q != it->second.end(); it_q++){
			delete *it_q;
		}
	}
	for(auto it=sensor_tracks.begin(); it!=sensor_tracks.end(); it++){
		for(auto it_q = it->second.begin(); it_q != it->second.end(); it_q++){
			delete *it_q;
		}
	}
	device_tracks.clear();
	sensor_tracks.clear();
	track_lock.unlock();

	buffer_lock.lock();
	device_buffer.clear();
	buffer_lock.unlock();

}

void WiFiTracker::localizeShopper(std::multiset<TrackerPacket>& packet_buff, track_point_buff_t& track_buff){
	// NOTE: it is assumed that packet_buff and track_buff have been locked with a mutex prior to
	// entering this function.  We'll be doing thread-unsafe things here...

	// get a pointer to the first element
	auto it_b = packet_buff.begin();

	// get a pointer to the element that we're working with
	auto it_e = it_b;

	// iterate over the entire packet buffer
	while(it_e != packet_buff.end()){
		// go until we have a sufficiently long period of silence
		while(it_e != packet_buff.end() &&
			 (it_e->getTimestamp() - it_b->getTimestamp()).total_milliseconds() < packet_window){
			//std::cout << "Localizing " << it_b->getPayload().getMAC_str()
			//		<< " T: " << (it_e->getTimestamp() - it_b->getTimestamp()).total_milliseconds() << std::endl;
			++it_e;
		}

		// OK, I know that either it_e is at the end OR I have a sufficiently long "silence gap"
		// If I'm at the end, compare with the current time to see if the "silence gap" is sufficiently long
		if(it_e != packet_buff.end() || (curr_time - it_b->getTimestamp()).total_milliseconds() >= packet_window){
			// at this point, it_b is the start of the ping storm and it_e is one past the end of the ping storm
			// NOTE: the hint in this case should be accurate for O(1) insertion time!

			std::cout << "Localizing " << it_b->getPayload().getMAC_str() << std::endl;

			TrackPoint* tp = localize(it_b, it_e);
			if(tp){
				track_buff.insert(track_buff.end(), tp);
			}

			// remove all of it_b through it_e from the packet buffer
			packet_buff.erase(it_b, it_e);

			// start with where we left off
			it_b = it_e;
		}
	}

	// at this point, the entire packet buffer has been processed, localized, and the results are in the tracking buffer
}

void WiFiTracker::localizeSensor(track_packet_buff_t& packet_buff, track_point_buff_t& track_buff){

	// get a pointer to the first element
	auto it_b = packet_buff.begin();

	// get a pointer to the element that we're working with
	auto it_e = it_b;

	unsigned int num_packets = 0;

	// iterate over the entire packet buffer
	while(it_e != packet_buff.end()){

		while(it_e != packet_buff.end() &&
			  num_packets++ >= min_stationary_packets &&
			  (it_e->getTimestamp() - it_b->getTimestamp()).total_milliseconds() < min_stationary_timespan){
			++it_e;
		}

		// OK, I know that either it_e is at the end OR I have a sufficiently long timespan and enough packets
		// If I'm at the end, compare with the current time to see if the "silence gap" is sufficiently long
		if(it_e != packet_buff.end() ||
		   (num_packets++ >= min_stationary_packets &&
		    (it_e->getTimestamp() - it_b->getTimestamp()).total_milliseconds() >= min_stationary_timespan)){
			// at this point, it_b is the start of the ping storm and it_e is one past the end of the ping storm
			// NOTE: the hint in this case should be accurate for O(1) insertion time!

			TrackPoint* tp = localize(it_b, it_e);
			if(tp){
				track_buff.insert(track_buff.end(), tp);
			}

			// remove all of it_b through it_e from the packet buffer
			packet_buff.erase(it_b, it_e);

			// start with where we left off
			it_b = it_e;
		}
	}
}

TrackPoint* WiFiTracker::localize(const track_packet_buff_t::const_iterator& beg, const track_packet_buff_t::const_iterator& end){
	//cout << "Localizing with " << std::distance(beg, end) << " points" << endl;
	return loc_impl->localize(beg, end, std::distance(beg, end));
}

void WiFiTracker::summarizeDeviceTrack(const std::string& name, track_point_buff_t& track_buff){

	//cout << "Summarizing track for " << name << endl;

	// First, let's see if we are a known service agent
	set_lock.lock();
	bool isAgent = known_service_agents.count(name);
	set_lock.unlock();

	// if we don't want to track svc agents, just clear the buffer
	if(isAgent && !track_service_agent){
		// make sure to delete those pointers!
		for(auto it=track_buff.begin(); it!=track_buff.end(); it++){
			delete *it;
		}

		track_buff.clear();
	}

	// much the same process here as the localizeShopper function.
	// we assume that track_buff has been sufficiently locked.
	auto it_b = track_buff.begin();
	auto it_e = it_b;
	auto it_p = it_e;

	// iterate over the entire track buffer
	while(it_e != track_buff.end()){

		unsigned int seq_num = 0;

		// go until we have a sufficiently long period of silence


		while(it_e != track_buff.end() &&
			 (static_cast<unsigned int>( ((*it_e)->getTimestamp() - (*it_p)->getTimestamp()).total_seconds() ) < max_device_time) ){


			//cout << "Silence_tm: " << static_cast<unsigned int>( ((*it_e)->getTimestamp() - (*it_p)->getTimestamp()).total_seconds() ) << endl;

			if(realtime_reporting && !(*it_e)->isSent()){
				//cout << "sending realtime report for " << name << ": " << (*it_e)->getX() << "," << (*it_e)->getY() << " S: " << (*it_e)->isSent() << endl;
				sendRealtimeReport(name, **it_e, ++seq_num);
				(*it_e)->setSent();
			}

			// check to see if we have an agent
			if( static_cast<unsigned int>( ((*it_e)->getTimestamp() - (*it_b)->getTimestamp()).total_seconds()) > max_track_time){
				// Uhh.. this IS an agent that we're just now finding out
				if(!isAgent){
					set_lock.lock();
					known_service_agents.insert(name);
					set_lock.unlock();
				}
				isAgent = true;
				if(track_service_agent){
					generateTrack(name, it_b, it_e);
					for(auto it=it_b; it!=it_e; it++){
						delete *it;
					}
					track_buff.erase(it_b, it_e);
					// Note the postincrement; after this line it_b and it_e will be one
					// element apart, and it_b will be where it_e was previously
					it_b = it_e++;
				} else {
					// make sure to delete those pointers!
					for(auto it=track_buff.begin(); it!=track_buff.end(); it++){
						delete *it;
					}

					track_buff.clear();
					it_b = it_e = track_buff.end();
				}
			} else {
				it_p = it_e;
				++it_e;
			}
		}

		// If I'm at the end, compare with the current time to see if the "silence gap" is sufficiently long
		//cout << "END: " << (it_p == track_buff.end()) << " CURR: " << curr_time << endl;
		if(it_e != track_buff.end() ||
			(it_p != track_buff.end() && static_cast<unsigned int>((curr_time - (*it_p)->getTimestamp()).total_seconds()) > max_device_time)){
			// at this point, it_b is the start of the ping storm and it_e is one past the end of the ping storm
			// NOTE: the hint in this case should be accurate for O(1) insertion time!

			cout << "Generating track for " << name << endl;

			generateTrack(name, it_b, it_e);
			for(auto it=it_b; it!=it_e; it++){
				delete *it;
			}
			track_buff.erase(it_b, it_e);

			// start with where we left off
			it_b = it_e;
		}
	}

}

void WiFiTracker::summarizeSensorTrack(const std::string& name, track_point_buff_t& track_buff){
	// just send
	// we assume that track_buff has been sufficiently locked.
	auto it_b = track_buff.begin();
	auto it_e = track_buff.end();

	// iterate over the entire track buffer
	while(it_e != track_buff.end()){

		unsigned int seq_num = 0;
		// go until we have a sufficiently long period of silence
		while(it_e != track_buff.end() &&
		      static_cast<unsigned int>( ((*it_e)->getTimestamp() - (*it_b)->getTimestamp()).total_seconds()) < max_track_time){

			if(realtime_reporting){
				sendRealtimeReport(name, **it_e, ++seq_num);
			}
			++it_e;
		}

		if(it_e != track_buff.end() ||
		   static_cast<unsigned int>((curr_time - (*it_b)->getTimestamp()).total_seconds()) > max_track_time){
			generateTrack(name, it_b, it_e, false);
			for(auto it=it_b; it!=it_e; it++){
				delete *it;
			}
			track_buff.erase(it_b, it_e);

			// start with where we left off
			it_b = it_e;
		}
	}
}

void WiFiTracker::generateTrack(const std::string& name, const track_point_buff_t::iterator& beg, const track_point_buff_t::iterator& end, bool compress) {

	// Here's the deal; we want to ensure that we're sending ONLY the "important" stuff.
	// We'll always send the first point and draw a ball of radius "max_dwell_radius" around it
	// All subsequent points that fall within this ball will be compressed.
	// The first time we step outside the ball, we send the last point that was inside
	// and start the process anew with the current point

	// If the beginning and end are identical, nothing to do
	if(beg != end){
		ostringstream os;
		boost::posix_time::ptime track_start = (*beg)->getTimestamp();

		json track_json;
		// dev_id is the MAC address we're tracking
		track_json["dev_id"] = name;
		track_json["start_time"] = convertTime(track_start);

		auto circ_start = beg;
		auto circ_prev = beg;
		float r_thresh = max_dwell_radius * max_dwell_radius;
		unsigned int cnt = 1;

		addTrackPoint(os, **beg, track_start);

		for(auto it = beg; it != end; it++){

			if(!compress ||
			   pow((*circ_start)->getX() - (*it)->getX(), 2) + pow((*circ_start)->getY() - (*it)->getY(), 2) > r_thresh){
				// We have stepped outside the ball
				if(circ_prev != circ_start){
					// this is true when there is >1 point in the ball
					addTrackPoint(os, **circ_prev, track_start);
					++cnt;
				}
				circ_start = it;
				addTrackPoint(os, **circ_start, track_start);
				++cnt;

			}

			circ_prev = it;
		}

		// make sure to send the last point
		if(circ_start != circ_prev){
			addTrackPoint(os, **circ_prev, track_start);
			++cnt;
		}

		track_json["A4"] = os.str();
		track_json["cnt"] = cnt;
		track_json["duration"] = ((*circ_prev)->getTimestamp() - track_start).ticks() / static_cast<double>(boost::posix_time::time_duration::ticks_per_second());

		// send to AWS on the "store/location/device" data subtopic
		if( static_cast<unsigned int>(((*circ_prev)->getTimestamp() - track_start).total_seconds()) >= min_track_time){
			sendData(track_json.dump(), getStoreLocation() + "/" + getDeviceName());
		}

	}

}

void WiFiTracker::addTrackPoint(ostream& os, const TrackPoint& p, const boost::posix_time::ptime& start) const {
	double offset = (p.getTimestamp() - start).ticks() / static_cast<double>(boost::posix_time::time_duration::ticks_per_second());
	os << std::fixed << std::setprecision(1) << offset << "," << p.getX() << "," << p.getY() << ";";
}

void WiFiTracker::sendRealtimeReport(const std::string& name, const TrackPoint& t, unsigned int seq_num){
	json json_out;
	json_out["time"] = convertTime(t.getTimestamp());

	ostringstream os;
	os << name << "-" << t.getX() << "-" << t.getY() << "-" << seq_num;

	json_out["loc"] = os.str();

	if(public_realtime){
		sendData(json_out, "realTimeLocs");
	} else {
		sendPrivateData(json_out, "realTimeLocs");
	}

}


// process a (set of) packets
void WiFiTracker::processPacket(const struct mosquitto_message* msg){
	string msg_str(static_cast<const char*>(msg->payload), msg->payloadlen);

	//cout << "Processing packet: " << msg_str << endl;
	istringstream is(msg_str);
	boost::archive::text_iarchive ar(is);

	vector<TrackerPacket> track_v;
	ar >> track_v;
	bool added = false;
	for(auto it=track_v.begin(); it!= track_v.end(); it++){
		added |= addPacket(*it);
	}

	if(added){
		// wake the mainloop
		main_lock.lock();
		//cout << "Notifying" << endl;
		main_cond.notify_one();
		main_lock.unlock();
	}

	//cout << "Processed " << track_v.size() << endl;
}

bool WiFiTracker::addPacket(const TrackerPacket& tp){

	bool added = false;

	// get the timestamp and set it to the "current" time
	if((tp.getTimestamp() - curr_time).total_microseconds() > 0){
		curr_time = tp.getTimestamp();
	} else if ((tp.getTimestamp() - curr_time).total_microseconds() < 0){
		sendError("Time is moving backwards, keeping current time", false);
	}

	// Add the packet to the appropriate device buffer
	buffer_lock.lock();

	// check to see what mode we're in, and make sure that the type is appropriate
	SnifferPacket::DeviceType pkt_type = tp.getPayload().getType();

	// shortcut for "I want to track this packet in tracking or verification mode
	bool want_track = (pkt_type == SnifferPacket::Station ||
			(track_ap && pkt_type == SnifferPacket::AP) ||
			(track_service_agent && pkt_type == SnifferPacket::ServiceAgent));

	// only add to the device buffer if it's a "packet of interest"
	if( (curr_mode == Calibration && pkt_type == SnifferPacket::OmniSensr) ||
		(curr_mode == Tracking && want_track) ||
		(curr_mode == Verification && (want_track || pkt_type == SnifferPacket::OmniSensr)) ){

		added = true;

		// NOTE: if we are in calibration mode, we want to sort the packets by who RECEIVED them
		// whereas if we are in tracking mode (or verification), we want to sort the packets
		// by whoever SENT them
		// TODO: optimization potential: emplace with a hint for the end of the multiset
		if(curr_mode == Calibration){
			device_buffer[tp.getName()].insert(tp);
		} else {
			device_buffer[tp.getPayload().getMAC_str()].insert(tp);
		}
	}

	buffer_lock.unlock();

	return added;

}

void WiFiTracker::readOnlyCallback(const std::string& name, const std::string& msg){
	sendError("Cannot set " + name + " from ThingShadow desired state, ignoring", false);
}

bool WiFiTracker::readCalibration(const string& fn){

	ifstream fin(fn.c_str());
	string line;
	bool retval = fin.good();

	boost::array<float, 7> params;

	// clear the calibration parameters
	calib_params.clear();

	unsigned int lineno;
	while(getline(fin, line)){
		++lineno;
		vector<string> tokens;

		// remove comments
		line = line.substr(0, line.find("#"));
		boost::algorithm::trim(line);

		// ignore empty lines
		if(line.size() > 0){
			boost::algorithm::split(tokens,line, boost::is_any_of(","));

			// make sure we have the proper format.
			// Must have either 4 or 8 tokens, depending on calib_indep_channel
			bool valid_line = (tokens.size() == static_cast<unsigned int>(2 + 2 + 4*calib_indep_channel));
			if(valid_line){
				try{
					for(unsigned int i=1; i<tokens.size()-1; i++){
						params[i-1] = boost::lexical_cast<float>(tokens[i]);
						// If combining into a single channel, just duplicate the parameters
						if(!calib_indep_channel){
							params[i+1] = params[i-1];
							params[i+3] = params[i-1];
						}
					}
					params[6] = boost::lexical_cast<float>(tokens[tokens.size() -1]);
				} catch (const boost::bad_lexical_cast& e){
					valid_line = false;
				}
			}

			if(valid_line){
				boost::algorithm::trim(tokens[0]);
				calib_params[tokens[0]] = params;
			} else {
				sendError("Malformed calibration file, line " + boost::lexical_cast<string>(lineno), false);
			}
		}
	}

	// make sure that for every working sniffer, we have calibration parameters
	for(auto it=remote_monitors.begin(); it!=remote_monitors.end(); it++){

		// retval is still valid if the monitor is not alive OR
		// the calibration parameters exist
		retval &= ( (!(*it)->isAlive()) ||
				(calib_params.find((*it)->getName()) != calib_params.end()));
	}

	return retval;
}

bool WiFiTracker::saveCalibration(const string& fn) const{
	// file format is the following:
	// Case of independent channels:
	// name,a_0_ch1,a_1_ch1,a_0_ch6,a_1_ch6,a_0_ch11,a_0_ch11
	// Case of merged channels:
	// name,a_0,a_1

	ofstream fout(fn.c_str());
	bool retval = false;

	unsigned int nlines = 0;
	while(fout.good() && nlines < calib_params.size()){

		for(auto it=calib_params.begin(); it!=calib_params.end(); it++){
			fout << it->first;
			for(int i=0; i < 2+4*calib_indep_channel; i++){
				fout << "," << it->second[i];
			}
			fout << "," << it->second[6] << endl;
			++nlines;
		}
	}

	retval = fout.good();
	fout.close();

	return retval;
}

bool WiFiTracker::readAPList(const string& fn){
	bool retval = false;
	ifstream file_in(fn.c_str());
	if(file_in.good()){
		string line;
		vector<string> tokens;
		unsigned int lineno = 0;
		unsigned int dataline = 0;
		while(getline(file_in, line)){
			++lineno;
			// remove everything after a "#"
			line = line.substr(0,line.find("#"));
			// trim whitespace
			boost::algorithm::trim(line);

			// NOTE: I want to drop the 1st non-empty line which is assumed to be a header
			// Fields must be (comma-separated):
			// 0 - name
			// 1 - MAC addr (wired)
			// 2 - Serial #
			// 3 - IP addr / hostname
			// 4 - x (pixels)
			// 5 - y (pixels)
			// 6 - rotation
			// 7 - z (ft)
			// 8 - x (ft)
			// 9 - y (ft)
			if(line.size() > 0 && ++dataline > 1){
				tokens.clear();
				boost::algorithm::split(tokens, line, boost::is_any_of(","));
				if(tokens.size() >= 10){
					try{
						float x, y, z;
						x = boost::lexical_cast<float>(tokens[8]);
						y = boost::lexical_cast<float>(tokens[9]);
						z = boost::lexical_cast<float>(tokens[7]) - device_plane;

						// Convert x,y,z here if needed

						// If we are replaying wifi, connect to the "remote" localhost
						// IMPORTANT: ensure that the WiFi sniffer is NOT running!
						// (i.e., no data being published on wifiRss/#)
						string host = "localhost";
						if(!replay_wifi){
							host = tokens[3];
						}

						RemoteSniffer *sniff = new RemoteSniffer(host, x, y, z, tokens[0]);
						remote_monitors.push_back(sniff);

						// Add sniff to any other convenience data structures here
						if(remote_sn_map.count(tokens[2]) > 0){
							sendError("Duplicate Camera serial numbers found for " + tokens[2], false);
						}
						remote_sn_map[tokens[2]] = sniff;

						if(remote_name_map.count(tokens[0]) > 0){
							sendError("Duplicate Camera names found for " + tokens[0], false);
						}
						remote_name_map[tokens[0]] = sniff;


					}catch(boost::bad_lexical_cast& e){
						sendError("Error parsing camera details on line " + boost::lexical_cast<string>(lineno) + " (bad lexical cast)");
					}
				} else {
					sendError("Error parsing camera details on line " + boost::lexical_cast<string>(lineno) + " (not enough fields)");
				}
			}
		}
		retval = true;
	}

	return retval;

}

bool WiFiTracker::readList(const string& fn, set<string>& output_set){
	// known APs are stored one per line, but ignore blanks and comments
	bool retval = false;
	ifstream file_in(fn.c_str());
	if(file_in.good()){
		string line;
		while(getline(file_in, line)){
			// remove everything after a "#"
			line = line.substr(0,line.find("#"));
			// trim whitespace
			boost::algorithm::trim(line);
			if(line.size() > 0){
				output_set.insert(line);
			}
		}
		retval = true;
	}

	return retval;
}

bool WiFiTracker::saveList(const string& fn, const set<string>& input_set) const{
	ofstream file_out(fn.c_str());

	auto it = input_set.begin();
	while(it != input_set.end() && file_out.good()){
		file_out << *it << endl;
		++it;
	}

	file_out.close();

	return (it == input_set.end());

}

int main(int argc, char** argv){
	string configfn = "config/WiFi_Shopper_Tracker/WiFi_Shopper_Tracker_config.txt";

	if(argc >= 2){
		configfn = argv[1];
	}

	WiFiTracker wft(configfn);
	wft.Run();

	return 0;
}


/*
 * WiFiAggregator.cpp
 *
 *  Created on: Feb 23, 2017
 *      Author: jwallace
 */

#include "projects/WiFi_Shopper_Tracker/WiFiAggregator.hpp"

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include <vector>
#include <fstream>
#include <iostream>

using std::string;
using std::vector;
using std::ifstream;
using std::endl;

const std::string WiFiAggregator::appName = "wifiAgg";

WiFiAggregator::WiFiAggregator(const string& configfn) :
		OmniSensrApp(appName, configfn),
		heartbeat_interval(60),
		killed(false) {

	// Nothing to initialize

}

WiFiAggregator::~WiFiAggregator() {

	// delete all remote sniffers
	for (auto it=remote_monitors.begin(); it!=remote_monitors.end(); it++){
		delete *it;
	}
}

void WiFiAggregator::start(){
	// read the camera details
	if(!readCamList(cam_detail_fn)){
		sendError("Could not read camera file", true);
	}

	boost::posix_time::ptime curr_time = boost::posix_time::second_clock::universal_time();
	boost::posix_time::ptime heartbeat_time = curr_time;
	heartbeat(3*heartbeat_interval);

	// basically, do nothing; the work is done by the individual remote sniffers
	while(!killed){
		main_lock.lock();
		main_cond.timed_wait(main_lock,boost::posix_time::seconds(heartbeat_interval));
		main_lock.unlock();

		curr_time = boost::posix_time::second_clock::universal_time();

		if(static_cast<unsigned int>( (curr_time - heartbeat_time).total_seconds() ) > heartbeat_interval){
			heartbeat_time = curr_time;
			heartbeat(3*heartbeat_interval);
		}
	}

}

unsigned int WiFiAggregator::initConfig(ParseConfigTxt& parser){
	unsigned int nparams = 0;

	nparams += parser.getValue("MONITORING_AP_IP_ADDR_LIST_FILENAME", cam_detail_fn);
	nparams += parser.getValue("HEARTBEAT_INTERVAL", heartbeat_interval);

	return nparams;
}

void WiFiAggregator::saveState(std::ostream& os) const{
	os << "MONITORING_AP_IP_ADDR_LIST_FILENAME " << cam_detail_fn << endl;
	os << "HEARTBEAT_INTERVAL " << heartbeat_interval << endl;
}

bool WiFiAggregator::readCamList(const string& fn){
	bool retval = false;
	ifstream file_in(fn.c_str());
	if(file_in.good()){
		string line;
		vector<string> tokens;
		unsigned int lineno = 0;
		unsigned int dataline = 0;
		while(getline(file_in, line)){
			++lineno;
			// remove everything after a "#"
			line = line.substr(0,line.find("#"));
			// trim whitespace
			boost::algorithm::trim(line);

			// NOTE: I want to drop the 1st non-empty line which is assumed to be a header
			// Fields must be (comma-separated):
			// 0 - name
			// 1 - MAC addr (wired)
			// 2 - Serial #
			// 3 - IP addr / hostname
			if(line.size() > 0 && ++dataline > 1){
				tokens.clear();
				boost::algorithm::split(tokens, line, boost::is_any_of(","));
				if(tokens.size() >= 4){
					// need x,y,z, but just dummy vals now
					float x=0, y=0, z=0;
					// Convert x,y,z here if needed

					RemoteSniffer *sniff = new RemoteSniffer(tokens[3], x, y, z, tokens[0], appName);
					remote_monitors.push_back(sniff);

				} else {
					sendError("Error parsing camera details on line " + boost::lexical_cast<string>(lineno) + " (not enough fields)");
				}
			}
		}
		retval = true;
	}

	return retval;

}

int main(int argc, char** argv){
	string configfn = "config/WiFi_Shopper_Tracker/WiFi_Shopper_Tracker_config.txt";

	if(argc >= 2){
		configfn = argv[1];
	}

	WiFiAggregator wfa(configfn);
	wfa.Run();

	return 0;
}


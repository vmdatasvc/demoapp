/*
 * WiFi_Shopper_Tracker.cpp
 *
 *  Created on: Jun 17, 2014
 *      Author: pshin
 */

// -- Refer to http://stackoverflow.com/questions/8425399/getting-stdout-from-an-executed-application


//#include "modules/utility/matrix.h"
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <algorithm>
#include <cctype>
#include <iostream>
#include <tuple>
#include <limits>

#include <dlib/optimization.h>

#include <boost/algorithm/string/predicate.hpp>		// -- For boost::iequals() for case insensitive comparison


#include "modules/utility/TimeKeeper.hpp"
#include "modules/utility/ParseConfigTxt.hpp"
#include "modules/utility/strutil.hpp"

#include "modules/iot/IOTCommunicator.hpp"

#include "projects/WiFi_Shopper_Tracker/Config.hpp"
#include "projects/WiFi_Shopper_Tracker/MonitoringAP.hpp"
#include "projects/WiFi_Shopper_Tracker/GUI.h"
#include "projects/WiFi_Shopper_Tracker/WiFi_Shopper_Tracker.hpp"

using namespace std;

extern pthread_mutex_t gMutex;

#ifdef USE_WIFI_PLAYER
Point GroundTruth_pos;
#endif

deque<Node_info_t> WiFi_Shopper_Tracker::mKnownAPs;
deque<Node_info_t> WiFi_Shopper_Tracker::mKnownServiceAgents;
deque<Node_info_t> WiFi_Shopper_Tracker::mDevicesOfInterest;
deque<OUI_Lookup_t> WiFi_Shopper_Tracker::mOUI_Lookup;
int WiFi_Shopper_Tracker::mPeriodicStateReportingInterval;
string WiFi_Shopper_Tracker::mAppStoreName;

double WiFi_Shopper_Tracker::mKalmanRssProcessNoise_var, WiFi_Shopper_Tracker::mKalmanRssMeasurementNoise_var, WiFi_Shopper_Tracker::mKalmanRssCovCoeff_pos_min, WiFi_Shopper_Tracker::mKalmanRssCovCoeff_accel_min, WiFi_Shopper_Tracker::mKalmanRssMaxStdAllowed;	// -- Unit: cm^2
double WiFi_Shopper_Tracker::mKalmanPosProcessNoise_var, WiFi_Shopper_Tracker::mKalmanPosMeasurementNoise_var, WiFi_Shopper_Tracker::mKalmanPosCovCoeff_pos_min, WiFi_Shopper_Tracker::mKalmanPosCovCoeff_accel_min;	// -- Unit: cm^2

WiFi_Shopper_Tracker::WiFi_Shopper_Tracker(string trackerId_suffix) {

    mMQTT_Tracker = NULL;
	mTrackerID_suffix = trackerId_suffix;
	mProcessId = getpid();
	mWiFiOperationMode_prev = WIFI_TRACKING;

		// -- Default debug Mode Selection
		mDebugVerboseLevel = 0;
		mDebugDeviceStatusVerboseMode = 0;
		mDebugTrackerStatus = true;
		mDebugCalib = false;
		mDebugLocalization = false;
		mDebugLocalizationErrorCode = false;

	mIsThereSomeProcessing = 0;
	mFoundDevice.clear();

	mStartWiFiCalib = false;
	mWiFiOperationMode = WIFI_TRACKING;
	mReportRealTimeTrackResult = false;

	memset(&mTrackerState, '\0', sizeof(TrackState_t));
	mTrackerState.WiFiSensorAvailability = 100;

    mShowSensors = true;
    mFontSizeScale = 1.0;
    mKillRequestPending = false;

	pthread_attr_init(&mThreadAttr);
	pthread_attr_setdetachstate(&mThreadAttr, PTHREAD_CREATE_JOINABLE);

    mPacketStreamParsing_thread = 0;
    mWiFiCalibProcessing_thread = 0;
    //mPeriodicIoTProcessing_thread = 0;
    mPeriodicBufferProcessing_thread = 0;
}

WiFi_Shopper_Tracker:: ~WiFi_Shopper_Tracker() {
    if(mMQTT_Tracker != NULL)
        delete mMQTT_Tracker;

    mRunningWiFiSensor.clear();
    mFoundDevice.clear();
    mKnownServiceAgents.clear();
    mKnownAPs.clear();
    mDevicesOfInterest.clear();
    mOUI_Lookup.clear();
}

void WiFi_Shopper_Tracker::showParsedMessage(tcpdump_t *meas) {
	if(DEBUG_FUNCTION_CALL) printf("showParsedMessage() is called. \n");
	if(meas == NULL) return;

	if(mShowParsedMsg) {
		if(meas->deviceType == AP)
			printf("%.3lf AP[%s]: AP[%s] -> Station[%s] RSS=%d dB (Dist = %.1f meter) @Ind_Ch %d [%s]\n", gTimeSinceStart(), meas->capturedBy.c_str(), meas->src.c_str(),  meas->dst.c_str(), meas->rss, meas->distToAP/100.0f, meas->channel, meas->protocol_s);
		 else
			printf("%.3lf AP[%s]: Station[%s] -> AP[%s] RSS=%d dB (Dist = %.1f meter) @Ind_Ch %d [%s]\n", gTimeSinceStart(), meas->capturedBy.c_str(), meas->src.c_str(),  meas->dst.c_str(), meas->rss, meas->distToAP/100.0f, meas->channel, meas->protocol_s);
		fflush(stdout);
	}

	if(DEBUG_FUNCTION_CALL) printf("showParsedMessage End.\n");
}

WiFiSensor_short_Ptr WiFi_Shopper_Tracker::findRunningWiFiSensor(int x, int y) {
	//if(DEBUG_FUNCTION_CALL) printf("findRunningAP() is called. \n");

	WiFiSensor_short_Ptr ap_ = NULL;

		if(mRunningWiFiSensor.size() > 0)
			for(std::deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it)
			{
				WiFiSensor_short_Ptr ap = *it;

				if(ap->mSensorPos_x == x && ap->mSensorPos_y == y) {
					ap_ = ap;
					break;
				}
			}

	//if(DEBUG_FUNCTION_CALL) printf("findRunningAP End.\n");
	return ap_;
}

WiFiSensor_short_Ptr WiFi_Shopper_Tracker::findRunningWiFiSensor(string addr) {
	if(DEBUG_FUNCTION_CALL) printf("findRunningAP() is called. \n");

	if(addr.empty()) return NULL;

	WiFiSensor_short_Ptr ap_ = NULL;

		if(mRunningWiFiSensor.size() > 0)
			for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it)
			{
				WiFiSensor_short_Ptr ap = *it;
//				if(ap->mIPaddr.compare(addr) == 0				// -- IP Address
//						|| ap->mSerialNumber.compare(addr) == 0		// -- Serial Number
//					) {
				if(boost::iequals(ap->mIPaddr, addr) == true				// -- IP Address
						|| boost::iequals(ap->mSerialNumber, addr) == true		// -- Serial Number
						|| boost::iequals(ap->mDevName, addr) == true		// -- Device Name
					) {
					ap_ = ap;
					break;
				}
			}

	if(DEBUG_FUNCTION_CALL) printf("findRunningAP End.\n");
	return ap_;
}


KalmanFilter_4S *WiFi_Shopper_Tracker::findKalmanSignal(RemoteDevicePtr dev, WiFiSensor_short_Ptr ap) {
	if(DEBUG_FUNCTION_CALL) printf("findKalmanSignal() is called. \n");

	KalmanFilter_4S *kal_ = NULL;

		if(dev->mKalmanAP_Signals.size() > 0 && ap != NULL)
			for(list<KalmanFilter_4S>::iterator it = dev->mKalmanAP_Signals.begin(); it != dev->mKalmanAP_Signals.end(); ++it)
			{
				KalmanFilter_4S *kal = &*it;
				if(kal->mCenterOfSensingField_X == ap->mSensorPos_x && kal->mCenterOfSensingField_Y == ap->mSensorPos_y) {
					kal_ = kal;
					break;
				}
			}

	if(DEBUG_FUNCTION_CALL) printf("findKalmanSignal End.\n");
	return kal_;
}

Node_info_t *WiFi_Shopper_Tracker::findAP(string addr) {
	if(DEBUG_FUNCTION_CALL) printf("findAP() is called. \n");

	if(addr.empty()) return NULL;

	Node_info_t *dev = NULL;
	unsigned int size;

	size = (uint)mKnownAPs.size();
	if(size > 0)
		for(unsigned int i=0; i<size; i++) {
			//if(mKnownAPs.at(i).addr.compare(addr) == 0) {
			if(boost::iequals(mKnownAPs.at(i).addr, addr) == true) {
				dev = &mKnownAPs.at(i);
				break;
			}
		}

	if(DEBUG_FUNCTION_CALL) printf("findAP End.\n");
	return dev;
}

Node_info_t *WiFi_Shopper_Tracker::findServiceAgent(string addr) {
	if(DEBUG_FUNCTION_CALL) printf("findServiceAgent() is called. \n");

	if(addr.empty()) return NULL;

	Node_info_t *dev = NULL;
	unsigned int size;

	size = (uint)mKnownServiceAgents.size();
	if(size > 0)
		for(unsigned int i=0; i<size; i++) {
			//if(mKnownServiceAgents.at(i).addr.compare(addr) == 0) {
			if(boost::iequals(mKnownServiceAgents.at(i).addr, addr) == true) {
				dev = &mKnownServiceAgents.at(i);
			}
		}

	if(DEBUG_FUNCTION_CALL) printf("findServiceAgent End.\n");
	return dev;
}

bool WiFi_Shopper_Tracker::isDevicesOfInterest(string addr) {
	if(DEBUG_FUNCTION_CALL) printf("isDevicesOfInterest() is called. \n");

	if(addr.empty()) return NULL;

	bool result = false;
	unsigned int size;

	size = (uint)mDevicesOfInterest.size();
	if(size > 0)
		for(unsigned int i=0; i<size; i++) {

			//if(mDevicesOfInterest.at(i).addr.compare(addr) == 0) {
			if(boost::iequals(mDevicesOfInterest.at(i).addr, addr) == true) {
				result = true;
				break;
			}
		}

	if(DEBUG_FUNCTION_CALL) printf("isDevicesOfInterest End. w/ %d\n", result);
	return result;
}

RemoteDevicePtr WiFi_Shopper_Tracker::findDevice(string addr) {
	if(DEBUG_FUNCTION_CALL) printf("findDevice() is called. \n");

	if(addr.empty()) return NULL;

	RemoteDevicePtr dev_ = NULL;

	if(mFoundDevice.size() > 0)
		for(deque<RemoteDevicePtr>::iterator it = mFoundDevice.begin(); it != mFoundDevice.end();++it) {
			RemoteDevicePtr dev = *it;

			if(dev == NULL)
				continue;

			//if(dev->GetDevAddr().compare(addr) == 0) {
			if(boost::iequals(dev->GetDevAddr(), addr) == true) {
				dev_ = dev;
				break;
			}
		}

	if(DEBUG_FUNCTION_CALL) printf("findDevice End. 2\n");
	return dev_;
}

// -- If the size of trajectory is too large, then compress the entire trajectory by getting an approximated contour.
/*
 * Assumption: WiFi-based shopper trajectories will be reconstructed and RESAMPLED, allowing us to keep just approximated trajectory.
 * Given a radius from the previous point, if the current point is within this radius, then merge it to the previous point.
 */
void WiFi_Shopper_Tracker::SimplifyTrajectory(RemoteDevicePtr dev, float precision_dist /*Unit: meter*/, float precision_time /*Unit: sec*/) {
	if((int)dev->GetTrackedPosBufSize() < 3)
		return;

	deque<TrackedPos_t_> simplifiedTraj;

	// -- Set the first location
	TrackedPos_t_ loc_prev = *(dev->AtTrackedPosBuf()->end() - 1);

	simplifiedTraj.push_front(loc_prev);

	float distTraveled, timeTraveled;

	// -- Go through from the end of the list since the end is the beginning of the trajectory.
	int howManyLocsAreSkipped = 0;
	for(deque<TrackedPos_t_>::iterator it = dev->AtTrackedPosBuf()->end() - 2; it >= dev->AtTrackedPosBuf()->begin(); --it) {
		TrackedPos_t_ loc_curr = *it;

		// -- Distance between loc_prev and loc_curr in [meter]
		distTraveled = sqrt(pow(loc_curr.pos.x - loc_prev.pos.x,2) + pow(loc_curr.pos.y - loc_prev.pos.y,2))/100.0f;

		// -- Time diff between loc_prev and loc_curr in [sec]
		timeTraveled = fabs(loc_curr.localtime - loc_prev.localtime);

		// -- Keep the location only if the travel distance is larger than precision.
		if(distTraveled > precision_dist || timeTraveled > precision_time) {

			simplifiedTraj.push_front(loc_curr);
			loc_prev = loc_curr;
			howManyLocsAreSkipped = 0;
		} else {
			howManyLocsAreSkipped++;		// -- TODO: Do we really need to count this?
		}
	}

	if(mDebugVerboseLevel > 1)
		if(dev->GetTrackedPosBufSize() > 20 && simplifiedTraj.size()/(float)dev->GetTrackedPosBufSize() < 0.9)
			printf("[SimplifyTrajectory()] Dev[%s]'s Trajectory size is reduced from [%d] --> [%d]\n", dev->GetDevAddr().c_str(), (int)dev->GetTrackedPosBufSize(), (int)simplifiedTraj.size());

	// -- Replace the trajectory buffer with the simplified one.
	*(dev->AtTrackedPosBuf()) = simplifiedTraj;

}

// -- Compose a trajectory message and send it to the cloud
/* Sample Vision Trajectory format in .csv
 * The three tuple is {dt,x,y}, where dt is the time elapsed from the start time, x & y is in pixel format.
Start Time,Duration,Trajectory
2014-11-08T07:03:25.193,5.37,"<trajectory id='1' start_time='2014-11-08T07:03:25.193'>0.000,272,136;0.663,260,128;1.331,252,120;1.993,244,120;2.405,268,116;3.076,244,116;3.332,252,116;5.370,252,116</trajectory>"
2014-11-08T07:03:30.928,2.004,"<trajectory id='3' start_time='2014-11-08T07:03:30.928'>0.000,192,136;2.005,192,136</trajectory>"

The Wifi Trajectories Data structure for publishing wifi trajectories is:

{
	"start_date" : start_date,
	"start_time": start_time,
	"A4": set of t,x,y Locations,
	"end_time" : TimeOfLastLocation,
	"mac" : Device Mac Address
}

The Publish Topic should be: data/wifiTrack/{retailer/store number}/{device_name}, where {retailer/store number} located in /usr/local/www/store_name.txt and {device_name} in /usr/local/www/dev_name.txt

Sample data set:
{
	"start_date" : “2016-09-03”,
	"start_time": “12:45:00.003”,
	# -- "A4": “(0|2535,2071), (19.3|2520,2072), (45.0|2508,2072), (65.2|2496,2072), (87.9|1137,2156)”,
	"A4": “0,2535,2071;19.3,2520,2072;45.0,2508,2072;65.2,2496,2072”,
	"end_time" : “13:47:02.05”,
	"mac" : "64:9a:be:bc:d5:be"
}

// -- Running Time = 23:45:54 (2016-09-07 10:09:37 AM)
sprintf(str_msg_2, "Running Time = %.2d:%.2d:%.2d (%s)", hr, min, sec, currentDateTime().c_str());

 */
void WiFi_Shopper_Tracker::SendCompletedTrajectory_summarized(RemoteDevicePtr dev) {

	// -- If the size of trajectory is too large, then compress the entire trajectory by getting an approximated contour.
	if((int)dev->GetTrackedPosBufSize() >= mTrackedPointsSizeToReport_min*3) {
		SimplifyTrajectory(dev, mTrajectoryPrecision_dist, mTrajectoryPrecision_time);
	}


//	string start_date, start_time, end_date, end_time;

	if((int)dev->GetTrackedPosBufSize() >= mTrackedPointsSizeToReport_min) {

		double timeMeas_start = (dev->AtTrackedPosBuf()->end() - 1)->localtime;

//		mClock.updateTime(timeMeas_start);
//
//		start_date = mClock.mDateOnly_str;
//		start_time = mClock.mTimeOnly_str;

		double timeMeas_end = dev->AtTrackedPosBuf()->begin()->localtime;
//		mClock.updateTime(timeMeas_end);
//
//		end_date = mClock.mDateOnly_str;
//		end_time = mClock.mTimeOnly_str;

		int tripDuration = (int)(timeMeas_end - timeMeas_start); // -- Unit: [sec]

		ostringstream traj;

		// -- Go through from the end of the list since the end is the beginning of the trajectory.
		for(deque<TrackedPos_t_>::iterator it = dev->AtTrackedPosBuf()->end() - 1; it >= dev->AtTrackedPosBuf()->begin();) {
			TrackedPos_t_ loc = *it;
			double elapsedTime = loc.localtime - timeMeas_start;

			traj << fixed << showpoint << setprecision(1);	// -- Make sure the precision is 1/10 second.
			//traj << "(" << elapsedTime << "|" << loc.pos.x << "," << loc.pos.y << ")";
			traj << elapsedTime << "," << loc.pos.x << "," << loc.pos.y << ";";

			// -- Chop the message if it gets too large to send to AWS IoT cloud
            if((int)traj.str().size() > MAX_AWS_IOT_DATA_MSG_SIZE*1024) {
			//if(totalLocs > 5) {		// -- TODO: TEMP
				dev->AtTrackedPosBuf()->erase(it);
				break;
			} else
				it = dev->AtTrackedPosBuf()->erase(it) - 1;
		}

		try {

			stringstream jsonStr;

			jsonStr << fixed << showpoint << setprecision(1);	// -- Make sure the precision is 1/10 second.

			jsonStr << "{"
						//<< "\"tracker_id\" : \"" << mTrackerID.str() << "\","
						<< "\"dev_id\":\"" << dev->GetDevAddr() << "\","		// -- TODO: Device ID may be encrypted or hashed before sending.
						//<< "\"start_date\":\"" << start_date << "\","
						//<< "\"start_time\":\"" << start_time << "\","
						<< "\"start_time\":" << timeMeas_start << ","
						<< "\"A4\":\"" << traj.str() << "\","
						<< "\"cnt\":" << dev->GetTrackedPosBufSize() << ","
						<< "\"duration\":" << tripDuration
					<< "}";

			//printf("Node[%s]: trajectory = \n %s\n", dev->GetDevAddr().c_str(), jsonStr.str().c_str());

			json trajectoryMsg;
			trajectoryMsg << jsonStr;

			// serialize JSON
			if(mDebugLocalization) {
				printf("Device[%s]: trajectory msg to cloud = \n", dev->GetDevAddr().c_str());
				std::cout << std::setw(2) << trajectoryMsg << '\n';
			}

			// -- Publish/report the real-time tracking results to AWS IoT Cloud
			{

	//			jsonStr.str("");
	//			jsonStr.clear();
	//
	//			// -- NOTE: Data message must be in JSON format to be processed by Rules Engine at AWS IoT cloud
	//			// -- NOTE: Don't format it like using std::setw(2), since then Lambda function at AWS wouldn't be able to parse the JSON. Don't know why.
	//			//jsonStr << std::setw(2) << trajectoryMsg << '\n';
	//			//jsonStr << "\"" << trajectoryMsg << "\"";
	//			jsonStr << trajectoryMsg;

				// -- Put into a message buffer in IoT Gateway for transmission to AWS Cloud via AWS_IOT_Connector module
				// -- The data message will be sent to a topic named by 'AWS_IOT_APP_DATA_CHANNEL_PREAMBLE/<store_name>/<serial_number>/<app_id>'
				//mIoTGateway.SendData(trajectoryMsg.dump(4));
				VM_IoT_Error_t errorCode =mIoTGateway.SendData("/completedTracks",trajectoryMsg.dump());
				//VM_IoT_Error_t errorCode = mIoTGateway.SendData(jsonStr.str());

				if(errorCode != NO_PROBLEM)
					cout << "VM_IoT_Error: mIoTGateway.SendData(): " << errorCode << endl;

		//		if(mMQTT_Tracker->send_message(mMQTT_Tracker->mPubTopic_Data, msg, MQTT_BROKER_QOS /* QoS level */)) {
		//			printf("### mMQTT_Tracker send FAIL ###\n");
		//		}
			}

		} catch (std::invalid_argument& e) {		// -- Handle exception when the received msg does not comply with JSON standard
			// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
			std::cout << "[completedTracks] invalid_argument: " << e.what() << '\n';
		} catch (std::out_of_range& e) {	// -- Handle exception when a non-existing filed is accessed.
			// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
			std::cout << "[completedTracks] out of range: " << e.what() << '\n';
		} catch (std::domain_error& e) {	// -- Handle exception when an incompatible conversion is attempted (e.g., float --> string).
			// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
			 std::cout << "[completedTracks] domain error: " << e.what() << '\n';
		}

		if(mDebugLocalization) {
			printf("Device[%s]: trajectory msg to cloud sent. DONE.\n", dev->GetDevAddr().c_str());
		}

	}
}

void WiFi_Shopper_Tracker::SendCompletedTrajectory(RemoteDevicePtr dev) {

	//string start_date, start_time, end_date, end_time;

	//if((int)dev->GetTrackedPosBufSize() >= mTrackedPointsSizeToReport_min)
	{

		// -- Get the start time from the latest tracked location
		double timeMeas_start = (dev->AtTrackedPosBuf()->end() - 1)->localtime;

//		mClock.updateTime(timeMeas_start);
//
//		start_date = mClock.mDateOnly_str;
//		start_time = mClock.mTimeOnly_str;

		double timeMeas_end = dev->AtTrackedPosBuf()->begin()->localtime;
//		mClock.updateTime(timeMeas_end);
//
//		end_date = mClock.mDateOnly_str;
//		end_time = mClock.mTimeOnly_str;

		double tripDuration = timeMeas_end - timeMeas_start; // -- Unit: [sec]
		int totalLocs = 0;

		ostringstream traj;

		// -- Go through from the end of the list since the end is the beginning of the trajectory.
		for(deque<TrackedPos_t_>::iterator it = dev->AtTrackedPosBuf()->end() - 1; it >= dev->AtTrackedPosBuf()->begin();) {
			TrackedPos_t_ loc = *it;
			double elapsedTime = loc.localtime - timeMeas_start;

			traj << fixed << showpoint << setprecision(1);	// -- Make sure the precision is 1/10 second.
			//traj << "(" << elapsedTime << "|" << loc.pos.x << "," << loc.pos.y << ")";
			traj << elapsedTime << "," << loc.pos.x << "," << loc.pos.y << ";";
			totalLocs++;

			// -- Chop the message if it gets too large to send to AWS IoT cloud
            if((int)traj.str().size() > MAX_AWS_IOT_DATA_MSG_SIZE*1024) {
			//if(totalLocs > 5) {		// -- TODO: TEMP
				dev->AtTrackedPosBuf()->erase(it);
				break;
			} else
				it = dev->AtTrackedPosBuf()->erase(it) - 1;
		}

		try {

			stringstream jsonStr;

			jsonStr << fixed << showpoint << setprecision(1);	// -- Make sure the precision is 1/10 second.

			jsonStr << "{"
						//<< "\"tracker_id\" : \"" << mTrackerID.str() << "\","
						<< "\"dev_id\":\"" << dev->GetDevAddr() << "\","		// -- TODO: Device ID may be encrypted or hashed before sending.
						//<< "\"start_date\":\"" << start_date << "\","
						//<< "\"start_time\":\"" << start_time << "\","
						<< "\"start_time\":" << timeMeas_start << ","
						<< "\"A4\":\"" << traj.str() << "\","
						<< "\"locCnt\":" << totalLocs << ","
						<< "\"duration\":" << tripDuration
					<< "}";

			//printf("Node[%s]: trajectory = \n %s\n", dev->GetDevAddr().c_str(), jsonStr.str().c_str());

			json trajectoryMsg;
			trajectoryMsg << jsonStr;

			// serialize JSON
			if(mDebugLocalization)
			{
				printf("Device[%s]: trajectory msg to cloud = \n", dev->GetDevAddr().c_str());
				std::cout << std::setw(2) << trajectoryMsg << '\n';
			}

			// -- Publish/report the real-time tracking results to AWS IoT Cloud
			{

	//			jsonStr.str("");
	//			jsonStr.clear();
	//
	//			// -- NOTE: Data message must be in JSON format to be processed by Rules Engine at AWS IoT cloud
	//			// -- NOTE: Don't format it like using std::setw(2), since then Lambda function at AWS wouldn't be able to parse the JSON. Don't know why.
	//			//jsonStr << std::setw(2) << trajectoryMsg << '\n';
	//			//jsonStr << "\"" << trajectoryMsg << "\"";
	//			jsonStr << trajectoryMsg;

				// -- Put into a message buffer in IoT Gateway for transmission to AWS Cloud via AWS_IOT_Connector module
				// -- The data message will be sent to a topic named by 'AWS_IOT_APP_DATA_CHANNEL_PREAMBLE/<store_name>/<serial_number>/<app_id>'
				//VM_IoT_Error_t errorCode = mIoTGateway.SendData(trajectoryMsg.dump(4));
				VM_IoT_Error_t errorCode = mIoTGateway.SendData("/completedTracks",trajectoryMsg.dump());
				//VM_IoT_Error_t errorCode = mIoTGateway.SendData(jsonStr.str());

				if(errorCode != NO_PROBLEM)
					cout << "VM_IoT_Error: mIoTGateway.SendData(): " << errorCode << endl;

		//		if(mMQTT_Tracker->send_message(mMQTT_Tracker->mPubTopic_Data, msg, MQTT_BROKER_QOS /* QoS level */)) {
		//			printf("### mMQTT_Tracker send FAIL ###\n");
		//		}
			}

		} catch (std::invalid_argument& e) {		// -- Handle exception when the received msg does not comply with JSON standard
			// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
			std::cout << "[completedTracks] invalid_argument: " << e.what() << '\n';
		} catch (std::out_of_range& e) {	// -- Handle exception when a non-existing filed is accessed.
			// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
			std::cout << "[completedTracks] out of range: " << e.what() << '\n';
		} catch (std::domain_error& e) {	// -- Handle exception when an incompatible conversion is attempted (e.g., float --> string).
			// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
			 std::cout << "[completedTracks] domain error: " << e.what() << '\n';
		}

		if(mDebugLocalization) {
			printf("Device[%s]: trajectory msg to cloud sent. DONE.\n", dev->GetDevAddr().c_str());
		}

	}
}

// -- MUST be a static function
//bool RuleToRemoveDevice(RemoteDevicePtr dev) { return dev->mTobeRemoved == true;}

// -- If a device is lastly seen longer than a threshold, then remove it from the list
void WiFi_Shopper_Tracker::clearUpDeviceList() {
	if(DEBUG_FUNCTION_CALL)
		printf("clearUpDeviceList() is called. \n");

	int foundDeviceNum_pre = mFoundDevice.size();

	for(deque<RemoteDevicePtr>::iterator it = mFoundDevice.begin(); it != mFoundDevice.end();)
	{
		RemoteDevicePtr dev = *it;
		bool needToRemove = false;

		if(dev == NULL) {
			needToRemove = true;
		} else if(dev->mTobeRemoved == true) {
			needToRemove = true;
		} else {

			if(dev->GetTrackedPosBufSize() > 1 && dev->GetTotalLocalizationDuringShopping() > MAX(5, dev->GetTrackedPosBufSize()*0.2)) {

				if(dev->mKalmanPos_m.GetTimeElapsedFromLastUpdate() > mTimeThresholdForDevAbsency*60) {

					// -- If this was a valid shopper and worth reported, then report all of the trajectory before removing this device
					if(dev->mKalmanPos_m.GetTimeElapsedFromInitialUpdateToLastUpdate() > mShoppingTime_min*60  /* Unit: [min] */
							&& dev->mKalmanPos_m.GetTimeElapsedFromInitialUpdateToLastUpdate() < mShoppingTime_max*60  /* Unit: [second] */
							&& dev->GetTotalShoppingTravelDist() > mShoppingDistance_min*100 /* Unit: [meter] */
							&& dev->GetTrackedPosAge() > mShopperDetection_min
							&& dev->GetDeviceType() != OmniSensr
                            && mAppID.compare(APP_ID_WIFI_TRACKER) == 0
							) {

						// -- Compose a trajectory message and send it to the cloud

						//SendCompletedTrajectory_summarized(dev);

						if((int)dev->GetTrackedPosBufSize() >= mTrackedPointsSizeToReport_min)
							while((int)dev->GetTrackedPosBufSize() > 0)
								SendCompletedTrajectory(dev);

					}


					if(mDebugDeviceStatusVerboseMode > 1)
						printf("REMOVE [%s] since it is disappeared longer than a threshold\n", dev->GetDevAddr().c_str());

//					dev->mTobeRemoved = true;
					needToRemove = true;
				}
			} else if (//dev->mKalmanPos_m.GetTimeElapsedFromLastUpdate() > mTimeThresholdForDevAbsency*60*0.5
						dev->mKalmanPos_m.GetTimeElapsedFromInitialCreation() > mTimeThresholdForDevAbsency*60*0.5
						&& dev->GetDeviceType() != OmniSensr
					) {
				if(mDebugDeviceStatusVerboseMode > 1)
					printf("REMOVE [%s] since it is disappeared longer than a threshold\n", dev->GetDevAddr().c_str());

//				dev->mTobeRemoved = true;
				needToRemove = true;
			}
		}

		if(needToRemove == true) {
			it = mFoundDevice.erase(it);
		} else {
			++it;
		}
	}

//	printf("Before REMOVE_IF: mFoundDevice.size() = %d\n", (int)mFoundDevice.size());

	// -- Remove the devices from the list
	//mFoundDevice.remove_if(RuleToRemoveDevice);
//	mFoundDevice.remove_if([](RemoteDevicePtr dev) { return dev->mTobeRemoved == true;});

//	printf("After REMOVE_IF: mFoundDevice.size() = %d\n", (int)mFoundDevice.size());

	int foundDeviceNum_post = mFoundDevice.size();

	if(mDebugDeviceStatusVerboseMode > 1)
		if(foundDeviceNum_post != foundDeviceNum_pre)
			printf("Erased [%d] devices from the list: From %d to %d\n", foundDeviceNum_pre - foundDeviceNum_post, foundDeviceNum_pre, foundDeviceNum_post);

	if(DEBUG_FUNCTION_CALL)
		printf("clearUpDeviceList() is ended. \n");
}

void WiFi_Shopper_Tracker::loadDevicesOfInterest() {

	FILE *fp_WiFi_Sensors;
	ostringstream filename;
	filename << APP_CONFIG_ROOT_PATH << "/" << WiFiTracker_APP_NAME << "/" << DEVICES_OF_INTEREST_LIST_FILENAME;

	fp_WiFi_Sensors = fopen(filename.str().c_str(), "r");
	if(fp_WiFi_Sensors == NULL) fprintf(stderr, "Error opening DEVICES_OF_INTEREST_LIST_FILENAME file\n");
	else {
		Node_info_t src;

	    char buffer[MAX_BUFFER];

		// -- Read from remote broker and publish to local broker
	    string lineInput;
	    vector<string> measBuffer;
		while(fgets(buffer, MAX_BUFFER, fp_WiFi_Sensors)) {	// -- Reads a line from the pipe and save it to buffer
			lineInput.clear();
			lineInput.append(buffer);

			// -- If there's any comment, then discard the rest of the line
			std::size_t found = lineInput.find_first_of("#");
			// -- If '#' is found, then discard the rest.
			if(found != string::npos)
				lineInput.resize(found);


			// -- Break the line input into words and save it into a word vector
			istringstream iss (lineInput);
			vector<string> v;
			string s;
			while(iss >> s)
				v.push_back(s);

			// -- If the size of word vector is greater than one, then it should be the first line of message containing {topic, #SN, IP}
			if(v.size() == 1 && v[0].size() > 0) {

				src.addr = v[0];
//				src.osType = OS_ETC; 	// -- Don't know the OS type

				// -- erase ':' from the address field if there is
				if(src.addr.size() > 0)
					src.addr.erase(std::remove(src.addr.begin(), src.addr.end(), ':'), src.addr.end());

				printf("Devices of interest [%d] = %s, with OS Type = DON'T KNOW \n", (int)mDevicesOfInterest.size(), src.addr.c_str());

				mDevicesOfInterest.push_back(src);
			}

			else if(v.size() > 1 && v[0].size() > 0 && v[1].size() > 0) {

				src.addr = v[0];

//				if(v[1].compare("Android") == 0) {
//					src.osType = ANDROID;
//					printf("Devices of interest [%d] = %s, with OS Type = ANDROID \n", (int)mDevicesOfInterest.size(), src.addr.c_str());
//				} else if(v[1].compare("iOS") == 0) {
//					src.osType = IOS;
//					printf("Devices of interest [%d] = %s, with OS Type = IOS \n", (int)mDevicesOfInterest.size(), src.addr.c_str());
//				} else {
//					src.osType = OS_ETC;
//					printf("Devices of interest [%d] = %s, with OS Type = DON'T KNOW \n", (int)mDevicesOfInterest.size(), src.addr.c_str());
//				}

				// -- erase ':' from the address field if there is
				if(src.addr.size() > 0)
					src.addr.erase(std::remove(src.addr.begin(), src.addr.end(), ':'), src.addr.end());

				printf("Devices of interest [%d] = %s\n", (int)mDevicesOfInterest.size(), src.addr.c_str());

				mDevicesOfInterest.push_back(src);
			}
		}


/*
		while(fscanf(fp_WiFi_Sensors, "%s", MAC_addr) != EOF) {

			// -- Skip comment line
			if(MAC_addr[0] == '#') {
				char c;
				do {
					c = fgetc(fp_WiFi_Sensors);
				} while(c != '\n' && c != EOF);
				if(c == EOF) break;
				continue;
			}

			src.addr = MAC_addr;

			// -- erase ':' from the address field if there is
			src.addr.erase(std::remove(src.addr.begin(), src.addr.end(), ':'), src.addr.end());

			printf("Devices of interest [%d] = %s \n", (int)mDevicesOfInterest.size(), src.addr.c_str());

			mDevicesOfInterest.push_back(src);
		}
		*/
	}
}

void WiFi_Shopper_Tracker::loadKnownAPs_n_ServiceAgents() {

	FILE *fp_APs, *fp_Agents;
	//int channel;

	fp_APs = fopen(mKnownApListFilePath.str().c_str(), "a+");

	if(fp_APs == NULL) fprintf(stderr, "Error opening %s file\n", mKnownApListFilePath.str().c_str());
	else {
		Node_info_t src;
		char MAC_addr[99] = {'\0'};
		while(fscanf(fp_APs, "%s", MAC_addr) != EOF) {
			// -- Skip comment line
			if(MAC_addr[0] == '#') {
				char c;
				do {
					c = fgetc(fp_APs);
				} while(c != '\n' && c != EOF);
				if(c == EOF) break;
				continue;
			}

			src.addr = MAC_addr;
			//src.channel = channel;

			// -- erase ':' from the address field if there is
			if(src.addr.size() > 0)
				src.addr.erase(std::remove(src.addr.begin(), src.addr.end(), ':'), src.addr.end());

			mKnownAPs.push_back(src);
			mTrackerState.noKnownAPs = mKnownAPs.size();
		}
	}

	fp_Agents = fopen(mKnownServiceAgentFilePath.str().c_str(), "a+");

	if(fp_Agents == NULL) fprintf(stderr, "Error opening %s file\n", mKnownServiceAgentFilePath.str().c_str());
	else {
		Node_info_t src;
		char MAC_addr[99] = {'\0'};
		while(fscanf(fp_Agents, "%s", MAC_addr) != EOF) {
			// -- Skip comment line
			if(MAC_addr[0] == '#') {
				char c;
				do {
					c = fgetc(fp_APs);
				} while(c != '\n' && c != EOF);
				if(c == EOF) break;
				continue;
			}

			src.addr = MAC_addr;
			src.channel = 0;

			// -- erase ':' from the address field if there is
			if(src.addr.size() > 0)
				src.addr.erase(std::remove(src.addr.begin(), src.addr.end(), ':'), src.addr.end());

			mKnownServiceAgents.push_back(src);
			mTrackerState.noKnownServiceAgents = mKnownServiceAgents.size();
		}
	}
}

// -- Load OUI look-up table to find a device manufacturer by checking the pre-fix of the MAC address
void WiFi_Shopper_Tracker::loadOUIlookuptable() {
	FILE *fp;

	ostringstream filename;
	filename << APP_CONFIG_ROOT_PATH << "/" << WiFiTracker_APP_NAME << "/" << OUI_LIST_FILENAME;
	fp = fopen(filename.str().c_str(), "a+");

	if(fp == NULL) fprintf(stderr, "Error opening OUI file\n");
	else {
		int c;
		do {
				OUI_Lookup_t oui;
				char prefix[99] = {'\0'};
				char manuf[99] = {'\0'};
				fscanf(fp,"%s", prefix);

				if(prefix[0] == '#') { // -- Skip comment line
					do {
						c = fgetc(fp);
					} while(c != '\n' && c != EOF);
				} else {

					fscanf(fp,"%s", manuf);

					oui.addr_prefix = prefix;

					// -- erase ':' from the address field
					if(oui.addr_prefix.size() > 0) {
						oui.addr_prefix.erase(std::remove(oui.addr_prefix.begin(), oui.addr_prefix.end(), ':'), oui.addr_prefix.end());
						oui.addr_prefix.erase(std::remove(oui.addr_prefix.begin(), oui.addr_prefix.end(), '-'), oui.addr_prefix.end());
					}

					// -- Make all to the lower case
					std::transform(oui.addr_prefix.begin(), oui.addr_prefix.end(), oui.addr_prefix.begin(), ::tolower);

					oui.manuf = manuf;

					mOUI_Lookup.push_back(oui);

					// -- Skip the remaining line
					do {
						c = fgetc(fp);
					} while(c != '\n' && c != EOF);
				}
		} while (c != EOF);

		 fclose(fp);

		// -- Debug
		printf("# of OUI list read = %d\n", (uint)mOUI_Lookup.size());

//		for(unsigned int i=0; i<mOUI_Lookup.size(); i++) {
//			// -- Debug
//			printf("%s [%d] -- %s \n", mOUI_Lookup.at(i).addr_prefix.c_str(), mOUI_Lookup.at(i).addr_prefix.size(), mOUI_Lookup.at(i).manuf.c_str());
//		}

	}
}

#ifdef USE_WIFI_PLAYER
void WiFi_Shopper_Tracker::loadRadioFingerprints() {
	FILE *fp;

	ostringstream filename;
	filename << "CalibData/" << START_DATE << "/" << APP_CONFIG_ROOT_PATH << "/" << WiFiTracker_APP_NAME << "/" << RADIO_MAP_FILE_NAME;
	fp = fopen(filename.str().c_str(), "a+");

	fingerprint_t f;

	if(fp == NULL) fprintf(stderr, "Error opening Radio Map file: %s\n", filename.str().c_str());
	else {

		int c;
		do {

			char word[99] = {'\0'};

			c = fscanf(fp, "%s", word);

			if(c == EOF) break;
			// -- Skip comment line
			// -- TODO: Maybe improved by using C++ type approach as in http://forums.devarticles.com/c-c-help-52/ignoring-commented-lines-when-using-fscanf-382249.html
			if(word[0] == '/') {
				do {
					c = fgetc(fp);
				} while(c != '\n');
				if(c == EOF) break;

			} else if(word[0] == '#') {

				if(f.sensors.size() > 0) {
					mRadioMap.push_back(f);
					f.sensors.clear();
//					printf("\n");
				}

				c = fscanf(fp, "%f %f %d %d", &f.x, &f.y, &f.time_v_start, &f.time_v_end);	// -- Scan physical location

//				// -- Convert unit from meter to centimeter
//				f.x *= 100;
//				f.y *= 100;

				// -- Convert centimeter to pixel
//				float scale = mStoreWidth_IMG_y / (float)mStoreWidth_PHY_y;
//				f.x *= scale;
//				f.y *= scale;

//				printf("Location at (%.2f, %.2f) \n", f.x, f.y);

			} else {
				calibOmniSensr_t os;
				os.addr.append(word);
				c = fscanf(fp, "%f %f", &os.mu, &os.std);

//				printf("[OmniSensr: %s] (mu, std) = (%.2f, %.2f)",os.addr.c_str(), os.mu, os.std);
//				printf("\n");

				f.sensors.push_back(os);
			}

		} while (c != EOF);

		// -- Push back the last element which is not stored yet
		if(f.sensors.size() > 0) {
			mRadioMap.push_back(f);
			f.sensors.clear();
		}

		fclose(fp);

		// -- Debug
		// -- Print the generated Radio Map in Statistics
		if(1) {
			printf("// -- Store[%s]'s Radio Map (in Statistics): -- <OmniSensor_ID> <mean> <std>\n", mIoTGateway.GetMyStoreName().c_str());
			for(list<fingerprint_t>::iterator it = mRadioMap.begin(); it != mRadioMap.end(); ++it) {
				fingerprint_t fp = *it;
				printf("#Location_(x,y): %.2f %.2f %d %d\n", fp.x, fp.y, fp.time_v_start, fp.time_v_start);

				for(list<calibOmniSensr_t>::iterator it = fp.sensors.begin(); it != fp.sensors.end(); ++it) {
					calibOmniSensr_t os = *it;
					printf("%s %.2f %.2f",os.addr.c_str(), os.mu, os.std);
					printf("\n");
				}
				printf("\n");
			}
		}
	}
}
#endif

string WiFi_Shopper_Tracker::findManufacturer(string addr) {
	if(DEBUG_FUNCTION_CALL) printf("findManufacturer() is called. \n");

	for(deque<OUI_Lookup_t>::iterator it = mOUI_Lookup.begin(); it != mOUI_Lookup.end(); ++it) {
		OUI_Lookup_t *OUI = &*it;
		if(addr.compare(0,6, OUI->addr_prefix) == 0) {
				//printf("%s is manufactured by [%s] [%s]\n", addr.c_str(), (*manuf).c_str(), mOUI_Lookup.at(i).manuf.c_str());

				return OUI->manuf;
				break;
			}
	}

	return "NULL";

	if(DEBUG_FUNCTION_CALL) printf("findManufacturer End.\n");
}

// -- Return true if it is already known
void WiFi_Shopper_Tracker::add_knownDeviceType(tcpdump_t *meas) {
	if(DEBUG_FUNCTION_CALL) printf("add_knownDeviceType() is called. \n");
	if(meas == NULL) return;

	// -- Check if it is from a new device
	if(meas->deviceType == AP) {

		// -- Add the new MAC addr to the vector database
		Node_info_t src;
		src.addr = meas->src;

		if((int)mKnownAPs.size() > mApListSize_max) {
			fprintf(stderr, "MAX_AP_LIST_SIZE reached: mKnownAPs.size = %d\n", (int)mKnownAPs.size());
			return;
		}

		pthread_mutex_lock(&gMutex);
		FILE *fp_APs;

		if(mKnownAPs.size() == 0 || mNeedToResetList_ap == true) {
			mKnownAPs.clear();

			// -- Create a new list file
			fp_APs = fopen(mKnownApListFilePath.str().c_str(), "w+");
			mNeedToResetList_ap = false;
		} else
			fp_APs = fopen(mKnownApListFilePath.str().c_str(), "a+");

		if(fp_APs == NULL) fprintf(stderr, "Error opening %s file\n", mKnownApListFilePath.str().c_str());
		else {
			fprintf(fp_APs, "%s\n", meas->src.c_str());
			 fclose(fp_APs);
		}

		mKnownAPs.push_back(src);
		mTrackerState.noKnownAPs = mKnownAPs.size();

//			printf("###### # of known APs = %d, Found a new AP: %s #############\n", mKnownAPs.size(), meas->src.c_str());

		pthread_mutex_unlock(&gMutex);
	}
	else if(meas->deviceType == ServiceAgent) {

		// -- Add the new MAC addr to the vector database
		Node_info_t src;
		src.addr = meas->src;

		if((int)mKnownServiceAgents.size() > mServiceAgentListSize_max) {
			fprintf(stderr, "MAX_SERVICE_AGENT_LIST_SIZE reached: mKnownServiceAgents.size = %d\n", (int)mKnownServiceAgents.size());
			return;
		}

		pthread_mutex_lock(&gMutex);
		FILE *fp_ServiceAgent;

		if(mKnownServiceAgents.size() == 0 || mNeedToResetList_serviceAgent == true) {
			mKnownServiceAgents.clear();

			// -- Create a new list file
			fp_ServiceAgent = fopen(mKnownServiceAgentFilePath.str().c_str(), "w+");
			mNeedToResetList_serviceAgent = false;
		} else
			fp_ServiceAgent = fopen(mKnownServiceAgentFilePath.str().c_str(), "a+");

		if(fp_ServiceAgent == NULL) fprintf(stderr, "Error opening %s file\n", mKnownServiceAgentFilePath.str().c_str());
		else {
			fprintf(fp_ServiceAgent, "%s\n", meas->src.c_str());
			 fclose(fp_ServiceAgent);
		}

		mKnownServiceAgents.push_back(src);
		mTrackerState.noKnownServiceAgents = mKnownServiceAgents.size();

//			printf("###### # of known Stations = %d, Found a new Station: %s ###########\n", mKnownStations.size(), meas->src.c_str());

		pthread_mutex_unlock(&gMutex);
	}

	if(DEBUG_FUNCTION_CALL) printf("add_knownDeviceType End. 3\n");
	return;
}


// -- Add the measurement to a database
// -- Create a log file per found MAC address, and store (timestamp, rss)
void WiFi_Shopper_Tracker::addCollectedMeasurementToDeviceBuffer(tcpdump_t *meas) {		// -- called in PacketAnalyzerThread_TCPIP
	if(DEBUG_FUNCTION_CALL) printf("addCollectedMeasurementToDeviceBuffer() is called. \n");
	if(meas == NULL) return;

	// -- If the measurement is for a new device, then allocate resources for the new device and store it to its device buffer
	if(meas->rss >= -100)
	{	// -- Discard too distant object
		RemoteDevicePtr device = findDevice(meas->src);

		if(device == NULL) {

				// -- If device is still NULL, then we must create a brand-new entry for this src. Otherwise, we reuse the entry.

				// -- Don't add any more device to the list if the track device list is already full.
				if((int)mFoundDevice.size() >= mFoundDevices_max) {
					if(DEBUG_FUNCTION_CALL) printf("addCollectedMeasurementToDeviceBuffer() is end at 4. \n");
					return;
				} else {

					device = make_shared<RemoteDevice>();

					// -- Initialize and store the measurement
					if(initializeRemoteDevice_distributed(device, meas) == true) {
						mFoundDevice.push_back(device);

	//					if(mDebugDeviceStatusVerboseMode > 1)
	//						printf("ADD a new device [%s] -- %dth device\n", meas->src.c_str(), (int)mFoundDevice.size());
					} else {
						device = NULL;
						//device.reset();
						//delete device;

//					if(mDebugDeviceStatusVerboseMode > 1)
//						printf("Attempted to add a new device [%s], but failed. -- %dth device\n", meas->src.c_str(), (int)mFoundDevice.size());
					}

				}

				// -- Create a new log file
				if(mCreateMeasLog) {
					ostringstream filename;
					filename << APP_CONFIG_ROOT_PATH << "/" << WiFiTracker_APP_NAME << "/" << APP_STORE_ROOT_PATH << "/" << mAppStoreName << "/" << "measurements/" << meas->src << ".txt";
					FILE *fp;

					fp = fopen(filename.str().c_str(), "w");
					if(fp == NULL) fprintf(stderr, "Error opening a NEW measurements file");
					else {
						fprintf(fp, "%.3lf RSS %d Ch %2d\n", gTimeSinceStart(), meas->rss, meas->channel);
						 fclose(fp);
					}
				}

		}

		if(device != NULL)
		{

			// -- Add the measurement to the device buffer
			pthread_mutex_lock(&gMutex);
			if(device->PutToMeasBuf(*meas, mMeasBufferPerDev_max) == false) {
				pthread_mutex_unlock(&gMutex);
				if(DEBUG_FUNCTION_CALL) printf("addCollectedMeasurementToDeviceBuffer() is end at 5. \n");
				return;
			}
			pthread_mutex_unlock(&gMutex);
		}

	}


	if(DEBUG_FUNCTION_CALL) printf("addCollectedMeasurementToDeviceBuffer() End.\n");
}

bool WiFi_Shopper_Tracker::initializeRemoteDevice_distributed(RemoteDevicePtr src, tcpdump_t *meas) {
	if(DEBUG_FUNCTION_CALL) printf("initializeRemoteDevice_distributed() is called. \n");

	if(meas == NULL || src == NULL) return false;

	src->DevInitialize();

	// -- In case that the newly found device is an AP or a Service Agent, then mark it
	Node_info_t *anAP = findAP(meas->src);
	if(anAP != NULL) {

		src->SetDeviceType(AP);

	} else if(meas->deviceType == AP) {

		add_knownDeviceType(meas);
	}

	Node_info_t *anAgent = findServiceAgent(meas->src);
	if(anAgent != NULL) {

		src->SetDeviceType(ServiceAgent);

	} else if(meas->deviceType == ServiceAgent) {

		add_knownDeviceType(meas);
	}

	if(meas->deviceType != 0) {
		src->SetDeviceType(meas->deviceType);
	}

	if(!mTrackAP && src->GetDeviceType() == AP)
		return false;

	if(!mTrackServiceAgent && src->GetDeviceType() == ServiceAgent)
		return false;


//		if(meas->deviceType == OmniSensr) {
//			printf("Received a packet from OmniSensr[%s] w/ rss = %d, src->GetDeviceType() = %d\n", meas->src.c_str(), meas->rss, (int)src->GetDeviceType());
//		}

	if(findRunningWiFiSensor(meas->src) != NULL)
		src->SetDeviceType(OmniSensr);

	pthread_mutex_lock(&gMutex);
	src->SetDevAddr(meas->src);

	src->mSiteInitialTime = gCurrentTime() - gTimeSinceStart();
	src->time_prevBufferProcessed = gTimeSinceStart();

	time(&src->mTime_stat.time_prev);

	// -- Set default WiFiCalibRssOffset
	src->mPerDevRssCalibOffset = mWifiCalibRssOffset;

	// -- Store the measurement
	src->PutToMeasBuf(*meas, mMeasBufferPerDev_max);

	src->ClearKalmanSignalList();

	// -- Initialize Kalman filter for location estimation
	src->mKalmanPos_m.DeepInitialize(mKalmanPosProcessNoise_var, mKalmanPosMeasurementNoise_var, mKalmanTimeScale_pos, mKalmanPosCovCoeff_pos_min, mKalmanPosCovCoeff_accel_min, 1000, 1000, kalman_position_covariance_11, kalman_position_covariance_33);

	for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it)
	{
		WiFiSensor_short_Ptr ap = *it;

		KalmanFilter_4S kalman;

		// -- Initialize Kalman filter for distance estimation
		kalman.DeepInitialize(mKalmanRssProcessNoise_var, mKalmanRssMeasurementNoise_var, mKalmanTimeScale_rss, mKalmanRssCovCoeff_pos_min, mKalmanRssCovCoeff_accel_min, kalman_signal_covariance_11, kalman_signal_covariance_33);

		kalman.mCenterOfSensingField_X = ap->mSensorPos_x;
		kalman.mCenterOfSensingField_Y = ap->mSensorPos_y;

		src->AddKalmanSignal(kalman);
	}

	pthread_mutex_unlock(&gMutex);

	// -- Find the manufacturer of the device by looking up the OUI table
	src->SetDevManuf(findManufacturer(src->GetDevAddr()));

	//src->SetDevOfInterest(isDevicesOfInterest(src->GetDevAddr()));

	if(!mTrackNullDev && src->IsDeviceBrandNull() == true)
		return false;


	if(mDebugVerboseLevel >= 2) {
			printf("====== # of found Devices = %d, Found a new Node device: %s ========== (%s) \n", (int)mFoundDevice.size(), meas->src.c_str(), src->GetDevAddr().c_str());
	}

	if(DEBUG_FUNCTION_CALL) printf("initializeRemoteDevice_distributed() is ended. \n");
	return true;
}

// -- http://stackoverflow.com/questions/2946327/inner-angle-between-two-lines
//bool WiFi_Shopper_Tracker::checkCollinearity(uint *rind, const int N_AP, RemoteDevicePtr dev) {
//	//if(DEBUG_FUNCTION_CALL) printf("checkCollinearity() is called. \n");
//
//
//// -- If any three AP group is non-collinear, then it is fine.
//	bool isCollinear = false;
//
//	Vector2D<double> p[N_AP*2];
//
//	int i=0;
//
//	// -- Store
//	for(; i<N_AP; i++)
//		p[i].set(dev->mKalmanAP_Signals[rind[i]].mCenterOfSensingField_X, dev->mKalmanAP_Signals[rind[i]].mCenterOfSensingField_Y);
//	for(i=N_AP; i<N_AP*2; i++)
//			p[i].set(dev->mKalmanAP_Signals[rind[i-N_AP]].mCenterOfSensingField_X, dev->mKalmanAP_Signals[rind[i-N_AP]].mCenterOfSensingField_Y);
//
//	// -- Compute angle and see if they are collinear
//	for(i=0; i<N_AP*2-2; i++) {
//		double rad = Vector2D<double>::angle(p[i+1]-p[i], p[i+2]-p[i]);
//		double deg = rad * 180.0 / M_PI;
//
//		deg = min(deg, 180 - deg);
//
//		//printf("AP Collinearity Angle = %d degree\n", (int)deg);
//
//		if(deg > AP_COLLINEARITY_ANGLE_THRESHOLD) {
//			isCollinear = true;
//			break;
//		}
//	}
//
//	//if(DEBUG_FUNCTION_CALL) printf("checkCollinearity() is ended. \n");
//
//	return isCollinear;
//}

// -- Determine the color of the parameter bar
// -- Red: high, Blue: low, Black: empty.
void WiFi_Shopper_Tracker::ParameterLevelColoring(float *r, float *g, float *b, float value, float max_value, float min_value)
{
	float c = (max_value - min_value)/3.0f;
	float A = c + min_value;
	float B = 2.0f*c + min_value;

	if(value >= max_value) {
		*b = 1.0f; *g = 0.0f; *r = 0.0f;
	} else if(value > B) {
		*b = 1.0f; *g = 1.0f - (value - B)/c; *r = 0;
	} else if(value <= B && value > A) {
		*b = (value - A)/c; *g = 1.0f; *r = 1.0f - (value - A)/c;
	} else if(value <= A && value > min_value) {
		*b = 0.0f; *g = (value - min_value)/c; *r = 1.0f;
	} else if(value <= min_value) {
		*b = 0.0f; *g = 0.0f; *r = 1.0f;
	} else if(value <= -9999) {
		*b = 0.0f; *g = 0.0f; *r = 0.0f;
	}

	*r *= 255.0f;
	*g *= 255.0f;
	*b *= 255.0f;

}
// -- Record error distance compared to ground truth
#ifdef USE_WIFI_PLAYER
void WiFi_Shopper_Tracker::DistErrorCollect(RemoteDevicePtr dev, int x, int y) {

	// -- If there's no ground truth (means that it may be a transition time), then discard this process
	if(GroundTruth_pos.x == 0 && GroundTruth_pos.y == 0) return;

	float distError = sqrt(pow(x - GroundTruth_pos.x,2) + pow(y - GroundTruth_pos.y,2));

	// -- See if this ground truth location exists
	for(deque<AccuracyEval_Point_t>::iterator it = dev->mAccuEval.begin(); it != dev->mAccuEval.end(); ++it) {
		AccuracyEval_Point_t *pt = &*it;
		if(pt->x == GroundTruth_pos.x && pt->y == GroundTruth_pos.y) {
			pt->distErr.push_back(distError);
			return;
		}
	}

	// -- If this is new, then create and push back
	AccuracyEval_Point_t pt;
	pt.x = GroundTruth_pos.x;
	pt.y = GroundTruth_pos.y;
	pt.mu = 0;
	pt.std = 0;
	pt.distErr.push_back(distError);

	dev->mAccuEval.push_back(pt);
}

void WiFi_Shopper_Tracker::DistErrorStatistics(RemoteDevicePtr dev) {
	for(deque<AccuracyEval_Point_t>::iterator it = dev->mAccuEval.begin(); it != dev->mAccuEval.end(); ++it) {
			AccuracyEval_Point_t *pt = &*it;

			if(pt->distErr.size() < 10) continue;

			// -- Compute the mean
			float distErr_avg = 0;
			for(deque<float>::iterator iit = pt->distErr.begin(); iit != pt->distErr.end(); ++iit)
				distErr_avg += *iit;
			distErr_avg /= (float)pt->distErr.size();
			pt->mu = distErr_avg;

			// -- Compute the variance and standard deviation
			float distErr_var = 0;
			for(deque<float>::iterator iit = pt->distErr.begin(); iit != pt->distErr.end(); ++iit)
				distErr_var += pow(*iit - distErr_avg, 2);
			distErr_var /= (float)pt->distErr.size();

			pt->std = sqrt((double)distErr_var);
	}

}

void WiFi_Shopper_Tracker::DistErrorStatisticsVisualize(RemoteDevicePtr dev) {
	ostringstream storeFilename;
	storeFilename << APP_CONFIG_ROOT_PATH << "/" << WiFiTracker_APP_NAME << "/" << APP_STORE_ROOT_PATH << "/" << mAppStoreName << "/" << mStoreLayoutFilename;
	Mat img_src = imread(storeFilename.str());
	resize(img_src, img_src, cvSize((int)ceil(mStoreWidth_IMG_x), (int)ceil(mStoreWidth_IMG_y)));

	float scale = mStoreWidth_IMG_y / (float)mStoreWidth_PHY_y;

	char mapName[99];

	sprintf(mapName,"DistErrorStatistics_%s_[%s]", mImgMapName.c_str(), dev->GetDevAddr().c_str());
	//namedWindow(mapName, CV_WINDOW_AUTOSIZE);

	// -- Mark colored levels
	for(deque<AccuracyEval_Point_t>::iterator it = dev->mAccuEval.begin(); it != dev->mAccuEval.end(); ++it) {
		AccuracyEval_Point_t *pt = &*it;

		if(pt->mu == 0 || pt->std == 0) continue;

		int radius = MAX(20, MIN(75, pt->std * scale * 0.4));
		float r, g, b;
		ParameterLevelColoring(&r, &g, &b, -1 * pt->mu, 0, -2000);	// -- Unit: cm

		//printf("(mu, r, g, b) = (%.0f, %.0f, %.0f, %.0f)\n", os.mu, r, g, b);

        circle(img_src, Point((int)pt->x*scale, (int)pt->y*scale), radius, CV_RGB(r, g, b), -1, 16, 0);

		char str_msg[255];
		sprintf(str_msg, "(%.1f, %.1f)[m]", pt->mu / 100.0f, pt->std / 100.0f);	// -- Unit: m
        putText(img_src, str_msg, Point((int)pt->x*scale - 35, (int)pt->y*scale + 4), 0, 0.4*mFontSizeScale, CV_RGB(0,0,155), 1, CV_AA);
	}

	// -- Show sensor locations
    if(mShowSensors)
        if(dev->GetKalmanSignalListSize() > 0)
            for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it)
            {
                WiFiSensor_short_Ptr ap = *it;
                show_sensors(ap, img_src, mStoreWidth_PHY_x, mStoreWidth_PHY_y, mFontSizeScale);
            }

	// -- Write image
	{
		char name[99];

		if(isAndroid(dev->GetDevAddr().c_str()))
			sprintf(name, "runtimeMetadata/Images/Android_%s.png", mapName);
		else if(isIOS(dev->GetDevAddr().c_str()))
			sprintf(name, "runtimeMetadata/Images/iOS_%s.png", mapName);
		else
			sprintf(name, "runtimeMetadata/Images/Etc_%s.png", mapName);

		imwrite(name, img_src);

		printf("Performance Analysis: Wrote image [%s]\n", mapName);
	}
}

#endif

// -- Visualize the converted distance based on RSS measurements
void WiFi_Shopper_Tracker::visualizeRSSestimation() {		// -- called in PeriodicBufferProcessingThread
	if(DEBUG_FUNCTION_CALL) printf("visualizeRSSestimation() is called. \n");

		//float scale = mStoreWidth_IMG_y / (float)mStoreWidth_PHY_y;
		//double currTime = gTimeSinceStart();
		double currTime = gCurrentTime();


#ifdef USE_WIFI_PLAYER
		// -- Show the ground truth locations of the device
		GroundTruth_pos.x = 0;		// -- MUST be zero
		GroundTruth_pos.y = 0;		// -- MUST be zero

		int ind = 0;
		static int ind_prev = 0;
		list<fingerprint_t>::iterator it = mRadioMap.begin();
		for(; it != mRadioMap.end(); ++it) {
			fingerprint_t fp = *it;
			if((double)fp.time_v_start <= mSimulationTime && mSimulationTime <= (double)fp.time_v_end )
			//if(mSimulationTime <= (double)fp.time_v_end)
			{
				GroundTruth_pos.x = fp.x;
				GroundTruth_pos.y = fp.y;

                circle(mImg_map, Point((int)fp.x*scale, (int)fp.y*scale), 17 /*radius*/, CV_RGB(255, 0, 0), -1, 16, 0);
				char msg[99];
				sprintf(msg, "%d", ind);
				string str_msg(msg);
                putText(mImg_map, str_msg, Point((int)fp.x*scale - 10, (int)fp.y*scale + 5), 0, 0.5*mFontSizeScale, CV_RGB(0,0,155), 1, CV_AA);

				if(ind > ind_prev) {

					printf("ind, ind_prev = (%d, %d)\n", ind, ind_prev);

					ind_prev = ind;

					if(mFoundDevice.size() > 0)
					for(deque<RemoteDevicePtr>::iterator it = mFoundDevice.begin(); it != mFoundDevice.end(); ++it)
					{
						RemoteDevicePtr dev = *it;

						DistErrorStatistics(dev);

						// -- Visualize the error statistics
						DistErrorStatisticsVisualize(dev);
					}
				}

				break;
			}
			ind++;
		}

		// -- If this is the end of the location, then compute the distance error statistics
		if(mSimulationTime > (double)mRadioMap.back().time_v_end) {
			if(mFoundDevice.size() > 0)
				for(deque<RemoteDevicePtr>::iterator it = mFoundDevice.begin(); it != mFoundDevice.end(); ++it)
				{
					RemoteDevicePtr dev = *it;

					DistErrorStatistics(dev);

					// -- Visualize the error statistics
					DistErrorStatisticsVisualize(dev);
				}

			printf("\n---------------------- COMPLETED -------------------- \n");
			exit(0);
		}
#endif

		// -- Show the polygon of the nodes that are used for trilateration
		if(mFoundDevice.size() > 0 && mShowLocalizationPolygon == true) {
			//RemoteDevicePtr dev = findDevice("a0000000055");		// -- TODO: Make parameter in QT-based GUI
			//RemoteDevicePtr dev = findDevice("680927abced6");		// -- TODO: Make parameter in QT-based GUI

			for(deque<RemoteDevicePtr>::iterator it = mFoundDevice.begin(); it != mFoundDevice.end(); ++it)
			{
				RemoteDevicePtr dev = *it;

				bool isDevOfInterest = isDevicesOfInterest(dev->GetDevAddr());
				bool isOmniSensr = dev->GetDeviceType() == OmniSensr;

				if(dev != NULL && isDevOfInterest == true && isOmniSensr == false) {

					// -- Mark the omnisensrs that has valid measurements
					if(dev->mMeasForTriangulation.size() > 0) {
						for(vector<pair<WiFiSensor_short_Ptr, double> >::iterator it = dev->mMeasForTriangulation.begin(); it != dev->mMeasForTriangulation.end(); ++it) {
							pair<WiFiSensor_short_Ptr, double> rss = *it;		// -- {AP pointer, distance from the signal source to the AP [Unit: cm]}
                            circle(mImg_map,Point(rss.first->mSensorPos_x_map,rss.first->mSensorPos_y_map), 11 /*radius*/,CV_RGB(100, 200, 0), -1 /*thickness, CV_FILLED*/,8,0);

						}
					}

					// -- Mark all the nodes participated in the localization
					if(dev->mChosenSensorsForTriangulation.size() > 1) {

//						// -- Show all the nodes connecting lines
//						if(0) {
//							WiFiSensor_short_Ptr os_prev = get<0>(*dev->mChosenSensorsForTriangulation.begin());
//							Point pt_curr, pt_prev = Point(os_prev->mSensorPos_x*scale, os_prev->mSensorPos_y*scale);
//							for(vector<tuple<WiFiSensor_short_Ptr, double, double> >::iterator itt = dev->mChosenSensorsForTriangulation.begin(); itt != dev->mChosenSensorsForTriangulation.end(); ++itt) {
//								WiFiSensor_short_Ptr os_curr = get<0>(*itt);
//								pt_curr = Point(os_curr->mSensorPos_x*scale, os_curr->mSensorPos_y*scale);
//								line(mImg_map, pt_prev, pt_curr, dev->GetDevColor(),1,8,0);
//								pt_prev = pt_curr;
//							}
//							os_prev = get<0>(*dev->mChosenSensorsForTriangulation.begin());
//							pt_prev = Point(os_prev->mSensorPos_x*scale, os_prev->mSensorPos_y*scale);
//							line(mImg_map, pt_curr, pt_prev, dev->GetDevColor(),1,8,0);
//						}

						for(vector<tuple<WiFiSensor_short_Ptr, double, double> >::iterator itt = dev->mChosenSensorsForTriangulation.begin(); itt != dev->mChosenSensorsForTriangulation.end(); ++itt) {
							WiFiSensor_short_Ptr os_curr = get<0>(*itt);
                            circle(mImg_map,Point(os_curr->mSensorPos_x_map,os_curr->mSensorPos_y_map), 11 /*radius*/,CV_RGB(200, 50, 50), -1 /*thickness, CV_FILLED*/,8,0);
						}


						// -- Show the convexhull only
                        Point pt_map;
						{
							pt_map = ConvertPhyCoordToMapCoord((*dev->mChosenSensorsConvexHullPts.begin()).x, (*dev->mChosenSensorsConvexHullPts.begin()).y);
                            Point2f pt_curr, pt_prev = Point2f(pt_map.x, pt_map.y);// = Point2D32f((*dev->mChosenSensorsConvexHullPts.begin()).x*scale, (*dev->mChosenSensorsConvexHullPts.begin()).y*scale);
							for(vector<Point2f>::iterator itt = dev->mChosenSensorsConvexHullPts.begin(); itt != dev->mChosenSensorsConvexHullPts.end(); ++itt) {
                                //pt_curr = Point2D32f((*itt).x*scale, (*itt).y*scale);
                                Point pt_cv = ConvertPhyCoordToMapCoord((*itt).x, (*itt).y);
								pt_curr = Point2f(pt_cv.x, pt_cv.y);
								line(mImg_map, pt_prev, pt_curr, dev->GetDevColor(),4,8,0);
								pt_prev = pt_curr;
							}
                            //pt_prev = Point2D32f((*dev->mChosenSensorsConvexHullPts.begin()).x*scale, (*dev->mChosenSensorsConvexHullPts.begin()).y*scale);
							pt_map = ConvertPhyCoordToMapCoord((*dev->mChosenSensorsConvexHullPts.begin()).x, (*dev->mChosenSensorsConvexHullPts.begin()).y);
							pt_prev = Point2f(pt_map.x, pt_map.y);
							line(mImg_map, pt_curr, pt_prev, dev->GetDevColor(),4,8,0);
						}
					}
				}
			}
		}


		if(mFoundDevice.size() > 0)
			for(deque<RemoteDevicePtr>::iterator it = mFoundDevice.begin(); it != mFoundDevice.end(); ++it)
			{
				RemoteDevicePtr dev = *it;

				if(dev == NULL)
					continue;

					if(dev->GetDeviceType() == AP) {
						if(mShowNoAPs)
							continue;
					} else if(dev->GetDeviceType() == ServiceAgent) {
						if(mShowNoServiceAgent)
							continue;
					}

					if(dev->IsDeviceBrandNull() == true) {
						if(mTrackNullDev == false)
							continue;
					}

					double state_pred[4];
					int r = 0, g = 0, b = 0;
					int thickness = 1, radius = 1;


					CvScalar dev_color = dev->GetDevColor();

					bool isDevOfInterest = isDevicesOfInterest(dev->GetDevAddr());
					bool isOmniSensr = dev->GetDeviceType() == OmniSensr;

					if(isDevOfInterest == true && isOmniSensr == false)
					{
						r = 0x2d; g = 0xc8; b = 0x00;
						radius = 2;
						thickness = 1;
					} else {
						r = 0xff; g = 0x00; b = 0x00;
						radius = 1;
						thickness = 1;
					}

					// -- In the WIFI_CALIB_VERIFICATION mode, shows only OmniSensrs' trajectory
					if(mWiFiOperationMode == WIFI_CALIB_VERIFICATION)
						if(isOmniSensr == false)
							continue;

					if(dev->GetTrackedPosAge() > 0)
					{

						// -- I thought the duration for capturing 3 measurements is a reasonable time to be waited for the stabilization of the position.
						if(mShowTrajectory)
							if(!mShowTrajectory_DevOfInt || (mShowTrajectory_DevOfInt
									&& (isDevOfInterest
											|| (mWiFiOperationMode == WIFI_CALIB_VERIFICATION && isOmniSensr)
										)
									)) {

								// -- Mark the trajectories passed in the past
                                Point pt_curr, pt_prev = Point(-999,-999);
                                Point pt_curr_sensr_center, pt_prev_sensr_center = Point(-999,-999);

								for(deque<TrackedPos_t_>::iterator it = dev->AtTrackedPosBuf()->begin(); it != dev->AtTrackedPosBuf()->end(); ++it)
								{

									if((*it).pos.x == 0 || (*it).pos.y == 0)
										continue;


//									if(isOmniSensr == true) {
//										printf("OS[%s] age = %d\n", dev->GetDevAddr().c_str(), dev->GetTrackedPosAge());
//									}

									// -- For the devices of interest, show the full trajectory. For others, show a partial trajectory

									if(((mOnlyDevOfInterest && isDevOfInterest == true && isOmniSensr == false && currTime - (*it).localtime < mKalmanEstimateVisualizationTimeout*3)
											|| (isDevOfInterest == false && currTime - (*it).localtime < mKalmanEstimateVisualizationTimeout))
										|| (isOmniSensr == true && currTime - (*it).localtime < mKalmanEstimateVisualizationTimeout)
										|| (!mOnlyDevOfInterest && currTime - (*it).localtime < mKalmanEstimateVisualizationTimeout)
										)
									{
										// -- Tracked Position
										if(pt_prev.x == -999 && pt_prev.y == -999) {
											pt_prev = ConvertPhyCoordToMapCoord((*it).pos.x, (*it).pos.y);
											circle(mImg_map, pt_prev, radius*3 /*radius*/, dev_color, thickness, 16, 0);
										} else {
											pt_curr = ConvertPhyCoordToMapCoord((*it).pos.x, (*it).pos.y);
											line(mImg_map, pt_prev, pt_curr, dev->GetDevColor(),1,8,0);
											circle(mImg_map, pt_curr, radius*3 /*radius*/, dev_color, thickness, 16, 0);
											pt_prev = pt_curr;
										}

										// -- Centroid of Selected Sensors
										if(mShowSelectedSensorCentroid)
											if(isDevOfInterest == true) {
												if(pt_prev_sensr_center.x == -999 && pt_prev_sensr_center.y == -999) {
													pt_prev_sensr_center = ConvertPhyCoordToMapCoord((*it).sensr_center.x, (*it).sensr_center.y);
													circle(mImg_map, pt_prev_sensr_center, radius*3 /*radius*/, CV_RGB(0,0,155), thickness, 16, 0);
												} else {
													pt_curr_sensr_center = ConvertPhyCoordToMapCoord((*it).sensr_center.x, (*it).sensr_center.y);
													line(mImg_map, pt_prev_sensr_center, pt_curr_sensr_center, CV_RGB(0,0,155),1,8,0);
													circle(mImg_map, pt_curr_sensr_center, radius*3 /*radius*/, CV_RGB(0,0,155), 3, 16, 0);
													pt_prev_sensr_center = pt_curr_sensr_center;
												}
											}
									}
									else {
										break;
									}
								}

								// -- Mark the current location on top of the shown trajectories (as a small red dot)
								if(currTime - dev->AtTrackedPosBuf()->begin()->localtime < mKalmanEstimateVisualizationTimeout)
								{
									if(dev->AtTrackedPosBuf()->begin()->pos.x != 0 && dev->AtTrackedPosBuf()->begin()->pos.y != 0) {
										circle(mImg_map,ConvertPhyCoordToMapCoord(dev->AtTrackedPosBuf()->begin()->pos.x, dev->AtTrackedPosBuf()->begin()->pos.y), thickness*3+1 /*radius*/, CV_RGB(r,g,b), CV_FILLED, 16,0);
										circle(mImg_map,ConvertPhyCoordToMapCoord(dev->AtTrackedPosBuf()->begin()->pos.x, dev->AtTrackedPosBuf()->begin()->pos.y), thickness*3+1 /*radius*/, CV_RGB(0,100,0), 1, 16,0);

										if(dev->GetDeviceType() == OmniSensr) {
											char text[99];
											sprintf(text, "[%s] %d", &dev->GetDevAddr().c_str()[dev->GetDevAddr().size()-4], dev->GetTrackedPosAge());
                                            Point pt = ConvertPhyCoordToMapCoord(dev->AtTrackedPosBuf()->begin()->pos.x, dev->AtTrackedPosBuf()->begin()->pos.y);
                                            putText(mImg_map, text,  Point(pt.x + 5, pt.y -3), 0, 0.4*mFontSizeScale,CV_RGB(0,125,0), 1, CV_AA);
										} else if(mOnlyDevOfInterest && isDevOfInterest == true) {
											char text[99];
											sprintf(text, "[%s] %d", dev->GetDevAddr().c_str(), dev->GetTrackedPosAge());
                                            Point pt = ConvertPhyCoordToMapCoord(dev->AtTrackedPosBuf()->begin()->pos.x, dev->AtTrackedPosBuf()->begin()->pos.y);
                                            putText(mImg_map, text, Point(pt.x + 5, pt.y -3), 0, 0.4*mFontSizeScale,CV_RGB(255,0,0), 1, CV_AA);

										} else if(mOnlyDevOfInterest && isDevOfInterest == false ) {
											char text[99];
											// -- Show the last four letters of the MAC address of the device with its tracking age
											sprintf(text, "[%s]%d", &dev->GetDevAddr().c_str()[dev->GetDevAddr().size()-4], dev->GetTrackedPosAge());
                                            Point pt = ConvertPhyCoordToMapCoord(dev->AtTrackedPosBuf()->begin()->pos.x, dev->AtTrackedPosBuf()->begin()->pos.y);
                                            putText(mImg_map, text, Point(pt.x + 3, pt.y -3), 0, 0.4*mFontSizeScale,CV_RGB(255,0,0), 1, CV_AA);
										} else if (!mOnlyDevOfInterest) {
											char text[99];
											// -- Show the last four letters of the MAC address of the device with its tracking age
											sprintf(text, "[%s]%d", &dev->GetDevAddr().c_str()[dev->GetDevAddr().size()-4], dev->GetTrackedPosAge());
                                            Point pt = ConvertPhyCoordToMapCoord(dev->AtTrackedPosBuf()->begin()->pos.x, dev->AtTrackedPosBuf()->begin()->pos.y);
                                            putText(mImg_map, text, Point(pt.x + 3, pt.y -3), 0, 0.4*mFontSizeScale,CV_RGB(255,0,0), 1, CV_AA);
										}
									}
								}


//								// -- Draw a circle for the selected-sensor-group center position
//								pt_prev = Point(-999,-999);
//								int cnt =0;
//								for(vector<Point>::iterator it = dev->mChosenSensorsForTriangulation_center.begin(); it != dev->mChosenSensorsForTriangulation_center.end() && cnt < 10; ++it, ++cnt)
//								{
//									// -- For the devices of interest, show the full trajectory. For others, show a partial trajectory
//									{
//										if(pt_prev.x == -999 && pt_prev.y == -999) {
//											pt_prev = Point((*it).x*scale, (*it).y*scale);
//											circle(mImg_map, pt_prev, 13 /*radius*/, CV_RGB(0,155,0), 7, 16, 0);
//										} else {
//											pt_curr = Point((*it).x*scale, (*it).y*scale);
//											line(mImg_map, pt_prev, pt_curr, dev->GetDevColor(),1,8,0);
//											circle(mImg_map, pt_curr, 13 /*radius*/, CV_RGB(0,155,0), 7, 16, 0);
//											pt_prev = pt_curr;
//										}
//									}
//								}


							}

						// -- Visualize the current estimated DETECTED position of the tracked devices
						if((mOnlyDevOfInterest && (isDevOfInterest))
								|| !mOnlyDevOfInterest)
						{

//							// -- Draw a circle for the selected-sensor-group center position
//							//if(mDebugVerboseLevel > 0)
//								circle(mImg_map, Point(dev->mChosenSensorsForTriangulation_center.x*scale,
//										dev->mChosenSensorsForTriangulation_center.y*scale), 13, CV_RGB(0,155,0), 7, CV_AA, 0);

//							#if ONLY_FOR_DEVICES_OF_INTEREST
//							if(isDevOfInterest == true)
//							#endif
//							if(withinLifetime && dev->mTrackedPos.size > 2) {
//
//								// -- Calculate the distance from the last tracked position and this estimate
//								float dist = sqrt(pow(estPos.x - dev->mTrackedPos.pos[dev->mTrackedPos.size-1].x,2) + pow(estPos.y - dev->mTrackedPos.pos[dev->mTrackedPos.size-1].y,2));
//
//								// -- Show only if the distance diff is less than 20 meter
//								if(dist < 2000) {
//
//									cvCircle(mImg_map,Point(estPos.x*scale, estPos.y*scale), thickness*3+1 /*radius*/, CV_RGB(r,g,b), CV_FILLED,8,0);
//									cvCircle(mImg_map,Point(estPos.x*scale, estPos.y*scale), thickness*3+1 /*radius*/, CV_RGB(0,100,0), 1, 8,0);
//
//									sprintf(str_Dev_id, "%d", dev->mKalmanPos.GetKalmanAge());
//									cvPutText(mImg_map, str_Dev_id, Point(estPos.x*scale -5, estPos.y*scale -7), &font_Dev, CV_RGB(0,0,155));
//
//									cvLine(mImg_map, Point(dev->mTrackedPos.pos[dev->mTrackedPos.size-1].x*scale, dev->mTrackedPos.pos[dev->mTrackedPos.size-1].y*scale),
//										Point(estPos.x*scale, estPos.y*scale), dev->getDevColor(),1,8,0);
//								}
//							}

							// -- Visualize the current estimated TRACKED position of the tracked devices
	//						{
    //							cvCircle(mImg_map,Point(dev->mTrackedPos.pos[dev->mTrackedPos.size-1].x*scale,
	//									dev->mTrackedPos.pos[dev->mTrackedPos.size-1].y*scale), thickness*3 /*radius*/, CV_RGB(r,g,b), CV_FILLED,8,0);
    //							cvCircle(mImg_map,Point(dev->mTrackedPos.pos[dev->mTrackedPos.size-1].x*scale,
	//									dev->mTrackedPos.pos[dev->mTrackedPos.size-1].y*scale), thickness*3 /*radius*/, CV_RGB(0,100,0), 2, 8,0);
	//
	//							sprintf(str_Dev_id, "%d", dev->mKalmanPos.GetKalmanAge());
    //							cvPutText(mImg_map, str_Dev_id, Point(dev->mTrackedPos.pos[dev->mTrackedPos.size-1].x*scale -5,
	//									dev->mTrackedPos.pos[dev->mTrackedPos.size-1].y*scale -7), &font_Dev, CV_RGB(0,0,155));
	//						}

							//printf("locationEstimationBasedOnRssEstimation is true. Dev = %s, isDevOfInterest = %d \n", dev->getAddr().c_str(), isDevOfInterest);


									// -- Visualize the current estimated TRACKED position of the tracked devices
//									{
//										dev->mKalmanPos.PredictForAdaptation();
//										dev->mKalmanPos.get_predicted_state(state_pred);
//										cvCircle(mImg_map,Point(state_pred[0]*scale, state_pred[1]*scale), thickness*3 /*radius*/, CV_RGB(0,100,0), 2, 8,0);
//									}


									// -- Visualize Distance Estimation from each AP
#if !defined(USE_RADIO_FINGERPRINT_BASED_LOCALIZATION)
									if(mShowRSSMeas)
										//for(unsigned int j=0; j<dev->GetKalmanSignalListSize(); j++) {
										for(list<KalmanFilter_4S>::iterator it = dev->mKalmanAP_Signals.begin(); it != dev->mKalmanAP_Signals.end(); ++it) {
											KalmanFilter_4S *kal = &*it;

											double tau = kal->GetTimeElapsedFromLastUpdate();

											if(kal->GetKalmanAge() > 0) {
												if(tau < mKalmanMeasurementLifetimeForTracking) {
													//kal->PredictForPrediction();
													kal->get_state(state_pred);
													{
														int thickness = 1;
                                                        Point rd = ConvertPhyCoordToMapCoord((int)state_pred[0], (int)state_pred[1]);
														int radius = (int)MAX(rd.x, 15);

														// -- See if the WiFiSensor is the one used for trilateration. If so, use bolder circle.
														WiFiSensor_short_Ptr sensr;
														bool isFound = false;
														//for(deque<WiFiSensor_short_Ptr>::iterator it = dev->mChosenSensorsForTriangulation.begin(); it != dev->mChosenSensorsForTriangulation.end(); ++it) {
														for(vector<tuple<WiFiSensor_short_Ptr, double, double> >::iterator it = dev->mChosenSensorsForTriangulation.begin(); it != dev->mChosenSensorsForTriangulation.end(); ++it) {
															sensr = get<0>(*it);

															//printf("mChosenSensorsForTriangulation = [%s]\n", sensr->mIPaddr.c_str());

															if(sensr->mSensorPos_x == kal->mCenterOfSensingField_X && sensr->mSensorPos_y == kal->mCenterOfSensingField_Y) {
																thickness = 2;
																isFound = true;
																break;
															}
														}

														if(isFound == false)
															continue;

														circle(mImg_map,ConvertPhyCoordToMapCoord(kal->mCenterOfSensingField_X, kal->mCenterOfSensingField_Y), radius /*radius*/, dev_color, thickness /*thickness, CV_FILLED*/, CV_AA, 0);
                                                        //circle(mImg_map,Point(sensr->mSensorPos_x_map, sensr->mSensorPos_y_map), radius /*radius*/, dev_color, thickness /*thickness, CV_FILLED*/, CV_AA, 0);
														{
															char str_AP_id[99];
															sprintf(str_AP_id, "%d", kal->GetKalmanAge());
                                                            putText(mImg_map, str_AP_id, Point(sensr->mSensorPos_x_map, sensr->mSensorPos_y_map - radius), 0, 0.5*mFontSizeScale,CV_RGB(0,0,0), 1, CV_AA);
														}

														if(mDebugVerboseLevel >= 2)
															printf("%.2f (%d): Node[%s] AP_pos(%d, %d) -- (dist, radius) = (%.1f, %d)\n", gTimeSinceStart(),
																	kal->GetKalmanAge(), dev->GetDevAddr().c_str(),
																	kal->mCenterOfSensingField_X, kal->mCenterOfSensingField_Y,
																	state_pred[0], radius);
													}
												}
//												else {
//														#if ONLY_FOR_DEVICES_OF_INTEREST
//														if(isDevOfInterest == true)
//														#endif
//															if(mDebugVerboseLevel > 0)
//															printf("Node[%s]: (mKalmanAP_Signals[%d].GetKalmanAge(), tau) = (%d, %.1lf) FAIL TO DRAW RSS CIRCLES!\n",
//																	dev->getAddr().c_str(), (int)j, kal->GetKalmanAge(), tau);
//												}
											}
										}

#endif

						}
					}

			}


	if(DEBUG_FUNCTION_CALL) printf("visualizeRSSestimation() End.\n");
}

bool WiFi_Shopper_Tracker::isNearbyNeighbors(WiFiSensor_short_Ptr os, WiFiSensor_short_Ptr peer) {
	if(os == NULL || peer == NULL || os->mNearbyNeighbors.size() == 0)
		return false;

	bool ret = false;
	for(deque<string>::iterator it = os->mNearbyNeighbors.begin(); it != os->mNearbyNeighbors.end(); ++it) {
		string sn = *it;
		if(boost::iequals(sn, peer->mIPaddr) == true				// -- IP Address
			|| boost::iequals(sn, peer->mSerialNumber) == true		// -- Serial Number
			|| boost::iequals(sn, peer->mDevName) == true		// -- Device Name
			) {

			ret = true;
			break;
		}
	}

	return ret;
}

void WiFi_Shopper_Tracker::MeasurementAdjustment() {
	/*
	 *  -- Measurement Pre-processing Process --
	 *
	 *  Rationale: the change in RSS value at a node must be consistent with its geographical neighbors because they should not be independent because the signal is from the same source.
	 *
	 *  Process:
	 *  1. Calculate its neighbors' average expected change in RSS value
	 *  2. Adjust its RSS value as a weighted sum of (its own RSS change from its current estimate) and (its neighbors' average RSS change from their current estimate).				 *
	 */
	for(deque<RemoteDevicePtr>::iterator it = mFoundDevice.begin(); it != mFoundDevice.end();++it)
	{
		RemoteDevicePtr dev = *it;

		if(dev == NULL)
			continue;

		if(dev->GetMeasBufSize() == 0)
			continue;

		vector<tcpdump_t> newMeasBuf;

		if(dev->GetMeasBufSize() > 0)
			for(vector<tcpdump_t>::iterator jt = dev->AtMeasBuf()->begin(); jt !=  dev->AtMeasBuf()->end(); ++jt) {
				tcpdump_t *meas = &*jt;
				if(meas == NULL) continue;

				WiFiSensor_short_Ptr ap = findRunningWiFiSensor(meas->capturedBy);

				if(ap == NULL)
					continue;

				// -- Find measurements from nearby neighbors
				deque<pair<WiFiSensor_short_Ptr,tcpdump_t> > peerMeases;
				for(vector<tcpdump_t>::iterator kt = dev->AtMeasBuf()->begin(); kt !=  dev->AtMeasBuf()->end(); ++kt) {
					tcpdump_t *measPeer = &*kt;
					if(measPeer == meas)
						continue;

					WiFiSensor_short_Ptr peer = findRunningWiFiSensor(measPeer->capturedBy);

					//printf("Dev[%s]: isNearbyNeighbors(%s, %s) = ?\n", dev->GetDevAddr().c_str(), ap->mDevName.c_str(), peer->mDevName.c_str());

					if(isNearbyNeighbors(ap, peer) == true) {
						peerMeases.push_back(make_pair(peer, *measPeer));
					}

				}

				//printf("Dev[%s]: peerMeases.size() = %d\n", dev->GetDevAddr().c_str(), (int)peerMeases.size());

				// -- Calculate the neighbors' average expected change in RSS value
				double peerCnt = 0;
						double peerExpectedDistChangeAvg = 0, peerExpectedDistChangeAbsAvg = 0;
						double peerExpectedRssChangeAvg = 0, peerExpectedRssChangeAbsAvg = 0;
				double peerVelocityAvg = 0, peerVelocityAbsAvg = 0;
				if((int)peerMeases.size() > 0) {
					//printf("\n");
					for(deque<pair<WiFiSensor_short_Ptr,tcpdump_t> >::iterator kt = peerMeases.begin(); kt != peerMeases.end(); ++kt) {
						pair<WiFiSensor_short_Ptr,tcpdump_t> peerMeas = *kt;

						KalmanFilter_4S *kal = findKalmanSignal(dev, peerMeas.first);

						if(kal == NULL || kal->GetKalmanAge() <= 3)
							continue;

						double sta[4];
						kal->get_state(sta);

						double peerVelocity = sta[2];
						peerVelocityAvg += peerVelocity;
						peerVelocityAbsAvg += fabs(peerVelocity);

								double measDist = peerMeas.first->convertRSStoDist_log(peerMeas.second.rss, peerMeas.second.channel, dev->mPerDevRssCalibOffset, dev->GetDeviceType() == OmniSensr);
								if(measDist < 0)
									continue;
								double peerExpectedDistChange = measDist - sta[0];

								int estRSS = peerMeas.first->convertDist_logToRSS(sta[0], peerMeas.second.channel, dev->mPerDevRssCalibOffset, dev->GetDeviceType() == OmniSensr);
								if(estRSS >= 0)
									continue;

								int peerExpectedRssChange = peerMeas.second.rss - estRSS;

						if(mDebugVerboseLevel > 1)
						printf("Node[%s] to OS[%s]: (rss, ch, offset)=(%d,%d,%.2f) (sta, measDist, peerExpectedDistChange = (%.4f, %.4f, %.4f) [m]\n", dev->GetDevAddr().c_str(), peerMeas.first->mDevName.c_str(),
								 peerMeas.second.rss, peerMeas.second.channel, dev->mPerDevRssCalibOffset, sta[0]/100.0, measDist/100.0, peerExpectedDistChange/100.0);

								peerExpectedDistChangeAvg += peerExpectedDistChange;
								peerExpectedDistChangeAbsAvg += fabs(peerExpectedDistChange);

								peerExpectedRssChangeAvg += peerExpectedRssChange;
								peerExpectedRssChangeAbsAvg += fabs(peerExpectedRssChange);

						peerCnt += 1;
					}

					if(peerCnt > 0) {

						peerVelocityAvg /= peerCnt;
						peerVelocityAbsAvg /= peerCnt;

								peerExpectedDistChangeAvg /= peerCnt;
								peerExpectedDistChangeAbsAvg /= peerCnt;

								peerExpectedRssChangeAvg /= peerCnt;
								peerExpectedRssChangeAbsAvg /= peerCnt;
					}
				}

				// -- Adjust its RSS value as a weighted sum
				if(peerCnt > 1) {
					tcpdump_t newMeas = *meas;

					KalmanFilter_4S *kal = findKalmanSignal(dev, ap);

					if(kal == NULL || kal->GetKalmanAge() < 3)
						continue;

					double sta[4];
					kal->get_state(sta);

					if(1) {
						double measDist = meas->distToAP;

//						if(mDebugVerboseLevel > 1)
//						printf("Node[%s] to OS[%s]: Conversion (rss, ch, offset) = (%d, %d, %.1f) => dist = %.2f [m]\n", dev->GetDevAddr().c_str(), ap->mDevName.c_str(),
//								meas->rss, meas->channel, dev->mPerDevRssCalibOffset, measDist/100.0);

						if(measDist < 0)
							continue;

						double myExpectedDistChange = measDist - sta[0];

						// -- Weighted sum
						bool isDirectionSame = myExpectedDistChange*peerExpectedDistChangeAvg > 0;
						bool isSignificantChange = (peerExpectedDistChangeAbsAvg/100.0 > 3) && fabs(peerExpectedDistChangeAvg)/100.0 > 3;

						double newDist;
						if(isDirectionSame == true) {
							if(isSignificantChange == true) {
								newDist = sta[0] + (0.4*myExpectedDistChange + 0.8*peerExpectedDistChangeAvg) + 0.5;
								//newDist = sta[0] + (0.5*myExpectedDistChange + 0.5*peerExpectedDistChangeAvg) + 0.5;
							} else {
								newDist = sta[0] + (0.6*myExpectedDistChange + 0.4*peerExpectedDistChangeAvg) + 0.5;
							}
						} else {
							newDist = sta[0] + (0.5*myExpectedDistChange) + 0.5;
						}

//									newDist = sta[0] + myExpectedDistChange;

						if(newDist <= 0)
							continue;

						newMeas.distToAP = newDist;
						newMeas.rss = ap->convertDist_logToRSS(newMeas.distToAP, newMeas.channel, dev->mPerDevRssCalibOffset, dev->GetDeviceType() == OmniSensr);

						if(newMeas.rss > 0)
							continue;

						//if(mDebugVerboseLevel > 1)
						if(isDevicesOfInterest(dev->GetDevAddr()) == true)
							printf("  Node[%s] to OS[%s]: Conversion (dist, ch, offset) = (%.2f [m], %d, %.1f) => rss = %d\n", dev->GetDevAddr().c_str(), ap->mDevName.c_str(),
									newMeas.distToAP/100.0, meas->channel, dev->mPerDevRssCalibOffset, newMeas.rss);

						if(mDebugVerboseLevel > 1)
						//if(isSignificantChange == true)
							printf("///// Node[%s] to OS[%s]: rss=%d, Dist (Est, Curr, my change, peer change) = (%.4f, %.4f, %.4f, %.4f) [m] ==> (dist, dist_adjusted) = (%.4f, %.4f) [m], (rss, rss_adjusted) = (%d, %d)\n",
										dev->GetDevAddr().c_str(), ap->mDevName.c_str(), meas->rss, sta[0]/100.0, measDist/100.0, myExpectedDistChange/100.0, peerExpectedDistChangeAvg/100.0, measDist/100.0, newDist/100.0, meas->rss, newMeas.rss);

					} else if(0) {
						int estRss = ap->convertDist_logToRSS(sta[0], meas->channel, dev->mPerDevRssCalibOffset, dev->GetDeviceType() == OmniSensr);
						int myExpectedRssChange = meas->rss - estRss;

						// -- Weighted sum
						bool isDirectionSame = myExpectedRssChange*peerExpectedRssChangeAvg > 0;
						bool isSignificantChange = fabs(peerExpectedRssChangeAbsAvg) > 5;

						int newRss;
						if(isDirectionSame == true) {
							if(isSignificantChange == true) {
								//newDist = sta[0] + (0.4*myExpectedDistChange + 1.0*peerExpectedDistChangeAvg) + 0.5;
								newRss = estRss + (0.5*myExpectedRssChange + 0.5*peerExpectedRssChangeAvg) + 0.5;
							} else {
								newRss = estRss + (0.8*myExpectedRssChange + 0.2*peerExpectedRssChangeAvg) + 0.5;
							}
						} else {
							newRss = estRss + (0.5*myExpectedRssChange) + 0.5;
						}

						if(newRss > 0)
							continue;

						newMeas.rss = newRss;
						newMeas.distToAP = ap->convertRSStoDist_log(newMeas.rss, newMeas.channel, dev->mPerDevRssCalibOffset, dev->GetDeviceType() == OmniSensr);

						if(newMeas.distToAP < 0)
							continue;
					} else {

						double myVelocity = sta[2];
						double myExpectedDistChange = meas->distToAP - sta[0];

						// -- Weighted sum
						bool isDirectionSame = myVelocity*peerVelocityAvg > 0;
						bool isSignificantChange = fabs(peerVelocityAbsAvg) > 2;

						int newDist;
						if(isDirectionSame == true) {
							if(isSignificantChange == true) {
								//newDist = sta[0] + (0.4*myExpectedDistChange + 1.0*peerExpectedDistChangeAvg) + 0.5;
								newDist = sta[0] + (0.5*myExpectedDistChange + 0.5*peerExpectedDistChangeAvg) + 0.5;
							} else {
								newDist = sta[0]  + (0.5*myExpectedDistChange + 0.5*peerExpectedDistChangeAvg) + 0.5;
							}
						} else {
							newDist = sta[0]  + (0.5*myExpectedDistChange) + 0.5;
						}

						if(newDist < 0)
							continue;

						newMeas.distToAP = newDist;
						newMeas.rss = ap->convertDist_logToRSS(newMeas.distToAP, newMeas.channel, dev->mPerDevRssCalibOffset, dev->GetDeviceType() == OmniSensr);

						if(newMeas.rss > 0)
							continue;

						if(mDebugVerboseLevel > 1)
						printf("///// Node[%s] to OS[%s]: rss=%d, Dist (Est, Curr, my change, peer change) = (%.4f, %.4f, %.4f, %.4f) [m] ==> (dist, dist_adjusted) = (%.4f, %.4f) [m], (rss, rss_adjusted) = (%d, %d)\n",
																			dev->GetDevAddr().c_str(), ap->mDevName.c_str(), meas->rss, sta[0]/100.0, meas->distToAP/100.0, myExpectedDistChange/100.0, peerExpectedDistChangeAvg/100.0, meas->distToAP/100.0, newDist/100.0, meas->rss, newMeas.rss);

					}

					newMeasBuf.push_back(newMeas);

				} else {
					newMeasBuf.push_back(*meas);
				}
			}

			*dev->AtMeasBuf() = newMeasBuf;
	}
}



void WiFi_Shopper_Tracker::PeriodicBufferProcessing_Loop() {

	printf("Analyzer: PeriodicBufferProcessing_Loop() is called\n");

	double currTime = gTimeSinceStart(), prevTime_buffer, prevTime_report, prevTime_buffer_pre, prevTime_clear, prevTime_fps, prevTime_heartbeat;
	prevTime_report = prevTime_buffer = prevTime_buffer_pre = prevTime_clear = prevTime_fps = currTime;

	IOT_Communicator iotc(APP_ID_WIFI_TRACKER);

	// -- NOTE: This will give put some drift in the time of processing for each device, and so distribute the computing load over time.
	// -- NOTE: This is very important since otherwise the CPU load peaks periodically, and may cause frequent broken pipes to each AP.


	WiFiSensor_short_Ptr ap;
	double timeElapsed;
	int cnt_fps = 0;
	int fps_calc_interval = 20;

	iotc.sendHeartbeat();
	prevTime_heartbeat = currTime;


	// -- Default is 100%
	mTrackerState.avgPeriodicProcessingRate = 100;

	while(1) {

		currTime = gTimeSinceStart();

        if(mKillRequestPending == true)
            break;

		// -- Periodic processing
		if(currTime - prevTime_buffer >= mPeriodicBufferProcessingInterval && mRunningWiFiSensor.size() > 0) {		// -- Unit: [second]

			// -- Wait until the other thread finishes its turn.
			do {
				pthread_mutex_lock(&gMutex);
				bool isOtherThreadProcessing = mIsThereSomeProcessing;
				pthread_mutex_unlock(&gMutex);

				if(isOtherThreadProcessing > 0) usleep(5000);
				else break;
			} while(true);

			pthread_mutex_lock(&gMutex);
			//mIsThereSomeProcessing |= (1<<PERIODIC_PROCESSING);
			mIsThereSomeProcessing = (1<<PERIODIC_PROCESSING);
			pthread_mutex_unlock(&gMutex);

			cnt_fps++;
			if(cnt_fps%fps_calc_interval == 0) {

				if((int)mFoundDevice.size() > 0) {
					double timeDiff = currTime - prevTime_fps;
					int expectedCntFps = (timeDiff / (double)mPeriodicBufferProcessingInterval + 0.5f);

					mTrackerState.avgPeriodicProcessingRate = (cnt_fps*100)/(double)expectedCntFps;	// -- Unit: [percentage]

					if(mTrackerState.avgPeriodicProcessingRate < 80) {
						// -- Something wrong, it is taking too long.
						if(mDebugTrackerStatus)
							fprintf(stderr, "[WiFi_Shopper_Tracker] PERIODIC_PROCESSING is taking too long: cnt_fps = %d, but expected to be %d\n", cnt_fps, expectedCntFps);
					}
				}

				prevTime_fps = currTime;
				cnt_fps = 0;
			}

			// -- Periodic processing
			if((int)mFoundDevice.size() > 0) {
				for(deque<RemoteDevicePtr>::iterator it = mFoundDevice.begin(); it != mFoundDevice.end();++it)
				{
					RemoteDevicePtr dev = *it;

					if(dev == NULL)
						continue;

					if(!mTrackServiceAgent && dev->GetDeviceType() == ServiceAgent)
						continue;

					if(!mTrackAP && dev->GetDeviceType() == AP)
						continue;

					// -- See if there's any unprocessed measurements
					if(dev->GetMeasBufSize() > 0)
					{

						periodicMeasurementProcessing_per_Station(dev);

//								if(mDebugLocalization)
//									if(dev->GetDeviceType() == OmniSensr) {
//										printf("OS[%s]  dev->GetMeasBufSize() = %d\n", dev->GetDevAddr().c_str(), (int)dev->GetMeasBufSize());
//									}

					}
				}


				if(mMeasurementAdjustmentEnable)
					MeasurementAdjustment();


				for(deque<RemoteDevicePtr>::iterator it = mFoundDevice.begin(); it != mFoundDevice.end();++it)
				{
					RemoteDevicePtr dev = *it;

					if(dev == NULL)
						continue;


//					if(mDebugLocalization)
//						if(dev->GetDeviceType() == OmniSensr) {
//							printf("OS[%s]  dev->GetMeasBufSize() = %d\n", dev->GetDevAddr().c_str(), (int)dev->GetMeasBufSize());
//						}

						// -- See if there's any unprocessed measurements
						if(dev->GetMeasBufSize() > 0)
						{

							int errorCode = periodicProcessing_per_Station(dev);

							if(errorCode < 0) {
								if(mDebugLocalizationErrorCode && isDevicesOfInterest(dev->GetDevAddr())) printf("ERROR!!: Node[%s]: periodicProcessing_per_Station() failed because of Error Code = %d\n", dev->GetDevAddr().c_str(), errorCode);
							}

						}

						dev->time_prevBufferProcessed = currTime;
				}
			}
			//pthread_mutex_unlock(&gMutex);


			// -- Check if there's any AP that is not reporting to us.
			int cnt_reporting = 0, cnt_total = 0;
			for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it)
			{
				ap = *it;

				if(ap == NULL)
					continue;

				timeElapsed = currTime - ap->time_MsglastReceived;

				if((int)timeElapsed == (int)(10*mPeriodicBufferProcessingInterval)) {
					if(mDebugDeviceStatusVerboseMode > 1)
						printf("AP[%s]: No message is received!! (time elapsed = %.1lf sec)\n", ap->mIPaddr.c_str(), timeElapsed);
				} else if((int)timeElapsed == (int)(20*mPeriodicBufferProcessingInterval)) {
					ap->mIsCurrentlyReceivingMessages = false;
					if(mDebugDeviceStatusVerboseMode > 1)
						printf("AP[%s]: !! AP has stopped working !! (time elapsed = %.1lf sec)\n", ap->mIPaddr.c_str(), timeElapsed);
				}

				if(ap->mIsCurrentlyReceivingMessages == true)
					cnt_reporting++;

				cnt_total++;
			}

			if(cnt_total > 0) {
				float wifiAvailability = cnt_reporting/(float)cnt_total * 100.0f; // -- Unit: [%]
				mTrackerState.WiFiSensorAvailability = (int)(0.6*(float)mTrackerState.WiFiSensorAvailability + 0.4*wifiAvailability + 0.5f); // -- Unit: [%]

				//printf("[WiFi_Shopper_Tracker] (mTrackerState.WiFiSensorAvailability, wifiAvailability) = (%d, %d)\n", mTrackerState.WiFiSensorAvailability, (int)wifiAvailability);
			}

			pthread_mutex_lock(&gMutex);
			//mIsThereSomeProcessing &= ~(1<<PERIODIC_PROCESSING);
			mIsThereSomeProcessing = 0;
			pthread_mutex_unlock(&gMutex);

			prevTime_buffer = currTime;
		}

		// -- Report the location of the devices
		if(mReportRealTimeTrackResult == true) {
			if(currTime - prevTime_report >= mPeriodicInterval_TrackingReport && mRunningWiFiSensor.size() > 0) {

				// -- Wait until the other thread finishes its turn.
				do {
					pthread_mutex_lock(&gMutex);
					bool isOtherThreadProcessing = mIsThereSomeProcessing; //mIsAddingNewDevice || mIsDrawingMap;
					pthread_mutex_unlock(&gMutex);

					if(isOtherThreadProcessing > 0) usleep(5000);
					else break;
				} while(true);

				pthread_mutex_lock(&gMutex);
				//mIsThereSomeProcessing |= (1<<PERIODIC_TRAJECTORY_REPORTING);
				mIsThereSomeProcessing = (1<<PERIODIC_TRAJECTORY_REPORTING);
				pthread_mutex_unlock(&gMutex);

				// -- Periodically publish the up-to-date tracking results to a MQTT Broker
				reportTrackingResults();

				pthread_mutex_lock(&gMutex);
				//mIsThereSomeProcessing &= ~(1<<PERIODIC_TRAJECTORY_REPORTING);
				mIsThereSomeProcessing = 0;
				pthread_mutex_unlock(&gMutex);

				prevTime_report = currTime;
			}
		}

        // -- Clear-up the device list
		if(currTime - prevTime_clear >= mPeriodicInterval_DevClearUp && mRunningWiFiSensor.size() > 0) {

			// -- Wait until the other thread finishes its turn.
			do {
				pthread_mutex_lock(&gMutex);
				bool isOtherThreadProcessing = mIsThereSomeProcessing; //mIsAddingNewDevice || mIsDrawingMap;
				pthread_mutex_unlock(&gMutex);

				if(isOtherThreadProcessing > 0) usleep(5000);
				else break;
			} while(true);

			pthread_mutex_lock(&gMutex);
			//mIsThereSomeProcessing |= (1<<CLEARING_UP_DEVICE);
			mIsThereSomeProcessing = (1<<CLEARING_UP_DEVICE);
			pthread_mutex_unlock(&gMutex);

			// -- To discover if there's any device from a service agent
			if(mFoundDevice.size() > 0) {
				for(deque<RemoteDevicePtr>::iterator it = mFoundDevice.begin(); it != mFoundDevice.end();++it)
				{
					RemoteDevicePtr dev = *it;
					if(dev == NULL || isDevicesOfInterest(dev->GetDevAddr()) == true || dev->GetDeviceType() == AP || dev->GetDeviceType() == OmniSensr)
						continue;

					//double elapsedTime = currTime - dev->AtTrackedPosBuf()->begin()->localtime;

					if(dev->mKalmanPos_m.GetTimeElapsedFromInitialUpdateToLastUpdate() > mTimeThreshold_ServiceAgent*60
							|| dev->GetTotalShoppingTime() > mTimeThreshold_ServiceAgent*60
//							&& dev->GetTrackedPosAge() > 50
							)
					{

						dev->SetDeviceType(ServiceAgent);

						Node_info_t *sa = findServiceAgent(dev->GetDevAddr());

						// -- Add to the ServiceAgent list if not existing
						if(sa == NULL) {
							tcpdump_t msg;
							msg.src = dev->GetDevAddr();
							msg.deviceType = ServiceAgent;

							add_knownDeviceType(&msg);
						}

						if(mDebugDeviceStatusVerboseMode > 0)
							printf("REMOVE [%s] since it is turned out to be a ServiceAgent w/ (Trajectory age = %d, Kalman Age = %d, GetTimeElapsedFromInitialUpdateToLastUpdate() = %.2f sec, GetTimeElapsedFromInitialUpdateToLastUpdate() = %.2f, dev->GetTotalShoppingTime() = %.2f).\n", dev->GetDevAddr().c_str(),
									(int)dev->GetTrackedPosAge(), dev->mKalmanPos_m.GetKalmanAge(), dev->mKalmanPos_m.GetTimeElapsedFromInitialUpdateToLastUpdate(), (float)dev->mKalmanPos_m.GetTimeElapsedFromInitialUpdateToLastUpdate(), (float)dev->GetTotalShoppingTime());

						if(!mTrackServiceAgent) {
							// -- Remove from the device list
							dev->mTobeRemoved = true;
						}
					}
				}
			}

			// -- Discard devices that are not active
			clearUpDeviceList();

			pthread_mutex_lock(&gMutex);
			//mIsThereSomeProcessing &= ~(1<<CLEARING_UP_DEVICE);
			mIsThereSomeProcessing = 0;
			pthread_mutex_unlock(&gMutex);

			prevTime_clear = currTime;
		}

		// send a heartbeat once per minute
		if(currTime -prevTime_heartbeat > 60){
			iotc.sendHeartbeat();
			prevTime_heartbeat = currTime;
		}

		usleep(50*1000);

	}

	pthread_exit(NULL);

    return;
}

void* WiFi_Shopper_Tracker::PeriodicBufferProcessingThread(void *apdata) {

	WiFi_Shopper_Tracker* pb = (WiFi_Shopper_Tracker*)apdata;

	pb->PeriodicBufferProcessing_Loop();

    return NULL;
}

// -- Un-scramble MAC address to a random one
//void WiFi_Shopper_Tracker::unscrambleRSSData(char *buf, char *src, int *rss, int *deviceType, int buffer_len) {
void WiFi_Shopper_Tracker::unscrambleRSSData(char *buf, int buffer_len, string &src, int &rss, int &deviceType, int &channel) {
	if(DEBUG_FUNCTION_CALL) printf("unscrambleRSSData() is called. \n");

	// -- Get the device type from the first byte
	deviceType = buf[0] - '0';

	// -- Check if the message is intended for WiFi Calibration
	if(deviceType == OmniSensr) {

		//printf("WiFi Calibration messge: %s\n", buf);

		// -- Get the channel that the message is detected by translating the hex-coded channel value in a one-byte char into an integer
		channel = (buf[1] > '9')? (buf[1] &~ 0x20) - 'A' + 10: (buf[1] - '0');

		// -- Get the Serial Number of the sender OmniSensr
		src.assign(&buf[2], 11);

		string rss_s;
		rss_s.assign(&buf[13], buffer_len - 13);

		// -- Get the RSS value
		sscanf(rss_s.c_str(), "%x", &rss);
		rss /= -2;

		//printf("Input: %s ==> (deviceType, src, rss, channel)  = (%d, %s, %d, %d)\n", buf, deviceType, src.c_str(), rss, channel);

	} else {
		// -- Get the channel that the message is detected by translating the hex-coded channel value in a one-byte char into an integer
		channel = (buf[1] > '9') ? (buf[1] &~ 0x20) - 'A' + 10: (buf[1] - '0');
		//printf("Msg[%s] --> Channel = %d\n", buf, *channel);

		// -- Get the device MAC address
		src.assign(&buf[2], 12);

		string rss_s;
		rss_s.assign(&buf[14], buffer_len - 14);

		// -- Get the RSS value
		sscanf(rss_s.c_str(), "%x", &rss);
		rss /= -2;

		//printf("Input: %s ==> (deviceType, src, rss, channel)  = (%d, %s, %d, %d)\n", buf, deviceType, src.c_str(), rss, channel);
	}


	if(DEBUG_FUNCTION_CALL) printf("unscrambleRSSData() is ended. \n");
}

// -- Parse the data from remote Wi-Fi sensor via MQTT
bool WiFi_Shopper_Tracker::parse_PacketProcessing_at_Sniffer_input(tcpdump_t *meas, char *buffer, int buffer_len) {
	if(DEBUG_FUNCTION_CALL) printf("parse_PacketProcessing_at_Sniffer_input() is called. \n");
	if(meas == NULL) return false;

	//printf("strlen(buffer) = %d, buffer = %s\n", strlen(buffer), buffer);

	if(buffer_len > 25 || buffer_len < 10) {	// -- Supposed to be 22(?)
		if(DEBUG_FUNCTION_CALL) printf("parse_PacketProcessing_at_Sniffer_input() End at 1. \n");
		return false;
	}

#ifdef USE_WIFI_PLAYER
	char addr[99] = {'\0'};
	sscanf(buffer, "%s %d", addr, &meas->rss);
	meas->src = addr;

	// -- erase ':' from the address field if there is
	if(meas->src.size() > 0)
		meas->src.erase(std::remove(meas->src.begin(), meas->src.end(), ':'), meas->src.end());

	meas->deviceType = 0;

#else

	// -- Unscramble src address and RSS data
	unscrambleRSSData(buffer, buffer_len, meas->src, meas->rss, meas->deviceType, meas->channel);

//		if(meas->deviceType == OmniSensr) {
//			printf("CalibPacket: (deviceType, src, rss, channel)  = (%d, %s, %d, %d)\n", meas->deviceType, meas->src.c_str(), meas->rss, meas->channel);
//		}

//	if(meas->deviceType == AP) {
//		printf("AP [%s]\n", meas->src);
//	}

#endif

	// -- TODO: Do we need to identify what protocol the device is used among a/b/g/n/ac? Let's just mark it with 0 as a default value.
	meas->protocol = 0;

	// -- See if the measurement is valid. If not, discard it.
	if(!(meas->rss <= 0 && meas->rss > -100 && (meas->src.size() == 12 || meas->src.size() == 11))) {
		if(DEBUG_FUNCTION_CALL || mDebugVerboseLevel > 2)
			printf("parse_PacketProcessing_at_Sniffer_input() FALSE: (src, rss) = (%s, %d) (meas->src.size() = %d), Buffer = %s \n", meas->src.c_str(), meas->rss, (int)meas->src.size(), buffer);

		if(DEBUG_FUNCTION_CALL) printf("parse_PacketProcessing_at_Sniffer_input() End at 3. \n");
		return false;
	}

	if(mShowRawMsg) {
		printf("%s %d \n", meas->src.c_str(), meas->rss);
	}


	if(DEBUG_FUNCTION_CALL) printf("parse_PacketProcessing_at_Sniffer_input() Ended. \n");
	return true;
}

int get_line(FILE *fp, char *buffer, size_t buflen)
{
    char *end = buffer + buflen - 1; /* Allow space for null terminator */
    char *dst = buffer;
    int c;
    while ((c = getc(fp)) != EOF && dst < end)
    {
        if ((*dst++ = c) == '\n')
            break;
    }
    *dst = '\0';
    return((c == EOF && dst == buffer) ? EOF : dst - buffer);
}

// -- Main function for receiving and parsing packet stream
void WiFi_Shopper_Tracker::packetStreamParsing() {

	FILE *streamFromAP;

#ifdef USE_WIFI_PLAYER

	// -- Read the stream from WiFi_Shopper_Player
	char player_cmd[299];
//	sprintf(player_cmd, "mosquitto_sub -h %s -u %s -P %s -p %d -q %d -k %d -t \"%s\"", MQTT_BROKER_URL, MQTT_BROKER_USERNAME, MQTT_BROKER_PASSWORD,
//							MQTT_BROKER_PORT, MQTT_BROKER_QOS, MQTT_BROKER_KEEP_ALIVE, mMQTT_subTopic.c_str());

	// -- Usage is <store_name> <start_date (e.g., 2015-09-09)> <start_hour (e.g., 15)> <end_date> <end_hour> <clock_speed>
	char filename_[299], monitoring_ap_list_filepath[299];
	sprintf(filename_,"../WiFi_Shopper_Player/Record/%s/%s-%.2dh.txt", mIoTGateway.GetMyStoreName().c_str(), START_DATE, START_HOUR);
	sprintf(monitoring_ap_list_filepath,"../WiFi_Shopper_Player/Record/%s", mIoTGateway.GetMyStoreName().c_str());
	sprintf(player_cmd, "../WiFi_Shopper_Player/Debug/./WiFi_Shopper_Player %s %s %d %s %d %f %s %s 0", mIoTGateway.GetMyStoreName().c_str(), START_DATE, START_HOUR, END_DATE, END_HOUR, CLOCK_SPEED, filename_, monitoring_ap_list_filepath);
	//sprintf(player_cmd, "../WiFi_Shopper_Player/Debug/./WiFi_Shopper_Player");

	printf("[Tracker] player_cmd = %s\n", player_cmd);

	// -- Subscribe to a MQTT broker
	streamFromAP = popen(player_cmd, "r");

#else

	// -- Connect to the AP and get the results through a MQTT broker
	char mqtt_sub_cmd[299];
	sprintf(mqtt_sub_cmd, "mosquitto_sub -h %s -p %d -q %d -k %d -t \"%s\" -v", MQTT_BROKER_URL,
							MQTT_BROKER_PORT, MQTT_BROKER_QOS, MQTT_BROKER_KEEP_ALIVE, mMQTT_subTopic.c_str());

	printf("[Tracker] mqtt_sub_cmd = %s\n", mqtt_sub_cmd);

	// -- Subscribe to a MQTT broker
	streamFromAP = popen(mqtt_sub_cmd, "r");
//	int streamFromAPFd = fileno(streamFromAP);
#endif

	mOpenedPipe.push_back(streamFromAP);

	double currTime = gTimeSinceStart();
	double timeElapsed = 0;
	WiFiSensor_short_Ptr ap = NULL;

    char buffer[MAX_BUFFER];

	// -- Read from remote broker and publish to local broker
    string lineInput;
    string topic, DevName, IP;
    vector<string> measBuffer;

//    while(true) {
//    	ssize_t rSize = read(streamFromAPFd, buffer, sizeof(buffer));
//
//        if (rSize == -1)
//            continue;

	while(fgets(buffer, MAX_BUFFER, streamFromAP)) {	// -- Reads a line from the pipe and save it to buffer
		lineInput.clear();
		lineInput.append(buffer);

		currTime = gTimeSinceStart();

        if(mKillRequestPending == true)
            break;


		// -- Break the line input into words and save it into a word vector
		istringstream iss (lineInput);
		vector<string> v;
		string s;
		while(iss >> s)
			v.push_back(s);

		// -- If the size of word vector is greater than one, then it should be the first line of message containing {topic, #DevName, IP}
		if(v.size() > 1) {

//			printf("[WiFi_Shopper_Tracker] [streamFromRemote HEADER] [word count = %d] %s", (int)v.size(), lineInput.c_str());

			#ifdef USE_WIFI_PLAYER
						if(v.size() == 6) {
//							char t0[99];
//							sscanf(buffer, "%s %lf %s %s %s %s"
//									, t0 				// -- #T=
//									, &mSimulationTime   		// -- 1441830884.1
//									, mSimulationTime_date			// -- 2015-09-09
//									, mSimulationTime_time			// -- 16:34:44.107
//									, t0				// -- OmniSensr_addr=
//									, current_IP_addr	// -- 10.7.6.7
//									);
//
//							mSimulationTime -= CLOCK_DRIFT_IN_PLAYER;	// -- TODO: It's weird, but there's 4hr difference between the radio map measured and the actual clock

							mSimulationTime = atof(v[1]);
							mSimulationTime_date = v[2];
							mSimulationTime_time = v[3];
							current_IP_addr = v[5];

						}

			#else

						if(v.size() == 3) {

							// -- If there's a buffer to send out, then flush it first before getting the next message
							if(measBuffer.size() > 0 && topic.size() > 0 && ap != NULL) {

								// -- Wait until the other thread finishes its turn.
								do {
									pthread_mutex_lock(&gMutex);
									bool isOtherThreadProcessing = mIsThereSomeProcessing; // mIsPeriodicProcessing || mIsDrawingMap;
									pthread_mutex_unlock(&gMutex);

									if(isOtherThreadProcessing > 0) usleep(5000);
									else break;
								} while(true);

								pthread_mutex_lock(&gMutex);
								//mIsThereSomeProcessing |= (1<<ADDING_NEW_DEVICE);
								mIsThereSomeProcessing = (1<<ADDING_NEW_DEVICE);
								pthread_mutex_unlock(&gMutex);

//								printf("[streamFromRemote HEADER] %s", lineInput.c_str());

								// -- Parse the messages
								for(vector<string>::iterator it = measBuffer.begin(); it != measBuffer.end(); ++it) {
									string measStr = *it;

//									printf("[streamFromRemote BODY] %s", measStr.c_str());

									tcpdump_t	meas;

									meas.src.clear();
									meas.dst.clear();

									// -- (1) Get and store the summarized packets from each AP
									bool wasSuccessful = parse_PacketProcessing_at_Sniffer_input(&meas, (char *)measStr.c_str(), measStr.size());

									// -- Store what AP captured the RSS measurement
									meas.capturedBy = ap->mDevName;

									if(wasSuccessful && ap != NULL) {	// -- Found a tcpdump input that has a source addr info

//										if(mDebugVerboseLevel > 0)
//											printf("AP[%s]: (src, rss) = (%s, %d)\n", ap->mDevName.c_str(), meas.src.c_str(), meas.rss);

										// -- (2) Mark the captured time
										//meas.localtime = gTimeSinceStart();
										meas.localtime = gCurrentTime();

										// -- Handle the WiFi Calibration packets
										if(meas.deviceType == OmniSensr) {

											//printf("OmniSensr[%s]: (src, rss) = (%s, %d) @Ch: %d\n", ap->mDevName.c_str(), meas.src.c_str(), meas.rss, meas.channel);

											// -- Feed the packet into a calibration module
											calibAddNewRSS(meas);

										}

										// -- Dispatch all the measurements from each AP to each device
										// -- Now, each device would have one measurement from each AP that captured any packet from the device
										addCollectedMeasurementToDeviceBuffer(&meas);

										// -- Mark timestamp for the received AP
										timeElapsed = currTime - ap->time_MsglastReceived;

										if((int)timeElapsed > (int)(3*mPeriodicBufferProcessingInterval)) {
											ap->mIsCurrentlyReceivingMessages = true;
											if(mDebugDeviceStatusVerboseMode > 1)
												printf("AP[%s]: Start receiving messages again (time elapsed = %.1lf sec)\n", ap->mIPaddr.c_str(), timeElapsed);
										}

										pthread_mutex_lock(&gMutex);
										ap->time_MsglastReceived = currTime;
										pthread_mutex_unlock(&gMutex);
									}		// -- End of if(wasSuccessful
								}		// -- End of for(vector<string>::iterator it

								pthread_mutex_lock(&gMutex);
								//mIsThereSomeProcessing &= ~(1<<ADDING_NEW_DEVICE);
								mIsThereSomeProcessing = 0;
								pthread_mutex_unlock(&gMutex);

								// -- Initialize measBuffer
								measBuffer.clear();
							}

							topic = v[0];
							DevName = v[1];
							IP = v[2];

							DevName = DevName.substr(1);	// -- Get rid of the first '#' from the string

//							printf("[WiFi_Shopper_Tracker] Received a msg w/ (Topic, DevName, IP) = (%s, %s, %s)\n", topic.c_str(), DevName.c_str(), IP.c_str());

							ap = findRunningWiFiSensor(DevName);
							if(ap == NULL)
								ap = findRunningWiFiSensor(IP);

							continue;
						}
			#endif

		}
		else if(v.size() == 1) {

			// -- Insert the body message
			measBuffer.push_back(lineInput);
		}
	}

    if(streamFromAP != 0)
    	pclose(streamFromAP);

#ifdef USE_WIFI_PLAYER

#else

    	fprintf(stderr, "[Tracker]: $$$$ Connection was closed, so packetStreamParsing() ended $$$$\n");
#endif

    	pthread_exit(NULL);

}

#ifdef USE_WIFI_PLAYER
void* WiFi_Shopper_Tracker::PacketStreamParsingThread(void *apdata) {
	WiFi_Shopper_Tracker* pb = (WiFi_Shopper_Tracker*)apdata;

	pb->packetStreamParsing();

	printf("\n########### End of recorded measurement file ##############\n");

	return NULL;
}
#else

void* WiFi_Shopper_Tracker::PacketStreamParsingThread(void *apdata) {
	WiFi_Shopper_Tracker* pb = (WiFi_Shopper_Tracker*)apdata;

	int cnt = 0;

	double time_diff_avg, prev_time = gCurrentTime();

	while(1) {

		pb->packetStreamParsing();

		cnt++;

		double time_diff = gCurrentTime() - prev_time;
		time_diff_avg = (time_diff_avg*cnt + time_diff)/(cnt+1);

		if( (cnt > 3 && time_diff_avg < 20)
		) {
			break;
		}

		sleep(10);
	}

	return NULL;
}

#endif

// -- Scramble the MAC address and location data
string WiFi_Shopper_Tracker::scrambleDATA(string addr, cv::Point loc, int track_age) {
	if(DEBUG_FUNCTION_CALL) printf("scrambleRSSData() is called. \n");


	string msg;
	msg.clear();
	msg.append(addr);

	std::stringstream stream;
	stream << setw(5) << setfill('0') << std::hex << loc.x + 499999;
	stream << setw(5) << setfill('0') << std::hex << loc.y + 499999;
	stream << setw(5) << setfill('0') << std::hex << track_age + 1000;
	msg.append( stream.str() );

//	if(mDebugVerboseLevel > 0)
//		printf("[Output msg]:[%s] (size:%d)\n\n", msg.c_str(), (int)msg.size());

	if(DEBUG_FUNCTION_CALL) printf("scrambleRSSData() is ended. \n");

	return msg;
}

// -- Un-scramble the shopper's MAC address and location data
void WiFi_Shopper_Tracker::unScrambleTrackData(const char *buf, int buf_len, string &src, cv::Point *loc, int *age) {
	if(DEBUG_FUNCTION_CALL) printf("unScrambleTrackData() is called. \n");

	string data;

	// -- Get the device MAC address
	src.assign(&buf[0], 12);

	string loc_s_x, loc_s_y, age_s;
	loc_s_x.assign(&buf[12], buf_len - 5);
	loc_s_y.assign(&buf[17], buf_len - 5);
	age_s.assign(&buf[22], buf_len -5);

	// -- Get the RSS value
	sscanf(loc_s_x.c_str(), "%x", &loc->x);
	sscanf(loc_s_y.c_str(), "%x", &loc->y);
	sscanf(age_s.c_str(), "%x", age);


	// -- Get the Location data
	loc->x -= 499999;
	loc->y -= 499999;
	*age -= 1000;

	if(DEBUG_FUNCTION_CALL) printf("unScrambleTrackData() is ended. \n");
}

// -- Periodically publish the tracking results of the shoppers' location to a MQTT Broker
void WiFi_Shopper_Tracker::reportTrackingResults() {
	if(DEBUG_FUNCTION_CALL) printf("reportTrackingResults() is called. \n");

	//double currTime = gTimeSinceStart();
//	string msg;
//	msg.clear();

					ostringstream locs;

//					// -- Go through from the end of the list since the end is the beginning of the trajectory.
//					for(deque<TrackedPos_t_>::iterator it = dev->AtTrackedPosBuf()->end() - 1; it >= dev->AtTrackedPosBuf()->begin(); --it) {
//						TrackedPos_t_ loc = *it;
//						double elapsedTime = loc.localtime - timeMeas_start;
//
//						if(it != dev->AtTrackedPosBuf()->end() - 1) {
//							traj << ",";
//						}
//
//						traj << fixed << showpoint << setprecision(1);	// -- Make sure the precision is 1/10 second.
//						traj << "(" << elapsedTime << "|" << loc.pos.x << "," << loc.pos.y << ")";
//					}

	pthread_mutex_lock(&gMutex);

	for(deque<RemoteDevicePtr>::iterator it = mFoundDevice.begin(); it != mFoundDevice.end();++it)
	{
		RemoteDevicePtr dev = *it;

		if(dev == NULL)
			continue;

		TrackedPos_t_ *curr_pos;

			if(dev->GetDeviceType() != AP && dev->GetDeviceType() != ServiceAgent
				&& dev->GetTrackedPosAge() > 2
				&& dev->mTobeRemoved == false
				//&& currTime - dev->mKalmanPos_m.GetTimeElapsedFromLastUpdate() < TIME_THRESHOLD_DEVICE_DISAPPEAR*60
				) {

				curr_pos = &(*dev->AtTrackedPosBuf()->begin());

				// -- Publish the current location of active shoppers to the MQTT broker, if not reported yet
				if(curr_pos->isReported == false) {

					//string msg_ = scrambleDATA(dev->GetDevAddr(), curr_pos->pos, dev->GetTrackedPosAge());
					//locs << msg_ << ",";

					locs << dev->GetDevAddr() << "-" << curr_pos->pos.x << "-" << curr_pos->pos.y << "-" << dev->GetTrackedPosAge() << ",";

//					msg.append(msg_);
//					msg.append("\n");

					curr_pos->isReported = true;

					// -- Debug
					//if(MQTT_mDebugVerboseLevel > 0)
//					{
//						//char src[18] = {'\0'};
//						string src;
//						cv::Point loc;
//						int track_age;
//						unScrambleTrackData(msg_.c_str(), msg.size(), src, &loc, &track_age);
//
//						//if(mDebugVerboseLevel > 0)
//							printf("unScrambled Data: [%s] (%d, %d) @age(%d) < (%s, %d, %d)@age(%d) >\n", src.c_str(), loc.x, loc.y, track_age, dev->GetDevAddr().c_str(), curr_pos->pos.x, curr_pos->pos.y, dev->GetTrackedPosAge());
//					}
				}
			}
	}

	pthread_mutex_unlock(&gMutex);

	// -- Publish/report the real-time tracking results to AWS IoT Cloud
	if(locs.str().size() > 0) {

		mClock.updateTime();

		string date = mClock.mDateOnly_str;
		string time = mClock.mTimeOnly_str;

		stringstream jsonStr;

		jsonStr << fixed << showpoint << setprecision(1);	// -- Make sure the precision is 1/10 second.

		jsonStr << "{"
					//<< "\"tracker_id\" : \"" << mTrackerID.str() << "\","
					//<< "\"date\" : \"" << date << "\","
					//<< "\"time\" : \"" << time << "\","
					<< "\"time\" : " << gCurrentTime() << ","
					<< "\"Locs\" : \"" << locs.str() << "\""
				<< "}";

		try {

			json trajectoryMsg;
			trajectoryMsg << jsonStr;

			// -- Publish/report the real-time tracking results to AWS IoT Cloud
			{

				jsonStr.clear();
				jsonStr.str("");

				// -- Note: Data message must be in JSON format to be processed by Rules Engine at AWS IoT cloud
				//jsonStr << std::setw(2) << trajectoryMsg << '\n';
				jsonStr << trajectoryMsg << '\n';

				// -- Put into a message buffer in IoT Gateway for transmission to AWS Cloud via AWS_IOT_Connector module
				// -- The data message will be sent to a topic named by 'data/<app_id>/<store_name_prefix>/<store_name_suffix>/<device_name>' (e.g., data/wifiRss/vmhq/16801/cam001)
				VM_IoT_Error_t errorCode = mIoTGateway.SendData("/realTimeLocs", jsonStr.str());

				if(errorCode != NO_PROBLEM)
					cout << "VM_IoT_Error: mIoTGateway.SendData(): " << errorCode << endl;
			}

		} catch (std::invalid_argument& e) {		// -- Handle exception when the received msg does not comply with JSON standard
			// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
			std::cout << "[realTimeLocs] invalid_argument: " << e.what() << '\n';
		} catch (std::out_of_range& e) {	// -- Handle exception when a non-existing filed is accessed.
			// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
			std::cout << "[realTimeLocs] out of range: " << e.what() << '\n';
		} catch (std::domain_error& e) {	// -- Handle exception when an incompatible conversion is attempted (e.g., float --> string).
			// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
		     std::cout << "[realTimeLocs] domain error: " << e.what() << '\n';
		}

//		if(mMQTT_Tracker->send_message(mMQTT_Tracker->mPubTopic_Data, msg, MQTT_BROKER_QOS /* QoS level */)) {
//			printf("### mMQTT_Tracker send FAIL ###\n");
//		}
	}

	if(DEBUG_FUNCTION_CALL) printf("reportTrackingResults() is ended. \n");

}

void WiFi_Shopper_Tracker::drawMap() {
	//if(DEBUG_FUNCTION_CALL) printf("drawMap() is called. \n");

	mImg_map_init.copyTo(mImg_map);

	// -- Visualize the converted distance based on RSS measurements
	visualizeRSSestimation();

	// -- Visualize the calibration process progress
	if(mWiFiOperationMode == WIFI_CALIBRATION)
		visualizeCalibrationProgress();

	// -- Show sensor locations
    if(mShowSensors)
        if(mRunningWiFiSensor.size() > 0)
            for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it)
            {
                WiFiSensor_short_Ptr ap = *it;

                // -- Show sensors only if the sensor is not working correctly
                show_sensors(ap, mImg_map, mFontSizeScale);
            }


	// -- Show shopper tracking summary info
	{
		char str_msg[255];

		sprintf(str_msg, "# of Devs (In, Total) = (%d, %d), "
				"# of (APs, SAs, NULLs) = (%d, %d, %d), "
				"Total # of (Active, MAX) Dev. = (%d, %d), FPS = %d%%, Avail = %d%%",
				mTrackerState.noTrackedShoppersWithinArea, mTrackerState.noTrackedShoppers,
				mTrackerState.noTrackedAPs,
				mTrackerState.noTrackedServiceAgent,
				mTrackerState.noTrackedDevs_NULL,
				mTrackerState.noTrackedDevs, mTrackerState.maxTrackedDevs,
				mTrackerState.avgPeriodicProcessingRate, mTrackerState.WiFiSensorAvailability);

        putText(mImg_map, str_msg, Point(10, 10 + 8.0*mFontSizeScale), 0, 0.5*mFontSizeScale, CV_RGB(0,0,155), 1, CV_AA);

		char str_msg_1[255];
		sprintf(str_msg_1, "(Avg, Max) Device Buffer Size = (%.1f, %d)", mTrackerState.avgBufferSize_dev, mTrackerState.maxBufferSize_dev);
        putText(mImg_map, str_msg_1, Point(10, 35 + 8.0*mFontSizeScale), 0, 0.5*mFontSizeScale, CV_RGB(0,0,155), 1, CV_AA);

		char str_msg_2[255];

#ifdef USE_WIFI_PLAYER
		sprintf(str_msg_2, "Running Time = %s Simulation Time = %d (%s %s)", getRunningTime().c_str(), (int)mSimulationTime, mSimulationTime_date, mSimulationTime_time);
#else
		// -- Running Time = 23:45:54 (2016-09-07 10:09:37 AM)
		sprintf(str_msg_2, "Running Time = %s", getRunningTime().c_str());
#endif

		if(mDebugVerboseLevel > 0) {
			static int i=0;
			if(i++%100==0)
				cout << str_msg_2 << ": " << str_msg << endl;

		}

        putText(mImg_map, str_msg_2, Point(10, mStoreWidth_IMG_y - 15), 0, 0.5*mFontSizeScale, CV_RGB(0,0,155), 1, CV_AA);

	}

	// --Show calibration progress
	if(mWiFiOperationMode == WIFI_CALIBRATION) {
		char str_msg[255];
		sprintf(str_msg, "Calib Time = %d [sec], to be completed at %d [sec]", (int)mCalibElapsedTime, mCalibPeriod_max*60);

        putText(mImg_map, str_msg, Point(10, mStoreWidth_IMG_y - 35), 0, 0.5*mFontSizeScale, CV_RGB(0,0,155), 1, CV_AA);
	}

    pthread_mutex_lock(&gMutex);
    mImg_map.copyTo(mImg_map_final);
    pthread_mutex_unlock(&gMutex);

    if(mShowMapVisualization) {
        imshow(mImgMapName, mImg_map);
    }

}

string WiFi_Shopper_Tracker::getRunningTime() {
	double currTime = gTimeSinceStart();

	int hr = (int)(currTime/3600);
	int min = (int)((currTime - hr*3600)/60);
	int sec = (int)(currTime - hr*3600 - min*60);

	char str_msg[255];

	// -- Running Time = 23:45:54 (2016-09-07 10:09:37 AM)
	sprintf(str_msg, "%.2d:%.2d:%.2d (%s)", hr, min, sec, currentDateTime().c_str());

	string runningTime(str_msg);

	return runningTime;
}

// Get current date/time, format is YYYY-MM-DD.HH:mm:ss
const string WiFi_Shopper_Tracker::currentDateTime() {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);

    // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
    // for more information about date/time format
    // --  2016-09-07 10:09:37 AM
    strftime(buf, sizeof(buf), "%Y-%m-%d %X", &tstruct);

    return buf;
}

MosqMQTT_agg::MosqMQTT_agg(const char * _id, bool clean_session) : MosqMQTT(_id, clean_session) {

	 mosqpp::lib_init();        // Mandatory initialization for mosquitto library
	 mPid = getpid();

	 mClientId.clear();
	 mClientId.str("");
	 mClientId << _id << "-" << mPid;

	 reinitialise(mClientId.str().c_str(), clean_session);
}
MosqMQTT_agg::~MosqMQTT_agg() {
	 loop_stop();            // Kill the thread
	 mosqpp::lib_cleanup();    // Mosquitto library cleanup
}

void MosqMQTT_agg::on_connect(int rc)
{
    if ( rc == 0 ) {     /* success */
        if(DEBUG_MQTT_AT_TRACKER)
            cout << ">> myMosq - connected with server" << endl;

//        mIsConnected = true;

        // -- Subscribe to topics after a successful connection is made
        if(mSubTopics.size() > 0)
			for(vector<string>::iterator it = mSubTopics.begin(); it != mSubTopics.end(); ++it) {
				try {
					string itt = *it;
					int ret;
					if((ret = subscribe(NULL, (const char*)(itt.c_str()), 1)) == MOSQ_ERR_SUCCESS) {

						if(DEBUG_MQTT_AT_TRACKER)
							printf("[Mosquitto MQTT] Subscribed to %s \n", itt.c_str());
					} else {
						fprintf(stderr,"[Mosquitto MQTT] Subscribed to %s: FAIL [%d]\n", itt.c_str(), ret);
					}
				} catch (exception& e) {
					if(DEBUG_MQTT_AT_TRACKER)
						printf("Error: Failed to subscribe\n%s\n", e.what());
				}
			}

    } else {
        if(DEBUG_MQTT_AT_TRACKER)
            cout << ">> myMosq - Impossible to connect with server(" << rc << ")" << endl;

        // -- Start the mosquitto MQTT client
        start();
    }

 }

void MosqMQTT_agg::on_disconnect(int rc)
{

	if(DEBUG_MQTT_AT_TRACKER)
		cout << ">> myMosq - disconnection(" << rc << ")" << endl;

	while(1) {

		sleep(1);

		int ret = reconnect();

		if(DEBUG_MQTT_AT_TRACKER)
			cout << ">> myMosq - reconnect(" << ret << ")" << endl;

		if(ret == MOSQ_ERR_SUCCESS) break;
		else usleep(1e5);
	}

 }

bool WiFi_Shopper_Tracker::addKalmanParams() {
	// -- Add to Kalman config file
	ostringstream configFilename;
	configFilename << APP_CONFIG_ROOT_PATH << "/" << WiFiTracker_APP_NAME << "/" << APP_STORE_ROOT_PATH << "/" << mAppStoreName << "/" << KALMAN_CONFIG_FILE;

    FILE *fp_kalman;

    fp_kalman = fopen(configFilename.str().c_str(), "a+");
    if(fp_kalman == NULL) {
        fprintf(stderr, "Error opening [WiFi_Shopper_Tracker] Kalman Param file\n");
        return false;
    } else {
    	fprintf(fp_kalman, "\n%f %f %f %f %f %f %f %f %f",
    				mKalmanRssProcessNoise_var, mKalmanRssMeasurementNoise_var, mKalmanRssCovCoeff_pos_min, mKalmanRssCovCoeff_accel_min, mKalmanRssMaxStdAllowed,
					mKalmanPosProcessNoise_var, mKalmanPosMeasurementNoise_var, mKalmanPosCovCoeff_pos_min, mKalmanPosCovCoeff_accel_min
    			);
    }

    fclose(fp_kalman);

    cout << "addKalmanParams() Done\n";

    return true;
}

bool WiFi_Shopper_Tracker::loadKalmanParams(string configfile) {

    FILE *fp_list;

    bool isSuccessful = false;

    fp_list = fopen(configfile.c_str(), "r");
    if(fp_list == NULL) {
        fprintf(stderr, "Error opening [WiFi_Shopper_Tracker] Kalman Param file\n");
        return false;
    } else {
        char buffer[255];

        string lineInput;
        vector<string> measBuffer;
        while(fgets(buffer, 255, fp_list)) {	// -- Reads a line from the pipe and save it to buffer
            lineInput.clear();
            lineInput.append(buffer);

            // -- If there's any comment, then discard the rest of the line
            std::size_t found = lineInput.find_first_of("#");
            // -- If '#' is found, then discard the rest.
            if(found != string::npos)
                lineInput.resize(found);

			istringstream iss (lineInput);
			vector<string> v;
			string s;
			while(iss >> s)
				v.push_back(s);

			// -- Keep the last parameter set in the file
			if(v.size() == 9) {
				mKalmanRssProcessNoise_var = atof(v[0].c_str());
				mKalmanRssMeasurementNoise_var = atof(v[1].c_str());
				mKalmanRssCovCoeff_pos_min = atof(v[2].c_str());
				mKalmanRssCovCoeff_accel_min = atof(v[3].c_str());
				mKalmanRssMaxStdAllowed = atof(v[4].c_str());

				mKalmanPosProcessNoise_var = atof(v[5].c_str());
				mKalmanPosMeasurementNoise_var = atof(v[6].c_str());
				mKalmanPosCovCoeff_pos_min = atof(v[7].c_str());
				mKalmanPosCovCoeff_accel_min = atof(v[8].c_str());

				isSuccessful = true;
			} else {
				// -- skip
			}
        }

    }

    return isSuccessful;
}

// -- Read config file
bool WiFi_Shopper_Tracker::ParseSetting(string configFile) {
	ParseConfigTxt config(configFile);

	int paramReadCnt = 0;

	if(config.getValue_string("VERSION", mSoftwareVersion)) { paramReadCnt++; printf("VERSION = %s\n", mSoftwareVersion.c_str()); }

	// -- Debug Mode Selection
	if(config.getValue_bool("CREATE_MEASUREMENT_LOGFILE", mCreateMeasLog)) { paramReadCnt++;	printf("CREATE_MEASUREMENT_LOGFILE = %d\n", (int)mCreateMeasLog); }

	// -- Select what to track
	if(config.getValue_bool("ONLY_FOR_DEVICES_OF_INTEREST", mOnlyDevOfInterest)) { paramReadCnt++;	printf("ONLY_FOR_DEVICES_OF_INTEREST = %d\n", (int)mOnlyDevOfInterest); }
	if(config.getValue_bool("TRACK_AP", mTrackAP)) { paramReadCnt++;	printf("TRACK_AP = %d\n", (int)mTrackAP); }
	if(config.getValue_bool("TRACK_SERVICE_AGENT", mTrackServiceAgent)) { paramReadCnt++;	printf("TRACK_SERVICE_AGENT = %d\n", (int)mTrackServiceAgent); }
	if(config.getValue_bool("TRACK_NULL_DEVICE", mTrackNullDev)) { paramReadCnt++;	printf("TRACK_NULL_DEVICE = %d\n", (int)mTrackNullDev); }

	// -- Select what to show
	if(config.getValue_bool("SHOW_NO_APS", mShowNoAPs)) { paramReadCnt++;	printf("SHOW_NO_APS = %d\n", (int)mShowNoAPs); }
	if(config.getValue_bool("SHOW_NO_SERVICE_AGENT", mShowNoServiceAgent)) { paramReadCnt++;	printf("SHOW_NO_SERVICE_AGENT = %d\n", (int)mShowNoServiceAgent); }
	if(config.getValue_bool("SHOW_RSS_MEASUREMENTS", mShowRSSMeas)) { paramReadCnt++;	printf("SHOW_RSS_MEASUREMENTS = %d\n", (int)mShowRSSMeas); }
	if(config.getValue_bool("SHOW_LOCALIZATION_POLYGON", mShowLocalizationPolygon)) { paramReadCnt++;	printf("SHOW_LOCALIZATION_POLYGON = %d\n", (int)mShowLocalizationPolygon); }
	if(config.getValue_bool("SHOW_TRAJECTORY", mShowTrajectory)) { paramReadCnt++;	printf("SHOW_TRAJECTORY = %d\n", (int)mShowTrajectory); }
		if(config.getValue_bool("SHOW_TRAJECTORY_ONLY_DevOfInt", mShowTrajectory_DevOfInt)) { paramReadCnt++;	printf("SHOW_TRAJECTORY_ONLY_DevOfInt = %d\n", (int)mShowTrajectory_DevOfInt); }
		if(config.getValue_bool("SHOW_SELECTED_SENSOR_CENTROID", mShowSelectedSensorCentroid)) { paramReadCnt++;	printf("SHOW_SELECTED_SENSOR_CENTROID = %d\n", (int)mShowSelectedSensorCentroid); }

	if(config.getValue_bool("SHOW_RAW_MESSAGE", mShowRawMsg)) { paramReadCnt++;	printf("SHOW_RAW_MESSAGE = %d\n", (int)mShowRawMsg); }
	if(config.getValue_bool("SHOW_PARSED_MESSAGE", mShowParsedMsg)) { paramReadCnt++;	printf("SHOW_PARSED_MESSAGE = %d\n", (int)mShowParsedMsg); }

	// -- Shopper-related parameters
	if(config.getValue_int("MAX_FOUND_DEVICES", mFoundDevices_max)) { paramReadCnt++;	printf("MAX_FOUND_DEVICES = %d\n", mFoundDevices_max); }
	if(config.getValue_int("MAX_MEASUREMENT_BUFFER_PER_DEV", mMeasBufferPerDev_max)) { paramReadCnt++;	printf("MAX_MEASUREMENT_BUFFER_PER_DEV = %d\n", mMeasBufferPerDev_max); }
	if(config.getValue_int("TIME_THRESHOLD_DEVICE_DISAPPEAR", mTimeThresholdForDevAbsency)) { paramReadCnt++;	printf("TIME_THRESHOLD_DEVICE_DISAPPEAR = %d\n", mTimeThresholdForDevAbsency); }
	if(config.getValue_int("MIN_SHOPPING_TIME", mShoppingTime_min)) { paramReadCnt++;	printf("MIN_SHOPPING_TIME = %d\n", mShoppingTime_min); }
	if(config.getValue_int("MAX_SHOPPING_TIME", mShoppingTime_max)) { paramReadCnt++;	printf("MAX_SHOPPING_TIME = %d\n", mShoppingTime_max); }
	if(config.getValue_int("MIN_SHOPPING_DISTANCE", mShoppingDistance_min)) { paramReadCnt++;	printf("MIN_SHOPPING_DISTANCE = %d\n", mShoppingDistance_min); }
	if(config.getValue_int("MIN_SHOPPER_DETECTION", mShopperDetection_min)) { paramReadCnt++;	printf("MIN_SHOPPER_DETECTION = %d\n", mShopperDetection_min); }
	if(config.getValue_int("MAX_TRACKED_POINTS", mTrackedPoints_max)) { paramReadCnt++;	printf("MAX_TRACKED_POINTS = %d\n", mTrackedPoints_max); }
	if(config.getValue_int("MIN_TRAJECTORY_POINTS_TO_REPORT", mTrackedPointsSizeToReport_min)) { paramReadCnt++;	printf("MIN_TRAJECTORY_POINTS_TO_REPORT = %d\n", mTrackedPointsSizeToReport_min); }
	if(config.getValue_float("TRAJECTORY_DIST_PRECISION_WHEN_REPORTING", mTrajectoryPrecision_dist)) { paramReadCnt++;	printf("TRAJECTORY_DIST_PRECISION_WHEN_REPORTING = %.2f\n", mTrajectoryPrecision_dist); }
	if(config.getValue_float("TRAJECTORY_TIME_PRECISION_WHEN_REPORTING", mTrajectoryPrecision_time)) { paramReadCnt++;	printf("TRAJECTORY_TIME_PRECISION_WHEN_REPORTING = %.2f\n", mTrajectoryPrecision_time); }


	if(config.getValue_bool("CHANNEL_SEPARATION", mChannelSeperation)) { paramReadCnt++;	printf("CHANNEL_SEPARATION = %d\n", mChannelSeperation); }
	if(config.getValue_int("MAX_CALIB_MEAS_WAIT_PERIOD", mCalibMeasWaitPeriod_max)) { paramReadCnt++;	printf("MAX_CALIB_MEAS_WAIT_PERIOD = %d\n", mCalibMeasWaitPeriod_max); }
	if(config.getValue_int("MAX_CALIB_PERIOD", mCalibPeriod_max)) { paramReadCnt++;	printf("MAX_CALIB_PERIOD = %d\n", mCalibPeriod_max); }
	if(config.getValue_int("MIN_CALIB_START_WAIT_PERIOD", mCalibStartWaitPeriod_min)) { paramReadCnt++;	printf("MIN_CALIB_START_WAIT_PERIOD = %d\n", mCalibStartWaitPeriod_min); }
	if(config.getValue_int("TIME_THRESHOLD_SERVICE_AGENT_DEVICE", mTimeThreshold_ServiceAgent)) { paramReadCnt++;	printf("TIME_THRESHOLD_SERVICE_AGENT_DEVICE = %d\n", mTimeThreshold_ServiceAgent); }
	if(config.getValue_int("MAX_SERVICE_AGENT_LIST_SIZE", mServiceAgentListSize_max)) { paramReadCnt++;	printf("MAX_SERVICE_AGENT_LIST_SIZE = %d\n", mServiceAgentListSize_max); }
	if(config.getValue_int("MAX_AP_LIST_SIZE", mApListSize_max)) { paramReadCnt++;	printf("MAX_AP_LIST_SIZE = %d\n", mApListSize_max); }
	if(config.getValue_int("MAX_ACCURACY_STAT_HISTORY", mStatAccuracySize_max)) { paramReadCnt++;	printf("MAX_ACCURACY_STAT_HISTORY = %d\n", mStatAccuracySize_max); }
	if(config.getValue_int("AP_MAX_RELIABLE_DISTANCE", mReliableDistanceInMeas_max)) { paramReadCnt++;	printf("AP_MAX_RELIABLE_DISTANCE = %d\n", mReliableDistanceInMeas_max); }


	// -- Localization parameters
	if(config.getValue_double("PERIODIC_BUFFER_PROCESSING_INTERVAL", mPeriodicBufferProcessingInterval)) {
		paramReadCnt++;
		mKalmanMeasurementLifetimeForTracking = mPeriodicBufferProcessingInterval*1.99;
		printf("PERIODIC_BUFFER_PROCESSING_INTERVAL = %.2f\n", mPeriodicBufferProcessingInterval);
	}

	//if(config.getValue_bool("REPORT_REAL_TIME_TRACK_RESULTS", mReportRealTimeTrackResult)) { paramReadCnt++;	printf("REPORT_REAL_TIME_TRACK_RESULTS = %d\n", (int)mReportRealTimeTrackResult); }
	if(config.getValue_int("MEAN_SHIFT_MAX_RADIUS_EXPANSION_COUNT", mMeanShiftRadiusExpansionCnt_max)) { paramReadCnt++;	printf("MEAN_SHIFT_MAX_RADIUS_EXPANSION_COUNT = %d\n", mMeanShiftRadiusExpansionCnt_max); }
	if(config.getValue_int("MIN_NUM_MEASUREMENTS_TO_DO_LOCALIZATION", mMinNumMeasToDoLocalization)) { paramReadCnt++;	printf("MIN_NUM_MEASUREMENTS_TO_DO_LOCALIZATION = %d\n", mMinNumMeasToDoLocalization); }
	if(config.getValue_int("MAX_NUM_SENSORS_TO_CHOOSE_FOR_LOCALIZATION", mNumSensorForLocalization_max)) { paramReadCnt++;	printf("MAX_NUM_SENSORS_TO_CHOOSE_FOR_LOCALIZATION = %d\n", mNumSensorForLocalization_max); }
	if(config.getValue_int("MIN_NUM_SENSORS_TO_CHOOSE_FOR_LOCALIZATION", mNumSensorForLocalization_min)) { paramReadCnt++;	printf("MIN_NUM_SENSORS_TO_CHOOSE_FOR_LOCALIZATION = %d\n", mNumSensorForLocalization_min); }
	if(config.getValue_int("MAX_NUM_SENSORS_TO_USE_FOR_MEANSHIFT_SENSOR_SELECTION", mNumSensorForSelection_max)) { paramReadCnt++;	printf("MAX_NUM_SENSORS_TO_USE_FOR_MEANSHIFT_SENSOR_SELECTION = %d\n", mNumSensorForSelection_max); }
	if(config.getValue_int("MIN_NUM_SENSORS_TO_USE_FOR_MEANSHIFT_SENSOR_SELECTION", mNumSensorForSelection_min)) { paramReadCnt++;	printf("MIN_NUM_SENSORS_TO_USE_FOR_MEANSHIFT_SENSOR_SELECTION = %d\n", mNumSensorForSelection_min); }
	if(config.getValue_double("MAX_AVG_SQUARED_ERROR_DISTANCE_IN_LOCALIZATION", mLocalizationMaxAvgSqrErrorDistance)) { paramReadCnt++;	printf("MAX_AVG_SQUARED_ERROR_DISTANCE_IN_LOCALIZATION = %f\n", mLocalizationMaxAvgSqrErrorDistance); }
	if(config.getValue_int("LOCALIZATION_ALGORITHM_TYPE", mLocalizationAlgorithmType)) { paramReadCnt++;	printf("LOCALIZATION_ALGORITHM_TYPE = %d\n", mLocalizationAlgorithmType); }
	if(config.getValue_int("LOCALIZATION_BOUNDINGBOX_MARGIN", mLocalizationBoundingBoxMargin)) { paramReadCnt++;	printf("LOCALIZATION_BOUNDINGBOX_MARGIN = %d\n", mLocalizationBoundingBoxMargin); }
	if(config.getValue_int("LOCALIZATION_MAX_DISTANCE_FROM_CONVEXHULL", mLocalizationMaxDistFromConvexHull)) { paramReadCnt++;	printf("LOCALIZATION_MAX_DISTANCE_FROM_CONVEXHULL = %d\n", mLocalizationMaxDistFromConvexHull); }
	if(config.getValue_int("MAX_DIST_FOR_NEARBY_NEIGHBORS", mMaxDistForNearbyNeighbors)) { paramReadCnt++;	printf("MAX_DIST_FOR_NEARBY_NEIGHBORS = %d\n", mMaxDistForNearbyNeighbors); }

	// -- Measurement pre-processing
	if(config.getValue_bool("MEASUREMENT_ADJUSTMENT_ENABLE", mMeasurementAdjustmentEnable)) { paramReadCnt++;	printf("MEASUREMENT_ADJUSTMENT_ENABLE = %d\n", (int)mMeasurementAdjustmentEnable); }

	// -- Kalman filter parameters for RSS estimation
	if(config.getValue_bool("KALMAN_RSS_ENABLE", mKalmanRssEnable)) { paramReadCnt++;	printf("KALMAN_RSS_ENABLE = %d\n", (int)mKalmanRssEnable); }
//	if(config.getValue_double("KALMAN_RSS_PROCESS_NOISE_VARIANCE", mKalmanRssProcessNoise_var)) { paramReadCnt++;	printf("KALMAN_RSS_PROCESS_NOISE_VARIANCE = %f\n", mKalmanRssProcessNoise_var); }
//	if(config.getValue_double("KALMAN_RSS_MEASUREMENT_NOISE_VARIANCE", mKalmanRssMeasurementNoise_var)) { paramReadCnt++;	printf("KALMAN_RSS_MEASUREMENT_NOISE_VARIANCE = %f\n", mKalmanRssMeasurementNoise_var); }
//	if(config.getValue_double("KALMAN_RSS_MIN_COV_COEFFICENT_POS", mKalmanRssCovCoeff_pos_min)) { paramReadCnt++;	printf("KALMAN_RSS_MIN_COV_COEFFICENT_POS = %f\n", mKalmanRssCovCoeff_pos_min); }
//	if(config.getValue_double("KALMAN_RSS_MIN_COV_COEFFICENT_ACCEL", mKalmanRssCovCoeff_accel_min)) { paramReadCnt++;	printf("KALMAN_RSS_MIN_COV_COEFFICENT_ACCEL = %f\n", mKalmanRssCovCoeff_accel_min); }
//	if(config.getValue_double("KALMAN_RSS_MAX_STD_ALLOWED", mKalmanRssMaxStdAllowed)) { paramReadCnt++;	printf("KALMAN_RSS_MAX_STD_ALLOWED = %f\n", mKalmanRssMaxStdAllowed); }
	if(config.getValue_double("KALMAN_RSS_TIME_SCALE", mKalmanTimeScale_rss)) { paramReadCnt++;	printf("KALMAN_RSS_TIME_SCALE = %f\n", mKalmanTimeScale_rss); }

	// -- Kalman filter parameters for Location estimation
	if(config.getValue_bool("KALMAN_POS_ENABLE", mKalmanPosEnable)) { paramReadCnt++;	printf("KALMAN_POS_ENABLE = %d\n", (int)mKalmanPosEnable); }
//	if(config.getValue_double("KALMAN_POS_PROCESS_NOISE_VARIANCE", mKalmanPosProcessNoise_var)) { paramReadCnt++;	printf("KALMAN_POS_PROCESS_NOISE_VARIANCE = %f\n", mKalmanPosProcessNoise_var); }
//	if(config.getValue_double("KALMAN_POS_MEASUREMENT_NOISE_VARIANCE", mKalmanPosMeasurementNoise_var)) { paramReadCnt++;	printf("KALMAN_POS_MEASUREMENT_NOISE_VARIANCE = %f\n", mKalmanPosMeasurementNoise_var); }
//	if(config.getValue_double("KALMAN_POS_MIN_COV_COEFFICENT_POS", mKalmanPosCovCoeff_pos_min)) { paramReadCnt++;	printf("KALMAN_POS_MIN_COV_COEFFICENT_POS = %f\n", mKalmanPosCovCoeff_pos_min); }
//	if(config.getValue_double("KALMAN_POS_MIN_COV_COEFFICENT_ACCEL", mKalmanPosCovCoeff_accel_min)) { paramReadCnt++;	printf("KALMAN_POS_MIN_COV_COEFFICENT_ACCEL = %f\n", mKalmanPosCovCoeff_accel_min); }
	if(config.getValue_double("KALMAN_POS_TIME_SCALE", mKalmanTimeScale_pos)) { paramReadCnt++;	printf("KALMAN_POS_TIME_SCALE = %f\n", mKalmanTimeScale_pos); }
	if(config.getValue_bool("KALMAN_POS_FEEDBACK_TO_RSS_ENABLE", mKalmanPosFeedbackToRssEnable)) { paramReadCnt++;	printf("KALMAN_POS_FEEDBACK_TO_RSS_ENABLE = %d\n", (int)mKalmanPosFeedbackToRssEnable); }
	if(config.getValue_double("KALMAN_POS_FEEDBACK_TO_RSS_NOSIE_SCALE", mKalmanPosFeedbackToRssNoiseScale)) { paramReadCnt++;	printf("KALMAN_POS_FEEDBACK_TO_RSS_NOSIE_SCALE = %f\n", mKalmanPosFeedbackToRssNoiseScale); }

	// -- Kalman filter general parameters
	if(config.getValue_int("KALMAN_ESTIMATE_VISUALIZATION_TIME_THRESHOLD", mKalmanEstimateVisualizationTimeout)) { paramReadCnt++;	printf("KALMAN_ESTIMATE_VISUALIZATION_TIME_THRESHOLD = %d\n", mKalmanEstimateVisualizationTimeout); }
//	if(config.getValue_double("KALMAN_MEASUREMENT_LIFETIME_FOR_TRACKING", mKalmanMeasurementLifetimeForTracking)) { paramReadCnt++;	printf("KALMAN_MEASUREMENT_LIFETIME_FOR_TRACKING = %f\n", mKalmanMeasurementLifetimeForTracking); }
	if(config.getValue_double("KALMAN_INITIALIZATION_TIME_THRESHOLD", mKalmanInitThreshold_time)) { paramReadCnt++;	printf("KALMAN_INITIALIZATION_TIME_THRESHOLD = %f\n", mKalmanInitThreshold_time); }
	if(config.getValue_double("KALMAN_INITIALIZATION_DIST_THRESHOLD", mKalmanInitThreshold_dist)) { paramReadCnt++;	printf("KALMAN_INITIALIZATION_DIST_THRESHOLD = %f\n", mKalmanInitThreshold_dist); }

	// -- Store-specific parameters
	string monitoringAP_IPsFileName;
	if(config.getValue_string("MONITORING_AP_IP_ADDR_LIST_FILENAME", monitoringAP_IPsFileName)) {
		paramReadCnt++;
		mMonitoringAP_IPsFilePath << APP_CONFIG_ROOT_PATH << "/" << WiFiTracker_APP_NAME << "/" << APP_STORE_ROOT_PATH << "/" << mAppStoreName << "/" << monitoringAP_IPsFileName;
		printf("MONITORING_AP_IP_ADDR_LIST_FILENAME = %s\n", mMonitoringAP_IPsFilePath.str().c_str());
	}

	string knownAPsFilename;
	if(config.getValue_string("KNOWN_AP_LIST_FILENAME", knownAPsFilename)) {
		paramReadCnt++;
		mKnownApListFilePath << APP_CONFIG_ROOT_PATH << "/" << WiFiTracker_APP_NAME << "/" << APP_STORE_ROOT_PATH << "/" << mAppStoreName << "/" << knownAPsFilename;
		printf("KNOWN_AP_LIST_FILENAME = %s\n", mKnownApListFilePath.str().c_str());
	}

	string knownServiceAgentsFilename;
	if(config.getValue_string("KNOWN_SERVICE_AGENT_LIST_FILENAME", knownServiceAgentsFilename)) {
		paramReadCnt++;
		mKnownServiceAgentFilePath << APP_CONFIG_ROOT_PATH << "/" << WiFiTracker_APP_NAME << "/" << APP_STORE_ROOT_PATH << "/" << mAppStoreName << "/" << knownServiceAgentsFilename;
		printf("KNOWN_SERVICE_AGENT_LIST_FILENAME = %s\n", mKnownServiceAgentFilePath.str().c_str());
	}

		// -- GUI parameters
		if(config.getValue_string("IMG_MAP_NAME", mImgMapName)) { paramReadCnt++;	printf("IMG_MAP_NAME = %s\n", mImgMapName.c_str()); }
		if(config.getValue_string("STORE_LAYOUT_FILENAME", mStoreLayoutFilename)) { paramReadCnt++;	printf("STORE_LAYOUT_FILENAME = %s\n", mStoreLayoutFilename.c_str()); }

		if(config.getValue_int("STORE_LAYOUT_ORIGINAL_IMAGE_X", mStoreLayoutOriginalImgSize_x)) { paramReadCnt++;	printf("STORE_LAYOUT_ORIGINAL_IMAGE_X = %d\n", mStoreLayoutOriginalImgSize_x); }
		if(config.getValue_int("STORE_LAYOUT_ORIGINAL_IMAGE_Y", mStoreLayoutOriginalImgSize_y)) { paramReadCnt++;	printf("STORE_LAYOUT_ORIGINAL_IMAGE_Y = %d\n", mStoreLayoutOriginalImgSize_y); }
		if(config.getValue_double("STORE_LAYOUT_PIXEL_PER_FT", mStoreLayoutPixelPerFt)) { paramReadCnt++;	printf("STORE_LAYOUT_PIXEL_PER_FT = %.2f\n", mStoreLayoutPixelPerFt); }

		if(config.getValue_int("STORE_WIDTH_IMG_x_MAX", mStoreWidth_img_x_max) && config.getValue_int("STORE_WIDTH_IMG_y_MAX", mStoreWidth_img_y_max)) {
			paramReadCnt += 2;

			printf("(STORE_WIDTH_IMG_x_MAX, STORE_WIDTH_IMG_y_MAX) = (%d, %d)\n", mStoreWidth_img_x_max, mStoreWidth_img_y_max);

			mStoreWidth_PHY_x = (mStoreLayoutOriginalImgSize_x / mStoreLayoutPixelPerFt)*30.48;		// -- Pixel to ft to cm
			mStoreWidth_PHY_y = (mStoreLayoutOriginalImgSize_y / mStoreLayoutPixelPerFt)*30.48;		// -- Pixel to ft to cm
		}

		if(config.getValue_int("IN_STORE_MAP_BOUNDINGBOX_MARGIN", mInStoreMapBBMargin)) { paramReadCnt++;	printf("IN_STORE_MAP_BOUNDINGBOX_MARGIN = %d\n", mInStoreMapBBMargin); }

		// -- Periodic processing intervals
		if(config.getValue_float("PERIODIC_MAP_REFRESH_INTERVAL", mPeriodicInterval_GuiMapRefresh)) { paramReadCnt++;	printf("PERIODIC_MAP_REFRESH_INTERVAL = %.2f\n", mPeriodicInterval_GuiMapRefresh); }
		if(config.getValue_float("PERIODIC_REPORT_TRACKING_INTERVAL", mPeriodicInterval_TrackingReport)) { paramReadCnt++;	printf("PERIODIC_REPORT_TRACKING_INTERVAL = %.2f\n", mPeriodicInterval_TrackingReport); }
		if(config.getValue_int("PERIODIC_CLEAR_UP_DEVICE_INTERVAL", mPeriodicInterval_DevClearUp)) { paramReadCnt++;	printf("PERIODIC_CLEAR_UP_DEVICE_INTERVAL = %d\n", mPeriodicInterval_DevClearUp); }

		// -- Calibration parameters
		if(config.getValue_bool("WIFI_CALIB_AUTOMATIC_BEST_RSS_OFFSET_DISCOVERY", mFindBestRssOffset)) { paramReadCnt++;	printf("WIFI_CALIB_AUTOMATIC_BEST_RSS_OFFSET_DISCOVERY = %d\n", mFindBestRssOffset); }
		if(config.getValue_int("WIFI_CALIB_AUTOMATIC_BEST_RSS_OFFSET_DISCOVERY_MIN_RSS_RANGE", mFindBestRssOffsetMinRss)) { paramReadCnt++;	printf("WIFI_CALIB_AUTOMATIC_BEST_RSS_OFFSET_DISCOVERY_MIN_RSS_RANGE = %d\n", mFindBestRssOffsetMinRss); }
		if(config.getValue_int("WIFI_CALIB_AUTOMATIC_BEST_RSS_OFFSET_DISCOVERY_MAX_RSS_RANGE", mFindBestRssOffsetMaxRss)) { paramReadCnt++;	printf("WIFI_CALIB_AUTOMATIC_BEST_RSS_OFFSET_DISCOVERY_MAX_RSS_RANGE = %d\n", mFindBestRssOffsetMaxRss); }
		if(config.getValue_bool("WIFI_CALIB_AUTOMATIC_PER_DEV_RSS_OFFSET_ADJUSTMENT", mAutomaticPerDevRssOffsetAdjustment)) { paramReadCnt++;	printf("WIFI_CALIB_AUTOMATIC_PER_DEV_RSS_OFFSET_ADJUSTMENT = %d\n", mAutomaticPerDevRssOffsetAdjustment); }

		ostringstream calibfilename;
		string calibfolder;
		if(config.getValue_string("CALIB_PARAM_FILE_PATH", calibfolder)) {
			printf("CALIB_PARAM_FILE_PATH = %s\n", calibfolder.c_str());

			mCalibFilePath << APP_CONFIG_ROOT_PATH << "/" << WiFiTracker_APP_NAME << "/" << APP_STORE_ROOT_PATH << "/" << mAppStoreName << "/" << calibfolder;
			ostringstream cmd; cmd << "mkdir " << mCalibFilePath.str(); system(cmd.str().c_str());	// -- create calib folder
			paramReadCnt++;
		}

		string calibfilenameprefix;
		if(config.getValue_string("CALIB_PARAM_FILE_NAME_PREFIX", calibfilenameprefix)) {
			printf("CALIB_PARAM_FILE_NAME_PREFIX = %s\n", calibfilenameprefix.c_str());

			mCalibFilePathPrefixFull << mCalibFilePath.str() << "/" << calibfilenameprefix;
			paramReadCnt++;
		}
		if(config.getValue_int("CALIB_MAX_RANSAC_TRY", mCalibRansacTry_max)) { paramReadCnt++; printf("CALIB_MAX_RANSAC_TRY = %d\n", mCalibRansacTry_max); }
		if(config.getValue_int("CALIB_MAX_NUM_SAMPLE_DISTANCE_PER_BIN", mCalibSampleDistPerBin_max)) { paramReadCnt++; printf("CALIB_MAX_NUM_SAMPLE_DISTANCE_PER_BIN = %d\n", mCalibSampleDistPerBin_max); }
		if(config.getValue_int("CALIB_MIN_NUM_MEASUREMENT_PER_BIN", mCalibMeasPerBin_min)) { paramReadCnt++; printf("CALIB_MIN_NUM_MEASUREMENT_PER_BIN = %d\n", mCalibMeasPerBin_min); }
		if(config.getValue_int("CALIB_MAX_NUM_MEASUREMENT_PER_BIN", mCalibMeasPerBin_max)) { paramReadCnt++; printf("CALIB_MAX_NUM_MEASUREMENT_PER_BIN = %d\n", mCalibMeasPerBin_max); }
		if(config.getValue_int("CALIB_MIN_DATA_SAMPLES_FOR_LEAST_SQUARE", mCalibDataSamplesForLeastSquare_min)) { paramReadCnt++; printf("CALIB_MIN_DATA_SAMPLES_FOR_LEAST_SQUARE = %d\n", mCalibDataSamplesForLeastSquare_min); }
		if(config.getValue_int("CALIB_FAIL_CNT_MAX_ALLOWED", mCalibFailCntAllowed_max)) { paramReadCnt++; printf("CALIB_FAIL_CNT_MAX_ALLOWED = %d\n", mCalibFailCntAllowed_max); }

		// -- Kalman filter parameters for RSS estimation
		if(config.getValue_double("KALMAN_CALIB_RSS_PROCESS_NOISE_VARIANCE", mKalmanCalibRssProcessNoise_var)) { paramReadCnt++;	printf("KALMAN_CALIB_RSS_PROCESS_NOISE_VARIANCE = %f\n", mKalmanCalibRssProcessNoise_var); }
		if(config.getValue_double("KALMAN_CALIB_RSS_MEASUREMENT_NOISE_VARIANCE", mKalmanCalibRssMeasurementNoise_var)) { paramReadCnt++;	printf("KALMAN_CALIB_RSS_MEASUREMENT_NOISE_VARIANCE = %f\n", mKalmanCalibRssMeasurementNoise_var); }
		if(config.getValue_double("KALMAN_CALIB_RSS_MIN_COV_COEFFICENT_POS", mKalmanCalibRssCovCoeff_pos_min)) { paramReadCnt++;	printf("KALMAN_CALIB_RSS_MIN_COV_COEFFICENT_POS = %f\n", mKalmanCalibRssCovCoeff_pos_min); }
		if(config.getValue_double("KALMAN_CALIB_RSS_MIN_COV_COEFFICENT_ACCEL", mKalmanCalibRssCovCoeff_accel_min)) { paramReadCnt++;	printf("KALMAN_CALIB_RSS_MIN_COV_COEFFICENT_ACCEL = %f\n", mKalmanCalibRssCovCoeff_accel_min); }

		// -- IoT parameters
		if(config.getValue_int("IOT_PERIODIC_STATE_REPORTING_INTERVAL", mPeriodicStateReportingInterval)) { paramReadCnt++; printf("PERIODIC_STATE_REPORTING_INTERVAL = %d\n", mPeriodicStateReportingInterval); }


	bool ret = false;
	if(paramReadCnt != (int)config.paramSize()) {
		fprintf(stderr, "!! paramReadCnt[%d] != config.paramSize() [%d] !!\n", paramReadCnt, config.paramSize());
		ret = false;
	} else
		ret = true;

	mCalibEnoughMeasBinsToKickOffCalib_min = (int)(CALIB_NUM_DISTANCE_BINS*0.5);

	return ret;

}

void WiFi_Shopper_Tracker::calculateTrackStats()
{
	mTrackerState.noTrackedShoppers = 0;
	mTrackerState.noTrackedShoppersWithinArea = 0;
	mTrackerState.noTrackedAPs = 0;
	mTrackerState.noTrackedServiceAgent = 0;
	mTrackerState.noTrackedDevs = 0;
	mTrackerState.noTrackedDevs_NULL = 0;

	mTrackerState.maxBufferSize_dev = 0;
	mTrackerState.avgBufferSize_dev = 0;

	mTrackerState.runngingTime_in_min = (int)(gTimeSinceStart() / 60.f);

	pthread_mutex_lock(&gMutex);
	for(deque<RemoteDevicePtr>::iterator it = mFoundDevice.begin(); it != mFoundDevice.end();++it)
	{
		RemoteDevicePtr dev = *it;
		if(dev == NULL)
			continue;

			mTrackerState.avgBufferSize_dev += dev->GetMeasBufSize();
			mTrackerState.maxBufferSize_dev = mTrackerState.maxBufferSize_dev > dev->GetMeasBufSize() ? mTrackerState.maxBufferSize_dev : dev->GetMeasBufSize();

			mTrackerState.noTrackedDevs++;

			if(dev->IsDeviceBrandNull() == true)
				mTrackerState.noTrackedDevs_NULL++;

			mTrackerState.maxTrackedDevs = MAX(mTrackerState.maxTrackedDevs, mTrackerState.noTrackedDevs);

			if(dev->GetDeviceType() == AP) {
				mTrackerState.noTrackedAPs++;

				if(mShowNoAPs) {
					//mTrackState.noTrackedServiceAgent = mKnownAPs.size();
					continue;
				}

			} else if(dev->GetDeviceType() == ServiceAgent) {
				mTrackerState.noTrackedServiceAgent++;

				if(mShowNoServiceAgent) {
					//mTrackState.noTrackedServiceAgent = mKnownServiceAgents.size();
					continue;
				}

			} else if(dev->GetTrackedPosBufSize() > 0 && dev->mKalmanPos_m.GetKalmanAge() > 0) {
				mTrackerState.noTrackedShoppers++;

				if(dev->GetTotalLocalizationDuringShopping() > 0)
					mTrackerState.noTrackedShoppersWithinArea++;
			}

			if(dev->GetTrackedPosBufSize() > 0) {
				// -- Calculate how long this device stayed inside the store
				double inStoreTime_begin = 0, inStoreTime_end = 0;

				// -- Count how many localization we got during shopping
				int totalLocalizationDuringShopping = 0;

				// -- Calculate the total shopping travel distance by the shopper
				double totalTravelDistance = 0;
				Point2i loc_prev, loc_curr;

//					float vertx[mRunningWiFiSensor.size()];
//					float verty[mRunningWiFiSensor.size()];
//					int ind = 0;
//					for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it) {
//						WiFiSensor_short_Ptr sensr = *it;
//						vertx[ind] = sensr->mSensorPos_x;
//						verty[ind] = sensr->mSensorPos_y;
//						ind++;
//					}

				// -- Go through from the end of the list since the end is the beginning of the trajectory.
				for(deque<TrackedPos_t_>::iterator it = dev->AtTrackedPosBuf()->end() - 1; it >= dev->AtTrackedPosBuf()->begin(); --it) {
					TrackedPos_t_ loc = *it;

					// -- Insert the latest location as well if it is not too far from the BoundingBox of the participating nodes
//					Point2D32f closestPtInConvexHull;
//					bool isInConvexHull = isInsideBoundingBox(ind, vertx, verty, mInStoreMapBBMargin, (float)pt.x, (float)pt.y, closestPtInConvexHull.x, closestPtInConvexHull.y);

					if(distToInStore(loc.pos.x, loc.pos.y) == 0) {

						if(totalLocalizationDuringShopping == 0) {
							loc_prev = Point2i(loc.pos.x, loc.pos.y);
							inStoreTime_begin = loc.localtime;
						}

						inStoreTime_end = loc.localtime;
						totalLocalizationDuringShopping++;

						loc_curr = Point2i(loc.pos.x, loc.pos.y);
						double dist = sqrt(pow(loc_prev.x - loc_curr.x,2) + pow(loc_prev.y - loc_curr.y,2));
						totalTravelDistance += dist;
					}
				}

				double totalShoppingDuration = inStoreTime_end - inStoreTime_begin;

				dev->SetTotalShoppingTime(totalShoppingDuration);
				dev->SetTotalLocalizationDuringShopping(totalLocalizationDuringShopping);
				dev->SetTotalShoppingTravelDist(totalTravelDistance);
			}
	}
	pthread_mutex_unlock(&gMutex);

	if(mTrackerState.noTrackedDevs > 0)
		mTrackerState.avgBufferSize_dev /= (float)mTrackerState.noTrackedDevs;

	//pthread_mutex_unlock(&gMutexx);
}

void WiFi_Shopper_Tracker::ReadMonitoringAPs_csv(string filepath)
{
	char buffer[MAX_BUFFER];

	FILE *fp_WiFi_Sensors;

	fp_WiFi_Sensors = fopen(filepath.c_str(), "r");

	if(fp_WiFi_Sensors == NULL) {
		fprintf(stderr, "[WiFi_Shopper_Tracker] Error opening MONITORING_AP_IP_ADDR_LIST_FILENAME [%s] file", filepath.c_str());
	} else {

        Point	SensorPos;

		string DeviceName, MacAddr, SN, IPaddr;
		double x_pos, y_pos, cam_height;//, rotation;

		// -- Read from file
		string lineInput;
		while(fgets(buffer, MAX_BUFFER, fp_WiFi_Sensors)) {	// -- Reads a line from the pipe and save it to buffer
			lineInput.clear();
			lineInput.append(buffer);

			// -- If there's any comment, then discard the rest of the line
			std::size_t found = lineInput.find_first_of("#");
			// -- If '#' is found, then discard the rest.
			if(found != string::npos)
				lineInput.resize(found);

			// -- To see if this is the first line. If so, skip it.
			if(lineInput.find(",serial_number,") != string::npos)
				continue;

			// -- Split the string into words
			if(lineInput.size() > 0) {

				//printf("[lineInput ] %s", lineInput.c_str());

				// -- Break the line input into words and save it into a word vector
				stringstream ss (lineInput);
				vector<string> v;
				string s;
				while(getline(ss, s, ','))
					v.push_back(s);

				if(v.size() >= 7) {
					// -- DeviceName, MacAddr, SN, IPaddr, SensorPos.x, SensorPos.y, rotation, cameras_height
					DeviceName = v[0];
					MacAddr = v[1];
					SN = v[2];
					IPaddr = v[3];

					x_pos = atof(v[4].c_str()) / mStoreLayoutPixelPerFt;		// -- [Unit: ft] Convert from pixel to ft
					y_pos = atof(v[5].c_str()) / mStoreLayoutPixelPerFt;		// -- [Unit: ft] Convert from pixel to ft
					//rotation = atof(v[6].c_str());
					cam_height =  atof(v[7].c_str());	// -- Unit: [ft]


					x_pos *= 30.48;			// -- [Unit: cm] Convert from ft to cm
					y_pos *= 30.48;			// -- [Unit: cm] Convert from ft to cm
					cam_height *= 30.48;	// -- [Unit: cm] Convert from ft to cm

					SensorPos.x = x_pos + 0.5f;
					SensorPos.y = y_pos + 0.5f;

                    if(mDebugDeviceStatusVerboseMode > 0)
                        printf("Location (%s, %s) [pixel] / pxPerFt(%.2f) * 30.48 --> (%d, %d) [cm]\n", v[4].c_str(), v[5].c_str(), mStoreLayoutPixelPerFt, SensorPos.x, SensorPos.y);

					WiFiSensor_short_Ptr os = make_shared<WiFiSensor_short>();

					os->initNode(DeviceName, IPaddr, "bond0", SN);
					os->setSensorPos(SensorPos.x, SensorPos.y, cam_height);
					os->time_MsglastReceived = gTimeSinceStart();

					// -- Set the calib param file path
					{
							ostringstream calibFilePathFullPerOS;

							calibFilePathFullPerOS << mCalibFilePathPrefixFull.str() << os->mDevName << ".txt";

							os->mCalibParamFilename = calibFilePathFullPerOS.str();

//                            if(mDebugDeviceStatusVerboseMode > 0)
//                                printf("Node[%s] CALIB_PARAM_FILE Full Path = %s\n", os->mDevName.c_str(), os->mCalibParamFilename.c_str());
					}

					mRunningWiFiSensor.push_back(os);

//					printf("AP[%s] is successfully added, located at (%d, %d): (# of WiFi_Sensors running = %d). Topic[%s] \n", IPaddr.c_str(), SensorPos.x, SensorPos.y, (int)mRunningWiFiSensor.size(), subTopic.c_str());

					// -- Wait a little
					usleep(183*100);
				}
			}

		}

		printf("# of running WiFi_Sensors = %d\n", (int)mRunningWiFiSensor.size());

		fclose(fp_WiFi_Sensors);
	}

	printf("mRunningAP.size() = %d \n", (int)mRunningWiFiSensor.size());
}

void WiFi_Shopper_Tracker::ReadMonitoringAPs_text(string filepath)
{
	char buffer[MAX_BUFFER];

	FILE *fp_WiFi_Sensors;

	fp_WiFi_Sensors = fopen(filepath.c_str(), "r");

	if(fp_WiFi_Sensors == NULL) {
		fprintf(stderr, "[WiFi_Shopper_Tracker] Error opening MONITORING_AP_IP_ADDR_LIST_FILENAME [%s] file", filepath.c_str());
	} else {

        Point	SensorPos;

		string DeviceName, IPaddr, NIC, SN;
		double x_pos, y_pos, cam_height;

		// -- Read from file
		string lineInput;
		while(fgets(buffer, MAX_BUFFER, fp_WiFi_Sensors)) {	// -- Reads a line from the pipe and save it to buffer
			lineInput.clear();
			lineInput.append(buffer);

			// -- If there's any comment, then discard the rest of the line
			std::size_t found = lineInput.find_first_of("#");
			// -- If '#' is found, then discard the rest.
			if(found != string::npos)
				lineInput.resize(found);

			if(lineInput.size() > 0) {

				//printf("[lineInput ] %s", lineInput.c_str());

				// -- Break the line input into words and save it into a word vector
				istringstream iss (lineInput);
				vector<string> v;
				string s;
				while(iss >> s)
					v.push_back(s);

				if(v.size() >= 6) {
					// -- DeviceName, IPaddr, SN, NIC, &SensorPos.x, &SensorPos.y, cam_height
					DeviceName = v[0];
					IPaddr = v[1];
					SN = v[2];
					NIC = v[3];

					x_pos = atof(v[4].c_str()) * 100;	// -- [Unit: cm] Convert from meter to cm
					y_pos = atof(v[5].c_str()) * 100; 	// -- [Unit: cm] Convert from meter to cm
					cam_height = atof(v[6].c_str()) * 100; 	// -- [Unit: cm] Convert from meter to cm

					SensorPos.x = x_pos + 0.5f;
					SensorPos.y = y_pos + 0.5f;

					WiFiSensor_short_Ptr os = make_shared<WiFiSensor_short>();

					os->initNode(DeviceName, IPaddr, NIC, SN);
					os->setSensorPos(SensorPos.x, SensorPos.y, cam_height);
					os->time_MsglastReceived = gTimeSinceStart();

					// -- Set the calib param file path
					{
							ostringstream calibFilePathFullPerOS;

							calibFilePathFullPerOS << mCalibFilePathPrefixFull.str() << os->mDevName << ".txt";

							os->mCalibParamFilename = calibFilePathFullPerOS.str();

							printf("Node[%s] CALIB_PARAM_FILE Full Path = %s\n", os->mDevName.c_str(), os->mCalibParamFilename.c_str());
					}

					mRunningWiFiSensor.push_back(os);

//					printf("AP[%s] (NIC:%s) is successfully added: (# of WiFi_Sensors running = %d). Topic[%s] \n", IPaddr.c_str(), NIC.c_str(), (int)mRunningWiFiSensor.size(), subTopic.c_str());

					// -- Wait a little
					usleep(183*100);
				}
			}

		}

		printf("# of running WiFi_Sensors = %d\n", (int)mRunningWiFiSensor.size());

		fclose(fp_WiFi_Sensors);
	}

	printf("mRunningAP.size() = %d \n", (int)mRunningWiFiSensor.size());
}

Point WiFi_Shopper_Tracker::ConvertPhyCoordToMapCoord(Point2f phyPt) {
    return Point(mPhyToMap_x_ratio*(phyPt.x - mPhyToMap_offset.x),  mPhyToMap_y_ratio*(phyPt.y - mPhyToMap_offset.y));
}

Point WiFi_Shopper_Tracker::ConvertPhyCoordToMapCoord(int phyX, int phyY) {
    return Point(mPhyToMap_x_ratio*(phyX - mPhyToMap_offset.x),  mPhyToMap_y_ratio*(phyY - mPhyToMap_offset.y));
}

Point2f WiFi_Shopper_Tracker::ConvertMapCoordToPhyCoord(Point2f imgPt) {
    return Point2f(imgPt.x/mPhyToMap_x_ratio + mPhyToMap_offset.x, imgPt.y/mPhyToMap_y_ratio + mPhyToMap_offset.y);
}

Point2f WiFi_Shopper_Tracker::ConvertMapCoordToPhyCoord(int imgX, int imgY) {
    return Point(imgX/mPhyToMap_x_ratio + mPhyToMap_offset.x, imgY/mPhyToMap_y_ratio + mPhyToMap_offset.y);
}

double WiFi_Shopper_Tracker::ConvertPhyDistanceToMapDistance(double phyDist) {
    return phyDist*mPhyToMap_x_ratio;
}

double WiFi_Shopper_Tracker::ConvertMapDistanceToPhyDistance(double imgDist) {
    return imgDist/mPhyToMap_x_ratio;
}

void WiFi_Shopper_Tracker::InitiateKillProcess() {

    pthread_mutex_lock(&gMutex);
    mKillRequestPending = true;
    pthread_mutex_unlock(&gMutex);

    for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it) {
        WiFiSensor_short_Ptr os = *it;
        if(os == NULL) continue;

        pthread_mutex_lock(&gMutex);
        os->mKillRequestPending = true;
        pthread_mutex_unlock(&gMutex);

//        if(os->mStreamProcessing_thread != 0)
//        	pthread_join(os->mStreamProcessing_thread, NULL);
    }
//    if(mPacketStreamParsing_thread != 0)
//    	pthread_join(mPacketStreamParsing_thread, NULL);
//
//    if(mWiFiCalibProcessing_thread != 0)
//    	pthread_join(mWiFiCalibProcessing_thread, NULL);
//
//    if(mPeriodicIoTProcessing_thread != 0)
//    	pthread_join(mPeriodicIoTProcessing_thread, NULL);
//
//    if(mPeriodicBufferProcessing_thread != 0)
//    	pthread_join(mPeriodicBufferProcessing_thread, NULL);

//    pthread_exit(NULL);

}

//void tempFunc(deque<int> &ii) {
//	ostringstream iistr;
//	// -- Go through from the end of the list since the end is the beginning of the trajectory.
//	for(deque<int>::iterator it = ii.end() - 1; it >= ii.begin();) {
//		int i = *it;
//
//		iistr << i << ", ";
//		// -- Chop the message if it gets too large to send to AWS IoT cloud
//		//if((int)traj.str().size() > MAX_AWS_IOT_DATA_MSG_SIZE*1000)
//		if((int)iistr.str().size() > 9) {		// -- TODO: TEMP
//			ii.erase(it);
//			break;
//		} else
//			it = ii.erase(it) - 1;
//	}
//
//	cout << "This round: " << iistr.str() << endl;
//
//	return;
//}


void WiFi_Shopper_Tracker::InitialMapGeneration(string storeFilename) {

    // -- Map Cropping and Resizing
    Mat img_src = imread(storeFilename);

    float x_ratio = img_src.size().width/(float)mStoreWidth_PHY_x;
    float y_ratio = img_src.size().height/(float)mStoreWidth_PHY_y;

    Mat img_src_resized, img_src_cropped;
    //resize(img_src, img_src_resized, cvSize((int)ceil(mStoreWidth_IMG_x), (int)ceil(mStoreWidth_IMG_y)));

    // -- Crop the image to show only the in-store region
    Point pt_lt_phy(numeric_limits<int>::max(),numeric_limits<int>::max());	// -- left-top point
    Point pt_rb_phy(numeric_limits<int>::min(),numeric_limits<int>::min());	// -- right-bottom point
    {
        float vertx, verty;
        for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it) {
            WiFiSensor_short_Ptr sensr = *it;

            // -- Physical location in [cm]
            vertx = sensr->mSensorPos_x;
            verty = sensr->mSensorPos_y;

            if(pt_lt_phy.x > vertx - mInStoreMapBBMargin) pt_lt_phy.x = MAX(0, vertx - mInStoreMapBBMargin);
            if(pt_rb_phy.x < vertx + mInStoreMapBBMargin) pt_rb_phy.x = MIN(mStoreWidth_PHY_x, vertx + mInStoreMapBBMargin);

            if(pt_lt_phy.y > verty - mInStoreMapBBMargin) pt_lt_phy.y = MAX(0, verty - mInStoreMapBBMargin);
            if(pt_rb_phy.y < verty + mInStoreMapBBMargin) pt_rb_phy.y = MIN(mStoreWidth_PHY_y, verty + mInStoreMapBBMargin);
        }
    }

//			printf("Physical Location: pt_lt = (%d, %d), pt_rb = (%d, %d) [cm]\n", pt_lt_phy.x, pt_lt_phy.y, pt_rb_phy.x, pt_rb_phy.y);

    // -- Convert physical coordinate to high-res layout image (i.e., img_src) coordinate
    Point pt_lt_img(pt_lt_phy.x*x_ratio, pt_lt_phy.y*y_ratio);
    Point pt_rb_img(pt_rb_phy.x*x_ratio, pt_rb_phy.y*y_ratio);

//			printf("IMG_SRC Image Location: pt_lt = (%d, %d), pt_rb = (%d, %d)\n", pt_lt_phy.x, pt_lt_phy.y, pt_rb_phy.x, pt_rb_phy.y);

    float newImgWidth = pt_rb_img.x - pt_lt_img.x;
    float newImgHeight = pt_rb_img.y - pt_lt_img.y;

    {
        int temp_y = (int)((newImgHeight/(float)newImgWidth)*(float)mStoreWidth_img_x_max);
        int temp_x = (int)((newImgWidth/(float)newImgHeight)*(float)mStoreWidth_img_y_max);
        if(temp_y <= mStoreWidth_img_y_max) {
            mStoreWidth_IMG_x = mStoreWidth_img_x_max;
            mStoreWidth_IMG_y = temp_y;
        } else {
            mStoreWidth_IMG_x = temp_x;
            mStoreWidth_IMG_y = mStoreWidth_img_y_max;
        }
    }

    mStoreMapRoiMask = Rect(pt_lt_img.x, pt_lt_img.y, pt_rb_img.x - pt_lt_img.x, pt_rb_img.y - pt_lt_img.y);
    img_src(mStoreMapRoiMask).copyTo(img_src_cropped);

    resize(img_src_cropped, mImg_map_init, cvSize((int)ceil(mStoreWidth_IMG_x), (int)ceil(mStoreWidth_IMG_y)));

    // -- This ratio and offset can be used to convert a physical coordinate to map coordinate
    mPhyToMap_x_ratio = x_ratio * mStoreWidth_IMG_x/(double)newImgWidth;
    mPhyToMap_y_ratio = y_ratio * mStoreWidth_IMG_y/(double)newImgHeight;
    mPhyToMap_offset = pt_lt_phy;


    // -- Calculate the coordinates of the sensors on the store layout/map
    for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it) {
        WiFiSensor_short_Ptr os = *it;

        Point mapPos = ConvertPhyCoordToMapCoord(os->mSensorPos_x, os->mSensorPos_y);

        os->mSensorPos_x_map = mapPos.x;
        os->mSensorPos_y_map = mapPos.y;
    }
}

bool WiFi_Shopper_Tracker::run(bool showGUI) {

//	deque<int> ii;
//	for(int i = 0; i<30; i++)
//		ii.push_back(i);
//
//	while((int)ii.size() > 0) {
//		tempFunc(ii);
//	}
//
//	cout << "END" <<endl;
//
//	return false;


	gSaveTime();

	mShowMapVisualization = showGUI;

	// -- Convert the store name from vmhq/16801 to vmhq-16801
	// -- Set Store Name: where to track shoppers. This must be consistent with the folder name under config/WiFi_Shopper_Tracker/Stores
	mAppStoreName = mIoTGateway.GetMyStoreName_dash();

	// -- Tracker ID should look like vmhq-16801-{tracker name in dev_name.txt}-{pid}.
	mTrackerID << mAppStoreName << "-" << mIoTGateway.GetMyDevName() << "-" << mTrackerID_suffix;

	// -- Create a folder to temporarily store any runtime metadata
	{
		ostringstream path_runtime, cmd; //,path_data;
		path_runtime << WiFiTracker_RUNTIME_ROOT_PATH;
		cmd.str(""); cmd << "mkdir " << path_runtime.str(); system(cmd.str().c_str());		// -- Create 'runtimeMetaData' folder

//		path_data << WiFiTracker_SYSTEM_ROOT_PATH;
//		cmd.str(""); cmd << "mkdir " << path_data.str(); system(cmd.str().c_str());		// -- Create 'config' folder


		path_runtime << "/" << WiFiTracker_APP_NAME;
		cmd.str(""); cmd << "rm -rf" << path_runtime.str();	system(cmd.str().c_str()); // -- Erase existing 'runtimeMetaData/WiFi_Shopper_Tracker' folder
		cmd.str(""); cmd << "mkdir " << path_runtime.str(); system(cmd.str().c_str());		// -- Create 'runtimeMetaData/WiFi_Shopper_Tracker' folder

//		path_data << "/" << WiFiTracker_APP_NAME;
//		cmd.str(""); cmd << "mkdir " << path_data.str(); system(cmd.str().c_str());		// -- Create 'config/WiFi_Shopper_Tracker' folder

		path_runtime << "/" << APP_STORE_ROOT_PATH;
		cmd.str(""); cmd << "mkdir " << path_runtime.str(); system(cmd.str().c_str());		// -- Create 'runtimeMetaData/WiFi_Shopper_Tracker/Stores' folder

//		path_data << "/" << STORE_ROOT_PATH;
//		cmd.str(""); cmd << "mkdir " << path_data.str(); system(cmd.str().c_str());		// -- Create 'config/WiFi_Shopper_Tracker/Stores' folder


		path_runtime << "/" << mAppStoreName;
		cmd.str(""); cmd << "mkdir " << path_runtime.str(); system(cmd.str().c_str());		// -- Create 'runtimeMetaData/WiFi_Shopper_Tracker/Stores/<store_name>' folder

//		path_data << "/" << STORE_NAME;
//		cmd.str(""); cmd << "mkdir " << path_data.str(); system(cmd.str().c_str());		// -- Create 'config/WiFi_Shopper_Tracker/Stores/<store_name>' folder

	}

	// -- Read App config file
	{
		ostringstream configFilename;
		configFilename << APP_CONFIG_ROOT_PATH << "/" << WiFiTracker_APP_NAME << "/" << APP_STORE_ROOT_PATH << "/" << mAppStoreName << "/" << APP_CONFIG_FILE;
		if(ParseSetting(configFilename.str()) == false) {
			fprintf(stderr, "[WiFi_Shopper_Tracker] ParseSetting() failed\n");
			exit(0);
		}
	}

	// -- Read Kalman config file
	{
		ostringstream configFilename;
		configFilename << APP_CONFIG_ROOT_PATH << "/" << WiFiTracker_APP_NAME << "/" << APP_STORE_ROOT_PATH << "/" << mAppStoreName << "/" << KALMAN_CONFIG_FILE;
		if(loadKalmanParams(configFilename.str()) == false) {
			fprintf(stderr, "[WiFi_Shopper_Tracker] loadKalmanParams() failed\n");
			exit(0);
		}
	}

	// -- Remote run
	if(mShowMapVisualization == false) {

	}

#ifdef USE_WIFI_PLAYER
GroundTruth_pos.x = 0;
GroundTruth_pos.y = 0;
#endif

	gStartTime();		// -- Mark the initial time

	// -- Load OUI Lookup table
	loadOUIlookuptable();

    // -- Load known APs and Stations
	loadKnownAPs_n_ServiceAgents();

	// -- Load devices of interest to be tracked explicitly
	loadDevicesOfInterest();

	// -- Load radio fingerprints for device localization
#if defined(USE_RADIO_FINGERPRINT_BASED_LOCALIZATION) || defined(USE_WIFI_PLAYER)
	loadRadioFingerprints();
#endif

    // -- Remove all the old log files
	if(mCreateMeasLog) {
		for(char i='0'; i<='9'; i++)
			for(char j='0'; j<='9'; j++) {
				char cmd[255];
				sprintf(cmd, "rm -f measurements/%c%c*", i, j);
				//printf("Performing: %s\n", cmd);
				system(cmd);
			}
		for(char i='a'; i<='z'; i++)
			for(char j='a'; j<='z'; j++) {
				char cmd[255];
				sprintf(cmd, "rm -f measurements/%c%c*", i, j);
				//printf("Performing: %s\n", cmd);
				system(cmd);
			}
	}

    // -- Initialize MQTT client for publishing shopper tracking results
    {
    	// -- Create a mosquitto MQTT client
    	string mqtt_id;
    	mqtt_id.append(AWS_IOT_APP_DATA_CHANNEL_PREAMBLE);
    	mqtt_id.append("-");
		mqtt_id.append(APP_ID_WIFI_TRACKER);
    	mqtt_id.append("-");
    	mqtt_id.append(mIoTGateway.GetMyStoreName_dash());

    	/* On connection, a client sets the "clean session" flag, which is sometimes also known as the "clean start" flag.
    	 * If clean session is set to false, then the connection is treated as durable.
    	 * This means that when the client disconnects, any subscriptions it has will remain and any subsequent QoS 1 or 2 messages
    	 * will be stored until it connects again in the future. If clean session is true, then all subscriptions will be removed for the client
    	 * when it disconnects.*/
    	bool clean_session = true;

        if(mMQTT_Tracker == NULL)
            mMQTT_Tracker = new MosqMQTT_agg((const char*)mqtt_id.c_str(), clean_session);

    	// -- Initialize the mosquitto MQTT client for Internal MQTT Broker
    	{
    		//myMQTT->threaded_set(true);		// -- Tell if this application is threaded or not
    		mMQTT_Tracker->username_pw_set((const char*)MQTT_BROKER_USERNAME, (const char*)MQTT_BROKER_PASSWORD);		// -- Set username and pwd to the broker
    		//mMQTT_Tracker->will_set(mqtt->mPubTopic.c_str(), 0, NULL, 1, true);

    		mMQTT_Tracker->mKeepalive = MQTT_BROKER_KEEP_ALIVE;    // Basic configuration setup for myMosq class
    		mMQTT_Tracker->mBrokerPort = MQTT_BROKER_PORT;
    		mMQTT_Tracker->mBrokerAddr.append(MQTT_BROKER_URL);

    		// -- Specify subscription topic. In this case, it will be all APs in a specific retail store
    		mMQTT_subTopic.clear();
    		mMQTT_subTopic.append(AWS_IOT_APP_DATA_CHANNEL_PREAMBLE);
    		mMQTT_subTopic.append("/");
    		mMQTT_subTopic.append(APP_ID_WIFI_PROCESSOR);
    		mMQTT_subTopic.append("/");
    		//mMQTT_subTopic.append("+/+");
    		mMQTT_subTopic.append(mIoTGateway.GetMyStoreName());
			mMQTT_subTopic.append("/");
    		mMQTT_subTopic.append("+");		// -- Note: Don't use '#' here, since there are also shopper tracking result reporting channel like 'Locs'.
    		mMQTT_subTopic.append("/");
    		mMQTT_subTopic.append(mTrackerID_suffix);	// -- To be differentiated with other trackers

    	}

    	printf("MQTT is starting...\n");

    	// -- Start the mosquitto MQTT client for the Internal MQTT Broker
    	mMQTT_Tracker->start();

    	printf("MQTT is started\n");
    }

    // -- Read IP addresses of APs
    if(mMonitoringAP_IPsFilePath.str().find(".txt") != std::string::npos)
    	ReadMonitoringAPs_text(mMonitoringAP_IPsFilePath.str());
    else if(mMonitoringAP_IPsFilePath.str().find(".csv") != std::string::npos)
    	ReadMonitoringAPs_csv(mMonitoringAP_IPsFilePath.str());
    else {
    	fprintf(stderr, "[WiFi_Shopper_Tracker] Error MONITORING_AP_IP_ADDR_LIST_FILENAME [%s] is not a valid file name/path", mMonitoringAP_IPsFilePath.str().c_str());
    	exit(0);
    }

    // -- Connect to each OmniSensrs
    {
		string subTopic;
		subTopic.append(AWS_IOT_APP_DATA_CHANNEL_PREAMBLE);
		subTopic.append("/");
		subTopic.append(APP_ID_WIFI_PROCESSOR);
		subTopic.append("/");
		//subTopic.append("+/+");
		subTopic.append(mIoTGateway.GetMyStoreName());
		subTopic.append("/");
		subTopic.append("+");

		for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it) {
			WiFiSensor_short_Ptr os = *it;
			// -- MQTT topic that this class will subscribe to in order to get the data stream from the actual Wi-Fi sensor that publishes data in its local MQTT Broker.
			os->ConnectAndGetStream(subTopic, mTrackerID_suffix);

			printf("AP[%s] is successfully added w/ Topic[%s] \n", os->mIPaddr.c_str(), subTopic.c_str());
		}
    }

   	// -- Create nearby neighbor list
   	{
   		for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it) {
			WiFiSensor_short_Ptr os = *it;
			for(deque<WiFiSensor_short_Ptr >::iterator jt = mRunningWiFiSensor.begin(); jt != mRunningWiFiSensor.end(); ++jt) {
				WiFiSensor_short_Ptr peer = *jt;
				if(os != peer) {
					// -- Calculate the distance
					double distBtwOSs = sqrt(pow(os->mSensorPos_x - peer->mSensorPos_x,2) + pow(os->mSensorPos_y - peer->mSensorPos_y,2));
					if(distBtwOSs < mMaxDistForNearbyNeighbors) {
						os->mNearbyNeighbors.push_back(peer->mDevName);

						if(mDebugVerboseLevel > 3)
							printf("OS[%s]: %dth Nearby peer[%s] w/ dist = %.2f [m]\n", os->mDevName.c_str(), (int)os->mNearbyNeighbors.size(), peer->mDevName.c_str(), distBtwOSs/100.0);

					}
				}
			}
   		}
   	}

   	// -- Create and initialize Kalman filters for calibration
   	{
		for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it) {
			WiFiSensor_short_Ptr os = *it;
			for(int ind_ch=0; ind_ch<NUM_CHANNELS_TO_CAPTURE; ind_ch++)
				for(deque<WiFiSensor_short_Ptr >::iterator jt = mRunningWiFiSensor.begin(); jt != mRunningWiFiSensor.end(); ++jt) {
					WiFiSensor_short_Ptr src = *jt;
					if(os == src) continue;

					KalmanFilter_4S kalman;

					// -- Initialize Kalman filter for RSS estimation
					kalman.DeepInitialize(mKalmanCalibRssProcessNoise_var, mKalmanCalibRssMeasurementNoise_var, mKalmanTimeScale_rss, mKalmanCalibRssCovCoeff_pos_min, mKalmanCalibRssCovCoeff_accel_min, kalman_signal_covariance_11, kalman_signal_covariance_33);

					kalman.mCenterOfSensingField_X = src->mSensorPos_x;
					kalman.mCenterOfSensingField_Y = src->mSensorPos_y;

					os->AddKalmanCalibRSS(kalman, ind_ch);
				}
		}
   	}


    // -------------------

	// -- Load per-ap calibration parameters
	loadCalibrationParams();


	// -- IoT Set-up
	{
		string mqttTopicState, mqttTopicData;
			// -- Define the topic for state
			{
				mAppID = APP_ID_WIFI_TRACKER;

				mqttTopicState.clear();
				mqttTopicState.append(AWS_IOT_APP_STATE_CHANNEL_PREAMBLE);
				mqttTopicState.append("/");
				mqttTopicState.append(mAppID);

				printf("[WiFi_Shopper_Tracker] mqttTopicState = %s\n", mqttTopicState.c_str());
			}

			// -- Set the AppId in string. (e.g., mAppType_Tracker = "8" for WiFi Tracker)
			{
				ostringstream appIdStr;
				appIdStr.clear();
				appIdStr.str("");
				appIdStr << wifiTrack;

				mAppType_Tracker = appIdStr.str();
			}

			// -- Define the topic for data
			{
				// -- Compose the publish topic
				// -- AWS_IoT_Gateway will publish data to a topic 'data/<app_id>/<store_name_prefix>/<store_name_suffix>/<device_name>' (e.g., data/wifiRss/vmhq/16801/cam001)
				mqttTopicData.append(AWS_IOT_APP_DATA_CHANNEL_PREAMBLE);
				mqttTopicData.append("/");
				mqttTopicData.append(mAppID);
				mqttTopicData.append("/");
				mqttTopicData.append(mIoTGateway.GetMyStoreName());
				mqttTopicData.append("/");
				mqttTopicData.append(mIoTGateway.GetMyDevName());
				mqttTopicData.append("/");
				mqttTopicData.append(mTrackerID_suffix);		// -- To differentiate different trackers running on the same device.

				printf("[WiFi_Shopper_Tracker] mqttTopicData = %s\n", mqttTopicData.c_str());
			}

			// -- Register Delta State callback function
			mIoTGateway.RegisterDeltaCallbackFunc(&IoT_DeltaState_CallBack_func);

			// -- Register Control Channel callback function
			mIoTGateway.RegisterControlCallbackFunc(&IoT_ControlChannel_CallBack_func);

			// -- ######################################################################
			// -- User-defined State Parameter Definition
			// -- Add state parameters
			// -- Note: You don't need to declare the state parameters to be 'global' since the pointers to the state parameters will be carried through.
			/*
			 * Note that all the types of JsonPrimitiveType of AWS ThingShadow parameters are grouped into just 6 types: int, uint, float, double, bool, string.
			 * So just use the 6 parameter types when defining a state parameter.
			 */


			/*
			 * << Things to be Reported >>
			 *
			 * # of device being tracked: Total		mTrackState.noTrackedShoppers				int
			 * # of device being tracked: InArea	mTrackState.noTrackedShoppersWithinArea		int
			 * # of Service Agents in the service agent list		mKnownServiceAgentsSize		int
			 * # of APs in the AP list								mKnownAPsSize				int
			 * Periodic processing rate				mTrackState.avgPeriodicProcessingRate		int
			 * Mode among {Calibration, Tracking}	mPerformingWiFiCalib						bool
			 *
			 *
			 * << Things to be Synced >>
			 * REPORT_REAL_TIME_TRACK_RESULTS, mReportRealTimeTrackResult			bool
			 * MEASUREMENT_ADJUSTMENT_ENABLE, mMeasurementAdjustmentEnable			bool
			 * KALMAN_POS_FEEDBACK_TO_RSS_ENABLE, mKalmanPosFeedbackToRssEnable		bool
			 * IOT_PERIODIC_STATE_REPORTING_INTERVAL, mPeriodicStateReportingInterval		int
			 *
			 * 	KALMAN_RSS_PROCESS_NOISE_VARIANCE		mKalmanRssProcessNoise_var			double
				KALMAN_RSS_MEASUREMENT_NOISE_VARIANCE	mKalmanRssMeasurementNoise_var		double
				KALMAN_RSS_MIN_COV_COEFFICENT_POS		mKalmanRssCovCoeff_pos_min			double
				KALMAN_RSS_MIN_COV_COEFFICENT_ACCEL		mKalmanRssCovCoeff_accel_min		double

				KALMAN_POS_PROCESS_NOISE_VARIANCE		mKalmanPosProcessNoise_var			double
				KALMAN_POS_MEASUREMENT_NOISE_VARIANCE	mKalmanPosMeasurementNoise_var		double
				KALMAN_POS_MIN_COV_COEFFICENT_POS		mKalmanPosCovCoeff_pos_min			double
				KALMAN_POS_MIN_COV_COEFFICENT_ACCEL		mKalmanPosCovCoeff_accel_min		double
			 *
			 */

//			string tempStr = "Hello_Hi_There";
//			mIoTGateway.AddStateParam(SHADOW_JSON_STRING, "tempStr", &tempStr, NULL, mPeriodicStateReportingInterval);

			mIoTGateway.AddStateParam(SHADOW_JSON_INT32, "noShoppers", &mTrackerState.noTrackedShoppers, NULL, mPeriodicStateReportingInterval);
			mIoTGateway.AddStateParam(SHADOW_JSON_INT32, "noShoppersInStore", &mTrackerState.noTrackedShoppersWithinArea, NULL, mPeriodicStateReportingInterval);
			//mIoTGateway.AddStateParam(SHADOW_JSON_INT32, "SASize", &mTrackerState.noKnownServiceAgents, NULL, mPeriodicStateReportingInterval);
			//mIoTGateway.AddStateParam(SHADOW_JSON_INT32, "APSize", &mTrackerState.noKnownAPs, NULL, mPeriodicStateReportingInterval);
			mIoTGateway.AddStateParam(SHADOW_JSON_INT32, "PeriodicProcRate", &mTrackerState.avgPeriodicProcessingRate, NULL, mPeriodicStateReportingInterval);
			mIoTGateway.AddStateParam(SHADOW_JSON_INT32, "WiFiAvail", &mTrackerState.WiFiSensorAvailability, NULL, mPeriodicStateReportingInterval);

			mIoTGateway.AddStateParam(SHADOW_JSON_UINT8, "WiFiMode", &mWiFiOperationMode, NULL, mPeriodicStateReportingInterval);

			mIoTGateway.AddStateParam(SHADOW_JSON_BOOL, "RealTimeReport", &mReportRealTimeTrackResult, NULL, mPeriodicStateReportingInterval);
			//mIoTGateway.AddStateParam(SHADOW_JSON_BOOL, "MeasAdjustEnable", &mMeasurementAdjustmentEnable, NULL, mPeriodicStateReportingInterval);

			mIoTGateway.AddStateParam(SHADOW_JSON_INT32, "StateReportInterval", &mPeriodicStateReportingInterval, NULL, mPeriodicStateReportingInterval);

			mIoTGateway.AddStateParam(SHADOW_JSON_INT8, "RssOffset", &mWifiCalibRssOffset, NULL, mPeriodicStateReportingInterval);
			mIoTGateway.AddStateParam(SHADOW_JSON_UINT16, "RunningTime", &mTrackerState.runngingTime_in_min, NULL, mPeriodicStateReportingInterval);


//			mIoTGateway.AddStateParam(SHADOW_JSON_BOOL, "K-PosFeedbackEnable", &mKalmanPosFeedbackToRssEnable, NULL, mPeriodicStateReportingInterval*10);

//			mIoTGateway.AddStateParam(SHADOW_JSON_DOUBLE, "K-RssProcNoise_var", &mKalmanRssProcessNoise_var, NULL, mPeriodicStateReportingInterval*10);
//			mIoTGateway.AddStateParam(SHADOW_JSON_DOUBLE, "K-RssMeasNoise_var", &mKalmanRssMeasurementNoise_var, NULL, mPeriodicStateReportingInterval*10);
//			mIoTGateway.AddStateParam(SHADOW_JSON_DOUBLE, "K-RssCovCoeff_pos_min", &mKalmanRssCovCoeff_pos_min, NULL, mPeriodicStateReportingInterval*10);
//			mIoTGateway.AddStateParam(SHADOW_JSON_DOUBLE, "K-RssCovCoeff_accel_min", &mKalmanRssCovCoeff_accel_min, NULL, mPeriodicStateReportingInterval*10);

//			mIoTGateway.AddStateParam(SHADOW_JSON_DOUBLE, "K-PosProcNoise_var", &mKalmanPosProcessNoise_var, NULL, mPeriodicStateReportingInterval*10);
//			mIoTGateway.AddStateParam(SHADOW_JSON_DOUBLE, "K-PosMeasNoise_var", &mKalmanPosMeasurementNoise_var, NULL, mPeriodicStateReportingInterval*10);
//			mIoTGateway.AddStateParam(SHADOW_JSON_DOUBLE, "K-PosCovCoeff_pos_min", &mKalmanPosCovCoeff_pos_min, NULL, mPeriodicStateReportingInterval*10);
//			mIoTGateway.AddStateParam(SHADOW_JSON_DOUBLE, "K-PosCovCoeff_acc_min", &mKalmanPosCovCoeff_accel_min, NULL, mPeriodicStateReportingInterval*10);



			// -- Start running aws-iot-gateway with a ThingShadow topic
			mIoTGateway.Run(mAppID, mqttTopicState, mqttTopicData, IOT_GATEWAY_CONFIG_FILE);
	}

	usleep(1e5);

	// -- Generate threads
	{
			// -- Create packet analyzer thread
            if(pthread_create(&mPacketStreamParsing_thread, &mThreadAttr, PacketStreamParsingThread, (void *)this))
			{
				fprintf(stderr, "Error in pthread_create(PacketStreamParsingThread)\n");
				return false;
			} else {
				printf( " -- pthread_create(PacketStreamParsingThread) -- \n");
			}

			// -- Create periodic buffer processing thread
            if (pthread_create(&mPeriodicBufferProcessing_thread, &mThreadAttr, PeriodicBufferProcessingThread, (void *)this))
			{
				fprintf(stderr, "Error in pthread_create(PeriodicBufferProcessingThread)\n");
				return false;
			} else {
				printf( " -- pthread_create(PeriodicBufferProcessingThread) -- \n");
			}

	}

	usleep(1e5);

	// -- GUI begin ---------------------------------------------------

	double currTime, prevTime_map, prevTime_calibProgress;
	prevTime_calibProgress = prevTime_map = mCalibStartTime = gTimeSinceStart();

	currTime = prevTime_map + mPeriodicInterval_GuiMapRefresh;


	if(mRunningWiFiSensor.size() >= 3) {

		// -- Displays the initial image of the room
		ostringstream storeFilename;
		storeFilename << APP_CONFIG_ROOT_PATH << "/" << WiFiTracker_APP_NAME << "/" << APP_STORE_ROOT_PATH << "/" << mAppStoreName << "/" << mStoreLayoutFilename;

        // -- Generate initial map and phy-to-img conversion function
        InitialMapGeneration(storeFilename.str());


        // -- Calculate the coordinates of the sensors on the store layout/map
        for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it) {
            WiFiSensor_short_Ptr os = *it;

            Point mapPos = ConvertPhyCoordToMapCoord(os->mSensorPos_x, os->mSensorPos_y);

            os->mSensorPos_x_map = mapPos.x;
            os->mSensorPos_y_map = mapPos.y;
        }

		if(mShowMapVisualization) {
			namedWindow(mImgMapName, CV_WINDOW_AUTOSIZE);
			moveWindow(mImgMapName, 0, 0);
		}


		while(1) {

	        // -- Periodically report the current status of the calibration
	        if(mWiFiOperationMode == WIFI_CALIBRATION) {

	        	mCalibElapsedTime = (int)(currTime - mCalibStartTime);

	        	if(currTime - prevTime_calibProgress > 5) {
					prevTime_calibProgress = currTime;

					SendWiFiCalibProgress();
	        	}
	        }

			// -- Refresh the display
			if(currTime - prevTime_map >= mPeriodicInterval_GuiMapRefresh) {

				// -- Wait until the other thread finishes its turn.
				do {
					pthread_mutex_lock(&gMutex);
					bool isOtherThreadProcessing = mIsThereSomeProcessing; // mIsAddingNewDevice || mIsPeriodicProcessing;
					pthread_mutex_unlock(&gMutex);

					if(isOtherThreadProcessing > 0) usleep(5000);
					else break;
				} while(true);

				pthread_mutex_lock(&gMutex);
				//mIsThereSomeProcessing |= (1<<DRAWING_MAP);
				mIsThereSomeProcessing = (1<<DRAWING_MAP);
				pthread_mutex_unlock(&gMutex);

				// -- Calculate tracker stats
				calculateTrackStats();

				// -- Draw and show the map                
                drawMap();

				pthread_mutex_lock(&gMutex);
				//mIsThereSomeProcessing &= ~(1<<DRAWING_MAP);
				mIsThereSomeProcessing = 0;
				pthread_mutex_unlock(&gMutex);

				prevTime_map = currTime;
			}

			if(mWiFiOperationMode_prev != mWiFiOperationMode) {
				mWiFiOperationMode_prev = mWiFiOperationMode;

				// -- Wait until the other thread finishes its turn.
				do {
					pthread_mutex_lock(&gMutex);
					bool isOtherThreadProcessing = mIsThereSomeProcessing; // mIsAddingNewDevice || mIsPeriodicProcessing;
					pthread_mutex_unlock(&gMutex);

					if(isOtherThreadProcessing > 0) usleep(5000);
					else break;
				} while(true);

				pthread_mutex_lock(&gMutex);
				mIsThereSomeProcessing = (1<<CALIBRATION);
				pthread_mutex_unlock(&gMutex);

				if(mWiFiOperationMode == WIFI_CALIBRATION) {

					if(mDebugCalib)
						printf("[wifiTrack] Sending WiFiCalibMsg(true) 1...\n");

					for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it) {
						WiFiSensor_short_Ptr os = *it;
						if(os == NULL) continue;

						// -- Send StartWiFiCalib msg to each OmniSensr
						os->SendWiFiCalibMsg(true);

					}
					if(mDebugCalib)
						printf("[wifiTrack] Send WiFiCalibMsg(true) Done 1\n");

					pthread_mutex_lock(&gMutex);
					mCalibStartTime = currTime;
					mStartWiFiCalib = true;
					mDebugCalib = true;
					mDebugIoT = true;

					// -- Prior to the new calibration, empty the existing buffer.
					for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it) {
						WiFiSensor_short_Ptr os = *it;
						if(os == NULL) continue;

						for(int ch=0; ch<NUM_CHANNELS_TO_CAPTURE; ch++) {	// -- For each of three channels
							os->mTotalMeasCntThreshold[ch] = mCalibEnoughMeasBinsToKickOffCalib_min*mCalibMeasPerBin_min*3;
							os->mCalibFailCnt[ch] = 0;
							os->mWiFiCalibStarted[ch] = true;
							os->mCalibMeasCollectionProgress[ch] = 0;
							for(int i=0; i<CALIB_NUM_DISTANCE_BINS; i++) {
								os->mCalibMeas[ch][i].clear();
								os->mCalibMeas_sampled[ch][i].clear();
							}

							os->mCalib_a[ch] = 0;
							os->mCalib_b[ch] = 0;

							os->mCalib_a_best[ch] = 0;
							os->mCalib_b_best[ch] = 0;

							os->mCalibMeas_per_peer[ch].clear();
						}


					}
					pthread_mutex_unlock(&gMutex);

					// -- Removing the existing calibration file
					// -- TODO: Instead of having a single calib param file for all the nodes, make a calib param file for each individual node and update it whenever calib for the node is done. XXXXXXXX
	//				FILE *fp_tr = fopen(mCalibParamFilename.str().c_str(), "w");
	//				fclose(fp_tr);

					printf("[WiFi_Shopper_Tracker] Will start WiFi Calibration process: waiting for measurements...\n");

				} else if(mWiFiOperationMode == WIFI_TRACKING) {

					if(mDebugCalib)
						printf("[wifiTrack] Sending WiFiCalibMsg(false) 0 ...\n");

					for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it) {
						WiFiSensor_short_Ptr os = *it;
						if(os == NULL) continue;

						// -- Send StartWiFiCalib msg to each OmniSensr
						os->SendWiFiCalibMsg(false);
					}

					if(mDebugCalib)
						printf("[wifiTrack] Send WiFiCalibMsg(false) Done 0\n");


					pthread_mutex_lock(&gMutex);
					mStartWiFiCalib = false;
					mDebugCalib = false;
					mDebugIoT = false;
					mNeedToTurnOffCalib = false;

					for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it) {
						WiFiSensor_short_Ptr os = *it;
						if(os == NULL) continue;

						for(int m=0; m<NUM_CHANNELS_TO_CAPTURE; m++) {	// -- For each of three channels
							os->mWiFiCalibStarted[m] = false;
						}
					}

					pthread_mutex_unlock(&gMutex);

				} else if(mWiFiOperationMode == WIFI_CALIB_VERIFICATION) {

					pthread_mutex_lock(&gMutex);
					mStartWiFiCalib = false;
					pthread_mutex_unlock(&gMutex);

					//if(mDebugCalib)
						printf("[wifiTrack] Sending WiFiCalibMsg(true) 2...\n");

					for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it) {
						WiFiSensor_short_Ptr os = *it;
						if(os == NULL) continue;

						// -- Send StartWiFiCalib msg to each OmniSensr
						os->SendWiFiCalibMsg(true);
					}

					//if(mDebugCalib)
						printf("[wifiTrack] Send WiFiCalibMsg(true) Done 2\n");
				}

				pthread_mutex_lock(&gMutex);
				//mIsThereSomeProcessing &= ~(1<<DRAWING_MAP);
				mIsThereSomeProcessing = 0;
				pthread_mutex_unlock(&gMutex);
			}

			// -- Force quit packet injection
			if(mNeedToTurnOffCalib == true) {

				pthread_mutex_lock(&gMutex);
				mNeedToTurnOffCalib = false;
				pthread_mutex_unlock(&gMutex);

				//if(mDebugCalib)
					printf("[wifiTrack] Sending WiFiCalibMsg(false) 9...\n");

				for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it) {
					WiFiSensor_short_Ptr os = *it;
					if(os == NULL) continue;

					// -- Send StartWiFiCalib msg to each OmniSensr
					os->SendWiFiCalibMsg(false);
				}

				//if(mDebugCalib)
					printf("[wifiTrack] Send WiFiCalibMsg(false) Done 9\n");
			}

			// -- After a wait period, it checks if the measurments are still coming. If no more measurements received, then start calculating the calibration parameters.
			if(mStartWiFiCalib == true && currTime - mCalibStartTime >= mCalibStartWaitPeriod_min) {

                // -- Create packet analyzer thread
                if(pthread_create(&mWiFiCalibProcessing_thread, &mThreadAttr, WiFiCalibProcessingThread, (void *)this))
				{
					fprintf(stderr, "Error in pthread_create(WiFiCalibProcessingThread)\n");
				} else {

					if(mDebugTrackerStatus)
						printf("[WiFi_Shopper_Tracker] Start WiFi Calibration: create WiFiCalibProcessingThread...\n");

					mStartWiFiCalib = false;
				}
			}

			currTime = gTimeSinceStart();

			// -- Note: Don't replace this with sleep() or usleep() function.
			char key = waitKey(20);

			if(key > 0 && mShowMapVisualization)
				switch(key) {
					default:
					case 'h':		// -- Show help message
						cout << "--------- Help Message --------" << endl;
						cout << " 'a': Show Accuracy Statistics of OmniSensrs" << endl;
						cout << " 's': Show Device Statistics" << endl;
						cout << " 'x': Stop printing anything on the screen" << endl;
						cout << " 'd': Change Debug Mode Params" << endl;

						cout << " 'h': Show Help Messages" << endl << endl;
						break;

						/*
						 * 		int mDebugVerboseLevel, mDebugDeviceStatusVerboseMode;
			bool mDebugTrackerStatus, mDebugCalib, mDebugLocalization, mDebugLocalizationErrorCode;
			static bool mDebugIoT;
						 */
					case 'x': {
						cout << "Disable all Debug Mode\n";
						mDebugVerboseLevel = mDebugDeviceStatusVerboseMode = 0;
						mDebugTrackerStatus = mDebugCalib = mDebugLocalization = mDebugLocalizationErrorCode = mDebugIoT = false;
						break;
					}
					case 'd': {
						char debugModeType;
						int debugValue;

						stringstream ss;
						cout << "------- Choose what Debug Param. do you want to change:" << endl;
						cout << "1: mDebugVerboseLevel (int)" << endl;
						cout << "2: mDebugDeviceStatusVerboseMode (int)" << endl;
						cout << "3: mDebugTrackerStatus (bool)" << endl;
						cout << "4: mDebugCalib (bool)" << endl;
						cout << "5: mDebugLocalization (bool)" << endl;
						cout << "6: mDebugLocalizationErrorCode (bool)" << endl;
						cout << "7: mDebugIoT (bool)" << endl;


						ss.str(""); ss << vml::getLineFromStdIn();
						ss >> debugModeType;

						cout << "---- Change to what value? (int) ---\n";
						ss.str(""); ss << vml::getLineFromStdIn();
						ss >> debugValue;

						switch(debugModeType) {
						case '1':
							mDebugVerboseLevel = debugValue;
							cout << "+++++ [SETTING CHANGE] mDebugVerboseLevel = " << mDebugVerboseLevel << endl;
							break;
						case '2':
							mDebugDeviceStatusVerboseMode = debugValue;
							cout << "+++++ [SETTING CHANGE] mDebugDeviceStatusVerboseMode = " << mDebugDeviceStatusVerboseMode << endl;
							break;
						case '3':
							mDebugTrackerStatus = debugValue;
							cout << "+++++ [SETTING CHANGE] mDebugTrackerStatus = " << mDebugTrackerStatus << endl;
							break;
						case '4':
							mDebugCalib = debugValue;
							cout << "+++++ [SETTING CHANGE] mDebugCalib = " << mDebugCalib << endl;
							break;
						case '5':
							mDebugLocalization = debugValue;
							cout << "+++++ [SETTING CHANGE] mDebugLocalization = " << mDebugLocalization << endl;
							break;
						case '6':
							mDebugLocalizationErrorCode = debugValue;
							cout << "+++++ [SETTING CHANGE] mDebugLocalizationErrorCode = " << mDebugLocalizationErrorCode << endl;
							break;
						case '7':
							mDebugIoT = debugValue;
							cout << "+++++ [SETTING CHANGE] mDebugIoT = " << mDebugIoT << endl;
							break;
						}

						break;
					}
					case 'a': {		// -- Show updated accuracy statistics when tracking OmniSensrs

						pthread_mutex_lock(&gMutex);
						if(mFoundDevice.size() > 0) {
							double distErrorAvg = 0, distErrorStdAvg = 0, cnt = 0;
							for(deque<RemoteDevicePtr>::iterator it = mFoundDevice.begin(); it != mFoundDevice.end();++it)
							{
								RemoteDevicePtr dev = *it;
								if(dev == NULL)
									continue;

								bool isOmniSensr = dev->GetDeviceType() == OmniSensr;
								if(isOmniSensr == true && dev->mAccuracyStat.distError.size() > 0) {
									printf("++ Accuracy Stat ++ OS[%s]: distError [size = %d] (avg, std) = (%.2f, %.2f) [m] w/ mPerDevRssCalibOffset = %.2f\n", dev->GetDevAddr().c_str(), (int)dev->mAccuracyStat.distError.size(), dev->mAccuracyStat.distErrorAvg/100.0, dev->mAccuracyStat.distErrorStd/100.0, dev->mPerDevRssCalibOffset);

									distErrorAvg += dev->mAccuracyStat.distErrorAvg;
									distErrorStdAvg += dev->mAccuracyStat.distErrorStd;
									cnt += 1;
								}
							}
							if(cnt > 0) {
								distErrorAvg /= cnt;
								distErrorStdAvg /= cnt;

								printf("SUMMARY ++ Accuracy Stat ++ Total distError (avg, std) = (%.2f, %.2f) [m]\n", distErrorAvg/100.0, distErrorStdAvg/100.0);
							} else {
								printf("No OmniSensrs are qualified for Accuracy Stat.\n");
							}
						}
						pthread_mutex_unlock(&gMutex);

						break;
					}

					case 's': {
						bool onlyDevOfInterest;
						char statType;

						stringstream ss;
						cout << "------- Only Device of Interest? (1 or 0)" << endl;
						ss.str(""); ss << vml::getLineFromStdIn();
						ss >> onlyDevOfInterest;

						cout << "------- Choose what stat do you want:" << endl;
						cout << "1: Total RSS Historgram" << endl;
						cout << "2: Per-channel RSS Historgram" << endl;
						cout << "3: Revised Per-channel RSS Historgram" << endl;
						cout << "4: Time-Diff Historgram of Packet arrivals" << endl;

						ss.str(""); ss << vml::getLineFromStdIn();
						ss >> statType;

						if(mFoundDevice.size() > 0) {
							for(deque<RemoteDevicePtr>::iterator it = mFoundDevice.begin(); it != mFoundDevice.end();++it)
							{
								RemoteDevicePtr dev = *it;
								if(dev == NULL)
									continue;

								if(onlyDevOfInterest == true && isDevicesOfInterest(dev->GetDevAddr()) == false)
									continue;

								switch(statType) {
									case '1':
										dev->ShowStats_RssHist_Total();
										break;
									case '2':
										dev->ShowStats_RssHist_PerChannel();
										break;
									case '3':
										dev->ShowStats_RssHist_PerChannel_Revised();
										break;
									case '4':
										dev->ShowStats_TimeDiffHistCDF();
										break;
								}
							}
						}

						break;
					}
				}
		}
	} else {
		printf("mRunningAP.size() = %d TOO LOW!\n", (int)mRunningWiFiSensor.size());
	}

	return true;
}

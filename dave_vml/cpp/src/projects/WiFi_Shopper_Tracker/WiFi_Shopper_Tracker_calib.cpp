/*
 * WiFi_Shopper_Tracker_calib.cpp
 *
 *  Created on: June 6, 2016
 *      Author: paulshin
 */

#include "unistd.h"
#include <iomanip>
#include <limits>

#include <dlib/optimization.h>

#include "modules/iot/IoT_Common.hpp"
#include "modules/iot/IoT_Gateway.hpp"
#include "modules/utility/TimeKeeper.hpp"
#include "modules/utility/vml_geometry.hpp"
#include "modules/utility/vml_math.hpp"

#include "projects/WiFi_Shopper_Tracker/Config.hpp"
#include "projects/WiFi_Shopper_Tracker/WiFi_Shopper_Tracker.hpp"

extern pthread_mutex_t gMutex;


typedef dlib::matrix<double,1,1> calibInputVector;
typedef dlib::matrix<double,2,1> calibParameterVector;

// ----------------------------------------------------------------------------------------

// We will use this function to generate data.  It represents a function of 2 variables
// and 3 parameters.   The least squares procedure will be used to infer the values of
// the 3 parameters based on a set of input/output pairs.
double calibFittingModel (
    const calibInputVector& rss,
    const calibParameterVector& params
)
{
    const double a = params(0);
    const double b = params(1);

    const double x = rss(0);

    const double ret = a*x + b;
    //const double ret = a*x + b + mWifiCalibRssOffset;

    return ret;
}

// This function is the "residual" for a least squares problem.   It takes an input/output
// pair and compares it to the output of our model and returns the amount of error.  The idea
// is to find the set of parameters which makes the residual small on all the data pairs.
// -- data.first := RSS
// -- data.second := log(dist)
// -- params := (a, b)
double calibFittingModelResidual_weighted(
    const std::pair<calibInputVector, double>& data,
    const calibParameterVector& params
)
{

	double errorToBeMinimized;

	//errorToBeMinimized = 1.f/pow(data.second,2) * pow(calibFittingModel(data.first, params) - data.second, 2) * 100.0f;
    //errorToBeMinimized = 1.f/fabs(data.second) * pow(calibFittingModel(data.first, params) - data.second, 2) * 100.0f;
    errorToBeMinimized = 1.f/log(fabs(data.second)) * pow(calibFittingModel(data.first, params) - data.second, 2) * 100.0f;

	return errorToBeMinimized;

	// -- We want to minimize { (a*(rss) + b) - (log(dist)) } ^ 2
	//return fabs(calibFittingModel(data.first, params) - data.second);
    //return pow(calibFittingModel(data.first, params) - data.second, 2);

}


// This function is the "residual" for a least squares problem.   It takes an input/output
// pair and compares it to the output of our model and returns the amount of error.  The idea
// is to find the set of parameters which makes the residual small on all the data pairs.
// -- data.first := RSS
// -- data.second := log(dist)
// -- params := (a, b)
double calibFittingModelResidual(
    const std::pair<calibInputVector, double>& data,
    const calibParameterVector& params
)
{
	// -- We want to minimize { (a*(rss) + b) - (log(dist)) } ^ 2
	//return fabs(calibFittingModel(data.first, params) - data.second);
	//return log(pow(calibFittingModel(data.first, params) - data.second,2));
    return pow(calibFittingModel(data.first, params) - data.second, 2);

}

// ----------------------------------------------------------------------------------------

// This function is the derivative of the residual() function with respect to the parameters.
calibParameterVector calibFittingModelResidualDerivative(
    const std::pair<calibInputVector, double>& data,
    const calibParameterVector& params
)
{
    calibParameterVector der;

    const double a = params(0);
    const double b = params(1);

    const double rss = data.first(0);
    const double ld = data.second;

    // -- Partial derivative with respect to X (i.e., dX) of temp
    der(0) = 2*(a*rss + b - ld)*(-1);		// -- Paul: Don't know why, but this one is correct
    //der(0) = 2*(a*rss + b - ld)*a;		// -- Paul: Don't know why, but this one is wrong.

    // -- Partial derivative with respect to Y (i.e., dy) of temp
    der(1) = 2*(a*rss + b - ld)*a;
    //der(1) = 2*(a*rss + b - ld)*(-1);

    return der;
}

bool WiFi_Shopper_Tracker::calibAddNewRSS(tcpdump_t	&meas) {
	if(DEBUG_FUNCTION_CALL)
		printf("calibAddNewRSS() Called. \n");


	// -- Identify the sender by its SN
	WiFiSensor_short_Ptr os_src = findRunningWiFiSensor(meas.src);
	WiFiSensor_short_Ptr os_capturedBy = findRunningWiFiSensor(meas.capturedBy);

	if(os_src == NULL || os_capturedBy == NULL) {
		if(DEBUG_FUNCTION_CALL)
			printf("calibAddNewRSS() Ended. false \n");
		return false;
	}

	if(meas.rss < -100 || meas.rss > 0) {
		if(DEBUG_FUNCTION_CALL)
			printf("calibAddNewRSS() Ended. false \n");
		return false;
	}

#if CHANNEL_SEPARATION == false
	meas.channel = 1;
#endif

	// -- Channel 1, 6, and 11 would be stored in Bin 0, 1, and 2, respectively.
	int ind_channel = ConvertChannelIntoIndex(meas.channel);

	if(ind_channel < 0)
		return false;

	// -- Perform Kalman Filtering on this RSS received
	KalmanFilter_4S *kal = os_capturedBy->findKalmanCalibRSS(os_src->mSensorPos_x, os_src->mSensorPos_y, ind_channel);

	if(kal == NULL) {
		if(DEBUG_FUNCTION_CALL)
			printf("calibAddNewRSS() Ended since No Kalman Found!. false (os_src, os_capturedBy) = (%s, %s)\n", os_src->mDevName.c_str(), os_capturedBy->mDevName.c_str());
		return false;
	}

//	printf("(os_src, os_capturedBy, RSS rcved, RSS ested, GetKalmanAge()) = (%s, %s, %d, %d, %d)\n",
//				os_src->mDevName.c_str(), os_capturedBy->mDevName.c_str(), meas.rss, (int)rssEstimated[0], kal->GetKalmanAge());

	// -- Update and get the estimated RSS
	if(kal->GetKalmanAge() == 0
			 || kal->mNeedToInitialize == true
			 ) {

//		printf("Initialize (os_src, os_capturedBy, RSS rcved, GetKalmanAge()) = (%s, %s, %d, %d)\n",
//					os_src->mDevName.c_str(), os_capturedBy->mDevName.c_str(), meas.rss, kal->GetKalmanAge());


		kal->Initialize(meas.rss, meas.rss, kalman_calib_rss_covariance_11, kalman_calib_rss_covariance_33);
		kal->mNeedToInitialize = false;

	}

	kal->Update(meas.rss, meas.rss);

	double rssEstimated[4];
	kal->get_state(rssEstimated);

	// -- Check if the estimated one is erroneous.
	if(meas.rss < -100 || meas.rss > 0) {
		kal->mNeedToInitialize = true;

		if(DEBUG_FUNCTION_CALL)
			printf("calibAddNewRSS() Ended. false \n");
		return false;
	} {

//		printf("(os_src, os_capturedBy, RSS rcved, RSS ested, GetKalmanAge()) = (%s, %s, %d, %d, %d)\n",
//					os_src->mDevName.c_str(), os_capturedBy->mDevName.c_str(), meas.rss, (int)rssEstimated[0], kal->GetKalmanAge());

		// -- Save it back to the measurement
		meas.rss = rssEstimated[0];

		kal->mNeedToInitialize = false;
	}


	// -- Construct a calibration measurement
	calibMeas_t m;
	m.src = os_src->mDevName;
	m.capturedBy = os_capturedBy->mDevName;
	//m.rss = MIN(meas.rss - mWifiCalibRssOffset, -5);
	m.rss = meas.rss;
	m.channel = meas.channel;
	m.dist = sqrt(pow(os_src->mSensorPos_x - os_capturedBy->mSensorPos_x, 2) + pow(os_src->mSensorPos_y - os_capturedBy->mSensorPos_y, 2));
	m.x = os_src->mSensorPos_x;		// -- Physical coordinate of the packet source
	m.y = os_src->mSensorPos_y;		// -- Physical coordinate of the packet source
	m.timestamp = gTimeSinceStart();

	// -- If the distance is equal to 0, then it must be from the same node, so throw away. If the distance is less than zero, then it must be corrupted, so throw away.
	if(m.dist <= 0) {
		//printf("$$$$$$ Node[%s] got weird message from Node[%s]: (rss, dist) = (%d, %f), (os_src.x, os_src.y) = (%d, %d), (os_capturedBy.x, os_capturedBy.y) = (%d, %d)\n",
		//		os_src->mDevName.c_str(), os_capturedBy->mDevName.c_str(), m.rss, m.dist, os_src->mSensorPos_x, os_src->mSensorPos_y, os_capturedBy->mSensorPos_x, os_capturedBy->mSensorPos_y);

		return false;
	}

	// -- Put into a calibration measurement buffer
	if(ind_channel >= 0 && ind_channel <= 2) {

		// -- Check if this measurement is the first calib packet for this round of WiFi Calibration. If so, mark it so that it is undergoing a wifi calibration process.
		int totalMeasCnt = 0;
		for(int i=0; i<CALIB_NUM_DISTANCE_BINS; i++)
			totalMeasCnt += os_capturedBy->mCalibMeas[ind_channel][i].size();

		if(totalMeasCnt == 0) {
			if(mDebugCalib)
				printf("### NEW (src, capturedBy, dist, rss, channel) = (%s, %s, %d [m], %d, %d)\n", os_src->mDevName.c_str(), os_capturedBy->mDevName.c_str(), (int)(m.dist/100.0f), m.rss, m.channel);

		}

		// -- Store into measurement buffers
		os_capturedBy->AddToPerDistanceBinBuffer(m, ind_channel, mCalibMeasPerBin_max);
		os_capturedBy->AddToPerPeerBuffer(m, ind_channel, mCalibMeasPerBin_max);


//		if(mDebugCalib)
//			if(totalMeasCnt%591 == 0) {
//				printf("-------- Node[%s] @Ind_Ch[%d] got %4dth measurements: ", os_capturedBy->mDevName.c_str(), ind_channel, (int)totalMeasCnt+1);
//				for(int i=0; i<CALIB_NUM_DISTANCE_BINS; i++)
//					printf("%3d ", (int)os_capturedBy->mCalibMeas[ind_channel][i].size());
//				printf("\n");
//			}

		if(DEBUG_FUNCTION_CALL)
			printf("calibAddNewRSS() Ended. true\n");

		return true;

	} else
		return false;

}

//bool too_less_measurements(const calibOmniSensr_t &os) {
//	return os.meas.size() < CALIB_MIN_CALIB_RSS_MEASUREMENTS_SIZE_PER_OMNISENSOR_PER_LOCATION;
//}

int WiFi_Shopper_Tracker::uniformlyRandomlySelectDistance(list<pair<int, float> > *sampledData_dst, WiFiSensor_short_Ptr os, int ind_ch) {

	//if(DEBUG_FUNCTION_CALL)
//		printf("Node[%s] Enter uniformlyRandomlySelectDistance()\n", os->mDevName.c_str());

	if(os == NULL) {
		fprintf(stderr, "uniformlyRandomlySelectDistance() @Ind_Ch[%d] got NULL device pointer", ind_ch);
		return ERROR_NULL_OMNISENSR_POINTER;
	}

	//list<pair<int, float> > sampledData[CALIB_NUM_DISTANCE_BINS];  // -- Pair of (rss, dist [cm])
//	list<calibMeas_t> sampledDataList[CALIB_NUM_DISTANCE_BINS];

	// -- Initialize the list
	for(int i=0; i<CALIB_NUM_DISTANCE_BINS; i++) {
		sampledData_dst[i].clear();
		//sampledData[i].clear();
//		sampledDataList[i].clear();
	}

	//os->mCalibMeas_sampled.clear();
//	for(list<calibMeas_t>::iterator iit = os->mCalibMeas.begin(); iit != os->mCalibMeas.end(); ++iit) {
//		calibMeas_t *me = &*iit;
//
//			// -- Sample in the distance space
//			//int ind = (int)(log(me->dist) / (log(CALIB_MAX_SIGNAL_DISTANCE)/CALIB_NUM_DISTANCE_BINS) + 0.5f);
//			int ind = (int)(me->dist / (CALIB_MAX_SIGNAL_DISTANCE/CALIB_NUM_DISTANCE_BINS) + 0.5f);
//			if(ind < 0) ind = 0;
//			else if(ind >= CALIB_NUM_DISTANCE_BINS) ind = CALIB_NUM_DISTANCE_BINS-1;
//
//			if(me->rss < 0 - mWifiCalibRssOffset && me->rss > -100 - mWifiCalibRssOffset) {
//				//sampledData[ind].push_back(make_pair(me->rss, me->dist));
//				sampledDataList[ind].push_back(*me);
//				//os->mCalibMeas_sampled.push_back(*me);
//				//printf("(dist, ind) = (%.0f, %d)\n", me->dist, ind);
//			}
//		}

	unsigned int maxSampleDataSize = 0;
	for(int i=0; i<CALIB_NUM_DISTANCE_BINS; i++)
		 //maxSampleDataSize = maxSampleDataSize > sampledData[i].size() ? maxSampleDataSize : sampledData[i].size();
		maxSampleDataSize = maxSampleDataSize > os->mCalibMeas[ind_ch][i].size() ? maxSampleDataSize : os->mCalibMeas[ind_ch][i].size();

	// -- Randomly select the same number of measurement from each distance bin
	for(int i=0; i<CALIB_NUM_DISTANCE_BINS; i++) {
		uint NumOfSampleToDraw;

//#if USE_LOG_DISTANCE_SPACE == true

		NumOfSampleToDraw = mCalibSampleDistPerBin_max*0.5;

//#else
//
//		// -- Sample more at the edge of the min and max distances than mid distances
////		float distFromMidInd; // -- Range [0, 1]
////		distFromMidInd = fabs(i - CALIB_NUM_DISTANCE_BINS*0.5)/(float)(CALIB_NUM_DISTANCE_BINS*0.5);	// -- Range [0, 1]
//////		distFromMidInd *= distFromMidInd;
//////		distFromMidInd *= distFromMidInd;
//
//		float distFromBeginInd = 1.0f - i/(float)(CALIB_NUM_DISTANCE_BINS-1);	// -- Range [0, 1]
//		distFromBeginInd *= distFromBeginInd;
//		distFromBeginInd *= distFromBeginInd;
//
//		// -- Sample more at the edge of the min and max distances than mid distances
////		NumOfSampleToDraw = mCalibSampleDistPerBin_max*0.05 + distFromMidInd*mCalibSampleDistPerBin_max*0.95 + 0.5f;
//
//		// -- Sample more at the shorter distances
//		NumOfSampleToDraw = distFromBeginInd*(float)mCalibSampleDistPerBin_max + 0.5f;
//#endif

		os->mCalibMeas_sampled[ind_ch][i].clear();

//		if(mDebugCalib) {
//			printf("-------- Bin index = [%d] among %d --> Will draw %d random samples among %d\n", i, CALIB_NUM_DISTANCE_BINS, NumOfSampleToDraw, (int)os->mCalibMeas[ind_ch][i].size());
//		}

		if((int)os->mCalibMeas[ind_ch][i].size() < int(mCalibSampleDistPerBin_max*0.25))
			continue;

		//list<pair<int, float> >::iterator it = sampledData[i].begin();
		list<calibMeas_t>::iterator it = os->mCalibMeas[ind_ch][i].begin();

		for(int j=0; j<(int)NumOfSampleToDraw; j++) {

			uint ind;
			do {
				ind = rand()%(maxSampleDataSize);
			} while (ind > os->mCalibMeas[ind_ch][i].size());

			for(uint k=0; k<ind; k++)
				++it;

			//printf("%d ", ind);

			pair<int, float> sample = make_pair((*it).rss, (*it).dist);

			// -- Check again if RSS value is valid and put into the buffer only if so. Otherwise, skip it.
			//if(sample.first < MIN(0 - mWifiCalibRssOffset, -5) && sample.first > -100 - mWifiCalibRssOffset) {
			if(sample.first <= -5 && sample.first >= -100) {
				sampledData_dst[i].push_back(sample);
				os->mCalibMeas_sampled[ind_ch][i].push_back(*it);

//				printf("(rss, dist) = (%d, %.0f)\n", sample.first, sample.second);
			} else {
				j--;
			}
		}

//		for(int j=0; j<(int)NumOfSampleToDraw; j++) {
//
//			uint ind;
//			do {
//				ind = rand()%(maxSampleDataSize);
//			} while (ind > sampledData[i].size());
//
//			for(uint k=0; k<ind; k++)
//				++it;
//
//			//printf("%d ", ind);
//
//			pair<int, float> sample = *it;
//
//			// -- Check again if RSS value is valid and put into the buffer only if so. Otherwise, skip it.
//			if(sample.first < MIN(0 - mWifiCalibRssOffset, -5) && sample.first > -100 - mWifiCalibRssOffset)
//				sampledData_dst[i].push_back(sample);
//			else {
//				j--;
//			}
//
//			//printf("(rss, dist) = (%d, %.0f)\n", sample.first, sample.second);
//		}
		//printf("\n");
	}
	//printf("\n");

	//if(DEBUG_FUNCTION_CALL)
//		printf("Node[%s] Exit uniformlyRandomlySelectDistance()\n", os->mDevName.c_str());

	return SUCCESS;
}


//void WiFi_Shopper_Tracker::generateRSStoDistanceMappingFunction(FILE *fp_tr, calibOmniSensr_t *os_selected) {
int WiFi_Shopper_Tracker::generateRSStoDistanceMappingFunction(WiFiSensor_short_Ptr os_selected, int ind_ch, double &residualSqrSum_avg_return) {
	// -- Generate per-OmniSensr RSS-Distance mapping function
	// -- It is known that there's a linear relationship between RSS and log(distance).
	// -- Find the linear function by using least-square line fitting algorithm: log(dist) = a*RSS + b --> dist = exp(a*RSS + b)

	// -- Note:
	// -- Using all the points for least-square regression may yield a biased result especially when the points are not uniformly distributed.
	// -- Thus, we need to uniformly sample points across different distance ranges.

	if(os_selected == NULL)
		return ERROR_NULL_OMNISENSR_POINTER;

//	float distError = 0;

	double residualSqrSum_avg = 0;
	double residualSqrSum_avg_min = numeric_limits<double>::max();
//	double residualSqrSum_max = 0;
	int try_cnt = 0, total_try_cnt = 0;;

	list<pair<int, float> > sampledData[CALIB_NUM_DISTANCE_BINS];   // -- Pair of (rss, dist [cm])

	calibParameterVector best_x;

	while (1)
	{

//		int num = 0;

		std::vector<std::pair<calibInputVector, double> > data_samples;
		calibInputVector RSS;
		calibParameterVector x;

		// -- Draw uniformly random samples over distances
	//	int cnt = 0;
	//	for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it) {
	//		WiFiSensor_short_Ptr os = *it;
	//
	//		cnt++;
	//
	//		if(os_selected != NULL && os_selected != os)
	//			continue;

		// -- Draw random samples only once
		int errorCode;
		if((errorCode = uniformlyRandomlySelectDistance(sampledData, os_selected, ind_ch)) < 0) {
			fprintf(stderr, "OS[%s] @Ind_Ch[%d] uniformlyRandomlySelectDistance() returns error[%d]", os_selected->mDevName.c_str(), ind_ch, errorCode);
			return errorCode;
		}

		int totalSampledDataNum = 0;

		for(int i=0; i<CALIB_NUM_DISTANCE_BINS; i++) {

			totalSampledDataNum += sampledData[i].size();

			//printf("OmniSensrs[%s]: (CALIB_NUM_DISTANCE_BINS, i) = (%d, %d), sampledData[%d].size() = %d\n", os_selected->mDevName.c_str(), CALIB_NUM_DISTANCE_BINS, i, i, sampledData[i].size());

			if(sampledData[i].size() > 0)
				for(list<pair<int, float> >::iterator iit = sampledData[i].begin(); iit != sampledData[i].end(); ++iit) {
						pair<int, float> rd = *iit;

						if(rd.second == 0) continue;

//						printf("OS[%s]: sampledData[%d] (rss, dist, log(dist)) = (%d, %.2f, %.3f)\n", os_selected->mDevName.c_str(), i, rd.first, rd.second, log(rd.second));

	//						if(residualSqrSum_avg > 0) {
	//							// -- line equeation: y = x(0) * x + x(1) ==> x(0)*x - y + x(1) = 0
	//							//distError = pow(distanceFromPointToLine(x(0), -1, x(1), rd.first, rd.second), 2);
	//							//distError = pow(x(0)*rd.first + x(1) - rd.second, 2);
	//							distError = pow(x(0)*rd.first + x(1) - rd.second, 4);
	//							//distError = fabs(x(0)*rd.first + x(1) - rd.second);
	//							//distError = sqrt(x(0)*rd.first + x(1) - rd.second);
	//						}

	//						if(residualSqrSum_avg == 0 || (residualSqrSum_avg != 0 && distError < residualSqrSum_avg)
	//							//distError < (residualSqrSum_avg + residualSqrSum_max)*0.5
	//							//distError < residualSqrSum_max*0.8
	//						) {
							//RSS(0) = rd.first;
							RSS(0) = rd.first - mWifiCalibRssOffset;
							data_samples.push_back(make_pair(RSS, log(rd.second)));

							//printf("data_samples [%d]th: (RSS, rd.first, rd.second, log(rd.second)) = (%d, %d, %f, %f)\n", (int)data_samples.size(), (int)RSS(0), (int)rd.first, rd.second, log(rd.second));

	//						}
				}

			//printf("1 data_samples.size() = %d\n", data_samples.size());

		}
//	}

	double initX = 1.0;

	x(0) = initX;
	x(1) = initX;

	if((int)data_samples.size() > mCalibDataSamplesForLeastSquare_min) {

//		if(mDebugCalib)
//			printf("OS[%s] Enters into least_square: (data_samples.size(), totalSampledDataNum) = (%d, %d)\n", os_selected->mDevName.c_str(), (int)data_samples.size(), totalSampledDataNum);

				// -- (3) Estimate the location of the device that minimizes the sum of error distance using non-linear least square

				try {
					// Now let's use the solve_least_squares_lm() routine to estimate the best position in trilateration

				pthread_mutex_lock(&gMutex);
					// -- Returned value is the total error distance from the estimated line
					residualSqrSum_avg = dlib::solve_least_squares_lm(dlib::objective_delta_stop_strategy(1e-5),
												   calibFittingModelResidual,
													//calibFittingModelResidual_weighted,
												    dlib::derivative(calibFittingModelResidual),
													//dlib::derivative(calibFittingModelResidual_weighted),
												   //calibFittingModelResidualDerivative,
												   data_samples, x);

	//				residualSqrSum_avg = dlib::solve_least_squares(dlib::objective_delta_stop_strategy(1e-3),
	//									   	   	   calibFittingModelResidual,
	//									   	   	   //dlib::derivative(residual),
	//									   	   	   calibFittingModelResidualDerivative,
	//									   	   	   data_samples, x);

				pthread_mutex_unlock(&gMutex);

				} catch (std::exception& e) {
					cout << e.what() << endl;

					continue;
				}


//		if(mDebugCalib)
//			printf("OS[%s] Exit from least_square: x = (%f %f) \n", os_selected->mDevName.c_str(), (double)x(0), (double)x(1));

//		num = 0;
//		residualSqrSum_avg = 0;
//		residualSqrSum_max = 0;

		// -- When drawing uniformly random samples over distances

		// -- Compute the total error distance from the estimated line
//		if(x(0) != initX && x(1) != initX) {
//
//			if(SIMULTANEOUS_LOCALIZATION_AND_TRACKING == true) {
//
//
//			} else {
//
////					for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it) {
////						WiFiSensor_short_Ptr os = *it;
////
////						if(os_selected != NULL && os_selected != os)
////							continue;
//
////						for(int i=0; i<CALIB_NUM_DISTANCE_BINS; i++) {
////
////							//printf("sampledData[%d].size() = %d\n", i, sampledData[i].size());
////
////							for(list<pair<int, float> >::iterator ite = sampledData[i].begin(); ite != sampledData[i].end(); ++ite) {
////								pair<int, float> rd = *ite;		// -- Pair of (rss, dist [cm])
////
////									// -- line equeation: y = x(0) * x + x(1) ==> x(0)*x - y + x(1) = 0
////									//double distSqr = pow(distanceFromPointToLine(x(0), -1, x(1), rd.first, rd.second), 2);
////									double distSqr = pow(x(0)*(rd.first - mWifiCalibRssOffset) + x(1) - rd.second, 2);
////									//double distSqr = pow(x(0)*(rd.first - mWifiCalibRssOffset) + x(1) - rd.second, 4);
////									//double distSqr = fabs(x(0)*rd.first + x(1) - rd.second);
////									//double distSqr = sqrt(x(0)*rd.first + x(1) - rd.second);
////									residualSqrSum_avg += distSqr;
////									residualSqrSum_max = residualSqrSum_max > distSqr ? residualSqrSum_max : distSqr;
////									num++;
////
////									//printf("(num, rd.first, rd.second, distSqr) = (%d, %d, %.0f, %.0f)\n", num, rd.first, rd.second, distSqr);
////							}
////						}
////					}
//			}
//		}
	} else {
		if(mDebugCalib)
			printf("OS[%s] Channel: %d, Passes least_square: since (data_samples.size(), totalSampledDataNum) = (%d, %d)\n", os_selected->mDevName.c_str(), ind_ch, (int)data_samples.size(), totalSampledDataNum);

		return ERROR_TOO_SMALL_DATA_SAMPLES_FOR_CALIBRATION_FUNCTION_FITTING;
	}


//		if(num > 0)
//			residualSqrSum_avg /= (double)num;
//		else
//			residualSqrSum_avg = numeric_limits<double>::max();

		if(residualSqrSum_avg_min > residualSqrSum_avg + 0.1) {
			residualSqrSum_avg_min = residualSqrSum_avg;

			best_x = x;

			//printf("OS[%s] (%d)th try: (residualSqrSum_avg_min, residualSqrSum_avg, num) = (%.3f, %.3f, %d) @ [%d]th try\n", os_selected->mDevName.c_str(), try_cnt, residualSqrSum_avg_min, residualSqrSum_avg, num, try_cnt);

			try_cnt = 0;
			total_try_cnt++;
		} else {
			//printf("OS[%s] (a,b)= %f %f, (residualSqrSum_avg_min, residualSqrSum_avg, num) = (%.3f, %.3f, %d) @ [%d]th try\n", os_selected->mDevName.c_str(), x(0), x(1), residualSqrSum_avg_min, residualSqrSum_avg, num, try_cnt);
			try_cnt++;
			total_try_cnt++;
		}

		// -- Stop condition
		if(try_cnt > mCalibRansacTry_max || total_try_cnt > mCalibRansacTry_max*3)
		{

			if((best_x(0) == initX && best_x(1) == initX)
					|| best_x(0) == 0 || fabs(best_x(0)) > 9999999
					|| best_x(1) == 0 || fabs(best_x(1)) > 9999999
					)
				return ERROR_NO_BEST_CALIB_PARAM_FOUND;

			pthread_mutex_lock(&gMutex);

			os_selected->mCalib_a[ind_ch] = best_x(0);
			os_selected->mCalib_b[ind_ch] = best_x(1);

			pthread_mutex_unlock(&gMutex);

			// -- Debug
			if(mDebugCalib) {
				printf("Node[%s] @Ch[%d] Best Calib(a,b) = (%f, %f) w/ residualSqrSum_avg_min = %f when (rssOffset = %d)\n", os_selected->mDevName.c_str(), ind_ch, os_selected->mCalib_a[ind_ch], os_selected->mCalib_b[ind_ch], residualSqrSum_avg_min, mWifiCalibRssOffset);

//				for(int rss = -30; rss >= -90 ; rss -= 5) {
//					float dist = exp(best_x(0)*(MIN(rss - mWifiCalibRssOffset, -5)) + best_x(1));
//					printf("Node[%s] @Ind_Ch[%d]: rss = %d --> dist = %.2f [meter]\n", os_selected->mDevName.c_str(), ind_ch, rss, dist/100.0f);
//				}
			}

			residualSqrSum_avg_return = residualSqrSum_avg_min;
			return SUCCESS;
		}

	}
}

// -- Replace the existing calib param file with new results
void WiFi_Shopper_Tracker::SaveWiFiCalibResults(WiFiSensor_short_Ptr os, int ch) {

	if(os == NULL) return;

	// -- Erase the existing file
//	FILE *fp_tr = fopen(os->mCalibParamFilename.c_str(), "w");
//	if(fp_tr != NULL)
//		fclose(fp_tr);

	// -- Open a measurement log file
	FILE *fp_tr = fopen(os->mCalibParamFilename.c_str(), "a+");

	// -- TODO: Change SN to DevName
	if(fp_tr != NULL) {
		if(mDebugCalib) {
			printf("(DevName,SN,channel,a,b,RssOffset)= %s %s %d %f %f %d\n", os->mDevName.c_str(), os->mSerialNumber.c_str(), ch, os->mCalib_a[ch], os->mCalib_b[ch], mWifiCalibRssOffset);
		}
		fprintf(fp_tr, "(DevName,SN,channel,a,b,RssOffset)= %s %s %d %f %f %d\n", os->mDevName.c_str(), os->mSerialNumber.c_str(), ch, os->mCalib_a[ch], os->mCalib_b[ch], mWifiCalibRssOffset);

		fclose(fp_tr);
	}

}

void WiFi_Shopper_Tracker::SaveWiFiCalibResultVisualization(WiFiSensor_short_Ptr os, int ch) {
	if(os == NULL) return;

	// -- Show RSS-to-distance mapping function graph

		int min_rss = -100;
		int img_width = 400;	// -- TODO: not hard-code here
		int img_height = 300;	// -- TODO: not hard-code here

		float scale_w = img_width / (float)CALIB_MAX_SIGNAL_DISTANCE;
		float scale_h = img_height / (float)abs(min_rss);

		Mat img_func( img_height, img_width, CV_8UC3, CV_RGB(255, 255, 255));
		char mImg_func_name[99];

		sprintf(mImg_func_name,"[%s] @Ind_Ch[%d] (Dist,Rss) Function", os->mDevName.c_str(), ch);
		//namedWindow(mImg_func_name, CV_WINDOW_AUTOSIZE);

		{
			// -- Compute some sample points to draw the fitted curve on the graph
			list< pair<float, float> > rss2dist;
			for(int dist = 10; dist < CALIB_MAX_SIGNAL_DISTANCE; dist += 20) {
				// -- rss = (log(d) - b) / a
				float r = (float)((log(dist) - (double)os->mCalib_b[ch]) / (double)os->mCalib_a[ch]) + mWifiCalibRssOffset;

				rss2dist.push_back(make_pair(dist, r));
			}

			// -- Draw the fitted curve (i.e., RSS-to-Distance mapping function) in a piece-wide linear manner
			list<pair<float,float> >::iterator ite = rss2dist.begin();
			pair<float,float> rd_prev = *ite;
			++ite;
			for(; ite != rss2dist.end(); ++ite) {
				pair<float,float> rd = *ite;
				//printf("(dist,rss) = (%d, %d)\n", (int)(rd.first*scale_w), (int)((rd.second+100)*scale_h));
				line(img_func, cvPoint(rd_prev.first*scale_w + 0.5, img_height - (rd_prev.second+100)*scale_h + 0.5), cvPoint(rd.first*scale_w + 0.5, img_height - (rd.second+100)*scale_h + 0.5), CV_RGB(255,0,0), 2, CV_AA);
				rd_prev = rd;
			}
		}

		// -- Mark the distance bins
//		for(int i=0; i<CALIB_NUM_DISTANCE_BINS; i++) {
//			line(img_func, cvPoint(i*(img_width/CALIB_NUM_DISTANCE_BINS), 0), cvPoint(i*(img_width/CALIB_NUM_DISTANCE_BINS), img_height), CV_RGB(200,200,200), 1, CV_AA);
//		}

		// -- Mark the distance at every 5 meter
//		for(int dist = 0; dist < CALIB_MAX_SIGNAL_DISTANCE; dist += 500) {
//			line(img_func, cvPoint(dist*scale_w, 0), cvPoint(dist*scale_w, img_height), CV_RGB(200,200,200), 1, CV_AA);
//		}

		// -- Mark the RSS at every 20 db
		for(int rss = 0; rss < abs(min_rss); rss += 20) {
			line(img_func, cvPoint(0, rss*scale_h), cvPoint(img_width, rss*scale_h), CV_RGB(200,200,200), 1, CV_AA);
		}


		// -- Mark all the measurements on the graph
		int rad, dist;
		// -- Show sampled measurements per distance bin
		if(1) {
			for(int j=0; j<CALIB_NUM_DISTANCE_BINS; j++) {
				int r = rand()%200;
				int g = rand()%200;
				int b = rand()%200;

					for(list<calibMeas_t>::iterator iter = os->mCalibMeas_sampled[ch][j].begin(); iter != os->mCalibMeas_sampled[ch][j].end(); ++iter) {
						calibMeas_t *me = &*iter;

						rad = (me->rss - min_rss)*scale_h;
						dist = me->dist*scale_w;

						circle(img_func, cvPoint(dist, img_height - rad), 3, CV_RGB(r,g,b), -1, 16, 0);
					}
			}

		// -- Show measurements	per peer node
		} else {
			if(os->mCalibMeas_per_peer[ch].size() > 0)
				for(list<calibMeasPerPeer_t>::iterator it = os->mCalibMeas_per_peer[ch].begin(); it != os->mCalibMeas_per_peer[ch].end(); ++it) {
					calibMeasPerPeer_t *measList_per_peer = &*it;

					int r = rand()%200;
					int g = rand()%200;
					int b = rand()%200;

					if(measList_per_peer->measBuffer.size() > 0)
						for(list<calibMeas_t>::iterator iter = measList_per_peer->measBuffer.begin(); iter != measList_per_peer->measBuffer.end(); ++iter) {
							calibMeas_t *me = &*iter;

							rad = (me->rss - min_rss)*scale_h;
							dist = me->dist*scale_w;
							CvPoint p = cvPoint(dist, img_height - rad);

							circle(img_func, p, 3, CV_RGB(r,g,b), -1, 16, 0);


							// -- Mark the angular difference between the source peer and the receiver node in counter-clock wise direction
							if(iter == measList_per_peer->measBuffer.begin()) {
								// -- [-180, 180] range
								CvPoint p1 = CvPoint(100, 0);		// -- X-axis vector
								CvPoint p2 = CvPoint(os->mSensorPos_x - me->x, os->mSensorPos_y - me->y);

								double angle = AngleInTwoPoints(p1.x, p1.y, p2.x, p2.y);

								char text0[99]; sprintf(text0, "%d", (int)angle);
								putText(img_func, text0, cvPoint(dist+5, img_height - rad), 0, 0.3, CV_RGB(0,50,255), 1, CV_AA);
							}
						}
				}
		}

		// -- Mark standard deviation line on the graph
//		for(list<fingerprint_t>::iterator iit = radioMap.begin(); iit != radioMap.end(); ++iit) {
//			fingerprint_t fp = *iit;
//
//			for(list<calibOmniSensr_t>::iterator itt = fp.sensors.begin(); itt != fp.sensors.end(); ++itt) {
//				calibOmniSensr_t os = *itt;
//
//				if(os_selected->addr.compare(os.addr) == 0 || os_selected->serialNum.compare(os.serialNum) == 0) {
//
//					//float dist = exp(os.calib_a*os.mu + os.calib_b);
//					float dist = sqrt(pow(fp.x - os_selected->x,2) + pow(fp.y - os_selected->y,2));
//
//					int r = (os.mu+100)*scale_h;
//					int d = dist*scale_w;
//
//					line(img_func, cvPoint(d, img_height - r - os.std*scale_h), cvPoint(d, img_height - r + os.std*scale_h), CV_RGB(0,0,255), 1, CV_AA);
//					line(img_func, cvPoint(d-3, img_height - r - os.std*scale_h), cvPoint(d+3, img_height - r - os.std*scale_h), CV_RGB(0,0,255), 1, CV_AA);
//					line(img_func, cvPoint(d-3, img_height - r + os.std*scale_h), cvPoint(d+3, img_height - r + os.std*scale_h), CV_RGB(0,0,255), 1, CV_AA);
//					break;
//				}
//			}
//		}

		// -- Add labels for Y axis (i.e., RSS)
		{
			char text0[99]; sprintf(text0, "RSS @Ind_Channel[%d]", ch);
			putText(img_func, text0, cvPoint(60, 18), 0, 0.5,CV_RGB(255,100,0), 1, CV_AA);
			putText(img_func, "0 [dB]", cvPoint(5, 15), 0, 0.4,CV_RGB(255,0,0), 1, CV_AA);

			char text1[99]; sprintf(text1, "%d [dB]", min_rss);
			putText(img_func, text1, cvPoint(5, img_height-30), 0, 0.4,CV_RGB(255,0,0), 1, CV_AA);
		}

		// -- Add labels for X axis (i.e., Distance)
		{
			putText(img_func, "Distance", cvPoint(img_width - 70, img_height-25), 0, 0.5,CV_RGB(0,100,255), 1, CV_AA);
			putText(img_func, "0 [m]", cvPoint(5, img_height-8), 0, 0.4,CV_RGB(0,0,255), 1, CV_AA);

			char text1[99]; sprintf(text1, "%d [m]", (int)(CALIB_MAX_SIGNAL_DISTANCE/100.0f));
			putText(img_func, text1, cvPoint(img_width - 50, img_height-8), 0, 0.4,CV_RGB(0,0,255), 1, CV_AA);
		}

		//imshow(mImg_func_name, img_func);

		// -- Write image
		{
			ostringstream filepathNname;
			filepathNname << mCalibFilePath.str() << "/" << mImg_func_name << ".png";
			imwrite(filepathNname.str(), img_func);
		}
}


void WiFi_Shopper_Tracker::loadCalibrationParams() {

	for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it)
	{
		WiFiSensor_short_Ptr os = *it;

		if(os == NULL) continue;

		FILE *fp = fopen(os->mCalibParamFilename.c_str(), "r+");

		for(int ch=0; ch<NUM_CHANNELS_TO_CAPTURE; ch++) {
			os->mCalib_a[ch] = 0;
			os->mCalib_b[ch] = 0;
		}

		if(fp == NULL) {
			fprintf(stderr, "Error opening Calib params file: %s\n", os->mCalibParamFilename.c_str());
		} else {

		    char buffer[MAX_BUFFER];

			// -- Read from remote broker and publish to local broker
		    string lineInput;
		    vector<string> measBuffer;
			while(fgets(buffer, MAX_BUFFER, fp)) {	// -- Reads a line from the pipe and save it to buffer
				lineInput.clear();
				lineInput.append(buffer);

				// -- If there's any comment, then discard the rest of the line
				std::size_t found = lineInput.find_first_of("#");
				// -- If '#' is found, then discard the rest.
				if(found != string::npos)
					lineInput.resize(found);

				// -- Break the line input into words and save it into a word vector
				istringstream iss (lineInput);
				vector<string> v;
				string s;
				while(iss >> s)
					v.push_back(s);

				// -- If the size of word vector is greater than one, then it should be the first line of message containing {topic, #SN, IP}
				if(v.size() == 7 && v[0].c_str()[0] == '(') {

					// -- (DevName, SN,channel,a,b,RssOffset)= cam001 A0000000011 0 -0.194135 1.857436 -33
					WiFiSensor_short_Ptr ap = findRunningWiFiSensor(v[1]);

					if(ap != NULL) {
						int channel = atoi(v[3].c_str());
						ap->mCalib_a[channel] = atof(v[4].c_str());
						ap->mCalib_b[channel] = atof(v[5].c_str());
						mWifiCalibRssOffset = atoi(v[6].c_str());
					}
				}
			}

			fclose(fp);

			// -- Print the calib parameters
			if(mDebugCalib) {
				for(int ch=0; ch<NUM_CHANNELS_TO_CAPTURE; ch++)
					printf("OmniSensr[%s] @Ind_Channel[%d] Calibration parameters for trilateration (a,b) = (%f, %f)\n", os->mDevName.c_str(), ch, os->mCalib_a[ch], os->mCalib_b[ch]);
			}
		}
	}

	// -- TEMP
	//mWifiCalibRssOffset = -47;
}



// -- Perform calibration again for all nodes with multiple RSS offsets to find the best RSS offset
int WiFi_Shopper_Tracker::findBestRssOffset() {

	double residualSqrSum_avg_min_sum_avg_min =  numeric_limits<double>::max();
	int WifiCalibRssOffset_best = 100;

	for(int rssOffset = mFindBestRssOffsetMaxRss; rssOffset >= mFindBestRssOffsetMinRss; rssOffset += -2) {
		mWifiCalibRssOffset = rssOffset;
		vector<double> residualSqrSum_avg_vec;

		if(mDebugCalib)
			printf("-- findBestRssOffset(): rssOffset = %d -- \n", rssOffset);

		for(int ch=0; ch<NUM_CHANNELS_TO_CAPTURE; ch++) {

			for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it) {
				WiFiSensor_short_Ptr os = *it;

				if(os == NULL) continue;

//				os->mCalib_a[ch] = 0;
//				os->mCalib_b[ch] = 0;

				double residualSqrSum_avg;

				int errorCode = generateRSStoDistanceMappingFunction(os, ch, residualSqrSum_avg);

				if(errorCode == SUCCESS) {
					residualSqrSum_avg_vec.push_back(residualSqrSum_avg);
				}
			}
		}

		//if(cnt > mRunningWiFiSensor.size()*NUM_CHANNELS_TO_CAPTURE*0.4)
		if(residualSqrSum_avg_vec.size() > 0)
		{
			double residualSqrSum_avg_min_sum_avg = Median(residualSqrSum_avg_vec.begin(), residualSqrSum_avg_vec.end());

			if(mDebugCalib)
				printf("^^^ findBestRssOffset(): rssOffset = %d: Median value of residualSqrSum_avg = %f\n", rssOffset, residualSqrSum_avg_min_sum_avg);

			if(residualSqrSum_avg_min_sum_avg_min > residualSqrSum_avg_min_sum_avg) {
				residualSqrSum_avg_min_sum_avg_min = residualSqrSum_avg_min_sum_avg;
				WifiCalibRssOffset_best = rssOffset;

				if(mDebugCalib)
					printf("------ findBestRssOffset(): (cnt, residualSqrSum_avg_min_sum_avg) = (%d, %f) ------ \n", (int)residualSqrSum_avg_vec.size(), residualSqrSum_avg_min_sum_avg);

				for(int ch=0; ch<NUM_CHANNELS_TO_CAPTURE; ch++) {
					for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it) {
						WiFiSensor_short_Ptr os = *it;

						if(os == NULL) continue;

						os->mCalib_a_best[ch] = os->mCalib_a[ch];
						os->mCalib_b_best[ch] = os->mCalib_b[ch];

					}
				}
			}
		}

	}

	if(mDebugCalib) {
		if(WifiCalibRssOffset_best <= 0) {
			printf("\n####################################################################################################\n");
			printf("#### Best WifiCalibRssOffset is [[%d]] with residualSqrSum_avg_min_sum_avg_min = %.4f #######\n", WifiCalibRssOffset_best, residualSqrSum_avg_min_sum_avg_min);
			printf("####################################################################################################\n");

		} else {
			printf("#### COULD NOT find the Best WifiCalibRssOffset ####\n");
		}
	}

	return WifiCalibRssOffset_best;

}

void WiFi_Shopper_Tracker::SendWiFiCalibProgress() {


		mClock.updateTime();

		string date = mClock.mDateOnly_str;
		string time = mClock.mTimeOnly_str;


		stringstream jsonStr;

		jsonStr << "{"
				<< "\"store_name\" : \"" << mIoTGateway.GetMyStoreName_dash() << "\","
				<< "\"date\" : \"" << date << "\","
				<< "\"time\" : \"" << time << "\","
				<< "\"calibElapsedTime\" : " << (int)mCalibElapsedTime << ","
				<< "\"calibDuration\" : " << mCalibPeriod_max*60 << ","
				<< "\"CalibProgress\" : {";

				for(deque<WiFiSensor_short_Ptr >::iterator jt = mRunningWiFiSensor.begin(); jt != mRunningWiFiSensor.end(); ++jt) {
					WiFiSensor_short_Ptr os = *jt;
					if(os == NULL) continue;

					jsonStr << "\"" << os->mDevName <<"\":\"";


					for(int i=0; i<CALIB_NUM_DISTANCE_BINS; i++) {
						jsonStr << os->mCalibMeas[0][i].size() <<",";
					}

					if(jt != mRunningWiFiSensor.end()-1)
						jsonStr << "\",";
					else
						jsonStr << "\"";
				}


		jsonStr	<< "}"
				<< "}";


		if(mDebugCalib) {
			cout << "SendWiFiCalibProgress() Msg to be Sent in JSON: " << "\n";
			cout << setw(2) << jsonStr.str() << "\n\n";
		}

		//bool ret = false;

		try {

			json trajectoryMsg;
			trajectoryMsg << jsonStr;

			// -- Publish/report the real-time tracking results to AWS IoT Cloud
			{

				jsonStr.clear();
				jsonStr.str("");

				// -- Note: Data message must be in JSON format to be processed by Rules Engine at AWS IoT cloud
				//jsonStr << std::setw(2) << trajectoryMsg << '\n';
				jsonStr << trajectoryMsg << '\n';

				// -- Put into a message buffer in IoT Gateway for transmission to AWS Cloud via AWS_IOT_Connector module
				// -- The data message will be sent to a topic named by 'data/<app_id>/<store_name_prefix>/<store_name_suffix>/<device_name>/<suffix>/<subtopic if there is>' (e.g., data/wifiRss/vmhq/16801/cam001/001/calib)
				mIoTGateway.SendData("/calibProgress",jsonStr.str());

				//ret = true;
			}

		} catch (std::invalid_argument& e) {		// -- Handle exception when the received msg does not comply with JSON standard
			// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
	    	std::cout << "invalid_argument: " << e.what() << '\n';
		} catch (std::out_of_range& e) {	// -- Handle exception when a non-existing filed is accessed.
			// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
			std::cout << "out of range: " << e.what() << '\n';
		} catch (std::domain_error& e) {	// -- Handle exception when an incompatible conversion is attempted (e.g., float --> string).
			// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
		    std::cout << "domain error: " << e.what() << '\n';
		}
}

void WiFi_Shopper_Tracker::SendWiFiCalibResults() {
	// -- When it's done, then send the latest calibration results to the cloud

		mClock.updateTime();

		string date = mClock.mDateOnly_str;
		string time = mClock.mTimeOnly_str;

		stringstream jsonStr;

		jsonStr << "{"
				<< "\"store_name\" : \"" << mIoTGateway.GetMyStoreName_dash() << "\","
				<< "\"date\" : \"" << date << "\","
				<< "\"time\" : \"" << time << "\","
				<< "\"rssOffset\" : " << mWifiCalibRssOffset << ","
				<< "\"CalibParams\" : {";

					/*
					 *
					 * "<dev_name>" : {
					 * "ch1":[<param_a>,<param_b>],
					 * "ch6":[<param_a>,<param_b>],
					 * "ch11":[<param_a>,<param_b>]
					 * }
					 */

				for(deque<WiFiSensor_short_Ptr >::iterator jt = mRunningWiFiSensor.begin(); jt != mRunningWiFiSensor.end(); ++jt) {
					WiFiSensor_short_Ptr os = *jt;
					if(os == NULL) continue;

					jsonStr << "\"" << os->mDevName <<"\":{";

					jsonStr << "\"ch1\":\"" << os->mCalib_a[0] << "," << os->mCalib_b[0] << "\",";
					jsonStr << "\"ch6\":\"" << os->mCalib_a[1] << "," << os->mCalib_b[1] << "\",";
					jsonStr << "\"ch11\":\"" << os->mCalib_a[2] << "," << os->mCalib_b[2] << "\"";

					if(jt != mRunningWiFiSensor.end()-1)
						jsonStr << "},";
					else
						jsonStr << "}";
				}


		jsonStr	<< "}"
				<< "}";

		bool ret = false;

		try {

			json trajectoryMsg;
			trajectoryMsg << jsonStr;

			// -- Publish/report the real-time tracking results to AWS IoT Cloud
			{

				jsonStr.clear();
				jsonStr.str("");

				// -- Note: Data message must be in JSON format to be processed by Rules Engine at AWS IoT cloud
				//jsonStr << std::setw(2) << trajectoryMsg << '\n';
				jsonStr << trajectoryMsg << '\n';

				// -- Put into a message buffer in IoT Gateway for transmission to AWS Cloud via AWS_IOT_Connector module
				// -- The data message will be sent to a topic named by 'data/<app_id>/<store_name_prefix>/<store_name_suffix>/<device_name>/<suffix>/<subtopic if there is>' (e.g., data/wifiRss/vmhq/16801/cam001/001/calib)
				mIoTGateway.SendData("/calibResult",jsonStr.str());

				ret = true;
			}

		} catch (std::invalid_argument& e) {		// -- Handle exception when the received msg does not comply with JSON standard
			// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
	    	//std::cout << "invalid_argument: " << e.what() << '\n';
		} catch (std::out_of_range& e) {	// -- Handle exception when a non-existing filed is accessed.
			// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
			//std::cout << "out of range: " << e.what() << '\n';
		} catch (std::domain_error& e) {	// -- Handle exception when an incompatible conversion is attempted (e.g., float --> string).
			// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
		   // std::cout << "domain error: " << e.what() << '\n';
		}

		// -- TODO: XXX
		if(mDebugCalib)
		{
			if(ret == false) {
				fprintf(stderr, "@@@ ERROR: Failed to send WiFiCalib Results: [%s]\n", jsonStr.str().c_str());
			}
			else {
				fprintf(stdout, "--- Sent WiFiCalib Results: [%s]\n", jsonStr.str().c_str());
			}
		}
}

// -- Ascending order (smaller one comes earlier): Compare the second element in {WiFiSensor_short_Ptr, estimatedDistToAP}
bool wayToSort_distance(pair<WiFiSensor_short_Ptr, double> a, pair<WiFiSensor_short_Ptr, double> b) { return a.second < b.second;}

void WiFi_Shopper_Tracker::WiFiCalibProcessing() {

	//double calibInitTime = gTimeSinceStart();
	//double prevTime_progress = calibInitTime;

	while(1) {

		double currTime = gTimeSinceStart();
		double timeElapsedFromCalibInit = currTime - mCalibStartTime;  // -- [sec]

        if(mKillRequestPending == true)
            pthread_exit(NULL);

//        // -- Periodically report the current status of the calibration
//        if(currTime - prevTime_progress > 10) {
//        	prevTime_progress = currTime;
//
//        	SendWiFiCalibProgress();
//        }

		bool isAllCalibrated = true;
		for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it) {
			WiFiSensor_short_Ptr os = *it;

			if(os == NULL) continue;

			// -- If the max calib time reached, then exit the process
			if(timeElapsedFromCalibInit > mCalibPeriod_max*60) {

				pthread_mutex_lock(&gMutex);
				for(int ch=0; ch<NUM_CHANNELS_TO_CAPTURE; ch++) {
					os->mWiFiCalibStarted[ch] = false;

					if(mDebugCalib)
						printf("[WiFi_Shopper_Tracker] OS[%s] @Ind_Ch[%d]: WiFi Calibration is FAILED!\n", os->mDevName.c_str(), ch);
				}
				pthread_mutex_unlock(&gMutex);

				isAllCalibrated = true;

				continue;
			}

			for(int ch=0; ch<NUM_CHANNELS_TO_CAPTURE; ch++) {	// -- For each of three channel
				double timeElapsedSinceLastMeas = numeric_limits<double>::max();

				for(int i=0; i<CALIB_NUM_DISTANCE_BINS; i++) {
					double timeElapsedSinceLastMeas_temp = currTime - os->mCalibMeas[ch][i].back().timestamp;
					if(timeElapsedSinceLastMeas > timeElapsedSinceLastMeas_temp) timeElapsedSinceLastMeas = timeElapsedSinceLastMeas_temp;
				}

				// -- See if we have enough measurements in many bins
				pthread_mutex_lock(&gMutex);
				os->mCalibMeasCollectionProgress[ch] = 0;
				int totalMeasCnt = 0;
				for(int i=0; i<CALIB_NUM_DISTANCE_BINS; i++) {
					if((int)os->mCalibMeas[ch][i].size() > mCalibMeasPerBin_min) {
						os->mCalibMeasCollectionProgress[ch]++;
						totalMeasCnt += os->mCalibMeas[ch][i].size();
					}
				}
				pthread_mutex_unlock(&gMutex);

				if(os->mWiFiCalibStarted[ch] == true && os->mIsCurrentlyReceivingMessages == true) {

					//printf("OS[%s] has %d meas. TtimeElapsedSinceLastMeas = %.3f\n", os->mDevName.c_str(), (int)os->mCalibMeas.size(), timeElapsedSinceLastMeas);


					// -- Starts WiFiCalib process if (1) the number of bins that have enough number of measurements (i.e., >mCalibMeasPerBin_min) is enough (i.e., >mCalibEnoughMeasBinsToKickOffCalib_min)
					// -- or (2) the total number of measurement received is much greater than a threshold (e.g., mCalibEnoughMeasBinsToKickOffCalib_min*mCalibMeasPerBin_min*2)
					if((os->mCalibMeasCollectionProgress[ch] >= mCalibEnoughMeasBinsToKickOffCalib_min)
							//|| totalMeasCnt >= mCalibEnoughMeasBinsToKickOffCalib_min*mCalibMeasPerBin_min*5
							//|| totalMeasCnt >= os->mTotalMeasCntThreshold[ch]
					) {
						printf("[WiFi_Shopper_Tracker] OS[%s] @Ind_Ch[%d] has %d meas. And has [%d] bins with enough measurements among total [%d] bins\n", os->mDevName.c_str(), ch, totalMeasCnt, os->mCalibMeasCollectionProgress[ch], CALIB_NUM_DISTANCE_BINS);
						// -- Calculate calibration parameters
						// -- Since it requires a heavy processing, we create a thread for that.

//						os->mCalib_a[ch] = 0;
//						os->mCalib_b[ch] = 0;

						double residualSqrSum_avg;

						int errorCode = generateRSStoDistanceMappingFunction(os, ch, residualSqrSum_avg);

						if(mDebugCalib)
							printf("Node[%s] @Ind_Ch[%d]: residualSqrSum_avg = %f\n", os->mDevName.c_str(), ch, residualSqrSum_avg);

						if(errorCode == SUCCESS) {

							if(mFindBestRssOffset == false) {
								os->mCalib_a_best[ch] = os->mCalib_a[ch];
								os->mCalib_b_best[ch] = os->mCalib_b[ch];
							}

							// -- Save the calibration results
							SaveWiFiCalibResults(os, ch);

							// -- Show and/or save the calibration result visualization.
							SaveWiFiCalibResultVisualization(os, ch);

							if(mDebugCalib)
								printf("[WiFi_Shopper_Tracker] OS[%s] @Ind_Ch[%d]: WiFi Calibration is DONE!\n", os->mDevName.c_str(), ch);

							pthread_mutex_lock(&gMutex);
							os->mWiFiCalibStarted[ch] = false;
							pthread_mutex_unlock(&gMutex);
						} else if(errorCode == ERROR_TOO_SMALL_DATA_SAMPLES_FOR_CALIBRATION_FUNCTION_FITTING) {

							// -- Increase the # of measurement collected
							os->mTotalMeasCntThreshold[ch] *= 1.1;

							os->mCalibFailCnt[ch]++;

							isAllCalibrated = false;

							if(mDebugCalib)
								printf("[WiFi_Shopper_Tracker] OS[%s] @Ind_Ch[%d]: WiFi Calibration is tried, but failed! becauseof Error[%d], mCalibFailCnt = %d, os->mTotalMeasCntThreshold = %d\n", os->mDevName.c_str(), ch, errorCode, os->mCalibFailCnt[ch], os->mTotalMeasCntThreshold[ch]);
						} else {

							os->mCalibFailCnt[ch]++;
							isAllCalibrated = false;

							if(mDebugCalib)
								printf("[WiFi_Shopper_Tracker] OS[%s] @Ind_Ch[%d]: WiFi Calibration is tried, but failed! becauseof Error[%d], mCalibFailCnt = %d\n", os->mDevName.c_str(), ch, errorCode, os->mCalibFailCnt[ch]);
						}

						if(os->mCalibFailCnt[ch] > mCalibFailCntAllowed_max) {
							pthread_mutex_lock(&gMutex);
							os->mCalib_a[ch] = 0;
							os->mCalib_b[ch] = 0;
							os->mCalib_a_best[ch] = 0;
							os->mCalib_b_best[ch] = 0;

							os->mWiFiCalibStarted[ch] = false;
							pthread_mutex_unlock(&gMutex);
						}


					} else if(timeElapsedSinceLastMeas > mCalibMeasWaitPeriod_max && totalMeasCnt < mCalibMeasPerBin_min) {

						// -- If a node does not get any more measurements, then force it to stop calibration.
						if(mDebugCalib)
							printf("[WiFi_Shopper_Tracker] OS[%s] @Ind_Ch[%d]: WiFi Calibration is FAILED!\n", os->mDevName.c_str(), ch);

						pthread_mutex_lock(&gMutex);
						os->mWiFiCalibStarted[ch] = false;
						pthread_mutex_unlock(&gMutex);

					} else {
						isAllCalibrated = false;
					}

				}
			}

		}


		if(isAllCalibrated == true)
		{


			// -- Wait until the other thread finishes its turn.
			do {
				pthread_mutex_lock(&gMutex);
				bool isOtherThreadProcessing = mIsThereSomeProcessing;
				pthread_mutex_unlock(&gMutex);

				if(isOtherThreadProcessing > 0) usleep(5000);
				else break;
			} while(true);

			pthread_mutex_lock(&gMutex);
			mIsThereSomeProcessing = (1<<CALIBRATION);
			pthread_mutex_unlock(&gMutex);


			if(mDebugCalib)
				printf("[WiFi_Shopper_Tracker] WiFi Calibration is done for ALL NODES ! It took %.2f[sec]\n", gTimeSinceStart() - mCalibStartTime);

			// -- Find the best RSS offset and best calibration parameters with that. Store them.
			if(mFindBestRssOffset == true) {
				int initRssOffset = mWifiCalibRssOffset;

				// -- Perform calibration again for all nodes with multiple RSS offsets to find the best RSS offset
				int WifiCalibRssOffset_best = findBestRssOffset();

				if(WifiCalibRssOffset_best <= 0)
					mWifiCalibRssOffset = WifiCalibRssOffset_best;
				else
					mWifiCalibRssOffset = initRssOffset;
			}

			// -- If we couldn't get calib params, then use nearby node's calib params instead as an approximation;
			{

				// -- Find the nearest peer's calib param.
				for(int ch=0; ch<NUM_CHANNELS_TO_CAPTURE; ch++) {
					for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it) {
						WiFiSensor_short_Ptr os = *it;
						if(os == NULL) continue;

						if(os->mCalib_a_best[ch] == 0 || os->mCalib_b_best[ch] == 0) {

							bool isFound = false;

							// -- First, see if the other channel has the calib param. If so, use it.
							for(int c=0; c<NUM_CHANNELS_TO_CAPTURE; c++) {

								if(os->mCalib_a_best[c] != 0 && os->mCalib_b_best[c] != 0) {
									os->mCalib_a[ch] = os->mCalib_a_best[c];
									os->mCalib_b[ch] = os->mCalib_b_best[c];

//									// -- Save the calibration results
//									SaveWiFiCalibResults(os, ch);
//
//									// -- Show and/or save the calibration result visualization.
//									SaveWiFiCalibResultVisualization(os, ch);

									isFound = true;
									break;
								}
							}

							if(mDebugCalib)
								printf("Node[%s] @In_ch[%d]: Couldn't find calib params\n", os->mDevName.c_str(), ch);

							if(isFound == false) {
								// -- Compute distance to the peers
								pair<WiFiSensor_short_Ptr, double> peerList[mRunningWiFiSensor.size()];
								int cnt = 0;
								for(deque<WiFiSensor_short_Ptr >::iterator jt = mRunningWiFiSensor.begin(); jt != mRunningWiFiSensor.end(); ++jt) {
									WiFiSensor_short_Ptr peer = *jt;
									if(peer == NULL) continue;

									double distToPeer = numeric_limits<double>::max();

									if(peer->mCalib_a_best[ch] != 0 && peer->mCalib_b_best[ch] != 0) {
										distToPeer = sqrt(pow(os->mSensorPos_x - peer->mSensorPos_x,2) + pow(os->mSensorPos_y - peer->mSensorPos_y,2));
									}

									peerList[cnt++] = make_pair(peer, distToPeer);
								}

								// -- Sort based on the distance
								sort(peerList, peerList + cnt, wayToSort_distance);

								if( peerList[0].second != numeric_limits<double>::max()) {

									if(mDebugCalib)
										printf("+++ Node[%s] @In_ch[%d]: Found nearby peer [%s] and will use its calib param (%f, %f) instead.\n", os->mDevName.c_str(), ch, peerList[0].first->mDevName.c_str(), peerList[0].first->mCalib_a_best[ch], peerList[0].first->mCalib_b_best[ch]);

									// -- Take the nearest available peer's calibration parameter
									for(int c=0; c<NUM_CHANNELS_TO_CAPTURE; c++) {
										os->mCalib_a[c] = peerList[0].first->mCalib_a_best[ch];
										os->mCalib_b[c] = peerList[0].first->mCalib_b_best[ch];
									}
								}
							}

						} else {

							os->mCalib_a[ch] = os->mCalib_a_best[ch];
							os->mCalib_b[ch] = os->mCalib_b_best[ch];
						}

						if(os->mCalib_a[ch] != 0 && os->mCalib_b[ch] != 0) {
							// -- Save the calibration results
							SaveWiFiCalibResults(os, ch);

							// -- Show and/or save the calibration result visualization.
							SaveWiFiCalibResultVisualization(os, ch);
						}
					}
				}
			}

			pthread_mutex_lock(&gMutex);
			mIsThereSomeProcessing = 0;
			pthread_mutex_unlock(&gMutex);

			SetTrackerOperationMode(WIFI_TRACKING);

			// -- When it's done, then send the latest calibration results to the cloud
			SendWiFiCalibResults();

			return;
		}

		usleep(1000*10);
	}
}

void* WiFi_Shopper_Tracker::WiFiCalibProcessingThread(void *apdata) {
	WiFi_Shopper_Tracker* pb = (WiFi_Shopper_Tracker*)apdata;

	pb->WiFiCalibProcessing();

	return NULL;
}


void WiFi_Shopper_Tracker::visualizeCalibrationProgress() {
	if(DEBUG_FUNCTION_CALL) printf("visualizeCalibrationProgress() is called. \n");

	int binHeight = 50;
	int binThickness = 10;

	CvScalar color;

	int ind_channel = 0;		// -- TODO: XXXXXXXXXX

	for(deque<WiFiSensor_short_Ptr >::iterator it = mRunningWiFiSensor.begin(); it != mRunningWiFiSensor.end(); ++it) {
		WiFiSensor_short_Ptr os = *it;
		if(os == NULL) continue;

        if(os->mWiFiCalibStarted[ind_channel] == true)
			for(int i=0; i<CALIB_NUM_DISTANCE_BINS; i++) {

				color = CV_RGB(0,0,0);

				float measRcvedPercentage = os->mCalibMeas[ind_channel][i].size()/(float)mCalibMeasPerBin_min;
				if(measRcvedPercentage >= 1.0f) {
					measRcvedPercentage = 1.0f;

					color = CV_RGB(0,0,255);
				} else if(measRcvedPercentage > 0) {

					color = CV_RGB(255,0,0);
				}

				// -- Show black background
				line(mImg_map, cvPoint(os->mSensorPos_x_map - binThickness*(CALIB_NUM_DISTANCE_BINS*0.5 - i - 0.5), os->mSensorPos_y_map - 15), cvPoint(os->mSensorPos_x_map - binThickness*(CALIB_NUM_DISTANCE_BINS*0.5 - i - 0.5), os->mSensorPos_y_map - 15 - binHeight), CV_RGB(0,0,0), binThickness, CV_AA,0);

				// -- Show the percentage of measurement received compared to mCalibMeasPerBin_min
				line(mImg_map, cvPoint(os->mSensorPos_x_map - binThickness*(CALIB_NUM_DISTANCE_BINS*0.5 - i - 0.5), os->mSensorPos_y_map - 15), cvPoint(os->mSensorPos_x_map - binThickness*(CALIB_NUM_DISTANCE_BINS*0.5 - i - 0.5), os->mSensorPos_y_map - 15 - binHeight*measRcvedPercentage), color, binThickness, CV_AA,0);
			}

	}



	if(DEBUG_FUNCTION_CALL) printf("visualizeCalibrationProgress() is ended. \n");
}

/*
 * main.cpp
 *
 *  Created on: Jul 3, 2014
 *      Author: pshin
 */

#include "WiFi_Shopper_Tracker.hpp"
#include <unistd.h>
pthread_mutex_t gMutex;

int main(int argc, char *argv[]) {
	fprintf(stderr, "Usage: $ ./WiFi_Shopper_Tracker {device_id_suffix} {(optional)show GUI or not (0 or 1)}\n");

	string trackerId_suffix = "001";
	bool showGUI;		// -- If true, show store map-based GUI/Visualization

	if(argc > 3) {
		exit(0);
	} else if(argc == 1) {
		showGUI = false;
	} else if(argc == 2) {
		trackerId_suffix = argv[1];
		showGUI = false;
	} else if(argc == 3) {
		trackerId_suffix = argv[1];
		showGUI = atoi(argv[2]);
	}

	fprintf(stdout, "New settings: (trackerId_suffix, showGUI) = (%s, %d)\n", trackerId_suffix.c_str(), showGUI);

	WiFi_Shopper_Tracker	wifi_tracker(trackerId_suffix);

	wifi_tracker.run(showGUI);

	fprintf(stderr, "[WiFi_Shopper_Tracker] Exited\n");

	return 1;
}

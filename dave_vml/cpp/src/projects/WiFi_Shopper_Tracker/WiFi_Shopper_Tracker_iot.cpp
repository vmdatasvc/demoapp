/*
 * WiFi_Shopper_Tracker_iot.cpp
 *
 *  Created on: May 23, 2016
 *      Author: paulshin
 */

#include "unistd.h"
#include <iomanip>

// -- AWS IoT SDK files for State Parameter Type definition
#include "aws_iot_error.h"
#include "aws_iot_shadow_json_data.h"

#include "modules/iot/IoT_Common.hpp"
#include "modules/iot/IoT_Gateway.hpp"
#include "modules/utility/TimeKeeper.hpp"

#include "projects/WiFi_Shopper_Tracker/WiFi_Shopper_Tracker.hpp"

extern pthread_mutex_t gMutex;
IoT_Gateway	WiFi_Shopper_Tracker::mIoTGateway;
unsigned int WiFi_Shopper_Tracker::mWiFiOperationMode = WIFI_TRACKING;

bool WiFi_Shopper_Tracker::mNeedToTurnOffCalib = false;
bool WiFi_Shopper_Tracker::mNeedToResetList_ap = false;
bool WiFi_Shopper_Tracker::mNeedToResetList_serviceAgent = false;
bool WiFi_Shopper_Tracker::mDebugIoT = true;
bool WiFi_Shopper_Tracker::mReportRealTimeTrackResult;

string WiFi_Shopper_Tracker::mAppType_Tracker;

// -- TODO: Don't know if we leave the app to parse the entire message or if we make a nested structure like Thing Shadow so that each app knows its own control field. For now, I will just leave the full freedom to the app.
void WiFi_Shopper_Tracker::IoT_ControlChannel_CallBack_func(string topic, string controlMsg) {

	// -- Parse the message as you wish and take actions if needed. The message must be in a JSON format
	json controlMsgJson;

	try {
		controlMsgJson = json::parse(controlMsg.c_str());

		if(mDebugIoT)
		{
			printf("+++++++ [WiFi_Shopper_Tracker] Control Message Received +++++++++++++++++++++++\n");
			std::cout << std::setw(2) << controlMsgJson << '\n';
			printf("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
		}

//		if(mDebugIoT)
//		{
//			//printf("******* Control Channel Msg Received *******\n%s\n********************************************\n", controlMsg.c_str());
//
//			// special iterator member functions for objects
////			cout << "Control Msg rsved in JSON: " << "\n";
////			cout << setw(4) << controlMsgJson << "\n\n";
//
//			// special iterator member functions for objects
//			for (json::iterator it = controlMsgJson.begin(); it != controlMsgJson.end(); ++it) {
//				string key = it.key();
//				ostringstream value;
//				value << it.value();
//
//				cout << key << " : " << value.str() << "\n";
//			}
//		}

		// -- Flatten the nested JSON into un-nested one
		json controlMsgJson_flat = controlMsgJson.flatten();

//		if(mDebugIoT)
//		{
//			cout << "Control Msg rsved in JSON (flattened): " << "\n";
//			cout << setw(4) << controlMsgJson_flat << "\n\n";
//		}


		// -- Try to access keys and get their values with a type
		// -- Note: Must catch the two exceptions {out_of_range, domain_error} whenever accessing a value.
		// -- 		Otherwise, it will exit the program if the type does not match or if the key does not exist.
		// -- ########### ADD USER-DEFINED ACTION BELOW #####################

			// -- Check if WiFi Calib command is set. If so, it will initiate packet injection for WiFi calibration. For example,
			if(controlMsgJson_flat.find("/wifiCalib/Mode") != controlMsgJson_flat.end()) {
				unsigned int wifiCalibMode = controlMsgJson_flat.at("/wifiCalib/Mode");

				pthread_mutex_lock(&gMutex);
				mNeedToTurnOffCalib = (wifiCalibMode == WIFI_TRACKING);
				mWiFiOperationMode = wifiCalibMode;
				pthread_mutex_unlock(&gMutex);
			}

			if(controlMsgJson_flat.find("/wifiTrack/ResetAgentList") != controlMsgJson_flat.end()) {
				bool needToResetList = controlMsgJson_flat.at("/wifiTrack/ResetAgentList");

				pthread_mutex_lock(&gMutex);
				mNeedToResetList_ap = needToResetList;
				mNeedToResetList_serviceAgent = needToResetList;
				pthread_mutex_unlock(&gMutex);
			}

			/*
			 * A template of JSON-type control msg for setting Kalman filter parameters

				{
					"wifiTrack": {
						"Kalman": {
							"RssProcNoise_var":1000000,
							"RssMeasNoise_var":10,
							"RssCovCoeff_pos_min":1,
							"RssCovCoeff_accel_min":1,
							"RssMaxStdAllowed":1000,
							"PosProcNoise_var":1000000,
							"PosMeasNoise_var":2,
							"PosCovCoeff_pos_min":0.5,
							"PosCovCoeff_acc_min":0.5
						}
					}
				}

			 */
			// -- Kalman filter parameters
			if(controlMsgJson_flat.find("/wifiTrack/Kalman/RssProcNoise_var") != controlMsgJson_flat.end()) {

				if(controlMsgJson_flat.find("/wifiTrack/Kalman/RssProcNoise_var") != controlMsgJson_flat.end()) { mKalmanRssProcessNoise_var = controlMsgJson_flat.at("/wifiTrack/Kalman/RssProcNoise_var"); }
				if(controlMsgJson_flat.find("/wifiTrack/Kalman/RssMeasNoise_var") != controlMsgJson_flat.end()) { mKalmanRssMeasurementNoise_var = controlMsgJson_flat.at("/wifiTrack/Kalman/RssMeasNoise_var"); }
				if(controlMsgJson_flat.find("/wifiTrack/Kalman/RssCovCoeff_pos_min") != controlMsgJson_flat.end()) { mKalmanRssCovCoeff_pos_min = controlMsgJson_flat.at("/wifiTrack/Kalman/RssCovCoeff_pos_min"); }
				if(controlMsgJson_flat.find("/wifiTrack/Kalman/RssCovCoeff_accel_min") != controlMsgJson_flat.end()) {mKalmanRssCovCoeff_accel_min = controlMsgJson_flat.at("/wifiTrack/Kalman/RssCovCoeff_accel_min"); }
				if(controlMsgJson_flat.find("/wifiTrack/Kalman/RssMaxStdAllowed") != controlMsgJson_flat.end()) {mKalmanRssMaxStdAllowed = controlMsgJson_flat.at("/wifiTrack/Kalman/RssMaxStdAllowed"); }

				if(controlMsgJson_flat.find("/wifiTrack/Kalman/PosProcNoise_var") != controlMsgJson_flat.end()) { mKalmanPosProcessNoise_var = controlMsgJson_flat.at("/wifiTrack/Kalman/PosProcNoise_var"); }
				if(controlMsgJson_flat.find("/wifiTrack/Kalman/PosMeasNoise_var") != controlMsgJson_flat.end()) { mKalmanPosMeasurementNoise_var = controlMsgJson_flat.at("/wifiTrack/Kalman/PosMeasNoise_var"); }
				if(controlMsgJson_flat.find("/wifiTrack/Kalman/PosCovCoeff_pos_min") != controlMsgJson_flat.end()) { mKalmanPosCovCoeff_pos_min = controlMsgJson_flat.at("/wifiTrack/Kalman/PosCovCoeff_pos_min"); }
				if(controlMsgJson_flat.find("/wifiTrack/Kalman/PosCovCoeff_acc_min") != controlMsgJson_flat.end()) { mKalmanPosCovCoeff_accel_min = controlMsgJson_flat.at("/wifiTrack/Kalman/PosCovCoeff_acc_min"); }

				// -- Add new Kalman filter and restart the process
				addKalmanParams();
				exit(0);
			}

			// -- Change between "sport" or "normal" mode
			if(controlMsgJson.find("RealTimeReport") != controlMsgJson.end()) {
				bool realtimeMode = controlMsgJson.at("RealTimeReport");

				//cout << "Got control Msg: RealTimeReport " << mReportRealTimeTrackResult << " --> " << realtimeMode << endl;

				SetRealtimeMode(realtimeMode);
			}



	} catch (std::invalid_argument& e) {		// -- Handle exception when the received msg does not comply with JSON standard
		// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
    	//std::cout << "invalid_argument: " << e.what() << '\n';
	} catch (std::out_of_range& e) {	// -- Handle exception when a non-existing filed is accessed.
		// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
		//std::cout << "out of range: " << e.what() << '\n';
	} catch (std::domain_error& e) {	// -- Handle exception when an incompatible conversion is attempted (e.g., float --> string).
		// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
	   // std::cout << "domain error: " << e.what() << '\n';
	}

}

void WiFi_Shopper_Tracker::SetRealtimeMode(bool realtimeMode) {

	mReportRealTimeTrackResult = realtimeMode;

	if(realtimeMode == true) {
		// -- Set the scale in IoT_Gateway to the minimum, which is MIN_STATE_REPORT_INTERVAL defined in IoT_Common.hpp
		int desiredIntervalScale = 0.01;

		//printf("^^^^^ mPeriodicStateReportingInterval (%d, %d) --> ", mPeriodicStateReportingInterval, desiredInterval);
		mIoTGateway.SetStateParamReportIntervalScale(desiredIntervalScale);
		mPeriodicStateReportingInterval = mIoTGateway.GetStateParamReportInterval(&mPeriodicStateReportingInterval);
	} else {
		// -- Reset the scale in IoT_Gateway back to the normal

		//printf("^^^^^ mPeriodicStateReportingInterval (%f, %d) --> ", mIoTGateway.GetStateParamReportInterval("RealTimeReport"), mPeriodicStateReportingInterval);
		mIoTGateway.ResetStateParamReportIntervalScale();
		mPeriodicStateReportingInterval = mIoTGateway.GetStateParamReportInterval(&mPeriodicStateReportingInterval);
	}
}

/*
 * Note that all the types of JsonPrimitiveType of AWS ThingShadow parameters are grouped into just 6 types: int, uint, float, double, bool, string.
 */
void WiFi_Shopper_Tracker::IoT_DeltaState_CallBack_func(string entireDeltaMsg) {

	json entireDeltaMsgJson = json::parse(entireDeltaMsg);

	// -- Check if there is at least a delta state destined to this app. If not, then exit this function
	if(entireDeltaMsgJson.find(mAppType_Tracker) == entireDeltaMsgJson.end())
		return;

	json deltaMsgJson = json::parse(entireDeltaMsgJson.at(mAppType_Tracker).dump());

	// -- If there is at least a delta state destined to this app, then start processing it by looking up the registered state params.
	try {

		if(mDebugIoT)
		{
			printf("+++++++ [WiFi Shopper Tracker] Delta State Received +++++++++++++++++++++++\n");
			std::cout << std::setw(2) << deltaMsgJson << '\n';
			printf("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
		}

		for (auto it = deltaMsgJson.begin(); it != deltaMsgJson.end(); ++it) {

			ostringstream actualValue;
			actualValue << it.value();

			devState_t *stateParam = mIoTGateway.FindStateParam(it.key());

					pthread_mutex_lock(&gMutex);
					// -- Check if there's matching state parameter
					if(stateParam != NULL) {

						if(mDebugIoT) printf("[Delta Message] stateParam FOUND (key = %s)\n", stateParam->key.c_str());

						// -- Do something here to react to the received delta message to this particular state parameter.
						switch(stateParam->type) {
							case SHADOW_JSON_INT32:
							case SHADOW_JSON_INT16:
							case SHADOW_JSON_INT8: {
								int stateParam_desired;
								sscanf(actualValue.str().c_str(), "%d", &stateParam_desired);

								if(mDebugIoT)
									printf("(report, desired) = (%d, %d) -- > ", *(int*)stateParam->value, stateParam_desired);

								// -- Replace the state parameter with the desired value
								*(int*)stateParam->value = stateParam_desired;

								// -- Reset this so that this state can be reported again immediately.
								stateParam->time_lastReported = 0;

								if(mDebugIoT)
									printf("(%d)\n", *(int*)stateParam->value);

							}
								break;
							case SHADOW_JSON_UINT32:
							case SHADOW_JSON_UINT16:
							case SHADOW_JSON_UINT8: {
								unsigned int stateParam_desired;
								sscanf(actualValue.str().c_str(), "%u", &stateParam_desired);

								if(mDebugIoT)
									printf("(report, desired) = (%u, %u) --> ", *(unsigned int*)stateParam->value, stateParam_desired);

								// -- Replace the state parameter with the desired value
								*(unsigned int*)stateParam->value = stateParam_desired;

								// -- Reset this so that this state can be reported again immediately.
								stateParam->time_lastReported = 0;

								if(mDebugIoT)
									printf("(%u)\n", *(unsigned int*)stateParam->value);
							}
								break;
							case SHADOW_JSON_FLOAT: {
								float stateParam_desired;
								sscanf(actualValue.str().c_str(), "%f", &stateParam_desired);

								if(mDebugIoT) {
									printf("[%s] ", actualValue.str().c_str());
									cout << fixed << setprecision(IOT_FLOAT_PRECISION);
									cout << " (report, desired) = ("<< *(float*)stateParam->value << ", " << stateParam_desired << ") --> ";
								}

								// -- Replace the state parameter with the desired value
								*(float*)stateParam->value = stateParam_desired;

								// -- Reset this so that this state can be reported again immediately.
								stateParam->time_lastReported = 0;

								if(mDebugIoT) {
									cout << fixed << setprecision(IOT_FLOAT_PRECISION);
									cout << "("<< *(float*)stateParam->value << ")" << endl;
								}
							}
								break;
							case SHADOW_JSON_DOUBLE: {
								double stateParam_desired;
								sscanf(actualValue.str().c_str(), "%lf", &stateParam_desired);

								if(mDebugIoT) {
									printf("[%s] ", actualValue.str().c_str());
									cout << fixed << setprecision(IOT_DOUBLE_PRECISION);
									cout << " (report, desired) = ("<< *(double*)stateParam->value << ", " << stateParam_desired << ") --> ";
								}

								// -- Replace the state parameter with the desired value
								*(double*)stateParam->value = stateParam_desired;

								// -- Reset this so that this state can be reported again immediately.
								stateParam->time_lastReported = 0;

								if(mDebugIoT) {
									cout << fixed << setprecision(IOT_DOUBLE_PRECISION);
									cout << "("<< *(double*)stateParam->value << ")" << endl;
								}
							}
								break;
							case SHADOW_JSON_BOOL: {
								char stateParam_desired[99] = {'\0'};
								sscanf(actualValue.str().c_str(), "%s", stateParam_desired);

								if(mDebugIoT)
									printf("(report, desired) = (%d, %s) --> ", *(bool*)stateParam->value, stateParam_desired);

								// -- Replace the state parameter with the desired value
								if(stateParam_desired[0] == 't')	// -- true
									*(bool*)stateParam->value = true;
								if(stateParam_desired[0] == 'f')	// -- false
									*(bool*)stateParam->value = false;

								// -- Reset this so that this state can be reported again immediately.
								stateParam->time_lastReported = 0;

								if(mDebugIoT)
									printf("(%d)\n", *(bool*)stateParam->value);

								//if(stateParam->key.compare("RealTimeReport") == 0) {
								if((void*)stateParam->value == (void *)&mReportRealTimeTrackResult) {
									//cout << "Changing to RealTimeReport Mode.....\n";
									SetRealtimeMode(*(bool*)stateParam->value);
									cout << "Changed to RealTimeReport Mode\n";
								}

							}
								break;

							case SHADOW_JSON_STRING: {
								if(mDebugIoT)
									printf("(report, desired) = (%s, %s) -->", (*(string*)stateParam->value).c_str(), actualValue.str().c_str());

								// -- String-type value in JSON is wrapped with double-quotation marks, so get rid of them.
								string strExtracted = actualValue.str().substr(1,actualValue.str().size()-2);

								// -- Replace the state parameter with the desired value
								*(string*)stateParam->value = strExtracted;

								// -- Reset this so that this state can be reported again immediately.
								stateParam->time_lastReported = 0;

								if(mDebugIoT)
									printf("(%s)\n", (*(string*)stateParam->value).c_str());

							}
								break;

							default:
								break;
						}
					}
					pthread_mutex_unlock(&gMutex);
		}

	} catch (std::invalid_argument& e) {		// -- Handle exception when the received msg does not comply with JSON standard
		// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
    	//std::cout << "invalid_argument: " << e.what() << '\n';
    } catch (std::out_of_range& e) {	// -- Handle exception when a non-existing filed is accessed.
		// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
		//std::cout << "out of range: " << e.what() << '\n';
	} catch (std::domain_error& e) {	// -- Handle exception when an incompatible conversion is attempted (e.g., float --> string).
		// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
	   // std::cout << "domain error: " << e.what() << '\n';
	}
}

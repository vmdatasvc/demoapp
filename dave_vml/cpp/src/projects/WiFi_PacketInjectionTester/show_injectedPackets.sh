# -- Detect and show the packets (that embeds SN) injected by other OmniSensrs
sudo tcpdump -B 4096 -U -nevv -tttt -y ieee802_11_radio -s 64 -i bond0 "link[0] != 0x80" | grep "SA:fe:fe:fe"

/*
 *  802.11 WEP replay & injection attacks
 *
 *  Copyright (C) 2006-2016 Thomas d'Otreppe <tdotreppe@aircrack-ng.org>
 *  Copyright (C) 2004, 2005 Christophe Devine
 *
 *  WEP decryption attack (chopchop) developed by KoreK
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *  In addition, as a special exception, the copyright holders give
 *  permission to link the code of portions of this program with the
 *  OpenSSL library under certain conditions as described in each
 *  individual source file, and distribute linked combinations
 *  including the two.
 *  You must obey the GNU General Public License in all respects
 *  for all of the code used other than OpenSSL. *  If you modify
 *  file(s) with this exception, you may extend this exception to your
 *  version of the file(s), but you are not obligated to do so. *  If you
 *  do not wish to do so, delete this exception statement from your
 *  version. *  If you delete this exception statement from all source
 *  files in the program, then also delete it here.
 */

/*
 * Modified by Paul J. Shin
 */

#if defined(linux)
    #include <linux/rtc.h>
#endif

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/wait.h>
#include <sys/time.h>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <dirent.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <time.h>
#include <getopt.h>
#include <assert.h>

#include <fcntl.h>
#include <ctype.h>

#include <limits.h>

#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>

#include "version.h"
#include "pcap.h"
#include "osdep/osdep.h"
#include "crypto.h"
#include "common.h"

#define RTC_RESOLUTION  8192

#define CALIB_PACKET_TX_REQUESTS_NUM    100			// -- Number of NULL data packets for WiFi calibration
#define CALIB_PACKET_TX_INTERVAL		400		// -- Unit: [ms]

#define MAX_APS     20

#define NEW_IV  1
#define RETRY   2
#define ABORT   3

#define DEAUTH_REQ      \
    "\xC0\x00\x3A\x01\xCC\xCC\xCC\xCC\xCC\xCC\xBB\xBB\xBB\xBB\xBB\xBB" \
    "\xBB\xBB\xBB\xBB\xBB\xBB\x00\x00\x07\x00"

#define AUTH_REQ        \
    "\xB0\x00\x3A\x01\xBB\xBB\xBB\xBB\xBB\xBB\xCC\xCC\xCC\xCC\xCC\xCC" \
    "\xBB\xBB\xBB\xBB\xBB\xBB\xB0\x00\x00\x00\x01\x00\x00\x00"

#define ASSOC_REQ       \
    "\x00\x00\x3A\x01\xBB\xBB\xBB\xBB\xBB\xBB\xCC\xCC\xCC\xCC\xCC\xCC"  \
    "\xBB\xBB\xBB\xBB\xBB\xBB\xC0\x00\x31\x04\x64\x00"

#define REASSOC_REQ       \
    "\x20\x00\x3A\x01\xBB\xBB\xBB\xBB\xBB\xBB\xCC\xCC\xCC\xCC\xCC\xCC"  \
    "\xBB\xBB\xBB\xBB\xBB\xBB\xC0\x00\x31\x04\x64\x00\x00\x00\x00\x00\x00\x00"

#define NULL_DATA       \
    "\x48\x01\x3A\x01\xBB\xBB\xBB\xBB\xBB\xBB\xCC\xCC\xCC\xCC\xCC\xCC"  \
    "\xBB\xBB\xBB\xBB\xBB\xBB\xE0\x1B"

#define RTS             \
    "\xB4\x00\x4E\x04\xBB\xBB\xBB\xBB\xBB\xBB\xCC\xCC\xCC\xCC\xCC\xCC"

#define RATES           \
    "\x01\x04\x02\x04\x0B\x16\x32\x08\x0C\x12\x18\x24\x30\x48\x60\x6C"

#define PROBE_REQ       \
    "\x40\x00\x00\x00\xFF\xFF\xFF\xFF\xFF\xFF\xCC\xCC\xCC\xCC\xCC\xCC"  \
    "\xFF\xFF\xFF\xFF\xFF\xFF\x00\x00"

#define RATE_NUM 12

#define RATE_1M 1000000
#define RATE_2M 2000000
#define RATE_5_5M 5500000
#define RATE_11M 11000000

#define RATE_6M 6000000
#define RATE_9M 9000000
#define RATE_12M 12000000
#define RATE_18M 18000000
#define RATE_24M 24000000
#define RATE_36M 36000000
#define RATE_48M 48000000
#define RATE_54M 54000000

int bitrates[RATE_NUM]={RATE_1M, RATE_2M, RATE_5_5M, RATE_6M, RATE_9M, RATE_11M, RATE_12M, RATE_18M, RATE_24M, RATE_36M, RATE_48M, RATE_54M};

extern char * getVersion(char * progname, int maj, int min, int submin, int svnrev, int beta, int rc);
extern int maccmp(unsigned char *mac1, unsigned char *mac2);
extern unsigned char * getmac(char * macAddress, int strict, unsigned char * mac);
extern int check_crc_buf( unsigned char *buf, int len );
extern const unsigned long int crc_tbl[256];
extern const unsigned char crc_chop_tbl[256][4];

char usage[] =

"\n"
"  %s - (C) 2006-2015 Thomas d\'Otreppe\n"
"  http://www.aircrack-ng.org\n"
"\n"
"  usage: aireplay-ng <options> <replay interface>\n"
"\n"
"  Filter options:\n"
"\n"
"      -b bssid  : MAC address, Access Point\n"
"      -d dmac   : MAC address, Destination\n"
"      -s smac   : MAC address, Source\n"
"      -m len    : minimum packet length\n"
"      -n len    : maximum packet length\n"
"      -u type   : frame control, type    field\n"
"      -v subt   : frame control, subtype field\n"
"      -t tods   : frame control, To      DS bit\n"
"      -f fromds : frame control, From    DS bit\n"
"      -w iswep  : frame control, WEP     bit\n"
"      -D        : disable AP detection\n"
"\n"
"  Replay options:\n"
"\n"
"      -x nbpps  : number of packets per second\n"
"      -p fctrl  : set frame control word (hex)\n"
"      -a bssid  : set Access Point MAC address\n"
"      -c dmac   : set Destination  MAC address\n"
"      -h smac   : set Source       MAC address\n"
"      -g value  : change ring buffer size (default: 8)\n"
"      -F        : choose first matching packet\n"
"\n"
"      Fakeauth attack options:\n"
"\n"
"      -e essid  : set target AP SSID\n"
"      -o npckts : number of packets per burst (0=auto, default: 1)\n"
"      -q sec    : seconds between keep-alives\n"
"      -Q        : send reassociation requests\n"
"      -y prga   : keystream for shared key auth\n"
"      -T n      : exit after retry fake auth request n time\n"
"\n"
"      Arp Replay attack options:\n"
"\n"
"      -j        : inject FromDS packets\n"
"\n"
"      Fragmentation attack options:\n"
"\n"
"      -k IP     : set destination IP in fragments\n"
"      -l IP     : set source IP in fragments\n"
"\n"
"      Test attack options:\n"
"\n"
"      -B        : activates the bitrate test\n"
"\n"
/*
"  WIDS evasion options:\n"
"      -y value  : Use packets older than n packets\n"
"      -z        : Ghosting\n"
"\n"
*/
"  Source options:\n"
"\n"
"      -i iface  : capture packets from this interface\n"
"      -r file   : extract packets from this pcap file\n"
"\n"
"  Miscellaneous options:\n"
"\n"
"      -R                    : disable /dev/rtc usage\n"
"      --ignore-negative-one : if the interface's channel can't be determined,\n"
"                              ignore the mismatch, needed for unpatched cfg80211\n"
"\n"
"  Attack modes (numbers can still be used):\n"
"\n"
"      --deauth      count : deauthenticate 1 or all stations (-0)\n"
"      --fakeauth    delay : fake authentication with AP (-1)\n"
"      --interactive       : interactive frame selection (-2)\n"
"      --arpreplay         : standard ARP-request replay (-3)\n"
"      --chopchop          : decrypt/chopchop WEP packet (-4)\n"
"      --fragment          : generates valid keystream   (-5)\n"
"      --caffe-latte       : query a client for new IVs  (-6)\n"
"      --cfrag             : fragments against a client  (-7)\n"
"      --migmode           : attacks WPA migration mode  (-8)\n"
"      --test              : tests injection and quality (-9)\n"
"\n"
"      --help              : Displays this usage screen\n"
"\n";


struct options
{
    unsigned char f_bssid[6];
    unsigned char f_dmac[6];
    unsigned char f_smac[6];
    int f_minlen;
    int f_maxlen;
    int f_type;
    int f_subtype;
    int f_tods;
    int f_fromds;
    int f_iswep;

    int r_nbpps;
    int r_fctrl;
    unsigned char r_bssid[6];
    unsigned char r_dmac[6];
    unsigned char r_smac[6];
    unsigned char r_dip[4];
    unsigned char r_sip[4];
    char r_essid[33];
    int r_fromdsinj;
    char r_smac_set;

    char ip_out[16];    //16 for 15 chars + \x00
    char ip_in[16];
    int port_out;
    int port_in;

    char *iface_out;
    char *s_face;
    char *s_file;
    unsigned char *prga;

    int a_mode;
    int a_count;
    int a_delay;
	int f_retry;

    int ringbuffer;
    int ghost;
    int prgalen;

    int delay;
    int npackets;

    int fast;
    int bittest;

    int nodetect;
    int ignore_negative_one;
    int rtc;

    int reassoc;
}
opt;

struct devices
{
    int fd_in,  arptype_in;
    int fd_out, arptype_out;
    int fd_rtc;

    unsigned char mac_in[6];
    unsigned char mac_out[6];

    int is_wlanng;
    int is_hostap;
    int is_madwifi;
    int is_madwifing;
    int is_bcm43xx;

    FILE *f_cap_in;

    struct pcap_file_header pfh_in;
}
dev;

static struct wif *_wi_in, *_wi_out;

struct ARP_req
{
    unsigned char *buf;
    int hdrlen;
    int len;
};

struct APt
{
    unsigned char set;
    unsigned char found;
    unsigned char len;
    unsigned char essid[255];
    unsigned char bssid[6];
    unsigned char chan;
    unsigned int  ping[CALIB_PACKET_TX_REQUESTS_NUM];
    int  pwr[CALIB_PACKET_TX_REQUESTS_NUM];
};

struct APt ap[MAX_APS];

unsigned long nb_pkt_sent;
unsigned char h80211[4096];
unsigned char tmpbuf[4096];
unsigned char srcbuf[4096];
char strbuf[512];

unsigned char ska_auth1[]     = "\xb0\x00\x3a\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
                        "\x00\x00\x00\x00\x00\x00\xb0\x01\x01\x00\x01\x00\x00\x00";

unsigned char ska_auth3[4096] = "\xb0\x40\x3a\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
                        "\x00\x00\x00\x00\x00\x00\xc0\x01";

int ctrl_c, alarmed;
char * iwpriv;

int send_packet(void *buf, size_t count)
{
	struct wif *wi = _wi_out; /* XXX globals suck */
	unsigned char *pkt = (unsigned char*) buf;

	if( (count > 24) && (pkt[1] & 0x04) == 0 && (pkt[22] & 0x0F) == 0)
	{
		pkt[22] = (nb_pkt_sent & 0x0000000F) << 4;
		pkt[23] = (nb_pkt_sent & 0x00000FF0) >> 4;
	}

	if (wi_write(wi, buf, count, NULL) == -1) {
		switch (errno) {
		case EAGAIN:
		case ENOBUFS:
			usleep(10000);
			return 0; /* XXX not sure I like this... -sorbo */
		}

		perror("wi_write()");
		return -1;
	}

	nb_pkt_sent++;
	return 0;
}

int read_packet(void *buf, size_t count, struct rx_info *ri)
{
	struct wif *wi = _wi_in; /* XXX */
	int rc;

        rc = wi_read(wi, buf, count, ri);
        if (rc == -1) {
            switch (errno) {
            case EAGAIN:
                    return 0;
            }

            perror("wi_read()");
            return -1;
        }

	return rc;
}

int grab_essid(unsigned char* packet, int len)
{
    int i=0, j=0, pos=0, tagtype=0, taglen=0, chan=0;
    unsigned char bssid[6];

    memcpy(bssid, packet+16, 6);
    taglen = 22;    //initial value to get the fixed tags parsing started
    taglen+= 12;    //skip fixed tags in frames
    do
    {
        pos    += taglen + 2;
        tagtype = packet[pos];
        taglen  = packet[pos+1];
    } while(tagtype != 3 && pos < len-2);

    if(tagtype != 3) return -1;
    if(taglen != 1) return -1;
    if(pos+2+taglen > len) return -1;

    chan = packet[pos+2];

    pos=0;

    taglen = 22;    //initial value to get the fixed tags parsing started
    taglen+= 12;    //skip fixed tags in frames
    do
    {
        pos    += taglen + 2;
        tagtype = packet[pos];
        taglen  = packet[pos+1];
    } while(tagtype != 0 && pos < len-2);

    if(tagtype != 0) return -1;
    if(taglen > 250) taglen = 250;
    if(pos+2+taglen > len) return -1;

    for(i=0; i<20; i++)
    {
        if( ap[i].set)
        {
            if( memcmp(bssid, ap[i].bssid, 6) == 0 )    //got it already
            {
                if(packet[0] == 0x50 && !ap[i].found)
                {
                    ap[i].found++;
                }
                if(ap[i].chan == 0) ap[i].chan=chan;
                break;
            }
        }
        if(ap[i].set == 0)
        {
            for(j=0; j<taglen; j++)
            {
                if(packet[pos+2+j] < 32 || packet[pos+2+j] > 127)
                {
                    return -1;
                }
            }

            ap[i].set = 1;
            ap[i].len = taglen;
            memcpy(ap[i].essid, packet+pos+2, taglen);
            ap[i].essid[taglen] = '\0';
            memcpy(ap[i].bssid, bssid, 6);
            ap[i].chan = chan;
            if(packet[0] == 0x50) ap[i].found++;
            return 0;
        }
    }
    return -1;
}

static int get_ip_port(char *iface, char *ip, const int ip_size)
{
	char *host;
	char *ptr;
	int port = -1;
	struct in_addr addr;

	host = strdup(iface);
	if (!host)
		return -1;

	ptr = strchr(host, ':');
	if (!ptr)
		goto out;

	*ptr++ = 0;

	if (!inet_aton(host, (struct in_addr *)&addr))
		goto out; /* XXX resolve hostname */

	if(strlen(host) > 15)
        {
            port = -1;
            goto out;
        }
	strncpy(ip, host, ip_size);
	port = atoi(ptr);
        if(port <= 0) port = -1;

out:
	free(host);
	return port;
}

struct net_hdr {
	uint8_t		nh_type;
	uint32_t	nh_len;
	uint8_t		nh_data[0];
} __packed;

// -- Embed 11-digit SN into 6 characters
void str2hstr(const char *str, int str_length, unsigned char *hstr, int hstr_length) {
    unsigned int u;
    int i = 0;
    // -- if the length is odd, then prepend a zero.
    unsigned int isOddLength = str_length%2;

    char buf[str_length + isOddLength];
    char *str_l = buf;

    str_l[0] = '0';
    memcpy(&str_l[isOddLength], str, str_length);

    while (i < hstr_length && sscanf(str_l, "%2x", &u) == 1)
    {
        hstr[i++] = u;
        str_l += 2;
    }

//    for (i = 0; i < hstr_length; i++)
//        printf("%d: %c (%d, 0x%02x)\n", i,
//               (isprint(hstr[i]) ? hstr[i] : '.'), hstr[i], hstr[i]);
}

/*
 *  Embed 11-digit SN into 6 characters
 *  E.g., A0000000001 will be translated into {0A, 00, 00, 00, 00, 01}
 */
void EmbedSNasDestAddr(const char *SN, unsigned char *dest) {
	int i=0;
//	PCT; printf("DEBUG: \n");
	for(i=5; i>0; i--) {
		char l2 = SN[i*2-1];
		char l1 = SN[i*2];
		dest[i] = (atoi(&l2) << 4) + atoi(&l1);
//		printf("[%d]th: %02X (%02X + %02X)\n", i, dest[i], atoi(&l2)<<4, atoi(&l1));

	}
	dest[0] = SN[0];

//	printf("[%d]th: %c\n", i, dest[i]);
//	printf("==> Dest Addr: %s\n", dest);

}

/*
 * Extract 11-digit SN from 6-character destination address
 */
void ExtractSNfromDestAddr( unsigned char *dest, char * SN) {
	char l;//, ll;
	int i=0;

	l = dest[0];

	SN[i] = l;

//		ll = SN[i];
//		printf("SN[%d] = %s\n", i, &ll);

	for(i=1; i<6; i++) {
		sprintf(&l, "%d", (dest[i] & 0xf0) >> 4);
		SN[2*i-1] = l;

//			ll = SN[2*i-1];
//			printf("SN[%d] = %s\n", i, &ll);

		sprintf(&l, "%d", dest[i] & 0x0f);
		SN[2*i] = l;

//			ll = SN[2*i];
//			printf("SN[%d] = %s\n", i, &ll);
	}

	printf("==> SN: %s\n", SN);


}

int do_attack_test()
{
    unsigned char packet[4096];
    struct timeval tv, tv2;
    int len_ProbeRequest=0, len=0, i=0, j=0;
    int answers=0, found=0;
    int caplen=0, essidlen=0;
    struct rx_info ri;
    unsigned long atime=200; // -- 200;  //time in ms to wait for answer packet (needs to be higher for airserv)
    char SN[11] = {'\0'};		// -- Serial Number of the OmniSensr

		unsigned int min, avg, max;
		float avg2;
		unsigned char nulldata[1024];

	if(opt.s_face && opt.port_in <= 0)
    {
        _wi_in = wi_open(opt.s_face);
        if (!_wi_in)
            return 1;
        dev.fd_in = wi_fd(_wi_in);
        wi_get_mac(_wi_in, dev.mac_in);
        printf("\n");
    }

    if(opt.port_in <= 0)
    {
        /* avoid blocking on reading the socket */
        if( fcntl( dev.fd_in, F_SETFL, O_NONBLOCK ) < 0 )
        {
            perror( "fcntl(O_NONBLOCK) failed" );
            return( 1 );
        }
    }

    srand( time( NULL ) );

    memset(ap, '\0', 20*sizeof(struct APt));

    essidlen = strlen(opt.r_essid);
    if( essidlen > 250) essidlen = 250;

    //if( essidlen > 0 )
    {
        ap[0].set = 1;
        ap[0].found = 0;
        ap[0].len = essidlen;
        memcpy(ap[0].essid, opt.r_essid, essidlen);
        ap[0].essid[essidlen] = '\0';
        memcpy(ap[0].bssid, opt.r_bssid, 6);
        found++;
    }

    PCT; printf("Trying broadcast probe requests...\n");

    memcpy(h80211, PROBE_REQ, 24);

    len = 24;

    h80211[24] = 0x00;      //ESSID Tag Number
    h80211[25] = 0x00;      //ESSID Tag Length

    len += 2;

    memcpy(h80211+len, RATES, 16);

    len += 16;

    answers=0;

    len_ProbeRequest = len;

    memcpy(h80211+10, opt.r_smac, 6);

	// -- Read Serial Number and use it as my Thing Name
	{
		char cmd[] = "cat /usr/local/www/serial_number.txt | grep Serial | sed 's/:/ /' | awk '{print $2}'";
		FILE *fp_serial = popen(cmd, "r");
		int ret = fscanf(fp_serial, "%s", SN);
		pclose(fp_serial);

		PCT; printf("OmniSensr SerialNumber = %s\n\n", SN);
		fflush(stdout);
	}

    /*
        random source so we can identify our packets
    */
    opt.r_smac[0] = 0xFE; // -- rand() & 0xFF
    opt.r_smac[1] = 0xFE; // -- rand() & 0xFF;
    opt.r_smac[2] = 0xFE; // -- rand() & 0xFF;
    opt.r_smac[3] = rand() & 0xFF;
    opt.r_smac[4] = rand() & 0xFF;
    opt.r_smac[5] = rand() & 0xFF;

    // -- Embed SN into the packet header
    {
    	str2hstr(SN, sizeof(SN), opt.r_bssid, sizeof(opt.r_bssid));		// -- Embed 11-digit SN into 6 characters
    	//EmbedSNasDestAddr(SN, ap[i].bssid);	// -- Embed 11-digit SN into 6 characters
    	//ExtractSNfromDestAddr(opt.r_dmac, SN);	// -- Extract 11-digit SN from 6-character destination address
    }

    //memcpy(h80211+4, opt.r_bssid, 6);
	memcpy(h80211+10, opt.r_smac, 6);
	memcpy(h80211+16, opt.r_bssid, 6);

	found = 0;
	j=0;

	while(1)	// -- Send NULL data packets until this process is killed. This process will be killed only if wifi shopper tracker received enough measurements.
	{
		int randTime = rand()%100;	// -- Add some randomness in backoff to avoid any potential pertinent packet collision
    //for(i=0; i<3; i++)
    //for(i=0; i<100;i++)
    //{

//        if(1) {
//        	EmbedSNasDestAddr(SN, opt.r_dmac);	// -- Embed 11-digit SN into 6 characters
//        	//ExtractSNfromDestAddr(opt.r_dmac, SN);	// -- Extract 11-digit SN from 6-character destination address
//        }

		// -- Debug
        if(0) {
//        	if(answers) {
				PCT; printf("\rSending [%d]th Probe Request (length = %d): ", i, len);
				for(j=0; j<len-1; j++) { //%02X:
					printf("%02X:", h80211[j]);
				}
				printf("%02X\r", h80211[j]); fflush(stdout);
//        	}
        }

        send_packet(h80211, len);

        gettimeofday( &tv, NULL );

        if(j == 0 && answers == 0)
			while (j == 0 && answers == 0)  //waiting for relayed packet
			{
				caplen = read_packet(packet, sizeof(packet), &ri);

				if (packet[0] == 0x50 ) //Is probe response
				{
					if (! memcmp(opt.r_smac, packet+4, 6)) //To our MAC
					{
						//if(grab_essid(packet, caplen) == 0)// && (!memcmp(opt.r_bssid, NULL_MAC, 6)))
						{
							found++;
							PCT; printf("Found %d APs so far\n", found);
						}
						//if(answers == 0)
						{
							PCT; printf("Injection is working!\n\n");
							//if(opt.fast) return 0;
							answers++;
						}
					}
				}

		//            if (packet[0] == 0x80 ) //Is beacon frame
		//            {
		//                if(grab_essid(packet, caplen) == 0 && (!memcmp(opt.r_bssid, NULL_MAC, 6)))
		//                {
		//                    found++;
		//                }
		//            }

				gettimeofday( &tv2, NULL );
				if (((tv2.tv_sec*1000000UL - tv.tv_sec*1000000UL) + (tv2.tv_usec - tv.tv_usec)) > (atime*1000)) //wait 'atime'ms for an answer
				{
					break;
				}
			}

		usleep((CALIB_PACKET_TX_INTERVAL+randTime)*1000);		// -- Time interval between packets
		j++;
    }

    if(answers == 0)
    {
        PCT; printf("No Answer...\n");
    }

    printf("\n\n"); PCT; printf("Found %d AP%c\n", found, ((found == 1) ? ' ' : 's' ) );

     // ------------  Directed Packet Injection with SN Embedded in Packets -------------------------

    /*
     *  Paul: Put the serial number of OmniSensr to the destination field
     *  So that others can recognize who sent this Probe Request. This is necessary for WiFi self-calibration method
     *  Wanted to put this in the source address, but didn't work. Need to find out later!
     */

    i = 0;

    // -- Embed SN into the packet header
    {
    	str2hstr(SN, sizeof(SN), opt.r_bssid, sizeof(opt.r_bssid));		// -- Embed 11-digit SN into 6 characters
    	//EmbedSNasDestAddr(SN, ap[i].bssid);	// -- Embed 11-digit SN into 6 characters
    	//ExtractSNfromDestAddr(opt.r_dmac, SN);	// -- Extract 11-digit SN from 6-character destination address
    }

	PCT; printf("%02X:%02X:%02X:%02X:%02X:%02X - channel: %d - \'%s\'\n", opt.r_bssid[0], opt.r_bssid[1],
				opt.r_bssid[2], opt.r_bssid[3], opt.r_bssid[4], opt.r_bssid[5], ap[i].chan, ap[i].essid);

	ap[i].found=0;
	min = INT_MAX;
	max = 0;
	avg = 0;
	avg2 = 0;

//        /*
//            random source so we can identify our packets
//        */
//        opt.r_smac[0] = 0xFE; // -- 0x00
//        opt.r_smac[1] = 0xFE; // -- rand() & 0xFF;
//        opt.r_smac[2] = 0xFE; // -- rand() & 0xFF;
//        opt.r_smac[3] = rand() & 0xFF;
//        opt.r_smac[4] = rand() & 0xFF;
//        opt.r_smac[5] = rand() & 0xFF;

	// -- Make it a broadcast packet
	opt.r_dmac[0] = 0xFF;
	opt.r_dmac[1] = 0xFF;
	opt.r_dmac[2] = 0xFF;
	opt.r_dmac[3] = 0xFF;
	opt.r_dmac[4] = 0xFF;
	opt.r_dmac[5] = 0xFF;

	//memcpy(h80211, PROBE_REQ, 24);
//        memcpy(h80211, NULL_DATA, 24);
//
//        len = 24;
//
//        h80211[24] = 0x00;      //ESSID Tag Number
//        h80211[25] = ap[i].len; //ESSID Tag Length
//        memcpy(h80211+len+2, ap[i].essid, ap[i].len);
//
//        len += ap[i].len+2;
//
//        memcpy(h80211+len, RATES, 16);
//
//        len += 16;

	//for(j=0; j<CALIB_PACKET_TX_REQUESTS_NUM; j++)
	j=0;
	while(1)	// -- Send NULL data packets until this process is killed. This process will be killed only if wifi shopper tracker received enough measurements.
	{
		int randTime = rand()%100;	// -- Add some randomness in backoff to avoid any potential pertinent packet collision

		//build/send null data packet
		memcpy(nulldata, NULL_DATA, 24);

		len = 24;

		nulldata[24] = 0x00;      //ESSID Tag Number
		nulldata[25] = ap[i].len; //ESSID Tag Length
		memcpy(nulldata+len+2, ap[i].essid, ap[i].len);

		len += ap[i].len+2;

		memcpy(nulldata+len, RATES, 16);

		len += 16;

		memcpy(nulldata+4, opt.r_bssid, 6);
		memcpy(nulldata+10, opt.r_smac, 6);		// -- Source addr
		//memcpy(nulldata+16, opt.r_bssid, 6);	// -- Destination addr
		memcpy(nulldata+16, opt.r_dmac, 6);	// -- Destination addr



		//send_packet(nulldata, 24);
		send_packet(nulldata, len_ProbeRequest);	// lengh of Probe Request is 42

		usleep((CALIB_PACKET_TX_INTERVAL+randTime)*1000);		// -- Time interval between packets

		gettimeofday( &tv, NULL );

		printf( "\r%2d/%2d: %3d%%\r", ap[i].found, j+1, ((ap[i].found*100)/(j+1)));
		fflush(stdout);

		j++;
	}

    return 0;
}

int main( int argc, char *argv[] )
{
    /* check the arguments */

    memset( &opt, 0, sizeof( opt ) );
    memset( &dev, 0, sizeof( dev ) );

    opt.f_type    = -1; opt.f_subtype   = -1;
    opt.f_minlen  = -1; opt.f_maxlen    = -1;
    opt.f_tods    = -1; opt.f_fromds    = -1;
    opt.f_iswep   = -1; opt.ringbuffer  =  8;

    opt.a_mode    = -1; opt.r_fctrl     = -1;
    opt.ghost     =  0;
    opt.delay     = 15; opt.bittest     =  0;
    opt.fast      =  0; opt.r_smac_set  =  0;
    opt.npackets  =  1; opt.nodetect    =  0;
    opt.rtc       =  1; opt.f_retry	=  0;
    opt.reassoc   =  0;

    opt.a_mode = 9;		// -- Force option for Injection Testing

        int option_index = 0;

        static struct option long_options[] = {
            {"deauth",      1, 0, '0'},
            {"fakeauth",    1, 0, '1'},
            {"interactive", 0, 0, '2'},
            {"arpreplay",   0, 0, '3'},
            {"chopchop",    0, 0, '4'},
            {"fragment",    0, 0, '5'},
            {"caffe-latte", 0, 0, '6'},
            {"cfrag",       0, 0, '7'},
            {"test",        0, 0, '9'},
            {"help",        0, 0, 'H'},
            {"fast",        0, 0, 'F'},
            {"bittest",     0, 0, 'B'},
            {"migmode",     0, 0, '8'},
            {"ignore-negative-one", 0, &opt.ignore_negative_one, 1},
            {0,             0, 0,  0 }
        };

        getopt_long( argc, argv,
                        "b:d:s:m:n:u:v:t:T:f:g:w:x:p:a:c:h:e:ji:r:k:l:y:o:q:Q0:1:23456789HFBDR",
                        long_options, &option_index );

	    if( argc - optind == 0)
	    {
	    	printf("No replay interface specified.\n");
	    	return( 1 );
	    }

    opt.iface_out = argv[optind];
    opt.port_out = get_ip_port(opt.iface_out, opt.ip_out, sizeof(opt.ip_out)-1);

    //don't open interface(s) when using test mode and airserv
    if( ! (opt.a_mode == 9 && opt.port_out >= 0 ) )
    {
        /* open the replay interface */
        _wi_out = wi_open(opt.iface_out);
        if (!_wi_out)
            return 1;
        dev.fd_out = wi_fd(_wi_out);

        /* open the packet source */
        if( opt.s_face != NULL )
        {
            //don't open interface(s) when using test mode and airserv
            if( ! (opt.a_mode == 9 && opt.port_in >= 0 ) )
            {
                _wi_in = wi_open(opt.s_face);
                if (!_wi_in)
                    return 1;
                dev.fd_in = wi_fd(_wi_in);
                wi_get_mac(_wi_in, dev.mac_in);
            }
        }
        else
        {
            _wi_in = _wi_out;
            dev.fd_in = dev.fd_out;

            /* XXX */
            dev.arptype_in = dev.arptype_out;
            wi_get_mac(_wi_in, dev.mac_in);
        }

        wi_get_mac(_wi_out, dev.mac_out);
    }

    // -- Perform Injection Test
    do_attack_test();

    /* that's all, folks */
    return( 0 );
}

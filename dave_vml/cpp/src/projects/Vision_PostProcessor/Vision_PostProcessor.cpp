/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/****************************************************************************/

// Vision_PostProcessor.cpp
//
//   command line tool for vision post processing
//
//   Youngrock Yoon
//   2017-01-28

#include <cstdio>
#include <getopt.h>
#include <boost/algorithm/string.hpp>

#include <modules/utility/Util.hpp>
#include <core/Error.hpp>
#include <modules/vision/VisionPostProcessing.hpp>

#define SETTINGS_PATH				"~/VM/ProjectData/Giant-6072"
#define INPUT_SOURCE_PATH			"~/VM/ProjectData/Giant-6072"	// mjpg-streamer url

#define DEBUG	0

// logger message macros
//#define STARTLOGGING			logger.startLogging()
//#define LOGMESSAGE(__MSG__)		if(bVerbose){logger.outputMessage(__MSG__);}

// globals
bool	bVisualize = false;
bool	bSaveIntermediateResult = false;

vml::Logger	logger;		// for verbose output

//std::string	settings_path = SETTINGS_PATH;
std::string input_source_path = INPUT_SOURCE_PATH;	// default is localhost mjpg-streamer

// settings module pointers
//boost::shared_ptr<SETTINGS> pSettings;
//vml::FileParts	settings_file_parts;

boost::shared_ptr<vml::VisionPostProcessor> 	pVisionPostProcessor;

void display_usage( const char *argv0 )
{

	vml::FileParts	cmd_name(argv0);
	printf("\n");

	printf("Usage:  %s [options] input_source_directory\n", cmd_name.filename().c_str());
	printf("  Options:\n");
	printf("\t [-h | --help]            print this message\n");
	printf("\t [-v | --visualize]       show visualize window (default off)\n");
	printf("\t [-s | --saveintermediate] save intermediate joining result\n");
	printf("\t [-d | --date]         	date string.\n");
	printf("\t [-1 | --level1]         	level 1 joining method and parameters.\n");
	printf("\t [-2 | --level2]         	level 2 joining method and parameters.\n");
	printf("\t [-3 | --level3]         	level 3 joining method and parameters.\n");
	printf("\t Two joining methods available:\n");
	printf(" - JO: Join by Overlap. Take 1 parameter <distance threshold>\n");
	printf("       example: -1 \"JO:35\" -- Join by Overlap with distance threshold of 35 \n");
	printf(" - JR: Join Rect. Take set of 3 parameters delimeted by comma. <mintime,maxtime,distance>. Iterate by the number of sets.\n");
	printf("       example: -2 \"JR:-5,10,50\" -- Join by Rect with time window [-5 10] with distance 50\n");
	printf("                -3 \"JR:-5,10,50:-5,10,100\" -- Iterate two with time window [-5 10] with distance 50 in the first iteration, then 100 in second iteration.\n");

	printf("\n\t If input source path is not provided, use current directory as the default input path\n");

	printf("  Examples:\n");
	printf("\t %s -v -d 20161113 -1 \"JO:35\" -2 \"JO:45\" -3 \"JR:-5,10,50\" ~/VM/ProjectData/Giant-6072\n",cmd_name.filename().c_str());
	printf("\n");

	exit(1);

}

int main(int argc, char* argv[])
{
	std::string	settingsPath = "";
	std::string	date_string;
	std::string level1_setting = "JO:35";
	std::string level2_setting = "JR:-5,10,50";
	std::string level3_setting = "JR:-5,10,50";

	static struct option longopts[] = {
			{ "help", 		no_argument,		NULL, 		'h'},
			{ "visualize",	no_argument,		NULL, 		'v'},
			{ "saveintermediate",		no_argument,		NULL, 		's'},
			{ "date",		required_argument,	NULL,       'd'},
			{ "level1",		required_argument,  NULL,		'1'},
			{ "level2",		required_argument,  NULL,		'2'},
			{ "level3",		required_argument,  NULL,		'3'},
			{ NULL,			0,					NULL,		0}
	};

	int c;
	while ((c = getopt_long(argc, argv, "hvsd:1:2:3:", longopts, NULL)) != -1)
	{
		switch (c)
		{
			case 'v':
			{
				bVisualize = true;
				break;
			}
			case 'h':
			{
				display_usage(argv[0]);
				return 0;
			}
			case 's':
			{
				bSaveIntermediateResult = true;
				break;
			}
			case 'd':
			{
				date_string = optarg;
				break;
			}
			case '1':
			{
				level1_setting = optarg;
				break;
			}
			case '2':
			{
				level2_setting = optarg;
				break;
			}
			case '3':
			{
				level3_setting = optarg;
				break;
			}

			default:
			{
				std::cerr << "Unrecognized commandline option -" << c << ". Aborting" << std::endl;
				display_usage(argv[0]);
				return (-1);
			}
		} // of switch (c)
	} // of while

	if (optind == argc)
	{
		VML_WARN(vml::MsgCode::runTimeErr, "Input data path not specified. Use current path")
		input_source_path = "./";
	}
	else {
		input_source_path = argv[optind];
	}

	std::cout << "Post processing at " << input_source_path << std::endl;

	pVisionPostProcessor.reset(new vml::VisionPostProcessor());

	if (bVisualize)
	{
		std::cout << "Display turned on" << std::endl;
		pVisionPostProcessor->setDisplay(bVisualize);
	}

	if (bSaveIntermediateResult)
	{
		std::cout << "Save intermediate results" << std::endl;
		pVisionPostProcessor->setSaveIntermediateResult(bSaveIntermediateResult);
	}

	// initialize and setup post processing app
	pVisionPostProcessor->initialize(date_string, input_source_path, level1_setting, level2_setting, level3_setting);

	// run
	pVisionPostProcessor->run(input_source_path+"/Data");

	return 0;
}


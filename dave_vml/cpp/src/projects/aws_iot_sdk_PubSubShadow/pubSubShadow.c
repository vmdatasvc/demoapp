/*
 * Copyright 2010-2015 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

/**
 * @file pubSubShadow.c
 * @brief A simple connected window example demonstrating the use of Thing Shadow
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>

#include <signal.h>
#include <memory.h>
#include <sys/time.h>
#include <limits.h>


#include "aws_iot_log.h"
#include "aws_iot_version.h"
#include "aws_iot_shadow_interface.h"
#include "aws_iot_shadow_json_data.h"
#include "aws_iot_mqtt_interface.h"

//#include <inttypes.h>
//#include "aws_iot_json_utils.h"

#include "aws_iot_config.h"

/*!
 * The goal of this sample application is to demonstrate the capabilities of shadow.
 * This device(say Connected Window) will open the window of a room based on temperature
 * It can report to the Shadow the following parameters:
 *  1. temperature of the room (double)
 *  2. status of the window (open or close)
 * It can act on commands from the cloud. In this case it will open or close the window based on the json object "windowOpen" data[open/close]
 *
 * The two variables from a device's perspective are double temperature and bool windowOpen
 * The device needs to act on only on windowOpen variable, so we will create a primitiveJson_t object with callback
 The Json Document in the cloud will be
 {
 "reported": {
 "temperature": 0,
 "windowOpen": false
 },
 "desired": {
 "windowOpen": false
 }
 }
 */

#define ROOMTEMPERATURE_UPPERLIMIT 32.0f
#define ROOMTEMPERATURE_LOWERLIMIT 25.0f
#define STARTING_ROOMTEMPERATURE ROOMTEMPERATURE_LOWERLIMIT

/**
 * @file shadow_console_echo.c
 * @brief  Echo received Delta message
 *
 * This application will echo the message received in delta, as reported.
 * for example:
 * Received Delta message
 * {
 *    "state": {
 *       "switch": "on"
 *   }
 * }
 * This delta message means the desired switch position has changed to "on"
 *
 * This application will take this delta message and publish it back as the reported message from the device.
 * {
 *    "state": {
 *     "reported": {
 *       "switch": "on"
 *      }
 *    }
 * }
 *
 * This update message will remove the delta that was created. If this message was not removed then the AWS IoT Thing Shadow is going to always have a delta and keep sending delta any time an update is applied to the Shadow
 * This example will not use any of the json builder/helper functions provided in the aws_iot_shadow_json_data.h.
 * @note Ensure the buffer sizes in aws_iot_config.h are big enough to receive the delta message. The delta message will also contain the metadata with the timestamps
 */

char certDirectory[PATH_MAX + 1] = "../data/certs";
char HostAddress[255] = AWS_IOT_MQTT_HOST;
uint32_t port = AWS_IOT_MQTT_PORT;
bool messageArrivedOnDelta = false;

uint8_t numPubs = 5;

float appParam_float = 0.0;

jsonStruct_t deltaObject_1;
jsonStruct_t deltaObject_float;

//typedef struct {
//	int cpu_temperature;
//	int cpu_usage;
//} ApplicationParameters_t;
//
//const ApplicationParameters_t ApplicationParametersDefault = {
//		.cpu_temperature = 100,
//		.cpu_usage = 100,
//};

/*
 * @note The delta message is always sent on the "state" key in the json
 * @note Any time messages are bigger than AWS_IOT_MQTT_RX_BUF_LEN the underlying MQTT library will ignore it. The maximum size of the message that can be received is limited to the AWS_IOT_MQTT_RX_BUF_LEN
 */
char JsonDocumentBuffer_Delta[SHADOW_MAX_SIZE_OF_RX_BUFFER];

// Helper functions
void parseInputArgsForConnectParams(int argc, char** argv);

// Shadow Callback for receiving the delta
void DeltaCallback(const char *pJsonValueBuffer, const uint32_t valueLength, jsonStruct_t *pJsonStruct_t);

void ShadowUpdateCallback(const char *pThingName, ShadowActions_t action, Shadow_Ack_Status_t status,
		const char *pReceivedJsonDocument, void *pContextData);

/**
 * @brief This function builds a full Shadow expected JSON document by putting the data in the reported section
 *
 * @param pJsonDocument Buffer to be filled up with the JSON data
 * @param maxSizeOfJsonDocument maximum size of the buffer that could be used to fill
 * @param pReceivedDeltaData This is the data that will be embedded in the reported section of the JSON document
 * @param lengthDelta Length of the data
 */
bool buildJSONForReported(char *pJsonDocument, size_t maxSizeOfJsonDocument, const char *pReceivedDeltaData, uint32_t lengthDelta) {
	int32_t ret;

	if (pJsonDocument == NULL) {
		return false;
	}

	char tempClientTokenBuffer[MAX_SIZE_CLIENT_TOKEN_CLIENT_SEQUENCE];

	if(aws_iot_fill_with_client_token(tempClientTokenBuffer, MAX_SIZE_CLIENT_TOKEN_CLIENT_SEQUENCE) != NONE_ERROR){
		return false;
	}

	ret = snprintf(pJsonDocument, maxSizeOfJsonDocument, "{\"state\":{\"reported\":%.*s}, \"clientToken\":\"%s\"}", lengthDelta, pReceivedDeltaData, tempClientTokenBuffer);

	if (ret >= maxSizeOfJsonDocument || ret < 0) {
		return false;
	}

	return true;
}

//bool extractAppParam_float(const char *pJsonDocument, void *pJsonHandler, int32_t tokenCount, float *pAppParam_float) {
//	int32_t i;
//	jsmntok_t *pJsonTokenStruct;
//	IoT_Error_t ret_val = NONE_ERROR;
//
//	pJsonTokenStruct = (jsmntok_t *) pJsonHandler;
//	for (i = 1; i < tokenCount; i++) {
//		if (jsoneq(pJsonDocument, &(jsonTokenStruct[i]), "AppParam_float") == 0) {
//			jsmntok_t dataToken = jsonTokenStruct[i + 1];
//			uint32_t dataLength = dataToken.end - dataToken.start;
//			ret_val = parseFloatValue(pAppParam_float, pJsonDocument, &dataToken);
//			if (ret_val == NONE_ERROR) {
//				return true;
//			}
//		}
//	}
//	return false;
//}

void DeltaCallback(const char *pJsonValueBuffer, const uint32_t valueLength, jsonStruct_t *pJsonStruct_t) {

	DEBUG("Received Delta message %.*s", valueLength, pJsonValueBuffer);
	printf("[Delta Msg Received]: %.*s\n", valueLength, pJsonValueBuffer);

	char valueOnlyBuf[AWS_IOT_MQTT_RX_BUF_LEN] = {'\0'};
	snprintf(valueOnlyBuf, valueLength+1, "%.*s", valueLength+1, pJsonValueBuffer);

	// -- Extract the Shadow parameter values from the received Json message
	if(pJsonStruct_t == &deltaObject_1) {
//		printf("#### pJsonStruct_t == deltaObject_1\n");

	} else if(pJsonStruct_t == &deltaObject_float) {
		printf("#### pJsonStruct_t == deltaObject_float\n");

		float appParam_float_desired;

		sscanf(valueOnlyBuf, "%f", &appParam_float_desired);

		// -- Update the current parameter with the 'desired' parameter received
		if(appParam_float_desired != appParam_float) {
			appParam_float = appParam_float_desired;
			messageArrivedOnDelta = true;

			printf("[Parsed] 'desired' temperature = (%s, %f)\n", valueOnlyBuf, appParam_float);
		}

	} else {
		printf("#### pJsonStruct_t ??? \n");
	}


	// -- Build a Shadow message to be sent to AWS IoT with the updated values
//	if (buildJSONForReported(JsonDocumentBuffer_Delta, SHADOW_MAX_SIZE_OF_RX_BUFFER, pJsonValueBuffer, valueLength)) {
//
//		printf("[Shadow Msg To be Sent upon reception of Delta Msg]: %s\n", JsonDocumentBuffer_Delta);
//
//		messageArrivedOnDelta = true;
//	}
}

static void simulateRoomTemperature(float *pRoomTemperature) {
	static float deltaChange;

	if (*pRoomTemperature >= ROOMTEMPERATURE_UPPERLIMIT) {
		deltaChange = -0.5f;
	} else if (*pRoomTemperature <= ROOMTEMPERATURE_LOWERLIMIT) {
		deltaChange = 0.5f;
	}

	*pRoomTemperature += deltaChange;
}

void updateShadowMsg(float temperature, char *shadowMsg) {

	sprintf(shadowMsg, "(Temperature = %f)", temperature);

}

void ShadowUpdateCallback(const char *pThingName, ShadowActions_t action, Shadow_Ack_Status_t status,
		const char *pReceivedJsonDocument, void *pContextData) {

	if (status == SHADOW_ACK_TIMEOUT) {
		INFO("Update Timeout--");
	} else if (status == SHADOW_ACK_REJECTED) {
		INFO("Update RejectedXX");
	} else if (status == SHADOW_ACK_ACCEPTED) {
		INFO("Update Accepted !!");
	}
}

void windowActuate_Callback(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext) {
	if (pContext != NULL) {
		INFO("Delta - Window state changed to %d", *(bool *)(pContext->pData));
	}
}

void parseInputArgsForConnectParams(int argc, char** argv) {
	int opt;

	while (-1 != (opt = getopt(argc, argv, "h:p:c:n:"))) {
		switch (opt) {
		case 'h':
			strcpy(HostAddress, optarg);
			DEBUG("Host %s", optarg);
			break;
		case 'p':
			port = atoi(optarg);
			DEBUG("arg %s", optarg);
			break;
		case 'c':
			strcpy(certDirectory, optarg);
			DEBUG("cert root directory %s", optarg);
			break;
		case 'n':
			numPubs = atoi(optarg);
			DEBUG("num pubs %s", optarg);
			break;
		case '?':
			if (optopt == 'c') {
				ERROR("Option -%c requires an argument.", optopt);
			} else if (isprint(optopt)) {
				WARN("Unknown option `-%c'.", optopt);
			} else {
				WARN("Unknown option character `\\x%x'.", optopt);
			}
			break;
		default:
			ERROR("ERROR in command line argument parsing");
			break;
		}
	}

}

#define MAX_LENGTH_OF_UPDATE_JSON_BUFFER 450

//typedef struct {
//	float cpu_temperature;
//	float cpu_usage;
//} HardwareModMsg_t;
//
//typedef struct {
//	HardwareModMsg_t	hadrwareMsg;
//} ShadowMsg_t;

void state_Callback(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext) {
	if (pJsonString != NULL) {
		INFO("Delta - state changed to %.*s", JsonStringDataLen, pJsonString);
	}
}

char gMyThingName[MAX_SIZE_OF_THING_NAME];

int main(int argc, char** argv) {

	sprintf(gMyThingName, AWS_IOT_MY_THING_NAME);

	IoT_Error_t rc = NONE_ERROR;
	int32_t i = 0;

	char rootCA[PATH_MAX + 1];
	char clientCRT[PATH_MAX + 1];
	char clientKey[PATH_MAX + 1];
	char CurrentWD[PATH_MAX + 1];
	char cafileName[] = AWS_IOT_ROOT_CA_FILENAME;
	char clientCRTName[] = AWS_IOT_CERTIFICATE_FILENAME;
	char clientKeyName[] = AWS_IOT_PRIVATE_KEY_FILENAME;

	parseInputArgsForConnectParams(argc, argv);

	INFO("\nAWS IoT SDK Version(dev) %d.%d.%d-%s\n", VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH, VERSION_TAG);

	getcwd(CurrentWD, sizeof(CurrentWD));
	sprintf(rootCA, "%s/%s/%s", CurrentWD, certDirectory, cafileName);
	sprintf(clientCRT, "%s/%s/%s", CurrentWD, certDirectory, clientCRTName);
	sprintf(clientKey, "%s/%s/%s", CurrentWD, certDirectory, clientKeyName);

	DEBUG("Using rootCA %s", rootCA);
	DEBUG("Using clientCRT %s", clientCRT);
	DEBUG("Using clientKey %s", clientKey);

	MQTTClient_t mqttClient;
	aws_iot_mqtt_init(&mqttClient);

	char JsonDocumentBuffer_Periodic[MAX_LENGTH_OF_UPDATE_JSON_BUFFER];
	size_t sizeOfJsonDocumentBuffer_Periodic = sizeof(JsonDocumentBuffer_Periodic) / sizeof(JsonDocumentBuffer_Periodic[0]);
	char *pJsonStringToUpdate;

		jsonStruct_t applicationParamHandler;
		applicationParamHandler.cb = NULL;
		applicationParamHandler.pKey = "appParam_float";
		applicationParamHandler.pData = &appParam_float;
		applicationParamHandler.type = SHADOW_JSON_FLOAT;

	bool windowOpen = true;
	jsonStruct_t windowActuator;
	windowActuator.cb = windowActuate_Callback;
	windowActuator.pData = &windowOpen;
	windowActuator.pKey = "windowOpen";
	windowActuator.type = SHADOW_JSON_BOOL;

//	float temperature = 0.0;
//
//	jsonStruct_t temperatureHandler;
//	temperatureHandler.cb = NULL;
//	temperatureHandler.pKey = "temperature";
//	temperatureHandler.pData = &appParam_float;
//	temperatureHandler.type = SHADOW_JSON_FLOAT;

	char	shadowMsg[99] = {'\0'};

	jsonStruct_t ShadowMsgHandler;
	ShadowMsgHandler.cb = NULL;
	//ShadowMsgHandler.cb = state_Callback;
	ShadowMsgHandler.pKey = "devState";
	ShadowMsgHandler.pData = shadowMsg;
	//ShadowMsgHandler.pData = NULL;//&shadowMsg;
	ShadowMsgHandler.type = SHADOW_JSON_STRING;

	//rc = aws_iot_shadow_register_delta(&mqttClient, &ShadowMsgHandler);

		ShadowParameters_t sp = ShadowParametersDefault;
		sp.pMyThingName = AWS_IOT_MY_THING_NAME;
		sp.pMqttClientId = AWS_IOT_MQTT_CLIENT_ID;
		sp.pHost = AWS_IOT_MQTT_HOST;
		sp.port = AWS_IOT_MQTT_PORT;
		sp.pClientCRT = clientCRT;
		sp.pClientKey = clientKey;
		sp.pRootCA = rootCA;

		INFO("Shadow Init");
		rc = aws_iot_shadow_init(&mqttClient);
		if (NONE_ERROR != rc) {
			ERROR("Shadow Connection Error");
			return rc;
		}

		INFO("Shadow Connect");
		rc = aws_iot_shadow_connect(&mqttClient, &sp);
		if (NONE_ERROR != rc) {
			ERROR("Shadow Connection Error");
			return rc;
		}

////		jsonStruct_t deltaObject_1;
		deltaObject_1.pData = JsonDocumentBuffer_Delta;
		deltaObject_1.pKey = "state";
		deltaObject_1.type = SHADOW_JSON_OBJECT;
		deltaObject_1.cb = DeltaCallback;

		deltaObject_float.pData = JsonDocumentBuffer_Delta;
		deltaObject_float.pKey = "temperature";
		deltaObject_float.type = SHADOW_JSON_FLOAT;
		deltaObject_float.cb = DeltaCallback;

	if (NONE_ERROR != rc) {
		ERROR("Shadow Connection Error %d", rc);
	}

	/*
	 * Register the jsonStruct object
	 */
	rc = aws_iot_shadow_register_delta(&mqttClient, &deltaObject_1);
	rc = aws_iot_shadow_register_delta(&mqttClient, &deltaObject_float);

	if (NONE_ERROR != rc) {
		ERROR("Shadow Register Delta Error");
	}
	appParam_float = STARTING_ROOMTEMPERATURE;

	// loop and publish a change in temperature
	while (NONE_ERROR == rc) {

		// -- Lets check for the incoming messages for 200 ms.
		rc = aws_iot_shadow_yield(&mqttClient, 200);

		INFO("\n=======================================================================================\n");
		INFO("On Device: window state %s", windowOpen?"true":"false");

		// -- Generate a Shadow value for an attribute
//		simulateRoomTemperature(&temperature);

		// -- Generate a Shadow value for another attribute
		updateShadowMsg(appParam_float, shadowMsg);

		// -- Initialize the Shadow message buffer
		rc = aws_iot_shadow_init_json_document(JsonDocumentBuffer_Periodic, sizeOfJsonDocumentBuffer_Periodic);

		// -- Update Shadow at AWS IoT with the new values created within the device
		if (rc == NONE_ERROR) {

			// -- Add the Shadow values for the attributes to the Shadow message buffer
			rc = aws_iot_shadow_add_reported(JsonDocumentBuffer_Periodic, sizeOfJsonDocumentBuffer_Periodic, 3, &applicationParamHandler, &ShadowMsgHandler, &windowActuator);

			if (rc == NONE_ERROR) {

				// -- Add some more meta-data to the message buffer in the JSON format and finalize the message composition
				rc = aws_iot_finalize_json_document(JsonDocumentBuffer_Periodic, sizeOfJsonDocumentBuffer_Periodic);

				if (rc == NONE_ERROR) {
					INFO("Update Shadow 1: %s", JsonDocumentBuffer_Periodic);

					// -- Actually send the Shadow update to AWS IoT
					rc = aws_iot_shadow_update(&mqttClient, AWS_IOT_MY_THING_NAME, JsonDocumentBuffer_Periodic, ShadowUpdateCallback, NULL, 4, true);
				}
			}
		}

		// -- Upon the detection of a update delta message, it tries to update Shadow with the new value from the delta message
		if (messageArrivedOnDelta) {
			INFO("\nSending delta message back %s\n", JsonDocumentBuffer_Delta);

			// -- Resend the current Shadow message to let AWS IoT know the current Shadow values are updated according to the 'desired' Shadow values.
			// -- AWS IoT then looks at the 'reported' Shadow values and see if they are the same as the 'desired' values. If they are not the same, then issues another Shadow Delta message to the device.
			{
				size_t sizeOfJsonDocumentBuffer_Delta = sizeof(JsonDocumentBuffer_Delta) / sizeof(JsonDocumentBuffer_Delta[0]);

				// -- Initialize the Shadow message buffer
				rc = aws_iot_shadow_init_json_document(JsonDocumentBuffer_Delta, sizeOfJsonDocumentBuffer_Delta);

				// -- Update Shadow at AWS IoT with the new values created within the device
				if (rc == NONE_ERROR) {

					// -- Add the Shadow values for the attributes to the Shadow message buffer
					rc = aws_iot_shadow_add_reported(JsonDocumentBuffer_Delta, sizeOfJsonDocumentBuffer_Delta, 3, &applicationParamHandler, &ShadowMsgHandler, &windowActuator);

					if (rc == NONE_ERROR) {

						// -- Add some more meta-data to the message buffer in the JSON format and finalize the message composition
						rc = aws_iot_finalize_json_document(JsonDocumentBuffer_Delta, sizeOfJsonDocumentBuffer_Delta);

						if (rc == NONE_ERROR) {
							INFO("Update Shadow 2: %s", JsonDocumentBuffer_Delta);

							// -- Actually send the Shadow update to AWS IoT
							rc = aws_iot_shadow_update(&mqttClient, AWS_IOT_MY_THING_NAME, JsonDocumentBuffer_Delta, ShadowUpdateCallback, NULL, 4, true);
						}
					}
				}
			}

			// -- Resend the current Shadow message to let AWS IoT know the current Shadow values are updated according to the 'desired' Shadow values.
			// -- AWS IoT then looks at the 'reported' Shadow values and see if they are the same as the 'desired' values. If they are not the same, then issues another Shadow Delta message to the device.
//			rc = aws_iot_shadow_update(&mqttClient, AWS_IOT_MY_THING_NAME, JsonDocumentBuffer_Delta, ShadowUpdateCallback, NULL, 4, true);

			fprintf(stdout, "Msg Arrived!\n");
			messageArrivedOnDelta = false;
		}

		INFO("*****************************************************************************************\n");
		sleep(1);
	}

	if (NONE_ERROR != rc) {
		ERROR("An error occurred in the loop %d", rc);
	}

	INFO("Disconnecting");
	rc = aws_iot_shadow_disconnect(&mqttClient);

	if (NONE_ERROR != rc) {
		ERROR("Disconnect error %d", rc);
	}

	return rc;
}

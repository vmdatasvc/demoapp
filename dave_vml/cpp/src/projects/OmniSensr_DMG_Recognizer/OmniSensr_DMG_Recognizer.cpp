/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/****************************************************************************/

// OmniSensr_DMG_Recognizer.cpp
//
//   command line automatic demographics recognizer tool for OmniSensr
//
//   Dave Donghun Kim
//   2016.9.5

#include <cstdio>
#include <getopt.h>
#include <dirent.h>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/replace.hpp>

#include <modules/utility/restAPI.hpp>

#include <core/Error.hpp>
#include <modules/utility/Util.hpp>
#include <modules/utility/TimeKeeper.hpp>
#include <modules/utility/DateTime.hpp>
#include <modules/ml/FaceVideoModule.hpp>
#include <modules/ml/MultiTargetFaceTracker.hpp>

// -- AWS IoT SDK files for State Parameter Type definition
#include <aws_iot_error.h>
#include <aws_iot_shadow_json_data.h>

#include <modules/iot/IoT_Common.hpp>
#include <modules/iot/IoT_Gateway.hpp>

// -- For Heartbeat
#include "modules/iot/IOTCommunicator.hpp"

// -- Refer to https://github.com/nlohmann/json  // For no error on building codes, required to update gcc/g++ version over 4.9
#include <../external/jsonByNlohmann/json.hpp>
#include <iosfwd>

//#include <json.hpp>
//using json = nlohmann::json;

#define IOT_GATEWAY_CONFIG_FILE		"./config/IoT_Gateway/IoT_Gateway_config.txt"
#define SETTINGS_PATH				"./config/OmniSensr_DMG_Recognizer"
#define TEST_VIDEO_FILE				"./TestVideo/TestVideo4.avi"
#define INPUT_VIDEO_URL				"http://localhost:8080/?action=stream?video.mjpeg"

// For secured communication with Cloudinary
#define USE_CLOUDINARY	0
#define USE_SFTP		1

#define API_KEY			"611288379721968"
#define API_SECRET		"QBQ3C3T1oQgs1OswI2knOMmcf9Q"
#define CLOUD_NAME		"vmldave"

#define DEBUG_IOT	0

// logger message macros
#define STARTLOGGING			logger.startLogging()
#define LOGMESSAGE(__MSG__)		if(bVerbose){logger.outputMessage(__MSG__);}

// globals
bool	bProcessStop = false;
bool	bThreadsStop = false;
bool	bVerbose = false;
bool	bDisplay = false;
bool	bTestRun = false;
bool	bCalFPS = false;
float 	nTimeLeng = 5;
// For face sending thread
bool 	bFaceSend = false;

// For multi-thread processing
//bool	bMultithread = false;
int	nProcessingThreads = 2;

// tracker module pointers
boost::shared_ptr<SETTINGS> pSettings;
boost::shared_ptr<vml::FaceVideoModule> pVideoModule;
boost::shared_ptr<vml::MultiTargetFaceTracker> pTracker;
boost::shared_ptr<IOT_Communicator>	pIOTc;

// for verbose output
vml::Logger	logger;		

// just for the testing data
std::string	settings_path = SETTINGS_PATH;
std::string input_source_addr = INPUT_VIDEO_URL;


//-----------------< IoT-realted codes  >---------------------//
// IoT members
IoT_Gateway	IoTGateway;
//std::map<std::string,std::string>	stateKeyMap;

// IoT app id
AppType_t		appID = demoApp;
std::string		mAppType;

//string  mAppType;

// IoT state variables;
//float	IoTState_FrameRate;
//std::string IoTState_FaceTracker_trackingRegion;
//int		IoTState_DataReportRefreshInterval;
//int		IoTState_StateReportRefreshInterval;
//float	IoTState_FaceDetector_scaleFactor;
//float	IoTState_FaceDetector_minNeighbors;

std::string IoTState_FaceTracker_trackingMethod;
double	IoTState_FaceTracker_searchWindowRatio;
double	IoTState_FaceTracker_endTrackerParam;
double	IoTState_FaceTracker_inactiveTimeout;
double	IoTState_FaceTarget_reactiveLikelihoodTh;
double	IoTState_FaceTarget_verifyTargetVisualSimilarityTh;

double	IoTState_DataReportRefreshInterval;
double	IoTState_StateReportRefreshInterval;
int 	IoTstate_numberStoreFaces;
int	IoTState_TrackRegionVersion;
double  IoTState_FaceSendRefreshInterval;

// track region control
std::string	IoTControl_TrackRegion;


typedef struct {
	JsonPrimitiveType type;
	std::string key;
	std::string path;
	bool		response_to_delta;
	void*		pVar;
} stateKeyMapStructType;

//std::map<std::string,std::string>	stateKeyMap;
typedef stateKeyMapStructType *stateKeyMapPtr;

std::map<std::string,stateKeyMapPtr> stateKeyMap;

#define N_STATEKEYMAP	11

stateKeyMapStructType	stateMapping[N_STATEKEYMAP] = {
	{SHADOW_JSON_STRING, "trackMethod","FaceTracker/trackingMethod",true,&IoTState_FaceTracker_trackingMethod},
	{SHADOW_JSON_DOUBLE, "searchWindowRatio", "FaceTracker/searchWindowRatio",true,&IoTState_FaceTracker_searchWindowRatio},
	{SHADOW_JSON_DOUBLE, "endTrackerParam","FaceTracker/endTrackerParam",true,&IoTState_FaceTracker_endTrackerParam},
	{SHADOW_JSON_DOUBLE, "inactiveTimeOut", "FaceTracker/inactiveTimeout",true,&IoTState_FaceTracker_inactiveTimeout},
	{SHADOW_JSON_DOUBLE, "reactivateTh","FaceTarget/reactivateLikelihoodThreshold",true,&IoTState_FaceTarget_reactiveLikelihoodTh},
	{SHADOW_JSON_DOUBLE, "visualSimilarityTh", "FaceTarget/verfyTargetVisualSimiliarityThreshold",true,&IoTState_FaceTarget_verifyTargetVisualSimilarityTh},
	{SHADOW_JSON_DOUBLE,"dataReportRefreshInt", "IoT/dataReportRefreshInterval",true,&IoTState_DataReportRefreshInterval},
	{SHADOW_JSON_DOUBLE,"stateReportRefreshInt", "IoT/stateReportRefreshInterval",true,&IoTState_StateReportRefreshInterval},
	{SHADOW_JSON_INT32,   "numStoreFaces", "IoT/numberStoreFaces",true,&IoTstate_numberStoreFaces},
	{SHADOW_JSON_INT32, "trackRegionVer", "IoT/trackPolygonVersion",false,&IoTState_TrackRegionVersion},
	{SHADOW_JSON_DOUBLE, "faceSendRefreshInt", "IoT/faceSendRefreshInterval",true,&IoTState_FaceSendRefreshInterval}
};

//std::string	IoTState_BaseVisionTracker_trackingMethod;

// Multi-threading members
pthread_attr_t	threadAttr;
pthread_t		dataReportThread;
pthread_t		stateReportThread;
pthread_mutex_t	gMutex;		// NOTE: need this because IoT_Gateway class declares gMutex as extern, so this should be somewhere in the source.
pthread_mutex_t appMutex=PTHREAD_MUTEX_INITIALIZER;	// for syncing apps

vml::FileParts	settings_file_parts;

// Face image sending thread
pthread_t 	faceSendThread;
pthread_mutex_t fdMutex=PTHREAD_MUTEX_INITIALIZER;	// for syncing apps

// -- Define callback functions for messages from Delta State and Control Channel

// thread prototypes
void *dataReportThreadFunc(void *apData);
void *stateReportThreadFunc(void *apData);
void startTracker();
void stopTracker();
void stateInitialization();
void updateStateFromSettings();

void *faceSendThreadFunc(void *apData);

// IoT callbacks
void IoT_DeltaState_CallBack_func(string entireDeltaMsg);
void IoT_ControlChannel_CallBack_func(std::string topic, std::string controlMsg);

// functions
void saveMetaDataToLocal(std::string pname, std::string msg);
void sendSingleFaceToCloud(std::string filename, std::string pid, cv::Mat faceMat); // vml::SingleFaceDataType facevec);
bool sendImageToCloud(std::string pid, cv::Mat fdata);
bool sendImageToSFTP(std::string pid, cv::Mat fdata);
bool sendImageFileToSFTP(std::string filename);

void initializeIoTGateway();
//void addStateParams();  // check out

// Body
void* dataReportThreadFunc(void *apdata) {

	vml::MultiTargetFaceTracker* pt = (vml::MultiTargetFaceTracker*)apdata;
	const int	dataReportRefreshInterval = (int)(1000000.f*pSettings->getDouble("IoT/dataReportRefreshInterval",2.f,false));
	const bool  DoDmgRecog = pSettings->getBool("DMGRecognition/doDMGRecognition [bool]",false,false);
	do {

		// -- Send data
//		if(data_cnt_str.str().size() > 0)
//		{
//			// -- Put into a message buffer for transmission
//			// -- The data message will be sent to a topic named by 'AWS_IOT_APP_DATA_CHANNEL_PREAMBLE/<store_name>/<serial_number>/<app_id>'
//			mIoTGateway.SendData(data_cnt_str.str());
//
//			if(DEBUG)
//				printf("[SampleApp] Sending Data: %s\n", data_cnt_str.str().c_str());
//		}

		usleep(dataReportRefreshInterval);	// data report interval. Default is 2 sec.

		// -- Periodically process/send data to the IoT Cloud

		// construct data message: 1) Labels and metadata, 2) corresponding face images

		// 1. send face images to cloudinary and get the urls
		// 2. incorporate the urls into the metadata json file including demographics labels and confidences, the number of available faces
		// 3. send the json file to the cloud by the MQTT brocker

		// get completed facedata
		std::vector<vml::TargetFaceData>  fdataVec;
		pt->getCompletedFaceData(fdataVec);

		if(fdataVec.size() > 0)   // if there is any completed target, then go down.
		{
			if(bVerbose)
			{
				std::stringstream pmsg;
				pmsg << fdataVec.size() <<" face data are completed and being reported"; // << std::endl;
				logger.outputMessage(pmsg.str());
			}

			// Send a face to cloudinary
			for (unsigned int i=0;i<fdataVec.size();i++)
			{
				Json::Value farr, sarr, larr, parrx, parry, parrsizex, parrsizey;

				std::vector<vml::SingleFaceDataType> sfvec = fdataVec[i].getSelectedFaceData();

				// for each completed target
				for (unsigned int j=0;j< sfvec.size();j++)
				{
					parrx[j] = (double)sfvec[j].loc.x / (double)pVideoModule->getCols();
					parry[j] = (double)sfvec[j].loc.y / (double)pVideoModule->getRows();
					sarr[j] = sfvec[j].time;

					parrsizex[j] = sfvec[j].faceSize.x;
					parrsizey[j] = sfvec[j].faceSize.y;

					// add the face url to Json
					// "public_id".jpg:  "cloud_url/storeName/devName/timestamp-uuid_index_gae".jpg

					vml::DateTime::Struct ds = vml::DateTime(sfvec[j].time).getStruct();
					//vml::DateTime::secsToLocal(gCurrentTime(), ds);

					std::stringstream sfilename, sfilename2;

					if(DoDmgRecog)
					{
						sfilename << IoTGateway.GetMyStoreName() << "/" << IoTGateway.GetMyDevName() << "/" << ds.year << "/" << ds.month << "/" << ds.day << "/" <<
							fdataVec[i].id << "_" << j << "_" << sfvec[j].genderLabel << sfvec[j].ageLabel << sfvec[j].ethnicityLabel;
					}
					else
					{
						sfilename << IoTGateway.GetMyStoreName() << "/" <<  IoTGateway.GetMyDevName() << "/" << ds.year << "/" << ds.month << "/" << ds.day << "/" <<
							fdataVec[i].id << "_" << j;
					}

					LOGMESSAGE("FaceImageName: " + sfilename.str());

					// change the / to -
					std::string savename(sfilename.str());
					boost::replace_all(savename, "/", "@");

					std::string face_url;

					if (USE_CLOUDINARY)
					{
						std::string clname(CLOUD_NAME);
						std::string cloud_url =  "https://api.cloudinary.com/v1_1/"+ clname +"/image/upload/";

						face_url = cloud_url + sfilename.str() + ".png";
					}
					else
					{
						face_url = sfilename.str() + ".png";
					}

					farr[j]= face_url;

					// Send a face image to cloudinary as the prefixed name (public_id)
					sendSingleFaceToCloud(face_url.c_str(),savename, sfvec[j].data);
				}

				Json::Value root;
				root = fdataVec[i].mvJson;
				root["faceURL"] = farr;
				root["faceTimestamp"] = sarr;
				root["facePosX"] = parrx;
				root["facePosY"] = parry;
				root["faceSizeX"] = parrsizex;
				root["faceSizeY"] = parrsizey;

				fdataVec[i].mvJson = root;
			}


			std::stringstream msgstream;

			// add data body
			for (unsigned int i=0;i<fdataVec.size();i++)
			{
				// load the json data to stringstream
				Json::StyledWriter styledWriter;
				msgstream << styledWriter.write(fdataVec[i].mvJson);
				//msgstream << fdata[i].toXmlString(pSettings->getInt("trajectoryReportDecimals",10,false));
			}

			// send the meta data
			IoTGateway.SendData(msgstream.str());
			LOGMESSAGE("META DATA: Sent meta data to the IoT server.");

/*
			bool use_local_cloud = pSettings->getBool("IoT/sendFaceDataToLocal",true,false);

			if(use_local_cloud)
			{
				saveMetaDataToLocal( fdataVec[0].id, msgstream.str());
				LOGMESSAGE("META DATA: Sent meta data to local drive.");
			}
			else
			{
				IoTGateway.SendData(msgstream.str());
				LOGMESSAGE("META DATA: Sent meta data to the IoT server.");
			}
*/
		}

	} while(!bThreadsStop);

    return NULL;
}

void saveMetaDataToLocal(std::string pname, std::string msg)
{

	std::string path = pSettings->getString("IoT/localStorePath", "", false);
	std::string filename = path + pname.c_str() + ".json";

	std::ofstream savefile;
	savefile.open(filename.c_str());
	savefile << msg.c_str();
	savefile.close();
}

void sendSingleFaceToCloud(std::string filename, std::string pid, cv::Mat faceMat) // vml::SingleFaceDataType facevec)
{

	// filename convention: store_name/cam_name/time_uuid_imageNumber.jpg
	//std::ostringstream ossr;
	//ossr << id << "_" << index;

	int use_cloud = pSettings->getInt("IoT/sendFaceDataToCloud",0,false);

	if(use_cloud == 0)
	{
		// write a face image to a local disk(path)
		std::string path = pSettings->getString("IoT/localStorePath", "", false);
		std::string facefile =  path + pid.c_str() + ".png";

		std::vector<int> im_params;
		//im_params.push_back(CV_IMWRITE_JPEG_QUALITY);
		//im_params.push_back(100);
		im_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
		im_params.push_back(0);
		cv::imwrite(facefile.c_str(), faceMat, im_params);

	}
	else
	{
		// Goal: Send the image file (or image buffer) to the cloud and recieve the url
		// TO-DO: Use the library from the cloud storage api like cloudinary API
		// write a face image to a local disk(path)

		if(use_cloud == 1)
		{
			bool ret =sendImageToCloud(filename, faceMat);

			if(bVerbose)
			{
				if(ret) {
					LOGMESSAGE("Cloudinary: failed to send a face image.");
				}
				else {
					LOGMESSAGE("Cloudinary: succeeded to send a face image.");
				}
			}
		}
		else if(use_cloud == 2)  // USE_SFTP direct access
		{
			// change the / to -
			std::string spid(pid.c_str());
			boost::replace_all(spid, "@", "/");

			bool ret =sendImageToSFTP(spid, faceMat);

			if(bVerbose)
			{
				if(ret) {
					LOGMESSAGE("SFTP: succeeded to send a face image.");
				}
				else {
					LOGMESSAGE("SFTP: failed to send a face image.");
				}
			}

		}

	}
}

// for sending an image to a cloud
// <Test enviroment>
// Cloud name in the cloudinary : vmldave
// API key: 611288379721968
// API Secrete: QBQ3C3T1oQgs1OswI2knOMmcf9Q
// CLOUDINARY_URL=cloudinary://611288379721968:QBQ3C3T1oQgs1OswI2knOMmcf9Q@vmldave
bool sendImageToCloud(std::string pid, cv::Mat fdata)
{
	//Clone frame and encode into buffer as JPEG
	std::vector<unsigned char> imgBuffer;
	cv::imencode(".jpg", fdata, imgBuffer);

	//Buffer to String with pointer/length
	std::string imgStrBuff = std::string(imgBuffer.begin(), imgBuffer.end());
    char *imgBuffPtr = (char *)imgBuffer.data();
    long imgBuffLength = static_cast<long>(imgStrBuff.size());

    // API_key
    std::string APIKey = API_KEY;

	// API_Secret:
	std::string APISecret = API_SECRET;

	// cloud_name
	std::string cloudName = CLOUD_NAME;

	// public_id
	std::string public_id = pid;

	long res_code;
	vml::RestAPI cp;

	cp.setConfig(APIKey, APISecret, cloudName, "");
	res_code = cp.uploadBufferedData(public_id, true, imgBuffPtr, imgBuffLength);

	if(res_code != 200)
	{
		return false;
	}
	else
	{
		return true;
	}

}

bool sendImageToSFTP(std::string pid, cv::Mat fdata)
{
	// 1. write a face image to a ram disk
	std::string filename = pid + ".png";
	std::string path = pSettings->getString("IoT/localStorePath", "", false);
	std::string facefile =  path + filename.c_str();

	std::vector<int> im_params;
	//im_params.push_back(CV_IMWRITE_JPEG_QUALITY);
	//im_params.push_back(100);
	im_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
	im_params.push_back(0);

	cv::imwrite(facefile.c_str(), fdata, im_params);

	// change the / to -
	std::string spid(pid.c_str());
	boost::replace_all(spid, "@", "/");


    // 2. send the image
      std::string username = "demographics";
      std::string password = "demoftp";  // changed new password at 1/12/17
      std::string url = "sftp://demoftp.videomining.com:22/images/";
      //std::string pid = "DKIM-16801-cam008-gae-0c16f18c-41b6-4746-8ea2-7a21cf72d355_n2";

      // Use the vml::RestAPI by dave

      long res_code = 0;
      vml::RestAPI  cp;

      // setup config data
      cp.setConfigSFTP(url,username,password);

      // a case of using filename
      //std::string filename ="ai.jpg";
      bool ret = cp.uploadFileSFTP(spid,facefile);

      if(ret)
      {
   	     // 3. remove a file
   	      if(std::remove(facefile.c_str()) != 0)
   	      {
   	    	  printf("Error deleting file in a ramdisk");
   	      }

    	  if(DEBUG_IOT)
    		  printf("SFTP: uploading is succeeded: %ld\n",  res_code);

      }
      else
      {
    	  if(DEBUG_IOT)
    		  printf("SFTP: uploading is failed: %ld\n",res_code);
      }

      return ret;
}

bool sendImageFileToSFTP(std::string pid, std::string filepath)
{

	// change the / to -
	std::string spid(pid.c_str());
	boost::replace_all(spid, "@", "/");


    // 2. send the image
      std::string username = "demographics";
      std::string password = "demoftp";  // changed new password at 1/12/17
      std::string url = "sftp://demoftp.videomining.com:22/images/";

      // Use the vml::RestAPI by dave

      long res_code = 0;
      vml::RestAPI  cp;

      // setup config data
      cp.setConfigSFTP(url,username,password);

      // a case of using filename
      //std::string filename ="ai.jpg";
      bool ret = cp.uploadFileSFTP(spid,filepath);

      // 200 is the
      if(ret)
      {
    	  if(DEBUG_IOT)
    		  printf("SFTP: uploading is successful.\n");

   	     // 3. remove a file
   	      if(std::remove(filepath.c_str()) != 0)
   	      {
   	    	  if(DEBUG_IOT)
   	   	    	  printf("SFTP: deleting file in a ramdisk is failed.\n");

   	    	  ret= false;
   	      }
   	      else
   	      {
   	    	  if(DEBUG_IOT)
   	   	    	  printf("SFTP: deleting file in a ramdisk is sucessful.\n");
   	      }

      }
      else
      {
    	  if(DEBUG_IOT)
    		  printf("SFTP: uploading is failed: %ld\n",res_code);

      }

      return ret;

}

void *faceSendThreadFunc(void *apData)
{
	const int	faceSendRefreshInterval = (int)(1000000.f*pSettings->getDouble("IoT/faceSendRefreshInterval",120.f,false));   // 1 min
	std::string sDir = pSettings->getString("IoT/localStorePath", "", false);
	boost::filesystem::path dir_path = sDir.c_str();

	std::vector<std::string> files;
	std::vector<std::string> ids;

	do {

		usleep(faceSendRefreshInterval);

		// TODO: do we have to lock this with mutex?
		// read the file list in the specific directory at a time
		// readFileList();

		for( auto i=boost::filesystem::directory_iterator(dir_path) ; i != boost::filesystem::directory_iterator() ; i++)
		{
			if (!boost::filesystem::is_directory(i->path()))
			{
				 files.push_back(i->path().string());
				 ids.push_back(i->path().stem().string());
			}
			else
			    continue;
		}

/*
		DIR *dir;
		struct direct *ent;
		if ( (dir = opendir(dirPath.c_str())) != NULL )
		{
			while ((ent = readdir(dir)) != NULL) {
				files.push_back(ent->d_name);
			}
			closedir(dir);
		} else {
			perror("");
			exit(0);
		}
*/
		// send files in the list and remove them one by one
		// hanldeFiles();
		// send the file
		unsigned int curfilesize = files.size();
		long total_file_size= 0;
		for(unsigned int i=0; i<files.size(); i++)
		{
			total_file_size += boost::filesystem::file_size(boost::filesystem::path(files[i]));
			sendImageFileToSFTP(ids[i],files[i]);
		}
		//printf("total_file_size: %ld\n", total_file_size);

		// If sftp server has a problem, images are stock to the local storage. How could you handle it.
	     //remove all files if the storage is over some amount.
		// SendError("Error: The SFTP server does not work properly! ");
		if(curfilesize == files.size() && total_file_size >= 90000000)   // Over 90 MBytes
		{
			for(unsigned int i=0; i<files.size(); i++)
			{
				std::remove(files[i].c_str());
			}
		}

		files.clear();
		ids.clear();

	} while(!bFaceSend);

	return (NULL);

}

/*
void *stateReportThreadFunc(void *apdata)
{
	do {
		usleep(pSettings->getInt("IoT/stateReportRefreshInterval",10000000,false)); // state report interval. Default is 10 sec

		// TODO: do we have to lock this with mutex?
		IoTGateway.SendStateInfo();

	} while(!bStop);

	return (NULL);
}
*/

void IoT_ControlChannel_CallBack_func(std::string topic, std::string controlMsg) {

//#define DEBUG_IOT 1
	// -- Parse the message as you wish and take actions if needed

	// parse the message

	// track region message

	// no track region message

	// app stop message

	// -- Parse the message as you wish and take actions if needed. The message must be in a JSON format
	nlohmann::json controlMsgJson;

	try {
		controlMsgJson = nlohmann::json::parse(controlMsg.c_str());

		if(DEBUG_IOT)
		{
			printf("+++++++ [OmniSensr_DMG_App] Control Message Received +++++++++++++++++++++++\n");
			std::cout << std::setw(2) << controlMsgJson << '\n';
			printf("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
		}

//		if(DEBUG_IOT)
//		{
//			//printf("******* Control Channel Msg Received *******\n%s\n********************************************\n", controlMsg.c_str());
//
//			// special iterator member functions for objects
//			cout << "Control Msg rsved in JSON: " << "\n";
//			cout << setw(4) << controlMsgJson << "\n\n";
//
//			// special iterator member functions for objects
//			for (json::iterator it = controlMsgJson.begin(); it != controlMsgJson.end(); ++it) {
//				string key = it.key();
//				ostringstream value;
//				value << it.value();
//
//				cout << key << " : " << value.str() << "\n";
//			}
//		}

		/*
		 * Flatten the nested JSON into un-nested one.
		 * For example, The value of "WiFiCalib, { "Start": true } can be accessed after flatten() by "/WiFiCalib/Start"
		 */
//		json controlJ_flat = controlMsgJson.flatten();

//		if(DEBUG_IOT)
//		{
//			cout << "Control Msg rsved in JSON (flattened): " << "\n";
//			cout << setw(4) << controlJ_flat << "\n\n";
//		}

		// -- Try to access keys and get their values with a type
		// -- Note: Must catch the two exceptions {out_of_range, domain_error} whenever accessing a value.
		// -- 		Otherwise, it will exit the program if the type does not match or if the key does not exist.
		// -- ########### ADD USER-DEFINED ACTION BELOW #####################

			/*std::string filename,
			 * Check if WiFi Calib command is set. If so, it will initiate packet injection for WiFi calibration. For example,
			 * gStartWiFiCalib = controlJ_flat.at("/Process/Start");
			 * 	if(controlJ_flat.find("/Process/Start") != controlJ_flat.end()) {
					mStartWiFiCalib = controlJ_flat.at("/Process/Start");
				}
			 */

		for (nlohmann::json::iterator it = controlMsgJson.begin(); it != controlMsgJson.end(); ++it) {

			string key = it.key();
			if (key == "TrackPolygon")
			{
				/*
				// change track polygon settings
				std::string trackpolystr = it.value();
				std::vector<std::string> trackpolystrsplit;
				boost::split(trackpolystrsplit,trackpolystr,boost::is_any_of(", "));
				std::vector<double>	trackpoly;
				for (auto titr = trackpolystrsplit.begin();titr!=trackpolystrsplit.end();titr++)
				{
					double tpvert = atof(titr->c_str());
					trackpoly.push_back(tpvert);
				}

				pSettings->setDoubleVector(key,trackpoly);
				*/
				std::string nkey = "FaceTracker/" + key;
				pSettings->setString(nkey, it.value());
			}
			/*
			if (key == "TrackROI")
			{
				std::string nkey = "FaceTracker/" + key;
				pSettings->setString(nkey, it.value());
			}
			*/
			else if (key == "trackRegionVer")
			{
				int ver = it.value();

				// track polygon version is a part of state.  Update state
				devState_t *stateParam = IoTGateway.FindStateParam(key);
				pthread_mutex_lock(&gMutex);
				*(int*)stateParam->value = ver;
				stateParam->time_lastReported =0;
				pthread_mutex_unlock(&gMutex);
				pSettings->setInt(stateKeyMap[key]->path,ver);
			}
			else if (key == "KillApp")
			{
				if (it.value() == true)
				{
					// kill this app
					stopTracker();	// stop tracker
					// stop threads
					bThreadsStop = true;
					// stop main process
					bProcessStop = true;
				}
			}
			else if (key == "RestartApp")
			{
				if (it.value() == true)
				{
					// kill this app
					//stopTracker();	// stop tracker
					// stop threads
					bThreadsStop = true;
				}
			}

			else
			{
				ostringstream msgstr;
				msgstr << *it;
				LOGMESSAGE("WARNING: Unsupported control message " + msgstr.str());
				//bAction = false;
			}
			// currently other control message is not supported
		}

	} catch (std::invalid_argument& e) {		// -- Handle exception when the received msg does not comply with JSON standard
		// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
    	//std::cout << "invalid_argument: " << e.what() << '\n';
	} catch (std::out_of_range& e) {	// -- Handle exception when a non-existing filed is accessed.
		// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
		//std::cout << "out of range: " << e.what() << '\n';
	} catch (std::domain_error& e) {	// -- Handle exception when an incompatible conversion is attempted (e.g., float --> string).
		// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
	   // std::cout << "domain error: " << e.what() << '\n';
	}

	// store settings and restart app
	// delta state received. Save settings, reload settings, then restart tracker.
	pSettings->writeXml(settings_file_parts.fullpath().c_str(),false);	// save settings

	// restart has stopped. Let health app standby enough time to wait for the tracker app to restart
	pIOTc->sendHeartbeat(pSettings->getInt("restartHeartbeatInterval", 120, false));

	// restart tracker
	stopTracker();	// stop tracker
	startTracker();	// restart tracker

}

/*
 * Note that all the types of JsonPrimitiveType of AWS ThingShadow parameters are grouped into just 6 types: int, uint, float, double, bool, string.
 *
 * TODO: This is basically a JSON parser plus extra, so we can make this better by using a formal JSON parser.
 */

void IoT_DeltaState_CallBack_func(string entireDeltaMsg)
{
//#define DEBUG_IOT 1

	nlohmann::json entireDeltaMsgJson = nlohmann::json::parse(entireDeltaMsg);
	bool restart_tracker = false;

	// -- Check if there is at least a delta state destined to this app. If not, then exit this function
	if(entireDeltaMsgJson.find(mAppType) == entireDeltaMsgJson.end())
		return;

	nlohmann::json deltaMsgJson = nlohmann::json::parse(entireDeltaMsgJson.at(mAppType).dump());

	// Move mutex_lock and mutex_unlock to the outside of try-catch statement because of deadlock possibility
	pthread_mutex_lock(&gMutex);

	// -- If there is at least a delta state destined to this app, then start processing it by looking up the registered state params.
	try {

		if(DEBUG_IOT)
		{
			printf("+++++++ [OmniSensr_DMG_App] Delta State Received +++++++++++++++++++++++\n");
			std::cout << std::setw(2) << deltaMsgJson << '\n';
			printf("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
		}

		for (auto it = deltaMsgJson.begin(); it != deltaMsgJson.end(); ++it) {

			ostringstream actualValue;
			actualValue << it.value();

			devState_t *stateParam = IoTGateway.FindStateParam(it.key());

					// -- Check if there's matching state parameter
					// Avoid the trackRegionVer
					if(stateParam != NULL) {

						if(DEBUG_IOT) printf("[Delta Message] stateParam FOUND (key = %s)\n", stateParam->key.c_str());
						// avoid updating the parameter for trackRegionVer

						// -- Do something here to react to the received delta message to this particular state parameter.
						switch(stateParam->type) {
							case SHADOW_JSON_INT32:
							case SHADOW_JSON_INT16:
							case SHADOW_JSON_INT8: {
								if(stateKeyMap[stateParam->key]->response_to_delta)
								{
									int stateParam_desired;
									sscanf(actualValue.str().c_str(), "%d", &stateParam_desired);

									if(DEBUG_IOT)
										printf("(report, desired) = (%d, %d) -- > ", *(int*)stateParam->value, stateParam_desired);

									// -- Replace the state parameter with the desired value
									*(int*)stateParam->value = stateParam_desired;

									// -- Reset this so that this state can be reported again immediately.
									stateParam->time_lastReported = 0;

									// reset settings
									pSettings->setInt(stateKeyMap[stateParam->key]->path,stateParam_desired);

									if(DEBUG_IOT)
										printf("(%d)\n", *(int*)stateParam->value);

									restart_tracker = true;
								}
							}
								break;
							case SHADOW_JSON_UINT32:
							case SHADOW_JSON_UINT16:
							case SHADOW_JSON_UINT8: {
								if(stateKeyMap[stateParam->key]->response_to_delta)
								{
									unsigned int stateParam_desired;
									sscanf(actualValue.str().c_str(), "%u", &stateParam_desired);

									if(DEBUG_IOT)
										printf("(report, desired) = (%u, %u) --> ", *(unsigned int*)stateParam->value, stateParam_desired);

									// -- Replace the state parameter with the desired value
									*(unsigned int*)stateParam->value = stateParam_desired;

									// -- Reset this so that this state can be reported again immediately.
									stateParam->time_lastReported = 0;

									// reset settings. NOTE: vml::Settings doesn't support unsigned int. Convert to Int
									pSettings->setInt(stateKeyMap[stateParam->key]->path,stateParam_desired);

									if(DEBUG_IOT)
										printf("(%u)\n", *(unsigned int*)stateParam->value);

									restart_tracker = true;
								}
							}
								break;
							case SHADOW_JSON_FLOAT: {
								if(stateKeyMap[stateParam->key]->response_to_delta)
								{
									float stateParam_desired;
									sscanf(actualValue.str().c_str(), "%f", &stateParam_desired);

									if(DEBUG_IOT) {
										printf("[%s] ", actualValue.str().c_str());
										cout << fixed << setprecision(IOT_FLOAT_PRECISION);
										cout << " (report, desired) = ("<< *(float*)stateParam->value << ", " << stateParam_desired << ") --> ";
									}

									// -- Replace the state parameter with the desired value
									*(float*)stateParam->value = stateParam_desired;

									// -- Reset this so that this state can be reported again immediately.
									stateParam->time_lastReported = 0;

									// reset settings
									pSettings->setFloat(stateKeyMap[stateParam->key]->path,stateParam_desired);
									//pSettings->setFloat(it.key(),stateParam_desired);

									if(DEBUG_IOT) {
										cout << fixed << setprecision(IOT_FLOAT_PRECISION);
										cout << "("<< *(float*)stateParam->value << ")" << endl;
									}

									restart_tracker = true;
								}
							}
								break;
							case SHADOW_JSON_DOUBLE: {
								if(stateKeyMap[stateParam->key]->response_to_delta)
								{
									double stateParam_desired;
									sscanf(actualValue.str().c_str(), "%lf", &stateParam_desired);

									if(DEBUG_IOT) {
										printf("[%s] ", actualValue.str().c_str());
										cout << fixed << setprecision(IOT_DOUBLE_PRECISION);
										cout << " (report, desired) = ("<< *(double*)stateParam->value << ", " << stateParam_desired << ") --> ";
									}

									// -- Replace the state parameter with the desired value
									*(double*)stateParam->value = stateParam_desired;

									// -- Reset this so that this state can be reported again immediately.
									stateParam->time_lastReported = 0;

									// reset settings
									pSettings->setDouble(stateKeyMap[stateParam->key]->path,stateParam_desired);

									if(DEBUG_IOT) {
										cout << fixed << setprecision(IOT_DOUBLE_PRECISION);
										cout << "("<< *(double*)stateParam->value << ")" << endl;
									}

									restart_tracker = true;
								}
							}
								break;
							case SHADOW_JSON_BOOL: {
								if(stateKeyMap[stateParam->key]->response_to_delta)
								{
									char stateParam_desired[99] = {'\0'};
									sscanf(actualValue.str().c_str(), "%s", stateParam_desired);

									if(DEBUG_IOT)
										printf("(report, desired) = (%d, %s) --> ", *(bool*)stateParam->value, stateParam_desired);

									// -- Replace the state parameter with the desired value
									if(stateParam_desired[0] == 't')	// -- true
									{
										*(bool*)stateParam->value = true;
										// reset settings
										pSettings->setBool(stateKeyMap[stateParam->key]->path,true);
									}
									if(stateParam_desired[0] == 'f')	// -- false
									{
										*(bool*)stateParam->value = false;
										// reset settings
										pSettings->setBool(stateKeyMap[stateParam->key]->path,false);
									}

									// -- Reset this so that this state can be reported again immediately.
									stateParam->time_lastReported = 0;

									if(DEBUG_IOT)
										printf("(%d)\n", *(bool*)stateParam->value);

									restart_tracker = true;
								}

							}
								break;

							case SHADOW_JSON_STRING: {
								if(stateKeyMap[stateParam->key]->response_to_delta)
								{
									// -- String-type value in JSON is wrapped with double-quotation marks, so get rid of them.
									std::string strExtracted = actualValue.str().substr(1,actualValue.str().size()-2);

									if(DEBUG_IOT)
										printf("(report, desired) = (%s, %s) -->", (*(std::string*)stateParam->value).c_str(), strExtracted.c_str());
										//printf("(report, desired) = (%s, %s) -->", (*(string*)stateParam->value).c_str(), actualValue.str().c_str());

									// -- Replace the state parameter with the desired value
									*(std::string*)stateParam->value = strExtracted;

									// -- Reset this so that this state can be reported again immediately.
									stateParam->time_lastReported = 0;

									// reset settings
									pSettings->setString(stateKeyMap[stateParam->key]->path,strExtracted);

									if(DEBUG_IOT)
										printf("(%s)\n", (*(string*)stateParam->value).c_str());

									restart_tracker = true;
								}

							}
								break;

							default:
								break;
						}
					}
		}

		if(restart_tracker)
		{
			// delta state received and updated. Save settings, reload settings, then restart tracker.
			pSettings->writeXml(settings_file_parts.fullpath().c_str(),false);	// save settings

			// restart has stopped. Let health app standby enough time to wait for the tracker app to restart
			pIOTc->sendHeartbeat(pSettings->getInt("restartHeartbeatInterval", 120, false));

			// restart tracker
			stopTracker();	// stop tracker
			startTracker();	// restart tracker
		}
		// -- Put into a message buffer for transmission
		//IoTGateway.SendStateInfo();

	} catch (std::invalid_argument& e) {		// -- Handle exception when the received msg does not comply with JSON standard
		// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
    	//std::cout << "invalid_argument: " << e.what() << '\n';
    } catch (std::out_of_range& e) {	// -- Handle exception when a non-existing filed is accessed.
		// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
		//std::cout << "out of range: " << e.what() << '\n';
	} catch (std::domain_error& e) {	// -- Handle exception when an incompatible conversion is attempted (e.g., float --> string).
		// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
	   // std::cout << "domain error: " << e.what() << '\n';
	}

	pthread_mutex_unlock(&gMutex);
}


void stateInitialization()
{
	double	stateReportInterval = pSettings->getDouble("IoT/stateReportRefreshInterval",900.f,false);	// default is 15 min.
	for (int i=0;i<N_STATEKEYMAP;i++)
	{
		// add state key and settings path mapping
		stateKeyMap[stateMapping[i].key] = &(stateMapping[i]);
		IoTGateway.AddStateParam(stateMapping[i].type,stateMapping[i].key,stateMapping[i].pVar,NULL,stateReportInterval);

	}

}

void updateStateFromSettings()
{
	for (int i=0;i<N_STATEKEYMAP;i++)
	{
		switch (stateMapping[i].type)
		{
			case SHADOW_JSON_INT32:
			case SHADOW_JSON_INT16:
			case SHADOW_JSON_INT8:
			{
				*((int*)stateMapping[i].pVar) = pSettings->getInt(stateMapping[i].path);
				break;
			}
			case SHADOW_JSON_UINT32:
			case SHADOW_JSON_UINT16:
			case SHADOW_JSON_UINT8:
			{
				*((unsigned int*)stateMapping[i].pVar) = (unsigned int)pSettings->getInt(stateMapping[i].path);
				break;
			}
			case SHADOW_JSON_FLOAT:
			{
				*((float*)stateMapping[i].pVar) = pSettings->getFloat(stateMapping[i].path);
				break;
			}
			case SHADOW_JSON_DOUBLE:
			{
				*((double*)stateMapping[i].pVar) = pSettings->getDouble(stateMapping[i].path);
				break;
			}
			case SHADOW_JSON_BOOL:
			{
				*((bool*)stateMapping[i].pVar) = pSettings->getBool(stateMapping[i].path);
				break;
			}
			case SHADOW_JSON_STRING:
			{
				*((string*)stateMapping[i].pVar) = pSettings->getString(stateMapping[i].path);
				break;
			}
			default:
				break;
		} // of switch
	} // of for
}


void startTracker()
{
	if (pTracker && pTracker->isRunning())
	{
		LOGMESSAGE("Error: other tracker is running! ")
		// give warning and just return;
		return;
	}

	pthread_mutex_lock(&appMutex);

	// settings instance
	pSettings.reset(new SETTINGS);
	// parse settings file
	LOGMESSAGE("Reading settings at "+settings_path)
	pSettings->parseXml(settings_file_parts.fullpath(),false);

	// settings has been parsed. Update state
	updateStateFromSettings();

	// video module instance
	pVideoModule.reset(new vml::FaceVideoModule());
	// initialize video module
	LOGMESSAGE("Initializing video")
	pVideoModule->initialize(pSettings,input_source_addr);

	pIOTc.reset(new IOT_Communicator(APP_ID_DEMO));

	pTracker.reset(new vml::MultiTargetFaceTracker());

	pTracker->setSettings(pSettings);
	pTracker->setVideoModule(pVideoModule);
	pTracker->setIOTCommunicator(pIOTc);

	// initialize tracker modules
	LOGMESSAGE("Initializing Tracker...")
	pTracker->initializeModules();

	// checkout current Image resolution
	if( pVideoModule->getCols() != pSettings->getInt("FaceTracker/inputImageWidth")
			|| pVideoModule->getRows() != pSettings->getInt("FaceTracker/inputImageHeight") )
	{
		pIOTc->sendError("Current image resolution is not correct w.r.t. FaceTracker/inputImageWidth and FaceTracker/inputImageHeight!",false,false);
	}

	LOGMESSAGE("Starting Tracker...")

	// initalize and start
	if (nProcessingThreads == 1)
	{
		pTracker->singleThreadStart();
	}
	else if(nProcessingThreads == 2) {
		pTracker->_2ThreadPipeliningStart();
	}
	else {
		pTracker->_3ThreadPipeliningStart();
	}

	LOGMESSAGE("Tracker started!")

	bThreadsStop = false;

	pthread_mutex_unlock(&appMutex);

	// start data report thread
	if(pthread_create(&dataReportThread, &threadAttr, dataReportThreadFunc, (void *)pTracker.get()))
	{
		fprintf(stderr, "Error in creating dataReportThread\n");
		exit(0);
	}


}

void stopTracker()
{
	pthread_mutex_lock(&appMutex);
	if (!(pTracker || pTracker->isRunning()))
	{
		// give warning and return;
		return;
	}

	bThreadsStop = true;	// stop each threads

	// stop data report thread
	pthread_join(dataReportThread,NULL);


	LOGMESSAGE("Stopping Tracker...")
	//pTracker->stopThreads();
	pTracker->stopThreads(nProcessingThreads);

	LOGMESSAGE("Tracker stopped!")
	pTracker.reset();
	// release settings
	pSettings.reset();
	// release video
	pVideoModule.reset();
	LOGMESSAGE("All modules reset.")
	pthread_mutex_unlock(&appMutex);

}


void initializeIoTGateway()
{
	ostringstream appIdStr;
	appIdStr.str("");
	appIdStr.clear();
	appIdStr << demoApp;

	mAppType = appIdStr.str();

	// initialize IoT
	std::string mqttTopicState, mqttTopicData;
	mqttTopicState.append(AWS_IOT_APP_STATE_CHANNEL_PREAMBLE);
	mqttTopicState.append("/");
	mqttTopicState.append(APP_ID_DEMO);

	// -- AWS_IoT_Gateway will publish data to a topic 'data/<app_id>/<store_name_prefix>/<store_name_suffix>/<device_name>' (e.g., data/wifiRss/vmhq/16801/cam001)
	mqttTopicData.append(AWS_IOT_APP_DATA_CHANNEL_PREAMBLE);
	mqttTopicData.append("/");
	mqttTopicData.append(APP_ID_DEMO);
	mqttTopicData.append("/");
	mqttTopicData.append(IoTGateway.GetMyStoreName());
	mqttTopicData.append("/");
	mqttTopicData.append(IoTGateway.GetMyDevName());

	LOGMESSAGE("Setup mqttTopic");

	// register delta state callback function
	IoTGateway.RegisterDeltaCallbackFunc(&IoT_DeltaState_CallBack_func);

	// register control channel callback function
	IoTGateway.RegisterControlCallbackFunc(&IoT_ControlChannel_CallBack_func);

	LOGMESSAGE("Registered IoTGateWay functions.");

	// start IoT threads
	IoTGateway.Run(APP_ID_DEMO, mqttTopicState, mqttTopicData, IOT_GATEWAY_CONFIG_FILE);

	LOGMESSAGE("Run IoTGateway.");
}

//----------------< End of IoT-related codes >--------------------//

void display_usage( const char *argv0 )
{

	vml::FileParts	cmd_name(argv0);
	printf("\n");

	printf("Usage:  %s [options] [input_source_add]\n", cmd_name.filename().c_str());
	printf("  Options:\n");
	printf("\t [-h | --help]            print this message\n");
	printf("\t [-v | --verbose]         verbose (default off)\n");
	printf("\t [-s | --settings]        settings path\n");
	printf("\t [-m | --multithread]     use multithreading pipeline\n");
	printf("\t [-d | --display]         display tracker output window (NOTE: Do not use this running in OmniSensor)\n");
	printf("\n\t If input source address is not provided, use default local host video streaming\n");

	printf("  Examples:\n");
	printf("\t %s http://localhost:8080/?action=stream?video.mjpeg\n",cmd_name.filename().c_str());
	printf("\t %s -v direct\n",cmd_name.filename().c_str());
	printf("\n");

	exit(1);

}


int main(int argc, char* argv[])
{
	std::string	settingsPath = "";

	static struct option longopts[] = {
			{ "help", 		no_argument,		NULL, 		'h'},
			{ "verbose",	no_argument,		NULL, 		'v'},
			{ "settings",	required_argument,	NULL, 		's'},
			{ "multithreads",	required_argument,	NULL, 		'm'},
			{ "display",    no_argument,        NULL,       'd'},
			{ "calfps", 	required_argument,		NULL,		'f'},
			{ NULL,			0,					NULL,		0}
	};

	int c;
	while ((c= getopt_long(argc, argv, "hvs:m:df:", longopts, NULL)) != -1)
	{
		switch (c)
		{
			case 'v':
			{
				bVerbose  = true;	
				break;
			}
			case 'h':
			{
				display_usage(argv[0]);
				break;
			}
			case 's':
			{
				settings_path = optarg;
				break;
			}
			case 'm':
			{
				nProcessingThreads = atoi(optarg);
				if (nProcessingThreads > 2) nProcessingThreads = 3;
				else if (nProcessingThreads == 2) nProcessingThreads = 2;
				else nProcessingThreads = 1;
				break;
			}
			case 'd':
			{
				bDisplay = true;
				break;
			}
			case 'f':
			{
				bCalFPS = true;
				nTimeLeng = atoi(optarg);
				break;
			}
			default:
			{
				std::cerr << "Unrecognized command line option -" << c << ". Aborting" << std::endl;
				display_usage(argv[0]);
				return (-1);
			}
		}	

	}
	
	if (optind == argc)
	{
		if(bTestRun)
		{
			input_source_addr = TEST_VIDEO_FILE;
		}
		else
		{
			input_source_addr = INPUT_VIDEO_URL;
		}
	}
	else 
	{
		input_source_addr = argv[optind];
	}

	// start logger
	STARTLOGGING;

	LOGMESSAGE("Processing video streaming at "+input_source_addr);

	// initialize settings path
	settings_file_parts.setpath(settings_path);
	settings_file_parts.setfilename("AutoDmgRecog.xml");

	initializeIoTGateway();

	// pre-parse settings instance before state initialization.
	pSettings.reset(new SETTINGS);
	// parse settings file
	LOGMESSAGE("Reading settings at "+settings_path)
	pSettings->parseXml(settings_file_parts.fullpath(),false);

	stateInitialization();

	// For heartbeat
	//IOT_Communicator IoT_Comm(APP_ID_DEMO);

	// -- ######################################################################
	pthread_attr_init(&threadAttr);
	pthread_attr_setdetachstate(&threadAttr, PTHREAD_CREATE_JOINABLE);


	// start face sending thread
	if(pthread_create(&faceSendThread, &threadAttr, faceSendThreadFunc, (void *)pTracker.get()))
	{	fprintf(stderr, "Error in creating faceSendThread\n");
		exit(0);

	}
/*
	// start state report thread
	if(pthread_create(&stateReportThread, &threadAttr, stateReportThreadFunc, (void *)pTracker.get()))
	{
		fprintf(stderr, "Error in creating stateReportThread\n");
		exit(0);
	}

*/
	// Single-thread processing
	// initiate processing loop
	if(bDisplay)
	{

		// video module instance
		pVideoModule.reset(new vml::FaceVideoModule());
		// initialize video module
		LOGMESSAGE("Initializing video")
		pVideoModule->initialize(pSettings,input_source_addr);

		pTracker.reset(new vml::MultiTargetFaceTracker());
		pTracker->setSettings(pSettings);
		pTracker->setVideoModule(pVideoModule);

		// initialize tracker modules
		pTracker->initializeModules();
		LOGMESSAGE("Initialized a tracker module");

		// start data report thread
		bThreadsStop = false;
		if(pthread_create(&dataReportThread, &threadAttr, dataReportThreadFunc, (void *)pTracker.get()))
		{
			fprintf(stderr, "Error in creating dataReportThread\n");
			exit(0);
		}

		cv::Mat TrackerDisplayImage;
		if(bDisplay)
		{
			TrackerDisplayImage.create(pVideoModule->getRows(),pVideoModule->getCols(),CV_8UC3);
			// create named window
			cv::namedWindow("Tracker Display",CV_WINDOW_AUTOSIZE);
		}

		gStartTime();

		while (!bProcessStop)
		{
			if (!pTracker->process())
			{
				break;
			}

			if (bDisplay)
			{
				pTracker->getGrabbedImage().copyTo(TrackerDisplayImage);
				pTracker->drawTrackingRegions(TrackerDisplayImage);
				pTracker->drawActiveTargetsWithDMG(TrackerDisplayImage);
				pTracker->drawActiveTrajectories(TrackerDisplayImage);
				pTracker->drawValidNewTargets(TrackerDisplayImage);

				cv::cvtColor(TrackerDisplayImage,TrackerDisplayImage,CV_BGR2RGB);
				cv::imshow("Tracker Display",TrackerDisplayImage);
				cv::waitKey(10);
			}

			if (bVerbose)
			{
				double elapsed_time = gTimeSinceStart();

				double fps = (double)pTracker->getCurrentFrameNumber()/elapsed_time;
				IoTState_StateReportRefreshInterval = pSettings->getInt("IoT/stateReportRefreshInterval");
	
				std::stringstream pmsg;
				pmsg << "Processed with " << std::fixed << std::setprecision(2) << fps << "fps";
				logger.outputMessage(pmsg.str());
			}
		}
	}
	else
	{
		startTracker();

		while(!bProcessStop)
		{
			long cFN;
			if(bCalFPS) {
				cFN = pTracker->getCurrentFrameNumber();
			}

			sleep(nTimeLeng); 	// report frame rate every 5 sec

			pthread_mutex_lock(&appMutex);
			if (bVerbose && pTracker && pVideoModule)
			{
				float fps;
				if(bCalFPS) {
					fps =  (float)(pTracker->getCurrentFrameNumber()-cFN)/ (float)nTimeLeng;
				}
				else {
					fps =  pVideoModule->getFrameRate();
				}

				std::stringstream pmsg;
				pmsg << "Processed with " << std::fixed << std::setprecision(2) << fps << "fps, (" << pTracker->getCurrentFrameNumber() << " frames, " << "active: " << pTracker->getNumActiveTargets() << ")";
				logger.outputMessage(pmsg.str());
			}
			pthread_mutex_unlock(&appMutex);

		}
	}


	return 0;
}


# add release notes in the following format

2017-04-11 -	Face App ver 0.97
				- This version is used for the Omnisensr OS release ver. 1.79 after updating codes as follows: 
				- Asked to set the period of heartbeats with 120 secoonds. 
				- Fixed a bug in the case that input iamge resolution is different from 1280x960. 
				- Updated codes for sending a facial size as a face information
				- Updated codes for the image timestamp which is same to the startTimeTrack all the times. 
				- Update codes for doing ethnicity recognition when a tracker is terminated. 
                  In the configuration file of AutoDmgRecog.xml, set the doDMGRecongnition(0), 
                  doDMGRecognitionEnd(1), doAge(0), doGender(0), and doEthnicity(1).

2017-03-29 -    Face App ver 0.96  
				- This version is used for the Omnisensr OS release ver. 1.78 after fixing the following issue. 
                - Issue: heartbeat messages are missing periodically where there are 
				  2 IoT_Communicator sharing the same app name.
				- Found the issue by monitoring the published message in the App 
				  through IoT_Communicator::sendHeartbeat and their subscribed message 
 			 	  in the OmniSensr by mosquitto_sub.
				- Resolved by using a single IoT_communicator for the demoApp. 

2017-02-28 - 	Face App ver 0.95 
				- Included a heartbeat function
				- Removed warnings messages during a compiling time
				- Demographics module is optional by setting up doin configuration file. 

2017-01-31 -   	Face App ver 0.94 
				- On the process of a field test while running in a Giant testbed
				- Fixed a bug for the image type in the SFTP server (from hardcorded jpg to the 	 						
				  original image format (png))
				- Changed the facePosX and facePosY as the normalized value with respect to
				  the input size of an image. 
				- Added a criteria (90MB) for removing files in ramdisk when all storing files are 
				  over 90MB due to an accessing error on the sftp server. 

2017-01-31 -   	Face App ver 0.93
				- Changed the float-type state variables to the double type for resolving 
				  the issue that we have lost the floating point precision when float values 
				  are transfered to the JSON. 

2017-01-12 - 	Face App ver 0.92 
				- Changed the directory path for a ramdisk as /home/omniadmin/rd/ 
				  along with the Gary's script. 
				- Changed the password of SFTP server.
				- Changed the password of SFTP server again and the folder name as images. (n2)

2017-01-10 - 	Face App ver 0.91
				- Added the facePosX and facePosY to the meta data. 
				- Tested the setup of ROI through IoT control channel.
				- In order to use the TAP and the live project for the ROI setup, 
				  updated the trackRegionVer as an integer to be compatable to the live project. 


2016-12-23 - 	Face App vers 0.9 (alpha version) 
				- IN: Given input video stream, 
				- OUT: a face metadata and face image(s) for each detected and tracked shopper  
				 		- 	metadata are sent to AWS RDS via MQTT brocker 
						-	face images are sent to SFTP server in HQ via REST API (temporally) 	
				


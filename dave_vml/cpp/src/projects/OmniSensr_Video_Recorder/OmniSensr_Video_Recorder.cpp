/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/****************************************************************************/

// OmniSensr_Video_Recorder.cpp
//
//   command line video recording tool for OmniSensr
//
//   Dave Donghun Kim
//   2016.10.18

#include <cstdio>
#include <getopt.h>
#include <core/Error.hpp>
#include <modules/utility/Util.hpp>

//#include <raspicam/raspicam_cv.h>

#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

// for verbose output
vml::Logger	logger;

// logger message macros
#define STARTLOGGING			logger.startLogging()
#define LOGMESSAGE(__MSG__)		if(bVerbose){logger.outputMessage(__MSG__);}

#define VIDEO_INPUT_URL 	"http://localhost:8080/?action=stream?video.mjpeg"
#define VIDEO_OUTPUT_PATH 	"./test.avi"

std::string	input_source_addr = VIDEO_INPUT_URL;
std::string	output_path = VIDEO_OUTPUT_PATH;

template <typename T> std::string tostr(const T& t) {
   std::ostringstream os;
   os<<t;
   return os.str();
}

bool	bVerbose = false;

void display_usage( const char *argv0 )
{

	vml::FileParts	cmd_name(argv0);
	printf("\n");

	printf("Usage:  %s [options] \n", cmd_name.filename().c_str());
	printf("  Options:\n");
	printf("\t [-h | --help]            print this message\n");
	printf("\t [-v | --verbose]         verbose (default off)\n");
	printf("\t [-i | --input]         	input source address (default: local host streaming)\n");
	printf("\t [-o | --output]          storage path (default: ./test.avi)\n");
	printf("\t [-x | --xs]        		image width (default: 1280)\n");
	printf("\t [-y | --ys]        		image height (default: 960)\n");
	printf("\t [-f | --fps]        		fps (default: 10)\n");
	printf("\t [-r | --rectime]         recording time (s) (default: 60 s)\n");
	printf("\t [-c | --codec]           codec (a fourcc code such as X264, XVID, MJPG, DIVX) (default: MJPG)\n");

	printf("  Examples:\n");
	printf("\t %s -v -i http://localhost:8080/?action=stream?video.mjpeg -o ./test.avi -x 1280 -y 960 -f 10 -r 60 -c DIVX\n",cmd_name.filename().c_str());
	printf("\n");

	exit(1);

}


int main(int argc, char* argv[])
{

	static struct option longopts[] = {
			{ "help", 		no_argument,		NULL, 		'h'},
			{ "verbose",	no_argument,		NULL, 		'v'},
			{ "input",		required_argument,	NULL, 		'i'},
			{ "output",		required_argument,	NULL, 		'o'},
			{ "iwidth",		required_argument,	NULL, 		'x'},
			{ "iheight",	required_argument,	NULL, 		'y'},
			{ "fps",		required_argument,	NULL, 		'f'},
			{ "rectime",	required_argument,	NULL, 		'r'},
			{ "codec",	required_argument,	NULL, 		'c'},
			{ NULL,			0,					NULL,		0}
	};

	double iwidth = 1280;
	double iheight = 960;
	float  fps =10;
	float  rectime=60;

	std::string swidth, sheight, sfps, srectime;
	std::string codec_type = "MJPG";

	int c;
	while ((c= getopt_long(argc, argv, "hvi:o:x:y:f:r:c:", longopts, NULL)) != -1)
	{
		switch (c)
		{
			case 'v':
			{
				bVerbose  = true;	
				break;
			}
			case 'h':
			{
				display_usage(argv[0]);
				break;
			}
			case 'i':
			{
				input_source_addr = optarg;
				LOGMESSAGE("Processing video streaming at "+input_source_addr);
				break;
			}
			case 'o':
			{
				output_path = optarg;
				break;
			}
			case 'x':
			{
				iwidth = atof(optarg);
				break;
			}
			case 'y':
			{
				iheight = atof(optarg);
				break;
			}
			case 'f':
			{
				fps = atof(optarg);
				break;
			}
			case 'r':
			{
				rectime = atof(optarg);
				break;
			}
			case 'c':
			{
				codec_type = optarg;
				break;
			}
			default:
			{
				std::cerr << "Unrecognized command line option -" << c << ". Aborting" << std::endl;
				display_usage(argv[0]);
				return (-1);
			}
		}	

	}

	int ex = cv::VideoWriter::fourcc(codec_type.c_str()[0],codec_type.c_str()[1],codec_type.c_str()[2],codec_type.c_str()[3]);

	// start logger
	STARTLOGGING;

	LOGMESSAGE("Processing video streaming at "+input_source_addr);
	LOGMESSAGE("Processing video file to "+ output_path);
	LOGMESSAGE("Processing video codec: "+ codec_type);
	LOGMESSAGE("Processing video width: "+ tostr(iwidth));
	LOGMESSAGE("Processing video height: "+ tostr(iheight));
	LOGMESSAGE("Processing video fps: "+ tostr(fps));
	LOGMESSAGE("Processing video recording time (s): "+ tostr(rectime));

	cv::VideoCapture cap;
	//raspicam::RaspiCam_Cv rcap;

	if(input_source_addr.c_str()[0] == '0')
	{
		cap.open(0);
/*
		rcap.set(CV_CAP_PROP_FRAME_WIDTH, iwidth);
		rcap.set(CV_CAP_PROP_FRAME_HEIGHT, iwidth);
		rcap.set(CV_CAP_PROP_FPS, fps);
		rcap.set(CV_CAP_PROP_FORMAT, CV_8UC3);
		if(!rcap.open())
		{
			LOGMESSAGE("Camera is not opened.");

		}
		else
		{
			LOGMESSAGE("Camera is opened.");
		}
*/
	}
	else
	{
		cap.open(input_source_addr.c_str());

	    if(!cap.isOpened()) {
	        printf("Not opened");
	        //return -1;
	    }
		else
		{
			LOGMESSAGE("Camera is opened.");
		}

	}
/*
	if(!cap.set(cv::CAP_PROP_FPS, fps)) {
		printf("Wrong fps.\n");
	}
	if(!cap.set(cv::CAP_PROP_FRAME_WIDTH, iwidth)) {
		printf("Wrong image width.\n");
	}
	if(!cap.set(cv::CAP_PROP_FRAME_HEIGHT, iheight)) {
		printf("Wrong image height.\n");
	}
	if(!cap.set(cv::CAP_PROP_FOURCC, (double)ex)) {
		printf("Wrong codec type.\n");
	}
*/

    cv::VideoWriter outputVideo(output_path, ex, fps, cv::Size(iwidth, iheight), true);

    //std::time_t start= std::time(0);
    int frame_count = 1;

    if(!bVerbose)
       	printf("/nStart the recording.../n");

	LOGMESSAGE("Start the recording...");

    while (1) {
        cv::Mat frame;
        cap >> frame;
        outputVideo << frame;
        if (frame_count > fps * rectime)  //(std::difftime(std::time(0), start) >= rectimevalue)
        {

        	LOGMESSAGE("Finished the recording!");
        	if(!bVerbose)
        		printf("/nFinished the recording!/n");

             break;
        }

        frame_count++;
    }

    outputVideo.release();
    cap.release();

	LOGMESSAGE("Released.");

	return 0;
}


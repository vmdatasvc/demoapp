/*
 * qaServo.cpp
 *
 *  Created on: Apr 12, 2017
 *      Author: ctseng
 */
#include <iostream>
#include <cstdlib>
#include <pigpiod_if2.h>

int main(int argc, char* argv[]) {
	int pi = pigpio_start(NULL, NULL);
	if (pi < 0) {
		std::cout << "Failed to connect to pigpiod" << std::endl;
		return -1;
	}
	set_servo_pulsewidth(pi, atoi(argv[1]), atoi(argv[2]));
	pigpio_stop(pi);
}




/*
 * alignAccel.cpp
 *
 *  Created on: Apr 12, 2017
 *      Author: ctseng
 */
#include <iostream>
#include <pigpiod_if2.h>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <unistd.h>
#include <sys/time.h>
#include <string>
#include <sstream>
#include <fstream>
#include "json.hpp"

#define THRESHOLD	0.05
#define PRESET_PATH	"/home/omniadmin/omnisensr-apps/config/OmniSensr_Servo_Controller/presets.json"
#define TIMEOUT		60
#define PULSE_MIN	800
#define PULSE_MAX	1800
#define SERVO1_PIN	12
#define SERVO2_PIN	13
#define SERVO3_PIN	20
#define PI 3.14159265

using json = nlohmann::json;

// get current time in seconds
double gCurrentTime()
{
   timeval tv;
   while(gettimeofday(&tv, NULL) != 0) {
	   usleep(10);
   }
   return ((double)(tv.tv_sec) + (double)(tv.tv_usec) / 1000000.0);
}
// initialize accelerometer on bus
unsigned int initAccel(int pi, int bus) {
	unsigned int handle = i2c_open(pi, bus, 0x1d, 0);
	i2c_write_byte_data(pi, handle, 0x2a, 0x01);
	return handle;
}
// get readings from accelerometer, average of 10 readings and normalized to unit length
void readAccel(int pi, unsigned int handle, float* reading) {
	int dig1, dig2, val[3];
	float len;
	int repeat = 10;
	reading[0] = 0;
	reading[1] = 0;
	reading[2] = 0;
	if (i2c_read_byte_data(pi, handle, 0x2a) != 0x01)
		i2c_write_byte_data(pi, handle, 0x2a, 0x01);
	for (int k = 0; k < repeat; k++) {
		len = 0;
		for (int i = 0; i < 3; i++) {
			dig1 = i2c_read_byte_data(pi, handle, 2 * i + 1) << 4;
			dig2 = i2c_read_byte_data(pi, handle, 2 * i + 2) >> 4;
			val[i] = dig1 | dig2;
			val[i] = (val[i] & 2047) - (val[i] & 2048);
			len += val[i] * val[i];
		}
		len = sqrt(len);
		for (int i = 0; i < 3; i++) {
			reading[i] += val[i] / len;
		}
	}
	reading[0] /= repeat;
	reading[1] /= repeat;
	reading[2] /= repeat;
}
// close i2c bus
void closeAccel(int pi, unsigned int handle) {
	i2c_write_byte_data(pi, handle, 0x2a, 0x00);
	i2c_close(pi, handle);
}

unsigned int checkRange(int width) {
	if (width == 0)
		return 0;
	if (width < PULSE_MIN)
		return PULSE_MIN;
	else if (width > PULSE_MAX)
		return PULSE_MAX;
	else
		return width;
}
// use dot product to calculate degree between input vectors
double getDegree(float* v1, float* v2) {
	double degree = v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
	degree = std::acos(degree) / PI * 180;
	return degree;
}
// move degrees in axis direction
void moveDeg(std::string axis, int degree, int pi, unsigned int handle) {
	float initalV[3], V[3];
	usleep(500000);
	readAccel(pi, handle, initalV);
	int w1, w3;
	double diff;
	w1 = get_servo_pulsewidth(pi, SERVO1_PIN);
	w3 = get_servo_pulsewidth(pi, SERVO3_PIN);
	while (1) {
		readAccel(pi, handle, V);
		diff = getDegree(initalV, V);
		if (diff >= std::abs(degree) - 0.5 && diff <= std::abs(degree) + 0.5) {
			//std::cout << diff << std::endl;
			return;
		}
		else {
			if (axis.compare("x") == 0) {
				if (w1 <= PULSE_MIN || w1 >= PULSE_MAX) {
					std::cout << "Reached limit: " << (degree > 0 ? 1 : -1) * diff << std::endl;
					return;
				}
				if (degree > 0) {
					set_servo_pulsewidth(pi, SERVO1_PIN, ++w1);
				}
				else {
					set_servo_pulsewidth(pi, SERVO1_PIN, --w1);
				}
			}
			else if (axis.compare("y") == 0) {
				if (w3 <= PULSE_MIN || w3 >= PULSE_MAX) {
					std::cout << "Reached limit: " << (degree > 0 ? 1 : -1) * diff << std::endl;
					return;
				}
				if (degree > 0) {
					set_servo_pulsewidth(pi, SERVO3_PIN, ++w3);
				}
				else {
					set_servo_pulsewidth(pi, SERVO3_PIN, --w3);
				}
			}
		}
		usleep(5000);
	}

}
// move camera lens to the limits and record servo settings
void calibrate(int pi, unsigned int handle, int x_min, int x_max, int y_min, int y_max) {
	int w1, w3;
	w1 = get_servo_pulsewidth(pi, SERVO1_PIN);
	w3 = get_servo_pulsewidth(pi, SERVO3_PIN);
	moveDeg("x", x_min, pi, handle);
	std::cout << "x " << x_min << ": " << get_servo_pulsewidth(pi, SERVO1_PIN) << std::endl;
	set_servo_pulsewidth(pi, SERVO1_PIN, w1);
	moveDeg("x", x_max, pi, handle);
	std::cout << "x " << x_max << ": " << get_servo_pulsewidth(pi, SERVO1_PIN) << std::endl;
	set_servo_pulsewidth(pi, SERVO1_PIN, w1);
	moveDeg("y", y_min, pi, handle);
	std::cout << "y " << y_min << ": " << get_servo_pulsewidth(pi, SERVO3_PIN) << std::endl;
	set_servo_pulsewidth(pi, SERVO3_PIN, w3);
	moveDeg("y", y_max, pi, handle);
	std::cout << "y " << y_max << ": " << get_servo_pulsewidth(pi, SERVO3_PIN) << std::endl;
	set_servo_pulsewidth(pi, SERVO3_PIN, w3);
}

int main(int argc, char* argv[]) {
	int pi = pigpio_start(NULL, NULL);
	if (pi < 0) {
		std::cout << "Failed to connected to pigpiod" << std::endl;
		return -1;
	}
	int handle0 = initAccel(pi, 0);
	int handle1 = initAccel(pi, 1);
	if (handle0 < 0 || handle1 < 0) {
		std::cout << "Failed to open i2c bus" << std::endl;
		pigpio_stop(pi);
		return -1;
	}
	float board_n[3], cam_n[3];
	int pWidth[3];
	float diff[2];
	pWidth[0] = 1500;
	set_servo_pulsewidth(pi, SERVO1_PIN, 1500);
	pWidth[1] = 1500;
	set_servo_pulsewidth(pi, SERVO2_PIN, 1500);
	pWidth[2] = 1500;
	set_servo_pulsewidth(pi, SERVO3_PIN, 1500);
	int count = 0;
	int orient;
	bool rc = -1;
	double cur_time, start_time;
	cur_time = start_time = gCurrentTime();
	// align accelerometers
	while (cur_time - start_time < TIMEOUT) {
		cur_time = gCurrentTime();
		readAccel(pi, handle0, board_n);
		readAccel(pi, handle1, cam_n);
		board_n[0] *= -1;
		board_n[1] *= -1;
		orient = (cam_n[2] < 0) ? 1 : -1;
		diff[0] = cam_n[0] - board_n[0];
		diff[1] = cam_n[1] - board_n[1];
		if (std::abs(diff[0]) < THRESHOLD && std::abs(diff[1]) < THRESHOLD) {
			if (count++ >= 10) {
				std::cout << "Align complete" << std::endl;
				rc = 0;
				break;
			}
			usleep(500000);
			continue;
		}
		if (diff[0] > THRESHOLD) {
			pWidth[0] = checkRange(pWidth[0] + orient);
			set_servo_pulsewidth(pi, SERVO1_PIN, pWidth[0]);
		}
		else if (diff[0] < (THRESHOLD * -1)) {
			pWidth[0] = checkRange(pWidth[0] - orient);
			set_servo_pulsewidth(pi, SERVO1_PIN, pWidth[0]);
		}
		if (diff[1] > THRESHOLD) {
			pWidth[2] = checkRange(pWidth[2] + orient);
			set_servo_pulsewidth(pi, SERVO3_PIN, pWidth[2]);
		}
		else if (diff[1] < (THRESHOLD * -1)) {
			pWidth[2] = checkRange(pWidth[2] - orient);
			set_servo_pulsewidth(pi, SERVO3_PIN, pWidth[2]);
		}
	}
	// save settings if align is successful
	if (rc == 0) {
		//calibrate(pi, handle1, -55, 30, -15, 15);
		json presets;
		json newsetting;
		newsetting["servo1"] = pWidth[0];
		newsetting["servo2"] = pWidth[1];
		newsetting["servo3"] = pWidth[2];
		std::filebuf fb;
		if (fb.open(PRESET_PATH, std::ios::in)) {
			std::istream is(&fb);
			presets << is;
			fb.close();
		}
		presets["onboard"] = newsetting;
		if (fb.open(PRESET_PATH, std::ios::out)) {
			std::ostream os(&fb);
			os << presets.dump(4);
			fb.close();
		}
	}
	else {
		std::cout << "Failed to align accelerometers" << std::endl;
	}
//	set_servo_pulsewidth(pi, 12, 0);
//	set_servo_pulsewidth(pi, 13, 0);
//	set_servo_pulsewidth(pi, 20, 0);
	closeAccel(pi, handle0);
	closeAccel(pi, handle1);
	pigpio_stop(pi);
	return rc;
}





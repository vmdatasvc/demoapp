/*
 * qaIR.cpp
 *
 *  Created on: Apr 12, 2017
 *      Author: ctseng
 */
/* Remember to create .lircrc file in home folder for lirc to work */
#include <iostream>
#include <lirc/lirc_client.h>
#include <sys/time.h>
#include <string>
#include <cstdio>
#include <boost/thread.hpp>
#include <fstream>
using std::string;
using std::cout;
using std::endl;

boost::mutex key_lock;
string key_received = "";

// get current time in milliseconds
long long current_time() {
	struct timeval te;
    gettimeofday(&te, NULL); // get current time
    long long milliseconds = te.tv_sec*1000LL + te.tv_usec/1000; // caculate milliseconds
    return milliseconds;
}

// have a thread running this function to get key press
void getKeyPress() {
	struct lirc_config *config;
	char *code;
	char lirc[5] = "lirc";
	long long prevPress = current_time();
	if(lirc_init(lirc, 1) == -1)
		exit(1);
	if(lirc_readconfig(NULL, &config, NULL) == 0) {
		while(lirc_nextcode(&code) == 0) {
			if(current_time() - prevPress > 400) {
				key_lock.lock();
				key_received = string(code);
				key_lock.unlock();
				free(code);
				prevPress = current_time();
			}
		}
	}
	lirc_freeconfig(config);
	lirc_deinit();
}

// wait for key press for 5 seconds, return false if no key or wrong key pressed
bool testKey(string key) {
	long long cTime;
	bool ret = false;
	cout << "Please press " << key << ": " << std::flush;
	cTime = current_time();
	string out = "";
	while(1) {
		if(current_time() - cTime > 5000) {
			cout << "No Key press received" << endl;
			ret = false;
			break;
		}
		else {
			if(key_received.length() > 0) {
				if(key_received.find(" " + key + " ") != string::npos) {
					cout << "OK" << endl;
					ret = true;
				}
				else {
					cout << "Wrong key" << endl;
					ret = false;
				}
				key_lock.lock();
				key_received = "";
				key_lock.unlock();
				break;
			}
			else
				continue;
		}
	}
	return ret;
}

// returns 0 if success, 1 if failed
int main(void) {
	boost::thread th(getKeyPress);
	bool ret = testKey("KEY_1");
	if (!ret) return 1;
	ret = testKey("KEY_2");
	if (!ret) return 1;
	ret = testKey("KEY_3");
	if (!ret) return 1;
	ret = testKey("KEY_RED");
	th.interrupt();
	return ret ? 0 : 1;
}





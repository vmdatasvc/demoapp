/*
 * OmniSensr_IR_Controller.cpp
 *
 *  Created on: Apr 5, 2017
 *      Author: ctseng
 */
#include "OmniSensr_IR_Controller.hpp"

#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib>
#include <sys/time.h>
#include <pigpiod_if2.h>
#include <lirc/lirc_client.h>
#include <boost/algorithm/string.hpp>
#include <boost/thread.hpp>
#include "json.hpp"

#include "modules/iot/IoT_Common.hpp"

using std::cout;
using std::string;
using std::endl;
using std::string;
using json = nlohmann::json;

OmniSensr_IR_Controller::OmniSensr_IR_Controller(const std::string& configPath) : OmniSensrApp(APP_ID_IR_CONTROLLER, configPath){

	// 60 seconds between heartbeats
	heartbeat_interval = 60;
	// led gpio pins
	led_red_pin[0] = 4;
	led_green_pin[0] = 5;
	led_red_pin[1] = 6;
	led_green_pin[1] = 7;
	led_red_pin[2] = 8;
	led_green_pin[2] = 9;

	killed = false;

}

OmniSensr_IR_Controller::~OmniSensr_IR_Controller(){
	if (keyPressThread) {
		keyPressThread->interrupt();
		delete keyPressThread;
	}
}

// Get current time in milliseconds
long long OmniSensr_IR_Controller::current_msec() {
	struct timeval te;
	gettimeofday(&te, NULL); // get current time
	long long milliseconds = te.tv_sec*1000LL + te.tv_usec/1000;
	return milliseconds;
}
// Set led set color
// set: 0, 1, 2
// color: "red", "green", "orange", "off"
void OmniSensr_IR_Controller::setLED(int num, string color) {
	if (num >= 0 && num < 3) {
		if (color.compare("red") == 0) {
			gpio_write(pi, led_red_pin[num], PI_HIGH);
			gpio_write(pi, led_green_pin[num], PI_LOW);
		}
		else if (color.compare("green") == 0) {
			gpio_write(pi, led_red_pin[num], PI_LOW);
			gpio_write(pi, led_green_pin[num], PI_HIGH);
		}
		else if (color.compare("orange") == 0) {
			gpio_write(pi, led_red_pin[num], PI_HIGH);
			gpio_write(pi, led_green_pin[num], PI_HIGH);
		}
		else if (color.compare("off") == 0) {
			gpio_write(pi, led_red_pin[num], PI_LOW);
			gpio_write(pi, led_green_pin[num], PI_LOW);
		}
	}
}
// Make all LED sets blink
void OmniSensr_IR_Controller::blinkLED(int count, string color) {
	for (int i = 0; i < count; i++) {
		setLED(0, color);
		setLED(1, color);
		setLED(2, color);
		usleep(500000);
		setLED(0, "off");
		setLED(1, "off");
		setLED(2, "off");
		usleep(500000);
	}
}
// This function gets ir signal and process the code accordingly
void OmniSensr_IR_Controller::getKeyPress() {
	char *code;
	long long prevPress = current_msec();
	long long curTime;
	std::ostringstream os;
	string devName = "";
	int ledset;
	boost::thread th;
	while(lirc_nextcode(&code) == 0) {
		curTime = current_msec();
		if (curTime - prevPress > 500) {
			prevPress = curTime;
			string temp(code);
			int idx1 = temp.find("KEY_");
			int idx2 = temp.find(" ", idx1 + 4);
			temp = temp.substr(idx1 + 4, idx2 - (idx1 + 4));
			if (temp.compare("BLUE") == 0) {
				devName = os.str();
				os.str("");
				if (devName.compare(devIP) == 0) {
					cout << "Got IR signal for device" << endl;
					th = boost::thread(boost::bind(&OmniSensr_IR_Controller::blinkLED, this, _1, _2), 4, "green");
				}
				continue;
			}
			if (temp.compare("RED") == 0) {
				if (os.str().length() > 0)
					ledset = std::stoi(os.str());
				os.str("");
				if (devName.compare(devIP) == 0) {
					cout << "Set " << ledset << " to red" << endl;
					setLED(ledset, "red");
				}
				continue;
			}
			if (temp.compare("GREEN") == 0) {
				if (os.str().length() > 0)
					ledset = std::stoi(os.str());
				os.str("");
				if (devName.compare(devIP) == 0) {
					cout << "Set " << ledset << " to green" << endl;
					setLED(ledset, "green");
				}
				continue;
			}
			if (temp.compare("YELLOW") == 0) {
				if (os.str().length() > 0)
					ledset = std::stoi(os.str());
				os.str("");
				if (devName.compare(devIP) == 0) {
					cout << "Set " << ledset << " to orange" << endl;
					setLED(ledset, "orange");
				}
				continue;
			}
			os << temp;
		}
	}
	th.join();
}

// main processing loop code goes here
void OmniSensr_IR_Controller::start(){
	// pigpio
	pi = pigpio_start(NULL, NULL);
	if (pi < 0) {
		cout << "Failed to connect to pigpiod" << endl;
		sendError("Failed to connect to pigpiod", true, false);
		exit(1);
	}
	// set gpio pins
	for (int i = 0; i < 3; i++) {
		set_mode(pi, led_red_pin[i], PI_OUTPUT);
		set_mode(pi, led_green_pin[i], PI_OUTPUT);
	}
	//setup lirc
	char lirc[5] = "lirc";
	if (lirc_init(lirc, 1) == -1) {
		cout << "Failed to initalize lirc" << endl;
		sendError("Failed to initalize lirc", true, false);
		exit(1);
	}
	struct lirc_config *config;
	if (lirc_readconfig(NULL, &config, NULL) != 0) {
		cout << "Failed to read lirc config file" << endl;
		sendError("Failed to read lirc config file", true, false);
		exit(1);
	}
	// start getKeyPress thread
	keyPressThread = new boost::thread(&OmniSensr_IR_Controller::getKeyPress, this);
	// get device ip
	char buff[50];
	FILE* fp = popen("ifconfig eth0 | awk \'/inet addr:/ {print substr($2, 6)}\'", "r");
	if (!fp || fgets(buff, 50, fp) == NULL) {
		cout << "Failed to get ip address" << endl;
		sendError("Failed to get ip address", true, false);
		exit(1);
	}
	pclose(fp);
	devIP = string(buff);
	boost::trim(devIP);
	// use last two number of ip address as devIP
	devIP = devIP.substr(devIP.find(".") + 1);
	devIP = devIP.substr(devIP.find(".") + 1);
	devIP.erase(devIP.find("."), 1);
	cout << "devIP: " << devIP << endl;
	while(!killed){
		main_lock.lock();
		heartbeat(2 * heartbeat_interval);
		main_cond.timed_wait(main_lock, boost::posix_time::seconds(heartbeat_interval));
		main_lock.unlock();
	}
	// close pigpio
	pigpio_stop(pi);

	// close lirc
	lirc_freeconfig(config);
	lirc_deinit();
	// save my state so I can restart from the last known good state
	SaveSetting();
}

// process a command
void OmniSensr_IR_Controller::processCommand(const std::string& cmd){
	json msg;
	try {
		msg = json::parse(cmd);
	}
	catch (std::exception &e) {
		cout << "Failed to parse control message" << endl;
		sendError("Failed to parse control message", false);
		return;
	}
	// gracefully shutdown the app
	if (!msg.value("die", json()).is_null()) {
		killed = true;
		main_cond.notify_one();
	}
	if (!msg.value("blink", json()).is_null() && msg["blink"].is_string()) {
		blinkLED(4, msg["blink"].get<string>());
	}
	if (!msg.value("set0", json()).is_null() && msg["set0"].is_string()) {
		setLED(0, msg["set0"].get<string>());
	}
	if (!msg.value("set1", json()).is_null() && msg["set1"].is_string()) {
		setLED(1, msg["set1"].get<string>());
	}
	if (!msg.value("set2", json()).is_null() && msg["set2"].is_string()) {
		setLED(2, msg["set2"].get<string>());
	}

}

unsigned int OmniSensr_IR_Controller::initConfig(ParseConfigTxt& parser){
	unsigned int n_params = 0;
	n_params += parser.getValue("HEARTBEAT_INTERVAL", heartbeat_interval);
	return n_params;
}


void OmniSensr_IR_Controller::saveState(std::ostream& os) const {

}


int main() {

	OmniSensr_IR_Controller irCtrl("config/OmniSensr_IR_Controller/OmniSensr_IR_Controller_config.txt");
	irCtrl.Run();

	// will never get here, unless the "die" command was sent
	return 0;
}




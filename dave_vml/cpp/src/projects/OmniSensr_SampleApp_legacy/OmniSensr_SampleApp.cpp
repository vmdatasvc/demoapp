//============================================================================
// Name        : OmniSensr_SampleApp.cpp
// Author      : Paul J. Shin
// Version     :
// Copyright   : Your copyright notice
// Description :
//============================================================================

#include <string.h>
#include <sstream>
#include <cstdio>
#include "unistd.h"
#include <pthread.h>
#include <iostream>
#include <iomanip>

// -- AWS IoT SDK files for State Parameter Type definition
#include "aws_iot_error.h"
#include "aws_iot_shadow_json_data.h"

#include "modules/iot/IoT_Common.hpp"
#include "modules/iot/IoT_Gateway.hpp"
#include "modules/utility/TimeKeeper.hpp"
#include "modules/utility/ParseConfigTxt.hpp"

#include "OmniSensr_SampleApp.hpp"

// -- Refer to https://github.com/nlohmann/json
#include "json.hpp"

using json = nlohmann::json;

using namespace std;

/* -- How to deploy and run
 *
 * sshpass -p 'videomininglabs' scp PacketProcessing_at_Sniffer.cpp root@10.250.1.45:
 * sshpass -p 'videomininglabs' ssh root@10.250.1.45 'g++ PacketProcessing_at_Sniffer.cpp -o PacketProcessing_at_Sniffer'
 * sshpass -p 'videomininglabs' ssh root@10.250.1.45 './PacketProcessing_at_Sniffer'
 *
 */

using namespace std;

#define DEBUG_SAMPLEAPP		true
#define DEBUG_MQTT			true
#define DEBUG_IOT			true

pthread_mutex_t gMutex;
IoT_Gateway	OmniSensr_SampleApp::mIoTGateway;
bool OmniSensr_SampleApp::mStartWiFiCalib;
string OmniSensr_SampleApp::mAppType;
int OmniSensr_SampleApp::mPeriodicStateReportingInterval;

OmniSensr_SampleApp::OmniSensr_SampleApp() {
	pthread_attr_init(&mThreadAttr);
	pthread_attr_setdetachstate(&mThreadAttr, PTHREAD_CREATE_JOINABLE);
}

OmniSensr_SampleApp::~OmniSensr_SampleApp() {
}

// -- TODO: Don't know if we leave the app to parse the entire message or if we make a nested structure like Thing Shadow so that each app knows its own control field. For now, I will just leave the full freedom to the app.
void OmniSensr_SampleApp::IoT_ControlChannel_CallBack_func(string topic, string controlMsg) {
// -- Parse the message as you wish and take actions if needed

	// -- Parse the message as you wish and take actions if needed. The message must be in a JSON format
	json controlMsgJson;

	try {
		controlMsgJson = json::parse(controlMsg.c_str());

		if(DEBUG_IOT)
		{
			printf("+++++++ [OmniSensr_SampleApp] Control Message Received +++++++++++++++++++++++\n");
			std::cout << std::setw(2) << controlMsgJson << '\n';
			printf("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
		}

//		if(DEBUG_IOT)
//		{
//			//printf("******* Control Channel Msg Received *******\n%s\n********************************************\n", controlMsg.c_str());
//
//			// special iterator member functions for objects
//			cout << "Control Msg rsved in JSON: " << "\n";
//			cout << setw(4) << controlMsgJson << "\n\n";
//
//			// special iterator member functions for objects
//			for (json::iterator it = controlMsgJson.begin(); it != controlMsgJson.end(); ++it) {
//				string key = it.key();
//				ostringstream value;
//				value << it.value();
//
//				cout << key << " : " << value.str() << "\n";
//			}
//		}

		/*
		 * Flatten the nested JSON into un-nested one.
		 * For example, The value of "WiFiCalib, { "Start": true } can be accessed after flatten() by "/WiFiCalib/Start"
		 */
		json controlJ_flat = controlMsgJson.flatten();

		if(DEBUG_IOT)
		{
			cout << "Control Msg rsved in JSON (flattened): " << "\n";
			cout << setw(4) << controlJ_flat << "\n\n";
		}

		// -- Try to access keys and get their values with a type
		// -- Note: Must catch the two exceptions {out_of_range, domain_error} whenever accessing a value.
		// -- 		Otherwise, it will exit the program if the type does not match or if the key does not exist.
		// -- ########### ADD USER-DEFINED ACTION BELOW #####################

			/*
			 * Check if WiFi Calib command is set. If so, it will initiate packet injection for WiFi calibration. For example,
			 * gStartWiFiCalib = controlJ_flat.at("/Process/Start");
			 * 	if(controlJ_flat.find("/Process/Start") != controlJ_flat.end()) {
					mStartWiFiCalib = controlJ_flat.at("/Process/Start");
				}
			 */

		// -- Change between "sport" or "normal" mode
		if(controlMsgJson.find("RealTimeReport") != controlMsgJson.end()) {
			bool realtimeMode = controlMsgJson.at("RealTimeReport");

			//cout << "Got control Msg: RealTimeReport " << mReportRealTimeTrackResult << " --> " << realtimeMode << endl;

			if(realtimeMode == true) {
				// -- Set the scale in IoT_Gateway to the minimum, which is MIN_STATE_REPORT_INTERVAL defined in IoT_Common.hpp
				int desiredIntervalScale = 0.01;

				mIoTGateway.SetStateParamReportIntervalScale(desiredIntervalScale);
				mPeriodicStateReportingInterval = mIoTGateway.GetStateParamReportInterval(&mPeriodicStateReportingInterval);
			} else {
				// -- Reset the scale in IoT_Gateway back to the normal
				mIoTGateway.ResetStateParamReportIntervalScale();
				mPeriodicStateReportingInterval = mIoTGateway.GetStateParamReportInterval(&mPeriodicStateReportingInterval);
			}
		}

	} catch (std::invalid_argument& e) {		// -- Handle exception when the received msg does not comply with JSON standard
		// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
    	//std::cout << "invalid_argument: " << e.what() << '\n';
	} catch (std::out_of_range& e) {	// -- Handle exception when a non-existing filed is accessed.
		// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
		//std::cout << "out of range: " << e.what() << '\n';
	} catch (std::domain_error& e) {	// -- Handle exception when an incompatible conversion is attempted (e.g., float --> string).
		// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
	   // std::cout << "domain error: " << e.what() << '\n';
	}

}


/*
 * Note that all the types of JsonPrimitiveType of AWS ThingShadow parameters are grouped into just 6 types: int, uint, float, double, bool, string.
 */
void OmniSensr_SampleApp::IoT_DeltaState_CallBack_func(string entireDeltaMsg) {

	json entireDeltaMsgJson = json::parse(entireDeltaMsg);

	// -- Check if there is at least a delta state destined to this app. If not, then exit this function
	if(entireDeltaMsgJson.find(mAppType) == entireDeltaMsgJson.end())
		return;

	json deltaMsgJson = json::parse(entireDeltaMsgJson.at(mAppType).dump());

	// -- If there is at least a delta state destined to this app, then start processing it by looking up the registered state params.
	try {

		if(DEBUG_IOT)
		{
			printf("+++++++ [OmniSensr_SampleApp] Delta State Received +++++++++++++++++++++++\n");
			std::cout << std::setw(2) << deltaMsgJson << '\n';
			printf("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
		}

		for (auto it = deltaMsgJson.begin(); it != deltaMsgJson.end(); ++it) {

			ostringstream actualValue;
			actualValue << it.value();

			devState_t *stateParam = mIoTGateway.FindStateParam(it.key());

					pthread_mutex_lock(&gMutex);
					// -- Check if there's matching state parameter
					if(stateParam != NULL) {
						if(DEBUG_IOT) printf("[Delta Message] stateParam FOUND (key = %s)\n", stateParam->key.c_str());

						// -- Do something here to react to the received delta message to this particular state parameter.
						switch(stateParam->type) {
							case SHADOW_JSON_INT32:
							case SHADOW_JSON_INT16:
							case SHADOW_JSON_INT8: {
								int stateParam_desired;
								sscanf(actualValue.str().c_str(), "%d", &stateParam_desired);

								if(DEBUG_IOT)
									printf("(report, desired) = (%d, %d) -- > ", *(int*)stateParam->value, stateParam_desired);

								// -- Replace the state parameter with the desired value
								*(int*)stateParam->value = stateParam_desired;

								// -- Reset this so that this state can be reported again immediately.
								stateParam->time_lastReported = 0;

								if(DEBUG_IOT)
									printf("(%d)\n", *(int*)stateParam->value);

							}
								break;
							case SHADOW_JSON_UINT32:
							case SHADOW_JSON_UINT16:
							case SHADOW_JSON_UINT8: {
								unsigned int stateParam_desired;
								sscanf(actualValue.str().c_str(), "%u", &stateParam_desired);

								if(DEBUG_IOT)
									printf("(report, desired) = (%u, %u) --> ", *(unsigned int*)stateParam->value, stateParam_desired);

								// -- Replace the state parameter with the desired value
								*(unsigned int*)stateParam->value = stateParam_desired;

								// -- Reset this so that this state can be reported again immediately.
								stateParam->time_lastReported = 0;

								if(DEBUG_IOT)
									printf("(%u)\n", *(unsigned int*)stateParam->value);
							}
								break;
							case SHADOW_JSON_FLOAT: {
								float stateParam_desired;
								sscanf(actualValue.str().c_str(), "%f", &stateParam_desired);

								if(DEBUG_IOT) {
									printf("[%s] ", actualValue.str().c_str());
									cout << fixed << setprecision(IOT_FLOAT_PRECISION);
									cout << " (report, desired) = ("<< *(float*)stateParam->value << ", " << stateParam_desired << ") --> ";
								}

								// -- Replace the state parameter with the desired value
								*(float*)stateParam->value = stateParam_desired;

								// -- Reset this so that this state can be reported again immediately.
								stateParam->time_lastReported = 0;

								if(DEBUG_IOT) {
									cout << fixed << setprecision(IOT_FLOAT_PRECISION);
									cout << "("<< *(float*)stateParam->value << ")" << endl;
								}
							}
								break;
							case SHADOW_JSON_DOUBLE: {
								double stateParam_desired;
								sscanf(actualValue.str().c_str(), "%lf", &stateParam_desired);

								if(DEBUG_IOT) {
									printf("[%s] ", actualValue.str().c_str());
									cout << fixed << setprecision(IOT_DOUBLE_PRECISION);
									cout << " (report, desired) = ("<< *(double*)stateParam->value << ", " << stateParam_desired << ") --> ";
								}

								// -- Replace the state parameter with the desired value
								*(double*)stateParam->value = stateParam_desired;

								// -- Reset this so that this state can be reported again immediately.
								stateParam->time_lastReported = 0;

								if(DEBUG_IOT) {
									cout << fixed << setprecision(IOT_DOUBLE_PRECISION);
									cout << "("<< *(double*)stateParam->value << ")" << endl;
								}
							}
								break;
							case SHADOW_JSON_BOOL: {
								char stateParam_desired[99] = {'\0'};
								sscanf(actualValue.str().c_str(), "%s", stateParam_desired);

								if(DEBUG_IOT)
									printf("(report, desired) = (%d, %s) --> ", *(bool*)stateParam->value, stateParam_desired);

								// -- Replace the state parameter with the desired value
								if(stateParam_desired[0] == 't')	// -- true
									*(bool*)stateParam->value = true;
								if(stateParam_desired[0] == 'f')	// -- false
									*(bool*)stateParam->value = false;

								// -- Reset this so that this state can be reported again immediately.
								stateParam->time_lastReported = 0;

								if(DEBUG_IOT)
									printf("(%d)\n", *(bool*)stateParam->value);

							}
								break;

							case SHADOW_JSON_STRING: {
								if(DEBUG_IOT)
									printf("(report, desired) = (%s, %s) -->", (*(string*)stateParam->value).c_str(), actualValue.str().c_str());

								// -- String-type value in JSON is wrapped with double-quotation marks, so get rid of them.
								string strExtracted = actualValue.str().substr(1,actualValue.str().size()-2);

								// -- Replace the state parameter with the desired value
								*(string*)stateParam->value = strExtracted;

								// -- Reset this so that this state can be reported again immediately.
								stateParam->time_lastReported = 0;

								if(DEBUG_IOT)
									printf("(%s)\n", (*(string*)stateParam->value).c_str());

							}
								break;

							default:
								break;
						}
					}
					pthread_mutex_unlock(&gMutex);
		}

	} catch (std::invalid_argument& e) {		// -- Handle exception when the received msg does not comply with JSON standard
		// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
    	//std::cout << "invalid_argument: " << e.what() << '\n';
    } catch (std::out_of_range& e) {	// -- Handle exception when a non-existing filed is accessed.
		// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
		//std::cout << "out of range: " << e.what() << '\n';
	} catch (std::domain_error& e) {	// -- Handle exception when an incompatible conversion is attempted (e.g., float --> string).
		// -- TODO: Implement how to handle this exception. Should we let the sender know about this?
	   // std::cout << "domain error: " << e.what() << '\n';
	}
}

void OmniSensr_SampleApp::PeriodicProcessing_Loop() {

	double currTime, prevTime_bufferProc;
	prevTime_bufferProc = gCurrentTime();

	// -- Infinite loop to periodically send data and state update.
	int data_cnt = 0;
	do {

		currTime = gCurrentTime();

		// -- Periodically process/send data to the IoT Cloud
		if(currTime - prevTime_bufferProc >= mPeriodicDataBufferProcessingInterval) {
			prevTime_bufferProc = currTime;


			// -- ######################################################################
			// -- User-defined Data Generation code
			ostringstream data_cnt_str;

			data_cnt_str << "[SampleApp] Data Count: " << data_cnt++ << endl;

			// -- Send data
			if(data_cnt_str.str().size() > 0)
			{
				VM_IoT_Error_t errorCode;

				// -- Put into a message buffer for transmission
				// -- AWS_IoT_Gateway will publish this data to a topic 'data/<app_id>/<store_name_prefix>/<store_name_suffix>/<device_name>' (e.g., data/wifiRss/vmhq/16801/cam001)
				errorCode = mIoTGateway.SendData(data_cnt_str.str());

				if(errorCode != NO_PROBLEM)
					cout << "VM_IoT_Error: mIoTGateway.SendData(): " << errorCode << endl;

				// -- Send it again in a subtopic just for demonstration
				// -- AWS_IoT_Gateway will publish this data to a topic 'data/<app_id>/<store_name_prefix>/<store_name_suffix>/<device_name>/<sub-topic>' (e.g., data/wifiRss/vmhq/16801/cam001/example)
				errorCode = mIoTGateway.SendData("/example",data_cnt_str.str());

				if(errorCode != NO_PROBLEM)
					cout << "VM_IoT_Error: mIoTGateway.SendData(): " << errorCode << endl;

				if(DEBUG_SAMPLEAPP)
					printf("[SampleApp] Sending Data: %s\n", data_cnt_str.str().c_str());
			}
			// -- ######################################################################

		}

	} while(1);
}

void* OmniSensr_SampleApp::PeriodicProcessingThread(void *apdata) {

	OmniSensr_SampleApp* pb = (OmniSensr_SampleApp*)apdata;

	pb->PeriodicProcessing_Loop();

    return NULL;
}

// -- Read config file
bool OmniSensr_SampleApp::ParseSetting(string configFile) {
	ParseConfigTxt config;
	config.parse(configFile);

	int paramReadCnt = 0;
	if(config.getValue_string("VERSION", mSoftwareVersion)) {
		paramReadCnt++;
		printf("VERSION = %s\n", mSoftwareVersion.c_str());
	}
	if(config.getValue_int("PERIODIC_DATA_BUFFER_PROCESSING_INTERVAL", mPeriodicDataBufferProcessingInterval)) {
		paramReadCnt++;
		printf("PERIODIC_DATA_BUFFER_PROCESSING_INTERVAL = %d\n", mPeriodicDataBufferProcessingInterval);
	}
	if(config.getValue_int("PERIODIC_STATE_REPORTING_INTERVAL", mPeriodicStateReportingInterval)) {
		paramReadCnt++;
		printf("PERIODIC_STATE_REPORTING_INTERVAL = %d\n", mPeriodicStateReportingInterval);
	}

	bool ret = false;
	if(paramReadCnt != (int)config.paramSize()) {
		fprintf(stderr, "!! paramReadCnt[%d] != config.paramSize() [%d] !!\n", paramReadCnt, config.paramSize());
		ret = false;
	} else
		ret = true;

	return ret;

}

void OmniSensr_SampleApp::Run(string configFile)
{
	// -- Set the initial time clock. Unit: [sec]
	mInitialTime = gCurrentTime();

	// -- Read config file
	ParseSetting(configFile);


	string mqttTopicState, mqttTopicData;

		// -- Define the topic for state
		{
			mAppID.clear();
			mAppID.append(APP_ID_SAMPLE_APP);

			mqttTopicState.clear();
			mqttTopicState.append(AWS_IOT_APP_STATE_CHANNEL_PREAMBLE);
			mqttTopicState.append("/");
			mqttTopicState.append(mAppID);

			printf("[SampleApp] mqttTopicState = %s\n", mqttTopicState.c_str());
		}

		// -- Set the AppId in string. (e.g., mAppType = "8" for WiFi Tracker)
		{
			ostringstream appIdStr;
			appIdStr.str("");
			appIdStr.clear();
			appIdStr << sampleApp;

			mAppType = appIdStr.str();
		}

		// -- Define the topic for data
		{
			// -- Compose the publish topic
			// -- AWS_IoT_Gateway will publish data to a topic 'data/<app_id>/<store_name_prefix>/<store_name_suffix>/<device_name>' (e.g., data/wifiRss/vmhq/16801/cam001)
			mqttTopicData.append(AWS_IOT_APP_DATA_CHANNEL_PREAMBLE);
			mqttTopicData.append("/");
			mqttTopicData.append(mAppID);
			mqttTopicData.append("/");
			mqttTopicData.append(mIoTGateway.GetMyStoreName());
			mqttTopicData.append("/");
			mqttTopicData.append(mIoTGateway.GetMyDevName());

			printf("[SampleApp] mqttTopicData = %s\n", mqttTopicData.c_str());
		}

	// -- Register Delta State callback function
	mIoTGateway.RegisterDeltaCallbackFunc(&IoT_DeltaState_CallBack_func);

	// -- Register Control Channel callback function
	mIoTGateway.RegisterControlCallbackFunc(&IoT_ControlChannel_CallBack_func);

	// -- Start running aws-iot-gateway with a ThingShadow topic
	mIoTGateway.Run(mAppID, mqttTopicState, mqttTopicData, IOT_GATEWAY_CONFIG_FILE);

	// -- ######################################################################
	// -- User-defined State Parameter Definition
	// -- Add state parameters
	// -- Note: You don't need to declare the state parameters to be 'global' since the pointers to the state parameters will be carried through.
	/*
	 * Note that all the types of JsonPrimitiveType of AWS ThingShadow parameters are grouped into just 6 types: int, uint, float, double, bool, string.
	 * So just use the 6 parameter types when defining a state parameter.
	 */
	float iot_stateParam_float = 3.345678f;
	mIoTGateway.AddStateParam(SHADOW_JSON_FLOAT, "state-float", &iot_stateParam_float, NULL, mPeriodicStateReportingInterval);

	double iot_stateParam_double = 3.345678912345678d;
	mIoTGateway.AddStateParam(SHADOW_JSON_DOUBLE, "state-double", &iot_stateParam_double, NULL, mPeriodicStateReportingInterval);

//	int iot_stateParam_int = 123456789;
//	mIoTGateway.AddStateParam(SHADOW_JSON_INT32, "state-int", &iot_stateParam_int, NULL, mPeriodicStateReportingInterval);
//
//	bool iot_stateParam_bool = true;
//	mIoTGateway.AddStateParam(SHADOW_JSON_BOOL, "state-bool", &iot_stateParam_bool, NULL, mPeriodicStateReportingInterval);
//
	//string iot_stateParam_string = "OmniSensr_SampleApp Testing \"Hello World!\" (x,y,z, 3456.876423) ";
	string iot_stateParam_string = "OmniSensr_SampleApp Testing(x,y,z, 3456.876423) ";
	mIoTGateway.AddStateParam(SHADOW_JSON_STRING, "state-string", &iot_stateParam_string, NULL, mPeriodicStateReportingInterval);
	// -- ######################################################################

	// -- Spawn a thread that periodically sends the state info via ThingShadow msg.
	if(pthread_create(&mPeriodicIoTProcessing_thread, &mThreadAttr, PeriodicProcessingThread, (void *)this))
	{
		fprintf(stderr, "Error in pthread_create(PeriodicProcessingThread in AWS_IoT_Module)\n");
		exit(0);
	}

	// -- ######################################################################
	// -- Below is a sample code on updating the state info. A user should implement it according to their use cases.

	double currTime, prevTime_stateUpdate;

	double periodicStateUpdateInterval = 4.0;

	prevTime_stateUpdate = gCurrentTime();

	// -- Infinite loop to demonstrate some dynamic state change.
	do {

		currTime = gCurrentTime();

		// -- Assume we make periodic changes in the state parameters
		if(currTime - prevTime_stateUpdate >= periodicStateUpdateInterval) {
			prevTime_stateUpdate = currTime;

			// -- User-defined State Update code
			{
				pthread_mutex_lock(&gMutex);
				iot_stateParam_float = iot_stateParam_float*1.1 > 999999.0f ? iot_stateParam_float - 899999.0f : iot_stateParam_float*1.1;
				iot_stateParam_double = iot_stateParam_double*1.1 > 999999.0f ? iot_stateParam_double - 899999.0f : iot_stateParam_double*1.1;
//				iot_stateParam_int = iot_stateParam_int*1.1 > 999999999 ? iot_stateParam_int - 899999999 : iot_stateParam_int*1.1;
//				iot_stateParam_bool = !iot_stateParam_bool;
				pthread_mutex_unlock(&gMutex);
			}

			cout << "iot_stateParam_string = [" << iot_stateParam_string << "]\n";
		}

		usleep(1000);

	} while(1);
	// -- ######################################################################

}

int main() {

	OmniSensr_SampleApp sampleApp;

	sampleApp.Run(APP_CONFIG_FILE);

	return 0;
}

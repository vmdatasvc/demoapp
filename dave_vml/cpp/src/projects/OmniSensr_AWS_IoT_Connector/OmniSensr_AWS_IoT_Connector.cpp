//============================================================================
// Name        : OmniSensr_AWS_IoT_Connector.cpp
// Author      : Jimmy Tseng
// Version     :
// Copyright   : Your copyright notice
// Description :
//============================================================================

#include "OmniSensr_AWS_IoT_Connector.hpp"
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include "json.hpp"
#include "modules/utility/JsonUtil.hpp"
#include "MQTTClient.hpp"
#include "modules/iot/Dev_Common.hpp"
#include <iostream>
#include "modules/iot/IOTUtility.hpp"
#include "modules/utility/TimeKeeper.hpp"

using json = nlohmann::json;
using std::cout;
using std::string;
using std::vector;
using std::endl;

// variables needed by both AWS_MQTT and Connector app
json desired;
json previousReport;
json lastUpdate;

std::string desired_json_filepath;
std::string last_update_json_filepath;
std::string previous_json_filepath;

IOTUtility iotutil;

// change appName in shadow update json to appType
void changeJsonAppName_state(json &input) {
	json temp;
	temp["state"] = {};
	temp["state"]["reported"] = {};
	for (auto it = input["state"]["reported"].begin(); it != input["state"]["reported"].end(); it++) {
		temp["state"]["reported"][iotutil.getAppType(it.key())] = it.value();
	}
	input = temp;
}
// change appType in shadow update json to appName
void changeJsonAppType_state(json &input) {
	json temp;
	temp["state"] = {};
	temp["state"]["reported"] = {};
	for (auto it = input["state"]["reported"].begin(); it != input["state"]["reported"].end(); it++) {
		temp["state"]["reported"][iotutil.getAppName(it.key())] = it.value();
	}
	input = temp;
}

// change appName in shadow delta json to appType
void changeJsonAppName_delta(json &input) {
	json temp;
	for (auto it = input.begin(); it != input.end(); it++) {
		temp[iotutil.getAppType(it.key())] = it.value();
	}
	input = temp;
}

// change appType in shadow delta json to appName
void changeJsonAppType_delta(json &input) {
	json temp;
	for (auto it = input.begin(); it != input.end(); it++) {
		temp[iotutil.getAppName(it.key())] = it.value();
	}
	input = temp;
}



void OmniSensr_AWS_IoT_Connector::AWS_MQTT::initial() {
	pmid = 0;
	subCount = 0;
	totalSub = 0;
	ack_track.clear();
	mosqpp::lib_init();
	if (tls_opts_set(1, "tlsv1.2", NULL)) {
		cout << "AWS_MQTT failed to set tls opts" << endl;
		return;
	}
	else if (tls_set(pRootCA.c_str(), NULL, pCert.c_str(), pPrivateKey.c_str())) {
		cout << "AWS MQTT failed to set tls" << endl;
		return;
	}
	else if (connect(pHost.c_str(), pPort, 20)) {
		cout << "AWS MQTT failed to connect" << endl;
		return;
	}
	loop_start();
	//message_retry_set(10);
}
OmniSensr_AWS_IoT_Connector::AWS_MQTT::AWS_MQTT(
		string thingname, string host, int port,
		string rootCA, string cert, string privateKey, OmniSensr_AWS_IoT_Connector& conn_in)
			: mosqpp::mosquittopp(NULL), conn(&conn_in) {
	thingshadow = "$aws/things/" + thingname + "/shadow";
	pRootCA = rootCA;
	pCert = cert;
	pPrivateKey = privateKey;
	pHost = host;
	pPort = port;
	initial();
}

void OmniSensr_AWS_IoT_Connector::AWS_MQTT::restart() {
	disconnect();
	initial();
}

OmniSensr_AWS_IoT_Connector::AWS_MQTT::~AWS_MQTT() {
	loop_stop(true);
	mosqpp::lib_cleanup();
}

// publish to topic in AWS
int OmniSensr_AWS_IoT_Connector::AWS_MQTT::pub(string topic, string payload, int qos) {
	int rc;
	if (connected) {
		// mid is the message id of the publish
		int mid = pmid;
		rc = publish(&pmid, topic.c_str(), payload.length(), payload.c_str(), qos, false);
		double startTime = gCurrentTime();
		// check for 5 seconds if on_publish acknowledged mid
		while (gCurrentTime() - startTime < 5) {
			auto it = ack_track.find(mid);
			if (it != ack_track.end()) {
				ack_track.erase(it);
				return rc;
			}
			usleep(10000);
		}
		rc = MOSQ_ERR_CONN_LOST;
		connected = false;
	}
	else {
		rc = MOSQ_ERR_NO_CONN;
	}
	return rc;
}

// subscribe to topic in AWS
int OmniSensr_AWS_IoT_Connector::AWS_MQTT::sub(string topic, int qos) {
	int rc;
	if (connected) {
		//		int mid = pmid;
		//		cout << "send: " << mid << endl;
		rc = subscribe(&pmid, topic.c_str(), qos);
		totalSub++;
		// somehow this trick to check ack doesn't work for subscribing
		//		double startTime = gCurrentTime();
		//		while (gCurrentTime() - startTime < 20) {
		//			if (ack_track[mid] == true) {
		//				ack_track.erase(ack_track.find(mid));
		//				return rc;
		//			}
		//			usleep(10000);
		//		}
		//		rc = MOSQ_ERR_CONN_LOST;
	}
	else {
		rc = MOSQ_ERR_NO_CONN;
	}
	if (rc == MOSQ_ERR_SUCCESS) cout << "Subscribed to " << topic << endl;
	else cout << "Sub error rc: " << rc << endl;
	return rc;
}

// send state update to thingshadow
int OmniSensr_AWS_IoT_Connector::AWS_MQTT::shadow_update(string payload) {
	string shadowUpdateTopic = thingshadow + "/update";
	return pub(shadowUpdateTopic, payload, 1);
}

// sned blank message to thingshadow get topic to request full document
void OmniSensr_AWS_IoT_Connector::AWS_MQTT::shadow_get() {
	string shadowGetTopic = thingshadow + "/get";
	publish(&pmid, shadowGetTopic.c_str(), 0, "", 1, false);
}

// once connected, subscribe to certain topics
void OmniSensr_AWS_IoT_Connector::AWS_MQTT::on_connect(int rc) {
	cout << "Connected to AWS" << endl;
	connected = true;
	string deltaTopic = thingshadow + "/update/delta";
	sub(deltaTopic, 1);
	string shadowGetTopic = thingshadow + "/get/accepted";
	sub(shadowGetTopic, 1);
	std::ostringstream controlTopic;
	controlTopic << "control";
	sub(controlTopic.str(), 1);
	controlTopic << "/+";
	sub(controlTopic.str(), 1);
	string storename = conn->getStoreLocation();
	string retail = storename.substr(0, storename.find("/"));
	string storeNum = storename.substr(storename.find("/") + 1);
	controlTopic << "/" << retail;
	sub(controlTopic.str(), 1);
	controlTopic << "/" << storeNum;
	sub(controlTopic.str(), 1);
	controlTopic << "/" << conn->getDeviceName();
	sub(controlTopic.str(), 1);
}

void OmniSensr_AWS_IoT_Connector::AWS_MQTT::on_disconnect(int rc) {
	cout << "Disconnected from AWS" << endl;
	connected = false;
}

void OmniSensr_AWS_IoT_Connector::AWS_MQTT::on_publish(int mid) {
	ack_track.insert(mid - 1);
}

void OmniSensr_AWS_IoT_Connector::AWS_MQTT::on_subscribe(int mid, int qos_count, const int *granted_qos) {
	subCount++;
}

// depending on the topics messages are from, perform some actions
void OmniSensr_AWS_IoT_Connector::AWS_MQTT::on_message(const struct mosquitto_message *message) {
	string payload = (char*)message->payload;
	payload.resize(message->payloadlen);

	// message from requesting full thing shadow document
	string shadowGet = thingshadow + "/get/accepted";
	if (shadowGet.compare(message->topic) == 0) {
		try {
			json msg = json::parse(payload);
			cout << "Local desired json updated" << endl;
			if (!msg.value("state", json()).is_null() && !msg["state"].value("desired", json()).is_null()) {
				desired = msg["state"]["desired"];
			}
			else {
				cout << "No desired state in thing shadow" << endl;
				conn->sendError("No desired state in thing shadow", false);
				desired.clear();
			}
			JsonUtil::writeJson(desired_json_filepath, desired);
			changeJsonAppType_delta(desired);
		}
		catch (std::exception &e) {
			cout << "Failed to parse desired state in shadow document" << endl;
			conn->sendError("Failed to parse desired state in shadow document", false);
		}
	}

	// message from AWS delta
	string deltaTopic = thingshadow + "/update/delta";
	if (deltaTopic.compare(message->topic) == 0) {
		json msg = json::parse(payload);
		cout << "Delta arrived" << endl;
		deltaMsg = msg["state"];
		changeJsonAppType_delta(deltaMsg);
		deltaArrived = true;
		json updateDesired = json::diff(desired, deltaMsg);
		for (auto it = updateDesired.begin(); it != updateDesired.end(); ) {
			string op = (*it)["op"];
			if (op.compare("replace") == 0 || op.compare("add") == 0) {
				it++;
			}
			else
				updateDesired.erase(it);
		}
		if (updateDesired.size() > 0) {
			desired = desired.patch(updateDesired);
			cout << "Local desired json updated" << endl;
			json temp = desired;
			changeJsonAppName_delta(temp);
			JsonUtil::writeJson(desired_json_filepath, temp);
		}
	}

	// pass along message if topic starts with "control"
	if (strncmp("control", message->topic, 7) == 0) {
		cout << "Control message received" << endl;
		controlReceived = true;
		controlMsg = payload;
		controlTopic = string(message->topic);
		// Strip the topic to only containing control/appName
		auto idx = controlTopic.find("/");
		if (idx != string::npos) {
			idx = controlTopic.find("/", idx + 1);
		}
		controlTopic = controlTopic.substr(0, idx);
	}
}

void OmniSensr_AWS_IoT_Connector::AWS_MQTT::on_error() {
	cout << "on error" << endl;
}

OmniSensr_AWS_IoT_Connector::OmniSensr_AWS_IoT_Connector(const std::string& configPath) : OmniSensrApp("connectorApp", configPath){

	// set the default values of parameters
	desired_json_filepath = "config/OmniSensr_AWS_IoT_Connector/desired.json";
	previous_json_filepath = "config/OmniSensr_AWS_IoT_Connector/previousReport.json";
	last_update_json_filepath = "config/OmniSensr_AWS_IoT_Connector/lastUpdate.json";
	shadow_periodic_report_interval = 300.0; // sec
	minimum_publish_interval = 0.1; // sec
	mqtt_command_timeout = 10000; // ms
	tls_handshake_timeout = 10000; // ms
	full_report_interval = -1; // sec, negative value to disable
	refresh_desired_interval = 43200; // sec
	heartbeat_interval = 60; // sec

	// functionality of this variable not implemented
	realTimeReport = false;

	insync = false;

	killed = false;

}

OmniSensr_AWS_IoT_Connector::~OmniSensr_AWS_IoT_Connector(){
	// Nothing to do here
	mqttRecv.stop();
}

void OmniSensr_AWS_IoT_Connector::localShadowCallback(const mosquitto_message *msg) {
	string s_msg((char*)msg->payload);
	s_msg.resize(msg->payloadlen);
	json updateJson;
	try {
		updateJson = json::parse(s_msg);
		if (updateJson.value("state", json()).is_null() || updateJson.value("/state/reported"_json_pointer, json()).is_null()) {
			cout << "Local shadow callback msg has wrong format" << endl;
			return;
		}
	} catch (std::exception &e) {
		cout << "Failed to parse local shadow msg to json" << endl;
		sendError("Failed to parse local shadow msg to json", false);
		return;
	}
	// Check if local delta exists
	json desired_diff = json::diff(updateJson["state"]["reported"], desired);
	for (auto it = desired_diff.begin(); it != desired_diff.end(); it++) {
		string op = (*it)["op"];
		if (op.compare("replace") == 0) {
			// check if variable is float
			string path = (*it)["path"];
			json::json_pointer ptr(path);
			if (updateJson["state"]["reported"].at(ptr).is_number_float() || desired.at(ptr).is_number_float()) {
				if (abs((float)updateJson["state"]["reported"].at(ptr) - (float)desired.at(ptr)) < 0.00001) {
					continue;
				}
			}
			JsonUtil::addPatchToJson(*it, localDelta);
		}
	}
	insync = true;
	if (localDelta.size() > 0) {
		sendLocalDelta = true;
		insync = false;
	}

	// Update accumulated update json
	json diff = json::diff(accUpdate, updateJson);
	for (auto it = diff.begin(); it != diff.end(); ) {
		string op = (*it)["op"];
		if (op.compare("remove") == 0) {
			diff.erase(it);
		}
		else {
			it++;
		}
	}
	accUpdate = accUpdate.patch(diff);

	// check if there's local delta thats not in the update
	if (insync && desired.size() > 0) {
		json currState = previousReport;
		changeJsonAppType_state(currState);
		// update with previous accumulated updates
		if (accUpdate.size() > 0) {
			json merge = json::diff(currState, accUpdate);
			for (auto it = merge.begin(); it != merge.end(); ) {
				string op = (*it)["op"];
				if (op.compare("remove") == 0) {
					merge.erase(it);
				}
				else {
					it++;
				}
			}
			currState = currState.patch(merge);
		}
		json anyDiff = json::diff(currState["state"]["reported"], desired);
		for (auto it = anyDiff.begin(); it != anyDiff.end(); it++) {
			string op = (*it)["op"];
			if (op.compare("replace") == 0) {
				string path = (*it)["path"];
				json::json_pointer ptr(path);
				if (currState["state"]["reported"].at(ptr).is_number_float() || desired.at(ptr).is_number_float()) {
					if (abs((float)currState["state"]["reported"].at(ptr) - (float)desired.at(ptr)) < 0.00001) {
						continue;
					}
				}
				insync = false;
				break;
			}
		}
	}

}

void OmniSensr_AWS_IoT_Connector::localDataCallback(const mosquitto_message *msg) {

	// Do not send data to AWS if it's from wifiRSS
	if (strncmp("data/wifiRSS", msg->topic, 12) == 0) {
		return;
	}

	string topic(msg->topic);
	string s_msg((char*)msg->payload);
	s_msg.resize(msg->payloadlen);
	json data;
	try {
		// try to cast payload as json
		data[topic] = json::parse(s_msg);
	} catch (std::invalid_argument &e) {
		// save payload as string
		data[topic] = s_msg;
	}
	data_lock.lock();
	data_queue.push(data);
	data_lock.unlock();
}

// re-pack the state messages and send json to shadow/update
void OmniSensr_AWS_IoT_Connector::localStateCallback(const mosquitto_message *msg) {
	json updateJson;
	string senderName (msg->topic);
	std::istringstream instream((char*)msg->payload);
	auto idx = senderName.find("/");
	if (idx != string::npos) {
		senderName = senderName.substr(idx + 1);
	}
	else {
		cout << "Wrong format for state message" << endl;
		return;
	}
	// delta channel
	if (senderName.compare("delta") == 0) {
		return;
	}
	string temp_line;
	// Convert msg to json HERE!!
	while(getline(instream, temp_line, '\n')) {
		auto idx1 = temp_line.find(" ");
		if (idx1 != string::npos) {
			auto idx2 = temp_line.find(" ", idx1 + 1);
			if (idx2 != string::npos) {
				auto idx3 = temp_line.find(" ", idx2 + 1);
				if (idx3 != string::npos) {
					string varName = temp_line.substr(idx1 + 1, idx2 - idx1 - 1);
					if (varName.compare("MessageId") != 0) {
						json val;
						string type = temp_line.substr(idx2 + 1, 1);
						// if data type is string
						if (type.compare("9") == 0) {
							val = temp_line.substr(idx3 + 1);
						}
						// if data type is boolean
						else if (type.compare("8") == 0) {
							if (temp_line.substr(idx3 + 1).compare("1") == 0) {
								val = true;
							}
							else if (temp_line.substr(idx3 + 1).compare("0") == 0) {
								val = false;
							}
						}
						else {
							try {
								val = json::parse(temp_line.substr(idx3 + 1));
							} catch (std::exception &e) {
								cout << "Failed to parse input message from state message to json" << endl;
								sendError("Failed to parse input message from state message to json", false);
							}
						}
						updateJson[varName] = val;
					}
				}
			}
		}
	}
	// wrap updateJson and send to local topic "shadow/update"
	json sendJson;
	sendJson["state"] = {};
	sendJson["state"]["reported"] = {};
	sendJson["state"]["reported"][senderName] = updateJson;
	mqttSender.publish("shadow/update", sendJson.dump(), 1);
}

// main processing loop code goes here
void OmniSensr_AWS_IoT_Connector::start(){

	// -- Load local jsons
	JsonUtil::readJson(desired_json_filepath, desired);
	if (desired.is_null())
		desired = json({});
	if (desired.size() > 0)
		changeJsonAppType_delta(desired);
	JsonUtil::readJson(previous_json_filepath, previousReport);
	JsonUtil::readJson(last_update_json_filepath, lastUpdate);
	if (lastUpdate.size() == 0)
		sendLastUpdate = false;

	// -- Setup local mqtt receiver
	boost::function<void (const struct mosquitto_message *message)> shadow_cb (boost::bind(&OmniSensr_AWS_IoT_Connector::localShadowCallback, this, _1));
	boost::function<void (const struct mosquitto_message *message)> data_cb (boost::bind(&OmniSensr_AWS_IoT_Connector::localDataCallback, this, _1));
	boost::function<void (const struct mosquitto_message *message)> state_cb (boost::bind(&OmniSensr_AWS_IoT_Connector::localStateCallback, this, _1));
	mqttRecv.subscribe("shadow/update", shadow_cb, 1);
	mqttRecv.subscribe("data/#",data_cb, 1);
	mqttRecv.subscribe("state/+", state_cb, 1);

	mqttRecv.start();
	mqttSender.start();

	// send full status to local topic "shadow/update"
	sendFullState();

	// -- Setup redis wrapper
	rwrapper.reset(new redis_wrapper());

	// -- Setup mqtt client to AWS
	aws_iot_cert_directory += "/";
	aws_iot_root_ca_filename = aws_iot_cert_directory + aws_iot_root_ca_filename;
	aws_iot_certificate_filename = aws_iot_cert_directory + aws_iot_certificate_filename;
	aws_iot_private_key_filename = aws_iot_cert_directory + aws_iot_private_key_filename;
	string thingname = boost::algorithm::replace_all_copy(getStoreLocation(), "/", "-") + "-" + getDeviceName();
	string aws_host;
	if (isTestbed) {
		aws_host = aws_iot_mqtt_host_testbed;
	}
	else {
		aws_host = aws_iot_mqtt_host_production;
	}
	awsMQTT = new AWS_MQTT(thingname, aws_host, 8883,
			aws_iot_root_ca_filename, aws_iot_certificate_filename, aws_iot_private_key_filename, *this);
	// -- Main loop, check timer to perform action
	double currTime, prevShadow, prevDesired, prevData, prevFullShadow, prevStat, prevHeartbeat, prevReconnect;
	prevShadow = prevDesired = prevData = prevFullShadow = prevStat = prevReconnect = gCurrentTime();
	prevDesired = -refresh_desired_interval;
	prevHeartbeat = -heartbeat_interval;
	int rc;
	while(1) {
		currTime = gCurrentTime();
		// -- Send last shadow update to AWS
		if (sendLastUpdate && awsMQTT->isConnected() && lastUpdate.size() > 0) {
			if (awsMQTT->shadow_update(lastUpdate.dump()) == 0) {

			}
			else {
				sendError("Connection to AWS possibly lost", false);
			}
			sendLastUpdate = false;
		}

		// -- Check and send stored data in database
		if (checkDatabase && awsMQTT->isConnected()) {
			rwrapper->connect();
			if (rwrapper->list_len("datachannel") > 0) {
				string topic;
				string payload;
				string combine = rwrapper->list_lpop("datachannel");
				while(combine.length() > 0) {
					auto it = combine.find(separate);
					topic = combine.substr(0, it);
					payload = combine.substr(it + separate.length());
					if (awsMQTT->pub(topic, payload, 1) == 0) {

					}
					combine = rwrapper->list_lpop("datachannel");
				}
			}
			checkDatabase = false;
			rwrapper->disconnect();
		}

		// -- Check if delta message is present
		if (awsMQTT->hasDeltaArrived()) {
			json msg = awsMQTT->getDeltaMsg();
			mqttSender.publish("shadow/update/delta", msg.dump(), 1);
			// send duplicate to this topic so old apps can get delta msg
			json temp = msg;
			changeJsonAppName_delta(temp);
			mqttSender.publish("state/delta", temp.dump(), 1);
			cout << msg.dump() << endl;
			awsMQTT->setDeltaFlag();
		}

		// -- Check if there's local delta to send
		if (sendLocalDelta) {
			cout << "Send local delta: " << endl;
			cout << localDelta.dump() << endl;
			mqttSender.publish("shadow/update/delta", localDelta.dump(), 1);
			// send duplicate to this topic so old apps can get delta msg
			json temp = localDelta;
			changeJsonAppName_delta(temp);
			mqttSender.publish("state/delta", temp.dump(), 1);
			localDelta.clear();
			sendLocalDelta = false;
		}

		// -- Check if control message is present
		if (awsMQTT->isControlReceived()) {
			mqttSender.publish(awsMQTT->getControlTopic(), awsMQTT->getControlMsg(), 1);
			awsMQTT->setControlFlag();
		}


		// -- Check if it's time to request full shadow document, need to check if awsMQTT is connected and has subscribed to topics
		if (awsMQTT->isReady() && (currTime - prevDesired >= refresh_desired_interval)) {
			cout << "Send shadow get" << endl;
			awsMQTT->shadow_get();
			prevDesired = currTime;
		}

		// -- Check if it's time to send full shadow update, default value for full_report_interval is -1 so this will not be executed
		if (awsMQTT->isConnected() && full_report_interval > 0 && currTime - prevFullShadow >= full_report_interval * reportScale) {
			changeJsonAppName_state(accUpdate);
			json diff = json::diff(previousReport, accUpdate);
			for (auto it = diff.begin(); it != diff.end(); ) {
				string op = (*it)["op"];
				if (op.compare("remove") == 0) {
					diff.erase(it);
				}
				else {
					it++;
				}
			}
			accUpdate.clear();
			previousReport = previousReport.patch(diff);
			if ((rc = (awsMQTT->shadow_update(previousReport.dump())) != 0)) {
				cout << "Failed to update shadow rc: " << rc << endl;
				sendError("Connection to AWS possibly lost", false);
				sendLastUpdate = true;
			}
			else {
				prevShadow = currTime;
				prevFullShadow = currTime;
				cout << "Send shadow update:" << endl;
				cout << previousReport.dump() << endl;
				lastUpdate = previousReport;
				JsonUtil::writeJson(last_update_json_filepath, lastUpdate);
				previousReport = previousReport.patch(diff);
				JsonUtil::writeJson(previous_json_filepath, previousReport);
			}
		}

		// -- Check if it's time to send shadow update
		if (awsMQTT->isConnected() && (currTime - prevShadow >= shadow_periodic_report_interval * reportScale) && accUpdate.size() > 0) {
			// check if there's difference between update and previous report
			changeJsonAppName_state(accUpdate);
			json diff = json::diff(previousReport, accUpdate);
			json updateJson;
			for (auto it = diff.begin(); it != diff.end(); ) {
				string op = (*it)["op"];
				if (op.compare("replace") == 0 || op.compare("add") == 0) {
					// Create update json
					JsonUtil::addPatchToJson(*it, updateJson);
					it++;
				}
				else {
					diff.erase(it);
				}
			}
			accUpdate.clear();
			if (updateJson.size() > 0 && !updateJson.value("/state/reported"_json_pointer, json()).is_null()) {
				if ((rc = (awsMQTT->shadow_update(updateJson.dump())) != 0)) {
					cout << "Failed to update shadow rc: " << rc << endl;
					sendError("Connection to AWS possibly lost", false);
					sendLastUpdate = true;
				}
				else {
					prevShadow = currTime;
					cout << "Send shadow update:" << endl;
					cout << updateJson.dump() << endl;
					lastUpdate = updateJson;
					JsonUtil::writeJson(last_update_json_filepath, lastUpdate);
					previousReport = previousReport.patch(diff);
					JsonUtil::writeJson(previous_json_filepath, previousReport);
				}
			}
			else {
				cout << "Nothing to update" << endl;
				prevShadow = currTime;
			}
		}

		// -- Check if it's time to send data
		data_lock.lock();
		if (currTime - prevData >= minimum_publish_interval && data_queue.size() > 0) {
			while (data_queue.size() > 0) {
				json temp = data_queue.front();
				if (awsMQTT->isConnected()) {
					string topic;
					for (auto it = temp.begin(); it != temp.end(); it++) {
						cout << "publish to: " << it.key() << endl;
						if ((rc = (awsMQTT->pub(it.key(), it.value().dump(), 1)) != 0)) {
							break;
						}
						else {

						}
					}
					if (rc != 0) {

					}
					else {
						data_queue.pop();
					}
				}
				else {
					cout << "save data channel message in redis" << endl;
					// Save to redis if connection to AWS is not available
					checkDatabase = true;
					rwrapper->connect();
					string combine;
					for (auto it = temp.begin(); it != temp.end(); it++) {
						combine = it.key() + separate + it.value().dump();
						rwrapper->list_rpush("datachannel", combine);
					}
					rwrapper->disconnect();
					data_queue.pop();
				}
			}
			prevData = currTime;
		}
		data_lock.unlock();

		updateAppState();

		// -- Send heartbeat
		if (currTime - prevHeartbeat >= heartbeat_interval) {
			prevHeartbeat = currTime;
			heartbeat(heartbeat_interval * 2);
		}

		// -- Attempt reconnecting to AWS
		if (!awsMQTT->isConnected() && currTime - prevReconnect > 30) {
			awsMQTT->reconnect();
			prevReconnect = currTime;
		}
		usleep(1000 * 10);
	}

	SaveSetting();
}

// Callback for control topics
// process a command
void OmniSensr_AWS_IoT_Connector::processCommand(const std::string& cmd){
	cout << "control message: " << cmd << endl;

}

unsigned int OmniSensr_AWS_IoT_Connector::initConfig(ParseConfigTxt& parser){
	unsigned int n_params = 0;

	// get variables from config file
	n_params += parser.getValue("AWS_IOT_MODULE_PERIODIC_SHADOW_REPORT_INTERVAL", shadow_periodic_report_interval);
	n_params += parser.getValue("AWS_IOT_MODULE_MIN_PUBLISH_INTERVAL", minimum_publish_interval);
	n_params += parser.getValue("AWS_IOT_MQTT_COMMAND_TIMEOUT", mqtt_command_timeout);
	n_params += parser.getValue("AWS_IOT_TLS_HANDSHAKE_TIMEOUT", tls_handshake_timeout);
	n_params += parser.getValue("AWS_IOT_MODULE_FULL_REPORT_INTERVAL", full_report_interval);
	n_params += parser.getValue("REFRESH_DESIRED_INTERVAL", refresh_desired_interval);
	n_params += parser.getValue("AWS_IOT_MQTT_HOST_TESTBED", aws_iot_mqtt_host_testbed);
	n_params += parser.getValue("AWS_IOT_MQTT_HOST_PRODUCTION", aws_iot_mqtt_host_production);
	n_params += parser.getValue("AWS_IOT_ROOT_CA_FILENAME", aws_iot_root_ca_filename);
	n_params += parser.getValue("AWS_IOT_CERTIFICATE_FILENAME", aws_iot_certificate_filename);
	n_params += parser.getValue("AWS_IOT_PRIVATE_KEY_FILENAME", aws_iot_private_key_filename);
	n_params += parser.getValue("AWS_IOT_CERT_DIRECTORY", aws_iot_cert_directory);
	n_params += parser.getValue("AWS_IOT_RESET_THINGSHADOW_ON_STARTING", aws_iot_reset_thingshadow_on_start);
	n_params += parser.getValue("HEARTBEAT_INTERVAL", heartbeat_interval);

	// Register variables to send
	registerVariable("sync", insync);
	registerVariable("Heartbeat_Intrvl", heartbeat_interval);

	registerVariable("AwsIoT_Intrvl", shadow_periodic_report_interval);
	registerVariable("FullReport_Intrvl", full_report_interval);
	registerVariable("RefreshDesired_Intrvl", refresh_desired_interval);
	registerVariable("RealTimeReport", realTimeReport);

	return n_params;
}

// Limit the number of digits of floating point number
double OmniSensr_AWS_IoT_Connector::trimDigits(float input) {
	return round(input * 10000) / 10000.0;
}

// Update appState json object with variables
void OmniSensr_AWS_IoT_Connector::updateAppState() {
	state_lock.lock();
	appState["AwsIoT_Intrvl"] = shadow_periodic_report_interval;
	appState["FullReport_Intrvl"] = full_report_interval;
	appState["RealTimeReport"] = realTimeReport;
	appState["RefreshDesired_Intrvl"] = refresh_desired_interval;
	appState["sync"] = insync;
	appState["Heartbeat_Intrvl"] = heartbeat_interval;
	state_lock.unlock();
}

void OmniSensr_AWS_IoT_Connector::saveState(std::ostream& os) const {

}

void OmniSensr_AWS_IoT_Connector::loadTestbedList(string filepath) {
	FILE *fp_list;
	fp_list = fopen(filepath.c_str(), "r");
	if(fp_list == NULL) {
		fprintf(stderr, "Error opening AWS_IOT_CONNECTOR_TESTBED_LIST_FILE file\n");
		//exit(0);
	} else {
		char buffer[255];
		// -- Read from remote broker and publish to local broker
		string lineInput;
		vector<string> measBuffer;
		while(fgets(buffer, 255, fp_list)) {	// -- Reads a line from the pipe and save it to buffer
			lineInput.clear();
			lineInput.append(buffer);

			// -- If there's any comment, then discard the rest of the line
			std::size_t found = lineInput.find_first_of("#");
			// -- If '#' is found, then discard the rest.
			if(found != string::npos)
				lineInput.resize(found);

			std::istringstream iss (lineInput);
			string testbed;
			iss >> testbed;
			if (testbed.compare(getStoreLocation()) == 0) {
				cout << "This is a testbed" << endl;
				isTestbed = true;
				break;
			}
		}
		fclose(fp_list);
	}
}

int main() {
	OmniSensr_AWS_IoT_Connector connectorApp("config/OmniSensr_AWS_IoT_Connector/AWS_IoT_Connector_config.txt");
	connectorApp.loadTestbedList("config/OmniSensr_AWS_IoT_Connector/testbed_list.txt");
	connectorApp.Run();

	// will never get here
	return 0;
}


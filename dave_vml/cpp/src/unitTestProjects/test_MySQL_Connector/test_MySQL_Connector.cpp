/*
 * test_MySQL_Connector.cpp
 *
 *  Created on: Nov 28, 2016
 *      Author: paulshin
 */







/* Copyright 2008, 2010, Oracle and/or its affiliates. All rights reserved.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

There are special exceptions to the terms and conditions of the GPL
as it is applied to this software. View the full text of the
exception in file EXCEPTIONS-CONNECTOR-C++ in the directory of this
software distribution.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/* Standard C++ includes */
#include <stdlib.h>
#include <iostream>

/*
  Include directly the different
  headers from cppconn/ and mysql_driver.h + mysql_util.h
  (and mysql_connection.h). This will reduce your build time!
*/
#include "mysql_connection.h"

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>

using namespace std;

int main(void)
{
cout << endl;
cout << "Running 'SELECT * FROM carealot_001.omni005_wifi_data'..." << endl;

try {
  sql::Driver *driver;
  sql::Connection *con;
  sql::Statement *stmt;
  sql::ResultSet *res;

  /* Create a connection */
  driver = get_driver_instance();
  con = driver->connect("tcp://production-1.cluster-cetyenwapymy.us-east-1.rds.amazonaws.com:3306", "omniadmin", "videoMININGLabs2016");

    /* Connect to the MySQL test database */
    con->setSchema("carealot_001");

    stmt = con->createStatement();
    res = stmt->executeQuery("SELECT * FROM carealot_001.omni005_wifi_data");

    /*
     * 			jsonStr << "{"
						<< "\"dev_id\":\"" << dev->GetDevAddr() << "\","
						<< "\"start_time\":" << timeMeas_start << ","
						<< "\"A4\":\"" << traj.str() << "\","
						<< "\"cnt\":" << dev->GetTrackedPosBufSize() << ","
						<< "\"duration\":" << tripDuration
					<< "}";
     */

  // -- Show the first or all rows
  res->next();
  //while (res->next())
  {
    cout << "...";
    cout << "(start_time, dev_id, cnt, duration) = (" << (double)res->getDouble("start_time") << ", " << res->getString("dev_id") << ", " <<  res->getUInt("cnt")
    				<< ", " << res->getDouble("duration") << "): A4 = " << res->getString("A4") << endl << endl;
  }
  delete res;
  delete stmt;
  delete con;

} catch (sql::SQLException &e) {
  cout << "# ERR: SQLException in " << __FILE__;
  cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
  cout << "# ERR: " << e.what();
  cout << " (MySQL error code: " << e.getErrorCode();
  cout << ", SQLState: " << e.getSQLState() << " )" << endl;
}

cout << endl;

return EXIT_SUCCESS;
}

/*
 * test_Astar.cpp
 *
 *  Created on: Dec 8, 2016
 *      Author: paulshin
 */


#include "../test_Astar/implementation.cpp"

template<typename Graph>
void breadth_first_search(Graph graph, typename Graph::Location start) {
  typedef typename Graph::Location Location;
  queue<Location> frontier;
  frontier.push(start);

  unordered_set<Location> visited;
  visited.insert(start);

  while (!frontier.empty()) {
    auto current = frontier.front();
    frontier.pop();

    std::cout << "Visiting " << current << std::endl;
    for (auto next : graph.neighbors(current)) {
      if (!visited.count(next)) {
        frontier.push(next);
        visited.insert(next);
      }
    }
  }
}


int main() {

  GridWithWeights grid = make_diagram4();

  SquareGrid::Location start{1, 4};
  SquareGrid::Location goal{8, 5};

  unordered_map<SquareGrid::Location, SquareGrid::Location> came_from;
  unordered_map<SquareGrid::Location, double> cost_so_far;

  a_star_search(grid, start, goal, came_from, cost_so_far);

  draw_grid(grid, 2, nullptr, &came_from);
  std::cout << std::endl;

  draw_grid(grid, 3, &cost_so_far, nullptr);
  std::cout << std::endl;

  vector<SquareGrid::Location> path = reconstruct_path(start, goal, came_from);
  draw_grid(grid, 3, nullptr, nullptr, &path);


  //breadth_first_search(example_graph, 'A');
}


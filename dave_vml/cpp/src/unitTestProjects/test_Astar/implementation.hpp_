/*
 * implementation.hpp
 *
 *  Created on: Dec 8, 2016
 *      Author: paulshin
 */

#ifndef SRC_UNITTESTPROJECTS_TEST_ASTAR_IMPLEMENTATION_HPP__
#define SRC_UNITTESTPROJECTS_TEST_ASTAR_IMPLEMENTATION_HPP__

#include <iostream>
#include <iomanip>
#include <unordered_map>
#include <unordered_set>
#include <array>
#include <vector>
#include <utility>
#include <queue>
#include <tuple>
#include <algorithm>

using std::unordered_map;
using std::unordered_set;
using std::array;
using std::vector;
using std::queue;
using std::priority_queue;
using std::pair;
using std::tuple;
using std::tie;
using std::string;

using namespace std;


struct SquareGrid {
  typedef tuple<int,int> Location;
  static array<Location, 4> DIRS;

  int width, height;
  unordered_set<Location> walls;

  SquareGrid(int width_, int height_)
     : width(width_), height(height_) {}

  inline bool in_bounds(Location id) const {
    int x, y;
    tie (x, y) = id;
    return 0 <= x && x < width && 0 <= y && y < height;
  }

  inline bool passable(Location id) const {
    return !walls.count(id);
  }

  vector<Location> neighbors(Location id) const {
    int x, y, dx, dy;
    tie (x, y) = id;
    vector<Location> results;

    for (auto dir : DIRS) {
      tie (dx, dy) = dir;
      Location next(x + dx, y + dy);
      if (in_bounds(next) && passable(next)) {
        results.push_back(next);
      }
    }

    if ((x + y) % 2 == 0) {
      // aesthetic improvement on square grids
      std::reverse(results.begin(), results.end());
    }

    return results;
  }
};

// -- Four directions to explore
array<SquareGrid::Location, 4> SquareGrid::DIRS {Location{1, 0}, Location{0, -1}, Location{-1, 0}, Location{0, 1}};

struct GridWithWeights: SquareGrid {
  unordered_set<Location> forests;
  GridWithWeights(int w, int h): SquareGrid(w, h) {}
  double cost(Location from_node, Location to_node) const {

	// -- Define and return the movement cost in the forest
    return forests.count(to_node) ? 5 : 1;
  }
};

template<typename T, typename priority_t>
struct PriorityQueue {
  typedef pair<priority_t, T> PQElement;
  priority_queue<PQElement, vector<PQElement>,
                 std::greater<PQElement>> elements;

  inline bool empty() const { return elements.empty(); }

  inline void put(T item, priority_t priority) {
    elements.emplace(priority, item);
  }

  inline T get() {
    T best_item = elements.top().second;
    elements.pop();
    return best_item;
  }
};



class Astar
{
public:
	Astar();
    virtual ~Astar();

    template<class Graph>
    void draw_grid(const Graph& graph, int field_width,
                   unordered_map<typename Graph::Location, double>* distances=nullptr,
                   unordered_map<typename Graph::Location, typename Graph::Location>* point_to=nullptr,
                   vector<typename Graph::Location>* path=nullptr);


    template<typename Location>
    vector<Location> reconstruct_path(
       Location start,
       Location goal,
       unordered_map<Location, Location>& came_from
    ) ;

    inline double heuristic(SquareGrid::Location a, SquareGrid::Location b);
    void add_rect(SquareGrid& grid, int x1, int y1, int x2, int y2);

    template<typename Graph>
    void a_star_search
      (const Graph& graph,
       typename Graph::Location start,
       typename Graph::Location goal,
       unordered_map<typename Graph::Location, typename Graph::Location>& came_from,
       unordered_map<typename Graph::Location, double>& cost_so_far);

    GridWithWeights make_diagram1();
    GridWithWeights make_diagram4();


};



#endif /* SRC_UNITTESTPROJECTS_TEST_ASTAR_IMPLEMENTATION_HPP__ */

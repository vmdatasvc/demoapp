/*
 * test_ParseConfigTxt.cpp
 *
 *  Created on: May 18, 2016
 *      Author: paulshin
 */

#include "modules/utility/ParseConfigTxt.hpp"

using namespace std;

#define APP_CONFIG_FILE				"config/OmniSensr_SampleApp/OmniSensr_SampleApp_config.txt"
#define IOT_GATEWAY_CONFIG_FILE		"config/IoT_Gateway/IoT_Gateway_config.txt"

int main() {

	ParseConfigTxt config;

	config.parse(APP_CONFIG_FILE);

	printf("paramSize = %d\n", config.paramSize());

	bool isFound;

	int buffer_interval;
	isFound = config.getValue_int("PERIODIC_BUFFER_PROCESSING_INTERVAL", buffer_interval);
	if(isFound)
		printf("PERIODIC_BUFFER_PROCESSING_INTERVAL = %d\n", buffer_interval);

	string buffer_interval_str;
	isFound = config.getValue_string("PERIODIC_BUFFER_PROCESSING_INTERVAL", buffer_interval_str);
	if(isFound)
		printf("PERIODIC_BUFFER_PROCESSING_INTERVAL_str = %s\n", buffer_interval_str.c_str());

	float version;
	isFound = config.getValue_float("VERSION", version);
	if(isFound)
		printf("VERSION = %f\n", version);


	return 0;
}

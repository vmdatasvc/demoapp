//============================================================================
// Name        : test_JsonByNlohmann.cpp
// Author      : Paul J. Shin
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdio>
#include <cstdlib>
#include <iosfwd>

#include <algorithm>    // For std::remove()

// -- Refer to https://github.com/nlohmann/json
#include "json.hpp"

using json = nlohmann::json;
using namespace std;

int main() {

	std::ifstream ifs("config/test_JsonByNlohmann/test3.json");
	json j(ifs);

//	std::cout << setw(4) << j; cout << endl;
//	std::cout << j.dump(4); cout << std::endl;

//	// special iterator member functions for objects
//	for (json::iterator it = j.begin(); it != j.end(); ++it) {
//		string key = it.key();
//		ostringstream value;
//		value << it.value();
//	  std::cout << key << " : " << value.str() << "\n";
//	}

	json::iterator it = j.find("glossary");
	std::cout << it.key() << " : " << it.value() << "\n";

	json j_f = j.flatten();

	// special iterator member functions for objects
	for (json::iterator it = j_f.begin(); it != j_f.end(); ++it) {
		string key = it.key();
		ostringstream value;
		value << it.value();
	  std::cout << key << " : " << value.str() << "\n";
	}

	json::iterator it1 = j_f.find("/glossary/GlossDiv/GlossList/GlossEntry/float");
	cout << "it1 is found? " << (it1 != j_f.end()) << endl;
	it1.value() = 4568.99;
//	j_f["/glossary/GlossDiv/GlossList/GlossEntry/float"] = 4568.99;
	std::cout << it.key() << " : " << it1.value() << "\n";

	float value_f = j["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["float"].get<float>();
	std::cout << it.key() << " : " << value_f << "\n";

	j["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["float"] = 987.8888;
	value_f = j["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["float"].get<float>();
	std::cout << it.key() << " : " << value_f << "\n";

	json j_uf = j_f.unflatten();


	// special iterator member functions for objects
	 std::cout << std::setw(4) << j << '\n';

	 std::cout << std::setw(4) << j_uf << '\n';

	// create object from string literal
//	json j1 = "{ \"happy\": true, \"pi\": 3.141 }"_json;
//
//	// or even nicer (thanks http://isocpp.org/blog/2015/01/json-for-modern-cpp)
//	auto j2 = R"(
//	  {
//	    "happy": true,
//	    "pi": 3.141
//	  }
//	)"_json;
//
//	// or explicitly
//	auto j3 = json::parse("{ \"happy\": true, \"pi\": 3.141 }");
//
//	// explicit conversion to string
//	std::string s = j1.dump();    // {\"happy\":true,\"pi\":3.141}
//
//	// serialization with pretty printing
//	// pass in the amount of spaces to indent
//	std::cout << j1.dump(4) << std::endl;
//	// {
//	//     "happy": true,
//	//     "pi": 3.141
//	// }
//
//	// the setw manipulator was overloaded to set the indentation for pretty printing
//	cout << setw(4) << j1 << endl;

	return 0;
}

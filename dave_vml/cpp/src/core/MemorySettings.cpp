/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

// Disable MSVC warning
#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

//#include "legacy/pv/PvUtil.hpp"
#include <core/MemorySettings.hpp>
//#include "RegistrySettings.hpp"
#include <core/Settings.hpp>
#include <core/Error.hpp>
#include <modules/utility/File.hpp>

#include <modules/utility/strutil.hpp>

#include <unistd.h>

namespace vml
{

/**
 * Settings handler class.
 */

SettingsHandler::SettingsHandler(const std::string& crntPath,std::vector<NodePtr> nodeStack)
{
  VML_ASSERT(!nodeStack.empty());
  mCrntPath = crntPath;
  mNodeStack = nodeStack;
  mRootNode = mNodeStack[0];
}

SettingsHandler::~SettingsHandler(void)
{
  clear();
}
  
void
SettingsHandler::clear(void)
{
  mTagStack.clear();
}
  
NodePtr
SettingsHandler::getData()
{
  return mRootNode;
}

std::string
SettingsHandler::topTag(void)
{
  std::string rv;
  int sz = mTagStack.size();
  
  if ( sz > 0 )
  {
    rv = mTagStack[sz-1];
  }
  
  return rv;
}

void
SettingsHandler::start(const std::string& ns, const std::string& tag, const char **attr)
{
  mTagStack.push_back(tag);

  std::string label;      // the tag's label
  std::string value;      // the value (if it's a parameter)
  bool isShared = false;  // is the value shared? (if it's a parameter)
  bool allUsers = false;

  std::string normPath;
  std::vector<std::string> strStack;

  if (tag == "folder")
  {
    for (int i = 0; attr[i] != 0; i+=2)
    {
      if (!strcmp(attr[i],"name"))
      {
        // Normalize the path.
        normPath = normalize_path(attr[i+1]);
        // Split the path into a stack of strings.
        strStack = split(normPath,"/");
      }
    }
    
    // Number of nodes that will need to be popped off the mNodeStack.
    mPopOff.push_back(strStack.size());

    std::vector<std::string>::iterator iStr;
    for (iStr = strStack.begin(); iStr != strStack.end(); iStr++)
    {
      // Handle all the folders.
      tagFolder((*iStr));
    }
  }
  else if (tag == "macro")
  {
    for (int i = 0; attr[i] != 0; i+=2)
    {
      if (!strcmp(attr[i],"name"))
      {
        // Normalize the path.
        normPath = normalize_path(attr[i+1]);
        // Split the path into a stack of strings.
        strStack = split(normPath,"/");
      }
      else if (!strcmp(attr[i],"value"))
      {
        value = attr[i+1];
      }
    }

    // Number of nodes that will need to be popped off the mNodeStack.
    mPopOff.push_back(strStack.size());

    label = strStack.back();  // The macro is the top of the stack.
    strStack.pop_back();      // Pop the macro off the stack.

    std::vector<std::string>::iterator iStr;
    for (iStr = strStack.begin(); iStr != strStack.end(); iStr++)
    {
      // Handle all the folders first.
      tagFolder((*iStr));
    }

    // Handle the macro now.
    tagMacro(label,value);
  }
  else if (tag == "param")
  {
    for (int i = 0; attr[i] != 0; i+=2)
    {
      if (!strcmp(attr[i],"name"))
      {
        // Normalize the path.
        normPath = normalize_path(attr[i+1]);
        // Split the path into a stack of strings.
        strStack = split(normPath,"/");
      }
      else if (!strcmp(attr[i],"value"))
      {
        value = attr[i+1];
      }
      else if (!strcmp(attr[i],"shared"))
      {
        if (!strcmp(attr[i+1],"1"))
        {
          isShared = true;
        }
        else if (!strcmp(attr[i+1],"0"))
        {
          isShared = false;
        }
      }
      else if (!strcmp(attr[i],"all_users"))
      {
        if (!strcmp(attr[i+1],"1"))
        {
          allUsers = true;
        }
        else if (!strcmp(attr[i+1],"0"))
        {
          allUsers = false;
        }
      }
    }
    
    // Number of nodes that will need to be popped off the mNodeStack.
    mPopOff.push_back(strStack.size());

    label = strStack.back();  // The parameter is the top of the stack.
    strStack.pop_back();      // Pop the parameter off the stack.

    std::vector<std::string>::iterator iStr;
    for (iStr = strStack.begin(); iStr != strStack.end(); iStr++)
    {
      // Handle all the folders first.
      tagFolder((*iStr));
    }

    // Handle the parameter now.
    tagParam(label,value,isShared,allUsers);
  }
  else if (tag == "include")
  {
    // Handle the include tag.
    tagInclude(attr);
  }
}
  
void
SettingsHandler::tagFolder(const std::string& label)
{ 
  // Look for the node in the parent node's subnodes.
  NodePtr pNode = findNode(mNodeStack.back(),label);
  
  if (pNode.get() == NULL)
  {
    // Create the node.
    pNode.reset(new Node);
    pNode->nodeType = NodeStruct::FOLDER;
    pNode->label = label;
    pNode->value = "";
    pNode->hasTemplate = false;
    pNode->isShared = false;
    pNode->allUsers = false;

    // Associate this node with its parent.
    mNodeStack.back()->subNodes.push_back(pNode);
  }

  // Add the node pointer to the stack.
  mNodeStack.push_back(pNode);
}

void
SettingsHandler::tagMacro(const std::string& label, const std::string& value)
{
  // Look for the node in the parent node's subnodes.
  NodePtr pNode = findNode(mNodeStack.back(),label);

  if (pNode.get() == NULL)
  {
    // Create the node.
    pNode.reset(new Node);
    pNode->nodeType = NodeStruct::MACRO;
    pNode->label = label;
    pNode->value = value;
    pNode->hasTemplate = false;
    pNode->isShared = false;
    pNode->allUsers = false;

    // Associate this node with its parent.
    mNodeStack.back()->subNodes.push_back(pNode);
  }

  // Add the node pointer to the stack.
  mNodeStack.push_back(pNode);
}

void
SettingsHandler::tagParam(const std::string& label, const std::string& value, bool isShared, bool allUsers)
{
  // Look for the node in the parent node's subnodes.
  NodePtr pNode = findNode(mNodeStack.back(),label);

  if (pNode.get() == NULL)
  {
    // Create the node.
    pNode.reset(new Node);
    pNode->nodeType = NodeStruct::PARAM;
    pNode->label = label;
    pNode->value = value;
    pNode->hasTemplate = false;
    pNode->isShared = isShared;
    pNode->allUsers = allUsers;

    // Associate this node with its parent.
    mNodeStack.back()->subNodes.push_back(pNode);
    
    // Write the settings to registry if shared.
    if (pNode->isShared)
    {
#ifdef WIN32
      // Create the path.
      std::string path = pathFromStack() + label;
      
      // Write the setting to the registry if they don't exist
      RegistrySettings reg;
      pNode->value = reg.getString(path,value,true,!(pNode->allUsers));
#endif
    }
  }
  else
  {
    if (!(pNode->isShared && isShared))
    {
      if (isShared)
      {
#ifdef WIN32
        // Create path.
        std::string path = pathFromStack() + label;
        
        // Write it to registry.
        RegistrySettings reg;
        pNode->value = reg.getString(path,value,true,!(pNode->allUsers));
#endif
      }
      else
      {
        pNode->value = value;
      }

      pNode->isShared = isShared;
    }
  }

  // Add the node pointer to the stack.
  mNodeStack.push_back(pNode);
}

void
SettingsHandler::tagInclude(const char **attr)
{
  for (int i = 0; attr[i] != 0; i+=2)
  {
    if (!strcmp(attr[i],"file"))
    {
      std::string attrPath = MemorySettings::instance()->replaceVars(attr[i+1],0,mNodeStack);
      // Split the file from the path.
      std::vector<std::string> path = split_path(attrPath);
      
      // Create new path from current path and included path.
      std::string newPath = combine_path(mCrntPath,path[0]);

      // Create full path of file to be included.
      std::string fullPath = combine_path(mCrntPath,attrPath);

      // Process xml string.
      XmlParser parser;

      SettingsHandler includeHand(newPath,mNodeStack);
      parser.registerNamespaceHandler("",&includeHand);
      if (!File::fileExists(fullPath.c_str()))
      {
//        PvUtil::exitError("File not found [%s]",fullPath.c_str());
    	  VML_ERROR(MsgCode::IOErr, "File not found" << fullPath << std::endl);
      }
      if (!parser.parseFile(fullPath))
      {
//        PvUtil::exitError("Error parsing Settings XML file [%s]",fullPath.c_str());
    	  VML_ERROR(MsgCode::methodFailedErr,"Error parsing Settings XML file " << fullPath << std::endl);
      }
      
      if (!parser.isWellFormed())
      {
        // Write in error log.
      }
    }
  }
}

void
SettingsHandler::cdata(const XML_Char *s, int len)
{
  // Do nothing.
}
  
void
SettingsHandler::end(const std::string& ns, const std::string& tag)
{
  if (tag == "folder" || tag == "macro" || tag == "param")
  {
    for (int i = 0; i < mPopOff.back(); i++)
    {
      mNodeStack.pop_back();
    }

    mPopOff.pop_back();
  }

  mTagStack.pop_back();
}

NodePtr
SettingsHandler::findNode(NodePtr pLookNode, std::string label)
{
  NodePtr pNode;
  
  std::vector<NodePtr>::iterator iNodePtr;
  for (iNodePtr = pLookNode->subNodes.begin();
       iNodePtr != pLookNode->subNodes.end() && !pNode.get();
       ++iNodePtr)
  {
    // Look for node in the subnodes.
    if ((*iNodePtr)->label == label)
    {
      pNode = (*iNodePtr);
    }
  }
  
  return pNode;
}

std::string
SettingsHandler::pathFromStack()
{
  std::string path = "";

  // Create path.
  std::vector<NodePtr>::iterator iNodePtr;
  for (iNodePtr = mNodeStack.begin();
       iNodePtr != mNodeStack.end();
       ++iNodePtr)
  {
    // Don't use the root node.
    if ((*iNodePtr)->label != "rootNode")
    {
      path = path + (*iNodePtr)->label + '\\';
    }
  }

  return path;
}

/**
 * MemorySettings class
 */

boost::shared_ptr<MemorySettings> MemorySettings::mpInstance;

MemorySettings::MemorySettings()
{
  mRootNode.reset(new Node());
  mRootNode->label = "rootNode";
}

MemorySettings::~MemorySettings()
{
}

boost::shared_ptr<MemorySettings>
MemorySettings::instance()
{
  if (mpInstance.get() == 0)
  {
    // Create sole instance.
    mpInstance.reset(new MemorySettings);
  }

  // Return address of sole instance.
  return mpInstance;
}

//
// OPERATIONS
//

std::string
MemorySettings::getString(const std::string& key, 
                          const std::string& defaultValue, 
                          bool writeDefault,
                          bool throwIfNotFound)
{
  // Mutex.
  boost::mutex::scoped_lock lk(mMutex);

  std::string str;

  if (mRootNode.get() != 0)
  {
    std::vector<NodePtr> stack;
    NodePtr pNode;
    
    // Find the setting.
    pathToStack(key,stack);
    if (stack.size() > 1)
    {
      pNode = stack.back();
    }
    
    // Is the setting valid.
    if (pNode.get() != NULL)
    {
      if (pNode->isShared)
      {
#ifdef WIN32
        // Get the value from the registry.
        RegistrySettings reg("");
        str = reg.getString(key,defaultValue,writeDefault,!(pNode->allUsers));
        
        // Set the memory value.
        pNode->value = str;
#endif
      }
      else
      {
        // If no value is there.
        if (pNode->value == "")
        {
          // Set value to default.
          if (writeDefault)
          {
            pNode->value = defaultValue;
          }
        }
        
        // Get the value from memory.
        str = replaceVars(pNode->value,0,stack);
      }
    }
    else
    {
      if (throwIfNotFound)
      {
        std::string errorMessage = "Setting not found: [" + key + "]";
        throw Settings::SettingNotFoundException(errorMessage);
      }

      // Add the node to memory.
      if (writeDefault)
      {
        addNode(key,defaultValue);
      }
      str = replaceVars(defaultValue,0,stack);
    }
  }
  else
  {
#ifdef WIN32
    // Memory is empty so just use the registry.
    RegistrySettings reg("");
    str = reg.getString(key,defaultValue,writeDefault);
#endif
  }
  
  return str;
}

std::string
MemorySettings::getString(const std::string& key, 
                          const std::string& defaultValue, 
                          bool writeDefault)
{
  return getString(key,defaultValue,writeDefault,false);
}

std::string
MemorySettings::getString(const std::string& key)
{
  return getString(key,std::string(),false,true);
}

void
MemorySettings::setString(const std::string& key,
                          const std::string& val)
{
  // Mutex.
  boost::mutex::scoped_lock lk(mMutex);

  if (mRootNode.get() != 0)
  {
    std::vector<NodePtr> stack;
    NodePtr pNode;
    
    // Find the setting.
    pathToStack(key,stack);
    if (stack.size() > 0)
    {
      pNode = stack.back();
    }
    
    // Is the setting valid.
    if (pNode.get() != NULL)
    {
      if (pNode->isShared)
      { 
#ifdef WIN32
        // Set the value in the registry.
        RegistrySettings reg("");
        reg.setString(key,val,!(pNode->allUsers));
#endif
      }
      
      // Set the value in memory.
      pNode->value = val;
    }
    else
    {
      addNode(key,val);
    }
  }
  else
  {
#ifdef WIN32
    // Memory is empty so just use the registry.
    RegistrySettings reg("");
    reg.setString(key,val);
#endif
  }
}

void
MemorySettings::deleteValue(const std::string& key)
{
  // Mutex.
  boost::mutex::scoped_lock lk(mMutex);

  if (mRootNode.get() != 0)
  {
    std::vector<NodePtr> stack;
    NodePtr pNode;
    NodePtr pParentNode;
    
    // Find the setting.
    pathToStack(key,stack);
    if (stack.size() >= 2)
    {
      pNode = stack.back();
      pParentNode = stack[stack.size()-2];
    }
    
    // Is the setting valid.
    if (pNode.get() != NULL)
    {
      if (pNode->isShared)
      { 
#ifdef WIN32
        // Set the value in the registry.
        RegistrySettings reg("");
        reg.setString(key,"",!(pNode->allUsers));
#endif
      }
      else
      {
        std::vector<NodePtr>::iterator iNode;
        for (iNode = pParentNode->subNodes.begin(); iNode != pParentNode->subNodes.end(); ++iNode)
        {
          if (*iNode == pNode)
          {
            pParentNode->subNodes.erase(iNode);
            break;
          }
        }
      }
    }
    else
    {
      std::string errorMessage = "Setting not found: [" + key + "]";
      throw Settings::SettingNotFoundException(errorMessage);
    }
  }
}

std::string
MemorySettings::getMacroValue(const std::string& macroName, std::vector<NodePtr>& stack)
{
  if (stack.empty())
  {
	  return "";
  }

  // The node was not found yet.
  bool found = false;

  // The value of the macro.
  std::string macroValue = "";

  std::vector<NodePtr>::reverse_iterator iNodePtr1;
  for (iNodePtr1 = stack.rbegin(); iNodePtr1 != stack.rend(); iNodePtr1++)
  {
    NodePtr pNode = (*iNodePtr1);

    std::vector<NodePtr>::iterator iNodePtr2;
    for (iNodePtr2 = pNode->subNodes.begin(); iNodePtr2 != pNode->subNodes.end(); iNodePtr2++)
    {
      if ((*iNodePtr2)->nodeType == NodeStruct::MACRO && (*iNodePtr2)->label == macroName)
      {
        // We found the macro.
        found = true;
        // Assign the macro value.
        macroValue = (*iNodePtr2)->value;
        // Break out of the loop.
        break;
      }
    }

    if (found)
    {
      break;
    }
  }
  
  return macroValue;
}

void
MemorySettings::pathToStack(const std::string& path, std::vector<NodePtr>& stack)
{
    // First normalize the path.
  std::string normPath = normalize_path(path);
  std::vector<std::string> appPathStack = split(normPath,"/");

  stack.clear();

  if (mRootNode.get() == NULL)
  {
	  return;
  }

  NodePtr pNode = mRootNode;
  stack.push_back(pNode);

  if (path == "")
  {
    return;
  }

  bool isFound = false;
  bool endSearch = false;
  std::vector<std::string>::iterator iStr;
  std::vector<NodePtr>::iterator iNodePtr;
  for (iStr = appPathStack.begin(); iStr != appPathStack.end() && !endSearch; iStr++)
  {
    for (iNodePtr = pNode->subNodes.begin();
         iNodePtr != pNode->subNodes.end();
         ++iNodePtr)
    {
      if ((*iStr) == (*iNodePtr)->label)
      {
        isFound = true;
        pNode = (*iNodePtr);
        stack.push_back(pNode);
        break;
      }
    }

    if (isFound)
    {
      isFound = false;
    }
    else
    {
      endSearch = true;
      pNode.reset();
      stack.clear();
    }
  }
}

void
MemorySettings::addNode(const std::string& keyPath, const std::string& value)
{
  // Create stack of the path for the key.
  std::string normPath = normalize_path(keyPath);
  std::vector<std::string> strStack = split(normPath,"/");
  
  // Counter is set to stack size.
  int cntStack = strStack.size();
  
  // Get pointer to outer most folder of mAppPath.
  NodePtr pNode = mRootNode;
  
  bool isSame = false;

  std::vector<std::string>::iterator iStr;
  std::vector<NodePtr>::iterator iNodePtr;
  for (iStr = strStack.begin(); iStr != strStack.end(); iStr++)
  {
    // Look in the pointer's subnodes.
    for (iNodePtr = pNode->subNodes.begin();
         iNodePtr != pNode->subNodes.end();
         ++iNodePtr)
    {
      if ((*iStr) == (*iNodePtr)->label)
      {
        isSame = true;
        pNode = (*iNodePtr);
        break;
      }
    }

    cntStack--;   // Decrement stack counter.

    // Did we find the node?
    if (isSame)
    {
      isSame = false;
    }
    else
    {
      // Create new node.
      NodePtr newNode(new Node);
      newNode->hasTemplate = false;
      newNode->isShared = false;
      newNode->allUsers = false;
      newNode->label = (*iStr);
      if (cntStack == 0)
      {
        // New node is a parameter.
        newNode->nodeType = NodeStruct::PARAM;
        newNode->value = value;
      }
      else
      {
        // New node is a folder.
        newNode->nodeType = NodeStruct::FOLDER;
        newNode->value = "";
      }
      
      // Associate this new node with its parent.
      pNode->subNodes.push_back(newNode);
      
      // Set current pointer to new node.
      pNode = newNode;
    }
  }
}

void
MemorySettings::setMemorySettings()
{
  // By default it will use memory settings now.
}


void
MemorySettings::parseXml(const std::string& fname /* = "settings.xml" */, bool merge)
{
  // Mutex.
  boost::mutex::scoped_lock lk(mMutex);

  // Process xml string.
  XmlParser parser;
  std::string crntPath = "";
  std::vector<NodePtr> initStack;
  if (!merge || !mRootNode.get())
  {
    mRootNode.reset(new Node());
    mRootNode->label = "rootNode";
  }
  initStack.push_back(mRootNode);
  
  SettingsHandler setHand(crntPath,initStack);
  
  parser.registerNamespaceHandler("",&setHand);

  // debugging
#define GetCurrentDir	getcwd

  char cCurrentPath[200];
  GetCurrentDir(cCurrentPath, sizeof(cCurrentPath));
  std::cout << cCurrentPath << std::endl;

  if (!File::fileExists(fname.c_str()))
  {
//    PvUtil::exitError("File not found [%s]",fname.c_str());
	  VML_ERROR(MsgCode::IOErr, "File not found " << fname << std::endl);
  }

  if (!parser.parseFile(fname))
  {
//    PvUtil::exitError("Error parsing Settings XML file [%s]",fname.c_str());
	  VML_ERROR(MsgCode::methodFailedErr, "Error parsing Settings XML file " << fname << std::endl);
  }

  if (!parser.isWellFormed())
  {
    // Write in error log.
  }
}

void
MemorySettings::parseMemoryXml(const std::string& xmlStr, bool merge)
{ 
  // Mutex.
  boost::mutex::scoped_lock lk(mMutex);

  // Process xml string.
  XmlParser parser;
  std::string crntPath = "";
  std::vector<NodePtr> emptyStack;
  if (!merge || !mRootNode.get())
  {
    mRootNode.reset(new Node());
    mRootNode->label = "rootNode";
  }
  emptyStack.push_back(mRootNode);
  SettingsHandler setHand(crntPath,emptyStack);
  
  parser.registerNamespaceHandler("",&setHand);
  parser.parse(const_cast<std::string&>(xmlStr));
  
  if (!parser.isWellFormed())
  {
    // Write in error log.
  }

  // Get the node structure.
  mRootNode = setHand.getData();
}

std::string
MemorySettings::replaceVars(const std::string& value, const std::string& currentPath)
{
  std::vector<NodePtr> nodePtrStack;
  pathToStack(currentPath,nodePtrStack);
  return replaceVars(value,0,nodePtrStack);
}

std::string
MemorySettings::replaceVars(const std::string& value, unsigned int startIndex, std::vector<NodePtr>& stack)
{
  size_t startToken = value.find("<",startIndex);

  if (startIndex < value.length())
  {
    size_t endToken = value.find(">",startIndex+1);

    if (endToken < value.length())
    {
      std::string macroName = value.substr(startToken+1,endToken-startToken-1);

      // First look for the macro in the macro tags.
      std::string macroValue = getMacroValue(macroName,stack);

      if (macroValue.empty())
      {
        // Didn't find the macro in the macro tags, look in the Installation folder.
        Settings s("Installation");

        macroValue = s.getString(macroName,"",false);

        if (macroValue.empty())
        {
          // Could not find the macro, continue searching for the next variable.
          return replaceVars(value,endToken,stack);
        }
      }

      // Substitute the obtained variable in the key name.
      std::string newVar = value.substr(0,startToken) + macroValue + value.substr(endToken+1);

      // Recursively resolve any other macros in the key.
      return replaceVars(newVar,startToken,stack);
    }
  }

  // If there are no macros, return the original string.
  return value;
}

void
MemorySettings::reset()
{
  mRootNode.reset();
}

void
MemorySettings::writeXml(const std::string& fname, bool resolveMacros)
{
  std::ofstream os(fname.c_str());
  writeXml(os,resolveMacros);
  os.close();
}

void
MemorySettings::writeXml(std::ostream& os, bool resolveMacros)
{
  // Mutex.
  boost::mutex::scoped_lock lk(mMutex);

  if (mRootNode.get())
  {
    // Print comments and settings open tag.
    os << "<?xml version=\"1.0\"?>\n";
    os << "<!--      XML output file       -->\n";
    os << "<!-- Produced by Settings class -->\n";
    os << "<settings version=\"1.0\">\n";
  
    // Print the node structure.
    std::vector<NodePtr> stack;
    stack.push_back(mRootNode);
    printXml(os,stack,resolveMacros);
    stack.pop_back();
  
    // Print settings close tag.
    os << "</settings>\n";
  }
}

static bool
nodePtrIsLesser(NodePtr pNode1, NodePtr pNode2)
{
  if (pNode1->nodeType == pNode2->nodeType)
  {
    return pNode1->label < pNode2->label;
  }

  return pNode1->nodeType < pNode2->nodeType;
}

void
MemorySettings::printXml(std::ostream& outs, std::vector<NodePtr>& stack, bool resolveMacros)
{
  NodePtr pNode = stack.back();

//  boost::mutex::scoped_lock lk(mMutex);	// this is recursion call. Nested mutex block will hang-up the process

  // Print the start tag.
  switch (pNode->nodeType)
  {
  case NodeStruct::FOLDER:
    {
      outs << "<folder name=\"" << xml_escape(pNode->label.c_str()) << "\">\n";
    }
    break;

  case NodeStruct::MACRO:
    {
      if (!resolveMacros)
      {
        outs << "<macro name=\"" << xml_escape(pNode->label.c_str()) << "\"";
        outs << " value=\"" << xml_escape(pNode->value.c_str()) << "\"/>\n";
      }
    }
    break;

  case NodeStruct::PARAM:
    {
      outs << "<param name=\"" << xml_escape(pNode->label.c_str()) << "\"";

      if (resolveMacros)
      {
        outs << " value=\"" << xml_escape(MemorySettings::replaceVars(pNode->value.c_str(),0,stack)) << "\"";
      }
      else
      {
        outs << " value=\"" << xml_escape(pNode->value.c_str()) << "\"";
      }
      if (pNode->isShared)
      {
        outs << " shared=\"1\"";
      }
      if (pNode->allUsers)
      {
        outs << " all_users=\"1\"";
      }
      outs << "/>\n";
    }
    break;
  default:
	// do nothing - should be undefined
	break;
  }
  
  std::vector<NodePtr> sortedNodes = pNode->subNodes;

  std::sort(sortedNodes.begin(),sortedNodes.end(),nodePtrIsLesser);

  std::vector<NodePtr>::iterator iNodePtr;
  for (iNodePtr = sortedNodes.begin();
       iNodePtr != sortedNodes.end();
       ++iNodePtr)
  {
    stack.push_back(*iNodePtr);
    // Call recursive print function.
    printXml(outs,stack,resolveMacros);
    stack.pop_back();
  }

  // Print the end tag.
  if (pNode->nodeType == NodeStruct::FOLDER)
  {
    outs << "</folder>\n";
  }
}

}; // namespace ait


/*
 * OmniSensrApp.cpp
 *
 *  Created on: Jan 10, 2017
 *      Author: jwallace
 */

#include "core/OmniSensrApp.hpp"

#include <vector>
#include <fstream>
#include <sstream>
#include <limits>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include <unistd.h>

using json = nlohmann::json;
using std::string;
using std::vector;
using std::ofstream;
using std::ostringstream;

using std::endl;
using std::cerr;

using boost::posix_time::ptime;


OmniSensrApp::OmniSensrApp(const string& nm, const string& path)
	: appName(nm), configPath(path), stateReportInterval(-1), IoT_Comm(nm) {

	// create the IOT gateway here

	// -- Register Delta State callback function
	IoT_Comm.RegisterDeltaCallback(boost::bind(&OmniSensrApp::deltaState_callback, this, _1));

	// -- Register Control Channel callback function
	IoT_Comm.RegisterControlCallback(boost::bind(&OmniSensrApp::control_callback, this, _1));

}

OmniSensrApp::~OmniSensrApp() {
	if(reportingThread){
		reportingThread->interrupt();

		delete reportingThread;
	}
}

// ==================== Public Functions ============================

void OmniSensrApp::Run() {

	// -- Read config file
	ParseSetting();

	// kick off thread for full state reporting
	reportingThread = new boost::thread(&OmniSensrApp::sendFullStateLoop, this);

	// and run the child code
	try{
		start();
	} catch (const std::exception& e){
		// send a fatal error with the message
		sendError(e.what());
	} catch (...){
		// send a fata error with a generic message
		sendError("Unknown exception caught, exiting");
	}

}

// ==================== Protected Functions =========================

bool OmniSensrApp::registerDeltaCallback(const std::string& variable, callback_t delta_fn){

	// attempt to lock the callback map
	bool ret = delta_cb_lock.try_lock();

	// only proceed if the callback map is locked
	if(ret){
		delta_callback_map.insert(callback_map_t::value_type(variable, delta_fn));
		// we must unlock manually
		delta_cb_lock.unlock();
	}

	return ret;
}

bool OmniSensrApp::unregisterDeltaCallback(const std::string& variable){

	// attempt to lock the callback map
	bool ret = delta_cb_lock.try_lock();

	// only proceed if the callback map is locked
	if(ret){
		delta_callback_map.erase(variable);
		// we must unlock manually
		delta_cb_lock.unlock();
	}

	return ret;
}


// send the state of a single variable
bool OmniSensrApp::sendState(const std::string& variable){
	// TODO: need a recursive lock around appState?
	state_lock.lock();
	json subset;
	subset[variable] = appState[variable];
	state_lock.unlock();

	bool ret = (subset[variable] != nullptr);

	if(ret){
		ret = sendPartialState(subset);
	}

	return ret;
}

bool OmniSensrApp::sendPartialState(const nlohmann::json& state){
	return IoT_Comm.sendState(state);
}

bool OmniSensrApp::sendFullState(){

	bool ret = false;
	state_lock.lock();
	ret = IoT_Comm.sendState(appState);
	state_lock.unlock();

	return ret;

}

bool OmniSensrApp::SaveSetting() const {
	ofstream os(configPath.c_str());

	bool ret = false;
	if(os){
		// save the generic settings first
		saveAppState(os);
		// Now save the child settings
		saveState(os);

		if(os){
			os.close();
			ret = true;
		}
	}

	return ret;
}

double OmniSensrApp::convertTime(const boost::posix_time::ptime& timestamp){
	static const ptime epoch = boost::posix_time::time_from_string("1970-01-01 00:00:00.000");

	return (timestamp - epoch).ticks() / static_cast<double>(boost::posix_time::time_duration::ticks_per_second());
}

// ==================== Private Functions ==============================

// -- Read config file
bool OmniSensrApp::ParseSetting() {
	ParseConfigTxt parser(configPath);

	unsigned int paramReadCnt = initConfig(parser);

	// every app has a periodic state reporting interval
	if ( parser.getValue("PERIODIC_STATE_REPORTING_INTERVAL", stateReportInterval)){
		++paramReadCnt;
		//cout << "PERIODIC_STATE_REPORTING_INTERVAL = " << stateReportInterval << endl;
	}

	// Add any other variables common to EVERY app

	bool ret = (paramReadCnt == parser.paramSize());
	if (!ret) {
		cerr << "!! paramReadCnt[" << paramReadCnt << "] != config.paramSize() ["
			 << parser.paramSize() << "] !!" << endl;
	}

	return ret;

}

// -- Save to config file
void OmniSensrApp::saveAppState(std::ostream& os) const{

	// save the states that we loaded
	os << "PERIODIC_STATE_REPORTING_INTERVAL " << stateReportInterval << endl;

}


// periodically send the full state
void OmniSensrApp::sendFullStateLoop(){
	while(1){
		sleep(static_cast<unsigned int>(stateReportInterval));
		sendFullState();
	}
}

void OmniSensrApp::deltaState_callback(const string& deltaMsg) {
	json entireDeltaMsgJson;
	try {
		entireDeltaMsgJson = json::parse(deltaMsg);
	} catch (std::invalid_argument &e) {
		std::cout << "Failed to parse delta message" << std::endl;
		sendError(e.what(), false);
		return;
	}
	json deltaMsgJson = entireDeltaMsgJson[appName];
	// -- Check if there is at least a delta state destined to this app. If not, then exit this function
	if (deltaMsgJson != nullptr){

		// lock the deltastate mutex
		delta_cb_lock.lock();

		// iterate over the keys in this delta message
		for (auto it = deltaMsgJson.begin(); it != deltaMsgJson.end(); ++it) {
			string val = boost::lexical_cast<string>(it.value());
			if (it->is_string()) val = val.substr(1, val.length() - 2);
			// For every callback registered to the given key, ...
			for (callback_map_t::const_iterator cbit = delta_callback_map.find(it.key());
					cbit != delta_callback_map.end() && cbit->first == it.key(); ++cbit){

				// do the actual callback
				cbit->second(val);
			}
		}

		// done with deltastate map
		delta_cb_lock.unlock();
	}
}

void OmniSensrApp::control_callback(const string& msg){
	// -- Parse the message as you wish and take actions if needed
	// check to make sure that the control message applies to me!
	// NOTE: this could be done with appropriate subscriptions, perhaps.
	// If that's the case, then this simply becomes processCommand(msg)

	processCommand(msg);

	/*vector<string> topicTokens;
	boost::algorithm::split(topicTokens, topic, boost::is_any_of("/"));

	bool is_applicable = true;

	// Mandatory "control/appName"
	is_applicable &= (topicTokens.size() >= 1 && topicTokens[0] == "control");
	is_applicable &= (topicTokens.size() >= 2 && topicTokens[1] == appName);

	vector<string> storeLoc;
	boost::algorithm::split(storeLoc, mIoTGateway.GetMyStoreName(), boost::is_any_of("/"));

	// Optional ".../store/location/device"
	is_applicable &= (topicTokens.size() < 3 || (storeLoc.size() > 0 && topicTokens[2] == storeLoc[0]));
	is_applicable &= (topicTokens.size() < 4 || (storeLoc.size() > 1 && topicTokens[3] == storeLoc[1]));
	is_applicable &= (topicTokens.size() < 5 || topicTokens[4] == mIoTGateway.GetMyDevName());

	if (is_applicable){
		processCommand(msg);
	}*/
}

template <>
void OmniSensrApp::setVariable<float>(const std::string& name, float& var, const std::string& msg){
	// nlohmann json uses double for floating point as default
	double tmp_val;
	try{
		tmp_val = boost::lexical_cast<double>(msg);
		tmp_val = round(tmp_val * 100000) / 100000.0;
		state_lock.lock();
		var = (float)tmp_val;
		appState[name] = tmp_val;
		sendState(name);
		state_lock.unlock();
	} catch(boost::bad_lexical_cast& e) {}
}

template <>
void OmniSensrApp::setVariable<bool>(const std::string& name, bool& var, const std::string& msg){
	// parse msg, set var to true if msg is "true" or "1"
	bool tmp_val;
	try{
		tmp_val = msg.compare("true") == 0 || msg.compare("1") == 0;
		state_lock.lock();
		var = tmp_val;
		appState[name] = tmp_val;
		sendState(name);
		state_lock.unlock();
	} catch(std::exception& e) {}
}

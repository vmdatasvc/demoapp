/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#include <vector>
#include <boost/shared_ptr.hpp>

// local include
#include <core/TextCodec.hpp>
#include <modules/utility/File.hpp>
#include <core/Error.hpp>

namespace vml
{

std::string TextCodec::CRYPT_KEY("!*&AD<");

TextCodec::TextCodec()
{
}

TextCodec::~TextCodec()
{
}

void
TextCodec::encodeToFile(const std::string& fileName, const std::string& data)
{
  std::vector<unsigned char> compressed;

  encodeToMemory(data, compressed);

  try{
    File::writeMemoryToBinaryFile(fileName, compressed);
  }

  catch(File::write_error){
//    printf("Error: Unable to write...\n");
	  VML_ERROR(MsgCode::IOErr,"Unable to write ..." << std::endl);
  }
}

void
TextCodec::encodeToMemory(const std::string &inData, std::vector<unsigned char> & outData)
{
  unsigned long compressedLength=0, uncompressedLength;

  outData.clear();
  outData.resize(inData.length()+12);

  uncompressedLength = inData.length();

  // temporary comment out. install zlib later
//  compress2((unsigned char*)&outData[0], &compressedLength, (unsigned char*)&inData[0], uncompressedLength, 1);

  char *uncomprLengthCharPtr = (char*)&uncompressedLength;

  outData.insert(outData.begin(), *(uncomprLengthCharPtr+3));
  outData.insert(outData.begin(), *(uncomprLengthCharPtr+2));
  outData.insert(outData.begin(), *(uncomprLengthCharPtr+1));
  outData.insert(outData.begin(), *(uncomprLengthCharPtr));
  outData.resize(compressedLength+4);

  encrypt(outData);
}

void
TextCodec::decodeFromFile(const std::string& fileName, std::string& stringData)
{
  if (isEncoded(fileName))
  {
    std::vector<unsigned char> data;
    File::readBinaryFileToMemory(fileName,data);
    decodeFromMemory(data,stringData);
  }

  else
  {
    File::readTextFileToMemory(fileName,stringData);
  }
}

void
TextCodec::decodeFromMemory(std::vector<unsigned char>& encodedString, std::string& stringData)
{
  std::string compressed = "";
  std::string uncompressed = "";
 
  unsigned int *uncompressedLength;
  //unsigned int compressedLength;

  uncompressedLength = (unsigned int*)(&encodedString[0]);

  uncompressed.resize(*uncompressedLength);
  //compressedLength = encodedString.size()-4;

  decrypt(encodedString);

  stringData.resize(*uncompressedLength);

  // temporary comment out
//  uncompress((unsigned char*)&stringData[0], (unsigned long*)uncompressedLength, (unsigned char*)&encodedString[4], compressedLength);

  encrypt(encodedString); // We want to keep the state of encodedString constant
}

bool 
TextCodec::isEncoded(const std::string& fileName)
{
  std::vector<unsigned char> header;
  header.clear();

  boost::shared_ptr<File> inFile(new File());

	try{
		inFile->readBinaryFileToMemory(fileName, header, CRYPT_KEY.length()+4);
	}

  catch(File::file_not_found)
	{
//    printf("Error: File not found...\n");
	  	VML_ERROR(MsgCode::IOErr,"File not found" << fileName << std::endl);
		return false;
	}

  decrypt(header);

  // Check the first two bytes of the compressed code
  if ((header[4] == 120) && (header[5] == 1)) // Is encoded...
  {
    return true;
  }

  else // Is not encoded...
  {
    return false;
  }

}

void
TextCodec::encrypt(std::vector<unsigned char>& buffer)
{
  for (unsigned int i = 4; i < CRYPT_KEY.length()+4; i++)
  {
    buffer[i] ^= CRYPT_KEY[i-4];
  }
}

void
TextCodec::decrypt(std::vector<unsigned char>& buffer)
{
  for (unsigned int i = 4; i < CRYPT_KEY.length()+4; i++)
  {
    buffer[i] ^= CRYPT_KEY[i-4];
  }
}

}; // namespace ait

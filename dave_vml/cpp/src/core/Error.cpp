/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
/*
 * Error.cpp
 *
 *  Created on: Oct 1, 2015
 *      Author: yyoon
 */

#ifndef _WIN32
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdarg.h>

#include "core/Error.hpp"

#include "opencv2/opencv.hpp"

#include <time.h>

using namespace vml;

//Redirect OpenCV errors to our error functions
//CvErrorCallback tmpRedirectErr = cvRedirectError(Error::cvErrorHandler);

Logger::Logger () : mFileStream(NULL),
	mOStream(&std::cerr),
	mIsSetInternally(false)
{
}


Logger::~Logger()
{
	stopLogging();
}


bool Logger::startLogging(const std::string fname, std::ios_base::openmode mode)
{
	std::ofstream *myfstream = new std::ofstream(fname.c_str(),mode);
	if (!myfstream || myfstream->fail())
	{
		std::cerr << "cLog: Couldn't open the file:"<< std::endl;
		return false;
	};
	startLogging(myfstream);
	mIsSetInternally = true;
	return true;
}

bool Logger::startLogging(std::ostream *os)
{
	if (os != NULL)
	{
		mOStream = os;
		return true;
	}
	return false;
}

bool Logger::startLogging(std::ofstream *fs)
{
	if (fs != NULL)
	{
		mFileStream = fs;
		//newline each time file is appended.
		*mFileStream <<"\n";
		return true;
	}
	return false;
}

void Logger::stopLogging()
{
	if (mFileStream!=NULL )
	{
		mFileStream->close();
		if (mIsSetInternally)
		{
			delete mFileStream;
		}
	}
	mOStream = &std::cerr;
	mFileStream = NULL;
	mIsSetInternally = false;
}

void Logger::logMessage(const std::string &msg)
{
	if (mFileStream) {
		*(mFileStream) << msg ;
		(mFileStream)->flush();
	}
}

void Logger::outputMessage(const std::string &msg)
{
	if (mOStream != NULL) {
		*(mOStream) << getTime() << msg << std::endl;
	}
}

std::string Logger::getTime()
{
	time_t rawtime;
	struct tm * timeinfo;
	char buffer[80];
	memset(buffer, 0, sizeof(buffer));
	time(&rawtime);

	timeinfo = localtime ( &rawtime );
	strftime(buffer,80,"[%x %I:%M:%S%p] ",timeinfo);
	return std::string(buffer);
}

Error::Error(const std::string &what_arg, const MsgCode::Code errCode) :
	std::runtime_error(what_arg),
	mCode(errCode)
{
	if(mCode > MsgCode::DisplayErr) {
		mCode = MsgCode::Code(mCode - MsgCode::DisplayErr);
	}
}

void Error::message(const char *msg, ... )
{
	va_list arglist;
	va_start(arglist, msg);

	//allocate memory
	char buffer[WARN_BUFFER_LEN]; // buffer2[1024];
	memset(buffer, 0, sizeof(buffer));

	//set memory
	vsnprintf(buffer, WARN_BUFFER_LEN, msg, arglist);
	va_end(arglist);

	Error::write_message( Error::getTime() + buffer );

} // message(const char *)


void Error::message(const std::string &msg)
{

	Error::write_message( Error::getTime() + msg );

} // message( std::string )

void Error::message(const std::stringstream &msg)
{

	Error::write_message( Error::getTime() + msg.str() );

} // message( std::stringstream )


void Error::write_message(const std::string &msg )
{

#ifndef DEPLOY
	// Otherwise, if not in DEPLOY mode, output the message
//	mLog.outputMessage(msg);
	std::cerr << msg << std::endl;
#endif

#ifndef NDEBUG
	// In DEBUG mode, also output the message to the log file:
//	mLog.logMessage(msg);
#endif

} // write_message()


void issueHelper(std::string &fullMessage,
                 MsgCode::Code &code, std::string &errMsg,
                 int line)
{
	//bool display = false;
	if(code > MsgCode::DisplayErr) {
		code = MsgCode::Code(code - MsgCode::DisplayErr);
		//display = true;
	}

	std::stringstream displayStr;


#ifndef NDEBUG
	if(errMsg.empty()) {
		errMsg = "<empty message>";
	}

	// Always display the message in debug, regardless of "display" setting
	// Debug-Mode Error message format (example):
	//   11/10/2011 11:16:18AM Error:17 (This is the mssage.), in funcName(),
	//     in file filename.cpp, at line 123

	displayStr << fullMessage << code << " \"" << errMsg <<  "\", at line " << line;
#else
#ifdef DEPLOY
	// Deploy, Release
//	std::string msgStr("");
//	if(display) {
//		msgStr = " \"" + errMsg + "\"";
//	}
//	displayStr << fullMessage << code << msgStr << " [" << file << std::hex << line << "]";
#else
//#include "md5.h"
//	// In internal Release mode only.
//	std::string msgStr("");
//	file = md5(file,5);
//	if(display) {
//		msgStr = " \"" + errMsg + "\"";
//	}
//	displayStr << fullMessage << code << msgStr << " [" << file << std::hex << line << "]";
#endif /*DEPLOY*/
#endif

	fullMessage = displayStr.str();

	// Add newline for display, but not for subsequent throw
	//std::stringstream displayStrNewline;
	//displayStrNewline <<
	displayStr << std::endl;
	std::string fullMessageNewLine(displayStr.str());
	Error::message(fullMessageNewLine);

} // issueHelper()


void Error::issueWarning(MsgCode::Code code, std::string msg, int line )
{
	std::string fullMessage("Warning:");

	issueHelper(fullMessage, code, msg, line);

} // issueWarning()


void Error::issueError(MsgCode::Code code, std::string msg, int line )
{
	std::string fullMessage("Error:");

	issueHelper(fullMessage, code, msg, line);

	throw Error(fullMessage, code);

} // issueError()


int Error::cvErrorHandler(int status, const char* err_msg, int line, void* userdata)
{
	std::stringstream displayStr;
	std::stringstream msgStr;

#ifndef NDEBUG
	// Debug mode: let us know it was OpenCV error and OpenCV's status code/msg
	displayStr << "OpenCV Error[" << status << "]:";
	msgStr << err_msg << " \"" << cvErrorStr(status) << "\"";

#else
	// Don't reveal OpenCV usage
//	displayStr << "Error:";
//	fileStr = "";
//	funcStr = "";
#endif

	std::string fullMessage(displayStr.str());
	MsgCode::Code code = MsgCode::openCVErr;
	std::string msg(msgStr.str());
	issueHelper(fullMessage, code, msg, line);

	throw Error(fullMessage, code);

} // cvErrorHandler()


std::string Error::getTime()
{
	time_t rawtime;
	struct tm * timeinfo;
	char buffer[80];
	memset(buffer, 0, sizeof(buffer));
	time(&rawtime);

	timeinfo = localtime ( &rawtime );
	strftime(buffer,80,"%x %I:%M:%S%p ",timeinfo);
	return std::string(buffer);
}

#endif


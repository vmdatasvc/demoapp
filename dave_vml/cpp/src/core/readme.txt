/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

vml/cpp/src/core: 
Place for codes that are independent from modules. Codes that could be shared
by different modules.

/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * CameraCalibration.cpp
 *
 *  Created on: Nov 16, 2015
 *      Author: yyoon
 */

#include <opencv2/imgproc.hpp>
#include <core/CameraCalibration.hpp>

#include <core/Error.hpp>

namespace vml {

CameraModelOpenCV::CameraModelOpenCV(int rows, int cols, boost::shared_ptr<SETTINGS> settings):
				mValid(false),
                mRows(rows),
                mCols(cols),
		        mCameraHeight(settings->getFloat("Camera/height",0.f,false)),
		        mCircleInc(settings->getFloat("Camera/circleIncrement",10.f,false)*M_PI/180.f),
				mCCDWidth(0.f),
				mCCDHeight(0.f),
				mFocalLength(0.f)
{
	// retrieve camera parameters
	// NOTE: Camera/distortionParameter field actually contains all the camera parameters needed for OpenCV
	//       The name "distortionParameters" is used for backward-compatibility with previous camera model code.
	cam_params = settings->getDoubleVector("Camera/distortionParameters","",false);

	if (cam_params.size() > 0)
	{
		if (cam_params.size() != CP_SIZE)
		{
				VML_ERROR(MsgCode::assertErr,"Camera/distortionParameters must have " << CP_SIZE << " elements");
		}

		// NOTE: following Paul's cam_distortion_correction_opencv.cpp code
		fx = (float)cam_params[CP_FX];
		fy = (float)cam_params[CP_FY];

		calibImageSize_width = (float)cam_params[CP_CISW];
		calibImageSize_height = (float)cam_params[CP_CISH];

		LENS_OFFSET_ZOOM_COMPENSATION = (float)cam_params[CP_LOZC];
		UNDISTORT_SCALE_FACTOR = (float)cam_params[CP_USF];

		isFishEye = ((int)cam_params[CP_FE] == 1);


		UNDISTORT_SCALE_FACTOR *= LENS_OFFSET_ZOOM_COMPENSATION;

		cx = cols*0.5f;
		cy = rows*0.5f;

		fx *= ((float)cols/(float)calibImageSize_width)*LENS_OFFSET_ZOOM_COMPENSATION;
		fy *= ((float)rows/(float)calibImageSize_height)*LENS_OFFSET_ZOOM_COMPENSATION;

		mK = cv::Mat_<float>(3,3);
		mK = 0.f;	// initialize
		mK(0,0) = fx;
		mK(0,2) = cx;
		mK(1,1) = fy;
		mK(1,2) = cy;
		mK(2,2) = 1.f;

		int dist_vec_size;
		if (isFishEye)
		{
				mD = cv::Mat::zeros(4,1,CV_32F);
				dist_vec_size = 4;
		}
		else
		{
				mD = cv::Mat::zeros(8,1,CV_32F);
				dist_vec_size = 6;
		}

		for (int i=0;i<dist_vec_size;i++)
		{
				mD.at<float>(i,0) = cam_params[i];
		}

		// create a new camera matrix that takes into account the zoom factor
		mK.copyTo(mKNew);

		mKNew.at<float>(0,0) *= UNDISTORT_SCALE_FACTOR;
		mKNew.at<float>(1,1) *= UNDISTORT_SCALE_FACTOR;

		k1 = cam_params[CP_D1];
		k2 = cam_params[CP_D2];
		k3 = cam_params[CP_D5];
		k4 = cam_params[CP_D6];

		// For some mysterious reason, estimated person shape doesn't fit real one with camera calib params.
		// The following factor tunes it to fit. This should be chosen emperically.
		// if zoomCompensationFactor is not provided, just use default zoom.

		mZoomCompensationFactor = settings->getFloat("Camera/zoomCompensateFactor") * LENS_OFFSET_ZOOM_COMPENSATION;
		mCameraHeight *= mZoomCompensationFactor;
		if (mCameraHeight > 0.f) {
			mCameraHeight += 0.2;	// camera height is distance to the lens. Add lens-to-virtual image plane offset.
		}

		if (mCameraHeight > 0.f)
		{
			mValid = true;
		}
	}

	// if cam_params size is zero, then it means camera parameters are not specified, so make camera model invalid and return false when accessed

}

// Construct camera model for trajectory backprojection.
CameraModelOpenCV::CameraModelOpenCV(int rows,
		int cols,
		std::string name,
		float camera_height,
		float ccd_width,
		float ccd_height,
		float focal_length,
		std::vector<double> distortion_parameters):
				mValid(false),
				mRows(rows),
				mCols(cols),
				mCameraHeight(camera_height),
				mZoomCompensationFactor(1.f),
				mCircleInc(15.f),
				mCameraModelName(name),
				mCCDWidth(ccd_width),
				mCCDHeight(ccd_height),
				mFocalLength(focal_length)
{
	if (distortion_parameters.size() > 0)
	{
	    if (distortion_parameters.size() != CP_SIZE)
	    {
	            VML_ERROR(MsgCode::assertErr,"distotion parameters must have " << CP_SIZE << " elements");
	    }

	    // NOTE: following Paul's cam_distortion_correction_opencv.cpp code
	    fx = (float)distortion_parameters[CP_FX];
	    fy = (float)distortion_parameters[CP_FY];

	    calibImageSize_width = (float)distortion_parameters[CP_CISW];
	    calibImageSize_height = (float)distortion_parameters[CP_CISH];

	    LENS_OFFSET_ZOOM_COMPENSATION = (float)distortion_parameters[CP_LOZC];
	    UNDISTORT_SCALE_FACTOR = (float)distortion_parameters[CP_USF];

	    isFishEye = ((int)distortion_parameters[CP_FE] == 1);


	    UNDISTORT_SCALE_FACTOR *= LENS_OFFSET_ZOOM_COMPENSATION;

	    cx = cols*0.5f;
	    cy = rows*0.5f;

	    fx *= ((float)cols/(float)calibImageSize_width)*LENS_OFFSET_ZOOM_COMPENSATION;
	    fy *= ((float)rows/(float)calibImageSize_height)*LENS_OFFSET_ZOOM_COMPENSATION;

	    mK = cv::Mat_<float>(3,3);
	    mK = 0.f;	// initialize
	    mK(0,0) = fx;
	    mK(0,2) = cx;
	    mK(1,1) = fy;
	    mK(1,2) = cy;
	    mK(2,2) = 1.f;

	    int dist_vec_size;
	    if (isFishEye)
	    {
	            mD = cv::Mat::zeros(4,1,CV_32F);
	            dist_vec_size = 4;
	    }
	    else
	    {
	            mD = cv::Mat::zeros(8,1,CV_32F);
	            dist_vec_size = 6;
	    }

	    for (int i=0;i<dist_vec_size;i++)
	    {
	            mD.at<float>(i,0) = distortion_parameters[i];
	    }

	    // create a new camera matrix that takes into account the zoom factor
	    mK.copyTo(mKNew);

	    mKNew.at<float>(0,0) *= UNDISTORT_SCALE_FACTOR;
	    mKNew.at<float>(1,1) *= UNDISTORT_SCALE_FACTOR;

	    k1 = distortion_parameters[CP_D1];
	    k2 = distortion_parameters[CP_D2];
	    k3 = distortion_parameters[CP_D5];
	    k4 = distortion_parameters[CP_D6];

		// For some mysterious reason, estimated person shape doesn't fit real one with camera calib params.
		// The following factor tunes it to fit. This should be chosen emperically.
	    // if zoomCompensationFactor is not provided, just use default zoom.
//	    mZoomCompensationFactor = settings->getFloat("Camera/zoomCompensateFactor") * LENS_OFFSET_ZOOM_COMPENSATION;
	    mZoomCompensationFactor = LENS_OFFSET_ZOOM_COMPENSATION;
		mCameraHeight *= mZoomCompensationFactor;

		if (mCameraHeight > 0.f) {
			mCameraHeight += 0.2;	// camera height is distance to the lens. Add lens-to-virtual image plane offset.
		}

		if (mCameraHeight > 0.f)
		{
			mValid = true;
		}

	}
}

CameraModelOpenCV::~CameraModelOpenCV()
{
}

cv::Point CameraModelOpenCV::undistortPoint(cv::Point p)
{
	return cv::Point(undistortPoint(cv::Point2f((float)p.x,(float)p.y)));
}

cv::Point CameraModelOpenCV::distortPoint(cv::Point p)
{
	return cv::Point(distortPoint(cv::Point2f((float)p.x,(float)p.y)));
}

cv::Point2f CameraModelOpenCV::undistortPoint(cv::Point2f p)
{
        std::vector<cv::Point2f> input_vector;
        input_vector.push_back(p);
        std::vector<cv::Point2f> output_vector;
        if (isFishEye)
        {
        	cv::fisheye::undistortPoints(input_vector,output_vector,mK,mD);
        }
        else
        {
            cv::undistortPoints(input_vector,output_vector,mK,mD);
        }
        return output_vector[0];
}

cv::Point2f CameraModelOpenCV::distortPoint(cv::Point2f p)
{
        if (isFishEye)
        {
            std::vector<cv::Point2f> input_vector;
            input_vector.push_back(p);
            std::vector<cv::Point2f> output_vector;
        	cv::fisheye::distortPoints(input_vector,output_vector,mK,mD);
            return output_vector[0];
        }
        else
        {
        	return nonFishEyeDistortion(p);
        }
}

void CameraModelOpenCV::undistortPoints(const std::vector<cv::Point> &input_points,
		std::vector<cv::Point> &output_points)
{
	std::vector<cv::Point2f> _input_points_2f(input_points.size());
	std::vector<cv::Point2f> _output_points_2f;
	{
		std::vector<cv::Point>::const_iterator _p;
		std::vector<cv::Point2f>::iterator _p2;
		for ( _p = input_points.begin(), _p2 = _input_points_2f.begin();
				_p != input_points.end(); _p++, _p2++)
		{
			_p2->x = (float)_p->x;
			_p2->y = (float)_p->y;
		}
	}
	undistortPoints(_input_points_2f, _output_points_2f);

	output_points.clear();
	output_points.resize(_output_points_2f.size());
	{
		std::vector<cv::Point>::iterator _p;
		std::vector<cv::Point2f>::iterator _p2;
		for (_p = output_points.begin(), _p2 = _output_points_2f.begin();
				_p != output_points.end(); _p++, _p2++)
		{
			_p->x = (int)_p2->x;
			_p->y = (int)_p2->y;
		}
	}
}

void CameraModelOpenCV::distortPoints(const std::vector<cv::Point> &input_points,
		std::vector<cv::Point> &output_points)
{
	std::vector<cv::Point2f> _input_points_2f(input_points.size());
	std::vector<cv::Point2f> _output_points_2f;
	{
		std::vector<cv::Point>::const_iterator _p;
		std::vector<cv::Point2f>::iterator _p2;
		for ( _p = input_points.begin(), _p2 = _input_points_2f.begin();
				_p != input_points.end(); _p++, _p2++)
		{
			_p2->x = (float)_p->x;
			_p2->y = (float)_p->y;
		}
	}

	distortPoints(_input_points_2f, _output_points_2f);

	output_points.clear();
	output_points.resize(_output_points_2f.size());

	{
		std::vector<cv::Point>::iterator _p;
		std::vector<cv::Point2f>::iterator _p2;
		for (_p = output_points.begin(), _p2 = _output_points_2f.begin();
				_p != output_points.end(); _p++, _p2++)
		{
			_p->x = (int)_p2->x;
			_p->y = (int)_p2->y;
		}
	}
}

void CameraModelOpenCV::undistortPoints(const std::vector<cv::Point2f> &input_points,
		std::vector<cv::Point2f> &output_points)
{
	output_points.clear();
	output_points.resize(input_points.size());

	if (isFishEye)
	{
		cv::fisheye::undistortPoints(input_points,output_points,mK,mD);
	}
	else
	{
		cv::undistortPoints(input_points,output_points,mK,mD);
	}
}

void CameraModelOpenCV::distortPoints(const std::vector<cv::Point2f> &input_points,
		std::vector<cv::Point2f> &output_points)
{
	output_points.clear();
	output_points.resize(input_points.size());

	if (isFishEye)
	{
		cv::fisheye::distortPoints(input_points,output_points,mK,mD);
	}
	else
	{
		for (unsigned int i=0;i<input_points.size();i++)
		{
			output_points[i] = nonFishEyeDistortion(input_points[i]);
		}
	}
}

cv::Point2f CameraModelOpenCV::nonFishEyeDistortion(cv::Point2f p)
{
	// Paul's implementation
	// NOTE: Paul's implementation doesn't work here because the output of undistortPoints are normalized coords
	// but the undistortion rectification map are unnormalized.
	// The exact size of the undistortion rectification is not known ahead so it is difficult to assume the size of the
	// rectification map and find the center of it.
	// Also fx and fy in the undistorted map is not well-defined. so it is hard to unnormalize undistorted coords.
/*
	cv::Mat map1,map2;

	cv::initUndistortRectifyMap(mK,mD,cv::Matx33d::eye(),mKNew,cv::Size(mCols,mRows),CV_32F,map1,map2);
	for (unsigned int i=0;i<input_points.size();i++)
	{
		int ipx,ipy;
		// NOTE: undistorted points are normalized. Unnormalize inputs before referencing the maps.
		ipx = (int)(input_points[i].x*fx+cx);
		ipy = (int)(input_points[i].y*fy+cy);
		ipx = ipx<0?0:ipx;
		ipy = ipy<0?0:ipy;
		ipx = ipx>=mCols?mCols-1:ipx;
		ipy = ipy>=mRows?mRows-1:ipy;
		output_points[i].x = map1.at<float>(ipy,ipx);
		output_points[i].y = map2.at<float>(ipy,ipx);
	}
*/
	// Use opencv distortion model
	// http://docs.opencv.org/2.4/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html
	// Ignoring tangential distortion
	double x0 = p.x, y0 = p.y;
	double r2 = x0*x0+y0*y0;
	double r4 = r2*r2;
	double r6 = r4*r2;
	double cdist = (1.f+k1*r2+k2*r4+k3*r6)/(1.f+k4*r2);
	double x_ = x0*cdist;
	double y_ = y0*cdist;
	x_ = x_*fx+cx;
	y_ = y_*fy+cy;
	return (cv::Point2f((float)x_,(float)y_));
}

void CameraModelOpenCV::undistortImage(cv::Mat &image_input, cv::Mat &image_output)
{
        if (isFishEye)
        {
                cv::fisheye::undistortImage(image_input,image_output,mK,mD,mKNew);
        }
        else
        {
                cv::undistort(image_input,image_output,mK,mD,mKNew);
        }
}

void CameraModelOpenCV::perspectiveCircleProjection(cv::Point center, float radius, std::vector<cv::Point> &projected_circle)
{
	projected_circle.clear();
	int numPts = (int)(2*M_PI/mCircleInc);
	projected_circle.resize(numPts);

	std::vector<cv::Point2f> input_vector;
	input_vector.push_back(cv::Point2f((float)center.x,(float)center.y));
	std::vector<cv::Point2f> output_vector;
	undistortPoints(input_vector,output_vector);

	// backproject to find the (x,y,z) coords in world
	float u = output_vector[0].x;	// output is already normalized
	float v = output_vector[0].y;

	// this is the center of the person
	float Z = mCameraHeight;
	float X = Z*u;
	float Y = Z*v;

	float theta = 0.f;
	input_vector.clear();
	input_vector.resize(numPts);
	output_vector.clear();
	output_vector.resize(numPts);
	for (int i=0;i<(int)(2*M_PI/mCircleInc);i++,theta+=mCircleInc)
	{
			cv::Point tp;
			float z = Z;
			float x = (X+radius*cos(theta))/z;
			float y = (Y+radius*sin(theta))/z;

			input_vector[i] = cv::Point2f(x,y);
	}

	distortPoints(input_vector,output_vector);
	for (int i=0; i < numPts ; i++)
	{
		projected_circle[i] = cv::Point(output_vector[i]);
	}

}

void CameraModelOpenCV::perspectiveCylinderProjection(cv::Point center,
		float cylinder_radius,
		float cylinder_height,
		std::vector<cv::Point> &projected_cylinder)
{
	projected_cylinder.clear();
    std::vector<cv::Point>	temp_contour;
    int	numPts = (int)(2*M_PI/mCircleInc)*2;
    temp_contour.resize(numPts);

	std::vector<cv::Point2f> input_vector;
	input_vector.push_back(cv::Point2f(center));
	std::vector<cv::Point2f> output_vector;
	undistortPoints(input_vector,output_vector);

	// backproject to find the (x,y,z) coords in world
	float u = output_vector[0].x;	// output is already normalized
	float v = output_vector[0].y;

	// this is the center of the cylinder
	float Z = mCameraHeight-cylinder_height/2;
	float X = Z*u;
	float Y = Z*v;

	float theta = 0.f;
	input_vector.clear();
	input_vector.resize(numPts);
	output_vector.clear();
	output_vector.resize(numPts);
	for (int i=0;i<(int)(2*M_PI/mCircleInc);i++,theta+=mCircleInc)
	{
			cv::Point tp;

			// upper circle points
			float z = Z-cylinder_height/2;
			float x = (X+cylinder_radius*cos(theta))/z;
			float y = (Y+cylinder_radius*sin(theta))/z;

			input_vector[2*i] = cv::Point2f(x,y);

			// lower circle points
			z = Z+cylinder_height/2;
			x = (X+cylinder_radius*cos(theta))/z;
			y = (Y+cylinder_radius*sin(theta))/z;
			input_vector[2*i+1] = cv::Point2f(x,y);
	}

	distortPoints(input_vector,output_vector);

	for (int i=0; i < numPts ; i++)
	{
		temp_contour[i] = cv::Point(output_vector[i]);
	}

	cv::convexHull(temp_contour,projected_cylinder);
}

//cv::Point CameraModelOpenCV::uprightPointProjection(cv::Point center,float length_multiple)
//{
//	cv::Point2f pos2f = cv::Point2f(footpos);
//	pos2f = mpCameraModel->undistortPoint(pos2f);
//	pos2f.x *= mCameraHeight/(mCameraHeight-(mPersonHeight*length_multiple));
//	pos2f.y *= mCameraHeight/(mCameraHeight-(mPersonHeight*length_multiple));
//	pos2f = mpCameraModel->distortPoint(pos2f);
//	return cv::Point(pos2f);
//}
//
//cv::Point PersonShapeModel::getFootPosFromHeadPos(cv::Point headpos, float length_multiple)
//{
//	cv::Point2f pos2f = cv::Point2f(headpos);
//	pos2f = mpCameraModel->undistortPoint(pos2f);
//	pos2f.x *= (mCameraHeight-mPersonHeight*length_multiple)/mCameraHeight;
//	pos2f.y *= (mCameraHeight-mPersonHeight*length_multiple)/mCameraHeight;
//	pos2f = mpCameraModel->distortPoint(pos2f);
//	return cv::Point(pos2f);
//}

cv::Point CameraModelOpenCV::perspectiveCylinderAxisPointProjection(cv::Point center,float length_multiple,float cylinder_height)
{
	// NOTE: this projection is specific for camera set up perpendicular to the floor.
	//       When we have generic extrinsic camera pose, this should be updated.
	cv::Point2f pos2f = cv::Point2f(center);
	pos2f = undistortPoint(pos2f);
	float cylinder_z_center = cylinder_height*0.5f;
	float z_factor = (mCameraHeight-cylinder_z_center)/(mCameraHeight+cylinder_z_center*(length_multiple-1.f));
	pos2f.x *= z_factor;
	pos2f.y *= z_factor;
	pos2f = distortPoint(pos2f);
	return cv::Point(pos2f);
}

void CameraModelOpenCV::perspectiveBackProjection(std::vector<cv::Point2f> &input_list, std::vector<cv::Point3f> &output_list)
{
	std::vector<cv::Point2f> undistorted_coords;
	// undistortion
	undistortPoints(input_list,undistorted_coords);

	// backprojection
	for (auto coords_i = undistorted_coords.begin();coords_i!=undistorted_coords.end();coords_i++)
	{
		cv::Point3f		world_coord;
		world_coord.z = mCameraHeight;
		world_coord.x = mCameraHeight*coords_i->x;
		world_coord.y = mCameraHeight*coords_i->y;

		output_list.push_back(world_coord);
	}
}

} // of namespace vml

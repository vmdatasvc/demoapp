/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#define XML_STATIC

#include <core/XmlParser.hpp>
#include <string>
#include <string.h>
//#include "legacy/pv/PvUtil.hpp"
#include <modules/utility/File.hpp>
#include <core/TextCodec.hpp>
#include <core/Error.hpp>

using namespace std;

/*********************************************************************
 * getNameSpaceTag:  This is an internal routine that takes a string
 *                   and returns the namespace and the tag.  This
 *                   routine handles if there is no namespace present.
 *                   An example of the input string might be the 
 *                   following: AIT:IMAGE.  In this example the 
 *                   namespace is AIT and the tag is IMAGE.
 *********************************************************************/
static void
getNameSpaceTag( const char* nm, string& ns, string& tag )
{
  int i;
  int len = strlen( nm );

  // Clear the return parameters
  ns = "";
  tag = "";
 
  // Get the namspace
  for( i = 0; i < len; i++ ){
    if ( nm[i] == ':' ){
      i++;
      break;
    }
    ns += nm[i];
  }

  if ( i == len ){
    tag = ns;
    ns = "";
  }
  else{
    for( ; i < len; i++ ){
      tag += nm[i];
    }
  }
}

/*********************************************************************
 * rawstart:  This routine is registered as the routine that is called
 *            by the actual expat parser when it encounters a start
 *            tag.  The constructor of XmlParser sends its this 
 *            pointer as the data.  The el parameter is the namespace
 *            tag combination, and the attr is the list of attributes
 *            found associated with this tag.
 *********************************************************************/
static void
rawstart( void *data, const char *el, const char **attr )
{
  string ns, tag;
  getNameSpaceTag ( el, ns, tag );
  XmlParser *p = (XmlParser *) data;
  p->start( ns, tag, attr );
}

/*********************************************************************
 * rawend:  This routine is registered as the routine that is called
 *            by the actual expat parser when it encounters an end
 *            tag.  The constructor of XmlParser sends its this 
 *            pointer as the data.  The el parameter is the namespace
 *            tag combination.
 *********************************************************************/
static void
rawend( void *data, const char *el )
{
  string ns, tag;
  getNameSpaceTag ( el, ns, tag );
  XmlParser *p = (XmlParser *) data;
  p->end( ns, tag );
}

/*********************************************************************
 * rawcharacterdatahandler:  This routine is registered as the routine 
 *            that is called by the actual expat parser when it 
 *            encounters cdata.  The constructor of XmlParser sends 
 *            its this pointer as the data.  The s parameter is the 
 *            the character data and the len parameter is the length
 *            of the character data.
 *********************************************************************/
static void
rawcharacterdatahandler( void *data, const char *s, int len )
{
  XmlParser *p = (XmlParser *) data;
  p->cdata( s, len );
}

/*********************************************************************
 * raw_pi_handler:  This routine is registered as the routine 
 *            that is called by the actual expat parser when it 
 *            encounters xml processing instructions.  The constructor 
 *            of XmlParser sends its this pointer as the data.  
 *            The target parameter is the target for the instruction
 *            and the data is the actual data for the instruction.
 *********************************************************************/
static void
raw_pi_handler (void *userData, const XML_Char *target, const XML_Char *data)
{
  XmlParser *p = (XmlParser *) userData;
  p->processInstruction( target, data );
}


// Constructors
XmlParser::XmlParser ()
{
  parentParser = NULL;
  state = x_none;
  ok = true;
  wellFormed = true;
  defaultHandler = NULL;

  // Create the actual expat xml parser
  parser = XML_ParserCreate(NULL);

  // Set the user data for the xml parser to be this
  XML_SetUserData( parser, (void *)this );

  // Register the start handler and the end handlers
  XML_SetElementHandler( parser, rawstart, rawend );

  // Register the cdata handler
  XML_SetCharacterDataHandler( parser, rawcharacterdatahandler );

  // Register the processing instruction handler
  XML_SetProcessingInstructionHandler( parser, raw_pi_handler );
}

// Destructors
XmlParser::~XmlParser ()
{
  XML_ParserFree(parser);
}

/*********************************************************************
 * registerTagHandler:  This routine registers the given handler
 *            for processing the given tag.
 *********************************************************************/
void
XmlParser::registerTagHandler (const string& tag, Handler* hand)
{
  tagHandlers[tag] = hand;
  hand->setParser (this);
}

/*********************************************************************
 * registerProcessingHandler:  This routine registers the given 
 *            handler for processing the given key.
 *********************************************************************/
void
XmlParser::registerProcessingHandler (const string& key, Handler* hand)
{
  piHandlers[key] = hand;
  hand->setParser (this);
}

/*********************************************************************
 * registerSpyHandler:  This routine registers the given handler
 *            as a spy handler.  Spy handlers are called prior to any
 *            other handler being called.
 *********************************************************************/
void
XmlParser::registerSpyHandler (Handler* hand)
{
  spyHandlers.push_back (hand);
}

/*********************************************************************
 * registerNamespaceHandler:  This routine registers the given handler
 *            for the given namespace.
 *********************************************************************/
void
XmlParser::registerNamespaceHandler (const string& ns, Handler* hand)
{
  if (ns.empty()) {
    defaultHandler = hand;
  } else {
    nsHandlers[ns] = hand;
  }
  hand->setParser (this);
}

/*********************************************************************
 * handlerForElement:  This routine returns a handler for the given
 *            namespace and the given tag.
 *********************************************************************/
Handler*
XmlParser::handlerForElement (const string& ns, const string& el)
{
  Handler *hand = NULL;
  
  if (!ns.empty()) {
    hand = (nsHandlers.find (ns) != nsHandlers.end()
          ? nsHandlers[ns] : NULL);
    if (hand) return hand;
  }
  hand = (tagHandlers.find (el) != tagHandlers.end()
          ? tagHandlers[el] : NULL);
  
  return (hand ? hand : defaultHandler);
}


/*********************************************************************
 * start:  This routine is called by the expat start callback
 *         function.  If there is a parent parser, it first calls the
 *         parent parser's start routine.  If there are any spy 
 *         handlers, they are then called.  Finally, it calls the
 *         handler associated with the given namespace and tag.
 *********************************************************************/
void
XmlParser::start (const string& ns, const string& tag, const char **attr)
{
  if (parentParser) parentParser->start (ns, tag, attr);

    for (int ndx = spyHandlers.size() - 1; ndx >= 0; --ndx) {
      spyHandlers[ndx]->start (ns, tag, attr);
    }

  Handler *hand = handlerForElement (ns, tag);
  if (hand) {
    activeHandlers.push_back (hand);
    hand->start (ns, tag, attr);
  }
}

/*********************************************************************
 * is_blank:  This routine is an internal routine used to determine
 *        if the given string contains all blanks.
 *********************************************************************/
static bool 
is_blank (const XML_Char *s, int len)
{
  for (--len; len >= 0 && isspace((int)(unsigned char)s[len]); --len);
  return (len < 0);
}

/*********************************************************************
 * cdata:  This routine is called by the expat cdata callback
 *         function.  If the given string is all blank then this
 *         routine just returns.  If there is a parent parser, 
 *         it first calls the parent parser's cdata routine.  
 *         If there are any spy handlers, they are then called.  
 *         Finally, it calls the handler associated with the 
 *         current namespace and tag.
 *********************************************************************/
void
XmlParser::cdata (const XML_Char *s, int len)
{
  if (is_blank (s, len)) return;

  if (parentParser) parentParser->cdata (s, len);

    for (int ndx = spyHandlers.size() - 1; ndx >= 0; --ndx) {
      spyHandlers[ndx]->cdata (s, len);
    }

  Handler *hand = activeHandlers.size() == 0
      ? NULL : activeHandlers[activeHandlers.size() - 1];

  if (hand) {
    hand->cdata (s, len);
  }
}

/*********************************************************************
 * processInstruction:  This routine is called by the expat process
 *         instruction callback function.  If there is a parent parser, 
 *         it first calls the parent parser's process instruction
 *         routine.  If there are any spy handlers, they are then called.  
 *         Finally, it calls the handler associated with the 
 *         current namespace and tag.
 *********************************************************************/
void
XmlParser::processInstruction (const XML_Char *target, const XML_Char *data)
{
  if (parentParser) parentParser->processInstruction (target, data);

    for (int ndx = spyHandlers.size() - 1; ndx >= 0; --ndx) {
      spyHandlers[ndx]->processInstruction (target, data);
    }

  Handler* hand = (piHandlers.find (target) != piHandlers.end()
          ? piHandlers[target] : defaultHandler);

  if (hand) {
    hand->processInstruction (target, data);
  }
}

/*********************************************************************
 * end:  This routine is called by the expat end callback function.  
 *         If there is a parent parser, it first calls the
 *         parent parser's end routine.  If there are any spy 
 *         handlers, they are then called.  Finally, it calls the
 *         handler associated with the given namespace and tag.
 *********************************************************************/
void
XmlParser::end (const string& ns, const string& tag)
{
  if (parentParser) parentParser->end (ns, tag);

  Handler *hand;
 
    for (int ndx = spyHandlers.size() - 1; ndx >= 0; --ndx) {
      spyHandlers[ndx]->end (ns, tag);
    }
 
   if (activeHandlers.size() == 0) {
    setWellFormed (false);
  }
  else {
    hand = activeHandlers[activeHandlers.size() - 1];
    hand->end (ns, tag);
    activeHandlers.pop_back();
  }
}


/*********************************************************************
 * parseFile:  This routine takes a filename as input.  It then reads
 *          the content of the file into a string.  It then calls the
 *          parse method.
 *********************************************************************/
bool
XmlParser::parseFile( const string& fname )
{
  bool rv;
  string str = "";

  vml::TextCodecPtr codec(new vml::TextCodec());

  try
  {
    codec->decodeFromFile(fname, str);
    rv = parse( str );
  }

  catch(vml::File::file_not_found)
  {
    rv = false;
  }

  return rv;
}

/*********************************************************************
 * parse:  This routine takes a string as input.  It then passes the
 *         string to the expat Xml parser.
 *********************************************************************/
bool
XmlParser::parse( string& str )
{
  char *buffer;
  int len = str.length();
  
  // TODO: need to install expat for xml parsing
  buffer = (char *)(XML_GetBuffer( parser, len+1 ));
  if ( buffer )
  {
    strcpy( buffer, str.c_str() );
    if ( XML_ParseBuffer( parser, len, true ) == 0 )
    {
      ofstream eff( "xml_error_result.xml");
      eff << str << endl;
      VML_MSG( "\n" << XML_ErrorString( XML_GetErrorCode( parser ) ) );
      VML_MSG( "\nError occurred on line " <<  XML_GetCurrentLineNumber( parser ) );
      VML_MSG( "\nError occurred in column " << XML_GetCurrentColumnNumber( parser ) );
      setWellFormed( false );
    }
  }
  else
  {
    setWellFormed( false );
  }
  
  return isWellFormed();
}

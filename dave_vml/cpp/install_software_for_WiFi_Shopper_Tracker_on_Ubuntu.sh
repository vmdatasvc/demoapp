#!/bin/bash
# Author: Paul J. Shin
# Instructions for Ubuntu 14.04.3 LTS 64 bit

# If you are installing these on AWS EC2 instance, then please make sure you have enough disk space. And unmount /tmp by
# sudo umount /tmp
# sudo mount -a
# Otherwise you will get 'no space left' error when installing

# << G++ 5.x >> ** Caution: This may destroy your system, so be cautious!
sudo add-apt-repository ppa:ubuntu-toolchain-r/test; sudo apt-get update
sudo apt-get purge gcc-4.*
sudo apt-get -y install gcc-5 g++-5
sudo update-alternatives 
sudo update-alternatives --remove-all gcc
sudo update-alternatives --remove-all g++
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-5 20
sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-5 20
sudo update-alternatives --config gcc
sudo update-alternatives --config g++

# << QT 5.5 >>
sudo apt-add-repository ppa:beineri/opt-qt551-trusty
sudo apt-get update; sudo apt-get -y upgrade
sudo apt-get -y install qt55creator qt55creator-dbg qt55creator-doc

# << OpenSSL for OmniSensr Connector >>
sudo apt-get -y install openssl libssl-dev

# << OpenGL >>
sudo apt-get -y install freeglut3-dev mesa-utils

# << CMake 3.x >>
sudo apt-get purge cmake
sudo apt-get -y install software-properties-common
sudo add-apt-repository ppa:george-edison55/cmake-3.x
sudo apt-get update; sudo apt-get install cmake

# << ffmpeg >>
sudo apt-get --purge remove ffmpeg
sudo apt-get --purge autoremove
sudo apt-get -y install ppa-purge
sudo ppa-purge ppa:jon-severinsson/ffmpeg
sudo add-apt-repository ppa:mc3man/trusty-media
sudo apt-get update; sudo apt-get -y dist-upgrade
sudo apt-get -y install ffmpeg

# << OpenCV 3.1.0 >>
# -- Transition Guide: http://docs.opencv.org/master/db/dfa/tutorial_transition_guide.html
sudo apt-get update; sudo apt-get -y upgrade
sudo apt-get -y install g++ libopencv-dev build-essential cmake git libgtk2.0-dev pkg-config python-dev python-numpy libdc1394-22 libdc1394-22-dev libjpeg-dev libpng12-dev libtiff4-dev libjasper-dev libavcodec-dev libavformat-dev libswscale-dev libxine-dev libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev libv4l-dev libtbb-dev libqt4-dev libfaac-dev libmp3lame-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev libvorbis-dev libxvidcore-dev x264 v4l-utils unzip libgtk2.0-dev pkg-config

cd ~; cd Downloads
wget https://github.com/Itseez/opencv/archive/3.1.0.zip; unzip 3.1.0.zip
wget https://github.com/Itseez/opencv_contrib/archive/3.1.0.zip -O 3.1.0-contrib.zip; unzip 3.1.0-contrib.zip
sudo mv opencv-3.1.0 /opt/opencv-3.1.0
sudo mv opencv_contrib-3.1.0 /opt/opencv_contrib-3.1.0 
cd /opt/opencv-3.1.0
sudo mkdir build; cd build
sudo cmake -D CMAKE_BUILD_TYPE=RELEASE -D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib-3.1.0/modules -D CMAKE_INSTALL_PREFIX=/usr/local -D INSTALL_C_EXAMPLES=ON -D ENABLE_NEON=ON -D ENABLE_VFPV3=ON -D WITH_TBB=ON -D BUILD_TBB=ON -D WITH_GTK=ON -D WITH_V4L=ON -D WITH_OPENGL=ON ..
sudo make -j $(nproc); sudo make install
sudo /bin/bash -c 'echo "/usr/local/lib" > /etc/ld.so.conf.d/opencv.conf'
sudo ldconfig

#To test some sample codes
cd ../samples/
sudo cmake .
sudo make -j $(nproc)

# << Boost 1.59 >>
sudo apt-get -y install build-essential g++ python-dev autotools-dev libicu-dev build-essential libbz2-dev libboost-all-dev
cd ~
mkdir Downloads 
cd ~/Downloads
wget -O boost_1_59_0.tar.gz http://sourceforge.net/projects/boost/files/boost/1.59.0/boost_1_59_0.tar.gz/download
tar xvf boost_1_59_0.tar.gz
cd boost_1_59_0
./bootstrap.sh --prefix=/usr/local
./b2
sudo ./b2 install

# << Mosquitto MQTT Broker for Raspberry Pi Jessie  >>
#wget http://repo.mosquitto.org/debian/mosquitto-repo.gpg.key
#sudo apt-key add mosquitto-repo.gpg.key
#cd /etc/apt/sources.list.d/
#sudo wget http://repo.mosquitto.org/debian/mosquitto-jessie.list
#sudo apt-get update
#sudo apt-get install mosquitto     

# << Mosquitto MQTT Broker for Ubuntu  >>
sudo apt-add-repository ppa:mosquitto-dev/mosquitto-ppa; sudo apt-get update
sudo apt-get install libmosquitto-dev libmosquittopp-dev mosquitto-clients

# << dlib >>
# Download and unzip the latest source code in /opt from http://dlib.net
cd /opt
sudo wget http://dlib.net/files/dlib-19.1.tar.bz2 
sudo tar jxf dlib-19.11.tar.bz2
cd /opt/dlib-19.1/examples
sudo mkdir build; cd build;
sudo cmake ../
sudo make -j $(nproc)

# << ssh2 >>
sudo apt-get install libssh2-1-dev 

# << libudev for iot>> 
sudo apt-get install libudev-dev 

# << recent libcurl >>
# if you have a problem for libcurl, try to compile recent one
#cd ~; mkdir Downloads
#cd ~/Downloads
#wget https://curl.haxx.se/download/curl-7.51.0.tar.gz
#tar -xvf curl-7.51.0.tar.gz
#cd curl-7.51.0
#./configure --with-libssh2=/usr/local/ --disable-shared
#make
#sudo make install

# Redis and Redis client hiredis
cd ~/Downloads
wget http://download.redis.io/releases/redis-3.2.6.tar.gz
tar -xzf redis-3.2.6.tar.gz
cd redis-3.2.6
make
sudo make install
# If you have an error for cc, export CC=gcc 
# If there is a fatal error in terms of jemalloc, 
# then cd deps/; make jemalloc; cd ..; make; sudo make install
cd utils
echo -ne '\n' | sudo ./install_server.sh
cd ../deps/hiredis
make
sudo make install

# << Clone VML Repository >>
mkdir ~/Repository; cd ~/Repository
# for example,
git clone https://pauljshin@bitbucket.org/pauljshin/vml.git

# Setup dlib in vml repository
cd vml/cpp/external/dlib-18.17/dlib
mkdir build; cd build
cmake ..
make -j $(nproc)

# -- Need to build figtree if we just get vml source code from the repository
cd ~/Repository/vml/cpp/external/figtree-0.9.3/
make -j $(nproc)
cd ../../../

# build vml repository
cd ~/Repository/vml
mkdir build
./_cpp_Build.sh

 
# << Eclipse >>
# sudo apt-get -y install eclipse-platform
# [Action] Download the latest Eclipse for C/C++ Developers from https://www.eclipse.org/downloads/
# [Action] Once running, make sure you crease the index cache size and the undo history size


# -- Install MySQL 5.7 on Ubuntu 14.04
# -- Refer to (https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-14-04)
cd ~/Downloads
wget http://dev.mysql.com/get/mysql-apt-config_0.6.0-1_all.deb
sudo dpkg -i mysql-apt-config_0.6.0-1_all.deb

	# -- You'll see a prompt that asks you which MySQL product you want to configure. The MySQL Server option, which is highlighted, should say mysql-5.7. If it doesn't, press ENTER, then scroll down to mysql-5.7 using the arrow keys, and press ENTER again.

sudo apt-get update
sudo apt-get install -y mysql-server libmysqlclient-dev 
mysql_upgrade --force
#sudo service mysql stop

# -- Install MySQL Connector vi apt-get
apt-get install libmysqlcppconn-dev

# -- Install MySQL Connector from Source Code
# -- Refer to (https://dev.mysql.com/doc/connector-cpp/en/connector-cpp-downloading.html)
#sudo apt-get install -y bzr 
#cd ~/Repository
#mkdir mysql-connector-cpp; cd mysql-connector-cpp
#bzr branch lp:~mysql/mysql-connector-cpp/trunk
#cd ~/Repository/vml/cpp/external/mysql-connector-cpp/trunk
#cmake . -DBOOST_ROOT=/usr/local/lib -DCMAKE_ENABLE_C++11=ON -DMYSQLCLIENT_STATIC_LINKING=1
#make -j
#sudo make install

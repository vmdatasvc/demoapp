#
#	CMakeLists.txt
#	vml/cpp/tools
#
#

project (vmltools)

# List of libraries required by most tools
set (TOOLS_LIBS expat vmlvision vmlcore)

include_directories (
	${VML_ROOT}/include
#	${VML_3RDPARY_ROOT}/${TARGET}/include
#	${VML_3RDPARY_ROOT}/${TARGET}/include/gsl
	)

macro (vml_add_tool TOOLNAME ENABLED)
	option (TOOLS_BUILD_${TOOLNAME} "Build tools/${TOOLNAME}" ${ENABLED})

	vml_add_executable (${TOOLNAME} ${TOOLNAME}.cpp)
	target_link_libraries (${TOOLNAME} ${ARGN})

	# Don't build target unless targetname is manually specified
	if (NOT TOOLS_BUILD_${TOOLNAME})
		set_target_properties (${TOOLNAME} PROPERTIES EXCLUDE_FROM_ALL 1)
	endif ()
endmacro ()

vml_add_tool (DoorCountingTest  ON ${TOOLS_LIBS}  )


# TODO: Link/include umfpack
#tvs_add_tool (test_umfpack                      OFF ${TOOLS_LIBS})

#
# Link some tools with additional libraries
#



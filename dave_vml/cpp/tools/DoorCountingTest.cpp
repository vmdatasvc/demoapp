/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/****************************************************************************/

// DoorCountingTest.cpp
//
//   command line tool for testing door counting mod
//
//   Youngrock Yoon
//   2015.9.3

#include <cstdio>
#include <getopt.h>

#include "core/Util.hpp"
#include "core/Image.hpp"

#include "legacy/vision/VisionMod.hpp"
#include "legacy/vision_blob/ForegroundSegmentMod.hpp"
#include "legacy/vision_blob/DoorCounterMod.hpp"
#include "legacy/vision/VisionFactory_fwd.hpp"
#include "legacy/vision/VisionFactory.hpp"

#define TEMPHACK

using namespace std;
using namespace ait;
using namespace ait::vision;

// globals
bool	bVerbose;
std::string	input_path = "./";
std::string	output_path = "./";
std::string	settings_path = "../cpp/settings/";

std::string input_file_name;

void display_usage( const char *argv0 )
{

	vml::FileParts	cmd_name(argv0);
	printf("\n");

#ifndef NDEBUG
	printf("***COMPILED IN DEBUG***\n\n");
#endif

	printf("Usage:  %s [options] inputvideofilename\n", cmd_name.filename().c_str());
	printf("  Options:\n");
	printf("\t [-h | --help]            print this message\n");
	printf("\t [-v | --verbose]         verbose (default off)\n");
	printf("\t [-s | --settings]        settings path\n");
	printf("\t [-i | --inputpath]       input path (default ./)\n");
	printf("\t [-o | --outputpath]      output path (default use inputpath)\n");

	printf("  Examples:\n");
	printf("\t %s door1.avi\n",cmd_name.filename().c_str());
	printf("\t %s -v door2.avi\n",cmd_name.filename().c_str());
	printf("\n");

#ifndef NDEBUG
	printf("***COMPILED IN DEBUG***\n\n");
#endif

	exit(1);

}

int main(int argc, char* argv[])
{
	std::string	settingsPath = "";

	static struct option longopts[] = {
			{ "help", 		no_argument,		NULL, 		'h'},
			{ "verbose",	no_argument,		NULL, 		'v'},
			{ "settings",	required_argument,	NULL, 		's'},
			{ "inputpath",	required_argument,	NULL, 		'i'},
			{ "outputpath", required_argument,	NULL, 		'o'},
			{ NULL,			0,					NULL,		0}
	};

	int c;
	while ((c = getopt_long(argc, argv, "hvs:i:o:", longopts, NULL)) != -1)
	{
		switch (c)
		{
			case 'v':
			{
				bVerbose = true;
				break;
			}
			case 'h':
			{
				display_usage(argv[0]);
				break;
			}
			case 's':
			{
				settings_path = optarg;
				break;
			}
			case 'i':
			{
				input_path = optarg;
				break;
			}
			case 'o':
			{
				output_path = optarg;
				break;
			}
			default:
			{
				std::cerr << "Unrecognized commandline option -" << c << ". Aborting" << std::endl;
				display_usage(argv[0]);
				return (-1);
			}
		} // of switch (c)
	} // of while

#ifdef TEMPHACK
	input_file_name = "../cpp/test/20150521_110000_327.avi";
#else
	if (optind == argc)
	{
		std::cerr << "No input video file specified" << std::endl;
		display_usage(argv[0]);
		return (-1);
	}

	input_file_name = input_path + optarg;
#endif

	// vision factory has a global instance. So, once liblegacy_vision is linked, vision factory instance can be accessed using ait::vision::VisionFactory::instance()
	// register required vision mods in vision factory
	std::cout << "Registering Vision Modules" << std::endl;

	AIT_REGISTER_VISION_MODULE(ImageAcquireMod);
	AIT_REGISTER_VISION_MODULE(ForegroundSegmentMod);
	AIT_REGISTER_VISION_MODULE(DoorCounterMod);

	// read settings
	Settings	settings("");
	// parse appropriate settings file
	settings.parseXml(settings_path+"vision_modules/ImageAcquireMod.xml");
	settings.parseXml(settings_path+"vision_modules/ForegroundSegmentMod.xml");
	settings.parseXml(settings_path+"vision_modules/DoorCounterMod.xml");
	settings.parseXml(settings_path+"vision_modules/ImageAcquireDeviceOpenCV.xml");

	// set customized settings in memory here, if necessary
	// set input file name
	settings.setString("ImageAcquireDeviceOpenCV/Source", input_file_name);

	// define module pointers for convenience
    std::string pathName, modName;

	ImageAcquireModPtr	ia_ptr;
    VisionFactory::parseFullName("ImageAcquireMod",pathName,modName);
    VisionFactory::instance()->getModule(pathName, modName, ia_ptr);

    ForegroundSegmentModPtr	fs_ptr;
    VisionFactory::parseFullName("ForegroundSegmentMod",pathName,modName);
    VisionFactory::instance()->getModule(pathName, modName, fs_ptr);

    DoorCounterModPtr	dc_ptr;
    VisionFactory::parseFullName("DoorCounterMod",pathName,modName);
    VisionFactory::instance()->getModule(pathName, modName, dc_ptr);


	// init mods
    ia_ptr->init();
    fs_ptr->init();
    dc_ptr->init(settings);

	// run process
    bool	last_frame = false;
    while (!last_frame)
    {
    	ia_ptr->process();
    	fs_ptr->process();
    	dc_ptr->process();
    }

	return 0;
}


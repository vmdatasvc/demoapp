// Copyright (C) 2003--2004 Ronan Collobert (collober@idiap.ch)
//                
// This file is part of Torch 3.1.
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "PartialLinear.h"
#include "Random.h"

namespace Torch {

PartialLinear::PartialLinear(int n_inputs_, int n_outputs_, int *connection_, SafeRandom *pSafeRandom_) : GradientMachine(n_inputs_, n_outputs_, (n_inputs_+1)*n_outputs_)
{
  int n_params_ = (n_inputs_+1)*n_outputs_;
  n_inputs = n_inputs_;
  n_outputs = n_outputs_;
  if(n_outputs > 0)
    outputs = new(allocator) Sequence(0, n_outputs);
  else
    outputs = NULL;
  if(n_inputs > 0)
    beta = new(allocator) Sequence(0, n_inputs);
  else
    beta = NULL;
//  if(n_inputs*n_outputs>0)
//  connection = connection_;
  params = new(allocator) Parameters(n_params_);
  der_params = new(allocator) Parameters(n_params_);
  partial_backprop = false;
  addROption("weight decay", &weight_decay, 0, "weight decay");
  weights = params->data[0];
  bias = params->data[0]+n_inputs*n_outputs;
  der_weights = der_params->data[0];
  der_bias = der_params->data[0]+n_inputs*n_outputs;
  pSafeRandom = pSafeRandom_;

  connection = new(allocator) Parameters(n_inputs*n_outputs);
  connection_mat = connection->data[0];

  if(1)
  for(int i = 0; i < n_outputs; i++)
  {
    for(int j = 0; j < n_inputs; j++)
      connection_mat[i*n_inputs + j] = (float)connection_[i*n_inputs + j];
  }
//  printf("n_inputs=%d n_outputs=%d\n",n_inputs,n_outputs);
  /*  Commented at Dec. 15
  if(0)
    for(int i = 0; i < n_outputs; i++)
    {
      for(int j = 0; j < n_inputs; j++)
        printf("%d ",connection[i*n_inputs + j]);
      printf("\n");
    }
  */
  reset_();
}

void PartialLinear::reset()
{
  reset_();
}

void PartialLinear::reset_()
{
  // Note: just to be compatible with "Torch II Dev"
  realv *weights_ = weights;
  realv *connection_ = connection_mat;
  realv bound = 1./sqrt((realv)n_inputs);

  for(int i = 0; i < n_outputs; i++)
  {
    for(int j = 0; j < n_inputs; j++)
      weights_[j] = connection_[j] * Random::boundedUniform(-bound, bound);
    weights_ += n_inputs;
    connection_ += n_inputs;
    bias[i] = Random::boundedUniform(-bound, bound);
  }
}

void PartialLinear::frameForward(int t, realv *f_inputs, realv *f_outputs)
{
  realv *weights_ = weights;
  realv *connection_ = connection_mat;
  for(int i = 0; i < n_outputs; i++)
  {
    realv out = bias[i];

    for(int j = 0; j < n_inputs; j++)
    {
      if(connection_[j])
      {
        out += weights_[j] * f_inputs[j];
      // Print out weights
       printf("%f ",weights_[j]);

        //        printf("%d ",(int)connection_[j]);
      }
    }
   printf(": %f\n",bias[i]);

    weights_ += n_inputs;
    connection_ += n_inputs;

    f_outputs[i] = out;
//    printf("outputs[%d]=%f\n",i,f_outputs[i]);
  }
}

void PartialLinear::frameBackward(int t, realv *f_inputs, realv *beta_, realv *f_outputs, realv *alpha_)
{
  if(!partial_backprop)
  {
    for(int i = 0; i < n_inputs; i++)
      beta_[i] = 0;
    
    realv *weights_ = weights;
    realv *connection_ = connection_mat;
    for(int i = 0; i < n_outputs; i++)
    {
      realv z = alpha_[i];
      for(int j = 0; j < n_inputs; j++)
      {
        if(connection_[j])
          beta_[j] += z * weights_[j];
      }
      weights_ += n_inputs;
    }
  }

  realv *der_weights_ = der_weights;
  realv *connection_ = connection_mat;
  for(int i = 0; i < n_outputs; i++)
  {
    realv z = alpha_[i];
    for(int j = 0; j < n_inputs; j++)
    {
      if(connection_[j])  
        der_weights_[j] += z * f_inputs[j];
    }
    der_weights_ += n_inputs;
    connection_ += n_inputs;
    der_bias[i] += z;
  }

  if(weight_decay != 0)
  {
    realv *src_ = params->data[0];
    realv *dest_ = der_params->data[0];
    // Note: pas de weight decay sur les biais.
    for(int i = 0; i < n_inputs*n_outputs; i++)
      dest_[i] += weight_decay * src_[i];
  }
}

PartialLinear::~PartialLinear()
{
}

}

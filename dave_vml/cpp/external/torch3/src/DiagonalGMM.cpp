// Copyright (C) 2003--2004 Johnny Mariethoz (Johnny.Mariethoz@idiap.ch)
//                and Samy Bengio (bengio@idiap.ch)
//                
// This file is part of Torch 3.1.
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "DiagonalGMM.h"
#include "MeanVarNorm.h"
#include "log_add.h"
#include "Random.h"

namespace Torch {

DiagonalGMM::DiagonalGMM(int n_inputs_, int n_gaussians_, EMTrainer* initial_kmeans_trainer_) : Distribution(n_inputs_,(n_inputs_*n_gaussians_)*2+n_gaussians_)
{
  n_gaussians = n_gaussians_;
  initial_kmeans_trainer = initial_kmeans_trainer_;
  
  initial_kmeans_trainer_measurers = NULL;

  var_threshold = (realv*)allocator->alloc(sizeof(realv)*n_inputs);
  for (int i=0;i<n_inputs;i++)
    var_threshold[i] = 1e-10; 

  addOOption("initial kmeans trainer measurers", (Object**) &initial_kmeans_trainer_measurers, NULL, "initial kmeans trainer measurers");
  addROption("prior weights", &prior_weights , 1e-3, "minimum weights for each gaussians");

  log_probabilities_g = new(allocator)Sequence(1,n_gaussians);
  realv* p = (realv*)params->data[0];
  realv* dp = (realv*)der_params->data[0];
  log_weights = p;
  dlog_weights = dp;
  p += n_gaussians;
  dp += n_gaussians;
  means = (realv**)allocator->alloc(sizeof(realv*)*n_gaussians);
  dmeans = (realv**)allocator->alloc(sizeof(realv*)*n_gaussians);
  var = (realv**)allocator->alloc(sizeof(realv*)*n_gaussians);
  dvar = (realv**)allocator->alloc(sizeof(realv*)*n_gaussians);
  means_acc = (realv**)allocator->alloc(sizeof(realv*)*n_gaussians);
  var_acc = (realv**)allocator->alloc(sizeof(realv*)*n_gaussians);
  weights_acc = (realv*)allocator->alloc(sizeof(realv)*n_gaussians);
  minus_half_over_var = (realv**)allocator->alloc(sizeof(realv*)*n_gaussians);
  for (int i=0;i<n_gaussians;i++) {
    means[i] = p;
    dmeans[i] = dp;
    p += n_inputs;
    dp += n_inputs;
    var[i] = p;
    dvar[i] = dp;
    p += n_inputs;
    dp += n_inputs;
    means_acc[i] = (realv*)allocator->alloc(sizeof(realv)*n_inputs);
    var_acc[i] = (realv*)allocator->alloc(sizeof(realv)*n_inputs);
    minus_half_over_var[i] = (realv*)allocator->alloc(sizeof(realv)*n_inputs);
  }
  sum_log_var_plus_n_obs_log_2_pi = (realv*)allocator->alloc(sizeof(realv)*n_gaussians);
	
	best_gauss = -1;
  best_gauss_per_frame = new(allocator)Sequence(1,1);
}

void DiagonalGMM::setVarThreshold(realv* var_threshold_){
  for (int i=0;i<n_inputs;i++)
    var_threshold[i] = var_threshold_[i];
}

void DiagonalGMM::reset(){
  // initialize randomly
  // first the weights
  if (!initial_kmeans_trainer) {
    realv sum = 0.;
    for (int i=0;i<n_gaussians;i++) {
      log_weights[i] = Random::boundedUniform(0.1,1);
      sum += log_weights[i];
    }
    for (int i=0;i<n_gaussians;i++) {
      log_weights[i] = log(log_weights[i]/sum);
    }

    // then the means and variances
    for (int i=0;i<n_gaussians;i++) {
      for (int j=0;j<n_inputs;j++) {
        means[i][j] = Random::boundedUniform(0,1);
        var[i][j] = Random::boundedUniform(var_threshold[j],var_threshold[j]*10);
      }
    }
  }
}


void DiagonalGMM::setDataSet(DataSet* data_)
{
  // here, initialize the parameters somehow...
  if (initial_kmeans_trainer) {
      initial_kmeans_trainer->train(data_,initial_kmeans_trainer_measurers);
      params->copy(initial_kmeans_trainer->distribution->params);
  }
  //check variance flooring
  for (int i=0;i<n_gaussians;i++) {
    realv* p_var_i = var[i];
    for (int j=0;j<n_inputs;j++,p_var_i++)
      if(*p_var_i < var_threshold[j])
        *p_var_i = var_threshold[j];
  }
  // check the weights
  realv log_sum = LOG_ZERO;
  for (int i=0;i<n_gaussians;i++)
    log_sum = logAdd(log_sum,log_weights[i]);
  for (int i=0;i<n_gaussians;i++)
    log_weights[i] -= log_sum;
}

void DiagonalGMM::eMSequenceInitialize(Sequence* inputs)
{
  if (!inputs)
    return;
	best_gauss_per_frame->resize(inputs->n_frames);
  log_probabilities_g->resize(inputs->n_frames);
  log_probabilities->resize(inputs->n_frames);
}

void DiagonalGMM::display()
{
  for(int i=0;i<n_gaussians;i++){
    printf("Mixture %d %.3g\n",i+1,exp(log_weights[i]));
    printf("Mean %d\n",i);
    for(int j=0;j<n_inputs;j++){
       printf("%.3g ",means[i][j]);
    }
    printf("\nVar\n");
    for(int j=0;j<n_inputs;j++){
       printf("%.3g ",var[i][j]);
    }
    printf("\n"); 
 }
}

void DiagonalGMM::sequenceInitialize(Sequence* inputs)
{
  // initialize the accumulators to 0 and compute pre-computed value
  eMSequenceInitialize(inputs);
  for (int i=0;i<n_gaussians;i++) {
    realv *sum = &sum_log_var_plus_n_obs_log_2_pi[i];
    *sum = n_inputs * LOG_2_PI;
    realv *mh_i = minus_half_over_var[i];
    realv *v = var[i];
    realv *vt = var_threshold;
    for (int j=0;j<n_inputs;j++,vt++) {
      if (*v < *vt)
        *v = *vt;
      *mh_i++ = -0.5 / *v;
      *sum += log(*v++);
    }
    *sum *= -0.5;
  }
}

void DiagonalGMM::generateSequence(Sequence* sequence)
{
  for(int i=0;i<sequence->n_frames;i++)
    generateObservation(sequence->frames[i]);
}

void DiagonalGMM::generateObservation(realv* observation)
{
  realv v_tot,v_partial;
  v_tot = Random::uniform();
  v_partial = 0.;
  realv* lw = log_weights;
  int j;
  for (j=0;j<n_gaussians;j++) {
    v_partial += exp(*lw++);
    if (v_partial > v_tot) break;
  }
  if(j>=n_gaussians)
    j = n_gaussians - 1;
  realv* v = var[j];
  realv* m = means[j];
  realv* obs = observation;

  for (int i=0;i<n_inputs;i++) {
    *obs++ = Random::normal(*m++, sqrt(*v++));
  }
}


realv DiagonalGMM::frameLogProbabilityOneGaussian(int g, realv *inputs)
{
  realv* means_g = means[g];
  realv* mh_g = minus_half_over_var[g];
  realv sum_xmu = 0.;
  realv *x = inputs;
  for(int j = 0; j < n_inputs; j++) {
    realv xmu = (*x++ - *means_g++);
    sum_xmu += xmu*xmu * *mh_g++;
  }
  realv lp = sum_xmu + sum_log_var_plus_n_obs_log_2_pi[g];
  return lp;
}


realv DiagonalGMM::viterbiFrameLogProbability(int t, realv *inputs)
{
  realv *p_log_w = log_weights;
  realv *lpg = log_probabilities_g->frames[t];
  realv max_lpg = LOG_ZERO;
  realv max_lp = LOG_ZERO;
  realv lpg_w = 0;
  for (int i=0;i<n_gaussians;i++,lpg++) {
    *lpg = frameLogProbabilityOneGaussian(i, inputs);
    lpg_w = *lpg + *p_log_w++;
    if(lpg_w > max_lpg){
			max_lpg = lpg_w;
			best_gauss = i;
     max_lp = *lpg;
		 }
  }
  log_probabilities->frames[t][0] = max_lp;
	best_gauss_per_frame->frames[t][0] = (realv)best_gauss;
  return max_lp;
}

realv DiagonalGMM::frameLogProbability(int t, realv *inputs)
{
  realv *p_log_w = log_weights;
  realv *lpg = log_probabilities_g->frames[t];
  realv log_prob = LOG_ZERO;
  for (int i=0;i<n_gaussians;i++) {
    *lpg = frameLogProbabilityOneGaussian(i, inputs);
    log_prob = logAdd(log_prob, *lpg++ + *p_log_w++);
  }
  log_probabilities->frames[t][0] = log_prob;
  return log_prob;
}

void DiagonalGMM::frameViterbiAccPosteriors(int t, realv *inputs, realv log_posterior)
{
  realv *p_weights_acc = weights_acc;
  realv *lp_i = log_probabilities_g->frames[t];
  realv *log_w_i = log_weights;
  realv max_lpg = LOG_ZERO;
  int best_g = 0;
  //findmax
  for (int i=0;i<n_gaussians;i++) {
    realv post_i =  *log_w_i++ + *lp_i++;
    if(post_i > max_lpg){
      best_g = i;
      max_lpg = post_i;
    }
  }
  p_weights_acc[best_g] += 1;
  realv* means_acc_i = means_acc[best_g];
  realv* var_acc_i = var_acc[best_g];
  realv *x = inputs;
  for(int j = 0; j < n_inputs; j++) {
    *var_acc_i++ +=  *x * *x;
    *means_acc_i++ +=  *x++;
  }
}

void DiagonalGMM::frameEMAccPosteriors(int t, realv *inputs, realv log_posterior)
{
  realv log_prob = log_probabilities->frames[t][0];
  realv *p_weights_acc = weights_acc;
  realv *lp_i = log_probabilities_g->frames[t];
  realv *log_w_i = log_weights;
  for (int i=0;i<n_gaussians;i++) {
    realv post_i = exp(log_posterior + *log_w_i++ + *lp_i++ - log_prob);
    *p_weights_acc++ += post_i;
    realv* means_acc_i = means_acc[i];
    realv* var_acc_i = var_acc[i];
    realv *x = inputs;
    for(int j = 0; j < n_inputs; j++) {
      *var_acc_i++ += post_i * *x * *x;
      *means_acc_i++ += post_i * *x++;
    }
  }
}

void DiagonalGMM::eMUpdate()
{
  // first the gaussians
  realv* p_weights_acc = weights_acc;
  for (int i=0;i<n_gaussians;i++,p_weights_acc++) {
    if (*p_weights_acc == 0) {
      warning("Gaussian %d of GMM is not used in EM",i);
    } else {
      realv* p_means_i = means[i];
      realv* p_var_i = var[i];
      realv* p_means_acc_i = means_acc[i];
      realv* p_var_acc_i = var_acc[i];
      for (int j=0;j<n_inputs;j++) {
        *p_means_i = *p_means_acc_i++ / *p_weights_acc;
        realv v = *p_var_acc_i++ / *p_weights_acc - *p_means_i * *p_means_i++;
        *p_var_i++ = v >= var_threshold[j] ? v : var_threshold[j];
      }
    }
  }
  // then the weights
  realv sum_weights_acc = 0;
  p_weights_acc = weights_acc;
  for (int i=0;i<n_gaussians;i++)
    sum_weights_acc += *p_weights_acc++;
  realv *p_log_weights = log_weights;
  realv log_sum = log(sum_weights_acc);
  p_weights_acc = weights_acc;
  for (int i=0;i<n_gaussians;i++)
    *p_log_weights++ = log(*p_weights_acc++) - log_sum;
}

void DiagonalGMM::update()
{
  // normalize log_weights
  realv log_sum = LOG_ZERO;
  for (int i=0;i<n_gaussians;i++)
    log_sum = logAdd(log_sum,log_weights[i]);
  for (int i=0;i<n_gaussians;i++)
    log_weights[i] -= log_sum;
}

void DiagonalGMM::eMIterInitialize()
{
  // initialize the accumulators to 0 and compute pre-computed value
  for (int i=0;i<n_gaussians;i++) {
    realv *pm = means_acc[i];
    realv *ps = var_acc[i];
    realv *v = var[i];
    realv *sum = &sum_log_var_plus_n_obs_log_2_pi[i];
    *sum = n_inputs * LOG_2_PI;
    realv *mh_i = minus_half_over_var[i];
    for (int j=0;j<n_inputs;j++) {
      *pm++ = 0.;
      *ps++ = 0.;
      *mh_i++ = -0.5 / *v;
      *sum += log(*v++);
    }
    *sum *= -0.5;
    weights_acc[i] = prior_weights;
  }
}

void DiagonalGMM::iterInitialize()
{
}

void DiagonalGMM::frameBackward(int t, realv *f_inputs, realv *beta_, realv *f_outputs, realv *alpha_)
{
  realv log_prob = log_probabilities->frames[t][0];
  realv *lp_i = log_probabilities_g->frames[t];
  realv *lw = log_weights;
  realv* dlw = dlog_weights;
  for (int i=0;i<n_gaussians;i++,lw++,lp_i++) {
    realv post_i =  *alpha_ * exp(*lw + *lp_i - log_prob);
    *dlw++ += post_i;
    realv *dlw2 = dlog_weights;
    realv *lw2 = log_weights;
    for (int j=0;j<n_gaussians;j++) {
      *dlw2++ -= post_i * exp(*lw2++);
    }
    realv* obs = f_inputs;
    realv* means_i = means[i];
    realv* dmeans_i = dmeans[i];
    realv* var_i = var[i];
    realv* dvar_i = dvar[i];
    for (int j=0;j<n_inputs;j++,var_i++,obs++,means_i++,dmeans_i++,dvar_i++) {
      realv xmuvar = (*obs - *means_i) / *var_i;
      //realv xmuvar = (*obs - *means_i);
      realv dm = post_i * 2. * xmuvar;
      *dmeans_i += dm;
      *dvar_i += post_i * 0.5 * (xmuvar*xmuvar - 1./ *var_i);
    }
  }
}

void DiagonalGMM::frameDecision(int t, realv *decision)
{
  realv *obs = decision;
  for (int i=0;i<n_inputs;i++) {
    *obs++ = 0;
  }
  realv *lw = log_weights;
  for (int i=0;i<n_gaussians;i++) {
    obs = decision;
    realv *means_i = means[i];
    realv w = exp(*lw++);
    for (int j=0;j<n_inputs;j++) {
      *obs++ += w * *means_i++;
    }
  }
}

void DiagonalGMM::setNGaussians(int n_gaussians_)
{
  n_gaussians = n_gaussians_;
}

DiagonalGMM::~DiagonalGMM()
{
}

}


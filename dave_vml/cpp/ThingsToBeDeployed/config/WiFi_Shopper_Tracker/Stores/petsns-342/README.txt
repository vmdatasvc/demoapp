<< What needs to get from Data team >>

- cam_details.csv containing (where dimension unit for x_pos and y_pos are assumed to be [pixel])
	- camera_name,mac_address,serial_number,IP,x_pos,y_pos,rotation
- Original store layout image and its image size where x_pos and y_pos are calculated from.
- High-res store layout image in jpg or pdf format
- pixeltoft dimension
- camera height


project file location: \\vmvase\Data\Updated Camera Layouts\ahld-6072

The x_pos and y_pos are the pixel coordinates of the cameras in relation to the first floorplan I sent you.
Store length: 350 feet, breadth: 246 feet. Height of cameras : 12' 6"
Original image size that x_pos and y_pos are based on: 2201x1701
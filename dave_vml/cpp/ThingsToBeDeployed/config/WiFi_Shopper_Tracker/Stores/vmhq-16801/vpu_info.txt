< Remote Desktop >

vmhq-16801-01.videomining.com
giant-72-01.videomining.com


Username: vmadmin
Password: g00gle


Be sure to use the local to the machine account, so for vmhq-16801-01, the user name is:
vmhq-16801-01\vmadmin

for giant giant-72-01 it would be:
giant-72-01\vmadmin

< sshCmdBlast >

The sshCmdBlast folder can be found on the desktop near the upper left hand corner
It is currently configured (in the config.json file) to use the commands.json and all_ips.json hosts file
Modify the commands.json file as necessary. To execute the commands, double click the command prompt shortcut in the folder
at the prompt type: python sshscript.py 
An new folder should be created in the logs folder showing the output of the commands run.

# -- usage: [MAC Addr] [OS Type among {Android, iOS, Etc}] 

#a0000000150
#a0000000147
a0000000155
#a0000000076
#a0000000154
#a0000000142

#58:a2:b5:e6:23:74	Android
#64:89:9a:5b:09:04
#64:89:9a:6c:88:e6
#fc:e9:98:4b:ff:47

#6c:0b:84:27:b3:ee	Android	 # -- Dell Venue Tablet
#e0:f5:c6:77:b1:42	iOS		 # -- Apple iPhone 4s (iOS 9)
68:09:27:ab:ce:d6	iOS		 # -- Apple iPhone 4s (iOS 9)
#2c:8a:72:86:ae:66	Android	 # -- HTC Desire

#f8:27:93:7b:ee:d3	iOS		 # -- Apple iPhone 5s (iOS 8)
#2c:f0:ee:47:52:d1	iOS		 # -- Apple iPad
#a8:66:7f:55:11:51	iOS		 # -- Apple iPhone 6 (iOS 9)
#e8:8d:28:0e:44:e2 	iOS		 # -- Apple iPad 2 (iOS 7 or 8)
#18:e7:f4:93:89:32	iOS		 # -- Apple iPod touch
#90:c1:15:ea:f6:0c	Android # -- Sony Xperia phone


#!/bin/bash
# Author: Paul J. Shin
# Instructions to setup a development board for Raspbian Jessie

# << QT -- For Raspbian Jessie >>
sudo apt-get -y install qtcreator	

# << OpenGL >>
sudo apt-get -y install freeglut3-dev mesa-utils

# << OpenCV 3.1.0 >>
# -- Transition Guide: http://docs.opencv.org/master/db/dfa/tutorial_transition_guide.html
sudo apt-get update; sudo apt-get -y upgrade
sudo apt-get -y install g++ build-essential cmake git libgtk2.0-dev pkg-config python-dev python-numpy libdc1394-22 libdc1394-22-dev libjpeg-dev libpng12-dev libtiff5-dev libjasper-dev libavcodec-dev libavformat-dev libswscale-dev libxine2-dev libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev libv4l-dev libqt4-dev libmp3lame-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev libvorbis-dev libxvidcore-dev x264 v4l-utils unzip
cd ~; cd Downloads
wget https://github.com/Itseez/opencv/archive/3.1.0.zip; unzip 3.1.0.zip
wget https://github.com/Itseez/opencv_contrib/archive/3.1.0.zip -O 3.1.0-contrib.zip; unzip 3.1.0-contrib.zip
sudo mv opencv-3.1.0 /opt/opencv-3.1.0
sudo mv opencv_contrib-3.1.0 /opt/opencv_contrib-3.1.0 
cd /opt/opencv-3.1.0
sudo mkdir build; cd build
sudo cmake -D CMAKE_BUILD_TYPE=RELEASE -D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib-3.1.0/modules -D CMAKE_INSTALL_PREFIX=/usr/local -D INSTALL_C_EXAMPLES=ON -D ENABLE_NEON=ON -D ENABLE_VFPV3=ON -D WITH_TBB=ON -D BUILD_TBB=ON -D WITH_GTK=ON -D WITH_V4L=ON -D WITH_OPENGL=ON ..
sudo make -j $(nproc); sudo make install
sudo /bin/bash -c 'echo "/usr/local/lib" > /etc/ld.so.conf.d/opencv.conf'
sudo ldconfig

# << Mosquitto MQTT Broker for Raspberry Pi Jessie  >>
cd ~/Downloads
wget http://repo.mosquitto.org/debian/mosquitto-repo.gpg.key
sudo apt-key add mosquitto-repo.gpg.key
cd /etc/apt/sources.list.d/
sudo wget http://repo.mosquitto.org/debian/mosquitto-jessie.list
sudo apt-get update; sudo apt-get -y install mosquitto     

# << Boost 1.59 >>
sudo apt-get -y install build-essential g++ python-dev autotools-dev libicu-dev build-essential libbz2-dev libboost-all-dev
cd ~; mkdir Downloads; cd Downloads
wget -O boost_1_59_0.tar.gz http://sourceforge.net/projects/boost/files/boost/1.59.0/boost_1_59_0.tar.gz/download
tar xvf boost_1_59_0.tar.gz
cd boost_1_59_0
./bootstrap.sh --prefix=/usr/local
./b2
sudo ./b2 install

# << libcurl and ssh2>>
sudo apt-get install libssh2-1-dev 
cd ~; mkdir Downloads
cd ~/Downloads
wget https://curl.haxx.se/download/curl-7.51.0.tar.gz
tar -xvf curl-7.51.0.tar.gz
cd curl-7.51.0
./configure --with-libssh2=/usr/local/ --disable-shared
make
sudo make install

# << RPi Firmware Roll-back to linux header 4.1.15 for Backports >>
sudo rpi-update 867452b746c440fdc81551030ba5b75feced70fe

# << Backports >>
# -- For packet injection

$ sudo apt-get install linux-headers-$(uname -r)
$ sudo apt-get -f install

#(OR if it doesn't work, then try downloading directly from (https://www.niksula.hut.fi/~mhiienka/Rpi/linux-headers-rpi/))
#
#	- linux-headers-4.1.15+_4.1.15+-2_armhf.deb     
#	- linux-headers-4.1.15-v7+_4.1.15-v7+-2_armhf.deb
#   $ dpkg -i linux-headers-4*.deb

$ cd ~/Downloads
$ wget https://www.kernel.org/pub/linux/kernel/projects/backports/stable/v4.4.2/backports-4.4.2-1.tar.xz
$ tar xvfJ backports-4.4.2-1.tar.xz; cd backports-4.4.2-1
$ sudo make defconfig-wifi
$ sudo make; sudo make install
$ sudo update-initramfs -u
$ reboot

# Redis and Redis client hiredis
sudo apt-get -y install redis-server
sudo apt-get -y install libhiredis-dev

# << VML Repository >>
# -- Clone vml repository
mkdir ~/Repository; cd ~/Repository
# for example,
git clone https://pauljshin@bitbucket.org/pauljshin/vml.git

# -- Setup dlib in vml repository
#cd vml/cpp/external/dlib-18.17/dlib
cd vml/cpp/external/dlib-19.1/dlib
mkdir build; cd build
cmake ..
make -j $(nproc)

# -- Need to build figtree if we just get vml source code from the repository
cd ~/Repository/vml/cpp/external/figtree-0.9.3/
make -j $(nproc)
cd ../../../

# -- Build VML Repository 
cd ~/Repository/vml
./_cpp_completeReBuild.sh

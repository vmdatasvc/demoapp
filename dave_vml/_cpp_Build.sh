#!/bin/bash
# Author: Paul J. Shin

# -- Build
cd build
make -j $(nproc)

# -- Copy config files to build/bin
cp -rf ../cpp/ThingsToBeDeployed/certs bin/
cp -rf ../cpp/ThingsToBeDeployed/config bin/
cd ..

# -- Copy binaries and files to /home/omniadmin/omnisensr-apps/
cp -rf build/bin/* /home/omniadmin/omnisensr-apps/.

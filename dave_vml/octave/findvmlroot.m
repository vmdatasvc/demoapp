## Copyright (C) 2015 Youngrock Yoon
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} findvmlroot (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Youngrock Yoon <yyoon@vmyyoon>
## Created: 2015-10-04

function [root_path] = findvmlroot ()

vmloct_path = fileparts(which('findvmlroot.m'));

root_path = fileparts(vmloct_path);

% Make sure findvmlroot is in the expected location
if ~exist(fullfile(root_path,'octave','findvmlroot.m'),'file')
  error('findvmlroot:pathnotfound', ...
      'Could not find path to vml');
endif

endfunction

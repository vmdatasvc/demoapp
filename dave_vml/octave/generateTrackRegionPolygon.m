## Copyright (C) 2016 Youngrock Yoon
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## generateTrackRegionPolygon (screenshot, savefilename)
##
## @seealso{}
## @end deftypefn

## Author: Youngrock Yoon <yyoon@yyoon-MacPro>
## Created: 2016-02-25

function generateTrackRegionPolygon (screenshot, savefilename)

figure;
if isa(screenshot,'char')
    inputimage = imread(screenshot);
    imshow(inputimage);
else
    imshow(screenshot);
end
hold on;

%% track region
disp('Generating track regions');
track_region = [];
while 1
    disp('Click poly point');
    [x,y,b] = ginput(1);
    track_region = [track_region round([x y]')];

    plot(x,y,'b*');
    if b==3
      plot([track_region(1,end) track_region(1,1)], ...
          [track_region(2,end) track_region(2,1)],'b-');
      break;
    end
    endi = size(track_region,2);
    if endi > 1
      plot([track_region(1,endi) track_region(1,endi-1)], ...
          [track_region(2,endi) track_region(2,endi-1)],'b-');
    end
    
endwhile

track_region = track_region(:);

%{ 
disp('Generating start/kill regions');

start_regions = {};

breakout = false;
while ~breakout

    skpoly = [];
    while 1
        disp('Click poly point');
        [x,y,b] = ginput(1);
        skpoly = [skpoly round([x y]')];

        plot(x,y,'r*');
        if b==3
          break;
        endif
        endi = size(skpoly,2);
        if endi > 1
          plot([skpoly(1,endi) skpoly(1,endi-1)], ...
              [skpoly(2,endi) skpoly(2,endi-1)],'r-');
        endif
    endwhile
    start_regions = [start_regions skpoly(:)];

    reply = input('More start polygons? Y/N [Y]:','s');
    if isempty(reply)
        reply = 'Y';
    endif
    
    if strcmpi(reply,'n')
        breakout = true;
    endif
endwhile
%}

%% print output
fid = fopen(savefilename,'w');
fprintf(fid,'<param name="TrackPolygon" value="');
tstr = sprintf('%d,',track_region);
tstr(end)=''; % remove trailing comma
fprintf(fid,'%s" />\n',tstr);

%{
fprintf(fid,'<folder name="StartPolygons">\n');
for i=1:length(start_regions)
    fprintf(fid,'\t<param name="P%d" value="',i-1);
    tstr = sprintf('%d,',start_regions{i});
    tstr(end)='';
    fprintf(fid,'%s" />\n',tstr);
endfor
fprintf(fid,'</folder>\n');
%}

fclose(fid);

endfunction

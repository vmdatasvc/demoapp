/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * vmloct.hpp
 *
 *  Created on: Oct 3, 2015
 *      Author: yyoon
 */

#ifndef VMLOCT_HPP_
#define VMLOCT_HPP_

#include "octave/oct.h"
#include "octave/dNDArray.h"

//#include "opencv2/core/core.hpp"
//#include "opencv2/imgproc/imgproc.hpp"

#include "core/Image.hpp"

// Prototypes

// Convert between octave array and TImage

// convert to double array
/*
template <typename T>
NDArray	image2NDArray(const TImage<RGBValue<T> > &img);

template <typename T>
NDArray image2NDArray(const TImage<GrayValue<T> > &img);

// convert to correponding type array
uint8NDArray image2Array(const RGBbImage &img);
uint8NDArray image2Array(const GraybImage &img);
uint16NDArray image2Array(const RGBsImage &img);
uint16NDArray image2Array(const GraysImage &img);
*/

// Convert between octave array and cv::Mat
// NOTE: the following methods convert any type T cvMat to double ND array
template <typename T>
void mapCvMat2NDArray(const cv::Mat &mat, NDArray &array)
{
	const int nstep = mat.rows*mat.cols;
	const int nrows = mat.rows;
	
//	assert(array.ndims() == mat.channels());
	dim_vector array_dims = array.dims();
	assert(array_dims(0)==mat.rows);
	assert(array_dims(1)==mat.cols);
	assert(mat.isContinuous());	// only continuous mats can be mapped!
	
	double *array_ptr = array.fortran_vec();
	T* mat_ptr = (T*)mat.data;
	
	for (int i=0;i<mat.rows;i++)
	{
		for (int j=0;j<mat.cols;j++)
		{
			for (int band=0;band<mat.channels();band++)
			{
				double *array_i = array_ptr + band*nstep + j*nrows + i;
				*array_i = (double)*mat_ptr;
				mat_ptr++;
			}
		}
	}
}

NDArray	cvMat2NDArray(const cv::Mat &mat);
//cv::Mat	NDArray2cvMat(NDArray &array);


#endif /* VMLOCT_HPP_ */

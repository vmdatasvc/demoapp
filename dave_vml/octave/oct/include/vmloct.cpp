/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * vmloct.cpp
 *
 *  Created on: Oct 3, 2015
 *      Author: yyoon
 */

#include "vmloct.hpp"

NDArray	cvMat2NDArray(const cv::Mat &mat)
{
	int type = mat.type();
	int nbands = mat.channels();
	int depth = mat.depth();
	
	int nrows = mat.rows, ncols = mat.cols;
	
	NDArray	outputArray(dim_vector(nrows,ncols,nbands));
	switch(depth)
	{
		case CV_8U:
		{
			mapCvMat2NDArray<uchar>(mat,outputArray);
			break;		
		}
		case CV_8S:
		{
			mapCvMat2NDArray<char>(mat,outputArray);
			break;		
		}
		case CV_16U:
		{
			mapCvMat2NDArray<unsigned short>(mat,outputArray);
			break;		
		}
		case CV_16S:
		{
			mapCvMat2NDArray<short>(mat,outputArray);
			break;		
		}
		case CV_32S:
		{
			mapCvMat2NDArray<int>(mat,outputArray);
			break;		
		}
		case CV_32F:
		{
			mapCvMat2NDArray<float>(mat,outputArray);
			break;		
		}
		case CV_64F:
		{
			mapCvMat2NDArray<double>(mat,outputArray);
			break;		
		}
		default:
		{
			error("Unsupported cv::Mat depth for conversion to NDArray!");
		}
	}
	
	return outputArray;
}



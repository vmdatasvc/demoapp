/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

/*
 * octReadRaw.cpp
 *
 *  Created on: Oct 3, 2015
 *      Author: yyoon
 */


#include "vmloct.hpp"

#include "core/Serialize.hpp"

void print_usage()
{
	std::cout << "[raw_mat] = octReadRaw(raw_file_name)" << std::endl;
	return;
}


DEFUN_DLD (octReadRaw, args, nargout, "Read raw data")
{
	octave_value_list	retval;
	
	if (args.length() != 1 || nargout != 1)
	{
		print_usage();
	}
	else {
		charMatrix ch = args(0).char_matrix_value();
		if (!error_state)
		{
			// read file name
			std::string	inputfilename;
			inputfilename = ch.row_as_string(0);	// assume filename has only one row

			// read file
			vml::deserializer	tser;
			tser.load(inputfilename);
			
			cv::Mat	tempMat;
			tser.deserialize_cvMatGen(tempMat);
			
			NDArray	outputArray = cvMat2NDArray(tempMat);
			
			retval(0) = outputArray;
		} // of if(!error_state

	} // of else
	
	return retval;

}

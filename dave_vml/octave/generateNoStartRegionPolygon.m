## Copyright (C) 2016 Youngrock Yoon
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## generateTrackRegionPolygon (screenshot, savefilename)
##
## @seealso{}
## @end deftypefn

## Author: Youngrock Yoon <yyoon@yyoon-MacPro>
## Created: 2016-02-25

function generateNoStartRegionPolygon (screenshot, savefilename)

figure;
if isa(screenshot,'char')
    inputimage = imread(screenshot);
    imshow(inputimage);
else
    imshow(screenshot);
end
hold on;

disp('Generating no start regions');

nostart_regions = {};

breakout = false;
while ~breakout

    skpoly = [];
    while 1
        disp('Click poly point');
        [x,y,b] = ginput(1);
        skpoly = [skpoly round([x y]')];

        plot(x,y,'r*');
        if b==3
          break;
        endif
        endi = size(skpoly,2);
        if endi > 1
          plot([skpoly(1,endi) skpoly(1,endi-1)], ...
              [skpoly(2,endi) skpoly(2,endi-1)],'r-');
        endif
    endwhile
    nostart_regions = [nostart_regions skpoly(:)];

    reply = input('More no start polygons? Y/N [Y]:','s');
    if isempty(reply)
        reply = 'Y';
    endif
    
    if strcmpi(reply,'n')
        breakout = true;
    endif
endwhile

%% print output
fid = fopen(savefilename,'w');
fprintf(fid,'<folder name="NoStartZone">\n');
fprintf(fid,'\t<param name="numPolygons" value="%d"/>\n',length(nostart_regions));
for i=1:length(nostart_regions)
    fprintf(fid,'\t<param name="P%d" value="',i-1);
    tstr = sprintf('%d,',nostart_regions{i});
    tstr(end)='';
    fprintf(fid,'%s" />\n',tstr);
endfor
fprintf(fid,'</folder>\n');

fclose(fid);

endfunction

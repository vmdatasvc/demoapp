## Copyright (C) 2016 Youngrock Yoon
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} show_trajectory (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Youngrock Yoon <yyoon@vmyyoon>
## Created: 2016-11-30

function show_trajectory (csvfile, floorplanimage)

if isa(floorplanimage,'char')
  floorplanimage = imread(floorplanfile);
endif

[st,dur,traj] = read_csv_file(csvfile);

figure;
imshow(floorplanimage);
hold on;

[st,si] = sort(st);
traj = traj(si);
dur = dur(si);

for ti=1:length(traj)
  lineh = plot(traj{ti}(2,:),traj{ti}(3,:),'r-');
  starh = plot(traj{ti}(2,:),traj{ti}(3,:),'b*');
  bh = plot(traj{ti}(2,1),traj{ti}(3,1),'g*');
  eh = plot(traj{ti}(2,end),traj{ti}(3,end),'r*');
  % display start time in hours
  sti = st(ti);
  stsec = mod(sti,60);
  sti = (sti - stsec)/60;
  stmin = mod(sti,60);
  sthour = (sti - stmin)/60;
  fprintf('%2d-%2d-%f\n',sthour,stmin,stsec);
  input('press a key');
  delete(lineh);
  delete(starh);
  delete(bh);
  delete(eh);
end

endfunction

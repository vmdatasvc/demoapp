## Copyright (C) 2016 Youngrock Yoon
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} read_csv_file (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Youngrock Yoon <yyoon@vmyyoon>
## Created: 2016-11-30

function [starttime,duration,trajs] = read_csv_file (csvfile)

% count number of lines first
[status,result] = system(['wc -l ' csvfile]);

[nline] = sscanf(result,'%d');
nline = nline(1)-1;

% trajs = cell(1,nline);
starttime = zeros(1,nline);
duration = zeros(1,nline);
trajs = cell(1,nline);

fid = fopen(csvfile,'r');

ntraj = 1;
% read each line
txtline = fgetl(fid);
while txtline ~= -1
  if ~strcmp(txtline,'Start Time,Duration,Trajectory')
    [st,d,tid,tcoords] = parse_traj_line(txtline);
    starttime(ntraj) = st;
    duration(ntraj) = d;
    %trajs{ntraj} = struct('starttime',starttime,'duration',duration,'id',tid,'coords',tcoords);
    trajs{ntraj} = tcoords;
    ntraj = ntraj+1;    
  end
  txtline = fgetl(fid);  
end

fclose(fid);

endfunction

## Copyright (C) 2016 Youngrock Yoon
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} parse_traj_line (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Youngrock Yoon <yyoon@vmyyoon>
## Created: 2016-11-30

function [starttime,duration,id,coords] = parse_traj_line (trajline)

    [txtstartdate,rem] = strtok(trajline,',');
    % convert start date
    tmstruct = strptime(txtstartdate,'%Y-%m-%dT%k:%M:%S');
    [dummy,mcrosecstr] = strtok(txtstartdate,'.');
    if isempty(mcrosecstr)
      tmstruct.usec = 0;
    else
      tmstruct.usec = str2double(mcrosecstr)*1e+6;
    endif
    %starttime = mktime(tmstruct);
    % calculate time from the hours, min and sec only for better redability
    starttime = (tmstruct.hour-17)*3600+tmstruct.min*60+tmstruct.sec+tmstruct.usec*1e-6;
    [txtduration,rem] = strtok(rem,',');
    % convert duration
    duration = str2double(txtduration);
    [txtid,rem] = strtok(rem,'>');
    id = 0;
    % convert id
    [txtcoords,rem] = strtok(rem,'<');
    txtcoords = txtcoords(2:end);
    coords = str2num(txtcoords)';
    coords = reshape(coords,[3 length(coords)/3]);   

endfunction

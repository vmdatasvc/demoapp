## Copyright (C) 2015 Youngrock Yoon
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} vml_build (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Youngrock Yoon <yyoon@vmyyoon>
## Created: 2015-10-04

function vml_build (varargin)
% compiles oct .cpp source files
%
%  The input argument 'cmd' can be one of the following strings (if 
%  unspecified, 'new' is used):
%
%   'all'   - Force a complete re-build of everything.
%   'clean' - Delete all mex binaries from the locations listed above. 
%             Note that binaries from building matlab_bgl are NOT cleaned.
%   'new'   - Only build those files for which the source is newer than the
%             corresponding mex binary.  For files in 'src', updates to 
%             libtvs, mex/include/*.h, and any external libs used will also
%             trigger a recompile. (Default)
%

vml_root = findvmlroot();
vml_lib_names = {'vmlcore','vmlvision'};
vml_build_root = fullfile(vml_root,'build');
vml_build_oct_root = fullfile(vml_build_root,'oct');
vml_octave_root = fullfile(vml_root,'octave');

% TODO: when vml_ext is setup, use the external dependency
% vml_ext_root = fullfile(vml_root,'../vml_ext');

if ~isdir(vml_build_root)
  mkdir(vml_build_root);
endif

if ~isdir(vml_build_oct_root)
  mkdir(vml_build_oct_root);
endif

if nargin == 0
  cmd = 'new';
else
  cmd = varargin{1};
endif

userFiles = {};

switch(lower(cmd))
  case 'clean'
    printf("Cleaning %s...\n",vml_build_oct_root);
    delete(fullfile(vml_build_oct_root,'*'));
    return;
  case 'all'
    build_all = true;
  case {'updated','new'}
    build_all = false;
  otherwise
    build_all = false;
    userFiles = cmd;
endswitch

userFiles = [userFiles varargin(2:end)];

% assuming C++ libraries are already built in Debug mode
vml_lib_dir = fullfile(vml_build_root,'lib','Debug');

vml_lib_files = cellfun(@(name)fullfile(vml_lib_dir,['lib' name '.a']), ...
    vml_lib_names, 'UniformOutput', false);
    
oct_include_dir = fullfile(vml_octave_root,'oct','include');

% paths
vml_include_dirs = { ...
    fullfile(vml_root,'cpp','include'), ...
    oct_include_dir };
    
vml_lib_dirs = { ...
    vml_lib_dir, ...
    vml_build_oct_root};
    
external_include_dirs = { ...
    '/usr/include', ...  % opencv
    '/usr/local/include', ...% boost
    };
        
external_lib_dirs = {
    '/usr/lib/x86_64-linux-gnu', ... % opencv
    '/usr/local/lib', ...% boost
    };
    
external_lib_names = {'opencv_core','opencv_imgproc','opencv_highgui', ...
    'opencv_video','opencv_calib3d','opencv_features2d','opencv_ml', ...
    'opencv_objdetect',...
    'boost_container','boost_exception','boost_filesystem','boost_graph', ...
    'boost_locale','boost_log','boost_math_c99','boost_math_tr1', ...
    'boost_math_tr1f','boost_math_tr1l','boost_random','boost_serialization', ...
    'boost_signals','boost_system','boost_thread','boost_timer','boost_wave', ...
    'boost_wserialization'};
        
lib_dirs = [vml_lib_dirs external_lib_dirs];
include_dirs = [vml_include_dirs external_include_dirs];
lib_names = [vml_lib_names external_lib_names];

oct_files = getOctFiles(fullfile(vml_octave_root,'oct'),'src',userFiles);

src_files = [dir(fullfile(oct_include_dir,'*.hpp'))];

timestamps = [vertcat(src_files.datenum);];

% add -I to each includeDir, -L to each libDir, and -l to each lib
libDirs = cellfun(@(x)['-L' x],lib_dirs,'UniformOutput',false);
includeDirs = cellfun(@(x)['-I' x],include_dirs,'UniformOutput',false);
libs = cellfun(@(x)['-l' x],lib_names,'UniformOutput',false);

output_dir = vml_build_oct_root;

curr_dir = pwd;

cd(output_dir);

for i=1:length(oct_files)
  try
    if build_all || needsRecompile(oct_files{i}, output_dir, timestamps)
      printf("Building against other libs: %s\n",oct_files{i});
      [~, outFile, outExt] = fileparts(oct_files{i});
      outputFile = fullfile(output_dir,[outFile '.oct']);
      mkoctfile('-v', ...
                includeDirs{:}, ...
                libDirs{:}, ...
                libs{:}, ...
                oct_files{i}, ...
                fullfile(vml_octave_root,'oct','include','vmloct.cpp'));
    endif
  catch %#ok<*CTCH>
    printf("\n*** ERROR caught while building lib dependent oct file :%s\n\n",oct_files{i});
    cd(curr_dir);
    any_failures = true;
  end_try_catch

endfor

cd(curr_dir);

endfunction

%% Helper Functions

function octFiles = getOctFiles(octRoot, subdir, userFiles)

octFiles = getfnames(fullfile(octRoot, subdir), '*.cpp', true);
if ~isempty(userFiles)
	
	% add .cpp extension if not already there
	%
	userFiles = cellfun(@addCppHelper, userFiles, ...
		'UniformOutput', false);
	
	% intersect with discovered mex files:
	octFiles = intersect(octFiles, ...
		cellfun( @(file) fullfile(octRoot, subdir, file), ...
		userFiles, 'UniformOutput', false));
endif

endfunction % of getOctFiles

function file = addCppHelper(file)
	if ~strcmp(file(end-3:end), '.cpp')
		file = [file '.cpp'];
	end
endfunction % of addCppHelper




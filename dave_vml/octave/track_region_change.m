## Copyright (C) 2016 Youngrock Yoon
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} track_region_change (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Youngrock Yoon <yyoon@vmyyoon>
## Created: 2016-08-23

function track_region_change (tp, imdim)

ncols = length(tp)/2;
nrows = 2;

tpr = reshape(tp,[nrows ncols]);

imdim = imdim(:);
 tpr = tpr./imdim;
 tp = reshape(tpr,[1 length(tp)]);
 fprintf('\n');
 fprintf('%f,',tp);
 fprintf('\n\n');
endfunction

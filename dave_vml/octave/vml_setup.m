## Copyright (C) 2015 Youngrock Yoon
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} vml_setup (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Youngrock Yoon <yyoon@vmyyoon>
## Created: 2015-10-04

function vml_setup ()

vml_root = findvmlroot();

addpath(genpath(fullfile(vml_root,'octave'),'include','src')); % exclude oct include and src

% Add oct binary directory
octBinDir = fullfile(vml_root,'build','oct');
if ~isdir(octBinDir)
  mkdir(octBinDir);
endif

addpath(octBinDir);

endfunction

function gb = gabor(sigma,b,theta_deg,gamma,psi,nstds)
% Create a single Gabor filter kernel.
%
% g = gabor(sigma,b,theta_deg,gamma,psi,nstds)
%
% sigma : Specifies the width of the major axis of the gaussian envelope.
%         Note: If gamma>=1, then sigma specifies the std of the x-axis 
%         (before rotation) else it specifies the std of the y-axis
% b     : Specifies the frequency of the cosine portion of the filter. The 
%         wavelength is related to sigma as w = 6*sigma / b. That is, 
%         there will be b wavelengths contained within 6 stds of the 
%         Gaussian envelope.
% theta : Angle of the gabor filter (in degrees)
% gamma : Specifies the eccentricity of the Gaussian envelope. For 
%         gamma > 1, the x-axis (before rotation) will be longer; for 
%         gamma < 1, the y-axis will be longer.
% psi   : Phase shift of the cosine curve (in radians)
% nstds : Specifies where to truncate the filter. Default is 3, which will 
%         include pixels up to 3 stds on either side of the Gaussian 
%         envelope.
%
% Note: Formerly texture/gabor_fn2.m
%
% See also: gabor_filter_bank
%
% ------------
% Neil Alldrin
%

if ~exist('nstds','var')
    nstds = 3;
end

if gamma < 1
    sigma = sigma*gamma;
end

sigma_x = sigma;
sigma_y = sigma/gamma;

lambda = 6*sigma/b;
theta = theta_deg*pi/180;

% Figure out bounds on x and y
xmax = max(abs(nstds*sigma_x*cos(theta)),abs(nstds*sigma_y*sin(theta)));
xmax = ceil(max(1,xmax));

ymax = max(abs(nstds*sigma_x*sin(theta)),abs(nstds*sigma_y*cos(theta)));
ymax = ceil(max(1,ymax));

% xmax = max(1,ceil(nstds * sigma/sqrt(cos(theta)^2 + gamma^2*sin(theta)^2)));
% ymax = max(1,ceil(nstds * sigma/sqrt(sin(theta)^2 + gamma^2*cos(theta)^2)));

xmin = -xmax; ymin = -ymax;
[x,y] = meshgrid(xmin:xmax,ymin:ymax);

% sz_x=fix(6*sigma_x);
% if mod(sz_x,2)==0, sz_x=sz_x+1;end
%
%
% sz_y=fix(6*sigma_y);
% if mod(sz_y,2)==0, sz_y=sz_y+1;end
%
% [x y]=meshgrid(-fix(sz_x/2):fix(sz_x/2),fix(-sz_y/2):fix(sz_y/2));

% Rotation
x_theta=x*cos(theta)+y*sin(theta);
y_theta=-x*sin(theta)+y*cos(theta);

gb=exp(-.5*(x_theta.^2/sigma_x^2+y_theta.^2/sigma_y^2)) .* ...
    cos(2*pi/lambda*x_theta+psi);

% Subtract the mean so that the DC component of the filter is 0
gb = gb - mean(gb(:));


function csvwrite_str(csvfile,C,varargin)
% Write cell array of strings to a comma separated file.
%
%  csvwrite_str(csvfile, C, <options>)
%
% csvfile    : Output file.
% C          : ncols x nrows Cell array of strings
%               OR
%              1 x ncols Cell array containing each column of data
%
% Options are specified as name / value pairs. Valid options are,
%  fmt        : Format string (can be used, e.g., to handle numeric data).
%               Default: %s for strings, %f for float data, %d for other numeric data
%  fieldnames : If provided (and non-empty), the first line of the csv file
%               will contain the provided field names.
%               Default: []
%
% ============
% Neil Alldrin
%

% Parse varargin name/value pairs
validopts = {'fmt','fieldnames'};
fmt = [];
fieldnames = [];
for i = 1:2:length(varargin)
	if ~ismember(varargin{i},validopts)
		temp = cellfun(@(x)([' ',x,'\n']),validopts,'UniformOutput',false);
		error(['Unknown option specified, valid options are,\n', ...
			temp{:}]);
	else
		eval(sprintf('%s = varargin{i+1};',varargin{i}));
	end
end

% If C is a cell of column values, then convert to an nrows x ncols cell array
if ~isscalar(C{1})
	% Convert numeric arrays to cell arrays
	cols = find(~cellfun(@iscell,C));
	for col = cols(:)'
		C{col} = num2cell(C{col});
	end
	% Horzcat the columns
	C = [C{:}];
end

[nrows,ncols] = size(C);

if isempty(fmt)
	% Create format string for fprintf
	fmt = repmat({'%d'},[1,ncols]);
	fmt(cellfun(@ischar,C(1,:))) = {'%s'};
	fmt(cellfun(@isfloat,C(1,:))) = {'%f'};
	
	fmt = [fmt(:)'; repmat({','},[1,ncols])];
	fmt{2,end} = '\n';
	fmt = [fmt{:}];
	%fmt = ['%s',repmat(',%s',[1,ncols-1]),'\n'];
end

% Transpose C so that the order is correct when passed to fprintf
C = C';

% Open csv file
fid = fopen(csvfile,'w');
cleanup = onCleanup(@()(fclose(fid)));

% Write to file
if ~isempty(fieldnames)
	fprintf(fid, ['%s',repmat(',%s',[1,ncols-1]),'\n'], fieldnames{:});
end
fprintf(fid,fmt,C{:});

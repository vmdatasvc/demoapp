function [g, dg, d2g, d3g] = gaussian_kernel_2D(sigma, theta, K)
%
% [G, <dG>, <d2G>, <d3G>] = myGaussian2D([xsigma ysigma], theta, <K>)
%
%  Computes a Gaussian distribution over a grid.  You may specify the
%  standard deviations (xsigma and ysigma) in the x and y directions (or just
%  a scalar 'sigma' if you want it to be rotationally symmetric), an
%  orientation angle (theta, defaults to zero), and a number of standard 
%  deviations to contain within the window (K, defaults to 3).  
%
%  Can also return the 1st, 2nd, and 3rd derivatives of the Gaussian, 
%  w.r.t to x, if requested.
% 
%  Note that the windows size will be K times the maximum of the two
%  standard deviations.  Also, if you specify a vector for K, such as
%  [-3:.1:3], the Gaussian will be computed over a square window at those
%  coordinates.
%
% See also: gaussian_kernel
% --------
% Andrew Stein
%

% Not implemented yet:
%
% [G, dG, d2G] = myGaussian2D([nrows ncols], <K>)
%
%  This will generate a [nrows x ncols] Gaussian which captures +/-
%  K*xsigma (or ysigma).  This is useful for returning a weighting mask for a 
%  given patch size.  K defaults to 1 here.
%

if nargin<3
	K = 3;
end

xsigma = sigma(1);
if isscalar(sigma)
	ysigma = sigma;
else
	ysigma = sigma(2);
end

if length(K)==1
	win_halfwidth = ceil(K*max(xsigma,ysigma));
	[xgrid,ygrid] = meshgrid(-win_halfwidth : win_halfwidth);
	win_width = 2*win_halfwidth + 1;
else
	[xgrid,ygrid] = meshgrid(K);
	win_width = length(K);
end

X = [xgrid(:) ygrid(:)]';

if theta ~= 0
	c = cos(theta);
	s = sin(theta);
	R = [c s; -s c];
	X = R*X;
end

xsigma2 = xsigma^2;
g = exp(-1/2 * (X(1,:).^2 / xsigma2  + X(2,:).^2 / ysigma^2));

% 1st Derivative, dG/dx:
if nargout==0 || nargout >= 2
    dg = -X(1,:)/(2*xsigma2) .* g;
    %dg = reshape(dg / sum(dg(xgrid>0)), [win_width, win_width]);
    dg = reshape(dg / sum(abs(dg)), [win_width win_width]);
end

% 2nd Derivative, d2G/dx2:
if nargout==0 || nargout >=3
    d2g = (X(1,:).^2/(xsigma^4) - 1/xsigma2) .* g;
    d2g = reshape(d2g / sum(abs(d2g)), [win_width win_width]);
end

% 3rd Derivative
if nargout==0 || nargout>=4
    d3g = (3*X(1,:)/(xsigma^4) - X(1,:).^3/(xsigma^6)) .* g;
	d3g = reshape(d3g / sum(abs(d3g)), [win_width win_width]);
end

g = reshape(g/sum(g), [win_width win_width]);

if nargout==0
    subplot 141
	imagesc(xgrid([1 end]), ygrid([1 end]), g), axis image
    subplot 142
    imagesc(xgrid([1 end]), ygrid([1 end]), dg), axis image
    subplot 143
    imagesc(xgrid([1 end]), ygrid([1 end]), d2g), axis image
	subplot 144
    imagesc(xgrid([1 end]), ygrid([1 end]), d3g), axis image
end
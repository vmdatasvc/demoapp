function [C,fieldnames] = csvread_str(csvfile,varargin)
% Read a CSV file into a cell array of strings.
%
%  C = csvread_str(csvfile, <options>)
%
% Input / Output,
%  csvfile : Input file.
%  C       : 1 x ncols Cell array containing each column of data
%
% Options are specified as name / value pairs. Valid options are,
%  fmt        : Format string (can be used, e.g., to specify numeric data).
%               Default: all string values '%s%s%s...'
%  header     : If true, the first row will be considered field names (and removed from C).
%               Default: false
%  tostruct   : If true, C will be converted to a struct array with field names taken
%               from the header. (Note: header must be true to enable this option).
%               Default: false
%  fieldnames : Specifies the name of each column. If unspecified, fieldnames are drawn
%               from the header (if present) or will be set to 'field1', 'field2', etc.
%  onlyheader : If true, only the first line will be processed. (default false)
%
% ============
% Sam Kwak
% Neil Alldrin
%

% Parse varargin name/value pairs
validopts = {'fmt','header','tostruct','fieldnames','onlyheader'};
fmt = [];
header = false;
tostruct = false;
onlyheader = false;
for i = 1:2:length(varargin)
	if ~ismember(varargin{i},validopts)
		temp = [{sprintf('\n')},cellfun(@(x)([' ',x,sprintf('\n')]),validopts,'UniformOutput',false)];
		error(['Unknown option specified, valid options are,', ...
			temp{:}]);
	else
		eval(sprintf('%s = varargin{i+1};',varargin{i}));
	end
end

if tostruct && ~(header || exist('fieldnames','var'))
	error('To return a struct array, fieldnames need to be specified or the csv file needs to contain a header row.');
end

% Open csv file
fid = fopen(csvfile,'r');
cleanup = onCleanup(@()(fclose(fid)));

% Pull out the first line
line1 = fgetl(fid);
line1_vals = textscan(line1, '%s', 'delimiter', ',');

if isempty(fmt)
	fmt = repmat('%s',[1,numel(line1_vals{1})]);
end

if ~exist('fieldnames','var')
	if header
		fieldnames = line1_vals{1}(:)';
	else
		fieldnames = arrayfun(@(x)(sprintf('field%d',x)),[1:numel(line1_vals{1})],'UniformOutput',false);
	end
end

if onlyheader
	
	% Only read in first line
	if header
		C = textscan(line1, repmat('%s',[1,numel(line1_vals{1})]), 'delimiter', ',');
	else
		C = textscan(line1, fmt, 'delimiter', ',');
	end
	
else
	
	% Read in rest of the file
	C = textscan(fid, fmt, 'delimiter', ',');
	
	% Merge with first line (if no header)
	if ~header
		line1_fmt_vals = textscan(line1, fmt, 'delimiter', ',');
		C = cellfun(@(a,b)([a;b]),line1_fmt_vals,C, ...
			'UniformOutput',false);
	end
	
end

% Convert to struct if requested by user
if tostruct
	% Textscan puts numeric data into arrays, convert these to cell arrays
	cols = find(~cellfun(@iscell,C));
	for col = cols(:)'
		C{col} = num2cell(C{col});
	end
	
	% Create struct
	structargs = [fieldnames(:), C(:)]';
	C = struct(structargs{:});
end

function files = getfnames(parent_dir, pattern, use_full_path, ...
    exclude_pattern, recursive)
% Simple function for getting filenames matching a pattern in a directory.
%
% filenames = getfnames(<parent_dir>, <pattern>, <use_full_path>,  ...
%    <exclude_pattern>, recursive)
% 
%  This is just a wrapper for Matlab's 'dir' command to make getting a list
%  of files from a directory easier.
%
%  If parent_dir is unspecified, the current working directory is used.
%
%  If pattern is unspecified, pattern defaults to '*.*'.  A cell array of
%  patterns can be specified and files matching any of those patterns will
%  all be returned, e.g. {'*.jpg', '*.tif', '*.png'}.  If the special
%  string 'images' is used, then common image formats will be used:
%    *.jp(e)g, *.tif(f), *.png, *.pgm, *.ppm, *.bmp
%
%  If use_full_path is true, then the filenames will include the path as
%  well.  Default is false.
%
%  A pattern for excluding files can also be specified, analogously to the
%  matching pattern.  If the special-case string 'hidden' is used, then
%  hidden files (i.e. those whose filename begins with '.') will be
%  excluded (which may be more efficient than specifying '.*' as the
%  exclude pattern).
%
%  If recursive is set true, it will recursively traverse all
%  subdirectories of the parent_dir to search for the files matching the
%  pattern
%
%
% See also: addpathname, getdirnames
% -------------
% Andrew Stein
%

if nargin < 5 || isempty(recursive)
    recursive = false;
end

if nargin < 4
    exclude_pattern = [];
end

if nargin < 3 || isempty(use_full_path)
	use_full_path = false;
end

if nargin < 2 || isempty(pattern)
    pattern = {'*.*'};
elseif ischar(pattern) && strcmpi(pattern, 'images')
	pattern = {'*.jpg', '*.jpeg', '*.tif', '*.tiff', ...
		'*.png', '*.pgm', '*.ppm', '*.bmp', '*.exr', ...
        '*.cr2','*.dng','*.raf','*.fff','*.mrw', ...
        '*.nef','*.orf','*.rw2'}; % raw images
	pattern = [pattern cellfun(@upper, pattern, 'UniformOutput', false)];
elseif ~iscell(pattern)
	pattern = {pattern};
end

if nargin < 1 || isempty(parent_dir)
    parent_dir = pwd;
end

files = cell(size(pattern));
for i_pattern = 1:length(pattern)
	files{i_pattern} = dir(fullfile(parent_dir, pattern{i_pattern}));
	files{i_pattern} = {files{i_pattern}(~[files{i_pattern}.isdir]).name}';
end

files = vertcat(files{:});

if use_full_path
	files = addpathname(parent_dir, files);
end

if ~isempty(exclude_pattern)
	
	if strcmpi(exclude_pattern, 'hidden')
		% This ought to be faster for this special case, since it doesn't
		% require a second call to getfnames
		first_char = cellfun(@(x)x(1), files);
		files = files(first_char ~= '.');
	else
		% Slower, but more general:
		exclude_files = getfnames(parent_dir, exclude_pattern, ...
            use_full_path);
		files = setdiff(files,exclude_files);
	end
end

tempflist = {};
if recursive
    % find sub directories 
    tempdirs = dir(parent_dir);
    tempdirs = {tempdirs([tempdirs.isdir]).name}';
    % remove '.' and '..'
    excludedirs = cellfun(@(x)(strcmp(x,'.') || strcmp(x,'..')),tempdirs);
    tempdirs = tempdirs(~excludedirs);
    
    % check exclude pattern
    if strcmpi(exclude_pattern,'hidden')
        excludedirs = cellfun(@(x)(x(1)=='.'),tempdirs);
        tempdirs = tempdirs(~excludedirs);
    else
        excludedirs = dir(exclude_pattern);
        tempdirs = setdiff(tempdirs,excludedirs);
    end
    for d_i = 1:length(tempdirs)
        tempflist = [tempflist; getfnames(fullfile(parent_dir, ...
            tempdirs{d_i}),pattern,use_full_path,exclude_pattern, ...
            recursive)];
    end
end

files = [files; tempflist];


return

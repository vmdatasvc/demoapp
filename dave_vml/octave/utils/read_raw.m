function mat = read_raw(filename)
% a wrapper for mexReadRawMat
%
%

if ~isa(filename,'char')
    error('Expecting file name string');
end

if ~exist(filename,'file')
    error('%s does not exist',filename);
else
    mat = octReadRaw(filename);
end
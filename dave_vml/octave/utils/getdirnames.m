function dirs = getdirnames(root_path, pattern, use_full_path)
% Helper function to get a list of directory names, excluding '.' and '..'
%
% dirs = getdirnames(<root_path>, <pattern>, <use_full_path>)
%
%   Return a cell array of directory names found in the given 'root_path'
%   which match the specified pattern.   This is just a simple 
%   wrapper around Matlab's "dir" command.
%
%   If unspecified, 'root_path' defaults to the current directory.  
%
%   If unspecified, 'pattern' default to '*' (everything) and the current 
%   and parent directories, "." and "..", are automatically excluded from 
%   the returned list. 
%
%   If 'use_full_path' is true (default is false), then the root path will
%   be added to each directory name so that each entry in the returned list
%   is the full path.
%
% See also: getfnames, addpathname
% ------------
% Andrew Stein
% 

if nargin == 0 || isempty(root_path)
    root_path = '.';
end

if nargin < 2 || isempty(pattern)
    pattern = '*';
end

if nargin < 3 || isempty(use_full_path)
    use_full_path = false;
end

dirs = dir(fullfile(root_path, pattern));
dirs = {dirs([dirs.isdir]).name};

if strcmp(pattern, '*')
    dirs(1:2) = []; % get rid of '.' and '..' entries
end

if use_full_path
	dirs = addpathname(root_path, dirs);
end

end % function
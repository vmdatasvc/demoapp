function fullfilenames = addpathname(pathname, filenames)
% Add a specified path to a cell array of filenames.
%
%  fullfilenames = addpathname(pathname, filenames)
%
%    Prefixes each filename in the cell array 'filenames' with the path
%    string in 'pathname'.
%
%    This is a simple wrapper around Matlab's CELLFUN.
%
% ------------
% Andrew Stein
%

if ~iscell(filenames)
	error('Filenames argument should be a cell array of filenames.');
elseif ~ischar(pathname)
	error('Pathname should be a string specifying a path.');
end

fullfilenames = cellfun(@(x) fullfile(pathname, x), filenames, 'UniformOutput', false);
function hText = xticklabel_rotate(XTick, XTickLabels, varargin)
%XTICKLABEL_ROTATE90 - Rotate numeric Xtick labels 
%
% Syntax: hText = xticklabel_rotate(<XTick>, <XTickLabels>, <TextPropertyParameters...>)
%
% Input:  XTick - vector array of XTick positions 
%         XTickLables - optional cell array of string labels
%         Text Parameter Name/Value pairs to apply to the labels.
%
%     If neither is provided, the ticks/labels from the current axes are
%     used.  If only XTick is provided, then the XTick values themselves
%     are used as the labels.
%
%     Default rotation is 90 degrees, but other angles can be specified by
%     simply using the 'Rotation' property as one of the optional
%     TextPropertyParameter inputs.
%
% Output:  hText - handles to the newly-created text objects
%


% Example 1:  Set the positions of the XTicks and rotate them
%    figure;  plot([1960:2004],randn(45,1)); xlim([1960 2004]);
%    xticklabel_rotate90([1960:2:2004]);
%    %If you wish, you may set a few text "Property-value" pairs
%    xticklabel_rotate90([1960:2:2004],'Color','m','Fontweight','bold');
%
% Example 2:  %Rotate XTickLabels at their current position
%    XTick = get(gca,'XTick');
%    xticklabel_rotate90(XTick);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: TEXT,  SET

% Major Revision: Andrew Stein

% Original Author: Denis Gilbert, Ph.D., physical oceanography
% Maurice Lamontagne Institute, Dept. of Fisheries and Oceans Canada
% email: gilbertd@dfo-mpo.gc.ca  Web: http://www.qc.dfo-mpo.gc.ca/iml/
% February 1998; Last revision: 24-Mar-2003

if nargin < 2 
	XTickLabels = [];
	if nargin < 1
		XTick = [];
	end
end

if isempty(XTick)
	if isempty(XTickLabels)
		XTickLabels = get(gca, 'XTickLabel');
	end
	XTick = get(gca, 'XTick');
end
XTick = XTick(:);

if isempty(XTickLabels)
	XTickLabels = num2str(XTick);
end

set(gca, 'XTick', XTick, 'XTickLabel', '');
	
% Determine the location of the labels based on the position
% of the xlabel
hxLabel = get(gca,'XLabel');  % Handle to xlabel
xLabelString = get(hxLabel,'String');

if ~isempty(xLabelString)
   warning('You may need to manually reset the XLABEL vertical position') %#ok<WNTAG>
end

set(hxLabel,'Units','data');
xLabelPosition = get(hxLabel,'Position');
y = xLabelPosition(2)*ones(length(XTick),1);

% Place the new xTickLabels by creating TEXT objects
hText = text(XTick, y, XTickLabels,'FontSize',get(gca, 'FontSize'));

% Rotate the text objects by 90 degrees
set(hText,'Rotation',90,'HorizontalAlignment','right',varargin{:})

if nargout==0
	clear hText
end


function cmap_new = shuffle_colormap(input, zero_color)
% Shuffle a figure's colormap, e.g. to make token ID map visualization easier.
%
%  shuffle_colormap(<handle>, <preserve_zero_color>)
%
%    Shuffles the colormap of the figure/axes corresponding to 'handle'.  
%    If no handle is provided, the current figure is used.  If 
%    'preserve_zero_color' is true (default is false), the first value in 
%    the colormap will not be shuffled.
%
%
%  shuffle_colormap(cmap, <zero_color>)
%
%    If a colormap is provided as input, that colormap is shuffled and
%    then used as the current colormap.  A color to use for "zero" can also
%    be specified, either as a 3-element RGB vector or a color character,
%    from 'rgbcmyk'.  The first entry in the colormap will then be this
%    color, irrespective of the shuffling, meaning that the minimum value
%    of any image (not just zero) will use this color.
%
%
%  cmap_shuffled = shuffle_colormap( ... )
%  
%    If an output is requested, the shuffled colormap is returned and the
%    colormap of the current figure is *not* changed.
%
% ---------
%  Andrew Stein
%

% Input argument checking:
if nargin==0
    % No arguments: use current figure's colormap, don't do anything with
    % zero color.
    cmap = colormap;
    zero_color = [];
else
    if isscalar(input) && ishandle(input)
        % User provided figure handle.  Use that figure's colormap.  Assume
        % zero color is not to be preserved if no 2nd argument given.
        switch(get(input, 'Type'))
            case 'figure'
                % Get the current axes of the given figure
                input = gca(input);
            case 'axes'
                % do nothing, axes handle provided directly
            otherwise
                error('Input handle must be a figure or axes.');
        end

        cmap = colormap(input);
        
        if nargin < 2 
            zero_color = [];
        elseif ~islogical(zero_color)
            error('"preserve_zero_color", if specified, should be true or false.');
        end
        
    elseif size(input,2)==3
        % Colormap provided.  If no second argument, don't do anything with
        % zero color.
        cmap = input;
        
        if nargin < 2
            zero_color = [];
        end
            
    elseif islogical(input) 
        % One logical argument.  Assume this is whether or not to preserve
        % the zero color.  Use the colormap of the current figure.
        cmap = colormap;
        zero_color = input;
        
    else
        error('Input should be a figure handle or a colormap.');
    end
end

if islogical(zero_color) && zero_color == false
    zero_color = [];
end

if ~isempty(zero_color)
    if ischar(zero_color)
        color_char = lower(zero_color(1));
        zero_color = [0 0 0];
        
        if color_char ~= 'k'
            if( any(color_char == 'rmyw') )
                zero_color(1) = 1;
            end
            if( any(color_char == 'gcyw') )
                zero_color(2) = 1;
            end
            if( any(color_char == 'bcmw') )
                zero_color(3) = 1;
            end
        end
    elseif islogical(zero_color) && zero_color==true
        zero_color = cmap(1,:);
        cmap(1,:) = [];
    else
        error('Unrecognized zero-color request.');
    end
end

% This is the only line that actually does the shuffling:
cmap = cmap(randperm(size(cmap,1)),:);


% If a zero color is specified, use it (an empty zero color will do 
% nothing here):
cmap = [zero_color; cmap];


% Return colormap as output if requested, otherwise just adjust current figure's
% colormap.
if nargout==0
    colormap(cmap);
else
    cmap_new = cmap;
end

   
function result = needsRecompile(srcfilename, binDir, otherdatenums, binext)
% Returns true if a source file is newer than mex binary.
%
% result = needsRecompile(srcfilename, binDir, <otherdatenums>, <binext>)
%
%   Returns true if source file is newer than the corresponding mex binary
%   file found in specified binDir (or the binary doesn't yet exist).  
%   E.g., compares someFile.cpp to binDir/someFile.mexmaci64.
%
%   If an optional array of other datenums is provided (e.g. from other 
%   manually-specified dependencies), result will also be true if any of 
%   those datenums is newer than the mex binary.
%
%   By default, the binary extension compared against is whatever is
%   returned by the "mexext" command.  You can override this using you own
%   string for the optional "binext" argument (no "." needed).
%
% See also: tvs5_build, datenum, dir
% ------------
% Andrew Stein
% 

[~,basename] = fileparts(srcfilename);

if nargin < 4
	binext = mexext;
end

binfilename = fullfile(binDir, [basename '.' binext]);

if ~exist(binfilename, 'file')
	% If binary file doesn't exist at all, return true.
	result = true;
else
	% Return true if source file's date is newer than corresponding binary
	% file's date.
	srcfile = dir(srcfilename);
	binfile = dir(binfilename);
	result = (srcfile.datenum > binfile.datenum);
	
	if nargin > 2 && ~isempty(otherdatenums)
		% If provided, check to see if libtvs is newer than existing binary
		result = result || any(otherdatenums > binfile.datenum);
	end
end

end % FUNCTION needsRecompile
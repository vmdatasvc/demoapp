#NOTE:To access the OmniData repository using the OmniSensrOS repo, there is a script "scripts/modules/clone_data.sh" that will clone 
#the OmniData repo (matching branch) into data/content.  Currently, it only works for the release/version-1.82 branch.
clone VML repo
clone omnisensros repo
clone omnidata repo
cd omnisensros/scripts/modules
sudo find . -name "*.sh" -exec chmod +x {} \;
cd ../../ec2/scripts
sudo find . -name "*.sh" -exec chmod +x {} \;

cd ..
sudo apt-get install -y cmake
sudo apt-get install -y libboost-all-dev
sudo apt-get install -y realpath
sudo add-apt-repository ppa:ubuntu-toolchain-r/test 
sudo apt-get update
sudo apt-get -y upgrade
sudo apt-get -y dist-upgrade
sudo chmod +x setup.sh
sudo ./setup.sh


cd ../../vml
sudo add-apt-repository ppa:mosquitto-dev/mosquitto-ppa
sudo apt-get install -y $(sed 's/#.*//' apt-get_dependencies.txt | tr '\n' ' ')
sudo mkdir build
cd build
sudo cmake -DVML_TARGET=ec2 -DCMAKE_BUILD_TYPE=Debug ../cpp
#use DESKTOP for local machine instead of ec2
make #for everything 
make Face_Postprocessor # for just FACE
sudo chmod +x Face_Postprocessor

#When we use the in-house module for joining broken tracks, just use the argument  option �p jbtvml.  
#Regarding new location of model data and configuration, recently we separate the data repository including models and configuration files from vml repository. The data repository is named as omnidata.   So, you need to copy the proper data for running the Face_Postprocessor from omnidata repo. Also, put the corresponding models and a config file to proper location.  The location should be followed as 
#create a config folder and Face_Postprocessor subfolder and then do the following.
# -Models:
o   From:    omnidata/models/ml
o   To:   <path>/config/Face_Postprocessor/ml
# -config file:         
o   From:    omnidata/config/Face_Postprocessor/confgFPP.xml
o   To:   <path>/config/Face_Postprocessor/FacePPConfig.xml
#At this point the Face_Postprocessor should work
Example:
./Face_Postprocessor -v -p dmg -b 20170328T0100 -e 20170328T0600 -s giant_6072 -d fac007 -t US/Eastern



#for ec2 machine with ubuntu runtime ami already created
clone vml:  git clone https://vmdatasvc@bitbucket.org/videomining/vml.git
cd /vml
sudo apt-get install -y $(sed 's/#.*//' apt-get_dependencies.txt | tr '\n' ' ')
mkdir build
cd build
sudo cmake -DVML_TARGET='ec2' -DCMAKE_BUILD_TYPE=Release ../cpp
sudo make Face_Postprocessor
sudo chmod +x ./Face_Postprocessor
Examples:
./Face_Postprocessor -v -p dmg -b 20170328T0100 -e 20170328T0600 -s giant_6072 -d fac007 -t US/Eastern
./Face_Postprocessor -v -p jbt -b 20170328T0100 -e 20170328T0600 -s giant_6072 -d fac007 -t US/Eastern
./Face_Postprocessor -v -p all -b 20170328T0100 -e 20170328T0600 -s giant_6072 -d fac007 -t US/Eastern -o 0
==== Notes for Face_Postprocessor argument

-p dmg -o 0    (dmg process in facxxx_demogphcs table (data from Omnisensr)
-p dmg -o 1    (dmg process in facxxx_demogphcs_pp table (data after broken track joining) 
                at this time, how do you select the face (Currently, select a face at the very initial 	
	 			time of tracks, think about it later more!)

-p jbt 			(jpt process results in joined meta data in facxxx_demogphcs_pp table and matched ids 
                with confidence in facxxx_id_map table) 

[-h | --help] print this message

[-v | --verbose] verbose (default: off)

[-p | --pp] post processing with an option among dmg, jbt, and all (default: all)

[-b | --sdate] start date

[-e | --edate] end date

[-s | --store] store name

[-d | --device] device name

[-t | --timezone] store timezone such as US/Central, US/Eastern, US/East-Indiana, US/Pacific (default: US/Eastern)
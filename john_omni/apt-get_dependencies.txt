libpcap0.8
libudev1
libssl1.0.0
libssh2-1
libhiredis-dev # silly developers putting the version name in the package! grumble...
libmysqlcppconn-dev # see above grumble
libjpeg-dev # Debian is a little crazy with their libjpeg in Jessie...
libtiff5
libjasper1
libpng12-0
librtmp1
libcurl4-openssl-dev
libldap-2.4-2
libopenblas-base
liblapack3
libgstreamer1.0-0
libdc1394-22

libmosquitto1 # From mosquitto PPA
libmosquittopp1 # From mosquitto PPA
mosquitto-clients # From mosquitto PPA

mysql-client
redis-server
ntpstat
bc
lsof
vim # Make John happy

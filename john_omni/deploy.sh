#!/bin/bash


case "$1" in
    third-party-libs)

		# If we asked to build, please build all libraries
		
		if test "$2" = "build"; then
			# here we want to build the opencv
			# build and install all 3rd party libs
			./scripts/modules/build_libraries.sh
		fi
		
		# make /usr/local if it does not exist
		if [ ! -d /usr/local ] ; then
			sudo mkdir /usr/local
		fi
		
		# For each external library in the repo
		for d in $(ls external); do
			# If the precompiled directory is empty for some reason...
			if test -z "$(ls ./scripts/apps/third-party-libs/$d)"; then
				# build and install
				./scripts/modules/$d.sh
			fi
			# Just install the prebuilt external library (and any others)
			echo "Installing $d to /usr/local"
			sudo cp -R --remove-destination ./scripts/apps/third-party-libs/$d/* /usr/local
			
		done
		
		# Make sure to modify ldconfig appropriately
		echo "/usr/local/lib" | sudo tee /etc/ld.so.conf.d/OmniSensr.conf >/dev/null
		sudo ldconfig
		
		;;
    lighttpd)
        #Remove old sysV style 
        sudo update-rc.d lighttpd disable
        sudo rm -f /etc/init.d/lighttpd
        sudo update-rc.d -f lighttpd remove
    
        # Copying config files for lighttpd
        sudo cp -R --remove-destination scripts/apps/lighttpd/etc/lighttpd/* /etc/lighttpd
        # Ensure omniadmin:omniadmin owns the log directory for lighttpd and any files that may be present
        if [ ! -d /var/log/lighttp ]; then
            sudo chown -R omniadmin:omniadmin /var/log/lighttpd
        fi
        # Copying new systemctl unit file
        sudo cp --remove-destination scripts/apps/lighttpd/lib/systemd/system/* /lib/systemd/system/
        sudo systemctl daemon-reload
        sudo systemctl enable lighttpd.service
        sudo systemctl restart lighttpd.service
        ;;
    mjpg-streamer)
    
    	# Make sure to modify ldconfig appropriately
		echo "/usr/local/lib/mjpg-streamer" | sudo tee /etc/ld.so.conf.d/mjpg-streamer.conf >/dev/null
		sudo ldconfig
    
    	# create a link from /usr/local/www to /usr/local/share/mjpg-streamer/www
    	if [ -d /usr/local/www ]; then
    		sudo mv /usr/local/www /usr/local/www.bak
    	fi
    	sudo ln -s /usr/local/share/mjpg-streamer/www /usr/local/www
    	
    	# Create the /usr/local/script directory, if needed
        if [ -f /usr/local/script ]; then
        	sudo mv /usr/local/script /usr/local/script.bak
        fi

        if [ ! -d /usr/local/script ]; then
            sudo mkdir /usr/local/script
        fi
        
        # Move our scripts into the necessary locations
        sudo cp --remove-destination scripts/apps/mjpg-streamer/usr/local/script/start_cam.sh /usr/local/script/start_cam.sh
        sudo cp --remove-destination scripts/apps/mjpg-streamer/usr/local/www/* /usr/local/www
       
       
       	# Create the systemd service and enable and start it
       	sudo cp scripts/apps/mjpg-streamer/lib/systemd/system/OmniSensr_Service_Camera.service /lib/systemd/system/OmniSensr_Service_Camera.service
               
        sudo systemctl daemon-reload
        sudo systemctl enable OmniSensr_Service_Camera.service
        sudo systemctl start OmniSensr_Service_Camera.service
        sudo systemctl status OmniSensr_Service_Camera.service

        ;;
    sniffer)
    
    	
        if [ ! -d /usr/local/script ]; then
            sudo mkdir /usr/local/script
        fi    
        sudo cp --remove-destination scripts/apps/sniffer/start_sniffer.sh /usr/local/script/start_sniffer.sh
        # no need to auto start start_sniffer.sh. The WiFi service will run start_sniffer before running the exectuable
        sudo /usr/local/script/start_sniffer.sh
        ;;
    wifitrack-processor)
    
        #create config/WiFi_Shopper_Tracker/Stores directory if not exists
        if ! [ -d /home/omniadmin/omnisensr-apps/config/WiFi_Shopper_Tracker/Stores ]; then
            sudo mkdir -p /home/omniadmin/omnisensr-apps/config/WiFi_Shopper_Tracker/Stores
        fi
        
        # copy config files to /home/omniadmin/omnisensr-apps/config/WiFi_Shopper_Tracker
        cp --remove-destination scripts/apps/omnisensr-apps/config/WiFi_Shopper_Tracker/* /home/omniadmin/omnisensr-apps/config/WiFi_Shopper_Tracker
        
        # change ownership appropriately
        sudo chown -R omniadmin:omniadmin /home/omniadmin/omnisensr-apps/

        if [ ! -d /usr/local/script ]; then
            sudo mkdir /usr/local/script
        fi
        sudo cp --remove-destination scripts/apps/track-processor/usr/local/script/start_WiFi_TrackProcessing.sh /usr/local/script/start_WiFi_TrackProcessing.sh
        # copy the service to init.d
        sudo cp --remove-destination scripts/apps/track-processor/lib/systemd/system/OmniSensr_Service_WifiTrack.service /lib/systemd/system/OmniSensr_Service_WifiTrack.service
        sudo systemctl daemon-reload
        ;;
    health-app)
    
        #create config/OmniSensr_Health_App directory if not exists
        if ! [ -d /home/omniadmin/omnisensr-apps/config/OmniSensr_Health_App ]; then
            sudo mkdir -p /home/omniadmin/omnisensr-apps/config/OmniSensr_Health_App
        fi
        # copy config files to /home/omniadmin/omnisensr-apps/config/OmniSensr_Health_App
        sudo cp --remove-destination scripts/apps/omnisensr-apps/config/OmniSensr_Health_App/* /home/omniadmin/omnisensr-apps/config/OmniSensr_Health_App
        
        # change ownership appropriately
        sudo chown -R omniadmin:omniadmin /home/omniadmin/omnisensr-apps/

        if [ ! -d /usr/local/script ]; then
            sudo mkdir /usr/local/script
        fi
        sudo cp --remove-destination scripts/apps/health-app/usr/local/script/OmniSensr_Health.sh /usr/local/script/
        sudo chmod +x /usr/local/script/OmniSensr_Health.sh
        # copy the service to init.d
        sudo cp --remove-destination scripts/apps/health-app/lib/systemd/system/OmniSensr_Service_Health.service /lib/systemd/system/
        sudo systemctl daemon-reload
        sudo systemctl enable OmniSensr_Service_Health.service
        ;;
    aws-iot-connector)

        #create config/OmniSensr_Health_App directory if not exists
        if ! [ -d /home/omniadmin/omnisensr-apps/config/OmniSensr_AWS_IoT_Connector ]; then
            sudo mkdir -p /home/omniadmin/omnisensr-apps/config/OmniSensr_AWS_IoT_Connector
        fi
        # copy config files to /home/omniadmin/omnisensr-apps/config/OmniSensr_Health_App
        sudo cp --remove-destination scripts/apps/omnisensr-apps/config/OmniSensr_AWS_IoT_Connector/* /home/omniadmin/omnisensr-apps/config/OmniSensr_AWS_IoT_Connector
        
        # change ownership appropriately
        sudo chown -R omniadmin:omniadmin /home/omniadmin/omnisensr-apps/

        if [ ! -d /usr/local/script ]; then
            sudo mkdir /usr/local/script
        fi
        sudo cp --remove-destination scripts/apps/iot-connector/usr/local/script/OmniSensr_AWS_IoT_Connector.sh /usr/local/script/

        # copy the service to init.d
        sudo cp --remove-destination scripts/apps/iot-connector/lib/systemd/system/OmniSensr_Service_AWS_IoT.service /lib/systemd/system/
        sudo systemctl daemon-reload
        sudo systemctl enable OmniSensr_Service_AWS_IoT.service
        ;;
    demographics-app)
    
        #create config/OmniSensr_DMG_Recognizer directory if not exists
        if ! [ -d /home/omniadmin/omnisensr-apps/config/OmniSensr_DMG_Recognizer ]; then
            mkdir -p /home/omniadmin/omnisensr-apps/config/OmniSensr_DMG_Recognizer
        fi
        # copy config files to /home/omniadmin/omnisensr-apps/config/OmniSensr_DMG_Recognizer
        cp --remove-destination scripts/apps/omnisensr-apps/config/OmniSensr_DMG_Recognizer/* /home/omniadmin/omnisensr-apps/config/OmniSensr_DMG_Recognizer

        # unzip ml.zip config files to /home/omniadmin/omnisensr-apps/config/OmniSensr_DMG_Recognizer/ml
        unzip -o scripts/apps/demographics-app/config/OmniSensr_DMG_Recognizer/ml.zip -d /home/omniadmin/omnisensr-apps/config/OmniSensr_DMG_Recognizer

        # change ownership appropriately
        sudo chown -R omniadmin:omniadmin /home/omniadmin/omnisensr-apps/

        if [ ! -d /usr/local/script ]; then
            sudo mkdir /usr/local/script
        fi
        #copy the start scripts for the OmniSensr_DMG_Recognizer        
        sudo cp --remove-destination scripts/apps/demographics-app/usr/local/script/start_demographics.sh /usr/local/script/
        sudo cp --remove-destination scripts/apps/demographics-app/usr/local/script/start_ramdisk.sh /usr/local/script/
        # copy the service to init.d
        sudo cp --remove-destination scripts/apps/demographics-app/lib/systemd/system/OmniSensr_Service_Demogrph.service /lib/systemd/system/
        sudo systemctl daemon-reload
        ;;
    vision-app)

        #create config/OmniSensr_Vision_Tracker directory if not exists
        if ! [ -d /home/omniadmin/omnisensr-apps/config/OmniSensr_Vision_Tracker ]; then
            sudo mkdir -p /home/omniadmin/omnisensr-apps/config/OmniSensr_Vision_Tracker
        fi
        # copy config files to /home/omniadmin/omnisensr-apps/config/OmniSensr_Vision_Tracker
        sudo cp --remove-destination scripts/apps/omnisensr-apps/config/OmniSensr_Vision_Tracker/* /home/omniadmin/omnisensr-apps/config/OmniSensr_Vision_Tracker

        # change ownership appropriately
        sudo chown -R omniadmin:omniadmin /home/omniadmin/omnisensr-apps/

		# helper scripts
        if [ ! -d /usr/local/script ]; then
            sudo mkdir /usr/local/script
        fi
        sudo cp --remove-destination scripts/apps/vision-app/usr/local/script/OmniSensr_Vision.sh /usr/local/script/

        # copy the service to init.d
        sudo cp --remove-destination scripts/apps/vision-app/lib/systemd/system/OmniSensr_Service_Vision.service /lib/systemd/system/
        sudo systemctl daemon-reload
        ;;
    packet-processor)

        #create config/WiFi_PacketProcessor directory if not exists
        if ! [ -d /home/omniadmin/omnisensr-apps/config/WiFi_PacketProcessor ]; then
            sudo mkdir -p /home/omniadmin/omnisensr-apps/config/WiFi_PacketProcessor
        fi
        # copy config files to /home/omniadmin/omnisensr-apps/config/WiFi_PacketProcessor
        sudo cp --remove-destination scripts/apps/omnisensr-apps/config/WiFi_PacketProcessor/* /home/omniadmin/omnisensr-apps/config/WiFi_PacketProcessor
        
        # change ownership appropriately
        sudo chown -R omniadmin:omniadmin /home/omniadmin/omnisensr-apps/
        
        
        # Additional helper scripts
        if [ ! -d /usr/local/script ]; then
            sudo mkdir /usr/local/script
        fi
        # copy the start script to /usr/local/script
        if [ ! -d /usr/local/script ]; then
            sudo mkdir /usr/local/script
        fi
        sudo cp --remove-destination scripts/apps/packet-processor/usr/local/script/start_WiFi_PacketProcessing.sh /usr/local/script/start_WiFi_PacketProcessing.sh

        # copy the service to init.d
        sudo cp --remove-destination scripts/apps/packet-processor/lib/systemd/system/OmniSensr_Service_Wifi.service /lib/systemd/system/
        sudo systemctl daemon-reload
        ;;
    beacon)
        ;;
    qa-test)
        if [ ! -d /home/omniadmin/QA_test ]; then
            sudo mkdir /home/omniadmin/QA_test
        fi
        sudo cp --remove-destination scripts/apps/QA_test/_test_WiFi_n_Bluetooth.sh /home/omniadmin/QA_test/_test_WiFi_n_Bluetooth.sh
        sudo chown -R omniadmin:omniadmin /home/omniadmin/QA_test
        ;;
    mqtt)
	# Optional TODO: any MQTT configuration customization (i.e., password, TLS, etc.)

        sudo systemctl enable mosquitto
        sudo systemctl restart mosquitto
        ;;
    blacklist)
    	# No blacklisting using next branch of firmware
        ;;
    omnisensr-apps)
    	# Copy binaries into their location
        if ! [ -d /home/omniadmin/omnisensr-apps ]; then
            sudo mkdir /home/omniadmin/omnisensr-apps
        fi
        
        sudo cp --remove-destination -R scripts/apps/omnisensr-apps/* /home/omniadmin/omnisensr-apps/
        
        sudo chown -R omniadmin:omniadmin /home/omniadmin/omnisensr-apps
        ;;
    *)
        echo "input parameter is missing"
        exit 1
        ;;
esac

#!/bin/bash

red="$(tput setaf 1)"
bgr="$(tput setab 7)"
reset="$(tput sgr 0)"

function gen_local_ver {
    # get version tags from repo and save it to os_version.txt in /usr/local/www
    # this version will be available at http://<ip>/os_version.txt
    currDir=$(pwd)
    OMNI_REPO_HOME=$(< /omnisensr-runners/repo_home.txt)
    cd $OMNI_REPO_HOME

    TMPF=$(mktemp)
    sudo git describe --abbrev=0 --tags > $TMPF

    # Don't check for existence of /usr/local/www - this should happen with mjpg-streamer install
    if [ -f /usr/local/www/version.txt ]; then
        sudo rm -rf /usr/local/www/version.txt
    fi
    sudo mv $TMPF /usr/local/www/version.txt
    cd $currDir
}

case "$1" in
    #-------------------------------------------------+
    #                third-party-libs                 |
    #-------------------------------------------------+
    third-party-libs)
        printf "\n\n\n"
        # Deploying Third-Party-Libraries
        echo "${red}${bgr}Installing Boost...${reset}"
        sudo apt-get install -y libboost-all-dev

        echo "${red}${bgr}Deploying third party libraries...${reset}"
        # Pass the second argument through
        ./deploy.sh "third-party-libs" "$2"
        echo "${red}${bgr}Done${reset}"
    ;;
    #-------------------------------------------------+
    #                lighttpd                         |
    #-------------------------------------------------+
    lighttpd)
        printf "\n\n\n"
        echo "${red}${bgr}Installing lighttpd server...${reset}"
        # Installing lighttpd
        sudo apt-get install -y lighttpd
        sudo lighty-enable-mod cgi        
        echo "${red}${bgr}Done${reset}"

        # deploying cgi scripts
        echo "${red}${bgr}Deploying lighttpd${reset}"
        ./deploy.sh "lighttpd"
        echo "${red}${bgr}Done${reset}"
        printf "\n\n\n"
    ;;
    #-------------------------------------------------+
    #              mjpg-streamer                      |
    #-------------------------------------------------+
    mjpg-streamer)
        printf "\n\n\n"

        # deploying cgi scripts
        echo "${red}${bgr}Deploying cgi scripts${reset}"
        ./deploy.sh "mjpg-streamer"
        echo "${red}${bgr}Done${reset}"
        printf "\n\n\n"
        ;;
    #-------------------------------------------------+
    #        multi-channel sniffer                    |
    #-------------------------------------------------+
    sniffer)
        printf "\n\n\n"
        echo "${red}${bgr}setting up OmniSensr as a multi-channel sniffer${reset}"
        ./scripts/modules/multi_channel_sniffer.sh
        echo "${red}${bgr}Done${reset}"
	./deploy.sh sniffer
        printf "\n\n\n"
        ;;
    #-------------------------------------------------+
    #   bond Wi-Fi interfaces to a single interface   |
    #-------------------------------------------------+
    bond-interfaces)
        printf "\n\n\n"
        echo "${red}${bgr}Installing ifenslave${reset}"
        sudo apt-get -y install ifenslave-2.6
        echo "${red}${bgr}Copying bonding.conf${reset}"
        sudo cp --remove-destination scripts/apps/sniffer/bonding.conf /etc/modprobe.d/bonding.conf
        echo "${red}${bgr}Setting up network interfaces${reset}"
        sudo cp --remove-destination scripts/modules/interfaces /etc/network/interfaces
        # restart networking
        echo "${red}${bgr}Re-start networking${reset}"
        sudo /etc/init.d/networking restart
        # start sniffing
        echo "${red}${bgr}deploying sniffer${reset}"
        ./deploy.sh "sniffer"
        echo "${red}$bgr}Done${reset}"
        printf "\n\n\n"
        ;;
    #-------------------------------------------------+
    #              Packet Processor                   |
    #-------------------------------------------------+
    packet-processor)
        echo "${red}${bgr}Installing custom VM software${reset}"
        printf "\n\n\n"
        echo "${red}${bgr}Deploying custom VM software${reset}"
        ./deploy.sh "packet-processor"
        echo "${red}${bgr}Done${reset}"
        printf "\n\n\n"
        ;;
    #-------------------------------------------------+
    #              Wifitrack Processor                |
    #-------------------------------------------------+
    wifitrack-processor)
        echo "${red}${bgr}Installing custom VM software${reset}"
        # ------------------------------------+
        printf "\n\n\n"
        echo "${red}${bgr}Deploying custom VM software${reset}"
        ./deploy.sh "wifitrack-processor"
        echo "${red}${bgr}Done${reset}"
        printf "\n\n\n"
        ;;
    #-------------------------------------------------+
    #              Health App                         |
    #-------------------------------------------------+
    health-app)
        echo "${red}${bgr}Installing custom VM software${reset}"
        # ------------------------------------+
        printf "\n\n\n"
        echo "${red}${bgr}Deploying custom VM software${reset}"
        ./deploy.sh "health-app"
        echo "${red}${bgr}Done${reset}"
        printf "\n\n\n"
        ;;
    #-------------------------------------------------+
    #              Vision App                         |
    #-------------------------------------------------+
    vision-app)
        echo "${red}${bgr}Installing custom VM software${reset}"
        # ------------------------------------+
        printf "\n\n\n"
        echo "${red}${bgr}Deploying custom VM software${reset}"
        ./deploy.sh "vision-app"
        echo "${red}${bgr}Done${reset}"
        printf "\n\n\n"
        ;;
    #-------------------------------------------------+
    #              Demographics App                   |
    #-------------------------------------------------+
    demographics-app)
        echo "${red}${bgr}Installing custom VM software${reset}"
        # ------------------------------------+
        printf "\n\n\n"
        echo "${red}${bgr}Deploying custom VM software${reset}"
        ./deploy.sh "demographics-app"
        echo "${red}${bgr}Done${reset}"
        printf "\n\n\n"
        ;;
    #-------------------------------------------------+
    #              AWS IoT Connector                   |
    #-------------------------------------------------+
    aws-iot-connector)
        echo "${red}${bgr}Installing custom VM software${reset}"
        # ------------------------------------+
        printf "\n\n\n"
        echo "${red}${bgr}Deploying custom VM software${reset}"
        ./deploy.sh "aws-iot-connector"
        echo "${red}${bgr}Done${reset}"
        printf "\n\n\n"
        ;;
    #-------------------------------------------------+
    #              Bluetooth Beacon                   |
    #-------------------------------------------------+
    beacon)
        printf "\n\n\n"
        echo "${red}${bgr}Installing BlueZ${reset}"
        sudo apt-get install -y bluez
        
        sudo ./scripts/modules/beacon.sh
        echo "${red}${bgr}Done${reset}"
        printf "\n\n\n"
        ;;
    #-------------------------------------------------+
    #              QA test script                     |
    #-------------------------------------------------+
    qa-test)
        printf "\n\n\n"
        echo "${red}${bgr}Deploying qa test script${reset}"
        ./deploy.sh "qa-test"
        echo "${red}${bgr}Done${reset}"
        printf "\n\n\n"
        ;;
    #-------------------------------------------------+
    #              System config                      |
    #-------------------------------------------------+
    system)
        printf "\n\n\n${red}${bgr}Configuring system...${reset}\n"
        printf "\t${red}${bgr}Deploying logging config script${reset}\n"
        # conf file for logrotate
        sudo cp --remove-destination ./scripts/modules/raspPi /etc/logrotate.d/raspPi
        # conf file for logrotate of gateway log
        sudo cp --remove-destination ./scripts/modules/gateway /etc/logrotate.d/gateway
        
        #-------------------------------+
        #    Application Environment    |
        #-------------------------------+
        printf "\t${red}${bgr}Installing packages to set up application environment...${reset}\n"
        printf "\t${red}${bgr}Installing MQTT${reset}\n"
        
        ./scripts/modules/mqtt.sh
        
        printf "\t${red}${bgr}Done${reset}\n"
        
        echo "${red}${bgr}Deploying mqtt unit files and script${reset}"
        ./deploy.sh "mqtt"
        printf "\t${red}${bgr}Done${reset}\n"
        
        echo -e "\t${red}${bgr}Installing apt-get dependencies${reset}"

        sudo apt-get install -y $(sed 's/#.*//' apt-get_dependencies.txt | tr '\n' ' ')
        sudo apt-get install -y $(sed 's/#.*//' apt-get_dependencies-pi.txt | tr '\n' ' ')

        printf "\t${red}${bgr}Done${reset}\n"

        echo "${red}${bgr}Deploying Pi3 Blacklist unit files and script${reset}"
        ./deploy.sh "blacklist"
        printf "\t${red}${bgr}Done${reset}\n"

        # Use rpi-update to get next branch of firmware!
        printf "\t${red}${bgr}Downloading and Installing new firmware${reset}\n"
        sudo apt-get install -y rpi-update
        sudo BRANCH=next rpi-update
        printf "\t${red}${bgr}Done${reset}\n"
        
        #-------------------------------+
        printf "\n"
        printf "${red}${bgr}Done${reset}\n"
        ;;
    #-------------------------------------------------+
    #           gateway-upgrade                       |
    #-------------------------------------------------+
    #      *ONLY* to be called from gateway.sh        |
    # if any system modules are installed here, add   |
    # them to "system" etc                            |
    # For new installs, kickoff will call other       |
    # sections and if they are complete and up to date|
    # nothing gets missed                             |
    #-------------------------------------------------+
    gateway-upgrade)
        printf "\n${red}${bgr}Done${reset}\n"
        ;;
    #-------------------------------------------------+
    #           updating binaries of apps             |
    #-------------------------------------------------+
    omnisensr-apps)
        printf "\n\n\n"
        echo "${red}${bgr}Updating binaries in ~/omnisensr-apps/ ${reset}"
        ./deploy.sh "omnisensr-apps"
        echo "${red}${bgr}Done${reset}"
        printf "\n\n\n"
        ;;
    *)
        printf "\n\n\n"
        echo "${red}${bgr}Usage: config.sh <component>${reset}"
        printf "\n\n\n"
        exit 1
        ;;
esac

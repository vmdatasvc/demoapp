#!/bin/bash

function timestamp() {
  local dt=$(date +"%D %T")
  echo "$dt"
}

function start_Wifi {
    ts=$(timestamp)
    printf "%s: starting WifiTracker service" "$ts" | sudo tee -a /var/log/raspPi.log
    cd /home/omniadmin/omnisensr-apps;./WiFi_Shopper_Tracker &
}
function stop_Wifi {
    ts=$(timestamp)
    printf "%s: stopping WifiTracker service" "$ts" | sudo tee -a /var/log/raspPi.log
	
    sudo killall WiFi_Shopper_Tracker
}

case "$1" in
  start)
    stop_Wifi
    start_Wifi
    ;;
  stop)
    stop_Wifi
    ;;
  restart)
    stop_Wifi
    start_Wifi
    ;;
  *)
    echo "Usage: sudo systemctl {start|stop|restart|status} OmniSensr_Service_WifiTrack.service";
    exit 1
    ;;
esac
exit 0
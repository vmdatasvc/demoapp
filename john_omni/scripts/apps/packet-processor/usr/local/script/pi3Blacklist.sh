#!/bin/bash
REV=`cat /proc/cpuinfo | grep Revision | cut -d ':' -f 2 | sed -e "s/ //g"|cut -c3-`
if [ $REV -eq 2082 ]; then
echo "Pi3 Blacklister" >> /var/log/raspPi.log
        DO_SYNC = 0
	BLKLIST[0]=0
	BLKLIST[1]=0
	BLKLIST[2]=0
	BLKLIST[3]=0
	BLKLISTVAL[0]='brcmfmac'
	BLKLISTVAL[1]='brcmutil'
	BLKLISTVAL[2]='btbcm'
	BLKLISTVAL[3]='hci_uart'
  if [ -a /etc/modprobe.d/raspi-blacklist.conf ]; then
      NUM=0
      while [ $NUM -lt 4  ];do
        if grep -iq "blacklist ${BLKLISTVAL[$NUM]}" /etc/modprobe.d/raspi-blacklist.conf; then
          BLKLIST[$NUM]=1
        fi
      NUM=$(expr $NUM + 1)
      done
  fi
  NUM=0
  while [ $NUM -lt 4  ];do
    if (( BLKLIST[$NUM]==0 )); then
      echo "blacklist ${BLKLISTVAL[$NUM]}" | sudo tee --append /etc/modprobe.d/raspi-blacklist.conf
      echo "Add blacklist ${BLKLISTVAL[$NUM]}">> /var/log/raspPi.log
      DO_SYNC = 1

    fi
  NUM=$(expr $NUM + 1)
  done
    if (( BLKLIST[0]==1 && BLKLIST[1]==1 && BLKLIST[2]==1 && BLKLIST[3]==1)); then
      echo "No changes required">> /var/log/raspPi.log
    else
      echo "write cache to SDCard">> /var/log/raspPi.log
      sudo sync
    fi
fi

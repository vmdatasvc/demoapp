#!/bin/bash

function timestamp() {
  local dt=$(date +"%D %T")
  echo "$dt"
}

function start_Wifi {
    ts=$(timestamp)
    printf "%s: starting Wifi service" "$ts" | sudo tee -a /var/log/raspPi.log
    sudo /usr/local/script/start_sniffer.sh
    cd /home/omniadmin/omnisensr-apps;./WiFi_PacketProcessor &
}
function stop_Wifi {
    ts=$(timestamp)
    printf "%s: stopping Wifi service" "$ts" | sudo tee -a /var/log/raspPi.log
	
    sudo killall WiFi_PacketProcessor
}

case "$1" in
  start)
    stop_Wifi
    start_Wifi
    ;;
  stop)
    stop_Wifi
    ;;
  restart)
    stop_Wifi
    start_Wifi
    ;;
  *)
    echo "Usage: sudo systemctl {start|stop|restart|status} OmniSensr_Service_Wifi.service";
    exit 1
    ;;
esac
exit 0
import sys
import RPi.GPIO as GPIO
def switchLED(channel, val):
	GPIO.setup(channel, GPIO.OUT)
	GPIO.output(channel, val)

def switchSet(red_channel, green_channel, color):
	if color == "red":
		switchLED(red_channel, 1)
		switchLED(green_channel, 0)
	elif color == "green":
		switchLED(red_channel, 0)
		switchLED(green_channel, 1)
	elif color == "orange":
		switchLED(red_channel, 1)
		switchLED(green_channel, 1)
	elif color == "off":
		switchLED(red_channel, 0)
		switchLED(green_channel, 0)

def main():
	GPIO.setmode(GPIO.BCM)
	GPIO.setwarnings(False)
	led_set = int(sys.argv[1])
	color = sys.argv[2]
	if led_set == 0:
		switchSet(4, 5, color)
	elif led_set == 1:
		switchSet(6, 7, color)
	elif led_set == 2:
		switchSet(8, 9, color)

if __name__ == "__main__":
	main()

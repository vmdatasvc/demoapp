#!/bin/bash
# set -x

red="$(tput setaf 1)"
bgr="$(tput setab 7)"
reset="$(tput sgr 0)"

NUM=0

LISTEN_TIME_PERIOD=4

STATE_MON[0]=0
STATE_MON[1]=0
STATE_MON[2]=0
STATE_BOND0=0
STATE_MQTT=0
STATE_BEACON=0

SUDO_CMD="sudo"
if [ $(id -u) -eq 0 ]; then
    SUDO_CMD=""
fi

function test_bluetooth {

    # -- Check if Bluetooth dongle is working
    HCICONFIG=hciconfig
    echo "${red}${bgr}HCICONFIG=$HCICONFIG${reset}"
    HCI_DEV=$($HCICONFIG | grep hci | grep USB | head -1 | sed 's/:/ /' | awk '{print $1}')
    echo "${red}${bgr}HCI_DEV=$HCI_DEV${reset}"

    $SUDO_CMD $HCICONFIG $HCI_DEV up
    $SUDO_CMD $HCICONFIG $HCI_DEV noleadv
    $SUDO_CMD $HCICONFIG $HCI_DEV leadv3
    $SUDO_CMD $HCICONFIG $HCI_DEV noscan
    RUNNING=$($HCICONFIG $HCI_DEV | grep RUNNING | awk '{print $2}')
    if [ "x$RUNNING" == "xRUNNING" ]; then
            printf "\n\n"
            echo "${red}${bgr}Beacon is working.${reset}"
            printf "\n\n"
            STATE_BEACON=1
    else
            printf "\n\n"
            echo "${red}${bgr}Beacon is NOT working.${reset}"
            printf "\n\n"
    fi


    # -- Make start transmitting beacon signals.
    # -- Use Beacon Toolkit app (https://itunes.apple.com/us/app/beacon-toolkit/id728479775) on an iPad/iPhone/iPod to detect these beacon signals
    HCITOOL=$(which hcitool)
    echo $($SUDO_CMD $HCITOOL -i $HCI_DEV cmd 0x08 0x0008 1E 02 01 1A 1A FF 4C 00 02 15 E2 0A 39 F4 73 F5 4B C4 A1 2F 17 D1 AD 07 A9 61 00 00 00 00 C8 00 )
}

function test_mqtt {
    # -- Check if Wi-Fi data streaming to a cloud-based MQTT broker is working
    SN=$($SUDO_CMD cat /usr/local/www/serial_number.txt | grep Serial | sed 's/:/ /' | awk '{print $2}')
    echo "${red}${bgr}Serial Number = $SN${reset}"
    echo "${red}${bgr}Capturing MQTT messages ....${reset}"
    LISTEN_TIME_PERIOD=$(expr $LISTEN_TIME_PERIOD + 3)
    mosquitto_sub -h m12.cloudmqtt.com -u fmzkerph -P tA1YWQIM4FUP -p 17831 -t "VM/+/$SN" & 
    MOSQ_PID=$!
    sleep ${LISTEN_TIME_PERIOD}s
    kill -9 $MOSQ_PID
    RESULT=$(echo $?)
    if [ $RESULT -eq 0 ]; then
            printf "\n\n"
            echo "${red}${bgr}MQTT messaging is working${reset}"
            printf "\n\n"
            STATE_MQTT=1
    else
            printf "\n\n"
            echo "${red}${bgr}MQTT messaging is NOT working${reset}"
            printf "\n\n"
    fi
}

function test_wifiSniffer {
    # -- Check if Wi-Fi sniffers (i.e., mon0, mon1, and mon2) are working
    printf "\n\n${red}${bgr}Testing WiFi Sniffer on mon0, mon1, mon2 interfaces.....${reset}\n"
    while [ $NUM -lt 3  ];do
        $SUDO_CMD tcpdump -n -e -tttt -vvv -i mon$NUM -s 32 > /dev/null 2>&1 & 
	TCP_PID=$!
	sleep ${LISTEN_TIME_PERIOD}s 
	$SUDO_CMD kill -9 $TCP_PID
        RESULT=$(echo $?)
        if [ $RESULT -eq 0 ]; then
            printf "\n\n"
            echo "${red}${bgr}mon$NUM is working${reset}"
            printf "\n\n"
            STATE_MON[$NUM]=1
        else
            printf "\n\n"
            echo "${red}${bgr}mon$NUM is NOT working${reset}"
            printf "\n\n"
        fi
        NUM=$(expr $NUM + 1)
    done
}
function checkMons {
# -- Check for presence of mon0, mon1, and mon2
NUMMONS=0
NUMBONS=0
printf "\n\n${red}${bgr}Testing mon0, mon1, mon2 interfaces.....${reset}\n"
ifconfig | \
(while read i
do
  if [[ $i =~ .*bond0.* ]];
    then
      echo "${red}${bgr}Found bond0 ${reset}"
      NUMBONS=$(expr $NUMBONS + 1)
  fi
  if [[ $i =~ .*mon.* ]];
    then
      mons=($i)
      echo "${red}${bgr}Found ${mons[0]} ${reset}"
      NUMMONS=$(expr $NUMMONS + 1)
  fi
done

if [ "$NUMMONS" -ne "3"  ] || [ "$NUMBONS" -ne "1" ];
  then
    echo "${red}${bgr}Missing interfaces! Restarting Wifi Packet Processing service${reset}"
    $SUDO_CMD /usr/local/script/start_sniffer.sh
    $SUDO_CMD airmon-ng
fi)

printf "${red}${bgr}Done${reset}\n"
}

function test_bondedInterface {
    # -- Check if bond0 is working
    $SUDO_CMD tcpdump -n -e -tttt -vvv -i bond0 -s 32 > /dev/null 2>&1 & 
    TCP_PID=$!
    sleep ${LISTEN_TIME_PERIOD}s 
    $SUDO_CMD kill -9 $TCP_PID
    RESULT=$(echo $?)
    if [ $RESULT -eq 0 ]; then
            printf "\n\n"
            echo "${red}${bgr}bond0 is working.${reset}"
            printf "\n\n"
            STATE_BOND0=1
    else
            printf "\n\n"
            echo "${red}${bgr}bond0 is NOT working.${reset}"
            printf "\n\n"
    fi
}

function test_awsConnectivity {
    mosquitto_sub --cafile ~/omnisensr-apps/certs/rootCA.pem --cert ~/omnisensr-apps/certs/certificate.pem.crt --key ~/omnisensr-apps/certs/private.pem.key --insecure -q 1 -d -h A9BFTZDLDJ0ZJ.iot.us-east-1.amazonaws.com -p 8883 -I test_sub_102 -v -t data/# & sleep 10s && pkill -HUP mosquitto_sub > /dev/null
    retCode=$(echo $?)
    echo $retCode
}

function test_awsConnectivity_vml {
    mosquitto_sub --cafile ~/omnisensr-apps/certs/rootCA.pem --cert ~/omnisensr-apps/certs/039f098178-certificate.pem.crt --key ~/omnisensr-apps/certs/039f098178-private.pem.key --insecure -q 1 -d -h A9BFTZDLDJ0ZJ.iot.us-east-1.amazonaws.com -p 8883 -I test_sub_102 -v -t data/# & sleep 10s && pkill -HUP mosquitto_sub > /dev/null
    retCode=$(echo $?)
    echo $retCode    
}

function pprint {
    # -- Print the result
    echo "----------------- SUMMARY ------------------"
    NUM=0

    while [ $NUM -lt 3  ];do
        if (( STATE_MON[$NUM]==1 )); then
            echo "${red}${bgr}STATE_MON[$NUM]=1 ==> Packet sniffing on mon$NUM is good.${reset}"
        else
            echo "${red}${bgr}STATE_MON[$NUM]=0 ==> Packet sniffing on mon$NUM is NOT good.${reset}"
        fi
        NUM=$(expr $NUM + 1)
    done
    echo "--------------------------------------------"
    
    if (( STATE_BOND0==1 )); then
            echo "${red}${bgr}STATE_BOND0 is good.${reset}"
    else
            echo "${red}${bgr}STATE_BOND0 is NOT good.${reset}"
    fi

    if (( STATE_MQTT==1 )); then
            echo "${red}${bgr}STATE_MQTT is good.${reset}"
    else
            echo "${red}${bgr}STATE_MQTT is NOT good.${reset}"
    fi

    if (( STATE_BEACON==1 )); then
            echo "${red}${bgr}STATE_BEACON is good.${reset}"
    else
            echo "${red}${bgr}STATE_BEACON is NOT good.${reset}"
    fi
    printf "\n"

    # -- Summary

    if (( STATE_MON[0]==1 && STATE_MON[1]==1 && STATE_MON[2]==1 && STATE_BOND0==1 && STATE_MQTT==1 && STATE_BEACON==1)); then
            echo "${red}${bgr}Congratulations! QA Testing for Wi-Fi Sniffer, Beacon, and MQTT has PASSED!!${reset}"
    else
            echo "${red}${bgr}Oh, no.. QA Testing for Wi-Fi Sniffer, Beacon, and MQTT has FAILED!!${reset}"
    fi
    echo "--------------------------------------------"
    printf "\n\n"
}

case "$1" in
    checkformons)
       checkMons
    ;;
    bluetooth)
        test_bluetooth
        ;;
    mqtt)
        test_mqtt
        ;;
    wifi)
        test_wifiSniffer
        test_bondedInterface
        ;;
    aws)
        retCode=$(test_awsConnectivity)
        echo $retCode
        ;;
    aws-vml)
        retCode=$(test_awsConnectivity_vml)
        echo $retCode
        ;;
    *)
        checkMons
        test_bluetooth
        test_mqtt
        test_wifiSniffer
        test_bondedInterface
        pprint
        ;;
esac

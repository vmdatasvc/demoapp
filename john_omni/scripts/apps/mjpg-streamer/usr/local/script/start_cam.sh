#!/bin/bash
# Author: Gary Panulla + Dilip Hari call tail to truncate the settings file to just the last line change to
# the root directory get the last known setting for the camera and restart it using those parametes
function timestamp() {
  local dt=$(date +"%D %T")
  echo "$dt"
}
function start_cam {
ts=$(timestamp)
params=($(< /usr/local/www/camera_settings.txt))
echo "$ts Launch Camera x=${params[0]} y=${params[1]} fps=${params[2]}" >> /var/log/raspPi.log
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/mjpg-streamer mjpg_streamer -o "output_http.so -w /usr/local/www" -i "input_raspicam.so -x ${params[0]} -y ${params[1]} -fps ${params[2]} -ex off -shutter 30000 -awb off -awbgainR 1.05 -awbgainB 2.5 -ISO 200 -quality 95" &
}

function stop_cam {
    ts=$(timestamp)
    echo "$ts Stopping Camera"
    echo "$ts Stopping Camera" >> /var/log/raspPi.log
    if [ "$(ps -ae | grep mjpg_streamer)" ]; then
        sudo killall mjpg_streamer
    fi
}
case "$1" in
  start)
    stop_cam
    start_cam
    ;;
  stop)
    stop_cam
    ;;
  restart)
    stop_cam
    start_cam
    ;;
  *)
    echo "Usage: sudo systemctl {start|stop|restart} OmniSensr_Service_Camera.service";
    exit 1
    ;;
esac
exit 0

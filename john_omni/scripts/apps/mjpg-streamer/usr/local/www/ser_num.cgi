#!/usr/bin/python

import cgi, os, sys, subprocess
import cgitb
cgitb.enable()
f=cgi.FieldStorage()
mac=f['mac'].value
sn=f['SN'].value
serNumPath= '/usr/local/www/serial_number.txt'
tempPath='/home/omniadmin/temp/store_name.txt'
triggerPath='/sys/class/leds/led0/trigger'
led0Path= '/sys/class/leds/led0/brightness'
led1Path= '/sys/class/leds/led1/brightness'

def checkExistence(filename):
    if not os.path.exists(os.path.dirname(filename)):
        try:
            os.makedirs(os.path.dirname(filename))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
checkExistence(tempPath)
checkExistence(serNumPath)
#checkExistence(led0Path)
#checkExistence(triggerPath)
#checkExistence(led1Path)
#creates local temp copy and move to the destination file
def writeAndMove(tempfile, filePath, key):
    with open(tempfile, 'w+') as file:
        if key == 'serial':
            file.write("MACAddress:%s\n" % mac)
            file.write("SerialNumber:%s" % sn)
        if key == 'trigger':
            file.write("none")
        if key == 'led0':
            file.write("1")
        if key == 'led1':
            file.write("0")
    file.close()
    check1=os.system("sudo cp %s %s" % (tempfile, filePath))

writeAndMove(tempPath, serNumPath, 'serial')
writeAndMove(tempPath, triggerPath, 'trigger')
writeAndMove(tempPath, led0Path, 'led0')
writeAndMove(tempPath, led1Path, 'led1')
print "Content-Type: text/plain"
print
print "MACAddress: %s" % mac
print "SerialNumber: %s" % sn
print '----------------------------------------------------------------------------------------------------------'
print "\n\nQA Test is running. Wait about 20 seconds, and see the very bottom of the messages to find the test result."
print '\n\n----------------------------------------------------------------------------------------------------------'
print '\n\n'
sys.stdout.flush()
popen=subprocess.Popen("/home/omniadmin/QA_test/_test_WiFi_n_Bluetooth.sh", stdout=subprocess.PIPE, shell=True)
results, err= popen.communicate()
print results

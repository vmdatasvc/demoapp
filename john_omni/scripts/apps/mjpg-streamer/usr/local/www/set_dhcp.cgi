#!/usr/bin/python

import cgi, os, subprocess

f=cgi.FieldStorage()
tmpFilename='/home/omniadmin/temp/temp_interface.txt'
ipFilename='/etc/network/interfaces'
templateFile='/home/omniadmin/OmniSensrOS/scripts/modules/interfaces'


check=os.system("sudo cp --remove-destination %s %s && /usr/local/www/getIp.cgi" % (templateFile, ipFilename))
popen=subprocess.Popen("grep -i denyinterfaces /etc/dhcpcd.conf | tail -1", stdout=subprocess.PIPE, shell=True)
deny, err= popen.communicate()
if deny=='':
    deny="set denyinterfaces"
    os.system("sudo printf \"\ndenyinterfaces eth0\n\" >> /etc/dhcpcd.conf")

if check == 0:
    print 'Status: 200 Ok'
    print
    print "Changed device to DHCP"
    os.system("sudo /etc/init.d/networking restart")
else:
    print "Status: 450"
    print
    print "Did not create the interface file"


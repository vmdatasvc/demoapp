#!/usr/bin/python

import cgi, os, sys
data = ''
for line in sys.stdin.readline():
    #print line
    data+= line
dir_path="/home/omniadmin/omnisensr-apps/certs"

filePath= os.path.join(dir_path,"certificate.pem.crt")
def checkExistence(filename):
    if not os.path.exists(os.path.dirname(filename)):
        try:
            os.makedirs(os.path.dirname(filename))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
checkExistence(filePath)

with open(filePath, 'w+') as file:
    file.write(data.replace('\r', '\n'))
file.close()
print "Status: 204 No Content"
print

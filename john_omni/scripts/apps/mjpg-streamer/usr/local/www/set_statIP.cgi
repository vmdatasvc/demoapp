#!/usr/bin/python

import cgi, os, subprocess

f=cgi.FieldStorage()
tmpFilename='/home/omniadmin/temp/temp_interface.txt'
ipFilename='/etc/network/interfaces'
templateFile='/home/omniadmin/OmniSensrOS/scripts/modules/interfaces'
def checkExistence(filename):
    if not os.path.exists(os.path.dirname(filename)):
        try:
            os.makedirs(os.path.dirname(filename))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
checkExistence(tmpFilename)

networkString=''
for line in open(templateFile, 'r').readlines():
    if line.startswith("iface eth0 inet"):
        networkString+="\niface eth0 inet static\n\naddress %s\nnetmask %s\ngateway %s\n\ndns-nameservers %s 208.67.222.222 8.8.8.8\n" % (f['IP'].value, f['subnet'].value, f['gway'].value, f['gway'].value)
    elif line.startswith("dns-nameservers"):
        networkString=networkString
    else:
        networkString+=line

with open(tmpFilename, 'w+') as file:
    file.write(networkString)
file.close()

check=os.system("sudo cp --remove-destination %s %s" % (tmpFilename, ipFilename))
popen=subprocess.Popen("grep -i denyinterfaces /etc/dhcpcd.conf | tail -1", stdout=subprocess.PIPE, shell=True)
deny, err= popen.communicate()
if deny=='':
    deny="set denyinterfaces"
    os.system("sudo printf \"\ndenyinterfaces eth0\n\" >> /etc/dhcpcd.conf")

if check == 0:
    print 'Status: 204 No Content'
    print
    print '-----------------------\n\n'
    print 'IP_Address:%s\nSubnet Mask:%s\nGateway:%s\nDeny:%s\n\n' % (f['IP'].value, f['subnet'].value, f['gway'].value, deny)
    print '-----------------------\n'

else:
    print "Status: 450"
    print
    print "Did not create the interface file"
#!/bin/bash
#settings.cgi?res=640x480&fps=15&hst=<host>&prt=<port#>&endit=1
saveIFS=$IFS
IFS='=&'
parm=($QUERY_STRING)
IFS='x'
res=(${parm[1]})
IFS=$saveIFS
echo "<html><head>"
echo "<!-- "
echo "setting the settings file" | sudo tee -a /var/log/raspPi.log
echo ${res[0]} ${res[1]} ${parm[3]} | sudo tee /usr/local/www/camera_settings.txt
echo " -->"
echo "<meta http-equiv='refresh' content='1; url=http://"${parm[5]}":"${parm[7]}"/imageSettings.html' />";
echo "</head><body >";
echo "New Camera settings: "${res[0]}" x "${res[1]}" at "${parm[3]}" fps";
echo "<br/>";
echo "VarName"${parm[5]}"<br/>";
echo "VarVal"${parm[7]};
echo "</body></html>";
sudo systemctl restart OmniSensr_Service_Camera.service
exit 0

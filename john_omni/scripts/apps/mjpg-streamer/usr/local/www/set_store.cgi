#!/usr/bin/python

import cgi, os
f=cgi.FieldStorage()
tmpFilenameStore='/home/omniadmin/temp/store_name.txt'
tmpFilenameDevice='/home/omniadmin/temp/dev_name.txt'
storeFile='/usr/local/www/'
deviceFile='/usr/local/www/'

def checkExistence(filename):
    if not os.path.exists(os.path.dirname(filename)):
        try:
            os.makedirs(os.path.dirname(filename))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise

checkExistence(tmpFilenameStore)
checkExistence(tmpFilenameDevice)

with open(tmpFilenameStore, 'w+') as file:
    file.write(f['Retailer'].value + '/' + f['storenum'].value)
file.close()
with open(tmpFilenameDevice, 'w+') as file1:
    file1.write(f['camName'].value)
file1.close()

#move to correct directory
s1="sudo mv %s %s" % (tmpFilenameStore, storeFile)
s2="sudo mv %s %s" % (tmpFilenameDevice, deviceFile)
check1=os.system(s1)
check2=os.system(s2)

if check1 == 0 & check2 == 0:
    print 'Status: 204 No Content'
    print
    print '<html> <body> <p>' + f['Retailer'].value + '/' + f['storenum'].value + 'device:' + f['camName'].value + '</p></body></html>'
else:
    print "Status: 450"
    print



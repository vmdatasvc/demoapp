#!/usr/bin/python

import cgi, os
f=cgi.FieldStorage()

zone=f['zone'].value

command_zone="sudo cp /usr/share/zoneinfo/US/%s /etc/localtime" % zone
check=os.system(command_zone)

if check == 0:
    print 'Status: 204 No Content'
    print
else:
    print "Status: 450"
    print

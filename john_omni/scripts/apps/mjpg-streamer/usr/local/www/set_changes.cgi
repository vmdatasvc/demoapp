#!/usr/bin/python
import os, cgi
f=cgi.FieldStorage()
tmpFilenameHostname='/home/omniadmin/temp/hostname'
tmpFilenameHosts='/home/omniadmin/temp/hosts'
hostnameFile='/etc/'
hostsFile='/etc/'
os.system("sudo sync")
reboot = ''
def changeHost():
    with open('/usr/local/www/dev_name.txt', 'r') as device:
        dev_name= device.read()
    device.close()
    with open('/etc/hostname', 'r') as fileHostname:
	currentHost= fileHostname.read()
    fileHostname.close()
    if dev_name != currentHost:
	#write hostname
	with open(tmpFilenameHostname, 'w+') as fileHostname:
	    fileHostname.write(dev_name)
	fileHostname.close()
	check1=os.system("sudo mv %s %s" % (tmpFilenameHostname, hostnameFile))
	#write hosts
	with open('/etc/hosts', 'r') as fileHost, open(tmpFilenameHosts, 'w+') as output:
	    hostString=fileHost.read()
	    if 'raspberrypi' in hostString.lower():
		hostString=hostString.replace("raspberrypi", dev_name)
            else:
		if 'cam' in hostString.lower():
		    hostN=hostString.lower().index('cam')
		    hostString=hostString[:hostN] + "" + dev_name
		elif 'omni' in hostString.lower():
		    hostN=hostString.lower().index('omni')
		    hostString=hostString[:hostN] + "" + dev_name
		elif 'spare' in hostString.lower():
		    hostN=hostString.lower().index('spare')
	 	    hostString=hostString[:hostN] + "" + dev_name
	    output.write(hostString)
	fileHost.close()
	output.close()
	check2=os.system("sudo mv %s %s &  mv %s %s" % (tmpFilenameHosts, hostsFile, tmpFilenameHostname, hostnameFile))
reboot= ''
network=''
for i in f:
    if i == 'reboot':
        reboot=f['reboot'].value
    if i == 'network':
        network=f['network'].value

if network == 'y':
    print 'Status: 200 Ok'
    print
    print 'Restarted Networking Service'
    os.system("/usr/local/www/getIp.cgi && sudo /etc/init.d/networking restart")
elif reboot == 'y':
    print 'Status: 200 Ok'
    print
    print 'Device is rebooting ... '    
    changeHost()
    os.system("sudo shutdown -r")
elif reboot == 'n':
    print 'Status: 200 Ok'
    print    
    print "device is shutting down .."
    changeHost()
    os.system("sudo shutdown -h now")
elif reboot=='only':
    print 'Status: 200 Ok'
    print
    print 'Device is rebooting ...'
    os.system("sudo shutdown -r")

else:
    print 'Status: 200 Ok'
    print
    print 'Device has been synced, reboot or shutdown not selected'
    


#!/bin/bash
function timestamp() {
    local dt=$(date +"%D %T")
    echo "$dt"
}
function start_ir {
    ts=$(timestamp)
    echo "$ts Starting OmniSensr_IR" | sudo tee -a /var/log/raspPi.log
    cd /home/omniadmin/omnisensr-apps/
    nohup ./OmniSensr_IR_Controller &
    echo $! | sudo tee /var/run/omnisensr_ir.pid
}

function stop_ir {
    ts=$(timestamp)
    echo "$ts Stopping OmniSensr_IR" | sudo tee -a /var/log/raspPi.log
    if [ -f /var/run/omnisensr_ir.pid ]; then
      myPID=$(cat /var/run/omnisensr_ir.pid);sudo kill -SIGTERM $myPID
    fi
}

case "$1" in
    start)
        stop_ir
        start_ir
        ;;
    stop)
        stop_ir
        ;;
    restart)
        stop_ir
        start_ir
        ;;
    *)
        echo "Usage sudo systemctl {start|stop|restart} OmniSensr_Service_IR.service"
        exit 1
        ;;
esac

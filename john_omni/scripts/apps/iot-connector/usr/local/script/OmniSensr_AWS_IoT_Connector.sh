#!/bin/bash
function timestamp() {
  local dt=$(date +"%D %T")
  echo "$dt"
}
function start_AWS_IoT {
    ts=$(timestamp)
    echo "$ts Starting OmniSensr_AWS_IoT_Connector" || sudo tee -a /var/log/raspPi.log
    cd /home/omniadmin/omnisensr-apps/
    nohup ./OmniSensr_AWS_IoT_Connector &
    echo $! |sudo tee /var/run/omnisensr_iot_connector.pid
}

function stop_AWS_IoT {
    ts=$(timestamp)
    echo "$ts Stopping OmniSensr_AWS_IoT_Connector" || sudo tee -a /var/log/raspPi.log
    if [ -f /var/run/omnisensr_iot_connector.pid ]; then
      myPID=$(cat /var/run/omnisensr_iot_connector.pid);sudo kill -SIGTERM $myPID
    fi
}
case "$1" in
  start)
    stop_AWS_IoT
    start_AWS_IoT
    ;;
  stop)
    stop_AWS_IoT
    ;;
  restart)
    stop_AWS_IoT
    start_AWS_IoT
    ;;
  *)
    echo "Usage: sudo systemctl {start|stop|restart} OmniSensr_AWS_IoT_Connector.service";
    exit 1
    ;;
esac
exit 0

#!/bin/bash
function timestamp() {
  local dt=$(date +"%D %T")
  echo "$dt"
}
function start_vision {
    ts=$(timestamp)
    echo "$ts Starting OmniSensr_Service_Vision" || sudo tee -a /var/log/raspPi.log
    cd /home/omniadmin/omnisensr-apps/
    nohup ./OmniSensr_Vision_Tracker &
    echo $! |sudo tee /var/run/omnisensr_vision.pid
}

function stop_vision {
    ts=$(timestamp)
    echo "$ts Stopping OmniSensr_Service_Vision" || sudo tee -a /var/log/raspPi.log
    if [ -f /var/run/omnisensr_vision.pid ]; then
      myPID=$(cat /var/run/omnisensr_vision.pid);sudo kill -SIGTERM $myPID
    fi
}
case "$1" in
  start)
    stop_vision
    start_vision
    ;;
  stop)
    stop_vision
    ;;
  restart)
    stop_vision
    start_vision
    ;;
  *)
    echo "Usage: sudo systemctl {start|stop|restart} OmniSensr_Service_Vision.service";
    exit 1
    ;;
esac
exit 0

#!/bin/bash

# -- Remove any existing monitoring interfaces
ifdown bond0

THE_MONS=($(airmon-ng |grep mon |cut -c 1-4))
for x in "${THE_MONS[@]}"; do
  foo=$(/usr/local/sbin/airmon-ng stop ${x})
  echo "Stopping $x"
done

# and find Ralink wlan interfaces
UP_WLAN_INTERFACE=($(ifconfig |grep wlan |cut -c 1-5))
WLAN_INTERFACE=($(airmon-ng |grep Ralink |grep wlan |cut -c 1-5))

# save the listen and inject NICs to a file (to be parsed by WiFiSniffer)
NIC_FILE=/usr/local/www/nic.txt
echo -e "listen=bond0\ninject=$(IFS=,;echo "${WLAN_INTERFACE[*]}")" > $NIC_FILE

array_contains2 () { 
    local array="$1[@]"
    local seeking=$2
    local in=1
    for element in "${!array}"; do
        if [[ $element == $seeking ]]; then
            in=0
            break
        fi
    done
    return $in
}

to_down() {
ifdown $1
echo $1 to down
}

for i in ${WLAN_INTERFACE[@]}; do
  array_contains2 UP_WLAN_INTERFACE ${i} && to_down ${i} || echo "${i} was not up"
done

#for i in ${WLAN_INTERFACE[@]}; do
#  foo=$(iwconfig ${i} mode monitor)
#  echo "$i monitor mode"
#done


#ifconfig wlan0 down
#ifconfig wlan1 down
#ifconfig wlan2 down

#iwconfig wlan0 mode monitor
#iwconfig wlan1 mode monitor
#iwconfig wlan2 mode monitor

#sleep 5s

THE_CHANNELS=(1 6 11)
NUM=0
for i in ${WLAN_INTERFACE[@]}; do
  foo=$(airmon-ng start ${i} ${THE_CHANNELS[$NUM]})  
  echo "start $i on channel ${THE_CHANNELS[$NUM]}"
  NUM=$(expr $NUM + 1)
done

NUM=0
for i in ${WLAN_INTERFACE[@]}; do
  # specify a channel for the wlan interface
  foo=$(iwconfig ${i} channel ${THE_CHANNELS[$NUM]}) 
  # set the wlan to monitor mode
  mon_out=$(iwconfig ${i} mode monitor)
  echo "iwconfig $i on channel ${THE_CHANNELS[$NUM]}"
  NUM=$(expr $NUM + 1)
done

# -- Create monitoring interface (e.g., mon0, mon1, ...) with specific channels. But the channel may not be set properly
#/usr/local/sbin/airmon-ng start wlan0 1
#/usr/local/sbin/airmon-ng start wlan1 6
#/usr/local/sbin/airmon-ng start wlan2 11

# -- Make sure the monitoring channels are set differently and properly.
#iwconfig wlan0 channel 1
#iwconfig wlan1 channel 6
#iwconfig wlan2 channel 11

# -- Bond the monitoring interfaces together so that we can get the packets sniffed via three different Wi-Fi channels into a single stream (via bond0).

#sleep 7s

# -- Make sure the interfaces are up.
# -- NOTE: wlanX must be down down. Otherwise, we may miss some packets. Don't know why yet.
#ifconfig mon0 up
#ifconfig mon1 up
#ifconfig mon2 up
/sbin/ifenslave -f bond0 mon0 mon1 mon2
ifup bond0



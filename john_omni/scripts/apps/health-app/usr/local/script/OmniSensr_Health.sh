#!/bin/bash
function timestamp() {
  local dt=$(date +"%D %T")
  echo "$dt"
}
function start_health {
    ts=$(timestamp)
    echo "$ts Starting OmniSensr_Health" || sudo tee -a /var/log/raspPi.log
    cd /home/omniadmin/omnisensr-apps/
    nohup ./OmniSensr_Health_App &
    echo $! |sudo tee /var/run/omnisensr_health.pid
}

function stop_health {
    ts=$(timestamp)
    echo "$ts Stopping OmniSensr_Health" || sudo tee -a /var/log/raspPi.log
    if [ -f /var/run/omnisensr_health.pid ]; then
      myPID=$(cat /var/run/omnisensr_health.pid);sudo kill -SIGTERM $myPID
    fi
}
case "$1" in
  start)
    stop_health
    start_health
    ;;
  stop)
    stop_health
    ;;
  restart)
    stop_health
    start_health
    ;;
  *)
    echo "Usage: sudo systemctl {start|stop|restart} OmniSensr_Health.service";
    exit 1
    ;;
esac
exit 0

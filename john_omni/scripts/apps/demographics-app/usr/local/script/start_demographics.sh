#!/bin/bash

function timestamp() {
  local dt=$(date +"%D %T")
  echo "$dt"
}

function start_Demogrphcs {
    ts=$(timestamp)
    printf "%s: starting Demographics service\n" "$ts" | sudo tee -a /var/log/raspPi.log
    cd /home/omniadmin/omnisensr-apps;./OmniSensr_DMG_Recognizer &
}
function stop_Demogrphcs {
    ts=$(timestamp)
    printf "%s: stopping Demographics service\n" "$ts" | sudo tee -a /var/log/raspPi.log
	
    sudo killall OmniSensr_DMG_Recognizer
}

case "$1" in
  start)
    stop_Demogrphcs
    start_Demogrphcs
    ;;
  stop)
    stop_Demogrphcs
    ;;
  restart)
    stop_Demogrphcs
    start_Demogrphcs
    ;;
  *)
    echo "Usage: sudo systemctl {start|stop|restart|status} OmniSensr_Service_Demogrph.service\n";
    exit 1
    ;;
esac
exit 0
#!/bin/bash

function timestamp() {
  local dt=$(date +"%D %T")
  echo "$dt"
}

function start_RamDisk {
    ts=$(timestamp)
    printf "%s: Check For Existing Demographics Ram Disk\n\n" "$ts" | sudo tee -a /var/log/raspPi.log
    if [ ! -d /home/omniadmin/rd ]; then
      ts=$(timestamp)
      printf "%s: Create Demographics Ram Disk Directory\n" "$ts" | sudo tee -a /var/log/raspPi.log
      mkdir /home/omniadmin/rd
      sync
    fi
    RDISK=$(mount |grep /rd)
    printf "Size of RDISK=${#RDISK}\n"
    if [ ${#RDISK} == 0 ]; then
      ts=$(timestamp)
      printf "%s: Create Demographics Ram Disk \n" "$ts" | sudo tee -a /var/log/raspPi.log
      sudo mount -v –ttmpfs –osize=100M,mode=777 tmpfs /home/omniadmin/rd
    fi
}
function stop_RamDisk {
    ts=$(timestamp)
    printf "%s: Checking for Demographics Ram Disk\n" "$ts" | sudo tee -a /var/log/raspPi.log
    if [ -d /home/omniadmin/rd ]; then
        ts=$(timestamp)
        printf "%s: Removing Demographics Ram Disk\n" "$ts" | sudo tee -a /var/log/raspPi.log
        RDISK=$(mount |grep /rd)
        if [ ${#RDISK} != 0 ]; then
          sudo umount /home/omniadmin/rd/
        fi
      ts=$(timestamp)
      printf "%s: Removing Demographics Ram Disk Directory\n" "$ts" | sudo tee -a /var/log/raspPi.log
       rm -df /home/omniadmin/rd
       sync
    fi
}

case "$1" in
  start)
    start_RamDisk
    ;;
  stop)
    stop_RamDisk
    ;;
  *)
    echo "Usage: sudo {start_RamDisk|stop_RamDisk}\n";
    exit 1
    ;;
esac
exit 0

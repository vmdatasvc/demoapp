#----------------------------------------------------------------
# Generated CMake target import file for configuration "RELEASE".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "aws-cpp-sdk-rekognition" for configuration "RELEASE"
set_property(TARGET aws-cpp-sdk-rekognition APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(aws-cpp-sdk-rekognition PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libaws-cpp-sdk-rekognition.so"
  IMPORTED_SONAME_RELEASE "libaws-cpp-sdk-rekognition.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS aws-cpp-sdk-rekognition )
list(APPEND _IMPORT_CHECK_FILES_FOR_aws-cpp-sdk-rekognition "${_IMPORT_PREFIX}/lib/libaws-cpp-sdk-rekognition.so" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)

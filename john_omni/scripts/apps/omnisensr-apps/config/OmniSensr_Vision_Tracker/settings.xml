<?xml version="1.0" ?>
<settings version="1.0">
	<folder name="VisionVideoModule">
		<folder name="VideoBuffer">
			<param name="bufferSize" value="1" />
		</folder>
		<folder name="BackgroundSubtraction">
			<param name="historyLength" value="5000" >
				<description> Length of background learning history. </description>
			</param>
			<param name="varThreshold" value="24.f" >
				<description> Variance threshold for foreground pixel color. </description>
			</param>
			<param name="shadowDetection [bool]" value="1" >
				<description> If on, shadow detection is used. </description>
			</param>
			<param name="numTrainingFrame" value="40" >
				<description> Number of frames for initial background learning. </description>
			</param>
		</folder>

		<param name="blurSigmaRatioToPersonRadiusinPixel" value="0.07f" />
	</folder>
	<folder name="Camera">
		<description> Camera calibration parameters for perspective projectin and undistortion using OpenCV camera model </description>
		<!-- Axis camera params -->
		<param name="ccdHeight" value="0.0048">
			<description> Currently not used for OpenCV camera model. </description>
		</param>
		<param name="ccdWidth" value="0.0064" />
		<param name="distortionParameters" value="0.2315890601951167,-0.01014790881235929,0,0,0.0003533324593994937,0.54676298923663,254.4,254.4,640,480,1.0,0.45,0"/>
		<param name="zoomCompensateFactor" value="1.2f" >
			<description> For some mysterious reason, projected human shape cylinder doesn't coincide with metric units. This factor is to compensate that effect. This factor should be tuned per each camera setup. </description>
		</param>

		<param name="focalLength" value="0.0027" />
		<param name="height" value="3.7f">
		<description> Camera height </description>
		</param>
		<param name="personRadius" value="0.4f">
			<description> Radius of the person modelled with a cylinder. This is unitless. </description>
		</param>
		<param name="personHeight" value="1.7f" >
			<description> Height of the person modelled with a cylinder. This is unitless. </description>
		</param>
		<param name="circleIncrement" value="15.0f" >
			<description> Increment of angles for drawing circle in degree. </description>
		</param>
	</folder>
	<!-- param name="TrackPolygon" value="84,326,86,167,317,126,534,153,546,321,323,359,82,325" /-->
	<param name="TrackPolygon" value="0.196875,0.354167,0.187500,0.645833,0.515625,0.675000,0.812500,0.625000,0.800000,0.341667,0.478125,0.325000" />
	<param name="TrackPolygonVersion" value="1.0" />
	<folder name="VisionDetectorTarget">
		<param name="detectionOverlapWithTrackingTargetThreshold" value="0.1" />
	</folder>
	<folder name="VisionDetector">

		<param name="occupancyPercentileThreshold" value=".1f" >
			<description> Percentile threshold for the ratio of detected blob area to the estimated person shape area (currently track box) for target foreground activity verification. </description>
		</param>
		<param name="morphOpenSERadius" value="2"/>
		<param name="morphCloseSERadius" value="3" />
		<param name="blobToPersonAreaRatioThreshold" value="0.2f" />

		<param name="contourAreaMinThreshold" value="0.0005f" />
		<param name="highAreaRatioToHumanShape" value="1.5f" />
		<param name="lowAreaRatioToHumanShape" value="0.7f" />
		<param name="minAreaRatioToHumanShape" value="0.5f" />
		<param name="mergeMahalanobisDistanceThreshold" value="2.0f" />
	</folder>
	<folder name="VisionTarget">
		<param name="TargetVerificationProportionToTrackbox" value="0.1f">
			<description> Minimum proportion to the trackbox of the detected blob area to keep the track target active.
			</description>
		</param>
	</folder>
	<folder name="VisionTargetManager">
		<param name="inactiveTargetInterval" value="2.f">
			<description> Maximum time (seconds) an inactive target stays in the list. Any inactive target after this interval will be deleted. </description>
		</param>
		<param name="inactiveMapWindowRadiusProportion" value="0.1f" >
			<description> Radius of the inactive map window for searching for inactive targets proportional to the diagonal of the image. </description>
		</param>
		<param name="verifyTarget [bool]" value="1">
		<description> If true, target manager verifies if there is enough target detection acitivity in the newly updated target position.</description>
		</param>
	
		<param name="trajectoryLengthThreshold" value="20.f">
			<description> Threshold for filtering out short trajectories. This is proportion to the person shape radius in pixels. </description>
		</param>
		<param name="trajectoryDurationThreshold" value="4.f">
			<description> Threshold for filtering out short duration trajectories. Unit is seconds. </description>
		</param>
		<param name="newTargetMahalanobisDistanceThreshold" value="2.5f" />
	</folder>
	<folder name="BaseVisionTracker">
		<param name="trackingMethod" value="TrackingMethod_HIST_RGB" >
			<description> Tracking method enum. Check include/modules/vision/VisualTrackers.hpp for enum definition. </description>
		</param>
		<param name="searchWindowRatio" value="3.f" >
			<description> Ratio of search window to the target window. </description>
		</param>

		<param name="matchLikelihoodThreshold" value="2.5f" />
		<param name="reactivateLikelihoodThreshold" value="0.3f">
			<description> Threshold for reacquiring lost target. The likelihood is gaussian kernel density output. [0,1]  </description>
		</param>

		
		<!-- Kalman filter uncertainty unites are the person radius in pixels when the person shape evaluated at the center of image -->
		<!-- For example, 2 means 2*(person shape radius in pixels at the center) -->
		<param name="KalmanFilterInitialStateUncertaintyLoc" value="2.f">
			<description> Initial uncertainty for motion Kalman Filter </description>
		</param>
		<param name="KalmanFilterInitialStateUncertaintyVel" value="1.f" />
		<param name="KalmanFilterProcessNoiseLoc" value="2.f">
			<description> State transition noise for Kalman filter </description>
		</param>
		<param name="KalmanFilterProcessNoiseVel" value="1.f" />
		<param name="KalmanFilterMeasurementNoise" value="1.f">
			<description> Measurement noise for Kalman Filter </description>
		</param>

	</folder>

	<folder name="HSHistogramTracker">

		<param name="hBins" value="30" >
			<description> Length between camera coordinate frame and the floor. This is unitless </description>
		</param>
		<param name="sBins" value="32">
			<description> Radius of the person modelled with a cylinder. This is unitless. </description>
		</param>
		<param name="hRangeMin" value="0.f" >
			<description> Height of the person modelled with a cylinder. This is unitless. </description>
		</param>
		<param name="hRangeMax" value="180.f" >
			<description> Height of the person modelled with a cylinder. This is unitless. </description>
		</param>
		<param name="sRangeMin" value="10.f" >
			<description> Height of the person modelled with a cylinder. This is unitless. </description>
		</param>
		<param name="sRangeMax" value="255.f" >
			<description> Height of the person modelled with a cylinder. This is unitless. </description>
		</param>
	</folder>

	<folder name="RBChromTracker">
		<param name="kernelClusterRadius" value="2.f" />
	</folder>

	<folder name="CIELabTracker">
		<param name="kernelBandwidth" value=".35f" />
	</folder>

	<folder name="RGChromHistogramTracker">

		<param name="rgBins" value="30" >
			<description> Length between camera coordinate frame and the floor. This is unitless </description>
		</param>
		<param name="colorRangeMin" value="0.35f">
			<description> Radius of the person modelled with a cylinder. This is unitless. </description>
		</param>
		<param name="colorRangeMax" value="2.0f" >
			<description> Height of the person modelled with a cylinder. This is unitless. </description>
		</param>
	</folder>

	<folder name="RGBHistogramTracker">

		<param name="rgbBins" value="128" >
			<description> Length between camera coordinate frame and the floor. This is unitless </description>
		</param>
		<param name="colorRangeMin" value="3.f">
			<description> Radius of the person modelled with a cylinder. This is unitless. </description>
		</param>
		<param name="colorRangeMax" value="254.f" >
			<description> Height of the person modelled with a cylinder. This is unitless. </description>
		</param>
	</folder>

	<folder name="HybridHistogramTracker">
		<param name="saturationLowThreshold" value="255.f">
			<description> Saturation threshold value to decide if target blob has enough color. </description>
		</param>
	</folder>

	<param name="dataReportRefreshInterval" value="2.f" />
	<param name="stateReportRefreshInterval" value="5.f" />
</settings>

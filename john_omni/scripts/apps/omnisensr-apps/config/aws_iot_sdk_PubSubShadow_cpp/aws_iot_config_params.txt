# -- AWS IoT Parameters

AWS_IOT_MQTT_HOST              A9BFTZDLDJ0ZJ.iot.us-east-1.amazonaws.com	 # -- < Customer specific MQTT HOST. The same will be used for Thing Shadow
AWS_IOT_MY_THING_NAME 		   TestDev-02	 		# -- < Thing Name of the Shadow this device is associated with
AWS_IOT_ROOT_CA_FILENAME       rootCA.pem	 		# -- < Root CA file name
AWS_IOT_CERTIFICATE_FILENAME   039f098178-certificate.pem.crt	 # -- < device signed certificate file name
AWS_IOT_PRIVATE_KEY_FILENAME   039f098178-private.pem.key		 # -- < Device private key filename
AWS_IOT_CERT_DIRECTORY			certs

# -- AWS_IOT_MQTT_PORT is already defined in aws_iot_config.h file (e.g., 8883) 	# -- < default port for MQTT/S
# -- AWS_IOT_MQTT_CLIENT_ID will be automatically set based on the thing name and pid.  # -- < MQTT client ID should be unique for every device
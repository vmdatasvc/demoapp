
# Interface to listen for packets on
# by default, read from the file
#NIC bond0

# Number of ms to wait before sending a "packet storm"
DEVICE_SILENCE_INTERVAL 250
# Number of seconds before pruning a device
TIME_THRESHOLD_DEVICE_DISAPPEAR 300
# When true, do not use beacon packets (typically sent by a router)
ONLY_NON_BEACON_PACKETS 1
# When true, only use probe request packets 
ONLY_BROADCAST_PACKETS 1
# Summarize all packets for a channel into a single measurment
USE_MEASUREMENT_SUMMRIZATION 0

# Comma-separated NICs to use for packet injection
# by default, read from the file
#INJECTION_NICS wlan0,wlan1,wlan2
# Number of ms between injection bursts
CALIB_PACKET_INJECTION_INTERVAL 250
# Maximum number of ms to add to the packet injection itnerval (Unif. random)
CALIB_RANDOM_BACKOFF_INTERVAL 50

NIC_FILENAME /usr/local/www/nic.txt

# Number of seconds between heartbeats
HEARTBEAT_INTERVAL 60
# Number of seconds between device cleanups
CLEANUP_INTERVAL 30
# Max number of ms to wait for new data on the sniffing interface
PROCESSING_INTERVAL 100


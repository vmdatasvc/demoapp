#!/bin/bash

# Find the current directory and execute the parallel library files
CURR_SOURCE=${BASH_SOURCE}
# First, check to see if we have a directory in BASH_SOURCE
# If not (i.e., was run via "bash script.sh", assume the current directory
if test "${BASH_SOURCE%/*}" = "${CURR_SOURCE}"; then
	CURR_SOURCE="./${CURR_SOURCE}"
fi

# Now, use the directory in "CURR_SOURCE"
CURR_SCRIPT_DIR="$(dirname "${CURR_SOURCE%/*}/x")"

for d in opencv torch3 dlib figtree; do
	# remove the directory
	sudo rm -rf ${CURR_SCRIPT_DIR}/../apps/third-party-libs/$d
	# Execute the "build and install library" script
	${CURR_SCRIPT_DIR}/$d.sh
done

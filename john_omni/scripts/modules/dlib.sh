#!/bin/bash


# Build script for OpenCV on a Raspberry Pi

CURR_SOURCE=${BASH_SOURCE}
# First, check to see if we have a directory in BASH_SOURCE
# If not (i.e., was run via "bash kickoff.sh", assume the current directory
if test "${BASH_SOURCE%/*}" = "${CURR_SOURCE}"; then
	CURR_SOURCE="./${CURR_SOURCE}"
fi

# Now, use the directory in "CURR_SOURCE"
CURR_SCRIPT_DIR="$(dirname "${CURR_SOURCE%/*}/x")"

# OPTIONAL: install to the given location
# default is the pre-compiled location of the repo
INSTALL_TARGET=$(realpath "${CURR_SCRIPT_DIR}/../apps/third-party-libs/dlib")
if test "$1"; then
	INSTALL_TARGET="$1"
fi

OLDDIR=$(pwd)
# go to the opencv directory
cd ${CURR_SCRIPT_DIR}/../../external

# initialize the opencv submodule
git submodule init dlib
git submodule update dlib

cd dlib

sudo apt-get install -y libopenblas-dev liblapack-dev

# create and build the repo
mkdir build
cd build
# jpeg support causes problems, especially with warnings...
# Solution: Don't use dlib to process JPEGs
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX="$INSTALL_TARGET" -DDLIB_NO_GUI_SUPPORT=ON -DDLIB_JPEG_SUPPORT=OFF ..

# dlib needs to build non-parallel
make

sudo make install

# clean up the build directory
cd ..
rm -rf build

cd $OLDDIR

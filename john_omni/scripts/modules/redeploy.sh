#!/bin/bash

#set -x

# Script to redeploy an OmniSensr, using the MAC address to find the 
# serial number, store, location and camera name

# install mysql-client needed below
sudo apt-get install -y mysql-client

# Get the interface name used for the default gateway
INAME=$(route -n | awk '{print $3 "\t" $8}' | grep '^0.0.0.0' | head -1 | cut -f2)

if test "x$INAME" = "x"; then
	echo "Could not determine default interface"
	exit 1
fi

# Now, get the MAC address
MACADDR=$(ifconfig $INAME | grep HWaddr | sed -e 's/.*HWaddr//' -e 's/://g' -e 's/ //g')

if test ${#MACADDR} -ne 12; then
	echo "Incorrect length for MAC address"
	exit 2
fi

# build the s/n query
TRACKER_HOST="vmomnisensrdb.videomining.com"
TRACKER_USER="omnidb"
TRACKER_PW="VMomni2017"
TRACKER_DB="omnisensr"

SN_QUERY="SELECT distinct(serial_number) FROM cam_proj_status join camera_inventory on cam_proj_status.cam_id = camera_inventory.id where mac_address = '$MACADDR';"

# Build the hostname/store query
OUTPUT=$(mysql -h $TRACKER_HOST -u $TRACKER_USER --password=$TRACKER_PW -D $TRACKER_DB -B --column-names=FALSE < <(echo "$SN_QUERY") 2>/dev/null) 

N_SN=$(echo "$OUTPUT" | wc -l)

if test -z "$OUTPUT"; then
	echo "No serial number found"
	exit 3
elif test $N_SN -gt 1; then
	echo "Multiple serial numbers found!"
	exit 4
fi

DEVF_LOC="/usr/local/www"

# OK, now ready to make some files
if test ! -d $DEVF_LOC; then
	echo "WARNING: $DEVF_LOC did not exist!"
	sudo mkdir -p $DEVF_LOC
fi

echo -e "MACAddress:$MACADDR\nSerialNumber:$OUTPUT" | sudo tee $DEVF_LOC/serial_number.txt

DEPLOY_QUERY="SELECT lower(camera_name), project_name FROM cam_proj_status join camera_inventory on cam_proj_status.cam_id = camera_inventory.id where mac_address = '$MACADDR' and status_type_id=1;"
OUTPUT=$(mysql -h $TRACKER_HOST -u $TRACKER_USER --password=$TRACKER_PW -D $TRACKER_DB -B --column-names=FALSE < <(echo "$DEPLOY_QUERY") 2>/dev/null) 
N_DEPLOY=$(echo "$OUTPUT" | wc -l)
if test -z "$OUTPUT"; then
	echo "No deployment information found"
	exit 5
elif test $N_DEPLOY -gt 1; then
	echo "Multiple deployment information found!"
	exit 6
fi

HOSTNAME=$(echo "$OUTPUT" | cut -f1)
PROJ_NAME=$(echo "$OUTPUT" | cut -f2)
STORE=$(echo "$PROJ_NAME" | sed -e 's/_.*//' -e 's|-|/|')

echo -n "Device_Name:"
echo "$HOSTNAME" | sudo tee $DEVF_LOC/dev_name.txt
echo -n "Store_Name:"
echo "$STORE" | sudo tee $DEVF_LOC/store_name.txt

# set the hostname
OLDHOST=$(hostname)
echo $HOSTNAME | sudo tee /etc/hostname > /dev/null
sudo sed -i "s/127.0.1.1.*$OLDHOST/127.0.1.1\t$HOSTNAME/g" /etc/hosts
sudo hostname $HOSTNAME

TZ_QUERY="SELECT distinct(timezone) FROM post_deploy.site_detail join cert_details on site_detail.cert_id=cert_details.id where live_name='$PROJ_NAME' and timezone is not null;"
TZ=$(mysql -h $TRACKER_HOST -u $TRACKER_USER --password=$TRACKER_PW -D $CERT_DB -B --column-names=FALSE < <(echo "$TZ_QUERY") 2>/dev/null)

N_TZ=$(echo "$TZ" | wc -l)
if test "$TZ" -a $N_TZ -eq 1; then
	# Set the timezone to a canonical tzdata representation by adding "US/" if no slash is present
	TZ=$(echo "$TZ" | sed '/\//!s:^:US/:')

	if test -f "/usr/share/zoneinfo/$TZ" ; then
		echo "$TZ" | sudo tee /etc/timezone >/dev/null
		sudo ln -sfb /usr/share/zoneinfo/$TZ /etc/localtime
	fi
fi

CERT_DB="post_deploy"
PEM_QUERY="SELECT certPem FROM site_detail join cert_details on site_detail.cert_id=cert_details.id where live_name='$PROJ_NAME';"
KEY_QUERY="SELECT keyPriv FROM site_detail join cert_details on site_detail.cert_id=cert_details.id where live_name='$PROJ_NAME';"
ROOT_QUERY="SELECT certPem FROM cert_details where certId is null;"

PEM=$(mysql -h $TRACKER_HOST -u $TRACKER_USER --password=$TRACKER_PW -D $CERT_DB -B --column-names=FALSE < <(echo "$PEM_QUERY") 2>/dev/null)
KEY=$(mysql -h $TRACKER_HOST -u $TRACKER_USER --password=$TRACKER_PW -D $CERT_DB -B --column-names=FALSE < <(echo "$KEY_QUERY") 2>/dev/null)  

N_PEM=$(echo "$PEM" | wc -l)
N_KEY=$(echo "$KEY" | wc -l)

if test ! -d $CERT_LOC; then
	echo "WARNING: $CERT_LOC did not exist"
	mkdir -p $CERT_LOC
	
	if test ! -d $CERT_LOC; then
		echo "ERROR: Could not create $CERT_LOC"
		exit 9
	fi
fi

# Check for the existence of a root CA - if not present, get it from the DB
if test ! -f "$CERT_LOC/rootCA.pem"; then
	# get the root CA
	ROOTCA=$(mysql -h $TRACKER_HOST -u $TRACKER_USER --password=$TRACKER_PW -D $CERT_DB -B --column-names=FALSE < <(echo "$ROOT_QUERY") 2>/dev/null)
	N_ROOT=$(echo "$ROOTCA" | wc -l)
	if test -z "$ROOTCA"; then
		echo "ERROR: Could not find root CA!"
		exit 10
	elif test $N_ROOT -gt 1; then
		echo "ERROR: Found more than one root CA!"
		exit 11
	fi
	
	echo -e "$ROOTCA" > $CERT_LOC/rootCA.pem
fi

if test -z "$PEM" -o "$KEY"; then
	echo "Could not find key or certificate"
	exit 7
elif test $N_PEM -gt 1 -o $N_KEY -gt 1; then
	echo "Found more than one key or certificate"
	exit 8
fi

CERT_LOC="/home/omniadmin/omnisensr-apps/certs"



echo -e "$PEM" > $CERT_LOC/certificate.pem.crt
echo -e "$KEY" > $CERT_LOC/private.pem.key


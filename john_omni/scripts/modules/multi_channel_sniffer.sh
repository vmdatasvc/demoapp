#!/bin/bash

CURR_SOURCE=${BASH_SOURCE}
# First, check to see if we have a directory in BASH_SOURCE
# If not (i.e., was run via "bash kickoff.sh", assume the current directory
if test "${BASH_SOURCE%/*}" = "${CURR_SOURCE}"; then
	CURR_SOURCE="./${CURR_SOURCE}"
fi

# Now, use the directory in "CURR_SOURCE"
CURR_SCRIPT_DIR="$(dirname "${CURR_SOURCE%/*}/x")"

echo "Installing lshw"
sudo apt-get install -y lshw
# disable DMI. not doing so will result in huge packet loss
echo "Disabling DMI"
sudo lshw -disable dmi

# this is part of install_sniffer.sh
sudo apt-get install -y tcpdump iw aircrack-ng

# NOTE: remove references to /usr/local and just assume we can get to the aircrack binaries

if ! [ -d /etc/aircrack-ng ]; then
    sudo mkdir /etc/aircrack-ng
fi
sudo cp --remove-destination ${CURR_SCRIPT_DIR}/../../scripts/modules/airodump-ng-oui.txt /etc/aircrack-ng/airodump-ng-oui.txt


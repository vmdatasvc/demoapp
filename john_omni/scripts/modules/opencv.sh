#!/bin/bash


# Build script for OpenCV on a Raspberry Pi

CURR_SOURCE=${BASH_SOURCE}
# First, check to see if we have a directory in BASH_SOURCE
# If not (i.e., was run via "bash kickoff.sh", assume the current directory
if test "${BASH_SOURCE%/*}" = "${CURR_SOURCE}"; then
	CURR_SOURCE="./${CURR_SOURCE}"
fi

# Now, use the directory in "CURR_SOURCE"
CURR_SCRIPT_DIR="$(dirname "${CURR_SOURCE%/*}/x")"

# OPTIONAL: install to the given location
# default is the pre-compiled location of the repo
INSTALL_TARGET=$(realpath "${CURR_SCRIPT_DIR}/../apps/third-party-libs/opencv")
if test "$1"; then
	INSTALL_TARGET="$1"
fi

OLDDIR=$(pwd)
# go to the opencv directory
cd ${CURR_SCRIPT_DIR}/../../external

# initialize the opencv submodule
git submodule init opencv
git submodule update opencv

cd opencv

# First, install some dependencies
sudo apt-get install -y cmake software-properties-common libjpeg-dev libtiff-dev libjasper-dev libpng-dev libgstreamer1.0-dev build-essential libopenblas-dev liblapack-dev

# go to the repo commit

# create and build the repo
mkdir build
cd build
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX="$INSTALL_TARGET" -D ENABLE_NEON=ON -D ENABLE_VFPV3=ON -D WITH_TBB=ON -D BUILD_TBB=ON ..
make -j$(nproc)

sudo make install

# clean up the build directory
cd ..
rm -rf build

cd $OLDDIR

red="$(tput setaf 1)"
bgr="$(tput setab 7)"
reset="$(tput sgr 0)"

echo "${red}${bgr}Activating the bluetooth dongle${reset}"
sudo hciconfig up 
sudo hciconfig noleadv
#Set the device to "advertise and not-connectable" 
sudo hciconfig leadv3
sudo hciconfig noscan
echo "${red}${bgr}DONE${reset}"

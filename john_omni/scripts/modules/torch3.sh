#!/bin/bash


# Build script torch3 on a Raspberry Pi

CURR_SOURCE=${BASH_SOURCE}
# First, check to see if we have a directory in BASH_SOURCE
# If not (i.e., was run via "bash kickoff.sh", assume the current directory
if test "${BASH_SOURCE%/*}" = "${CURR_SOURCE}"; then
	CURR_SOURCE="./${CURR_SOURCE}"
fi

# Now, use the directory in "CURR_SOURCE"
CURR_SCRIPT_DIR="$(dirname "${CURR_SOURCE%/*}/x")"

# OPTIONAL: install to the given location
# default is the pre-compiled location of the repo
INSTALL_TARGET=$(realpath "${CURR_SCRIPT_DIR}/../apps/third-party-libs/torch3")
if test "$1"; then
	INSTALL_TARGET="$1"
fi

OLDDIR=$(pwd)
# go to the opencv directory
cd ${CURR_SCRIPT_DIR}/../../external/torch3
# go to the repo commit

# create and build the repo
mkdir build
cd build
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX="$INSTALL_TARGET" ..
make -j$(nproc)

sudo make install

# clean up the build directory
cd ..
rm -rf build

cd $OLDDIR

red="$(tput setaf 1)"
bgr="$(tput setab 7)"
reset="$(tput sgr 0)"

# Use APT repository and mosquitto PPA
# Install the Mosquitto PPA

echo "${red}${bgr}Adding Mosquitto PPA${reset}"

TMPDIR=$(mktemp -d)
cd $TMPDIR
wget https://repo.mosquitto.org/debian/mosquitto-repo.gpg.key
sudo apt-key add mosquitto-repo.gpg.key
cd -
rm -rf $TMPDIR
cd /etc/apt/sources.list.d
wget https://repo.mosquitto.org/debian/mosquitto-jessie.list -O - | sudo tee ./mosquitto-jessie.list > /dev/null
cd -

sudo apt-get update

echo "${red}${bgr}Installing Mosquitto${reset}"

sudo apt-get install -y mosquitto mosquitto-clients
sudo systemctl start mosquitto
sudo systemctl enable mosquitto

echo "${red}${bgr}Done${reset}"

#!/bin/bash

# Script to set up an EC2 instance.


# Get the directory of the script
CURR_SOURCE=${BASH_SOURCE}
# First, check to see if we have a directory in BASH_SOURCE
# If not (i.e., was run via "bash kickoff.sh", assume the current directory
if test "${BASH_SOURCE%/*}" = "${CURR_SOURCE}"; then
        CURR_SOURCE="./${CURR_SOURCE}"
fi

# Now, use the directory in "CURR_SOURCE"
CURR_SCRIPT_DIR="$(dirname "${CURR_SOURCE%/*}/x")"

# Add the mosquitto PPA
sudo apt-add-repository ppa:mosquitto-dev/mosquitto-ppa


AGD_BASE="$CURR_SCRIPT_DIR/../apt-get_dependencies";

# Install packages for the base and EC2-specific
for ext in '.txt' '-ec2.txt'; do
	AGD_FILE="${AGD_BASE}${ext}"
	if test -f "$AGD_FILE"; then
		sudo apt-get install -y $(sed 's/#.*//' "$AGD_FILE" | tr '\n' ' ')
	fi
done

# Install packages compiled from source
for e in $(ls "$CURR_SCRIPT_DIR/../external"); do

	echo "Installing $e"

	# First check for procompiled source
	if test -d "$CURR_SCRIPT_DIR/compiled/$e"; then
		cp -rf "$CURR_SCRIPT_DIR/compiled/$e" /usr/local 
	
	# Then check for a custom ec2-specific build/install script
	elif test -f "$CURR_SCRIPT_DIR/scripts/$e.sh"; then
		$CURR_SCRIPT_DIR/scripts/$e.sh /usr/local

	# If all else fails, revert to the raspberry-pi script and hope it works
	elif test -f "$CURR_SCRIPT_DIR/../scripts/modules/$e.sh"; then
		$CURR_SCRIPT_DIR/../scripts/modules/$e.sh /usr/local
	else
		echo "Could not find a suitable installer for external package $e"
	fi
done

# Add /usr/local to the ldconfig
echo "/usr/local" | sudo tee /etc/ld.so.conf.d/vml.conf > /dev/null
sudo ldconfig

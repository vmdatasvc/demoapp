#!/bin/bash


CURR_SOURCE=${BASH_SOURCE}
# First, check to see if we have a directory in BASH_SOURCE
# If not (i.e., was run via "bash kickoff.sh", assume the current directory
if test "${BASH_SOURCE%/*}" = "${CURR_SOURCE}"; then
    CURR_SOURCE="./${CURR_SOURCE}"
fi

# Now, use the directory in "CURR_SOURCE"
CURR_SCRIPT_DIR="$(dirname "${CURR_SOURCE%/*}/x")"

# OPTIONAL: install to the given location
# default is /usr/local
INSTALL_TARGET=/usr/local
if test "$1"; then
   INSTALL_TARGET="$1"
fi

CURRDIR=$(pwd)

# install prereqs
sudo apt-get install -y libcurl4-openssl-dev libssl-dev uuid-dev zlib1g-dev libpulse-dev

cd $CURR_SCRIPT_DIR/../../external
git submodule init aws-sdk-cpp
git submodule update aws-sdk-cpp

cd aws-sdk-cpp

mkdir build
cd build

cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX="$INSTALL_TARGET" -D BUILD_ONLY="core;rekognition" ..
make

sudo make install

cd ..
rm -rf build

cd $CURRDIR

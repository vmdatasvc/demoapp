#!/bin/bash
# --------------------------------------------------------------------------------------------------------------------------+
#                                             DO NOT MODIFY THIS BLOCK                                                      |
# --------------------------------------------------------------------------------------------------------------------------+
function timestamp() {
  local dt=$(date +"%D %T")
  echo "$dt"
}
ts=$(timestamp)
installed_ver=$(cat /usr/local/www/version.txt)
# gateway should always be called with the version number of the installed version of OmniSensrOS - typically from 
# omnisensr-runners/sync.sh. If gateway is called directly from bash terminal, it just prints an apprpriate message 
# in the log file.
printf "%s: Entering Gateway\n" "$ts"| sudo tee -a /var/log/gateway.log
if [ -z "$1" ]; then
    printf "%s: Running Gateway from the command line\n" "$ts"| sudo tee -a /var/log/gateway.log
else
    printf "%s: Upgrading OmniSensrOS from %s to %s\n" "$ts" "$installed_ver" "$1" | sudo tee -a /var/log/gateway.log
fi
ts=$(timestamp)
printf "\t%s: Stopping Health Service\n" "$ts" | sudo tee -a /var/log/gateway.log
sudo systemctl stop OmniSensr_Service_Health.service
# --------------------------------------------------------------------------------------------------------------------------+


# --------------------------------------------------------------------------------------------------------------------------+
#                                           Add upgrade code below                                                          |
# --------------------------------------------------------------------------------------------------------------------------+
ts=$(timestamp)
#Stopping Services
printf "\t%s: Stopping Services\n" "$ts" | sudo tee -a /var/log/gateway.log
sudo systemctl stop OmniSensr_Service_AWS_IoT.service
sudo systemctl stop OmniSensr_Service_Wifi.service
sudo systemctl stop OmniSensr_Service_WifiTrack.service
sudo systemctl stop OmniSensr_Service_Vision.service
sudo systemctl stop OmniSensr_Service_Demogrph.service
ts=$(timestamp)
printf "\t%s: Remove user pi" "$ts" | sudo tee -a /var/log/gateway.log
ts=$(timestamp)
ID=$(id -u pi)
if [ -z $ID ]; then
    printf "\t%s: User pi already removed\n" "$ts" | sudo tee -a /var/log/gateway.log
else
    printf "\t%s: User pi removed\n" "$ts" | sudo tee -a /var/log/gateway.log
    sudo userdel pi
fi

#Update mosquitto mqtt to version 1.4.11
ts=$(timestamp)
printf "\t%s: Update mosquitto mqtt to version 1.4.11" "$ts" | sudo tee -a /var/log/gateway.log
sudo systemctl stop mosquitto.service
scripts/modules/mqtt.sh
# redundant - mqtt.sh starts mosquitto
sudo systemctl start mosquitto.service

#Deploy latest AWS IoT Connector
ts=$(timestamp)
printf "\t%s: Deploy new AWS IoT Connector binary and any config files\n" "$ts" | sudo tee -a /var/log/gateway.log
./config.sh aws-iot-connector

ts=$(timestamp)
#Deploy update VM binaries
printf "\t%s: Deploy update VM binaries\n" "$ts" | sudo tee -a /var/log/gateway.log
if ! [ -d /home/omniadmin/omnisensr-apps ]; then
    mkdir /home/omniadmin/omnisensr-apps
fi
cp --remove-destination scripts/apps/omnisensr-apps/* /home/omniadmin/omnisensr-apps
chmod +x /home/omniadmin/omnisensr-apps/*

ts=$(timestamp)
#Deploy updated config files for binaries
printf "\t%s: Deploy updated config files for binaries\n" "$ts" | sudo tee -a /var/log/gateway.log
if ! [ -d /home/omniadmin/omnisensr-apps/config ]; then
    mkdir /home/omniadmin/omnisensr-apps/config
fi
cp -R --remove-destination scripts/apps/omnisensr-apps/config/* /home/omniadmin/omnisensr-apps/config

ts=$(timestamp)
#Deploy updated start_cam.sh
printf "\t%s: Deploy updated start_cam.sh\n" "$ts" | sudo tee -a /var/log/gateway.log
sudo cp --remove-destination scripts/apps/mjpg-streamer/usr/local/script/* /usr/local/script

ts=$(timestamp)
#Deploy updated start_sniffer.sh
printf "\t%s: Deploy updated start_sniffer.sh\n" "$ts" | sudo tee -a /var/log/gateway.log
sudo cp --remove-destination scripts/apps/sniffer/* /usr/local/script

ts=$(timestamp)
#Deploy updated MQTT.sh
# Now managed via APT - no need for this
#printf "\t%s: Deploy updated MQTT.sh\n" "$ts" | sudo tee -a /var/log/gateway.log
#sudo cp --remove-destination scripts/apps/mqtt/usr/local/script/* /usr/local/script

ts=$(timestamp)
#Make updated scripts executable
printf "\t%s: Make updated scripts executable\n" "$ts" | sudo tee -a /var/log/gateway.log
sudo chmod +x /usr/local/script/*

ts=$(timestamp)
#Deploy updated health app service file
printf "\t%s: Deploy updated health app service file\n" "$ts" | sudo tee -a /var/log/gateway.log
sudo cp --remove-destination scripts/apps/health-app/lib/systemd/system/* /lib/systemd/system

ts=$(timestamp)
#Deploy updated demogrph service file
printf "\t%s: Deploy updated demogrph service file\n" "$ts" | sudo tee -a /var/log/gateway.log
sudo cp --remove-destination scripts/apps/demographics-app/lib/systemd/system/* /lib/systemd/system

ts=$(timestamp)
#Deploy updated iot-connector service file
printf "\t%s: Deploy updated iot-connector service file\n" "$ts" | sudo tee -a /var/log/gateway.log
sudo cp --remove-destination scripts/apps/iot-connector/lib/systemd/system/* /lib/systemd/system

ts=$(timestamp)
#Deploy updated wifi packet processor service file
printf "\t%s: Deploy updated wifi packet processor service file\n" "$ts" | sudo tee -a /var/log/gateway.log
sudo cp --remove-destination scripts/apps/packet-processor/lib/systemd/system/* /lib/systemd/system

ts=$(timestamp)
#Deploy updated wifi tracker processor service file
printf "\t%s: Deploy updated wifi tracker processor service file\n" "$ts" | sudo tee -a /var/log/gateway.log
sudo cp --remove-destination scripts/apps/track-processor/lib/systemd/system/* /lib/systemd/system

ts=$(timestamp)
#Deploy updated vision service file
printf "\t%s: Deploy updated vision service file\n" "$ts" | sudo tee -a /var/log/gateway.log
sudo cp --remove-destination scripts/apps/vision-app/lib/systemd/system/* /lib/systemd/system

ts=$(timestamp)
#Deploy updated QA_Test script
printf "\t%s: Deploy updated QA_Test script\n" "$ts" | sudo tee -a /var/log/gateway.log
cp --remove-destination scripts/apps/QA_test/* /home/omniadmin/QA_test

ts=$(timestamp)
#Make updated scripts executable
printf "\t%s: Make updated QA script executable\n" "$ts" | sudo tee -a /var/log/gateway.log
chmod +x /home/omniadmin/QA_test/*

ts=$(timestamp)
#Reload systemctl daemon
sudo systemctl daemon-reload
printf "\t%s: Reload systemctl daemon\n" "$ts" | sudo tee -a /var/log/gateway.log

ts=$(timestamp)
printf "\t%s: Starting AWS IoT Connector Service\n" "$ts" | sudo tee -a /var/log/gateway.log
sudo systemctl start OmniSensr_Service_AWS_IoT.service

# --------------------------------------------------------------------------------------------------------------------------+
# IMPORTANT! - Force the updates to be written to the SDRam Card
ts=$(timestamp)
printf "\t%s: Write out cache to SDRam\n" "$ts" | sudo tee -a /var/log/gateway.log
sudo sync
printf "\tDone\n" | sudo tee -a /var/log/gateway.log
# --------------------------------------------------------------------------------------------------------------------------+

# --------------------------------------------------------------------------------------------------------------------------+
#                                           End of upgrade code block                                                       |
# --------------------------------------------------------------------------------------------------------------------------+


# --------------------------------------------------------------------------------------------------------------------------+
#                                             DO NOT MODIFY THIS BLOCK                                                      |
# --------------------------------------------------------------------------------------------------------------------------+
ts=$(timestamp)
printf "\t%s: Starting Health Service\n" "$ts" | sudo tee -a /var/log/gateway.log
sudo systemctl start OmniSensr_Service_Health.service
ts=$(timestamp)
printf "%s: Exiting Gateway\n" "$ts"| sudo tee -a /var/log/gateway.log
# --------------------------------------------------------------------------------------------------------------------------+
